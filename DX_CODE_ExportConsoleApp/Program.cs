﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
/*
 * Ben Bloodworth
 * 10/15/15
 * 
 * This program created the Dispatch Crude C.O.D.E. export file using the procedures and views from SQL server.  This program exists because SQL server
 * cannot export to a file without adding a large amount of blank characters that cannot be in this export.  Using this program creates the file per C.O.D.E.
 * spec so that it can be setup as a scheduled process and uploaded to the appropriate party wihout human interaction.
 * 
 * Since this program is directly related to dispatch crude I'm planning on zipping this project and including it in the Dispatch Crude code repository for easy access
 */
namespace DC_CODE_Export
{
    class Program
    {
        static void Main(string[] args)
        {
            //Get default user level variables from the config file in the same diretory as this application
            string appPath = AppDomain.CurrentDomain.BaseDirectory;

            if (File.Exists(appPath + "CODE_CONFIG.cfg"))
            {
                //Get values from config file
                loadUserVars(appPath);
            }
            else
            {
                //Create default config file
                createConfigFile(appPath);
            }
            
            

            //If no settings or invalid settings have been defined, do not continue
            if (!string.IsNullOrEmpty(Properties.Settings.Default.conStr)
                && !string.IsNullOrEmpty(Properties.Settings.Default.fileName)
                && !string.IsNullOrEmpty(Properties.Settings.Default.strSQL)
                && !string.IsNullOrEmpty(Properties.Settings.Default.exportFilePath)
                && !string.IsNullOrEmpty(Properties.Settings.Default.customerID)
                && !string.IsNullOrEmpty(Properties.Settings.Default.databaseName))
            {
                //DECLARATIONS
                string fileName = Properties.Settings.Default.exportFilePath + Properties.Settings.Default.fileName;
                List<string> qResult = new List<string>();
                SqlConnection conn = new SqlConnection(Properties.Settings.Default.conStr);
                SqlCommand command = new SqlCommand(Properties.Settings.Default.strSQL, conn);

                //open connection to the database
                conn.Open();

                //Run the command and get the contents
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    //while there is another record available
                    while (reader.Read())
                    {
                        //put the query result into a string array
                        //Console.WriteLine(reader[0].ToString());
                        qResult.Add(reader[0].ToString());
                    }
                }

                //close & dispose connection to the database
                conn.Close();
                conn.Dispose();

                //write query result to file
                using (StreamWriter sw = new StreamWriter(fileName))
                {
                    for (int i = 0; i < qResult.Count; i++)
                    {
                        sw.WriteLine(qResult[i].ToString());
                    }
                }
            }
            else
            {
                //write error log entry if no settings are defined in config file
                using (StreamWriter sw = File.AppendText(appPath + "DC_CODE_Export_ErrorLog.err"))
                {
                    sw.WriteLine(DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss") + " : No settings defined in config file or invalid settings.");
                }
            }
            
        }

        //If no config file exists, this creates the default config in the application's directory
        static void createConfigFile(string appPath)
        {             
            using (StreamWriter file = new StreamWriter(appPath + "CODE_CONFIG.cfg"))
            {
                file.WriteLine("# DC_CODE_Eport.exe config file");
                file.WriteLine("# Comments are denoted by the pound symbol at the beginning of a line and will be ignored");
                file.WriteLine("#");
                file.WriteLine("# Please use the format below.  Line numbers are counted from the first non-comment line and comment lines between are skipped.  Do not use quotation marks.");
                file.WriteLine("#");
                file.WriteLine("# Line 1 = Database to use (DispatchCrude.Dev, DispatchCrude.Test, etc.)");
                file.WriteLine("# Line 2 = Customer/Shipper ID to run export against (this is the CustomerID value in the database)");
                file.WriteLine("# Line 3 = File export path (X:\\this folder\\that folder\\)");
                file.WriteLine("# Line 4 = Custom/Unique file name prefix (CW2_, BVL_, xxx_, etc.)");
                file.WriteLine("#");
                file.WriteLine("# NOTE: this program employs only minimal error checking so incorrect settings still have the potential to cause trouble. Be careful!");
            }
        }

        static void loadUserVars(string appPath)
        {
            //Get values from file
            using (StreamReader sr = new StreamReader(appPath + "CODE_CONFIG.cfg"))
            {
                string line;
                int i = 1;
                while ((line = sr.ReadLine()) != null)
                {
                    if (string.IsNullOrEmpty(line) || line.Substring(0, 1) == "#")
                    {
                        //Do nothing, # starts a comment line
                    }
                    else
                    {
                        //Set the appropriate variable
                        switch (i)
                        {
                            case 1:
                                //Set database name and then the connection string
                                if (!string.IsNullOrEmpty(line))
                                {
                                    Properties.Settings.Default.databaseName = line;
                                    Properties.Settings.Default.conStr = "Data Source=CTSDATA2;Initial Catalog="
                                        + Properties.Settings.Default.databaseName
                                        + ";Persist Security Info=True;User ID=sa;Password=Sp33dy0n3$";
                                }
                                i++;
                                break;
                            case 2:
                                //Set customer id and then the query to be run
                                if (!string.IsNullOrEmpty(line))
                                {
                                    Properties.Settings.Default.customerID = line;
                                    Properties.Settings.Default.strSQL = "EXEC spOrderExportFinalCustomer "
                                        + Properties.Settings.Default.customerID
                                        + ", 'DXEXPORT', 'CODE_STANDARD'";                                   //Add ", 0" to the end of this string to turn on "preview mode"
                                }
                                i++;
                                break;
                            case 3:
                                //Set export file path
                                if (!string.IsNullOrEmpty(line))
                                {
                                    Properties.Settings.Default.exportFilePath = line;
                                }
                                i++;
                                break;
                            case 4:
                                //Set file name
                                if (!string.IsNullOrEmpty(line))
                                {
                                    Properties.Settings.Default.fileName = line.Trim() + DateTime.Now.ToString("yyyyMMdd_HHmm") + ".txt";
                                }
                                i++;
                                break;
                            default:
                                //do nothing
                                break;
                        }
                    }
                }
            }
        }
    }
}
