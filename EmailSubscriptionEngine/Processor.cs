﻿using System;
using System.Net;
using System.Net.Mail;
using AlonsIT;
using System.Data;
using DispatchCrude.DataExchange;
using DispatchCrude.EmailSubscriptionEngine.Models;
using DispatchCrude.Extensions;

namespace DispatchCrude.EmailSubscriptionEngine
{
    public class Processor
    {
        public void Process()
        {
            DataTable dtSubscriptions = SupportEngine.RetrieveData(SupportEngine.RetrieveType.rtEmailSubscription);
            foreach (DataRow drSubscription in dtSubscriptions.Rows)
            {
                Subscription subscription = new Subscription(drSubscription);
                if (subscription.Enabled)
                    Process(subscription);
            }
        }

        private void Process(Subscription subscription)
        {
            string location = "ProcessSubscription";

            subscription.StartUTC = DateTime.UtcNow;
            try
            {
                location = "Setting DBName";
                // change the connection string to the specified database so the report will be generated properly
                (SSDB.ConnectionFactory as ConnFactory_WinForm).DBName = subscription.DBName;

                location = "Instantiating ReportCenterExcelExport";
                ReportCenterExcelExport generator = new ReportCenterExcelExport(subscription.UserName, null, subscription.UserReportID);

                location = "Generating Report Content";
                subscription.AttachmentStream = generator.Export();
                // ensure the stream is reset to the beginning
                subscription.AttachmentStream.Seek(0, 0);

                // generate and send the email
                location = "Sending Email";
                subscription.AddResult(SendEmail(subscription));
            }
            catch (Exception ex)
            {
                subscription.AddResult("Error @" + location + ": " + ex.Message);
            }
            subscription.EndUTC = DateTime.UtcNow;

            // log execution
            LogExecution(subscription);
        }

         //return null if successful(no error) or an error string if failure occurred
        private string SendEmail(Subscription subscription)
        {
            string ret = null;

            try
            {
                new CTSEmail(subscription.EmailAddressCSV
                        , "DC Report Subscription Email - " + subscription.FileName
                        , Core.Settings.Value(Core.Settings.SettingsID.EmailSubscriptionHtmlBody).ToString()
                        , true)
                    .Attach(subscription.AttachmentStream, subscription.FileName)
                    .Send();
            }
            catch (Exception ex)
            {
                ret = ex.Message;
            }

            return ret;
        }
 
        private void LogExecution(Subscription subscription)
        {
            using (SSDB db = new SSDB())
            {
                db.ExecuteSql(@"INSERT INTO tblUserReportEmailSubscriptionLog(UserName, UserReportID, Name, EmailAddressCSV, ExecutionDateUTC, ExecutionDurationSeconds, Successful, Errors)
                    VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7})"
                    , DBHelper.QuoteStr(subscription.UserName)
                    , subscription.UserReportID
                    , DBHelper.QuoteStr(subscription.FileName)
                    , DBHelper.QuoteStr(subscription.EmailAddressCSV)
                    , DBHelper.QuoteStr(subscription.StartUTC)
                    , subscription.DurationSeconds
                    , (subscription.Errors.Count == 0) ? 1 : 0
                    , DBHelper.QuoteStr(subscription.ErrorCSV));
            }
        }
    }
}
