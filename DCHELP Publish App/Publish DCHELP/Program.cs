﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Publish_DCHELP
{
    class Program
    {
        public static string CONFIG_FILENAME = "DCHELP_CONFIG.cfg";

        static void Main(string[] args)
        {
            //Get publish directories from the config file in the same diretory as this application
            string appPath = AppDomain.CurrentDomain.BaseDirectory;
            
            List<string> publishPaths = new List<string>();

            if (!File.Exists(appPath + CONFIG_FILENAME))
            {
                CreateConfigFile(appPath);  //Create default config file
                return;     //End program
            }
            
            //Get publish paths from config file
            GetPublishPaths(appPath, publishPaths);

            //Copy updated files to the specified paths
            PublishHelpFiles(publishPaths);

            return;
        }

        //Copy the help files from the temp directory to each path specified in the config file
        static void PublishHelpFiles(List<string> publishPaths)
        {
            //Get the source path (ALWAYS the first non-comment entry in the config file)
            string sourcePath = publishPaths.FirstOrDefault();

            //Delete the files in all the specified paths first (except for the web.config files)
            foreach (String path in publishPaths)
            {
                if (path != sourcePath)
                {
                    try
                    {
                        Directory.Delete(path + "css", true);
                        Directory.Delete(path + "img", true);
                        Directory.Delete(path + "js", true);
                        Directory.Delete(path + "lib", true);

                        string[] htmlFiles = Directory.GetFiles(path);
                        foreach (string htmlFile in htmlFiles)
                        {
                            if (htmlFile.Contains(".html"))
                                File.Delete(htmlFile);
                        }
                    }
                    catch
                    {
                        //Ignore errors (Unless this proves to cause an issue later)
                    }
                }
            }

            //Copy the files from the source to the specified path(s)
            string[] baseFiles = Directory.GetFiles(sourcePath);
            string[] cssFiles1 = Directory.GetFiles(sourcePath + "css\\");
            string[] cssFiles2 = Directory.GetFiles(sourcePath + "css\\dynatree\\");
            string[] cssFiles3 = Directory.GetFiles(sourcePath + "css\\dynatree\\chm\\");
            string[] cssFiles4 = Directory.GetFiles(sourcePath + "css\\dynatree\\folder\\");
            string[] cssFiles5 = Directory.GetFiles(sourcePath + "css\\dynatree\\vista\\");
            string[] cssFiles6 = Directory.GetFiles(sourcePath + "css\\silver-theme\\");
            string[] cssFiles7 = Directory.GetFiles(sourcePath + "css\\silver-theme\\images\\");
            string[] imgFiles = Directory.GetFiles(sourcePath + "img\\");
            string[] jsFiles = Directory.GetFiles(sourcePath + "js\\");
            string[] libFiles = Directory.GetFiles(sourcePath + "lib\\");
            string fileName;

            foreach (String path in publishPaths)
            {
                if (path != sourcePath)
                {
                    //Create the subdirectories 
                    Directory.CreateDirectory(path + "css\\");
                    Directory.CreateDirectory(path + "css\\dynatree\\");
                    Directory.CreateDirectory(path + "css\\dynatree\\chm\\");
                    Directory.CreateDirectory(path + "css\\dynatree\\folder\\");
                    Directory.CreateDirectory(path + "css\\dynatree\\vista\\");
                    Directory.CreateDirectory(path + "css\\silver-theme\\");
                    Directory.CreateDirectory(path + "css\\silver-theme\\images\\");

                    Directory.CreateDirectory(path + "img\\");
                    Directory.CreateDirectory(path + "js\\");
                    Directory.CreateDirectory(path + "lib\\");

                    //HTML FILES
                    foreach (string f in baseFiles)
                    {
                        fileName = Path.GetFileName(f);

                        File.Copy(f, Path.Combine(path, fileName), true);
                    }

                    //CSS FILES
                    foreach (string f in cssFiles1)
                    {
                        fileName = Path.GetFileName(f);

                        File.Copy(f, Path.Combine(path + "css\\", fileName), true);
                    }

                    foreach (string f in cssFiles2)
                    {
                        fileName = Path.GetFileName(f);

                        File.Copy(f, Path.Combine(path + "css\\dynatree\\", fileName), true);
                    }

                    foreach (string f in cssFiles3)
                    {
                        fileName = Path.GetFileName(f);

                        File.Copy(f, Path.Combine(path + "css\\dynatree\\chm\\", fileName), true);
                    }

                    foreach (string f in cssFiles4)
                    {
                        fileName = Path.GetFileName(f);

                        File.Copy(f, Path.Combine(path + "css\\dynatree\\folder\\", fileName), true);
                    }

                    foreach (string f in cssFiles5)
                    {
                        fileName = Path.GetFileName(f);

                        File.Copy(f, Path.Combine(path + "css\\dynatree\\vista\\", fileName), true);
                    }

                    foreach (string f in cssFiles6)
                    {
                        fileName = Path.GetFileName(f);

                        File.Copy(f, Path.Combine(path + "css\\silver-theme\\", fileName), true);
                    }

                    foreach (string f in cssFiles7)
                    {
                        fileName = Path.GetFileName(f);

                        File.Copy(f, Path.Combine(path + "css\\silver-theme\\images\\", fileName), true);
                    }

                    //IMG FILES
                    foreach (string f in imgFiles)
                    {
                        fileName = Path.GetFileName(f);

                        File.Copy(f, Path.Combine(path + "img\\", fileName), true);
                    }

                    //JS FILES
                    foreach (string f in jsFiles)
                    {
                        fileName = Path.GetFileName(f);

                        File.Copy(f, Path.Combine(path + "js\\", fileName), true);
                    }

                    //LIB FILES
                    foreach (string f in libFiles)
                    {
                        fileName = Path.GetFileName(f);

                        File.Copy(f, Path.Combine(path + "lib\\", fileName), true);
                    }
                }
            }
        }

        //Get the publish path(s) from the config file
        static void GetPublishPaths(string appPath, List<string> publishPaths)
        {
            //Get values from file
            using (StreamReader sr = new StreamReader(appPath + CONFIG_FILENAME))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    if (string.IsNullOrEmpty(line) || line.Substring(0, 1) == "#")
                    {
                        //Do nothing, # starts a comment line
                    }
                    else
                    {
                        publishPaths.Add(line);
                    }
                }

            }            
        }

        //If no config file exists, this creates the default config in the application's directory
        static void CreateConfigFile(string appPath)
        {
            using (StreamWriter file = new StreamWriter(appPath + CONFIG_FILENAME))
            {
                file.WriteLine("# Publish_DCHLEP.exe config file");
                file.WriteLine("# Comments are denoted by the pound symbol at the beginning of a line and will be ignored");
                file.WriteLine("#");
                file.WriteLine("# Line numbers are counted from the first non-comment line and comment lines between are skipped.  Do not use quotation marks.");
                file.WriteLine("#");
                file.WriteLine("# The first uncommented line is ALWAYS the source path.");
                file.WriteLine("# Please put the full path to each publish folder below the source path, one per line. (The backslash \\ is required at the end of the path)");                
                file.WriteLine("# Example:    X:\\this folder\\that folder\\");                
                file.WriteLine("#");
            }
        }
    }
}
