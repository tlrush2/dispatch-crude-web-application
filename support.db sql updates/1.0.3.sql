-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20) = '1.0.2'
	, @NewVersion varchar(20) = '1.0.3'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'Add PROCEDURE spOrderProcessStatusChangesAccomplishAll'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/*********************************************
Creation Info: 1.0.3 - 2016/07/06
Author: Kevin Alons
Description: execute spOrderProcessStatusChangesAccomplish for each DB (including DEV if parameter is set)
*********************************************/
CREATE PROCEDURE spOrderProcessStatusChangesAccomplishAll AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @sql varchar(max)
	SELECT @sql = value FROM tblSetting WHERE ID = 1
	-- retrieve the list of eligible databases
	DECLARE @DBs TABLE (db varchar(100))
	INSERT INTO @DBs EXEC (@sql)

	DECLARE @sqlBase varchar(max) = 'IF (OBJECT_ID(''[%DBNAME%].dbo.spOrderProcessStatusChangesAccomplish'') IS NOT NULL) EXEC (''[%DBNAME%].dbo.spOrderProcessStatusChangesAccomplish'')'
		, @dbName varchar(100)

	-- define the output table
	DECLARE @ret TABLE (ID int, DBName varchar(100), UserReportID int, UserName varchar(100), EmailAddressCSV varchar(255), Name varchar(100))

	-- iterate over each eligible database and execute the spOrderProcessStatusChangesAccomplish PROCEDURE
	WHILE EXISTS (SELECT * FROM @DBs)
	BEGIN
		SELECT TOP 1 @dbName = db FROM @DBs
		SET @sql = replace(@sqlBase, '%DBNAME%', @dbName)
		PRINT 'EXEC(' + @sql + ')' 
		EXEC (@sql)

		DELETE FROM @DBs WHERE db = @dbName
	END
END
GO

COMMIT
SET NOEXEC OFF