-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20) = '1.0.4'
	, @NewVersion varchar(20) = '1.0.5'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'DCWEB-1945 Disable subscriptions from dev sites'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO




/*********************************************
Creation Info: 1.0.0 - 2016/04/20
Author: Kevin Alons
Description: retrieve all eligible UserReportEmailSubscription records for processing
Changes: 
	- 1.0.1		JAE		2016/06/07	Add day of week check to where clause
	- 1.0.5		JAE		2016/11/16	Filter DEV databases so subscription don't accidentally run
*********************************************/
ALTER PROCEDURE spEligibleUserReportEmailSubscriptions AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @sql varchar(max)

	SELECT @sql = 'SELECT * FROM (' + value + ') Q WHERE Name NOT IN (''DispatchCrude.Test'', ''DispatchCrude.Demo'', ''DispatchCrude.Dev'', ''DispatchCrude.BenDev'', ''DispatchCrude.JoeDev'', ''DispatchCrude.KevDev'')'
	FROM tblSetting WHERE ID = 1

	-- retrieve the list of eligible databases
	DECLARE @DBs TABLE (db varchar(100))
	INSERT INTO @DBs EXEC (@sql)

	-- define the base sql statement used to retrieve each database's eligible user report email subscriptions
	DECLARE @sqlBase varchar(max) = 'SELECT URES.ID, ''%DBNAME%'', URES.UserReportID, UserName, EmailAddressCSV, Name, Prefiltered = case when URCD.FilterOperatorID = 2 AND URCD.FilterValue1 IS NOT NULL then 1 else 0 end 
		FROM [%DBNAME%].dbo.tblUserReportEmailSubscription URES 
		LEFT JOIN [%DBNAME%].dbo.tblUserReportColumnDefinition URCD ON URCD.UserReportID = URES.UserReportID AND reportcolumnid = 4 
		WHERE ExecuteHourUTC = datepart(hour, getutcdate()) 
		AND CASE DATEPART(WEEKDAY, getutcdate()) WHEN 1 THEN Sun
                                         WHEN 2 THEN Mon
										 WHEN 3 THEN Tue
										 WHEN 4 THEN Wed
										 WHEN 5 THEN Thu
										 WHEN 6 THEN Fri
										 WHEN 7 THEN Sat END = 1'
		, @dbName varchar(100)

	-- define the output table
	DECLARE @ret TABLE (ID int, DBName varchar(100), UserReportID int, UserName varchar(100), EmailAddressCSV varchar(255), Name varchar(100), Prefiltered int)

	-- iterate over each eligible database and add its eligible report subscription data
	WHILE EXISTS (SELECT * FROM @DBs)
	BEGIN
		SELECT @dbName = min(db) FROM @DBs
		SET @sql = replace(replace(@sqlBase, '%DBNAME%', @dbName), '%DOW%', left(datename(weekday, getutcdate()), 3))
		-- add the records for this database to the output data
		INSERT INTO @ret EXEC (@sql)
		DELETE FROM @DBs WHERE db = @dbName
	END
	-- return the output data
	SELECT * FROM @ret
END
GO

COMMIT
SET NOEXEC OFF