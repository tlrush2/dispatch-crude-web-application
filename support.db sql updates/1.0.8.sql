-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20) = '1.0.7'
	, @NewVersion varchar(20) = '1.0.8'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'Include check to only query online databases (was causing app error)'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


-- Update the "DispatchCrude eligible databases sql" setting to only query online databases
UPDATE tblSetting
SET Name = 'DispatchCrude Site eligible databases sql' 
	, Value = ltrim(
		replace(
			'SELECT Name
				, Suffix = SUFFIX.Value
				/* the returned <type> will be either PROD or DEV */
				, type = CASE WHEN SUFFIX.Value LIKE "%DEV" OR SUFFIX.Value LIKE "Demo" OR SUFFIX.Value LIKE "Test" THEN "DEV" ELSE "PROD" END
			FROM sys.databases 
			/* get the <last> . delimited value of the database name */
			CROSS APPLY (SELECT TOP 1 Value FROM dbo.fnSplitString(Name, ".") WHERE isnumeric(Value) = 0 ORDER BY Pos DESC) SUFFIX
			WHERE ((name LIKE "DispatchCrude.%" AND name <> "DispatchCrude.NewCustomer") OR name LIKE "DispatchFuel.%")
			AND state = 0 /* online */', '"', '''')
		)
WHERE ID = 1
GO

COMMIT
SET NOEXEC OFF
