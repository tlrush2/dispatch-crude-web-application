SET NOEXEC OFF  

BEGIN TRANSACTION DBUPDATE

/* Create app changes table so that we can keep a log of changes to the support database just like the customer databases */
CREATE TABLE tblAppChanges(
	ID int IDENTITY(1,1) NOT NULL CONSTRAINT PK_AppChanges PRIMARY KEY CLUSTERED,
	VersionNum varchar(25) NOT NULL,
	ForPublic bit NOT NULL CONSTRAINT DF_AppChanges_ForPublic  DEFAULT ((0)),
	ChangeDate datetime NOT NULL CONSTRAINT DF_AppChanges_ChangeDate  DEFAULT (getdate()),
	ChangeDescription varchar(255) NOT NULL,
) 
GO

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT '1.0.0', 1, 'Initial email subscription feature implemented.'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

CREATE TABLE tblSetting
(
  ID int NOT NULL CONSTRAINT PK_Setting PRIMARY KEY
, Name varchar(100) NOT NULL CONSTRAINT UC_Setting_Name UNIQUE
, Value varchar(255) NOT NULL
, CreateDateUTC datetime NOT NULL CONSTRAINT DF_Setting_CreateDateUTC DEFAULT (getutcdate())
, CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_Setting_CreatedByUser DEFAULT ('System')
, LastChangeDateUTC datetime NULL
, LastChangedByUser varchar(100) NULL
)
GO

INSERT INTO tblSetting (ID, Name, Value) VALUES (0, 'Support DB Version', '1.0.0')
GO

INSERT INTO tblSetting (ID, Name, Value) 
	VALUES (1, 'UserReportEmailSubscription list SQL statement (SELECT name FROM master..sysdatabases WHERE ???)'
		, 'SELECT name FROM master..sysdatabases WHERE name LIKE ''DispatchCrude.%'' OR name LIKE ''DispatchFuel.%''')
GO

/*********************************************
Creation Info: 1.0.0 - 2016/04/20
Author: Kevin Alons
Description: retrieve all eligible UserReportEmailSubscription records for processing
*********************************************/
CREATE PROCEDURE spEligibleUserReportEmailSubscriptions AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @sql varchar(max)
	SELECT @sql = value FROM tblSetting WHERE ID = 1

	-- retrieve the list of eligible databases
	DECLARE @DBs TABLE (db varchar(100))
	INSERT INTO @DBs EXEC (@sql)

	-- define the base sql statement used to retrieve each database's eligible user report email subscriptions
	DECLARE @sqlBase varchar(max) = 'SELECT URES.ID, ''%DBNAME%'', URES.UserReportID, UserName, EmailAddressCSV, Name, Prefiltered = case when URCD.FilterOperatorID = 2 AND URCD.FilterValue1 IS NOT NULL then 1 else 0 end FROM [%DBNAME%].dbo.tblUserReportEmailSubscription URES LEFT JOIN [%DBNAME%].dbo.tblUserReportColumnDefinition URCD ON URCD.UserReportID = URES.UserReportID AND reportcolumnid = 4 WHERE ExecuteHourUTC = datepart(hour, getutcdate())'
		, @dbName varchar(100)

	-- define the output table
	DECLARE @ret TABLE (ID int, DBName varchar(100), UserReportID int, UserName varchar(100), EmailAddressCSV varchar(255), Name varchar(100), Prefiltered int)

	-- iterate over each eligible database and add it's eligible report subscription data
	WHILE EXISTS (SELECT * FROM @DBs)
	BEGIN
		SELECT @dbName = min(db) FROM @DBs
		SET @sql = replace(replace(@sqlBase, '%DBNAME%', @dbName), '%DOW%', left(datename(weekday, getutcdate()), 3))
		-- add the records for this database to the output data
		INSERT INTO @ret EXEC (@sql)
		DELETE FROM @DBs WHERE db = @dbName
	END
	-- return the output data
	SELECT * FROM @ret
END
GO


COMMIT
SET NOEXEC OFF