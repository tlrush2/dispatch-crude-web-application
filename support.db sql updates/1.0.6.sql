-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20) = '1.0.5'
	, @NewVersion varchar(20) = '1.0.6'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'Add Support for Driver Settlement Statement Emails and consolidate logic to simplify/reduce redundancy'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/* update the tblSetting table to allow an unlimited value string length */
ALTER TABLE dbo.tblSetting
	DROP CONSTRAINT DF_Setting_CreateDateUTC
GO
ALTER TABLE dbo.tblSetting
	DROP CONSTRAINT DF_Setting_CreatedByUser
GO
CREATE TABLE dbo.Tmp_tblSetting
(
	ID int NOT NULL,
	Name varchar(100) NOT NULL,
	Value nvarchar(MAX) NOT NULL,
	CreateDateUTC datetime NOT NULL,
	CreatedByUser varchar(100) NOT NULL,
	LastChangeDateUTC datetime NULL,
	LastChangedByUser varchar(100) NULL
)  
GO
ALTER TABLE dbo.Tmp_tblSetting ADD CONSTRAINT
	DF_Setting_CreateDateUTC DEFAULT (getutcdate()) FOR CreateDateUTC
GO
ALTER TABLE dbo.Tmp_tblSetting ADD CONSTRAINT
	DF_Setting_CreatedByUser DEFAULT ('System') FOR CreatedByUser
GO
IF EXISTS(SELECT * FROM dbo.tblSetting)
	 EXEC('INSERT INTO dbo.Tmp_tblSetting (ID, Name, Value, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser)
		SELECT ID, Name, CONVERT(varchar(MAX), Value), CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser FROM dbo.tblSetting WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.tblSetting
GO
EXECUTE sp_rename N'dbo.Tmp_tblSetting', N'tblSetting', 'OBJECT' 
GO
ALTER TABLE dbo.tblSetting ADD CONSTRAINT PK_Setting PRIMARY KEY CLUSTERED 
(
	ID
) 
GO
ALTER TABLE dbo.tblSetting ADD CONSTRAINT UC_Setting_Name UNIQUE NONCLUSTERED 
(
	Name
) 
GO

/* update the "DispatchCrude eligible databases sql" Setting to have a Suffix value and a type value */
update tblSetting set Name = 'DispatchCrude Site eligible databases sql' 
	, value = ltrim(
		replace(
			'SELECT Name
				, Suffix = SUFFIX.Value
				/* the returned <type> will be either PROD or DEV */
				, type = CASE WHEN SUFFIX.Value LIKE "%DEV" OR SUFFIX.Value LIKE "Demo" OR SUFFIX.Value LIKE "Test" THEN "DEV" ELSE "PROD" END
			FROM master..sysdatabases 
			/* get the <last> . delimited value of the database name */
			CROSS APPLY (SELECT TOP 1 Value FROM dbo.fnSplitString(Name, ".") WHERE isnumeric(Value) = 0 ORDER BY Pos DESC) SUFFIX
			WHERE (name LIKE "DispatchCrude.%" AND name <> "DispatchCrude.NewCustomer") OR name LIKE "DispatchFuel.%"', '"', '''')
		)
where ID = 1
go

delete from tblsetting where id in (2,3)
insert into tblSetting (ID, Name, Value)
	select 2
		, 'Driver Settlement Statement Email SQL BASE'
		, replace(
			'SELECT DBName = "%DBNAME%", ID, DriverSettlementStatementEmailDefinitionID, BatchID, PdfExportID, DriverID
				, EmailSubject = (SELECT EmailSubject FROM [%DBNAME%].dbo.tblDriverSettlementStatementEmailDefinition WHERE ID = DriverSettlementStatementEmailDefinitionID)
				, EmailAddress, CCEmailAddress
			FROM [%DBNAME%].dbo.tblDriverSettlementStatementEmail
			WHERE QueueEmailSend = 1', '"', '''')
	union 
	select 3
		, 'Email Subscription SQL BASE'
		, replace(
		N'SELECT URES.ID, DBName = "%DBNAME%", URES.UserReportID, UserName, EmailAddressCSV, Name
			, Prefiltered = CASE WHEN URCD.FilterOperatorID = 2 AND URCD.FilterValue1 IS NOT NULL THEN 1 ELSE 0 END 
		FROM [%DBNAME%].dbo.tblUserReportEmailSubscription URES 
		LEFT JOIN [%DBNAME%].dbo.tblUserReportColumnDefinition URCD ON URCD.UserReportID = URES.UserReportID AND ReportColumnID = 4 
		WHERE ExecuteHourUTC = datepart(hour, getutcdate()) 
		AND CASE DATEPART(WEEKDAY, getutcdate()) 
				WHEN 1 THEN Sun
                WHEN 2 THEN Mon
				WHEN 3 THEN Tue
				WHEN 4 THEN Wed
				WHEN 5 THEN Thu
				WHEN 6 THEN Fri
				WHEN 7 THEN Sat END = 1'
		, '"', '''')
go

if  (OBJECT_ID('fnSplitString') IS NOT NULL) drop function fnSplitString
go
/*********************************************
 Creation Info: 1.0.6 - 2017.05.07 - Kevin Alons
 Purpose: split a string based on the provided delimiter (based on http://stackoverflow.com/questions/2647/how-do-i-split-a-string-so-i-can-access-item-x)
 Changes: 
*********************************************/
CREATE FUNCTION fnSplitString
(
  @List NVARCHAR(MAX)
, @Delim VARCHAR(255)
)
RETURNS TABLE AS
    RETURN ( 
		SELECT Pos = ROW_NUMBER() OVER (ORDER BY Number), Value
		FROM ( 
			SELECT Value = LTRIM(
				RTRIM(
					SUBSTRING(@List
						, Number
						, CHARINDEX(@Delim, @List + @Delim, Number) - Number
						)
					)
				)
			, number
			FROM (
				SELECT Number = ROW_NUMBER() OVER (ORDER BY name)
				FROM sys.all_objects
			) x
			WHERE Number <= LEN(@List)
			AND SUBSTRING(@Delim + @List, Number, LEN(@Delim)) = @Delim
		) y
    );
go
grant select on fnSplitString to dispatchcrude_iis_acct
go

IF (OBJECT_ID('spExecuteDispatchCrudeDBSql') IS NOT NULL) DROP PROCEDURE spExecuteDispatchCrudeDBSql
GO
/*********************************************
Creation Info: 1.0.6 - 2016.05.07 - Kevin Alons
Purpose: retrieve a sql statement based on the provided baseSql value intersected with the eligible databases
Parameters: @sql - the base sql tblSetting.ID of the corresponding base sql statement record
			@typeLIKE - specify a different type LIKE value (PROD is default, DEV is all other databases)
			@suffixLIKE - specify a different suffix LIKE value (% is default, specify JOEDEV, etc to get a specific PROD/DEV db)
			@debug - specify 1 if you want the procedure to only generate the SQL to execute and return it (without execution)
Changes: 
*********************************************/
CREATE PROCEDURE spExecuteDispatchCrudeDBSql
(
  @settingID int
, @typeLIKE varchar(25) = 'PROD'
, @suffixLIKE varchar(25) = '%'
, @debug bit = 0
) AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @sqlDB varchar(max)
	SELECT @sqlDB = replace(
			'SELECT Name 
			FROM (' + Value + ') X 
			WHERE type LIKE "' + @typeLIKE + '" AND suffix LIKE "' + @suffixLIKE + '"'
		, '"', '''')
	FROM tblSetting 
	WHERE ID = 1
	
	/* retrieve the list of eligible databases */
	DECLARE @DBs TABLE (db varchar(100))
	INSERT INTO @DBs EXEC (@sqlDB)

	DECLARE @sqlBase nvarchar(max) = (SELECT Value FROM tblSetting WHERE ID = @settingID)

	/* generate the final OUTPUT sql statement */
	DECLARE @sqlRun nvarchar(max)
	SELECT @sqlRun = isnull(@sqlRun + '
		UNION ALL ', '') + replace(@sqlBase, '%DBNAME%', db)
	FROM @DBs

	IF (@debug = 1) -- return the statement to execute
		SELECT SQL = @SqlRun
	ELSE -- return the sql results
		EXEC sp_executesql @sqlRun
END

GO
grant execute on spExecuteDispatchCrudeDBSql to dispatchcrude_iis_acct
go

/*********************************************
Creation Info: 1.0.0 - 2016/04/20
Author: Kevin Alons
Description: retrieve all eligible UserReportEmailSubscription records for processing
Changes: 
	- 1.0.1		JAE		2016/06/07	Add day of week check to where clause
	- 1.0.5		JAE		2016/11/16	Filter DEV databases so subscription don't accidentally run
	- 1.0.6		KDA		2017.05.16	defensively code @sql so it doesn't use * and grab more data than will fit in the @DBs table variable
*********************************************/
ALTER PROCEDURE spEligibleUserReportEmailSubscriptions AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @sql varchar(max)

	SELECT @sql = 'SELECT Name FROM (' + value + ') Q WHERE Name NOT IN (''DispatchCrude.Test'', ''DispatchCrude.Demo'', ''DispatchCrude.Dev'', ''DispatchCrude.BenDev'', ''DispatchCrude.JoeDev'', ''DispatchCrude.KevDev'')'
	FROM tblSetting WHERE ID = 1

	-- retrieve the list of eligible databases
	DECLARE @DBs TABLE (db varchar(100))
	INSERT INTO @DBs EXEC (@sql)

	-- define the base sql statement used to retrieve each database's eligible user report email subscriptions
	DECLARE @sqlBase varchar(max) = 'SELECT URES.ID, ''%DBNAME%'', URES.UserReportID, UserName, EmailAddressCSV, Name, Prefiltered = case when URCD.FilterOperatorID = 2 AND URCD.FilterValue1 IS NOT NULL then 1 else 0 end 
		FROM [%DBNAME%].dbo.tblUserReportEmailSubscription URES 
		LEFT JOIN [%DBNAME%].dbo.tblUserReportColumnDefinition URCD ON URCD.UserReportID = URES.UserReportID AND reportcolumnid = 4 
		WHERE ExecuteHourUTC = datepart(hour, getutcdate()) 
		AND CASE DATEPART(WEEKDAY, getutcdate()) WHEN 1 THEN Sun
                                         WHEN 2 THEN Mon
										 WHEN 3 THEN Tue
										 WHEN 4 THEN Wed
										 WHEN 5 THEN Thu
										 WHEN 6 THEN Fri
										 WHEN 7 THEN Sat END = 1'
		, @dbName varchar(100)

	-- define the output table
	DECLARE @ret TABLE (ID int, DBName varchar(100), UserReportID int, UserName varchar(100), EmailAddressCSV varchar(255), Name varchar(100), Prefiltered int)

	-- iterate over each eligible database and add its eligible report subscription data
	WHILE EXISTS (SELECT * FROM @DBs)
	BEGIN
		SELECT @dbName = min(db) FROM @DBs
		SET @sql = replace(replace(@sqlBase, '%DBNAME%', @dbName), '%DOW%', left(datename(weekday, getutcdate()), 3))
		-- add the records for this database to the output data
		INSERT INTO @ret EXEC (@sql)
		DELETE FROM @DBs WHERE db = @dbName
	END
	-- return the output data
	SELECT * FROM @ret
END

GO

COMMIT
SET NOEXEC OFF

/* TEST CODE
exec spExecuteDispatchCrudeDBSql 2, 'dev', 'dev'
select * from [DispatchCrude.Dev].dbo.tblDriverSettlementStatementEmail where QueueEmailSend = 1
*/