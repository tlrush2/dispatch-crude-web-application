using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DispatchCrude.Core;

namespace DispatchCrude.Models
{
    [Table("tblTerminal")]
    public class Terminal : AuditModelDeleteBase, IValidatableObject
    {
        [Key]
        public int ID { get; set; }

        [Required]
        [DisplayName("Name")]
        [StringLength(50)]
        public string Name { get; set; }

        [DisplayName("LAT")]
        [RegularExpression(@"^[0-9\-\.]+$", ErrorMessage = "Invalid character(s) in Lattitude.")]
        public decimal? Lat { get; set; }

        [DisplayName("LON")]
        [RegularExpression(@"^[0-9\-\.]+$", ErrorMessage = "Invalid character(s) in Longitude.")]
        public decimal? Lon { get; set; }

        [DisplayName("Address")]
        [StringLength(50)]
        public string Address { get; set; }

        [DisplayName("City")]
        [StringLength(30)]
        public string City { get; set; }
                
        [DisplayName("State")]
        [UIHint("_ForeignKeyDDL")]
        public int? StateID { get; set; }
        [ForeignKey("StateID")]
        public State State { get; set; }

        [DisplayName("Zip")]
        [StringLength(10)]
        public string Zip { get; set; }
                
        [DisplayName("Office Phone")]
        [UIHint("PhoneNumber")]
        [StringLength(20)]
        public string OfficePhone { get; set; }

        [NotMapped]
        [DisplayName("GPS")]
        public GPS GPS
        {
            get
            {
                return new GPS(Lat, Lon);
            }
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var ret = new List<ValidationResult>();
            
            if (!ValidateDuplicateName(ID, Name))
                ret.Add(new ValidationResult("Terminal name already in use"));

            Validated = true;

            return ret;
        }

        public bool ValidateDuplicateName(int id, string name)
        {
            using (DispatchCrudeDB db = new DispatchCrudeDB())
            {
                return db.Terminals.Where(m => m.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase) && (id == 0 || m.ID != id)).Count() == 0;
            }
        }
    }
}
