using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using System.Web.Script.Serialization;
using System.Linq;
using DispatchCrude.Extensions;
using System.Collections.Generic;
using System.Web;

namespace DispatchCrude.Models
{
    public class DriverSettlementStatementEmailBase : AuditModelCreateBase
    {
        [Key]
        public int ID { get; set; }

        [RequiredGreaterThanZero]
        [DisplayName("Definition")]
        [UIHint("_ForeignKeyDDL")]
        public int DriverSettlementStatementEmailDefinitionID { get; set; }

        [RequiredGreaterThanZero]
        [DisplayName("Pdf Export")]
        [UIHint("_ForeignKeyDDL")]
        public int PdfExportID { get; set; }

        [RequiredGreaterThanZero]
        [DisplayName("Batch")]
        [UIHint("_ForeignKeyDDL", "HTML", "Table=DriverSettlementBatch")]
        public int BatchID { get; set; }

        [RequiredGreaterThanZero]
        [DisplayName("Driver")]
        [UIHint("_ForeignKeyDDL", "HTML", "Table=Driver", "ActiveOnly=true")]
        public int DriverID { get; set; }

        [DisplayName("Email Address(es)")]
        [StringLength(1000)]
        public string EmailAddress { get; set; }

        [DisplayName("CC Email"), StringLength(255)]
        public string CCEmailAddress { get; set; }

        [DisplayName("Queue Email Send?")]
        [UIHint("Boolean")]
        public bool QueueEmailSend { get; set; }

        [DisplayName("Email Date UTC")]
        [UIHint("Date")]
        public DateTime? EmailDateUTC { get; set; }

        [DisplayName("Emailed by")]
        [StringLength(100)]
        public string EmailedByUser { get; set; }

        [NotMapped]
        public override bool allowRowDeactivate
        {
            // never allow these rows to be deleted (this is a "log" table)
            get { return false; }
        }
    }

    [Table("tblDriverSettlementStatementEmail")]
    public class DriverSettlementStatementEmail : DriverSettlementStatementEmailBase
    {
        [DisplayName("Definition")]
        [ForeignKey("DriverSettlementStatementEmailDefinitionID")]
        public virtual DriverSettlementStatementEmailDefinition DriverSettlementStatementEmailDefinition { get; set; }

        [DisplayName("Pdf Export")]
        [ForeignKey("PdfExportID")]
        public virtual PdfExport PdfExport { get; set; }

        [DisplayName("Batch")]
        [ForeignKey("BatchID")]
        public virtual DriverSettlementBatch Batch { get; set; }

        [DisplayName("Driver")]
        [ForeignKey("DriverID")]
        public virtual Driver Driver { get; set; }

        [NotMapped]
        public DateTime? EmailDate
        {
            get
            {
                // use the user's timezone for the conversion
                return EmailDateUTC.HasValue ? Core.DateHelper.ToLocal(EmailDateUTC.Value, HttpContext.Current) : EmailDateUTC;
            }
        }

        static public void GenerateFromBatch(int definitionID, int batchID, string userName, bool queueEmailSend, int? pdfExportID = null)
        {
            using (AlonsIT.SSDB db = new AlonsIT.SSDB())
            {
                using (System.Data.SqlClient.SqlCommand cmd = db.BuildCommand("spDriverSettlementStatementEmail"))
                {
                    cmd.Parameters["@DefinitionID"].Value = definitionID;
                    cmd.Parameters["@BatchID"].Value = batchID;
                    cmd.Parameters["@UserName"].Value = userName;
                    cmd.Parameters["@QueueEmailSend"].Value = queueEmailSend;
                    cmd.Parameters["@PdfExportID"].Value = pdfExportID;
                    cmd.ExecuteNonQuery();
                }
            }
        }

    }
}
