﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchCrude.Models
{
    [Table("tblSettlementFactor")]
    public class SettlementFactor
    {
        [Key]
        public int ID { get; set; }

        [Required, StringLength(25)]
        public string Name { get; set; }

        [StringLength(25)]
        public string OrderUnitsField { get; set; }
    }
}
