﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace DispatchCrude.Models
{
    [Table("tblOrderSignature")]
    public class OrderSignature: AuditModelCreateBase
    {
        public OrderSignature()
        {
            this.UID = new Guid();
        }
        public OrderSignature(Guid uid)
        {
            this.UID = uid;
        }
        
        [Key]
        public int ID { get; set; }

        public Guid UID { get; set; }
        [DisplayName("Order")]
        public int? OrderID { get; set; }
        [DisplayName("Origin")]
        public int? OriginID { get; set; }
        [DisplayName("Destination")]
        public int? DestinationID { get; set; }
        [DisplayName("Driver")]
        public int? DriverID { get; set; }

        public byte[] SignatureBlob { get; set; }

        [ForeignKey("OrderID")]
        public virtual Order Order { get; set; }
        [ForeignKey("OriginID")]
        public virtual Origin Origin { get; set; }
        [ForeignKey("DestinationID")]
        public virtual Destination Destination { get; set; }
        [ForeignKey("DriverID")]
        public virtual Driver Driver { get; set; }

        [NotMapped]
        public string SignatureBlobSrc
        {
            get
            {
                string fileName = (OriginID.HasValue ? "Origin" : "Destination") + (DriverID.HasValue ? "Driver" : "");
                return SignatureBlob.Length == 0 ? "" : string.Format("data:image/{1};base64,{0}", Convert.ToBase64String(SignatureBlob), "png");
            }
        }
    }
}