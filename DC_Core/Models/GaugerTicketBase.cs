﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DispatchCrude.Models.Sync.GaugerApp
{
    public class GaugerTicketBase: AuditModelDeleteUTCBase
    {
        //public int? ID { get; set; }
        [Key]
        public Guid UID { get; set; }
        public int OrderID { get; set; }
        public string CarrierTicketNum { get; set; }
        public int? OriginTankID { get; set; }
        public string TankNum { get; set; }
        public int TicketTypeID { get; set; }
        public byte? BottomFeet { get; set; }
        public byte? BottomInches { get; set; }
        public byte? BottomQ { get; set; }
        public decimal? ProductObsGravity { get; set; }
        public decimal? ProductObsTemp { get; set; }
        public decimal? ProductBSW { get; set; }
        public byte? OpeningGaugeFeet { get; set; }
        public byte? OpeningGaugeInch { get; set; }
        public byte? OpeningGaugeQ { get; set; }
        public decimal? ProductHighTemp { get; set; }
        public bool Rejected { get; set; }
        public int? RejectReasonID { get; set; }
        public string RejectNotes { get; set; }
        public string SealOff { get; set; }
        public string SealOn { get; set; }
        public string DispatchConfirmNum { get; set; }
        public bool FromMobileApp { get; set; }
    }
}