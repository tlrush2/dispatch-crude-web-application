﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DispatchCrude.Models
{
    [Table("tblUserReportDefinition")]
    public class UserReportDefinition : AuditModelLastChangeUTCBase
    {
        public int ID { get; set; }

        [Required, StringLength(50)]
        public string Name { get; set; }

        public int ReportID { get; set; }
        [ForeignKey("ReportID")]
        public virtual ReportDefinition Report { get; set; }

        public string UserNames { get; set; }
    }

}