﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using Newtonsoft.Json;

namespace DispatchCrude.Models
{
    [Table("tblOrderPhoto")]
    public class OrderPhoto: AuditModelCreateBase
    {
        public OrderPhoto()
        {
            this.UID = new Guid();
        }
        public OrderPhoto(Guid uid)
        {
            this.UID = uid;
        }
        
        [Key]
        public int ID { get; set; }

        public Guid UID { get; set; }
        [DisplayName("Order")]
        public int? OrderID { get; set; }
        [DisplayName("Origin")]
        public int? OriginID { get; set; }
        [DisplayName("Destination")]
        public int? DestinationID { get; set; }
        [DisplayName("PhotoType")]
        public byte? PhotoTypeID { get; set; }
        [DisplayName("Driver")]
        public int? DriverID { get; set; }

        public byte[] PhotoBlob { get; set; }

        [DisplayName("GPS Data")]
        public string GpsData { get; set; }

        [ForeignKey("OrderID")]
        public virtual Order Order { get; set; }
        [ForeignKey("OriginID")]
        public virtual Origin Origin { get; set; }
        [ForeignKey("DestinationID")]
        public virtual Destination Destination { get; set; }
        //[ForeignKey("PhotoTypeID")]
        //public virtual PhotoType PhotoType { get; set; }
        [ForeignKey("DriverID")]
        public virtual Driver Driver { get; set; }

        [NotMapped]
        public string OrderPhotoType
        {
            get { 
                if (PhotoTypeID == (int) PhotoType.TYPES.Reject)
                    return "Reject";
                else if (PhotoTypeID == (int) PhotoType.TYPES.Location && OriginID != null)
                    return "Origin";
                else if (PhotoTypeID == (int) PhotoType.TYPES.Location && DestinationID != null)
                    return "Destination";
                else
                    return "UNKNOWN";
            }
        }

        [NotMapped]
        public string PhotoBlobSrc
        {
            get
            {
                string fileName = (OriginID.HasValue ? "Origin" : "Destination") + (DriverID.HasValue ? "Driver" : "");
                return PhotoBlob.Length == 0 ? "" : string.Format("data:image/{1};base64,{0}", Convert.ToBase64String(PhotoBlob), "jpg");
            }
        }

        [NotMapped]
        public string GPSLocation
        {
            get
            {
                if (!string.IsNullOrEmpty(GpsData))
                {
                    dynamic json = JsonConvert.DeserializeObject(GpsData);
                    return json["GpsLatLon"].ToString();
                }

                return "";
            }
        }

        [NotMapped]
        public string PhotoDateUTC
        {
            get
            {
                if (!string.IsNullOrEmpty(GpsData))
                {
                    dynamic json = JsonConvert.DeserializeObject(GpsData);
                    return json["DateTime"].ToString();
                }
                
                return "";
            }
        }
    }
}