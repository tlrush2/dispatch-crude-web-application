﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using System.Web;

namespace DispatchCrude.Models
{
    [Table("aspnet_Users")]
    public class DCUser : IEDTO, IValidatableObject
    {
        public DCUser()
        {
            //this.UserId = Guid.NewGuid();
            this.Groups = new List<DCGroup>();            
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid UserId { get; set; }

        [Required]
        [DisplayName("Username")]
        [StringLength(256, MinimumLength = 3, ErrorMessage = "Name must be at least 3 characters long")]
        public string UserName { get; set; }

        [Required]
        public string LoweredUserName {
            get {
                    return UserName.ToLower();
                }
            set {
                    value = UserName.ToLower();
                }
        }

        public DateTime LastActivityDate { get; set; }
        
        //[NotMapped]
        //[DisplayName("Last User Activity")]
        //[DisplayFormat(DataFormatString = "{0:MM/dd/yyyy hh:mm tt}")]
        //public DateTime? LastActivityDateDisplay {
        //    get
        //    {
        //        return LastActivityDate.HasValue ? Core.DateHelper.ToLocal(LastActivityDate.Value, HttpContext.Current) : LastActivityDate;
        //    }
        //}

        public ICollection<DCGroup> Groups { get; set; }

        [ForeignKey("UserId")]
        public DCMembership Membership { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!ValidateDuplicateName(UserId, UserName))
                yield return new ValidationResult("Username is already in use");
            Validated = true;
        }

        private bool ValidateDuplicateName(Guid id, string name)
        {
            using (DispatchCrudeDB db = new DispatchCrudeDB())
            {
                return db.DCUsers.Where(u => u.UserName.Equals(name, StringComparison.CurrentCultureIgnoreCase) && (id == null || u.UserId != id)).Count() == 0;
            }
        }
    }
}
