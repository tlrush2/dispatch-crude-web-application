using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Script.Serialization;

namespace DispatchCrude.Models
{
    [Table("tblState")]
    public class State : AuditModelLastChangeBase
    {
        [Key]
        public int ID { get; set; }

        [Required, StringLength(2)]
        public string Abbreviation { get; set; }
        
        [Required, StringLength(30)]
        public string FullName { get; set; }

        [Required, DisplayName("Country")]
        public int CountryID { get; set; }

        [ForeignKey("CountryID")]
        public virtual Country Country { get; set; }
    }
}
