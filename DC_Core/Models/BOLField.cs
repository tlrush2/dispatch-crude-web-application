using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DispatchCrude.Models
{
    [Table("tblCustomBOLFields")]
    public class BOLField : AuditModelLastChangeBase
    {
        public BOLField()
        {   
        }        

        [Key]
        public int ID { get; set; }

        [Required, StringLength(100)]
        [DisplayName("MVC Field Name")]
        public string DataField { get; set; }

        [Required, StringLength(100)]
        [DisplayName("Field Name")]
        public string InternalLabel { get; set; }

        [Required, StringLength(100)]
        [DisplayName("Display Label")]
        public string DataLabel { get; set; }

        [Required]
        [DisplayName("Show on BOL?")]
        public bool DisplayOnBOL { get; set; }

        [DisplayName("Sort Order")]
        public int? SortNum { get; set; }
    }
}
