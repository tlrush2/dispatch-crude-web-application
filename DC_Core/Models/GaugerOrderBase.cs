﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using AlonsIT;
using AlonsIT.ExtensionMethods;

namespace DispatchCrude.Models.Sync.GaugerApp
{
    public class GaugerOrderBase: AuditModelLastChangeUTCBase
    {
        #region Fields

        [Key]
        public int OrderID { get; set; }
        public int StatusID { get; set; }
        public DateTime? ArriveTimeUTC { get; set; }
        public DateTime? DepartTimeUTC { get; set; }
        public bool Rejected { get; set; }
        public int? RejectReasonID { get; set; }
        public string RejectNotes { get; set; }
        public bool Handwritten { get; set; }
        public DateTime? PrintDateUTC { get; set; }
        public string GaugerNotes { get; set; }

        #endregion
    }
}