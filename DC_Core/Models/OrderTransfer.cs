using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DispatchCrude.Models
{
    [Table("tblOrderTransfer")]
    public class OrderTransfer: AuditModelLastChangeUTCBase
    {
        [StringLength(255)]
        public string Notes { get; set; }

        [Key, ForeignKey("Order")]
        public int OrderID { get; set; }

        [ForeignKey("OrderID")]
        public virtual Order Order { get; set; }

        public int OriginDriverID { get; set; }
        [ForeignKey("OriginDriverID")]
        public virtual Driver OriginDriver { get; set; }

        public int OriginTruckID { get; set; }
        [ForeignKey("OriginTruckID")]
        public virtual Truck OriginTruck { get; set; }

        [DisplayName("Origin Truck End Mileage")]
        public int? OriginTruckEndMileage { get; set; }
        [DisplayName("Dest Truck Start Mileage")]
        public int? DestTruckStartMileage { get; set; }

        [DisplayName("Transfer %"),Range(0,100)]
        public int? PercentComplete { get; set; }

        [DisplayName("Transfer Complete")]
        public bool TransferComplete { get; set; }
    }


    public class OrderTransferViewModel
    {
        public OrderTransferViewModel() { }
        public OrderTransferViewModel(int OrderID)
        {
            this.OrderID = OrderID;
        }

        [Required]
        public int OrderID { get; set; }

        [Required]
        [DisplayName("Transfer Driver")]
        public int DestDriverID { get; set; }

        [DisplayName("Transfer Truck")]
        public int? DestTruckID { get; set; }

        [DisplayName("Current Truck Odometer")]
        public int? OriginTruckEndMileage { get; set; }

        [Required]
        [DisplayName("% Complete"), Range(0,100)]
        public int? PercentComplete { get; set; }

        [DisplayName("Transfer Notes"), StringLength(255), DataType(DataType.MultilineText)]
        public string Notes { get; set; }
    }
}
