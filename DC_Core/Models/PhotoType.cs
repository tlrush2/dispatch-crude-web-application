﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace DispatchCrude.Models
{
    [Table("tblPhotoType")]
    public class PhotoType : AuditModelCreateBase
    {
        public enum TYPES { Location = 1, Reject = 2, ConditionBefore = 3, ConditionAfter = 4, Location2 = 5 }

        public int ID { get; set; }
        public string Name { get; set; }
    }
}