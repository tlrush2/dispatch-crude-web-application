﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using DispatchCrude.Core;
using System.Web;

namespace DispatchCrude.Models
{
    [Table("tblDriverLocation")]
    public class DriverLocation: AuditModelCreateBase
    {
        public enum Status { UNAVAILABLE = -1, NO_ORDER = 0, UNACCEPTED = 1, HEADED_TO_ORIGIN = 2, AT_ORIGIN = 3, HEADED_TO_DEST = 4, AT_DEST = 5, ORDER_COMPLETE = 6 }

        public DriverLocation()
        {
            this.UID = new Guid();
        }

        [Key]
        public int ID { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid UID { get; set; }

        [DisplayName("Driver")]
        public int DriverID { get; set; }

        [DisplayName("Order")]
        public int? OrderID { get; set; }
        [DisplayName("Origin")]
        public int? OriginID { get; set; }
        [DisplayName("Destination")]
        public int? DestinationID { get; set; }

        [ForeignKey("DriverID")]
        public virtual Driver Driver { get; set; }

        [ForeignKey("OrderID")]
        public virtual Order Order { get; set; }
        [ForeignKey("OriginID")]
        public virtual Origin Origin { get; set; }
        [ForeignKey("DestinationID")]
        public virtual Destination Destination { get; set; }

        [DisplayName("Location Type")]
        public byte LocationTypeID { get; set; }
        [ForeignKey("LocationTypeID")]
        public virtual LocationType LocationType { get; set; }

        public decimal Lat { get; set; }
        public decimal Lon { get; set; }
        
        public int? DistanceToPoint { get; set; }

        public Int16? SourceAccuracyMeters { get; set; }
        public DateTime? SourceDateUTC { get; set; }

        [NotMapped, DisplayName("Source Date")]
        public DateTime? SourceDate
        {
            get
            {
                return DateHelper.ToLocal((SourceDateUTC.HasValue ? SourceDateUTC.Value : CreateDateUTC.Value), DispatchCrudeHelper.GetProfileTimeZone(HttpContext.Current));
            }
            set
            {
                SourceDateUTC = DateHelper.ToUTC((value.HasValue ? value.Value : CreateDate.Value), DispatchCrudeHelper.GetProfileTimeZone(HttpContext.Current));
            }
        }
        [NotMapped]
        public string SourceDateWithTZ
        {
            get { return SourceDate.ToString() + " " + DispatchCrudeHelper.GetProfileTimeZoneAbbrev(HttpContext.Current); }
        }

        [NotMapped]
        public int DriverStatus
        {
            get {
                if (OrderID == null)
                    return (int)Status.NO_ORDER;
                else if (Order.StatusID == (int)OrderStatus.STATUS.Dispatched) // have not accepted order (is this state valid for driver location?)
                    return (int)Status.UNACCEPTED;
                else if (Order.OriginArriveTimeUTC == null) // have not arrived at origin
                    return (int)Status.HEADED_TO_ORIGIN;
                else if (Order.OriginDepartTimeUTC == null) // have not left pickup
                    return (int)Status.AT_ORIGIN;
                else if (Order.DestArriveTimeUTC == null) // have not arrived at dest
                    return (int)Status.HEADED_TO_DEST;
                else if (Order.DestDepartTimeUTC == null) // have not left delivery
                    return (int)Status.AT_DEST;
                else if (Order.StatusID == (int)OrderStatus.STATUS.Delivered || Order.StatusID == (int)OrderStatus.STATUS.Audited)
                    return (int)Status.ORDER_COMPLETE;

                return (int)Status.UNAVAILABLE;
            }
        }

        public string getEvent()
        {
            switch (LocationTypeID)
            {
                case ((int)LocationType.Types.DRIVER_ARRIVE):
                    if (OriginID != null)
                        return "Origin Arrive";
                    else
                        return "Dest. Arrive";
                case ((int)LocationType.Types.DRIVER_DEPART):
                    if (OriginID != null)
                        return "Origin Depart";
                    else
                        return "Dest. Depart";
                case ((int)LocationType.Types.PICKUP_PRINT_DONE):
                    return "Pickup Print";
                case ((int)LocationType.Types.DELIVERY_PRINT_DONE):
                    return "Delivery Print";
                // case HOS ...
                default:
                    return LocationType.Name;
            }
        }


        /**************************************************************************************************/
        /** DESCRIPTION: Determine miles it will take a driver to travel from his current location and   **/
        /**       arrive at a point.  The RunOrderIfAccepted flag determines if a driver must finish the **/
        /**       order (true) or if he/she can leave the current order before picking it up to take     **/
        /**       this one.  If an origin or destination is used but has no GPS coordinates, the formula **/
        /**       cannot calculate a distance and will return null                                       **/
        /**************************************************************************************************/
        public double? getMilesToPoint(double lat, double lon, bool RunOrderIfAccepted = true)
        {
            if (DriverStatus == (int)Status.HEADED_TO_ORIGIN && RunOrderIfAccepted)
            {
                // must travel to origin, then dest, then to point
                if (Order.Origin.LAT == null || Order.Origin.LON == null || Order.Destination.LAT == null || Order.Destination.LON == null)
                    return null;

                return LocationHelper.getMiles(Lat, Lon, decimal.Parse(Order.Origin.LAT), decimal.Parse(Order.Origin.LON))
                     + ((Order.Route != null && Order.Route.ActualMiles != null && Order.Route.ActualMiles > 0) ? Order.Route.ActualMiles : LocationHelper.getMiles(decimal.Parse(Order.Origin.LAT), decimal.Parse(Order.Origin.LON), decimal.Parse(Order.Destination.LAT), decimal.Parse(Order.Destination.LON)))
                     + LocationHelper.getMiles(decimal.Parse(Order.Destination.LAT), decimal.Parse(Order.Destination.LON), (decimal)lat, (decimal)lon);
            }
            else if (DriverStatus == (int)Status.AT_ORIGIN || DriverStatus == (int)Status.HEADED_TO_DEST)
            {
                // must deliver, then travel to point
                if (Order.Destination.LAT == null || Order.Destination.LON == null)
                    return null;

                return LocationHelper.getMiles(Lat, Lon, decimal.Parse(Order.Destination.LAT), decimal.Parse(Order.Destination.LON))
                     + LocationHelper.getMiles(decimal.Parse(Order.Destination.LAT), decimal.Parse(Order.Destination.LON), (decimal)lat, (decimal)lon);
            }
            else
            {
                // travel to point
                return LocationHelper.getMiles(Lat, Lon, (decimal)lat, (decimal)lon);
            }

        }
   }
}