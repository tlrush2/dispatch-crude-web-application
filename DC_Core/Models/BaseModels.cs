﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;

namespace DispatchCrude.Models
{
    abstract public class IEDTO // Interface: Editable Data Table Object
    {
        // used to prevent validation from being fired twice (see CustomModelBinder.cs)
        [NotMapped]
        [ScriptIgnore]
        public bool Validated { get; set; }

        // base class we can use to find any DTO object by base interface
        [NotMapped]
        virtual public bool allowRowEdit  //works with customKendoButtons.js to get rid of the edit button for locked rows
        {
            get
            {
                // default behavior is to always allow (override in inheriting classes if required */
                return true;
            }
        }
        [NotMapped]
        virtual public bool allowRowDeactivate  //works with customKendoButtons.js to get rid of the deactivate button for locked rows
        {
            get
            {
                // default behavior is to always allow (override in inheriting classes if required */
                return true;
            }
        }

        // clone the object (to a like object or a subclass) returning the new object cast as the originating object
        static public T CloneToMe<T>(T from, Type toType = null, params string[] skipProperties)
        {
            return (T)(object)(from as IEDTO).Clone(toType, skipProperties);
        }
        // clone the object (to a like object or a subclass) returning the new object cast as IEDTO
        public IEDTO Clone(Type toType = null, params string[] skipProperties)
        {
            Type thisType = this.GetType();
            if (toType == null) toType = thisType;

            IEDTO ret = (IEDTO)Activator.CreateInstance(toType);

            if (!this.GetType().IsEquivalentTo(toType) && !toType.IsSubclassOf(thisType))
                throw new Exception("To instance is a subclass of the current instance");
            else
            {
                foreach (PropertyInfo pi in thisType.GetProperties(BindingFlags.Instance | BindingFlags.Public))
                {
                    Type piType = pi.PropertyType;
                    if (pi.CanWrite && (skipProperties == null || !skipProperties.Contains(pi.Name)))
                    {
                        if (piType.IsSubclassOf(typeof(IEDTO)) && pi.GetValue(this) != null)
                        {
                            pi.SetValue(ret, ((IEDTO)pi.GetValue(this)).Clone(null, skipProperties));
                        }
                        else
                        {
                            pi.SetValue(ret, pi.GetValue(this));
                        }
                    }
                }
            }
            return ret;
        }
    }

    abstract public class AuditModelCreateUTCBase: IEDTO
    {
        // common create fields
        [DisplayName("Created [UTC]"), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy hh:mm tt}")]
        public DateTime? CreateDateUTC { get; set; }
        [DisplayName("Created by")]
        public string CreatedByUser { get; set; }
    }

    abstract public class AuditModelCreateBase : AuditModelCreateUTCBase
    {
        [NotMapped, DisplayName("Create Date"), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy hh:mm tt}")]
        public DateTime? CreateDate
        {
            get
            {
                // use the user's timezone for the conversion
                return CreateDateUTC.HasValue ? Core.DateHelper.ToLocal(CreateDateUTC.Value, HttpContext.Current) : CreateDateUTC;
            }
            set
            {
                // use the user's timezone for the conversion
                CreateDateUTC = value.HasValue ? Core.DateHelper.ToUTC(value.Value, HttpContext.Current) : value;
            }
        }
    }

    abstract public class AuditModelLastChangeUTCBase : AuditModelCreateUTCBase
    {
        public DateTime? LastChangeDateUTC { get; set; }
        public string LastChangedByUser { get; set; }
    }

    abstract public class AuditModelLastChangeBase : AuditModelCreateBase
    {
        // common change fields
        [DisplayName("Last Changed [UTC]"), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy hh:mm tt}")]
        public DateTime? LastChangeDateUTC { get; set; }
        [NotMapped, DisplayName("Last Changed"), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy hh:mm tt}")]
        public DateTime? LastChangeDate
        {
            get
            {
                // use the user's timezone for the conversion
                return LastChangeDateUTC.HasValue ? Core.DateHelper.ToLocal(LastChangeDateUTC.Value, HttpContext.Current) : LastChangeDateUTC;
            }
            set
            {
                // use the user's timezone for the conversion
                LastChangeDateUTC = value.HasValue ? Core.DateHelper.ToUTC(value.Value, HttpContext.Current) : value;
            }
        }
        [DisplayName("Last Changed by")]
        public string LastChangedByUser { get; set; }
    }

    abstract public class AuditModelDeleteUTCBase : AuditModelLastChangeUTCBase
    {
        public DateTime? DeleteDateUTC { get; set; }
        public string DeletedByUser { get; set; }
    }

    abstract public class AuditModelDeleteBase : AuditModelLastChangeBase
    {
        public static string DELETED_TEXT = "DEACTIVATED";
        public static string DELETED_APPEND_TEXT = " [" + DELETED_TEXT + "]";

        [DisplayName("Deleted [UTC]"), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy hh:mm tt}")]
        public DateTime? DeleteDateUTC { get; set; }
        [NotMapped, DisplayName("Deleted"), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy hh:mm tt}")]
        public DateTime? DeleteDate
        {
            get
            {
                // use the user's timezone for the conversion
                return DeleteDateUTC.HasValue ? Core.DateHelper.ToLocal(DeleteDateUTC.Value, HttpContext.Current) : DeleteDateUTC;
            }
            set
            {
                // use the user's timezone for the conversion
                DeleteDateUTC = value.HasValue ? Core.DateHelper.ToUTC(value.Value, HttpContext.Current) : value;
            }
        }
        [DisplayName("Deleted by")]
        public string DeletedByUser { get; set; }

        [NotMapped, DefaultValue(true)]
        public bool Active
        {
            get
            {
                return
                  !DeleteDateUTC.HasValue;
            }
            set
            {
                DeleteDateUTC = value ? (DateTime?)null : DateTime.UtcNow;
            }
        }
        [NotMapped]
        public bool Deleted { get { return DeleteDateUTC.HasValue; } }

        [NotMapped]
        public bool PermanentDelete { get; set; }
    }
}
