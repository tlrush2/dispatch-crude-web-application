﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DispatchCrude.Models
{
    [Table("tblReportDefinition")]
    public class ReportDefinition: IEDTO
    {
        public ReportDefinition()
        {
            Columns = new List<ReportColumnDefinition>();
        }
        public int ID { get; set; }

        [Required, StringLength(25)]
        public string Name { get; set; }

        [Required, StringLength(50)]
        public string SourceTable { get; set; }

        public IEnumerable<ReportColumnDefinition> Columns { get; set; }
    }
}