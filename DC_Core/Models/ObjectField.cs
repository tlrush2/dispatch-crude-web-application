using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Data.Entity;
using System.Web.Mvc;
using AlonsIT;

namespace DispatchCrude.Models
{
    [Table("tblObjectField")]
    public class ObjectField : AuditModelLastChangeBase
    {
        public enum TYPE {
            Order_DeliverPrintDate = 410, Order_DestArriveTime = 411, Order_DestDepartTime = 412, Order_OriginArriveTime = 413, Order_OriginDepartTime = 414, Order_PickupPrintDate = 415,
        }

        public ObjectField()
        {
        }

        [Key]
        public int ID { get; set; }

        [Column("ObjectID")]
        [DisplayName("Object")]
        public int ObjectDefID{ get; set; }
        [ForeignKey("ObjectDefID")]
        public virtual ObjectDef ObjectDef { get; set; }

        [StringLength(50), DisplayName("Field Name")]
        public string FieldName { get; set; }
        [Required, StringLength(50), ]
        public string Name { get; set; }

        [Required]
        public bool ReadOnly { get; set; }

        public Int16 ObjectFieldTypeID { get; set; }
        [ForeignKey("ObjectFieldTypeID")]
        public virtual ObjectFieldType ObjectFieldType { get; set; }

        [Required]
        public int AllowNullID { get; set; }
        [ForeignKey("AllowNullID")]
        public virtual ObjectFieldAllowNull AllowNull { get; set; }

        [Required, DisplayName("Is Key?")]
        public bool IsKey { get; set; }

        [Required, DisplayName("Is Custom?")]
        public bool IsCustom { get; set; }

        [DisplayName("Parent Object")]
        public int? ParentObjectID { get; set; }
        [ForeignKey("ParentObjectID"), DisplayName("Parent Object")]
        public virtual ObjectDef ParentObject { get; set; }

        [DisplayName("Parent Object ID Field Name")]
        public string ParentObjectIDFieldName { get; set; }

        [InverseProperty("ObjectField")]
        public List<ObjectCustomData> CustomData { get; set; }

        [NotMapped]
        public bool Locked
        {
            get
            {
                if (ID == 0)
                    return false;
                else if (CustomData != null)
                    return CustomData.Count > 0;
                else
                    return DBHelper.ToInt32(SSDB.QuerySingleValueStatic("SELECT count(*) FROM tblObjectCustomData WHERE ObjectFieldID={0}", this.ID)) > 0;
            }
        }
    }

}
