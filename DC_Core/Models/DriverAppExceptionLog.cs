﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DispatchCrude.Models
{
    [Table("tblDriverAppExceptionLog")]
    public class DriverAppExceptionLog: Sync.DriverApp.ExceptionLog
    {
        public DriverAppExceptionLog()
        {
            this.UID = new Guid();
        }
        public DriverAppExceptionLog(Guid uid)
        {
            this.UID = uid;
        }
    }
}