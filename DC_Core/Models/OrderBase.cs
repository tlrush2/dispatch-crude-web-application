﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace DispatchCrude.Models
{
    /*********************************************************************************************************/
    /*********************************************************************************************************/
    /**     NOT CURRENTLY USED.  Did not work initially.  See Driver & Driverbase for a later implementation **/
    /*********************************************************************************************************/
    /*********************************************************************************************************/

    abstract public class OrderBase : AuditModelDeleteBase
    {
        public OrderBase()
        {
        }

        [Key]
        public int ID { get; set; }

        public int? OrderNum { get; set; }

        [DisplayName("Job #")]
        public string JobNumber { get; set; }

        [DisplayName("Contract #")]
        public int? ContractID { get; set; }
        [ForeignKey("ContractID")]
        public virtual Contract Contract { get; set; }

        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}"), UIHint("Date")]
        public DateTime DueDate { get; set; }

        [DisplayName("Tank")]
        public int? OriginTankID { get; set; }

        [StringLength(20), DisplayName("Tank Details")]
        public string OriginTankNum { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy hh:mm tt}")]
        public DateTime? OriginArriveTimeUTC { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy hh:mm tt}")]
        public DateTime? OriginDepartTimeUTC { get; set; }

        [DisplayName("Origin Wait Min")]
        public int? OriginMinutes { get; set; }

        [DisplayName("Origin Wait Reason")]
        [ClientDropDownMustBeSelectedAttribute(ErrorMessage = "Wait Reason must be specified")]
        public int? OriginWaitReasonID { get; set; }

        [StringLength(255), DisplayName("Origin Wait Notes")]
        public string OriginWaitNotes { get; set; }

        [StringLength(15), DisplayName("Origin BOL #")]
        public string OriginBOLNum { get; set; }

        [DisplayName("Order Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}"), UIHint("Date")]
        public DateTime? OrderDate { get; set; }

        [DisplayName("Product")]
        public int ProductID { get; set; }

        [DisplayName("GOV"), DisplayFormat(DataFormatString = "{0:0.00}")]
        public decimal? OriginGrossUnits { get; set; }
        [DisplayName("GSV"), DisplayFormat(DataFormatString = "{0:0.00}")]
        public decimal? OriginGrossStdUnits { get; set; }
        [DisplayName("NSV"), DisplayFormat(DataFormatString = "{0:0.00}")]
        public decimal? OriginNetUnits { get; set; }

        [DisplayName("Origin Net Weight")]
        public decimal? OriginWeightNetUnits { get; set; }

        [DefaultValue(1)]
        public int? OriginUomID { get; set; }

        [DefaultValue(1)]
        public int? DestUomID { get; set; }

        [DisplayName("Sulfur Content"), DisplayFormat(DataFormatString = "{0:0.0000}"), Range(0, 10)]
        public decimal? OriginSulfurContent { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy hh:mm tt}")]
        public DateTime? DestArriveTimeUTC { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy hh:mm tt}")]
        public DateTime? DestDepartTimeUTC { get; set; }

        [DisplayName("Dest Wait Min")]
        public int? DestMinutes { get; set; }

        [DisplayName("Destination Wait Reason")]
        [ClientDropDownMustBeSelectedAttribute(ErrorMessage = "Wait Reason must be specified")]
        public int? DestWaitReasonID { get; set; }

        [StringLength(255), DisplayName("Dest Wait Notes")]
        public string DestWaitNotes { get; set; }

        [StringLength(30), DisplayName("Dest BOL #")]
        public string DestBOLNum { get; set; }

        [DisplayName("Dest GOV"), DisplayFormat(DataFormatString = "{0:0.00}", ApplyFormatInEditMode = true)]
        public decimal? DestGrossUnits { get; set; }
        [DisplayName("Dest NSV"), DisplayFormat(DataFormatString = "{0:0.00}", ApplyFormatInEditMode = true)]
        public decimal? DestNetUnits { get; set; }

        [DisplayName("Dest Opening Meter Vol")]
        public decimal? DestOpenMeterUnits { get; set; }
        [DisplayName("Dest Closing Meter Vol")]
        public decimal? DestCloseMeterUnits { get; set; }

        [DisplayName("Dest Product Temp (F°)"), DisplayFormat(DataFormatString = "{0:0.0}", ApplyFormatInEditMode = true)]
        public decimal? DestProductTemp { get; set; }
        [DisplayName("Dest Product BS&W"), DisplayFormat(DataFormatString = "{0:0.000\\%}")]
        public decimal? DestProductBSW { get; set; }
        [DisplayName("Dest Product API Gravity")]

        public decimal? DestProductGravity { get; set; }

        [DisplayName("Dest Gross"), DisplayFormat(DataFormatString = "{0:0.000}", ApplyFormatInEditMode = true)]
        public decimal? DestWeightGrossUnits { get; set; }
        [DisplayName("Dest Tare"), DisplayFormat(DataFormatString = "{0:0.000}", ApplyFormatInEditMode = true)]
        public decimal? DestWeightTareUnits { get; set; }
        [DisplayName("Dest Net"), DisplayFormat(DataFormatString = "{0:0.000}", ApplyFormatInEditMode = true)]
        public decimal? DestWeightNetUnits { get; set; }

        [DisplayName("Railcar #"), StringLength(25)]
        public string DestRailCarNum { get; set; }

        [DisplayName("BOL Trailer Water Cap.")]
        public int? DestTrailerWaterCapacity { get; set; }

        [Required, DisplayName("Rejected?"), UIHint("Switch")]
        public bool Rejected { get; set; }
        [StringLength(255), DisplayName("Reject Notes")]
        public string RejectNotes { get; set; }

        [DisplayName("Reject Reason")]
        [ClientDropDownMustBeSelectedAttribute(ErrorMessage = "Reject Reason must be specified")]
        public int? RejectReasonID { get; set; }

        [Required, DisplayName("Chain-Up?"), UIHint("Switch")]
        public bool OriginChainUp { get; set; }

        [Required, DisplayName("Chain-Up?"), UIHint("Switch")]
        public bool DestChainUp { get; set; }

        [DisplayName("Origin Truck Mileage")]
        public int? OriginTruckMileage { get; set; }

        [DisplayName("Dest Truck Mileage")]
        public int? DestTruckMileage { get; set; }

        [StringLength(15)]
        [DisplayName("Carrier Ticket #")]
        public string CarrierTicketNum { get; set; }

        [DisplayName("Pickup Print Status")]
        public int PickupPrintStatusID { get; set; }
        [DisplayName("Delivery Print Status")]
        public int DeliverPrintStatusID { get; set; }

        [DisplayName("Pickup Print Date")]
        public DateTime? PickupPrintDateUTC { get; set; }
        [DisplayName("Delivery Print Date")]
        public DateTime? DeliverPrintDateUTC { get; set; }

        [DisplayName("Last Accept [UTC]")]
        public DateTime? AcceptLastChangeDateUTC { get; set; }

        [DisplayName("Last Pickup [UTC]")]
        public DateTime? PickupLastChangeDateUTC { get; set; }

        [DisplayName("Last Deliver [UTC]")]
        public DateTime? DeliverLastChangeDateUTC { get; set; }

        [DisplayName("Correction Notes"), StringLength(255)]
        public string AuditNotes { get; set; }

        [DisplayName("Status")]
        public int StatusID { get; set; }

        [DisplayName("Ticket Type")]
        public int TicketTypeID { get; set; }

        [DisplayName("Truck")]
        [ClientDropDownMustBeSelectedAttribute(ErrorMessage = "Truck must be specified")]
        public int? TruckID { get; set; }

        [DisplayName("Trailer")]
        [ClientDropDownMustBeSelectedAttribute(ErrorMessage = "Trailer 1 must be specified")]
        public int? TrailerID { get; set; }

        [DisplayName("Trailer 2")]
        public int? Trailer2ID { get; set; }

        [DisplayName("Dispatch Notes"), StringLength(500)]
        public string DispatchNotes { get; set; }
        [DisplayName("Driver Pickup Notes"), StringLength(255)]
        public string PickupDriverNotes { get; set; }
        [DisplayName("Driver Deliver Notes"), StringLength(255)]
        public string DeliverDriverNotes { get; set; }
        [DisplayName("Shipper PO"), StringLength(25)]
        public string DispatchConfirmNum { get; set; }

        [DisplayName("Carrier")]
        public int? CarrierID { get; set; }

        [DisplayName("Shipper")]
        public int? CustomerID { get; set; }

        [DisplayName("Destination")]
        public int? DestinationID { get; set; }

        [DisplayName("Driver")]
        public int? DriverID { get; set; }

        [DisplayName("Operator")]
        public int? OperatorID { get; set; }

        [DisplayName("Producer")]
        public int? ProducerID { get; set; }

        [DisplayName("Origin")]
        public int? OriginID { get; set; }

        [DisplayName("Priority")]
        public byte PriorityID { get; set; }

        [DisplayName("Sequence No.")]
        public int? SequenceNum { get; set; }

        [DisplayName("Pumper")]
        public int? PumperID { get; set; }

        [DisplayName("Route")]
        public int? RouteID { get; set; }

        [DisplayName("Dest Rack/Bay")]
        public string DestRackBay { get; set; }

        [DisplayName("Reassign Key")]
        //[DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public int? ReassignKey { get; set; }
    }
}
