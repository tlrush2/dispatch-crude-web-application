﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using System.Web;

namespace DispatchCrude.Models
{
    [Table("aspnet_Membership")]
    public class DCMembership : AuditModelDeleteBase 
    {
        [Key]
        public Guid UserId { get; set; }

        //[Required]
        //public Guid ApplicationId { get { return Guid.Parse("446467D6-39CD-45E3-B3E0-CAC7945AF3E8"); } set { ApplicationId = Guid.Parse("446467D6-39CD-45E3-B3E0-CAC7945AF3E8"); } }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        //[RegularExpression(@"([A-Za-z0-9~!@#$%^&*()_+\[\]{}|;:',<.>\/?]{6,})+", ErrorMessage = "Your password does not meet the complexity requirements")]
        public string Password { get; set; }
                
        [Required]
        public int PasswordFormat { get { return 1; } set { PasswordFormat = 1; } }
                
        public string PasswordSalt { get; set; }

        [DisplayName("Contact Email")]
        [StringLength(256)]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Please enter a valid email address.")]
        public string Email { get; set; }
                
        public string LoweredEmail
        {
            get
            {
                return Email.ToLower();
            }
            set
            {
                value = Email.ToLower();
            }
        }
        
        //public DateTime CreateDate { get; set; }

        [Required]
        [DisplayName("Locked Out?")]
        public bool IsLockedOut { get; set; }

        [StringLength(256)]
        public string Comment { get; set; }
        
        [Column("CreateDate")]  //This mapping is needed because I did not want to change the curerntly existing aspnet column name to "CreateDateUTC" -Ben 6/24/16
        [DisplayName("Created [UTC]"), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy hh:mm tt}")]
        new public DateTime? CreateDateUTC { get; set; }  //NOTE: This field intentionally hides/overrides the AuditModel field

        //
        //The rest of these columns below are required to be not null by the table but we are not planning to use them on the front end.
        //

        public bool IsApproved { get; set; }

        public DateTime LastLoginDate { get; set; }

        public DateTime LastPasswordChangedDate { get; set; }

        public DateTime LastLockoutDate { get; set; }

        public int FailedPasswordAttemptCount { get; set; }

        public DateTime FailedPasswordAttemptWindowStart { get; set; }

        public int FailedPasswordAnswerAttemptCount { get; set; }
        
        public DateTime FailedPasswordAnswerAttemptWindowStart { get; set; }
    }
}