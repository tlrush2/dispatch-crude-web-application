﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using AlonsIT;
using AlonsIT.ExtensionMethods;

namespace DispatchCrude.Models
{
    [Table("tblGaugerOrder")]
    public class GaugerOrder : Sync.GaugerApp.GaugerOrderBase
    {
        public GaugerOrder()
        {
            this.GaugerTickets = new List<GaugerTicket>();
        }

        public int TicketTypeID { get; set; }
        public int? GaugerID { get; set; }
        public string DispatchNotes { get; set; }
        public DateTime? DueDate { get; set; }
        public byte PriorityID { get; set; }
        public int? OriginTankID { get; set; }
        public string OriginTankNum { get; set; }

        [ForeignKey("StatusID")]
        GaugerOrderStatus Status { get; set; }

        [ForeignKey("OrderID")]
        public Order Order { get; set; }

        [InverseProperty("GaugerOrder")]
        public ICollection<GaugerTicket> GaugerTickets { get; set; }
    }
}