using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DispatchCrude.Models
{
    [Table("tblDestinationType")]
    public class DestinationType : AuditModelDeleteBase, IValidatableObject
    {
        public DestinationType()
        {
            this.Destinations = new List<Destination>();
        }

        [InverseProperty("DestinationType")]
        public ICollection<Destination> Destinations { get; set; }

        [Key]
        public int ID { get; set; }

        [Required, Column("DestinationType"), StringLength(20)]
        public string Name { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!ValidateDuplicateName(ID, Name))
                yield return new ValidationResult("Name is already in use");
            Validated = true;
        }

        private bool ValidateDuplicateName(int id, string name)
        {
            using (DispatchCrudeDB db = new DispatchCrudeDB())
            {
                return db.DestinationTypes.Where(m => m.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase) && (id == 0 || m.ID != id)).Count() == 0;
            }
        }
    }
}
