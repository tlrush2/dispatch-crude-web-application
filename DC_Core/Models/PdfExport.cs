﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using System.Linq;
using System;

namespace DispatchCrude.Models
{
    [Table("tblPdfExport")]
    public class PdfExport : AuditModelDeleteBase, IValidatableObject
    {
        [Key]
        public int ID { get; set; }

        [Required, StringLength(30)]
        public string Name { get; set; }

        [DisplayName("Export Type")]
        [UIHint("_ForeignKeyDDL")]
        public int PdfExportTypeID { get; set; }

        [DisplayName("Export Type")]
        [ForeignKey("PdfExportTypeID")]
        public PdfExportType PdfExportType { get; set; }

        [DisplayName("Shipper")]
        public int? ShipperID { get; set; }
        [DisplayName("Carrier")]
        public int? CarrierID { get; set; }
        [DisplayName("Producer")]
        public int? ProducerID { get; set; }
        [DisplayName("Product")]
        public int? ProductID { get; set; }

        [DisplayName("Word Template")]
        public byte[] WordDocument { get; set; }
        [DisplayName("Word Template")]
        public string WordDocName { get; set; }

        [ForeignKey("ShipperID")]
        public Customer Shipper { get; set; }
        [ForeignKey("CarrierID")]
        public Customer Carrier { get; set; }
        [ForeignKey("ProducerID")]
        public Customer Producer { get; set; }
        [ForeignKey("ProductID")]
        public Customer Product { get; set; }


        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!ValidateDuplicateName(ID, Name))
                yield return new ValidationResult("Name is already in use");
            Validated = true;
        }

        static protected bool ValidateDuplicateName(int id, string name)
        {
            using (DispatchCrudeDB db = new DispatchCrudeDB())
            {
                return db.Set<PdfExport>().Where(m => m.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase) && (id == 0 || m.ID != id)).Count() == 0;
            }
        }
    }
}
