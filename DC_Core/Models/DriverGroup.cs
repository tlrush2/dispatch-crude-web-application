using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using System.Linq;

namespace DispatchCrude.Models
{
    [Table("tblDriverGroup")]
    public class DriverGroup : AuditModelDeleteBase
    {
        public DriverGroup()
        {
        }

        [Key]
        public int ID { get; set; }

        [Required, StringLength(50)]
        public string Name { get; set; }
    }
}
