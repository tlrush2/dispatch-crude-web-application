﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using AlonsIT;
using AlonsIT.ExtensionMethods;

namespace DispatchCrude.Models
{
    [Table("tblGaugerOrderTicket")]
    public class GaugerTicket : Sync.GaugerApp.GaugerTicketBase
    {
        public GaugerTicket()
        {
            this.UID = new Guid();
        }

        public GaugerTicket(Guid uid, int orderID)
        {
            this.UID = uid;
            this.OrderID = orderID;
        }

        [ForeignKey("OrderID")]
        public virtual GaugerOrder GaugerOrder { get; set; }
    }
}