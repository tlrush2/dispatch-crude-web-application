using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Web.Script.Serialization;
//using System.Web.Mvc;  //needed for remote validation
using Newtonsoft.Json;

namespace DispatchCrude.Models
{
    [Table("tblCustomer")]
    public class Customer : AuditModelDeleteBase , IValidatableObject
    {
        public Customer()
        {
            this.Orders = new List<Order>();
            this.Origins = new List<Origin>();
            this.Destinations = new List<Destination>();
        }

        public ICollection<Order> Orders { get; set; }
        public ICollection<Origin> Origins { get; set; }
        public ICollection<Destination> Destinations { get; set; }

        [Key]
        public int ID { get; set; }

        [Required(ErrorMessage="The shipper name field is required.")]
        [StringLength(40, MinimumLength=2, ErrorMessage="Shipper name length must be between 2 and 40 characters.")]
        //[Remote("IsNameAvailable", "Customer", AdditionalFields = "ID", ErrorMessage = "This {0} is already used.")]
        public string Name { get; set; }

        [StringLength(40)]
        public string Address { get; set; }

        [StringLength(30)]
        public string City { get; set; }

        [UIHint("_StateDDL")]
        [DisplayName("State")]
        public int? StateID { get; set; }
        [ForeignKey("StateID")]
        public virtual State State { get; set; }

        [StringLength(10)]
        public string Zip { get; set; }

        [DisplayName("Contact Name")]
        [StringLength(40)]
        public string ContactName { get; set; }

        [DisplayName("Contact Email")]
        [StringLength(50)]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage="Please enter a valid email address.")]
        public string ContactEmail { get; set; }

        [UIHint("PhoneNumber")]
        [DisplayName("Contact Phone #")]
        [StringLength(15)]
        public string ContactPhone { get; set; }

        [DisplayName("ZPL Header Filename")]
        [StringLength(255)]
        public string ZPLHeaderFileName { get; set; }
        [DisplayName("ZPL Header File")]
        [ScriptIgnore]
        public byte[] ZPLHeaderBlob { get; set; }

        [DisplayName("BOL Header Filename")]
        [StringLength(255)]        
        public string BOLHeaderFileName { get; set; }
        [DisplayName("BOL Header")]
        [ScriptIgnore]        
        public byte[] BOLHeaderBlob { get; set; }

        [NotMapped]
        [ScriptIgnore]        
        public string BOLHeaderImageSrc
        {
            get
            {
                return BOLHeaderBlob == null || BOLHeaderBlob.Length == 0 
                    ? "" 
                    : string.Format("data:image/{1};base64,{0}", Convert.ToBase64String(BOLHeaderBlob), Path.GetExtension(BOLHeaderFileName));
            }
        }

        [DisplayName("Emergency Info")]
        [StringLength(255)]
        public string EmergencyInfo { get; set; }

        [UIHint("PhoneNumber")]
        [DisplayName("Help Desk Phone #")]
        [StringLength(25)]
        public string HelpDeskPhone { get; set; }

        [DisplayName("Shipper C.O.D.E. ID")]
        [StringLength(2)]
        public string CodeDXCode { get; set; }

        [DisplayName("SXS Client ID")]
        [StringLength(10)]
        public string ShipXpressDXClient { get; set; }

        [DisplayName("Use SXS Export?")]
        public bool ShipXpressDXExport { get; set; }

        [DisplayName("Net Term Days")]
        public int? NetTermDays { get; set; }

        [DisplayName("S.I.P.O.")]
        public bool SingleInvoicePerOrder { get; set; }        

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!ValidateDuplicateName(ID, Name))
                yield return new ValidationResult("Name is already in use");
            Validated = true;
        }

        private bool ValidateDuplicateName(int id, string name)
        {
            using (DispatchCrudeDB db = new DispatchCrudeDB())
            {
                return db.Customers.Where(s => s.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase) && (id == 0 || s.ID != id)).Count() == 0;
            }
        }

    }

}
