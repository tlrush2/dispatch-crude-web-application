﻿using DispatchCrude.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace DispatchCrude.Models
{

    [Table("tblHosPolicy")]
    public class HosPolicy : AuditModelDeleteBase, IValidatableObject
    {
        public static int DEFAULT_HOS_POLICY = 1;


        [Key]
        public int ID { get; set; }

        [Required, StringLength(60)]
        public string Name { get; set; }

        [StringLength(800)]
        public string Description { get; set; }

        [DisplayName("On-Duty Weekly Limit")]
        [UIHint("Duration"), Range(0, int.MaxValue)]
        public int OnDutyWeeklyLimit { get; set; }
        static public string HELP_OnDutyWeeklyLimit = "Maximum time a driver can be on-duty in a given \"week\".";

        [DisplayName("Driving Weekly Limit")]
        [UIHint("Duration"), Range(0, int.MaxValue)]
        public int DrivingWeeklyLimit { get; set; }
        static public string HELP_DrivingWeeklyLimit = "Maximum time a driver can drive in a given \"week\".";

        [DisplayName("On-Duty Daily Limit")]
        [UIHint("Duration"), Range(0,24*60)]
        public int OnDutyDailyLimit { get; set; }
        static public string HELP_OnDutyDailyLimit = "Maximum time a driver can be on-duty in a given day.";

        [DisplayName("Driving Daily Limit")]
        [UIHint("Duration"), Range(0,24*60)]
        public int DrivingDailyLimit { get; set; }
        static public string HELP_DrivingDailyLimit = "Maximum time a driver can drive in a given day.";

        [DisplayName("Driving Before Break Required")]
        [UIHint("Duration"), Range(0,24*60)]
        public int DrivingBreakLimit { get; set; }
        static public string HELP_DrivingBreakLimit = "Maximum time a driver can drive before taking a required break.";

        [DisplayName("Personal Conveyance")]
        [UIHint("Duration"), Range(0, int.MaxValue)]
        public int PersonalDailyLimit { get; set; }
        static public string HELP_PersonalDailyLimit = "Maximum time a driver is allowed for personal use in a given day.";


        [DisplayName("Shift Interval (Days)")]
        [Range(0, int.MaxValue)]
        public int ShiftIntervalDays { get; set; }
        static public string HELP_ShiftIntervalDays = "The number of days constituting a shift or \"week\"";


        [DisplayName("Weekly Reset")]
        [UIHint("Duration"), Range(0, int.MaxValue)]
        public int WeeklyReset { get; set; }
        static public string HELP_WeeklyReset = "Minimum time a driver must rest before weekly status is reset.";

        [DisplayName("Daily Reset")]
        [UIHint("Duration"), Range(0, int.MaxValue)]
        public int DailyReset { get; set; }
        static public string HELP_DailyReset = "Minimum time a driver must rest before daily status is reset.";

        [DisplayName("Sleep Break")]
        [UIHint("Duration"), Range(0, int.MaxValue)]
        public int SleeperBreak { get; set; }
        static public string HELP_SleeperBreak = "Minimum time a driver must rest to constitute a sleep break.";

        [DisplayName("Driving Break")]
        [UIHint("Duration"), Range(0, int.MaxValue)]
        public int DrivingBreak { get; set; }
        static public string HELP_DrivingBreak = "Minimum time a driver must rest to constitute a driving break.";

        [DisplayName("Sleeper-Berth Split Total")]
        [UIHint("Duration"), Range(0, int.MaxValue)]
        public int SleeperBerthSplitTotal { get; set; }
        static public string HELP_SleeperBerthSplitTotal = "Total time that resets a split sleeper-berth.";

        [DisplayName("Sleeper-Berth Split Minimum")]
        [UIHint("Duration"), Range(0, int.MaxValue)]
        public int SleeperBerthSplitMinimum { get; set; }
        static public string HELP_SleeperBerthSplitMinimum = "Minimum time that constitues a qualified break when determining the sleeper provision.";


        [DisplayName("Warn Logoff")]
        [UIHint("Duration"), Range(0, int.MaxValue)]
        public int WarnLogoff { get; set; }
        static public string HELP_WarnLogoff = "Time on-duty before a driver is warned to log off. (0 = no warning)";

        [DisplayName("Force Logoff")]
        [UIHint("Duration"), Range(0, int.MaxValue)]
        public int ForceLogoff { get; set; }
        static public string HELP_ForceLogoff = "Time on-duty before a driver is forced to log off.  (0 = not enforced)";

        // DriverAppLogRetentionDays Range Notes:
        // MIN: 8 Days ("Today" + last 7 days) is the minimum requirement by DOT (ref. 4.8.1.3) and must not be changed.  
        // MAX: 30 is a max amount deemed "reasonable" in office and can be changed if necessary.
        [DisplayName("Driver App Log Retention (Days)")]
        [Range(8, 30)]  
        public int DriverAppLogRetentionDays { get; set; }
        static public string HELP_DriverAppLogRetentionDays = "Number of days of HOS history available on the driver app. (Range: 8 - 30)";


        [NotMapped]
        override public bool allowRowDeactivate {  get { return ID != 1; } } // Don't allow deactivate of Default


        [NotMapped]
        public string OnDutyWeeklyLimitHR { get { return DateHelper.FormatDuration(OnDutyWeeklyLimit / 60.0); } }
        [NotMapped]
        public string DrivingWeeklyLimitHR { get { return DateHelper.FormatDuration(DrivingWeeklyLimit / 60.0); } }
        [NotMapped]
        public string OnDutyDailyLimitHR { get { return DateHelper.FormatDuration(OnDutyDailyLimit / 60.0); } }
        [NotMapped]
        public string DrivingDailyLimitHR { get { return DateHelper.FormatDuration(DrivingDailyLimit / 60.0); } }
        [NotMapped]
        public string DrivingBreakLimitHR { get { return DateHelper.FormatDuration(DrivingBreak / 60.0); } }
        [NotMapped]
        public string PersonalDailyLimitHR { get { return DateHelper.FormatDuration(PersonalDailyLimit / 60.0); } }
        [NotMapped]
        public string WeeklyResetHR { get { return DateHelper.FormatDuration(WeeklyReset / 60.0); } }
        [NotMapped]
        public string DailyResetHR { get { return DateHelper.FormatDuration(DailyReset / 60.0); } }
        [NotMapped]
        public string SleeperBreakHR { get { return DateHelper.FormatDuration(SleeperBreak / 60.0); } }
        [NotMapped]
        public string DrivingBreakHR { get { return DateHelper.FormatDuration(DrivingBreak / 60.0); } }
        [NotMapped]
        public string SleeperBerthSplitTotalHR { get { return DateHelper.FormatDuration(SleeperBerthSplitTotal / 60.0); } }
        [NotMapped]
        public string SleeperBerthSplitMinimumHR { get { return DateHelper.FormatDuration(SleeperBerthSplitMinimum / 60.0); } }
        [NotMapped]
        public string WarnLogoffHR { get { return DateHelper.FormatDuration(WarnLogoff / 60.0); } }
        [NotMapped]
        public string ForceLogoffHR { get { return DateHelper.FormatDuration(ForceLogoff / 60.0); } }


        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!ValidateDuplicateName(ID, Name))
                yield return new ValidationResult("Name is already in use");
            Validated = true;
        }

        private bool ValidateDuplicateName(int id, string name)
        {
            using (DispatchCrudeDB db = new DispatchCrudeDB())
            {
                return db.HosPolicies.Where(m => m.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase) && (id == 0 || m.ID != id)).Count() == 0;
            }
        }
    }

}
