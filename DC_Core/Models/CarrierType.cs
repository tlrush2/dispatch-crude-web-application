﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using System.Data.Entity;
using DispatchCrude.Core;

namespace DispatchCrude.Models
{
    [Table("tblCarrierType")]
    public class CarrierType : AuditModelDeleteBase, IValidatableObject
    {
        [Key]
        public int ID { get; set; }

        [Required]
        [DisplayName("Carrier Type")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "Length must be between 2 and 50 characters.")]
        public string Name { get; set; }

        public int getCarrierCount {
            get
            {
                using (DispatchCrudeDB db = new DispatchCrudeDB())
                {
                    return db.Carriers.Where(c => c.CarrierTypeID == ID).Count();
                }
            }
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!ValidateDuplicateName(ID, Name))
                yield return new ValidationResult("Name is already in use");
            Validated = true;
        }

        private bool ValidateDuplicateName(int id, string name)
        {
            using (DispatchCrudeDB db = new DispatchCrudeDB())
            {
                return db.CarrierTypes.Where(s => s.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase) && (id == 0 || s.ID != id)).Count() == 0;
            }
        }

        [NotMapped]
        override public bool allowRowDeactivate  //works with customKendoButtons.js to get rid of the deactivate button for locked rows
        {
            get
            {
                // only allow deactivate if no carriers are using this carrier type
                return getCarrierCount == 0;
            }
        }
    }
}
