﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchCrude.Models
{
    [Table("tblDriverScheduleStatus")]
    public class DriverScheduleStatus : AuditModelDeleteBase
    {
        public enum TYPE { UnDefined = 1, OnDuty = 2, NotAvailable = 3, Open = 4, }

        public DriverScheduleStatus()
        {
            Active = true; // default value
            OnDuty = false;
            Utilized = true;
        }

        [Key]
        public Int16 ID { get; set; }

        [Required, StringLength(25)]
        public string Name { get; set; }
        
        [Required, StringLength(3)]
        public string Abbrev { get; set; }

        [DisplayName("On Duty?")]
        [DefaultValue(false)]
        public bool OnDuty { get; set; }

        [DisplayName("Utilized?")]
        [DefaultValue(true)]
        public bool Utilized { get; set; }
    }
}
