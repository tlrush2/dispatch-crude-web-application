using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DispatchCrude.Models
{
    [Table("tblAndroidOSVersion")]
    public class AndroidOSVersion 
    {
        [Key]
        public int ID { get; set; }

        [DisplayName("Name")]
        public string Name { get; set; }

        [Required]
        [DisplayName("Version")]
        public string VersionNumber { get; set; }

        [Required]
        [DisplayName("API Level")]        
        public string ApiLevel { get; set; }        

        [NotMapped]
        public string NameAndVersion
        {
            get
            {
                return Name + " (" + VersionNumber + ")";
            }
        }
    }
}
