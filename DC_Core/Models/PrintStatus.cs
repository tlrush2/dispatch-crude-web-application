using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DispatchCrude.Models
{

    [Table("tblPrintStatus")]
    public class PrintStatus
    {
        public enum STATUS { NotFinalized = 0, Finalized = 1, Printed = 2, Handwritten = 3, }

        [Key]
        public int ID { get; set; }

        [Required, DisplayName("Print Status"), StringLength(25)]
        public string Name { get; set; }

        [DisplayName("Is Completed")]
        public bool IsCompleted { get; set; }
    }
}
