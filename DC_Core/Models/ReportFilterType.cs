using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DispatchCrude.Models
{
    [Table("tblReportFilterType")]
    public class ReportFilterType
    {
        public enum TYPE { NoFilter_DateTime = 0, Text = 1, DropDown = 2, DatePeriod = 3, Number = 4, Yes_No = 5, DateRange = 6 }

        [Key]
        public int ID { get; set; }

        [Required, StringLength(25)]
        public string Name { get; set; }

        [StringLength(50)]
        public string FilterOperatorID_CSV { get; set; }
    }
}
