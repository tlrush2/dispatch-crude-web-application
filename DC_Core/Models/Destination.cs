using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using System.Linq;
using Newtonsoft.Json;

namespace DispatchCrude.Models
{
    [Table("tblDestination")]
    public class Destination : AuditModelDeleteBase
    {
        public Destination()
        {
            //this.Routes = new List<Route>();
            this.Shippers = new List<Customer>();
            this.Products = new List<Product>();
        }

        //[InverseProperty("Destination")]
        //public virtual ICollection<Route> Routes { get; set; }

        public /*virtual*/ ICollection<Customer> Shippers { get; set; }

        public /*virtual*/ ICollection<Product> Products { get; set; } 

        [Key]
        public int ID { get; set; }

        [Required, DisplayName("Type")]
        public int DestinationTypeID { get; set; }

        [ForeignKey("DestinationTypeID"), DisplayName("Type")]
        public /*virtual*/ DestinationType DestinationType { get; set; }

        [Required, DisplayName("Name"), StringLength(50, MinimumLength=4, ErrorMessage = "Destination name must be between 4 and 50 characters.")]
        public string Name { get; set; }
        
        [DisplayName("Address"), StringLength(50)]
        public string Address { get; set; }

        [DisplayName("City"), StringLength(30)]
        public string City { get; set; }

        [Required, DisplayName("State")]
        public int? StateID { get; set; }

        [ForeignKey("StateID"), DisplayName("State")]
        public /*virtual*/ State State { get; set; }

        [DisplayName("Zip"), StringLength(10)]
        public string Zip { get; set; }

        [DisplayName("LAT"), StringLength(20)]
        public string LAT { get; set; }

        [DisplayName("LON"), StringLength(20)]
        public string LON { get; set; }

        [DisplayName("Geo Fence Radius (meter)")]
        int GeoFenceRadiusMeters { get; set; }

        [Required, DisplayName("Region")]
        public int? RegionID { get; set; }

        [ForeignKey("RegionID"), DisplayName("Region")]
        public /*virtual*/ Region Region { get; set; }

        [Required, DisplayName("Ticket Type")]
        public int TicketTypeID { get; set; }

        [ForeignKey("TicketTypeID"), DisplayName("Ticket Type")]
        public /*virtual*/ DestTicketType TicketType { get; set; }

        [Required, DisplayName("Time Zone"), DefaultValue(1)]
        public byte TimeZoneID { get; set; }

        [ForeignKey("TimeZoneID"), DisplayName("Time Zone")]
        public virtual TimeZone TimeZone { get; set; }

        [Required, DefaultValue(true), DisplayName("Use DST?")]
        public bool UseDST { get; set; }

        //[NotMapped]  // Maverick does not like the "full name" format anymore so we should not need this now - 9/23/16
        //public string FullName { get { return this.DestinationType.Name + ": " + this.Name; } }

        [Required, DisplayName("UOM"), DefaultValue(1)]
        public int UomID { get; set; }

        [ForeignKey("UomID"), DisplayName("UOM")]
        public Uom UOM { get; set; }

        [DisplayName("Station"), StringLength(25)]
        public string Station { get; set; }

        [DisplayName("Driving Directions"), StringLength(500)]
        public string DrivingDirections { get; set; }

        [DisplayName("Private Road Miles")]
        public decimal? PrivateRoadMiles { get; set; }

        [DisplayName("Terminal")]
        public int? TerminalID { get; set; }
        [ForeignKey("TerminalID")]
        [DisplayName("Terminal")]
        public Terminal Terminal { get; set; }

        [NotMapped]
        public string NameWithStatus
        {
            get {
                if (Active)
                    return Name;
                else
                    return "[DEACTIVATED] " + Name;
            }
        }
    }
}
