﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace DispatchCrude.Models
{
    [Table("tblDriverShiftType")]
    public class DriverShiftType : AuditModelDeleteBase
    {
        public DriverShiftType()
        {
            Days = new List<DriverShiftTypeDay>();
        }
        [Key]
        public Int16 ID { get; set; }

        [Required, StringLength(25)]
        public string Name { get; set; }

        [NotMapped]
        public int DayCount {
            get
            {
                return Days == null ? 0 : Days.Count();
            }
            set
            {
                if (Days.Count() > value)
                {
                    Days.RemoveRange(Days.Count(), value = Days.Count());
                }
                else if (Days.Count() < value)
                {
                    for (byte i = (byte)Days.Count(); i < value; i++)
                    {
                        Days.Add(new DriverShiftTypeDay(this.ID, i));
                    }
                }
            }
        }

        virtual public List<DriverShiftTypeDay> Days { get; set; }
    }
}
