using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DispatchCrude.Models
{
    [Table("tblSettingType")]
    public class SettingType  : AuditModelLastChangeBase
    {
        public enum TYPE { String = 1, Bool = 2, Int = 3, Double = 4, Html = 5 }

        public SettingType()
        {
            this.Settings = new List<Setting>();
        }

        public IEnumerable<Setting> Settings { get; set; }

        [Key]
        public byte ID { get; set; }

        [Required, StringLength(50)]
        [DisplayName("Type")]
        public string Name { get; set; }
    }
}
