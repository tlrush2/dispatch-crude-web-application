﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DispatchCrude.Models
{
    [Table("tblImportCenterComparisonType")]
    public class ImportCenterComparisonType
    {
        public enum TYPE { None = 0, Equals = 1, PlusMinus1Day = 2, PlusMinus1Hour = 3, IN = 4, NotEquals = 5, NotIN = 6, }

        [Key]
        public int ID { get; set; }

        public string Name { get; set; }
    }
}