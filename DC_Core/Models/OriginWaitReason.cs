﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using System.Linq;

namespace DispatchCrude.Models
{
    [Table("tblOriginWaitReason")]
    public class OriginWaitReason: AuditModelDeleteBase, IValidatableObject
    {
        [Key]
        public int ID { get; set; }

        [DisplayName("Number")]
        [Required, StringLength(5)]
        [RegularExpression(@"[0-9]+", ErrorMessage = "Please enter a numeric value.")]
        public string Num { get; set; }


        public int Number {
            //This field was added to allow actual numeric sorting of the Num field on the new MVC grids
            get
            {
                try
                {
                    return int.Parse(Num);
                }
                catch
                {
                    return -1;
                }                
            }
        }

        [DisplayName("Description")]
        [StringLength(40)]
        public string Description { get; set; }

        [DisplayName("Notes Required?")]
        [Required, DefaultValue(false)]
        public bool RequireNotes { get; set; }

        /* return a negative ID value when notes are required */
        [NotMapped]
        public int NID
        {
            get
            {
                return RequireNotes ? -1 * ID : ID;
            }
            set
            {
                ID = Math.Abs(value);
            }
        }

        [NotMapped]
        public string NumDesc
        {
            get
            {
                return string.Format("{0}: {1}", Num, Description);
            }
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!ValidateDuplicateName(ID, Num))
                yield return new ValidationResult("Number is already in use");
            Validated = true;
        }

        private bool ValidateDuplicateName(int id, string num)
        {
            using (DispatchCrudeDB db = new DispatchCrudeDB())
            {
                return db.OriginWaitReasons.Where(m => m.Num.Equals(num, StringComparison.CurrentCultureIgnoreCase) && (id == 0 || m.ID != id)).Count() == 0;
            }
        }
    }
}