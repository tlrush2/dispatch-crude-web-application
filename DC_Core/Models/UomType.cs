using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.SqlClient;
using AlonsIT;

namespace DispatchCrude.Models
{

    [Table("tblUomType")]
    public class UomType : AuditModelDeleteBase
    {
        public enum Types { VOLUME = 1, WEIGHT = 2 }

        [Key]
        public int ID { get; set; }

        [Required, StringLength(25)]
        public string Name { get; set; }
    }
}
