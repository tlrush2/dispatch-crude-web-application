using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web.Mvc;

namespace DispatchCrude.Models
{
    [Table("tblSetting")]
    public class Setting : AuditModelLastChangeBase
    {
        [Key]
        public int ID { get; set; }

        [Required, StringLength(75)]
        [DisplayName("Setting")]
        public string Name { get; set; }

        [AllowHtml]
        [DisplayName("Value")]
        public string Value { get; set; }

        [StringLength(35)]
        [DisplayName("Category")]
        public string Category { get; set; }

        [DisplayName("Setting Type")]        
        public byte SettingTypeID { get; set; }
        [ForeignKey("SettingTypeID")]
        public virtual SettingType SettingType { get; set; }

        [ReadOnly(true)]
        public bool ReadOnly { get; set; }

        public string Description { get; set; }


        // 6/10/16 - BB - The "Value" field needs to be decorated with [AllowHtml] in order to 
        // prevent an error that happens across the https connection when trying to put html code in the email body text setting.
    }
}
