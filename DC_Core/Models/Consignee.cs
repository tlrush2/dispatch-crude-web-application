using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using DispatchCrude.Extensions;

namespace DispatchCrude.Models
{
    [Table("tblConsignee")]
    public class Consignee : AuditModelDeleteBase, IValidatableObject
    {
        public Consignee()
        {
//            this.Destinations = new List<Destination>();
        }

//        public ICollection<Destination> Destinations { get; set; }

        [Key]
        public int ID { get; set; }

        [Required, StringLength(60)]
        public string Name { get; set; }

        [StringLength(40)]
        public string Address { get; set; }

        [StringLength(30)]
        public string City { get; set; }

        [UIHint("_StateDDL")]
        [DisplayName("State")]
        public virtual int? StateID { get; set; }
        [ForeignKey("StateID")]
        public virtual State State { get; set; }

        [StringLength(10)]
        public string Zip { get; set; }

        [DisplayName("Contact Name")]
        [StringLength(40)]
        public string ContactName { get; set; }

        [DisplayName("Contact Email")]
        [StringLength(75)]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Please enter a valid email address.")]
        public string ContactEmail { get; set; }

        [UIHint("PhoneNumber")]
        [DisplayName("Contact Phone")]
        [StringLength(20)]
        public string ContactPhone { get; set; }

        [StringLength(255)]
        public string Notes { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!ValidateDuplicateName(ID, Name))
                yield return new ValidationResult("Name is already in use");
            Validated = true;
        }

        private bool ValidateDuplicateName(int id, string name)
        {
            using (DispatchCrudeDB db = new DispatchCrudeDB())
            {
                return db.Consignees.Where(m => m.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase) && (id == 0 || m.ID != id)).Count() == 0;
            }
        }

    }

    [Table("tblDestinationConsignees")]
    public class DestinationConsignee : IEDTO, IValidatableObject
    {
        public int ID { get; set; }

        [UIHint("_ForeignKeyDDL_S2"), RequiredGreaterThanZero]
        [DisplayName("Consignee")]
        public int ConsigneeID { get; set; }
        [ForeignKey("ConsigneeID")]
        public Consignee Consignee { get; set; }

        [UIHint("_ForeignKeyDDL_S2"), RequiredGreaterThanZero]
        [DisplayName("Destination")]
        public int DestinationID { get; set; }
        [ForeignKey("DestinationID")]
        public Destination Destination { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!ValidateDuplicate(ID, ConsigneeID, DestinationID))
                yield return new ValidationResult("This relationship already exists!");
            Validated = true;
        }

        private bool ValidateDuplicate(int id, int ConsigneeID, int DestinationID)
        {
            using (DispatchCrudeDB db = new DispatchCrudeDB())
            {
                return db.DestinationConsignees.Where(m => m.ConsigneeID == ConsigneeID && m.DestinationID == DestinationID
                                    && (id == 0 || m.ID != id)).Count() == 0;
            }
        }

    }
}
