using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DispatchCrude.Models
{
    [Table("tblDriverEquipmentManufacturer")]
    public class DriverEquipmentManufacturer : AuditModelDeleteBase, IValidatableObject
    {
        public DriverEquipmentManufacturer()
        {
        }
        
        [Key]
        public int ID { get; set; }

        [Required, StringLength(50)]        
        public string Name { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!ValidateDuplicateName(ID, Name))
                yield return new ValidationResult("Name is already in use");
            Validated = true;
        }

        private bool ValidateDuplicateName(int id, string name)
        {
            using (DispatchCrudeDB db = new DispatchCrudeDB())
            {
                return db.DriverEquipmentManufacturers.Where(m => m.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase) && (id == 0 || m.ID != id)).Count() == 0;
            }
        }

        public override bool allowRowEdit
        {
            get
            {
                //Override normal edit behavior: The "Unknown" record needs to be permanent due to SQL dependency (spDriverSync_UpdateDevices)
                return ID != 1;
            }
        }

        public override bool allowRowDeactivate
        {
            get
            {
                //Override normal deactivate behavior: The "Unknown" record needs to be permanent due to SQL dependency (spDriverSync_UpdateDevices)
                return ID != 1;
            }
        }
    }
}
