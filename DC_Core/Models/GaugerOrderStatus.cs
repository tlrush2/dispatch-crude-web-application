using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DispatchCrude.Models
{
    [Table("tblGaugerOrderStatus")]
    public class GaugerOrderStatus : Sync.GaugerApp.GaugerOrderStatusGA
    {
        [NotMapped, DisplayName("Status #")]
        public int StatusNum { get { return 10 - ID; } }
    }

}
