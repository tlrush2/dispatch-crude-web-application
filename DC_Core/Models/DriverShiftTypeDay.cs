﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchCrude.Models
{

    [Table("tblDriverShiftTypeDay")]
    public class DriverShiftTypeDay: AuditModelCreateBase
    {
        public DriverShiftTypeDay() { }
        public DriverShiftTypeDay(Int16 driverShiftTypeID, byte position, Int16 statusID = (Int16)DriverScheduleStatus.TYPE.UnDefined, byte? startHours = null)
        {
            DriverShiftTypeID = driverShiftTypeID;
            Position = position;
            StatusID = statusID;
            StartHours = startHours;
        }

        [Key]
        public int ID { get; set; }

        public Int16 DriverShiftTypeID { get; set; }

        public byte Position { get; set; }

        public Int16 StatusID { get; set; }

        public byte? StartHours { get; set; }

        [DisplayName("Hours")]
        [Range(0, 24)]
        public byte? DurationHours { get; set; }

        [ForeignKey("DriverShiftTypeID")]
        public DriverShiftType DriverShiftType { get; set; }

        [ForeignKey("StatusID")]
        virtual public DriverScheduleStatus Status { get; set; }
    }
}
