﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DispatchCrude.Models
{
    [Table("tblGaugerAppExceptionLog")]
    public class GaugerAppExceptionLog: Sync.GaugerApp.ExceptionLog
    {
        public GaugerAppExceptionLog()
        {
            this.UID = new Guid();
        }
        public GaugerAppExceptionLog(Guid uid)
        {
            this.UID = uid;
        }
    }
}