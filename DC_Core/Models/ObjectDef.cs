using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace DispatchCrude.Models
{
    [Table("tblObject")]
    public class ObjectDef : AuditModelCreateBase
    {
        public enum TYPE {  Order = 1, Ticket = 2, }

        public ObjectDef()
        {
            this.Fields = new List<ObjectField>();
        }

        [Key]
        public int ID { get; set; }

        [Required, StringLength(30)]
        public string Name { get; set; }

        [StringLength(255)]
        public string SqlSourceName { get; set; }
        [StringLength(255)]
        public string SqlTargetName { get; set; }

        public ICollection<ObjectField> Fields { get; set; }

        public ICollection<ImportCenterDefinition> ImportCenterDefinitions { get; set; }

        [NotMapped]
        public Type Class
        {
            get {
                Type t = Type.GetType("DispatchCrude.Models." + Name.Replace(" ", ""));
                return (t == null) ? Type.GetType("Object") : t;
            }

        }

        [NotMapped]
        public bool CanDelete
        {
            get
            {
                return (Class != null && Class.GetProperty("DeleteDateUTC") != null);
            }
        }

        [NotMapped]
        public string KeyField
        {
            get
            {
                if (Class != null)
                {
                    if (Class.GetProperty("Name") != null)
                        return "Name";
                    if (Class.GetProperty("OrderNum") != null)
                        return "OrderNum";
                    if (Class.GetProperty("FirstName") != null && Class.GetProperty("LastName") != null)
                        return "FirstName+' '+LastName";
                    if (Class.GetProperty("IDNumber") != null)
                        return "IDNumber";
                    if (Class.GetProperty("Description") != null)
                        return "Description";
                }

                return "ID";
            }
        }

    }

}
