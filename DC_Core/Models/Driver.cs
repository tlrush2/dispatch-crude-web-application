using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using System.Linq;
using DispatchCrude.Extensions;
using AlonsIT;
using System.Web;

namespace DispatchCrude.Models
{
    [Table("tblDriver")]
    public class Driver : AuditModelDeleteBase, IValidatableObject
    {
        public Driver()
        {
        }

        [Key]
        public int ID { get; set; }

        [DisplayName("First Name")]
        [Required, StringLength(20)]
        public string FirstName { get; set; }

        [DisplayName("Last Name")]
        [Required, StringLength(20)]
        public string LastName { get; set; }

        [DisplayName("Carrier")]
        [UIHint("_ForeignKeyDDL")]
        
        [RequiredGreaterThanZero]
        public int CarrierID { get; set; }
        [ForeignKey("CarrierID")]
        public /*virtual*/ Carrier Carrier { get; set; }

        [DisplayName("Truck")]
        [UIHint("_ForeignKeyDDL")]
        [Required]
        public int TruckID { get; set; }
        [ForeignKey("TruckID")]
        public /*virtual*/ Truck Truck { get; set; }

        [DisplayName("Trailer")]
        [UIHint("_ForeignKeyDDL")]
        [Required]
        public int TrailerID { get; set; }
        [ForeignKey("TrailerID")]
        public /*virtual*/ Trailer Trailer { get; set; }

        [DisplayName("Trailer 2")]
        [UIHint("_ForeignKeyDDL")]
        public int? Trailer2ID { get; set; }
        [ForeignKey("Trailer2ID")]
        public /*virtual*/ Trailer Trailer2 { get; set; }

        [UIHint("SSN")]
        [DisplayName("SSN")]
        public string SSN { get; set; }

        [DisplayName("Terminal")]
        [UIHint("_ForeignKeyDDL")]
        public int? TerminalID { get; set; }
        [ForeignKey("TerminalID")]
        [DisplayName("Terminal")]
        public Terminal Terminal { get; set; }

        [DisplayName("Operating Region")]
        [UIHint("_ForeignKeyDDL")]
        public int? RegionID { get; set; }
        [ForeignKey("RegionID")]
        public /*virtual*/ Region Region { get; set; }

        [DisplayName("Operating State")]
        [UIHint("_ForeignKeyDDL")]
        public int? OperatingStateID { get; set; }
        [ForeignKey("OperatingStateID")]
        public /*virtual*/ State OperatingState { get; set; }

        [DisplayName("Hire Date")]
        [UIHint("Date")]
        public DateTime? HireDate { get; set; }

        [UIHint("Date")]
        public DateTime? DOB { get; set; }

        [Required, StringLength(20), DisplayName("ID #")]                  //TODO: ensure this is validated to be unique on the new page
        public string IDNumber { get; set; }

        [DisplayName("Mobile App?")]
        [UIHint("Switch")]
        [Required, DefaultValue(true)]
        public bool MobileApp { get; set; }

        [DisplayName("Mobile Print?")]
        [UIHint("Switch")]
        [Required, DefaultValue(true)]
        public bool MobilePrint { get; set; }


        [StringLength(40), DisplayName("Address")]
        public string Address { get; set; }

        [StringLength(30), DisplayName("City")]
        public string City { get; set; }

        [DisplayName("State")]
        [UIHint("_ForeignKeyDDL")]
        public int? StateID { get; set; }
        [ForeignKey("StateID")]
        public /*virtual*/ State State { get; set; }

        [StringLength(10), DisplayName("Zip")]
        public string Zip { get; set; }

        [UIHint("PhoneNumber")]
        [DisplayName("Mobile Phone")]
        public string MobilePhone { get; set; }

        [DisplayName("Mobile Provider")]
        [StringLength(30)]
        public string MobileProvider { get; set; }

        [StringLength(75), DisplayName("Email")]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage="Please enter a valid email address.")]
        public string Email { get; set; }
        
        [DisplayName("Driver Group")]
        [UIHint("_ForeignKeyDDL")]
        public int? DriverGroupID { get; set; }
        [ForeignKey("DriverGroupID")]
        public /*virtual*/ DriverGroup DriverGroup { get; set; }


        [DisplayName("Shift Type")]
        [UIHint("_ForeignKeyDDL")]
        public Int16? DriverShiftTypeID { get; set; }
        [ForeignKey("DriverShiftTypeID")]
        public /*virtual*/ DriverShiftType DriverShiftType { get; set; }

        [DisplayName("Shift Start Date")]
        [UIHint("Date")]
        public DateTime? DriverShiftStartDate { get; set;}

        [DisplayName("Notes")]
        [StringLength(255)]
        public string Notes { get; set; }

        [DisplayName("Termination Date")]
        [UIHint("Date")]
        public DateTime? TerminationDate { get; set; }        
        
        [NotMapped]
        public string FullName
        {
            get { return getName(); }
        }

        [NotMapped]
        public string FullNameLF
        {
            get { return getName(lastNameFirst: true); }
        }

        [NotMapped]
        public string FullName_FlagInactive
        {
            get { return getName(flagDeleted: true); }
        }

        [NotMapped]
        public string FullNameLF_FlagInactive
        {
            get { return getName(lastNameFirst: true, flagDeleted: true); }
        }

        [NotMapped]
        public DriverLocation LastLocation { get; set; }

        public int getLoadCount()
        {
            using (DispatchCrudeDB db = new DispatchCrudeDB())
            {
                return db.Orders.Where(o => o.DriverID == ID 
                                            && o.DeleteDateUTC == null
                                            && (o.StatusID == (int) OrderStatus.STATUS.Dispatched
                                                || o.StatusID == (int) OrderStatus.STATUS.Accepted
                                                || o.StatusID == (int) OrderStatus.STATUS.PickedUp)).Count();
            }
        }

        //10/11/16 Changed the display order to put "deactivated" in front for usability's sake.  
        //When in dropdown boxes "name + [DEACTIVATED]" becomes very difficult to read/hard on 
        //the eyes because it creates an uneven edge.
        public string getName(bool lastNameFirst = false, bool flagDeleted = false )
        {
            if (lastNameFirst)
                return (flagDeleted && Deleted ? "[DEACTIVATED] " + LastName + ", " + FirstName : LastName + ", " + FirstName);
            else
                return (flagDeleted && Deleted ? "[DEACTIVATED] " + FirstName + " " + LastName : FirstName + " " + LastName);
        }

        public static string getNextIDNumber()
        {
            using (SSDB db = new SSDB())
            {
                return db.QuerySingleValue("SELECT max(isnull(CASE WHEN ISNUMERIC(IdNumber) = 1 THEN IdNumber ELSE 0 END, 1000)) + 1 FROM tblDriver").ToString();
            }
        }

        public viewDriver ViewInfo { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var ret = new List<ValidationResult>();
            if (!ValidateDuplicateIdNumber())
                ret.Add(new ValidationResult(string.Format("ID # '{0}' is already in use", this.IDNumber), new string[] { "IdNumber" }));
            if (!ValidateDuplicateName())
                ret.Add(new ValidationResult("Driver already exists for this carrier"));
            Validated = true;
            return ret;
        }

        public bool ValidateDuplicateIdNumber()
        {
            using (DispatchCrudeDB db = new DispatchCrudeDB())
            {
                return db.Drivers.Count(m => m.IDNumber == this.IDNumber && m.ID != this.ID) == 0;
            }
        }
        public bool ValidateDuplicateName()
        {
            using (DispatchCrudeDB db = new DispatchCrudeDB())
            {
                // return false (error) when another driver with same first name last name exists for a carrier, otherwise return true
                return db.Drivers.Count(d => d.CarrierID == this.CarrierID
                                && d.FirstName.Equals(this.FirstName, StringComparison.CurrentCultureIgnoreCase)
                                && d.LastName.Equals(this.LastName, StringComparison.CurrentCultureIgnoreCase)
                                && (this.ID == 0 || d.ID != this.ID)) == 0;
            }
        }
    }

    /* "extra" driver info class to retrieve UserNames values & other "translated" values */
    [Table("viewDriver")]
    public class viewDriver
    {
        [Key][ForeignKey("Driver")]
        public int ID { get; set; }

        public Driver Driver { get; set; }

        [DisplayName("Username(s)")]
        public string UserNames { get; set; }

        [Column("Carrier")]
        [DisplayName("Carrier")]
        public string CarrierName { get; set; }

        [DisplayName("Carrier Type")]
        public string CarrierType { get; set; }

        [Column("Truck")]
        [DisplayName("Truck")]
        public string TruckName { get; set; }

        [Column("Trailer")]
        [DisplayName("Trailer")]
        public string TrailerName { get; set; }

        [Column("Trailer2")]
        [DisplayName("Trailer 2")]
        public string Trailer2Name { get; set; }

        [Column("Region")]
        [DisplayName("Region")]
        public string RegionName { get; set; }

        [DisplayName("Operating State")]
        public string OperatingStateAbbrev { get; set; }

        [DisplayName("State")]
        public string StateAbbrev { get; set; }

        [DisplayName("Driver Group")]
        public string DriverGroupName { get; set; }

        [DisplayName("Shift Type")]
        public string DriverShiftTypeName { get; set; }

        [DisplayName("Mobile App Version")]
        public string MobileAppVersion { get; set; }

        [DisplayName("Tablet ID")]
        public string TabletID { get; set; }

        [DisplayName("Printer Serial")]
        public string PrinterSerial { get; set; }

        [Column("LastDeliveredOrderTime")]
        [DisplayName("Last Delivery")]
        public DateTime? LastDeliveredOrderTimeUTC { get; set; }

        [NotMapped]
        [DisplayName("Last Delivery")]
        public DateTime? LastDeliveredOrderTime
        {
            get
            {
                return LastDeliveredOrderTimeUTC.HasValue 
                    ? DateHelper.ToLocal(LastDeliveredOrderTimeUTC.Value, Core.DispatchCrudeHelper.GetProfileTimeZone(HttpContext.Current))
                    : LastDeliveredOrderTimeUTC;
            }
        }

        [DisplayName("DL #")]
        public string DLNumber { get; set; }

        [DisplayName("DL State")]
        public string DLState { get; set; }

        [DisplayName("Endorsements")]
        public string DLEndorsements { get; set; }

        [DisplayName("Restrictions")]
        public string DLRestrictions { get; set; }

        [DisplayName("H2S Expiration")]
        public DateTime? H2SExpiration { get; set; }

        public bool H2S { get; set; }
    }

    public class DriverEligibility
    {
        public int ID { get; set; }
        public int DriverScore { get; set;}

        public decimal AvailabilityFactor { get; set;}
        public decimal ComplianceFactor { get; set;}
        public decimal TruckAvailabilityFactor { get; set;}
        public decimal HOSFactor { get; set; }
        public int CurrentWorkload { get; set;}
        public int CurrentECOT { get; set;}
        public decimal WeeklyOnDutyHoursLeft { get; set; } 
        public decimal WeeklyDrivingHoursLeft { get; set; } 
        public decimal OnDutyHoursLeft { get; set; }
        public decimal DrivingHoursLeft { get; set; }

        public decimal? Lat { get; set; }
        public decimal? Lon { get; set; }

        public string getMessage()
        {
            string message = "";
            if (AvailabilityFactor == 0)
                message += "Not scheduled! ";
            if (ComplianceFactor == 0)
                message += "Not compliant! ";
            if (TruckAvailabilityFactor == 0)
                message += "Truck not available!";
            if (CurrentECOT/60 > WeeklyOnDutyHoursLeft || CurrentECOT/60 > OnDutyHoursLeft || CurrentECOT/60 > DrivingHoursLeft)
                message += "Workload exceeds hours left!";

            return message;
        }

        public string DriverName
        {
            get
            {
                using (DispatchCrudeDB db = new DispatchCrudeDB())
                {
                    return db.Drivers.Find(ID).FullName + (DriverScore == 0 ? " [NOT AVAILABLE]" : "");
                }
            }
        }
    }

    public class DriverPlusLoc
    {
        public Driver driver { get; set; }
        public DriverEligibility eligibility { get; set; }
        public DriverLocation location { get; set; }
    }
}
