﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using AlonsIT;

namespace DispatchCrude.Models
{
    [Table("tblUserReportEmailSubscription")]
    public class UserReportEmailSubscription : AuditModelLastChangeUTCBase
    {
        [Key]
        public int ID { get; set; }

        [Required]
        public string Name { get; set; }

        [Required, StringLength(100)]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Required")]
        [DisplayName("Report")]
        public int UserReportID { get; set; }

        public bool Sun { get; set; }
        public bool Mon { get; set; }
        public bool Tue { get; set; }
        public bool Wed { get; set; }
        public bool Thu { get; set; }
        public bool Fri { get; set; }
        public bool Sat { get; set; }

        [Required]
        public byte ExecuteHourUTC { get; set; }

        [Required]
        [DisplayName("Recipient Email Addresses")]
        public string EmailAddressCSV { get; set; }

        [ScriptIgnore]
        [JsonIgnore]
        [ForeignKey("UserReportID")]
        public UserReportDefinition UserReport { get; set; }

        private string UserTimeZone { get { return Core.DateHelper.ContextTimeZoneName(HttpContext.Current); } }

        [NotMapped]
        public DateTime? ExecuteTime
        {
            get
            {
                DateTime executeUTC = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, ExecuteHourUTC, 0, 0);
                return Core.DateHelper.ToLocal(executeUTC, UserTimeZone);
            }
            set
            {
                if (value.HasValue) ExecuteHourUTC = Convert.ToByte(Core.DateHelper.ToUTC(value.Value, UserTimeZone).Hour);
            }
        }
    }
}