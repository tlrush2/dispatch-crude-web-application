using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace DispatchCrude.Models
{
    [Table("tblOperator")]
    public class Operator : AuditModelDeleteBase, IValidatableObject
    {
        public Operator()
        {
            this.Origins = new List<Origin>();
            this.Pumpers = new List<Pumper>();
        }

        public ICollection<Origin> Origins { get; set; }
        public ICollection<Pumper> Pumpers { get; set; }

        [Key]
        public int ID { get; set; }

        [Required, StringLength(60)]
        public string Name { get; set; }

        [StringLength(40)]
        public string Address { get; set; }

        [StringLength(30)]
        public string City { get; set; }

        [UIHint("_StateDDL")]
        [DisplayName("State")]
        public virtual int? StateID { get; set; }
        [ForeignKey("StateID")]
        public virtual State State { get; set; }

        [StringLength(10)]
        public string Zip { get; set; }

        [DisplayName("Contact Name")]
        [StringLength(40)]
        public string ContactName { get; set; }

        [DisplayName("Contact Email")]
        [StringLength(75)]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Please enter a valid email address.")]
        public string ContactEmail { get; set; }

        [DisplayName("Contact Phone")]
        [StringLength(20)]
        [RegularExpression(@"\([0-9]{3}\) [0-9]{3}-[0-9]{4}", ErrorMessage = "Please enter a valid phone number (###) ###-####.")]
        public string ContactPhone { get; set; }

        [StringLength(255)]
        public string Notes { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!ValidateDuplicateName(ID, Name))
                yield return new ValidationResult("Name is already in use");
            Validated = true;
        }

        private bool ValidateDuplicateName(int id, string name)
        {
            using (DispatchCrudeDB db = new DispatchCrudeDB())
            {
                return db.Operators.Where(m => m.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase) && (id == 0 || m.ID != id)).Count() == 0;
            }
        }
    }
}
