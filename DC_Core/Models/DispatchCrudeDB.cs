using System;
using System.Linq;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;
using System.Transactions;
using DispatchCrude.Core;
using DispatchCrude.Extensions;
using AlonsIT;
using System.Reflection;
using System.Collections;
using System.Data.Entity.Migrations;
using System.Text.RegularExpressions;

namespace DispatchCrude.Models
{
    public class DispatchCrudeDB : DbContext
    {
        public static string SESSION_ADMIN_SWITCHED_USER = "SESSION_ADMIN_SWITCHED_USER";

        static DispatchCrudeDB()
        {
            Database.SetInitializer<DispatchCrudeDB>(null);
        }

        public DispatchCrudeDB()
            : base("Name=DispatchCrudeDB")
        {
            //this.Database.Log = message => Trace.WriteLine(message);
        }

        // hide the inherited SaveChanges() method - to force the User identity to passed in
        override public int SaveChanges()
        {
            throw new NotImplementedException("Must call the SaveChanges(string userName) method instead");
        }

        /******************************
         * Save Changes
         * parameters:
         * 1) userName: required
         * 2) effectiveDateUTC: optional - supply to override using NOW
         * 3) modelState: optional - supply to get any exceptions returned to the modelState variable (instead of a "stringified" exception list)
         * ***************************/
        public int SaveChanges(string userName, DateTime? effectiveDateUTC = null, ModelStateDictionary modelState = null)
        {
            int ret = 0;
            try
            {
                string sessionName = DBHelper.ToString(HttpContext.Current.Session[SESSION_ADMIN_SWITCHED_USER]);
                if (string.IsNullOrEmpty(sessionName) == false && sessionName != userName)
                {
                    // add log session admin in parenthesis
                    userName = userName + " (" + sessionName + ")";
                }
                ChangeTracker.DetectChanges();

                // Get all Added/Deleted/Modified entities (not Unmodified or Detached)
                if (!effectiveDateUTC.HasValue)
                    effectiveDateUTC = DateTime.UtcNow;
                DateTime effectiveDateLocal = effectiveDateUTC.Value.ToLocalTime();
                foreach (DbEntityEntry ent in this.ChangeTracker.Entries())
                {
                    if (ent.State == EntityState.Added)
                    {
                        // if the State is Added, but an ID is already available, then we have to assume this is an unchanged record (and DETACHED)
                        if (ent.Entity.GetType().GetProperty("ID") != null && DBHelper.ToInt32(ent.Entity.GetType().GetProperty("ID").GetValue(ent.Entity)) != 0)
                            ent.State = EntityState.Detached;
                        else
                        {
                            bool matched = false;
                            if (matched = ent.Entity is AuditModelCreateBase)
                                ent.Property("CreateDate").CurrentValue = effectiveDateLocal;
                            else if (matched = ent.Entity is AuditModelCreateUTCBase)
                                ent.Property("CreateDateUTC").CurrentValue = effectiveDateUTC.Value;
                            if (matched)
                                ent.Property("CreatedByUser").CurrentValue = userName;
                            if (ent.Entity is AuditModelDeleteBase && !(ent.Entity as AuditModelDeleteBase).DeleteDateUTC.HasValue)
                                (ent.Entity as AuditModelDeleteBase).Active = true;
                        }

                    }
                    else if (ent.State == EntityState.Modified)
                    {
                        bool matched = false;
                        if (matched = ent.Entity is AuditModelLastChangeBase)
                            ent.Property("LastChangeDate").CurrentValue = effectiveDateLocal;
                        else if (matched = ent.Entity is AuditModelLastChangeUTCBase)
                            ent.Property("LastChangeDateUTC").CurrentValue = effectiveDateUTC.Value;
                        if (matched)
                            ent.Property("LastChangedByUser").CurrentValue = userName;
                    }
                    // if the record is being deleted and the table contains a DeletedDate field, then mark as deleted (but don't actually delete the record)
                    else if (ent.State == EntityState.Deleted && ent.OriginalValues != null && ent.OriginalValues.PropertyNames.Contains("DeleteDateUTC"))
                    {
                        bool matched = false, undelete = DBHelper.ToBoolean(ent.Property("Deleted").CurrentValue);
                        // attempt a regular "delete" if PermanentDelete=TRUE was specified (if it fails then the operation will entire operation will fail)
                        if (matched = (ent.Entity is AuditModelDeleteBase && !(ent.Entity as AuditModelDeleteBase).PermanentDelete))
                            ent.Property("DeleteDate").CurrentValue = undelete ? null : (object)effectiveDateLocal;
                        else if (matched = ent.Entity is AuditModelDeleteUTCBase)
                            ent.Property("DeleteDateUTC").CurrentValue = undelete ? null : effectiveDateUTC;
                        if (matched)
                        {
                            // if the DeleteDate parameter is available, then we are not actually deleting but instead marking them deleted
                            ent.State = EntityState.Modified;
                            ent.Property("DeletedByUser").CurrentValue = undelete ? null : userName;

                            // reset any Nullable property value back to the original value (for some reason these sometimes were getting set to NULL on a db.xxx.Remove() operation)
                            // KDA: I believe EF sets these values to NULL as its way of not deleting any referenced (parent) objects on a true DELETE operation, but since we aren't deleting, we need to ensure we don't lose any current data
                            foreach (string propName in ent.OriginalValues.PropertyNames.Except(new string[] { "DeleteDate", "DeleteDateUTC", "DeletedByUser" }))
                            {
                                if (ent.Property(propName) != null && ent.Property(propName).CurrentValue == null && ent.Property(propName).OriginalValue != null)
                                    ent.Property(propName).CurrentValue = ent.Property(propName).OriginalValue;
                            }
                        }
                    }
                }
                ret = base.SaveChanges();
            }
            catch (DbUpdateException dbex)
            {
                throw new Exception(FormatDbUpdateException(dbex, modelState));
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException vex)
            {
                string msg = string.Empty;
                foreach (System.Data.Entity.Validation.DbEntityValidationResult res in vex.EntityValidationErrors)
                {
                    foreach (System.Data.Entity.Validation.DbValidationError err in res.ValidationErrors)
                    {
                        if (modelState != null) modelState.AddModelError(err.PropertyName, err.ErrorMessage);
                        msg += string.Format("{0}: {1}\n;", err.PropertyName, err.ErrorMessage);
                    }
                }
                throw new Exception(msg);
            }
            return ret;
        }

        static protected string FormatDbUpdateException(DbUpdateException dbex, ModelStateDictionary modelState = null)
        {
            //string example = @"Violation of UNIQUE KEY constraint 'UQ_DriverShiftType_Name'. Cannot insert duplicate key in object 'dbo.tblDriverShiftType'.The duplicate key value is (999).The statement has been terminated.";
            string msg = dbex.GetBaseException().Message;

            Regex regex = new Regex(@"Violation of UNIQUE KEY constraint 'UQ_[^'._]*_([^'._]*)'.*The duplicate key value is \(([^\)]*)\)");
            foreach (Match match in regex.Matches(msg))
            {
                if (match.Groups.Count == 3)
                {
                    if (modelState != null)
                        modelState.AddModelError(match.Groups[1].Value, string.Format("Duplicate {0} value = {1} was submitted", match.Groups[2].Value));
                    else
                        return string.Format("Duplicate value = {1} was submitted - please use a different value and retry", match.Groups[1].Value, match.Groups[2].Value);
                }
            }
            // if no conversions happened, then just return the raw exception message
            if (modelState != null) modelState.AddModelError("", msg);
            return msg;
        }

        // method that can be called to do an add/update + save operation in 1 single step
        public IEDTO AddOrUpdateSave(string userName, IEDTO changes, string recursePropertiesCSV = null, params string[] skipProperties)
        {
            // pass a lower-case string[] of recurseProperties to the base method
            AddOrUpdate(changes, (recursePropertiesCSV ?? "").TrimSplit(true), new HashSet<Type>(), skipProperties);
            SaveChanges(userName);
            return changes;
        }

        // method that allows an add/update operation to happen in a single step (with a single string recursePropertiesCSV value)
        public IEDTO AddOrUpdate(IEDTO changes, string recursePropertiesCSV = null, params string[] skipProperties)
        {
            // pass a lower-case string[] of recurseProperties to the base method
            return AddOrUpdate(changes, (recursePropertiesCSV ?? "").TrimSplit(true), new HashSet<Type>(), skipProperties);
        }

        // if this method is implemented, the caller can intervene during the CopyValues process:
        //   (either update the "changes" values, or replace the entire operation and return TRUE - to indicate that the process is completed)
        public delegate bool UpdateExisting(IEDTO existing, IEDTO changes);
        public event UpdateExisting OnUpdateExisting;

        // method that allows the Type to be specified (instead of inferring the type from the changes object)
        public IEDTO AddOrUpdate(IEDTO changes, params string[] skipProperties)
        {
            Type type = changes.GetType();
            int id = DBHelper.ToInt32(type.GetProperty("ID").GetValue(changes));
            if (id == 0)
            {
                // ensure all new records are marked "Active" (otherwise they will be created deleted)
                if (changes is AuditModelDeleteBase) (changes as AuditModelDeleteBase).Active = true;

                DbSet set = this.Set(type);
                // if the passed in "changes" instance is not the specified "type", then we need to "clone" it into an instance that is
                if (!type.IsInstanceOfType(changes))
                {
                    changes = changes.Clone(type, skipProperties);
                }
                set.Add(changes);
            }
            else
            {
                var existing = this.Set(type).Find(id);
                if (!(OnUpdateExisting?.Invoke(existing as IEDTO, changes as IEDTO) ?? false))
                    CopyEntityValues(existing as IEDTO, changes, skipProperties);
            }
            return changes;
        }

        // PROTECTED method that allows an add/update operation to happen in a single step (with a string array of skipProperties [in lower case])
        protected IEDTO AddOrUpdate(IEDTO changes, string[] recurseProperties, HashSet<Type> doneTypes, params string[] skipProperties)
        {
            // do the basic AddOrUpdate operation first
            IEDTO ret = AddOrUpdate(changes, skipProperties);

            // process any recursive properties (objects) next
            if (recurseProperties == null)
            {
                // record this type as "done" to prevent cycling processing (when recursing)
                doneTypes.Add(changes.GetType());

                // iterate over each public instance property of the incoming object to see if any are other IEDTO objects that might be present
                foreach (PropertyInfo pi in changes.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public))
                {
                    Type piType = pi.PropertyType;
                    if (recurseProperties.Contains(pi.Name.ToLower()) && !doneTypes.Contains(piType) && !skipProperties.Contains(piType.Name))
                    {
                        IEDTO child = null;
                        if (piType.IsSubclassOf(typeof(IEDTO)) && (child = pi.GetValue(changes) as IEDTO) != null)
                        {
                             AddOrUpdate(child, recurseProperties, doneTypes, skipProperties);
                        }
                        else if (piType.IsGenericType && piType.GetGenericArguments().Length > 0 && (piType = piType.GetGenericArguments()[0]).IsSubclassOf(typeof(IEDTO)))
                        {
                            // iterate over every eligible "child" IEDTO instance and call AddOrUpdate on it as well
                            IEnumerable iEnumerator = (IEnumerable)pi.GetValue(changes);
                            if (iEnumerator != null)
                            {
                                foreach (var childObj in iEnumerator)
                                {
                                    child = childObj as IEDTO;
                                    if (child != null)
                                        AddOrUpdate(child, recurseProperties, doneTypes, skipProperties);
                                }
                            }
                        }
                    }
                }
            }

            return ret;
        }

        public IEDTO CopyEntityValues(IEDTO existing, IEDTO changes, params string[] skipProperties)
        {
            this.Entry(existing).CurrentValues.SetValues(changes);

            // keep the original change date/person records
            string rowStateName = null;

            if (existing is AuditModelCreateUTCBase || existing is AuditModelCreateBase)
            {
                this.Entry(existing).CurrentValues[rowStateName = "CreateDateUTC"] = this.Entry(existing).OriginalValues[rowStateName];
                this.Entry(existing).CurrentValues[rowStateName = "CreatedByUser"] = this.Entry(existing).OriginalValues[rowStateName];
            }

            if (existing is AuditModelLastChangeUTCBase || existing is AuditModelLastChangeBase)
            {
                this.Entry(existing).CurrentValues[rowStateName = "LastChangeDateUTC"] = this.Entry(existing).OriginalValues[rowStateName];
                this.Entry(existing).CurrentValues[rowStateName = "LastChangedByUser"] = this.Entry(existing).OriginalValues[rowStateName];
            }

            if (existing is AuditModelDeleteUTCBase || existing is AuditModelDeleteBase)
            {
                this.Entry(existing).CurrentValues[rowStateName = "DeleteDateUTC"] = this.Entry(existing).OriginalValues[rowStateName];
               this.Entry(existing).CurrentValues[rowStateName = "DeletedByUser"] = this.Entry(existing).OriginalValues[rowStateName];

            }
            if (skipProperties != null)
            {
                foreach (string skipProperty in skipProperties.Intersect(this.Entry(existing).CurrentValues.PropertyNames))
                {
                    this.Entry(existing).CurrentValues[skipProperty] = this.Entry(existing).OriginalValues[skipProperty];
                }
            }

            return existing;
        }

        public DbSet<AndroidOSVersion> AndroidOSVersions { get; set; }
        public DbSet<Carrier> Carriers { get; set; }
        public DbSet<CarrierCompliance> CarrierCompliances { get; set; }
        public DbSet<CarrierComplianceType> CarrierComplianceTypes { get; set; }
        public DbSet<CarrierType> CarrierTypes { get; set; }
        public DbSet<Consignee> Consignees { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Contract> Contracts { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Destination> Destinations { get; set; }
        public DbSet<DestinationConsignee> DestinationConsignees { get; set; }
        public DbSet<DestinationType> DestinationTypes { get; set; }
        public DbSet<DestinationWaitReason> DestinationWaitReasons { get; set; }
        public DbSet<DispatchMessage> DispatchMessages { get; set; }
        public DbSet<Driver> Drivers { get; set; }
        public DbSet<DriverCompliance> DriverCompliances { get; set; }
        public DbSet<DriverComplianceCategory> DriverComplianceCategories { get; set; }
        public DbSet<DriverComplianceType> DriverComplianceTypes { get; set; }
        public DbSet<DriverGroup> DriverGroups { get; set; }
        public DbSet<DriverLocation> DriverLocations { get; set; }
        public DbSet<DriverScheduleStatus> DriverScheduleStatuses { get; set; }
        public DbSet<DriverSettlementBatch> DriverSettlementBatches { get; set; }
        public DbSet<DriverSettlementStatementEmailDefinition> DriverSettlementStatementEmailDefinitions { get; set; }
        public DbSet<DriverSettlementStatementEmail> DriverSettlementStatementEmails { get; set; }
        public DbSet<DriverShiftType> DriverShiftTypes { get; set; }
        public DbSet<DriverShiftTypeDay> DriveShipTypeDays { get; set; }
        public DbSet<DriverWeekShiftType> DriverWeekShiftTypes { get; set; }
        public DbSet<DriverAppExceptionLog> DriverAppExceptionLogs { get; set; }
        public DbSet<DriverEquipment> DriverEquipments { get; set; }
        public DbSet<DriverEquipmentManufacturer> DriverEquipmentManufacturers { get; set; }
        public DbSet<DriverEquipmentType> DriverEquipmentTypes { get; set; }
        public DbSet<LocationType> LocationTypes { get; set; }
        public DbSet<Gauger> Gaugers { get; set; }
        public DbSet<GaugerOrder> GaugerOrders { get; set; }
        public DbSet<GaugerTicket> GaugerTickets { get; set; }
        public DbSet<GaugerAppExceptionLog> GaugerAppExceptionLogs { get; set; }
        public DbSet<Hos> Hoss { get; set; }
        //public DbSet<Hos_DBAudit> Hoss_DBAudit { get; set; }
        public DbSet<HosDriverStatus> HosDriverStatuses { get; set; }
        public DbSet<HosEventType> HosEventTypes { get; set; }
        public DbSet<HosPolicy> HosPolicies { get; set; }
        public DbSet<HosSignature> HosSignatures { get; set; }
        public DbSet<BOLField> BOLFields { get; set; }
        public DbSet<DCGroup> DCGroups { get; set; }
        public DbSet<DCMembership> DCMemberships { get; set; }
        public DbSet<DCPermission> DCPermissions { get; set; }
        public DbSet<DCUser> DCUsers { get; set; }
        public DbSet<ImportCenterDefinition> ImportCenterDefinitions { get; set; }
        public DbSet<ImportCenterFieldDefinition> ImportCenterFieldDefinitions { get; set; }
        public DbSet<ImportCenterGroup> ImportCenterGroups { get; set; }
        public DbSet<ImportCenterGroupStep> ImportCenterGroupSteps { get; set; }
        public DbSet<ImportCenterComparisonType> ImportCenterComparisonTypes { get; set; }
        public DbSet<ObjectDef> Objects { get; set; }
        public DbSet<ObjectField> ObjectFields { get; set; }
        public DbSet<ObjectFieldType> ObjectFieldTypes { get; set; }
        public DbSet<ObjectFieldAllowNull> ObjectFieldsAllowNulls { get; set; }
        public DbSet<ObjectCustomData> ObjectCustomData { get; set; }
        public DbSet<Operator> Operators { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<CarrierOrderFinancials> CarrierOrderFinancials { get; set; }
        public DbSet<OrderRejectReason> OrderRejectReasons { get; set; }
        public DbSet<OrderReroute> OrderReroutes { get; set; }
        public DbSet<OrderSignature> OrderSignatures { get; set; }
        public DbSet<OrderPhoto> OrderPhotos { get; set; }
        public DbSet<OrderStatus> OrderStatus { get; set; }
        public DbSet<OrderTicket> OrderTickets { get; set; }
        public DbSet<OrderTicketRejectReason> OrderTicketRejectReasons { get; set; }
        public DbSet<Origin> Origins { get; set; }
        public DbSet<OriginTank> OriginTanks { get; set; }
        public DbSet<OriginTankDefinition> OriginTankDefinitions { get; set; }
        public DbSet<OriginTankStrapping> OriginTankStrappings { get; set; }
        public DbSet<OriginWaitReason> OriginWaitReasons { get; set; }
        public DbSet<OriginType> OriginTypes { get; set; }
        public DbSet<PdfExport> PdfExports { get; set; }
        public DbSet<PdfExportType> PdfExportTypes { get; set; }
        public DbSet<Priority> Priorities { get; set; }
        public DbSet<PrintStatus> PrintStatuses { get; set; }
        public DbSet<PrintTemplate> PrintTemplates { get; set; }
        public DbSet<Producer> Producers { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductGroup> ProductGroups { get; set; }
        public DbSet<Pumper> Pumpers { get; set; }
        public DbSet<QuestionnaireQuestion> QuestionnaireQuestions { get; set; }
        public DbSet<QuestionnaireQuestionType> QuestionnaireQuestionTypes { get; set; }
        public DbSet<QuestionnaireResponse> QuestionnaireResponses { get; set; }
        public DbSet<QuestionnaireResponseBlob> QuestionnaireResponseBlobs { get; set; }
        public DbSet<QuestionnaireSubmission> QuestionnaireSubmissions { get; set; }
        public DbSet<QuestionnaireTemplate> QuestionnaireTemplates { get; set; }
        public DbSet<QuestionnaireTemplateType> QuestionnaireTemplateTypes { get; set; }
        public DbSet<Region> Regions { get; set; }
        public DbSet<ReportDefinition> ReportDefinitions { get; set; }
        public DbSet<ReportColumnDefinition> ReportColumnDefinitions { get; set; }
        public DbSet<ReportFilterOperator> ReportOperators { get; set; }
        public DbSet<ReportFilterType> ReportFilterTypes { get; set; }
        public DbSet<ReportGroupingFunction> ReportGroupingFunctions { get; set; }
        public DbSet<Route> Routes { get; set; }
        public DbSet<Setting> Settings { get; set; }
        public DbSet<SettingType> SettingTypes { get; set; }
        public DbSet<SettlementFactor> SettlementFactors { get; set; }
        public DbSet<State> States { get; set; }
        public DbSet<Terminal> Terminals { get; set; }
        public DbSet<TicketType> TicketTypes { get; set; }
        public DbSet<TimeZone> TimeZones { get; set; }
        public DbSet<Trailer> Trailers { get; set; }
        public DbSet<TrailerType> TrailerTypes { get; set; }
        public DbSet<Truck> Trucks { get; set; }
        public DbSet<TruckCompliance> TruckCompliances { get; set; }
        public DbSet<TruckComplianceType> TruckComplianceTypes { get; set; }
        public DbSet<TrailerCompliance> TrailerCompliances { get; set; }
        public DbSet<TrailerComplianceType> TrailerComplianceTypes { get; set; }
        public DbSet<TruckMaintenance> TruckMaintenance { get; set; }
        public DbSet<TrailerMaintenance> TrailerMaintenance { get; set; }
        public DbSet<TruckTrailerMaintenanceType> TruckTrailerMaintenanceTypes { get; set; }
        public DbSet<TruckType> TruckTypes { get; set; }
        public DbSet<Uom> Uoms { get; set; }
        public DbSet<UomType> UomTypes { get; set; }        
        public DbSet<UserReportDefinition> UserReports { get; set; }
        public DbSet<UserReportEmailSubscription> UserReportEmailSubscriptions { get; set; }

        public DbSet<CommodityPrice> CommodityPrices { get; set; }
        public DbSet<CommodityIndex> CommodityIndexes { get; set; }
        public DbSet<CommodityMethod> CommodityMethods { get; set; }
        public DbSet<CommodityPurchasePricebook> CommodityPurchasePricebooks { get; set; }
        public DbSet<NonTradeDays> NonTradeDays { get; set; }

        public DbSet<OrderApprovalRO> OrderApprovals { get; set; }        

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DriverLocation>().Property(x => x.Lat).HasPrecision(9, 6);
            modelBuilder.Entity<DriverLocation>().Property(x => x.Lon).HasPrecision(9, 6);

            modelBuilder.Entity<Hos>().Property(x => x.Lat).HasPrecision(9, 6);
            modelBuilder.Entity<Hos>().Property(x => x.Lon).HasPrecision(9, 6);

            modelBuilder.Entity<Order>().Property(x => x.OriginGrossUnits).HasPrecision(9, 3);
            modelBuilder.Entity<Order>().Property(x => x.OriginNetUnits).HasPrecision(9, 3);
            modelBuilder.Entity<Order>().Property(x => x.OriginWeightNetUnits).HasPrecision(9, 3);
            modelBuilder.Entity<Order>().Property(x => x.OriginSulfurContent).HasPrecision(10, 4);
            modelBuilder.Entity<Order>().Property(x => x.DestGrossUnits).HasPrecision(9, 3);
            modelBuilder.Entity<Order>().Property(x => x.DestNetUnits).HasPrecision(9, 3);
            modelBuilder.Entity<Order>().Property(x => x.DestProductTemp).HasPrecision(9, 3);
            modelBuilder.Entity<Order>().Property(x => x.DestProductBSW).HasPrecision(9, 3);
            modelBuilder.Entity<Order>().Property(x => x.DestProductGravity).HasPrecision(9, 3);
            modelBuilder.Entity<Order>().Property(x => x.DestOpenMeterUnits).HasPrecision(18, 3);
            modelBuilder.Entity<Order>().Property(x => x.DestCloseMeterUnits).HasPrecision(18, 3);

            modelBuilder.Entity<OrderTicket>().Property(x => x.OpenMeterUnits).HasPrecision(18, 3);
            modelBuilder.Entity<OrderTicket>().Property(x => x.CloseMeterUnits).HasPrecision(18, 3);
            modelBuilder.Entity<OrderTicket>().Property(x => x.GrossUnits).HasPrecision(9, 3);
            modelBuilder.Entity<OrderTicket>().Property(x => x.NetUnits).HasPrecision(9, 3);
            modelBuilder.Entity<OrderTicket>().Property(x => x.ProductHighTemp).HasPrecision(9, 3);
            modelBuilder.Entity<OrderTicket>().Property(x => x.ProductLowTemp).HasPrecision(9, 3);
            modelBuilder.Entity<OrderTicket>().Property(x => x.ProductObsTemp).HasPrecision(9, 3);
            modelBuilder.Entity<OrderTicket>().Property(x => x.ProductBSW).HasPrecision(9, 3);
            modelBuilder.Entity<OrderTicket>().Property(x => x.ProductObsGravity).HasPrecision(9, 3);
            modelBuilder.Entity<OrderTicket>().Property(x => x.ProductObsSpecificWeight).HasPrecision(10, 4);
            modelBuilder.Entity<OrderTicket>().Property(x => x.WeightGrossUnits).HasPrecision(9, 3);
            modelBuilder.Entity<OrderTicket>().Property(x => x.WeightNetUnits).HasPrecision(9, 3);
            modelBuilder.Entity<OrderTicket>().Property(x => x.WeightTareUnits).HasPrecision(9, 3);
			
			modelBuilder.Entity<GaugerTicket>().Property(x => x.ProductBSW).HasPrecision(9, 3);

            modelBuilder.Entity<OriginTankDefinition>().Property(x => x.TopBPQ).HasPrecision(18, 10);
            modelBuilder.Entity<OriginTankStrapping>().Property(x => x.BPQ).HasPrecision(18, 10);

            modelBuilder.Entity<Terminal>().Property(x => x.Lat).HasPrecision(18, 10);
            modelBuilder.Entity<Terminal>().Property(x => x.Lon).HasPrecision(18, 10);

            modelBuilder.Entity<Uom>().Property(x => x.GallonEquivalent).HasPrecision(18, 12);

            /*
            // Map Users to Groups:
            modelBuilder.Entity<ApplicationGroup>()
                .HasMany((ApplicationGroup g) => g.Users)
                .WithRequired()
                .HasForeignKey<string>((UserInGroup ag) => ag.GroupId.ToString());

            modelBuilder.Entity<UserInGroup>()
                .HasKey((UserInGroup r) =>
                    new
                    {
                        UserId = r.UserId,
                        GroupId = r.GroupId
                    }).ToTable("aspnet_UsersInGroups");
        	*/

            // Map Group to Users
            modelBuilder.Entity<DCGroup>()
                             .HasMany(d => d.Users).WithMany(c => c.Groups)
                             .Map(x => x.MapLeftKey("GroupId")
                                 .MapRightKey("UserID")
                                 .ToTable("aspnet_UsersInGroups"));
            
            // Map Group to Permissions (roles)
            modelBuilder.Entity<DCGroup>()
                             .HasMany(d => d.Permissions).WithMany(c => c.Groups)
                             .Map(x => x.MapLeftKey("GroupId")
                                 .MapRightKey("RoleId")
                                 .ToTable("aspnet_RolesInGroups"));


            // Override ObjectDef to use Object (not allowed as a class within C#)
            modelBuilder.Entity<ObjectField>()
                .HasRequired(f => f.ObjectDef)
                .WithMany(o => o.Fields)
                .Map(x => x.MapKey("ObjectID"));


            // Map Shippers (Customers) to Origins
            modelBuilder.Entity<Customer>()
                .HasMany(s => s.Origins).WithMany(d => d.Shippers)
                .Map(x => x.MapLeftKey("CustomerID").MapRightKey("OriginID").ToTable("tblOriginCustomers")
                );
/*
            // Map Products to Origins
            modelBuilder.Entity<Product>()
                .HasMany(s => s.Origins).WithMany(d => d.Products)
                .Map(x => x.MapLeftKey("ProductID").MapRightKey("OriginID").ToTable("tblOriginProducts")
                );
*/
            // Map Shippers (Customers) to Destinations
            modelBuilder.Entity<Customer>()
                .HasMany(s => s.Destinations).WithMany(d => d.Shippers)
                .Map(x => x.MapLeftKey("CustomerID").MapRightKey("DestinationID").ToTable("tblDestinationCustomers")
                );

            // Map Products to Destinations
            modelBuilder.Entity<Product>()
                .HasMany(s => s.Destinations).WithMany(d => d.Products)
                .Map(x => x.MapLeftKey("ProductID").MapRightKey("DestinationID").ToTable("tblDestinationProducts")
                );
        }
    }

}
