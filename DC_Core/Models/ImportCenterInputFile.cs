﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DispatchCrude.Models
{
    [Table("tblImportCenterInputFile")]
    public class ImportCenterInputFile: AuditModelCreateUTCBase
    {
        public ImportCenterInputFile()
        {
        }

        [Key][ForeignKey("ImportCenterDefinition")]
        public int ImportCenterDefinitionID { get; set; }

        [Required]
        public string OriginalFileName { get; set; }

        [Required]
        public string InputDataTableXml { get; set; }

        [Required]
        public string FieldNameCSV { get; set; }

        public ImportCenterDefinition ImportCenterDefinition { get; set; }
    }
}