﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DispatchCrude.Models.Sync
{
    public class OrderPresent
    {
        public int ID { get; set; }
        public DateTime? LastChangeDateUTC { get; set; }
        public DateTime? OELastChangeDateUTC { get; set; }
    }
}