﻿using System;
using System.Linq;
using System.Reflection;
using System.Data;
using System.Collections.Generic;
using AlonsIT;

namespace DispatchCrude.Models.Sync
{
    public class SyncHelper
    {
        static public bool UpdateValues(object source, object dest, string[] skipPropertyNames = null, string[] includePropertyNames = null)
        {
            bool ret = false;

            // note: this uses reflection which has performance ramifications
            // get the "type" of the source object (generically)
            Type sourceItemType = source.GetType();

            // get the "type" of the destination object (generically)
            Type destItemType = dest.GetType();

            // iterate over all public instance properties of the destination object and update those values available in the source object
            foreach (PropertyInfo pi in destItemType.GetProperties(BindingFlags.Instance | BindingFlags.Public))
            {
                bool done = !pi.CanWrite; // default to DONE = True if not writable

                if (!done && includePropertyNames != null)
                {
                    done = true;  // default to TRUE since we are only including those that are specified
                    foreach (string includePropertyName in includePropertyNames)
                    {
                        if (includePropertyName.Equals(pi.Name, StringComparison.CurrentCultureIgnoreCase))
                        {
                            done = false;
                            break;
                        }
                    }
                }
                if (!done && skipPropertyNames != null)
                {
                    foreach (string skipPropertyName in skipPropertyNames)
                    {
                        if (skipPropertyName.Equals(pi.Name, StringComparison.CurrentCultureIgnoreCase))
                        {
                            done = true;
                            break;
                        }
                    }
                }
                if (!done)
                {
                    PropertyInfo spi = sourceItemType.GetProperty(pi.Name);
                    if (spi != null)
                    {
                        pi.SetValue(dest, spi.GetValue(source, null), null);
                        ret = true;
                    }
                }
            }
            return ret;
        }

        // public static support methods
        static public void PopulateListFromDataTable(object list, System.Data.DataTable dt)
        {
            // note: this uses reflection which has performance ramifications
            foreach (System.Data.DataRow row in dt.Rows)
            {
                String propName = null;
                try
                {
                    // get the "type" of the list elements (generically)
                    Type itemType = list.GetType().GetProperty("Item").PropertyType;
                    // create a new element instance
                    var item = Activator.CreateInstance(itemType);
                    // iterate over all public instance properties to see if data from the datarow can be used to populate it
                    foreach (PropertyInfo pi in itemType.GetProperties(BindingFlags.Instance | BindingFlags.Public))
                    {
                        propName = pi.Name;
                        // update the new element from the DataRow (clone the row as a list element)
                        if (row.Table.Columns.Contains(pi.Name))
                        {
                            pi.SetValue(item, row[pi.Name] == DBNull.Value ? null : row[pi.Name], null);
                        }
                    }
                    // add the newly created item to the list
                    list.GetType().InvokeMember("Add", BindingFlags.Instance | BindingFlags.InvokeMethod | BindingFlags.Public, null, list, new object[] { item });
                }
                catch (Exception e)
                {
                    throw new Exception(String.Format("Error in PopulateListFromDataTable: Field={0}", propName), e);
                }
            }
        }

        static public object SyncDateWhereClause(DateTime? syncDateUTC, string tblAlias = "", bool hasDeleteDateField = false, bool noneIfNull = false)
        {
            tblAlias = tblAlias.Length > 0 ? tblAlias + "." : "";
            string syncDateQS = syncDateUTC.HasValue ? DBHelper.QuoteStr(syncDateUTC.Value.ToString("M/dd/yyyy HH:mm:ss")) : "";

            return !syncDateUTC.HasValue 
                ? (noneIfNull ? "1=0" : (hasDeleteDateField ? string.Format("{0}DeleteDateUTC IS NULL", tblAlias) : "1=1"))
                : string.Format("({1}LastChangeDateUTC >= {0} AND {2})"
                    , syncDateQS
                    , tblAlias
                    , hasDeleteDateField ? string.Format("({1}DeleteDateUTC IS NULL OR {1}DeleteDateUTC >= {0})", syncDateQS, tblAlias) : "1=1");
        }

        static public object SyncDateWhereCreateClause(DateTime? syncDateUTC, string tblAlias = "", bool hasDeleteDateField = false, bool noneIfNull = false)
        {
            tblAlias = tblAlias.Length > 0 ? tblAlias + "." : "";
            string syncDateQS = syncDateUTC.HasValue ? DBHelper.QuoteStr(syncDateUTC.Value.ToString("M/dd/yyyy HH:mm:ss")) : "";

            return !syncDateUTC.HasValue
                ? (noneIfNull ? "1=0" : (hasDeleteDateField ? string.Format("{0}DeleteDateUTC IS NULL", tblAlias) : "1=1"))
                : string.Format("({1}CreateDateUTC >= {0} AND {2})"
                    , syncDateQS
                    , tblAlias
                    , hasDeleteDateField ? string.Format("({1}DeleteDateUTC IS NULL OR {1}DeleteDateUTC >= {0})", syncDateQS, tblAlias) : "1=1");
        }

        // public static support methods
        static public DataTable PopulateDataTableFromList<T>(List<T> list, string[] skipPropertyNames = null)
        {
            DataTable ret = new DataTable();
            // note: this uses reflection which has performance ramifications

            // get the "type" of the list elements (generically)
            Type itemType = typeof(T);
            // iterate over all public instance properties and add each as a DataColumn to the returned DataTable
            foreach (PropertyInfo pi in itemType.GetProperties(BindingFlags.Instance | BindingFlags.Public))
            {
                if (skipPropertyNames == null || !skipPropertyNames.Contains(pi.Name))
                {
                    bool allowNull = true;
                    Type propType = Nullable.GetUnderlyingType(pi.PropertyType);
                    if (propType == null)
                    {
                        propType = pi.PropertyType;
                        allowNull = false;
                    }
                    ret.Columns.Add(pi.Name, propType).AllowDBNull = allowNull;
                }
            }

            foreach (T item in list)
            {
                DataRow row = ret.NewRow();
                // iterate over all public instance properties and add each as a DataColumn to the returned DataTable
                foreach (PropertyInfo pi in itemType.GetProperties(BindingFlags.Instance | BindingFlags.Public))
                {
                    if (ret.Columns.Contains(pi.Name))
                    {
                        object value = pi.GetValue(item, BindingFlags.Instance | BindingFlags.Public, null, null, null);
                        row[pi.Name] = value == null ? (row.Table.Columns[pi.Name].AllowDBNull ? DBNull.Value : (object)"") : value;
                    }
                }
                ret.Rows.Add(row);
            }
            return ret;
        }

    }
}