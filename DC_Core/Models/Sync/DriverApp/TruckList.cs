﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AlonsIT;
using DispatchCrude.Core;

namespace DispatchCrude.Models.Sync.DriverApp
{
    public class TruckList : List<TruckReadOnly>
    {
        public TruckList() { }
        public TruckList(int driverID, DateTime? syncDateUTC)
        {
            using (SSDB db = new SSDB())
            {
                SyncHelper.PopulateListFromDataTable(this
                    , db.GetPopulatedDataTable(
                        "SELECT T.*, cast(CASE WHEN T.DeleteDateUTC IS NULL THEN 0 ELSE 1 END as bit) AS Deleted " +
                        "FROM viewTruck T " +
                        "JOIN tblDriver D ON D.CarrierID = T.CarrierID " +
						"CROSS JOIN fnSyncLCDOffset('{0}') LCD " +
                        "WHERE D.ID = {1}" +
                        "AND (D.TerminalID IS NULL OR T.TerminalID IS NULL OR T.TerminalID = D.TerminalID) " +
                        ((syncDateUTC == null || Settings.SettingsID.RequireTruckMileage.AsBool()) ? "" : " AND (t.LastChangeDateUTC >= LCD.LCD OR t.CreateDateUTC >= LCD.LCD OR t.DeleteDateUTC >= LCD.LCD OR LastOdometerDateUTC >= LCD.LCD)")
                        , syncDateUTC
                        , driverID));
            }
        }
    }
}