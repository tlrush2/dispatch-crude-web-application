﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlonsIT;
using System.Data.Entity;

namespace DispatchCrude.Models.Sync.DriverApp
{
    public class DriverData
    {
        public DriverData() { }
        public DriverData(int DriverID)
        {
            using (DispatchCrudeDB db = new DispatchCrudeDB())
            {
                Driver driver = db.Drivers.Where(d => d.ID == DriverID).Include(d => d.ViewInfo).First();
                ID = driver.ID;
                IDNumber = driver.IDNumber;
                FirstName = driver.FirstName;
                LastName = driver.LastName;
                TruckID = driver.TruckID;
                TrailerID = driver.TrailerID;
                Trailer2ID = driver.Trailer2ID;
                DLNumber = driver.ViewInfo.DLNumber;
                DLState = driver.ViewInfo.DLState;
            }
        }


        public int ID { get; set; }
        public string IDNumber { get; set; }

        public int TruckID { get; set; }
        public int TrailerID { get; set; }
        public int? Trailer2ID { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        //public string FullName
        //{
        //    get { return FirstName + " " + LastName; }
        //}
/*
        public DateTime? HireDate { get; set; }
        public DateTime? DOB { get; set; }

        public int CarrierID { get; set; }
        public int? RegionID { get; set; }
        public int? OperatingStateID { get; set; }
*/
        public string DLNumber { get; set; }
        public string DLState { get; set; }
        //public string Endorsements { get; set; }
        //public string Restrictions { get; set; }

/*
        public string Address { get; set; }
        public string City { get; set; }
        public int? StateID { get; set; }
        public string Zip { get; set; }

        public string MobilePhone { get; set; }
        public string MobileProvider { get; set; }
        public string Email { get; set; }

*/
    }
}
