﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DispatchCrude.Models.Sync.DriverApp
{
    public class ExceptionLog: IExceptionLog
    {
        public int DriverID { get; set; }
        public bool Deleted { get; set; }
    }
}