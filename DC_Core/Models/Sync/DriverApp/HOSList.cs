﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AlonsIT;
using System.Data;

namespace DispatchCrude.Models.Sync.DriverApp
{
    public class HosList : List<HosDA>
    {
        public HosList() { }
        public HosList(int DriverID, DateTime? syncDateUTC)
        {
            using (SSDB db = new SSDB())
            {
                SyncHelper.PopulateListFromDataTable(this
                    , db.GetPopulatedDataTable(
                        "EXEC spDriverHOSLog {0}, {1}", DriverID, syncDateUTC.HasValue ? DBHelper.QuoteStr(syncDateUTC.Value.ToString("M/d/yyyy HH:mm:ss")) : "NULL"
                        ));
            }
        }
    }
}