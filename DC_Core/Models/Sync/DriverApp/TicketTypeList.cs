﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AlonsIT;

namespace DispatchCrude.Models.Sync.DriverApp
{
    public class TicketTypeList : List<TicketTypeReadOnly>
    {
        public TicketTypeList() { }
        public TicketTypeList(DateTime? syncDateUTC)
        {
            using (SSDB db = new SSDB())
            {
                SyncHelper.PopulateListFromDataTable(this
                    , db.GetPopulatedDataTable("SELECT *, CAST(0 as bit) AS Deleted FROM tblTicketType WHERE {0}"
                        , SyncHelper.SyncDateWhereClause(syncDateUTC)));
            }
        }
    }
}