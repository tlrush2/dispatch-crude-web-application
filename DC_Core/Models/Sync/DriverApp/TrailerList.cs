﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AlonsIT;

namespace DispatchCrude.Models.Sync.DriverApp
{
    public class TrailerList : List<TrailerReadOnly>
    {
        public TrailerList() { }
        public TrailerList(int driverID, DateTime? syncDateUTC)
        {
            using (SSDB db = new SSDB())
            {
                SyncHelper.PopulateListFromDataTable(this
                    , db.GetPopulatedDataTable(
                        "SELECT T.*, cast(CASE WHEN T.DeleteDateUTC IS NULL THEN 0 ELSE 1 END as bit) AS Deleted " +
                        "FROM tblTrailer T " +
                        "JOIN tblDriver D ON D.CarrierID = T.CarrierID " +
                        "WHERE {0} AND D.ID = {1}" +
                        "AND (D.TerminalID IS NULL OR T.TerminalID IS NULL OR T.TerminalID = D.TerminalID)"
                        , SyncHelper.SyncDateWhereClause(syncDateUTC, "T", true)
                        , driverID));
            }
        }
    }
}