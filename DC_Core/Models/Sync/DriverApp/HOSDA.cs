﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DispatchCrude.Models.Sync.DriverApp
{
    public class HosDA : AuditModelLastChangeUTCBase
    {
        public int ID { get; set; }
        public Guid UID { get; set; }
        public int? DriverID { get; set; }
        public int HosDriverStatusID { get; set; }
        public int? HosEventTypeID { get; set; }
        public decimal Lat { get; set; }
        public decimal Lon { get; set; }
        public string LocationDescription { get; set; }
        public bool PersonalUse { get; set; }
        public bool YardMove { get; set; }

        public string ELDUserName { get; set; }
        public int? DriverTimeZoneOffset { get; set; }
        public int? CoDriverID { get; set; }
        public string HosEventCode { get; set; }
        public byte? DistanceSinceLastValidCoordinates { get; set; }
        public string EventDataCheckValue { get; set; }
        public int EventRecordOriginID { get; set; }
        public int EventRecordStatusID { get; set; }
        public int? EventSequenceNumber { get; set; }
        public string Geolocation { get; set; }
        public string VIN { get; set; }
        public decimal? EngineHours { get; set; }
        public int? VehicleMiles { get; set; }
        public bool DataDiagnosticEventIndicator { get; set; }
        public bool MalfunctionIndicator { get; set; }
        public int? DiagnosticCodeID { get; set; }
        public string ELDIdentifier { get; set; }
        public string ELDAuthenticationValue { get; set; }
        public Guid? HosParentUID { get; set; }
        public Guid? HosSignatureUID { get; set; }

        public DateTime LogDateUTC { get; set; }
        public int TimeInState { get; set; }
        public string Notes { get; set; }
    }
}