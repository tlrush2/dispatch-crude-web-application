﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DispatchCrude.Models.Sync.DriverApp
{
    public class DriverLocationDA: AuditModelCreateUTCBase
    {
        public string ID { get; set; }
        public string UID { get; set; }
        public int DriverID { get; set; }
        public int? OrderID { get; set; }
        public int? OriginID { get; set; }
        public int? DestinationID { get; set; }
        public int LocationTypeID { get; set; }
        public decimal Lat { get; set; }
        public decimal Lon { get; set; }
        public int? DistanceToPoint { get; set; }
        public int? SourceAccuracyMeters { get; set; }
        public DateTime? SourceDateUTC { get; set; }
    }
}