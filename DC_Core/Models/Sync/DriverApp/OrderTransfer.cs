﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DispatchCrude.Models.Sync.DriverApp
{
    public class OrderTransfer: AuditModelLastChangeUTCBase
    {
        public int OrderID { get; set; }
        public int? DestTruckStartMileage { get; set; }
        public bool TransferComplete { get; set; }
        public string Notes { get; set; }
    }
}