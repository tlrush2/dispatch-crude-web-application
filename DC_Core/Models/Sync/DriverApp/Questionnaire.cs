﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DispatchCrude.Models.Sync.DriverApp
{
    public class QuestionnaireQuestionType
    {
        public int ID { get; set; }
        public string QuestionType { get; set; }
    }

    public class QuestionnaireTemplate : AuditModelDeleteUTCBase
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int QuestionnaireTemplateTypeID { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? CarrierID { get; set; }
        public bool LoadDefaults { get; set; }
    }
        
    public class QuestionnaireQuestion : AuditModelDeleteUTCBase
    {
        public int ID { get; set; }
        public int QuestionnaireTemplateID { get; set; }
        public int QuestionnaireQuestionTypeID { get; set; }
        public string QuestionText { get; set; }
        public string AnswersFormat { get; set; }
        public int? SortNum { get; set; }
        public bool IsRequired { get; set; }
        public string DefaultAnswer { get; set; }
        public bool IncludeNotes { get; set; }
    }

    public class QuestionnaireSubmissionDA : AuditModelCreateUTCBase
    {
        public int ID { get; set; }
        public int QuestionnaireTemplateID { get; set; }
        public int? DriverID { get; set; }
        public int? TruckID { get; set; }
        public int? TrailerID { get; set; }
        public int? Trailer2ID { get; set; }
        public DateTime SubmissionDateUTC { get; set; }
    }

    public class QuestionnaireResponseDA : AuditModelCreateUTCBase
    {
        public int ID { get; set; }
        public int QuestionnaireSubmissionID { get; set; }
        public int QuestionnaireQuestionID { get; set; }
        public string Answer { get; set; }
        public string Notes { get; set; }
    }

    public class QuestionnaireResponseBlobDA : AuditModelCreateUTCBase
    {
        public int ID { get; set; }
        public int QuestionnaireResponseID { get; set; }
        public byte[] AnswerBlob { get; set; }
    }
}