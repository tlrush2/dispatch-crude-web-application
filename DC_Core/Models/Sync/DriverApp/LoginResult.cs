﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DispatchCrude.Models.Sync.DriverApp
{
    public class LoginResult
    {
        public LoginResult(Outcome outcome, MasterData masterData = null)
        {
            LoginOutcome = outcome;
            Master = masterData ?? new MasterData();
        }

        public Outcome LoginOutcome { get; set; }
        public MasterData Master { get; set; }
    }
}