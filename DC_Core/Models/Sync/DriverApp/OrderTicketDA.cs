﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DispatchCrude.Models.Sync.DriverApp
{
    public class OrderTicketDA: AuditModelDeleteUTCBase
    {
        #region Fields
        public int? ID { get; set; }
        public Guid UID { get; set; }
        public int OrderID { get; set; }
        public string CarrierTicketNum { get; set; }
        public int? OriginTankID { get; set; }
        public string TankNum { get; set; }
        public int TicketTypeID { get; set; }
        public byte? BottomFeet { get; set; }
        public byte? BottomInches { get; set; }
        public byte? BottomQ { get; set; }
        public decimal? ProductObsGravity { get; set; }
        public decimal? ProductHighTemp { get; set; }
        public decimal? ProductLowTemp { get; set; }
        public decimal? ProductObsTemp { get; set; }
        public decimal? ProductBSW { get; set; }
        public byte? OpeningGaugeFeet { get; set; }
        public byte? OpeningGaugeInch { get; set; }
        public byte? OpeningGaugeQ { get; set; }
        public byte? ClosingGaugeFeet { get; set; }
        public byte? ClosingGaugeInch { get; set; }
        public byte? ClosingGaugeQ { get; set; }
        public decimal? GrossUnits { get; set; }
        public decimal? GrossStdUnits { get; set; }
        public decimal? NetUnits { get; set; }
        public bool Rejected { get; set; }
        public int? RejectReasonID { get; set; }
        public string RejectNotes { get; set; }
        public string SealOff { get; set; }
        public string SealOn { get; set; }
        public string BOLNum { get; set; }
        public string MeterFactor { get; set; }
        public decimal? OpenMeterUnits { get; set; }
        public decimal? CloseMeterUnits { get; set; }
        public string DispatchConfirmNum { get; set; }
        public decimal? WeightGrossUnits { get; set; }
        public decimal? WeightNetUnits { get; set; }
        public decimal? WeightTareUnits { get; set; }
        public string ScaleTicketNum { get; set; }
        public decimal? ProductObsSpecificWeight { get; set; }
        #endregion

        public bool UpdateValues(OrderTicket current)
        {
            bool ret = SyncHelper.UpdateValues(this, current, new string[] { "ID" } );
            current.FromMobileApp = true;
            return ret;
        }
    }
}