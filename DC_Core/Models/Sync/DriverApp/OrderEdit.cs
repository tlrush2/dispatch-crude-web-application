﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AlonsIT;
using DispatchCrude.Extensions;

namespace DispatchCrude.Models.Sync.DriverApp
{

    public class OrderEdit
    {
        #region Fields

        public int ID { get; set; }
        public int StatusID { get; set; }
        public int TruckID { get; set; }
        public int TrailerID { get; set; }
        public int? Trailer2ID { get; set; }
        public int? TicketTypeID { get; set; }
        public DateTime? AcceptLastChangeDateUTC { get; set; }
        public DateTime? OriginArriveTimeUTC { get; set; }
        public DateTime? OriginDepartTimeUTC { get; set; }
        public int? OriginMinutes { get; set; }
        public int? OriginWaitReasonID { get; set; }
        public string OriginWaitNotes { get; set; }
        public string CarrierTicketNum { get; set; }
        public string OriginBOLNum { get; set; }
        public decimal? OriginGrossUnits { get; set; }
        public decimal? OriginGrossStdUnits { get; set; }
        public decimal? OriginNetUnits { get; set; }
        public bool Rejected { get; set; }
        public int? RejectReasonID { get; set; }
        public string RejectNotes { get; set; }
        public bool ChainUp { get; set; }
        public int OriginTruckMileage { get; set; }
        public DateTime? PickupLastChangeDateUTC { get; set; }
        public DateTime? DestArriveTimeUTC { get; set; }
        public DateTime? DestDepartTimeUTC { get; set; }
        public int? DestMinutes { get; set; }
        public int? DestWaitReasonID { get; set; }
        public string DestWaitNotes { get; set; }
        public string DestBOLNum { get; set; }
        public string DestRailCarNum { get; set; }
        public int? DestTrailerWaterCapacity { get; set; }
        public decimal? DestGrossUnits { get; set; }
        public decimal? DestNetUnits { get; set; }
        public decimal? DestProductBSW { get; set; }
        public decimal? DestProductGravity { get; set; }
        public decimal? DestProductTemp { get; set; }
        public decimal? DestOpenMeterUnits { get; set; }
        public decimal? DestCloseMeterUnits { get; set; }
        public bool DestChainUp { get; set; }
        public int DestTruckMileage { get; set; }
        public DateTime? DeliverLastChangeDateUTC { get; set; }
        public int PickupPrintStatusID { get; set; }
        public int DeliverPrintStatusID { get; set; }
        public DateTime? PickupPrintDateUTC { get; set; }
        public DateTime? DeliverPrintDateUTC { get; set; }
        public string PickupDriverNotes { get; set; }
        public string DeliverDriverNotes { get; set; }
        public string DestRackBay { get; set; }
        public decimal? OriginWeightNetUnits { get; set; }
        public decimal? DestWeightGrossUnits { get; set; }
        public decimal? DestWeightTareUnits { get; set; }
        public decimal? DestWeightNetUnits { get; set; }
        public DateTime? OELC { get; set; } // "OELC" = "OrderEditLastChangeDateUTC"

        #endregion

        private bool UpdateStatus(Order updated)
        {
            bool ret = false;

            // validate the incoming status's to those that can be assigned by a driver
            if (
                // never allow an AUDITed or DECLINED records from being updated
                !updated.StatusID.IsIn((int)OrderStatus.STATUS.Audited, (int)OrderStatus.STATUS.Declined)
                // only accept incoming orders with a valid status assigned
                && this.StatusID.IsIn(
                    (int)OrderStatus.STATUS.Dispatched
                    , (int)OrderStatus.STATUS.Accepted
                    , (int)OrderStatus.STATUS.PickedUp
                    , (int)OrderStatus.STATUS.Delivered)
                // only update if the status is being changed
                && this.StatusID != updated.StatusID)
            {
                updated.StatusID = this.StatusID;
                ret = true;
            }
            return ret;
        }

        public bool UpdateValues(Order updated)
        {
            bool ret = false;
            if (updated.StatusID == (int)OrderStatus.STATUS.Audited)
            {
                //TODO: persist the driver proposed changes to a table for analysis
            }
            else
            {
                ret = UpdateStatus(updated);
                if ((!updated.AcceptLastChangeDateUTC.HasValue && this.AcceptLastChangeDateUTC.HasValue) || this.AcceptLastChangeDateUTC != updated.AcceptLastChangeDateUTC)
                {
                    if (SyncHelper.UpdateValues(this, updated, includePropertyNames: new string[] { "AcceptLastChangeDateUTC", "TruckID", "TrailerID", "Trailer2ID" }))
                        ret = true;
                }
                if ((!updated.PickupLastChangeDateUTC.HasValue && this.PickupLastChangeDateUTC.HasValue) 
                    || this.PickupLastChangeDateUTC != updated.PickupLastChangeDateUTC
                    || this.PickupPrintStatusID != updated.PickupPrintStatusID
                    // make totally sure these changes are always synced to the server
                    || this.OriginArriveTimeUTC.HasValue && !updated.OriginArriveTimeUTC.HasValue
                    || this.OriginDepartTimeUTC.HasValue && !updated.OriginDepartTimeUTC.HasValue)
                {
                    // ensure we never replace non-null Arrive/Depart times with a null one from the driver app
                    if (!this.OriginArriveTimeUTC.HasValue && updated.OriginArriveTimeUTC.HasValue) this.OriginArriveTimeUTC = updated.OriginArriveTimeUTC;
                    if (!this.OriginDepartTimeUTC.HasValue && updated.OriginDepartTimeUTC.HasValue) this.OriginDepartTimeUTC = updated.OriginDepartTimeUTC;
                    // ensure we don't try to assign an invalid ticket type to the server order record
                    if (!this.TicketTypeID.HasValue) this.TicketTypeID = updated.TicketTypeID;

                    if (SyncHelper.UpdateValues(this, updated, includePropertyNames: new string[] {
                        "PickupLastChangeDateUTC", "OriginArriveTimeUTC", "OriginDepartTimeUTC", "OriginMinutes", "OriginWaitReasonID", "OriginWaitNotes"
                        , "CarrierTicketNum", "OriginBOLNum", "Rejected", "RejectReasonID", "RejectNotes", "Chainup", "OriginTruckMileage"
                        , "PickupPrintStatusID", "PickupPrintDateUTC", "OriginGrossUnits", "OriginGrossStdUnits", "OriginNetUnits", "OriginWeightNetUnits"
                        , "PickupDriverNotes"
                        , "TicketTypeID" }))
                    {
                        ret = true;
                    }
                }
                if ((!updated.DeliverLastChangeDateUTC.HasValue && this.DeliverLastChangeDateUTC.HasValue) 
                    || this.DeliverLastChangeDateUTC != updated.DeliverLastChangeDateUTC
                    || this.DeliverPrintStatusID != updated.DeliverPrintStatusID
                    // make totally sure these changes are always synced to the server
                    || this.DestArriveTimeUTC.HasValue && !updated.DestArriveTimeUTC.HasValue
                    || this.DestDepartTimeUTC.HasValue && !updated.DestDepartTimeUTC.HasValue)
                {
                    // ensure we never replace non-null Arrive/Depart times with a null one from the driver app
                    if (!this.DestArriveTimeUTC.HasValue && updated.DestArriveTimeUTC.HasValue) this.DestArriveTimeUTC = updated.DestArriveTimeUTC;
                    if (!this.DestDepartTimeUTC.HasValue && updated.DestDepartTimeUTC.HasValue) this.DestDepartTimeUTC = updated.DestDepartTimeUTC;

                    if (SyncHelper.UpdateValues(this, updated, includePropertyNames: new string[] {
                       "DeliverLastChangeDateUTC", "DestArriveTimeUTC", "DestDepartTimeUTC", "DestMinutes", "DestWaitReasonID", "DestWaitNotes"
                        , "DestNetUnits", "DestGrossUnits", "DestChainup"
                        , "DestBOLNum", "DestRailcarNum", "DestTrailerWaterCapacity", "DestOpenMeterUnits", "DestCloseMeterUnits", "DestProductBSW"
                        , "DestProductGravity", "DestProductTemp", "DestTruckMileage" 
                        , "DeliverPrintStatusID", "DeliverPrintDateUTC", "DeliverDriverNotes", "DestRackBay"
                        , "DestWeightGrossUnits", "DestWeightTareUnits", "DestWeightNetUnits" }))
                    {
                        ret = true;
                    }
                }
                if (updated.RejectReasonID == 0)
                    updated.RejectReasonID = new int?();
                if (updated.OriginWaitReasonID == 0)
                    updated.OriginWaitReasonID = new int?();
                if (updated.DestWaitReasonID == 0)
                    updated.DestWaitReasonID = new int?();

                if (ret)
                {
                    // ensure that any changes will also update the LastChangeDateUTC to NOW
                    updated.LastChangeDateUTC = DateTime.UtcNow;

                    // if any changes from the Driver App are applied to the server record, then ensure the server record is not "DELETED"
                    if (updated.DeleteDateUTC.HasValue
                    && ((this.AcceptLastChangeDateUTC.HasValue && this.AcceptLastChangeDateUTC > updated.DeleteDateUTC)
                        || (this.PickupLastChangeDateUTC.HasValue && this.PickupLastChangeDateUTC > updated.DeleteDateUTC)
                        || (this.DeliverLastChangeDateUTC.HasValue && this.DeliverLastChangeDateUTC > updated.DeleteDateUTC)))
                    {
                        updated.DeleteDateUTC = new DateTime?();
                        updated.DeletedByUser = null;
                    }
                }
            }
            return ret;
        }
    }
}