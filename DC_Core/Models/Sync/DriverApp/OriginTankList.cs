﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AlonsIT;

namespace DispatchCrude.Models.Sync.DriverApp
{
    public class OriginTankList : List<OriginTankReadOnly>
    {
        public OriginTankList() { }
        public OriginTankList(DateTime? syncDateUTC, int driverID)
        {
            using (SSDB db = new SSDB())
            {
                SyncHelper.PopulateListFromDataTable(this
                    , db.GetPopulatedDataTable(
                        "SELECT * FROM fnOriginTank_DriverApp({0}, {1})"
                            , driverID
                            , syncDateUTC.HasValue ? DBHelper.QuoteStr(syncDateUTC.Value.ToString("M/d/yyyy HH:mm:ss")) : "NULL"));
            }
        }
    }
}