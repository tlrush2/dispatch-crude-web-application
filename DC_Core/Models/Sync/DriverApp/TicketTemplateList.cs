﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AlonsIT;
using System.Data;

namespace DispatchCrude.Models.Sync.DriverApp
{
    public class TicketTemplateList : List<TicketTemplate>
    {
        public TicketTemplateList() { }
        public TicketTemplateList(DateTime? syncDateUTC)
        {
            using (SSDB db = new SSDB())
            {
                System.Data.DataTable dt = db.GetPopulatedDataTable("SELECT * FROM tblDriverAppPrintTicketTemplate WHERE {0}", SyncHelper.SyncDateWhereClause(syncDateUTC));
                foreach (DataRow row in dt.Rows)
                {
                    row.BeginEdit();
                    // remove any present ^XA / ^XZ codes (these are added by the app)
                    row["TemplateText"] = row["TemplateText"].ToString().Replace("^XA", "").Replace("^XZ", "");
                    row.EndEdit();
                    row.AcceptChanges();
                }
                SyncHelper.PopulateListFromDataTable(this, dt);
            }
        }
    }
}