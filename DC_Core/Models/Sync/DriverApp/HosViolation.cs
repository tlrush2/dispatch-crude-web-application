﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AlonsIT;
using System.Data;
using DispatchCrude.Core;

namespace DispatchCrude.Models.Sync.DriverApp
{
    public class HOSViolation
    {
        public int DriverID { get; set; }
	    public DateTime LogDateUTC { get; set; } 
	    public DateTime EndDateUTC { get; set; } 
	    public double TotalHours { get; set; }  
	    public double? TotalHoursSleeping { get; set; } 
	    public double? TotalHoursDriving { get; set; } 
	    public int SleeperStatusID { get; set; } 
	    public bool WeeklyReset { get; set; } 
	    public bool DailyReset { get; set; } 
	    public bool SleepBreak { get; set; }  
	    public bool DriverBreak { get; set; } 
	    public string Status { get; set; } 
	    public bool SleeperReset { get; set; } 
	    public DateTime? LastWeeklyReset { get; set; } 
	    public DateTime? LastDailyReset { get; set; } 
	    public DateTime? LastBreak { get; set; } 
	    public DateTime? LastSleeperBerthReset { get; set; } 
	    public double HoursSinceWeeklyReset { get; set; } 
        public double OnDutyHoursSinceWeeklyReset { get; set; }
	    public double DrivingHoursSinceWeeklyReset { get; set; } 
	    public double OnDutyHoursSinceDailyReset { get; set; }    
	    public double DrivingHoursSinceDailyReset { get; set; } 
	    public double OnDutyDailyLimit { get; set; } 
	    public double HoursSinceBreak { get; set; } 
	    public double OnDutyHoursLeft { get; set; } 
	    public bool OnDutyViolation { get; set; } 
	    public double DrivingDailyLimit { get; set; } 
	    public double DrivingHoursLeft { get; set; } 
	    public bool DrivingViolation { get; set; }  
        public double BreakLimit { get; set; }
	    public double HoursTilBreak { get; set; } 
	    public bool BreakViolation { get; set; } 
	    public double WeeklyOnDutyLimit { get; set; } 
	    public double WeeklyOnDutyHoursLeft { get; set; } 
	    public bool WeeklyOnDutyViolation { get; set; } 
	    public double WeeklyDrivingLimit { get; set; } 
	    public double WeeklyDrivingHoursLeft { get; set; } 
	    public bool WeeklyDrivingViolation { get; set; } 
        public DateTime? OnDutyViolationDateUTC { get; set; }
	    public DateTime? DrivingViolationDateUTC { get; set; }  
	    public DateTime? BreakViolationDateUTC { get; set; } 
	    public DateTime? WeeklyDrivingViolationDateUTC { get; set; } 
	    public DateTime? WeeklyOnDutyViolationDateUTC { get; set; } 
    }

    public class HosViolationList : List<HOSViolation>
    {
        public HosViolationList() { }
        public HosViolationList(int DriverID)
        {
            using (SSDB db = new SSDB())
            { 
                SyncHelper.PopulateListFromDataTable(this
                    , db.GetPopulatedDataTable(
                        @"
                        SELECT * FROM fnHosViolationDetail({0}, DATEADD(DAY, -CAST(ISNULL(
                                (
	                                SELECT TOP 1 DriverAppLogRetentionDays FROM viewHOSPolicy WHERE Name IN (
                                            SELECT r.Value FROM tblDriver d
	                                        OUTER APPLY fnCarrierRules(GETUTCDATE(), GETUTCDATE(), {1}, d.ID, d.CarrierID, d.TerminalID, d.OperatingStateID, d.RegionID, 1) r
	                                        WHERE d.ID = {0}
                                    )
                                ), 0) AS INT), GETUTCDATE()), GETUTCDATE())"

                        , DriverID,
                        (int) CarrierRuleHelper.Type.HOS_POLICY));
            }
        }
    }

}