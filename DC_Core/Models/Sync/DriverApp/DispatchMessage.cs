﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlonsIT;
using DispatchCrude.Core;

namespace DispatchCrude.Models.Sync.DriverApp
{
    public class DispatchMessage
    {
        public int ID { get; set; }
        public Guid UID { get; set; }
        public int? ThreadID { get; set; }
        public string ToUser { get; set; }
        public string ToUserName { get; set; }
        public string FromUser { get; set; }
        public string FromUserName { get; set; }
        public string Message { get; set; }
        public bool Seen { get; set; }
        public DateTime MessageDateUTC { get; set; }
    }

    public class DispatchMessageList : List<DispatchMessage>
    {
        public DispatchMessageList() { }
        public DispatchMessageList(int DriverID)
        {
            if (Settings.SettingsID.DispatchMessaging.AsBool())
            {
                using (SSDB db = new SSDB())
                {
                    SyncHelper.PopulateListFromDataTable(this
                        , db.GetPopulatedDataTable(
                            "SELECT * FROM fnDriverMessages({0})", DriverID));
                }
            }
        }
    }
}
