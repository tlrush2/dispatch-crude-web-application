﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AlonsIT;

namespace DispatchCrude.Models.Sync.DriverApp
{
    public class QuestionnaireQuestionTypeList : List<QuestionnaireQuestionType>
    {
        public QuestionnaireQuestionTypeList() { }
        public QuestionnaireQuestionTypeList(DateTime? syncDateUTC)
        {
            // Send all Question Types login only (no need to continually sync)
            if (!syncDateUTC.HasValue)
            {
                using (SSDB db = new SSDB())
                {
                    SyncHelper.PopulateListFromDataTable(this
                        , db.GetPopulatedDataTable("SELECT * FROM tblQuestionnaireQuestionType"));
                }
            }
        }
    }

    
    public class QuestionnaireTemplateList : List<QuestionnaireTemplate>
    {
        public QuestionnaireTemplateList() { }
        public QuestionnaireTemplateList(DateTime? syncDateUTC, int driverID)
        {
            // Send all relevant Templates based off last sync and current driver's carrier
            using (SSDB db = new SSDB())
            {
                string query = "SELECT X.*, CAST(CASE WHEN X.DeleteDateUTC IS NULL THEN 0 ELSE 1 END as bit) AS Deleted " +
                                 "FROM tblQuestionnaireTemplate X " +
                                "WHERE (CarrierID IS NULL OR CarrierID = (SELECT CarrierID FROM tblDriver WHERE ID = " + driverID + ")) " +
                                  "AND CAST(GETDATE() AS DATE) BETWEEN CAST(EffectiveDate AS DATE) AND CAST(EndDate AS DATE)";
                if (syncDateUTC.HasValue)
                    query += " AND ISNULL(LastChangeDateUTC, CreateDateUTC) > " + DBHelper.QuoteStr(syncDateUTC.Value.ToString("M/dd/yyyy HH:mm:ss"));

                SyncHelper.PopulateListFromDataTable(this, db.GetPopulatedDataTable(query));
            }
        }
    }


    public class QuestionnaireQuestionList : List<QuestionnaireQuestion>
    {
        public QuestionnaireQuestionList() { }
        public QuestionnaireQuestionList(DateTime? syncDateUTC, int driverID)
        {
            // Send all relevant Questions based off last sync and current driver's carrier
            // trigger updates the template when a question is updated, so only need to check against template last change date
            using (SSDB db = new SSDB())
            {
                string query = "SELECT * " +
                                 "FROM tblQuestionnaireQuestion qq " +
                                "WHERE EXISTS (SELECT 1 " +
                                                "FROM tblQuestionnaireTemplate qt " +
                                               "WHERE (CarrierID IS NULL OR CarrierID = (SELECT CarrierID FROM tblDriver WHERE ID = " + driverID + ")) " +
                                                 "AND GETUTCDATE() BETWEEN EffectiveDate AND EndDate ";
                if (syncDateUTC.HasValue)
                    query +=                     "AND ISNULL(LastChangeDateUTC, CreateDateUTC) > " + DBHelper.QuoteStr(syncDateUTC.Value.ToString("M/dd/yyyy HH:mm:ss"));
                query +=                         "AND qq.QuestionnaireTemplateID = qt.ID)"+
                                  "AND qq.DeleteDateUTC IS NULL";

                SyncHelper.PopulateListFromDataTable(this, db.GetPopulatedDataTable(query));
            }
        }
    }


    public class QuestionnaireSubmissionList : List<QuestionnaireSubmissionDA>
    {
        public QuestionnaireSubmissionList() { }
    }
 

    public class QuestionnaireResponseList : List<QuestionnaireResponseDA>
    {
        public QuestionnaireResponseList() { }
    }

    
    public class QuestionnaireResponseBlobList : List<QuestionnaireResponseBlobDA>
    {
        public QuestionnaireResponseBlobList() { }
    }
}