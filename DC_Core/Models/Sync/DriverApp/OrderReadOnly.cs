﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DispatchCrude.Models.Sync.DriverApp
{
    public class OrderReadOnly: AuditModelDeleteUTCBase
    {
        /*****************************************************************************************************/
        /***  IF YOU ADD A FIELD HERE PLEASE ADD TO trigOrder_AppChanges_U TO PUSH UPDATES TO THE MOBILE   ***/
        /*****************************************************************************************************/

        public int? ID { get; set; }
        public int? OrderNum { get; set; }
        public int? TicketTypeID { get; set; } // DCDRV-221 (App can change Ticket Type): READ-ONLY version for backward compatability.
        public int? PriorityNum { get; set; }
        public string Product { get; set; }
        public DateTime? DueDate { get; set; }
        public int? OriginID { get; set; }
        public int? OriginTankID { get; set; }
        public string OriginTankNum { get; set; }
        public string Origin { get; set; }
        public string OriginType { get; set; }
        public string OriginFull { get; set; }
        public string OriginStation { get; set; }
        public string OriginCounty { get; set; }
        public string OriginState { get; set; }
        public string OriginAPI { get; set; }
        public string OriginLeaseNum { get; set; }
        public string OriginLegalDescription { get; set; }
        public int? OriginUomID { get; set; }
        public string OriginLat { get; set; }
        public string OriginLon { get; set; }
        public int? OriginGeoFenceRadiusMeters { get; set; }
        public int? DestinationID { get; set; }
        public string Destination { get; set; }
        public string DestType { get; set; }
        public string DestinationFull { get; set; }
        public string DestinationStation { get; set; }
        public int? DestUomID { get; set; }
        public string DestLat { get; set; }
        public string DestLon { get; set; }
        public int? DestGeoFenceRadiusMeters { get; set; }
        public int? ProducerID { get; set; }
        public string Producer { get; set; }
        public int? OperatorID { get; set; }
        public string Operator { get; set; }
        public int? CustomerID { get; set; }
        public string Customer { get; set; }
        public int? CarrierID { get; set; }
        public string Carrier { get; set; }
        public byte[] PrintHeaderBlob { get; set; }
		public int HeaderImageLeft { get; set; }
		public int HeaderImageTop { get; set; }
		public int HeaderImageWidth { get; set; }
		public int HeaderImageHeight { get; set; }
        public string PickupTemplateText { get; set; }
        public string TicketTemplateText { get; set; }
        public string DeliverTemplateText { get; set; }
        public string FooterTemplateText { get; set; }
        public int? PriorityID { get; set; }
        public int? ProductID { get; set; }
        public string EmergencyInfo { get; set; }
        public int? DestTicketTypeID { get; set; }
        public string DestTicketType { get; set; }
        public int? RouteActualMiles { get; set; }
        public string DispatchNotes { get; set; }
        public string DispatchConfirmNum { get; set; }
        public string CarrierAuthority { get; set; }
        public string OriginNDIC { get; set; }
        public string OriginNDM { get; set; }
        public string OriginCA { get; set; }
        public string OriginTimeZone { get; set; }
        public string DestTimeZone { get; set; }
        public int OriginThresholdMinutes { get; set; }
        public int DestThresholdMinutes { get; set; }
        public string ShipperHelpDeskPhone { get; set; }
        public string OriginDrivingDirections { get; set; }
        public string OriginDriver { get; set; }
        public string DestDrivingDirections { get; set; }
        public string DestDriver { get; set; }
        public int OriginTruckID { get; set; }
        public int? SequenceNum { get; set; }
        public string JobNumber { get; set; }
        public string ContractNumber { get; set; }
        public string ContractPrintDescription { get; set; }
        public int? ConsigneeID { get; set; }
        public string Consignee { get; set; }

        /*****************************************************************************************************/
        /***  IF YOU ADD A FIELD HERE PLEASE ADD TO trigOrder_AppChanges_U TO PUSH UPDATES TO THE MOBILE   ***/
        /*****************************************************************************************************/
    }
}