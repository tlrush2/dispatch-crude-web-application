﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AlonsIT;

namespace DispatchCrude.Models.Sync.DriverApp
{
    public class OrderStatusList : List<OrderStatusDA>
    {
        public OrderStatusList() { }
        public OrderStatusList(DateTime? syncDateUTC)
        {
            using (SSDB db = new SSDB())
            {
                SyncHelper.PopulateListFromDataTable(this
                    , db.GetPopulatedDataTable("SELECT *, OrderStatus AS Name, CAST(0 as bit) AS Deleted FROM tblOrderStatus WHERE {0}", SyncHelper.SyncDateWhereClause(syncDateUTC)));
            }
        }
    }
}