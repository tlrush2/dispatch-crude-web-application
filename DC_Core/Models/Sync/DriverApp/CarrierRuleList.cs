﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AlonsIT;

namespace DispatchCrude.Models.Sync.DriverApp
{
    public class CarrierRuleList : List<CarrierRule>
    {
        public CarrierRuleList() { }
        public CarrierRuleList(int driverID, DateTime? syncDateUTC)
        {

            using (SSDB db = new SSDB())
            {
                // Copy TypeID as ID for driver app to use as key (eliminate duplicate rules)
                SyncHelper.PopulateListFromDataTable(this
                    , db.GetPopulatedDataTable(
                        "SELECT ID=r.TypeID, r.TypeID, r.Value FROM tblDriver d " +
                        "CROSS APPLY dbo.fnCarrierRules(GETDATE(), null, null, d.ID, d.CarrierID, d.TerminalID ,d.OperatingStateID, d.RegionID, 1) r " +
                        "WHERE d.ID = {0}"
                        , driverID));
            }
        }
    }
}