﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AlonsIT;

namespace DispatchCrude.Models.Sync.DriverApp
{
    public class MasterData
    {
        // constant fieldnames used in multiple places
        private const string MOBILEAPPVERSION = "MobileAppVersion"
                            , TABLETID = "TabletID"
                            , PRINTERSERIAL = "PrinterSerial"
                            , ANDROID_API_LEVEL = "AndroidApiLevel"
                            , MODEL_NUM = "ModelNum"
                            , SERIAL_NUM = "SerialNum"
                            , MANUFACTURER = "Manufacturer";

        // generic parameterless constructor 
        public MasterData()
        {
            ID = 1;
            DriverID = 0;
            MobilePrint = false;
        }
        // constructor that takes a Master table DataRow
        public MasterData(System.Data.DataRow drMasterTable)
        {
            this.ID = 1;
            this.UserName = drMasterTable["UserName"].ToString();
            this.DriverName = drMasterTable["DriverName"].ToString();
            this.DriverID = DBHelper.ToInt32(drMasterTable["DriverID"]);
            this.MobilePrint = DBHelper.ToBoolean(drMasterTable["MobilePrint"]);
            if (!DBHelper.IsNull(drMasterTable["LastSyncUTC"]))
                this.LastSyncUTC = DBHelper.ToDateTime(drMasterTable["LastSyncUTC"]);
            this.SyncMinutes = DBHelper.ToInt32(drMasterTable["SyncMinutes"]);
            this.SchemaVersion = drMasterTable["SchemaVersion"].ToString();
            this.LatestAppVersion = drMasterTable["LatestAppVersion"].ToString();
            this.MobileAppVersion = drMasterTable[MOBILEAPPVERSION].ToString();
            this.PasswordHash = drMasterTable["PasswordHash"].ToString();
            this.TabletID = drMasterTable[TABLETID].ToString();
            this.PrinterSerial = drMasterTable[PRINTERSERIAL].ToString();
            this.AndroidApiLevel = drMasterTable[ANDROID_API_LEVEL].ToString();
            this.ModelNum = !DBHelper.IsNull(drMasterTable[MODEL_NUM])              //If null check is not done, value is sent to db as blank string ("")
                ? this.ModelNum = drMasterTable[MODEL_NUM].ToString()
                : this.ModelNum = null;       
            this.SerialNum = !DBHelper.IsNull(drMasterTable[SERIAL_NUM])            //If null check is not done, value is sent to db as blank string ("")
                ? this.SerialNum = drMasterTable[SERIAL_NUM].ToString()
                : this.SerialNum = null;
            this.Manufacturer = !DBHelper.IsNull(drMasterTable[MANUFACTURER])       //If null check is not done, value is sent to db as blank string ("")
                ? this.Manufacturer = drMasterTable[MANUFACTURER].ToString()
                : this.Manufacturer = null;
        }
        // static Init class that takes the incoming data, validates/records them with the database and returns a populated MasterData instance
        public static MasterData Init(string userName
                                    , int driverID
                                    , ref Outcome outcome
                                    , DateTime? syncDateUTC = null
                                    , string passwordHash = ""
                                    , string mobileAppVersion = null
                                    , string tabletID = null
                                    , string printerSerial = null
                                    , string androidApiLevel = null
                                    , string modelNum = null
                                    , string serialNum = null
                                    , string manufacturer = null)
        {
            MasterData ret = null;
            System.Data.DataTable dt = DriverSync(userName
                                                    , driverID
                                                    , syncDateUTC
                                                    , ref outcome
                                                    , passwordHash
                                                    , mobileAppVersion
                                                    , tabletID
                                                    , printerSerial
                                                    , androidApiLevel
                                                    , modelNum
                                                    , serialNum
                                                    , manufacturer);
            if (dt.Rows.Count > 0)
            {
                ret = new MasterData(dt.Rows[0]);
            }
            return ret;
        }
        public static MasterData ResetPasswordHash(string userName, int driverID, ref Outcome outcome)
        {
            MasterData ret = null;
            using (SSDB db = new SSDB())
            {
                using (System.Data.SqlClient.SqlCommand cmd = db.BuildCommand("spDriver_ResetPasswordHash"))
                {
                    cmd.Parameters["@UserName"].Value = userName;
                    cmd.Parameters["@DriverID"].Value = driverID;
                    System.Data.DataTable dt = SSDB.GetPopulatedDataTable(cmd);
                    outcome.Success = DBHelper.ToBoolean(cmd.Parameters["@Valid"].Value);
                    outcome.Message = (cmd.Parameters["@Message"].Value ?? "").ToString();
                    if (dt.Rows.Count > 0)
                    {
                        ret = new MasterData(dt.Rows[0]);
                    }
                }
            }
            return ret;
        }
        // generates a new lastSyncDateUTC then calls Init (record this update to the server and return the resultant populated MasterData instance)
        public static MasterData Sync(MasterData md, ref Outcome outcome)
        {
            // have the sql server generate the time stamp (other comparisons will be done on the server, this ensures compatible times if server clocks not synced)
            DateTime newSyncDateUTC = DBHelper.ToDateTime(SSDB.QuerySingleValueStatic("SELECT getutcdate()"));
            return Init(md.UserName
                        , md.DriverID
                        , ref outcome
                        , newSyncDateUTC
                        , md.PasswordHash
                        , md.MobileAppVersion
                        , md.TabletID
                        , md.PrinterSerial
                        , md.AndroidApiLevel
                        , md.ModelNum
                        , md.SerialNum
                        , md.Manufacturer);
        }

        public int? ID { get; set; }
        public string UserName { get; set; }
        public int DriverID { get; set; }
        public string DriverName { get; set; }
        public bool MobilePrint { get; set; }
        public DateTime? LastSyncUTC { get; set; }
        public string SchemaVersion { get; set; }
        public string PasswordHash { get; set; }
        public int SyncMinutes { get; set; }
        public string LatestAppVersion { get; set; }
        public string MobileAppVersion { get; set; }
        public string TabletID { get; set; }
        public string PrinterSerial { get; set; }
        public string AndroidApiLevel { get; set; }
        public string ModelNum { get; set; }
        public string SerialNum { get; set; }
        public string Manufacturer { get; set; }

        /* This function calls the spDriver_Sync stored procedure, which does the following logic:
         * 1) validates the incoming data
         * 2) records the incoming data in tblDriver_Sync table
         * 3) calls spDriverSync_UpdateDevices, which records the last used TabletID, PrinterSerial, and the Android version of the device
         */
        static protected System.Data.DataTable DriverSync(string userName
                                                        , int driverID
                                                        , DateTime? syncDateUTC
                                                        , ref Outcome outcome
                                                        , string passwordHash = ""
                                                        , string mobileAppVersion = null
                                                        , string tabletID = null
                                                        , string printerSerial = null
                                                        , string androidApiLevel = null
                                                        , string modelNum = null
                                                        , string serialNum = null
                                                        , string manufacturer = null)
        {
            using (SSDB db = new SSDB())
            {
                using (System.Data.SqlClient.SqlCommand cmd = db.BuildCommand("spDriver_Sync"))
                {
                    cmd.Parameters["@UserName"].Value = userName;
                    cmd.Parameters["@DriverID"].Value = driverID;
                    cmd.Parameters["@PasswordHash"].Value = passwordHash;
                    if (mobileAppVersion != null)
                        cmd.Parameters["@MobileAppVersion"].Value = mobileAppVersion;
                    if (syncDateUTC.HasValue)
                        cmd.Parameters["@SyncDateUTC"].Value = syncDateUTC.Value;
                    if (tabletID != null)
                        cmd.Parameters["@TabletID"].Value = tabletID;
                    if (printerSerial != null)
                        cmd.Parameters["@PrinterSerial"].Value = printerSerial;
                    if (androidApiLevel != null)
                        cmd.Parameters["@AndroidApiLevel"].Value = androidApiLevel;
                    if (modelNum != null)
                        cmd.Parameters["@ModelNum"].Value = modelNum;
                    if (serialNum != null)
                        cmd.Parameters["@SerialNum"].Value = serialNum;
                    if (manufacturer != null)
                        cmd.Parameters["@Manufacturer"].Value = manufacturer;
                    System.Data.DataTable ret = SSDB.GetPopulatedDataTable(cmd);
                    outcome.Success = DBHelper.ToBoolean(cmd.Parameters["@Valid"].Value);
                    outcome.Message = (cmd.Parameters["@Message"].Value ?? "").ToString();
                    return ret;
                }
            }
        }
    }
}