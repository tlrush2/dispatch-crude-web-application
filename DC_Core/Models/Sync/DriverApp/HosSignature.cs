﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AlonsIT;

namespace DispatchCrude.Models.Sync.DriverApp
{
    public class HosSignature : AuditModelCreateUTCBase
    {
        public int ID { get; set; }
        public Guid UID { get; set; }
        public int DriverID { get; set; }
        public DateTime HOSDate { get; set; }
        public byte[] SignatureBlob { get; set; }
    }

    public class HosSignatureList : List<HosSignature>
    {
        public HosSignatureList() { }
        public HosSignatureList(int driverID, DateTime? syncDateUTC)
        {
            using (SSDB db = new SSDB())
            {
                using (System.Data.SqlClient.SqlCommand cmd = db.BuildCommand("spHosSignatures_DriverApp"))//IS THIS NEEDED?
                {
                    cmd.Parameters["@DriverID"].Value = driverID;
                    if (syncDateUTC.HasValue)
                        cmd.Parameters["@LastChangeDateUTC"].Value = syncDateUTC.Value;
                    SyncHelper.PopulateListFromDataTable(this, SSDB.GetPopulatedDataTable(cmd));
                }
            }
        }
    }
}