﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DispatchCrude.Models.Sync
{
    public class SettingReadOnly: IDelete
    {
        public int? ID { get; set; }
        public string Value { get; set; }
    }
}