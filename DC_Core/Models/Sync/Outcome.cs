﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DispatchCrude.Models.Sync
{
    public class Outcome
    {
        public Outcome()
        {   // default values
            Success = false;
            Message = "";
        }
        public bool Success { get; set; }
        public string Message { get; set; }
    }
}