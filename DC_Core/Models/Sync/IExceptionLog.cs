﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DispatchCrude.Models.Sync
{
    public class IExceptionLog : AuditModelCreateUTCBase
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid UID { get; set; }
        public int? OrderID { get; set; }
        public DateTime? ExceptionDateUTC { get; set; }
        public string ExceptionLocation { get; set; }
        public string ExceptionName { get; set; }
        public string ExceptionDetails { get; set; }

        public IExceptionLog()
        {
            CreateDateUTC = DateTime.UtcNow;
        }
    }
}