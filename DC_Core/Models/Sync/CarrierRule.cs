﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DispatchCrude.Models.Sync
{
    public class CarrierRule
    {
        public int ID { get; set; }  // ID field is needed for mobile app to manage duplicates, 
                                         // set to TypeID instead of database ID since only one rule (best match) should be sent
        public int TypeID { get; set; }
        public string Value { get; set; }
    }
}