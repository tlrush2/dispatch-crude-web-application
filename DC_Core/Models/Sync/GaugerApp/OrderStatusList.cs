﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AlonsIT;

namespace DispatchCrude.Models.Sync.GaugerApp
{
    public class OrderStatusList : List<OrderStatusGA>
    {
        public OrderStatusList() { }
        public OrderStatusList(DateTime? syncDateUTC)
        {
            if (!syncDateUTC.HasValue)
            {
                using (SSDB db = new SSDB())
                {
                    SyncHelper.PopulateListFromDataTable(this
                        , db.GetPopulatedDataTable("SELECT *, Name, CAST(0 as bit) AS Deleted FROM tblGaugerOrderStatus"));
                }
            }
        }
    }
}