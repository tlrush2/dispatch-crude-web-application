﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AlonsIT;
using System.Data;

namespace DispatchCrude.Models.Sync.GaugerApp
{
    public class OrderReadOnlyList: List<OrderReadOnly>
    {
        public OrderReadOnlyList(DateTime? syncDateUTC, int gaugerID)
        {
            using (SSDB db = new SSDB())
            {
                using (System.Data.SqlClient.SqlCommand cmd = db.BuildCommand("spOrderReadOnly_GaugerApp"))
                {
                    cmd.Parameters["@GaugerID"].Value = gaugerID;
                    if (syncDateUTC.HasValue)
                        cmd.Parameters["@LastChangeDateUTC"].Value = syncDateUTC.Value;
                    DataTable data = SSDB.GetPopulatedDataTable(cmd);
                    SyncHelper.PopulateListFromDataTable(this, data);
                }
            }
        }
    }
}