﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AlonsIT;

namespace DispatchCrude.Models.Sync.GaugerApp
{
    public class MasterData
    {
        public MasterData()
        {
            ID = 1;
            GaugerID = 0;
            MobilePrint = false;
        }
        public MasterData(System.Data.DataRow masterTableDataRow)
        {
            this.ID = 1;
            this.UserName = masterTableDataRow["UserName"].ToString();
            this.GaugerName = masterTableDataRow["GaugerName"].ToString();
            this.GaugerID = DBHelper.ToInt32(masterTableDataRow["GaugerID"]);
            this.MobilePrint = DBHelper.ToBoolean(masterTableDataRow["MobilePrint"]);
            if (!DBHelper.IsNull(masterTableDataRow["LastSyncUTC"]))
                this.LastSyncUTC = DBHelper.ToDateTime(masterTableDataRow["LastSyncUTC"]);
            this.SyncMinutes = DBHelper.ToInt32(masterTableDataRow["SyncMinutes"]);
            this.SchemaVersion = masterTableDataRow["SchemaVersion"].ToString();
            this.LatestAppVersion = masterTableDataRow["LatestAppVersion"].ToString();
            this.AppVersion = masterTableDataRow["AppVersion"].ToString();
            this.PasswordHash = masterTableDataRow["PasswordHash"].ToString();
        }
        public static MasterData Init(string userName, int gaugerID, ref Outcome outcome, DateTime? syncDateUTC = null, string passwordHash = "", string appVersion = null)
        {
            MasterData ret = null;
            System.Data.DataTable dt = GaugerSync(userName, gaugerID, syncDateUTC, ref outcome, passwordHash, appVersion);
            if (dt.Rows.Count > 0)
            {
                ret = new MasterData(dt.Rows[0]);
            }
            return ret;
        }
        public static MasterData ResetPasswordHash(string userName, int gaugerID, ref Outcome outcome)
        {
            MasterData ret = null;
            using (SSDB db = new SSDB())
            {
                using (System.Data.SqlClient.SqlCommand cmd = db.BuildCommand("spGauger_ResetPasswordHash"))
                {
                    cmd.Parameters["@UserName"].Value = userName;
                    cmd.Parameters["@GaugerID"].Value = gaugerID;
                    System.Data.DataTable dt = SSDB.GetPopulatedDataTable(cmd);
                    outcome.Success = DBHelper.ToBoolean(cmd.Parameters["@Valid"].Value);
                    outcome.Message = (cmd.Parameters["@Message"].Value ?? "").ToString();
                    if (dt.Rows.Count > 0)
                    {
                        ret = new MasterData(dt.Rows[0]);
                    }
                }
            }
            return ret;
        }

        public static MasterData Sync(MasterData md, ref Outcome outcome)
        {
            return Init(md.UserName, md.GaugerID, ref outcome, DateTime.UtcNow, md.PasswordHash, md.AppVersion);
        }

        public int? ID { get; set; }
        public string UserName { get; set; }
        public int GaugerID { get; set; }
        public string GaugerName { get; set; }
        public bool MobilePrint { get; set; }
        public DateTime? LastSyncUTC { get; set; }
        public string SchemaVersion { get; set; }
        public string PasswordHash { get; set; }
        public int SyncMinutes { get; set; }
        public string LatestAppVersion { get; set; }
        public string AppVersion { get; set; }

        static protected System.Data.DataTable GaugerSync(string userName, int gaugerID, DateTime? syncDateUTC, ref Outcome outcome, string passwordHash = "", string appVersion = null)
        {
            using (SSDB db = new SSDB())
            {
                using (System.Data.SqlClient.SqlCommand cmd = db.BuildCommand("spGaugerSync"))
                {
                    cmd.Parameters["@UserName"].Value = userName;
                    cmd.Parameters["@GaugerID"].Value = gaugerID;
                    cmd.Parameters["@PasswordHash"].Value = passwordHash;
                    if (appVersion != null)
                        cmd.Parameters["@AppVersion"].Value = appVersion;
                    if (syncDateUTC.HasValue)
                        cmd.Parameters["@SyncDateUTC"].Value = syncDateUTC.Value;
                    System.Data.DataTable ret = SSDB.GetPopulatedDataTable(cmd);
                    outcome.Success = DBHelper.ToBoolean(cmd.Parameters["@Valid"].Value);
                    outcome.Message = (cmd.Parameters["@Message"].Value ?? "").ToString();
                    return ret;
                }
            }
        }
    }
}