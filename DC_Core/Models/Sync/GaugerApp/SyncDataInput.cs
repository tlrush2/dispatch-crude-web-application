﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using DispatchCrude.Core;

namespace DispatchCrude.Models.Sync.GaugerApp
{
    public class SyncDataInput
    {
        public SyncDataInput() { }
        static public SyncDataInput GetFromJSON(string json)
        {
            return JsonConvert.DeserializeObject<SyncDataInput>(json, JSonSettings());
        }
        public MasterData Master { get; set; }
        public GaugerOrderList OrderEdits { get; set; }
        public GaugerTicketList OrderTickets { get; set; }
        public OrderSignatureList OrderSignatures { get; set; }
        public ExceptionLogList Exceptions { get; set; }

        static public JsonSerializerSettings JSonSettings(bool forLogging = false)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings();
            if (forLogging)
                settings.ContractResolver = new JsonPropListExcludeContractResolver(new string[] { "MobileAppVersion", "TabletID", "PrinterSerial", "PrintHeaderBlob", "TemplateText", "SignatureBlob", "PhotoBlob", "PickupTemplateText", "TicketTemplateText", "DeliverTemplateText", "FooterTemplateText" });
            else
                settings.ContractResolver = new JsonPropListExcludeContractResolver(new string[] { "MobileAppVersion", "TabletID", "PrinterSerial" });
            settings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            settings.Formatting = Formatting.Indented;
            settings.DateFormatHandling = DateFormatHandling.IsoDateFormat;
            return settings;
        }

    }

    public class SyncData : SyncDataInput
    {
        public SyncData() : base() { }
        public SyncData(Outcome syncOutcome) { SyncOutcome = syncOutcome; }
        public SyncData(DateTime? lastSyncDateUTC, Outcome syncOutcome, MasterData masterData)
            : base()
        {
            if (syncOutcome.Success)
            {
                Master = MasterData.Sync(masterData, ref syncOutcome);
                Orders = new OrderReadOnlyList(lastSyncDateUTC, masterData.GaugerID);
                OrderEdits = new GaugerOrderList(lastSyncDateUTC, masterData.GaugerID);
                OrderTickets = new GaugerTicketList(lastSyncDateUTC, masterData.GaugerID);
                OrderSignatures = new OrderSignatureList(lastSyncDateUTC, masterData.GaugerID);
                OrderStatuses = new OrderStatusList(lastSyncDateUTC);
                PrintTemplates = new PrintTemplateList(lastSyncDateUTC);
                Settings = new SettingList(lastSyncDateUTC);
                OriginTanks = new OriginTankList(lastSyncDateUTC, masterData.GaugerID);
                OriginTankStrappings = new OriginTankStrappingList(lastSyncDateUTC, masterData.GaugerID);
                TicketTypes = new TicketTypeList(lastSyncDateUTC);
                Uoms = new UomList(lastSyncDateUTC);
                PrintStatuses = new PrintStatusList(lastSyncDateUTC);
                OrderRejectReasons = new OrderRejectReasonList(lastSyncDateUTC);
                OrderTicketRejectReasons = new OrderTicketRejectReasonList(lastSyncDateUTC);
                OrderRules = new OrderRuleList(masterData.GaugerID, lastSyncDateUTC);
            }
            SyncOutcome = syncOutcome;
        }

        public string SerializeAsJson(bool forLogging = false)
        {
            string ret = JsonConvert.SerializeObject(this, JSonSettings(forLogging));
            return ret;
        }
        public Outcome SyncOutcome { get; set; }
        public OrderReadOnlyList Orders { get; set; }
        public OrderStatusList OrderStatuses { get; set; }
        public PrintTemplateList PrintTemplates { get; set; }
        public SettingList Settings { get; set; }
        public OriginTankList OriginTanks { get; set; }
        public OriginTankStrappingList OriginTankStrappings { get; set; }
        public TicketTypeList TicketTypes { get; set; }
        public UomList Uoms { get; set; }
        public PrintStatusList PrintStatuses { get; set; }
        public OrderRejectReasonList OrderRejectReasons { get; set; }
        public OrderTicketRejectReasonList OrderTicketRejectReasons { get; set; }
        public OrderRuleList OrderRules { get; set; }

        static public int AppVersionAsInt(string appVersion)
        {
            int ret = 0;
            int multiplier = 10000000;
            if (appVersion != null)
            {
                foreach (String element in appVersion.Split(new char[] { '.' }))
                {
                    int iElement = 0;
                    if (int.TryParse(element, out iElement))
                    {
                        ret += iElement * multiplier;
                        multiplier /= 100;
                    }
                }
            }
            return ret;
        }
    }
}