﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AlonsIT;

namespace DispatchCrude.Models.Sync
{
    public class UomList : List<UomReadOnly>
    {
        public UomList() { }
        public UomList(DateTime? syncDateUTC)
        {
            using (SSDB db = new SSDB())
            {
                SyncHelper.PopulateListFromDataTable(this
                    , db.GetPopulatedDataTable(
                        "SELECT U.*, cast(CASE WHEN U.DeleteDateUTC IS NULL THEN 0 ELSE 1 END as bit) AS Deleted FROM tblUom U WHERE {0}"
                        , SyncHelper.SyncDateWhereClause(syncDateUTC, "U", true)));
            }
        }
    }
}