﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Data.Entity;
using System.Web.Mvc;
using AlonsIT;

namespace DispatchCrude.Models
{
    [Table("tblObjectFieldType")]
    public class ObjectFieldType : AuditModelCreateBase
    {
        public enum Types { String = 1, Boolean = 2, Integer = 3, Double = 4, DateTime = 5, Date = 6, Time = 7, GUID = 8, }

        public ObjectFieldType()
        {
        }

        [Key]
        public Int16 ID { get; set; }
        
        [Required]
        public string Name { get; set; }
    }
}