using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DispatchCrude.Models
{
    [Table("tblReportFilterGroupingFunction")]
    public class ReportGroupingFunction
    {
        public enum TYPE { None = 1, SummaryFunc = 3, Sum = 10, Avg = 11, Count = 12, Min = 13, Max = 14, DailyAvg = 15, Sum_LastDay = 16 }

        [Key]
        public int ID { get; set; }

        [Required, StringLength(25)]
        public string Name { get; set; }

    }
}
