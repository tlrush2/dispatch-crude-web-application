﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Data.Entity;
using System.Web.Mvc;
using AlonsIT;

namespace DispatchCrude.Models
{
    [Table("tblObjectFieldAllowNull")]
    public class ObjectFieldAllowNull
    {
        public enum AllowNullEnum { False = 0, True = 1, Dynamic = 2 }

        public ObjectFieldAllowNull()
        {
        }

        [Key]
        public int ID { get; private set; }
        
        [Required]
        public string Name { get; private set; }

        public bool? AllowNull { get; private set; }
    }
}