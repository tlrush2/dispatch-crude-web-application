﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.SqlClient;
using AlonsIT;

namespace DispatchCrude.Models
{
    [Table("tblDriverScheduleStatus")]
    public class DriverSchedule : AuditModelLastChangeBase
    {
        [Key]
        public int ID { get; set; }

        [DisplayName("Driver")]
        public int DriverID { get; set; }

        [DisplayName("Schedule Date")]
        public DateTime ScheduleDate { get; set; }

        public Int16 StatusID { get; set; }

        [DisplayName("Start Time")]
        public byte? StartHours { get; set; }

        public byte? DurationHours { get; set; }

        public bool ManualAssignment { get; set; }

        static public void UpdateDriverSchedule(string userName, int driverID, DateTime schedulDate, int statusID, byte? startHours, byte? durationHours, bool manualAssignment = true)
        {
            using (SSDB db = new SSDB())
            {
                using (SqlCommand cmd = db.BuildCommand("spUpdateDriverSchedule"))
                {
                    cmd.Parameters["@DriverID"].Value = driverID;
                    cmd.Parameters["@ScheduleDate"].Value = schedulDate;
                    cmd.Parameters["@StatusID"].Value = statusID;
                    cmd.Parameters["@StartHours"].Value = startHours;
                    cmd.Parameters["@DurationHours"].Value = durationHours;
                    cmd.Parameters["@ManualAssignment"].Value = manualAssignment;
                    cmd.Parameters["@UserName"].Value = userName;
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}
