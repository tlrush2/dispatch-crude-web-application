﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DispatchCrude.Models
{
    public class ELD
    {
        public static String ELD_PROVIDER = "DispatchCrude";
        public static String ELD_REGISTRATION_ID = "DC01";

        public static int EventDataCheckMask = 0xC3;
        public static int LineDataCheckMask = 0x96;
        public static int FileDataCheckMask = 0x969C;


        public static int getCheckSum(string str)
        {
            // From Table 3 in ELD mandate (4.4.5)
            int total = 0;

            for (int i = 0; i < str.Length; i++)
            {
                char c = str[i];
                int ascii = (int) c;
                if (ascii >= 49 && ascii <= 122) // 1-9 or A-Z or a-z
                    total = total + ascii - 48;
            }
            return total;
        }
    }



    [Table("tblELDEventRecordOrigin")]
    public class ELDEventRecordOrigin
    {
        public enum Type { ELD = 1, DRIVER = 2, SUPPORT_OTHER = 3, UNIDENTIFIED = 4 };

        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
    }


    [Table("tblELDEventRecordStatus")]
    public class ELDEventRecordStatus
    {
        public enum Type { ACTIVE = 1, INACTIVE_CHANGED = 2, INACTIVE_CHANGE_REQUESTED = 3, INACTIVE_CHANGE_REJECTED = 4 };

        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
    }


    [Table("tblELDDiagnosticCode")]
    public class ELDDiagnosticCode
    {
        public enum Type {
            POWER_DIAGNOSTIC = 1, ENGINE_SYNC_DIAGNOSTIC = 2, MISSING_DATA_DIAGNOSTIC = 3, DATA_TRANSFER_DIAGNOSTIC = 4, UNIDENTIFIED_DRIVER_DIAGNOSTIC = 5,
            OTHER_DIAGNOSTIC = 6,

            POWER_MALFUNCTION = 7, ENGINE_SYNC_MALFUNCTION = 8, TIMING_MALFUNCTION = 9, POSITIONING_MALFUNCTION = 10, DATA_RECORD_MALFUNCTION = 11,
            DATA_TRANSFER_MALFUNCTION = 12, OTHER_MALFUNCTION = 13
        };

        [Key]
        public int ID { get; set; }
        public string Code { get; set; }
        public string ShortName { get; set; }
        public bool IsMalfunction { get; set; }
    }


    [Table("tblELDDriverDrivingCategory")]
    public class ELDDrivingCategory
    {
        public enum Type { AUTHORIZED_PERSONAL_USE = 1, YARD_MOVE = 2 };

        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
        public string Abbreviation { get; set; }
    }

}
