using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using System.Linq;
using System;

namespace DispatchCrude.Models
{
    [Table("tblCarrierComplianceType")]
    public class CarrierComplianceType : AuditModelDeleteBase, IValidatableObject
    {
        [Key]
        public int ID { get; set; }

        [Required]
        public string Name { get; set; }

        [DisplayName("Carrier Type")]
        [UIHint("_ForeignKeyDDL")]        
        public int? CarrierTypeID { get; set; }

        [ForeignKey("CarrierTypeID")]
        public virtual CarrierType CarrierType { get; set; }
        
        [DisplayName("Is System?")]
        public bool IsSystem { get; set; }

        [DisplayName("Is Required?")]
        [UIHint("Switch")]
        public bool IsRequired { get; set; }

        [DisplayName("Requires Document?")]
        [UIHint("Switch")]
        public bool RequiresDocument { get; set; }

        [DisplayName("Expiration Length (Days)")]
        public int? ExpirationLength { get; set; }

        /// <summary>
        /// Returns boolean based upon the existence of an expiration date
        /// </summary>
        [NotMapped]
        public bool Expires
        {
            get { return ExpirationLength != null && ExpirationLength > 0; }
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!ValidateDuplicateName(ID, Name))
                yield return new ValidationResult("Type name is already in use");
            Validated = true;
        }

        private bool ValidateDuplicateName(int id, string name)
        {
            using (DispatchCrudeDB db = new DispatchCrudeDB())
            {
                return db.CarrierComplianceTypes.Where(m => m.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase) && (id == 0 || m.ID != id)).Count() == 0;
            }
        }

        public override bool allowRowDeactivate
        {
            get
            {
                //Override normal deactivate behavior: If the record is marked as a system record.  Do no allow it to be deactivated.
                return !IsSystem;
            }
        }
    }    
}
