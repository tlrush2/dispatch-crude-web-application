﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace DispatchCrude.Models
{
    [Table("tblLocationType")]
    public class LocationType : AuditModelCreateBase
    {
        public enum Types { AUTO = 1, DRIVER_ARRIVE = 2, DRIVER_DEPART = 3, GEOFENCE_IN = 4, GEOFENCE_OUT = 5,
                PICKUP_PRINT_DONE = 6, DELIVERY_PRINT_DONE = 7, PHOTO = 8, ACCEPT = 9, TRANSFER = 10,
                HOS_OFF_DUTY = 11, HOS_SLEEPER = 12, HOS_ON_DUTY_DRIVING = 13, HOS_ON_DUTY_NOT_DRIVING = 14 }

        public byte ID { get; set; }
        public string Name { get; set; }
    }
}