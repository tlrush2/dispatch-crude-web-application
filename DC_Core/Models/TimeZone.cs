using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DispatchCrude.Models
{

    [Table("tblTimeZone")]
    public class TimeZone
    {
        public enum TYPE { CentralStandardTime = 1, EasternStandardTime = 2, MountainStandardTime = 3, PacificStandardTime = 4 }
        public static string GetName(int id) { return ((TYPE)id).ToString(); }
        public enum TYPE_ABBREV { CST = 1, EST = 2, MST = 3, PST = 4 }
        public static string GetAbbrev(int id) { return ((TYPE_ABBREV)id).ToString(); }

        [Key]
        public byte ID { get; set; }

        [Required, DisplayName("TimeZone"), StringLength(100)]
        public string Name { get; set; }
        [Required, DisplayName("TimeZone Full"), StringLength(100)]
        public string SystemName { get; set; }

        [Required, DisplayName("Standard Abbreviation"), StringLength(3)]
        public string StandardAbbrev { get; set; }
        [Required, DisplayName("Daylight Abbreviation"), StringLength(3)]
        public string DaylightAbbrev { get; set; }

        [Required, DisplayName("Standard Offset [hours]")]
        public int StandardOffsetHours { get; set; }
        [Required, DisplayName("Daylight Offset [hours]")]
        public int DaylightOffsetHours { get; set; }
    }
}
