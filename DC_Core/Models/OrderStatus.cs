using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DispatchCrude.Models
{
    [Table("tblOrderStatus")]
    public class OrderStatus : AuditModelLastChangeBase
    {
        public enum STATUS { GaugerRejected = -11, Generated = -10, Gauger = -9, Assigned = 1, Dispatched = 2, Delivered = 3, Audited = 4, Accepted = 7, PickedUp = 8, Declined = 9 };

        public OrderStatus()
        {
        }

        [Key]
        public int ID { get; set; }

        [Required, Column("OrderStatus"), StringLength(25)]
        public string Name { get; set; }

        [Required, DisplayName("Status #")]
        public short StatusNum { get; set; }

        [Required, StringLength(25), DisplayName("Action")]
        public string ActionText { get; set; }

        public int? NextID { get; set; }

        public int? EntrySortNum { get; set; }
    }

}
