using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DispatchCrude.Models
{
    [Table("tblDriverComplianceCategory")]
    public class DriverComplianceCategory : IEDTO
    {
        public enum TYPES { Initial = 1, Licenses = 2, Medical = 3, Certificates = 4 }

        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
    }        
}
