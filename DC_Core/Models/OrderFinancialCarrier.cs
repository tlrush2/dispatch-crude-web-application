﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlonsIT;

namespace DispatchCrude.Models
{
    //[Table("viewOrder_Financial_Carrier_Financial")]
    public class CarrierOrderFinancials: OrderFinancialBase
    {
        static public DataTable GetDataTable(DateTime? start, DateTime? end, int shipperID, int carrierID, int productGroupID, int truckTypeID, int driverGroupID, int originStateID, int destStateID, int producerID
            , int? batchID, bool onlyShipperSettled, string jobNumber, string contractNumber, int rejected
            , ref string sessionID, bool startSession = false)
        {
            using (SSDB db = new SSDB())
            {
                using (SqlCommand cmd = db.BuildCommand("spRetrieveOrdersFinancialCarrier"))
                {
                    cmd.Parameters["@StartDate"].Value = start;
                    cmd.Parameters["@EndDate"].Value = end;
                    cmd.Parameters["@ShipperID"].Value = shipperID;
                    cmd.Parameters["@CarrierID"].Value = carrierID;
                    cmd.Parameters["@ProductGroupID"].Value = productGroupID;
                    cmd.Parameters["@TruckTypeID"].Value = truckTypeID;
                    cmd.Parameters["@DriverGroupID"].Value = driverGroupID;
                    cmd.Parameters["@OriginStateID"].Value = originStateID;
                    cmd.Parameters["@DestStateID"].Value = destStateID;
                    cmd.Parameters["@ProducerID"].Value = producerID;
                    cmd.Parameters["@BatchID"].Value = batchID;
                    cmd.Parameters["@OnlyShipperSettled"].Value = onlyShipperSettled;
                    cmd.Parameters["@JobNumber"].Value = jobNumber;
                    cmd.Parameters["@ContractNumber"].Value = contractNumber;
                    cmd.Parameters["@UserName"].Value = Core.UserSupport.UserName;
                    cmd.Parameters["@StartSession"].Value = startSession ? 1 : 0;
                    cmd.Parameters["@SessionID"].Value = sessionID;
                    cmd.Parameters["@Rejected"].Value = rejected;
                    DataTable ret = SSDB.GetPopulatedDataTable(cmd);
                    sessionID = !DBHelper.IsNull(cmd.Parameters["@SessionID"].Value) ? cmd.Parameters["@SessionID"].Value.ToString() : null;
                    Core.DateHelper.AddLocalRowStateDateFields(ret, null, "InvoiceRatesAppliedDateUTC");
                    //AddManualFieldCSV(ret);
                    return ret;
                }
            }
        }

        static public List<CarrierOrderFinancials> GetList(DateTime? start, DateTime? end, int shipperID, int carrierID, int productGroupID, int truckTypeID, int driverGroupID, int originStateID, int destStateID, int producerID, int rejected
            , int? batchID = null, bool onlyShipperSettled = false, string jobNumber = null, string contractNumber = null)
        {
            List<CarrierOrderFinancials> ret = new List<CarrierOrderFinancials>();
            string sessionID = null;
            DataTable data = GetDataTable(start, end, shipperID, carrierID, productGroupID, truckTypeID, driverGroupID, originStateID, destStateID, producerID, batchID, onlyShipperSettled, jobNumber, contractNumber, rejected, ref sessionID, false);
            Sync.SyncHelper.PopulateListFromDataTable(ret, data);
            return ret;
        }

        static public bool? UpdateSessionBatchSel(int sessionID, int? orderID, bool batchSel)
        {
            bool? ret = batchSel, rateApplySel = null;
            UpdateSessionSel(sessionID, orderID, ref ret, ref rateApplySel);
            return ret;
        }
        static public bool? UpdateSessionRateApplySel(int sessionID, int? orderID, bool rateApplySel)
        {
            bool? ret = rateApplySel, batchSel = null;
            UpdateSessionSel(sessionID, orderID, ref batchSel, ref ret);
            return ret;
        }
        static public void UpdateSessionSel(int sessionID, int? orderID, ref bool? batchSel, ref bool? rateApplySel)
        {
            using (SSDB db = new SSDB())
            {
                using (SqlCommand cmd = db.BuildCommand("spUpdateFinancialOrderSessionCarrier"))
                {
                    cmd.Parameters["@SessionID"].Value = sessionID;
                    cmd.Parameters["@OrderID"].Value = orderID;
                    cmd.Parameters["@BatchSel"].Value = batchSel;
                    cmd.Parameters["@RateApplySel"].Value = rateApplySel;
                    cmd.ExecuteNonQuery();
                    batchSel = Core.Converter.ToNullableBool(cmd.Parameters["@BatchSel"].Value);
                    rateApplySel = Core.Converter.ToNullableBool(cmd.Parameters["@RateApplySel"].Value);
                }
            }
        }
    }
}
