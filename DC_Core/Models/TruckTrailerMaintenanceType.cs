﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DispatchCrude.Models
{
    [Table("tblTruckTrailerMaintenanceType")]
    public class TruckTrailerMaintenanceType : AuditModelDeleteBase, IValidatableObject
    {
        [Key]
        public int ID { get; set; }

        [Required]
        [DisplayName("Name")]
        [StringLength(50)]
        public string Name { get; set; }

        [UIHint("Switch")]
        [DisplayName("For Trucks?")]
        public bool ForTruck { get; set; }

        [UIHint("Switch")]
        [DisplayName("For Trailers?")]
        public bool ForTrailer { get; set; }                

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!ValidateDuplicateName(ID, Name))
                yield return new ValidationResult("Name is already in use");
            if (!ValidateBooleans())
                yield return new ValidationResult("Choose at least one resource this type applies to");
            Validated = true;
        }

        private bool ValidateDuplicateName(int id, string name)
        {
            using (DispatchCrudeDB db = new DispatchCrudeDB())
            {
                return db.TruckTrailerMaintenanceTypes.Where(m => 
                                m.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase) 
                                && (id == 0 || m.ID != id)
                            ).Count() == 0;
            }
        }

        private bool ValidateBooleans()
        {
            return (this.ForTruck == false && this.ForTrailer == false) ? false : true;            
        }
    }
}
