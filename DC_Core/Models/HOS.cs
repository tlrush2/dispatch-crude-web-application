﻿using DispatchCrude.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace DispatchCrude.Models
{
    [Table("tblHos")]
    public class Hos : AuditModelLastChangeBase
    {
        public Hos()
        {
            this.UID = Guid.NewGuid();
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid UID { get; set; }

        [DisplayName("Driver")]
        public int? DriverID { get; set; } // can be nullable if unidentified driver
        [ForeignKey("DriverID")]
        public virtual Driver Driver { get; set; }

        [DisplayName("Driver Status")]
        public int HosDriverStatusID { get; set; }
        [ForeignKey("HosDriverStatusID")]
        public virtual HosDriverStatus HosDriverStatus { get; set; }

        [DisplayName("Event Type")]
        public int? HosEventTypeID { get; set; }
        [ForeignKey("HosEventTypeID")]
        public virtual HosEventType HosEventType { get; set; }

        [DisplayName("Lat")]
        public decimal Lat { get; set; }

        [DisplayName("Lon")]
        public decimal Lon { get; set; }

        [DisplayName("Location Description")]
        public string LocationDescription { get; set; }

        [DisplayName("Personal Use")]
        public bool PersonalUse { get; set; }

        [DisplayName("Yard Move")]
        public bool YardMove { get; set; }

        public DateTime LogDateUTC { get; set; }

        [NotMapped, DisplayName("Log Date")]
        public DateTime LogDate
        {
            get
            {
                return DateHelper.ToLocal(LogDateUTC, DispatchCrudeHelper.GetProfileTimeZone(HttpContext.Current));
            }
            set
            {
                LogDateUTC = DateHelper.ToUTC(value, DispatchCrudeHelper.GetProfileTimeZone(HttpContext.Current));
            }
        }

        [DisplayName("Log Time Zone")]
        public byte? LogTimeZoneID { get; set; }
        [ForeignKey("LogTimeZoneID")]
        public TimeZone LogTimeZone { get; set; }


        [Required]
        [MinLength(4), MaxLength(60)]  // Length Per ELD Mandate 4.3.2.8.1
        [DisplayName("Notes")]
        public string Notes { get; set; } // if editing on server notes are required

        [DisplayName("ELD Username")]
        public string ELDUserName { get; set; }

        [DisplayName("Driver Time Zone Offset")]
        public int? DriverTimeZoneOffset { get; set; }

        [DisplayName("Co-Driver")]
        public int? CoDriverID { get; set; }
        [ForeignKey("CoDriverID")]
        public virtual Driver CoDriver { get; set; }

        [DisplayName("HOS Event Code")]
        public string HosEventCode { get; set; }

        [DisplayName("Distance Since Last Valid Coordinates")]
        public byte? DistanceSinceLastValidCoordinates { get; set; }

        [UIHint("Hex")]
        [DisplayName("Event Data Check Value")]
        public byte[] EventDataCheckValue { get; set; }

        [DisplayName("Event Record Origin")]
        public int EventRecordOriginID { get; set; }
        [ForeignKey("EventRecordOriginID")]
        public virtual ELDEventRecordOrigin EventRecordOrigin { get; set; }

        [DisplayName("Event Record Status")]
        public int EventRecordStatusID { get; set; }
        [ForeignKey("EventRecordStatusID")]
        public virtual ELDEventRecordStatus EventRecordStatus { get; set; }

        [UIHint("Hex")]
        [DisplayName("Event Sequence Number")]
        public byte[] EventSequenceNumber { get; set; }

        [MaxLength(60)]
        [DisplayName("Geolocation")]
        public string Geolocation { get; set; }

        [MaxLength(17)]
        [DisplayName("VIN")]
        public string VIN { get; set; }

        [DisplayName("Engine Hours")]
        public decimal? EngineHours { get; set; }

        [DisplayName("Vehicle Miles")]
        public int? VehicleMiles { get; set; }

        [DisplayName("Data Diagnostic Event Indicator")]
        public bool DataDiagnosticEventIndicator {get; set;}

        [DisplayName("Malfunction Indicator")]
        public bool MalfunctionIndicator { get; set; }

        [DisplayName("Diagnostic Code")]
        public int? DiagnosticCodeID { get; set; }
        [ForeignKey("DiagnosticCodeID")]
        public virtual ELDDiagnosticCode DiagnosticCode { get; set; }

        [MaxLength(6)]
        [DisplayName("ELD Identifier")]
        public string ELDIdentifier { get; set; }

        [MaxLength(32)]
        [DisplayName("ELD Authentication Value")]
        public string ELDAuthenticationValue { get; set; }

        [DisplayName("HOS Parent UID")]
        public Guid? HosParentUID { get; set; }

        [DisplayName("HOS Signature UID")]
        public Guid? HosSignatureUID { get; set; }

        [NotMapped]
        public bool isEditable
        {
            get
            {
                // Per ELD Mandate, only active hos records (4.4.4.2.5) with a type not in the exluded list (4.3.2.8.2) can be edited
                return EventRecordStatusID == 0 || EventRecordStatusID == (int)ELDEventRecordStatus.Type.ACTIVE && HosEventType.isEditable;
            }
        }

        [NotMapped]
        public bool hasChanged
        {
            get
            {
                return HosParentUID != null;
            }
        }
        [NotMapped]
        public Hos OriginalRecord
        {
            get
            {
                using (DispatchCrudeDB db = new DispatchCrudeDB())
                {
                    var results = db.Hoss.Where(s => s.UID == this.HosParentUID).ToList();
                    return results.Any() ? results.First() : null;
                }
            }
        }

        [NotMapped]
        public bool IsCertified
        {
            get
            {
                return HosSignatureUID != null;
            }
        }
        [NotMapped]
        public HosSignature Signature
        {
            get
            {
                using (DispatchCrudeDB db = new DispatchCrudeDB())
                {
                    var results = db.HosSignatures.Where(s => s.UID == this.HosSignatureUID);
                    return (results.Any()) ? results.First() : null;
                }
            }
        }
        [NotMapped]
        public bool hasPendingChange
        {
            get
            {
                using (DispatchCrudeDB db = new DispatchCrudeDB())
                {
                    return db.Hoss.Where(s => s.HosParentUID == this.UID 
                                && s.EventRecordStatusID == (int)ELDEventRecordStatus.Type.INACTIVE_CHANGE_REQUESTED).Any(); // other HOS records with this as a parent
                }
            }
        }

        public byte[] getEventCode()
        {
            int binarySize = 1;
            string str = HosEventTypeID
                        + HosEventCode
                        + LogDate.ToString("MMddyy") + LogDate.ToString("HHmmss")
                        + VehicleMiles
                        + EngineHours
                        + Lat + Lon
                        + "0" //VIN
                        + ELDUserName;
            int i = ELD.getCheckSum(str) & ((int)Math.Pow(2, 8 * binarySize) - 1); // apply mask to get 8-bit lower byte of checksum (ie. 0xFF)
            i = (i << 3) & ((int)Math.Pow(2, 8 * binarySize) - 1) | (i >> (8*binarySize - 3)); // circular shift left 3 times
            i = i ^ ELD.EventDataCheckMask; // XOR C3

            return BitConverter.GetBytes(i);
        }

        [NotMapped]
        public bool isOriginal
        {
            get
            {
                // compare generated event code to original (db) value to see if the record has been altered
                return EventDataCheckValue != null && getEventCode()[0] == EventDataCheckValue[0];
            }
        }
    }


    [Table("tblELDEventType")]
    public class HosEventType
    {
        public enum Type { STATUS_CHANGE = 1, INTERMEDIATE = 2, DRIVER_PC_OR_YM = 3, CERTIFICATION = 4, LOGIN_LOGOUT = 5, CMV_POWER = 6, MALFUNCTION_OR_DIAGNOSTIC = 7 };

        [Key]
        public int ID { get; set; }
        public string Name { get; set; }

        [DisplayName("Short Name")]
        public string ShortName { get; set; }

        [NotMapped]
        public bool isEditable
        {
            get
            {
                return ID != (int)Type.INTERMEDIATE && ID != (int)Type.LOGIN_LOGOUT && ID != (int)Type.CMV_POWER && ID != (int)Type.MALFUNCTION_OR_DIAGNOSTIC;
            }
        }
    }


    [Table("tblHosDriverStatus")]
    public class HosDriverStatus
    {
        public enum Status { OFFDUTY = 1, SLEEPER = 2, DRIVING = 3, ONDUTY = 4 };

        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
        public bool Driving { get; set; }
        public string Abbreviation { get; set; }
    }

 }
