﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Data.Entity;
using System.Web.Mvc;
using AlonsIT;

namespace DispatchCrude.Models
{
    [Table("tblImportCenterFieldDefinition")]
    public class ImportCenterFieldDefinition : AuditModelLastChangeUTCBase
    {
        public ImportCenterFieldDefinition()
        {
            this.Fields = new List<ImportCenterFieldDefinitionField>();
        }

        [Key]
        public int ID { get; set; }

        [Required, DisplayName("Import Center Definition")]
        public int ImportCenterDefinitionID { get; set; }
        [ForeignKey("ImportCenterDefinitionID"), DisplayName("Import Center Definition")]
        public ImportCenterDefinition ImportCenterDefinition { get; set; }

        [DisplayName("Target Field")]
        public int ObjectFieldID { get; set; }
        [ForeignKey("ObjectFieldID")]
        public ObjectField ObjectField { get; set; }

        [NotMapped]
        public int? ParentObjectID 
        { 
            get
            {
                int? ret = null;
                using (SSDB db = new SSDB())
                {
                    object val = db.QuerySingleValue("SELECT ParentObjectID FROM tblObjectField WHERE ID = {0}", this.ObjectFieldID);
                    if (!DBHelper.IsNull(val)) 
                        ret = DBHelper.ToInt32(val);
                }
                return ret;
            }
        }

        [NotMapped]
        public int ObjectFieldTypeID
        {
            get
            {
                int ret = 0;
                using (SSDB db = new SSDB())
                {
                    object val = db.QuerySingleValue("SELECT ObjectFieldTypeID FROM tblObjectField WHERE ID = {0}", this.ObjectFieldID);
                    if (!DBHelper.IsNull(val))
                        ret = DBHelper.ToInt32(val);
                }
                return ret;
            }
        }

        [DisplayName("Parent Field")]
        public int? ParentFieldID { get; set; }
        [ForeignKey("ParentFieldID")]
        public ImportCenterFieldDefinition ParentField { get; set; }
 
        [DisplayName("C# Format")]
        public string CSharpExpression { get; set; }

        [DisplayName("Key Comparison Type")]
        public int? KeyComparisonTypeID { get; set; }
        [ForeignKey("KeyComparisonTypeID")]
        public ImportCenterComparisonType KeyComparisonType { get; set; }

        public virtual List<ImportCenterFieldDefinitionField> Fields { get; set; }
    }
}