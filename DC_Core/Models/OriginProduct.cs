﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DispatchCrude.Models
{
    [Table("tblOriginProducts")]
    public class OriginProduct: IEDTO
    {
        [Key]
        public int ID { get; set; }

        [Required, DisplayName("Origin")]
        public int OriginID { get; set; }
        [Required, DisplayName("Product")]
        public int ProductID { get; set; }

        [ForeignKey("ProductID")]
        public virtual Product Product { get; set; }
    }
}