using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace DispatchCrude.Models
{
    [Table("tblOrigin")]
    public class Origin : AuditModelDeleteBase
    {
        public Origin()
        {
            //this.Routes = new List<Route>();
            this.OriginProducts = new List<OriginProduct>();
            this.Shippers = new List<Customer>();
            //this.Products = new List<Product>();
        }

        public /*virtual*/ ICollection<Customer> Shippers { get; set; }

        public /*virtual*/ ICollection<Product> Products { get; set; }


        [Key]
        public int ID { get; set; }

        [Required, StringLength(50, MinimumLength = 4, ErrorMessage = "Origin name must be between 4 and 50 characters.")]
        public string Name { get; set; }

        [StringLength(50)]
        public string Address { get; set; }

        [StringLength(30)]
        public string City { get; set; }

        [StringLength(10)]
        public string Zip { get; set; }

        [StringLength(20)]
        public string LAT { get; set; }

        [StringLength(20)]
        public string LON { get; set; }

        [DisplayName("Geo Fence Radius (meter)")]
        int GeoFenceRadiusMeters { get; set; }

        [StringLength(25)]
        public string County { get; set; }

        int? TotalDepth { get; set; }
        DateTime? SpudDate { get; set; }

        [StringLength(25), DisplayName("Field Name")]
        public string FieldName { get; set; }

        [StringLength(25)]
        public string Station { get; set; }

        [Required]
        [DisplayName("Lease")]        
        [StringLength(50, MinimumLength = 4, ErrorMessage = "Origin lease name must be between 4 and 50 characters.")]
        public string LeaseName { get; set; }

        [StringLength(30), DisplayName("Lease #")]
        public string LeaseNum { get; set; }

        [StringLength(50), DisplayName("Legal Description")]
        public string LegalDescription { get; set; }

        [StringLength(10), DisplayName("NDIC File #")]
        public string NDICFileNum { get; set; }

        [StringLength(10), DisplayName("CTB #")]
        public string CTBNum { get; set; }

        [StringLength(50), DisplayName("BLM Info")]
        public string NDM { get; set; }

        [StringLength(25), DisplayName("CA")]
        public string CA { get; set; }

        [DisplayName("Private Road Miles")]
        public decimal? PrivateRoadMiles { get; set; }

        [DisplayName("Ticket Type")]
        public int TicketTypeID { get; set; }

        public bool H2S { get; set; }

        [Required, DisplayName("Time Zone"), DefaultValue(1)]
        public byte TimeZoneID { get; set; }

        [Required, DefaultValue(true), DisplayName("Uses DST")]
        public bool UseDST { get; set; }

        //[InverseProperty("Origin")]
        //public ICollection<Route> Routes { get; set; }
        public ICollection<OriginProduct> OriginProducts { get; set; }

        [DisplayName("Terminal")]
        public int? TerminalID { get; set; }
        [ForeignKey("TerminalID")]
        [DisplayName("Terminal")]
        public Terminal Terminal { get; set; }

        [NotMapped]
        public string[] ProductIDs
        {
            get
            {
                return OriginProducts.Select(op => op.ProductID.ToString()).ToArray<string>();
            }
        }

        [DisplayName("State")]
        public int? StateID { get; set; }
        public virtual State State { get; set; }

        [NotMapped]
        public int? CountryID
        {
            get
            {
                if (this.State == null)
                {
                    //This null check is required for adding tanks to work properly on the origin tanks mvc page
                    return null;
                }
                else
                {
                    return this.State.CountryID;
                }                
            }
        }

        [StringLength(20), DisplayName("API")]
        public string WellAPI { get; set; }

        [DisplayName("Tax Rate")]
        public decimal? TaxRate { get; set; }

        [DisplayName("Operator")]
        public int? OperatorID { get; set; }
        [ForeignKey("OperatorID")]
        public Operator Operator { get; set; }

        [DisplayName("Producer")]
        public int? ProducerID { get; set; }
        [ForeignKey("ProducerID")]
        public Producer Producer { get; set; }

        [DisplayName("Origin Type")]
        public int OriginTypeID { get; set; }

        [DisplayName("Origin Type")]
        public /*virtual*/ OriginType OriginType { get; set; }

        [DisplayName("Pumper")]
        public int? PumperID { get; set; }
        [ForeignKey("PumperID")]
        public Pumper Pumper { get; set; }

        [DisplayName("Region")]
        public int? RegionID { get; set; }

        [ForeignKey("RegionID")]
        public /*virtual*/ Region Region { get; set; }

        [DisplayName("Ticket Type")]
        public TicketType TicketType { get; set; }

        [ForeignKey("TimeZoneID"), DisplayName("Time Zone")]
        public virtual TimeZone TimeZone { get; set; }

        [Required, DisplayName("UOM"), DefaultValue(1)]
        public int UomID { get; set; }

        [ForeignKey("UomID"), DisplayName("UOM")]
        public Uom UOM { get; set; }

        [DisplayName("Driving Directions")]
        public string DrivingDirections { get; set; }

        //[NotMapped, DisplayName("Full Name")] // Maverick does not like the "full name" format anymore so we should not need this now - 9/23/16
        //public string FullName { get { return (this.H2S ? "H2S-" : "") +  this.OriginType.Name + ": " + this.Name; } }

        [DisplayName("Sulfur Content")]
        public decimal? SulfurContent { get; set; }

        [NotMapped]
        public string NameWithStatus
        {
            get
            {
                if (Active)
                    return Name;
                else
                    return "[DEACTIVATED] " + Name;
            }
        }
    }

}
