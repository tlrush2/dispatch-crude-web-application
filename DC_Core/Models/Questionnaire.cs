﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DispatchCrude.Core;

namespace DispatchCrude.Models
{
    // A table of the types of answers
    [Table("tblQuestionnaireQuestionType")]
    public class QuestionnaireQuestionType : IEDTO
    {
        public enum TYPE { Label = 0, Boolean = 1, Integer = 2, Decimal = 3, String = 4, DropDown = 5, Timestamp = 6, Blob = 7 }

        public int ID { get; set; }

        [Required]
        [DisplayName("Type")]
        public String QuestionType { get; set; }
    }


    // A table of the types of templates
    [Table("tblQuestionnaireTemplateType")]
    public class QuestionnaireTemplateType : AuditModelDeleteBase, IValidatableObject
    {
        public int ID { get; set; }

        [Required]
        [DisplayName("Type")]
        [StringLength(50)]
        public String Name { get; set; }

        [DisplayName("Is System?")]
        public bool IsSystem { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!ValidateDuplicateName(ID, Name))
                yield return new ValidationResult("Type name is already in use");
            Validated = true;
        }

        private bool ValidateDuplicateName(int id, string name)
        {
            using (DispatchCrudeDB db = new DispatchCrudeDB())
            {
                return db.QuestionnaireTemplateTypes.Where(m => m.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase) && (id == 0 || m.ID != id)).Count() == 0;
            }
        }
    
        public override bool allowRowEdit
        {
            get
            {
                //Override normal edit behavior: If the record is marked as a system record.  Do no allow it to be edited.
                return !IsSystem;
            }
        }

        public override bool allowRowDeactivate
        {
            get
            {
                //Override normal deactivate behavior: If the record is marked as a system record.  Do no allow it to be deactivated.
                return !IsSystem;
            }
        }
    }

    
    // A table of templates
    [Table("tblQuestionnaireTemplate")]
    public class QuestionnaireTemplate : AuditModelDeleteBase
    {
        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public String Name { get; set; }

        [DisplayName("Type")]
        public int QuestionnaireTemplateTypeID { get; set; }
        [ForeignKey("QuestionnaireTemplateTypeID")]
        public virtual QuestionnaireTemplateType QuestionnaireTemplateType { get; set; }

        [Required]
        [DisplayName("Effective Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}"), UIHint("Date")]
        public DateTime? EffectiveDate { get; set; }
        [Required]
        [DisplayName("End Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}"), UIHint("Date")]
        public DateTime? EndDate { get; set; }

        [DisplayName("Carrier")]
        public int? CarrierID { get; set; }
        [ForeignKey("CarrierID")]
        public virtual Carrier Carrier { get; set; }

        static public string EXCEPTIONNHELP = "When a questionnaire using this template is submitted but does not pass validation, this comma-separated list of recipients will be sent an email notifying them of a \"failed\" submission.  If blank, exception alerts will not be sent.";
        public string ExceptionEmail { get; set; }
        public string ExceptionFormat { get; set; }

        [DisplayName("Load Defaults")]
        public bool LoadDefaults { get; set; }

        [InverseProperty("QuestionnaireTemplate")]
        public virtual List<QuestionnaireQuestion> QuestionnaireQuestions { get; set; }

        [InverseProperty("QuestionnaireTemplate")]
        public virtual List<QuestionnaireSubmission> QuestionnaireSubmissions { get; set; }

        [NotMapped]
        public bool Locked
        {
            get { return QuestionnaireSubmissions != null && QuestionnaireSubmissions.Any(); } // check if form submitted 
        }
    }


    // A table of questions for each template
    [Table("tblQuestionnaireQuestion")]
    public class QuestionnaireQuestion : AuditModelDeleteBase
    {
        static public char DROPDOWNDELIMITER = '|';

        static public string DROPDOWNHELP = "Enter selections for the dropdown list.  Hit ENTER after each entry to add to the choices";
        static public string RANGEHELP = "Enter maximum and minimum ranges a user can enter.  One or both can be blank which means no limit.  For integers or decimals use a number.  For dates, you can use a date or an offset (-20 would be 20 minutes ago, 0 would be NOW)";

        static public string PASSINGDROPDOWNHELP = "Enter selections that make this question valid.  Hit ENTER after each entry to add to the choices.  Answers not in this list will cause an exception.  If blank, no validation is checked and any answer is passing";
        static public string PASSINGRANGEHELP = "Enter maximum and minimum passing ranges.  One or both can be blank which means no limit.  For integers or decimals use a number.  For dates, you can use a date or an offset (-20 would be 20 minutes ago, 0 would be NOW).  Answers not in this range will cause an exception.  If both are blank, no validation is checked and any answer is passing";
        static public string PASSINGBOOLHELP = "Enter the passing choice.  Answers not set to this value will cause an exception.  Selecting NONE means no validation will occur and any answer is passing";

        static public string DEFAULTBOOLHELP = "Enter a default choice.  Selecting NONE means no value will be selected initially";


        public int ID { get; set; }

        public int QuestionnaireTemplateID { get; set; }
        [ForeignKey("QuestionnaireTemplateID")]
        public virtual QuestionnaireTemplate QuestionnaireTemplate { get; set; }

        [DisplayName("Type")]
        public int QuestionnaireQuestionTypeID { get; set; }
        [ForeignKey("QuestionnaireQuestionTypeID")]
        public virtual QuestionnaireQuestionType QuestionnaireQuestionType { get; set; }

        [Required]
        [DisplayName("Question")]
        [StringLength(255)]
        public String QuestionText { get; set; }

        [Column("AnswersFormat")]
        public String _AnswersFormat { get; set; }

        [NotMapped]
        [DisplayName("Format")]
        public String AnswersFormat 
        {
            get { return String.IsNullOrEmpty(_AnswersFormat) ? _AnswersFormat : _AnswersFormat.Replace(DROPDOWNDELIMITER, ','); } 
            set { _AnswersFormat = String.IsNullOrEmpty(value) ? value : value.Replace(',', DROPDOWNDELIMITER); } 
        }

        [Range(1, Int32.MaxValue)]
        [DisplayName("Sort Order")]
        public int? SortNum { get; set; }

        [DisplayName("Is Required")]
        public bool IsRequired { get; set; }
        [DisplayName("Default Answer")]
        public string DefaultAnswer { get; set; }
        [DisplayName("Include Notes")]
        public bool IncludeNotes { get; set; }

        public SelectList ToSelectList(string selected = null)
        {
            if (this.QuestionnaireQuestionTypeID != (int)QuestionnaireQuestionType.TYPE.DropDown)
                return null;

            return new SelectList(this._AnswersFormat.Split(DROPDOWNDELIMITER)
                .Select(s => s).ToList(), selected);
        }

        //public string DefaultValue { get; set; }
        [Column("PassingCondition")]
        public string _PassingCondition { get; set; }

        [NotMapped]
        [DisplayName("Passing Condition")]
        public string PassingCondition 
        {
            get { return String.IsNullOrEmpty(_PassingCondition) ? _PassingCondition : _PassingCondition.Replace(DROPDOWNDELIMITER, ','); } 
            set { _PassingCondition = String.IsNullOrEmpty(value) ? value : value.Replace(',', DROPDOWNDELIMITER); } 
        }

        [NotMapped]
        public DateTime? DefaultDate
        {
            get
            {
                if (this.QuestionnaireQuestionTypeID != (int)QuestionnaireQuestionType.TYPE.Timestamp)
                    return null;
                DateTime d;
                if (DateTime.TryParse(DefaultAnswer, out d))
                    return d;
                int i;
                if (int.TryParse(DefaultAnswer, out i))
                    return DateTime.Now.AddMinutes(i);
                return null;
            }
        }

        /// --------------------------------------------------------------------------------------------
        /// <summary>
        ///     Checks the value to see if it is valid based on the question type.  For integers or 
        ///         decimals, must be a number.  For dates, can be a date or a number (offset in minutes)
        /// </summary>
        /// <param name="val">Value to test</param>
        /// <returns>True if valid</returns>
        /// --------------------------------------------------------------------------------------------
        private bool IsValidValue(string val)
        {
            if (QuestionnaireQuestionTypeID == (int)QuestionnaireQuestionType.TYPE.Integer)
            {
                // If set, make sure value is a valid integer
                int parsed;
                return (String.IsNullOrEmpty(val) || Int32.TryParse(val, out parsed));
            }
            else if (QuestionnaireQuestionTypeID == (int)QuestionnaireQuestionType.TYPE.Decimal)
            {
                // If set, make sure value is a valid decimal
                decimal parsed;
                return (String.IsNullOrEmpty(val) || Decimal.TryParse(val, out parsed));
            }
            else if (QuestionnaireQuestionTypeID == (int)QuestionnaireQuestionType.TYPE.Timestamp)
            {
                // If set, make sure value is a valid date or offset (minutes as integer) 
                int parsedInt;
                DateTime parsedDate;
                return (String.IsNullOrEmpty(val) || DateTime.TryParse(val, out parsedDate) || Int32.TryParse(val, out parsedInt));
            }

            return true;
        }


        /// --------------------------------------------------------------------------------------------
        /// <summary>
        ///     Checks the default to see if it is valid based on the question type.  For integers or 
        ///         decimals, must be a number.  For dates, can be a date or a number (offset in minutes)
        /// </summary>
        /// <returns>True if valid</returns>
        /// --------------------------------------------------------------------------------------------
        public bool IsValidDefault(string val)
        {
            return IsValidValue(val);
        }


        /// <summary>
        ///     Checks the min and max values to see if they are valid based on the question type. 
        ///         One or both fields can be null which means no limit.  For integers or decimals, 
        ///         must be numbers.  For dates, can be a date or a number (offset in minutes)
        /// </summary>
        /// <param name="min">The minimum value in the range</param>
        /// <param name="max">The maximum value in the range</param>
        /// <returns>True if valid</returns>
        /// --------------------------------------------------------------------------------------------
        public bool IsValidRange(string min, string max)
        {
            // TODO: check max > min?
            if (QuestionnaireQuestionTypeID == (int)QuestionnaireQuestionType.TYPE.Integer)
            {
                // If set, make sure max and min are valid integers
                int parsed;
                return (   (String.IsNullOrEmpty(min) || Int32.TryParse(min, out parsed))
                        && (String.IsNullOrEmpty(max) || Int32.TryParse(max, out parsed)));
            }
            else if (QuestionnaireQuestionTypeID == (int)QuestionnaireQuestionType.TYPE.Decimal)
            {
                // If set, make sure max and min are valid decimals 
                decimal parsed;
                return (   (String.IsNullOrEmpty(min) || Decimal.TryParse(min, out parsed))
                        && (String.IsNullOrEmpty(max) || Decimal.TryParse(max, out parsed)));
            }
            else if (QuestionnaireQuestionTypeID == (int)QuestionnaireQuestionType.TYPE.Timestamp)
            {
                // If set, make sure max and min are valid dates or offsets (minutes as integer) 
                int parsedInt;
                DateTime parsedDate;
                return (   (String.IsNullOrEmpty(min) || DateTime.TryParse(min, out parsedDate) || Int32.TryParse(min, out parsedInt))
                        && (String.IsNullOrEmpty(max) || DateTime.TryParse(max, out parsedDate) || Int32.TryParse(max, out parsedInt)));
            }

            return true;
        }
    }


    // A table of completed questionnaires
    [Table("tblQuestionnaireSubmission")]
    public class QuestionnaireSubmission : AuditModelCreateBase
    {
        public int ID { get; set; }

        public int QuestionnaireTemplateID { get; set; }
        [ForeignKey("QuestionnaireTemplateID")]
        public virtual QuestionnaireTemplate QuestionnaireTemplate { get; set; }

        public int? DriverID { get; set; }
        [ForeignKey("DriverID")]
        public virtual Driver Driver { get; set; }

        public int? TruckID { get; set; }
        [ForeignKey("TruckID")]
        public virtual Truck Truck { get; set; }

        public int? TrailerID { get; set; }
        [ForeignKey("TrailerID")]
        public virtual Trailer Trailer { get; set; }

        public int? Trailer2ID { get; set; }
        [ForeignKey("Trailer2ID")]
        public virtual Trailer Trailer2 { get; set; }

        [InverseProperty("QuestionnaireSubmission")]
        public virtual List<QuestionnaireResponse> QuestionnaireResponses { get; set; }

        [Required, DisplayName("Submission Date")]
        public DateTime SubmissionDateUTC { get; set; }

        public byte? SubmissionTimeZoneID { get; set; }
        [ForeignKey("SubmissionTimeZoneID")]
        public virtual TimeZone TimeZone { get; set; }

        public string getAnswer(string field)
        {
            QuestionnaireResponse qr = QuestionnaireResponses.Where(r => r.QuestionnaireQuestion.QuestionText == field).FirstOrDefault();
            return (qr == null) ? null : qr.Answer;
        }
        public string getAnswer(int id)
        {
            QuestionnaireResponse qr = QuestionnaireResponses.Where(r => r.QuestionnaireQuestionID == id).FirstOrDefault();
            return (qr == null) ? null : qr.Answer;
        }

        [NotMapped]
        public bool Passing
        {
            get
            {
                bool pass = true;
                if (QuestionnaireResponses != null)
                {
                    foreach (QuestionnaireResponse r in QuestionnaireResponses)
                    {
                        pass = pass && r.Passing;
                    }
                }
                return pass;
            }
        }
    }


    // A table of answers corresponding to a completed entry
    [Table("tblQuestionnaireResponse")]
    public class QuestionnaireResponse : AuditModelCreateBase
    {
        public int ID { get; set; }

        public int QuestionnaireSubmissionID { get; set; }
        [ForeignKey("QuestionnaireSubmissionID")]
        public virtual QuestionnaireSubmission QuestionnaireSubmission { get; set; }

        public int QuestionnaireQuestionID { get; set; }
        [ForeignKey("QuestionnaireQuestionID")]
        public virtual QuestionnaireQuestion QuestionnaireQuestion { get; set; }

        [StringLength(255)]
        public String Answer { get; set; }

        [DisplayName("Notes")]
        public string Notes { get; set; }

        [InverseProperty("QuestionnaireResponse")]
        public virtual List<QuestionnaireResponseBlob> QuestionnaireResponseBlobs { get; set; }

        [NotMapped]
        public QuestionnaireResponseBlob QuestionnaireResponseBlob
        {
            get { return this.QuestionnaireResponseBlobs == null || this.QuestionnaireResponseBlobs.Count == 0 ? null : this.QuestionnaireResponseBlobs.Where(s => s.QuestionnaireResponseID == this.ID).FirstOrDefault(); }
        }


        [NotMapped]
        public bool Passing
        {
            get
            {
                char delimiter = QuestionnaireQuestion.DROPDOWNDELIMITER;
                // during sync question seems to be null, so fetch
                QuestionnaireQuestion question = QuestionnaireQuestion;
                if (question == null)
                {
                    using (DispatchCrudeDB db = new DispatchCrudeDB())
                    {
                        question = db.QuestionnaireQuestions.Find(QuestionnaireQuestionID);
                    }
                }

                if (question.IsRequired == false && string.IsNullOrEmpty(Answer))
                {
                    return true; // empty but not required
                }
                if (string.IsNullOrEmpty(question._PassingCondition))
                {
                    return true; // no passing validation to check
                }

                switch (question.QuestionnaireQuestionTypeID)
                {
                    case (int)QuestionnaireQuestionType.TYPE.Integer:
                    case (int)QuestionnaireQuestionType.TYPE.Decimal:
                        // check as number, answer should fall within min|max range
                        try
                        {
                            string[] range = question._PassingCondition.Split(delimiter);
                            // check min
                            if (!string.IsNullOrEmpty(range[0]) && double.Parse(Answer) < double.Parse(range[0]))
                                return false;
                            // check max
                            else if (!string.IsNullOrEmpty(range[1]) && double.Parse(Answer) > double.Parse(range[1]))
                                return false;
                            else
                                return true;
                        }
                        catch
                        {
                            return false;
                        }
                    //case (int)QuestionnaireQuestionType.TYPE.String:
                    case (int)QuestionnaireQuestionType.TYPE.DropDown:
                        // check as string, answer is one of the delimiter-separated possible answers
                        return (delimiter + question._PassingCondition + delimiter).Contains(delimiter + Answer + delimiter);
                    case (int)QuestionnaireQuestionType.TYPE.Boolean:
                        // check as boolean, answer should equal value
                        return Converter.ToBoolean(question._PassingCondition) == Converter.ToBoolean(Answer);
                    case (int)QuestionnaireQuestionType.TYPE.Timestamp:
                        // check as datetime
                        return true;
                    default:
                        return true; // nothing to validate
                }
            }
        }

    }


    // A table of answers corresponding to a blob question
    [Table("tblQuestionnaireResponseBlob")]
    public class QuestionnaireResponseBlob : AuditModelCreateBase
    {
        public int ID { get; set; }

        public int QuestionnaireResponseID { get; set; }
        [ForeignKey("QuestionnaireResponseID")]
        public virtual QuestionnaireResponse QuestionnaireResponse { get; set; }

        public byte[] AnswerBlob { get; set; }

        [NotMapped]
        public string SignatureBlobSrc
        {
            get
            {
                return AnswerBlob.Length == 0 ? "" : string.Format("data:image/{1};base64,{0}", Convert.ToBase64String(AnswerBlob), "png");
            }
        }

    }
}