﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AlonsIT;

namespace DispatchCrude.Core
{
    public class CarrierRuleHelper
    {
        public enum Type {
                HOS_POLICY = 1
                , TRUCK_SELECTION = 2
                , TRAILER_SELECTION = 3
                , DVIR_INTERVAL = 4
                //*********
                , ENFORCE_DRIVER_SCHEDULING = 13
                , ENFORCE_DRIVER_COMPLIANCE = 14
                , ENFORCE_HOS_HOURS = 15
                //*********
                , DRIVER_MESSAGING_HISTORY_DAYS = 20
                , MOBILE_ORDER_SEQUENCING = 21
                , LIMIT_TO_SINGLE_ACTIVE_ORDER = 22
        }

        static public object GetRuleValue(DateTime effDate, Type type, int? driverID, int? carrierID, int? terminalID, int? stateID, int? regionID, object defaultValue = null)
        {
            using (SSDB ssdb = new SSDB())
            {
                return ssdb.QuerySingleValue(
                    "SELECT R.Value "
                    + "FROM dbo.fnCarrierRules('{0:M/d/yyyy}', NULL, {1}, {2}, {3}, {4}, {5}, {6}, 1) R"
/* 0 */             , effDate as object
/* 1 */             , (int)type
/* 2 */             , driverID.HasValue ? driverID.Value.ToString() : "NULL"
/* 3 */             , carrierID.HasValue ? carrierID.Value.ToString() : "NULL"
/* 4 */             , terminalID.HasValue ? terminalID.Value.ToString() : "NULL"
/* 5 */             , stateID.HasValue ? stateID.Value.ToString() : "NULL"
/* 6 */             , regionID.HasValue ? regionID.Value.ToString() : "NULL") ?? defaultValue;
            }
        }
    }
}