﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchCrude.Core
{
    public class DateFilter
    {
        static public DateFilter CURRENT_MONTH { get { return new DateFilter("Current Month", DateHelper.StartOfMonth(DateTime.Today), DateHelper.EndOfMonth(DateTime.Today)); } }
        static public DateFilter PREVIOUS_MONTH { get { return new DateFilter("Previous Month", DateHelper.StartOfMonth(DateTime.Today.AddMonths(-1)), DateHelper.EndOfMonth(DateTime.Today.AddMonths(-1))); } }

        public DateFilter(string name, DateTime startDate, DateTime endDate)
        {
            Name = name;
            StartDate = startDate;
            EndDate = endDate;
        }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
    
}
