﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace DispatchCrude.Core
{
    /// <summary>
    /// support class to ensure Data values are formatted correctly
    /// see: "http://www.telerik.com/forums/the-value-'-date(-62135578800000)-'-is-not-valid-for"
    /// </summary>
    public class CustomDateBinder : DefaultModelBinder
    {
        static public void Register(ModelBinderDictionary modelBinders)
        {
            modelBinders.Add(typeof(DateTime), new CustomDateBinder());
        }

        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var value = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);

            if (value.AttemptedValue.StartsWith("/Date("))
            {
                try
                {
                    DateTime date = new DateTime(1970, 01, 01, 0, 0, 0, DateTimeKind.Utc).ToUniversalTime();
                    string attemptedValue = value.AttemptedValue.Replace("/Date(", "").Replace(")/", "");
                    double milliSecondsOffset = Convert.ToDouble(attemptedValue);
                    DateTime result = date.AddMilliseconds(milliSecondsOffset);
                    result = result.ToUniversalTime();
                    return result;
                }
                catch
                {
                    // eat exceptions ?
                }
            }
            return base.BindModel(controllerContext, bindingContext);
        }
    }
}
