﻿using System.Web;
using DispatchCrude.Models;
using DispatchCrude.Extensions;

namespace DispatchCrude.Core
{
    public class DispatchCrudeHelper
    {
        public enum ReportDatePeriodType { WEEKTODATE = 1, MONTHTODATE = 2, LASTMONTH = 3, QTRTODATE = 4, YEARTODATE = 5, YESTERDAY = 6, LAST7DAYS = 7, LAST7DAYS_INCLUSIVE = 8 }

        static public bool IsEstimatedVolume(int ticketTypeID)
        {
            return ticketTypeID.IsIn((int)TicketType.TYPE.GaugeRun, (int)TicketType.TYPE.Gauge_Auto_Close, (int)TicketType.TYPE.GaugeNet, (int)TicketType.TYPE.NetVolume);
        }

        static public int GetProfileValueInt(HttpContext ctx, string propName, int defaultVal = 0)
        {
            return Converter.ToInt32(ctx.Profile.GetPropertyValue(propName), defaultVal);
        }
        static public bool GetProfileValueBool(HttpContext ctx, string propName, bool defaultVal = true)
        {
            return Converter.ToBoolean(ctx.Profile.GetPropertyValue(propName), true);
        }

        static public bool GetProfileUseDST(HttpContext ctx)
        {
            return GetProfileValueBool(ctx, "UseDST", true);
        }
        static public string GetProfileTimeZone(HttpContext ctx)
        {
            return Converter.SplitCamelCase(TimeZone.GetName(GetProfileValueInt(ctx, "TimeZoneID", 1)));
        }
        static public string GetProfileTimeZoneAbbrev(HttpContext ctx)
        {
            return TimeZone.GetAbbrev(GetProfileValueInt(ctx, "TimeZoneID", 1));
        }

    }

}