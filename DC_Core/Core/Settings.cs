﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlonsIT;

namespace DispatchCrude.Core
{
    public static class Settings
    {
        public static object Value(this SettingsID id, object defaultValue = null)
        {
            // TODO: add the items to cache, and reload them whenever a change is saved using this page
            using (SSDB ssdb = new SSDB())
            {
                object ret = ssdb.QuerySingleValue("SELECT Value FROM tblSetting WHERE ID = {0}", (Int32)id);
                if (ret == null)
                    ret = defaultValue;
                return ret;
            }
        }
        public static bool AsBool(this SettingsID id, bool defaultValue = false)
        {
            return DBHelper.ToBoolean(id.Value(defaultValue));
        }
        static public int AsInt(this SettingsID id, int defaultValue = 0)
        {
            return DBHelper.ToInt32(id.Value(defaultValue));
        }
        static public decimal AsDecimal(this SettingsID id, decimal defaultValue = 0)
        {
            return DBHelper.ToDecimal(id.Value(defaultValue));
        }
        static public string AsString(this SettingsID id, string defaultValue = "")
        {
            return id.Value(defaultValue).ToString();
        }

        public enum SettingsID
        {
            BswMinID = 1
            , BswMaxID = 2
            , GravityMinID = 3
            , GravityMaxID = 4
            , DriverCanOverrideID = 5
            , WaitNotesRequiredThreshold = 6
            //, WaitMinutesUnbillableMinutes = 7    //Removed 1/11/17
            //No setting at ID 8, 9, or 10
            , DA_SyncFrequencyMinutes = 11
            , DA_LatestAppVersion = 12
            , DA_ValidatePasswordHash = 13
            , DA_OrderPickupBackDateMaxHours = 14
            //, DA_OrderForwardDateMaxHours = 15    //Removed 1/11/17
            , Report_UOM = 16
            , Default_UOM = 17
            //No setting at ID 18
            , ProductTempMinID = 19
            , ProductTempMaxID = 20
            , Stale_Origin_Days = 21
            , Stale_Destination_Days = 22
            , ProductMin_Max_Temp_Deviation_Degrees = 23
            , DA_ShowBottomDetailEntryFields = 24
            //, AllowUnstrappedTanks = 25   //Removed 12/15/16 with the change to the new origin tanks page
            , Driver_GPS_Coarse_Interval_Meters = 26
            , Driver_GPS_Coarse_Interval_Seconds = 27
            , Driver_GPS_Fine_Interval_Meters = 28
            , Driver_GPS_Fine_Interval_Seconds = 29
            , Driver_GPS_Fine_Threshold_Meters = 30
            , GeoFenceRadiusMeters_Default = 31
            //, DBAuditEnabled = 32    //Removed 1/11/17
            , DA_MinimumAppVersion = 33
            , PurgeDeletedOrdersObsoleteDays = 34
            , ALLOW_AUDIT_UPDATE = 35
            , CountryCode = 36
            , TableMxPageSize = 37
            , DisableAJAX = 38
            , RequireTruckMileage = 39
            , UnloadedMilesWarningThreshold = 40
            , DA_ArriveDepartEntryRangeMin = 41
            , UnloadedMilesWarningPercentThreshold = 42
            //, Limit_To_Single_Active_Order = 43    //Removed 1/11/17
            , DA_DEBUG_SYNC = 44
            , GA_SyncFrequencyMinutes = 45
            , GA_LatestAppVersion = 46
            , GA_ValidatePasswordHash = 47
            , Minimum_Gauger_App_Version = 48
            , GA_DEBUG_SYNC = 49
            , ArriveDepartEntryRangeFromNOW = 50
            , GA_ShowBottomDetailEntryFields = 51
            , DA_GA_SyncPriorOffsetSeconds = 52
            , SUNDEX_TicketSourceCode = 53
            , AutoAuditQualifiedOrders = 54
            , EnableOrderApproval = 55
            , DA_ShowRateMiles = 56
            , DA_PhotoQuality = 57
            , RestrictUserToProfileRegion = 58
            , SUNDEX_CompanyCode = 59
            , EmailSubscriptionHtmlBody = 60
            , LimitDestinationSelectionToOriginRegion = 61
            , UseHeuristicDataToAverageGPSLocations = 62
            , DriverLocationsLimitHours = 63
            , ECOTNumberOfDaysBackForAveragingRouteTimes = 64
            , DisplayProducerOnTruckOrdersPage = 65
            , DisplayRateMilesOnTruckOrdersPage = 66
            , DispatchMessaging = 67
            , SUNDEX_IncludeRejects = 68
            , DurationFormat = 69
            , RequireTanksDuringCreate = 70
            , ShowRecentlyDelivered = 71
            , DispatchPlanner_HideUnavailableDrivers = 72
            , Settlement_IncludePendingStatus = 73
        }

        public static int DefaultPageSize { get { return Settings.SettingsID.TableMxPageSize.AsInt(100); } }

        public static int[] MvcGridPageSizes  //Used to set the MVC grid page sizes list in the pager dropdown - .PageSizes(Settings.MvcGridPageSizes)
        {
            get
            {
                //Set all default available pages sizes
                List<int> pageSizes = new List<int> { 10, 25, 50, 100, 250 };

                //If the user defined default page size matches anything in the list, remove the duplicate from the list
                if (pageSizes.IndexOf(DefaultPageSize) > -1)
                    pageSizes.RemoveAt(pageSizes.IndexOf(DefaultPageSize));

                //Insert the user defined default page size and sort the list
                pageSizes.Add(DefaultPageSize);
                pageSizes.Sort();                    

                return pageSizes.ToArray();
            }
        }

        // This will need to be made more specific if we ever do anything outside the U.S. and Canada.
        static public bool IsCountryUSA { get { return SettingsID.CountryCode.AsBool(true); } }
    }
}