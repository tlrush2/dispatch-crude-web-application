﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using AlonsIT;
using DispatchCrude.Extensions;

namespace DispatchCrude.Core
{
    public class DataTableHelper
    {
        static public DataTable AddActiveColumn(System.Data.DataTable data)
        {
            bool hasDelete = data.Columns.Contains("DeleteDate")
                , hasDeleteUTC = data.Columns.Contains("DeleteDateUTC")
                , hasActive = data.Columns.Contains("Active");
            if ((hasDelete || hasDeleteUTC) && !hasActive)
            {
                data.Columns.Add(new System.Data.DataColumn("Active", typeof(bool)));
                foreach (System.Data.DataRow row in data.Rows)
                {
                    row["Active"] = DBHelper.IsNull(row[hasDelete ? "DeleteDate" : "DeleteDateUTC"]);
                    row.AcceptChanges();
                }
            }
            return data;
        }

        static public bool IsIntegerType(Type dataType)
        {
            return dataType.IsIn(Type.GetType("System.Int32"), Type.GetType("System.Int16"), Type.GetType("System.Byte"));
        }

        static public DataTable SetIDNullsToZero(DataTable dt)
        {
            foreach (DataRow row in dt.Rows)
            {
                foreach (DataColumn col in dt.Columns)
                {
                    if (col.ColumnName.EndsWith("ID") && IsIntegerType(col.DataType) && DBHelper.IsNull(row[col]))
                        row[col] = 0;
                }
                row.AcceptChanges();
            }
            return dt;
        }
    }
}
