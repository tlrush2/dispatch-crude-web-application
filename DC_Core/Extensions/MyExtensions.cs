﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DispatchCrude.Extensions
{
    public static class MyExtensions
    {
        public static string Truncate(this string s, int length, bool addEllipse = false)
        {
            if (s.Length > length) return s.Substring(0, length) + (addEllipse ? "..." : "");
            return s;
        }
        public static string ReplaceFirst(this string text, string search, string replace)
        {
            int pos = text.IndexOf(search);
            if (pos < 0)
            {
                return text;
            }
            return text.Substring(0, pos) + replace + text.Substring(pos + search.Length);
        }
        public static bool IsIn<T>(this T generic, params T[] collection)
        {
            if (collection == null || collection.Count() == 0) return false; // just for sure
            return collection.Contains(generic);
        }
        public static string QuoteStr(this string text)
        {
            return AlonsIT.DBHelper.QuoteStr(text);
        }
		public static bool CsvContains(this string s, string expression)
        {
            return ("," + s + ",").Contains("," + expression + ",");
        }

        public static Int32 ToInt32(this object o, int defValue = 0)
        {
            return AlonsIT.DBHelper.ToInt32(o, defValue);
        }

        public static string TrimWrappingChars(this string s, char l, char r, bool recurse = true)
        {
            bool done = false;
            while (s.StartsWith(l.ToString()) && s.EndsWith(r.ToString()) && !done)
            {
                s = s.Substring(1, s.Length - 2);
                done = !recurse;
            }
            return s;
        }
	}
}