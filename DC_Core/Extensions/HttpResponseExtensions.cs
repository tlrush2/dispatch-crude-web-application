﻿using System;
using System.IO;
using System.Web;

namespace DispatchCrude.Extensions
{
    static public class HttpResponseExtensions
    {
        // use this instead of Response.End() - which causes an Thread.Terminated exception (noticed and annoying while debugging)
        // http://stackoverflow.com/questions/2265412/set-timeout-to-an-operation
        public static void EndSafe(this HttpResponse response)
        {
            response.Flush(); // Sends all currently buffered output to the client.
            response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
        }

        public static void ExportExcelStream(this HttpResponse response, MemoryStream excelStream, string filename)
        {
            ExportFileStream(response, excelStream, filename, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        }

        public static void ExportFileStream(this HttpResponse response, MemoryStream fileStream, string filename, string contentType = null)
        {
            response.Clear();
            response.AppendCookie(new HttpCookie("fileDownloadToken", "done"));
            if (contentType != null)
                response.ContentType = contentType;
            response.AddHeader("Content-Disposition", string.Format("attachment;filename=\"{0}\";", filename));
            fileStream.WriteTo(response.OutputStream);
            response.StatusCode = 200;
            response.EndSafe();
        }
    }
}