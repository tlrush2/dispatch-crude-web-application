﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using DispatchCrude.Core;

namespace DispatchCrude.Extensions
{
    static public class JsonPropListSerializer
    {
        static public string PropListSerialize(this object obj, string[] props)
        {
            return JsonConvert.SerializeObject(obj, Formatting.Indented, new JsonSerializerSettings { ContractResolver = new PropListContractResolver(props) });
        }

        private class PropListContractResolver : DefaultContractResolver
        {
            private readonly string[] _props;

            public PropListContractResolver(string[] props = null)
            {
                if (props == null)
                    props = new string[] { "ID", "Name" };
                _props = props;
            }

            protected override IList<JsonProperty> CreateProperties(Type type, MemberSerialization memberSerialization)
            {
                IList<JsonProperty> properties = base.CreateProperties(type, memberSerialization);
                // only serializer properties that were specified in the constructor
                properties = properties.Where(p => p.PropertyName.IsIn(_props)).ToList();

                return properties;
            }
        }
    }
}