﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchCrude.Extensions
{
    public static class StringUtils
    {
        static public string[] TrimSplit(this string expr, params char[] separator)
        {
            return TrimSplit(expr, false, separator);
        }
        static public string[] TrimSplit(this string expr, bool toLower, params char[] separator)
        {
            // if no parameter is specified, default to a ',' character
            if (separator.Length == 0) separator = new char[] { ',' };
            string[] ret = expr.Split(separator, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < ret.Length; i++)
            {
                ret[i] = ret[i].Trim();
                if (toLower) ret[i] = ret[i].ToLower();
            }
            return ret;
        }
        // return null if the value is null or matches one of the provided values, otherwise return the trimed value
        static public string TZ(this string val, params string[] nullMatch)
        {
            return val != null && !string.IsNullOrWhiteSpace(val) && !nullMatch.Contains(val) ? val.Trim() : null;
        }
    }
}
