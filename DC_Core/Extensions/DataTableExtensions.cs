﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Web.UI;

namespace DispatchCrude.Extensions
{
    static public class DataTableExtensions
    {
        public static void OnlyGridColumns(this DataTable dt, GridTableView gridTableView, params string[] extraColNames)
        {
            for (int i = 0; i < extraColNames.Length; i++)
            {
                extraColNames[i] = extraColNames[i].ToLower();
            }
            HashSet<string> dataFields = new HashSet<string>();
            PropertyInfo pi = null;
            foreach (GridColumn col in gridTableView.Columns)
            {
                if ((pi = col.GetType().GetProperty("DataField")) != null)
                {
                    dataFields.Add(pi.GetValue(col, null).ToString().ToLower());
                }
            }
            for (int i = dt.Columns.Count - 1; i >= 0; i--)
            {
                string colName = dt.Columns[i].ColumnName.ToLower();
                if (!dataFields.Contains(colName) && !extraColNames.Contains(colName))
                    dt.Columns.Remove(dt.Columns[i]);
            }
        }
    }
}
