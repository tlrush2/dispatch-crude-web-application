﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlonsIT;

namespace DispatchCrude.DataExchange
{
    public class SupportEngine
    {
        public enum RetrieveType { rtDriverSettlementStatementEmail = 2, rtEmailSubscription = 3, }
        static public DataTable RetrieveData(RetrieveType retrieveType)
        {
            string type = AppSettings.GetAppSetting("Type")
                , suffix = AppSettings.GetAppSetting("Suffix");
            using (SSDB db = new SSDB())
            {
                return db.GetPopulatedDataTable("EXEC spExecuteDispatchCrudeDBSql {0}, {1}, {2}"
                    , (int)retrieveType
                    , (object)DBHelper.QuoteStr(AppSettings.GetAppSetting("Type"))
                    , (object)DBHelper.QuoteStr(AppSettings.GetAppSetting("Suffix")));

            }
        }
    }
}
