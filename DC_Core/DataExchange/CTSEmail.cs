﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace DispatchCrude.DataExchange
{
    public class CTSEmail: MailMessage
    {
        // SMTP settings
        public string SMTP_HOST = "smtp04.domainlocalhost.com"; // default value
        public int SMTP_PORT = 587;
        public SecurityProtocolType SMTP_SecurityProtocolType = SecurityProtocolType.Tls;
        public bool SMTP_EnableSSL = true;
        public int SMTP_Timeout = 10000;

        public static string SMTP_ID = "support@dispatchcrude.com";
        private static string SMTP_Password = "D1spatchCrud3";

        public static string DEFAULT_FROM = SMTP_ID; // default value


        // constructors
        public CTSEmail(string to, string subject, string body, bool IsHtml = false) : this(to, DEFAULT_FROM, subject, body, IsHtml)
        {
        }
        public CTSEmail(string to, string from, string subject, string body, bool IsHtml = false)
        {
            this.From = new MailAddress(from);
            this.To.Add(to); // can be comma separated List

            this.Subject = subject;

            this.Body = body;
            this.IsBodyHtml = IsHtml;
        }

        public CTSEmail AddCC(string cc)
        {
            if (cc.Length > 0) this.CC.Add(new MailAddress(cc));
            return this;
        }

        public CTSEmail Attach(Stream stream, string filename)
        {
            this.Attachments.Add(new Attachment(stream, filename));
            return this;  // returning "this" allows method chaining ( new Email(xxx).Attach().Attach().Send(); in a single call )
        }

        public CTSEmail Send()
        {
            SmtpClient smtp = new SmtpClient(SMTP_HOST, SMTP_PORT);
            ServicePointManager.SecurityProtocol = SMTP_SecurityProtocolType;
            smtp.EnableSsl = SMTP_EnableSSL;
            smtp.Timeout = SMTP_Timeout;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(SMTP_ID, SMTP_Password);
            smtp.Send(this);
            return this;
        }
    }
}
