﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using AlonsIT;
using Syncfusion.XlsIO;

namespace DispatchCrude.Models.ImportCenter
{
    public class ImportCenterInputHelper
    {
        static public DataTable ToDataTable(string filename, System.IO.Stream fileStream, bool firstRowHeader, string tableName = null)
        {
            DataTable ret = null;
            if (filename.EndsWith(".xlsx", StringComparison.CurrentCultureIgnoreCase))
            {
                ExcelEngine excelEngine = new ExcelEngine();
                IApplication application = excelEngine.Excel;
                IWorkbook workbook = application.Workbooks.Open(fileStream);
                IWorksheet sheet = workbook.Worksheets[0];
                ExcelExportDataTableOptions options = ExcelExportDataTableOptions.None;
                if (firstRowHeader)
                    options |= ExcelExportDataTableOptions.ColumnNames;

                ret = sheet.ExportDataTable(sheet.UsedRange, options);
                workbook.Close();
            }
            else if (filename.EndsWith(".csv", StringComparison.CurrentCultureIgnoreCase))
            {
                using (CsvFileReader reader = new CsvFileReader(fileStream))
                {
                    ret = reader.ToDataTable(firstRowHeader);
                }
            }
            // remove any rows that have no data in them (all values are blank/NULL)
            for (int i = ret.Rows.Count-1; i >= 0; i--)
            {
                bool hasData = false;
                foreach (DataColumn col in ret.Columns)
                {
                    if (!DBHelper.IsNull(ret.Rows[i][col]))
                    {
                        hasData = true;
                        break;
                    }
                }
                if (!hasData)
                    ret.Rows.Remove(ret.Rows[i]);
            }
            if (tableName != null) ret.TableName = tableName;
            return ret;
        }
    }
}