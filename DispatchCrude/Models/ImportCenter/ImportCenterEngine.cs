﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using DispatchCrude.App_Code;
using DispatchCrude.Core;
using DispatchCrude.Models;
using DispatchCrude.Extensions;
using AlonsIT;
using System.Text.RegularExpressions;
using System.Text;

namespace DispatchCrude.Models.ImportCenter
{
    public class ProcessResult
    {
        public DataTable DataTable { get; set; }
        public int RootObjectID { get; set; }
        public int ImportCenterID { get; set; }
        public string ImportFileName { get; set; }
        public int SuccessCount { get; set; }
        public int WarningCount { get; set; }
        public int ErrorCount { get; set; }
        public string GeneralError { get; set; }
        public ProcessResult(int rootObjectID, int importcenterID, string importFileName)
        {
            RootObjectID = rootObjectID;
            ImportCenterID = importcenterID;
            ImportFileName = importFileName;
        }
    }

    /* class to retrieve and persist relevant fields for a single ImportField DataRow */
    class ImportFieldInfo
    {
        public int ID { get; set; }
        public int TypeID { get; set; }
        public string CSharpExpression { get; set; }
        public string ObjectName { get; set; }
        public string FieldName { get; set; }
        public string[] InputFields { get; private set; }
        public bool ReadOnly { get; set; }
        public ImportFieldInfo()
        {
            InputFields = new string[] { };
        }
        public ImportFieldInfo(DataRow drImportField): this()
        {
            Init(drImportField);
        }
        private void Init(DataRow drImportField)
        {
            ID = DBHelper.ToInt32(drImportField["ID"]);
            TypeID = DBHelper.ToInt32(drImportField["ObjectFieldTypeID"]);
            if (drImportField.Table.Columns.Contains("CSharpExpression"))
                CSharpExpression = DBHelper.ToString(drImportField["CSharpExpression"]);
            FieldName = DBHelper.ToString(drImportField["FieldName"]);

            if (drImportField.Table.Columns.Contains("InputFieldsCSV") && DBHelper.ToString(drImportField["InputFieldsCSV"], true) != null)
                InputFields = drImportField["InputFieldsCSV"].ToString().Trim().Split(',');

            if (drImportField.Table.Columns.Contains("ReadOnly"))
                ReadOnly = DBHelper.ToBoolean(drImportField["ReadOnly"]);
        }
    }

    /* base class to persist a retrieved value and supporting data regarding the retrieval operation */
    internal class RetrieveValueOutcome
    {
        public object Value { get; set; }
        public bool Success { get; set; }
        public int FieldUpdateCount { get; set; }
        public HashSet<string> Errors { get; private set; }
        public HashSet<string> Warnings { get; private set; }
        public object ValueToDbNull { get { return DBHelper.IsNull(Value, DBNull.Value, true); } }
        public RetrieveValueOutcome(object value = null) 
        { 
            Value = value;
            Success = true;
            FieldUpdateCount = 0;
            Errors = new HashSet<string>(); 
            Warnings = new HashSet<string>(); 
        }
    }
    internal class ProcessOutcome: RetrieveValueOutcome
    {
        public int ObjectID { get; set; }
        public string IDFieldName { get; set; }
        public string Action { get; set; }
        public ProcessOutcome(RetrieveValueOutcome outcome = null): base()
        {
            if (outcome == null)
                Action = "Add";
            else
                Append(outcome);
        }
        public ProcessOutcome Append(RetrieveValueOutcome outcome, bool includeValueOnly = false) 
        {
            Success = outcome.Success;
            Value = outcome.Success ? outcome.Value : null;
            FieldUpdateCount += outcome.FieldUpdateCount;
            foreach (string err in outcome.Errors) Errors.Add(err);
            foreach (string warn in outcome.Warnings) Warnings.Add(warn);
            if (!includeValueOnly && outcome is ProcessOutcome)
            {
                ObjectID = (outcome as ProcessOutcome).ObjectID;
                IDFieldName = (outcome as ProcessOutcome).IDFieldName;
                Action = (outcome as ProcessOutcome).Action;
            }
            return this;
        }
    }
    internal class RetrieveRecordOutcome: ProcessOutcome
    {
        new private object Value { get; set; }
        public DataRow DataRow { get; set; }
        public Dictionary<string, object> Values { get; private set; }
        public string SqlSourceName { get; private set; }
        public string SqlTargetName { get; private set; }
        public string ObjectName { get; private set; }
        public bool HasActiveField { get; private set; }
        public bool HasDeleteDateUTCField { get; private set; }
        public bool Editable { get; private set; }
        public RetrieveRecordOutcome(int objectID, bool editable)
        {
            Init();
            // find the info about the object (table) to retrieve - it is either the root object or the parent field object
            object[] values = SSDB.QuerySingleRowStatic(
                @"SELECT Name, SqlSourceName, isnull(SqlTargetName, ''), IDFieldName, ltrim(ID), HasActiveField, HasDeleteDateUTCField 
                FROM viewObject WHERE ID ={0}", objectID);
            ObjectName = values[0].ToString();
            SqlSourceName = values[1].ToString();
            SqlTargetName = values[2].ToString();
            IDFieldName = values[3].ToString();
            ObjectID = values[4].ToInt32();
            HasActiveField = DBHelper.ToBoolean(values[5]);
            HasDeleteDateUTCField = DBHelper.ToBoolean(values[6]);
            Editable = editable;

            // any object that has a NULL SqlTargetName value is not editable (really should never be encountered since editing child objects is not allowed except for Ticket-Order)
            if (Editable && string.IsNullOrWhiteSpace(SqlTargetName)) Editable = false;

            // if the SqlTargetName was NULL (empty), then use the SqlSourceName instead
            if (string.IsNullOrWhiteSpace(SqlTargetName)) SqlTargetName = SqlSourceName;
        }
        public RetrieveRecordOutcome(ProcessOutcome outcome = null): base(outcome) 
        {
            Init();
        }
        private void Init()
        {
            Values = new Dictionary<string, object>();
        }
    }

    public class ImportCenterEngine
    {
        static public string INPUT_ACTION_FIELDNAME = "ImportAction"
            , INPUT_OUTCOME_FIELDNAME = "ImportOutcome"
            , INPUT_ERRORS_FIELDNAME = "ImportErrors"
            , INPUT_WARNINGS_FIELDNAME = "ImportWarnings";
        static public int INPUT_ACTION_COL_INDEX = 0
            , INPUT_OUTCOME_COL_INDEX = 1
            , INPUT_ERRORS_COL_INDEX = 2
            , INPUT_WARNINGS_COL_INDEX = 3
            , INPUT_ID_COL_INDEX = 4
            , INPUT_ORDERNUM_COL_INDEX = 5
            , INPUT_ORDERDATE_COL_INDEX = 6;

        static public string[] IMPORT_FIELDNAMES { get { return new string[] { INPUT_ACTION_FIELDNAME, INPUT_OUTCOME_FIELDNAME, INPUT_WARNINGS_FIELDNAME }; } }
        
        private enum OBJECTS { Order = 1, Ticket = 2, Carrier = 12, Driver = 13, Truck = 14, Trailer = 15, }
        private enum OBJECT_FIELDS { Ticket_Order = 316, }
        private enum ALLOWNULL { No = 0, Yes = 1, Dynamic = 2, }

        public class ProcessingException_MissingInputFile : Exception { public ProcessingException_MissingInputFile(string message = null) : base(message) { } }
        public class ProcessingException_InvalidMapping : Exception
        {
            public ProcessingException_InvalidMapping(string message = null, params string[] invalidFields) : base(message)
            {
                InvalidFields = new List<string>();
                if (invalidFields.Length > 0) InvalidFields.AddRange(invalidFields);
            }

            public List<string> InvalidFields { get; private set; }
        }

        private SSDB _db = null;
        private int _importCenterID, _rootObjectID;
        private DataTable _dtInput;
        private DataTable _dtImportFields;
        private string _importFileName;
        private bool _testOnly; // if true, do not complete the import transaction

        public ImportCenterEngine(int importCenterID, int rootObjectID, DataTable dtInput, string importFileName)
        {
            _importCenterID = importCenterID;
            _rootObjectID = rootObjectID;
            _importFileName = importFileName;
            if (dtInput == null)
            {
                throw new ProcessingException_MissingInputFile("Input File timed-out - please re-provide an inpout file and resubmit");
            }
            else if (new DispatchCrudeDB().ImportCenterDefinitions.Count(x => x.ID == importCenterID && x.RootObjectID == rootObjectID) == 0)
            {
                throw new ProcessingException_InvalidMapping("Import Spec doesn't match the specified Root Object");
            }
            // retrieve the importfields for the specified import spec
            using (SSDB db = new SSDB())
            {
                // QUERY SUPPORTING DATA
                _dtImportFields = db.GetPopulatedDataTable(
                    "SELECT * FROM dbo.viewImportCenterFieldDefinition WHERE ImportCenterDefinitionID={0}"
                    , (object)importCenterID);
            }
            _dtInput = GenerateInputTable(_dtImportFields, dtInput);

        }

        public ProcessResult Process(bool testOnly = false)
        {   // if testOnly is specified, do not actually save any changes to the database (the transaction will be ROLLED BACK)
            _testOnly = testOnly;

            ProcessResult ret = new ProcessResult(_rootObjectID, _importCenterID, _importFileName);

            /* add support fields (at front of input datatable) to store processing data throughout the processing */
            if (!_dtInput.Columns.Contains(INPUT_ACTION_FIELDNAME))
                _dtInput.Columns.Add(INPUT_ACTION_FIELDNAME).SetOrdinal(INPUT_ACTION_COL_INDEX);
            if (!_dtInput.Columns.Contains(INPUT_OUTCOME_FIELDNAME))
                _dtInput.Columns.Add(INPUT_OUTCOME_FIELDNAME).SetOrdinal(INPUT_OUTCOME_COL_INDEX);
            if (!_dtInput.Columns.Contains(INPUT_ERRORS_FIELDNAME))
                _dtInput.Columns.Add(INPUT_ERRORS_FIELDNAME).SetOrdinal(INPUT_ERRORS_COL_INDEX);
            if (!_dtInput.Columns.Contains(INPUT_WARNINGS_FIELDNAME))
                _dtInput.Columns.Add(INPUT_WARNINGS_FIELDNAME).SetOrdinal(INPUT_WARNINGS_COL_INDEX);

            // PROCESS THE INPUT FILE- ITERATE OVER EVERY INPUT ROW
            foreach (DataRow drInput in _dtInput.Rows)
            {
                drInput[INPUT_OUTCOME_FIELDNAME] = string.Empty;
                drInput.AcceptChanges();

                ProcessOutcome outcome = null;
                using (SSDB db = new SSDB(true))
                {
                    _db = db;
                    outcome = ProcessInputRow(db, drInput);

                    // if process was succesful, and testOnly = false, then attempt to commit the transaction (report failure if unsuccessful)
                    if (outcome.Success && !testOnly)
                    {
                        if (!db.CommitTransaction(false))
                            outcome.Success = false;
                    }
                    _db = null;
                }
                drInput[INPUT_ACTION_FIELDNAME] = outcome.Action;
                drInput[INPUT_OUTCOME_FIELDNAME] = outcome.Success ? (outcome.FieldUpdateCount > 0 ? "Success" : "No Change") : "Failure";
                drInput[INPUT_WARNINGS_FIELDNAME] = string.Join("\r\n", outcome.Warnings);
                ret.WarningCount += outcome.Warnings.Count > 0 ? 1 : 0;

                if (!outcome.Success)
                {
                    drInput[INPUT_ERRORS_FIELDNAME] = string.Join("\r\n", outcome.Errors);
                    ret.ErrorCount++;
                }
                else
                {
                    ret.SuccessCount++;

                    if (outcome.IDFieldName != null && !drInput.Table.Columns.Contains(outcome.IDFieldName))
                        drInput.Table.Columns.Add(outcome.IDFieldName).SetOrdinal(INPUT_ID_COL_INDEX);
                    drInput[outcome.IDFieldName] = outcome.ValueToDbNull;
                    // special field data reporting for Order & Ticket imports
                    if (outcome.ObjectID.IsIn((int)OBJECTS.Order, (int)OBJECTS.Ticket))
                    {
                        int newID = DBHelper.ToInt32(outcome.Value);
                        Order newOrder = null;
                        if (outcome.ObjectID == (int)OBJECTS.Order)
                            newOrder = new DispatchCrudeDB().Orders.FirstOrDefault(o => o.ID == newID);
                        else
                        {
                            var ticket = new DispatchCrudeDB().OrderTickets.Include("Order").FirstOrDefault(t => t.ID == newID);
                            if (ticket != null)
                                newOrder = ticket.Order;
                        }

                        if (!drInput.Table.Columns.Contains("OrderNum"))
                            drInput.Table.Columns.Add("OrderNum").SetOrdinal(INPUT_ORDERNUM_COL_INDEX);
                        if (newOrder != null)
                            drInput["OrderNum"] = newOrder.OrderNum;

                        if (!drInput.Table.Columns.Contains("OrderDate"))
                            drInput.Table.Columns.Add("OrderDate").SetOrdinal(INPUT_ORDERDATE_COL_INDEX);
                        if (newOrder != null && newOrder.OrderDate.HasValue)
                            drInput["OrderDate"] = newOrder.OrderDate.Value.ToShortDateString();

                    }
                }
            }
            ret.DataTable = _dtInput;
            return ret;
        }

        private DataTable GenerateInputTable(DataTable dtImportFields, DataTable dtFullInput)
        {
            DataTable ret = new DataTable();

            // use a HashSet to eliminate duplicates
            HashSet<string> includedColumns = new HashSet<string>();
            foreach (DataRow drIF in dtImportFields.Rows)
            {
                foreach (string inputField in new ImportFieldInfo(drIF).InputFields)
                {
                    includedColumns.Add(inputField);
                }
            }
            List<string> invalidColumns = new List<string>();
            foreach (string colName in includedColumns)
            {

                if (!string.IsNullOrWhiteSpace(colName) && !dtFullInput.Columns.Contains(colName))
                {
                    invalidColumns.Add(colName);
                }
            }
            if (invalidColumns.Count > 0)
            {
                StringBuilder sb = new StringBuilder("Missing Input Columns:");
                invalidColumns.ForEach(a => sb.Append("\r\n" + a));
                throw new ProcessingException_InvalidMapping(sb.ToString(), invalidColumns.ToArray());
            }
            else
                ret = new DataView(dtFullInput).ToTable(true, includedColumns.ToArray());

            ret.TableName = "InputFile";
            return ret;
        }

        private RetrieveValueOutcome RetrieveInputValue(SSDB db, ImportFieldInfo fieldInfo, DataRow drInput)
        {
            RetrieveValueOutcome ret = new RetrieveValueOutcome();
            string[] inputFields = fieldInfo.InputFields;
            if (DBHelper.IsNull(fieldInfo.CSharpExpression))
            {
                // if inputFields.Length == 0 then ret = null (the default value)
                if (inputFields.Length == 1)
                    ret.Value = drInput[inputFields[0]];
            }
            else
            {
                string cSharpExpr = fieldInfo.CSharpExpression;
                try
                {
                    if (cSharpExpr.Contains("[0]"))
                    {
                        for (int i = 0; i < inputFields.Length; i++)
                            cSharpExpr = cSharpExpr.Replace(string.Format("[{0}]", i), DBHelper.DblQuoteStr(drInput[inputFields[i]].ToString()));
                    }
                    ret.Value = Eval.ExecuteStatic(cSharpExpr);
                }
                catch (Exception)
                {
                    ret.Success = false;
                    ret.Errors.Add("CSharp Expression parsing error: " + cSharpExpr);
                }
            }
            ret = ConvertToObjectFieldType(ret.Value, fieldInfo);
            return ret;
        }

        private RetrieveValueOutcome ConvertToObjectFieldType(object value, ImportFieldInfo fieldInfo)
        {
            RetrieveValueOutcome ret = new RetrieveValueOutcome(value);
            try
            {
                if (value == null)
                    ret.Value = DBNull.Value;
                else
                {
                    switch ((ObjectFieldType.Types)fieldInfo.TypeID)
                    {
                        case ObjectFieldType.Types.String:
                            if (!(value is string))
                                ret.Value = value.ToString();
                            break;
                        case ObjectFieldType.Types.Boolean:
                            if (!(value is bool))
                                ret.Value = DBHelper.ToBoolean(value);
                            break;
                        case ObjectFieldType.Types.Integer:
                            if (!(value is Int16) && !(value is Int16) && !(value is Int64) && !(value is UInt16) && !(value is UInt32) && !(value is UInt64))
                            {
                                Int64 i64;
                                if (Int64.TryParse(value.ToString(), out i64))
                                    ret.Value = i64;
                            }
                            break;
                        case ObjectFieldType.Types.Double:
                            if (!(value is decimal) & !(value is double))
                            {
                                double dbl;
                                if (double.TryParse(value.ToString(), out dbl))
                                    ret.Value = dbl;
                            }
                            break;
                        case ObjectFieldType.Types.DateTime:
                        case ObjectFieldType.Types.Date:
                        case ObjectFieldType.Types.Time:
                            if (!(value is DateTime))
                            {
                                DateTime dt;
                                if (DateTime.TryParse(value.ToString(), out dt))
                                    ret.Value = dt;
                                else
                                    ret.Value = DBNull.Value;
                            }
                            break;
                        case ObjectFieldType.Types.GUID:
                            if (DBHelper.IsNull(ret.Value) || ret.Value.ToString().ToLower().Contains("newid()")) ret.Value = Guid.NewGuid().ToString();
                            break;
                    }
                }
            }
            catch (Exception e)
            {
                ret.Warnings.Add(string.Format("Data [{0}] for {1}.{2} was invalid - data might have been lost during import - Details: {3}", value, fieldInfo.ObjectName, fieldInfo.FieldName, e.Message));
            }
            return ret;
        }

        // pass in the inputrow to process, and the importFieldID (NULL if this is the rootobjectid) and the indentLevel (start at -1 since we increment before processing)
        // recursive function that returns the value that was found (for other than the first call when importFieldID is null)
        private ProcessOutcome ProcessInputRow(SSDB db, DataRow drInput, int? parentFieldID = null, int nestLevel = 0)
        {
            ProcessOutcome ret = new ProcessOutcome();

            // the root table is always editable, or ORDER when rootTableID = TICKET
            int objectFieldID = parentFieldID.HasValue ? DBHelper.ToInt32(_dtImportFields.Select(string.Format("ID={0}", parentFieldID.Value))[0]["ObjectFieldID"]) : 0;
            bool editable = nestLevel == 0 || (nestLevel == 1 && _rootObjectID == (int)OBJECTS.Ticket && objectFieldID == (int)OBJECT_FIELDS.Ticket_Order);
            string baseFilter = string.Format("NestLevel={0}", nestLevel);
            if (parentFieldID.HasValue)
                baseFilter += string.Format(" AND ParentFieldID={0}", parentFieldID.Value);
            
            DataView dvImportFields = new DataView(_dtImportFields, baseFilter, "HasChildren DESC", DataViewRowState.CurrentRows);

            RetrieveRecordOutcome outcome = RetrieveExistingRecord(db, drInput, parentFieldID, dvImportFields, nestLevel, ref editable);
            
            ret.Append(outcome);
            if (outcome.DataRow != null)
            {
                if (editable)
                    ret.Append(UpdateTargetData(db, drInput, dvImportFields, outcome, nestLevel));
                else if (ret.Success && ret.IDFieldName != null)
                    ret.Value = outcome.DataRow[ret.IDFieldName];
            }

            return ret;
        }

        private RetrieveRecordOutcome RetrieveExistingRecord(SSDB db, DataRow drInput, int? parentFieldID, DataView dvImportFields, int nestLevel, ref bool editable)
        {
            int parentObjectID = nestLevel == 0
                ? _rootObjectID
                : DBHelper.ToInt32(_dtImportFields.Select(string.Format("ID={0}", parentFieldID.Value))[0]["ParentObjectID"]);

            RetrieveRecordOutcome ret = new RetrieveRecordOutcome(parentObjectID, editable);

            // if a parentFieldID is specified, determine if that field value is required (Dynamic is treated as TRUE for this purpose)
            bool parentFieldAllowNull = parentFieldID.HasValue ? DBHelper.ToBoolean(dvImportFields.Table.Select(string.Format("ID={0}", parentFieldID.Value))[0]["AllowNull"]) : true;

            bool hasNonNullParameter = false;
            // build up a WHERE clause (and save the values used for potential later use)
            WhereClause where = new WhereClause();
            foreach (DataRowView dvr in dvImportFields)
            {
                if (!DBHelper.IsNull(dvr["KeyComparisonTypeID"]))
                {
                    object inputValue = null;

                    // if this field has children, then recursively find the value to use for lookup
                    if (DBHelper.ToBoolean(dvr["HasChildren"]))
                    {
                        var x = ProcessInputRow(db, drInput, DBHelper.ToInt32(dvr["ID"]), nestLevel + 1);
                        ret.Append(x, true);
                        inputValue = x.Value;
                    }
                    else // else just retrieve the value from the input data (directly)
                    {
                        var x = RetrieveInputValue(db, new ImportFieldInfo(dvr.Row), drInput);
                        ret.Append(x, true);
                        inputValue = x.ValueToDbNull;
                    }

                    // record if any inputValues were non-null
                    if (!DBHelper.IsNull(inputValue)) hasNonNullParameter = true;
                    // store the lookup values
                    string fieldName = dvr["FieldName"].ToString();
                    ret.Values.Add(fieldName, inputValue);
                    bool isCustom = DBHelper.ToBoolean(dvr["IsCustom"]);
                    if (isCustom)
                        fieldName = "Value";
                    // build up a sql WHERE clause
                    string clause = null;
                    ImportCenterComparisonType.TYPE type = (ImportCenterComparisonType.TYPE)DBHelper.ToInt32(dvr["KeyComparisonTypeID"]);
                    switch (type)
                    {
                        case ImportCenterComparisonType.TYPE.Equals:
                        case ImportCenterComparisonType.TYPE.NotEquals:
                        {
                            string value = DBHelper.QuoteStr(inputValue);
                            string op = "="; // default value
                            if (value.Equals("NULL", StringComparison.CurrentCultureIgnoreCase))
                                op = type == ImportCenterComparisonType.TYPE.Equals ? "IS" : "IS NOT";
                            else if (type == ImportCenterComparisonType.TYPE.NotEquals)
                                op = "<>";
                            clause += string.Format("[{0}] {1} {2}", fieldName, op, DBHelper.QuoteStr(inputValue));
                            break;
                        }
                        case ImportCenterComparisonType.TYPE.PlusMinus1Day:
                            clause += string.Format("abs(datediff(day, [{0}], {1})) <= 1", fieldName, DBHelper.QuoteStr(inputValue));
                            break;
                        case ImportCenterComparisonType.TYPE.PlusMinus1Hour:
                            clause += string.Format("abs(datediff(hour, [{0}], {1})) <= 1", fieldName, DBHelper.QuoteStr(inputValue));
                            break;
                        case ImportCenterComparisonType.TYPE.IN:
                        case ImportCenterComparisonType.TYPE.NotIN:
                        {
                            string op = type == ImportCenterComparisonType.TYPE.IN ? "IN" : "NOT IN";
                            string[] values = inputValue.ToString().Split(',');
                            for (int i = 0; i < values.Length; i++)
                            {
                                values[i] = DBHelper.QuoteStr(values[i]);
                            }
                            clause += string.Format("[{0}] {1} ({2})", fieldName, op, string.Join(",", values));
                            break;
                        }
                    }
                    if (isCustom)
                    {
                        clause = string.Format("ID IN (SELECT RecordID FROM tblObjectCustomData WHERE ObjectFieldID={0} AND {1})"
                            , DBHelper.ToInt32(dvr["ObjectFieldID"]), clause);
                    }
                    where.Add(clause);
                }
            }
            // prevent "looking" up an un-editable (reference) object that is already inactive/deleted (this way they could be undeleted via the Import Center)
            if (!ret.Editable)
            {
                if (ret.HasActiveField && !dvImportFields.Cast<DataRowView>().Any(rv => rv.Row.Field<string>("FieldName") == "Active"))
                where.Add("Active=1");
                else if (ret.HasDeleteDateUTCField && !dvImportFields.Cast<DataRowView>().Any(rv => rv.Row.Field<string>("FieldName") == "DeleteDateUTC"))
                    where.Add("DeleteDateUTC IS NULL");
            }
            // retrieve the record (if it exists)
            DataTable dtTarget = db.GetPopulatedDataTable("SELECT * FROM {0} {1}", (object)ret.SqlTargetName, (object)(where.Get.Length > 0 ? where.Get : " WHERE 1 = 0"));
            switch (dtTarget.Rows.Count)
            {
                case 0:
                    if (editable)
                    {
                        try
                        {
                            ret.DataRow = AddStaticDefaultValues(ret.ObjectID, dtTarget.NewRow());
                        }
                        catch (Exception ex)
                        {
                            ret.Warnings.Add("At least 1 Default Value was not properly applied: " + ex.Message);
                        }
                    }
                    else
                    {
                        ret.Success = false;
                        string fieldName = parentFieldID == null ? ret.ObjectName : _dtImportFields.Select(string.Format("ID={0}", parentFieldID))[0]["Name"].ToString();
                        if (parentFieldAllowNull)
                        {
                            // only set a warning if at least 1 non-null criteria was specified and no record was found (otherwise assume the user wasn't expecting to find a record)
                            if (hasNonNullParameter)
                                ret.Warnings.Add("Matching " + fieldName + " record not found.  Optional data might have been lost in the import");
                        }
                        else
                            ret.Errors.Add("Matching " + fieldName + " record not found.  Check the data and your Import Definition Lookup Fields and try again");
                    }
                    break;
                case 1:
                    ret.DataRow = dtTarget.Rows[0];
                    ret.Action = editable ? "Update" : "Retrieve";
                    break;
                default:
                    ret.Success = false;
                    ret.Errors.Add("Multiple matching " + ret.ObjectName + " records found.  Check the data and your Import Definition Lookup Fields and try again");
                    break;
            }

            return ret;
        }

        private DataTable _dtDefaults = null;
        private DataRow AddStaticDefaultValues(int objectID, DataRow dr)
        {
            if (_dtDefaults == null)
            {
                _dtDefaults = _db.GetPopulatedDataTable("SELECT ObjectID, ID, FieldName, ObjectFieldTypeID, DefaultValue FROM tblObjectField WHERE DefaultValue IS NOT NULL");
            }
            foreach (DataRow drIF in _dtDefaults.Select(string.Format("ObjectID={0}", objectID)))
            {
                string fieldName = drIF["FieldName"].ToString();
                if (DBHelper.IsNull(dr[fieldName]))
                    dr[fieldName] = ConvertToObjectFieldType(drIF["DefaultValue"].ToString().TrimWrappingChars('(', ')'), new ImportFieldInfo(drIF)).ValueToDbNull;
            }
            return dr;
        }
        private DataRow AddDynamicDefaultValues(int objectID, DataRow dr)
        {
            switch (objectID)
            {
                case (int)OBJECTS.Order:
                    if (!DBHelper.IsNull(dr["OriginID"]))
                    {
                        if (DBHelper.IsNull(dr["TicketTypeID"]))
                            dr["TicketTypeID"] = _db.QuerySingleValue("SELECT TicketTypeID FROM tblOrigin WHERE ID = {0}", dr["OriginID"]);
                        if (DBHelper.IsNull(dr["ProductID"]))
                            dr["ProductID"] = _db.QuerySingleValue("SELECT min(ProductID) FROM tblOriginProducts WHERE OriginID = {0}", dr["OriginID"]);
                        if (DBHelper.IsNull(dr["CustomerID"]))
                            dr["CustomerID"] = _db.QuerySingleValue("SELECT min(CustomerID) FROM tblOriginCustomers WHERE OriginID = {0}", dr["OriginID"]);
                        if (DBHelper.IsNull(dr["OriginUomID"]))
                            dr["OriginUomID"] = _db.QuerySingleValue("SELECT UomID FROM tblOrigin WHERE ID = {0}", dr["OriginID"]);
                        if (DBHelper.IsNull(dr["ProducerID"]))
                            dr["ProducerID"] = _db.QuerySingleValue("SELECT ProducerID FROM tblOrigin WHERE ID = {0}", dr["OriginID"]);
                        if (DBHelper.IsNull(dr["OperatorID"]))
                            dr["OperatorID"] = _db.QuerySingleValue("SELECT OperatorID FROM tblOrigin WHERE ID = {0}", dr["OriginID"]);
                        if (DBHelper.IsNull(dr["PumperID"]))
                            dr["PumperID"] = _db.QuerySingleValue("SELECT PumperID FROM tblOrigin WHERE ID = {0}", dr["OriginID"]);
                    }
                    if (!DBHelper.IsNull(dr["DestinationID"]))
                    {
                        if (DBHelper.IsNull(dr["DestUomID"]))
                            dr["DestUomID"] = _db.QuerySingleValue("SELECT UomID FROM tblDestination WHERE ID = {0}", dr["DestinationID"]);
                    }
                    if (DBHelper.IsNull(dr["PriorityID"]))
                        dr["PriorityID"] = 3;

                    int statusID = DBHelper.ToInt32(dr["StatusID"]);
                    if (DBHelper.IsNull(dr["PickupPrintStatusID"]))
                    {
                        if (statusID.IsIn((int)OrderStatus.STATUS.PickedUp, (int)OrderStatus.STATUS.Delivered, (int)OrderStatus.STATUS.Audited))
                            dr["PickupPrintStatusID"] = (int)PrintStatus.STATUS.Handwritten;
                        else
                            dr["PickupPrintStatusID"] = (int)PrintStatus.STATUS.NotFinalized;
                    }
                    if (DBHelper.IsNull(dr["DeliverPrintStatusID"]))
                    {
                        if (statusID.IsIn((int)OrderStatus.STATUS.Delivered, (int)OrderStatus.STATUS.Audited))
                            dr["DeliverPrintStatusID"] = (int)PrintStatus.STATUS.Handwritten;
                        else
                            dr["DeliverPrintStatusID"] = (int)PrintStatus.STATUS.NotFinalized;
                    }
                    if (DBHelper.IsNull(dr["DueDate"]))
                    {
                        if (!DBHelper.IsNull(dr["OrderDate"]))
                            dr["DueDate"] = DBHelper.ToDateTime(dr["OrderDate"]).Date;
                        else // default to TOMORROW
                            dr["DueDate"] = DateTime.Now.Date.AddDays(1);
                    }
                    if (DBHelper.IsNull(dr["StatusID"]))
                        dr["StatusID"] = (int)Models.OrderStatus.STATUS.Generated;
                    break;
                case (int)OBJECTS.Ticket:
                    if (!DBHelper.IsNull(dr["OrderID"]))
                    {
                        if (DBHelper.IsNull(dr["TicketTypeID"]))
                            dr["TicketTypeID"] = _db.QuerySingleValue("SELECT TicketTypeID FROM tblOrder WHERE ID = {0}", dr["OrderID"]) ?? DBNull.Value;
                        if (DBHelper.IsNull(dr["FromMobileApp"]))
                            dr["FromMobileApp"] = false;
                        if (DBHelper.IsNull(dr["NoDataCalc"]))
                            dr["NoDataCalc"] = false;
                    }
                    break;
                case (int)OBJECTS.Carrier:
                    if (DBHelper.IsNull(dr["IdNumber"]))
                    {
                        dr["IdNumber"] = _db.QuerySingleValue("SELECT isnull((SELECT max(IdNumber) FROM tblCarrier), 10000) + 1");
                    }
                    break;
                case (int)OBJECTS.Driver:
                    if (DBHelper.IsNull(dr["IdNumber"]))
                    {
                        dr["IdNumber"] = _db.QuerySingleValue("SELECT isnull((SELECT max(IdNumber) FROM tblDriver), 10000) + 1");
                    }
                    break;
                case (int)OBJECTS.Truck:
                    if (DBHelper.IsNull(dr["IdNumber"]))
                    {
                        dr["IdNumber"] = _db.QuerySingleValue("SELECT isnull((SELECT max(IdNumber) FROM tblTruck), 10000) + 1");
                    }
                    break;
                case (int)OBJECTS.Trailer:
                    if (DBHelper.IsNull(dr["IdNumber"]))
                    {
                        dr["IdNumber"] = _db.QuerySingleValue("SELECT isnull((SELECT max(IdNumber) FROM tblTrailer), 10000) + 1");
                    }
                    break;
            }
            // ensure any UID column is populated with a new GUID value
            if (dr.Table.Columns.Contains("UID") && DBHelper.IsNull(dr["UID"]))
                dr["UID"] = Guid.NewGuid().ToString();

            return dr;
        }

        private Dictionary<string, int> _stringMaxLengths = null;
        private DataRow TruncateStringValues(int objectID, DataRow dr, DataView dvImportFields)
        {
            const string MAXSTRINGLEN = "MaxStringLength";
            if (_stringMaxLengths == null)
            {
                _stringMaxLengths = new Dictionary<string, int>();
                foreach (DataRowView drv in dvImportFields)
                {
                    if (!DBHelper.IsNull(drv[MAXSTRINGLEN]) && drv[MAXSTRINGLEN].ToInt32() != -1)
                        _stringMaxLengths[drv["FieldName"].ToString().ToLower()] = DBHelper.ToInt32(drv[MAXSTRINGLEN]);
                }
            }
            foreach (DataColumn dc in dr.Table.Columns)
            {
                string colName = dc.ColumnName.ToLower();
                if (_stringMaxLengths.ContainsKey(colName))
                    dr[dc] = dr[dc].ToString().Truncate(_stringMaxLengths[colName]);
            }
            return dr;
        }

        private bool WriteConvertedValue(int objectFieldID, DataRow drTarget, string fieldName, RetrieveValueOutcome value)
        {
            bool ret = false;
            switch (objectFieldID)
            {
                case (int)ObjectField.TYPE.Order_OriginArriveTime:
                case (int)ObjectField.TYPE.Order_OriginDepartTime:
                case (int)ObjectField.TYPE.Order_PickupPrintDate:
                    if (!DBHelper.IsNull(value.Value) && drTarget.Table.Columns.Contains("OriginID") && !DBHelper.IsNull(drTarget["OriginID"]))
                    {
                        drTarget[fieldName + "UTC"] = Core.DateHelper.ToUTC(DBHelper.ToDateTime(value.Value), DispatchCrude_ConnFactory.QuerySingleValue("SELECT TimeZone + ' Standard Time' FROM viewOrigin WHERE ID = {0}", drTarget["OriginID"]).ToString());
                        ret = true;
                    }
                    break;
                case (int)ObjectField.TYPE.Order_DeliverPrintDate:
                case (int)ObjectField.TYPE.Order_DestArriveTime:
                case (int)ObjectField.TYPE.Order_DestDepartTime:
                    if (!DBHelper.IsNull(value.Value) && drTarget.Table.Columns.Contains("DestinationID") && !DBHelper.IsNull(drTarget["DestinationID"]))
                    {
                        drTarget[fieldName + "UTC"] = Core.DateHelper.ToUTC(DBHelper.ToDateTime(value.Value), DispatchCrude_ConnFactory.QuerySingleValue("SELECT TimeZone + ' Standard Time' FROM viewDestination WHERE ID = {0}", drTarget["DestinationID"]).ToString());
                        ret = true;
                    }
                    break;
            }
            return ret;
        }

        private ProcessOutcome UpdateTargetData(SSDB db, DataRow drInput, DataView dvImportFields, RetrieveRecordOutcome retrievalOutcome, int nestLevel)
        {
            ProcessOutcome ret = new ProcessOutcome(retrievalOutcome);

            DataRow drTarget = retrievalOutcome.DataRow;
            Dictionary<int, object> customData = new Dictionary<int, object>();
            foreach (DataRowView dvr in dvImportFields)
            {
                // don't update ReadOnly fields
                if (DBHelper.ToBoolean(dvr["ReadOnly"]))
                    continue;

                string fieldName = dvr["FieldName"].ToString();

                RetrieveValueOutcome value = null;
                // if the Retrieval Action already computed some Values, use them instead of recomputing them again
                if (retrievalOutcome.Values.ContainsKey(fieldName))
                    value = new RetrieveValueOutcome(retrievalOutcome.Values[fieldName] ?? DBNull.Value);
                // if this field has children, then recursively find the value to use for lookup
                else if (DBHelper.ToBoolean(dvr["HasChildren"]))
                    value = ProcessInputRow(db, drInput, DBHelper.ToInt32(dvr["ID"]), nestLevel + 1);
                // else just retrieve the value from the input data (directly)
                else
                    value = RetrieveInputValue(db, new ImportFieldInfo(dvr.Row), drInput);

                ret.Append(value, true);
                if (DBHelper.ToBoolean(dvr["IsCustom"]))
                    customData.Add(DBHelper.ToInt32(dvr["ObjectFieldID"]), value.ValueToDbNull);
                else if (drTarget.Table.Columns.Contains(fieldName))
                {
                    // don't save the change if no difference is detected
                    if (!SSDBSave.CompareField(drTarget.Table.Columns[fieldName].DataType, drTarget[fieldName], value.ValueToDbNull))
                        drTarget[fieldName] = value.ValueToDbNull;
                }
                else
                    WriteConvertedValue(DBHelper.ToInt32(dvr["ObjectFieldID"]), drTarget, fieldName, value);
            }
            try
            {
                drTarget = AddDynamicDefaultValues(retrievalOutcome.ObjectID, drTarget);
                drTarget = TruncateStringValues(retrievalOutcome.ObjectID, drTarget, dvImportFields);
                // save the base table changes
                SSDBSave.ValueChanges newValues = new SSDBSave.ValueChanges();
                ret.Value = SSDBSave.SaveDataRow(db, drTarget, retrievalOutcome.SqlTargetName, retrievalOutcome.IDFieldName, SSDBSave.UpdateMode.UpdateAllSourceValuesOnly, UserSupport.UserName, newValues);

                // evaluate the saved data, ensuring that all required fields were indeed set (if changed)
                if (ret.Action == "Add" || newValues.HasActualChanges)
                {
                    foreach (DataRow dr in db.GetPopulatedDataTable("SELECT FieldName FROM tblObjectField WHERE ObjectID={0} AND AllowNullID=0", (object)ret.ObjectID).Rows)
                    {
                        string fieldName = dr["FieldName"].ToString();
                        // if this is a new record and a required value is missing or NULL
                        if ((ret.Action == "Add" && (!newValues.ContainsKey(fieldName) || DBHelper.IsNull(newValues[fieldName]))) 
                            // or this is an updated record, and a required value was updated to NULL
                            || (newValues.ContainsKey(fieldName) && DBHelper.IsNull(newValues[fieldName])))
                        {
                            ret.Errors.Add(string.Format("{0}.{1} value cannot be null", retrievalOutcome.ObjectName, fieldName));
                        }
                    }
                }
            
                // save any CustomData (if any)
                if (customData.Count > 0)
                    SaveCustomData(db, DBHelper.ToInt32(drTarget["ID"]), customData);

                ret.FieldUpdateCount += (newValues.HasActualChanges ? newValues.Count : 0) + customData.Count;

                ret.Success = ret.Errors.Count == 0;
            }
            catch (Exception ex)
            {
                string error = ex.Message.Replace("The transaction ended in the trigger. The batch has been aborted.", "");
                bool handled = false;
                // Cannot insert the value NULL into column 'OriginUomID', table 'DispatchCrude.Dev.dbo.tblOrder'; column does not allow nulls. INSERT fails.\r\nThe statement has been terminated.
                Regex r = new Regex("^([^']+) '([^']+)', table '([^']+)'; column does not allow nulls\\..+");
                foreach (Match m in r.Matches(error))
                {
                    if (m.Success)
                    {
                        handled = true;
                        string errMsg = m.Groups[1].Value.ToString()
                            , errFieldName = m.Groups[2].Value.ToString();
                        if (errMsg.Contains("Cannot insert the value NULL"))
                        {
                            object errName = _db.QuerySingleValue("SELECT Name FROM tblObjectField WHERE ObjectID={0} AND FieldName LIKE {1}", ret.ObjectID, DBHelper.QuoteStr(errFieldName));
                            if (!DBHelper.IsNull(errName))
                            {
                                ret.Errors.Add(
                                    string.Format("Required field: [{0}].[{1}] not found.  Check your Import Center Mapping and input file and try again.", retrievalOutcome.ObjectName, errName));
                            }
                            else
                                ret.Errors.Add("Error adding new " + retrievalOutcome.ObjectName + " record: " + error);
                        }
                    }
                }
                if (!handled)
                    ret.Errors.Add(error);
                ret.Success = false;
            }

            return ret;
        }

        public int SaveCustomData(SSDB db, int recordID, Dictionary<int, object> customData)
        {
            int ret = 0;
            foreach (KeyValuePair<int, object> entry in customData)
            {
                string sql = string.Format(
                    "UPDATE tblObjectCustomData SET Value={0}, LastChangeDateUTC=getutcdate(), LastChangedByUser={3} WHERE ObjectFieldID={1} AND RecordID={2};"
                    + " IF (@@ROWCOUNT = 0) INSERT INTO tblObjectCustomData(ObjectFieldID, RecordID, Value, CreatedByUser) VALUES ({1}, {2}, {0}, {3})"
                    , DBHelper.QuoteStr(DBHelper.ToString(entry.Value, true))
                    , entry.Key
                    , recordID
                    , DBHelper.QuoteStr(UserSupport.UserName));
                ret += db.ExecuteSql(sql);
            }
            return ret;
        }
    }
    public class WhereClause
    {
        private string _whereClause = string.Empty;

        public string Add(string clause)
        {
            if (clause != null) _whereClause += string.Format("{0} {1}", _whereClause.Length == 0 ? "WHERE" : " AND", clause);
            return clause;
        }
        public string Get { get { return _whereClause; } }
    }
}