using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mobile.DispatchCrude.Models
{
    [Table("tblDriverEndorsementRestrictType", Schema="dbo")]
    public partial class tblDriverEndorsementRestrictType
    {
        public tblDriverEndorsementRestrictType()
        {
            this.tblDriverEndorseRestricts = new List<tblDriverEndorseRestrict>();
        }

        public int ID { get; set; }
        public Nullable<bool> IsRestriction { get; set; }
        public string Name { get; set; }
        public virtual ICollection<tblDriverEndorseRestrict> tblDriverEndorseRestricts { get; set; }
    }
}
