using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mobile.DispatchCrude.Models
{
    [Table("tblPumper", Schema="dbo")]
    public partial class tblPumper
    {
        public tblPumper()
        {
            this.tblOrders = new List<tblOrder>();
            this.tblOrigins = new List<tblOrigin>();
        }

        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int OperatorID { get; set; }
        public string ContactEmail { get; set; }
        public string ContactPhone { get; set; }
        public virtual tblOperator tblOperator { get; set; }
        public virtual ICollection<tblOrder> tblOrders { get; set; }
        public virtual ICollection<tblOrigin> tblOrigins { get; set; }
    }
}
