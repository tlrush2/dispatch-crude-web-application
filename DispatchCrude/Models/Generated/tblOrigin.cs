using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mobile.DispatchCrude.Models
{
    [Table("tblOrigin", Schema="dbo")]
    public partial class tblOrigin
    {
        public tblOrigin()
        {
            this.tblOrders = new List<tblOrder>();
            this.tblRoutes = new List<tblRoute>();
        }

        public int ID { get; set; }
        public int OriginTypeID { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public Nullable<int> StateID { get; set; }
        public string Zip { get; set; }
        public string wellAPI { get; set; }
        public string LAT { get; set; }
        public string LON { get; set; }
        public Nullable<int> OperatorID { get; set; }
        public Nullable<int> PumperID { get; set; }
        public string County { get; set; }
        public string LeaseName { get; set; }
        public string LeaseNum { get; set; }
        public int TicketTypeID { get; set; }
        public Nullable<int> RegionID { get; set; }
        public virtual tblOperator tblOperator { get; set; }
        public virtual ICollection<tblOrder> tblOrders { get; set; }
        public virtual tblOrigin tblOrigin1 { get; set; }
        public virtual tblOrigin tblOrigin2 { get; set; }
        public virtual tblOriginType tblOriginType { get; set; }
        public virtual tblPumper tblPumper { get; set; }
        public virtual tblRegion tblRegion { get; set; }
        public virtual tblState tblState { get; set; }
        public virtual tblTicketType tblTicketType { get; set; }
        public virtual ICollection<tblRoute> tblRoutes { get; set; }
    }
}
