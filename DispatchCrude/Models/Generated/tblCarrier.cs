using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mobile.DispatchCrude.Models
{
    [Table("tblCarrier", Schema="dbo")]
    public partial class tblCarrier
    {
        public tblCarrier()
        {
            this.tblOrders = new List<tblOrder>();
            this.tblDrivers = new List<tblDriver>();
            this.tblTrailers = new List<tblTrailer>();
            this.tblTrucks = new List<tblTruck>();
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public Nullable<int> StateID { get; set; }
        public string Zip { get; set; }
        public string ContactName { get; set; }
        public string ContactEmail { get; set; }
        public string ContactPhone { get; set; }
        public string Notes { get; set; }
        public decimal LoadRatePercent { get; set; }
        public decimal WaitRate { get; set; }
        public string FEINTaxID { get; set; }
        public Nullable<bool> W9OnFile { get; set; }
        public byte[] W9Attachment { get; set; }
        public Nullable<System.DateTime> InsCertRenewDate { get; set; }
        public byte[] InsCertAttachment { get; set; }
        public Nullable<System.DateTime> OwnOperAgmtRenewDate { get; set; }
        public byte[] OwnOperAgmtAttachment { get; set; }
        public string ACHInstructions { get; set; }
        public virtual ICollection<tblOrder> tblOrders { get; set; }
        public virtual tblState tblState { get; set; }
        public virtual ICollection<tblDriver> tblDrivers { get; set; }
        public virtual ICollection<tblTrailer> tblTrailers { get; set; }
        public virtual ICollection<tblTruck> tblTrucks { get; set; }
    }
}
