using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mobile.DispatchCrude.Models
{
    [Table("tblTankType", Schema="dbo")]
    public partial class tblTankType
    {
        public tblTankType()
        {
            this.tblOrderTickets = new List<tblOrderTicket>();
        }

        public int ID { get; set; }
        public short CapacityBarrels { get; set; }
        public short HeightFeet { get; set; }
        public Nullable<decimal> CapacityBarrelsActual { get; set; }
        public virtual ICollection<tblOrderTicket> tblOrderTickets { get; set; }
    }
}
