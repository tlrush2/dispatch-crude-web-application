using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mobile.DispatchCrude.Models
{
    [Table("tblRegion", Schema="dbo")]
    public partial class tblRegion
    {
        public tblRegion()
        {
            this.tblOrigins = new List<tblOrigin>();
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public virtual ICollection<tblOrigin> tblOrigins { get; set; }
    }
}
