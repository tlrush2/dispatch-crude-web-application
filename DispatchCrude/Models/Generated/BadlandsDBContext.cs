using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Mobile.DispatchCrude.Models.Mapping;

namespace Mobile.DispatchCrude.Models
{
    public partial class BadlandsDBContext : DbContext
    {
        static BadlandsDBContext()
        {
            Database.SetInitializer<BadlandsDBContext>(null);
        }

        public BadlandsDBContext()
            : base("Name=BadlandsDBContext")
        {
        }

        public DbSet<tblAccessorialRate> tblAccessorialRates { get; set; }
        public DbSet<tblCarrier> tblCarriers { get; set; }
        public DbSet<tblCustomer> tblCustomers { get; set; }
        public DbSet<tblCustomerRegionRate> tblCustomerRegionRates { get; set; }
        public DbSet<tblDestination> tblDestinations { get; set; }
        public DbSet<tblDestinationType> tblDestinationTypes { get; set; }
        public DbSet<tblDriver> tblDrivers { get; set; }
        public DbSet<tblDriverEndorsementRestrictType> tblDriverEndorsementRestrictTypes { get; set; }
        public DbSet<tblDriverEndorseRestrict> tblDriverEndorseRestricts { get; set; }
        public DbSet<tblOperator> tblOperators { get; set; }
        public DbSet<tblOrder> tblOrders { get; set; }
        public DbSet<tblOrderAccessorialRate> tblOrderAccessorialRates { get; set; }
        public DbSet<tblOrderReroute> tblOrderReroutes { get; set; }
        public DbSet<tblOrderStatu> tblOrderStatus { get; set; }
        public DbSet<tblOrderTicket> tblOrderTickets { get; set; }
        public DbSet<tblOrigin> tblOrigins { get; set; }
        public DbSet<tblOriginType> tblOriginTypes { get; set; }
        public DbSet<tblPriority> tblPriorities { get; set; }
        public DbSet<tblPumper> tblPumpers { get; set; }
        public DbSet<tblRegion> tblRegions { get; set; }
        public DbSet<tblRoute> tblRoutes { get; set; }
        public DbSet<tblSetting> tblSettings { get; set; }
        public DbSet<tblSettingType> tblSettingTypes { get; set; }
        public DbSet<tblState> tblStates { get; set; }
        public DbSet<tblTankType> tblTankTypes { get; set; }
        public DbSet<tblTicketType> tblTicketTypes { get; set; }
        public DbSet<tblTrailer> tblTrailers { get; set; }
        public DbSet<tblTrailerCompartment> tblTrailerCompartments { get; set; }
        public DbSet<tblTrailerType> tblTrailerTypes { get; set; }
        public DbSet<tblTruck> tblTrucks { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new tblAccessorialRateMap());
            modelBuilder.Configurations.Add(new tblCarrierMap());
            modelBuilder.Configurations.Add(new tblCustomerMap());
            modelBuilder.Configurations.Add(new tblCustomerRegionRateMap());
            modelBuilder.Configurations.Add(new DestinationMap());
            modelBuilder.Configurations.Add(new tblDestinationTypeMap());
            modelBuilder.Configurations.Add(new tblDriverMap());
            modelBuilder.Configurations.Add(new tblDriverEndorsementRestrictTypeMap());
            modelBuilder.Configurations.Add(new tblDriverEndorseRestrictMap());
            modelBuilder.Configurations.Add(new tblOperatorMap());
            modelBuilder.Configurations.Add(new tblOrderMap());
            modelBuilder.Configurations.Add(new tblOrderAccessorialRateMap());
            modelBuilder.Configurations.Add(new tblOrderRerouteMap());
            modelBuilder.Configurations.Add(new tblOrderStatuMap());
            modelBuilder.Configurations.Add(new tblOrderTicketMap());
            modelBuilder.Configurations.Add(new tblOriginMap());
            modelBuilder.Configurations.Add(new tblOriginTypeMap());
            modelBuilder.Configurations.Add(new tblPriorityMap());
            modelBuilder.Configurations.Add(new tblPumperMap());
            modelBuilder.Configurations.Add(new tblRegionMap());
            modelBuilder.Configurations.Add(new tblRouteMap());
            modelBuilder.Configurations.Add(new tblSettingMap());
            modelBuilder.Configurations.Add(new tblSettingTypeMap());
            modelBuilder.Configurations.Add(new tblStateMap());
            modelBuilder.Configurations.Add(new tblTankTypeMap());
            modelBuilder.Configurations.Add(new tblTicketTypeMap());
            modelBuilder.Configurations.Add(new tblTrailerMap());
            modelBuilder.Configurations.Add(new tblTrailerCompartmentMap());
            modelBuilder.Configurations.Add(new tblTrailerTypeMap());
            modelBuilder.Configurations.Add(new tblTruckMap());
        }
    }
}
