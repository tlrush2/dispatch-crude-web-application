using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mobile.DispatchCrude.Models
{
    [Table("tblOrderAccessorialRate", Schema="dbo")]
    public partial class tblOrderAccessorialRate
    {
        public int ID { get; set; }
        public int OrderID { get; set; }
        public int AccessorialRateID { get; set; }
        public decimal Rate { get; set; }
        public virtual tblAccessorialRate tblAccessorialRate { get; set; }
        public virtual tblOrder tblOrder { get; set; }
    }
}
