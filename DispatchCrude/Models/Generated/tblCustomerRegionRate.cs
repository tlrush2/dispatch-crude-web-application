using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mobile.DispatchCrude.Models
{
    [Table("tblCustomerRegionRate", Schema="dbo")]
    public partial class tblCustomerRegionRate
    {
        public int ID { get; set; }
        public int CustomerID { get; set; }
        public int RegionID { get; set; }
        public short StartMiles { get; set; }
        public short EndMiles { get; set; }
        public decimal Rate { get; set; }
    }
}
