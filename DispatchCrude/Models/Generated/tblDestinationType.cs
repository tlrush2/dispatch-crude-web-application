using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mobile.DispatchCrude.Models
{
    [Table("tblDestinationType", Schema="dbo")]
    public partial class tblDestinationType
    {
        public tblDestinationType()
        {
            this.tblDestinations = new List<tblDestination>();
        }

        public int ID { get; set; }
        public string DestinationType { get; set; }
        public bool BOLRequired { get; set; }
        public virtual ICollection<tblDestination> tblDestinations { get; set; }
    }
}
