using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mobile.DispatchCrude.Models
{
    [Table("tblSettingType", Schema="dbo")]
    public partial class tblSettingType
    {
        public tblSettingType()
        {
            this.tblSettings = new List<tblSetting>();
        }

        public byte ID { get; set; }
        public string Name { get; set; }
        public virtual ICollection<tblSetting> tblSettings { get; set; }
    }
}
