using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mobile.DispatchCrude.Models
{
    [Table("tblOrderReroute", Schema="dbo")]
    public partial class tblOrderReroute
    {
        public int ID { get; set; }
        public int OrderID { get; set; }
        public int PreviousDestinationID { get; set; }
        public string UserName { get; set; }
        public string Notes { get; set; }
        public Nullable<System.DateTime> RerouteDate { get; set; }
        public virtual tblOrder tblOrder { get; set; }
    }
}
