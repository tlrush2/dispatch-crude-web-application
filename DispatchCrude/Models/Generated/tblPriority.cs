using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mobile.DispatchCrude.Models
{
    [Table("tblPriority", Schema="dbo")]
    public partial class tblPriority
    {
        public tblPriority()
        {
            this.tblOrders = new List<tblOrder>();
        }

        public byte ID { get; set; }
        public string Name { get; set; }
        public byte PriorityNum { get; set; }
        public string ForeColor { get; set; }
        public string BackColor { get; set; }
        public virtual ICollection<tblOrder> tblOrders { get; set; }
    }
}
