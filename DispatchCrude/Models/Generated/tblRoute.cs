using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mobile.DispatchCrude.Models
{
    [Table("tblRoute", Schema="dbo")]
    public partial class tblRoute
    {
        public tblRoute()
        {
            this.tblOrders = new List<tblOrder>();
        }

        public int ID { get; set; }
        public int OriginID { get; set; }
        public int DestinationID { get; set; }
        public int RateMiles { get; set; }
        public Nullable<int> ActualMiles { get; set; }
        public virtual tblDestination tblDestination { get; set; }
        public virtual ICollection<tblOrder> tblOrders { get; set; }
        public virtual tblOrigin tblOrigin { get; set; }
    }
}
