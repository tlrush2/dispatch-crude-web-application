using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mobile.DispatchCrude.Models
{
    [Table("tblTrailerCompartment", Schema="dbo")]
    public partial class tblTrailerCompartment
    {
        public int ID { get; set; }
        public string CompartmentNumber { get; set; }
        public int TrailerID { get; set; }
        public int CapacityInBarrels { get; set; }
        public virtual tblTrailer tblTrailer { get; set; }
    }
}
