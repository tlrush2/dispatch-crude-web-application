using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mobile.DispatchCrude.Models
{
    [Table("tblTrailer", Schema="dbo")]
    public partial class tblTrailer
    {
        public tblTrailer()
        {
            this.tblDrivers = new List<tblDriver>();
            this.tblDrivers1 = new List<tblDriver>();
            this.tblOrders = new List<tblOrder>();
            this.tblOrders1 = new List<tblOrder>();
            this.tblTrailerCompartments = new List<tblTrailerCompartment>();
        }

        public int ID { get; set; }
        public int CarrierID { get; set; }
        public Nullable<int> TrailerTypeID { get; set; }
        public Nullable<System.DateTime> TrailerCertDate { get; set; }
        public string TrailerNumber { get; set; }
        public string DOTnumber { get; set; }
        public string VIN { get; set; }
        public string LicenseNumber { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public Nullable<int> Year { get; set; }
        public string Composition { get; set; }
        public Nullable<int> NumberOfAxels { get; set; }
        public Nullable<int> CompartmentCount { get; set; }
        public Nullable<int> CapacityInBarrels { get; set; }
        public Nullable<System.DateTime> TrailerLastInspection { get; set; }
        public virtual tblCarrier tblCarrier { get; set; }
        public virtual ICollection<tblDriver> tblDrivers { get; set; }
        public virtual ICollection<tblDriver> tblDrivers1 { get; set; }
        public virtual ICollection<tblOrder> tblOrders { get; set; }
        public virtual ICollection<tblOrder> tblOrders1 { get; set; }
        public virtual tblTrailerType tblTrailerType { get; set; }
        public virtual ICollection<tblTrailerCompartment> tblTrailerCompartments { get; set; }
    }
}
