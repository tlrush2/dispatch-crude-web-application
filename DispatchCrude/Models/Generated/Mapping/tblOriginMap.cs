using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Mobile.DispatchCrude.Models.Mapping
{
    public class tblOriginMap : EntityTypeConfiguration<tblOrigin>
    {
        public tblOriginMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(30);

            this.Property(t => t.Address)
                .HasMaxLength(50);

            this.Property(t => t.City)
                .HasMaxLength(30);

            this.Property(t => t.Zip)
                .HasMaxLength(50);

            this.Property(t => t.wellAPI)
                .HasMaxLength(20);

            this.Property(t => t.LAT)
                .HasMaxLength(20);

            this.Property(t => t.LON)
                .HasMaxLength(20);

            this.Property(t => t.County)
                .HasMaxLength(25);

            this.Property(t => t.LeaseName)
                .HasMaxLength(35);

            this.Property(t => t.LeaseNum)
                .HasMaxLength(30);


            // Relationships
            this.HasOptional(t => t.tblOperator)
                .WithMany(t => t.tblOrigins)
                .HasForeignKey(d => d.OperatorID);
            this.HasRequired(t => t.tblOrigin2)
                .WithOptional(t => t.tblOrigin1);
            this.HasRequired(t => t.tblOriginType)
                .WithMany(t => t.tblOrigins)
                .HasForeignKey(d => d.OriginTypeID);
            this.HasOptional(t => t.tblPumper)
                .WithMany(t => t.tblOrigins)
                .HasForeignKey(d => d.PumperID);
            this.HasOptional(t => t.tblRegion)
                .WithMany(t => t.tblOrigins)
                .HasForeignKey(d => d.RegionID);
            this.HasOptional(t => t.tblState)
                .WithMany(t => t.tblOrigins)
                .HasForeignKey(d => d.StateID);
            this.HasRequired(t => t.tblTicketType)
                .WithMany(t => t.tblOrigins)
                .HasForeignKey(d => d.TicketTypeID);

        }
    }
}
