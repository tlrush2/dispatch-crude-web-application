using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Mobile.DispatchCrude.Models.Mapping
{
    public class tblDriverEndorsementRestrictTypeMap : EntityTypeConfiguration<tblDriverEndorsementRestrictType>
    {
        public tblDriverEndorsementRestrictTypeMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(30);

        }
    }
}
