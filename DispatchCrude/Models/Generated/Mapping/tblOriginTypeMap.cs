using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Mobile.DispatchCrude.Models.Mapping
{
    public class tblOriginTypeMap : EntityTypeConfiguration<tblOriginType>
    {
        public tblOriginTypeMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.OriginType)
                .IsRequired()
                .HasMaxLength(25);

        }
    }
}
