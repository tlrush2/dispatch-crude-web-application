using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Mobile.DispatchCrude.Models.Mapping
{
    public class tblDriverEndorseRestrictMap : EntityTypeConfiguration<tblDriverEndorseRestrict>
    {
        public tblDriverEndorseRestrictMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.DriverID, t.DriverEndorseRestrictTypeID });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.DriverID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.DriverEndorseRestrictTypeID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);


            // Relationships
            this.HasRequired(t => t.tblDriverEndorsementRestrictType)
                .WithMany(t => t.tblDriverEndorseRestricts)
                .HasForeignKey(d => d.DriverEndorseRestrictTypeID);

        }
    }
}
