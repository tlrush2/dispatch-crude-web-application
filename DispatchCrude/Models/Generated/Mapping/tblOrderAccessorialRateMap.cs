using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Mobile.DispatchCrude.Models.Mapping
{
    public class tblOrderAccessorialRateMap : EntityTypeConfiguration<tblOrderAccessorialRate>
    {
        public tblOrderAccessorialRateMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties

            // Relationships
            this.HasRequired(t => t.tblAccessorialRate)
                .WithMany(t => t.tblOrderAccessorialRates)
                .HasForeignKey(d => d.AccessorialRateID);
            this.HasRequired(t => t.tblOrder)
                .WithMany(t => t.tblOrderAccessorialRates)
                .HasForeignKey(d => d.OrderID);

        }
    }
}
