using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Mobile.DispatchCrude.Models.Mapping
{
    public class tblOrderRerouteMap : EntityTypeConfiguration<tblOrderReroute>
    {
        public tblOrderRerouteMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.UserName)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.Notes)
                .HasMaxLength(255);


            // Relationships
            this.HasRequired(t => t.tblOrder)
                .WithMany(t => t.tblOrderReroutes)
                .HasForeignKey(d => d.OrderID);

        }
    }
}
