using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Mobile.DispatchCrude.Models.Mapping
{
    public class tblCustomerMap : EntityTypeConfiguration<tblCustomer>
    {
        public tblCustomerMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(25);

            this.Property(t => t.Address)
                .HasMaxLength(40);

            this.Property(t => t.City)
                .HasMaxLength(30);

            this.Property(t => t.Zip)
                .HasMaxLength(10);

            this.Property(t => t.ContactName)
                .HasMaxLength(40);

            this.Property(t => t.ContactEmail)
                .HasMaxLength(50);

            this.Property(t => t.ContactPhone)
                .HasMaxLength(15);


            // Relationships
            this.HasOptional(t => t.tblState)
                .WithMany(t => t.tblCustomers)
                .HasForeignKey(d => d.StateID);

        }
    }
}
