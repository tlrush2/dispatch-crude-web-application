using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Mobile.DispatchCrude.Models.Mapping
{
    public class tblStateMap : EntityTypeConfiguration<tblState>
    {
        public tblStateMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Abbreviation)
                .HasMaxLength(2);

            this.Property(t => t.FullName)
                .HasMaxLength(30);

        }
    }
}
