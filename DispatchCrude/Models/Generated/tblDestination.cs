using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mobile.DispatchCrude.Models
{
    [Table("tblDestination", Schema="dbo")]
    public partial class tblDestination
    {
        public tblDestination()
        {
            this.tblOrders = new List<tblOrder>();
            this.tblRoutes = new List<tblRoute>();
        }

        public int ID { get; set; }
        public int DestinationTypeID { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public Nullable<int> StateID { get; set; }
        public string Zip { get; set; }
        public string LAT { get; set; }
        public string LON { get; set; }
        public virtual ICollection<tblOrder> tblOrders { get; set; }
        public virtual tblDestinationType tblDestinationType { get; set; }
        public virtual tblState tblState { get; set; }
        public virtual ICollection<tblRoute> tblRoutes { get; set; }
    }
}
