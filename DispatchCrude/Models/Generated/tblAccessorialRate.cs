using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mobile.DispatchCrude.Models
{
    [Table("tblAccessorialRate", Schema="dbo")]
    public partial class tblAccessorialRate
    {
        public tblAccessorialRate()
        {
            this.tblOrderAccessorialRates = new List<tblOrderAccessorialRate>();
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public decimal FlatRate { get; set; }
        public bool IsSystem { get; set; }
        public virtual ICollection<tblOrderAccessorialRate> tblOrderAccessorialRates { get; set; }
    }
}
