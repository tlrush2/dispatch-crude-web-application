using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mobile.DispatchCrude.Models
{
    [Table("tblDriverEndorseRestrict", Schema="dbo")]
    public partial class tblDriverEndorseRestrict
    {
        public int ID { get; set; }
        public int DriverID { get; set; }
        public int DriverEndorseRestrictTypeID { get; set; }
        public string Notes { get; set; }
        public virtual tblDriverEndorsementRestrictType tblDriverEndorsementRestrictType { get; set; }
    }
}
