using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mobile.DispatchCrude.Models
{
    [Table("tblTicketType", Schema="dbo")]
    public partial class tblTicketType
    {
        public tblTicketType()
        {
            this.tblOrders = new List<tblOrder>();
            this.tblOrderTickets = new List<tblOrderTicket>();
            this.tblOrigins = new List<tblOrigin>();
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public virtual ICollection<tblOrder> tblOrders { get; set; }
        public virtual ICollection<tblOrderTicket> tblOrderTickets { get; set; }
        public virtual ICollection<tblOrigin> tblOrigins { get; set; }
    }
}
