﻿using System;
using DispatchCrude.Extensions;

namespace DispatchCrude.Site.TableMaint
{
    public partial class DriverUpdate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Print tab navigation html to page
            tabmenu.InnerHtml = NavigationHelper.PrintTabArray(NavigationHelper.TabSet_Resources, "Tab_Equipment").ToString();

            //Print button navigation html to page
            speedbuttons.InnerHtml = NavigationHelper.PrintButtonArray(NavigationHelper.ButtonSet_TabEquipment, "Button_AppHistory").ToString();
        }
    }
}