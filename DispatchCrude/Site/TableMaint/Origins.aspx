﻿<%@ Page Title="Locations" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Origins.aspx.cs" Inherits="DispatchCrude.Site.TableMaint.Origins" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="contentMain" ContentPlaceHolderID="MainContent" runat="server">
    <telerik:RadCodeBlock ID="radCodeBlock" runat="server">
        <style type="text/css">
            .productsMaxHeight {
                max-height: 105px;
            }
        </style>
    </telerik:RadCodeBlock>
    <script>
        $("#ctl00_ctl00_EntityCaption").html("Origins");
    </script>
    <div class="tabbable TabRepaint">
        <ul class="nav nav-tabs" id="tabmenu" runat="server">
            <!--Tabs will print here (see code behind)-->
        </ul>
        <div class="tab-content">
            <div class="tab-pane in active">

                <div id="speedbuttons" class="well speedButtonContainer" runat="server">
                    <!--Buttons will print here (see code behind)-->
                </div>

                <div id="DataEntryFormGrid" style="height:100%;">
                    <telerik:RadWindowManager ID="radWindowManager" runat="server" EnableShadow="true" />
                    <telerik:RadGrid ID="rgMain" runat="server" CellSpacing="0" GridLines="None" Height="800" CssClass="GridRepaint" EnableLinqExpressions="false"
                                     AutoGenerateColumns="False" ShowGroupPanel="False" AllowFilteringByColumn="true" AllowSorting="True" AllowPaging="True" EnableEmbeddedSkins="true" Skin="Vista" AlternatingItemStyle-BackColor="#dcf2fc"
                                     PageSize='<%# Settings.DefaultPageSize %>'
                                     DataSourceID="dsMain" OnItemCommand="grid_ItemCommand" OnItemDataBound="grid_ItemDataBound">
                        <ClientSettings AllowDragToGroup="True" AllowKeyboardNavigation="true" EnablePostBackOnRowClick="false">
                            <Scrolling AllowScroll="True" UseStaticHeaders="True" FrozenColumnsCount="1" />
                        </ClientSettings>
                        <GroupingSettings CaseSensitive="false" ShowUnGroupButton="true" />
                        <ExportSettings FileName="Origins" ExportOnlyData="true" IgnorePaging="true" HideStructureColumns="true" Excel-FileExtension="xls" Excel-Format="Biff" OpenInNewWindow="true" />
                        <MasterTableView DataKeyNames="ID" CommandItemDisplay="Top" EditMode="PopUp" CommandItemSettings-AddNewRecordText="Add New Origin">
                            <CommandItemTemplate>
                                <div style="padding: 5px;">
                                    <asp:LinkButton Style="vertical-align: middle" runat="server"
                                                    CommandName="InitInsert">
                                        <img style="border:0px" alt="" src="../../images/add.png" />
                                        Add New Origin
                                    </asp:LinkButton>

                                    <asp:LinkButton ID="cmdDeactivateStale" Style="vertical-align: middle" runat="server" CommandName="ImportExcel"
                                                    OnClick="cmdDeactivateStale_Click" Enabled="true" CssClass="pull-right NOAJAX">
                                        <img style="border:0px" alt="" src="../../images/wrench_orange.png" />
                                        Deactivate Stale
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="cmdExportExcel" Style="vertical-align: middle" runat="server"
                                                    CommandName="ExportToExcel" CssClass="pull-right NOAJAX">
                                        <img style="border:0px" alt="" src="../../images/exportToExcel.png" />
                                        Excel Export &nbsp;&nbsp;
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" Text="Refresh" CommandName="Rebind" CssClass="pull-right" runat="server">Refresh&nbsp;&nbsp;|&nbsp;&nbsp;</asp:LinkButton>
                                    <asp:Button runat="server" ID="buttonRefresh" CommandName="Rebind" CssClass="rgRefresh pull-right" Text="Refresh" Title="Refresh" />
                                </div>
                            </CommandItemTemplate>

                            <EditFormSettings UserControlName="CtrlOrigin.ascx" EditFormType="WebUserControl"
                                              CaptionDataField="Name" CaptionFormatString="Edit Origin: {0}">
                                <PopUpSettings Modal="true" />
                                <EditColumn UniqueName="EditColumn" />
                            </EditFormSettings>
                            <RowIndicatorColumn Visible="True">
                                <HeaderStyle Width="20px" />
                            </RowIndicatorColumn>
                            <ExpandCollapseColumn Visible="True">
                                <HeaderStyle Width="20px" />
                            </ExpandCollapseColumn>
                            <Columns>
                                <telerik:GridTemplateColumn UniqueName="ActionColumn" HeaderStyle-Width="100px" AllowFiltering="false" AllowSorting="false">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" ID="btnEdit" CssClass="btn btn-xs btn-default shiny" CommandName="Edit" Text="Edit" ImageUrl="~/images/edit.png" />
                                        <asp:ImageButton runat="server" ID="btnDelete" CssClass="btn btn-xs btn-default shiny" CommandName="Delete" Text="Delete" ConfirmText="Are you sure?" ImageUrl="~/images/delete.png" />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>

                                <telerik:GridCheckBoxColumn DataField="Active" HeaderText="Active?" SortExpression="Active" UniqueName="Active" FilterControlWidth="50%"
                                                            ReadOnly="true" HeaderStyle-Width="70px" />

                                <telerik:GridBoundColumn DataField="OriginType"
                                                         HeaderText="Type" SortExpression="OriginType" FilterControlWidth="70%" UniqueName="OriginType">
                                    <HeaderStyle Width="100px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Name" HeaderText="Name"
                                                         SortExpression="Name" FilterControlWidth="70%" UniqueName="Name" MaxLength="50">
                                    <HeaderStyle Width="300px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridCheckBoxColumn DataField="H2S" UniqueName="H2S" HeaderText="H2S" SortExpression="H2S" DefaultInsertValue="false"
                                                            HeaderStyle-Width="60px" FilterControlWidth="60%" />

                                <telerik:GridHyperLinkColumn DataTextField="TankCount" UniqueName="TankCount" HeaderText="Tanks" HeaderStyle-Width="80px"
                                                             DataTextFormatString="{0} tank(s)" FilterControlWidth="60%" SortExpression="TankCount" DataType="System.Int64"
                                                             DataNavigateUrlFields="ID" DataNavigateUrlFormatString="/OriginTanks?OriginID={0}" />

                                <telerik:GridBoundColumn DataField="Station" HeaderText="Station" SortExpression="Station"
                                                         FilterControlWidth="70%" UniqueName="Station" MaxLength="15" ItemStyle-Width="150px">
                                    <HeaderStyle Width="100px" />
                                </telerik:GridBoundColumn>

                                <telerik:GridBoundColumn DataField="Region"
                                                         HeaderText="Region" SortExpression="Region" FilterControlWidth="70%" UniqueName="Region">
                                    <HeaderStyle Width="150px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Terminal"
                                                         HeaderText="Terminal" SortExpression="Terminal" FilterControlWidth="70%" UniqueName="Terminal">
                                    <HeaderStyle Width="150px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Address" HeaderText="Address"
                                                         MaxLength="50" SortExpression="Address" FilterControlWidth="70%" UniqueName="Address" ItemStyle-Width="150px">
                                    <HeaderStyle Width="150px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="City" HeaderText="City" FilterControlWidth="70%" UniqueName="City"
                                                         MaxLength="30" SortExpression="City" ItemStyle-Width="150px">
                                    <HeaderStyle Width="100px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="StateAbbrev"
                                                         HeaderText="State" SortExpression="StateAbbrev" FilterControlWidth="50%" UniqueName="StateID">
                                    <HeaderStyle Width="60px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Zip" HeaderText="Zip"
                                                         FilterControlWidth="70%" UniqueName="Zip" MaxLength="10" ItemStyle-Width="150px">
                                    <HeaderStyle Width="100px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="TimeZone"
                                                         HeaderText="Time Zone" SortExpression="TimeZone" FilterControlWidth="70%" UniqueName="TimeZone">
                                    <HeaderStyle Width="125px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridCheckBoxColumn DataField="UseDST" HeaderText="Use DST?" SortExpression="UseDST" UniqueName="UseDST" FilterControlWidth="50%"
                                                            DefaultInsertValue="True" HeaderStyle-Width="70px" />
                                <telerik:GridBoundColumn DataField="Products" HeaderText="Products" SortExpression="Products" UniqueName="Products"
                                                         FilterControlWidth="70%" HeaderStyle-Width="100px" ItemStyle-CssClass="showEllipsis">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn UniqueName="Uom" DataField="Uom" HeaderText="UOM" SortExpression="Uom"
                                                         FilterControlWidth="70%" DataType="System.String">
                                    <HeaderStyle Width="90px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Customers" ItemStyle-CssClass="showEllipsis"
                                                         HeaderText="Shippers" SortExpression="Customers" FilterControlWidth="70%" UniqueName="Customers">
                                    <HeaderStyle Width="200px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="County"
                                                         HeaderText="County" SortExpression="County" FilterControlWidth="70%" UniqueName="County">
                                    <HeaderStyle Width="150px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Producer"
                                                         HeaderText="Producer" SortExpression="Producer" FilterControlWidth="70%" UniqueName="Producer">
                                    <HeaderStyle Width="250px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FieldName"
                                                         HeaderText="FieldName" SortExpression="FieldName" FilterControlWidth="70%" UniqueName="FieldName">
                                    <HeaderStyle Width="200px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridDateTimeColumn DataField="SpudDate" HeaderText="Spud Date" SortExpression="SpudDate" FilterControlWidth="70%" UniqueName="SpudDate" PickerType="DatePicker" DataFormatString="{0:d}">
                                    <HeaderStyle Width="90px" />
                                </telerik:GridDateTimeColumn>
                                <telerik:GridBoundColumn DataField="TotalDepth"
                                                         HeaderText="Total Depth (TD)" SortExpression="TotalDepth" FilterControlWidth="70%" UniqueName="TotalDepth" DataType="System.Int32">
                                    <HeaderStyle Width="95px" />
                                    <ItemStyle Width="150px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="LeaseName"
                                                         HeaderText="Lease Name" SortExpression="LeaseName" FilterControlWidth="70%" UniqueName="LeaseName">
                                    <HeaderStyle Width="300px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="LeaseNum"
                                                         HeaderText="Lease #" SortExpression="LeaseNum" FilterControlWidth="70%" UniqueName="LeaseNum">
                                    <HeaderStyle Width="100px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridNumericColumn DataField="TaxRate" EditFormColumnIndex="4" HeaderText="Tax Rate (%)" SortExpression="TaxRate"
                                                           FilterControlWidth="70%" UniqueName="TaxRate" NumericType="Percent">
                                    <HeaderStyle Width="90px" HorizontalAlign="Right" />
                                    <ItemStyle HorizontalAlign="Right" Width="140px" />
                                </telerik:GridNumericColumn>
                                <telerik:GridBoundColumn DataField="Operator"
                                                         HeaderText="Operator" SortExpression="OperatorID" FilterControlWidth="70%" UniqueName="OperatorID">
                                    <HeaderStyle Width="250px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Pumper"
                                                         HeaderText="Pumper" SortExpression="PumperID" FilterControlWidth="70%" UniqueName="PumperID">
                                    <HeaderStyle Width="140px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="TicketType" HeaderText="Ticket Type"
                                                         FilterControlWidth="70%" UniqueName="TicketTypeID" SortExpression="TicketType">
                                    <HeaderStyle Width="100px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="LegalDescription" HeaderText="Legal Desc/LSD"
                                                         MaxLength="35" SortExpression="LegalDescription" FilterControlWidth="70%" UniqueName="LegalDescription">
                                    <HeaderStyle Width="200px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="NDICFileNum" HeaderText="NDIC File#"
                                                         MaxLength="10" SortExpression="NDICFileNum" FilterControlWidth="70%" UniqueName="NDICFileNum">
                                    <HeaderStyle Width="100px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="CTBNum" HeaderText="CTB #"
                                                         MaxLength="10" SortExpression="CTBNum" FilterControlWidth="70%" UniqueName="CTBNum">
                                    <HeaderStyle Width="100px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="CA" HeaderText="CA"
                                                         MaxLength="25" SortExpression="CA" FilterControlWidth="70%" UniqueName="CA">
                                    <HeaderStyle Width="90px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="NDM" HeaderText="BLM Info"
                                                         MaxLength="50" SortExpression="NDM" FilterControlWidth="70%" UniqueName="NDM">
                                    <HeaderStyle Width="200px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="wellAPI" HeaderText="Well API"
                                                         MaxLength="20" SortExpression="wellAPI" FilterControlWidth="70%" UniqueName="wellAPI">
                                    <HeaderStyle Width="120px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn DataField="OriginLatLon" UniqueName="OriginLatLon" HeaderText="Lat/Lon" HeaderStyle-Width="180px" FilterControlWidth="75%"
                                                            SortExpression="OriginLatLon" ItemStyle-Wrap="true">
                                    <ItemTemplate>
                                        <asp:Hyperlink ID="lblOriginLL" runat="server" width="100%"
                                                       Text='<%# string.Format("{0}", Converter.IfGPSNullOrEmpty(Eval("LAT"),Eval("LON"), ", ")) %>'
                                                       NavigateUrl='<%# string.Format("http://maps.google.com/maps?z=12&t=k&q=loc:{0}", Converter.IfGPSNullOrEmpty(Eval("LAT"),Eval("LON"), ",")) %>'
                                                       Target="_blank" />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridNumericColumn DataField="GeoFenceRadiusMeters" HeaderText="GeoFence Meters"
                                                           DataType="System.Int16" SortExpression="GeoFenceRadiusMeters" FilterControlWidth="70%" UniqueName="GeoFenceRadiusMeters"
                                                           HeaderStyle-Width="100px" />
                                <telerik:GridNumericColumn DataField="SulfurContent" HeaderText="Sulfur Content"
                                                           SortExpression="SulfurContent" FilterControlWidth="70%" UniqueName="SulfurContent"
                                                           DecimalDigits="4" HeaderStyle-Width="100px" />
                                <telerik:GridBoundColumn DataField="PrivateRoadMiles" HeaderText="PRM"
                                                         FilterControlWidth="70%" UniqueName="PrivateRoadMiles" SortExpression="PrivateRoadMiles" EditFormColumnIndex="2">
                                    <HeaderStyle Width="100px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="CreateDate" UniqueName="CreateDate" SortExpression="CreateDate" DataType="System.DateTime" DataFormatString="{0:M/d/yy}"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Create Date" HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="CreatedByUser" UniqueName="CreatedByUser" SortExpression="CreatedByUser"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Created By" HeaderStyle-Width="90px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="LastChangeDate" UniqueName="LastChangeDate" SortExpression="LastChangeDate" DataType="System.DateTime" DataFormatString="{0:M/d/yy HH:mm}"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Last Update Date" HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="LastChangedByUser" UniqueName="LastChangedByUser" SortExpression="LastChangedByUser"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Last Updated By" HeaderStyle-Width="95px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="DeleteDate" UniqueName="DeleteDate" SortExpression="DeleteDate" DataType="System.DateTime" DataFormatString="{0:M/d/yy HH:mm}"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Delete Date" HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="DeletedByUser" UniqueName="DeletedByUser" SortExpression="DeletedByUser"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Deleted By" HeaderStyle-Width="90px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />

                            </Columns>
                            <EditFormSettings ColumnNumber="6">
                                <EditColumn ButtonType="ImageButton" CancelImageUrl="~/images/cancel.png"
                                            UpdateImageUrl="~/images/apply.png" InsertImageUrl="~/images/apply.png" />
                            </EditFormSettings>
                            <PagerStyle AlwaysVisible="True" />
                        </MasterTableView>
                        <HeaderStyle Wrap="False" />
                        <PagerStyle AlwaysVisible="True" />
                        <FilterMenu EnableImageSprites="False">
                        </FilterMenu>
                    </telerik:RadGrid>
                </div>
                <blc:RadGridDBCtrl ID="dbcMain" runat="server" ControlID="rgMain" UpdateTableName="tblOrigin" />
                <blac:DBDataSource ID="dsMain" runat="server" SelectCommand="
                                   SELECT O.*, Products=P.Products_CSV, P.ProductID_CSV, Customers=C.Customers_CSV, C.CustomerID_CSV, OriginLatLon=O.Lat + ',' + O.Lon
                                   FROM viewOrigin O
                                   LEFT JOIN tblOriginProductsCSV P ON P.OriginID=O.ID
                                   LEFT JOIN tblOriginCustomersCSV C ON C.OriginID=O.ID
                                   ORDER BY Name" />
                <script type="text/javascript">
                    $(document).ready(function () {
                        $('#FormMain').attr('action', '/Site/TableMaint/Origins.aspx');
                    });
                </script>
            </div>
        </div>
    </div>
</asp:Content>
