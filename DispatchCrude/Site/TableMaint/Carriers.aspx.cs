﻿using System;
using System.Collections;
using Telerik.Web.UI;
using DispatchCrude.App_Code;
using AlonsIT;
using DispatchCrude.Core;
using DispatchCrude.Extensions;

namespace DispatchCrude.Site.TableMaint
{
    public partial class Carriers : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ConfigureAjax(!Settings.SettingsID.DisableAJAX.AsBool());

            //Print tab navigation html to page
            tabmenu.InnerHtml = NavigationHelper.PrintTabArray(NavigationHelper.TabSet_Resources, "Tab_Carriers").ToString();

            //Print button navigation html to page
            speedbuttons.InnerHtml = NavigationHelper.PrintButtonArray(NavigationHelper.ButtonSet_TabCarriers, "Button_Carriers").ToString();

            // filter to the specified Carrier (if any)
            if (Request.QueryString["ID"] != null)
            {
                using (SSDB ssdb = new SSDB())
                {
                    string carrierName = ssdb.QuerySingleValue("SELECT Name FROM tblCarrier WHERE ID={0}", (object)Request.QueryString["ID"]).ToString();
                    App_Code.RadGridHelper.SetFilter(rgMain.MasterTableView, "Name", carrierName, GridKnownFunction.EqualTo);
                }
            }
        }

        private void ConfigureAjax(bool enabled = true)
        {
            RadAjaxManager.GetCurrent(this.Page).EnableAJAX = enabled;
            if (enabled)
            {
                RadAjaxHelper.AddAjaxSetting(this.Page, rgMain, rgMain);
            }
        }

        protected void grid_ItemCommand(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.InitInsertCommandName) //"Add new" button clicked
            {
                // cancel the default operation
                e.Canceled = true;

                //Prepare an IDictionary with the predefined values
                Hashtable newValues = new Hashtable();
                using (SSDB db = new SSDB())
                {
                    newValues["IDNumber"] = db.QuerySingleValue("SELECT max(isnull(CASE WHEN ISNUMERIC(IDNumber) = 1 THEN IDNumber ELSE 0 END, 1000)) + 1 FROM tblCarrier").ToString();
                }
                //Insert the item and rebind
                e.Item.OwnerTableView.InsertItem(newValues);
            }
        }
    }
}