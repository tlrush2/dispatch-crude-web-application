﻿<%@ Page Title="Resources" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Trailers.aspx.cs" Inherits="DispatchCrude.Site.TableMaint.Trailers" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script>
        $("#ctl00_ctl00_EntityCaption").html("Trailers");
    </script>
    <div class="tabbable TabRepaint">
        <ul class="nav nav-tabs" id="tabmenu" runat="server">
            <!--Tabs will print here (see code behind)-->
        </ul>
        <div class="tab-content">
            <div class="tab-pane in active">

                <div id="speedbuttons" class="well speedButtonContainer" runat="server">
                    <!--Buttons will print here (see code behind)-->
                </div>

                <telerik:GridNumericColumnEditor ID="editorCompVol" runat="server" NumericTextBox-DataType="System.Decimal" NumericTextBox-EnabledStyle-HorizontalAlign="Right"
                                                 NumericTextBox-MinValue="0" NumericTextBox-NumberFormat-DecimalDigits="1" />
                <div id="DataEntryFormGrid" style="height:100%;">
                    <asp:ValidationSummary ID="validationSummaryMain" runat="server" DisplayMode="BulletList" Enabled="true" EnableClientScript="true" CssClass="NullValidator" />
                    <telerik:RadGrid ID="rgMain" runat="server" CellSpacing="0" GridLines="None" Height="800" CssClass="GridRepaint" EnableLinqExpressions="false"
                                     AutoGenerateColumns="False" ShowGroupPanel="False" AllowFilteringByColumn="true" AllowSorting="True" AllowPaging="True" EnableEmbeddedSkins="true" Skin="Vista" AlternatingItemStyle-BackColor="#dcf2fc"
                                     PageSize='<%# Settings.DefaultPageSize %>'
                                     DataSourceID="dsMain" OnItemCommand="grid_ItemCommand" OnItemDataBound="grid_ItemDataBound">
                        <ClientSettings AllowDragToGroup="True">
                            <Scrolling AllowScroll="True" UseStaticHeaders="True" FrozenColumnsCount="2" />
                        </ClientSettings>
                        <GroupingSettings CaseSensitive="false" ShowUnGroupButton="true" />
                        <ExportSettings FileName="Trailers" ExportOnlyData="true" IgnorePaging="true" HideStructureColumns="true" Excel-FileExtension="xls" Excel-Format="Biff" OpenInNewWindow="true" />
                        <MasterTableView DataKeyNames="ID" CommandItemDisplay="Top" CommandItemSettings-AddNewRecordText="Add New Trailer">
                            <CommandItemTemplate>
                                <div style="padding: 5px;">
                                    <asp:LinkButton Style="vertical-align: middle" runat="server"
                                                    CommandName="InitInsert">
                                        <img style="border:0px" alt="" src="../../images/add.png" />
                                        Add New Trailer
                                    </asp:LinkButton>

                                    <asp:LinkButton ID="cmdExportQR" Style="vertical-align: middle" runat="server" CommandName="ExportQR"
                                                    OnClick="cmdExportQR_Click" Enabled="true" CssClass="pull-right NOAJAX">
                                        <i class="fa fa-qrcode"></i>
                                        Download QR Codes
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="cmdExportExcel" Style="vertical-align: middle" runat="server"
                                                    CommandName="ExportToExcel" CssClass="pull-right NOAJAX">
                                        <img style="border:0px" alt="" src="../../images/exportToExcel.png" />
                                        Excel Export &nbsp;&nbsp;
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" Text="Refresh" CommandName="Rebind" CssClass="pull-right" runat="server">Refresh&nbsp;&nbsp;|&nbsp;&nbsp;</asp:LinkButton>
                                    <asp:Button runat="server" ID="buttonRefresh" CommandName="Rebind" CssClass="rgRefresh pull-right" Text="Refresh" Title="Refresh" />
                                </div>
                            </CommandItemTemplate>
                            <RowIndicatorColumn Visible="True">
                                <HeaderStyle Width="20px" />
                            </RowIndicatorColumn>
                            <ExpandCollapseColumn Visible="True">
                                <HeaderStyle Width="20px" />
                            </ExpandCollapseColumn>
                            <Columns>
                                <telerik:GridTemplateColumn UniqueName="ActionColumn" HeaderStyle-Width="100px" AllowFiltering="false" AllowSorting="false">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" ID="btnEdit" CssClass="btn btn-xs btn-default shiny" CommandName="Edit" Text="Edit" ImageUrl="~/images/edit.png" />
                                        <asp:ImageButton runat="server" ID="btnDelete" CssClass="btn btn-xs btn-default shiny" CommandName="Delete" Text="Delete" ConfirmText="Are you sure?" ImageUrl="~/images/delete.png" />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>

                                <telerik:GridCheckBoxColumn DataField="Active" UniqueName="Active" HeaderText="Active?" SortExpression="Active"
                                                            ReadOnly="true" HeaderStyle-Width="80px" FilterControlWidth="60%" />

                                <telerik:GridBoundColumn DataField="CarrierType" UniqueName="CarrierType" HeaderText="Carrier Type" ReadOnly="true"
                                                         SortExpression="CarrierType" FilterControlWidth="70%" HeaderStyle-Width="120px" />
                                <telerik:GridTemplateColumn EditFormColumnIndex="0" DataField="Carrier" HeaderText="Carrier" GroupByExpression="Carrier GROUP BY Carrier"
                                                            SortExpression="Carrier" FilterControlWidth="70%" UniqueName="CarrierID" DefaultInsertValue="0">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="ddlCarrier" runat="server" DataSourceID="dsCarrier" DataValueField="ID" DataTextField="Name"
                                                          SelectedValue='<%# Bind("CarrierID") %>' Width="200px" CssClass="btn-xs" />
                                        <asp:RequiredFieldValidator ID="rfvCarrier" runat="server" ControlToValidate="ddlCarrier"
                                                                    Text="*" ErrorMessage="Carrier is required" CssClass="NullValidator" InitialValue="0" />
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblCarrier" runat="server" Text='<%# Eval("Carrier") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="250px" />
                                </telerik:GridTemplateColumn>

                                <telerik:GridTemplateColumn UniqueName="QRCode" AllowFiltering="false" HeaderText="QR" HeaderStyle-HorizontalAlign="Center">
                                    <HeaderStyle Width="30px" />
                                    <ItemTemplate>
                                        <a href="../../Trailers/QRList/<%# Eval(" ID") %>"><i class="fa fa-qrcode"></i></a>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn EditFormColumnIndex="0" DataField="IDNumber" GroupByExpression="IDNumber GROUP BY IDNumber"
                                                            HeaderText="Trailer #" SortExpression="IDNumber" FilterControlWidth="70%" UniqueName="IDNumber">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtIDNumber" runat="server" Text='<%# Bind("IDNumber") %>' MaxLength="10" Width="200px" />
                                        <asp:RequiredFieldValidator ID="rfvIDNumber" runat="server" ControlToValidate="txtIDNumber"
                                                                    Text="*" ErrorMessage="Trailer Number is required" CssClass="NullValidator" />
                                        <blac:UniqueValidator ID="uvIDNumber" runat="server"
                                                              ControlToValidate="txtIDNumber" TableName="tblTrailer" DataField="IDNumber"
                                                              OtherUniqueNames_CSV="CarrierID" OnDataBinding="uvIDNumber_DataBinding" OnOnGetOtherCellValue="uvIDNumber_GetOtherFieldValue"
                                                              ErrorMessage="Trailer ID Number is already in use" Text="*" CssClass="NullValidator" />
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblIDNumber" runat="server" Text='<%# Eval("IDNumber") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="90px" />
                                </telerik:GridTemplateColumn>

                                <telerik:GridTemplateColumn EditFormColumnIndex="0" DataField="Terminal" HeaderText="Terminal" SortExpression="Terminal"
                                                            FilterControlWidth="70%" UniqueName="TerminalID">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="ddlTerminal" runat="server" DataSourceID="dsTerminal" DataTextField="Name" DataValueField="ID"
                                                          SelectedValue='<%# Bind("TerminalID") %>' CssClass="btn-xs">
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblTerminal" runat="server" Text='<%# Eval("Terminal") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="180px" />
                                </telerik:GridTemplateColumn>

                                <telerik:GridTemplateColumn EditFormColumnIndex="0" DataField="TrailerType" GroupByExpression="TrailerType GROUP BY TrailerType"
                                                            HeaderText="Type" SortExpression="TrailerType" FilterControlWidth="70%" UniqueName="TrailerTypeID">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="ddlTrailerType" runat="server" DataSourceID="dsTrailerType" DataTextField="Name" DataValueField="ID"
                                                          SelectedValue='<%# Bind("TrailerTypeID") %>' Width="200px" CssClass="btn-xs">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rfvTrailerType" runat="server" ControlToValidate="ddlTrailerType"
                                                                    Text="*" ErrorMessage="Trailer Type must be provided" CssClass="NullValidator" />
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblTrailerType" runat="server" Text='<%# Eval("TrailerType") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="100px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn UniqueName="DOTNumber" EditFormColumnIndex="0" DataField="DOTNumber" HeaderText="DOT #"
                                                         MaxLength="20" SortExpression="DOTNumber" FilterControlWidth="70%">
                                    <HeaderStyle Width="100px" />
                                    <ItemStyle Width="200px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn UniqueName="VIN" EditFormColumnIndex="0" DataField="VIN" HeaderText="VIN" ItemStyle-Width="200px"
                                                         MaxLength="20" SortExpression="VIN" FilterControlWidth="70%">
                                    <HeaderStyle Width="140px" />
                                    <ItemStyle Width="200px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn UniqueName="LicenseNumber" EditFormColumnIndex="1" DataField="LicenseNumber"
                                                         HeaderText="License #" MaxLength="20" SortExpression="LicenseNumber" FilterControlWidth="70%">
                                    <HeaderStyle Width="100px" />
                                    <ItemStyle Width="120px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn UniqueName="Make" EditFormColumnIndex="1" DataField="Make" HeaderText="Make"
                                                         MaxLength="20" SortExpression="Make" FilterControlWidth="70%">
                                    <HeaderStyle Width="150px" />
                                    <ItemStyle Width="120px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn UniqueName="Model" EditFormColumnIndex="1" DataField="Model" HeaderText="Model"
                                                         MaxLength="20" SortExpression="Model" FilterControlWidth="70%">
                                    <HeaderStyle Width="90px" />
                                    <ItemStyle Width="120px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn UniqueName="Year" EditFormColumnIndex="1" DataField="Year" DataType="System.Int32"
                                                         HeaderText="Year" MaxLength="4" SortExpression="Year" FilterControlWidth="70%">
                                    <HeaderStyle Width="90px" />
                                    <ItemStyle Width="120px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn UniqueName="Composition" EditFormColumnIndex="1" DataField="Composition"
                                                         HeaderText="Composition" MaxLength="50" SortExpression="Composition" FilterControlWidth="70%">
                                    <HeaderStyle Width="150px" />
                                    <ItemStyle Width="120px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn UniqueName="AxleCount" EditFormColumnIndex="1" DataField="AxleCount" DataType="System.Int32"
                                                         HeaderText="# Axles" SortExpression="AxleCount" FilterControlWidth="70%">
                                    <HeaderStyle Width="90px" />
                                    <ItemStyle Width="120px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn DataField="TireSize" EditFormColumnIndex="2" GroupByExpression="TireSize GROUP BY TireSize"
                                                            HeaderText="Tire Size" SortExpression="TireSize" FilterControlWidth="70%" UniqueName="TireSize">
                                    <EditItemTemplate>
                                        <telerik:RadComboBox ID="rcboTireSize" runat="server" AllowCustomText="true" Filter="StartsWith" ShowDropDownOnTextboxClick="true"
                                                             DataSourceID="dsTireSize" DataTextField="TireSize" DataValueField="TireSize" Text='<%# Bind("TireSize") %>' MaxLength="15"
                                                             Width="150px" />
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblTireSize" runat="server" Text='<%# Eval("TireSize") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="85px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn UniqueName="MeterType" DataField="MeterType" HeaderText="Meter Type" FilterControlWidth="70%"
                                                            EditFormColumnIndex="3" HeaderStyle-Width="100">
                                    <EditItemTemplate>
                                        <telerik:RadComboBox ID="rcboMeterType" runat="server" AllowCustomText="true" Filter="StartsWith" ShowDropDownOnTextboxClick="true"
                                                             DataSourceID="dsMeterType" DataTextField="MeterType" DataValueField="MeterType" Text='<%# Bind("MeterType") %>' MaxLength="25"
                                                             Width="100px" />
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblMeterType" runat="server" Text='<%# Eval("MeterType") %>' />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>

                                <telerik:GridTemplateColumn UniqueName="Uom" DataField="Uom" GroupByExpression="Uom GROUP BY Uom" HeaderText="UOM" SortExpression="Uom"
                                                            FilterControlWidth="70%" EditFormColumnIndex="3" HeaderStyle-Width="80px">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="ddlUom" runat="server" Width="100px"
                                                          DataSourceID="dsUom" DataValueField="ID" DataTextField="Name" SelectedValue='<%# Bind("UomID") %>' />
                                        <asp:RequiredFieldValidator ID="rfvUom" runat="server" ControlToValidate="ddlUom"
                                                                    Text="*" ErrorMessage="UOM value must be specified" CssClass="NullValidator" />
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblUom" runat="server" Text='<%# Eval("Uom") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="75px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridNumericColumn DataField="Compartment1Units" UniqueName="Compartment1Units" SortExpression="Compartment1Units"
                                                           HeaderText="Comp #1 Vol" EditFormColumnIndex="3" FilterControlWidth="70%" HeaderStyle-Width="0px" Display="false"
                                                           ColumnEditorID="editorCompVol" ItemStyle-Width="100px" />
                                <telerik:GridNumericColumn DataField="Compartment2Units" UniqueName="Compartment2Units" SortExpression="Compartment2Units"
                                                           HeaderText="Comp #2 Vol" EditFormColumnIndex="3" FilterControlWidth="70%" HeaderStyle-Width="0px" Display="false"
                                                           ColumnEditorID="editorCompVol" ItemStyle-Width="100px" />
                                <telerik:GridNumericColumn DataField="Compartment3Units" UniqueName="Compartment3Units" SortExpression="Compartment3Units"
                                                           HeaderText="Comp #3 Vol" EditFormColumnIndex="3" FilterControlWidth="70%" HeaderStyle-Width="0px" Display="false"
                                                           ColumnEditorID="editorCompVol" ItemStyle-Width="100px" />
                                <telerik:GridNumericColumn DataField="Compartment4Units" UniqueName="Compartment4Units" SortExpression="Compartment4Units"
                                                           HeaderText="Comp #4 Vol" EditFormColumnIndex="3" FilterControlWidth="70%" HeaderStyle-Width="0px" Display="false"
                                                           ColumnEditorID="editorCompVol" ItemStyle-Width="100px" />
                                <telerik:GridNumericColumn DataField="Compartment5Units" UniqueName="Compartment5Units" SortExpression="Compartment25nits"
                                                           HeaderText="Comp #5 Vol" EditFormColumnIndex="3" FilterControlWidth="70%" HeaderStyle-Width="0px" Display="false"
                                                           ColumnEditorID="editorCompVol" ItemStyle-Width="100px" />
                                <telerik:GridBoundColumn EditFormColumnIndex="3" DataField="CompartmentCount" DataType="System.Int32" ReadOnly="true"
                                                         HeaderText="# of Compartments" SortExpression="Compartment #" FilterControlWidth="70%" UniqueName="CompartmentCount">
                                    <HeaderStyle Width="110px" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="TotalCapacityUnits" UniqueName="TotalCapacityUnits" DataType="System.Decimal"
                                                         SortExpression="TotalCapacityUnits" HeaderText="Total Capacity"
                                                         ReadOnly="true" FilterControlWidth="70%">
                                    <HeaderStyle Width="100px" />
                                    <ItemStyle HorizontalAlign="Right" />
                                </telerik:GridBoundColumn>

                                <telerik:GridNumericColumn EditFormColumnIndex="2" DataField="PurchasePrice" DataType="System.Decimal" NumericType="Currency"
                                                           HeaderText="Purchase $$" SortExpression="PurchasePrice" FilterControlWidth="70%" UniqueName="PurchasePrice"
                                                           ItemStyle-HorizontalAlign="Right">
                                    <HeaderStyle Width="100px" />
                                    <ItemStyle Width="150px" />
                                </telerik:GridNumericColumn>
                                <telerik:GridBoundColumn EditFormColumnIndex="2" DataField="GarageCity" UniqueName="GarageCity" SortExpression="GarageCity"
                                                         HeaderText="Garage City" HeaderStyle-Width="100px" ItemStyle-Width="150px" FilterControlWidth="70%" />
                                <telerik:GridDropDownColumn EditFormColumnIndex="2" DataField="GarageStateID" UniqueName="GarageStateID"
                                                            DataSourceID="dsGarageState" ListValueField="ID" ListTextField="Abbreviation"
                                                            HeaderText="Garage State" HeaderStyle-Width="90px" ItemStyle-Width="150px" FilterControlWidth="70%" />

                                <telerik:GridBoundColumn EditFormColumnIndex="2" DataField="CIDNumber" UniqueName="CIDNumber" SortExpression="CIDNumber"
                                                         HeaderText="C ID #" HeaderStyle-Width="90px" ItemStyle-Width="150px" FilterControlWidth="70%"
                                                         MaxLength="10" EditFormHeaderTextFormat="Carrier ID#" />
                                <telerik:GridBoundColumn EditFormColumnIndex="2" DataField="SIDNumber" UniqueName="SIDNumber" SortExpression="SIDNumber"
                                                         HeaderText="S ID #" HeaderStyle-Width="90px" ItemStyle-Width="150px"
                                                         FilterControlWidth="70%" MaxLength="10" EditFormHeaderTextFormat="Shipper ID#" />
                                <%--
                                <telerik:GridTemplateColumn EditFormColumnIndex="4" DataField="QRCode" GroupByExpression="QRCode GROUP BY QRCode"
                                                            HeaderText="QR Code" SortExpression="QRCode" FilterControlWidth="70%" UniqueName="QRCode">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtQRCode" runat="server" Text='<%# Bind("QRCode") %>' MaxLength="10" Width="200px" />
                                        <blac:UniqueValidator ID="uvQRCode" runat="server"
                                                              ControlToValidate="txtQRCode" TableName="tblTrailer" DataField="QRCode"
                                                              OtherUniqueNames_CSV="CarrierID" OnDataBinding="uvQRCode_DataBinding" OnOnGetOtherCellValue="uvQRCode_GetOtherFieldValue"
                                                              ErrorMessage="QRCode is already in use for this carrier" Text="*" CssClass="NullValidator" />
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblQRCode" runat="server" Text='<%# Eval("QRCode") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Width="120px" />
                                </telerik:GridTemplateColumn>
                                --%>

                                <telerik:GridTemplateColumn DataField="OwnerInfo" EditFormColumnIndex="4"
                                                            HeaderText="Owner Info" SortExpression="OwnerInfo" FilterControlWidth="70%" UniqueName="OwnerInfo" Groupable="false">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtOwnerInfo" runat="server" Text='<%# Bind("OwnerInfo") %>' MaxLength="255" TextMode="MultiLine" Height="82px" width="250px" />
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblOwnerInfo" runat="server" Text='<%# Eval("OwnerInfo") %>' />
                                    </ItemTemplate>
                                    <ItemStyle Width="200px" />
                                    <HeaderStyle Width="300px" />
                                </telerik:GridTemplateColumn>

                                <telerik:GridBoundColumn DataField="CreateDate" UniqueName="CreateDate" SortExpression="CreateDate" DataType="System.DateTime" DataFormatString="{0:M/d/yy}"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Create Date" HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="CreatedByUser" UniqueName="CreatedByUser" SortExpression="CreatedByUser"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Created By" HeaderStyle-Width="90px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="LastChangeDate" UniqueName="LastChangeDate" SortExpression="LastChangeDate" DataType="System.DateTime" DataFormatString="{0:M/d/yy HH:mm}"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Last Update Date" HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="LastChangedByUser" UniqueName="LastChangedByUser" SortExpression="LastChangedByUser"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Last Updated By" HeaderStyle-Width="95px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="DeleteDate" UniqueName="DeleteDate" SortExpression="DeleteDate" DataType="System.DateTime" DataFormatString="{0:M/d/yy HH:mm}"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Delete Date" HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="DeletedByUser" UniqueName="DeletedByUser" SortExpression="DeletedByUser"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Deleted By" HeaderStyle-Width="90px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />

                            </Columns>
                            <EditFormSettings ColumnNumber="5">
                                <EditColumn FilterControlAltText="Filter EditCommandColumn column" ButtonType="ImageButton" CancelImageUrl="~/images/cancel.png"
                                            UpdateImageUrl="~/images/apply.png" InsertImageUrl="~/images/apply.png" />
                            </EditFormSettings>
                            <PagerStyle AlwaysVisible="True" />
                        </MasterTableView>
                        <HeaderStyle Wrap="False" />
                        <PagerStyle AlwaysVisible="True" />
                        <FilterMenu EnableImageSprites="False">
                        </FilterMenu>
                    </telerik:RadGrid>
                </div>
                <%--This control will hide the carrier column when data for only 1 carrier is being displayed--%>
                <blac:HandleRadGridDefaultValues ID="hideCarrierColumn" runat="server" ControlID="rgMain">
                    <DefaultParameters>
                        <blac:HasAllChoiceProfileParameter Name="CarrierID" DbType="Int32" DefaultValue="0"
                                                           CaptionEntity="" CaptionTableName="tblCarrier" CaptionFieldName="Name" />
                    </DefaultParameters>
                </blac:HandleRadGridDefaultValues>
                <blac:DBDataSource ID="dsMain" runat="server"
                                   SelectCommand="SELECT ID, CarrierType, CarrierID, Carrier, IdNumber, TerminalID, Terminal, TrailerType, TrailerTypeID, DOTNumber, VIN, LicenseNumber, Make, Model, Year, Composition, AxleCount, TireSize, MeterType, Uom, UomID, Compartment1Units, Compartment2Units, Compartment3Units, Compartment4Units, Compartment5Units, CompartmentCount, TotalCapacityUnits, PurchasePrice, GarageCity, GarageStateID, CIDNumber, SIDNumber, OwnerInfo, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser FROM viewTrailer WHERE (@CarrierID=-1 OR CarrierID=@CarrierID) ORDER BY Carrier, IDNumber">
                    <SelectParameters>
                        <asp:ProfileParameter Name="CarrierID" PropertyName="CarrierID" ConvertEmptyStringToNull="false" DbType="String" DefaultValue="0" />
                    </SelectParameters>
                </blac:DBDataSource>
                <blc:RadGridDBCtrl ID="dbcMain" runat="server" ControlID="rgMain" UpdateTableName="tblTrailer" />
                <blac:DBDataSource ID="dsCarrier" runat="server" SelectCommand="SELECT ID, Name FROM dbo.tblCarrier WHERE DeleteDateUTC IS NULL UNION SELECT 0, '(Select Carrier)' ORDER BY Name" />
                <blac:DBDataSource ID="dsTrailerType" runat="server" SelectCommand="SELECT ID, Name FROM dbo.tblTrailerType UNION SELECT NULL, '(Select Type)' ORDER BY Name" />
                <blac:DBDataSource ID="dsState" runat="server" SelectCommand="SELECT ID, Abbreviation FROM dbo.tblState UNION SELECT NULL, '(Select State)' ORDER BY Abbreviation" />
                <blac:DBDataSource ID="dsGarageState" runat="server" SelectCommand="SELECT ID, Abbreviation FROM dbo.tblState UNION SELECT NULL, '(N/A)' ORDER BY Abbreviation" />
                <blac:DBDataSource ID="dsUom" runat="server" SelectCommand="SELECT ID, Name FROM tblUom UNION SELECT NULL, '(Select)' ORDER BY Name" />
                <blac:DBDataSource ID="dsTireSize" runat="server" SelectCommand="SELECT DISTINCT TireSize FROM tblTrailer WHERE TireSize IS NOT NULL ORDER BY TireSize" />
                <blac:DBDataSource ID="dsMeterType" runat="server" SelectCommand="SELECT DISTINCT MeterType FROM tblTrailer WHERE MeterType IS NOT NULL ORDER BY MeterType" />
                <blac:DBDataSource ID="dsTerminal" runat="server" SelectCommand="SELECT ID, Name FROM tblTerminal WHERE DeleteDateUTC IS NULL UNION SELECT NULL, '(All Terminals)' ORDER BY Name" />
            </div>
        </div>
    </div>
</asp:Content>