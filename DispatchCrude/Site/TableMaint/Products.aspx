﻿<%@ Page Title="Locations" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Products.aspx.cs"
    Inherits="DispatchCrude.Site.TableMaint.Products" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script>
        $("#ctl00_ctl00_EntityCaption").html("Products");
    </script>
    <div class="tabbable TabRepaint">
        <ul class="nav nav-tabs" id="tabmenu" runat="server">
            <!--Tabs will print here (see code behind)-->
        </ul>
        <div class="tab-content">
            <div class="tab-pane in active">

                <div id="speedbuttons" class="well speedButtonContainer" runat="server">
                    <!--Buttons will print here (see code behind)-->
                </div>
                <div id="DataEntryFormGrid" style="height:100%;">
                    <asp:ValidationSummary ID="validationSummaryMain" runat="server" DisplayMode="BulletList" Enabled="true" EnableClientScript="true" CssClass="NullValidator" />
                    <telerik:RadGrid ID="rgMain" runat="server" CellSpacing="0" GridLines="None" Height="800" CssClass="GridRepaint" EnableLinqExpressions="false"
                                     AutoGenerateColumns="False" ShowGroupPanel="false" AllowFilteringByColumn="true" AllowSorting="True" AllowPaging="True" EnableEmbeddedSkins="true" Skin="Vista" AlternatingItemStyle-BackColor="#dcf2fc"
                                     PageSize='<%# Settings.DefaultPageSize %>'
                                     OnItemCreated="grid_ItemCreated">
                        <ClientSettings AllowDragToGroup="True">
                            <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                        </ClientSettings>
                        <GroupingSettings CaseSensitive="false" ShowUnGroupButton="true" />
                        <ExportSettings FileName="Products" ExportOnlyData="true" IgnorePaging="true" HideStructureColumns="true" Excel-FileExtension="xls" Excel-Format="Biff" OpenInNewWindow="true" />
                        <MasterTableView DataKeyNames="ID" CommandItemDisplay="Top" CommandItemSettings-AddNewRecordText="Add New Product">
                            <CommandItemSettings ShowExportToExcelButton="true" />
                            <RowIndicatorColumn Visible="True">
                                <HeaderStyle Width="20px" />
                            </RowIndicatorColumn>
                            <ExpandCollapseColumn Visible="True">
                                <HeaderStyle Width="20px" />
                            </ExpandCollapseColumn>
                            <Columns>
                                <telerik:GridTemplateColumn UniqueName="ActionColumn" HeaderStyle-Width="75px" AllowFiltering="false" AllowSorting="false">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" ID="btnEdit" CssClass="btn btn-xs btn-default shiny" CommandName="Edit" Text="Edit" ImageUrl="~/images/edit.png" />
                                        <asp:ImageButton runat="server" ID="btnDelete" CssClass="btn btn-xs btn-default shiny" CommandName="Delete" Text="Delete" ConfirmText="Are you sure?" ImageUrl="~/images/delete.png" />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="ID" DataType="System.Int32" ReadOnly="True" UniqueName="ID" Visible="False" />

                                <telerik:GridTemplateColumn DataField="Name" HeaderText="Legal Name" SortExpression="Name" FilterControlWidth="70%" UniqueName="Name"
                                                            GroupByExpression="Name GROUP BY Name" EditFormColumnIndex="0">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtName" runat="server" Text='<%# Bind("Name") %>' MaxLength="100" Width="175px" />
                                        <asp:RequiredFieldValidator ID="rfvName" runat="server" Text="*" ErrorMessage="Name is required" ControlToValidate="txtName"
                                                                    InitialValue="0" CssClass="NullValidator" />
                                        <blac:UniqueValidator ID="uvName" runat="server" Text="*" ErrorMessage="Name is not unique"
                                                              ControlToValidate="txtName" TableName="tblProduct" DataField="Name" CssClass="NullValidator" />
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblName" runat="server" Text='<%# Eval("Name") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="250px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn DataField="ShortName" HeaderText="Friendly Name" SortExpression="ShortName" FilterControlWidth="70%"
                                                            UniqueName="ShortName" GroupByExpression="ShortName GROUP BY ShortName" EditFormColumnIndex="1">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtShortName" runat="server" Text='<%# Bind("ShortName") %>' MaxLength="25" />
                                        <asp:RequiredFieldValidator ID="rfvShortName" runat="server" Text="*" ErrorMessage="Short Name is required" ControlToValidate="txtShortName"
                                                                    CssClass="NullValidator" />
                                        <blac:UniqueValidator ID="uvShortName" runat="server" Text="*" ErrorMessage="Short Name is not unique"
                                                              ControlToValidate="txtShortName" TableName="tblProduct" DataField="ShortName" CssClass="NullValidator" />
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblShortName" runat="server" Text='<%# Eval("ShortName") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="175px" />
                                </telerik:GridTemplateColumn>

                                <telerik:GridTemplateColumn DataField="ProductGroup" HeaderText="Product Group" EditFormColumnIndex="2"
                                                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="130px" ForceExtractValue="Always"
                                                            SortExpression="ProductGroup" FilterControlWidth="70%" UniqueName="ProductGroupID">
                                    <ItemTemplate>
                                        <asp:Label ID="lblProductGroup" runat="server" Text='<%# Eval("ProductGroup") %>' />
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <telerik:RadComboBox ID="rcbProductGroup" runat="server" DataSourceID="dsProductGroup" DataTextField="Name"
                                                             DataValueField="ID" SelectedValue='<%# Bind("ProductGroupID") %>'
                                                             EmptyMessage="(Select)" />
                                        <asp:RequiredFieldValidator ID="rfvProductGroup" runat="server" ControlToValidate="rcbProductGroup"
                                                                    CssClass="NullValidator" ErrorMessage="Product Group value is required" Text="*" InitialValue="" />
                                    </EditItemTemplate>
                                </telerik:GridTemplateColumn>

                                <telerik:GridBoundColumn DataField="CreateDate" UniqueName="CreateDate" SortExpression="CreateDate" DataType="System.DateTime" DataFormatString="{0:M/d/yy}"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Create Date" HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="CreatedByUser" UniqueName="CreatedByUser" SortExpression="CreatedByUser"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Created By" HeaderStyle-Width="80px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="LastChangeDate" UniqueName="LastChangeDate" SortExpression="LastChangeDate" DataType="System.DateTime" DataFormatString="{0:M/d/yy HH:mm}"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Last Update Date" HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="LastChangedByUser" UniqueName="LastChangedByUser" SortExpression="LastChangedByUser"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Last Updated By" HeaderStyle-Width="95px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />

                            </Columns>
                            <EditFormSettings ColumnNumber="3">
                                <EditColumn FilterControlAltText="Filter EditCommandColumn column" ButtonType="ImageButton" CancelImageUrl="~/images/cancel.png"
                                            UpdateImageUrl="~/images/apply.png" InsertImageUrl="~/images/apply.png" />
                            </EditFormSettings>
                            <PagerStyle AlwaysVisible="True" />
                        </MasterTableView>
                        <HeaderStyle Wrap="False" />
                        <PagerStyle AlwaysVisible="True" />
                        <FilterMenu EnableImageSprites="False">
                        </FilterMenu>
                    </telerik:RadGrid>
                </div>
                <blc:RadGridDBCtrl ID="dbcMain" runat="server"
                                   ControlID="rgMain"
                                   SelectCommand="SELECT ID, Name, ShortName, ProductGroup, ProductGroupID, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser FROM viewProduct ORDER BY ShortName"
                                   UpdateTableName="tblProduct" />
                <blac:DBDataSource ID="dsProductGroup" runat="server" SelectCommand="SELECT ID, Name FROM dbo.tblProductGroup ORDER BY Name" />
            </div>
        </div>
    </div>
</asp:Content>