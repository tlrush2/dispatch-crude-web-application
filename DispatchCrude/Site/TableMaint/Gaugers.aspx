﻿<%@ Page Title="Resources" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Gaugers.aspx.cs" Inherits="DispatchCrude.Site.TableMaint.Gaugers" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="mainContent" ContentPlaceHolderID="MainContent" runat="server">
    <script>
        $("#ctl00_ctl00_EntityCaption").html("Gaugers");
    </script>
    <div class="tabbable TabRepaint">
        <ul class="nav nav-tabs" id="tabmenu" runat="server">
            <!--Tabs will print here (see code behind)-->
        </ul>
        <div class="tab-content">
            <div class="tab-pane in active">

                <div id="DataEntryFormGrid" style="height:100%;">
                    <asp:ValidationSummary ID="validationSummaryMain" runat="server" DisplayMode="BulletList" Enabled="true" EnableClientScript="true" CssClass="NullValidator" />
                    <telerik:RadGrid ID="rgMain" runat="server" CellSpacing="0" GridLines="None" Height="800" CssClass="GridRepaint" AutoGenerateColumns="False"
                                     ShowGroupPanel="False" AllowFilteringByColumn="true" AllowSorting="True" AllowPaging="True" EnableLinqExpressions="false" EnableEmbeddedSkins="true" Skin="Vista" AlternatingItemStyle-BackColor="#dcf2fc"
                                     PageSize='<%# Settings.DefaultPageSize %>'
                                     DataSourceID="dsMain" OnItemDataBound="grid_ItemDataBound" OnItemCommand="grid_ItemCommand">
                        <ClientSettings AllowDragToGroup="True" Resizing-AllowColumnResize="true" Resizing-AllowResizeToFit="true">
                            <Scrolling AllowScroll="True" UseStaticHeaders="True" FrozenColumnsCount="2" />
                        </ClientSettings>
                        <GroupingSettings CaseSensitive="false" ShowUnGroupButton="true" />
                        <ExportSettings FileName="Gaugers" ExportOnlyData="true" IgnorePaging="true" HideStructureColumns="true" Excel-FileExtension="xls" Excel-Format="Biff" OpenInNewWindow="true" />
                        <MasterTableView DataKeyNames="ID" CommandItemDisplay="Top" CommandItemSettings-AddNewRecordText="Add New Gauger">
                            <CommandItemSettings ShowExportToExcelButton="true" />
                            <RowIndicatorColumn Visible="True">
                                <HeaderStyle Width="20px" />
                            </RowIndicatorColumn>
                            <ExpandCollapseColumn Visible="True">
                                <HeaderStyle Width="20px" />
                            </ExpandCollapseColumn>
                            <Columns>
                                <telerik:GridTemplateColumn UniqueName="ActionColumn" HeaderStyle-Width="100px" AllowFiltering="false" AllowSorting="false">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" ID="btnEdit" CssClass="btn btn-xs btn-default shiny" CommandName="Edit" Text="Edit" ImageUrl="~/images/edit.png" />
                                        <asp:ImageButton runat="server" ID="btnDelete" CssClass="btn btn-xs btn-default shiny" CommandName="Delete" Text="Delete" ConfirmText="Are you sure?" ImageUrl="~/images/delete.png" />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>

                                <telerik:GridCheckBoxColumn DataField="Active" UniqueName="Active" HeaderText="Active?" SortExpression="Active"
                                                            ReadOnly="true" HeaderStyle-Width="80px" FilterControlWidth="60%" />

                                <telerik:GridTemplateColumn DataField="FirstName" HeaderText="First Name" SortExpression="FirstName" FilterControlWidth="70%"
                                                            UniqueName="FirstName" GroupByExpression="FirstName GROUP BY FirstName">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtFirstName" runat="server" Text='<%# Bind("FirstName") %>' Width="200px" />
                                        <asp:RequiredFieldValidator ID="rfvFirstName" runat="server" ControlToValidate="txtFirstName"
                                                                    Text="*" ErrorMessage="First Name is required" CssClass="NullValidator" />
                                        <asp:CustomValidator ID="cvFirstName_Unique" runat="server"
                                                             ControlToValidate="txtFirstName" OnServerValidate="ServerValidate_Unique"
                                                             Text="*" ErrorMessage="Gauger First | Last Name is already in use." CssClass="NullValidator" />
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblFirstName" runat="server" Text='<%# Eval("FirstName") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="100px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn DataField="LastName" HeaderText="Last Name" SortExpression="LastName" FilterControlWidth="70%"
                                                            UniqueName="LastName" GroupByExpression="LastName GROUP BY LastName">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtLastName" runat="server" Text='<%# Bind("LastName") %>' Width="200px" />
                                        <asp:RequiredFieldValidator ID="rfvLastName" runat="server" ControlToValidate="txtLastName"
                                                                    Text="*" ErrorMessage="Last Name is required" CssClass="NullValidator" />
                                        <asp:CustomValidator ID="cvLastName_Unique" runat="server"
                                                             ControlToValidate="txtLastName" OnServerValidate="ServerValidate_Unique"
                                                             Text="*" CssClass="NullValidator" />
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblLastName" runat="server" Text='<%# Eval("LastName") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="100px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="UserNames" UniqueName="UserNames" ReadOnly="true" FilterControlWidth="80%"
                                                         HeaderText="UserName(s)" HeaderStyle-Width="150px" />
                                <telerik:GridTemplateColumn DataField="IdNumber" HeaderText="Gauger ID" SortExpression="IdNumber" FilterControlWidth="60%" UniqueName="IdNumber"
                                                            GroupByExpression="IdNumber GROUP BY IdNumber">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtIdNumber" runat="server" Text='<%# Bind("IdNumber") %>' />
                                        <asp:RequiredFieldValidator ID="rfvIdNumber" runat="server" Text="*" ErrorMessage="IdNumber cannot be blank!" ControlToValidate="txtIdNumber"
                                                                    CssClass="NullValidator" />
                                        <blac:UniqueValidator ID="uvIdNumber" runat="server" Text="*" ErrorMessage="Gauger ID is not unique!"
                                                              ControlToValidate="txtIdNumber" TableName="tblGauger" DataField="IdNumber" CssClass="NullValidator" />
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblIdNumber" runat="server" Text='<%# Eval("IdNumber") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="100px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn EditFormColumnIndex="1" DataField="MobilePhone" HeaderText="Mobile Phone" SortExpression="MobilePhone" FilterControlWidth="70%" UniqueName="MobilePhone" MaxLength="20">
                                    <HeaderStyle Width="100px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn EditFormColumnIndex="1" DataField="MobileProvider" HeaderText="Mobile Provider" SortExpression="MobileProvider" FilterControlWidth="70%" UniqueName="MobileProvider" MaxLength="30">
                                    <HeaderStyle Width="150px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn EditFormColumnIndex="1" DataField="Email" HeaderText="Email" SortExpression="Email" FilterControlWidth="70%" UniqueName="Email" MaxLength="75">
                                    <HeaderStyle Width="250px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn EditFormColumnIndex="2" DataField="Region" HeaderText="Operating Region" SortExpression="Region" FilterControlWidth="70%" UniqueName="RegionID">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="ddlRegion" runat="server" DataSourceID="dsRegion" DataTextField="Name"
                                                          DataValueField="ID" SelectedValue='<%# Bind("RegionID") %>' CssClass="btn-xs">
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblRegion" runat="server" Text='<%# Eval("Region") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="200px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridDropDownColumn EditFormColumnIndex="2" DataField="HomeStateID" UniqueName="HomeStateID" HeaderText="Home State"
                                                            DataSourceID="sdsState" ListTextField="Abbreviation" ListValueField="ID" HeaderStyle-Width="100px" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="LastGaugerOrderTime" UniqueName="LastGaugerOrderTime" HeaderText="Last Gauging" SortExpression="LastGaugerOrderTime"
                                                         ReadOnly="true" DataFormatString="{0:M/d/yy}" HeaderStyle-Width="80px" FilterControlWidth="60%" />
                                <telerik:GridCheckBoxColumn DataField="MobileApp" UniqueName="MobileApp" HeaderText="Mobile App?" SortExpression="MobileApp"
                                                            HeaderStyle-Width="100px" FilterControlWidth="60%" EditFormColumnIndex="2" />
                                <telerik:GridCheckBoxColumn DataField="MobilePrint" UniqueName="MobilePrint" HeaderText="Mobile Print?" SortExpression="MobilePrint"
                                                            HeaderStyle-Width="100px" FilterControlWidth="60%" EditFormColumnIndex="2" />
                                <telerik:GridBoundColumn DataField="AppVersion" UniqueName="AppVersion" HeaderText="App Ver" SortExpression="AppVersion"
                                                         HeaderStyle-Width="120px" FilterControlAltText="70%" ReadOnly="true" />

                                <telerik:GridBoundColumn DataField="CreateDate" UniqueName="CreateDate" SortExpression="CreateDate" DataType="System.DateTime" DataFormatString="{0:M/d/yy}"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Create Date" HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="CreatedByUser" UniqueName="CreatedByUser" SortExpression="CreatedByUser"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Created By" HeaderStyle-Width="90px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="LastChangeDate" UniqueName="LastChangeDate" SortExpression="LastChangeDate" DataType="System.DateTime" DataFormatString="{0:M/d/yy HH:mm}"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Last Update Date" HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="LastChangedByUser" UniqueName="LastChangedByUser" SortExpression="LastChangedByUser"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Last Updated By" HeaderStyle-Width="95px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="DeleteDate" UniqueName="DeleteDate" SortExpression="DeleteDate" DataType="System.DateTime" DataFormatString="{0:M/d/yy HH:mm}"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Delete Date" HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="DeletedByUser" UniqueName="DeletedByUser" SortExpression="DeletedByUser"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Deleted By" HeaderStyle-Width="90px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />

                            </Columns>
                            <EditFormSettings ColumnNumber="3">
                                <EditColumn FilterControlAltText="Filter EditCommandColumn column" ButtonType="ImageButton" CancelImageUrl="~/images/cancel.png"
                                            UpdateImageUrl="~/images/apply.png" InsertImageUrl="~/images/apply.png" />
                            </EditFormSettings>
                            <PagerStyle AlwaysVisible="True" />
                        </MasterTableView>
                        <HeaderStyle Wrap="False" />
                        <PagerStyle AlwaysVisible="True" />
                        <FilterMenu EnableImageSprites="False">
                        </FilterMenu>
                    </telerik:RadGrid>
                </div>
                <blac:DBDataSource ID="dsMain" runat="server" SelectCommand="SELECT FirstName, LastName, UserNames, IDNumber, MobilePhone, MobileProvider, Email, Region, RegionID, HomeStateID, LastGaugerOrderTime, MobileApp, MobilePrint, AppVersion, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser, ID FROM dbo.viewGauger ORDER BY LastName, FirstName" />
                <%--This control is handing the data updates & special logic for the "rgMain" RadGrid control --%>
                <blc:RadGridDBCtrl ID="dbcMain" runat="server" ControlID="rgMain" UpdateTableName="tblGauger" />
                <blac:DBDataSource ID="sdsState" runat="server"
                                   SelectCommand="SELECT ID, FullName, Abbreviation FROM dbo.tblState WHERE ID > 0 UNION SELECT NULL, '(Select State)', '(N/A)' ORDER BY FullName" />
                <blac:DBDataSource ID="dsRegion" runat="server" SelectCommand="SELECT ID, Name FROM tblRegion UNION SELECT NULL, '(Specify Region)' ORDER BY Name" />
            </div>
        </div>
    </div>
</asp:Content>