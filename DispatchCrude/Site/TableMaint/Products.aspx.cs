﻿using System;
using DispatchCrude.Extensions;

namespace DispatchCrude.Site.TableMaint
{
    public partial class Products : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Print tab navigation html to page
            tabmenu.InnerHtml = NavigationHelper.PrintTabArray(NavigationHelper.TabSet_Locations, "Tab_Products").ToString();

            //Print button navigation html to page
            speedbuttons.InnerHtml = NavigationHelper.PrintButtonArray(NavigationHelper.ButtonSet_TabProducts, "Button_Products").ToString();
        }

        protected void grid_ItemCreated(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
        }

    }
}