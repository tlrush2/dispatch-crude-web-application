﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using AlonsIT;
using DispatchCrude.Extensions;

namespace DispatchCrude.Site.TableMaint
{
    public partial class Trucks : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Print tab navigation html to page
            tabmenu.InnerHtml = NavigationHelper.PrintTabArray(NavigationHelper.TabSet_Resources, "Tab_Trucks").ToString();

            //Print button navigation html to page
            speedbuttons.InnerHtml = NavigationHelper.PrintButtonArray(NavigationHelper.ButtonSet_TabTrucks, "Button_Trucks").ToString();

            // filter to the specified Truck (if any)
            if (Request.QueryString["ID"] != null)
            {
                using (SSDB ssdb = new SSDB())
                {
                    string truckNum = ssdb.QuerySingleValue("SELECT IDNumber FROM tblTruck WHERE ID={0}", (object)Request.QueryString["ID"]).ToString();
                    App_Code.RadGridHelper.SetFilter(rgMain.MasterTableView, "IDNumber", truckNum, GridKnownFunction.EqualTo);
                }
            }
        }

        protected void grid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            //Princy:  http://www.telerik.com/forums/hide-display-column-in-edit-mode#f_55AV2X0EWeWPuK1gVI5Q
            //Hide the LastOdometerDateUTC field so users dont see it (The field needs to be here though
            //in order to send NULL to the db so that the trigger will take care of the date if necessary)
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                GridEditableItem eitem = (GridEditableItem)e.Item;
                eitem["LastOdometerDateUTC"].Visible = false;    // for making the cell invisible during editing
                eitem["LastOdometerDateUTC"].Parent.Visible = false; // for making its label also invisible
            }

            //(Original code before above if statement was added)
            if (e.Item.IsInEditMode && e.Item is GridEditFormInsertItem)
            {
                using (SSDB db = new SSDB())
                {
                    ((e.Item as GridEditFormInsertItem).FindControl("txtIdNumber") as TextBox).Text = db.QuerySingleValue("SELECT max(isnull(CASE WHEN ISNUMERIC(IdNumber) = 1 THEN IdNumber ELSE 0 END, 1000)) + 1 FROM tblTruck").ToString();
                }
            }
        }

        protected void uvIDNumber_DataBinding(object sender, EventArgs e)
        {
            // ensure this event is "wired" at databind
            (sender as App_Code.Controls.UniqueValidator).OnGetOtherCellValue += uvIDNumber_GetOtherFieldValue;
        }

        protected object uvIDNumber_GetOtherFieldValue(GridItem item, string uniqueName)
        {
            object ret = null;
            if (uniqueName == "CarrierID" && item is GridEditableItem)
                ret = (App_Code.RadGridHelper.GetColumnDropDown((item as GridEditableItem)[uniqueName], "ddlCarrier") as DropDownList).SelectedValue;
            return ret;
        }

        protected void grid_ItemCommand(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == "Rebind")  // This is required for the custom refresh button to work as expected
                rgMain.Rebind();
        }

        /*
        protected void uvQRCode_DataBinding(object sender, EventArgs e)
        {
            // ensure this event is "wired" at databind
            (sender as App_Code.Controls.UniqueValidator).OnGetOtherCellValue += uvQRCode_GetOtherFieldValue;
        }

        protected object uvQRCode_GetOtherFieldValue(GridItem item, string uniqueName)
        {
            object ret = null;
            if (uniqueName == "CarrierID" && item is GridEditableItem)
                ret = (App_Code.RadGridHelper.GetColumnDropDown((item as GridEditableItem)[uniqueName], "ddlCarrier") as DropDownList).SelectedValue;
            return ret;
        }
        */
        private List<int> SelectedIDs()
        {
            // Fetch all records, not just current page
            rgMain.MasterTableView.AllowPaging = false;
            rgMain.MasterTableView.Rebind();

            List<int> ret = new List<int>();
            foreach (GridItem item in rgMain.MasterTableView.Items)
            {
                ret.Add(Core.Converter.ToInt32((item as GridDataItem).GetDataKeyValue("ID")));
            }
            return ret;
        }

        protected void cmdExportQR_Click(object source, EventArgs e)
        {
            // Load IDs into session variable and redirect
            Session[DispatchCrude.Controllers.TrucksController.SESSION_NAME_TRUCKIDLIST] = SelectedIDs();
            Response.Redirect("~/Trucks/QRList");
        }
    }
}