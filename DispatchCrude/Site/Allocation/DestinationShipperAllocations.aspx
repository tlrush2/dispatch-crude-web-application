﻿<%@  Title="Allocation" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DestinationShipperAllocations.aspx.cs"
    Inherits="DispatchCrude.Site.Allocation.DestinationShipperAllocations" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script>
        $("#ctl00_ctl00_EntityCaption").html("Destination Shipper Allocation");
    </script>
    <div class="tabbable TabRepaint">
        <ul class="nav nav-tabs" id="tabmenu" runat="server">
            <!--Tabs will print here (see code behind)-->
        </ul>
        <div class="tab-content">
            <div class="tab-pane in active">

                <div id="speedbuttons" class="well speedButtonContainer" runat="server">
                    <!--Buttons will print here (see code behind)-->
                </div>

                <asp:HiddenField ID="hfRestrictedTerminalID" ClientIDMode="Static" runat="server" value="-1" />
                <asp:HiddenField ID="hfRestrictedRegionID" ClientIDMode="Static" runat="server" value="-1" />
                <div>
                    <div class="leftpanel">
                        <div class="tabbable">
                            <ul class="nav nav-tabs">
                                <li class="active tab-blue">
                                    <a data-toggle="tab" href="#Filters" aria-expanded="true">Filters</a>
                                </li>
                                <li class="tab-green">
                                    <a data-toggle="tab" href="#Export" aria-expanded="true">Export</a>
                                </li>
                            </ul>
                            <div id="leftTabs" class="tab-content">
                                <div id="Filters" class="tab-pane active">
                                    <asp:Panel ID="panelFilter" runat="server" DefaultButton="btnRefresh">
                                        <div class="Entry">
                                            <asp:Label ID="lblShipper" runat="server" Text="Shipper" AssociatedControlID="ddShipper" />
                                            <asp:DropDownList CssClass="btn-xs" Width="100%" runat="server" ID="ddShipper" DataTextField="Name" DataValueField="ID" DataSourceID="dsShipperAll"
                                                              AutoPostBack="true" />
                                        </div>
                                        <div class="Entry">
                                            <asp:Label ID="lblDestination" runat="server" Text="Destination" AssociatedControlID="ddDestination" />
                                            <asp:DropDownList CssClass="btn-xs" Width="100%" runat="server" ID="ddDestination" DataTextField="Name" DataValueField="ID" DataSourceID="dsDestinationAll" />
                                        </div>
                                        <div class="Entry">
                                            <asp:Label ID="lblProductGroup" runat="server" Text="Product Group" AssociatedControlID="ddProductGroup" />
                                            <asp:DropDownList CssClass="btn-xs" Width="100%" runat="server" ID="ddProductGroup" DataTextField="Name" DataValueField="ID" DataSourceID="dsProductGroupAll" />
                                        </div>
                                        <div>
                                            <div class="Entry floatLeft-date-row">
                                                <asp:Label ID="lblStartDate" runat="server" Text="Start Date" AssociatedControlID="rdpStartDate" CssClass="Entry" />
                                                <telerik:RadDatePicker ID="rdpStartDate" runat="server" Width="100px">
                                                    <DateInput runat="server" DateFormat="M/d/yyyy" />
                                                    <DatePopupButton Enabled="true" />
                                                </telerik:RadDatePicker>
                                            </div>
                                            <div class="Entry floatRight-date-row">
                                                <asp:Label ID="lblEndDate" runat="server" Text="End Date" AssociatedControlID="rdpEndDate" CssClass="Entry" />
                                                <telerik:RadDatePicker ID="rdpEndDate" runat="server" Width="100px">
                                                    <DateInput runat="server" DateFormat="M/d/yyyy" />
                                                    <DatePopupButton Enabled="true" />
                                                </telerik:RadDatePicker>
                                            </div>
                                            <br /><br /><br />
                                            <div class="center">
                                                <asp:Button ID="btnRefresh" runat="server" Text="Refresh" CssClass="btn btn-blue shiny" OnClick="filterValueChanged" />
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                    </asp:Panel>
                                </div>
                                <div id="Export" class="tab-pane">
                                    <asp:Panel ID="panelExcel" runat="server" DefaultButton="cmdExport">
                                        <div>
                                            <div>
                                                <asp:Button ID="cmdExport" runat="server" Text="Export to Excel" CssClass="btn btn-blue shiny"
                                                            Enabled="true" OnClick="cmdExport_Click" />
                                            </div>
                                            <div class="spacer10px"></div>
                                            <div>
                                                <asp:Button ID="cmdImport" runat="server" ClientIDMode="Static" Text="Import Excel file"
                                                            Enabled="false" OnClick="cmdImport_Click" CssClass="floatRight btn btn-blue shiny" />
                                            </div>
                                        </div>
                                        <div class="spacer10px"></div>
                                        <div class="center">
                                            <asp:FileUpload ID="excelUpload" runat="server" ClientIDMode="Static" CssClass="floatLeft" Width="99%" />
                                            <asp:CustomValidator ID="cvUpload" runat="server" ControlToValidate="excelUpload" CssClass="NullValidator floatLeft" Display="Dynamic"
                                                                 Text="*" ErrorMessage="Only Excel (*.xlsx) files allowed)" OnServerValidate="cvUpload_ServerValidate" />
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="gridArea" style="height: 100%; min-height: 500px;">
                        <telerik:RadGrid ID="rgMain" runat="server" EnableHeaderContextMenu="False" CellSpacing="0" AllowPaging="true"
                                         GridLines="None" EnableLinqExpressions="false" AllowSorting="True" AllowFilteringByColumn="false" Height="800" CssClass="GridRepaint"
                                         ShowGroupPanel="false" EnableEmbeddedSkins="true" Skin="Vista" AlternatingItemStyle-BackColor="#dcf2fc"
                                         DataSourceID="dsMain" OnItemCreated="grid_ItemCreated" OnItemDataBound="grid_ItemDataBound" OnItemCommand="grid_ItemCommand"
                                         PageSize='<%# Settings.DefaultPageSize %>'>
                            <ClientSettings AllowDragToGroup="true" Resizing-AllowColumnResize="true" Resizing-AllowResizeToFit="true">
                                <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                            </ClientSettings>
                            <SortingSettings EnableSkinSortStyles="false" />
                            <GroupingSettings CaseSensitive="False" ShowUnGroupButton="true" />
                            <MasterTableView AutoGenerateColumns="False" DataKeyNames="ID" CommandItemDisplay="Top" AllowMultiColumnSorting="true">
                                <CommandItemSettings ShowAddNewRecordButton="true" AddNewRecordText="Add New Allocation" ShowExportToExcelButton="false" />
                                <RowIndicatorColumn Visible="True">
                                    <HeaderStyle Width="20px"></HeaderStyle>
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True">
                                    <HeaderStyle Width="20px" />
                                </ExpandCollapseColumn>
                                <SortExpressions>
                                    <telerik:GridSortExpression FieldName="EffectiveDate" SortOrder="Ascending" />
                                    <telerik:GridSortExpression FieldName="Destination" SortOrder="Ascending" />
                                    <telerik:GridSortExpression FieldName="Shipper" SortOrder="Ascending" />
                                    <telerik:GridSortExpression FieldName="ProductGroup" SortOrder="Ascending" />
                                </SortExpressions>
                                <Columns>
                                    <telerik:GridTemplateColumn UniqueName="ActionColumn" HeaderStyle-Width="100px" AllowFiltering="false" AllowSorting="false">
                                        <ItemTemplate>
                                            <asp:ImageButton runat="server" ID="btnEdit" CssClass="btn btn-xs btn-default shiny" CommandName="Edit" Text="Edit" ImageUrl="~/images/edit.png" />
                                            <asp:ImageButton runat="server" ID="btnDelete" CssClass="btn btn-xs btn-default shiny" CommandName="Delete" Text="Delete" ConfirmText="Are you sure?" ImageUrl="~/images/delete.png" />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="AddNew" Text="Add New" UniqueName="AddNewColumn"
                                                              Display="false"
                                                              ImageUrl="~/images/add.png">
                                        <HeaderStyle Width="25px" />
                                    </telerik:GridButtonColumn>

                                    <telerik:GridBoundColumn DataField="ID" UniqueName="ID" HeaderText="ID" ReadOnly="true" Display="false" ForceExtractValue="Always" />

                                    <telerik:GridDropDownColumn UniqueName="ShipperID" DataField="ShipperID" HeaderText="Shipper" SortExpression="Shipper"
                                                                FilterControlWidth="70%" HeaderStyle-Width="250px" EditFormColumnIndex="0" ItemStyle-Width="250px"
                                                                FilterControlAltText="LightGreen"
                                                                DataSourceID="dsShipper" ListValueField="ID" ListTextField="Name" />
                                    <telerik:GridDropDownColumn UniqueName="DestinationID" DataField="DestinationID" HeaderText="Destination"
                                                                FilterControlWidth="70%" HeaderStyle-Width="250px" EditFormColumnIndex="0" ItemStyle-Width="250px" SortExpression="Destination"
                                                                FilterControlAltText="LightGreen"
                                                                DataSourceID="dsDestination" ListValueField="ID" ListTextField="Name" />
                                    <telerik:GridDropDownColumn UniqueName="ProductGroupID" DataField="ProductGroupID" HeaderText="Product Group" SortExpression="ProductGroup"
                                                                FilterControlWidth="70%" HeaderStyle-Width="150px" EditFormColumnIndex="0" ItemStyle-Width="150px"
                                                                FilterControlAltText="LightGreen"
                                                                DataSourceID="dsProductGroup" ListValueField="ID" ListTextField="Name" />
                                    <telerik:GridNumericColumn DataField="Units" UniqueName="Units" SortExpression="Units"
                                                               HeaderText="Units" FilterControlWidth="60%" ItemStyle-Width="100px"
                                                               DataType="System.Int32" NumericType="Number" DataFormatString="{0,000:#0}"
                                                               FilterControlAltText="LightGreen"
                                                               HeaderStyle-Width="100px" ItemStyle-CssClass="RightAlign" ItemStyle-HorizontalAlign="Right" EditFormColumnIndex="1">
                                        <ColumnValidationSettings EnableRequiredFieldValidation="true">
                                            <RequiredFieldValidator ErrorMessage="Units value is required" Text="!" CssClass="NullValidator" InitialValue="" />
                                        </ColumnValidationSettings>
                                    </telerik:GridNumericColumn>
                                    <telerik:GridDropDownColumn UniqueName="UomID" DataField="UomID" HeaderText="UOM"
                                                                SortExpression="Uom" FilterControlWidth="70%" ItemStyle-Width="100px"
                                                                DataSourceID="dsUom" ListValueField="ID" ListTextField="Name"
                                                                FilterControlAltText="LightGreen"
                                                                HeaderStyle-Width="100px" EditFormColumnIndex="1">
                                    </telerik:GridDropDownColumn>

                                    <telerik:GridDateTimeColumn DataField="EffectiveDate" HeaderText="Effective Date" DataType="System.DateTime"
                                                                UniqueName="EffectiveDate" FilterControlWidth="70%" EditFormColumnIndex="2" ForceExtractValue="Always"
                                                                FilterControlAltText="LightGreen"
                                                                HeaderStyle-Width="100px" DataFormatString="{0:M/d/yyyy}">
                                        <ColumnValidationSettings EnableRequiredFieldValidation="true">
                                            <RequiredFieldValidator ErrorMessage="Effective Date value is required" CssClass="NullValidator" Text="!" />
                                        </ColumnValidationSettings>
                                    </telerik:GridDateTimeColumn>
                                    <telerik:GridDateTimeColumn DataField="EndDate" HeaderText="End Date" DataType="System.DateTime"
                                                                UniqueName="EndDate" FilterControlWidth="70%" EditFormColumnIndex="2" ForceExtractValue="Always"
                                                                FilterControlAltText="LightGreen"
                                                                HeaderStyle-Width="100px" DataFormatString="{0:M/d/yyyy}">
                                        <ColumnValidationSettings EnableRequiredFieldValidation="true">
                                            <RequiredFieldValidator ErrorMessage="End Date value is required" CssClass="NullValidator" Text="!" />
                                        </ColumnValidationSettings>
                                    </telerik:GridDateTimeColumn>

                                    <telerik:GridTemplateColumn UniqueName="Notes" DataField="Notes" HeaderText="Notes" HeaderStyle-Width="200px"
                                                                ItemStyle-CssClass="showEllipsis"
                                                                ForceExtractValue="Always"
                                                                FilterControlAltText="LightGreen"
                                                                EditFormColumnIndex="3">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtNotes" runat="server" Text='<%# Bind( "Notes") %>' Columns="30" Rows="5" TextMode="MultiLine" />
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblNotes" runat="server" Text='<%# Eval("Notes") %>' />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn UniqueName="Validation" HeaderStyle-Width="0px" Display="false" EditFormColumnIndex="3" EditFormHeaderTextFormat="">
                                        <EditItemTemplate>
                                            <asp:CustomValidator ID="cvGridError" runat="server" Display="None" ErrorMessage="Generic Message" />
                                            <asp:ValidationSummary ID="validationSummaryMain" runat="server" DisplayMode="BulletList" Enabled="true" EnableClientScript="true"
                                                                   CssClass="NullValidator" />
                                        </EditItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="CreateDate" UniqueName="CreateDate" SortExpression="CreateDate" DataType="System.DateTime" DataFormatString="{0:M/d/yy}"
                                                             ForceExtractValue="Always" ReadOnly="true"
                                                             HeaderText="Create Date" HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                    <telerik:GridBoundColumn DataField="CreatedByUser" UniqueName="CreatedByUser" SortExpression="CreatedByUser"
                                                             ForceExtractValue="Always" ReadOnly="true"
                                                             HeaderText="Created By" HeaderStyle-Width="90px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                    <telerik:GridBoundColumn DataField="LastChangeDate" UniqueName="LastChangeDate" SortExpression="LastChangeDate" DataType="System.DateTime" DataFormatString="{0:M/d/yy HH:mm}"
                                                             ForceExtractValue="Always" ReadOnly="true"
                                                             HeaderText="Last Update Date" HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                    <telerik:GridBoundColumn DataField="LastChangedByUser" UniqueName="LastChangedByUser" SortExpression="LastChangedByUser"
                                                             ForceExtractValue="Always" ReadOnly="true"
                                                             HeaderText="Last Updated By" HeaderStyle-Width="95px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />

                                    <telerik:GridDropDownColumn DataField="ImportAction" UniqueName="ImportAction" HeaderText="Import Action"
                                                                ReadOnly="true" Display="false" ForceExtractValue="Always" ItemStyle-BackColor="Olive" HeaderStyle-Width="0px"
                                                                FilterControlAltText="LightGreen"
                                                                DataSourceID="dsImportAction" ListTextField="Name" ListValueField="Name" DefaultInsertValue="Add">
                                    </telerik:GridDropDownColumn>
                                    <telerik:GridBoundColumn DataField="ImportOutcome" UniqueName="ImportOutcome" HeaderText="Import Outcome" HeaderStyle-Width="0px"
                                                             ReadOnly="true" Display="false" ForceExtractValue="Always" ItemStyle-BackColor="Olive" />
                                </Columns>
                                <EditFormSettings ColumnNumber="4">
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column" ButtonType="ImageButton" CancelImageUrl="~/images/cancel.png"
                                                UpdateImageUrl="~/images/apply.png" InsertImageUrl="~/images/apply.png" />
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <HeaderStyle Wrap="False" />
                            <PagerStyle AlwaysVisible="True" />
                            <FilterMenu EnableImageSprites="True" />
                        </telerik:RadGrid>
                        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
                            <script type="text/javascript">
                                function validateEndDate(source, args) {
                                    debugger;
                                    args.isValid = true;
                                    endDate = moment(args.Value);
                                    if (endDate.isValid()) {
                                        var earliest = moment($(".EffectiveDateRDP").length == 1
                                            ? getDatePickerByClass("EffectiveDateRDP").get_focusedDate()
                                            : $(".EarliestEndDate").val());
                                        if (earliest.isValid() && endDate.diff(earliest) < 0) {
                                            args.isValid = false;
                                            source.errormessage = "End Date must be after the Effective Date (or empty)";
                                        }
                                    }
                                };
                                function onEffectiveDateSelected(sender, e) {
                                    if (e.get_newDate() != null) {
                                        debugger;
                                        var dtpEnd = $find($(".GridEndDate").attr("ID").replace("_wrapper", ""));
                                        dtpEnd.set_selectedDate(moment(e.get_newDate()).add(1, "months").add(-1, "days").toDate());
                                    }
                                }
                                $(function () {
                                    $("#excelUpload").change(function () {
                                        var disabled = $("#excelUpload").val().length == 0;
                                        $("#cmdImport").prop("disabled", disabled);
                                    });
                                    $("#chkReplaceMatching").click(function () {
                                        debugger;
                                        if ($("#chkReplaceMatching").is(':checked')) {
                                            $("#chkReplaceOverlapping").prop("disabled", false);
                                        }
                                        else {
                                            $("#chkReplaceOverlapping").attr("checked", false).prop("disabled", true);
                                        }
                                    });
                                });
                            </script>
                        </telerik:RadScriptBlock>
                    </div>
                </div>
                <blac:DBDataSource ID="dsMain" runat="server" SelectIDNullsToZero="true"
                                   SelectCommand="SELECT *, ImportAction='None' 
                                   FROM dbo.fnAllocationDestinationShipper(@DestinationID, @ShipperID, @ProductGroupID, @TerminalID, @EffectiveDate, @EndDate) ">
                    <SelectParameters>
                        <asp:ControlParameter Name="DestinationID" ControlID="ddDestination" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
                        <asp:ControlParameter Name="ShipperID" ControlID="ddShipper" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
                        <asp:ControlParameter Name="ProductGroupID" ControlID="ddProductGroup" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
                        <asp:ProfileParameter Name="TerminalID" PropertyName="TerminalID" ConvertEmptyStringToNull="true" DbType="string" DefaultValue="-1" />
                        <asp:ControlParameter Name="EffectiveDate" ControlID="rdpStartDate" PropertyName="DbSelectedDate" Type="String" DefaultValue="NULL" />
                        <asp:ControlParameter Name="EndDate" ControlID="rdpEndDate" PropertyName="DbSelectedDate" Type="String" DefaultValue="NULL" />
                    </SelectParameters>
                </blac:DBDataSource>
                <blc:RadGridDBCtrl ID="dbcMain" runat="server"
                                   ControlID="rgMain"
                                   UpdateTableName="tblAllocationDestinationShipper"
                                   FilterActiveEntities="True" />

                <blac:DBDataSource ID="dsShipperAll" runat="server" 
                                   SelectCommand="SELECT DISTINCT C.ID, C.Name 
                                   FROM dbo.tblCustomer C 
                                    JOIN tblDestinationCustomers DC ON DC.CustomerID = C.ID 
                                   UNION 
                                   SELECT -1, '(All)' ORDER BY Name" />

                <blac:DBDataSource ID="dsShipper" runat="server" 
                                   SelectCommand="SELECT DISTINCT C.ID, C.Name 
                                   FROM dbo.tblCustomer C 
                                    JOIN tblDestinationCustomers DC ON DC.CustomerID = C.ID 
                                   ORDER BY Name" />

                <blac:DBDataSource ID="dsDestinationAll" runat="server" 
                                   SelectCommand="SELECT DISTINCT D.ID, Name = dbo.fnNameWithDeleted(D.Name, D.DeleteDateUTC), Active = dbo.fnIsActive(D.DeleteDateUTC) 
                                   FROM dbo.viewDestination D 
                                    JOIN tblDestinationCustomers DC ON DC.DestinationID = D.ID 
                                   WHERE @SID IN (-1, DC.CustomerID) 
                                    AND (TerminalID IS NULL OR @TerminalID = -1 OR TerminalID = @TerminalID) 
                                    AND (RegionID IS NULL OR @RegionID = -1 OR RegionID = @RegionID) 
                                   UNION 
                                    SELECT -1, '(All)', 1 
                                   ORDER BY Active DESC, Name">
                    <SelectParameters>
                        <asp:ControlParameter Name="SID" ControlID="ddShipper" PropertyName="SelectedValue" DefaultValue="-1" />
                        <asp:ControlParameter Name="TerminalID" ControlID="hfRestrictedTerminalID" PropertyName="Value" DefaultValue="-1" />
                        <asp:ControlParameter Name="RegionID" ControlID="hfRestrictedRegionID" PropertyName="Value" DefaultValue="-1" />
                    </SelectParameters>
                </blac:DBDataSource>
                <blac:DBDataSource ID="dsDestination" runat="server"
                                   SelectCommand="SELECT DISTINCT D.ID, Name = dbo.fnNameWithDeleted(D.Name, D.DeleteDateUTC), Active = dbo.fnIsActive(D.DeleteDateUTC) 
                                   FROM dbo.viewDestination D 
                                    JOIN tblDestinationCustomers DC ON DC.DestinationID = D.ID 
                                   WHERE (TerminalID IS NULL OR @TerminalID = -1 OR TerminalID = @TerminalID) 
                                    AND (RegionID IS NULL OR @RegionID = -1 OR RegionID = @RegionID) 
                                   ORDER BY Active DESC, Name">
                    <SelectParameters>
                        <asp:ControlParameter Name="TerminalID" ControlID="hfRestrictedTerminalID" PropertyName="Value" DefaultValue="-1" />
                        <asp:ControlParameter Name="RegionID" ControlID="hfRestrictedRegionID" PropertyName="Value" DefaultValue="-1" />
                    </SelectParameters>
                </blac:DBDataSource>

                <blac:DBDataSource ID="dsProductGroupAll" runat="server" 
                                   SelectCommand="SELECT ID, Name 
                                   FROM dbo.tblProductGroup 
                                   UNION 
                                    SELECT -1, '(All)' 
                                   ORDER BY Name" />

                <blac:DBDataSource ID="dsProductGroup" runat="server" 
                                   SelectCommand="SELECT ID, Name 
                                   FROM dbo.tblProductGroup 
                                   ORDER BY Name" />

                <blac:DBDataSource ID="dsUom" runat="server" 
                                   SelectCommand="SELECT ID, Name 
                                   FROM dbo.tblUOM 
                                   ORDER BY Name" />

                <blac:DBDataSource ID="dsImportAction" runat="server" 
                                   SelectCommand="SELECT Name = 'None' 
                                   UNION 
                                   SELECT 'Add' 
                                   UNION 
                                   SELECT 'Update' 
                                   UNION 
                                   SELECT 'Delete'" />
            </div>
        </div>
    </div>
</asp:Content>
