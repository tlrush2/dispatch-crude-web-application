﻿using System;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
//add for sql stuff
using System.Data;
using System.Collections;
using System.Drawing;
using Telerik.Web.UI;
using AlonsIT;
using DispatchCrude.App_Code;
using DispatchCrude.Core;
using DispatchCrude.Extensions;

namespace DispatchCrude.Site.Allocation
{
    public partial class DestinationShipperAllocations : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            rdpStartDate.SelectedDate = DateTime.Now.Date.AddDays(1 - DateTime.Now.Day).AddYears(-1);
            rdpEndDate.SelectedDate = DateTime.Now.Date;
            rdpStartDate.Calendar.ShowRowHeaders = rdpEndDate.Calendar.ShowRowHeaders = false;
        }

        private void ConfigureAjax(bool enabled = true)
        {
            if (enabled)
            {
                RadAjaxHelper.AddAjaxSetting(this.Page, ddShipper, ddDestination, true);
                RadAjaxHelper.AddAjaxSetting(this.Page, btnRefresh, rgMain, true);
                RadAjaxHelper.AddAjaxSetting(this.Page, rgMain, rgMain);
            }
            RadAjaxManager.GetCurrent(this.Page).EnableAJAX = enabled;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Print tab navigation html to page
            tabmenu.InnerHtml = NavigationHelper.PrintTabArray(NavigationHelper.TabSet_Allocation, "Tab_DestinationAllocation").ToString();

            //Print button navigation html to page
            speedbuttons.InnerHtml = NavigationHelper.PrintButtonArray(NavigationHelper.ButtonSet_TabAllocation, "Button_ManageAllocation").ToString();

            // Filter destinations to those in user's profile terminal
            int terminalID;
            if (Context.Profile.GetPropertyValue("TerminalID") != null
                && (terminalID = DBHelper.ToInt32(Context.Profile.GetPropertyValue("TerminalID"))) != 0)
            {
                hfRestrictedTerminalID.Value = terminalID.ToString();
            }

            // DCWEB-1027: if this user has a specific Profile Region specified, and the site is restricting users to that region, make the control readonly
            int regionID;
            if (Context.Profile.GetPropertyValue("RegionID") != null
                && (regionID = DBHelper.ToInt32(Context.Profile.GetPropertyValue("RegionID"))) != 0
                && Settings.SettingsID.RestrictUserToProfileRegion.AsBool())
            {
                hfRestrictedRegionID.Value = regionID.ToString();
            }

            ConfigureAjax(!Settings.SettingsID.DisableAJAX.AsBool());
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            // Hide ID column on the website
            rgMain.MasterTableView.Columns.FindByUniqueName("ID").Display = false;

            if (!IsPostBack)
            {
                bool updateGrid = false;
                if (Request.QueryString["ShipperID"] != null)
                {
                    ddShipper.DataBind();
                    DropDownListHelper.SetSelectedValue(ddShipper, Request.QueryString["ShipperID"]);
                    updateGrid = true;
                }
                if (Request.QueryString["EffectiveDate"] != null)
                {
                    DateTime effectiveDate = DateTime.Now.Date;
                    if (DateTime.TryParse(Request.QueryString["EffectiveDate"], out effectiveDate))
                    {
                        rdpStartDate.SelectedDate = effectiveDate;
                        updateGrid = true;
                    }
                }
                if (updateGrid)
                    rgMain.Rebind();
            }
        }

        protected void filterValueChanged(object sender, EventArgs e)
        {
            rgMain.Rebind();
        }

        protected void grid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.EditCommandName)
            {
                if (e.Item is GridEditableItem)
                {
                    //DropDownList ddl = RadGridHelper.GetColumnDropDown((e.Item as GridEditableItem)["ShipperID"]);
                }
            }
            if (e.CommandName == "AddNew" && e.Item is GridDataItem) //Row "Clone" button clicked - not used
            {
                // cancel the default operation
                e.Canceled = true;

                e.Item.OwnerTableView.InsertItem(GetRowValues(e.Item as GridDataItem));
            }
            else if (e.CommandName == RadGrid.InitInsertCommandName)
            {
                // cancel the default operation
                e.Canceled = true;

                e.Item.OwnerTableView.InsertItem(GetNewRowValues(e.Item as GridDataItem));
            }
            else if (e.CommandName == "ExportToExcel")
            {
                e.Canceled = true;
                ExportGridToExcel();
            }
        }

        private Hashtable GetRowValues(GridEditableItem gdi)
        {
            //Prepare an IDictionary with the predefined values
            Hashtable ret = new Hashtable();
            gdi.ExtractValues(ret);
            
            DateTime date = DBHelper.ToDateTime(
                gdi["EffectiveDate"].Controls[1] is Label
                    ? (gdi["EffectiveDate"].Controls[1] as Label).Text
                    : (gdi["EffectiveDate"].Controls[1] as RadDatePicker).DbSelectedDate);
            ret["EffectiveDate"] = date.Date;
            ret["EndDate"] = DateTime.Now.Date.AddYears(1);
            return ret;
        }
        private Hashtable GetNewRowValues(GridEditableItem gdi)
        {
            Hashtable ret = new Hashtable();
            // set ID to DBNULL (since we are creating a new record)
            ret["ID"] = DBNull.Value;
            ret["ShipperID"] = 0;
            ret["ProductGroupID"] = 0;
            ret["DestinationID"] = 0;
            ret["StateID"] = 0;
            ret["TerminalID"] = 0;
            ret["RegionID"] = 0;
            // default the new Effective Date to the first day of the next month
            DateTime effDate = DateTime.Now.Date.AddDays(1 - DateTime.Now.Day).AddMonths(1);
            ret["EffectiveDate"] = effDate;
            ret["EndDate"] = effDate.AddMonths(1).AddDays(-1);
            return ret;
        }

        protected void cmdExport_Click(object sender, EventArgs e)
        {
            ExportGridToExcel();
        }
        protected void cvUpload_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = Path.GetExtension(excelUpload.FileName).ToLower() == ".xlsx";
        }

        protected void cmdImport_Click(object sender, EventArgs e)
        {
            Page.Validate();
            if (Page.IsValid)
            {
                FinancialImporter fi = new FinancialImporter();
                fi.AddSpec(rgMain, "ID", FinancialImporter.FISpec.FISType.ID);
                fi.AddSpec(rgMain, "ShipperID", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgMain, "DestinationID", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgMain, "ProductGroupID", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgMain, "Units", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgMain, "UomID", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgMain, "EffectiveDate", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgMain, "EndDate", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgMain, "Notes", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec("CreatedByUser", typeof(string), FinancialImporter.FISpec.FISType.NEW | FinancialImporter.FISpec.FISType.USERNAME);
                fi.AddSpec("CreateDateUTC", typeof(DateTime), FinancialImporter.FISpec.FISType.NEW | FinancialImporter.FISpec.FISType.NOW);
                fi.AddSpec("LastChangedByUser", typeof(string), FinancialImporter.FISpec.FISType.UPDATE | FinancialImporter.FISpec.FISType.USERNAME);
                fi.AddSpec("LastChangeDateUTC", typeof(DateTime), FinancialImporter.FISpec.FISType.UPDATE | FinancialImporter.FISpec.FISType.NOW);
                fi.AddSpec(rgMain, "ImportAction", FinancialImporter.FISpec.FISType.ACTION);
                fi.AddSpec(rgMain, "ImportOutcome", FinancialImporter.FISpec.FISType.OUTCOME);
                Response.ExportExcelStream(fi.ProcessSql(excelUpload.FileContent, dbcMain.UpdateTableName), Path.GetFileNameWithoutExtension(excelUpload.FileName) + "_ImportResults.xlsx");
            }
        }

        protected void CellBackColorChanged(GridDataItem gridRow, string colName, ref Color color)
        {
            switch (colName.ToLower())
            {
                case "destinationid":
                case "shipperid":
                case "productgroupid":
                case "units":
                case "uomid":
                case "effectivedate":
                case "enddate":
                    color = Color.LightGreen;
                    break;
            }
        }

        private void ExportGridToExcel()
        {
            string filename = string.Format("Destination Shipper Allocations as of {0:yyyyMMdd}.xlsx", rdpStartDate.SelectedDate);
            string[] hiddenToInclude = { "ID", "ImportAction", "ImportOutcome" }
                , visibleToSkip = { "CreateDate", "CreatedByUser", "LastChangeDate", "LastChangedByUser" };
            RadGridExcelExporter exporter = new RadGridExcelExporter(
                    hiddenColNamesToInclude: hiddenToInclude
                    , visibleColNamesToSkip: visibleToSkip
                    , dropDownColumnDataValidationList: true);
            exporter.OnCellBackColorChanged += CellBackColorChanged;
            rgMain.AllowPaging = false;
            rgMain.Rebind();
            Response.ExportExcelStream(exporter.ExportSheet(rgMain.MasterTableView, "Destination Shipper Allocations"), filename);
        }

        protected void grid_ItemCreated(object sender, GridItemEventArgs e)
        {
        }

        protected void grid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            DataRowView data = e.Item.DataItem as DataRowView;
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                (RadGridHelper.GetControlByType(e.Item, "EndDate", typeof(RadDatePicker)) as RadDatePicker).CssClass = "GridEndDate";
                RadDatePicker dtp = RadGridHelper.GetControlByType(e.Item, "EffectiveDate", typeof(RadDatePicker)) as RadDatePicker;
                dtp.ClientEvents.OnDateSelected = "onEffectiveDateSelected";
            }
            else if (e.Item is GridDataItem && !e.Item.IsInEditMode)
            {
                ((e.Item as GridDataItem)["AddNewColumn"].Controls[0] as ImageButton).Enabled = false; // isTermination;
                ((e.Item as GridDataItem)["AddNewColumn"].Controls[0] as ImageButton).Visible = false; //isTermination;
            }
        }

    }
}