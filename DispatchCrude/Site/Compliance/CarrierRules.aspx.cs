﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
//add for sql stuff
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Drawing;
using Telerik.Web.UI;
using AlonsIT;
using DispatchCrude.App_Code;
using DispatchCrude.Core;
using DispatchCrude.Extensions;

namespace DispatchCrude.Site.Compliance
{
    public partial class CarrierRules : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            rdpStartDate.DbSelectedDate = DateTime.Now.Date;
            rdpEndDate.DbSelectedDate = DBNull.Value;
            rdpStartDate.Calendar.ShowRowHeaders = rdpEndDate.Calendar.ShowRowHeaders = false;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ConfigureAjax(!Settings.SettingsID.DisableAJAX.AsBool());
            dbcMain.GetUnboundChangedValues += GetChangedValues;

            //Print tab navigation html to page
            tabmenu.InnerHtml = NavigationHelper.PrintTabArray(NavigationHelper.TabSet_BusinessRules, "Tab_CarrierRules").ToString();
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {

            // Hide ID and add new columns on the website
            rgMain.MasterTableView.Columns.FindByUniqueName("ID").Display = false;
            rgMain.MasterTableView.Columns.FindByUniqueName("TypeID").Display = false;
            rgMain.MasterTableView.Columns.FindByUniqueName("AddNewColumn").Display = false;

            if (!IsPostBack)
            {
                DataRow dr = null;

                if (dr != null || Request.QueryString["DriverID"] != null)
                {
                    ddDriver.DataBind();
                    DropDownListHelper.SetSelectedValue(ddDriver, dr != null ? dr["DriverID"].ToString() : Request.QueryString["DriverID"]);
                }
                if (dr != null || Request.QueryString["CarrierID"] != null)
                {
                    ddCarrier.DataBind();
                    DropDownListHelper.SetSelectedValue(ddCarrier, dr != null ? dr["CarrierID"].ToString() : Request.QueryString["CarrierID"]);
                }
                if (dr != null || Request.QueryString["TerminalID"] != null)
                {
                    ddTerminal.DataBind();
                    DropDownListHelper.SetSelectedValue(ddTerminal, dr != null ? dr["TerminalID"].ToString() : Request.QueryString["TerminalID"]);
                }
                if (dr != null || Request.QueryString["StateID"] != null)
                {
                    ddState.DataBind();
                    DropDownListHelper.SetSelectedValue(ddState, dr != null ? dr["StateID"].ToString() : Request.QueryString["StateID"]);
                }
                if (dr != null || Request.QueryString["RegionID"] != null)
                {
                    ddRegion.DataBind();
                    DropDownListHelper.SetSelectedValue(ddRegion, dr != null ? dr["RegionID"].ToString() : Request.QueryString["RegionID"]);
                }                
                if (dr != null || Request.QueryString["EffectiveDate"] != null)
                {
                    DateTime effectiveDate = DateTime.Now.Date;
                    if (DateTime.TryParse(dr != null ? dr["OrderDate"].ToString() : Request.QueryString["EffectiveDate"], out effectiveDate))
                    {
                        rdpStartDate.SelectedDate = effectiveDate;
                    }
                }
                rgMain.Rebind();
            }
        }

        private void ConfigureAjax(bool enabled = true)
        {
            RadAjaxManager.GetCurrent(this.Page).EnableAJAX = enabled;
            if (enabled)
            {
                RadAjaxHelper.AddAjaxSetting(this.Page, btnRefresh, rgMain, true);
                RadAjaxHelper.AddAjaxSetting(this.Page, rgMain, rgMain);
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            rgMain.Rebind();
        }

        protected void grid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.EditCommandName)
            {
                if (e.Item is GridEditableItem)
                {
                    //DropDownList ddl = RadGridHelper.GetColumnDropDown((e.Item as GridEditableItem)["CarrierID"]);
                }
            }
            if (e.CommandName == "AddNew" && e.Item is GridDataItem) //Row "Add new" button clicked
            {
                // cancel the default operation
                e.Canceled = true;

                e.Item.OwnerTableView.InsertItem(GetRowValues(e.Item as GridDataItem));
            }
            else if (e.CommandName == RadGrid.InitInsertCommandName)
            {
                // cancel the default operation
                e.Canceled = true;

                e.Item.OwnerTableView.InsertItem(GetNewRowValues(e.Item as GridDataItem));
            }
            else if (e.CommandName == "ExportToExcel")
            {
                e.Canceled = true;
                ExportGridToExcel();
            }
        }

        private Hashtable GetRowValues(GridEditableItem gdi)
        {
            //Prepare an IDictionary with the predefined values
            Hashtable ret = new Hashtable();
            gdi.ExtractValues(ret);
            
            DateTime date = DBHelper.ToDateTime(
                gdi["EffectiveDate"].Controls[1] is Label
                    ? (gdi["EffectiveDate"].Controls[1] as Label).Text
                    : (gdi["EffectiveDate"].Controls[1] as RadDatePicker).DbSelectedDate);
            ret["EffectiveDate"] = date.Date;
            ret["EndDate"] = DateTime.Now.Date.AddYears(1);
            return ret;
        }
        private Hashtable GetNewRowValues(GridEditableItem gdi)
        {
            Hashtable ret = new Hashtable();
            // set ID to DBNULL (since we are creating a new record)
            ret["ID"] = DBNull.Value;
            ret["TypeID"] = 0;            
            ret["DriverID"] = 0;
            ret["CarrierID"] = 0;
            ret["TerminalID"] = 0;
            ret["StateID"] = 0;
            ret["RegionID"] = 0;
            // default the new Effective Date to the first day of the current month
            ret["EffectiveDate"] = DateTime.Now.Date.AddDays(1 - DateTime.Now.Day);
            ret["EndDate"] = DateTime.Now.Date.AddYears(1);
            return ret;
        }

        protected void cmdExport_Click(object sender, EventArgs e)
        {
            ExportGridToExcel();
        }
        protected void cvUpload_ServerValidate(object source, ServerValidateEventArgs args)
        {
            /*
            args.IsValid = Path.GetExtension(excelUpload.FileName).ToLower() == ".xlsx";
            */
        }

        protected void cmdImport_Click(object sender, EventArgs e)
        {
            /*
            Page.Validate();
            if (Page.IsValid)
            {
                FinancialImporter fi = new FinancialImporter();
                fi.AddSpec(rgMain, "ID", FinancialImporter.FISpec.FISType.ID);
                fi.AddSpec(rgMain, "xlsTypeID", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgMain, "CarrierID", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgMain, "DriverID", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgMain, "StateID", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgMain, "RegionID", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgMain, "Value", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgMain, "EffectiveDate", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgMain, "EndDate", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec("CreatedByUser", typeof(string), FinancialImporter.FISpec.FISType.NEW | FinancialImporter.FISpec.FISType.USERNAME);
                fi.AddSpec("CreateDateUTC", typeof(DateTime), FinancialImporter.FISpec.FISType.NEW | FinancialImporter.FISpec.FISType.NOW);
                fi.AddSpec("LastChangedByUser", typeof(string), FinancialImporter.FISpec.FISType.UPDATE | FinancialImporter.FISpec.FISType.USERNAME);
                fi.AddSpec("LastChangeDateUTC", typeof(DateTime), FinancialImporter.FISpec.FISType.UPDATE | FinancialImporter.FISpec.FISType.NOW);
                fi.AddSpec(rgMain, "ImportAction", FinancialImporter.FISpec.FISType.ACTION);
                fi.AddSpec(rgMain, "ImportOutcome", FinancialImporter.FISpec.FISType.OUTCOME);
                Response.ExportExcelStream(fi.ProcessSql(excelUpload.FileContent, dbcMain.UpdateTableName), Path.GetFileNameWithoutExtension(excelUpload.FileName) + "_ImportResults.xlsx");
            }
            */
        }

        private void exporter_CellValueChanged(GridDataItem item, string colName, ref object value)
        {
            if (colName == "Value")
                value = (RadGridHelper.GetControlByType(item, "Value", typeof(Label)) as Label).Text;
        }

        protected void exporter_CellBackColorChanged(GridDataItem gridRow, string colName, ref Color color)
        {
            switch (colName.ToLower())
            {
                case "typeid":
                case "driverid":
                case "carrierid":                
                case "terminalid":
                case "stateid":
                case "regionid":                
                case "value":
                case "effectivedate":
                case "enddate":
                    color = Color.LightGreen;
                    break;
            }
        }

        private void ExportGridToExcel()
        {
            string filename = string.Format("Carrier Rules as of {0:yyyyMMdd}.xlsx", rdpStartDate.SelectedDate);
            string[] hiddenToInclude = { "ID" } //, "ImportAction", "ImportOutcome" }
                , visibleToSkip = { "CreateDate", "CreatedByUser", "LastChangeDate", "LastChangedByUser" };
            RadGridExcelExporter exporter = new RadGridExcelExporter(
                    hiddenColNamesToInclude: hiddenToInclude
                    , visibleColNamesToSkip: visibleToSkip
                    , dropDownColumnDataValidationList: true);
            exporter.OnCellBackColorChanged += exporter_CellBackColorChanged;
            exporter.OnCellValueChanged += exporter_CellValueChanged;
            exporter.AddDropDownColumnDS("TypeID", "dsType");
            rgMain.AllowPaging = false;
            rgMain.Rebind();
            Response.ExportExcelStream(exporter.ExportSheet(rgMain.MasterTableView, "Carrier Rules"), filename);
        }

        protected void grid_ItemCreated(object sender, GridItemEventArgs e)
        {
        }

        protected void grid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            DataRowView data = e.Item.DataItem as DataRowView;
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                RadGridHelper.GetColumnRadComboBox(e.Item, "DriverID").Enabled = true;
                RadGridHelper.GetColumnRadComboBox(e.Item, "CarrierID").Enabled = true;
                RadGridHelper.GetColumnRadComboBox(e.Item, "TerminalID").Enabled = true;
                RadGridHelper.GetColumnRadComboBox(e.Item, "StateID").Enabled = true;
                RadGridHelper.GetColumnRadComboBox(e.Item, "RegionID").Enabled = true;                
                if (data != null)
                {
                    RadComboBoxHelper.SetSelectedValue(RadGridHelper.GetColumnRadComboBox(e.Item, "TypeID"), data["TypeID"]);
                    RadComboBox rcbValue = RadGridHelper.GetColumnRadComboBox(e.Item, "Value");
                    PopulateValueDropDown(rcbValue, DBHelper.ToInt32(data["TypeID"]), Converter.ToNullString(data["Value"]));
                    RadGridHelper.GetColumnRadComboBox(e.Item, "TypeID").Enabled = false;
                } 
                else // this is a new record so we need to handle "Type" value changes
                {
                    RadComboBox rcbType = RadGridHelper.GetColumnRadComboBox(e.Item, "TypeID");
                    rcbType.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(rcbType_SelectedIndexChanged);
                    rcbType.AutoPostBack = true;
                    rcbType.Enabled = true;
                    rcbType_SelectedIndexChanged(rcbType, EventArgs.Empty as RadComboBoxSelectedIndexChangedEventArgs);
                }
                (RadGridHelper.GetControlByType(e.Item, "EffectiveDate", typeof(RadDatePicker)) as RadDatePicker).Enabled = true;
            }
            else if (e.Item is GridDataItem && !e.Item.IsInEditMode)
            {
                // highlight best match records with a yellow background
                if (DBHelper.ToBoolean(data["BestMatch"])) e.Item.BackColor = Color.Goldenrod;

                ((e.Item as GridDataItem)["DeleteColumn"].Controls[0] as ImageButton).Enabled = true;
                ((e.Item as GridDataItem)["DeleteColumn"].Controls[0] as ImageButton).Visible = true;

                ((e.Item as GridDataItem)["AddNewColumn"].Controls[0] as ImageButton).Enabled = false; // isTermination;
                ((e.Item as GridDataItem)["AddNewColumn"].Controls[0] as ImageButton).Visible = false; // isTermination;
            }
        }

        protected void rcbType_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            RadComboBox rcbValue = RadGridHelper.GetControlByType(
                ((sender as Control).NamingContainer as GridEditFormInsertItem)["Value"]
                , typeof(RadComboBox)) as RadComboBox;
            //RadComboBox rcbValue = RadGridHelper.GetColumnRadComboBox((sender as RadComboBox).Parent as GridDataItem, "Value");
            PopulateValueDropDown(rcbValue, RadComboBoxHelper.SelectedValue((sender as RadComboBox)));
        }

        private void PopulateValueDropDown(RadComboBox rcbValue, int carrierRuleTypeID, string defValue = null)
        {
            rcbValue.Items.Clear();
            int itemCount = 0;
            using (SSDB db = new SSDB())
            {
                using (SqlCommand cmd = db.BuildCommand("spCarrierRuleDropDownValues"))
                {
                    cmd.Parameters["@CarrierRuleTypeID"].Value = carrierRuleTypeID;
                    DataTable dtValues = SSDB.GetPopulatedDataTable(cmd);
                    itemCount = dtValues.Rows.Count;
                    // if no default value was inputted, then use the one from the db (if any available)
                    if (defValue == null && !DBHelper.IsNull(cmd.Parameters["@DefaultValue"].Value)) 
                        defValue = cmd.Parameters["@DefaultValue"].Value.ToString();
                    foreach (DataRow drValue in dtValues.Rows)
                    {
                        string value = drValue["Value"].ToString();
                        rcbValue.Items.Add(new RadComboBoxItem(value, value));
                    }
                }
            }
            if (itemCount > 0 && !DBHelper.IsNull(defValue))
            {
                RadComboBoxHelper.SetSelectedValue(rcbValue, defValue);
                rcbValue.AllowCustomText = false;
            }
            else
                rcbValue.Text = defValue;
        }

        private void GetChangedValues(GridEditableItem item, System.Data.DataRow values)
        {
            RadComboBox rcb = RadGridHelper.GetColumnRadComboBox(item, "Value");
            values["Value"] = rcb.Text;
            rcb = RadGridHelper.GetColumnRadComboBox(item, "TypeID");
            values["TypeID"] = RadComboBoxHelper.SelectedValue(rcb);
        }

    }
}