﻿using System;
using DispatchCrude.Extensions;

namespace DispatchCrude.Site.Financials
{
    public partial class DriverGroups : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ConfigureAjax(true);

            //Print tab navigation html to page
            tabmenu.InnerHtml = NavigationHelper.PrintTabArray(NavigationHelper.TabSet_Settlement, "Tab_DriverGroups").ToString();

            //Print button navigation html to page
            speedbuttons.InnerHtml = NavigationHelper.PrintButtonArray(NavigationHelper.ButtonSet_TabDriverGroups, "Button_Groups").ToString();
        }
        private void ConfigureAjax(bool enabled)
        {
            if (enabled)
                App_Code.RadAjaxHelper.AddAjaxSetting(this.Page, rgMain, rgMain);
        }
    }
}