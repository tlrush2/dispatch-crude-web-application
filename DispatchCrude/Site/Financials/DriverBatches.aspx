﻿<%@ Page Title="Settlement" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DriverBatches.aspx.cs"
    Inherits="DispatchCrude.Site.Orders.DriverBatches" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script>
        $("#ctl00_ctl00_EntityCaption").html("Driver Settlement Batches");
    </script>
    <div class="tabbable TabRepaint">
        <ul class="nav nav-tabs" id="tabmenu" runat="server">
            <!--Tabs will print here (see code behind)-->
        </ul>
        <div class="tab-content">
            <div class="tab-pane in active">

                <div id="speedbuttons" class="well speedButtonContainer" runat="server">
                    <!--Buttons will print here (see code behind)-->
                </div>

                <div class="leftpanel">
                    <div class="tabbable">
                        <ul class="nav nav-tabs">
                            <li class="active tab-blue">
                                <a data-toggle="tab" href="#Filters" aria-expanded="true">Filters</a>
                            </li>
                            <li class="tab-yellow">
                                <a data-toggle="tab" href="#Batches" aria-expanded="true">Batches</a>
                            </li>
                            <li class="tab-green">
                                <a data-toggle="tab" href="#Export" aria-expanded="true">Export</a>
                            </li>
                            <li class="tab-orange">
                                <a data-toggle="tab" href="#Email" aria-expanded="true">Email</a>
                            </li>
                        </ul>
                        <div id="leftTabs" class="tab-content">
                            <div id="Filters" class="tab-pane active">
                                <asp:Panel ID="panelGenerate" runat="server" DefaultButton="cmdFilter">
                                    <div class="Entry">
                                        <asp:Label ID="lblBatchNum" runat="server" Text="Batch #" AssociatedControlID="txtBatchNum" CssClass="Entry" />
                                        <asp:TextBox Width="100%" runat="server" ID="txtBatchNum" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblOrderNum" runat="server" Text="Order #" AssociatedControlID="txtOrderNum" CssClass="Entry" />
                                        <asp:TextBox Width="100%" runat="server" ID="txtOrderNum" />
                                    </div>

                                    <hr />

                                    <div class="Entry">
                                        <asp:Label ID="lblCarrier" runat="server" Text="Carrier" AssociatedControlID="ddlCarrier" CssClass="Entry" />
                                        <asp:DropDownList CssClass="btn-xs" Width="100%" runat="server" ID="ddlCarrier" DataTextField="FullName" DataValueField="ID" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblDriverGroup" runat="server" Text="Driver Group" AssociatedControlID="ddlDriverGroup" CssClass="Entry" />
                                        <asp:DropDownList CssClass="btn-xs" Width="100%" runat="server" ID="ddlDriverGroup" DataTextField="FullName" DataValueField="ID" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblDriver" runat="server" Text="Driver" AssociatedControlID="ddlDriver" CssClass="Entry" />
                                        <asp:DropDownList CssClass="btn-xs" Width="100%" runat="server" ID="ddlDriver" DataTextField="Name" DataValueField="ID" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblDateField" runat="server" Text="Date" AssociatedControlID="ddlDateField" CssClass="Entry" />
                                        <asp:DropDownList CssClass="btn-xs" Width="100%" runat="server" ID="ddlDateField" DataTextField="FullName" DataValueField="ID">
                                            <asp:ListItem Text="Batch Date" Value="BatchDate"></asp:ListItem>
                                            <asp:ListItem Text="Period End Date" Value="PeriodEndDate"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div>
                                        <div class="Entry floatLeft-date-row">
                                            <asp:Label ID="lblStartDate" runat="server" Text="Start" AssociatedControlID="rdpStart" CssClass="Entry" />
                                            <telerik:RadDatePicker ID="rdpStart" runat="server" Width="100px">
                                                <DateInput runat="server" DateFormat="M/d/yyyy" />
                                                <DatePopupButton Enabled="true" />
                                            </telerik:RadDatePicker>
                                        </div>
                                        <div class="Entry floatRight-date-row">
                                            <asp:Label ID="lblEndDate" runat="server" Text="End" AssociatedControlID="rdpEnd" CssClass="Entry" />
                                            <telerik:RadDatePicker ID="rdpEnd" runat="server" Width="100px">
                                                <DateInput runat="server" DateFormat="M/d/yyyy" />
                                                <DatePopupButton />
                                            </telerik:RadDatePicker>
                                        </div>
                                    </div>
                                    <br /><br /><br />
                                    <div class="clear"></div>
                                    <div class="spacer5px"></div>
                                    <div style="text-align: center;">
                                        <asp:Button ID="cmdFilter" runat="server" Text="Refresh" CssClass="btn btn-blue shiny"
                                                    UseSubmitBehavior="false" OnClientClicked="DisableFilterButton" OnClick="cmdFilter_Click" />
                                    </div>
                                </asp:Panel>
                            </div>
                            <div id="Batches" class="tab-pane">
                                <asp:Panel ID="panelSelectedCtrl" runat="server" DefaultButton="cmdUnsettle">
                                    <asp:Label ID="lblSelectionAction" runat="server" Text="Select Batch" style="font-weight:bold" />
                                    <div style="height: 2px;"></div>
                                    <asp:DropDownList CssClass="btn-xs" ID="ddlBatch" runat="server" DataTextField="FullName" DataValueField="ID"
                                                      AutoPostBack="true" OnSelectedIndexChanged="ddlBatch_SelectedIndexChanged" Width="100%" />
                                    <div class="spacer5px"></div>
                                    <div>
                                        <asp:TextBox ID="txtCreatedByUser" runat="server" ReadOnly="true" CssClass="floatLeft" Width="45%" PlaceHolder="Created By User"
                                                     ForeColor="Gray" />
                                        <asp:TextBox ID="txtCreateDate" runat="server" ReadOnly="true" CssClass="floatRight" Width="45%" PlaceHolder="Create Date"
                                                     ForeColor="Gray" />
                                    </div>
                                    <div style="height: 5px;" class="clear"></div>
                                    <asp:TextBox ID="txtInvoiceNum" runat="server" Width="97%" Enabled="false" Locked="true" PlaceHolder="Invoice #" ForeColor="Gray" />
                                    <div style="height: 2px;" class="clear"></div>
                                    <asp:TextBox ID="txtNotes" runat="server" Width="97%" TextMode="MultiLine" Height="40px" Enabled="false" Locked="true" PlaceHolder="Notes"
                                                 ForeColor="Gray" />
                                    <div class="spacer10px"></div>
                                    <div style="text-align: center;">
                                        <asp:Button ID="cmdUnsettle" runat="server" Text="Unsettle Selected Batch" CssClass="btn btn-blue shiny"
                                                    OnClientClick="javascript:if(!confirm('This action will delete the selected batch and reset all its orders. Are you sure?')){return false;}"
                                                    UseSubmitBehavior="false" OnClick="cmdUnsettle_Click" />
                                    </div>
                                </asp:Panel>
                            </div>
                            <div id="Export" class="tab-pane">
                                <asp:Panel ID="panelExportActions" runat="server" DefaultButton="cmdExport">
                                    <div class="Entry">
                                        <asp:Label ID="lblReportCenterSel" runat="server" Text="Report Center Selection" AssociatedControlID="rcbUserReport" CssClass="Entry" />
                                        <telerik:RadComboBox ID="rcbUserReport" runat="server" AllowCustomText="false" ShowDropDownOnTextboxClick="true"
                                                             DataSourceID="dsUserReport" DataTextField="Name" DataValueField="ID" MaxLength="30"
                                                             Width="100%"
                                                             OnItemDataBound="rcbUserReport_ItemDataBound" />
                                        <blac:DBDataSource ID="dsUserReport" runat="server"
                                                           SelectCommand="SELECT * FROM viewUserReportDefinition WHERE (UserNames IS NULL OR @UserName IN (SELECT Value FROM dbo.fnSplitCSV(UserNames))) ORDER BY Name">
                                            <SelectParameters>
                                                <asp:Parameter Name="UserName" DefaultValue="" DbType="String" />
                                            </SelectParameters>
                                        </blac:DBDataSource>
                                    </div>

                                    <div class="Entry">
                                        <asp:Label ID="lblFileName" runat="server" Text="Export File Name" AssociatedControlID="rtxFileName" CssClass="Entry" />
                                        <telerik:RadTextBox ID="rtxFileName" runat="server" MaxLength="100" Width="100%" ValidationGroup="vgFileName" />
                                        <asp:RequiredFieldValidator ID="rfvFileName" runat="server" ControlToValidate="rtxFileName"
                                                                    ValidationGroup="vgFileName" Text="Export file name is required." CssClass="NullValidator" Display="Dynamic" />
                                        <asp:RegularExpressionValidator ID="revFileName" runat="server" ControlToValidate="rtxFileName"
                                                                        ValidationExpression="^[a-zA-Z0-9_\s\-\.]+$"
                                                                        ValidationGroup="vgFileName" Text="Invalid file name was provided." CssClass="NullValidator" Display="Dynamic" />
                                        <asp:RegularExpressionValidator ID="revFileName_NoExtension" runat="server" ControlToValidate="rtxFileName"
                                                                        ValidationExpression="[^\.]+$"
                                                                        ValidationGroup="vgFileName" Text="Do not enter a filename extension (.xlsx will automatically be used)." CssClass="NullValidator" Display="Dynamic" />
                                    </div>
                                    <div class="spacer5px"></div>
                                    <div class="center">
                                        <asp:Button ID="cmdExport" runat="server" Text="Export" CssClass="NOAJAX btn btn-blue shiny"
                                                    CausesValidation="true" ValidationGroup="vgFileName"
                                                    UseSubmitBehavior="false" OnClick="cmdExport_Click" />
                                    </div>
                                    <div class="spacer50px"></div>
                                    <div class="Entry">
                                        <asp:Label ID="lblPdfExport" runat="server" Text="Pdf Export Selection" AssociatedControlID="rddlPdfExport" CssClass="Entry" />
                                        <telerik:RadDropDownList ID="rddlPdfExport" runat="server"
                                            DataSourceID="dsPdfExport" DataTextField="Name" DataValueField="ID"
                                            Width="100%" />
                                        <blac:DBDataSource ID="dsPdfExport" runat="server"
                                            SelectCommand="SELECT ID, Name FROM tblPdfExport WHERE PdfExportTypeID = 1 AND DeleteDateUTC IS NULL ORDER BY Name">
                                        </blac:DBDataSource>
                                    </div>
                                    <div class="spacer5px"></div>
                                    <div class="center">
                                        <asp:Button ID="cmdExportPdfExportZip" runat="server" Text="Export Driver Settlement Statement Zip" CssClass="NOAJAX btn btn-blue shiny"
                                                    UseSubmitBehavior="false" OnClick="cmdExportPdfExportZip_Click" />
                                    </div>
                                </asp:Panel>
                            </div>
                            <div id="Email" class="tab-pane">
                                <asp:Panel ID="panel1" runat="server" DefaultButton="cmdGenerateStatementEmails">
                                    <div class="Entry">
                                        <asp:Label ID="lblEmailDefinition" runat="server" Text="Email Definition Selection" AssociatedControlID="rddlEmailDefinition" CssClass="Entry" />
                                        <telerik:RadDropDownList ID="rddlEmailDefinition" runat="server"
                                            DataSourceID="dsEmailDefinition" DataTextField="Name" DataValueField="ID"
                                            Width="100%" />
                                        <blac:DBDataSource ID="dsEmailDefinition" runat="server"
                                            SelectCommand="SELECT ID, Name FROM tblDriverSettlementStatementEmailDefinition WHERE DeleteDateUTC IS NULL ORDER BY Name">
                                        </blac:DBDataSource>
                                    </div>
                                    <div class="spacer5px"></div>
                                    <div class="Entry">
                                        <asp:CheckBox ID="chkQueueEmailSend" runat="server" Text="&nbsp;Immediately queue emails to send?" />
                                    </div>

                                    <div class="center">
                                        <asp:Button ID="cmdGenerateStatementEmails" runat="server" Text="Generate Statement Emails" CssClass="btn btn-blue shiny"
                                                    UseSubmitBehavior="false" OnClick="cmdGenerateStatementEmails_Click" />
                                    </div>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="gridArea" style="height:100%">
                    <telerik:RadGrid ID="rgMain" runat="server" EnableHeaderContextMenu="False" Height="800" CssClass="GridRepaint" CellSpacing="0" GridLines="None"
                                     AllowSorting="false" ShowFooter="true" EnableViewState="false" EnableEmbeddedSkins="true" Skin="Vista" AlternatingItemStyle-BackColor="#dcf2fc"
                                     PageSize="100" AllowPaging="false" ShowStatusBar="false"
                                     OnDataBound="grid_DataBound" OnItemDataBound="grid_ItemDataBound">
                        <ClientSettings>
                            <Scrolling AllowScroll="True" UseStaticHeaders="True" FrozenColumnsCount="0" />
                        </ClientSettings>
                        <MasterTableView AutoGenerateColumns="False" DataKeyNames="ID" CommandItemDisplay="None">
                            <ColumnGroups>
                                <telerik:GridColumnGroup HeaderText="Order Details" Name="OrderDetails" HeaderStyle-HorizontalAlign="Center" />
                                <telerik:GridColumnGroup HeaderText="Pickup Details" Name="PickupDetails" HeaderStyle-HorizontalAlign="Center" />
                                <telerik:GridColumnGroup HeaderText="Delivery Details" Name="DeliveryDetails" HeaderStyle-HorizontalAlign="Center" />
                                <telerik:GridColumnGroup HeaderText="Ticket Details" Name="TicketDetails" HeaderStyle-HorizontalAlign="Center" />
                                <telerik:GridColumnGroup HeaderText="Route Details" Name="RouteDetails" HeaderStyle-HorizontalAlign="Center" />
                                <telerik:GridColumnGroup HeaderText="Settlement Details" Name="InvoiceDetails" HeaderStyle-HorizontalAlign="Center" />
                            </ColumnGroups>
                            <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                            <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                <HeaderStyle Width="20px"></HeaderStyle>
                            </RowIndicatorColumn>
                            <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                <HeaderStyle Width="20px"></HeaderStyle>
                            </ExpandCollapseColumn>
                            <Columns>
                                <telerik:GridBoundColumn DataField="BatchNum" UniqueName="BatchNum" HeaderText="Batch #" SortExpression="BatchNum"
                                                         ReadOnly="true">
                                    <HeaderStyle Width="70px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="OrderNum" ReadOnly="true" SortExpression="OrderNum"
                                                         HeaderText="Order #" ColumnGroupName="OrderDetails" ForceExtractValue="Always">
                                    <HeaderStyle Width="65px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="JobNumber" ReadOnly="true" SortExpression="JobNumber"
                                                         HeaderText="Job #" ColumnGroupName="OrderDetails" ForceExtractValue="Always">
                                    <HeaderStyle Width="65px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ContractNumber" ReadOnly="true" SortExpression="ContractNumber"
                                                         HeaderText="Contract #" ColumnGroupName="OrderDetails" ForceExtractValue="Always">
                                    <HeaderStyle Width="65px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridDateTimeColumn DataField="OrderDate" UniqueName="OrderDate" HeaderText="Date" SortExpression="OrderDate"
                                                            DataFormatString="{0:M/d/yyyy}" ReadOnly="true" ForceExtractValue="Always" ColumnGroupName="OrderDetails">
                                    <HeaderStyle Width="70px" />
                                </telerik:GridDateTimeColumn>
                                <telerik:GridBoundColumn DataField="Shipper" UniqueName="Shipper" HeaderText="Shipper" SortExpression="Shipper"
                                                         ReadOnly="true" ForceExtractValue="Always" ColumnGroupName="OrderDetails"
                                                         Aggregate="Count" FooterStyle-HorizontalAlign="Right" HeaderStyle-Width="200px" />

                                <telerik:GridBoundColumn DataField="Carrier" UniqueName="Carrier" HeaderText="Carrier" SortExpression="Carrier"
                                                         ReadOnly="true" ForceExtractValue="Always" ColumnGroupName="OrderDetails" HeaderStyle-Width="200px" />

                                <telerik:GridBoundColumn DataField="DriverGroup" UniqueName="DriverGroup" HeaderText="Driver Group" SortExpression="DriverGroup"
                                                         ReadOnly="true" ForceExtractValue="Always" ColumnGroupName="OrderDetails" HeaderStyle-Width="80px" />

                                <telerik:GridBoundColumn DataField="DriverFirst" UniqueName="DriverFirst" HeaderText="Driver First" SortExpression="DriverFirst"
                                                         ReadOnly="true" ForceExtractValue="Always" ColumnGroupName="OrderDetails" HeaderStyle-Width="80px" />
                                <telerik:GridBoundColumn DataField="DriverLast" UniqueName="DriverLast" HeaderText="Driver Last" SortExpression="DriverLast"
                                                         ReadOnly="true" ForceExtractValue="Always" ColumnGroupName="OrderDetails" HeaderStyle-Width="80px" />

                                <telerik:GridBoundColumn DataField="Product" UniqueName="Product" HeaderText="Product" SortExpression="Product"
                                                         ReadOnly="true" FilterControlWidth="70%" ForceExtractValue="Always" ColumnGroupName="OrderDetails"
                                                         Visible="false" Display="true">
                                    <HeaderStyle Width="175px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Truck" UniqueName="Truck" HeaderText="Truck" SortExpression="Truck"
                                                         ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="OrderDetails">
                                    <HeaderStyle Width="75px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Trailer" UniqueName="Trailer" HeaderText="Trailer" SortExpression="Trailer"
                                                         ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="OrderDetails">
                                    <HeaderStyle Width="75px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Trailer2" UniqueName="Trailer2" HeaderText="Trailer2" SortExpression="Trailer2"
                                                         ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="OrderDetails" Visible="false" Display="true">
                                    <HeaderStyle Width="75px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Origin" UniqueName="Origin" HeaderText="Origin" SortExpression="Origin"
                                                         ReadOnly="true" ForceExtractValue="Always" ColumnGroupName="OrderDetails">
                                    <HeaderStyle Width="300px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="LeaseName" UniqueName="LeaseName" HeaderText="Lease Name" SortExpression="LeaseName"
                                                         ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="OrderDetails"
                                                         Visible="false" Display="true">
                                    <HeaderStyle Width="75px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="LeaseNum" UniqueName="LeaseNum" HeaderText="Lease #" SortExpression="LeaseNum"
                                                         ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="OrderDetails"
                                                         Visible="false" Display="true">
                                    <HeaderStyle Width="65px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Operator" UniqueName="Operator" HeaderText="Operator" SortExpression="Operator"
                                                         ReadOnly="true" ForceExtractValue="Always" ColumnGroupName="OrderDetails"
                                                         Visible="false" Display="true">
                                    <HeaderStyle Width="100px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Producer" UniqueName="Producer" HeaderText="Producer" SortExpression="Producer"
                                                         ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="OrderDetails"
                                                         Visible="false" Display="true">
                                    <HeaderStyle Width="100px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="AuditNotes" UniqueName="AuditNotes" HeaderText="Audit Notes" AllowSorting="false"
                                                         ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="OrderDetails"
                                                         HeaderStyle-Width="100px" Visible="false" Display="true"
                                                         ItemStyle-Wrap="true" />
                                <telerik:GridBoundColumn DataField="OriginStateAbbrev" UniqueName="OriginStateAbbrev" HeaderText="State" SortExpression="OriginStateAbbrev"
                                                         ReadOnly="true" ForceExtractValue="Always" ColumnGroupName="PickupDetails"
                                                         Visible="false" Display="true">
                                    <HeaderStyle Width="60px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="OriginStation" HeaderText="Station" SortExpression="OriginStation"
                                                         FilterControlWidth="70%" UniqueName="OriginStation" ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="PickupDetails"
                                                         Visible="false" Display="true">
                                    <HeaderStyle Width="90px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="OriginTimeZone" UniqueName="OriginTimeZone" HeaderText="TZ" SortExpression="OriginTimeZone"
                                                         ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="PickupDetails"
                                                         Visible="false" Display="true">
                                    <HeaderStyle Width="35px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridDateTimeColumn DataField="OriginArriveTime" UniqueName="OriginArriveTime" HeaderText="Load Start" SortExpression="OriginArriveTime"
                                                            DataFormatString="{0:hh:mm tt}" ReadOnly="true" ForceExtractValue="Always" ColumnGroupName="PickupDetails"
                                                            Visible="false" Display="true">
                                    <HeaderStyle Width="70px" />
                                </telerik:GridDateTimeColumn>
                                <telerik:GridDateTimeColumn DataField="OriginDepartTime" UniqueName="OriginDepartTime" HeaderText="Load Stop" SortExpression="OriginDepartTime"
                                                            DataFormatString="{0:hh:mm tt}" ReadOnly="true" ForceExtractValue="Always" ColumnGroupName="PickupDetails"
                                                            Visible="false" Display="true">
                                    <HeaderStyle Width="70px" />
                                </telerik:GridDateTimeColumn>
                                <telerik:GridBoundColumn DataField="OriginMinutes" UniqueName="OriginMinutes" HeaderText="Load Time" SortExpression="OriginMinutes"
                                                         ReadOnly="true" ForceExtractValue="Always" DataType="System.Int16" ColumnGroupName="PickupDetails"
                                                         Visible="false" Display="true">
                                    <HeaderStyle Width="75px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="OriginWaitNotes" UniqueName="OriginWaitNotes" HeaderText="Wait Notes" SortExpression="OriginWaitNotes"
                                                         ReadOnly="true" ForceExtractValue="Always" DataType="System.String" ConvertEmptyStringToNull="false" ColumnGroupName="PickupDetails">
                                    <HeaderStyle Width="300px" />
                                    <ItemStyle Wrap="true" />
                                </telerik:GridBoundColumn>
                                <telerik:GridCheckBoxColumn DataField="H2S" HeaderText="H2S" UniqueName="H2S" FilterControlWidth="60%" ReadOnly="true"
                                                            ForceExtractValue="InBrowseMode" HeaderStyle-Width="40px" ColumnGroupName="PickupDetails"
                                                            visible="false" Display="true" />
                                <telerik:GridCheckBoxColumn DataField="OriginChainup" UniqueName="OriginChainup" HeaderText="Chain-Up?" SortExpression="OriginChainup"
                                                            ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="PickupDetails"
                                                            Visible="false" Display="true">
                                    <HeaderStyle Width="60px" />
                                </telerik:GridCheckBoxColumn>
                                <telerik:GridCheckBoxColumn DataField="Rejected" UniqueName="Rejected" HeaderText="Rejected?" SortExpression="Rejected"
                                                            ReadOnly="true" ForceExtractValue="InBrowseMode" ItemStyle-HorizontalAlign="Center" ColumnGroupName="PickupDetails"
                                                            visible="false" Display="true">
                                    <HeaderStyle Width="95px" />
                                </telerik:GridCheckBoxColumn>
                                <telerik:GridBoundColumn DataField="RejectNotes" UniqueName="RejectNotes" HeaderText="Reject Notes" AllowFiltering="false"
                                                         ReadOnly="true" ForceExtractValue="Always" ItemStyle-HorizontalAlign="Left" ColumnGroupName="PickupDetails"
                                                         HeaderStyle-Width="100px"
                                                         Visible="false" Display="true"
                                                         ItemStyle-Wrap="true" />
                                <telerik:GridBoundColumn DataField="OriginBOLNum" UniqueName="OriginBOLNum" HeaderText="BOL" SortExpression="OriginBOLNum"
                                                         ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="PickupDetails"
                                                         Visible="false" Display="true">
                                    <HeaderStyle Width="90px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="OriginGrossUnits" UniqueName="OriginGrossUnits" HeaderText="Gross Vol" SortExpression="OriginGrossUnits"
                                                         ReadOnly="true" ForceExtractValue="Always" DataFormatString="{0:0.000}" ItemStyle-HorizontalAlign="Right" ColumnGroupName="PickupDetails"
                                                         Aggregate="Sum" FooterAggregateFormatString="{0:#,##0.00}" FooterStyle-HorizontalAlign="Right" HeaderStyle-Width="98px" />
                                <telerik:GridBoundColumn DataField="OriginNetUnits" UniqueName="OriginNetUnits" HeaderText="Net Vol" SortExpression="OriginNetUnits"
                                                         ReadOnly="true" ForceExtractValue="Always" DataFormatString="{0:0.000}" ItemStyle-HorizontalAlign="Right" ColumnGroupName="PickupDetails"
                                                         Aggregate="Sum" FooterAggregateFormatString="{0:#,##0.00}" FooterStyle-HorizontalAlign="Right" HeaderStyle-Width="95px" />
                                <telerik:GridBoundColumn DataField="Destination" UniqueName="Destination" HeaderText="Destination" SortExpression="Destination"
                                                         ReadOnly="true" ForceExtractValue="Always" ColumnGroupName="DeliveryDetails">
                                    <HeaderStyle Width="300px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="DestinationStateAbbrev" UniqueName="DestinationStateAbbrev" HeaderText="State" SortExpression="DestinationStateAbbrev"
                                                         ReadOnly="true" ForceExtractValue="Always" ColumnGroupName="DeliveryDetails"
                                                         Visible="false" Display="true">
                                    <HeaderStyle Width="60px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="DestStation" HeaderText="Station" SortExpression="DestStation"
                                                         FilterControlWidth="70%" UniqueName="DestStation" ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="DeliveryDetails"
                                                         Visible="false" Display="true">
                                    <HeaderStyle Width="90px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="DestTimeZone" UniqueName="DestTimeZone" HeaderText="TZ" SortExpression="DestTimeZone"
                                                         ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="DeliveryDetails"
                                                         Visible="false" Display="true">
                                    <HeaderStyle Width="35px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridDateTimeColumn DataField="DestArriveTime" UniqueName="DestArriveTime" HeaderText="Unload Start" SortExpression="DestArriveTime"
                                                            DataFormatString="{0:hh:mm tt}" ReadOnly="true" ForceExtractValue="Always" ColumnGroupName="DeliveryDetails"
                                                            Visible="false" Display="true">
                                    <HeaderStyle Width="75px" />
                                </telerik:GridDateTimeColumn>
                                <telerik:GridDateTimeColumn DataField="DestDepartTime" UniqueName="DestDepartTime" HeaderText="Unload Stop" SortExpression="DestDepartTime"
                                                            DataFormatString="{0:hh:mm tt}" ReadOnly="true" ForceExtractValue="Always" ColumnGroupName="DeliveryDetails"
                                                            Visible="false" Display="true">
                                    <HeaderStyle Width="75px" />
                                </telerik:GridDateTimeColumn>
                                <telerik:GridBoundColumn DataField="DestMinutes" UniqueName="DestMinutes" HeaderText="Unload Time" SortExpression="DestMinutes"
                                                         ReadOnly="true" ForceExtractValue="InBrowseMode" DataType="System.Int16" ColumnGroupName="DeliveryDetails"
                                                         Visible="false" Display="true">
                                    <HeaderStyle Width="85px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="DestWaitNotes" UniqueName="DestWaitNotes" HeaderText="Wait Notes" SortExpression="DestWaitNotes"
                                                         ReadOnly="true" ForceExtractValue="Always" DataType="System.String" ConvertEmptyStringToNull="false" ColumnGroupName="DeliveryDetails">
                                    <HeaderStyle Width="300px" />
                                    <ItemStyle Wrap="true" />
                                </telerik:GridBoundColumn>
                                <telerik:GridCheckBoxColumn DataField="DestChainup" UniqueName="DestChainup" HeaderText="Chain-Up?" SortExpression="DestChainup"
                                                        ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="DeliveryDetails" 
                                                        Visible="false" Display="true" >
                                    <HeaderStyle Width="60px" />
                                </telerik:GridCheckBoxColumn>
                                <telerik:GridBoundColumn DataField="DestBOLNum" UniqueName="DestBOLNum" HeaderText="BOL" SortExpression="DestBOLNum"
                                                         ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="DeliveryDetails"
                                                         Visible="false" Display="true">
                                    <HeaderStyle Width="90px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="RerouteNotes" UniqueName="RerouteNotes" HeaderText="Reroute Notes" SortExpression="RerouteNotes"
                                                         ReadOnly="true" ForceExtractValue="Always" DataType="System.String" ColumnGroupName="RouteDetails"
                                                         Visible="false" Display="true">
                                    <ItemStyle Wrap="true" />
                                    <HeaderStyle Width="100px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="PreviousDestinations" UniqueName="PreviousDestinations" HeaderText="Previous Destinations" SortExpression="PreviousDestinations"
                                                         ReadOnly="true" ForceExtractValue="Always" ColumnGroupName="RouteDetails">
                                    <HeaderStyle Width="300px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ActualMiles" UniqueName="ActualMiles" HeaderText="Rate Miles" SortExpression="ActualMiles"
                                                         ReadOnly="true" ForceExtractValue="Always" DataType="System.Int16" ItemStyle-HorizontalAlign="Right" ColumnGroupName="RouteDetails">
                                    <HeaderStyle Width="80px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="TicketType" UniqueName="TicketType" HeaderText="Ticket Type" SortExpression="TicketType"
                                                         ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="TicketDetails"
                                                         Visible="false" Display="true">
                                    <HeaderStyle Width="85px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="TicketNums" UniqueName="TicketNums" HeaderText="Tickets" SortExpression="TicketNum"
                                                         ReadOnly="true" ForceExtractValue="Always" ColumnGroupName="TicketDetails">
                                    <HeaderStyle Width="85px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="TankNums" UniqueName="TankNums" HeaderText="Tank #" SortExpression="TankNums"
                                                         ReadOnly="true" ForceExtractValue="Always" ColumnGroupName="TicketDetails"
                                                         Visible="false" Display="true">
                                    <HeaderStyle Width="85px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridNumericColumn DataField="InvoiceH2SRate" HeaderText="H2S Rate ($)" SortExpression="InvoiceH2SRate" DecimalDigits="4" DataFormatString="{0:$#,##0.0000}" NumericType="Currency"
                                                           FilterControlWidth="70%" UniqueName="InvoiceH2SRate" ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="InvoiceDetails"
                                                           Visible="false" Display="true">
                                    <HeaderStyle Width="90px" HorizontalAlign="Right" />
                                    <ItemStyle HorizontalAlign="Right" />
                                </telerik:GridNumericColumn>
                                <telerik:GridNumericColumn DataField="InvoiceH2SAmount" HeaderText="H2S Fee $$" SortExpression="InvoiceH2SAmount" DecimalDigits="4" DataFormatString="{0:$#,##0.0000}" NumericType="Currency"
                                                           FilterControlWidth="70%" UniqueName="InvoiceH2SAmount" ForceExtractValue="InBrowseMode" ColumnGroupName="InvoiceDetails"
                                                           Aggregate="Sum" FooterAggregateFormatString="{0:$#,##0.00}" FooterStyle-HorizontalAlign="Right">
                                    <HeaderStyle Width="90px" HorizontalAlign="Right" />
                                    <ItemStyle HorizontalAlign="Right" Font-Bold="true" />
                                </telerik:GridNumericColumn>
                                <telerik:GridNumericColumn DataField="InvoiceTaxRate" HeaderText="Tax Rate (%)" SortExpression="InvoiceTaxRate" DecimalDigits="4" DataFormatString="{0:0.0000}"
                                                           FilterControlWidth="70%" UniqueName="InvoiceTaxRate" NumericType="Percent" ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="InvoiceDetails">
                                    <HeaderStyle Width="90px" HorizontalAlign="Right" />
                                    <ItemStyle HorizontalAlign="Right" />
                                </telerik:GridNumericColumn>
                                <telerik:GridNumericColumn DataField="InvoiceOriginChainupAmount" UniqueName="InvoiceOriginChainupAmount" HeaderText="Origin Chain-Up $$" SortExpression="InvoiceOriginChainupAmount"
                                                           DataType="System.Decimal" DecimalDigits="4" DataFormatString="{0:$#,##0.0000}" NumericType="Currency" HeaderStyle-Width="80px" 
                                                           ItemStyle-HorizontalAlign="Right" ItemStyle-Font-Bold="true" ColumnGroupName="InvoiceDetails" ForceExtractValue="Always"
                                                           Visible="false" Display="true" 
                                                           Aggregate="Sum" FooterAggregateFormatString="{0:$#,##0.00}" FooterStyle-HorizontalAlign="Right" />
                                <telerik:GridNumericColumn DataField="InvoiceDestChainupAmount" UniqueName="InvoiceDestChainupAmount" HeaderText="Dest Chain-Up $$" SortExpression="InvoiceDestChainupAmount"
                                                           DataType="System.Decimal" DecimalDigits="4" DataFormatString="{0:$#,##0.0000}" NumericType="Currency" HeaderStyle-Width="80px"
                                                           ItemStyle-HorizontalAlign="Right" ItemStyle-Font-Bold="true" ColumnGroupName="InvoiceDetails" ForceExtractValue="Always"
                                                           Visible="false" Display="true"
                                                           Aggregate="Sum" FooterAggregateFormatString="{0:$#,##0.00}" FooterStyle-HorizontalAlign="Right" />
                                <telerik:GridNumericColumn DataField="InvoiceRerouteAmount" UniqueName="InvoiceRerouteAmount" HeaderText="Reroute $$" SortExpression="InvoiceRerouteAmount"
                                                           DataType="System.Decimal" DecimalDigits="4" DataFormatString="{0:$#,##0.0000}" NumericType="Currency" HeaderStyle-Width="80px"
                                                           ItemStyle-HorizontalAlign="Right" ItemStyle-Font-Bold="true" ColumnGroupName="InvoiceDetails" ForceExtractValue="Always"
                                                           Aggregate="Sum" FooterAggregateFormatString="{0:$#,##0.00}" FooterStyle-HorizontalAlign="Right" />
                                <telerik:GridNumericColumn DataField="InvoiceOrderRejectAmount" UniqueName="InvoiceOrderRejectAmount" HeaderText="Reject $$" SortExpression="InvoiceOrderRejectAmount"
                                                           DataType="System.Decimal" DecimalDigits="4" DataFormatString="{0:$#,##0.0000}" NumericType="Currency" HeaderStyle-Width="80px"
                                                           ItemStyle-HorizontalAlign="Right" ItemStyle-Font-Bold="true" ColumnGroupName="InvoiceDetails" ForceExtractValue="Always"
                                                           Aggregate="Sum" FooterAggregateFormatString="{0:$#,##0.00}" FooterStyle-HorizontalAlign="Right" />
                                <telerik:GridBoundColumn DataField="TotalMinutes" UniqueName="TotalMinutes" HeaderText="Total Load/Unload Time" SortExpression="TotalMinutes"
                                                         ReadOnly="true" ForceExtractValue="InBrowseMode" DataType="System.Int16" ItemStyle-HorizontalAlign="Right" ColumnGroupName="InvoiceDetails"
                                                         Visible="false" Display="true">
                                    <HeaderStyle Width="145px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="InvoiceWaitFeeSubUnit" UniqueName="InvoiceWaitFeeSubUnit" HeaderText="Wait Fee Sub Unit" SortExpression="InvoiceWaitFeeSubUnit"
                                                         ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="InvoiceDetails"
                                                         Visible="false" Display="true">
                                    <HeaderStyle Width="105px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="InvoiceWaitFeeRoundingType" UniqueName="InvoiceWaitFeeRoundingType" HeaderText="Wait Fee Rounding" SortExpression="InvoiceWaitFeeRoundingType"
                                                         ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="InvoiceDetails"
                                                         Visible="false" Display="true">
                                    <HeaderStyle Width="115px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="InvoiceOriginWaitBillableMinutes" UniqueName="InvoiceOriginWaitBillableMinutes" HeaderText="Origin Billable Wait" SortExpression="InvoiceOriginWaitBillableMinutes"
                                                         ReadOnly="true" ForceExtractValue="InBrowseMode" DataType="System.Int16" ItemStyle-HorizontalAlign="Right" ColumnGroupName="InvoiceDetails"
                                                         Visible="false" Display="true">
                                    <HeaderStyle Width="125px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="InvoiceDestWaitBillableMinutes" UniqueName="InvoiceDestWaitBillableMinutes" HeaderText="Dest Billable Wait" SortExpression="InvoiceDestWaitBillableMinutes"
                                                         ReadOnly="true" ForceExtractValue="InBrowseMode" DataType="System.Int16" ItemStyle-HorizontalAlign="Right" ColumnGroupName="InvoiceDetails"
                                                         Visible="false" Display="true">
                                    <HeaderStyle Width="105px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="InvoiceTotalWaitBillableMinutes" UniqueName="InvoiceTotalWaitBillableMinutes" HeaderText="Total Billable Wait Time" SortExpression="InvoiceTotalWaitBillableMinutes"
                                                         ReadOnly="true" ForceExtractValue="InBrowseMode" DataType="System.Int16" ItemStyle-HorizontalAlign="Right" ColumnGroupName="InvoiceDetails"
                                                         Visible="false" Display="true">
                                    <HeaderStyle Width="105px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridNumericColumn DataField="InvoiceOriginWaitRate" UniqueName="InvoiceOriginWaitRate" HeaderText="Origin Wait Rate" SortExpression="InvoiceOriginWaitRate"
                                                           DataType="System.Decimal" DecimalDigits="4" DataFormatString="{0:$#,##0.0000}" NumericType="Currency" HeaderStyle-Width="120px"
                                                           ItemStyle-HorizontalAlign="Right" ReadOnly="true" ColumnGroupName="InvoiceDetails" />
                                <telerik:GridNumericColumn DataField="InvoiceOriginWaitAmount" UniqueName="InvoiceOriginWaitAmount" HeaderText="Origin Wait $$" SortExpression="InvoiceOriginWaitAmount"
                                                           DataType="System.Decimal" DecimalDigits="4" DataFormatString="{0:$#,##0.0000}" NumericType="Currency" HeaderStyle-Width="100px"
                                                           ItemStyle-HorizontalAlign="Right" ItemStyle-Font-Bold="true" ColumnGroupName="InvoiceDetails" ForceExtractValue="Always"
                                                           Aggregate="Sum" FooterAggregateFormatString="{0:$#,##0.00}" FooterStyle-HorizontalAlign="Right" />
                                <telerik:GridNumericColumn DataField="InvoiceDestinationWaitRate" UniqueName="InvoiceDestinationWaitRate" HeaderText="Destination Wait Rate" SortExpression="InvoiceDestinationWaitRate"
                                                           DataType="System.Decimal" DecimalDigits="4" DataFormatString="{0:$#,##0.0000}" NumericType="Currency" HeaderStyle-Width="140px"
                                                           ItemStyle-HorizontalAlign="Right" ReadOnly="true" ColumnGroupName="InvoiceDetails" />
                                <telerik:GridNumericColumn DataField="InvoiceDestinationWaitAmount" UniqueName="InvoiceDestinationWaitAmount" HeaderText="Dest Wait $$" SortExpression="InvoiceDestinationWaitAmount"
                                                           DataType="System.Decimal" DecimalDigits="4" DataFormatString="{0:$#,##0.0000}" NumericType="Currency" HeaderStyle-Width="80px"
                                                           ItemStyle-HorizontalAlign="Right" ItemStyle-Font-Bold="true" ColumnGroupName="InvoiceDetails" ForceExtractValue="Always"
                                                           Aggregate="Sum" FooterAggregateFormatString="{0:$#,##0.00}" FooterStyle-HorizontalAlign="Right" />
                                <telerik:GridNumericColumn DataField="InvoiceTotalWaitAmount" UniqueName="InvoiceTotalWaitAmount" HeaderText="Total Wait $$" SortExpression="InvoiceTotalWaitAmount"
                                                           DataType="System.Decimal" DecimalDigits="4" DataFormatString="{0:$#,##0.0000}" NumericType="Currency" HeaderStyle-Width="80px"
                                                           ItemStyle-HorizontalAlign="Right" ItemStyle-Font-Bold="true" ColumnGroupName="InvoiceDetails" ForceExtractValue="Always"
                                                           Aggregate="Sum" FooterAggregateFormatString="{0:$#,##0.00}" FooterStyle-HorizontalAlign="Right" />
                                <telerik:GridNumericColumn DataField="InvoiceFuelSurchargeRate" UniqueName="InvoiceFuelSurchargeRate" HeaderText="Fuel Surcharge Rate" SortExpression="InvoiceFuelSurchargeRate"
                                                           DataType="System.Decimal" DecimalDigits="4" DataFormatString="{0:$#,##0.0000}" NumericType="Currency" HeaderStyle-Width="140px"
                                                           ItemStyle-HorizontalAlign="Right" ReadOnly="true" ColumnGroupName="InvoiceDetails" />
                                <telerik:GridNumericColumn DataField="InvoiceFuelSurchargeAmount" UniqueName="InvoiceFuelSurchargeAmount" HeaderText="Fuel Surcharge Amount" SortExpression="InvoiceFuelSurchargeAmount"
                                                           DataType="System.Decimal" DecimalDigits="2" DataFormatString="{0:$#,##0.00}" NumericType="Currency" HeaderStyle-Width="140px"
                                                           ItemStyle-HorizontalAlign="Right" ReadOnly="true" ColumnGroupName="InvoiceDetails" />
                                <telerik:GridNumericColumn DataField="InvoiceSplitLoadAmount" UniqueName="InvoiceSplitLoadAmount" HeaderText="Split Load Amount" SortExpression="InvoiceSplitLoadAmount"
                                                           DataType="System.Decimal" DecimalDigits="2" DataFormatString="{0:$#,##0.00}" NumericType="Currency" HeaderStyle-Width="110px"
                                                           ItemStyle-HorizontalAlign="Right" ReadOnly="true" ColumnGroupName="InvoiceDetails" />
                                <telerik:GridTemplateColumn DataField="InvoiceOtherDetailsTSV" UniqueName="InvoiceOtherDetailsTSV" HeaderText="Misc Details" AllowSorting="false"
                                                            HeaderStyle-Width="220px" ColumnGroupName="InvoiceDetails" />
                                <telerik:GridTemplateColumn DataField="InvoiceOtherAmountsTSV" UniqueName="InvoiceOtherAmountsTSV" HeaderText="Misc $$" AllowSorting="false"
                                                            HeaderStyle-Width="80px" ColumnGroupName="InvoiceDetails" ItemStyle-Font-Bold="true" />
                                <telerik:GridBoundColumn DataField="InvoiceUom" UniqueName="InvoiceUom" HeaderText="Settlement UOM" SortExpression="InvoiceUom"
                                                         HeaderStyle-Width="100px" ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="InvoiceDetails"
                                                         Visible="false" Display="true" />
                                <telerik:GridBoundColumn DataField="InvoiceMinSettlementUnits" UniqueName="InvoiceMinSettlementUnits" HeaderText="Min Settle Vol" SortExpression="InvoiceMinSettlementUnits"
                                                         ReadOnly="true" ForceExtractValue="InBrowseMode" DataType="System.Decimal" ItemStyle-HorizontalAlign="Right" ColumnGroupName="InvoiceDetails"
                                                         Visible="false" Display="true">
                                    <HeaderStyle Width="100px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="InvoiceUnits" UniqueName="InvoiceUnits" HeaderText="Actual Units" SortExpression="InvoiceUnits"
                                                         ReadOnly="true" ForceExtractValue="InBrowseMode" DataType="System.Decimal" ItemStyle-HorizontalAlign="Right" ColumnGroupName="InvoiceDetails"
                                                         Visible="false" Display="true">
                                    <HeaderStyle Width="100px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn DataField="InvoiceRouteRate" UniqueName="InvoiceRouteRate" HeaderText="Route Rate" SortExpression="InvoiceRouteRate"
                                                            HeaderStyle-Width="90px" DataType="System.Decimal" ItemStyle-HorizontalAlign="Right" ReadOnly="true" ColumnGroupName="InvoiceDetails"
                                                            ForceExtractValue="Always">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hlRouteRate" runat="server" ForeColor="Blue"
                                                       Text='<%# String.Format("{0:$#,##0.0000}", Eval("InvoiceRouteRate")) %>'
                                                       NavigateUrl='<%# String.Format("~/Site/Financials/DriverRouteRates.aspx?customerid={0}&originid={1}&destinationid={2}&effectivedate={3:M/d/yy}", Eval("CustomerID"), Eval("OriginID"), Eval("DestinationID"), Eval("OrderDate")) %>'
                                                       ColumnGroupName="InvoiceDetails">
                                        </asp:HyperLink>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn DataField="InvoiceRateSheetRate" UniqueName="InvoiceRateSheetRate" HeaderText="RateSheet Rate" SortExpression="InvoiceRateSheetRate"
                                                            HeaderStyle-Width="90px" DataType="System.Decimal" ItemStyle-HorizontalAlign="Right" ReadOnly="true" ColumnGroupName="InvoiceDetails"
                                                            ForceExtractValue="Always">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hlRateSheetRate" runat="server" ForeColor="Blue"
                                                       Text='<%# String.Format("{0:$#,##0.0000}", Eval("InvoiceRateSheetRate")) %>'
                                                       NavigateUrl='<%# String.Format("~/Site/Financials/DriverRateSheets.aspx?orderid={0}", Eval("ID")) %>'
                                                       ColumnGroupName="InvoiceDetails">
                                        </asp:HyperLink>
                                    </ItemTemplate>
                                    <EditItemTemplate>&nbsp;</EditItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridNumericColumn DataField="InvoiceLoadAmount" UniqueName="InvoiceLoadAmount" HeaderText="Load $$" SortExpression="InvoiceLoadAmount"
                                                           DataType="System.Decimal" DecimalDigits="4" DataFormatString="{0:$#,##0.0000}" NumericType="Currency" HeaderStyle-Width="90px"
                                                           Aggregate="Sum" ItemStyle-HorizontalAlign="Right" FooterAggregateFormatString="{0:$#,##0.00}" FooterStyle-Font-Bold="true"
                                                           ForceExtractValue="Always"
                                                           FooterStyle-HorizontalAlign="Right" ItemStyle-Font-Bold="true" ColumnGroupName="InvoiceDetails" />
                                <telerik:GridNumericColumn DataField="InvoiceTotalAmount" UniqueName="InvoiceTotalAmount" HeaderText="Total $$" SortExpression="InvoiceTotalAmount"
                                                           Aggregate="Sum" ItemStyle-HorizontalAlign="Right" ReadOnly="true" ForceExtractValue="Always"
                                                           FooterAggregateFormatString="{0:$#,##0.00}" FooterStyle-HorizontalAlign="Right" FooterStyle-Font-Bold="true"
                                                           DataType="System.Decimal" DecimalDigits="4" DataFormatString="{0:$#,##0.0000}" NumericType="Currency" HeaderStyle-Width="90px" ItemStyle-Font-Bold="true"
                                                           ColumnGroupName="InvoiceDetails" />
                                <telerik:GridBoundColumn ReadOnly="true" HeaderText=" " HeaderStyle-Width="30px" UniqueName="Spacer" DataField="Spacer" />
                                <telerik:GridBoundColumn DataField="ID" UniqueName="ID" Visible="false" Display="false" DataType="System.Int32" ForceExtractValue="Always" ReadOnly="true" />
                            </Columns>
                            <EditFormSettings ColumnNumber="3">
                                <EditColumn FilterControlAltText="Filter EditCommandColumn column" ButtonType="ImageButton" CancelImageUrl="~/images/cancel.png"
                                            UpdateImageUrl="~/images/apply.png" InsertImageUrl="~/images/apply.png" />
                            </EditFormSettings>
                            <PagerStyle AlwaysVisible="True" />
                        </MasterTableView>
                        <HeaderStyle Wrap="False" />
                        <PagerStyle AlwaysVisible="True" />
                        <FilterMenu EnableImageSprites="False" />
                    </telerik:RadGrid>
                </div>
            </div>
        </div>
    </div>
    <telerik:RadScriptBlock runat="server">
        <script type="text/javascript">
            var fileDownloadCheckTimer, button, filterButton;
            function GetGridRowCount() {
                var items = $("#" + "<%= rgMain.ClientID %>").get_masterTableView().get_dataItems();
                return items.length;
            };
            function DisableFilterButton(sender) {
                DisableSender(filterButton = sender);
                DisableSender(sender);
            };
            function DisableSender(sender) {
                sender.set_enabled(false);
            };
            function WaitForExport(sender, args) {
                //disable button
                DisableSender(button = sender);

                fileDownloadCheckTimer = window.setInterval(function () {
                    //get cookie and compare
                    var cookieValue = readCookie("fileDownloadToken");
                    if (cookieValue == "done") {
                        finishDownload(false);
                    }
                }, 1000);
            };

            function finishDownload(refresh) {
                window.clearInterval(fileDownloadCheckTimer);
                //clear cookie
                eraseCookie("fileDownloadToken");
                //enable button
                button.set_enabled(true);
                // rebind the data
                if (refresh) {
                    $("#" + "<%= cmdFilter.ClientID %>").click();
                }
            };

            function createCookie(name, value, days) {
                var expires = "";
                if (days) {
                    var date = new Date();
                    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                    expires = "; expires=" + date.toGMTString();
                }
                document.cookie = name + "=" + value + expires + "; path=/";
            };

            function readCookie(name) {
                var nameEQ = name + "=";
                var ca = document.cookie.split(';');
                for (var i = 0; i < ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
                }
                return null;
            };

            function DeleteBatch() {
                debugger;
                // if false is returned then the subsequent POSTBACK operation will be terminated
                return confirm("Are you sure you want to unsettle the selected batch? \n \n WARNING: This action cannot be undone.");
            };

            $(document).ready(function () {
                $("#<%= txtBatchNum.ClientID %>").change(enableFilter);
                $("#<%= txtOrderNum.ClientID %>").change(enableFilter);
                $("#<%= ddlCarrier.ClientID %>").change(enableFilter);
                $("#<%= ddlDriverGroup.ClientID %>").change(enableFilter);
                $("#<%= ddlDriver.ClientID %>").change(enableFilter);
                $("#<%= rdpStart.ClientID %>").change(enableFilter);
                $("#<%= rdpEnd.ClientID %>").change(enableFilter);
                enableFilter();
            });
            function enableFilter()
            {
                // force to filter by batch, order, or at least a date range to limit results
                if ($("#<%= txtBatchNum.ClientID %>").val() != "" || $("#<%= txtOrderNum.ClientID %>").val() != "" || $("#<%= rdpStart.ClientID %>").val() != "" && $("#<%= rdpEnd.ClientID %>").val() != "") 
                    $("#<%= cmdFilter.ClientID %>").removeAttr('disabled');
                else
                    $("#<%= cmdFilter.ClientID %>").attr('disabled', 'disabled');
            }


        </script>
    </telerik:RadScriptBlock>
</asp:Content>

