﻿<%@ Page Title="Commodities" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProducerBatches.aspx.cs"
    Inherits="DispatchCrude.Site.Orders.ProducerBatches" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script>
        $("#ctl00_ctl00_EntityCaption").html("Batches");
    </script>
    <div class="tabbable TabRepaint">
        <ul class="nav nav-tabs" id="tabmenu" runat="server">
            <!--Tabs will print here (see code behind)-->
        </ul>
        <div class="tab-content">
            <div class="tab-pane in active">

                <div class="leftpanel">
                    <div class="tabbable">
                        <ul class="nav nav-tabs">
                            <li class="active tab-blue">
                                <a data-toggle="tab" href="#Filters" aria-expanded="true">Filters</a>
                            </li>
                            <li class="tab-yellow">
                                <a data-toggle="tab" href="#Batches" aria-expanded="true">Batches</a>
                            </li>
                            <li class="tab-green">
                                <a data-toggle="tab" href="#Export" aria-expanded="true">Export</a>
                            </li>
                        </ul>
                        <div id="leftTabs" class="tab-content">
                            <div id="Filters" class="tab-pane active">
                                <asp:Panel ID="panelGenerate" runat="server" DefaultButton="cmdFilter">
                                    <div class="Entry">
                                        <asp:Label ID="lblProducer" runat="server" Text="Producer" AssociatedControlID="ddlProducer" CssClass="Entry" />
                                        <asp:DropDownList CssClass="btn-xs" Width="100%" runat="server" ID="ddlProducer" DataTextField="FullName" DataValueField="ID" />
                                    </div>
                                    <div class="center">
                                        <div class="Entry floatLeft-date-row">
                                            <asp:Label ID="lblStartDate" runat="server" Text="Start Date" AssociatedControlID="rdpStart" CssClass="Entry" />
                                            <telerik:RadDatePicker ID="rdpStart" runat="server" Width="100px">
                                                <DateInput runat="server" DateFormat="M/d/yyyy" />
                                                <DatePopupButton Enabled="true" />
                                            </telerik:RadDatePicker>
                                        </div>
                                        <div class="Entry floatRight-date-row">
                                            <asp:Label ID="lblEndDate" runat="server" Text="End Date" AssociatedControlID="rdpEnd" CssClass="Entry" />
                                            <telerik:RadDatePicker ID="rdpEnd" runat="server" Width="100px">
                                                <DateInput runat="server" DateFormat="M/d/yyyy" />
                                                <DatePopupButton />
                                            </telerik:RadDatePicker>
                                        </div>
                                    </div>
                                    <br /><br /><br />
                                    <div class="clear"></div>
                                    <div class="spacer5px"></div>
                                    <div style="text-align: center;">
                                        <asp:Button CssClass="btn btn-blue shiny" ID="cmdFilter" runat="server" Text="Refresh"
                                                    UseSubmitBehavior="false" OnClientClicked="DisableFilterButton" OnClick="cmdFilter_Click" />
                                    </div>
                                </asp:Panel>
                            </div>
                            <div id="Batches" class="tab-pane">
                                <asp:Panel ID="panelSelectedCtrl" runat="server" DefaultButton="cmdUnsettle">
                                    <asp:Label ID="lblSelectionAction" runat="server" Text="Select Batch" style="font-weight:bold" />
                                    <div style="height: 2px;"></div>
                                    <asp:DropDownList CssClass="btn-xs" ID="ddlBatch" runat="server" DataTextField="FullName" DataValueField="ID"
                                                      AutoPostBack="true" OnSelectedIndexChanged="ddlBatch_SelectedIndexChanged" Width="100%" />
                                    <div class="spacer5px"></div>
                                    <div>
                                        <asp:TextBox ID="txtCreatedByUser" runat="server" ReadOnly="true" CssClass="floatLeft" Width="45%" PlaceHolder="Created By User"
                                                     ForeColor="Gray" />
                                        <asp:TextBox ID="txtCreateDate" runat="server" ReadOnly="true" CssClass="floatRight" Width="45%" PlaceHolder="Create Date"
                                                     ForeColor="Gray" />
                                    </div>
                                    <div style="height: 5px;" class="clear"></div>
                                    <asp:TextBox ID="txtInvoiceNum" runat="server" Width="97%" Locked="true" PlaceHolder="Invoice #" ForeColor="Gray" />
                                    <div style="height: 2px;" class="clear"></div>
                                    <asp:TextBox ID="txtNotes" runat="server" Width="97%" TextMode="MultiLine" Height="40px" Locked="true" PlaceHolder="Notes"
                                                 ForeColor="Gray" />
                                    <div style="text-align: center;">
                                        <asp:Button ID="cmdUnsettle" runat="server" Text="Unsettle Selected Batch" CssClass="btn btn-blue shiny"
                                                    OnClientClick="javascript:if(!confirm('This action will delete the selected batch and reset all its orders. Are you sure?')){return false;}"
                                                    UseSubmitBehavior="false" OnClientClicked="DeleteBatch" OnClick="cmdUnsettle_Click" />
                                    </div>
                                </asp:Panel>
                            </div>
                            <div id="Export" class="tab-pane">
                                <asp:Panel ID="panelExportActions" runat="server">
                                    <div class="Entry">
                                        <asp:Label ID="lblReportCenterSel" runat="server" Text="Report Center Selection" AssociatedControlID="rcbUserReport" CssClass="Entry" />
                                        <telerik:RadComboBox ID="rcbUserReport" runat="server" AllowCustomText="false" ShowDropDownOnTextboxClick="true"
                                                             DataSourceID="dsUserReport" DataTextField="Name" DataValueField="ID" MaxLength="30"
                                                             Width="100%"
                                                             OnItemDataBound="rcbUserReport_ItemDataBound" />
                                        <blac:DBDataSource ID="dsUserReport" runat="server"
                                                           SelectCommand="SELECT * FROM viewUserReportDefinition WHERE (UserNames IS NULL OR @UserName IN (SELECT Value FROM dbo.fnSplitCSV(UserNames))) ORDER BY Name">
                                            <SelectParameters>
                                                <asp:Parameter Name="UserName" DefaultValue="" DbType="String" />
                                            </SelectParameters>
                                        </blac:DBDataSource>
                                    </div>

                                    <div class="Entry">
                                        <asp:Label ID="lblFileName" runat="server" Text="Export File Name" AssociatedControlID="rtxFileName" CssClass="Entry" />
                                        <telerik:RadTextBox ID="rtxFileName" runat="server" MaxLength="100" Width="100%" ValidationGroup="vgFileName" />
                                        <asp:RequiredFieldValidator ID="rfvFileName" runat="server" ControlToValidate="rtxFileName"
                                                                    ValidationGroup="vgFileName" Text="Export file name is required." CssClass="NullValidator" Display="Dynamic" />
                                        <asp:RegularExpressionValidator ID="revFileName" runat="server" ControlToValidate="rtxFileName"
                                                                        ValidationExpression="^[a-zA-Z0-9_\s\-\.]+$"
                                                                        ValidationGroup="vgFileName" Text="Invalid file name was provided." CssClass="NullValidator" Display="Dynamic" />
                                        <asp:RegularExpressionValidator ID="revFileName_NoExtension" runat="server" ControlToValidate="rtxFileName"
                                                                        ValidationExpression="[^\.]+$"
                                                                        ValidationGroup="vgFileName" Text="Do not enter a filename extension (.xlsx will automatically be used)." CssClass="NullValidator" Display="Dynamic" />
                                    </div>
                                    <div class="spacer5px"></div>
                                    <div class="center">
                                        <asp:Button ID="cmdExport" runat="server" Text="Export" CssClass="NOAJAX btn btn-blue shiny"
                                                    CausesValidation="true" ValidationGroup="vgFileName"
                                                    UseSubmitBehavior="false" OnClick="cmdExport_Click" />
                                    </div>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="gridArea" style="height:100%">
                    <telerik:RadGrid ID="rgMain" runat="server" EnableHeaderContextMenu="False" Height="800" CssClass="GridRepaint" CellSpacing="0" GridLines="None"
                                     AllowSorting="false" ShowFooter="true" EnableViewState="false" EnableEmbeddedSkins="true" Skin="Vista" AlternatingItemStyle-BackColor="#dcf2fc"
                                     PageSize="100" AllowPaging="false" ShowStatusBar="false"
                                     OnDataBound="grid_DataBound" OnItemCommand="grid_ItemCommand" OnItemDataBound="grid_ItemDataBound">
                        <ClientSettings>
                            <Scrolling AllowScroll="True" UseStaticHeaders="True" FrozenColumnsCount="0" />
                        </ClientSettings>
                        <MasterTableView AutoGenerateColumns="False" DataKeyNames="ID" CommandItemDisplay="None">
                            <ColumnGroups>
                                <telerik:GridColumnGroup HeaderText="Order Details" Name="OrderDetails" HeaderStyle-HorizontalAlign="Center" />
                                <telerik:GridColumnGroup HeaderText="Settlement Details" Name="InvoiceDetails" HeaderStyle-HorizontalAlign="Center" />
                            </ColumnGroups>
                            <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                            <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                <HeaderStyle Width="20px"></HeaderStyle>
                            </RowIndicatorColumn>
                            <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                <HeaderStyle Width="20px"></HeaderStyle>
                            </ExpandCollapseColumn>
                            <Columns>
                                <telerik:GridBoundColumn DataField="InvoiceBatchNum" UniqueName="InvoiceBatchNum" HeaderText="Batch #" SortExpression="InvoiceBatchNum"
                                                         ReadOnly="true">
                                    <HeaderStyle Width="70px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="OrderNum" ReadOnly="true" SortExpression="OrderNum"
                                                         HeaderText="Order #" ColumnGroupName="OrderDetails" ForceExtractValue="Always">
                                    <HeaderStyle Width="65px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridDateTimeColumn DataField="OrderDate" UniqueName="OrderDate" HeaderText="Date" SortExpression="OrderDate"
                                                            DataFormatString="{0:M/d/yyyy}" ReadOnly="true" ForceExtractValue="Always" ColumnGroupName="OrderDetails">
                                    <HeaderStyle Width="70px" />
                                </telerik:GridDateTimeColumn>
                                <telerik:GridBoundColumn DataField="Producer" UniqueName="Producer" HeaderText="Producer" SortExpression="Producer"
                                                         ReadOnly="true" ForceExtractValue="Always" ColumnGroupName="OrderDetails"
                                                         Aggregate="Count" FooterStyle-HorizontalAlign="Right">
                                    <HeaderStyle Width="200px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Origin" UniqueName="Origin" HeaderText="Origin" SortExpression="Origin"
                                                         ReadOnly="true" ForceExtractValue="Always" ColumnGroupName="OrderDetails">
                                    <HeaderStyle Width="300px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="LeaseNum" UniqueName="LeaseNum" HeaderText="Lease #" SortExpression="LeaseNum"
                                                         ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="OrderDetails"
                                                         Visible="true" Display="true">
                                    <HeaderStyle Width="65px" />
                                </telerik:GridBoundColumn>

                                <telerik:GridBoundColumn DataField="InvoiceSettlementFactor" UniqueName="InvoiceSettlementFactor" HeaderText="Settle Unit" SortExpression="InvoiceSettlementFactor"
                                                         HeaderStyle-Width="110px" ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="InvoiceDetails"
                                                         Visible="true" Display="true" />
                                <telerik:GridNumericColumn DataField="InvoiceSettlementUnits" UniqueName="InvoiceSettlementUnits" HeaderText="Volume" SortExpression="InvoiceSettlementUnits"
                                                           HeaderStyle-Width="90px" ColumnGroupName="InvoiceDetails" DataFormatString="{0:#,##0.000}" />
                                <telerik:GridBoundColumn DataField="InvoiceSettlementUomShort" UniqueName="InvoiceSettlementUomShort" HeaderText="UOM" SortExpression="InvoiceSettlementUomShort"
                                                         HeaderStyle-Width="60px" ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="InvoiceDetails"
                                                         Visible="true" Display="true" />
                                <telerik:GridHyperLinkColumn DataTextField="InvoiceIndexPrice" DataTextFormatString="{0:$#,##0.0000}"
                                                             DataNavigateUrlFields="ID,InvoiceCommodityPurchasePricebookID" DataNavigateUrlFormatString="~/CommodityPricing/Pricebook?orderid={0}&pricebook={1}" Target="_blank"
                                                             HeaderText="Index Price" SortExpression="InvoiceIndexPrice"
                                                             FilterControlWidth="70%" UniqueName="IndexPrice" ColumnGroupName="InvoiceDetails"
                                                             Visible="true" Display="true">
                                    <HeaderStyle Width="90px" HorizontalAlign="Right" />
                                    <ItemStyle HorizontalAlign="Right" />
                                </telerik:GridHyperLinkColumn>
                                <telerik:GridBoundColumn DataField="InvoiceCommodityIndex" UniqueName="CommodityIndex" HeaderText="Index" SortExpression="InvoiceCommodityIndex"
                                                         HeaderStyle-Width="90px" ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="InvoiceDetails"
                                                         Visible="true" Display="true" />
                                <telerik:GridBoundColumn DataField="InvoiceCommodityMethod" UniqueName="CommodityMethod" HeaderText="Method" SortExpression="InvoiceCommodityMethod"
                                                         HeaderStyle-Width="70px" ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="InvoiceDetails"
                                                         Visible="true" Display="true" />
                                <telerik:GridDateTimeColumn DataField="InvoiceIndexStartDate" UniqueName="IndexStartDate" HeaderText="Index Start" SortExpression="InvoiceIndexStartDate"
                                                            DataFormatString="{0:M/d/yyyy}" ReadOnly="true" ForceExtractValue="Always" ColumnGroupName="InvoiceDetails">
                                    <HeaderStyle Width="70px" />
                                </telerik:GridDateTimeColumn>
                                <telerik:GridDateTimeColumn DataField="InvoiceIndexEndDate" UniqueName="IndexEndDate" HeaderText="Index End" SortExpression="InvoiceIndexEndDate"
                                                            DataFormatString="{0:M/d/yyyy}" ReadOnly="true" ForceExtractValue="Always" ColumnGroupName="InvoiceDetails">
                                    <HeaderStyle Width="70px" />
                                </telerik:GridDateTimeColumn>
                                <telerik:GridNumericColumn DataField="InvoiceCommodityPurchasePricebookPriceAmount" UniqueName="InvoiceCommodityPurchasePricebookPriceAmount" HeaderText="Price $$" SortExpression="InvoiceCommodityPurchasePricebookPriceAmount"
                                                           Aggregate="Sum" ItemStyle-HorizontalAlign="Right" ReadOnly="true" ForceExtractValue="Always"
                                                           FooterAggregateFormatString="{0:$#,##0.00}" FooterStyle-HorizontalAlign="Right" FooterStyle-Font-Bold="true"
                                                           DataType="System.Decimal" DecimalDigits="4" DataFormatString="{0:$#,##0.0000}" NumericType="Currency" HeaderStyle-Width="90px" ItemStyle-Font-Bold="true"
                                                           ColumnGroupName="InvoiceDetails" />
                                <telerik:GridBoundColumn DataField="InvoiceDeduct" UniqueName="InvoiceDeduct" HeaderText="Deduct" SortExpression="InvoiceSettlementDeduct"
                                                         HeaderStyle-Width="80px" ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="InvoiceDetails"
                                                         Visible="true" Display="true" />
                                <telerik:GridNumericColumn DataField="InvoiceCommodityPurchasePricebookDeductAmount" UniqueName="InvoiceCommodityPurchasePricebookDeductAmount" HeaderText="Deduct $$" SortExpression="InvoiceCommodityPurchasePricebookDeductAmount"
                                                           Aggregate="Sum" ItemStyle-HorizontalAlign="Right" ReadOnly="true" ForceExtractValue="Always"
                                                           FooterAggregateFormatString="{0:$#,##0.00}" FooterStyle-HorizontalAlign="Right" FooterStyle-Font-Bold="true"
                                                           DataType="System.Decimal" DecimalDigits="4" DataFormatString="{0:$#,##0.0000}" NumericType="Currency" HeaderStyle-Width="90px" ItemStyle-Font-Bold="true"
                                                           ColumnGroupName="InvoiceDetails" />
                                <telerik:GridBoundColumn DataField="InvoicePremium" UniqueName="InvoicePremium" HeaderText="Premium" SortExpression="InvoicePremium"
                                                         HeaderStyle-Width="80px" ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="InvoiceDetails"
                                                         Visible="true" Display="true" />
                                <telerik:GridBoundColumn DataField="InvoicePremiumDesc" UniqueName="InvoicePremiumDesc" HeaderText="Premium Desc" SortExpression="InvoicePremiumDesc"
                                                         HeaderStyle-Width="200px" ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="InvoiceDetails"
                                                         Visible="true" Display="true" />
                                <telerik:GridNumericColumn DataField="InvoiceCommodityPurchasePricebookPremiumAmount" UniqueName="InvoiceCommodityPurchasePricebookPremiumAmount" HeaderText="Premium $$" SortExpression="InvoiceCommodityPurchasePricebookPremiumAmount"
                                                           Aggregate="Sum" ItemStyle-HorizontalAlign="Right" ReadOnly="true" ForceExtractValue="Always"
                                                           FooterAggregateFormatString="{0:$#,##0.00}" FooterStyle-HorizontalAlign="Right" FooterStyle-Font-Bold="true"
                                                           DataType="System.Decimal" DecimalDigits="4" DataFormatString="{0:$#,##0.0000}" NumericType="Currency" HeaderStyle-Width="90px" ItemStyle-Font-Bold="true"
                                                           ColumnGroupName="InvoiceDetails" />
                                <telerik:GridNumericColumn DataField="InvoiceCommodityPurchasePricebookTotalAmount" UniqueName="InvoiceCommodityPurchasePricebookTotalAmount" HeaderText="Total $$" SortExpression="InvoiceCommodityPurchasePricebookTotalAmount"
                                                           Aggregate="Sum" ItemStyle-HorizontalAlign="Right" ReadOnly="true" ForceExtractValue="Always"
                                                           FooterAggregateFormatString="{0:$#,##0.00}" FooterStyle-HorizontalAlign="Right" FooterStyle-Font-Bold="true"
                                                           DataType="System.Decimal" DecimalDigits="4" DataFormatString="{0:$#,##0.0000}" NumericType="Currency" HeaderStyle-Width="90px" ItemStyle-Font-Bold="true"
                                                           ColumnGroupName="InvoiceDetails" />
                                <telerik:GridBoundColumn ReadOnly="true" HeaderText=" " HeaderStyle-Width="30px" UniqueName="Spacer" DataField="Spacer" />
                                <telerik:GridBoundColumn DataField="ID" UniqueName="ID" Visible="false" Display="false" DataType="System.Int32" ForceExtractValue="Always" ReadOnly="true" />
                            </Columns>
                            <EditFormSettings ColumnNumber="3">
                                <EditColumn FilterControlAltText="Filter EditCommandColumn column" ButtonType="ImageButton" CancelImageUrl="~/images/cancel.png"
                                            UpdateImageUrl="~/images/apply.png" InsertImageUrl="~/images/apply.png" />
                            </EditFormSettings>
                            <PagerStyle AlwaysVisible="True" />
                        </MasterTableView>
                        <HeaderStyle Wrap="False" />
                        <PagerStyle AlwaysVisible="True" />
                        <FilterMenu EnableImageSprites="False" />
                    </telerik:RadGrid>
                </div>
            </div>
        </div>
    </div>
    <telerik:RadScriptBlock runat="server">
        <script type="text/javascript">
            var fileDownloadCheckTimer, button, filterButton;
            function GetGridRowCount() {
                var items = $("#" + "<%= rgMain.ClientID %>").get_masterTableView().get_dataItems();
                return items.length;
            }
            function DisableFilterButton(sender) {
                DisableSender(filterButton = sender);
                DisableSender(sender);
            };
            function DisableSender(sender) {
                sender.set_enabled(false);
            };
            function WaitForExport(sender, args) {
                //disable button
                DisableSender(button = sender);

                fileDownloadCheckTimer = window.setInterval(function () {
                    //get cookie and compare
                    var cookieValue = readCookie("fileDownloadToken");
                    if (cookieValue == "done") {
                        finishDownload(false);
                    }
                }, 1000);
            };

            function finishDownload(refresh) {
                window.clearInterval(fileDownloadCheckTimer);
                //clear cookie
                eraseCookie("fileDownloadToken");
                //enable button
                button.set_enabled(true);
                // rebind the data
                if (refresh) {
                    $("#" + "<%= cmdFilter.ClientID %>").click();
                }
            };

            function createCookie(name, value, days) {
                var expires = "";
                if (days) {
                    var date = new Date();
                    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                    expires = "; expires=" + date.toGMTString();
                }
                document.cookie = name + "=" + value + expires + "; path=/";
            };

            function readCookie(name) {
                var nameEQ = name + "=";
                var ca = document.cookie.split(';');
                for (var i = 0; i < ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
                }
                return null;
            };

            function DeleteBatch() {
                debugger;
                // if false is returned then the subsequent POSTBACK operation will be terminated
                return confirm("Are you sure you want to unsettle the selected batch? \n \n WARNING: This action cannot be undone.");
            };

        </script>
    </telerik:RadScriptBlock>
</asp:Content>
