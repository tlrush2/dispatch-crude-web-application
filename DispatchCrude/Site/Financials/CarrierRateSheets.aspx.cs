﻿using System;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//add for sql stuff
using System.Data;
using System.Collections;
using System.Drawing;
using Telerik.Web.UI;
using AlonsIT;
using DispatchCrude.App_Code;
using DispatchCrude.Core;
using DispatchCrude.Extensions;

namespace DispatchCrude.Site.Financials
{
    public partial class CarrierRateSheets : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                rdpStartDate.DbSelectedDate = DateTime.Now.Date;
                rdpEndDate.DbSelectedDate = DBNull.Value;
                rdpStartDate.Calendar.ShowRowHeaders = rdpEndDate.Calendar.ShowRowHeaders = false;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ConfigureAjax(!Settings.SettingsID.DisableAJAX.AsBool());

            //Print tab navigation html to page
            tabmenu.InnerHtml = NavigationHelper.PrintTabArray(NavigationHelper.TabSet_RouteRates, "Tab_RateSheets").ToString();

            //Print button navigation html to page
            speedbuttons.InnerHtml = NavigationHelper.PrintButtonArray(NavigationHelper.ButtonSet_RateSheets, "Button_Carrier").ToString();

            dbcRateSheets.OnRowSelected += dbcRateSheets_RowSelected;
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            // Hide ID columns on the website
            rgRangeRates.MasterTableView.Columns.FindByUniqueName("ID").Display = false;
            rgRateSheets.MasterTableView.Columns.FindByUniqueName("ID").Display = false;

            if (!IsPostBack)
            {
                DataRow dr = null;
                if (Request.QueryString["OrderID"] != null)
                {
                    using (SSDB ssdb = new SSDB())
                    {
                        DataTable dt = ssdb.GetPopulatedDataTable("SELECT * FROM viewOrder WHERE ID = {0}", Request.QueryString["OrderID"] as object);
                        if (dt.Rows.Count == 1)
                            dr = dt.Rows[0];
                    }
                }

                if (dr != null || Request.QueryString["ShipperID"] != null)
                {
                    ddShipper.DataBind();
                    DropDownListHelper.SetSelectedValue(ddShipper, dr != null ? dr["CustomerID"].ToString() : Request.QueryString["ShipperID"]);
                }
                if (dr != null || Request.QueryString["CarrierID"] != null)
                {
                    ddCarrier.DataBind();
                    DropDownListHelper.SetSelectedValue(ddCarrier, dr != null ? dr["CarrierID"].ToString() : Request.QueryString["CarrierID"]);
                }
                if (dr != null || Request.QueryString["ProductGroupID"] != null)
                {
                    ddProductGroup.DataBind();
                    DropDownListHelper.SetSelectedValue(ddProductGroup, dr != null ? dr["ProductGroupID"].ToString() : Request.QueryString["ProductGroupID"]);
                }
                if (dr != null || Request.QueryString["DriverGroupID"] != null)
                {
                    ddDriverGroup.DataBind();
                    DropDownListHelper.SetSelectedValue(ddDriverGroup, dr != null ? dr["DriverGroupID"].ToString() : Request.QueryString["DriverGroupID"]);
                }
                if (dr != null || Request.QueryString["OriginStateID"] != null)
                {
                    ddOriginState.DataBind();
                    DropDownListHelper.SetSelectedValue(ddOriginState, dr != null ? dr["OriginStateID"].ToString() : Request.QueryString["OriginStateID"]);
                }
                if (dr != null || Request.QueryString["DestStateID"] != null)
                {
                    ddDestinationState.DataBind();
                    DropDownListHelper.SetSelectedValue(ddDestinationState, dr != null ? dr["DestStateID"].ToString() : Request.QueryString["DestStateID"]);
                }
                if (dr != null || Request.QueryString["RegionID"] != null)
                {
                    ddRegion.DataBind();
                    DropDownListHelper.SetSelectedValue(ddRegion, dr != null ? dr["OriginRegionID"].ToString() : Request.QueryString["RegionID"]);
                }
                if (dr != null || Request.QueryString["ProducerID"] != null)
                {
                    ddProducer.DataBind();
                    DropDownListHelper.SetSelectedValue(ddProducer, dr != null ? dr["ProducerID"].ToString() : Request.QueryString["ProducerID"]);
                }
                if (dr != null || Request.QueryString["EffectiveDate"] != null)
                {
                    DateTime effectiveDate = DateTime.Now.Date;
                    if (DateTime.TryParse(dr != null ? dr["OrderDate"].ToString() : Request.QueryString["EffectiveDate"], out effectiveDate))
                    {
                        rdpStartDate.SelectedDate = effectiveDate;
                    }
                }
                if (dr != null || Request.QueryString["RouteMiles"] != null)
                {
                    rcbRouteMiles.Text = dr != null ? dr["ActualMiles"].ToString() : Request.QueryString["RouteMiles"];
                }
                rgRateSheets.Rebind();
                if (rgRateSheets.SelectedIndexes.Count == 0 && rgRateSheets.Items.Count > 0)
                {
                    rgRateSheets.Items[0].Selected = true;
                    rgRangeRates.Rebind();
                }
            }
        }

        private void ConfigureAjax(bool enabled = true)
        {
            if (enabled)
            {
                RadAjaxHelper.AddAjaxSetting(this.Page, btnRefresh, rgRateSheets, true);
                RadAjaxHelper.AddAjaxSetting(this.Page, rgRateSheets, rgRateSheets);
                RadAjaxHelper.AddAjaxSetting(this.Page, rgRateSheets, rgRangeRates);
                RadAjaxHelper.AddAjaxSetting(this.Page, btnRefresh, rgRangeRates, true);
                RadAjaxHelper.AddAjaxSetting(this.Page, rgRangeRates, rgRangeRates);
            }
            RadAjaxManager.GetCurrent(this.Page).EnableAJAX = enabled;
        }

        protected void filterValueChanged(object sender, EventArgs e)
        {
            dbcRateSheets.RebindGrid();
            rgRangeRates.Rebind();
        }

        protected void gridRS_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.InitInsertCommandName)
            {
                // cancel the default operation
                e.Canceled = true;
                e.Item.OwnerTableView.InsertItem(GetNewRowValues(e.Item as GridDataItem));
            }
        }

        protected void dbcRateSheets_RowSelected(object sender, EventArgs e)
        {
            rgRangeRates.Rebind();
        }

        protected void gridRR_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.InitInsertCommandName)
            {
                // cancel the default operation
                e.Canceled = true;
                Hashtable newValues = new Hashtable();
                newValues.Add("RateSheetID", DBHelper.ToInt32(rgRateSheets.SelectedValue));
                e.Item.OwnerTableView.InsertItem(newValues);
            }
        }

        private Hashtable GetRowValues(GridEditableItem gdi)
        {
            //Prepare an IDictionary with the predefined values
            Hashtable ret = new Hashtable();
            gdi.ExtractValues(ret);
            
            DateTime date = DBHelper.ToDateTime(
                gdi["EffectiveDate"].Controls[1] is Label
                    ? (gdi["EffectiveDate"].Controls[1] as Label).Text
                    : (gdi["EffectiveDate"].Controls[1] as RadDatePicker).DbSelectedDate);
            ret["EffectiveDate"] = date.Date;
            ret["EndDate"] = DateTime.Now.Date.AddYears(1);
            return ret;
        }
        private Hashtable GetNewRowValues(GridEditableItem gdi)
        {
            Hashtable ret = new Hashtable();
            // set ID to DBNULL (since we are creating a new record)
            ret["ID"] = DBNull.Value;
            ret["ShipperID"] = 0;
            ret["CarrierID"] = 0;
            ret["ProductGroupID"] = 0;
            ret["DriverGroupID"] = 0;
            ret["OriginStateID"] = 0;
            ret["DestStateID"] = 0;
            ret["RegionID"] = 0;
            ret["ProducerID"] = 0;
            ret["RateTypeID"] = 1;
            ret["UomID"] = 1;
            // default the new Effective Date to the first day of the current month
            ret["EffectiveDate"] = DateTime.Now.Date.AddDays(1 - DateTime.Now.Day);
            ret["EndDate"] = DateTime.Now.Date.AddYears(1);
            return ret;
        }

        protected void cmdExport_Click(object sender, EventArgs e)
        {
            ExportGridToExcel();
        }
        protected void cvUpload_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = Path.GetExtension(excelUpload.FileName).ToLower() == ".xlsx";
        }

        protected void cmdImport_Click(object sender, EventArgs e)
        {
            Page.Validate();
            if (Page.IsValid)
            {
                FinancialImporter fi = new FinancialImporter();
                fi.AddSpec(rgRangeRates, "ID", FinancialImporter.FISpec.FISType.ID);
                fi.AddSpec(rgRangeRates, "RateSheetID", FinancialImporter.FISpec.FISType.UPDATE);
                fi.AddSpec(rgRangeRates, "ShipperID", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgRangeRates, "CarrierID", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgRangeRates, "ProductGroupID", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgRangeRates, "TruckTypeID", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgRangeRates, "DriverGroupID", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgRangeRates, "OriginStateID", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgRangeRates, "DestStateID", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgRangeRates, "RegionID", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgRangeRates, "ProducerID", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgRangeRates, "MinRange", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgRangeRates, "MaxRange", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgRangeRates, "Rate", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgRangeRates, "RateTypeID", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgRangeRates, "UomID", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgRangeRates, "EffectiveDate", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgRangeRates, "EndDate", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec("CreatedByUser", typeof(string), FinancialImporter.FISpec.FISType.NEW | FinancialImporter.FISpec.FISType.USERNAME);
                fi.AddSpec("CreateDateUTC", typeof(DateTime), FinancialImporter.FISpec.FISType.NEW | FinancialImporter.FISpec.FISType.NOW);
                fi.AddSpec("LastChangedByUser", typeof(string), FinancialImporter.FISpec.FISType.UPDATE | FinancialImporter.FISpec.FISType.USERNAME);
                fi.AddSpec("LastChangeDateUTC", typeof(DateTime), FinancialImporter.FISpec.FISType.UPDATE | FinancialImporter.FISpec.FISType.NOW);
                fi.AddSpec(rgRangeRates, "ImportAction", FinancialImporter.FISpec.FISType.ACTION);
                fi.AddSpec(rgRangeRates, "ImportOutcome", FinancialImporter.FISpec.FISType.OUTCOME);
                Response.ExportExcelStream(fi.ProcessSql(excelUpload.FileContent, "viewCarrierRateSheetRangeRate"), Path.GetFileNameWithoutExtension(excelUpload.FileName) + "_ImportResults.xlsx");
            }
        }

        protected void CellBackColorChanged(GridDataItem gridRow, string colName, ref Color color)
        {
            switch (colName.ToLower())
            {
                case "shipperid":
                case "carrierid":
                case "productgroupid":
                case "trucktypeid":
                case "drivergroupid":
                case "originstateid":
                case "deststateid":
                case "regionid":
                case "producerid":
                case "minrange":
                case "maxrange":
                case "ratetypeid":
                case "rate":
                case "uomid":
                case "effectivedate":
                {
                    CheckBox chkLocked = RadGridHelper.GetControlByType(gridRow, "Locked", typeof(CheckBox)) as CheckBox;
                    if (chkLocked != null && chkLocked.Checked)
                        color = Color.White;
                    else
                        color = Color.LightGreen;
                    break;
                }
                case "enddate":
                    color = Color.LightGreen;
                    break;
            }
        }

        private void ExportGridToExcel()
        {
            string filename = string.Format("Carrier Rate Sheet as of {0:yyyyMMdd}.xlsx", rdpStartDate.SelectedDate);
            string[] hiddenToInclude = { "ID", "RateSheetID", "ImportAction", "ImportOutcome", "EffectiveDate", "EndDate" }
                , visibleToSkip = { "Validation", "CreateDate", "CreatedByUser", "LastChangeDate", "LastChangedByUser" };
            RadGridExcelExporter exporter = new RadGridExcelExporter(
                    hiddenColNamesToInclude: hiddenToInclude
                    , visibleColNamesToSkip: visibleToSkip
                    , dropDownColumnDataValidationList: true);
            exporter.OnCellBackColorChanged += CellBackColorChanged;
            rgRangeRates.AllowPaging = false;
            rgRangeRates.Rebind();
            Response.ExportExcelStream(exporter.ExportSheet(rgRangeRates.MasterTableView, "RateSheet"), filename);
        }

        protected void gridRS_ItemDataBound(object sender, GridItemEventArgs e)
        {
            DataRowView data = e.Item.DataItem as DataRowView;
            bool locked = data != null && DBHelper.ToBoolean(data["Locked"]);
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                if (data != null)
                {
                    DateTime? date = null;
                    if ((date = Converter.ToNullableDateTime(data["PriorEndDate"])).HasValue)
                        RadGridHelper.GetColumnDatePicker(e.Item, "EffectiveDate").MinDate = date.Value;
                    if ((date = Converter.ToNullableDateTime(data["MaxEffectiveDate"])).HasValue)
                        RadGridHelper.GetColumnDatePicker(e.Item, "EffectiveDate").MaxDate = date.Value;
                    if ((date = Converter.ToNullableDateTime(data["MinEndDate"])).HasValue)
                        RadGridHelper.GetColumnDatePicker(e.Item, "EndDate").MinDate = date.Value;
                    if ((date = Converter.ToNullableDateTime(data["NextEffectiveDate"])).HasValue)
                        RadGridHelper.GetColumnDatePicker(e.Item, "EndDate").MaxDate = date.Value;
                }

                RadGridHelper.GetColumnRadComboBox(e.Item, "ShipperID").Enabled = !locked;
                RadGridHelper.GetColumnRadComboBox(e.Item, "CarrierID").Enabled = !locked;
                RadGridHelper.GetColumnRadComboBox(e.Item, "ProductGroupID").Enabled = !locked;
                RadGridHelper.GetColumnRadComboBox(e.Item, "TruckTypeID").Enabled = !locked;
                RadGridHelper.GetColumnRadComboBox(e.Item, "DriverGroupID").Enabled = !locked;
                RadGridHelper.GetColumnRadComboBox(e.Item, "OriginStateID").Enabled = !locked;
                RadGridHelper.GetColumnRadComboBox(e.Item, "DestStateID").Enabled = !locked;
                RadGridHelper.GetColumnRadComboBox(e.Item, "RegionID").Enabled = !locked;
                RadGridHelper.GetColumnRadComboBox(e.Item, "ProducerID").Enabled = !locked;
                (RadGridHelper.GetControlByType(e.Item, "EffectiveDate", typeof(RadDatePicker)) as RadDatePicker).Enabled = !locked;
            }
            else if (e.Item is GridDataItem && !e.Item.IsInEditMode)
            {
                // highlight best match records with a yellow background
                if (DBHelper.ToBoolean(data["BestMatch"])) e.Item.BackColor = Color.Goldenrod;

                ImageButton btnDelete = (ImageButton)RadGridHelper.GetColumnControlByType((e.Item as GridDataItem)["ActionColumn"], typeof(ImageButton), "btnDelete");
                if (btnDelete != null)
                {
                    btnDelete.Visible = btnDelete.Enabled = !locked;
                }
            }
        }

        protected void gridRR_ItemDataBound(object sender, GridItemEventArgs e)
        {
            DataRowView data = e.Item.DataItem as DataRowView;
            bool locked = data != null && DBHelper.ToBoolean(data["Locked"]);
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                (RadGridHelper.GetControlByType(e.Item, "MinRange", typeof(RadNumericTextBox)) as RadNumericTextBox).Enabled = !locked;
                (RadGridHelper.GetControlByType(e.Item, "MaxRange", typeof(RadNumericTextBox)) as RadNumericTextBox).Enabled = !locked;
                (RadGridHelper.GetControlByType(e.Item, "Rate", typeof(RadNumericTextBox)) as RadNumericTextBox).Enabled = !locked;
            }
            else if (e.Item is GridDataItem && !e.Item.IsInEditMode)
            {
                // highlight best match records with a yellow background
                if (DBHelper.ToBoolean(data["BestMatch"])) e.Item.BackColor = Color.Goldenrod;

                ImageButton btnDelete = (ImageButton)RadGridHelper.GetColumnControlByType((e.Item as GridDataItem)["ActionColumn"], typeof(ImageButton), "btnDelete");
                if (btnDelete != null)
                {
                    btnDelete.Visible = btnDelete.Enabled = !locked;
                }
            }
        }

        protected void gridRR_DataBound(object sender, EventArgs e)
        {
            if (rgRateSheets.SelectedIndexes.Count == 0)
                RadGridHelper.DisableAddNewRecordButton(rgRangeRates);
        }
    }
}