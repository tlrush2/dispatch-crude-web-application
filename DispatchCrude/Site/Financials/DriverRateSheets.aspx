﻿<%@  Title="Route Rates" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DriverRateSheets.aspx.cs"
    Inherits="DispatchCrude.Site.Financials.DriverRateSheets" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="pageHeadContent" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="/styles/radgrid_rates.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="pageMainContent" ContentPlaceHolderID="MainContent" runat="server">
    <style type="text/css">
        .compositeSplitter {
            margin: 0 auto;
        }
    </style>
    <telerik:GridNumericColumnEditor ID="num10DigitEditor" runat="server">
        <NumericTextBox runat="server" EnabledStyle-HorizontalAlign="Right" MinValue="0" MaxValue="9999">
            <NumberFormat AllowRounding="true" DecimalDigits="10" KeepNotRoundedValue="true" KeepTrailingZerosOnFocus="false" />
        </NumericTextBox>
    </telerik:GridNumericColumnEditor>
    <script>
        $("#ctl00_ctl00_EntityCaption").html("Driver Rate Sheets");
    </script>
    <div class="tabbable TabRepaint">
        <ul class="nav nav-tabs" id="tabmenu" runat="server">
            <!--Tabs will print here (see code behind)-->
        </ul>
        <div class="tab-content">
            <div class="tab-pane in active">

                <div id="speedbuttons" class="well speedButtonContainer" runat="server">
                    <!--Buttons will print here (see code behind)-->
                </div>

                <div class="leftpanel">
                    <div class="tabbable">
                        <ul class="nav nav-tabs">
                            <li class="active tab-blue">
                                <a data-toggle="tab" href="#Filters" aria-expanded="true">Filters</a>
                            </li>
                            <li class="tab-green">
                                <a data-toggle="tab" href="#Export" aria-expanded="true">Export</a>
                            </li>
                        </ul>
                        <div id="leftTabs" class="tab-content">
                            <div id="Filters" class="tab-pane active">
                                <asp:Panel ID="panelFilter" runat="server" DefaultButton="btnRefresh">
                                    <div class="Entry">
                                        <asp:Label ID="lblTruckType" runat="server" Text="Truck Type" AssociatedControlID="ddTruckType" />
                                        <asp:DropDownList CssClass="btn-xs" Width="100%" runat="server" ID="ddTruckType" DataTextField="Name" DataValueField="ID" DataSourceID="dsTruckType" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblShipper" runat="server" Text="Shipper" AssociatedControlID="ddShipper" />
                                        <asp:DropDownList CssClass="btn-xs" Width="100%" runat="server" ID="ddShipper" DataTextField="Name" DataValueField="ID" DataSourceID="dsShipper" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblCarrier" runat="server" Text="Carrier" AssociatedControlID="ddCarrier" />
                                        <asp:DropDownList CssClass="btn-xs" Width="100%" runat="server" ID="ddCarrier" DataTextField="Name" DataValueField="ID" DataSourceID="dsCarrier" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblProductGroup" runat="server" Text="Product Group" AssociatedControlID="ddProductGroup" />
                                        <asp:DropDownList CssClass="btn-xs" Width="100%" runat="server" ID="ddProductGroup" DataTextField="Name" DataValueField="ID" DataSourceID="dsProductGroup" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblDriverGroup" runat="server" Text="Driver Group" AssociatedControlID="ddDriverGroup" />
                                        <asp:DropDownList CssClass="btn-xs" Width="100%" runat="server" ID="ddDriverGroup" DataTextField="Name" DataValueField="ID" DataSourceID="dsDriverGroup" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblDriver" runat="server" Text="Driver" AssociatedControlID="ddDriver" />
                                        <asp:DropDownList CssClass="btn-xs" Width="100%" runat="server" ID="ddDriver" DataTextField="Name" DataValueField="ID" DataSourceID="dsDriver" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblOriginState" runat="server" Text="Origin State" AssociatedControlID="ddOriginState" />
                                        <asp:DropDownList CssClass="btn-xs" Width="100%" runat="server" ID="ddOriginState" DataTextField="Name" DataValueField="ID" DataSourceID="dsState" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblDestinationState" runat="server" Text="Destination State" AssociatedControlID="ddDestinationState" />
                                        <asp:DropDownList CssClass="btn-xs" Width="100%" runat="server" ID="ddDestinationState" DataTextField="Name" DataValueField="ID" DataSourceID="dsState" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblRegion" runat="server" Text="Region" AssociatedControlID="ddRegion" />
                                        <asp:DropDownList CssClass="btn-xs" Width="100%" runat="server" ID="ddRegion" DataTextField="Name" DataValueField="ID" DataSourceID="dsRegion" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblProducer" runat="server" Text="Producer" AssociatedControlID="ddProducer" />
                                        <asp:DropDownList CssClass="btn-xs" Width="100%" runat="server" ID="ddProducer" DataTextField="Name" DataValueField="ID" DataSourceID="dsProducer" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblRouteMiles" runat="server" Text="Route Miles" AssociatedControlID="rcbRouteMiles" />
                                        <telerik:RadComboBox ID="rcbRouteMiles" runat="server" Width="100%"
                                                             AllowCustomText="true" Filter="StartsWith" ShowDropDownOnTextboxClick="true">
                                            <Items>
                                                <telerik:RadComboBoxItem Value="-1" Text="(All)" Selected="true" />
                                            </Items>
                                        </telerik:RadComboBox>
                                    </div>
                                    <div>
                                        <div class="Entry floatLeft-date-row">
                                            <asp:Label ID="lblStartDate" runat="server" Text="Start Date" AssociatedControlID="rdpStartDate" CssClass="Entry" />
                                            <telerik:RadDatePicker ID="rdpStartDate" runat="server" Width="100px">
                                                <DateInput runat="server" DateFormat="M/d/yyyy" />
                                                <DatePopupButton Enabled="true" />
                                            </telerik:RadDatePicker>
                                        </div>
                                        <div class="Entry floatRight-date-row">
                                            <asp:Label ID="lblEndDate" runat="server" Text="End Date" AssociatedControlID="rdpEndDate" CssClass="Entry" />
                                            <telerik:RadDatePicker ID="rdpEndDate" runat="server" Width="100px">
                                                <DateInput runat="server" DateFormat="M/d/yyyy" />
                                                <DatePopupButton Enabled="true" />
                                            </telerik:RadDatePicker>
                                        </div>
                                        <br /><br /><br />
                                        <div class="center">
                                            <asp:Button ID="btnRefresh" runat="server" Text="Refresh" CssClass="btn btn-blue shiny" OnClick="filterValueChanged" />
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                            <div id="Export" class="tab-pane">
                                <asp:Panel ID="panelExcel" runat="server" DefaultButton="cmdExport">
                                    <div>
                                        <div>
                                            <asp:Button ID="cmdExport" runat="server" Text="Export to Excel" Enabled="true" CssClass="btn btn-blue shiny" OnClick="cmdExport_Click" />
                                        </div>
                                        <div class="spacer10px"></div>
                                        <div>
                                            <asp:Button ID="cmdImport" runat="server" ClientIDMode="Static" Text="Import Excel file" CssClass="floatRight btn btn-blue shiny"
                                                        Enabled="false" OnClick="cmdImport_Click" />
                                        </div>
                                    </div>
                                    <div class="spacer10px"></div>
                                    <div class="center">
                                        <asp:FileUpload ID="excelUpload" runat="server" ClientIDMode="Static" CssClass="floatLeft" Width="99%" />
                                        <asp:CustomValidator ID="cvUpload" runat="server" ControlToValidate="excelUpload" CssClass="NullValidator floatLeft" Display="Dynamic"
                                                             Text="*" ErrorMessage="Only Excel (*.xlsx) files allowed)" OnServerValidate="cvUpload_ServerValidate" />
                                    </div>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="gridArea">
                    <div class="dual-grid-vertical">
                        <telerik:RadGrid ID="rgRateSheets" runat="server" EnableHeaderContextMenu="False" CellSpacing="0" GridLines="None" EnableLinqExpressions="false"
                                         AllowMultiRowSelection="false" EnableEmbeddedSkins="true" Skin="Vista" AlternatingItemStyle-BackColor="#dcf2fc"
                                         DataSourceID="dsRateSheets" AllowSorting="True" AllowFilteringByColumn="false" Height="400" CssClass="GridRepaint" ShowGroupPanel="false"
                                         OnItemDataBound="gridRS_ItemDataBound" OnItemCommand="gridRS_ItemCommand" 
                                         AllowPaging="true" PageSize='<%# Settings.DefaultPageSize %>'>
                            <ClientSettings AllowDragToGroup="false" Resizing-AllowColumnResize="true" Resizing-AllowResizeToFit="true"
                                            AllowKeyboardNavigation="true" EnablePostBackOnRowClick="true">
                                <ClientEvents OnPopUpShowing="GridPopupShowing" />
                                <Selecting AllowRowSelect="true" />
                                <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                            </ClientSettings>
                            <SortingSettings EnableSkinSortStyles="false" />
                            <GroupingSettings CaseSensitive="False" ShowUnGroupButton="true" />
                            <MasterTableView AutoGenerateColumns="False" DataKeyNames="ID" CommandItemDisplay="Top" AllowMultiColumnSorting="true" EditMode="Popup">
                                <CommandItemSettings ShowAddNewRecordButton="true" AddNewRecordText="Add New Rate Sheet" ShowExportToExcelButton="false" ShowRefreshButton="false" />
                                <EditFormSettings ColumnNumber="2" CaptionFormatString="Rate Sheet Settings: Edit" InsertCaption="Rate Sheet Settings: Create" PopUpSettings-ShowCaptionInEditForm="false">
                                    <EditColumn ButtonType="ImageButton" 
                                                CancelImageUrl="~/images/cancel_imageonly.png"
                                                UpdateImageUrl="~/images/apply_imageonly.png" 
                                                InsertImageUrl="~/images/apply_imageonly.png" />
                                    <PopUpSettings Width="560px" />
                                </EditFormSettings>
                                <SortExpressions>
                                    <telerik:GridSortExpression FieldName="EffectiveDate" SortOrder="Ascending" />
                                    <telerik:GridSortExpression FieldName="Carrier" SortOrder="Ascending" />
                                    <telerik:GridSortExpression FieldName="ProductGroup" SortOrder="Ascending" />
                                    <telerik:GridSortExpression FieldName="DriverGroup" SortOrder="Ascending" />
                                </SortExpressions>
                                <Columns>
                                    <telerik:GridClientSelectColumn UniqueName="ClientSelectColumn" HeaderStyle-Width="40px" Visible="true" Display="true"></telerik:GridClientSelectColumn>

                                    <telerik:GridTemplateColumn UniqueName="ActionColumn" HeaderStyle-Width="90px" AllowFiltering="false" AllowSorting="false">
                                        <ItemTemplate>
                                            <asp:ImageButton runat="server" ID="btnEdit" CssClass="btn btn-xs btn-default shiny" CommandName="Edit" Title="Edit" ImageUrl="~/images/edit.png" />
                                            <asp:ImageButton runat="server" ID="btnDelete" CssClass="btn btn-xs btn-default shiny" CommandName="Delete" Title="Delete" ImageUrl="~/images/delete.png" />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridBoundColumn DataField="ID" UniqueName="ID" HeaderText="ID" ReadOnly="true" Display="false" ForceExtractValue="Always" />
                                    <telerik:GridCheckBoxColumn DataField="Locked" HeaderText="Locked?" UniqueName="Locked" HeaderStyle-Width="60px" ReadOnly="true" />

                                    <telerik:GridDropDownColumn UniqueName="RateTypeID" DataField="RateTypeID" HeaderText="Rate Type"
                                                                SortExpression="RateType" FilterControlWidth="70%"
                                                                HeaderStyle-Width="100px" ItemStyle-Width="100px" EditFormColumnIndex="1"
                                                                FilterControlAltText="LightGreen"
                                                                DataSourceID="dsRateType" ListValueField="ID" ListTextField="Name">
                                    </telerik:GridDropDownColumn>
                                    <telerik:GridDropDownColumn UniqueName="UomID" DataField="UomID" HeaderText="UOM"
                                                                SortExpression="UOM" FilterControlWidth="70%"
                                                                DataSourceID="dsUOM" ListValueField="ID" ListTextField="Name"
                                                                FilterControlAltText="LightGreen"
                                                                HeaderStyle-Width="80px" ItemStyle-Width="100px" EditFormColumnIndex="1">
                                    </telerik:GridDropDownColumn>

                                    <telerik:GridDateTimeColumn DataField="EffectiveDate" HeaderText="Effective Date" DataType="System.DateTime"
                                                                UniqueName="EffectiveDate" FilterControlWidth="70%" EditFormColumnIndex="1" ForceExtractValue="Always"
                                                                FilterControlAltText="LightGreen"
                                                                HeaderStyle-Width="95px" ItemStyle-Width="100px" DataFormatString="{0:M/d/yyyy}">
                                        <ColumnValidationSettings EnableRequiredFieldValidation="true">
                                            <RequiredFieldValidator ErrorMessage="Effective Date value is required" CssClass="NullValidator" Text="&nbsp;!" />
                                        </ColumnValidationSettings>
                                    </telerik:GridDateTimeColumn>
                                    <telerik:GridDateTimeColumn DataField="EndDate" HeaderText="End Date" DataType="System.DateTime"
                                                                UniqueName="EndDate" FilterControlWidth="70%" EditFormColumnIndex="1" ForceExtractValue="Always"
                                                                FilterControlAltText="LightGreen"
                                                                HeaderStyle-Width="95px" ItemStyle-Width="100px" DataFormatString="{0:M/d/yyyy}">
                                        <ColumnValidationSettings EnableRequiredFieldValidation="true">
                                            <RequiredFieldValidator ErrorMessage="End Date value is required" CssClass="NullValidator" Text="&nbsp;!" />
                                        </ColumnValidationSettings>
                                    </telerik:GridDateTimeColumn>

                                    <telerik:GridDropDownColumn UniqueName="TruckTypeID" DataField="TruckTypeID" HeaderText="Truck Type" SortExpression="TruckType"
                                                                FilterControlWidth="70%" HeaderStyle-Width="150px" EditFormColumnIndex="0" ItemStyle-Width="250px"
                                                                FilterControlAltText="LightGreen"
                                                                DataSourceID="dsTruckType" ListValueField="ID" ListTextField="Name" />
                                    <telerik:GridDropDownColumn UniqueName="ShipperID" DataField="ShipperID" HeaderText="Shipper" SortExpression="Shipper"
                                                                FilterControlWidth="70%" HeaderStyle-Width="250px" EditFormColumnIndex="0" ItemStyle-Width="250px"
                                                                DataSourceID="dsShipper" ListValueField="ID" ListTextField="Name"
                                                                FilterControlAltText="LightGreen|White" />
                                    <telerik:GridDropDownColumn UniqueName="CarrierID" DataField="CarrierID" HeaderText="Carrier" SortExpression="Carrier"
                                                                FilterControlWidth="70%" HeaderStyle-Width="250px" EditFormColumnIndex="0" ItemStyle-Width="250px"
                                                                FilterControlAltText="LightGreen"
                                                                DataSourceID="dsCarrier" ListValueField="ID" ListTextField="Name" />
                                    <telerik:GridDropDownColumn UniqueName="ProductGroupID" DataField="ProductGroupID" HeaderText="Product Group" SortExpression="ProductGroup"
                                                                FilterControlWidth="70%" HeaderStyle-Width="150px" EditFormColumnIndex="0" ItemStyle-Width="250px"
                                                                FilterControlAltText="LightGreen"
                                                                DataSourceID="dsProductGroup" ListValueField="ID" ListTextField="Name" />
                                    <telerik:GridDropDownColumn UniqueName="DriverGroupID" DataField="DriverGroupID" HeaderText="Driver Group" SortExpression="DriverGroup"
                                                                FilterControlWidth="70%" HeaderStyle-Width="150px" EditFormColumnIndex="0" ItemStyle-Width="250px"
                                                                DataSourceID="dsDriverGroup" ListValueField="ID" ListTextField="Name"
                                                                FilterControlAltText="LightGreen|White" />
                                    <telerik:GridDropDownColumn UniqueName="DriverID" DataField="DriverID" HeaderText="Driver" SortExpression="Driver"
                                                                FilterControlWidth="70%" HeaderStyle-Width="150px" EditFormColumnIndex="0" ItemStyle-Width="250px"
                                                                DataSourceID="dsDriver" ListValueField="ID" ListTextField="Name"
                                                                FilterControlAltText="LightGreen|White" />
                                    <telerik:GridDropDownColumn UniqueName="OriginStateID" DataField="OriginStateID" HeaderText="Origin State"
                                                                FilterControlWidth="70%" HeaderStyle-Width="150px" EditFormColumnIndex="0" ItemStyle-Width="250px" SortExpression="OriginState"
                                                                FilterControlAltText="LightGreen"
                                                                DataSourceID="dsState" ListValueField="ID" ListTextField="Name" />
                                    <telerik:GridDropDownColumn UniqueName="DestStateID" DataField="DestStateID" HeaderText="Destination State" SortExpression="DestinationState"
                                                                FilterControlWidth="70%" HeaderStyle-Width="150px" EditFormColumnIndex="0" ItemStyle-Width="250px"
                                                                FilterControlAltText="LightGreen"
                                                                DataSourceID="dsState" ListValueField="ID" ListTextField="Name" />
                                    <telerik:GridDropDownColumn UniqueName="RegionID" DataField="RegionID" HeaderText="Region"
                                                                FilterControlWidth="70%" HeaderStyle-Width="150px" EditFormColumnIndex="0" ItemStyle-Width="250px" SortExpression="Region"
                                                                DataSourceID="dsRegion" ListValueField="ID" ListTextField="Name" />
                                    <telerik:GridDropDownColumn UniqueName="ProducerID" DataField="ProducerID" HeaderText="Producer" SortExpression="Producer"
                                                                FilterControlWidth="70%" HeaderStyle-Width="150px" EditFormColumnIndex="0" ItemStyle-Width="250px"
                                                                FilterControlAltText="LightGreen"
                                                                DataSourceID="dsProducer" ListValueField="ID" ListTextField="Name" />

                                    <telerik:GridTemplateColumn UniqueName="Validation" HeaderStyle-Width="0px" Display="false" EditFormColumnIndex="1" EditFormHeaderTextFormat="">
                                        <EditItemTemplate>
                                            <asp:CustomValidator ID="cvGridError" runat="server" Display="None" ErrorMessage="Generic Message" />
                                            <asp:ValidationSummary ID="validationSummaryMain" runat="server" DisplayMode="BulletList" Enabled="true" EnableClientScript="true"
                                                                   CssClass="NullValidator gridPopupErrors" />
                                        </EditItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="CreateDate" UniqueName="CreateDate" SortExpression="CreateDate" DataType="System.DateTime" DataFormatString="{0:M/d/yy}"
                                                             ForceExtractValue="Always" ReadOnly="true"
                                                             HeaderText="Create Date" HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                    <telerik:GridBoundColumn DataField="CreatedByUser" UniqueName="CreatedByUser" SortExpression="CreatedByUser"
                                                             ForceExtractValue="Always" ReadOnly="true"
                                                             HeaderText="Created By" HeaderStyle-Width="90px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                    <telerik:GridBoundColumn DataField="LastChangeDate" UniqueName="LastChangeDate" SortExpression="LastChangeDate" DataType="System.DateTime" DataFormatString="{0:M/d/yy HH:mm}"
                                                             ForceExtractValue="Always" ReadOnly="true"
                                                             HeaderText="Last Update Date" HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                    <telerik:GridBoundColumn DataField="LastChangedByUser" UniqueName="LastChangedByUser" SortExpression="LastChangedByUser"
                                                             ForceExtractValue="Always" ReadOnly="true"
                                                             HeaderText="Last Updated By" HeaderStyle-Width="95px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                    <telerik:GridCheckBoxColumn DataField="BestMatch" UniqueName="BestMatch" Visible="false" ForceExtractValue="Always" ReadOnly="true" />
                                </Columns>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <HeaderStyle Wrap="False" />
                            <PagerStyle AlwaysVisible="True" />
                            <FilterMenu EnableImageSprites="False" />
                        </telerik:RadGrid>
                    </div>
                    <div class="dual-grid-vertical">
                        <telerik:RadGrid ID="rgRangeRates" runat="server" EnableHeaderContextMenu="False" CellSpacing="0" GridLines="None"
                                         EnableLinqExpressions="false" DataSourceID="dsRangeRates" AllowSorting="True" AllowFilteringByColumn="false" Height="400"
                                         CssClass="GridRepaint" ShowGroupPanel="false" EnableEmbeddedSkins="true" Skin="Vista" AlternatingItemStyle-BackColor="#dcf2fc"
                                         OnItemDataBound="gridRR_ItemDataBound" OnItemCommand="gridRR_ItemCommand" OnDataBound="gridRR_DataBound" 
                                         AllowPaging="true" PageSize='<%# Settings.DefaultPageSize %>'>
                            <ClientSettings AllowDragToGroup="false" Resizing-AllowColumnResize="true" Resizing-AllowResizeToFit="true">
                                <ClientEvents OnPopUpShowing="GridPopupShowing" />
                                <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                            </ClientSettings>
                            <SortingSettings EnableSkinSortStyles="false" />
                            <GroupingSettings CaseSensitive="False" ShowUnGroupButton="true" />
                            <MasterTableView AutoGenerateColumns="False" DataKeyNames="ID" CommandItemDisplay="Top" AllowMultiColumnSorting="true" EditMode="Popup">
                                <CommandItemSettings ShowAddNewRecordButton="true" AddNewRecordText="Add New Range Rate" ShowExportToExcelButton="false" ShowRefreshButton="false" />
                                <SortExpressions>
                                    <telerik:GridSortExpression FieldName="EffectiveDate" SortOrder="Ascending" />
                                    <telerik:GridSortExpression FieldName="Carrier" SortOrder="Ascending" />
                                    <telerik:GridSortExpression FieldName="ProductGroup" SortOrder="Ascending" />
                                    <telerik:GridSortExpression FieldName="DriverGroup" SortOrder="Ascending" />
                                </SortExpressions>
                                <Columns>
                                    <telerik:GridTemplateColumn UniqueName="ActionColumn" HeaderStyle-Width="90px" AllowFiltering="false" AllowSorting="false">
                                        <ItemTemplate>
                                            <asp:ImageButton runat="server" ID="btnEdit" CssClass="btn btn-xs btn-default shiny" CommandName="Edit" Title="Edit" ImageUrl="~/images/edit.png" />
                                            <asp:ImageButton runat="server" ID="btnDelete" CssClass="btn btn-xs btn-default shiny" CommandName="Delete" Title="Delete" ImageUrl="~/images/delete.png" />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="RateSheetID" UniqueName="RateSheetID" HeaderText="RateSheetID" ReadOnly="true" ForceExtractValue="Always" Display="false" HeaderStyle-Width="0px" />

                                    <telerik:GridBoundColumn DataField="ID" UniqueName="ID" HeaderText="ID" ReadOnly="true" Display="false" ForceExtractValue="Always" />
                                    <telerik:GridCheckBoxColumn DataField="Locked" HeaderText="Locked?" UniqueName="Locked" HeaderStyle-Width="60px" ReadOnly="true" Visible="false" />

                                    <telerik:GridDropDownColumn UniqueName="RateTypeID" DataField="RateTypeID" HeaderText="Rate Type"
                                                                SortExpression="RateType" FilterControlWidth="70%" ItemStyle-Width="100px" ReadOnly="true" Visible="false"
                                                                HeaderStyle-Width="100px"
                                                                FilterControlAltText="LightGreen"
                                                                DataSourceID="dsRateType" ListValueField="ID" ListTextField="Name">
                                    </telerik:GridDropDownColumn>
                                    <telerik:GridDropDownColumn UniqueName="UomID" DataField="UomID" HeaderText="UOM"
                                                                SortExpression="UOM" FilterControlWidth="70%" ItemStyle-Width="100px" ReadOnly="true" Visible="false"
                                                                DataSourceID="dsUOM" ListValueField="ID" ListTextField="Name"
                                                                FilterControlAltText="LightGreen"
                                                                HeaderStyle-Width="80px">
                                    </telerik:GridDropDownColumn>
                                    <telerik:GridNumericColumn DataField="MinRange" UniqueName="MinRange" SortExpression="MinRange"
                                                               HeaderText="Min Range" FilterControlWidth="60%" ItemStyle-Width="100px"
                                                               DataType="System.Int32" NumericType="Number" MinValue="0"
                                                               FilterControlAltText="LightGreen"
                                                               HeaderStyle-Width="80px" ItemStyle-CssClass="RightAlign" ItemStyle-HorizontalAlign="Right" EditFormColumnIndex="0">
                                        <ColumnValidationSettings EnableRequiredFieldValidation="true">
                                            <RequiredFieldValidator ErrorMessage="Min Range value is required" Text="&nbsp;!" CssClass="NullValidator" InitialValue="" />
                                        </ColumnValidationSettings>
                                    </telerik:GridNumericColumn>
                                    <telerik:GridNumericColumn DataField="MaxRange" UniqueName="MaxRange" SortExpression="MaxRange"
                                                               HeaderText="Max Range" FilterControlWidth="60%" ItemStyle-Width="100px"
                                                               DataType="System.Int32" NumericType="Number" MinValue="0"
                                                               FilterControlAltText="LightGreen"
                                                               HeaderStyle-Width="80px" ItemStyle-CssClass="RightAlign" ItemStyle-HorizontalAlign="Right" EditFormColumnIndex="0">
                                        <ColumnValidationSettings EnableRequiredFieldValidation="true">
                                            <RequiredFieldValidator ErrorMessage="Max Range value is required" Text="&nbsp;!" CssClass="NullValidator" InitialValue="" />
                                        </ColumnValidationSettings>
                                    </telerik:GridNumericColumn>
                                    <telerik:GridNumericColumn DataField="Rate" UniqueName="Rate" SortExpression="Rate"
                                                               HeaderText="Rate" FilterControlWidth="60%" ItemStyle-Width="100px" ColumnEditorID="num10DigitEditor"
                                                               DataType="System.Decimal" NumericType="Number" DataFormatString="{0:#0.0000000000}"
                                                               FilterControlAltText="LightGreen"
                                                               HeaderStyle-Width="100px" ItemStyle-CssClass="RightAlign" ItemStyle-HorizontalAlign="Right" EditFormColumnIndex="0">
                                        <ColumnValidationSettings EnableRequiredFieldValidation="true">
                                            <RequiredFieldValidator ErrorMessage="Rate value is required" Text="&nbsp;!" CssClass="NullValidator" InitialValue="" />
                                        </ColumnValidationSettings>
                                    </telerik:GridNumericColumn>

                                    <telerik:GridDateTimeColumn DataField="EffectiveDate" HeaderText="Effective Date" DataType="System.DateTime"
                                                                UniqueName="EffectiveDate" FilterControlWidth="70%" ForceExtractValue="Always" ReadOnly="true" Display="false"
                                                                FilterControlAltText="LightGreen"
                                                                HeaderStyle-Width="0px" DataFormatString="{0:M/d/yyyy}">
                                    </telerik:GridDateTimeColumn>
                                    <telerik:GridDateTimeColumn DataField="EndDate" HeaderText="End Date" DataType="System.DateTime"
                                                                UniqueName="EndDate" FilterControlWidth="70%" ForceExtractValue="Always" ReadOnly="true" Display="false"
                                                                FilterControlAltText="LightGreen"
                                                                HeaderStyle-Width="0px" DataFormatString="{0:M/d/yyyy}">
                                    </telerik:GridDateTimeColumn>

                                    <telerik:GridDropDownColumn UniqueName="TruckTypeID" DataField="TruckTypeID" HeaderText="Truck Type" SortExpression="TruckType"
                                                                FilterControlWidth="70%" HeaderStyle-Width="150px" ItemStyle-Width="250px" ReadOnly="true" Visible="false"
                                                                FilterControlAltText="LightGreen"
                                                                DataSourceID="dsTruckType" ListValueField="ID" ListTextField="Name" />
                                    <telerik:GridDropDownColumn UniqueName="ShipperID" DataField="ShipperID" HeaderText="Shipper" SortExpression="Shipper"
                                                                FilterControlWidth="70%" HeaderStyle-Width="250px" ItemStyle-Width="250px" ReadOnly="true" Visible="false"
                                                                DataSourceID="dsShipper" ListValueField="ID" ListTextField="Name"
                                                                FilterControlAltText="LightGreen|White" />
                                    <telerik:GridDropDownColumn UniqueName="CarrierID" DataField="CarrierID" HeaderText="Carrier" SortExpression="Carrier"
                                                                FilterControlWidth="70%" HeaderStyle-Width="250px" ItemStyle-Width="250px" ReadOnly="true" Visible="false"
                                                                FilterControlAltText="LightGreen"
                                                                DataSourceID="dsCarrier" ListValueField="ID" ListTextField="Name" />
                                    <telerik:GridDropDownColumn UniqueName="ProductGroupID" DataField="ProductGroupID" HeaderText="Product Group" SortExpression="ProductGroup"
                                                                FilterControlWidth="70%" HeaderStyle-Width="150px" ItemStyle-Width="250px" ReadOnly="true" Visible="false"
                                                                FilterControlAltText="LightGreen"
                                                                DataSourceID="dsProductGroup" ListValueField="ID" ListTextField="Name" />
                                    <telerik:GridDropDownColumn UniqueName="DriverGroupID" DataField="DriverGroupID" HeaderText="Driver Group" SortExpression="DriverGroup"
                                                                FilterControlWidth="70%" HeaderStyle-Width="150px" ReadOnly="true" Visible="false" ItemStyle-Width="250px"
                                                                DataSourceID="dsDriverGroup" ListValueField="ID" ListTextField="Name"
                                                                FilterControlAltText="LightGreen|White" />
                                    <telerik:GridDropDownColumn UniqueName="DriverID" DataField="DriverID" HeaderText="Driver" SortExpression="Driver"
                                                                FilterControlWidth="70%" HeaderStyle-Width="150px" ReadOnly="true" Visible="false" ItemStyle-Width="250px"
                                                                DataSourceID="dsDriver" ListValueField="ID" ListTextField="Name"
                                                                FilterControlAltText="LightGreen|White" />
                                    <telerik:GridDropDownColumn UniqueName="OriginStateID" DataField="OriginStateID" HeaderText="Origin State" SortExpression="OriginState"
                                                                FilterControlWidth="70%" HeaderStyle-Width="150px" ItemStyle-Width="250px" ReadOnly="true" Visible="false"
                                                                FilterControlAltText="LightGreen"
                                                                DataSourceID="dsState" ListValueField="ID" ListTextField="Name" />
                                    <telerik:GridDropDownColumn UniqueName="DestStateID" DataField="DestStateID" HeaderText="Destination State" SortExpression="DestinationState"
                                                                FilterControlWidth="70%" HeaderStyle-Width="150px" ItemStyle-Width="250px" ReadOnly="true" Visible="false"
                                                                FilterControlAltText="LightGreen"
                                                                DataSourceID="dsState" ListValueField="ID" ListTextField="Name" />
                                    <telerik:GridDropDownColumn UniqueName="RegionID" DataField="RegionID" HeaderText="Region" SortExpression="Region"
                                                                FilterControlWidth="70%" HeaderStyle-Width="150px" ItemStyle-Width="250px" ReadOnly="true" Visible="false"
                                                                FilterControlAltText="LightGreen"
                                                                DataSourceID="dsRegion" ListValueField="ID" ListTextField="Name" />
                                    <telerik:GridDropDownColumn UniqueName="ProducerID" DataField="ProducerID" HeaderText="Producer" SortExpression="Producer"
                                                                FilterControlWidth="70%" HeaderStyle-Width="150px" ItemStyle-Width="250px" ReadOnly="true" Visible="false"
                                                                FilterControlAltText="LightGreen"
                                                                DataSourceID="dsProducer" ListValueField="ID" ListTextField="Name" />

                                    <telerik:GridTemplateColumn UniqueName="Validation" HeaderStyle-Width="0px" Display="false" EditFormColumnIndex="0" EditFormHeaderTextFormat="">
                                        <EditItemTemplate>
                                            <asp:CustomValidator ID="cvGridError" runat="server" Display="None" ErrorMessage="Generic Message" />
                                            <asp:ValidationSummary ID="validationSummaryMain" runat="server" DisplayMode="BulletList" Enabled="true" EnableClientScript="true"
                                                                   CssClass="NullValidator gridPopupErrors" />
                                        </EditItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="CreateDate" UniqueName="CreateDate" SortExpression="CreateDate" DataType="System.DateTime" DataFormatString="{0:M/d/yy}"
                                                             ForceExtractValue="Always" ReadOnly="true"
                                                             HeaderText="Create Date" HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                    <telerik:GridBoundColumn DataField="CreatedByUser" UniqueName="CreatedByUser" SortExpression="CreatedByUser"
                                                             ForceExtractValue="Always" ReadOnly="true"
                                                             HeaderText="Created By" HeaderStyle-Width="90px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                    <telerik:GridBoundColumn DataField="LastChangeDate" UniqueName="LastChangeDate" SortExpression="LastChangeDate" DataType="System.DateTime" DataFormatString="{0:M/d/yy HH:mm}"
                                                             ForceExtractValue="Always" ReadOnly="true"
                                                             HeaderText="Last Update Date" HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                    <telerik:GridBoundColumn DataField="LastChangedByUser" UniqueName="LastChangedByUser" SortExpression="LastChangedByUser"
                                                             ForceExtractValue="Always" ReadOnly="true"
                                                             HeaderText="Last Updated By" HeaderStyle-Width="95px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />

                                    <telerik:GridDropDownColumn DataField="ImportAction" UniqueName="ImportAction" HeaderText="Import Action"
                                                                ForceExtractValue="Always" ItemStyle-BackColor="Olive" ReadOnly="true" Display="false" HeaderStyle-Width="0px"
                                                                FilterControlAltText="LightGreen"
                                                                DataSourceID="dsImportAction" ListTextField="Name" ListValueField="Name" DefaultInsertValue="Add">
                                    </telerik:GridDropDownColumn>
                                    <telerik:GridBoundColumn DataField="ImportOutcome" UniqueName="ImportOutcome" HeaderText="Import Outcome"
                                                             ForceExtractValue="Always" ItemStyle-BackColor="Olive" ReadOnly="true" Display="false" HeaderStyle-Width="0px" />
                                </Columns>
                                <EditFormSettings ColumnNumber="1" FormMainTableStyle-CssClass="rangeEditTable" 
                                                  CaptionFormatString="Rate Sheet Range Rate: Edit" InsertCaption="Rate Sheet Range Rate: Create" 
                                                  PopUpSettings-ShowCaptionInEditForm="false" PopUpSettings-Width="260">
                                    <EditColumn ButtonType="ImageButton" 
                                                CancelImageUrl="~/images/cancel_imageonly.png"
                                                UpdateImageUrl="~/images/apply_imageonly.png" 
                                                InsertImageUrl="~/images/apply_imageonly.png" />
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <HeaderStyle Wrap="False" />
                            <PagerStyle AlwaysVisible="True" />
                            <FilterMenu EnableImageSprites="False" />
                        </telerik:RadGrid>
                    </div>
                    <telerik:RadScriptBlock ID="rsbMain" runat="server">
                        <script src="/scripts/radgrid_rates.js" type="text/javascript" ></script>
                    </telerik:RadScriptBlock>
                </div>
            </div>
        </div>
    </div>
    <blac:DBDataSource ID="dsRateSheets" runat="server" SelectIDNullsToZero="true"
                       SelectCommand="SELECT *, ImportAction='None', spacer=NULL
            FROM dbo.fnDriverRateSheetDisplay(isnull(@StartDate, getdate()), coalesce(@EndDate, @StartDate, getdate()), @ShipperID, @CarrierID, @ProductGroupID, @TruckTypeID, @DriverGroupID, @DriverID, @OriginStateID, @DestStateID, @RegionID, @ProducerID) ORDER BY Carrier, EffectiveDate DESC">
        <SelectParameters>
            <asp:ControlParameter Name="ShipperID" ControlID="ddShipper" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
            <asp:ControlParameter Name="CarrierID" ControlID="ddCarrier" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
            <asp:ControlParameter Name="ProductGroupID" ControlID="ddProductGroup" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
            <asp:ControlParameter Name="TruckTypeID" ControlID="ddTruckType" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
            <asp:ControlParameter Name="DriverGroupID" ControlID="ddDriverGroup" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
            <asp:ControlParameter Name="DriverID" ControlID="ddDriver" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
            <asp:ControlParameter Name="OriginStateID" ControlID="ddOriginState" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
            <asp:ControlParameter Name="DestStateID" ControlID="ddDestinationState" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
            <asp:ControlParameter Name="RegionID" ControlID="ddRegion" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
            <asp:ControlParameter Name="ProducerID" ControlID="ddProducer" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
            <asp:ControlParameter Name="StartDate" ControlID="rdpStartDate" PropertyName="DbSelectedDate" Type="String" DefaultValue="NULL" />
            <asp:ControlParameter Name="EndDate" ControlID="rdpEndDate" PropertyName="DbSelectedDate" Type="String" DefaultValue="NULL" />
        </SelectParameters>
    </blac:DBDataSource>
    <blac:DBDataSource ID="dsRangeRates" runat="server" SelectIDNullsToZero="true"
                       SelectCommand="SELECT *, BestMatch=cast(CASE WHEN @RouteMiles BETWEEN MinRange AND MaxRange THEN 1 ELSE 0 END as bit), ImportAction='None', ImportOutcome=NULL, spacer=NULL
            FROM dbo.viewDriverRateSheetRangeRatesDisplay WHERE RateSheetID = @RSID ORDER BY DriverGroup, EffectiveDate DESC, RateSheetID, MinRange">
        <SelectParameters>
            <asp:ControlParameter Name="RSID" ControlID="rgRateSheets" PropertyName="SelectedValue" DefaultValue="0" />
            <asp:ControlParameter Name="RouteMiles" ControlID="rcbRouteMiles" PropertyName="SelectedValue" DefaultValue="-1" />
        </SelectParameters>
    </blac:DBDataSource>
    <blc:RadGridDBCtrl ID="dbcRateSheets" runat="server"
                       ControlID="rgRateSheets"
                       UpdateTableName="tblDriverRateSheet"
                       OnRowSelected="dbcRateSheets_RowSelected"
                       FilterActiveEntities="False" />
    <blc:RadGridDBCtrl ID="dbcRangeRates" runat="server"
                       ControlID="rgRangeRates"
                       UpdateTableName="tblDriverRangeRate"
                       FilterActiveEntities="False" />
    <blac:DBDataSource ID="dsShipper" runat="server" SelectCommand="SELECT ID, Name FROM dbo.tblCustomer UNION SELECT 0, '(All)' ORDER BY Name" />
    <blac:DBDataSource ID="dsCarrier" runat="server"
                       SelectCommand="SELECT ID, Name = CASE WHEN DeleteDateUTC IS NOT NULL THEN 'Deleted:' ELSE '' END + Name, Active = CASE WHEN DeleteDateUTC IS NULL THEN 1 ELSE 0 END FROM dbo.tblCarrier UNION SELECT 0, '(All)', 1 ORDER BY Active DESC, Name">
    </blac:DBDataSource>
    <blac:DBDataSource ID="dsProductGroup" runat="server" SelectCommand="SELECT ID, Name FROM dbo.tblProductGroup UNION SELECT 0, '(All)' ORDER BY Name" />
    <blac:DBDataSource ID="dsTruckType" runat="server" SelectCommand="SELECT ID, Name FROM dbo.tblTruckType UNION SELECT 0, '(All)' ORDER BY Name" />
    <blac:DBDataSource ID="dsDriverGroup" runat="server" SelectCommand="SELECT ID, Name FROM dbo.tblDriverGroup UNION SELECT 0, '(All)' ORDER BY Name" />
    <blac:DBDataSource ID="dsDriver" runat="server" SelectCommand="SELECT ID, Name = CASE WHEN EXISTS (SELECT * FROM viewDriverBase D2 WHERE D2.FullNameD = D.FullNameD AND D2.ID <> D.ID) THEN dbo.fnNameWithDeleted(Carrier + ': ' + FullName, DeleteDateUTC) ELSE FullNameD END, Active, N = FullName FROM dbo.viewDriverBase D UNION SELECT 0, '(All)', 1, '(All)' ORDER BY Active DESC, N, Name" />
    <blac:DBDataSource ID="dsState" runat="server" SelectCommand="SELECT ID, Name = FullName FROM dbo.tblState UNION SELECT 0, '(All)' ORDER BY Name" />
    <blac:DBDataSource ID="dsRegion" runat="server" SelectCommand="SELECT ID, Name FROM dbo.tblRegion UNION SELECT 0, '(All)' ORDER BY Name" />
    <blac:DBDataSource ID="dsProducer" runat="server" SelectCommand="SELECT ID, Name FROM dbo.tblProducer UNION SELECT 0, '(All)' ORDER BY Name" />
    <blac:DBDataSource ID="dsRateType" runat="server" SelectCommand="SELECT ID, Name FROM dbo.tblRateType WHERE ForDriver = 1 AND ForLoadFee = 1 ORDER BY Name" />
    <blac:DBDataSource ID="dsUom" runat="server" SelectCommand="SELECT ID, Name FROM dbo.tblUOM ORDER BY Name" />
    <blac:DBDataSource ID="dsImportAction" runat="server" SelectCommand="SELECT Name = 'None' UNION SELECT 'Add' UNION SELECT 'Update' UNION SELECT 'Delete'" />
</asp:Content>

