﻿using System;
using Telerik.Web.UI;
using DispatchCrude.Extensions;

namespace DispatchCrude.Site.Financials
{
    public partial class FuelPrices : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ConfigureAjax(true);

            //Print tab navigation html to page
            tabmenu.InnerHtml = NavigationHelper.PrintTabArray(NavigationHelper.TabSet_Accessorial, "Tab_PADDPricing").ToString();
        }
        private void ConfigureAjax(bool enabled)
        {
            if (enabled)
                App_Code.RadAjaxHelper.AddAjaxSetting(this.Page, rgMain, rgMain);
        }

        protected void grid_ItemCommand(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.InitInsertCommandName) //"Add new" button clicked
            {
            }
        }
    }
}