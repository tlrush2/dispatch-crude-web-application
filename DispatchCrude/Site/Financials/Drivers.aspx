﻿<%@ Page Title="Settlement" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Drivers.aspx.cs"
    Inherits="DispatchCrude.Site.Financials.Drivers" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="mainContent" ContentPlaceHolderID="MainContent" runat="server">
    <script>
        $("#ctl00_ctl00_EntityCaption").html("Driver Group Assignment");
    </script>
    <div class="tabbable TabRepaint">
        <ul class="nav nav-tabs" id="tabmenu" runat="server">
            <!--Tabs will print here (see code behind)-->
        </ul>
        <div class="tab-content">
            <div class="tab-pane in active">

                <div id="speedbuttons" class="well speedButtonContainer" runat="server">
                    <!--Buttons will print here (see code behind)-->
                </div>

                <div id="DataEntryFormGrid" style="height:100%;">
                    <asp:ValidationSummary ID="validationSummaryMain" runat="server" DisplayMode="BulletList" Enabled="true" EnableClientScript="true" CssClass="NullValidator" />
                    <telerik:RadGrid ID="rgMain" runat="server" CellSpacing="0" GridLines="None" CssClass="GridRepaint" EnableLinqExpressions="false"
                                     AutoGenerateColumns="False" ShowGroupPanel="False" AllowFilteringByColumn="true" AllowSorting="True" 
                                     EnableEmbeddedSkins="true" Skin="Vista" AlternatingItemStyle-BackColor="#dcf2fc" Height="800"
                                     DataSourceID="dsMain"
                                     AllowPaging="True" PageSize='<%# Settings.DefaultPageSize %>'>
                        <ClientSettings AllowDragToGroup="True">
                            <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                        </ClientSettings>
                        <GroupingSettings CaseSensitive="false" ShowUnGroupButton="false" />
                        <ExportSettings FileName="Driver Group Assignment" ExportOnlyData="true" IgnorePaging="true" HideStructureColumns="true" Excel-FileExtension="xls" Excel-Format="Biff" OpenInNewWindow="true" />
                        <MasterTableView DataKeyNames="ID" CommandItemDisplay="Top" EditMode="InPlace">
                            <CommandItemSettings ShowExportToExcelButton="true" ShowAddNewRecordButton="false" />
                            <Columns>
                                <telerik:GridTemplateColumn UniqueName="ActionColumn" HeaderStyle-Width="90px" AllowFiltering="false" AllowSorting="false">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" ID="btnEdit" CssClass="btn btn-xs btn-default shiny" CommandName="Edit" Title="Edit" ImageUrl="~/images/edit.png" />
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:ImageButton runat="server" CssClass="btn btn-xs btn-default shiny" CommandName="Update" ImageUrl="~/images/apply_imageonly.png" CausesValidation="true" />
                                        <asp:ImageButton runat="server" CssClass="btn btn-xs btn-default shiny" CommandName="Cancel" ImageUrl="~/images/cancel_imageonly.png" CausesValidation="false" />
                                    </EditItemTemplate>
                                </telerik:GridTemplateColumn>

                                <telerik:GridCheckBoxColumn DataField="Active" UniqueName="Active" HeaderText="Active?" SortExpression="Active"
                                                            ReadOnly="true" HeaderStyle-Width="80px" FilterControlWidth="60%" />

                                <telerik:GridBoundColumn UniqueName="Carrier" DataField="Carrier"
                                                         HeaderText="Carrier" ReadOnly="true" HeaderStyle-Width="250px" FilterControlWidth="70%" />

                                <telerik:GridBoundColumn UniqueName="CarrierType" DataField="CarrierType"
                                                         HeaderText="Carrier Type" ReadOnly="true" HeaderStyle-Width="120px" />

                                <telerik:GridBoundColumn UniqueName="LastName" DataField="LastName"
                                                         HeaderText="Last Name" ReadOnly="true" HeaderStyle-Width="150px" />

                                <telerik:GridBoundColumn UniqueName="FirstName" DataField="FirstName"
                                                         HeaderText="First Name" ReadOnly="true" HeaderStyle-Width="150px" />

                                <telerik:GridBoundColumn UniqueName="IDNumber" DataField="IDNumber"
                                                         HeaderText="ID #" ReadOnly="true" HeaderStyle-Width="80px" FilterControlWidth="60%" />

                                <telerik:GridTemplateColumn UniqueName="DriverGroupID" DataField="DriverGroupName" HeaderText="Driver Group" SortExpression="Driver Group"
                                                            FilterControlWidth="70%">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="ddlDriverGroupName" runat="server" DataSourceID="dsDriverGroup" DataTextField="GroupName" DataValueField="ID"
                                                          SelectedValue='<%# Bind("DriverGroupID") %>'>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblDriverGroupName" runat="server" Text='<%# Eval("DriverGroupName") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="150px" />
                                </telerik:GridTemplateColumn>

                                <telerik:GridBoundColumn DataField="CreateDate" UniqueName="CreateDate" SortExpression="CreateDate" DataType="System.DateTime" DataFormatString="{0:M/d/yy}"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Create Date" HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="CreatedByUser" UniqueName="CreatedByUser" SortExpression="CreatedByUser"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Created By" HeaderStyle-Width="90px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="LastChangeDate" UniqueName="LastChangeDate" SortExpression="LastChangeDate" DataType="System.DateTime" DataFormatString="{0:M/d/yy HH:mm}"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Last Update Date" HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="LastChangedByUser" UniqueName="LastChangedByUser" SortExpression="LastChangedByUser"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Last Updated By" HeaderStyle-Width="95px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />

                            </Columns>
                            <PagerStyle AlwaysVisible="True" />
                        </MasterTableView>
                        <HeaderStyle Wrap="False" />
                        <PagerStyle AlwaysVisible="True" />
                        <FilterMenu EnableImageSprites="False">
                        </FilterMenu>
                    </telerik:RadGrid>
                </div>
                <blc:RadGridDBCtrl ID="dbcMain" runat="server" ControlID="rgMain" UpdateTableName="tblDriver" />
                <blac:DBDataSource ID="dsMain" runat="server" SelectCommand="SELECT * FROM viewDriver ORDER BY LastName, FirstName" />
                <blac:DBDataSource ID="dsDriverGroup" runat="server" SelectCommand="SELECT ID, Name AS GroupName FROM tblDriverGroup WHERE DeleteDateUTC IS NULL UNION SELECT NULL, '(None)' ORDER BY Name" />
            </div>
        </div>
    </div>
</asp:Content>