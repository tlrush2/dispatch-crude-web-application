﻿<%@  Title="Accessorial" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ShipperOrderRejectRates.aspx.cs"
    Inherits="DispatchCrude.Site.Financials.ShipperOrderRejectRates" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="pageHeadContent" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="/styles/radgrid_rates.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="pageMainContent" ContentPlaceHolderID="MainContent" runat="server">
    <telerik:GridNumericColumnEditor ID="num10DigitEditor" runat="server">
        <NumericTextBox runat="server" EnabledStyle-HorizontalAlign="Right" MinValue="0" MaxValue="9999">
            <NumberFormat AllowRounding="true" DecimalDigits="10" KeepNotRoundedValue="true" KeepTrailingZerosOnFocus="false" />
        </NumericTextBox>
    </telerik:GridNumericColumnEditor>
    <script>
        $("#ctl00_ctl00_EntityCaption").html("Shipper Reject Rates");
    </script>
    <div class="tabbable TabRepaint">
        <ul class="nav nav-tabs" id="tabmenu" runat="server">
            <!--Tabs will print here (see code behind)-->
        </ul>
        <div class="tab-content">
            <div class="tab-pane in active">

                <div id="speedbuttons" class="well speedButtonContainer" runat="server">
                    <!--Buttons will print here (see code behind)-->
                </div>

                <div class="leftpanel">
                    <div class="tabbable">
                        <ul class="nav nav-tabs">
                            <li class="active tab-blue">
                                <a data-toggle="tab" href="#Filters" aria-expanded="true">Filters</a>
                            </li>
                            <li class="tab-green">
                                <a data-toggle="tab" href="#Export" aria-expanded="true">Export</a>
                            </li>
                        </ul>
                        <div id="leftTabs" class="tab-content">
                            <div id="Filters" class="tab-pane active">
                                <asp:Panel ID="panelFilter" runat="server" DefaultButton="btnRefresh">
                                    <div class="Entry">
                                        <asp:Label ID="lblReason" runat="server" Text="Reason" AssociatedControlID="ddReason" />
                                        <asp:DropDownList CssClass="btn-xs" Width="100%" runat="server" ID="ddReason" DataTextField="Name" DataValueField="ID" DataSourceID="dsReason" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblTruckType" runat="server" Text="Truck Type" AssociatedControlID="ddTruckType" />
                                        <asp:DropDownList CssClass="btn-xs" Width="100%" runat="server" ID="ddTruckType" DataTextField="Name" DataValueField="ID" DataSourceID="dsTruckType" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblShipper" runat="server" Text="Shipper" AssociatedControlID="ddShipper" />
                                        <asp:DropDownList CssClass="btn-xs" Width="100%" runat="server" ID="ddShipper" DataTextField="Name" DataValueField="ID" DataSourceID="dsShipper" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblProductGroup" runat="server" Text="Product Group" AssociatedControlID="ddProductGroup" />
                                        <asp:DropDownList CssClass="btn-xs" Width="100%" runat="server" ID="ddProductGroup" DataTextField="Name" DataValueField="ID" DataSourceID="dsProductGroup" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblOrigin" runat="server" Text="Origin" AssociatedControlID="ddOrigin" />
                                        <asp:DropDownList CssClass="btn-xs" Width="100%" runat="server" ID="ddOrigin" DataTextField="Name" DataValueField="ID" DataSourceID="dsOrigin" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblState" runat="server" Text="State" AssociatedControlID="ddState" />
                                        <asp:DropDownList CssClass="btn-xs" Width="100%" runat="server" ID="ddState" DataTextField="Name" DataValueField="ID" DataSourceID="dsState" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblRegion" runat="server" Text="Region" AssociatedControlID="ddRegion" />
                                        <asp:DropDownList CssClass="btn-xs" Width="100%" runat="server" ID="ddRegion" DataTextField="Name" DataValueField="ID" DataSourceID="dsRegion" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblProducer" runat="server" Text="Producer" AssociatedControlID="ddProducer" />
                                        <asp:DropDownList CssClass="btn-xs" Width="100%" runat="server" ID="ddProducer" DataTextField="Name" DataValueField="ID" DataSourceID="dsProducer" />
                                    </div>
                                    <div>
                                        <div class="Entry floatLeft-date-row">
                                            <asp:Label ID="lblStartDate" runat="server" Text="Start Date" AssociatedControlID="rdpStartDate" CssClass="Entry" />
                                            <telerik:RadDatePicker ID="rdpStartDate" runat="server" Width="100px">
                                                <DateInput runat="server" DateFormat="M/d/yyyy" />
                                                <DatePopupButton Enabled="true" />
                                            </telerik:RadDatePicker>
                                        </div>
                                        <div class="Entry floatRight-date-row">
                                            <asp:Label ID="lblEndDate" runat="server" Text="End Date" AssociatedControlID="rdpEndDate" CssClass="Entry" />
                                            <telerik:RadDatePicker ID="rdpEndDate" runat="server" Width="100px">
                                                <DateInput runat="server" DateFormat="M/d/yyyy" />
                                                <DatePopupButton Enabled="true" />
                                            </telerik:RadDatePicker>
                                        </div>
                                        <br /><br /><br />
                                        <div class="center">
                                            <asp:Button ID="btnRefresh" runat="server" Text="Refresh" CssClass="btn btn-blue shiny" OnClick="filterValueChanged" />
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                            <div id="Export" class="tab-pane">
                                <asp:Panel ID="panelExcel" runat="server" DefaultButton="cmdExport">
                                    <div>
                                        <div>
                                            <asp:Button ID="cmdExport" runat="server" Text="Export to Excel" CssClass="btn btn-blue shiny" Enabled="true" OnClick="cmdExport_Click" />
                                        </div>
                                        <div class="spacer10px"></div>
                                        <div>
                                            <asp:Button ID="cmdImport" runat="server" ClientIDMode="Static" Text="Import Excel file" CssClass="floatRight btn btn-blue shiny"
                                                        Enabled="false" OnClick="cmdImport_Click" />
                                        </div>
                                    </div>
                                    <div class="spacer10px"></div>
                                    <div class="center">
                                        <asp:FileUpload ID="excelUpload" runat="server" ClientIDMode="Static" CssClass="floatLeft" Width="99%" />
                                        <asp:CustomValidator ID="cvUpload" runat="server" ControlToValidate="excelUpload" CssClass="NullValidator floatLeft" Display="Dynamic"
                                                             Text="*" ErrorMessage="Only Excel (*.xlsx) files allowed)" OnServerValidate="cvUpload_ServerValidate" />
                                    </div>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="gridArea" style="height: 100%; min-height: 500px;">
                    <telerik:RadGrid ID="rgMain" runat="server" EnableHeaderContextMenu="False" CellSpacing="0" GridLines="None" EnableLinqExpressions="false"
                                     AllowSorting="True" AllowFilteringByColumn="false" Height="800" CssClass="GridRepaint" ShowGroupPanel="false" EnableEmbeddedSkins="true" 
                                     Skin="Vista" AlternatingItemStyle-BackColor="#dcf2fc"
                                     DataSourceID="dsMain" OnItemDataBound="grid_ItemDataBound" OnItemCommand="grid_ItemCommand" 
                                     AllowPaging="true" PageSize='<%# Settings.DefaultPageSize %>'>
                        <ClientSettings AllowDragToGroup="true">
                            <ClientEvents OnPopUpShowing="GridPopupShowing" />
                            <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                        </ClientSettings>
                        <SortingSettings EnableSkinSortStyles="false" />
                        <GroupingSettings CaseSensitive="False" ShowUnGroupButton="true" />
                        <MasterTableView AutoGenerateColumns="False" DataKeyNames="ID" CommandItemDisplay="Top" AllowMultiColumnSorting="true" EditMode="PopUp">
                            <CommandItemSettings ShowAddNewRecordButton="true" AddNewRecordText="Add New Order Reject Rate" ShowExportToExcelButton="false" ShowRefreshButton="false" />
                            <SortExpressions>
                                <telerik:GridSortExpression FieldName="EffectiveDate" SortOrder="Ascending" />
                                <telerik:GridSortExpression FieldName="Shipper" SortOrder="Ascending" />
                                <telerik:GridSortExpression FieldName="ProductGroup" SortOrder="Ascending" />
                            </SortExpressions>
                            <Columns>
                                <telerik:GridTemplateColumn UniqueName="ActionColumn" HeaderStyle-Width="90px" AllowFiltering="false" AllowSorting="false">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" ID="btnEdit" CssClass="btn btn-xs btn-default shiny" CommandName="Edit" Title="Edit" ImageUrl="~/images/edit.png" />
                                        <asp:ImageButton runat="server" ID="btnDelete" CssClass="btn btn-xs btn-default shiny" CommandName="Delete" Title="Delete" ImageUrl="~/images/delete.png" />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>

                                <telerik:GridBoundColumn DataField="ID" UniqueName="ID" HeaderText="ID" ReadOnly="true" Display="false" ForceExtractValue="Always" />
                                <telerik:GridCheckBoxColumn DataField="Locked" HeaderText="Locked?" UniqueName="Locked" HeaderStyle-Width="60px" ReadOnly="true" />

                                <telerik:GridDropDownColumn UniqueName="ReasonID" DataField="ReasonID" HeaderText="Reason" SortExpression="Reason"
                                                            FilterControlWidth="70%" HeaderStyle-Width="150px" EditFormColumnIndex="0" ItemStyle-Width="250px"
                                                            FilterControlAltText="LightGreen"
                                                            DataSourceID="dsReason" ListValueField="ID" ListTextField="Name" />

                                <telerik:GridDropDownColumn UniqueName="RateTypeID" DataField="RateTypeID" HeaderText="Rate Type"
                                                            SortExpression="RateType" FilterControlWidth="70%" ItemStyle-Width="100px"
                                                            HeaderStyle-Width="100px" EditFormColumnIndex="1"
                                                            FilterControlAltText="LightGreen"
                                                            DataSourceID="dsRateType" ListValueField="ID" ListTextField="Name">
                                </telerik:GridDropDownColumn>
                                <telerik:GridDropDownColumn UniqueName="UomID" DataField="UomID" HeaderText="UOM"
                                                            SortExpression="Uom" FilterControlWidth="70%" ItemStyle-Width="100px"
                                                            DataSourceID="dsUom" ListValueField="ID" ListTextField="Name"
                                                            FilterControlAltText="LightGreen"
                                                            HeaderStyle-Width="80px" EditFormColumnIndex="1">
                                </telerik:GridDropDownColumn>
                                <telerik:GridNumericColumn DataField="Rate" UniqueName="Rate" SortExpression="Rate"
                                                           HeaderText="Rate" FilterControlWidth="60%" ItemStyle-Width="100px" ColumnEditorID="num10DigitEditor"
                                                           DataType="System.Decimal" NumericType="Number" DataFormatString="{0:#0.0000000000}"
                                                           FilterControlAltText="LightGreen"
                                                           HeaderStyle-Width="100px" ItemStyle-CssClass="RightAlign" ItemStyle-HorizontalAlign="Right" EditFormColumnIndex="1">
                                    <ColumnValidationSettings EnableRequiredFieldValidation="true">
                                        <RequiredFieldValidator ErrorMessage="Rate value is required" Text="&nbsp;!" CssClass="NullValidator" InitialValue="" />
                                    </ColumnValidationSettings>
                                </telerik:GridNumericColumn>

                                <telerik:GridDateTimeColumn DataField="EffectiveDate" HeaderText="Effective Date" DataType="System.DateTime"
                                                            UniqueName="EffectiveDate" FilterControlWidth="70%" EditFormColumnIndex="2" ForceExtractValue="Always"
                                                            FilterControlAltText="LightGreen"
                                                            HeaderStyle-Width="95px" DataFormatString="{0:M/d/yyyy}">
                                    <ColumnValidationSettings EnableRequiredFieldValidation="true">
                                        <RequiredFieldValidator ErrorMessage="Effective Date value is required" CssClass="NullValidator" Text="&nbsp;!" />
                                    </ColumnValidationSettings>
                                </telerik:GridDateTimeColumn>
                                <telerik:GridDateTimeColumn DataField="EndDate" HeaderText="End Date" DataType="System.DateTime"
                                                            UniqueName="EndDate" FilterControlWidth="70%" EditFormColumnIndex="2" ForceExtractValue="Always"
                                                            FilterControlAltText="LightGreen"
                                                            HeaderStyle-Width="95px" DataFormatString="{0:M/d/yyyy}">
                                    <ColumnValidationSettings EnableRequiredFieldValidation="true">
                                        <RequiredFieldValidator ErrorMessage="End Date value is required" CssClass="NullValidator" Text="&nbsp;!" />
                                    </ColumnValidationSettings>
                                </telerik:GridDateTimeColumn>

                                <telerik:GridDropDownColumn UniqueName="TruckTypeID" DataField="TruckTypeID" HeaderText="Truck Type" SortExpression="TruckType"
                                                            FilterControlWidth="70%" HeaderStyle-Width="150px" EditFormColumnIndex="0" ItemStyle-Width="250px"
                                                            FilterControlAltText="LightGreen"
                                                            DataSourceID="dsTruckType" ListValueField="ID" ListTextField="Name" />
                                <telerik:GridDropDownColumn UniqueName="ShipperID" DataField="ShipperID" HeaderText="Shipper" SortExpression="Shipper"
                                                            FilterControlWidth="70%" HeaderStyle-Width="250px" EditFormColumnIndex="0" ItemStyle-Width="250px"
                                                            FilterControlAltText="LightGreen"
                                                            DataSourceID="dsShipper" ListValueField="ID" ListTextField="Name" />
                                <telerik:GridDropDownColumn UniqueName="ProductGroupID" DataField="ProductGroupID" HeaderText="Product Group" SortExpression="ProductGroup"
                                                            FilterControlWidth="70%" HeaderStyle-Width="150px" EditFormColumnIndex="0" ItemStyle-Width="250px"
                                                            FilterControlAltText="LightGreen"
                                                            DataSourceID="dsProductGroup" ListValueField="ID" ListTextField="Name" />
                                <telerik:GridDropDownColumn UniqueName="OriginID" DataField="OriginID" HeaderText="Origin"
                                                            FilterControlWidth="70%" HeaderStyle-Width="250px" EditFormColumnIndex="0" ItemStyle-Width="250px" SortExpression="Origin"
                                                            FilterControlAltText="LightGreen"
                                                            DataSourceID="dsOrigin" ListValueField="ID" ListTextField="Name" />
                                <telerik:GridDropDownColumn UniqueName="StateID" DataField="StateID" HeaderText="State" SortExpression="State"
                                                            FilterControlWidth="70%" HeaderStyle-Width="150px" EditFormColumnIndex="0" ItemStyle-Width="250px"
                                                            FilterControlAltText="LightGreen"
                                                            DataSourceID="dsState" ListValueField="ID" ListTextField="Name" />
                                <telerik:GridDropDownColumn UniqueName="RegionID" DataField="RegionID" HeaderText="Region"
                                                            FilterControlWidth="70%" HeaderStyle-Width="150px" EditFormColumnIndex="0" ItemStyle-Width="250px" SortExpression="Region"
                                                            FilterControlAltText="LightGreen"
                                                            DataSourceID="dsRegion" ListValueField="ID" ListTextField="Name" />
                                <telerik:GridDropDownColumn UniqueName="ProducerID" DataField="ProducerID" HeaderText="Producer"
                                                            FilterControlWidth="70%" HeaderStyle-Width="150px" EditFormColumnIndex="0" ItemStyle-Width="250px" SortExpression="Producer"
                                                            DataSourceID="dsProducer" ListValueField="ID" ListTextField="Name"
                                                            FilterControlAltText="LightGreen|White" />

                                <telerik:GridTemplateColumn UniqueName="Validation" HeaderStyle-Width="0px" Display="false" EditFormColumnIndex="2" EditFormHeaderTextFormat="">
                                    <EditItemTemplate>
                                        <asp:CustomValidator ID="cvGridError" runat="server" Display="None" ErrorMessage="Generic Message" />
                                        <asp:ValidationSummary ID="validationSummaryMain" runat="server" DisplayMode="BulletList" Enabled="true" EnableClientScript="true"
                                                               CssClass="NullValidator gridPopupErrors" />
                                    </EditItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="CreateDate" UniqueName="CreateDate" SortExpression="CreateDate" DataType="System.DateTime" DataFormatString="{0:M/d/yy}"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Create Date" HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="CreatedByUser" UniqueName="CreatedByUser" SortExpression="CreatedByUser"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Created By" HeaderStyle-Width="90px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="LastChangeDate" UniqueName="LastChangeDate" SortExpression="LastChangeDate" DataType="System.DateTime" DataFormatString="{0:M/d/yy HH:mm}"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Last Update Date" HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="LastChangedByUser" UniqueName="LastChangedByUser" SortExpression="LastChangedByUser"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Last Updated By" HeaderStyle-Width="95px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />

                                <telerik:GridDropDownColumn DataField="ImportAction" UniqueName="ImportAction" HeaderText="Import Action"
                                                            ReadOnly="true" Display="false" ForceExtractValue="Always" ItemStyle-BackColor="Olive"
                                                            FilterControlAltText="LightGreen"
                                                            DataSourceID="dsImportAction" ListTextField="Name" ListValueField="Name" DefaultInsertValue="Add">
                                </telerik:GridDropDownColumn>
                                <telerik:GridBoundColumn DataField="ImportOutcome" UniqueName="ImportOutcome" HeaderText="Import Outcome"
                                                         ReadOnly="true" Display="false" ForceExtractValue="Always" ItemStyle-BackColor="Olive" />
                                <telerik:GridBoundColumn DataField="spacer" UniqueName="spacer" ReadOnly="true" HeaderStyle-Width="90px" />
                            </Columns>
                            <EditFormSettings CaptionFormatString="Edit" InsertCaption="Create" PopUpSettings-ShowCaptionInEditForm="false" PopUpSettings-Width="796" ColumnNumber="3">
                                <EditColumn ButtonType="ImageButton" 
                                            CancelImageUrl="~/images/cancel_imageonly.png"
                                            UpdateImageUrl="~/images/apply_imageonly.png" 
                                            InsertImageUrl="~/images/apply_imageonly.png" />
                            </EditFormSettings>
                            <PagerStyle AlwaysVisible="True" />
                        </MasterTableView>
                        <HeaderStyle Wrap="False" />
                        <PagerStyle AlwaysVisible="True" />
                        <FilterMenu EnableImageSprites="False" />
                    </telerik:RadGrid>
                    <telerik:RadScriptBlock ID="rsbMain" runat="server">
                        <script src="/scripts/radgrid_rates.js" type="text/javascript" ></script>
                    </telerik:RadScriptBlock>
                </div>
            </div>
        </div>
    </div>
    <blac:DBDataSource ID="dsMain" runat="server" SelectIDNullsToZero="true"
                       SelectCommand="SELECT *, ImportAction='None', spacer=NULL FROM dbo.fnShipperOrderRejectRatesDisplay(isnull(@StartDate, getdate()), coalesce(@EndDate, @StartDate, getdate()), @ReasonID, @ShipperID, @ProductGroupID, @TruckTypeID, @OriginID, @StateID, @RegionID, @ProducerID) ORDER BY Shipper, EffectiveDate DESC">
        <SelectParameters>
            <asp:ControlParameter Name="ReasonID" ControlID="ddReason" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
            <asp:ControlParameter Name="ShipperID" ControlID="ddShipper" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
            <asp:ControlParameter Name="ProductGroupID" ControlID="ddProductGroup" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
            <asp:ControlParameter Name="TruckTypeID" ControlID="ddTruckType" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
            <asp:ControlParameter Name="OriginID" ControlID="ddOrigin" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
            <asp:ControlParameter Name="StateID" ControlID="ddState" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
            <asp:ControlParameter Name="RegionID" ControlID="ddRegion" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
            <asp:ControlParameter Name="ProducerID" ControlID="ddProducer" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
            <asp:ControlParameter Name="StartDate" ControlID="rdpStartDate" PropertyName="DbSelectedDate" Type="String" DefaultValue="NULL" />
            <asp:ControlParameter Name="EndDate" ControlID="rdpEndDate" PropertyName="DbSelectedDate" Type="String" DefaultValue="NULL" />
        </SelectParameters>
    </blac:DBDataSource>
    <blc:RadGridDBCtrl ID="dbcMain" runat="server"
                       ControlID="rgMain"
                       UpdateTableName="tblShipperOrderRejectRate"
                       FilterActiveEntities="False" />
    <blac:DBDataSource ID="dsReason" runat="server" SelectCommand="SELECT ID, Name FROM (SELECT ID, Name = dbo.fnNameWithDeleted(NumDesc, DeleteDateUTC), Active = dbo.fnIsActive(DeleteDateUTC) FROM viewOrderRejectReason UNION SELECT 0, '(All)', 1) X ORDER BY Active DESC, Name" />
    <blac:DBDataSource ID="dsShipper" runat="server" SelectCommand="SELECT ID, Name FROM dbo.tblCustomer UNION SELECT 0, '(All)' ORDER BY Name" />
    <blac:DBDataSource ID="dsProductGroup" runat="server" SelectCommand="SELECT ID, Name FROM dbo.tblProductGroup UNION SELECT 0, '(All)' ORDER BY Name" />
    <blac:DBDataSource ID="dsTruckType" runat="server" SelectCommand="SELECT ID, Name FROM dbo.tblTruckType UNION SELECT 0, '(All)' ORDER BY Name" />
    <blac:DBDataSource ID="dsOrigin" runat="server" SelectCommand="SELECT ID, Name FROM dbo.viewOrigin UNION SELECT 0, '(All)' ORDER BY Name" />
    <blac:DBDataSource ID="dsState" runat="server" SelectCommand="SELECT ID, Name = FullName FROM dbo.tblState UNION SELECT 0, '(All)' ORDER BY Name" />
    <blac:DBDataSource ID="dsRegion" runat="server" SelectCommand="SELECT ID, Name FROM dbo.tblRegion UNION SELECT 0, '(All)' ORDER BY Name" />
    <blac:DBDataSource ID="dsProducer" runat="server" SelectCommand="SELECT ID, Name FROM dbo.tblProducer UNION SELECT 0, '(All)' ORDER BY Name" />
    <blac:DBDataSource ID="dsRateType" runat="server" SelectCommand="SELECT ID, Name FROM dbo.tblRateType WHERE ForShipper = 1 AND ForOrderReject = 1 ORDER BY Name" />
    <blac:DBDataSource ID="dsUom" runat="server" SelectCommand="SELECT ID, Name FROM dbo.tblUOM ORDER BY Name" />
    <blac:DBDataSource ID="dsImportAction" runat="server" SelectCommand="SELECT Name = 'None' UNION SELECT 'Add' UNION SELECT 'Update' UNION SELECT 'Delete'" />
</asp:Content>
