﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CtrlDriverSettlement.ascx.cs" Inherits="DispatchCrude.Site.Financials.CtrlDriverSettlement" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<style type="text/css">
    .rgEditForm {
            width: auto !important;
    }
    .rgEditForm > div + div,
    .RadGrid .rgEditForm {
            height: auto !important;
    }
    .rgEditForm > div > table{
            height: 100%;
    }
    .rgEditForm > div > table > tbody > tr > td{
            padding: 4px 10px;
    }
    .H2S
    {
        color: red;
    }
    .Deleted
    {
        color: gray;
    }
    .label
    {
        width: 150px;
        color:black;
        padding-left: 0px !important;
        text-align: left !important;
    }
    .spacer 
    {
        height: 3px;
    }
</style>
<link href='<%= Page.ResolveUrl("~/Styles/tablemx.css?version=1.0")%>' rel="stylesheet" type="text/css" />
<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server" >
    <script type="text/javascript">
        function validateItemSpecified(sender, args) {
            debugger;
            //invalid if first character is a "("
            args.IsValid = !args.Value.startsWith("(");
        };
    </script>
</telerik:RadScriptBlock>
<asp:Table id="tblMain" runat="server" border="0" style="border-collapse: collapse">
    <asp:TableRow ID="trData" runat="server" style="vertical-align:top;" >
        <asp:TableCell runat="server" >
            <div class="label">
                  <asp:Label runat="server" Text="Origin Wait Amount:" />
            </div>
            <div class ="data">
                <asp:HiddenField ID="hfOrderID" runat="server" ClientIDMode="Static" Value='<%# Eval("ID") %>' />
                <telerik:RadNumericTextBox ID="rntxOriginWaitAmount" runat="server" ClientIDMode="Static" Width="100px"
                    Type="Currency" DataType="Decimal" EnabledStyle-HorizontalAlign="Right" NumberFormat-DecimalDigits="2" />
            </div>
            <div class="spacer" ></div>
            <div class="label">
                  <asp:Label runat="server" Text="Destination Wait Amount:" />
            </div>
            <div class ="data">
                <telerik:RadNumericTextBox ID="rntxDestinationWaitAmount" runat="server" ClientIDMode="Static" Width="100px"
                    Type="Currency" DataType="Decimal" EnabledStyle-HorizontalAlign="Right" NumberFormat-DecimalDigits="2" />
            </div>
            <div class="spacer" ></div>
            <div class="label">
                  <asp:Label runat="server" Text="Order Reject Amount:" />
            </div>
            <div class ="data">
                <telerik:RadNumericTextBox ID="rntxOrderRejectAmount" runat="server" ClientIDMode="Static" Width="100px"
                    Type="Currency" DataType="Decimal" EnabledStyle-HorizontalAlign="Right" NumberFormat-DecimalDigits="2" />
            </div>
            <div class="spacer" ></div>
            <div class="label">
                  <asp:Label runat="server" Text="Fuel Surcharge Amount:" />
            </div>
            <div class ="data">
                <telerik:RadNumericTextBox ID="rntxFuelSurchargeAmount" runat="server" ClientIDMode="Static" Width="100px"
                    Type="Currency" DataType="Decimal" EnabledStyle-HorizontalAlign="Right" NumberFormat-DecimalDigits="2" />
            </div>
            <div class="spacer" ></div>
            <div class="label">
                  <asp:Label runat="server" Text="Load Amount:" />
            </div>
            <div class ="data">
                <telerik:RadNumericTextBox ID="rntxLoadAmount" runat="server" ClientIDMode="Static" Width="100px"
                    Type="Currency" DataType="Decimal" EnabledStyle-HorizontalAlign="Right" NumberFormat-DecimalDigits="2" />
            </div>
       </asp:TableCell>
        <asp:TableCell>
            <div class="label">
                  <asp:Label runat="server" Text="Accessorial Rate Amounts:" />
            </div>
            <div class ="data">
                <telerik:RadGrid ID="rgAR" runat="server" ClientIDMode="Static" Width="400px" Height="200px" 
                    AllowSorting="True" CellSpacing="0" GridLines="Both" 
                    AutoGenerateColumns="False" OnNeedDataSource="rgAR_NeedDataSource" OnItemCommand="rgAR_ItemCommand" >
                    <ClientSettings>
                        <Scrolling AllowScroll="True" UseStaticHeaders="True" FrozenColumnsCount="0" />
                    </ClientSettings>
                    <MasterTableView DataKeyNames="AssessorialRateTypeID" CommandItemDisplay="Top" EnableViewState="True" EditMode="InPlace" >
                        <BatchEditingSettings EditType="Row" />
                        <CommandItemSettings ShowRefreshButton="false" ShowAddNewRecordButton="true" AddNewRecordText="Add New Accessorial Rate Override Amount" />
                        <Columns>
                            <telerik:GridTemplateColumn UniqueName="ActionColumn" HeaderStyle-Width="100px" AllowFiltering="false" >
                                <ItemTemplate>
                                    <asp:ImageButton runat="server" CssClass="btn btn-xs btn-default shiny" CommandName="Edit" ImageUrl="~/images/edit.png" CausesValidation="false" />
                                    <asp:ImageButton runat="server" CssClass="btn btn-xs btn-default shiny" id="cmdDelete" CommandName="Delete" ImageUrl="~/images/delete.png" CausesValidation="false" />
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:ImageButton runat="server" CssClass="btn btn-xs btn-default shiny" CommandName="Update" ImageUrl="~/images/apply_imageonly.png" CausesValidation="true" />
                                    <asp:ImageButton runat="server" CssClass="btn btn-xs btn-default shiny" CommandName="Cancel" ImageUrl="~/images/cancel_imageonly.png" CausesValidation="false" />
                                </EditItemTemplate>
                            </telerik:GridTemplateColumn>

                            <telerik:GridDropDownColumn UniqueName="AssessorialRateTypeID" DataField="AssessorialRateTypeID" HeaderText="Rate Type"
                                DataSourceID="dsAssessorialRateType" ListValueField="ID" ListTextField="Name" >
                                <ColumnValidationSettings>
                                    <RequiredFieldValidator Enabled="true" InitialValue="" CssClass="NullValidator"
                                        ErrorMessage="Accessorial Rate Type is required" Text="!" ValidationGroup="SettlementEdit" />
                                </ColumnValidationSettings>
                            </telerik:GridDropDownColumn>
                            <telerik:GridNumericColumn UniqueName="Amount" DataField="Amount" HeaderText="Override Amount" 
                                NumericType="Currency" ItemStyle-HorizontalAlign="Right" DecimalDigits="2" DataFormatString="{0:#,##0.00}">
                                <ColumnValidationSettings>
                                    <RequiredFieldValidator Enabled="true" InitialValue="" 
                                        CssClass="NullValidator" ErrorMessage="Amount is required" Text="!" ValidationGroup="SettlementEdit" />
                                </ColumnValidationSettings>
                            </telerik:GridNumericColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
            </div>
        </asp:TableCell>
        <asp:TableCell>
            <asp:ValidationSummary ID="validationSummaryMain" runat="server" DisplayMode="BulletList" Enabled="true" EnableClientScript="true"
                ValidationGroup="SettlementEdit"
                CssClass="NullValidator" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow runat="server" style="vertical-align:top;" >
        <asp:TableCell runat="server" ColumnSpan="2" >
            <div class="label">
                  <asp:Label runat="server" Text="Settlement Notes:" />
            </div>
            <div class ="data">
                <telerik:RadTextBox ID="rtxtOrderNotes" runat="server" ClientIDMode="Static" Width="100%" TextMode="MultiLine" Rows="3" MaxLength="255" />
            </div>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow id="trControls" runat="server">
        <asp:TableCell runat="server" style="text-align:left" ColumnSpan="1">
            <div style="height:10px;" ></div>
            <asp:Button ID="cmdUpdate" Text="Update" runat="server" CssClass="btn btn-blue shiny" 
                CommandName="Update" CausesValidation="true" ValidationGroup="SettlementEdit"
                OnClick="UpdateDataItem" 
                Visible='<%# !(DataItem is Telerik.Web.UI.GridInsertionObject) %>'>
            </asp:Button>
            <asp:Button Text="Save New" runat="server" CssClass="floatLeft"
                CommandName="PerformInsert" CausesValidation="true" ValidationGroup="SettlementEdit"
                OnClick="UpdateDataItem" 
                Visible='<%# (DataItem is Telerik.Web.UI.GridInsertionObject) %>' />
            &nbsp;
            <asp:Button ID="cmdCancel" runat="server" Text="Cancel" CssClass="btn btn-blue shiny" 
                CommandName="Cancel" CausesValidation="False" ValidationGroup="SettlementEdit" />
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
<blac:DBDataSource ID="dsAR" runat="server" SelectCommand="SELECT * FROM dbo.tblOrderSettlementDriverAssessorialCharge WHERE OrderID=@OrderID" >
    <SelectParameters>
        <asp:ControlParameter Name="OrderID" ControlID="hfOrderID" DbType="Int32" PropertyName="Value" DefaultValue="0" />
    </SelectParameters>
</blac:DBDataSource>
<blac:DBDataSource ID="dsAssessorialRateType" runat="server" SelectCommand="SELECT ID, Name FROM dbo.tblAssessorialRateType WHERE DeleteDateUTC IS NULL ORDER BY Name" />