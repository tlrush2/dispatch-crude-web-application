﻿using System;
using System.IO;
//add for sql stuff
using System.Data;
using System.Data.SqlClient;
using Telerik.Web.UI;
using AlonsIT;
using DispatchCrude.App_Code;
using DispatchCrude.Core;
using DispatchCrude.DataExchange;
using DispatchCrude.Extensions;
using System.Web.Services;
using DispatchCrude.Models;

namespace DispatchCrude.Site.Financials
{
    public partial class CarrierInvoicing : System.Web.UI.Page
    {
        static public string SESSION_DATA_SESSIONID = "SESSION_CARRIER_SETTLEMENT_SESSIONID"
            , SESSION_DATA = "SESSION_CARRIER_SETTLEMENT_DATATABLE";

        static protected string CHANGES_ENABLED = "DeleteEnabled", REPORT_SOURCE = "SourceTable";

        // set to true by actions that force a new Data Session (cmdFilter, cmdApplyRates)
        protected bool _startSession = false;

        protected void Page_Init(object sender, EventArgs e)
        {
            rdpEnd.SelectedDate = DateTime.Now.Date.AddDays(-1); // today.AddDays(7);
            rdpStart.Calendar.ShowRowHeaders = rdpEnd.Calendar.ShowRowHeaders = false;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ConfigureAjax(!Settings.SettingsID.DisableAJAX.AsBool());

            //Print tab navigation html to page
            tabmenu.InnerHtml = NavigationHelper.PrintTabArray(NavigationHelper.TabSet_Settlement, "Tab_Settle").ToString();

            //Print button navigation html to page
            speedbuttons.InnerHtml = NavigationHelper.PrintButtonArray(NavigationHelper.ButtonSet_TabSettlement, "Button_Carrier").ToString();

            // Show/hide pending batches drop down and pending status on action tab based on system setting
            panelPendingBatches.Visible = panelFinalize.Visible = ddlPendingStatus.Visible = lblPendingStatus.Visible 
                = Settings.SettingsID.Settlement_IncludePendingStatus.AsBool();
            // Set appropriate confirm text for deleting a batch
            cmdDeletePendingBatch.OnClientClick = "javascript:if(!confirm('" + SettlementBatch.UNSETTLE_WARNING_TEXT + "')){return false;}";

            if (!IsPostBack)
            {
                dsUserReport.SelectParameters["UserName"].DefaultValue = UserSupport.UserName;
            }
            else // ensure any existing error is reset
                ShowError("");
        }

        [WebMethod]
        static public bool? UpdateBatchAllSel(int sessionID, bool selected)
        {
            return Models.CarrierOrderFinancials.UpdateSessionBatchSel(sessionID, null, selected);
        }
        [WebMethod]
        static public bool? UpdateBatchSel(int sessionID, int orderID, bool selected)
        {
            return Models.CarrierOrderFinancials.UpdateSessionBatchSel(sessionID, orderID, selected);
        }
        [WebMethod]
        static public bool? UpdateRateApplyAllSel(int sessionID, bool selected)
        {
            return Models.CarrierOrderFinancials.UpdateSessionRateApplySel(sessionID, null, selected);
        }
        [WebMethod]
        static public bool? UpdateRateApplySel(int sessionID, int orderID, bool selected)
        {
            return Models.CarrierOrderFinancials.UpdateSessionRateApplySel(sessionID, orderID, selected);
        }
        [WebMethod]
        static public Object GetSessionSel(int sessionID)
        {
            bool? batchSel = null
                , rateApplySel = null;
            Models.CarrierOrderFinancials.UpdateSessionSel(sessionID, null, ref batchSel, ref rateApplySel);
            return new { BatchSel = batchSel, RateApplySel = rateApplySel };
        }

        private void ConfigureAjax(bool enabled = true)
        {
            if (enabled)
            {
                // necessary to ensure the calendar popup displayes on Chrome
                rdpStart.EnableAjaxSkinRendering = rdpEnd.EnableAjaxSkinRendering = true;
                RadAjaxHelper.AddAjaxSetting(this.Page, ddlCarrier, cmdSettleBatch);
                RadAjaxHelper.AddAjaxSetting(this.Page, ddlCarrier, cmdApplyRates);
                RadAjaxHelper.AddAjaxSetting(this.Page, ddlCarrier, txtInvoiceNum);
                RadAjaxHelper.AddAjaxSetting(this.Page, ddlCarrier, txtNotes);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, cmdFilter, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, rgMain);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, cmdApplyRates, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, cmdSettleBatch, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, cmdExport, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, txtNotes, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, txtInvoiceNum, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, hfSessionID, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, txtErrors, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdApplyRates, cmdApplyRates);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdApplyRates, rgMain);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdApplyRates, hfSessionID, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdApplyRates, txtErrors);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdSettleBatch, cmdSettleBatch);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdSettleBatch, cmdApplyRates, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdSettleBatch, rgMain);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdSettleBatch, txtNotes, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdSettleBatch, txtInvoiceNum, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdSettleBatch, txtErrors, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdSettleBatch, radWindowManager, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, rgMain, rgMain);
                RadAjaxHelper.AddAjaxSetting(this.Page, rgMain, txtNotes, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, rgMain, txtInvoiceNum, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, rgMain, txtErrors, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdExport, cmdExport);
                if (Settings.SettingsID.Settlement_IncludePendingStatus.AsBool())
                {
                    RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, ddlPendingStatus, false);
                    RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, lblPendingStatus, false);
                    RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, lblPendingBatchNum);
                    RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, txtPendingBatchNum);
                    RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, lblPendingInvoiceNum);
                    RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, txtPendingInvoiceNum);
                    RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, lblPendingNotes);
                    RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, txtPendingNotes);
                    RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, cmdDeletePendingBatch, false);
                    RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, cmdFinalizeBatch);
                }
            }
            RadAjaxManager.GetCurrent(this.Page).EnableAJAX = enabled;
        }
        
        protected void cmdFilter_Click(object source, EventArgs e)
        {
            // refresh the grid
            rgMain.CurrentPageIndex = 0;
            rgMain.DataSource = null;
            _startSession = true;
            rgMain.Rebind();
            cmdFilter.Enabled = true;
            if (ddlPendingBatches.SelectedIndex > 0)
            {
                // hide settlement panel but show finalized panel
                panelSettle.Visible = false;
                panelFinalize.Visible = true;
            }
            else
            {
                // ensure these "Settlement" fields are reset
                txtInvoiceNum.Text = txtNotes.Text = "";
                // ensure settlement panel is visible and hide finalized panel
                panelSettle.Visible = true;
                panelFinalize.Visible = false;
            }
        }

        protected void cmdApplyRates_Click(object source, EventArgs e)
        {
            ApplyRates(chkResetManualRates.Checked);
            _startSession = true;
            rgMain.Rebind();
            cmdApplyRates.Enabled = true;
        }

        private void AlertUser(string userMsg, int width = 300, int height = 75)
        {
            radWindowManager.RadAlert(userMsg, width, height, "Settlement Feedback", null);
        }
        private void ShowError(string error)
        {
            txtErrors.Text = error;
        }

        protected void cmdSettleBatch_Click(object sender, EventArgs e)
        {
            bool isFinal = ddlPendingStatus.SelectedValue.ToString() != "0"; // only applicable if pending statuses are turned on (system setting)
            // update the orders as SETTLED (with a Settlement Batch)
            using (SSDB db = new SSDB())
            {
                using (SqlCommand cmd = db.BuildCommand("spCreateCarrierSettlementBatch"))
                {
                    cmd.Parameters["@SessionID"].Value = hfSessionID.Value;
                    cmd.Parameters["@CarrierID"].Value = ddlCarrier.SelectedValue;
                    cmd.Parameters["@PeriodEndDate"].Value = rdpEnd.DbSelectedDate;
                    cmd.Parameters["@InvoiceNum"].Value = Converter.ToDBNullFromEmpty(txtInvoiceNum.Text);
                    cmd.Parameters["@Notes"].Value = Converter.ToDBNullFromEmpty(txtNotes.Text);
                    cmd.Parameters["@UserName"].Value = User.Identity.Name;
                    cmd.Parameters["@IsFinal"].Value = (isFinal) ? true : false;
                    cmd.ExecuteNonQuery();
                    if (cmd.Parameters["@Outcome"].Value.ToString().ToLower() == "error")
                    {
                        ShowError(cmd.Parameters["@Msg"].Value.ToString());
                    }
                    else
                    {
                        object batchID = cmd.Parameters["@BatchID"].Value;
                        // navigate to the new batch
                        string url = "/Site/Financials/" + (isFinal ? "CarrierBatches" : "CarrierInvoicing") + ".aspx?BatchID={0}";
                        Response.Redirect(string.Format(url, batchID));
                    }
                }
            }   
        }

        protected void cmdDeletePendingBatch_Click(object source, EventArgs e)
        {
            // delete the pending batch
            string BatchID = DropDownListHelper.SelectedValue(ddlPendingBatches).ToString();
            new CarrierSettlementBatch().DeleteBatch(BatchID);

            // reload page clean
            Response.Redirect("/Site/Financials/CarrierInvoicing.aspx");
        }

        protected void cmdFinalizeBatch_Click(object sender, EventArgs e)
        {
            // update the pending batch as Settled - Final
            string BatchID = DropDownListHelper.SelectedValue(ddlPendingBatches).ToString();
            new CarrierSettlementBatch().FinalizeBatch(BatchID, txtPendingInvoiceNum.Text, txtPendingNotes.Text);

            // navigate to the new batch
            Response.Redirect(string.Format("/Site/Financials/CarrierBatches.aspx?BatchID={0}", BatchID));
        }
        private void ApplyRates(bool resetOverrides = false)
        {
            using (SSDB db = new SSDB(false))
            {
                using (SqlCommand cmd = db.BuildCommand("spApplyRatesCarrierSession"))
                {
                    cmd.Parameters["@SessionID"].Value = hfSessionID.Value;
                    cmd.Parameters["@UserName"].Value = User.Identity.Name;
                    cmd.Parameters["@ResetOverrides"].Value = resetOverrides;
                    try
                    {
                        cmd.ExecuteNonQuery();
                        AlertUser(string.Format("Rates Applied to {0} orders", cmd.Parameters["@ApplyCount"].Value));
                    }
                    catch (Exception ex)
                    {
                        ShowError(string.Format("Error Applying Rates:\n{0}", ex.Message));
                    }
                }
            }
        }

        private DataTable GetData(bool startSession, ref string sessionID, bool useSessionCache = false)
        {
            DataTable ret = null;
            if (!useSessionCache 
                || startSession 
                || sessionID.ToInt32() != Session[SESSION_DATA_SESSIONID].ToInt32() || (ret = Session[SESSION_DATA] as DataTable) == null)
            {
                if (Settings.SettingsID.Settlement_IncludePendingStatus.AsBool() && DropDownListHelper.SelectedValue(ddlPendingBatches) > 0)
                {
                    ret = Models.CarrierOrderFinancials.GetDataTable(null
                        , null
                        , -1
                        , -1
                        , -1
                        , -1
                        , -1
                        , -1
                        , -1
                        , -1
                        , DropDownListHelper.SelectedValue(ddlPendingBatches)
                        , false
                        , null
                        , null
                        , -1
                        , ref sessionID
                        , startSession);
                }
                else
                {
                    ret = Models.CarrierOrderFinancials.GetDataTable(rdpStart.SelectedDate
                        , rdpEnd.SelectedDate.HasValue ? rdpEnd.SelectedDate : DateTime.Now.Date
                        , DropDownListHelper.SelectedValue(ddlShipper)
                        , DropDownListHelper.SelectedValue(ddlCarrier)
                        , DropDownListHelper.SelectedValue(ddlProductGroup)
                        , DropDownListHelper.SelectedValue(ddlTruckType)
                        , DropDownListHelper.SelectedValue(ddlDriverGroup)
                        , DropDownListHelper.SelectedValue(ddlOriginState)
                        , DropDownListHelper.SelectedValue(ddlDestState)
                        , DropDownListHelper.SelectedValue(ddlProducer)
                        , null
                        , chkOnlyShipperSettled.Checked
                        , txtJobNumber.Text
                        , txtContractNumber.Text
                        , DropDownListHelper.SelectedValue(ddlRejected)
                        , ref sessionID
                        , startSession);
                }
                if (useSessionCache)
                {
                    Session[SESSION_DATA_SESSIONID] = sessionID;
                    Session[SESSION_DATA] = ret;
                }
            }
            return ret;
        }

        protected void grid_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            RadGrid grid = (sender as RadGrid);
            bool startSession = e.RebindReason == GridRebindReason.InitialLoad || (_startSession && e.RebindReason == GridRebindReason.ExplicitRebind);
            _startSession = false;  // reset now
            string sessionID = startSession ? null : hfSessionID.Value;
            grid.DataSource = GetData(startSession, ref sessionID);
            hfSessionID.Value = sessionID;
            // for new session, always set the grid to the first page
            if (startSession) grid.CurrentPageIndex = 0;
        }

        protected void grid_DataBound(object sender, EventArgs e)
        {
            cmdFilter.Enabled = true;
            cmdApplyRates.Enabled = cmdExport.Enabled = chkResetManualRates.Enabled
                = rgMain.Items.Count > 0;
            // only allow settling a batch for a specific Carrier
            cmdSettleBatch.Enabled = txtInvoiceNum.Enabled = txtNotes.Enabled = ddlPendingStatus.Enabled
                = rgMain.Items.Count > 0 && DBHelper.ToInt32(ddlCarrier.SelectedValue) > 0;
            // only allow settling a pending batch
            cmdFinalizeBatch.Enabled = cmdDeletePendingBatch.Enabled = txtPendingInvoiceNum.Enabled = txtPendingNotes.Enabled
                = rgMain.Items.Count > 0 && DBHelper.ToInt32(ddlPendingBatches.SelectedValue) > 0;
        }

        protected void grid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            // the popup editor has already handled the "update" so just terminate editing and rebind
            if (e.CommandName == RadGrid.UpdateCommandName || e.CommandName == RadGrid.PerformInsertCommandName || e.CommandName == RadGrid.CancelCommandName)
            {
                e.Item.Edit = false;
                (sender as RadGrid).DataSource = null;
                (sender as RadGrid).Rebind();
            }
        }

        protected void grid_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            SettlementRateCellHelper.grid_ItemDataBound(sender, e, "Carrier");
            if (e.Item is GridDataItem)
			{
                txtPendingBatchNum.Text = ((DataRowView)e.Item.DataItem)["BatchNum"].ToString();
                txtPendingInvoiceNum.Text = ((DataRowView)e.Item.DataItem)["InvoiceNum"].ToString();
                txtPendingNotes.Text = ((DataRowView)e.Item.DataItem)["Notes"].ToString();
            }
        }

        protected void rcbUserReport_ItemDataBound(object sender, RadComboBoxItemEventArgs e)
        {
            e.Item.Attributes.Add(CHANGES_ENABLED
                , (UserSupport.IsInRole("Administrator") || UserSupport.IsInRole("Management") || !DBHelper.IsNull((e.Item.DataItem as DataRowView)["UserNames"])).ToString());
            e.Item.Attributes.Add(REPORT_SOURCE, (e.Item.DataItem as DataRowView)[REPORT_SOURCE].ToString());
        }

        protected void cmdExport_Click(object source, EventArgs e)
        {
            if (rgMain.Items.Count > 0)
            {
                Export(DBHelper.ToInt32(hfSessionID.Value));
            }
        }

        private void Export(int sessionID)
        {
            string filename = "CARRIER_SETTLEMENT_PREVIEW.xlsx";
            MemoryStream ms = (MemoryStream)new ReportCenterExcelExport(UserSupport.UserName
                    , rcbUserReport.SelectedItem.Attributes[REPORT_SOURCE].ToString()
                    , RadComboBoxHelper.SelectedValue(rcbUserReport))
                .ExportCarrierSession(sessionID, true);
            Response.ExportExcelStream(ms, filename);
        }        
    }
}