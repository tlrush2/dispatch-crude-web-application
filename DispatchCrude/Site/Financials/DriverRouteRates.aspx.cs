﻿using System;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//add for sql stuff
using System.Data;
using System.Collections;
using System.Drawing;
using Telerik.Web.UI;
using AlonsIT;
using DispatchCrude.App_Code;
using DispatchCrude.Core;
using DispatchCrude.Extensions;

namespace DispatchCrude.Site.Financials
{
    public partial class DriverRouteRates : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            rdpStartDate.DbSelectedDate = DateTime.Now.Date;
            rdpEndDate.DbSelectedDate = DBNull.Value;
            rdpStartDate.Calendar.ShowRowHeaders = rdpEndDate.Calendar.ShowRowHeaders = false;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ConfigureAjax(!Core.Settings.SettingsID.DisableAJAX.AsBool());

            //Print tab navigation html to page
            tabmenu.InnerHtml = NavigationHelper.PrintTabArray(NavigationHelper.TabSet_RouteRates, "Tab_OverrideRates").ToString();

            //Print button navigation html to page
            speedbuttons.InnerHtml = NavigationHelper.PrintButtonArray(NavigationHelper.ButtonSet_OverrideRates, "Button_Driver").ToString();
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            // Hide ID column on the website
            rgMain.MasterTableView.Columns.FindByUniqueName("ID").Display = false;

            if (!IsPostBack)
            {
                DataRow dr = null;
                if (Request.QueryString["OrderID"] != null)
                {
                    using (SSDB ssdb = new SSDB())
                    {
                        DataTable dt = ssdb.GetPopulatedDataTable("SELECT * FROM viewOrder WHERE ID = {0}", Request.QueryString["OrderID"] as object);
                        if (dt.Rows.Count == 1)
                            dr = dt.Rows[0];
                    }
                }

                if (dr != null || Request.QueryString["ShipperID"] != null)
                {
                    ddShipper.DataBind();
                    DropDownListHelper.SetSelectedValue(ddShipper, dr != null ? dr["CustomerID"].ToString() : Request.QueryString["ShipperID"]);
                }
                if (dr != null || Request.QueryString["CarrierID"] != null)
                {
                    ddCarrier.DataBind();
                    DropDownListHelper.SetSelectedValue(ddCarrier, dr != null ? dr["CarrierID"].ToString() : Request.QueryString["CarrierID"]);
                }
                if (dr != null || Request.QueryString["ProductGroupID"] != null)
                {
                    ddProductGroup.DataBind();
                    DropDownListHelper.SetSelectedValue(ddProductGroup, dr != null ? dr["ProductGroupID"].ToString() : Request.QueryString["ProductGroupID"]);
                }
                if (dr != null || Request.QueryString["DriverGroupID"] != null)
                {
                    ddDriverGroup.DataBind();
                    DropDownListHelper.SetSelectedValue(ddDriverGroup, dr != null ? dr["DriverGroupID"].ToString() : Request.QueryString["DriverGroupID"]);
                }
                if (dr != null || Request.QueryString["DriverID"] != null)
                {
                    ddDriver.DataBind();
                    DropDownListHelper.SetSelectedValue(ddDriver, dr != null ? dr["DriverID"].ToString() : Request.QueryString["DriverID"]);
                }
                if (dr != null || Request.QueryString["OriginID"] != null)
                {
                    ddOrigin.DataBind();
                    DropDownListHelper.SetSelectedValue(ddOrigin, dr != null ? dr["OriginID"].ToString() : Request.QueryString["OriginID"]);
                }
                if (dr != null || Request.QueryString["DestinationID"] != null)
                {
                    ddDestination.DataBind();
                    DropDownListHelper.SetSelectedValue(ddDestination, dr != null ? dr["DestinationID"].ToString() : Request.QueryString["DestinationID"]);
                }
                if (dr != null || Request.QueryString["EffectiveDate"] != null)
                {
                    DateTime effectiveDate = DateTime.Now.Date;
                    if (DateTime.TryParse(dr != null ? dr["OrderDate"].ToString() : Request.QueryString["EffectiveDate"], out effectiveDate))
                    {
                        rdpStartDate.SelectedDate = effectiveDate;
                    }
                }
                rgMain.Rebind();
            }
        }

        private void ConfigureAjax(bool enabled = true)
        {
            if (enabled)
            {
                RadAjaxHelper.AddAjaxSetting(this.Page, btnRefresh, rgMain, true);
                RadAjaxHelper.AddAjaxSetting(this.Page, rgMain, rgMain);
            }
            RadAjaxManager.GetCurrent(this.Page).EnableAJAX = enabled;
        }

        protected void filterValueChanged(object sender, EventArgs e)
        {
            rgMain.Rebind();
        }

        protected void grid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.EditCommandName)
            {
                if (e.Item is GridEditableItem)
                {
                }
            }
            if (e.CommandName == "AddNew" && e.Item is GridDataItem) //Row "Add new" button clicked
            {
                // cancel the default operation
                e.Canceled = true;

                e.Item.OwnerTableView.InsertItem(GetRowValues(e.Item as GridDataItem));
            }
            else if (e.CommandName == RadGrid.InitInsertCommandName)
            {
                // cancel the default operation
                e.Canceled = true;

                e.Item.OwnerTableView.InsertItem(GetNewRowValues(e.Item as GridDataItem));
            }
            else if (e.CommandName == "ExportToExcel")
            {
                e.Canceled = true;
                ExportGridToExcel();
            }
        }

        private Hashtable GetRowValues(GridEditableItem gdi)
        {
            //Prepare an IDictionary with the predefined values
            Hashtable ret = new Hashtable();
            gdi.ExtractValues(ret);
            
            DateTime date = DBHelper.ToDateTime(
                gdi["EffectiveDate"].Controls[1] is Label
                    ? (gdi["EffectiveDate"].Controls[1] as Label).Text
                    : (gdi["EffectiveDate"].Controls[1] as RadDatePicker).DbSelectedDate);
            ret["EffectiveDate"] = date.Date;
            ret["EndDate"] = DateTime.Now.Date.AddYears(1);
            return ret;
        }
        private Hashtable GetNewRowValues(GridEditableItem gdi)
        {
            Hashtable ret = new Hashtable();
            // set ID to DBNULL (since we are creating a new record)
            ret["ID"] = DBNull.Value;
            ret["ShipperID"] = 0;
            ret["CarrierID"] = 0;
            ret["ProductGroupID"] = 0;
            ret["DriverGroupID"] = 0;
            ret["DriverID"] = 0;
            ret["OriginID"] = 0;
            ret["DestinationID"] = 0;
            ret["RateTypeID"] = 1;
            ret["UomID"] = 1;
            // default the new Effective Date to the first day of the current month
            DateTime effDate = DateTime.Now.Date.AddDays(1 - DateTime.Now.Day);
            ret["EffectiveDate"] = effDate;
            ret["EndDate"] = effDate.AddYears(1).AddDays(-1);
            return ret;
        }

        protected void cmdExport_Click(object sender, EventArgs e)
        {
            ExportGridToExcel();
        }
        protected void cvUpload_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = Path.GetExtension(excelUpload.FileName).ToLower() == ".xlsx";
        }

        protected void cmdImport_Click(object sender, EventArgs e)
        {
            Page.Validate();
            if (Page.IsValid)
            {
                FinancialImporter fi = new FinancialImporter();
                fi.AddSpec(rgMain, "ID", FinancialImporter.FISpec.FISType.ID);
                fi.AddSpec(rgMain, "ShipperID", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgMain, "CarrierID", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgMain, "ProductGroupID", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgMain, "TruckTypeID", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgMain, "DriverGroupID", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgMain, "DriverID", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgMain, "OriginID", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgMain, "DestinationID", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgMain, "Rate", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgMain, "RateTypeID", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgMain, "UomID", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgMain, "EffectiveDate", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgMain, "EndDate", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec("CreatedByUser", typeof(string), FinancialImporter.FISpec.FISType.NEW | FinancialImporter.FISpec.FISType.USERNAME);
                fi.AddSpec("CreateDateUTC", typeof(DateTime), FinancialImporter.FISpec.FISType.NEW | FinancialImporter.FISpec.FISType.NOW);
                fi.AddSpec("LastChangedByUser", typeof(string), FinancialImporter.FISpec.FISType.UPDATE | FinancialImporter.FISpec.FISType.USERNAME);
                fi.AddSpec("LastChangeDateUTC", typeof(DateTime), FinancialImporter.FISpec.FISType.UPDATE | FinancialImporter.FISpec.FISType.NOW);
                fi.AddSpec(rgMain, "ImportAction", FinancialImporter.FISpec.FISType.ACTION);
                fi.AddSpec(rgMain, "ImportOutcome", FinancialImporter.FISpec.FISType.OUTCOME);
                Response.ExportExcelStream(fi.ProcessSql(excelUpload.FileContent, dbcMain.UpdateTableName), Path.GetFileNameWithoutExtension(excelUpload.FileName) + "_ImportResults.xlsx");
            }
        }

        protected void CellBackColorChanged(GridDataItem gridRow, string colName, ref Color color)
        {
            switch (colName.ToLower())
            {
                case "shipperid":
                case "carrierid":
                case "productgroupid":
                case "trucktypeid":
                case "drivergroupid":
                case "driverid":
                case "originid":
                case "destinationid":
                case "ratetypeid":
                case "rate":
                case "uomid":
                case "effectivedate":
                    {
                        CheckBox chkLocked = RadGridHelper.GetControlByType(gridRow, "Locked", typeof(CheckBox)) as CheckBox;
                        if (chkLocked != null && chkLocked.Checked)
                            color = Color.White;
                        else
                            color = Color.LightGreen;
                        break;
                    }
                case "enddate":
                    color = Color.LightGreen;
                    break;
            }
        }

        private void ExportGridToExcel()
        {
            string filename = string.Format("Driver Override Route Rates as of {0:yyyyMMdd}.xlsx", rdpStartDate.SelectedDate);
            string[] hiddenToInclude = { "ID", "ImportAction", "ImportOutcome" }
                , visibleToSkip = { "CreateDate", "CreatedByUser", "LastChangeDate", "LastChangedByUser" };
            RadGridExcelExporter exporter = new RadGridExcelExporter(
                    hiddenColNamesToInclude: hiddenToInclude
                    , visibleColNamesToSkip: visibleToSkip
                    , dropDownColumnDataValidationList: true);
            rgMain.AllowPaging = false;
            rgMain.Rebind();
            exporter.OnCellBackColorChanged += CellBackColorChanged;
            Response.ExportExcelStream(exporter.ExportSheet(rgMain.MasterTableView, "Route Rates"), filename);
        }

        protected void grid_ItemCreated(object sender, GridItemEventArgs e)
        {
        }

        protected void grid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            DataRowView data = e.Item.DataItem as DataRowView;
            bool locked = data != null && DBHelper.ToBoolean(data["Locked"]);
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                if (data != null)
                {
                    DateTime? date = null;
                    if ((date = Converter.ToNullableDateTime(data["PriorEndDate"])).HasValue)
                        RadGridHelper.GetColumnDatePicker(e.Item, "EffectiveDate").MinDate = date.Value;
                    if ((date = Converter.ToNullableDateTime(data["MaxEffectiveDate"])).HasValue)
                        RadGridHelper.GetColumnDatePicker(e.Item, "EffectiveDate").MaxDate = date.Value;
                    if ((date = Converter.ToNullableDateTime(data["MinEndDate"])).HasValue)
                        RadGridHelper.GetColumnDatePicker(e.Item, "EndDate").MinDate = date.Value;
                    if ((date = Converter.ToNullableDateTime(data["NextEffectiveDate"])).HasValue)
                        RadGridHelper.GetColumnDatePicker(e.Item, "EndDate").MaxDate = date.Value;
                }

                RadGridHelper.GetColumnRadComboBox(e.Item, "ShipperID").Enabled = !locked;
                RadGridHelper.GetColumnRadComboBox(e.Item, "CarrierID").Enabled = !locked;
                RadGridHelper.GetColumnRadComboBox(e.Item, "ProductGroupID").Enabled = !locked;
                RadGridHelper.GetColumnRadComboBox(e.Item, "TruckTypeID").Enabled = !locked;
                RadGridHelper.GetColumnRadComboBox(e.Item, "DriverGroupID").Enabled = !locked;
                RadGridHelper.GetColumnRadComboBox(e.Item, "DriverID").Enabled = !locked;
                RadGridHelper.GetColumnRadComboBox(e.Item, "OriginID").Enabled = !locked;
                RadGridHelper.GetColumnRadComboBox(e.Item, "DestinationID").Enabled = !locked;
                (RadGridHelper.GetControlByType(e.Item, "EffectiveDate", typeof(RadDatePicker)) as RadDatePicker).Enabled = !locked;
            }
            else if (e.Item is GridDataItem && !e.Item.IsInEditMode)
            {
                // highlight best match records with a yellow background
                if (DBHelper.ToBoolean(data["BestMatch"])) e.Item.BackColor = Color.Goldenrod;

                ImageButton btnDelete = (ImageButton)RadGridHelper.GetColumnControlByType((e.Item as GridDataItem)["ActionColumn"], typeof(ImageButton), "btnDelete");
                if (btnDelete != null)
                {
                    btnDelete.Visible = btnDelete.Enabled = !locked;
                }
            }
        }

    }
}