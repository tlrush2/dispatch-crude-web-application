﻿using System;
using System.Web.UI.WebControls;
//add for sql stuff
using System.Data;
using System.Collections;
using Telerik.Web.UI;
using DispatchCrude.App_Code;
using DispatchCrude.Core;
using AlonsIT;
using DispatchCrude.Extensions;

namespace DispatchCrude.Site.Financial
{
    public partial class AccessorialRateTypes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ConfigureAjax(!Settings.SettingsID.DisableAJAX.AsBool());

            //Print tab navigation html to page
            tabmenu.InnerHtml = NavigationHelper.PrintTabArray(NavigationHelper.TabSet_Accessorial, "Tab_AccessorialRates").ToString();

            //Print button navigation html to page
            speedbuttons.InnerHtml = NavigationHelper.PrintButtonArray(NavigationHelper.ButtonSet_TabAccessorialRates, "Button_Types").ToString();
        }

        private void ConfigureAjax(bool enabled)
        {
            RadAjaxHelper.AddAjaxSetting(this.Page, rgMain, rgMain);
        }

        protected void grid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem && !e.Item.IsInEditMode)
            {
                DataRowView data = e.Item.DataItem as DataRowView;
                bool locked = DBHelper.ToBoolean(data["IsSystem"]);

                ImageButton btnDelete = App_Code.RadGridHelper.GetControlByType((e.Item as GridDataItem)["ActionColumn"], typeof(ImageButton), false, "btnDelete") as ImageButton;
                ImageButton btnEdit = App_Code.RadGridHelper.GetControlByType((e.Item as GridDataItem)["ActionColumn"], typeof(ImageButton), false, "btnEdit") as ImageButton;

                btnEdit.Enabled = !locked;
                btnEdit.Visible = !locked;
                btnDelete.Enabled = !locked;
                btnDelete.Visible = !locked;
            }
        }

        protected void grid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.InitInsertCommandName)
            {
                // cancel the default operation
                e.Canceled = true;
                Hashtable newValues = new Hashtable();
                newValues["IsSystem"] = false;
                e.Item.OwnerTableView.InsertItem(newValues);
            }
        }

    }
}