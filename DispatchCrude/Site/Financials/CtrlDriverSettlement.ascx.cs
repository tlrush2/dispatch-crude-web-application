﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using DispatchCrude.App_Code;
using AlonsIT;
using DispatchCrude.Core;

namespace DispatchCrude.Site.Financials
{
    public partial class CtrlDriverSettlement : System.Web.UI.UserControl
    {
        private const string SESSION_IDCSV = "DriverSettlement.IDCSV"
            , SESSION_AMOUNTCSV = "DriverSettlement.AmountCSV";

        private object _dataItem = null;

        protected void Page_Init(object sender, System.EventArgs e)
        {
        }

        private void ConfigureAjax(bool enabled)
        {
            RadAjaxHelper.AddAjaxSetting(this.Page, rgAR, rgAR);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            ConfigureAjax(!Settings.SettingsID.DisableAJAX.AsBool());
            if (hfOrderID.Value == "")
            {
                hfOrderID.Value = "0";
                // populate the Destination fields
                if (DataItem != null && DataItem is DataRowView)
                {
                    DataRowView dv = DataItem as DataRowView;
                    hfOrderID.Value = dv["ID"].ToString();
                    if (DBHelper.IsNull(dv["InvoiceOriginWaitRate"]))
                        rntxOriginWaitAmount.DbValue = Converter.ToDBNullFromEmpty(dv["InvoiceOriginWaitAmount"]);
                    if (DBHelper.IsNull(dv["InvoiceDestinationWaitRate"]))
                        rntxDestinationWaitAmount.DbValue = Converter.ToDBNullFromEmpty(dv["InvoiceDestinationWaitAmount"]);
                    if (DBHelper.IsNull(dv["InvoiceOrderRejectRate"]))
                        rntxOrderRejectAmount.DbValue = Converter.ToDBNullFromEmpty(dv["InvoiceOrderRejectAmount"]);
                    if (DBHelper.IsNull(dv["InvoiceFuelSurchargeRate"]))
                        rntxFuelSurchargeAmount.DbValue = Converter.ToDBNullFromEmpty(dv["InvoiceFuelSurchargeAmount"]);
                    if (DBHelper.IsNull(dv["InvoiceRouteRate"]) && DBHelper.IsNull(dv["InvoiceRateSheetRate"]))
                        rntxLoadAmount.DbValue = Converter.ToDBNullFromEmpty(dv["InvoiceLoadAmount"]);
                    rtxtOrderNotes.Text = dv["InvoiceNotes"].ToString();
                    using (SSDB ssdb = new SSDB())
                    {
                        DataTable dtARC = ssdb.GetPopulatedDataTable(
                            string.Format(
                                "SELECT AssessorialRateTypeID, Amount FROM tblOrderSettlementDriverAssessorialCharge WHERE OrderID={0} AND AssessorialRateID IS NULL"
                                , hfOrderID.Value));
                        Session[SESSION_IDCSV] = new IdContainer(dtARC, "AssessorialRateTypeID").CSV();
                        Session[SESSION_AMOUNTCSV] = new IdContainer(dtARC, "Amount").CSV();
                    }

                }
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///          Required method for Designer support - do not modify
        ///          the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion

        protected void rgAR_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            DataTable dtData = new DataTable();
            dtData.Columns.Add("AssessorialRateTypeID", typeof(System.Int32));
            dtData.Columns.Add("Amount", typeof(System.Decimal));
            IdContainer ids = new IdContainer(Session[SESSION_IDCSV].ToString())
                , amounts = new IdContainer(Session[SESSION_AMOUNTCSV].ToString());
            for (int i = 0; i < ids.Count; i++)
            {
                dtData.Rows.Add(new object[] { DBHelper.ToInt32(ids.ElementAt(i)), DBHelper.ToDecimalSafe(amounts.ElementAt(i)) });
            }
            dtData.AcceptChanges();
            rgAR.DataSource = dtData;
        }

        protected void rgAR_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName.Equals(RadGrid.UpdateCommandName) || e.CommandName.Equals(RadGrid.PerformInsertCommandName))
            {
                int artid = RadGridHelper.GetGridItemID(e.Item, "AssessorialRateTypeID");
                decimal amount = DBHelper.ToDecimalSafe((RadGridHelper.GetControlByType(e.Item, "Amount", typeof(RadNumericTextBox)) as RadNumericTextBox).DbValue);
                IdContainer ids = new IdContainer(Session[SESSION_IDCSV].ToString());
                StringList oldAmounts = new StringList(Session[SESSION_AMOUNTCSV].ToString()), newAmounts = new StringList();
                bool found = false;
                foreach (int id in ids.IntIDS)
                {
                    if (id == artid)
                    {
                        newAmounts.Add(amount.ToString());
                        found = true;
                    }
                    else
                        newAmounts.Add(oldAmounts.ElementAt(0));
                    oldAmounts.RemoveAt(0);
                }
                if (!found)
                {
                    ids.Add(artid.ToString());
                    Session[SESSION_IDCSV] = ids.CSV();
                    newAmounts.Add(amount.ToString());
                }
                Session[SESSION_AMOUNTCSV] = newAmounts.CSV();
                rgAR.DataSource = null;
                rgAR.Rebind();
            }
            else if (e.CommandName.Equals(RadGrid.DeleteCommandName))
            {
                int artid = RadGridHelper.GetGridItemID(e.Item, "AssessorialRateTypeID");
                IdContainer ids = new IdContainer(Session[SESSION_IDCSV].ToString());
                if (ids.Contains(artid.ToString()))
                {
                    StringList amounts = new StringList(Session[SESSION_AMOUNTCSV].ToString());
                    int pos = 0;
                    foreach (int id in ids.IntIDS)
                    {
                        if (id == artid)
                        {
                            amounts.RemoveAt(pos);
                            break;
                        }
                        pos++;
                    }
                    ids.Remove(artid.ToString());
                    Session[SESSION_IDCSV] = ids.CSV();
                    Session[SESSION_AMOUNTCSV] = amounts.CSV();
                }
                rgAR.DataSource = null;
                rgAR.Rebind();
            }
        }

        public object DataItem
        {
            get
            {
                return this._dataItem;
            }
            set
            {
                this._dataItem = value;
            }
        }

        protected void UpdateDataItem(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                int orderID = DBHelper.ToInt32(hfOrderID.Value);
                using (SSDB ssdb = new SSDB(true))
                {
                    using (System.Data.SqlClient.SqlCommand cmd = ssdb.BuildCommand("spApplyRatesDriver"))
                    {
                        cmd.Parameters["@ID"].Value = orderID;
                        cmd.Parameters["@UserName"].Value = UserSupport.UserName;
                        cmd.Parameters["@OriginWaitAmount"].Value = Converter.ToDBNullFromEmpty(rntxOriginWaitAmount.Value);
                        cmd.Parameters["@DestWaitAmount"].Value = rntxDestinationWaitAmount.Value;
                        cmd.Parameters["@RejectionAmount"].Value = rntxOrderRejectAmount.Value;
                        cmd.Parameters["@FuelSurchargeAmount"].Value = rntxFuelSurchargeAmount.Value;
                        cmd.Parameters["@LoadAmount"].Value = rntxLoadAmount.Value;
                        cmd.Parameters["@Notes"].Value = rtxtOrderNotes.Text;
                        cmd.Parameters["@AssessorialRateTypeID_CSV"].Value = Session[SESSION_IDCSV].ToString();
                        cmd.Parameters["@AssessorialAmount_CSV"].Value = Session[SESSION_AMOUNTCSV].ToString();
                        cmd.Parameters["@ResetOverrides"].Value = true;
                        cmd.ExecuteNonQuery();
                    }
                    ssdb.CommitTransaction();
                    DataTable sessionData = null;
                    DataRow sessionRow = null;
                    if ((sessionData = Session[DriverInvoicing.SESSION_DATA] as DataTable) != null && (sessionRow = sessionData.Select(string.Format("ID={0}", orderID)).FirstOrDefault()) != null)
                    {
                        DataRow drUpdated = ssdb.GetPopulatedDataTable("SELECT * FROM viewOrder_Financial_Driver WHERE ID = {0}", (object)orderID).Rows[0];
                        foreach (DataColumn col in sessionData.Columns)
                        {
                            if (drUpdated.Table.Columns.Contains(col.ColumnName))
                                sessionRow[col] = drUpdated[col.ColumnName];
                        }
                        sessionRow.AcceptChanges();
                    }
                }
            }
        }

    }

}