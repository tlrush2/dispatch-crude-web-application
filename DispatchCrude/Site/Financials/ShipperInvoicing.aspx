﻿<%@ Page Title="Settlement" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ShipperInvoicing.aspx.cs"
    Inherits="DispatchCrude.Site.Financials.ShipperInvoicing" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script src="/scripts/SettlementLogic.js" type="text/javascript"></script>
    <script>
        $("#ctl00_ctl00_EntityCaption").html("Shipper Settlement");
    </script>
    <div class="tabbable TabRepaint">
        <ul class="nav nav-tabs" id="tabmenu" runat="server">
            <!--Tabs will print here (see code behind)-->
        </ul>
        <div class="tab-content">
            <div class="tab-pane in active">

                <div id="speedbuttons" class="well speedButtonContainer" runat="server">
                    <!--Buttons will print here (see code behind)-->
                </div>

                <div class="leftpanel">
                    <div class="tabbable">
                        <ul class="nav nav-tabs">
                            <li class="active tab-blue">
                                <a data-toggle="tab" href="#Filters" aria-expanded="true">Filters</a>
                            </li>
                            <li class="tab-yellow">
                                <a data-toggle="tab" href="#ProcessActions" aria-expanded="true">Actions</a>
                            </li>
                            <li class="tab-green">
                                <a data-toggle="tab" href="#Export" aria-expanded="true">Export</a>
                            </li>
                            <li class="tab-red">
                                <a data-toggle="tab" class="aErrors" href="#Errors" aria-expanded="true">Errors</a>
                            </li>
                        </ul>
                        <div id="leftTabs" class="tab-content">
                            <div id="Filters" class="tab-pane active">
                                <asp:Panel ID="panelGenerate" runat="server" DefaultButton="cmdFilter">
                                    <div class="Entry">
                                        <asp:Label ID="lblTruckType" runat="server" Text="Truck Type" AssociatedControlID="ddlTruckType" CssClass="Entry" />
                                        <asp:DropDownList CssClass="btn-xs" Width="100%" ID="ddlTruckType" runat="server" DataTextField="Name" DataValueField="ID" DataSourceID="dsTruckType" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblShipper" runat="server" Text="Shipper" AssociatedControlID="ddlShipper" CssClass="Entry" />
                                        <asp:DropDownList CssClass="ddlRequired btn-xs" Width="100%" ID="ddlShipper" runat="server" DataTextField="FullName" DataValueField="ID" DataSourceID="dsShipper" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblJobNumber" runat="server" Text="Job #" AssociatedControlID="txtJobNumber" CssClass="Entry" />
                                        <asp:TextBox runat="server" width="100%" ID="txtJobNumber" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblContractNumber" runat="server" Text="Contract #" AssociatedControlID="txtContractNumber" CssClass="Entry" />
                                        <asp:TextBox runat="server" width="100%" ID="txtContractNumber" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblProductGroup" runat="server" Text="Product Group" AssociatedControlID="ddlProductGroup" CssClass="Entry" />
                                        <asp:DropDownList CssClass="btn-xs" Width="100%" ID="ddlProductGroup" runat="server" DataTextField="Name" DataValueField="ID" DataSourceID="dsProductGroup" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblOriginState" runat="server" Text="Origin State" AssociatedControlID="ddlOriginState" CssClass="Entry" />
                                        <asp:DropDownList CssClass="btn-xs" Width="100%" ID="ddlOriginState" runat="server" DataTextField="Name" DataValueField="ID" DataSourceID="dsState" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblDestState" runat="server" Text="Destination State" AssociatedControlID="ddlDestState" CssClass="Entry" />
                                        <asp:DropDownList CssClass="btn-xs" Width="100%" ID="ddlDestState" runat="server" DataTextField="Name" DataValueField="ID" DataSourceID="dsState" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblProducer" runat="server" Text="Producer" AssociatedControlID="ddlProducer" CssClass="Entry" />
                                        <asp:DropDownList CssClass="btn-xs" Width="100%" ID="ddlProducer" runat="server" DataTextField="Name" DataValueField="ID" DataSourceID="dsProducer" />
                                    </div>
                                    <div class="Entry">
                                        <asp:label id="lblRejected" runat="server" Text="Rejected Orders" AssociatedControlID="ddlRejected" />
                                        <asp:DropDownList CssClass="btn-xs" Width="100%" id="ddlRejected" runat="server">
                                            <asp:ListItem Text="(All)" Value="-1" />
                                            <asp:ListItem Text="Only Rejected Orders" Value="1" />
                                            <asp:ListItem Text="Exclude Rejected Orders" Value="0" />
                                        </asp:DropDownList>
                                    </div>
                                    <div>
                                        <div class="Entry floatLeft-date-row">
                                            <asp:Label ID="lblStartDate" runat="server" Text="Start Date" AssociatedControlID="rdpStart" CssClass="Entry" />
                                            <telerik:RadDatePicker ID="rdpStart" runat="server" Width="100px">
                                                <DateInput runat="server" DateFormat="M/d/yyyy" />
                                                <DatePopupButton Enabled="true" />
                                            </telerik:RadDatePicker>
                                        </div>
                                        <div class="Entry floatRight-date-row">
                                            <asp:Label ID="lblEndDate" runat="server" Text="End Date" AssociatedControlID="rdpEnd" CssClass="Entry" />
                                            <telerik:RadDatePicker ID="rdpEnd" runat="server" Width="100px">
                                                <DateInput runat="server" DateFormat="M/d/yyyy" />
                                                <DatePopupButton />
                                            </telerik:RadDatePicker>
                                        </div>
                                    </div>
                                    <br /><br /><br />
                                    <div class="clear"></div>
                                    <div class="spacer5px"></div>
                                    <div style="text-align: center;">
                                        <asp:HiddenField id="hfSessionID" runat="server" ClientIDMode="Static" />
                                        <asp:Button ID="cmdFilter" runat="server" Text="Refresh" CssClass="btn btn-blue shiny cmdFilter"
                                                    UseSubmitBehavior="false" OnClick="cmdFilter_Click" />
                                    </div>
                                    <asp:Panel ID="panelPendingBatches" runat="server">
                                        <hr />
                                        <div class="Entry">
                                            <asp:Label ID="lblPendingBatches" runat="server" Text="Pending Batches" AssociatedControlID="ddlPendingBatches" CssClass="Entry" />
                                            <asp:DropDownList CssClass="btn-xs ddlPendingBatches" Width="100%" ID="ddlPendingBatches" runat="server" DataTextField="Name" DataValueField="ID" 
                                                DataSourceID="dsPendingBatches" onchange="$('.cmdFilter').click()" />
                                        </div>
                                    </asp:Panel>
                                </asp:Panel>
                            </div>
                            <div id="ProcessActions" class="tab-pane">
                                <asp:Panel ID="panelSelectedCtrl" runat="server">
                                    <div class="center">
                                        <div class="btn-group">
                                            <asp:CheckBox ID="chkResetManualRates" runat="server" /> Reset Manual Rates
                                            <div class="clear"></div>
                                            <div class="spacer5px"></div>
                                            <asp:Button ID="cmdApplyRates" runat="server" Text="Apply Rates" 
                                                    CssClass="cmdApplyRate btn btn-sm btn-blue shiny"
                                                    UseSubmitBehavior="false" OnClick="cmdApplyRates_Click" />
                                        </div>
                                    </div>
                                    <div class="center">
                                        <hr />
                                        <asp:Panel ID="panelSettle" runat="server">
                                            <telerik:RadTextBox ID="txtInvoiceNum" runat="server" ClientIDMode="static" Width="100%" Enabled="false" EmptyMessage="External Invoice #" />
                                            <div class="spacer"></div>
                                            <telerik:RadTextBox ID="txtNotes" runat="server" ClientIDMode="static" TextMode="MultiLine" Width="100%" Height="33px" Enabled="false" EmptyMessage="Notes" />
                                            <table>
                                                <tr>
                                                    <td class="txtAlignLeft padding-right20px">
                                                        <asp:Label ID="lblPendingStatus" runat="server" Text="Status" AssociatedControlID="ddlPendingStatus" CssClass="Entry" />
                                                    </td>
                                                    <td class="txtAlignLeft">
                                                        <asp:DropDownList CssClass="btn-xs" Width="100%" ID="ddlPendingStatus" runat="server" 
                                                            DataTextField="Name" DataValueField="ID" DataSourceID="dsPendingStatus" />
                                                    </td>
                                                </tr>
                                            </table>
                                            
                                            <div class="spacer10px"></div>
                                            <asp:Button ID="cmdSettleBatch" runat="server" Text="Settle Batch"
                                                        CssClass="cmdBatch btn btn-sm btn-blue shiny"
                                                        UseSubmitBehavior="false" OnClick="cmdSettleBatch_Click" />
                                        </asp:Panel>
                                        <asp:Panel ID="panelFinalize" runat="server">
                                            <table>
                                                <tr>
                                                    <td class="txtAlignLeft padding-right20px"><asp:Label ID="lblPendingBatchNum" runat="server">Batch #:</asp:Label></td>
                                                    <td class="txtAlignLeft"><asp:Label ID="txtPendingBatchNum" runat="server"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td class="txtAlignLeft padding-right20px"><asp:Label ID="lblPendingInvoiceNum" runat="server">External Invoice #:</asp:Label></td>
                                                    <td class="txtAlignLeft"><telerik:RadTextBox ID="txtPendingInvoiceNum" runat="server" /></td>
                                                </tr>
                                                <tr>
                                                    <td class="txtAlignLeft padding-right20px"><asp:Label ID="lblPendingNotes" runat="server">Notes:</asp:Label></td>
                                                    <td class="txtAlignLeft"><telerik:RadTextBox ID="txtPendingNotes" runat="server" /></td>
                                                </tr>
                                            </table>
                                            <div class="spacer10px"></div>
                                            <asp:Button ID="cmdDeletePendingBatch" runat="server" Text="Delete Batch"
                                                        CssClass="cmdDeletePendingBatch btn btn-danger shiny"
                                                        UseSubmitBehavior="false" OnClick="cmdDeletePendingBatch_Click" />

                                            <asp:Button ID="cmdFinalizeBatch" runat="server" Text="Finalize Batch"
                                                        CssClass="cmdFinalize btn btn-blue shiny"
                                                        UseSubmitBehavior="false" OnClick="cmdFinalizeBatch_Click" />
                                        </asp:Panel>
                                    </div>
                                    <div class="clear"></div>
                                </asp:Panel>
                            </div>
                            <div id="Export" class="tab-pane">
                                <asp:Panel ID="panelExportActions" runat="server">
                                    <div class="Entry">
                                        <div class="center"><b>SETTLEMENT PREVIEW ONLY</b></div>
                                        <div class="spacer10px"></div>
                                        <asp:Label ID="lblReportCenterSel" runat="server" Text="Report Center Selection" AssociatedControlID="rcbUserReport" CssClass="Entry" />
                                        <telerik:RadComboBox ID="rcbUserReport" runat="server" AllowCustomText="false" ShowDropDownOnTextboxClick="true"
                                                             DataSourceID="dsUserReport" DataTextField="Name" DataValueField="ID" MaxLength="30"
                                                             Width="100%"
                                                             OnItemDataBound="rcbUserReport_ItemDataBound" />
                                        <blac:DBDataSource ID="dsUserReport" runat="server"
                                                           SelectCommand="SELECT * FROM viewUserReportDefinition WHERE (UserNames IS NULL OR @UserName IN (SELECT Value FROM dbo.fnSplitCSV(UserNames))) ORDER BY Name">
                                            <SelectParameters>
                                                <asp:Parameter Name="UserName" DefaultValue="" DbType="String" />
                                            </SelectParameters>
                                        </blac:DBDataSource>
                                    </div>
                                    <div class="spacer5px"></div>
                                    <div class="center">
                                        <asp:Button ID="cmdExport" runat="server" Text="Export" CssClass="cmdExport NOAJAX btn btn-blue shiny"
                                                    CausesValidation="true" ValidationGroup="vgFileName"
                                                    UseSubmitBehavior="false" OnClick="cmdExport_Click" />
                                    </div>
                                </asp:Panel>
                            </div>
                            <div id="Errors" class="tab-pane">
                                <asp:Panel ID="panelErrors" runat="server">
                                    <asp:TextBox ID="txtErrors" runat="server" CssClass="NullValidator ErrorTextBox"
                                                 TextMode="MultiLine" Enabled="false" />
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div id="gridArea" style="height:100%">
                        <telerik:RadWindowManager ID="radWindowManager" runat="server" EnableShadow="true" />
                        <telerik:RadGrid ID="rgMain" runat="server" EnableHeaderContextMenu="False" Height="800" CssClass="GridRepaint" CellSpacing="0" GridLines="None"
                                         AllowSorting="True" ShowFooter="true" ShowStatusBar="false"
                                         AllowPaging="true" AllowCustomPaging="false" PageSize='<%# Settings.DefaultPageSize %>'
                                         EnableViewState="true" EnableEmbeddedSkins="true" Skin="Vista" AlternatingItemStyle-BackColor="#dcf2fc"
                                         OnNeedDataSource="grid_NeedDataSource" OnDataBound="grid_DataBound" OnItemCommand="grid_ItemCommand" OnItemDataBound="grid_ItemDataBound">
                            <ClientSettings>
                                <Scrolling AllowScroll="True" UseStaticHeaders="True" FrozenColumnsCount="0" />
                                <ClientEvents OnCommand="grid_OnCommand" />
                            </ClientSettings>
                            <MasterTableView AutoGenerateColumns="False" DataKeyNames="ID" CommandItemDisplay="None" AllowMultiColumnSorting="true" EditMode="PopUp">
                                <EditFormSettings UserControlName="CtrlShipperSettlement.ascx" EditFormType="WebUserControl"
                                                  CaptionDataField="OrderNum" CaptionFormatString="Order # {0} Shipper Manual $$">
                                    <PopUpSettings Modal="true" />
                                    <EditColumn UniqueName="EditColumn" />
                                </EditFormSettings>
                                <ColumnGroups>
                                    <telerik:GridColumnGroup HeaderText="R" Name="RateApplySelection" HeaderStyle-CssClass="RAS" HeaderStyle-HorizontalAlign="Center" />
                                    <telerik:GridColumnGroup HeaderText="B" Name="BatchSelection" HeaderStyle-CssClass="BS" HeaderStyle-HorizontalAlign="Center" />
                                    <telerik:GridColumnGroup HeaderText="Processing" Name="ProcessingDetails" HeaderStyle-HorizontalAlign="Center" />
                                    <telerik:GridColumnGroup HeaderText="Order Details" Name="OrderDetails" HeaderStyle-HorizontalAlign="Center" />
                                    <telerik:GridColumnGroup HeaderText="Pickup Details" Name="PickupDetails" HeaderStyle-HorizontalAlign="Center" />
                                    <telerik:GridColumnGroup HeaderText="Delivery Details" Name="DeliveryDetails" HeaderStyle-HorizontalAlign="Center" />
                                    <telerik:GridColumnGroup HeaderText="Ticket Details" Name="TicketDetails" HeaderStyle-HorizontalAlign="Center" />
                                    <telerik:GridColumnGroup HeaderText="Route Details" Name="RouteDetails" HeaderStyle-HorizontalAlign="Center" />
                                    <telerik:GridColumnGroup HeaderText="Settlement Details" Name="InvoiceDetails" HeaderStyle-HorizontalAlign="Center" />
                                </ColumnGroups>
                                <SortExpressions>
                                    <telerik:GridSortExpression FieldName="HasError" SortOrder="Descending" />
                                    <telerik:GridSortExpression FieldName="OrderDate" SortOrder="Ascending" />
                                </SortExpressions>
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="false" HeaderStyle-Width="20px" />
                                <ExpandCollapseColumn Visible="false" HeaderStyle-Width="20px" />
                                <Columns>
                                    <telerik:GridTemplateColumn UniqueName="RateApplySelColumn" DataField="RateApplySel" HeaderText="&nbsp;" AllowFiltering="false" Groupable="false" ReadOnly="true" ColumnGroupName="RateApplySelection">
                                        <HeaderTemplate>
                                            <div style="text-align: center; margin-left: -5px;">
                                                <telerik:RadButton ID="chkRateApplyAll" runat="server" ToggleType="CustomToggle" ButtonType="ToggleButton" ClientIDMode="Static"
                                                                   OnClientToggleStateChanged="chkAll_ToggleStateChanged" CssClass="chkRateApplyAll"
                                                                   AutoPostBack="false" ToolTip="Rate Apply - All|None">
                                                    <ToggleStates>
                                                        <telerik:RadButtonToggleState Text="" PrimaryIconCssClass="rbToggleCheckbox" Value="false" Selected="true"></telerik:RadButtonToggleState>
                                                        <telerik:RadButtonToggleState Text="" PrimaryIconCssClass="rbToggleCheckboxFilled" Value="neither"></telerik:RadButtonToggleState>
                                                        <telerik:RadButtonToggleState Text="" PrimaryIconCssClass="rbToggleCheckboxChecked" Value="true"></telerik:RadButtonToggleState>
                                                    </ToggleStates>
                                                </telerik:RadButton>
                                            </div>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkRateApply" runat="server" Checked='<%# Eval("RateApplySel") %>' CssClass="chkSel chkRateApply" onclick="chk_CheckedChanged(this, 'RateApply')" />
                                        </ItemTemplate>
                                        <ItemStyle BackColor="LightBlue" />
                                        <HeaderStyle Width="30px" />
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn UniqueName="BatchSelColumn" DataField="BatchSel" HeaderText="&nbsp;" AllowFiltering="false" Groupable="false" ReadOnly="true" ColumnGroupName="BatchSelection">
                                        <HeaderTemplate>
                                            <div style="text-align: center; margin-left: -5px;">
                                                <telerik:RadButton ID="chkBatchAll" runat="server" ToggleType="CustomToggle" ButtonType="ToggleButton" ClientIDMode="Static"
                                                                   OnClientToggleStateChanged="chkAll_ToggleStateChanged" CssClass="chkBatchAll"
                                                                   AutoPostBack="false" ToolTip="Batch - All|None">
                                                    <ToggleStates>
                                                        <telerik:RadButtonToggleState Text="" PrimaryIconCssClass="rbToggleCheckbox" Value="false" Selected="true"></telerik:RadButtonToggleState>
                                                        <telerik:RadButtonToggleState Text="" PrimaryIconCssClass="rbToggleCheckboxFilled" Value="neither"></telerik:RadButtonToggleState>
                                                        <telerik:RadButtonToggleState Text="" PrimaryIconCssClass="rbToggleCheckboxChecked" Value="true"></telerik:RadButtonToggleState>
                                                    </ToggleStates>
                                                </telerik:RadButton>
                                            </div>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkBatch" runat="server" Checked='<%# Eval("BatchSel") %>' CssClass="chkSel chkBatch" onclick="chk_CheckedChanged(this, 'Batch')" />
                                        </ItemTemplate>
                                        <ItemStyle BackColor="LightBlue" />
                                        <HeaderStyle Width="30px" />
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Edit" Text="Edit" UniqueName="EditColumn" ImageUrl="~/images/edit.png" ColumnGroupName="ProcessingDetails">
                                        <HeaderStyle Width="40px" />
                                    </telerik:GridButtonColumn>
                                    <telerik:GridDateTimeColumn DataField="InvoiceRatesAppliedDate" UniqueName="InvoiceRatesAppliedDate" HeaderText="Rates Applied" SortExpression="InvoiceRatesAppliedDate" ColumnGroupName="ProcessingDetails"
                                                                DataFormatString="{0:M/d/yy HH:mm}" ReadOnly="true" ForceExtractValue="Always">
                                        <HeaderStyle Width="100px" />
                                    </telerik:GridDateTimeColumn>
                                    <telerik:GridCheckBoxColumn DataField="Approved" UniqueName="Approved" HeaderText="A?" SortExpression="Approved" ColumnGroupName="ProcessingDetails"
                                                                ReadOnly="true" ForceExtractValue="Always">
                                        <HeaderStyle Width="40px" CssClass="Header-Approved" />
                                    </telerik:GridCheckBoxColumn>
                                    <telerik:GridCheckBoxColumn DataField="HasError" UniqueName="HasError" HeaderText="E?" SortExpression="HasError" AllowSorting="true" ColumnGroupName="ProcessingDetails"
                                                                ReadOnly="true" ForceExtractValue="Always">
                                        <HeaderStyle Width="40px" CssClass="Header-HasError" />
                                    </telerik:GridCheckBoxColumn>
                                    <telerik:GridBoundColumn DataField="OrderNum" ReadOnly="true" SortExpression="OrderNum"
                                                             HeaderText="Order #" ColumnGroupName="OrderDetails" ForceExtractValue="Always">
                                        <HeaderStyle Width="65px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="JobNumber" ReadOnly="true" SortExpression="JobNumber"
                                                             HeaderText="Job #" ColumnGroupName="OrderDetails" ForceExtractValue="Always">
                                        <HeaderStyle Width="65px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ContractNumber" ReadOnly="true" SortExpression="ContractNumber"
                                                             HeaderText="Contract #" ColumnGroupName="OrderDetails" ForceExtractValue="Always">
                                        <HeaderStyle Width="65px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridDateTimeColumn DataField="OrderDate" UniqueName="OrderDate" HeaderText="Date" SortExpression="OrderDate"
                                                                DataFormatString="{0:M/d/yyyy}" ReadOnly="true" ForceExtractValue="Always" ColumnGroupName="OrderDetails">
                                        <HeaderStyle Width="70px" />
                                    </telerik:GridDateTimeColumn>
                                    <telerik:GridBoundColumn DataField="Shipper" UniqueName="Shipper" HeaderText="Shipper" SortExpression="Shipper"
                                                             ReadOnly="true" ForceExtractValue="Always" ColumnGroupName="OrderDetails"
                                                             Aggregate="Count" FooterStyle-HorizontalAlign="Right">
                                        <HeaderStyle Width="200px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="DriverFirst" UniqueName="DriverFirst" HeaderText="Driver First" SortExpression="DriverFirst"
                                                             ReadOnly="true" ForceExtractValue="Always" ColumnGroupName="OrderDetails" Visible="false" Display="true">
                                        <HeaderStyle Width="80px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="DriverLast" UniqueName="DriverLast" HeaderText="Driver Last" SortExpression="DriverLast"
                                                             ReadOnly="true" ForceExtractValue="Always" ColumnGroupName="OrderDetails" Visible="false" Display="true">
                                        <HeaderStyle Width="80px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Product" UniqueName="Product" HeaderText="Product" SortExpression="Product"
                                                             ReadOnly="true" FilterControlWidth="70%" ForceExtractValue="Always" ColumnGroupName="OrderDetails"
                                                             Visible="false" Display="true">
                                        <HeaderStyle Width="175px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Truck" UniqueName="Truck" HeaderText="Truck" SortExpression="Truck"
                                                             ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="OrderDetails" Visible="false" Display="true">
                                        <HeaderStyle Width="75px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Trailer" UniqueName="Trailer" HeaderText="Trailer" SortExpression="Trailer"
                                                             ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="OrderDetails" Visible="false" Display="true">
                                        <HeaderStyle Width="75px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Trailer2" UniqueName="Trailer2" HeaderText="Trailer2" SortExpression="Trailer2"
                                                             ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="OrderDetails" Visible="false" Display="true">
                                        <HeaderStyle Width="75px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Origin" UniqueName="Origin" HeaderText="Origin" SortExpression="Origin"
                                                             ReadOnly="true" ForceExtractValue="Always" ColumnGroupName="OrderDetails">
                                        <HeaderStyle Width="300px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="LeaseName" UniqueName="LeaseName" HeaderText="Lease Name" SortExpression="LeaseName"
                                                             ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="OrderDetails"
                                                             Visible="false" Display="true">
                                        <HeaderStyle Width="75px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="LeaseNum" UniqueName="LeaseNum" HeaderText="Lease #" SortExpression="LeaseNum"
                                                             ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="OrderDetails"
                                                             Visible="false" Display="true">
                                        <HeaderStyle Width="65px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Operator" UniqueName="Operator" HeaderText="Operator" SortExpression="Operator"
                                                             ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="OrderDetails"
                                                             Visible="false" Display="true">
                                        <HeaderStyle Width="100px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Producer" UniqueName="Producer" HeaderText="Producer" SortExpression="Producer"
                                                             ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="OrderDetails"
                                                             Visible="false" Display="true">
                                        <HeaderStyle Width="100px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="TruckType" UniqueName="TruckType" HeaderText="Truck Type" SortExpression="TruckType"
                                                             ReadOnly="true" ForceExtractValue="Always" DataType="System.String" ConvertEmptyStringToNull="false" ColumnGroupName="OrderDetails">
                                        <HeaderStyle Width="100px" />
                                        <ItemStyle Wrap="true" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="AuditNotes" UniqueName="AuditNotes" HeaderText="Audit Notes" AllowSorting="false"
                                                             ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="OrderDetails"
                                                             HeaderStyle-Width="100px" Visible="false" Display="true"
                                                             ItemStyle-Wrap="true" />

                                    <telerik:GridBoundColumn DataField="OriginStateAbbrev" UniqueName="OriginStateAbbrev" HeaderText="State" SortExpression="OriginStateAbbrev"
                                                             ReadOnly="true" ForceExtractValue="Always" ColumnGroupName="PickupDetails"
                                                             Visible="false" Display="true">
                                        <HeaderStyle Width="60px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="OriginStation" HeaderText="Station" SortExpression="OriginStation"
                                                             FilterControlWidth="70%" UniqueName="OriginStation" ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="PickupDetails"
                                                             Visible="false" Display="true">
                                        <HeaderStyle Width="90px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="OriginTimeZone" UniqueName="OriginTimeZone" HeaderText="TZ" SortExpression="OriginTimeZone"
                                                             ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="PickupDetails"
                                                             Visible="false" Display="true">
                                        <HeaderStyle Width="35px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridDateTimeColumn DataField="OriginArriveTime" UniqueName="OriginArriveTime" HeaderText="Load Start" SortExpression="OriginArriveTime"
                                                                DataType="System.String" DataFormatString="{0:hh:mm tt}" ReadOnly="true" ForceExtractValue="Always" ColumnGroupName="PickupDetails"
                                                                Visible="false" Display="true">
                                        <HeaderStyle Width="70px" />
                                    </telerik:GridDateTimeColumn>
                                    <telerik:GridDateTimeColumn DataField="OriginDepartTime" UniqueName="OriginDepartTime" HeaderText="Load Stop" SortExpression="OriginDepartTime"
                                                                DataType="System.String" DataFormatString="{0:hh:mm tt}" ReadOnly="true" ForceExtractValue="Always" ColumnGroupName="PickupDetails"
                                                                Visible="false" Display="true">
                                        <HeaderStyle Width="70px" />
                                    </telerik:GridDateTimeColumn>
                                    <telerik:GridBoundColumn DataField="FinalOriginMinutes" UniqueName="OriginMinutes" HeaderText="Load Time" SortExpression="OriginMinutes"
                                                             ReadOnly="true" ForceExtractValue="InBrowseMode" DataType="System.Int16" ColumnGroupName="PickupDetails"
                                                             Visible="true" Display="true">
                                        <HeaderStyle Width="75px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="OriginWaitNumDesc" UniqueName="OriginWaitNumDesc" HeaderText="Wait Reason" SortExpression="OriginWaitNumDesc"
                                                             ReadOnly="true" ForceExtractValue="Always" DataType="System.String" ConvertEmptyStringToNull="false" ColumnGroupName="PickupDetails">
                                        <HeaderStyle Width="200px" />
                                        <ItemStyle Wrap="true" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="OriginWaitNotes" UniqueName="OriginWaitNotes" HeaderText="Wait Notes" SortExpression="OriginWaitNotes"
                                                             ReadOnly="true" ForceExtractValue="Always" DataType="System.String" ConvertEmptyStringToNull="false" ColumnGroupName="PickupDetails"
                                                             Visible="false" Display="true">
                                        <HeaderStyle Width="100px" />
                                        <ItemStyle Wrap="true" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridCheckBoxColumn DataField="FinalH2S" HeaderText="H2S" UniqueName="H2S" FilterControlWidth="60%" ReadOnly="true"
                                                                ForceExtractValue="InBrowseMode" HeaderStyle-Width="40px" ColumnGroupName="PickupDetails"
                                                                visible="false" Display="true" />
                                    <telerik:GridCheckBoxColumn DataField="FinalOriginChainup" UniqueName="OriginChainup" HeaderText="Chain-Up?" SortExpression="OriginChainup"
                                                                ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="PickupDetails"
                                                                Visible="false" Display="true">
                                        <HeaderStyle Width="60px" />
                                    </telerik:GridCheckBoxColumn>
                                    <telerik:GridBoundColumn DataField="TicketCount" UniqueName="TicketCount" HeaderText="# of Tickets" SortExpression="TicketCount"
                                                             ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="PickupDetails"
                                                             Visible="false" Display="true" />
                                    <telerik:GridCheckBoxColumn DataField="Rejected" UniqueName="Rejected" HeaderText="Rejected?" SortExpression="Rejected"
                                                                ReadOnly="true" ForceExtractValue="InBrowseMode" ItemStyle-HorizontalAlign="Center" ColumnGroupName="PickupDetails"
                                                                visible="false" Display="true">
                                        <HeaderStyle Width="95px" />
                                    </telerik:GridCheckBoxColumn>
                                    <telerik:GridBoundColumn DataField="RejectNotes" UniqueName="RejectNotes" HeaderText="Reject Notes" AllowFiltering="false"
                                                             ReadOnly="true" ForceExtractValue="InBrowseMode" ItemStyle-HorizontalAlign="Left" ColumnGroupName="PickupDetails"
                                                             HeaderStyle-Width="100px"
                                                             Visible="false" Display="true"
                                                             ItemStyle-Wrap="true" />
                                    <telerik:GridBoundColumn DataField="OriginBOLNum" UniqueName="OriginBOLNum" HeaderText="BOL" SortExpression="OriginBOLNum"
                                                             ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="PickupDetails"
                                                             Visible="false" Display="true">
                                        <HeaderStyle Width="90px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="OriginGrossUnits" UniqueName="OriginGrossUnits" HeaderText="GOV" SortExpression="OriginGrossUnits"
                                                             ReadOnly="true" ForceExtractValue="Always" DataFormatString="{0:0.000}" ColumnGroupName="PickupDetails"
                                                             Aggregate="Sum" FooterAggregateFormatString="{0:#,##0.00}" FooterStyle-HorizontalAlign="Right" HeaderStyle-Width="90px" />
                                    <telerik:GridBoundColumn DataField="OriginGrossStdUnits" UniqueName="OriginStdUnits" HeaderText="GSV" SortExpression="OriginGrossStdUnits"
                                                             ReadOnly="true" ForceExtractValue="Always" DataFormatString="{0:0.000}" ColumnGroupName="PickupDetails"
                                                             Aggregate="Sum" FooterAggregateFormatString="{0:#,##0.00}" FooterStyle-HorizontalAlign="Right" HeaderStyle-Width="90px" />
                                    <telerik:GridBoundColumn DataField="OriginNetUnits" UniqueName="OriginNetUnits" HeaderText="NSV" SortExpression="OriginNetUnits"
                                                             ReadOnly="true" ForceExtractValue="Always" DataFormatString="{0:0.000}" ColumnGroupName="PickupDetails"
                                                             Aggregate="Sum" FooterAggregateFormatString="{0:#,##0.00}" FooterStyle-HorizontalAlign="Right" HeaderStyle-Width="90px" />
                                    <telerik:GridBoundColumn DataField="OriginWeightNetUnits" UniqueName="OriginWeightNetUnits" HeaderText="Net Weight" SortExpression="OriginWeightNetUnits"
                                                             ReadOnly="true" ForceExtractValue="Always" DataFormatString="{0:0.000}" ColumnGroupName="PickupDetails"
                                                             Aggregate="Sum" FooterAggregateFormatString="{0:#,##0.00}" FooterStyle-HorizontalAlign="Right" HeaderStyle-Width="90px" />
                                    <telerik:GridBoundColumn DataField="ProductGroup" UniqueName="ProductGroup" HeaderText="Product Group" SortExpression="ProductGroup"
                                                             ReadOnly="true" ForceExtractValue="Always" DataType="System.String" ConvertEmptyStringToNull="false" ColumnGroupName="PickupDetails">
                                        <HeaderStyle Width="100px" />
                                        <ItemStyle Wrap="true" />
                                    </telerik:GridBoundColumn>


                                    <telerik:GridBoundColumn DataField="Destination" UniqueName="Destination" HeaderText="Destination" SortExpression="Destination"
                                                             ReadOnly="true" ForceExtractValue="Always" ColumnGroupName="DeliveryDetails">
                                        <HeaderStyle Width="300px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="DestinationStateAbbrev" UniqueName="DestinationStateAbbrev" HeaderText="State" SortExpression="DestinationStateAbbrev"
                                                             ReadOnly="true" ForceExtractValue="Always" ColumnGroupName="DeliveryDetails"
                                                             Visible="false" Display="true">
                                        <HeaderStyle Width="60px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="DestStation" HeaderText="Station" SortExpression="DestStation"
                                                             FilterControlWidth="70%" UniqueName="DestStation" ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="DeliveryDetails"
                                                             Visible="false" Display="true">
                                        <HeaderStyle Width="90px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="DestTimeZone" UniqueName="DestTimeZone" HeaderText="TZ" SortExpression="DestTimeZone"
                                                             ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="DeliveryDetails"
                                                             Visible="false" Display="true">
                                        <HeaderStyle Width="35px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridDateTimeColumn DataField="DestArriveTime" UniqueName="DestArriveTime" HeaderText="Unload Start" SortExpression="DestArriveTime"
                                                                DataType="System.String" DataFormatString="{0:hh:mm tt}" ReadOnly="true" ForceExtractValue="Always" ColumnGroupName="DeliveryDetails"
                                                                Visible="false" Display="true">
                                        <HeaderStyle Width="75px" />
                                    </telerik:GridDateTimeColumn>
                                    <telerik:GridDateTimeColumn DataField="DestDepartTime" UniqueName="DestDepartTime" HeaderText="Unload Stop" SortExpression="DestDepartTime"
                                                                DataType="System.String" DataFormatString="{0:hh:mm tt}" ReadOnly="true" ForceExtractValue="Always" ColumnGroupName="DeliveryDetails"
                                                                Visible="false" Display="true">
                                        <HeaderStyle Width="75px" />
                                    </telerik:GridDateTimeColumn>
                                    <telerik:GridBoundColumn DataField="FinalDestMinutes" UniqueName="DestMinutes" HeaderText="Unload Time" SortExpression="DestMinutes"
                                                             ReadOnly="true" ForceExtractValue="InBrowseMode" DataType="System.Int16" ColumnGroupName="DeliveryDetails"
                                                             Visible="true" Display="true">
                                        <HeaderStyle Width="85px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="DestWaitNumDesc" UniqueName="DestWaitNumDesc" HeaderText="Wait Reason" SortExpression="DestWaitNumDesc"
                                                             ReadOnly="true" ForceExtractValue="Always" DataType="System.String" ConvertEmptyStringToNull="false" ColumnGroupName="DeliveryDetails">
                                        <HeaderStyle Width="200px" />
                                        <ItemStyle Wrap="true" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="DestWaitNotes" UniqueName="DestWaitNotes" HeaderText="Wait Notes" SortExpression="DestWaitNotes"
                                                             ReadOnly="true" ForceExtractValue="Always" DataType="System.String" ConvertEmptyStringToNull="false" ColumnGroupName="DeliveryDetails"
                                                             Visible="false" Display="true">
                                        <HeaderStyle Width="100px" />
                                        <ItemStyle Wrap="true" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridCheckBoxColumn DataField="FinalDestChainup" UniqueName="DestChainup" HeaderText="Chain-Up?" SortExpression="DestChainup"
                                                                ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="DeliveryDetails" 
                                                                Visible="false" Display="true" >
                                        <HeaderStyle Width="60px" />
                                    </telerik:GridCheckBoxColumn>
                                    <telerik:GridBoundColumn DataField="DestBOLNum" UniqueName="DestBOLNum" HeaderText="BOL" SortExpression="DestBOLNum"
                                                             ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="DeliveryDetails"
                                                             Visible="false" Display="true">
                                        <HeaderStyle Width="90px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="DestGrossUnits" UniqueName="DestGrossUnits" HeaderText="GOV" SortExpression="DestGrossUnits"
                                                             ReadOnly="true" ForceExtractValue="Always" DataFormatString="{0:0.000}" ColumnGroupName="DeliveryDetails"
                                                             Aggregate="Sum" FooterAggregateFormatString="{0:#,##0.00}" FooterStyle-HorizontalAlign="Right" HeaderStyle-Width="90px" />
                                    <telerik:GridBoundColumn DataField="DestNetUnits" UniqueName="DestNetUnits" HeaderText="NSV" SortExpression="DestNetUnits"
                                                             ReadOnly="true" ForceExtractValue="Always" DataFormatString="{0:0.000}" ColumnGroupName="DeliveryDetails"
                                                             Aggregate="Sum" FooterAggregateFormatString="{0:#,##0.00}" FooterStyle-HorizontalAlign="Right" HeaderStyle-Width="90px" />
                                    <telerik:GridBoundColumn DataField="DestWeightNetUnits" UniqueName="DestWeightNetUnits" HeaderText="Net Weight" SortExpression="DestWeightNetUnits"
                                                             ReadOnly="true" ForceExtractValue="Always" DataFormatString="{0:0.000}" ColumnGroupName="DeliveryDetails"
                                                             Aggregate="Sum" FooterAggregateFormatString="{0:#,##0.00}" FooterStyle-HorizontalAlign="Right" HeaderStyle-Width="90px" />
                                    <telerik:GridBoundColumn DataField="RerouteNotes" UniqueName="RerouteNotes" HeaderText="Reroute Notes" SortExpression="RerouteNotes"
                                                             ReadOnly="true" ForceExtractValue="Always" DataType="System.String" ColumnGroupName="RouteDetails"
                                                             Visible="false" Display="true">
                                        <ItemStyle Wrap="true" />
                                        <HeaderStyle Width="100px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="PreviousDestinations" UniqueName="PreviousDestinations" HeaderText="Previous Destinations" SortExpression="PreviousDestinations"
                                                             ReadOnly="true" ForceExtractValue="Always" ColumnGroupName="RouteDetails">
                                        <HeaderStyle Width="300px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="FinalActualMiles" UniqueName="ActualMiles" HeaderText="Rate Miles" SortExpression="ActualMiles"
                                                             ReadOnly="true" ForceExtractValue="Always" DataType="System.Int16" ItemStyle-HorizontalAlign="Right" ColumnGroupName="RouteDetails">
                                        <HeaderStyle Width="80px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="TicketType" UniqueName="TicketType" HeaderText="Ticket Type" SortExpression="TicketType"
                                                             ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="TicketDetails"
                                                             Visible="false" Display="true">
                                        <HeaderStyle Width="85px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="TicketNums" UniqueName="TicketNums" HeaderText="Tickets" SortExpression="TicketNum"
                                                             ReadOnly="true" ForceExtractValue="Always" ColumnGroupName="TicketDetails">
                                        <HeaderStyle Width="85px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="TankNums" UniqueName="TankNums" HeaderText="Tank #" SortExpression="TankNums"
                                                             ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="TicketDetails"
                                                             Visible="false" Display="true">
                                        <HeaderStyle Width="85px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="InvoiceH2SRateType" HeaderText="H2S Rate Type" SortExpression="InvoiceH2SRateType"
                                                             FilterControlWidth="70%" UniqueName="InvoiceH2SRateType" ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="InvoiceDetails"
                                                             Visible="false" Display="true">
                                        <HeaderStyle Width="90px" />
                                    </telerik:GridBoundColumn>

                                    <telerik:GridHyperLinkColumn DataTextField="InvoiceH2SRate" DataTextFormatString="{0:$#,##0.0000}"
                                                                 DataNavigateUrlFields="ID" DataNavigateUrlFormatString="~/Site/Financials/ShipperAccessorialRates.aspx?orderid={0}&typeid=4"
                                                                 HeaderText="H2S Rate" SortExpression="InvoiceH2SRate" ItemStyle-ForeColor="Blue" Target="_blank"
                                                                 FilterControlWidth="70%" UniqueName="InvoiceH2SRate" ColumnGroupName="InvoiceDetails"
                                                                 Visible="true" Display="true">
                                        <HeaderStyle Width="90px" HorizontalAlign="Right" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </telerik:GridHyperLinkColumn>
                                    <telerik:GridNumericColumn DataField="InvoiceH2SAmount" HeaderText="H2S Fee $$" SortExpression="InvoiceH2SAmount" DecimalDigits="4" DataFormatString="{0:$#,##0.0000}" NumericType="Currency"
                                                               FilterControlWidth="70%" UniqueName="InvoiceH2SAmount" ForceExtractValue="InBrowseMode" ColumnGroupName="InvoiceDetails"
                                                               Aggregate="Sum" FooterAggregateFormatString="{0:$#,##0.00}" FooterStyle-HorizontalAlign="Right">
                                        <HeaderStyle Width="90px" HorizontalAlign="Right" />
                                        <ItemStyle HorizontalAlign="Right" Font-Bold="true" />
                                    </telerik:GridNumericColumn>
                                    <telerik:GridBoundColumn DataField="InvoiceOriginChainupRateType" HeaderText="Origin Chain-Up Rate Type" SortExpression="InvoiceOriginChainupRateType" 
                                                             FilterControlWidth="70%" UniqueName="InvoiceOriginChainupRateType" ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="InvoiceDetails" 
                                                             Visible="false" Display="true">
                                                <HeaderStyle Width="90px" />
                                            </telerik:GridBoundColumn>
                                    <telerik:GridHyperLinkColumn DataTextField="InvoiceOriginChainupRate" DataTextFormatString="{0:$#,##0.0000}" 
                                                                 DataNavigateUrlFields="ID" DataNavigateUrlFormatString="~/Site/Financials/ShipperAccessorialRates.aspx?orderid={0}&typeid=1"
                                                                 HeaderText="Origin Chain-Up Rate" SortExpression="InvoiceOriginChainupRate" ItemStyle-ForeColor="Blue" Target="_blank"
                                                                 FilterControlWidth="70%" UniqueName="InvoiceOriginChainupRate" ColumnGroupName="InvoiceDetails" 
                                                                 Visible="true" Display="true" >
                                        <HeaderStyle Width="90px" HorizontalAlign="Right" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </telerik:GridHyperLinkColumn>
                                    <telerik:GridNumericColumn DataField="InvoiceOriginChainupAmount" UniqueName="InvoiceOriginChainupAmount" HeaderText="Origin Chain-Up $$" SortExpression="InvoiceOriginChainupAmount"
                                                               DataType="System.Decimal" DecimalDigits="4" DataFormatString="{0:$#,##0.0000}" NumericType="Currency" HeaderStyle-Width="80px" 
                                                               ItemStyle-HorizontalAlign="Right" ItemStyle-Font-Bold="true" ColumnGroupName="InvoiceDetails" ForceExtractValue="Always"
                                                               Visible="true" Display="true" 
                                                               Aggregate="Sum" FooterAggregateFormatString="{0:$#,##0.00}" FooterStyle-HorizontalAlign="Right" />


                                    <telerik:GridBoundColumn DataField="InvoiceDestChainupRateType" HeaderText="Dest Chain-Up Rate Type" SortExpression="InvoiceDestChainupRateType" 
                                                             FilterControlWidth="70%" UniqueName="InvoiceDestChainupRateType" ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="InvoiceDetails" 
                                                             Visible="false" Display="true" >
                                        <HeaderStyle Width="90px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridHyperLinkColumn DataTextField="InvoiceDestChainupRate" DataTextFormatString="{0:$#,##0.0000}" 
                                                                 DataNavigateUrlFields="ID" DataNavigateUrlFormatString="~/Site/Financials/ShipperAccessorialRates.aspx?orderid={0}&typeid=5"
                                                                 HeaderText="Dest Chain-Up Rate" SortExpression="InvoiceDestChainupRate" ItemStyle-ForeColor="Blue" Target="_blank"
                                                                 FilterControlWidth="70%" UniqueName="InvoiceDestChainupRate" ColumnGroupName="InvoiceDetails" 
                                                                 Visible="true" Display="true">
                                                <HeaderStyle Width="90px" HorizontalAlign="Right" />
                                                <ItemStyle HorizontalAlign="Right" />
                                            </telerik:GridHyperLinkColumn>
                                    <telerik:GridNumericColumn DataField="InvoiceDestChainupAmount" UniqueName="InvoiceDestChainupAmount" HeaderText="Dest Chain-Up $$" SortExpression="InvoiceDestChainupAmount"
                                                               DataType="System.Decimal" DecimalDigits="4" DataFormatString="{0:$#,##0.0000}" NumericType="Currency" HeaderStyle-Width="80px"
                                                               ItemStyle-HorizontalAlign="Right" ItemStyle-Font-Bold="true" ColumnGroupName="InvoiceDetails" ForceExtractValue="Always"
                                                               Visible="true" Display="true"
                                                               Aggregate="Sum" FooterAggregateFormatString="{0:$#,##0.00}" FooterStyle-HorizontalAlign="Right" />
                                    <telerik:GridBoundColumn DataField="InvoiceRerouteRateType" HeaderText="Reroute Rate Type" SortExpression="InvoiceRerouteRateType"
                                                             FilterControlWidth="70%" UniqueName="InvoiceRerouteRateType" ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="InvoiceDetails"
                                                             Visible="false" Display="true">
                                        <HeaderStyle Width="90px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridHyperLinkColumn DataTextField="InvoiceRerouteRate" DataTextFormatString="{0:$#,##0.0000}"
                                                                 DataNavigateUrlFields="ID" DataNavigateUrlFormatString="~/Site/Financials/ShipperAccessorialRates.aspx?orderid={0}&typeid=2"
                                                                 HeaderText="Reroute Rate" SortExpression="InvoiceRerouteRate" ItemStyle-ForeColor="Blue" Target="_blank"
                                                                 FilterControlWidth="70%" UniqueName="InvoiceRerouteRate" ColumnGroupName="InvoiceDetails"
                                                                 Visible="true" Display="true">
                                        <HeaderStyle Width="90px" HorizontalAlign="Right" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </telerik:GridHyperLinkColumn>
                                    <telerik:GridNumericColumn DataField="InvoiceRerouteAmount" UniqueName="InvoiceRerouteAmount" HeaderText="Reroute $$" SortExpression="InvoiceRerouteAmount"
                                                               DataType="System.Decimal" DecimalDigits="4" DataFormatString="{0:$#,##0.0000}" NumericType="Currency" HeaderStyle-Width="80px"
                                                               ItemStyle-HorizontalAlign="Right" ItemStyle-Font-Bold="true" ColumnGroupName="InvoiceDetails" ForceExtractValue="Always"
                                                               Aggregate="Sum" FooterAggregateFormatString="{0:$#,##0.00}" FooterStyle-HorizontalAlign="Right" />
                                    <telerik:GridBoundColumn DataField="InvoiceOrderRejectRateType" HeaderText="Rejection Rate Type" SortExpression="InvoiceOrderRejectRateType"
                                                             FilterControlWidth="70%" UniqueName="InvoiceOrderRejectRateType" ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="InvoiceDetails"
                                                             Visible="false" Display="true">
                                        <HeaderStyle Width="90px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridHyperLinkColumn DataTextField="InvoiceOrderRejectRate" DataTextFormatString="{0:$#,##0.0000}"
                                                                 DataNavigateUrlFields="ID" DataNavigateUrlFormatString="~/Site/Financials/ShipperOrderRejectRates.aspx?orderid={0}"
                                                                 HeaderText="Reject Rate" SortExpression="InvoiceOrderRejectRate" ItemStyle-ForeColor="Blue" Target="_blank"
                                                                 FilterControlWidth="70%" UniqueName="InvoiceOrderRejectRate" ColumnGroupName="InvoiceDetails"
                                                                 Visible="true" Display="true">
                                        <HeaderStyle Width="90px" HorizontalAlign="Right" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </telerik:GridHyperLinkColumn>
                                    <telerik:GridNumericColumn DataField="InvoiceOrderRejectAmount" UniqueName="InvoiceOrderRejectAmount" HeaderText="Reject $$" SortExpression="InvoiceOrderRejectAmount"
                                                               DataType="System.Decimal" DecimalDigits="4" DataFormatString="{0:$#,##0.0000}" NumericType="Currency" HeaderStyle-Width="80px"
                                                               ItemStyle-HorizontalAlign="Right" ItemStyle-Font-Bold="true" ColumnGroupName="InvoiceDetails" ForceExtractValue="Always"
                                                               Aggregate="Sum" FooterAggregateFormatString="{0:$#,##0.00}" FooterStyle-HorizontalAlign="Right" />
                                    <telerik:GridNumericColumn DataField="InvoiceTaxRate" HeaderText="Tax Rate (%)" SortExpression="InvoiceTaxRate" DecimalDigits="4" DataFormatString="{0:0.0000}"
                                                               FilterControlWidth="70%" UniqueName="InvoiceTaxRate" NumericType="Percent" ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="InvoiceDetails">
                                        <HeaderStyle Width="90px" HorizontalAlign="Right" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </telerik:GridNumericColumn>
                                    <telerik:GridBoundColumn DataField="InvoiceSplitLoadRateType" HeaderText="SplitLoad Rate Type" SortExpression="InvoiceSplitLoadRateType"
                                                             FilterControlWidth="70%" UniqueName="InvoiceSplitLoadRateType" ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="InvoiceDetails"
                                                             Visible="false" Display="true">
                                        <HeaderStyle Width="90px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridHyperLinkColumn DataTextField="InvoiceSplitLoadRate" DataTextFormatString="{0:$#,##0.0000}"
                                                                 DataNavigateUrlFields="ID" DataNavigateUrlFormatString="~/Site/Financials/ShipperAccessorialRates.aspx?orderid={0}&typeid=3"
                                                                 HeaderText="SplitLoad Rate" SortExpression="InvoiceSplitLoadRate" ItemStyle-ForeColor="Blue" Target="_blank"
                                                                 FilterControlWidth="70%" UniqueName="InvoiceSplitLoadRate" ColumnGroupName="InvoiceDetails"
                                                                 Visible="true" Display="true">
                                        <HeaderStyle Width="90px" HorizontalAlign="Right" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </telerik:GridHyperLinkColumn>
                                    <telerik:GridNumericColumn DataField="InvoiceSplitLoadAmount" UniqueName="InvoiceSplitLoadAmount" HeaderText="Split Load $$" SortExpression="InvoiceSplitLoadAmount"
                                                               DataType="System.Decimal" DecimalDigits="2" DataFormatString="{0:$#,##0.00}" NumericType="Currency" HeaderStyle-Width="110px"
                                                               ItemStyle-HorizontalAlign="Right" ReadOnly="true" ColumnGroupName="InvoiceDetails" ItemStyle-Font-Bold="true" />
                                    <telerik:GridTemplateColumn DataField="InvoiceOtherDetailsTSV" UniqueName="InvoiceOtherDetailsTSV" HeaderText="Misc Details" AllowSorting="false"
                                                                HeaderStyle-Width="220px" ColumnGroupName="InvoiceDetails" />
                                    <telerik:GridTemplateColumn DataField="InvoiceOtherAmountsTSV" UniqueName="InvoiceOtherAmountsTSV" HeaderText="Misc $$" AllowSorting="false"
                                                                HeaderStyle-Width="80px" ColumnGroupName="InvoiceDetails" ItemStyle-Font-Bold="true" />
                                    <telerik:GridBoundColumn DataField="TotalMinutes" UniqueName="TotalMinutes" HeaderText="Total Load/Unload Time" SortExpression="TotalMinutes"
                                                             ReadOnly="true" ForceExtractValue="InBrowseMode" DataType="System.Int16" ItemStyle-HorizontalAlign="Right" ColumnGroupName="InvoiceDetails"
                                                             Visible="false" Display="true">
                                        <HeaderStyle Width="145px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="InvoiceWaitFeeSubUnit" UniqueName="InvoiceWaitFeeSubUnit" HeaderText="Wait Fee Sub Unit" SortExpression="InvoiceWaitFeeSubUnit"
                                                             ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="InvoiceDetails"
                                                             Visible="false" Display="true">
                                        <HeaderStyle Width="105px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="InvoiceWaitFeeRoundingType" UniqueName="InvoiceWaitFeeRoundingType" HeaderText="Wait Fee Rounding" SortExpression="InvoiceWaitFeeRoundingType"
                                                             ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="InvoiceDetails"
                                                             Visible="false" Display="true">
                                        <HeaderStyle Width="115px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridHyperLinkColumn DataTextField="InvoiceMinSettlementUnits"
                                                                 UniqueName="InvoiceMinSettlementUnits" HeaderText="Min Settle Units" SortExpression="InvoiceMinSettlementUnits"
                                                                 DataNavigateUrlFields="ID" DataNavigateUrlFormatString="~/Site/Financials/ShipperMinSettlementUnits.aspx?orderid={0}" Target="_blank"
                                                                 DataType="System.Int16" ItemStyle-HorizontalAlign="Right" ColumnGroupName="InvoiceDetails"
                                                                 Visible="true" Display="true">
                                        <HeaderStyle Width="125px" />
                                    </telerik:GridHyperLinkColumn>
                                    <telerik:GridHyperLinkColumn DataTextField="InvoiceSettlementFactor"
                                                                 UniqueName="InvoiceSettlementFactor" HeaderText="Settle Unit Type" SortExpression="InvoiceSettlementFactor"
                                                                 DataNavigateUrlFields="ID" DataNavigateUrlFormatString="~/Site/Financials/ShipperSettlementFactor.aspx?orderid={0}" Target="_blank"
                                                                 ColumnGroupName="InvoiceDetails"
                                                                 Visible="true" Display="true">
                                        <HeaderStyle Width="100px" />
                                    </telerik:GridHyperLinkColumn>
                                    <telerik:GridHyperLinkColumn DataTextField="InvoiceOriginWaitBillableMinutes" DataTextFormatString="{0}"
                                                                 UniqueName="InvoiceOriginWaitBillableMinutes" HeaderText="Origin Billable Wait" SortExpression="InvoiceOriginWaitBillableMinutes"
                                                                 DataNavigateUrlFields="ID" DataNavigateUrlFormatString="~/Site/Financials/ShipperWaitFeeParameters.aspx?orderid={0}" Target="_blank"
                                                                 DataType="System.Int16" ItemStyle-HorizontalAlign="Right" ColumnGroupName="InvoiceDetails"
                                                                 Visible="true" Display="true">
                                        <HeaderStyle Width="125px" />
                                    </telerik:GridHyperLinkColumn>
                                    <telerik:GridHyperLinkColumn DataTextField="InvoiceDestinationWaitBillableMinutes" DataTextFormatString="{0}"
                                                                 UniqueName="InvoiceDestinationWaitBillableMinutes" HeaderText="Dest Billable Wait" SortExpression="InvoiceDestinationWaitBillableMinutes"
                                                                 DataNavigateUrlFields="ID" DataNavigateUrlFormatString="~/Site/Financials/ShipperWaitFeeParameters.aspx?orderid={0}" Target="_blank"
                                                                 DataType="System.Int16" ItemStyle-HorizontalAlign="Right" ColumnGroupName="InvoiceDetails"
                                                                 Visible="true" Display="true">
                                        <HeaderStyle Width="120px" />
                                    </telerik:GridHyperLinkColumn>
                                    <telerik:GridBoundColumn DataField="InvoiceTotalWaitBillableMinutes" UniqueName="InvoiceTotalWaitBillableMinutes" HeaderText="Total Billable Wait Time" SortExpression="InvoiceTotalWaitBillableMinutes"
                                                             ReadOnly="true" ForceExtractValue="InBrowseMode" DataType="System.Int16" ItemStyle-HorizontalAlign="Right" ColumnGroupName="InvoiceDetails"
                                                             Visible="false" Display="true">
                                        <HeaderStyle Width="105px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridHyperLinkColumn DataTextField="InvoiceOriginWaitRate" DataTextFormatString="{0:$#,##0.0000}"
                                                                 DataNavigateUrlFields="ID" DataNavigateUrlFormatString="~/Site/Financials/ShipperOriginWaitRates.aspx?orderid={0}"
                                                                 HeaderText="Origin Wait Rate" SortExpression="InvoiceOriginWaitRate" ItemStyle-ForeColor="Blue" Target="_blank"
                                                                 FilterControlWidth="70%" UniqueName="InvoiceOriginWaitRate" ColumnGroupName="InvoiceDetails"
                                                                 Visible="true" Display="true">
                                        <HeaderStyle Width="120px" HorizontalAlign="Right" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </telerik:GridHyperLinkColumn>
                                    <telerik:GridNumericColumn DataField="InvoiceOriginWaitAmount" UniqueName="InvoiceOriginWaitAmount" HeaderText="Origin Wait $$" SortExpression="InvoiceOriginWaitAmount"
                                                               DataType="System.Decimal" DecimalDigits="4" DataFormatString="{0:$#,##0.0000}" NumericType="Currency" HeaderStyle-Width="100px"
                                                               ItemStyle-HorizontalAlign="Right" ItemStyle-Font-Bold="true" ColumnGroupName="InvoiceDetails" ForceExtractValue="Always"
                                                               Aggregate="Sum" FooterAggregateFormatString="{0:$#,##0.00}" FooterStyle-HorizontalAlign="Right" />
                                    <telerik:GridHyperLinkColumn DataTextField="InvoiceDestinationWaitRate" DataTextFormatString="{0:$#,##0.0000}"
                                                                 DataNavigateUrlFields="ID" DataNavigateUrlFormatString="~/Site/Financials/ShipperDestinationWaitRates.aspx?orderid={0}"
                                                                 HeaderText="Dest Wait Rate" SortExpression="InvoiceDestinationWaitRate" ItemStyle-ForeColor="Blue" Target="_blank"
                                                                 FilterControlWidth="70%" UniqueName="InvoiceDestinationWaitRate" ColumnGroupName="InvoiceDetails"
                                                                 Visible="true" Display="true">
                                        <HeaderStyle Width="90px" HorizontalAlign="Right" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </telerik:GridHyperLinkColumn>
                                    <telerik:GridNumericColumn DataField="InvoiceDestinationWaitAmount" UniqueName="InvoiceDestinationWaitAmount" HeaderText="Dest Wait $$" SortExpression="InvoiceDestinationWaitAmount"
                                                               DataType="System.Decimal" DecimalDigits="4" DataFormatString="{0:$#,##0.0000}" NumericType="Currency" HeaderStyle-Width="80px"
                                                               ItemStyle-HorizontalAlign="Right" ItemStyle-Font-Bold="true" ColumnGroupName="InvoiceDetails" ForceExtractValue="Always"
                                                               Aggregate="Sum" FooterAggregateFormatString="{0:$#,##0.00}" FooterStyle-HorizontalAlign="Right" />
                                    <telerik:GridNumericColumn DataField="InvoiceTotalWaitAmount" UniqueName="InvoiceTotalWaitAmount" HeaderText="Total Wait $$" SortExpression="InvoiceTotalWaitAmount"
                                                               DataType="System.Decimal" DecimalDigits="4" DataFormatString="{0:$#,##0.0000}" NumericType="Currency" HeaderStyle-Width="80px"
                                                               ItemStyle-HorizontalAlign="Right" ItemStyle-Font-Bold="true" ColumnGroupName="InvoiceDetails" ForceExtractValue="Always"
                                                               Aggregate="Sum" FooterAggregateFormatString="{0:$#,##0.00}" FooterStyle-HorizontalAlign="Right" />
                                    <telerik:GridHyperLinkColumn DataTextField="InvoiceFuelSurchargeRate" DataTextFormatString="{0:$#,##0.0000}"
                                                                 UniqueName="InvoiceFuelSurchargeRate" HeaderText="Fuel Surcharge Rate" SortExpression="InvoiceFuelSurchargeRate"
                                                                 DataNavigateUrlFields="ID" DataNavigateUrlFormatString="~/Site/Financials/ShipperFuelSurchargeRates.aspx?orderid={0}"
                                                                 DataType="System.Decimal" HeaderStyle-Width="120px"
                                                                 ItemStyle-HorizontalAlign="Right" ColumnGroupName="InvoiceDetails" />
                                    <telerik:GridNumericColumn DataField="InvoiceFuelSurchargeAmount" UniqueName="InvoiceFuelSurchargeAmount" HeaderText="Fuel Surcharge $$" SortExpression="InvoiceFuelSurchargeAmount"
                                                               DataType="System.Decimal" DecimalDigits="2" DataFormatString="{0:$#,##0.00}" NumericType="Currency" HeaderStyle-Width="110px" ItemStyle-Font-Bold="true"
                                                               ItemStyle-HorizontalAlign="Right" ReadOnly="true" ColumnGroupName="InvoiceDetails" />
                                    <telerik:GridBoundColumn DataField="InvoiceSettlementUom" UniqueName="InvoiceSettlementUom" HeaderText="Settlement UOM" SortExpression="InvoiceSettlementUom"
                                                             HeaderStyle-Width="100px" ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="InvoiceDetails"
                                                             Visible="false" Display="true" />
                                    <telerik:GridBoundColumn DataField="InvoiceMinSettlementUnits" UniqueName="InvoiceMinSettlementUnits" HeaderText="Min Settle Vol" SortExpression="InvoiceMinSettlementUnits"
                                                             ReadOnly="true" ForceExtractValue="InBrowseMode" DataType="System.Decimal" ItemStyle-HorizontalAlign="Right" ColumnGroupName="InvoiceDetails"
                                                             Visible="false" Display="true">
                                        <HeaderStyle Width="100px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="InvoiceUnits" UniqueName="InvoiceUnits" HeaderText="Settlement Units" SortExpression="InvoiceUnits"
                                                             ReadOnly="true" ForceExtractValue="InBrowseMode" DataType="System.Decimal" ItemStyle-HorizontalAlign="Right" ColumnGroupName="InvoiceDetails"
                                                             Visible="false" Display="true">
                                        <HeaderStyle Width="100px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="InvoiceRouteRateType" HeaderText="Route Rate Type" SortExpression="InvoiceRouteRateType"
                                                             FilterControlWidth="70%" UniqueName="InvoiceRouteRateType" ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="InvoiceDetails"
                                                             Visible="false" Display="true">
                                        <HeaderStyle Width="90px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn DataField="InvoiceRouteRate" UniqueName="InvoiceRouteRate" HeaderText="Route Rate" SortExpression="InvoiceRouteRate"
                                                                HeaderStyle-Width="90px" DataType="System.Decimal" ItemStyle-HorizontalAlign="Right" ReadOnly="true" ColumnGroupName="InvoiceDetails"
                                                                ForceExtractValue="Always">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlRouteRate" runat="server" ForeColor="Blue"
                                                           Text='<%# String.Format("{0:$#,##0.0000}", Eval("InvoiceRouteRate")) %>'
                                                           NavigateUrl='<%# String.Format("~/Site/Financials/ShipperRouteRates.aspx?orderid={0}", Eval("ID")) %>'
                                                           ColumnGroupName="InvoiceDetails">
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                        <EditItemTemplate>&nbsp;</EditItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="InvoiceRateSheetRateType" HeaderText="RateSheet Rate Type" SortExpression="InvoiceRateSheetRateType"
                                                             FilterControlWidth="70%" UniqueName="InvoiceRateSheetRateType" ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="InvoiceDetails"
                                                             Visible="false" Display="true">
                                        <HeaderStyle Width="90px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn DataField="InvoiceRateSheetRate" UniqueName="InvoiceRateSheetRate" HeaderText="RateSheet Rate" SortExpression="InvoiceRateSheetRate"
                                                                HeaderStyle-Width="90px" DataType="System.Decimal" ItemStyle-HorizontalAlign="Right" ReadOnly="true" ColumnGroupName="InvoiceDetails"
                                                                ForceExtractValue="Always">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlRateSheetRate" runat="server" ForeColor="Blue"
                                                           Text='<%# String.Format("{0:$#,##0.0000}", Eval("InvoiceRateSheetRate")) %>'
                                                           NavigateUrl='<%# String.Format("~/Site/Financials/ShipperRateSheets.aspx?orderid={0}", Eval("ID")) %>'
                                                           ColumnGroupName="InvoiceDetails">
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                        <EditItemTemplate>&nbsp;</EditItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridNumericColumn DataField="InvoiceLoadAmount" UniqueName="InvoiceLoadAmount" HeaderText="Load $$" SortExpression="InvoiceLoadAmount"
                                                               DataType="System.Decimal" DecimalDigits="4" DataFormatString="{0:$#,##0.0000}" NumericType="Currency" HeaderStyle-Width="90px"
                                                               Aggregate="Sum" ItemStyle-HorizontalAlign="Right" FooterAggregateFormatString="{0:$#,##0.00}" FooterStyle-Font-Bold="true"
                                                               ForceExtractValue="Always"
                                                               FooterStyle-HorizontalAlign="Right" ItemStyle-Font-Bold="true" ColumnGroupName="InvoiceDetails" />
                                    <telerik:GridNumericColumn DataField="InvoiceTotalAmount" UniqueName="InvoiceTotalAmount" HeaderText="Total $$" SortExpression="InvoiceTotalAmount"
                                                               Aggregate="Sum" ItemStyle-HorizontalAlign="Right" ReadOnly="true" ForceExtractValue="Always"
                                                               FooterAggregateFormatString="{0:$#,##0.00}" FooterStyle-HorizontalAlign="Right" FooterStyle-Font-Bold="true"
                                                               DataType="System.Decimal" DecimalDigits="4" DataFormatString="{0:$#,##0.0000}" NumericType="Currency" HeaderStyle-Width="90px" ItemStyle-Font-Bold="true"
                                                               ColumnGroupName="InvoiceDetails" />
                                    <telerik:GridBoundColumn DataField="ID" UniqueName="ID" Visible="false" Display="false" DataType="System.Int32" ForceExtractValue="Always" ReadOnly="true" />
                                </Columns>
                                <EditFormSettings ColumnNumber="3">
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column" ButtonType="ImageButton" CancelImageUrl="~/images/cancel.png"
                                                UpdateImageUrl="~/images/apply.png" InsertImageUrl="~/images/apply.png" />
                                </EditFormSettings>
                                <PagerStyle Mode="NextPrevAndNumeric" AlwaysVisible="true" />
                            </MasterTableView>
                            <HeaderStyle Wrap="False" />
                            <PagerStyle AlwaysVisible="True" />
                            <FilterMenu EnableImageSprites="False" />
                        </telerik:RadGrid>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <telerik:RadScriptBlock runat="server">
        <script type="text/javascript">
            var fileDownloadCheckTimer, button, filterButton;

            function DisableSender(sender) {
                sender.set_enabled(false);
            }

            function WaitForExport(sender, args) {
                //disable button
                DisableSender(button = sender);

                fileDownloadCheckTimer = window.setInterval(function () {
                    //get cookie and compare
                    var cookieValue = readCookie("fileDownloadToken");
                    if (cookieValue == "done") {
                        finishDownload(false);
                    }
                }, 1000);
            }

            function finishDownload(refresh) {
                window.clearInterval(fileDownloadCheckTimer);
                //clear cookie
                eraseCookie("fileDownloadToken");
                //enable button
                button.set_enabled(true);
                // rebind the data
                if (refresh) {
                    $("#" + "<%= cmdFilter.ClientID %>").click();
                }
            }

            function createCookie(name, value, days) {
                if (days) {
                    var date = new Date();
                    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                    var expires = "; expires=" + date.toGMTString();
                }
                else var expires = "";
                    document.cookie = name + "=" + value + expires + "; path=/";
            }

            function readCookie(name) {
                var nameEQ = name + "=";
                var ca = document.cookie.split(';');
                for (var i = 0; i < ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
                }
                return null;
            }

            function eraseCookie(name) {
                createCookie(name, "", -1);
            };

            $(document).ready(function () {
                if (<%= string.IsNullOrEmpty(Request.QueryString["BatchID"]) ? 0 : 1 %>)
                {
                    $(".ddlPendingBatches").val("<%= Request.QueryString["BatchID"] %>").prop('selected', true); // auto select the batch id
                    $("#" + "<%= cmdFilter.ClientID %>").click(); // simulate filter click
                }
            })

        </script>
    </telerik:RadScriptBlock>
    <blc:RadGridDBCtrl ID="rgdbMain" runat="server" ControlID="rgMain" />
    <blac:DBDataSource ID="dsShipper" runat="server" SelectCommand="SELECT ID, FullName = Name FROM dbo.tblCustomer UNION SELECT -1, '(All)' ORDER BY Name" />
    <blac:DBDataSource ID="dsProductGroup" runat="server" SelectCommand="SELECT ID, Name FROM tblProductGroup UNION SELECT -1, '(All)' ORDER BY Name" />
    <blac:DBDataSource ID="dsTruckType" runat="server" SelectCommand="SELECT ID, Name FROM tblTruckType UNION SELECT -1, '(All)' ORDER BY Name" />
    <blac:DBDataSource ID="dsState" runat="server" SelectCommand="SELECT ID, Name = FullName FROM dbo.tblState UNION SELECT -1, '(All)' ORDER BY Name" />
    <blac:DBDataSource ID="dsProducer" runat="server" SelectCommand="SELECT ID, Name FROM dbo.tblProducer UNION SELECT -1, '(All)' ORDER BY Name" />

    <blac:DBDataSource ID="dsPendingBatches" runat="server" SelectCommand="SELECT ID, Name = FullName
                            FROM viewShipperSettlementBatch ssb WHERE IsFinal = 0 
                            UNION SELECT -1, ' Select Batch ' ORDER BY NAME" />
    <blac:DBDataSource ID="dsPendingStatus" runat="server" SelectCommand="SELECT ID = 0, Name = 'Pending' UNION SELECT 1, 'Final'" />
</asp:Content>
