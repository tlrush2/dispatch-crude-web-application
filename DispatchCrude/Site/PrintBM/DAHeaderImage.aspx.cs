﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//add for sql stuff
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web.Security;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using Syncfusion.XlsIO;
using Telerik.Web.UI;
using AlonsIT;
using DispatchCrude.Extensions;
using DispatchCrude.Core;

namespace DispatchCrude.Site.PrintBM
{
    public partial class DAHeaderImage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Print tab navigation html to page
            tabmenu.InnerHtml = NavigationHelper.PrintTabArray(NavigationHelper.TabSet_PrintConfiguration, "Tab_DCDriver").ToString();

            //Print button navigation html to page
            speedbuttons.InnerHtml = NavigationHelper.PrintButtonArray(NavigationHelper.ButtonSet_TabDCDriver, "Button_HeaderImage").ToString();
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            rgMain.Rebind();
        }

        protected void grid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.EditCommandName)
            {
                if (e.Item is GridEditableItem)
                {
                    //DropDownList ddl = RadGridHelper.GetColumnDropDown((e.Item as GridEditableItem)["CarrierID"]);
                }
            }
            else if (e.CommandName == RadGrid.InitInsertCommandName)
            {
                // cancel the default operation
                e.Canceled = true;

                e.Item.OwnerTableView.InsertItem(GetNewRowValues(e.Item as GridDataItem));
            }
        }

        private Hashtable GetNewRowValues(GridEditableItem gdi)
        {
            Hashtable ret = new Hashtable();
            // set ID to DBNULL (since we are creating a new record)
            ret["ID"] = DBNull.Value;
            ret["TicketTypeID"] = 0;
            ret["ShipperID"] = 0;
            ret["ProductGroupID"] = 0;
            ret["ProducerID"] = 0;
            ret["ImageLeft"] = 0;
            ret["ImageTop"] = 0;
            ret["ImageWidth"] = 576;
            ret["ImageHeight"] = 400;
            return ret;
        }

        protected void grid_ItemCreated(object sender, GridItemEventArgs e)
        {
        }

        protected void grid_ItemDataBound(object sender, GridItemEventArgs e)
        {
        }

    }
}