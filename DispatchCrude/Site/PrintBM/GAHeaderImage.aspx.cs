﻿using System;
using System.Collections;
using Telerik.Web.UI;
using DispatchCrude.Extensions;

namespace DispatchCrude.Site.PrintBM
{
    public partial class GAHeaderImage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Print tab navigation html to page
            tabmenu.InnerHtml = NavigationHelper.PrintTabArray(NavigationHelper.TabSet_PrintConfiguration, "Tab_DCGauger").ToString();

            //Print button navigation html to page
            speedbuttons.InnerHtml = NavigationHelper.PrintButtonArray(NavigationHelper.ButtonSet_TabDCGauger, "Button_HeaderImage").ToString();
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            rgMain.Rebind();
        }

        protected void grid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.EditCommandName)
            {
                if (e.Item is GridEditableItem)
                {
                    //DropDownList ddl = RadGridHelper.GetColumnDropDown((e.Item as GridEditableItem)["CarrierID"]);
                }
            }
            else if (e.CommandName == RadGrid.InitInsertCommandName)
            {
                // cancel the default operation
                e.Canceled = true;

                e.Item.OwnerTableView.InsertItem(GetNewRowValues(e.Item as GridDataItem));
            }
        }

        private Hashtable GetNewRowValues(GridEditableItem gdi)
        {
            Hashtable ret = new Hashtable();
            // set ID to DBNULL (since we are creating a new record)
            ret["ID"] = DBNull.Value;
            ret["TicketTypeID"] = 0;
            ret["ShipperID"] = 0;
            ret["ProductGroupID"] = 0;
            ret["ProducerID"] = 0;
            ret["ImageLeft"] = 0;
            ret["ImageTop"] = 0;
            ret["ImageWidth"] = 576;
            ret["ImageHeight"] = 400;
            return ret;
        }

        protected void grid_ItemCreated(object sender, GridItemEventArgs e)
        {
        }

        protected void grid_ItemDataBound(object sender, GridItemEventArgs e)
        {
        }

    }
}