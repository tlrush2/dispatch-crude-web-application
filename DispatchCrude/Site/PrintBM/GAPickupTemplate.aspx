﻿<%@  Title="Print Configuration" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="GAPickupTemplate.aspx.cs"
    Inherits="DispatchCrude.Site.PrintBM.GAPickupTemplate" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script>
        $("#ctl00_ctl00_EntityCaption").html("Gauger App Pickup ZPL");
    </script>
    <div class="tabbable TabRepaint">
        <ul class="nav nav-tabs" id="tabmenu" runat="server">
            <!--Tabs will print here (see code behind)-->
        </ul>
        <div class="tab-content">
            <div class="tab-pane in active">

                <div id="speedbuttons" class="well speedButtonContainer" runat="server">
                    <!--Buttons will print here (see code behind)-->
                </div>

                <div class="leftpanel">
                    <asp:Panel ID="panelFilter" runat="server" DefaultButton="btnRefresh" CssClass="well with-header">
                        <div class="header bordered-blue">Filters</div>
                        <div class="Entry">
                            <asp:Label ID="lblTicketType" runat="server" Text="Ticket Type" AssociatedControlID="ddTicketType" />
                            <asp:DropDownList CssClass="btn-xs" Width="100%" runat="server" ID="ddTicketType" DataTextField="Name" DataValueField="ID" DataSourceID="dsTicketType" />
                        </div>
                        <div class="Entry">
                            <asp:Label ID="lblShipper" runat="server" Text="Shipper" AssociatedControlID="ddShipper" />
                            <asp:DropDownList CssClass="btn-xs" Width="100%" runat="server" ID="ddShipper" DataTextField="Name" DataValueField="ID" DataSourceID="dsShipper" />
                        </div>
                        <div class="Entry">
                            <asp:Label ID="lblProductGroup" runat="server" Text="Product Group" AssociatedControlID="ddProductGroup" />
                            <asp:DropDownList CssClass="btn-xs" Width="100%" runat="server" ID="ddProductGroup" DataTextField="Name" DataValueField="ID" DataSourceID="dsProductGroup" />
                        </div>
                        <div class="Entry">
                            <asp:Label ID="lblProducer" runat="server" Text="Producer" AssociatedControlID="ddProducer" />
                            <asp:DropDownList CssClass="btn-xs" Width="100%" runat="server" ID="ddProducer" DataTextField="Name" DataValueField="ID" DataSourceID="dsProducer" />
                        </div>
                        <div>
                            <div class="center">
                                <asp:Button ID="btnRefresh" runat="server" Text="Refresh" CssClass="btn btn-blue shiny" OnClick="btnRefresh_Click" UseSubmitBehavior="false" />
                            </div>
                        </div>
                    </asp:Panel>
                </div>
                <div id="gridArea" style="height: 100%; min-height: 500px;">
                    <telerik:RadGrid ID="rgMain" runat="server" EnableHeaderContextMenu="False" CellSpacing="0" GridLines="None" EnableLinqExpressions="false"
                                     AllowSorting="True" AllowFilteringByColumn="false" Height="800" CssClass="GridRepaint" ShowGroupPanel="false" EnableEmbeddedSkins="true" Skin="Vista" AlternatingItemStyle-BackColor="#dcf2fc"
                                     DataSourceID="dsMain" OnItemCreated="grid_ItemCreated" OnItemDataBound="grid_ItemDataBound" OnItemCommand="grid_ItemCommand">
                        <ClientSettings AllowDragToGroup="True">
                            <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                        </ClientSettings>
                        <SortingSettings EnableSkinSortStyles="false" />
                        <GroupingSettings CaseSensitive="False" ShowUnGroupButton="true" />
                        <MasterTableView AutoGenerateColumns="False" DataKeyNames="ID" CommandItemDisplay="Top" AllowMultiColumnSorting="true">
                            <CommandItemSettings ShowAddNewRecordButton="true" AddNewRecordText="Add New Pickup ZPL" ShowExportToExcelButton="false" ShowRefreshButton="false" />
                            <RowIndicatorColumn Visible="True">
                                <HeaderStyle Width="20px"></HeaderStyle>
                            </RowIndicatorColumn>
                            <ExpandCollapseColumn Visible="True">
                                <HeaderStyle Width="20px" />
                            </ExpandCollapseColumn>
                            <Columns>
                                <telerik:GridTemplateColumn UniqueName="ActionColumn" HeaderStyle-Width="100px" AllowFiltering="false" AllowSorting="false">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" ID="btnEdit" CssClass="btn btn-xs btn-default shiny" CommandName="Edit" Text="Edit" ImageUrl="~/images/edit.png" />
                                        <asp:ImageButton runat="server" ID="btnDelete" CssClass="btn btn-xs btn-default shiny" CommandName="Delete" Text="Delete" ConfirmText="Are you sure?" ImageUrl="~/images/delete.png" />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>

                                <telerik:GridTemplateColumn DataField="TemplateText" EditFormColumnIndex="1"
                                                            HeaderText="Template Text" SortExpression="Notes" FilterControlWidth="70%" UniqueName="Notes" Groupable="false">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtTemplateText" runat="server" Text='<%# Bind("TemplateText") %>' TextMode="MultiLine" Height="150px" Width="450px" />
                                    </EditItemTemplate>
                                    <HeaderStyle Width="100px" />
                                </telerik:GridTemplateColumn>

                                <telerik:GridDropDownColumn UniqueName="TicketTypeID" DataField="TicketTypeID" HeaderText="Ticket Type" SortExpression="TicketType"
                                                            FilterControlWidth="70%" HeaderStyle-Width="150px" EditFormColumnIndex="0" ItemStyle-Width="250px"
                                                            DataSourceID="dsTicketType" ListValueField="ID" ListTextField="Name"
                                                            FilterControlAltText="LightGreen|White" />
                                <telerik:GridDropDownColumn UniqueName="ShipperID" DataField="ShipperID" HeaderText="Shipper" SortExpression="Shipper"
                                                            FilterControlWidth="70%" HeaderStyle-Width="150px" EditFormColumnIndex="0" ItemStyle-Width="250px"
                                                            DataSourceID="dsShipper" ListValueField="ID" ListTextField="Name"
                                                            FilterControlAltText="LightGreen|White" />
                                <telerik:GridDropDownColumn UniqueName="ProductGroupID" DataField="ProductGroupID" HeaderText="Product Group" SortExpression="ProductGroup"
                                                            FilterControlWidth="70%" HeaderStyle-Width="150px" EditFormColumnIndex="0" ItemStyle-Width="250px"
                                                            DataSourceID="dsProductGroup" ListValueField="ID" ListTextField="Name"
                                                            FilterControlAltText="LightGreen|White" />
                                <telerik:GridDropDownColumn UniqueName="ProducerID" DataField="ProducerID" HeaderText="Producer"
                                                            FilterControlWidth="70%" HeaderStyle-Width="150px" EditFormColumnIndex="0" ItemStyle-Width="250px" SortExpression="Producer"
                                                            DataSourceID="dsProducer" ListValueField="ID" ListTextField="Name"
                                                            FilterControlAltText="LightGreen|White" />

                                <telerik:GridTemplateColumn UniqueName="Validation" HeaderStyle-Width="0px" Display="false" EditFormColumnIndex="2" EditFormHeaderTextFormat="">
                                    <EditItemTemplate>
                                        <asp:CustomValidator ID="cvGridError" runat="server" Display="None" ErrorMessage="Generic Message" />
                                        <asp:ValidationSummary ID="validationSummaryMain" runat="server" DisplayMode="BulletList" Enabled="true" EnableClientScript="true"
                                                               CssClass="NullValidator" />
                                    </EditItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="CreateDate" UniqueName="CreateDate" SortExpression="CreateDate" DataType="System.DateTime" DataFormatString="{0:M/d/yy}"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Create Date" HeaderStyle-Width="130px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="CreatedByUser" UniqueName="CreatedByUser" SortExpression="CreatedByUser"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Created By" HeaderStyle-Width="80px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="LastChangeDate" UniqueName="LastChangeDate" SortExpression="LastChangeDate" DataType="System.DateTime" DataFormatString="{0:M/d/yy HH:mm}"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Last Update Date" HeaderStyle-Width="130px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="LastChangedByUser" UniqueName="LastChangedByUser" SortExpression="LastChangedByUser"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Last Updated By" HeaderStyle-Width="95px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                            </Columns>
                            <EditFormSettings ColumnNumber="3">
                                <EditColumn FilterControlAltText="Filter EditCommandColumn column" ButtonType="ImageButton" CancelImageUrl="~/images/cancel.png"
                                            UpdateImageUrl="~/images/apply.png" InsertImageUrl="~/images/apply.png" />
                            </EditFormSettings>
                            <PagerStyle AlwaysVisible="False" />
                        </MasterTableView>
                        <HeaderStyle Wrap="False" />
                        <PagerStyle AlwaysVisible="True" />
                        <FilterMenu EnableImageSprites="False" />
                    </telerik:RadGrid>
                </div>
                <blac:DBDataSource ID="dsMain" runat="server" SelectIDNullsToZero="true"
                                   SelectCommand="SELECT *, spacer=NULL FROM fnBestMatchGaugerAppPrintPickupTemplateDisplay(@TicketTypeID, @ShipperID, @ProductGroupID, @ProducerID) ORDER BY TicketType, Shipper, ProductGroup, Producer">
                    <SelectParameters>
                        <asp:ControlParameter Name="TicketTypeID" ControlID="ddTicketType" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
                        <asp:ControlParameter Name="ShipperID" ControlID="ddShipper" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
                        <asp:ControlParameter Name="ProductGroupID" ControlID="ddProductGroup" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
                        <asp:ControlParameter Name="ProducerID" ControlID="ddProducer" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
                    </SelectParameters>
                </blac:DBDataSource>
                <blc:RadGridDBCtrl ID="dbcMain" runat="server"
                                   ControlID="rgMain"
                                   UpdateTableName="tblGaugerAppPrintPickupTemplate"
                                   FilterActiveEntities="False" />
                <blac:DBDataSource ID="dsTicketType" runat="server" SelectCommand="SELECT ID, Name FROM tblTicketType UNION SELECT 0, '(All)' ORDER BY Name" />
                <blac:DBDataSource ID="dsShipper" runat="server" SelectCommand="SELECT ID, Name FROM dbo.tblCustomer UNION SELECT 0, '(All)' ORDER BY Name" />
                <blac:DBDataSource ID="dsProductGroup" runat="server" SelectCommand="SELECT ID, Name FROM dbo.tblProductGroup UNION SELECT 0, '(All)' ORDER BY Name" />
                <blac:DBDataSource ID="dsProducer" runat="server" SelectCommand="SELECT ID, Name FROM dbo.tblProducer UNION SELECT 0, '(All)' ORDER BY Name" />
            </div>
        </div>
    </div>
</asp:Content>