﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.IO;
//add for sql stuff
using System.Data;
using System.Data.SqlClient;
using Telerik.Web.UI;
using AlonsIT;
using DispatchCrude.Core;
using DispatchCrude.Extensions;
//Add for image stuff
using DispatchCrude.Controllers;
using DispatchCrude.Models;
using System.Drawing;

namespace DispatchCrude.Site.Orders
{
    public partial class Orders_BOLPrint : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            rdpStart.SelectedDate = rdpEnd.SelectedDate = DateTime.Now.AddDays(+1).Date;
            rdpStart.Calendar.ShowRowHeaders = rdpEnd.Calendar.ShowRowHeaders = false;

            // only show the Carrier column when the logged in user has access to all Carriers
            rgMain.Columns.FindByUniqueName("Carrier").Visible = DBHelper.ToInt32(Context.Profile.GetPropertyValue("CarrierID")) == -1;
            // only show the Customer [Shipper] column when the logged in user has access to all Shippers
            rgMain.Columns.FindByUniqueName("Customer").Visible = DBHelper.ToInt32(Context.Profile.GetPropertyValue("CustomerID")) == -1;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ConfigureAjax(!Settings.SettingsID.DisableAJAX.AsBool());

            //Print tab navigation html to page
            tabmenu.InnerHtml = NavigationHelper.PrintTabArray(NavigationHelper.TabSet_Reports, "Tab_BOLs").ToString();

            //Hide left filters based upon association with an "outside" company (Carrier/Shipper)
            if (DBHelper.ToInt32(Context.Profile.GetPropertyValue("CarrierID")) != -1)
            {
                ddCarrier.Visible = false;
                lblCarrier.Visible = false;
            }

            if (DBHelper.ToInt32(Context.Profile.GetPropertyValue("CustomerID")) != -1)
            {
                ddShipper.Visible = false;
                lblShipper.Visible = false;
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            // Hide ID column on the website
            rgMain.MasterTableView.Columns.FindByUniqueName("ID").Display = false;
        }

        private bool IsInRole(string roleName)
        {
            return Context.User.IsInRole(roleName);
        }

        private void ConfigureAjax(bool enabled = true)
        {
            App_Code.RadAjaxHelper.AddAjaxSetting(this.Page, rgMain, rgMain);
            App_Code.RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, rgMain);
        }

        protected void cmdFilter_Click(object source, EventArgs e)
        {
            // refresh the grid
            this.rgMain.Rebind();
        }
        protected void cmdExportToExcel_Click(object source, EventArgs e)
        {
            if (SelectedIDs().Count == 0)
                AlertUserNoSelection();
            else
                ExportOrdersToExcel();
        }
        protected void cmdExportBOLZip_Click(object source, EventArgs e)
        {
            if (SelectedIDs().Count == 0)
                AlertUserNoSelection();
            else
                ExportBOLZip();
        }
        private void AlertUserNoSelection()
        {
            // provide a dialog to the user indicating nothing was selected (so no orders exported)
            radWindowManager.RadAlert("No orders were selected", 250, 75, "Export Feedback", null);
        }

        private List<int> SelectedIDs()
        {
            List<int> ret = new List<int>();
            foreach (GridItem item in rgMain.MasterTableView.Items)
            {
                if (item.Selected)
                {
                    ret.Add(Converter.ToInt32((item as GridDataItem).GetDataKeyValue("ID")));
                }
            }
            return ret;
        }

        private void ExportBOLZip()
        {
            Session[DispatchCrude.Controllers.OrdersController.SESSION_NAME_ORDERIDLIST] = SelectedIDs();
            string filenameBase = string.Format("BOLS_{0:yyyyMMdd}-{1:yyyyMMdd}", rdpStart.SelectedDate.Value, rdpEnd.SelectedDate.Value);
            Response.Redirect(string.Format("~/Orders/PrintBOLs?filename={0}.zip", filenameBase));
        }

        private void ExportOrdersToExcel()
        {
            string filename = string.Format("Orders_{0:yyyyMMdd}-{1:yyyyMMdd}.xlsx", rdpStart.SelectedDate.Value, rdpEnd.SelectedDate.Value);
            MemoryStream ms = new App_Code.RadGridExcelExporter(onlySelected: true).ExportSheet(rgMain.MasterTableView);
            Response.ExportExcelStream(ms, filename);
        }

        protected void grid_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            using (SSDB db = new SSDB())
            {
                using (SqlCommand cmd = db.BuildCommand("spOrdersBasicExport"))
                {
                    //Date range is always required
                    cmd.Parameters["@StartDate"].Value = rdpStart.SelectedDate.Value;
                    cmd.Parameters["@EndDate"].Value = rdpEnd.SelectedDate.Value;
                    
                    //Filter based upon user's profile settings or user choice if no profile restriction is set
                    cmd.Parameters["@CarrierID"].Value = (DBHelper.ToInt32(Context.Profile.GetPropertyValue("CarrierID")) != -1) ? DBHelper.ToInt32(Context.Profile.GetPropertyValue("CarrierID")) : ((ddCarrier.SelectedValue == "0") ? -1 : DBHelper.ToInt32(ddCarrier.SelectedValue));
                    cmd.Parameters["@CustomerID"].Value = (DBHelper.ToInt32(Context.Profile.GetPropertyValue("CustomerID")) != -1) ? DBHelper.ToInt32(Context.Profile.GetPropertyValue("CustomerID")) : ((ddShipper.SelectedValue == "0") ? -1 : DBHelper.ToInt32(ddShipper.SelectedValue));
                    cmd.Parameters["@ProducerID"].Value = DBHelper.ToInt32(Context.Profile.GetPropertyValue("ProducerID"));

                    //Filter based upon user choice
                    cmd.Parameters["@DriverID"].Value = (ddDriver.SelectedValue != "0") ? DBHelper.ToInt32(ddDriver.SelectedValue) : 0;
                    cmd.Parameters["@OriginID"].Value = (ddOrigin.SelectedValue != "0") ? DBHelper.ToInt32(ddOrigin.SelectedValue) : 0;
                    cmd.Parameters["@DestinationID"].Value = (ddDestination.SelectedValue != "0") ? DBHelper.ToInt32(ddDestination.SelectedValue) : 0;
                    cmd.Parameters["@Rejected"].Value = ddlRejected.SelectedValue;

                    if (txtOrderNum.Text.Length > 0)  //If order number is specified
                        cmd.Parameters["@OrderNum"].Value = txtOrderNum.Text;

                    if (txtJobNumber.Text.Length > 0)  //If job number is specified
                        cmd.Parameters["@JobNumber"].Value = txtJobNumber.Text;

                    if (txtContractNumber.Text.Length > 0)  //If contract number is specified
                        cmd.Parameters["@ContractNumber"].Value = txtContractNumber.Text;

                    cmd.Parameters["@StatusID_CSV"].Value = "3,4"; //Delivered and Audited orders only
                    (sender as RadGrid).DataSource = Core.DateHelper.AddLocalRowStateDateFields(SSDB.GetPopulatedDataTable(cmd));
                }
            }
        }

        // Download single PDF
        protected void cmdExportSingle_Click(object sender, EventArgs e)
        {
            object id = ((sender as Button).NamingContainer as GridDataItem).GetDataKeyValue("ID");
            this.Response.Redirect(string.Format("~/Orders/PrintBOL/{0}", id));
        }

        // Download single origin photo
        protected void cmdOriginPhoto_Click(object sender, EventArgs e)
        {
            HiddenField hf = (HiddenField)App_Code.RadGridHelper.GetControlByType((sender as WebControl).NamingContainer as GridItem, "OriginPhotoColumn", typeof(HiddenField));
            ExportPhoto(Response.OutputStream, DBHelper.ToInt32(hf.Value), "Origin");
        }

        // Download single origin photo
        protected void cmdOriginPhoto2_Click(object sender, EventArgs e)
        {
            HiddenField hf = (HiddenField)App_Code.RadGridHelper.GetControlByType((sender as WebControl).NamingContainer as GridItem, "OriginPhoto2Column", typeof(HiddenField));
            ExportPhoto(Response.OutputStream, DBHelper.ToInt32(hf.Value), "Origin2");
        }

        // Download single origin arrive photo
        protected void cmdOriginArrivePhoto_Click(object sender, EventArgs e)
        {
            HiddenField hf = (HiddenField)App_Code.RadGridHelper.GetControlByType((sender as WebControl).NamingContainer as GridItem, "OriginArrivePhotoColumn", typeof(HiddenField));
            ExportPhoto(Response.OutputStream, DBHelper.ToInt32(hf.Value), "Origin_Arrive");
        }

        // Download single origin depart photo
        protected void cmdOriginDepartPhoto_Click(object sender, EventArgs e)
        {
            HiddenField hf = (HiddenField)App_Code.RadGridHelper.GetControlByType((sender as WebControl).NamingContainer as GridItem, "OriginDepartPhotoColumn", typeof(HiddenField));
            ExportPhoto(Response.OutputStream, DBHelper.ToInt32(hf.Value), "Origin_Depart");
        }

        // Download single destination photo
        protected void cmdDestPhoto_Click(object sender, EventArgs e)
        {
            HiddenField hf = (HiddenField)App_Code.RadGridHelper.GetControlByType((sender as WebControl).NamingContainer as GridItem, "DestPhotoColumn", typeof(HiddenField));
            ExportPhoto(Response.OutputStream, DBHelper.ToInt32(hf.Value), "Destination");
        }

        // Download single destination photo
        protected void cmdDestPhoto2_Click(object sender, EventArgs e)
        {
            HiddenField hf = (HiddenField)App_Code.RadGridHelper.GetControlByType((sender as WebControl).NamingContainer as GridItem, "DestPhoto2Column", typeof(HiddenField));
            ExportPhoto(Response.OutputStream, DBHelper.ToInt32(hf.Value), "Destination2");
        }

        // Download single Destination arrive photo
        protected void cmdDestArrivePhoto_Click(object sender, EventArgs e)
        {
            HiddenField hf = (HiddenField)App_Code.RadGridHelper.GetControlByType((sender as WebControl).NamingContainer as GridItem, "OriginArrivePhotoColumn", typeof(HiddenField));
            ExportPhoto(Response.OutputStream, DBHelper.ToInt32(hf.Value), "Destination_Arrive");
        }

        // Download single destination depart photo
        protected void cmdDestDepartPhoto_Click(object sender, EventArgs e)
        {
            HiddenField hf = (HiddenField)App_Code.RadGridHelper.GetControlByType((sender as WebControl).NamingContainer as GridItem, "OriginDepartPhotoColumn", typeof(HiddenField));
            ExportPhoto(Response.OutputStream, DBHelper.ToInt32(hf.Value), "Destination_Depart");
        }

        // Download single reject photo
        protected void cmdRejectPhoto_Click(object sender, EventArgs e)
        {
            HiddenField hf = (HiddenField)App_Code.RadGridHelper.GetControlByType((sender as WebControl).NamingContainer as GridItem, "RejectPhotoColumn", typeof(HiddenField));
            ExportPhoto(Response.OutputStream, DBHelper.ToInt32(hf.Value), "Reject");
        }

        private void ExportPhoto(Stream stream, int photoID, string locationName)
        {
            OrderPhoto photo = null;
            using (DispatchCrudeDB dcdb = new DispatchCrudeDB())
            {
                photo = dcdb.OrderPhotos.Find(photoID);
            }

            using (SSDB db = new SSDB())
            {
                string filename = string.Format(
                    "{0}_{1}.jpg"
                    , db.QuerySingleValue("SELECT O.OrderNum FROM tblOrder O JOIN tblOrderPhoto OP ON OP.OrderID = O.ID WHERE OP.ID = {0}", photoID)
                    , locationName);
                Response.Clear();
                Response.AppendCookie(new HttpCookie("fileDownloadToken", "done"));
                Response.ContentType = "application/jpg";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename=\"{0}\";", filename));
                //db.QuerySingleToStream(Response.OutputStream, "SELECT PhotoBlob FROM tblOrderPhoto WHERE ID = {0}", photoID);

                //****DCWEB-1674 Changed the way we had to get the photos for the bol zip
                MemoryStream ms = new MemoryStream(photo.PhotoBlob);
                System.Drawing.Image currentImage = new Bitmap(ms);

                byte[] newImage = OrdersController.ImageToByte2(OrdersController.AppendFooter(currentImage, photo.PhotoDateUTC, photo.GPSLocation));

                ms = new MemoryStream(newImage);
                System.Drawing.Image finalImage = new Bitmap(ms);
                finalImage.Save(Response.OutputStream, System.Drawing.Imaging.ImageFormat.Jpeg);
                //******

                Response.StatusCode = 200;
                Response.EndSafe();
            }
        }

        protected void grid_ItemCreated(object sender, GridItemEventArgs e)
        {
        } 

        protected void grid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem && e.Item.DataItem is DataRowView)
            {
                // Show/Hide photo columns button's based on existence of photos
                ImageButton btn = (ImageButton)App_Code.RadGridHelper.GetControlByType(e.Item, "OriginPhotoColumn", typeof(ImageButton));
                if (btn != null)
                    btn.Visible = !DBHelper.IsNull((e.Item.DataItem as DataRowView)["OriginPhotoID"]);

                btn = (ImageButton)App_Code.RadGridHelper.GetControlByType(e.Item, "OriginPhoto2Column", typeof(ImageButton));
                if (btn != null)
                    btn.Visible = !DBHelper.IsNull((e.Item.DataItem as DataRowView)["OriginPhoto2ID"]);

                btn = (ImageButton)App_Code.RadGridHelper.GetControlByType(e.Item, "OriginArrivePhotoColumn", typeof(ImageButton));
                if (btn != null)
                    btn.Visible = !DBHelper.IsNull((e.Item.DataItem as DataRowView)["OriginArrivePhotoID"]);

                btn = (ImageButton)App_Code.RadGridHelper.GetControlByType(e.Item, "OriginDepartPhotoColumn", typeof(ImageButton));
                if (btn != null)
                    btn.Visible = !DBHelper.IsNull((e.Item.DataItem as DataRowView)["OriginDepartPhotoID"]);

                btn = (ImageButton)App_Code.RadGridHelper.GetControlByType(e.Item, "DestPhotoColumn", typeof(ImageButton));
                if (btn != null)
                    btn.Visible = !DBHelper.IsNull((e.Item.DataItem as DataRowView)["DestPhotoID"]);

                btn = (ImageButton)App_Code.RadGridHelper.GetControlByType(e.Item, "DestPhoto2Column", typeof(ImageButton));
                if (btn != null)
                    btn.Visible = !DBHelper.IsNull((e.Item.DataItem as DataRowView)["DestPhoto2ID"]);

                btn = (ImageButton)App_Code.RadGridHelper.GetControlByType(e.Item, "DestArrivePhotoColumn", typeof(ImageButton));
                if (btn != null)
                    btn.Visible = !DBHelper.IsNull((e.Item.DataItem as DataRowView)["DestArrivePhotoID"]);

                btn = (ImageButton)App_Code.RadGridHelper.GetControlByType(e.Item, "DestDepartPhotoColumn", typeof(ImageButton));
                if (btn != null)
                    btn.Visible = !DBHelper.IsNull((e.Item.DataItem as DataRowView)["DestDepartPhotoID"]);

                btn = (ImageButton)App_Code.RadGridHelper.GetControlByType(e.Item, "RejectPhotoColumn", typeof(ImageButton));
                if (btn != null)
                    btn.Visible = !DBHelper.IsNull((e.Item.DataItem as DataRowView)["RejectPhotoID"]);
            }
        }

        protected void grid_DataBound(object sender, EventArgs e)
        {
            //cmdExportToExcel.Enabled = cmdExportBOLZip.Enabled = SelectedIDs().Count > 0;
        }

        protected void grid_ItemCommand(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == "PrintSingle")
            {
                e.Canceled = true;
                this.Response.Redirect(string.Format("~/Orders/PrintBOL/{0}", (e.Item as GridDataItem).GetDataKeyValue("ID")));
            }
        }        

    }
}