﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Order_Reroute_Popup.aspx.cs" Inherits="DispatchCrude.Site.Orders.Order_Reroute_Popup" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title/>
    <link href="~/Content/beyondadmin/bootstrap.min.css" rel="stylesheet"/>
    <!--Beyond styles-->
    <link href="~/Content/beyondadmin/beyond.min.css" rel="stylesheet"/>
    <link href="~/Content/beyondadmin/demo.min.css" rel="stylesheet"/>
    <link href="~/Content/beyondadmin/font-awesome.min.css" rel="stylesheet"/>
    <link href="~/Content/beyondadmin/typicons.min.css" rel="stylesheet"/>
    <link href="~/Content/beyondadmin/weather-icons.min.css" rel="stylesheet"/>
    <link href="~/Content/beyondadmin/animate.min.css" rel="stylesheet"/>

    <!-- Custom DC stylesheet including overrides -->
    <link href="~/Content/dispatchcrude.css" rel="stylesheet"/>
</head>
<body>
    <form id="form1" runat="server" class="popupform">
        <telerik:RadScriptManager ID="radScriptMgr" runat="server">
            <Scripts>
            </Scripts>
        </telerik:RadScriptManager>
        <telerik:RadAjaxManager ID="ram" runat="server">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="ddlNewDest" >
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="cmdReroute" UpdatePanelRenderMode="Inline" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <asp:Panel id="panelMain" runat="server" Width="350px" Height="240px" >
            <asp:HiddenField ID="hiddenOrderNum" runat="server" />
            <blac:SingleRowDataHelper ID="srdhOrderNum" runat="server" ControlToBind="hiddenOrderNum" DataField="OrderNum" PropertyName="Value" />
            <asp:HiddenField ID="hiddenOriginID" runat="server" />
            <blac:SingleRowDataHelper ID="srdhOriginID" runat="server" ControlToBind="hiddenOriginID" DataField="OriginID" PropertyName="Value" />
            <asp:HiddenField ID="hiddenCustomerID" runat="server" />
            <blac:SingleRowDataHelper ID="srdhCustomerID" runat="server" ControlToBind="hiddenCustomerID" DataField="CustomerID" PropertyName="Value" />
            <asp:HiddenField ID="hiddenProductID" runat="server" />
            <blac:SingleRowDataHelper ID="srdhProductID" runat="server" ControlToBind="hiddenProductID" DataField="ProductID" PropertyName="Value" />
            <asp:HiddenField ID="hiddenCurrentDestID" runat="server" />
            <blac:SingleRowDataHelper ID="srdhCurrentDestID" runat="server" ControlToBind="hiddenCurrentDestID" DataField="DestinationID" PropertyName="Value" />

            <asp:HiddenField ID="hfRestrictedTerminalID" ClientIDMode="Static" runat="server" value="-1" />

            <asp:Label ID="lblCurrentDest" runat="server" AssociatedControlID="txtCurrentDest" Text="Current Destination" />
            <asp:TextBox ID="txtCurrentDest" runat="server" Width="200px" ReadOnly="true" />
            <blac:SingleRowDataHelper ID="srdhCurrentDest" runat="server" ControlToBind="txtCurrentDest" DataField="Destination" PropertyName="Text" />
            <br />
            <div class="spacer5px"></div>
            <asp:Label ID="lblNewDest" runat="server" AssociatedControlID="ddlNewDest" Text="Reroute Destination" />
            <asp:DropDownList id="ddlNewDest" runat="server" DataSourceID="dsDestination" DataTextField="Destination" DataValueField="DestinationID" Width="200px" 
                AutoPostBack="true" OnSelectedIndexChanged="EntryValueChanged" />
            <asp:RequiredFieldValidator ID="rfvNewDest" runat="server" ControlToValidate="ddlNewDest" 
                Text="*" ErrorMessage="New Destination must be specified" CssClass="NullValidator" />
            <br />
            <div class="spacer5px"></div>
            <asp:Label ID="lblNotes" runat="server" AssociatedControlID="txtNotes" Text="Notes" />
            <asp:TextBox ID="txtNotes" runat="server" Rows="3" Width = "320px" TextMode="MultiLine" />
                <%--AutoPostBack="true" OnTextChanged="EntryValueChanged" />--%>
            <asp:RequiredFieldValidator ID="rfvNotes" runat="server" ControlToValidate="txtNotes" 
                Text="*" ErrorMessage="Notes documenting the Reroute are required" CssClass="NullValidator" />

            <asp:Panel ID="panelCtrl" runat="server" HorizontalAlign="Center" BackColor="Transparent" >
                <asp:Button ID="cmdReroute" runat="server" Text="Reroute" CssClass="btn btn-blue shiny" 
                    OnClientClick="cmdReroute_ClientClicked" OnClick="cmdReroute_Click" 
                    Enabled="false" CausesValidation="true" />
            </asp:Panel>
            <asp:ValidationSummary ID="validateSummaryMain" runat="server" DisplayMode="BulletList" CssClass="NullValidator" BackColor="White" ShowSummary="true" />
        </asp:Panel>
        <telerik:RadScriptBlock runat="server">
            <script type="text/javascript">
                function GetRadWindow() {
                    var oWindow = null;
                    if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog 
                    else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz as well) 

                    return oWindow;
                }
                function CloseWnd() {
                    debugger;
                    GetRadWindow().close();
                }
                $(document).ready(function () {
                    alert ("hey")
                })
            </script>
        </telerik:RadScriptBlock>
    </form>
    <blac:DBDataSource ID="dsDestination" runat="server" 
        SelectCommand="SELECT ID AS DestinationID, Destination = Name FROM dbo.fnRetrieveEligibleDestinations(@OriginID, @ShipperID, @ProductID, 0) D WHERE ID <> @CurrentDestID AND (ISNULL(@TerminalID, 0) <= 0 OR @TerminalID = TerminalID OR TerminalID IS NULL) UNION SELECT NULL, '(Select Destination)' ORDER BY Destination" >
        <SelectParameters>
            <asp:ControlParameter ControlID="hiddenOriginID" PropertyName="Value" Name="OriginID" DefaultValue="0" />
            <asp:ControlParameter ControlID="hiddenCustomerID" PropertyName="Value" Name="ShipperID" DefaultValue="0" />
            <asp:ControlParameter ControlID="hiddenProductID" PropertyName="Value" Name="ProductID" DefaultValue="0" />
            <asp:ControlParameter ControlID="hiddenCurrentDestID" PropertyName="Value" Name="CurrentDestID" DefaultValue="0" />
            <asp:ControlParameter ControlID="hfRestrictedTerminalID" PropertyName="Value" Name="TerminalID" DefaultValue="-1" />
        </SelectParameters>
    </blac:DBDataSource>
</body>
</html>
