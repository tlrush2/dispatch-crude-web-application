﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Order_Transfer_Popup.aspx.cs" Inherits="DispatchCrude.Site.Orders.Order_Transfer_Popup" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%-- ---------------------------------------------------------------------- --%>
<%-- Author: Joe Engler                                                     --%>
<%-- Date: 9/24/2015                                                        --%>
<%--                                                                        --%>
<%-- Summary: Popup dialog to transfer an order                             --%>
<%-- Notes:                                                                 --%>
<%--   * Screen uses the REQUIRE TRUCK MILEAGE variable to determine        --%>
<%--       which fields are captured/required                               --%>
<%--   * Key on Order ID prevents a field from being transferred twice      --%>
<%--                                                                        --%>
<%-- ---------------------------------------------------------------------- --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title/>
    <link href="~/Content/beyondadmin/bootstrap.min.css" rel="stylesheet"/>
    <!--Beyond styles-->
    <link href="~/Content/beyondadmin/beyond.min.css" rel="stylesheet"/>
    <link href="~/Content/beyondadmin/demo.min.css" rel="stylesheet"/>
    <link href="~/Content/beyondadmin/font-awesome.min.css" rel="stylesheet"/>
    <link href="~/Content/beyondadmin/typicons.min.css" rel="stylesheet"/>
    <link href="~/Content/beyondadmin/weather-icons.min.css" rel="stylesheet"/>
    <link href="~/Content/beyondadmin/animate.min.css" rel="stylesheet"/>

    <!-- Custom DC stylesheet including overrides -->
    <link href="~/Content/dispatchcrude.css" rel="stylesheet"/>
</head>
<body>
    <form id="form1" runat="server" class="popupform">
        <telerik:RadScriptManager ID="radScriptMgr" runat="server">
            <Scripts>
            </Scripts>
        </telerik:RadScriptManager>
        <telerik:RadAjaxManager ID="ram" runat="server">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="ddlNewDest" >
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="cmdTransfer" UpdatePanelRenderMode="Inline" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <asp:Panel id="panelMain" runat="server" Width="350px" Height="340px" >
            <asp:HiddenField ID="hiddenOrderNum" runat="server" />
            <blac:SingleRowDataHelper ID="srdhOrderNum" runat="server" ControlToBind="hiddenOrderNum" DataField="OrderNum" PropertyName="Value" />
            <asp:HiddenField ID="hiddenOriginID" runat="server" />
            <blac:SingleRowDataHelper ID="srdhOriginID" runat="server" ControlToBind="hiddenOriginID" DataField="OriginID" PropertyName="Value" />
            <asp:HiddenField ID="hiddenCustomerID" runat="server" />
            <blac:SingleRowDataHelper ID="srdhCustomerID" runat="server" ControlToBind="hiddenCustomerID" DataField="CustomerID" PropertyName="Value" />
            <asp:HiddenField ID="hiddenProductID" runat="server" />
            <blac:SingleRowDataHelper ID="srdhProductID" runat="server" ControlToBind="hiddenProductID" DataField="ProductID" PropertyName="Value" />
            <asp:HiddenField ID="hiddenCurrentDriverID" runat="server" />
            <blac:SingleRowDataHelper ID="srdhCurrentDriverID" runat="server" ControlToBind="hiddenCurrentDriverID" DataField="DriverID" PropertyName="Value" />
            <asp:HiddenField ID="hiddenCurrentTruckID" runat="server" />
            <blac:SingleRowDataHelper ID="srdhCurrentTruckID" runat="server" ControlToBind="hiddenCurrentTruckID" DataField="TruckID" PropertyName="Value" />
            <asp:HiddenField ID="hiddenCarrierID" runat="server" />
            <blac:SingleRowDataHelper ID="srdhCarrierID" runat="server" ControlToBind="hiddenCarrierID" DataField="CarrierID" PropertyName="Value" />
            <asp:HiddenField ID="hiddenRegionID" runat="server" />
            <blac:SingleRowDataHelper ID="srdhRegionID" runat="server" ControlToBind="hiddenRegionID" DataField="RegionID" PropertyName="Value" />
            <asp:HiddenField ID="hiddenDueDate" runat="server" />
            <blac:SingleRowDataHelper ID="srdhDueDate" runat="server" ControlToBind="hiddenDueDate" DataField="DueDate" PropertyName="Value" />
            <asp:HiddenField ID="hfOriginTruckEndMileage" runat="server" />
            <blac:SingleRowDataHelper ID="srdhCurrentTruckOldMiles" runat="server" ControlToBind="hfOriginTruckEndMileage" DataField="OriginTruckMileage" PropertyName="Value" />
            <asp:HiddenField ID="hiddenRouteMiles" runat="server" />
            <blac:SingleRowDataHelper ID="srdhRouteMiles" runat="server" ControlToBind="hiddenRouteMiles" DataField="ActualMiles" PropertyName="Value" />

            <asp:HiddenField ID="hfRestrictedTerminalID" ClientIDMode="Static" runat="server" value="-1" />

            <asp:Label ID="lblCurrentDriver" runat="server" AssociatedControlID="txtCurrentDriver" Text="Current Driver" />
            <asp:TextBox ID="txtCurrentDriver" runat="server" Width="200px" Enabled="false" />
            <blac:SingleRowDataHelper ID="srdhCurrentDriver" runat="server" ControlToBind="txtCurrentDriver" DataField="Driver" PropertyName="Text" />

            <br /><div class="spacer5px"></div>

            <asp:Label ID="lblNewDriver" runat="server" AssociatedControlID="rcbNewDriver" Text="Transfer Driver" />
            <telerik:RadComboBox ID="rcbNewDriver" runat="server" Width="200px" 
                DataSourceID="dsDriver" DataTextField="Driver" DataValueField="DriverID" 
                CausesValidation="true" ValidationGroup="OrderEdit"
                AutoPostBack="true" OnItemDataBound="rcbNewDriver_ItemDataBound" OnSelectedIndexChanged="EntryValueChanged" />
            <asp:Label ID="rfvNewDriver" runat="server" AssociatedControlID="rcbNewDriver" class="NullValidator" Text="" />

            <br /><div class="spacer5px"></div>

            <asp:Label ID="lblCurrentTruck" runat="server" AssociatedControlID="txtCurrentTruck" Text="Current Truck" />
            <asp:TextBox ID="txtCurrentTruck" runat="server" Width="200px" Enabled="false" />
            <blac:SingleRowDataHelper ID="srdhCurrentTruck" runat="server" ControlToBind="txtCurrentTruck" DataField="Truck" PropertyName="Text" />

            <br /><div class="spacer5px"></div>


            <asp:Label ID="lblNewTruck" runat="server" AssociatedControlID="ddlNewTruck" Text="Transfer Truck" />
            <asp:DropDownList id="ddlNewTruck" runat="server" DataSourceID="dsTruck" DataTextField="Truck" DataValueField="TruckID" Width="200px" 
                AutoPostBack="true" OnSelectedIndexChanged="EntryValueChanged" />
            
            <br /><div class="spacer5px"></div>
            
            <asp:Label ID="lblOriginTruckEndMileage" runat="server" AssociatedControlID="txtOriginTruckEndMileage" Text="Current Truck Odometer" />
            <asp:TextBox ID="txtOriginTruckEndMileage" runat="server" Width = "100px" />
            <asp:Label ID="rfvOriginTruckEndMileage" runat="server" AssociatedControlID="txtOriginTruckEndMileage" CssClass="NullValidator" Text="" />

            <br /><div class="spacer5px"></div>

            <asp:Label ID="lblPctComplete" runat="server" AssociatedControlID="txtPctComplete" Text="% Complete" />
            <asp:TextBox ID="txtPctComplete" runat="server" Width = "50px" />
            <asp:Label ID="rfvPctComplete" runat="server" CssClass="NullValidator" Text="" />

            <br /><div class="spacer5px"></div>
            
            <asp:Label ID="lblNotes" runat="server" AssociatedControlID="txtNotes" Text="Notes" />
            <asp:TextBox ID="txtNotes" runat="server" Rows="3" Width = "320px" TextMode="MultiLine" />

            <br /><div class="spacer5px"></div>

            <asp:Panel ID="panelCtrl" runat="server" HorizontalAlign="Center">
                <asp:Button ID="cmdTransfer" runat="server" Text="Transfer" class="btn btn-blue shiny" 
                    OnClientClick="cmdTransfer_ClientClicked" OnClick="cmdTransfer_Click" 
                    Enabled="false" CausesValidation="true" />
                <asp:Button ID="btnClose" runat="server" Text="Cancel" CssClass="btn btn-blue shiny" OnClientClick="CloseWnd();return false;" /><br />
            </asp:Panel>
            <asp:ValidationSummary ID="validateSummaryMain" runat="server" DisplayMode="BulletList" CssClass="NullValidator" BackColor="White" ShowSummary="true" />
        </asp:Panel>
        <telerik:RadScriptBlock runat="server">
            <script type="text/javascript">
                function GetRadWindow() {
                    var oWindow = null;
                    if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog 
                    else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz as well) 

                    return oWindow;
                }
                function CloseWnd() {
                    debugger;
                    GetRadWindow().close();
                }
            </script>
        </telerik:RadScriptBlock>
    </form>
    <blac:DBDataSource ID="dsDriver" runat="server" 
        SelectCommand="SELECT DriverID = ID, Driver = FullName, CarrierID, Carrier, DriverScore FROM dbo.fnRetrieveEligibleDrivers_NoGPS(@CarrierID, @TerminalID, NULL, @RegionID, NULL, @StartDate, ISNULL((SELECT h2s FROM tblOrigin where ID = @OriginID), 0)) UNION SELECT 0, '(Select Driver)', 0, '', 100 ORDER BY DriverScore DESC, FullName" >
        <SelectParameters>
            <asp:ControlParameter Name="CarrierID" ControlID="hiddenCarrierID" PropertyName="Value" DbType="Int32" DefaultValue="0" />
            <asp:ControlParameter Name="TerminalID" ControlID="hfRestrictedTerminalID" PropertyName="Value" DefaultValue="-1" />
            <asp:ControlParameter Name="RegionID" ControlID="hiddenRegionID" PropertyName="Value" Type="Int32" DefaultValue="-1" />
            <asp:ControlParameter Name="OriginID" ControlID="hiddenOriginID" PropertyName="Value" Type="Int32" DefaultValue="-1" />
            <asp:ControlParameter Name="StartDate" ControlID="hiddenDueDate" PropertyName="Value" Type="String" DefaultValue="NULL" />
        </SelectParameters>
    </blac:DBDataSource>
    <blac:DBDataSource ID="dsTruck" runat="server" 
        SelectCommand="SELECT ID as TruckID, FullName as Truck FROM viewTruck WHERE DeleteDateUTC IS NULL AND (@CarrierID = -1 OR CarrierID = @CarrierID) AND (@TerminalID = -1 OR TerminalID = @TerminalID OR TerminalID IS NULL) AND (@TruckID = 0 OR ID!=@TruckID) UNION SELECT 0, '(Current Truck)' ORDER BY FullName" >
        <SelectParameters>
            <asp:ControlParameter Name="CarrierID" ControlID="hiddenCarrierID" PropertyName="Value" DbType="Int32" DefaultValue="0" />
            <asp:ControlParameter Name="TerminalID" ControlID="hfRestrictedTerminalID" PropertyName="Value" DefaultValue="-1" />
            <asp:QueryStringParameter Name="TruckID" DbType="Int32" DefaultValue="0" QueryStringField="TruckID" ConvertEmptyStringToNull="true" />
        </SelectParameters>
    </blac:DBDataSource>
</body>
</html>
