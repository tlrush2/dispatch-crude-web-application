﻿using System;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using DispatchCrude.Core;
using DispatchCrude.Extensions;
using AlonsIT;
using Telerik.Web.UI;

namespace DispatchCrude.Site.Orders
{
    public partial class Orders_SummaryReport : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Print tab navigation html to page
            tabmenu.InnerHtml = NavigationHelper.PrintTabArray(NavigationHelper.TabSet_Reports, "Tab_OrderSummary").ToString();

            if (!IsPostBack)
            {
                DateTime start = DateTime.Now.Date.AddDays(1 - DateTime.Now.Date.Day);
                rdpStart.SelectedDate = start;
                rdpEnd.SelectedDate = start.AddMonths(1).AddDays(-1);
            }

            RemoveExistingGrid();
            foreach (ListItem item in cblEntities.Items)
            {
                if (item.Selected && item.Enabled)
                {
                    RadGrid grid = DefineGridStructure(cblEntities.Items);
                    phGrid.Controls.Add(grid);
                    break;
                }
            }
        }

        protected void cblEntities_SelectedIndexChanged(object source, EventArgs e)
        {
            foreach (ListItem item in cblEntities.Items)
            {
                if (item.Selected && item.Enabled)
                {
                    cmdPopulate.Enabled = true;
                    return;
                }
            }
            cmdPopulate.Enabled = false;
        }

        protected void cmdPopulate_Click(object source, EventArgs e)
        {
            //grid.Rebind();
        }
        protected void cmdExport_Click(object source, EventArgs e)
        {
            RadGrid grid = phGrid.FindControl("rgMain") as RadGrid;
            if (grid != null)
            {
                grid_PreRender(grid, EventArgs.Empty);
                string filename = string.Format("OrderSummaryReport_{0:yyyyMMdd}-{1:yyyyMMdd}.xlsx", rdpStart.SelectedDate, rdpEnd.SelectedDate);
                System.IO.MemoryStream ms = new App_Code.RadGridExcelExporter().ExportSheet(grid.MasterTableView);
                Response.ExportExcelStream(ms, filename);
            }
        }

        private void RemoveExistingGrid()
        {
            phGrid.Controls.Clear();
        }

        protected void grid_PreRender(object sender, EventArgs e)
        {
            GridTableView tbl = (sender as RadGrid).MasterTableView;
            // merge the adjacent column cells with the same values
            foreach (GridDataItem dataItem in tbl.Items)
            {
                for (int i = 0; i < tbl.Columns.Count - 1; i++)
                {
                    int j = 1;
                    if (dataItem[tbl.Columns[i]].Visible && dataItem[tbl.Columns[i]].Text == "Total")
                    {
                        while (dataItem[tbl.Columns[i + j]].Text == "Total")
                        {
                            dataItem[tbl.Columns[i + j]].Visible = false;
                            j++;
                        }
                        if (j > 1)
                            dataItem[tbl.Columns[i]].ColumnSpan = j;
                        dataItem[tbl.Columns[i]].Font.Bold = true;
                        // bold the qty && barrel column cell values
                        for (int bc = 1; bc < 5; bc++)
                        {
                            dataItem[tbl.Columns[tbl.Columns.Count - bc]].Font.Bold = true;
                        }
                    }
                }

                // merge the adjacent row cells with the same values
                for (int i = 0; i < tbl.Columns.Count - 4; i++)
                {
                    GridColumn col = tbl.Columns[i];
                    if (dataItem[col].Visible)
                    {
                        int pos = dataItem.ItemIndex + 1;
                        while (pos < tbl.Items.Count && tbl.Items[pos][col].Text != "Total" && tbl.Items[pos][col].Text == dataItem[col].Text)
                        {
                            tbl.Items[pos][col].Visible = false;
                            pos++;
                        }
                        if (pos > dataItem.ItemIndex + 1)
                        {
                            dataItem[col].RowSpan = (pos - dataItem.ItemIndex);
                        }
                    }
                }
            }  
        }

        class GridDataHelper
        {   // sql based on http://sqlandme.com/2011/07/07/sql-server-tsql-group-by-with-rollup/
            static public DataTable GetData(DateTime start, DateTime end, int uomID, ListItemCollection entities)
            {
                string sql = SelectClause(entities) + FromClause(start, end, uomID) + GroupByClause(entities) + OrderByClause(entities);
                using (SSDB ssdb = new SSDB())
                {
                    DataTable ret = ssdb.GetPopulatedDataTable(sql);
                    foreach (DataColumn col in ret.Columns)
                    {
                        col.ReadOnly = false;
                    }
                    if (ret.Rows.Count > 0)
                    {
                        int totalQty = DBHelper.ToInt32(ret.Rows[ret.Rows.Count - 1]["OrderQty"]);
                        decimal totalVol = Converter.ToDecimal(ret.Rows[ret.Rows.Count - 1]["NetVol"]);
                        foreach (DataRow row in ret.Rows)
                        {
                            row["PercentOfTotalQty"] = System.Math.Round(Converter.ToDecimal(row["OrderQty"]) / (decimal)totalQty, 4);

                            //This check was added to avoid a potential divide by zero error when totalVol is equal to 0.0 -- DCWEB-1939 11/15/16
                            row["PercentofTotalVol"] = (totalVol > (decimal)0.0) ? Math.Round(Converter.ToDecimal(row["NetVol"]) / (decimal)totalVol, 4) : (decimal)0.0;
                        }
                    }
                    return Core.DateHelper.AddLocalRowStateDateFields(ret);
                }
            }
            private static string SelectClause(ListItemCollection entities)
            {
                string ret = "SELECT ";
                foreach (ListItem item in entities)
                {
                    if (item.Selected)
                    {   
                        if (item.Value == "OrderQty")
                            ret += "COUNT(1) AS OrderQty, cast(0 as decimal) as PercentOfTotalQty";
                        else if (item.Value == "NetVol")
                            ret += "sum(Units) AS NetVol, cast(0 as decimal) as PercentofTotalVol";
                        else
                            ret += GroupingSelectClause(item.Value);
                        ret += ",";
                    }
                }
                return ret.Trim(',');
            }
            private static string GroupingSelectClause(string fieldName)
            {
                return string.Format("CASE GROUPING([{0}]) WHEN 1 THEN 'Total' ELSE [{0}] END AS [{0}]", fieldName);
            }
            private static string FromClause(DateTime start, DateTime end, int uomID)
            {
                int uomTypeID;
                string unitFields;

                using (SSDB ssdb = new SSDB()) //Check for selected UOM type and use the weight fields if a weight type UOM was selected
                {
                    uomTypeID = ssdb.QuerySingleValue("SELECT UomTypeID FROM tblUom WHERE ID = {0}", uomID.ToString()).ToInt32();
                    unitFields = (uomTypeID == 1) ? "OriginNetUnits, OriginGrossUnits" : "OriginWeightNetUnits, OriginWeightGrossUnits";
                }                

                return string.Format(" FROM ("
                + "SELECT Units = dbo.fnConvertUom(coalesce(" + unitFields + "), OriginUomID, {2})"
                + ", Uom = (SELECT min(Name) FROM tblUOM WHERE ID={2})"
                + ", CarrierType, Carrier, ProductGroup, Product, Driver, isnull(Truck, 'N/A') AS Truck, isnull(Trailer, 'N/A') AS Trailer "
                + "FROM viewOrderLocalDates "
                + "WHERE DeleteDateUTC IS NULL AND PrintStatusID IN (3,4) AND OrderDate BETWEEN {0} AND {1}"
                + "{3}"
                + ") x "
                , DBHelper.QuoteStr(start.ToShortDateString())
                , DBHelper.QuoteStr(end.ToShortDateString())
                , uomID
                , UserSupport.IsInRole("Customer") ? " AND CustomerID=" + Core.DispatchCrudeHelper.GetProfileValueInt(HttpContext.Current, "CustomerID", 0) : "");
            }
            private static string GroupByClause(ListItemCollection entities)
            {
                string ret = "GROUP BY ";
                foreach (ListItem item in entities)
                {
                    if (item.Selected && item.Value != "OrderQty" && item.Value != "NetVol")
                    {
                        ret += "[" + item.Value + "],";
                    }
                }
                return ret.Trim(',') + " WITH ROLLUP ";
             }
            private static string OrderByClause(ListItemCollection entities)
            {
                string ret = "ORDER BY ";
                foreach (ListItem item in entities)
                {
                    if (item.Selected && item.Value != "OrderQty" && item.Value != "NetVol")
                    {
                        ret += string.Format("CASE WHEN [{0}] = 'Total' THEN 1 ELSE 0 END", item.Value) + ",";
                    }
                }
                return ret.Trim(',');
            }
        }

        private RadGrid DefineGridStructure(ListItemCollection entities)
        {
            RadGrid grid = new RadGrid();
            grid.ID = "rgMain";
            grid.MasterTableView.Columns.Clear();
            grid.ViewStateMode = System.Web.UI.ViewStateMode.Disabled;
            grid.AlternatingItemStyle.BackColor = grid.ItemStyle.BackColor;
            grid.Height = Unit.Pixel(800);
            grid.CssClass = "GridRepaint";
            grid.EnableEmbeddedSkins = true;
            grid.Skin = "Vista";

            grid.AutoGenerateColumns = false;
            grid.CellSpacing = 0;
            grid.GridLines = GridLines.Both;

            foreach (ListItem item in entities)
            {
                if (item.Selected)
                {
                    GridBoundColumn col = new GridBoundColumn();
                    col.DataField = item.Value;
                    col.HeaderText = item.Text.Replace(" *", "");
                    col.UniqueName = item.Value;
                    col.HeaderStyle.Width = 200;
                    col.ItemStyle.VerticalAlign = VerticalAlign.Top;
                    grid.MasterTableView.Columns.Add(col);
                    if (item.Value == "OrderQty")
                    {
                        // add the percentage of total grid column
                        GridBoundColumn pcol = new GridBoundColumn();
                        pcol.DataFormatString = col.DataFormatString = "{0:#,##0}";
                        pcol.UniqueName = pcol.DataField = "PercentOfTotalQty";
                        pcol.HeaderText = "% of Total Qty";
                        col.HeaderStyle.Width = pcol.HeaderStyle.Width = 100;
                        col.HeaderStyle.HorizontalAlign = pcol.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                        pcol.DataFormatString = "{0:0.00%}";
                        pcol.ItemStyle.VerticalAlign = VerticalAlign.Top;
                        col.ItemStyle.HorizontalAlign = pcol.ItemStyle.HorizontalAlign = HorizontalAlign.Right;
                        grid.MasterTableView.Columns.Add(pcol);
                    }
                    else if (item.Value == "NetVol")
                    {
                        // add the percentage of total grid column
                        GridBoundColumn pcol = new GridBoundColumn();
                        pcol.DataFormatString = col.DataFormatString = "{0:#,##0.00}";
                        pcol.UniqueName = pcol.DataField = "PercentofTotalVol";
                        pcol.HeaderText = "% of Total Vol";
                        col.HeaderStyle.Width = pcol.HeaderStyle.Width = 100;
                        col.HeaderStyle.HorizontalAlign = pcol.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                        pcol.DataFormatString = "{0:0.00%}";
                        pcol.ItemStyle.VerticalAlign = VerticalAlign.Top;
                        col.ItemStyle.HorizontalAlign = pcol.ItemStyle.HorizontalAlign = HorizontalAlign.Right;
                        grid.MasterTableView.Columns.Add(pcol);
                    }
                    else if (!col.DataTypeName.Contains("String"))
                    {
                        col.ItemStyle.HorizontalAlign = HorizontalAlign.Right;
                        col.DataFormatString = "{0:#,##0}";
                    }
                }
            }

            grid.PreRender += new EventHandler(grid_PreRender);
            grid.DataSource = GridDataHelper.GetData(
                rdpStart.SelectedDate.Value.Date
                , rdpEnd.SelectedDate.Value.Date
                , DBHelper.ToInt32(rcbUOM.SelectedValue)
                , cblEntities.Items);
            grid.Rebind();

            grid.ClientSettings.Scrolling.UseStaticHeaders = true;
            grid.ClientSettings.Scrolling.AllowScroll = true;
            grid.ClientSettings.Scrolling.ScrollHeight = Unit.Empty;
            return grid;
        }

    }
}