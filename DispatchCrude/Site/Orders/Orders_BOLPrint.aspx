﻿<%@ Page Title="Reports" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Orders_BOLPrint.aspx.cs"
    Inherits="DispatchCrude.Site.Orders.Orders_BOLPrint" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="contentMain" ContentPlaceHolderID="MainContent" runat="server">
    <script>
        $("#ctl00_ctl00_EntityCaption").html("Order BOLs");
    </script>
    <div class="tabbable TabRepaint">
        <ul class="nav nav-tabs" id="tabmenu" runat="server">
            <!--Tabs will print here (see code behind)-->
        </ul>
        <div class="tab-content">
            <div class="tab-pane in active">

                <div class="leftpanel">
                    <div class="tabbable">
                        <ul class="nav nav-tabs">
                            <li class="active tab-blue">
                                <a data-toggle="tab" href="#Filters" aria-expanded="true">Filters</a>
                            </li>
                            <li class="tab-green">
                                <a data-toggle="tab" href="#Export" aria-expanded="true">Export</a>
                            </li>
                        </ul>
                        <div id="leftTabs" class="tab-content">
                            <div id="Filters" class="tab-pane active">
                                <asp:Panel ID="panelFilter" runat="server">
                                    <div class="Entry">
                                        <asp:Label runat="server" Text="Order #" AssociatedControlID="txtOrderNum" />
                                        <asp:TextBox CssClass="width100" ID="txtOrderNum" runat="server" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label runat="server" Text="Job #" AssociatedControlID="txtJobNumber" />
                                        <asp:TextBox CssClass="width100" ID="txtJobNumber" runat="server" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label runat="server" Text="Contract #" AssociatedControlID="txtContractNumber" />
                                        <asp:TextBox CssClass="width100" ID="txtContractNumber" runat="server" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblShipper" runat="server" Text="Shipper" AssociatedControlID="ddShipper" />
                                        <asp:DropDownList CssClass="width100 btn-xs" ID="ddShipper" runat="server" DataTextField="Name" DataValueField="ID" DataSourceID="dsShipper" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblCarrier" runat="server" Text="Carrier" AssociatedControlID="ddCarrier" />
                                        <asp:DropDownList CssClass="width100 btn-xs" ID="ddCarrier" runat="server" DataTextField="Name" DataValueField="ID" DataSourceID="dsCarrier" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblDriver" runat="server" Text="Driver" AssociatedControlID="ddDriver" />
                                        <asp:DropDownList CssClass="width100 btn-xs" ID="ddDriver" runat="server" DataTextField="FullName" DataValueField="ID" DataSourceID="dsDriver" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblOrigin" runat="server" Text="Origin" AssociatedControlID="ddOrigin" />
                                        <asp:DropDownList CssClass="width100 btn-xs" ID="ddOrigin" runat="server" DataTextField="Name" DataValueField="ID" DataSourceID="dsOrigin" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblDestination" runat="server" Text="Destination" AssociatedControlID="ddDestination" />
                                        <asp:DropDownList CssClass="width100 btn-xs" ID="ddDestination" runat="server" DataTextField="Name" DataValueField="ID" DataSourceID="dsDestination" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblRejected" runat="server" Text="Rejected Orders" AssociatedControlID="ddlRejected" />
                                        <asp:DropDownList CssClass="width100 btn-xs" ID="ddlRejected" runat="server">
                                            <asp:ListItem Text="(All)" Value="-1" />
                                            <asp:ListItem Text="Only Rejected Orders" Value="1" />
                                            <asp:ListItem Text="Exclude Rejected Orders" Value="0" />
                                        </asp:DropDownList>
                                    </div>
                                    <div class="spacer5px"></div>
                                    <div class="center">
                                        <div class="Entry floatLeft-date-row">
                                            <asp:Label ID="lblStartDate" runat="server" Text="Start Date" AssociatedControlID="rdpStart" CssClass="Entry" />
                                            <telerik:RadDatePicker ID="rdpStart" runat="server" Width="100px">
                                                <DateInput runat="server" DateFormat="M/d/yyyy" />
                                                <DatePopupButton Enabled="true" />
                                            </telerik:RadDatePicker>
                                        </div>
                                        <div class="Entry floatRight-date-row">
                                            <asp:Label ID="lblEndDate" runat="server" Text="End Date" AssociatedControlID="rdpEnd" CssClass="Entry" />
                                            <telerik:RadDatePicker ID="rdpEnd" runat="server" Width="100px">
                                                <DateInput runat="server" DateFormat="M/d/yyyy" />
                                                <DatePopupButton />
                                            </telerik:RadDatePicker>
                                        </div>
                                    </div>
                                    <br /><br /><br />
                                    <div class="spacer10px"></div>
                                    <div class="center">
                                        <asp:Button ID="cmdFilter" runat="server" Text="Refresh" CssClass="btn btn-blue shiny" OnClick="cmdFilter_Click" />
                                    </div>
                                </asp:Panel>
                            </div>
                            <div id="Export" class="tab-pane">
                                <asp:Panel ID="panelExcel" runat="server">
                                    <div class="center">
                                        <asp:Label ID="lblSelectionAction" runat="server" Text="Actions for selected orders:" />
                                    </div>
                                    <div class="spacer10px"></div>
                                    <div class="center">
                                        <asp:Button ID="cmdExportToExcel" runat="server" Text="Export Excel" ClientIDMode="Static" CssClass="btn btn-blue shiny"
                                                    UseSubmitBehavior="false" OnClick="cmdExportToExcel_Click" />
                                        &nbsp;&nbsp;
                                        <asp:Button ID="cmdExportBOLZip" runat="server" Text="Export BOL Zip" ClientIDMode="Static" CssClass="btn btn-blue shiny"
                                                    UseSubmitBehavior="false" OnClick="cmdExportBOLZip_Click" />
                                    </div>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="gridArea" style="height:100%">
                    <telerik:RadWindowManager ID="radWindowManager" runat="server" EnableShadow="true" />
                    <telerik:RadGrid ID="rgMain" runat="server" EnableHeaderContextMenu="False" Height="800" CssClass="GridRepaint" CellSpacing="0" GridLines="None"
                                     AllowSorting="True" ShowGroupPanel="false" AllowFilteringByColumn="false" EnableViewState="true" AllowMultiRowSelection="true"
                                     OnNeedDataSource="grid_NeedDataSource" AllowPaging="True" EnableEmbeddedSkins="true" Skin="Vista" AlternatingItemStyle-BackColor="#dcf2fc"
                                     PageSize='<%# Settings.DefaultPageSize %>'
                                     OnItemDataBound="grid_ItemDataBound" OnItemCreated="grid_ItemCreated" OnDataBound="grid_DataBound" OnItemCommand="grid_ItemCommand">
                        <ClientSettings AllowDragToGroup="false">
                            <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                            <Selecting AllowRowSelect="true" />
                            <ClientEvents />
                        </ClientSettings>
                        <GroupingSettings ShowUnGroupButton="true" CaseSensitive="false" />
                        <MasterTableView AutoGenerateColumns="False" DataKeyNames="ID" CommandItemDisplay="None">
                            <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                            <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                <HeaderStyle Width="20px"></HeaderStyle>
                            </RowIndicatorColumn>
                            <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                <HeaderStyle Width="20px"></HeaderStyle>
                            </ExpandCollapseColumn>
                            <ColumnGroups>
                                <telerik:GridColumnGroup HeaderText="Origin" Name="OriginGroup" HeaderStyle-HorizontalAlign="Center" />
                                <telerik:GridColumnGroup HeaderText="Destination" Name="DestinationGroup" HeaderStyle-HorizontalAlign="Center" />
                                <telerik:GridColumnGroup HeaderText="Reject" Name="RejectGroup" HeaderStyle-HorizontalAlign="Center" />
                            </ColumnGroups>
                            <Columns>
                                <telerik:GridClientSelectColumn UniqueName="ClientSelectColumn" HeaderStyle-Width="40px" />
                                <telerik:GridTemplateColumn UniqueName="PrintColumn" AllowFiltering="false">
                                    <HeaderStyle Width="90px" />
                                    <ItemTemplate>
                                        <asp:Button CssClass="NOAJAX btn btn-blue btn-xs shiny" runat="server" id="cmdExportSingle" Text="Download" OnClick="cmdExportSingle_Click" />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridDateTimeColumn DataField="OrderDate" UniqueName="OrderDate" HeaderText="Date" SortExpression="OrderDate"
                                                            DataFormatString="{0:M/d/yyyy}" ReadOnly="true" ForceExtractValue="Always" FilterControlWidth="80%">
                                    <HeaderStyle Width="100px" />
                                </telerik:GridDateTimeColumn>
                                <telerik:GridBoundColumn DataField="OrderNum" HeaderText="Order #" ReadOnly="true" SortExpression="OrderNum"
                                                         FilterControlWidth="70%" HeaderStyle-Width="80px">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="JobNumber" HeaderText="Job #" ReadOnly="true" SortExpression="JobNumber"
                                                         FilterControlWidth="70%" HeaderStyle-Width="80px">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ContractNumber" HeaderText="Contract #" ReadOnly="true" SortExpression="ContractNumber"
                                                         FilterControlWidth="70%" HeaderStyle-Width="100px">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Customer" UniqueName="Customer" HeaderText="Shipper" SortExpression="Customer"
                                                         ReadOnly="true" FilterControlWidth="70%" ForceExtractValue="Always">
                                    <HeaderStyle Width="160px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Carrier" UniqueName="Carrier" HeaderText="Carrier" SortExpression="Carrier"
                                                         ReadOnly="true" AllowFiltering="true" FilterControlWidth="70%" ForceExtractValue="Always">
                                    <HeaderStyle Width="160px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Driver" UniqueName="Driver" HeaderText="Driver" SortExpression="Driver"
                                                         ReadOnly="true" AllowFiltering="true" FilterControlWidth="70%" ForceExtractValue="Always">
                                    <HeaderStyle Width="175px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Origin" UniqueName="Origin" HeaderText="Origin" SortExpression="Origin"
                                                         ReadOnly="true" AllowFiltering="true" FilterControlWidth="70%" ForceExtractValue="Always" ColumnGroupName="OriginGroup">
                                    <HeaderStyle Width="300px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="OriginBOLNum" HeaderText="BOL #" ReadOnly="true" SortExpression="OriginBOLNum" ColumnGroupName="OriginGroup"
                                                         FilterControlWidth="70%" HeaderStyle-Width="100px">
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn UniqueName="OriginPhotoColumn" AllowFiltering="false" ColumnGroupName="OriginGroup" HeaderImageUrl="~/images/image.png"
                                                            HeaderStyle-HorizontalAlign="Center" HeaderText="Origin Photo">
                                    <HeaderStyle Width="30px" />
                                    <ItemTemplate>
                                        <asp:HiddenField runat="server" Value='<%# Eval("OriginPhotoID") %>' />
                                        <asp:ImageButton CssClass="NOAJAX" runat="server" id="cmdOriginPhoto" ImageUrl="~/images/image.png"
                                                         OnClick="cmdOriginPhoto_Click" ToolTip="Download Origin Photo" />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>

                                <telerik:GridTemplateColumn UniqueName="OriginPhoto2Column" AllowFiltering="false" ColumnGroupName="OriginGroup" HeaderImageUrl="~/images/image.png"
                                                            HeaderStyle-HorizontalAlign="Center" HeaderText="Origin Photo #2">
                                    <HeaderStyle Width="30px" />
                                    <ItemTemplate>
                                        <asp:HiddenField runat="server" Value='<%# Eval("OriginPhoto2ID") %>' />
                                        <asp:ImageButton CssClass="NOAJAX" runat="server" id="cmdOriginPhoto2" ImageUrl="~/images/image.png"
                                                         OnClick="cmdOriginPhoto2_Click" ToolTip="Download Origin #2 Photo" />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>

                                <telerik:GridTemplateColumn UniqueName="OriginArrivePhotoColumn" AllowFiltering="false" ColumnGroupName="OriginGroup" HeaderImageUrl="~/images/image.png"
                                                            HeaderStyle-HorizontalAlign="Center" HeaderText="Origin Arrive Photo">
                                    <HeaderStyle Width="30px" />
                                    <ItemTemplate>
                                        <asp:HiddenField runat="server" Value='<%# Eval("OriginArrivePhotoID") %>' />
                                        <asp:ImageButton CssClass="NOAJAX" runat="server" id="cmdOriginArrivePhoto" ImageUrl="~/images/image.png"
                                                         OnClick="cmdOriginArrivePhoto_Click" ToolTip="Download Origin Arrive Photo" />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn UniqueName="OriginDepartPhotoColumn" AllowFiltering="false" ColumnGroupName="OriginGroup" HeaderImageUrl="~/images/image.png"
                                                            HeaderStyle-HorizontalAlign="Center" HeaderText="Origin Depart Photo">
                                    <HeaderStyle Width="30px" />
                                    <ItemTemplate>
                                        <asp:HiddenField runat="server" Value='<%# Eval("OriginDepartPhotoID") %>' />
                                        <asp:ImageButton CssClass="NOAJAX" runat="server" id="cmdOriginDepartPhoto" ImageUrl="~/images/image.png"
                                                         OnClick="cmdOriginDepartPhoto_Click" ToolTip="Download Origin Depart Photo" />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>

                                <telerik:GridBoundColumn DataField="Destination" UniqueName="Destination" HeaderText="Destination" SortExpression="Destination"
                                                         ReadOnly="true" AllowFiltering="true" FilterControlWidth="70%" ForceExtractValue="Always" ColumnGroupName="DestinationGroup">
                                    <HeaderStyle Width="300px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="DestBOLNum" HeaderText="BOL #" ReadOnly="true" SortExpression="DestBOLNum" ColumnGroupName="DestinationGroup"
                                                         FilterControlWidth="70%" HeaderStyle-Width="100px">
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn UniqueName="DestPhotoColumn" AllowFiltering="false" ColumnGroupName="DestinationGroup" HeaderImageUrl="~/images/image.png"
                                                            HeaderStyle-HorizontalAlign="Center" HeaderText="Destination Photo">
                                    <HeaderStyle Width="30px" />
                                    <ItemTemplate>
                                        <asp:HiddenField runat="server" Value='<%# Eval("DestPhotoID") %>' />
                                        <asp:ImageButton CssClass="NOAJAX" runat="server" id="cmdDestPhoto" ImageUrl="~/images/image.png"
                                                         OnClick="cmdDestPhoto_Click" ToolTip="Download Destination Photo" />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>

                                <telerik:GridTemplateColumn UniqueName="DestPhoto2Column" AllowFiltering="false" ColumnGroupName="DestinationGroup" HeaderImageUrl="~/images/image.png"
                                                            HeaderStyle-HorizontalAlign="Center" HeaderText="Destination Photo #2">
                                    <HeaderStyle Width="30px" />
                                    <ItemTemplate>
                                        <asp:HiddenField runat="server" Value='<%# Eval("DestPhoto2ID") %>' />
                                        <asp:ImageButton CssClass="NOAJAX" runat="server" id="cmdDestPhoto2" ImageUrl="~/images/image.png"
                                                         OnClick="cmdDestPhoto2_Click" ToolTip="Download Destination Photo #2" />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>

                                <telerik:GridTemplateColumn UniqueName="DestArrivePhotoColumn" AllowFiltering="false" ColumnGroupName="DestinationGroup" HeaderImageUrl="~/images/image.png"
                                                            HeaderStyle-HorizontalAlign="Center" HeaderText="Destination Arrive Photo">
                                    <HeaderStyle Width="30px" />
                                    <ItemTemplate>
                                        <asp:HiddenField runat="server" Value='<%# Eval("DestArrivePhotoID") %>' />
                                        <asp:ImageButton CssClass="NOAJAX" runat="server" id="cmdDestArrivePhoto" ImageUrl="~/images/image.png"
                                                         OnClick="cmdDestArrivePhoto_Click" ToolTip="Download Destination Arrive Photo" />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn UniqueName="DestDepartPhotoColumn" AllowFiltering="false" ColumnGroupName="DestinationGroup" HeaderImageUrl="~/images/image.png"
                                                            HeaderStyle-HorizontalAlign="Center" HeaderText="Destination Depart Photo">
                                    <HeaderStyle Width="30px" />
                                    <ItemTemplate>
                                        <asp:HiddenField runat="server" Value='<%# Eval("DestDepartPhotoID") %>' />
                                        <asp:ImageButton CssClass="NOAJAX" runat="server" id="cmdDestDepartPhoto" ImageUrl="~/images/image.png"
                                                         OnClick="cmdDestDepartPhoto_Click" ToolTip="Download Destination Depart Photo" />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>

                                <telerik:GridCheckBoxColumn DataField="Rejected" HeaderText="Rejected?" SortExpression="Rejected" UniqueName="Rejected" AllowFiltering="true"
                                                            ReadOnly="true" HeaderStyle-Width="75px" ColumnGroupName="RejectGroup" />
                                <telerik:GridTemplateColumn UniqueName="RejectPhotoColumn" AllowFiltering="false" ColumnGroupName="RejectGroup" HeaderImageUrl="~/images/image.png"
                                                            HeaderStyle-HorizontalAlign="Center" HeaderText="Reject Photo">
                                    <HeaderStyle Width="30px" />
                                    <ItemTemplate>
                                        <asp:HiddenField runat="server" Value='<%# Eval("RejectPhotoID") %>' />
                                        <asp:ImageButton CssClass="NOAJAX" runat="server" id="cmdRejectPhoto" ImageUrl="~/images/image.png"
                                                         OnClick="cmdRejectPhoto_Click" ToolTip="Download Reject Photo" />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="ID" UniqueName="ID" Visible="false" DataType="System.Int32" ForceExtractValue="Always" />
                            </Columns>
                            <PagerStyle AlwaysVisible="True" />
                        </MasterTableView>
                        <HeaderStyle Wrap="False" />
                        <PagerStyle AlwaysVisible="True" />
                        <FilterMenu EnableImageSprites="False" />
                    </telerik:RadGrid>
                </div>

                <blac:DBDataSource ID="dsShipper" runat="server" SelectCommand="SELECT ID, Name = dbo.fnNameWithDeleted(Name, DeleteDateUTC), Active = dbo.fnIsActive(DeleteDateUTC) FROM dbo.tblCustomer UNION SELECT 0, '(All)', 1 ORDER BY Active DESC, Name" />
                <blac:DBDataSource ID="dsCarrier" runat="server" SelectCommand="SELECT ID, Name = dbo.fnNameWithDeleted(Name, DeleteDateUTC), Active = dbo.fnIsActive(DeleteDateUTC) FROM dbo.tblCarrier UNION SELECT 0, '(All)', 1 ORDER BY Active DESC, Name" />
                <blac:DBDataSource ID="dsDriver" runat="server" SelectCommand="SELECT ID, FullName = dbo.fnNameWithDeleted((FirstName + ' ' + LastName), DeleteDateUTC), Active = dbo.fnIsActive(DeleteDateUTC) FROM dbo.tblDriver UNION SELECT 0, '(All)', 1 ORDER BY Active DESC, FullName" />
                <blac:DBDataSource ID="dsOrigin" runat="server" SelectCommand="SELECT ID, Name = dbo.fnNameWithDeleted(Name, DeleteDateUTC), Active = dbo.fnIsActive(DeleteDateUTC) FROM dbo.tblOrigin UNION SELECT 0, '(All)', 1 ORDER BY Active DESC, Name" />
                <blac:DBDataSource ID="dsDestination" runat="server" SelectCommand="SELECT ID, Name = dbo.fnNameWithDeleted(Name, DeleteDateUTC), Active = dbo.fnIsActive(DeleteDateUTC) FROM dbo.tblDestination UNION SELECT 0, '(All)', 1 ORDER BY Active DESC, Name" />
            </div>
        </div>
    </div>
</asp:Content>