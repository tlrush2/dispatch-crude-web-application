﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CtrlGaugerOrderEdit.ascx.cs" Inherits="DispatchCrude.Orders.CtrlGaugerOrderEdit" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<style type="text/css">
    .rgEditForm {
            width: auto !important;
    }
    .rgEditForm > div + div,
    .RadGrid .rgEditForm {
            height: auto !important;
    }
    .rgEditForm > div > table{
            height: 100%;
    }
    .rgEditForm > div > table > tbody > tr > td{
            padding: 4px 10px;
    }
    .H2S
    {
        color: red;
    }
    .Deleted
    {
        color: gray;
    }
</style>
<telerik:RadScriptBlock runat="server" >
    <script type="text/javascript">
        function validateItemSpecified(sender, args) {
            //invalid if first character is a "("
            args.IsValid = !args.Value.startsWith("(");
        };
        function validateGaugerOrderDispatchedStatus(sender, args) {
            debugger;
            args.IsValid = true;    // default value
            var status = $find("<%= rcbGaugerStatus.ClientID %>").get_value();
            if (status != 1) { // not REQUESTED
                var gaugerID = $find("<%= rcbGauger.ClientID %>").get_value();
                args.IsValid = gaugerID > 0;
            }
        };
    </script>
</telerik:RadScriptBlock>
<asp:Table id="tblMain" runat="server" border="0" style="border-collapse: collapse">
    <asp:TableRow runat="server" >
        <asp:TableCell runat="server" ColumnSpan="2" >
            <asp:ValidationSummary ID="validationSummaryMain" runat="server" DisplayMode="BulletList" Enabled="true" EnableClientScript="true"
                ValidationGroup="GaugerOrderEdit"
                CssClass="NullValidator" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow runat="server" >
        <asp:TableCell runat="server" >
            <asp:Table runat="server" id="tblLeft" border="0" style="margin-right: 0px">
                <asp:TableRow runat="server" >
                    <asp:TableCell runat="server" >
                        Priority/Status:
                    </asp:TableCell>
                    <asp:TableCell runat="server" >
                        <asp:HiddenField ID="hfID" runat="server" />
                        <blac:PriorityDropDownList CssClass="btn-xs" ID="ddPriority" runat="server" Width="60px" />&nbsp;
                        <telerik:RadComboBox ID="rcbGaugerStatus" runat="server" Width="175px"  
                                DataSourceID="dsGaugerStatus" DataTextField="Name" DataValueField="ID" 
                                CausesValidation="false" ValidationGroup="GaugerOrderEdit"
                                AutoPostBack="true" OnSelectedIndexChanged="rcbGaugerStatus_SelectedIndexChanged" />
                                <%--SelectedValue='<%# Bind("StatusID") %>' />--%>
                        <asp:CustomValidator ID="cvStatusDispatched" runat="server" 
                            OnServerValidate="cvStatus_DispatchedValidate" ClientValidationFunction="validateGaugerOrderDispatchedStatus" 
                            ControlToValidate="rcbGaugerStatus" ValidationGroup="GaugerOrderEdit"
                            Text="&nbsp;!" ErrorMessage="Gauger is required for the DISPATCHED gauger status" CssClass="NullValidator" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow runat="server" >
                    <asp:TableCell runat="server" >
                        Origin:
                    </asp:TableCell>
                    <asp:TableCell runat="server" >
                        <telerik:RadComboBox runat="server" ID="rcbOrigin" AllowCustomText="false" Filter="Contains"
                            DataSourceID="dsOrigin" DataTextField="LeaseNum_Name" DataValueField="ID" 
                            CausesValidation="false" ValidationGroup="GaugerOrderEdit"
                            AutoPostBack="true" OnItemDataBound="rcbOrigin_ItemDataBound" OnSelectedIndexChanged="rcbOrigin_SelectedIndexChanged"  
                            Width="250px" DropDownWidth="275" >
                            <HeaderTemplate>
                                <ul>
                                    <li class="col oCol1" style="width:100% !important;">Name</li>
                                </ul>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <ul>
                                    <li class="col oCol1" style="width:100% !important;">
                                        <%# DataBinder.Eval(Container.DataItem, "Name") %></li>
                                </ul>
                            </ItemTemplate>
                        </telerik:RadComboBox>
                        <asp:CustomValidator ID="cvOrigin" runat="server" ControlToValidate="rcbOrigin"
                            ValidateEmptyText="true" ClientValidationFunction="validateItemSpecified" ValidationGroup="GaugerOrderEdit"
                            Text="&nbsp;!" ErrorMessage="An Origin must be specified" CssClass="NullValidator" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow runat="server" >
                    <asp:TableCell runat="server" >
                        <asp:Label ID="lblCustomer" runat="server" Text="Shipper:" />
                    </asp:TableCell>
                    <asp:TableCell runat="server" >
                        <telerik:RadComboBox ID="rcbCustomer" runat="server" Width="250px" 
                            DataSourceID="dsCustomer" DataTextField="Name" DataValueField="ID" ValidationGroup="GaugerOrderEdit" />
                            <%--SelectedValue='<%# Bind("CustomerID") %>' />--%>
                        <asp:CustomValidator ID="cvCustomer" runat="server" ControlToValidate="rcbCustomer" ValidationGroup="GaugerOrderEdit"
                            ValidateEmptyText="true" ClientValidationFunction="validateItemSpecified"
                            Text="*" ErrorMessage="A valid Shipper must be selected" CssClass="NullValidator" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow runat="server" >
                    <asp:TableCell runat="server" >
                        Product:
                    </asp:TableCell>
                    <asp:TableCell runat="server" >
                        <telerik:RadComboBox id="rcbProduct" runat="server" Width="250px"
                            DataSourceID="dsProduct" DataTextField="Name" DataValueField="ID" 
                            CausesValidation="true" ValidationGroup="GaugerOrderEdit"
                            AutoPostBack="true" OnSelectedIndexChanged="rcbProduct_SelectedIndexChanged" />
                            <%--SelectedValue='<%# Bind("ProductID") %>' />--%>
                        <asp:CustomValidator ID="cvProduct" runat="server" ControlToValidate="rcbProduct" ValidationGroup="GaugerOrderEdit"
                            ValidateEmptyText="true" ClientValidationFunction="validateItemSpecified"
                            Text="&nbsp;!" ErrorMessage="A Product must be specified" CssClass="NullValidator" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow runat="server" >
                    <asp:TableCell runat="server" >
                        Tank ID:
                    </asp:TableCell>
                    <asp:TableCell runat="server" >
                        <telerik:RadComboBox id="rcbOriginTank" runat="server" Filter="Contains" 
                            DataSourceID="dsOriginTank" DataTextField="TankNum" DataValueField="ID" 
                            CausesValidation="false" ValidationGroup="GaugerOrderEdit"
                            AutoPostBack="true" OnItemDataBound="rcbOriginTank_ItemDataBound" OnSelectedIndexChanged="rcbOriginTank_SelectedIndexChanged" 
                            Width="250px" >
                            <HeaderTemplate>
                                <ul>
                                    <li class="col otCol1">Tank #</li>
                                    <li class="col otCol2">Lease #/Name</li>
                                </ul>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <ul>
                                    <li class="col otCol1">
                                        <%# DataBinder.Eval(Container.DataItem, "TankNum") %></li>
                                    <li class="col otCol2">
                                        <%# DataBinder.Eval(Container.DataItem, "LeaseNum_Name") %></li>
                                </ul>
                            </ItemTemplate>
                        </telerik:RadComboBox>
                        <asp:CustomValidator ID="cvOriginTank" runat="server" ControlToValidate="rcbOriginTank" ValidationGroup="GaugerOrderEdit"
                            ValidateEmptyText="true" 
                            ClientValidationFunction="validateItemSpecified" 
                            Text="*" ErrorMessage="Tank selection must be made" CssClass="NullValidator" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow runat="server" >
                    <asp:TableCell runat="server" >
                        Destination:
                    </asp:TableCell>
                    <asp:TableCell runat="server" >
                        <telerik:RadComboBox runat="server" ID="rcbDestination" 
                            DataSourceID="dsDestination" DataTextField="FullName" DataValueField="ID" 
                            CausesValidation="false" ValidationGroup="GaugerOrderEdit"
                            AutoPostBack="true" 
                            Width="250px" DropDownWidth="275" >
                            <HeaderTemplate>
                                <ul>
                                    <li class="col dCol1">Destination</li>
                                </ul>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <ul>
                                    <li class="col dCol1">
                                        <%# DataBinder.Eval(Container.DataItem, "Name") %></li>
                                </ul>
                            </ItemTemplate>
                        </telerik:RadComboBox>
                        <asp:CustomValidator ID="cvDestination" runat="server" ControlToValidate="rcbDestination" ValidationGroup="GaugerOrderEdit"
                            ValidateEmptyText="true" ClientValidationFunction="validateItemSpecified"
                            Text="&nbsp;!" ErrorMessage="A Destination must be specified" CssClass="NullValidator" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow runat="server" >
                    <asp:TableCell runat="server" >
                        Gauger:
                    </asp:TableCell>
                    <asp:TableCell runat="server" >
                        <telerik:RadComboBox ID="rcbGauger" runat="server" Width="250px" 
                            DataSourceID="dsGauger" DataTextField="FullName" DataValueField="ID" 
                            CausesValidation="true" ValidationGroup="GaugerOrderEdit"
                            AutoPostBack="true" OnSelectedIndexChanged="rcbGauger_SelectedIndexChanged" />
                            <%--SelectedValue='<%# Bind("GaugerID") %>' />--%>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
        <asp:TableCell runat="server" >
            <asp:Table runat="server" id="tblRight" BorderStyle="None" >
                <asp:TableRow runat="server" >
                    <asp:TableCell runat="server" >
                        Due Date:
                    </asp:TableCell>
                    <asp:TableCell runat="server" >
                        <telerik:RadDatePicker ID="rdpDueDate" runat="server" Width="150px" />
                            <%--DateInput-DateFormat="M/d/yy" DbSelectedDate='<%# Bind("DueDate") %>' />--%>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow runat="server" >
                    <asp:TableCell runat="server" >
                        Ticket Type:
                    </asp:TableCell>
                    <asp:TableCell runat="server" >
                        <telerik:RadComboBox ID="rcbTicketType" runat="server" Width="150px" 
                            DataSourceID="dsTicketType" DataTextField="Name" DataValueField="ID" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow runat="server" >
        <asp:TableCell runat="server" ColumnSpan="2" >
            <asp:Table runat="server">
                <asp:TableRow runat="server">
                    <asp:TableCell runat="server" Width="45%">
                        <asp:Table runat="server">
                            <asp:TableRow runat="server">
                                <asp:TableCell runat="server">
                                    Shipper PO:
                                </asp:TableCell>
                                <asp:TableCell runat="server">
                                    <asp:TextBox id="txtDispatchConfirmNum" runat="server" MaxLength="30"  ValidationGroup="OrderEdit" />
                                    <asp:RequiredFieldValidator ID="rfvDispatchConfirmNum" runat="server" 
                                        ControlToValidate="txtDispatchConfirmNum" ValidationGroup="OrderEdit" 
                                        Text="!" ErrorMessage="Dispatch Confirmation is required for DISPATCHED orders" CssClass="NullValidator" />
                                    <asp:CustomValidator ID="cvDispatchConfirmNum" runat="server" ControlToValidate="txtDispatchConfirmNum"
                                        ValidationGroup="OrderEdit"
                                        OnServerValidate="cvDispatchConfirmNum_ServerValidate" Display="Dynamic" 
                                        CssClass="NullValidator" Text="*" ErrorMessage="Dispatch Confirm # already assigned to this Shipper" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow runat="server">
                                <asp:TableCell runat="server">
                                    Job #:
                                </asp:TableCell>
                                <asp:TableCell runat="server">
                                    <asp:TextBox id="txtJobNumber" runat="server" MaxLength="20" />                                    
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                    <asp:TableCell runat="server" Width="55%">
                        <asp:Table runat="server">
                            <asp:TableRow runat="server">
                                <asp:TableCell runat="server">
                                    Dispatch Notes:
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow runat="server">
                                <asp:TableCell runat="server">
                                    <asp:TextBox id="txtDispatchNotes" runat="server" Width="450px" Height="50px" MaxLength="500" TextMode="MultiLine" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow runat="server" >
        <asp:TableCell runat="server" style="text-align:left" ColumnSpan="2">
            <asp:Button ID="cmdUpdate" runat="server" Text="Update" CssClass="btn btn-blue shiny" CommandName="Update" 
                CausesValidation="true" ValidationGroup="GaugerOrderEdit"
                OnClick="UpdateDataItem" 
                Visible='<%# !(DataItem is Telerik.Web.UI.GridInsertionObject) %>'>
            </asp:Button>
            &nbsp;
            <asp:Button ID="cmdCancel" runat="server" Text="Cancel" CssClass="btn btn-blue shiny" CommandName="Cancel" CausesValidation="False" ValidationGroup="GaugerOrderEdit" />
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
    <blac:DBDataSource ID="dsOrigin" runat="server" 
        SelectCommand="SELECT ID, LeaseNum = isnull(LeaseNum, 'N/A'), Name, LeaseNum_Name = isnull(LeaseNum + ' - ', '') + Name, H2S 
                        FROM dbo.viewOrigin 
                        WHERE GaugerTicketTypeID IS NOT NULL AND HasTanks = 1 AND Active=1 AND DeleteDateUTC IS NULL
                        UNION SELECT 0, '(Select Origin)', '(Select Origin)', '(Select Origin)', 0 
                        ORDER BY LeaseNum, Name">
    </blac:DBDataSource>
<blac:DBDataSource ID="dsProduct" runat="server" SelectCommand="SELECT ID, Name, ShortName FROM dbo.tblProduct WHERE ID IN (SELECT ProductID FROM tblOriginProducts WHERE OriginID = @OriginID) UNION SELECT 0, '(Select Product)', '(Select Product)' ORDER BY Name" >
    <SelectParameters>
        <asp:ControlParameter Name="OriginID" ControlID="rcbOrigin" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
    </SelectParameters>
</blac:DBDataSource>
<blac:DBDataSource ID="dsOriginTank" runat="server"
    SelectCommand="SELECT ID, OriginID, TankNum, LeaseNum_Name = isnull(OriginLeaseNum+'-'+Origin, Origin) FROM viewOriginTank WHERE Unstrapped = 0 AND ForGauger=1 AND (@OriginID = 0 OR OriginID=@OriginID) AND DeleteDateUTC IS NULL UNION SELECT NULL, 0, '(Select Tank)', '(Select Tank)' ORDER BY TankNum" >
    <SelectParameters>
        <asp:ControlParameter Name="OriginID" ControlID="rcbOrigin" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
    </SelectParameters>
</blac:DBDataSource>
<blac:DBDataSource ID="dsDestination" runat="server" 
    SelectCommand="SELECT ID, Name, FullName, DestinationType FROM dbo.fnRetrieveEligibleDestinations(@OriginID, @ShipperID, @ProductID, 0) UNION SELECT 0, '(Select Destination)', '(Select Destination)', NULL ORDER BY FullName">
    <SelectParameters>
        <asp:ControlParameter Name="OriginID" ControlID="rcbOrigin" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
        <asp:ControlParameter Name="ShipperID" ControlID="rcbCustomer" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
        <asp:ControlParameter Name="ProductID" ControlID="rcbProduct" PropertyName="SelectedValue" Type="Int32" DefaultValue="1" />
    </SelectParameters>
</blac:DBDataSource>
<blac:DBDataSource ID="dsCustomer" runat="server" SelectCommand="SELECT ID, Name FROM dbo.tblCustomer WHERE ID IN (SELECT CustomerID FROM tblOriginCustomers WHERE OriginID=@OriginID) UNION SELECT NULL, '(Select Shipper)' ORDER BY Name" >
    <SelectParameters>
        <asp:ControlParameter Name="OriginID" ControlID="rcbOrigin" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
    </SelectParameters>
</blac:DBDataSource>
<blac:DBDataSource ID="dsGauger" runat="server" 
    SelectCommand="SELECT ID, FullName FROM dbo.viewGauger WHERE DeleteDateUTC IS NULL UNION SELECT 0, '(Select)' ORDER BY FullName" >
</blac:DBDataSource >
<blac:DBDataSource ID="dsTicketType" runat="server" SelectCommand="SELECT ID, Name FROM dbo.tblGaugerTicketType ORDER BY Name" />
<blac:DBDataSource ID="dsPriority" runat="server" SelectCommand="SELECT ID, PriorityNum FROM tblPriority ORDER BY PriorityNum" />
<blac:DBDataSource ID="dsGaugerStatus" runat="server" SelectCommand="SELECT ID, Name FROM tblGaugerOrderStatus ORDER BY ID" />
<blac:DBDataSource ID="dsUom" runat="server" SelectCommand="SELECT ID, Name FROM tblUom UNION ALL SELECT 0, '(Select UOM)' ORDER BY Name" />
