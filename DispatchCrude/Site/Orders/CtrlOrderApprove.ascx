﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CtrlOrderApprove.ascx.cs" Inherits="DispatchCrude.Orders.CtrlOrderApprove" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<%-- ------------------------------------------------------------------------------------------ --%>
<%-- Author: Kevin Alons                                                                        --%>
<%-- Date:                                                                                      --%>
<%--                                                                                            --%>
<%-- Summary: Popup approval page to edit overrides for an order                                --%>
<%-- Notes:                                                                                     --%>
<%--   * Data is retrieved from the Orders_Approve page (spOrderApprovalSource)                 --%>
<%--                                                                                            --%>
<%-- NAME           DATE        UPDATES                                                         --%>
<%-- =============  ==========  ============================================================    --%>
<%-- J Engler       10/23/2015  Added the transfer % override for order transfers               --%>
<%-- J Engler       12/21/2015  Added overrides for min settlement units (shipper and carrier)  --%>
<%--                                                                                            --%>
<%-- ------------------------------------------------------------------------------------------ --%>

<style type="text/css">
    .rgEditForm {
            width: auto !important;
    }
    .rgEditForm > div + div,
    .RadGrid .rgEditForm {
            height: auto !important;
    }
    .rgEditForm > div > table{
            height: 100%;
    }
    .rgEditForm > div > table > tbody > tr > td{
            padding: 4px 10px;
    }
</style>
<asp:Table id="tblMain" runat="server" border="0" style="border-collapse: collapse">
    <asp:TableRow runat="server" >
        <asp:TableCell runat="server" ColumnSpan="4" >
            <asp:ValidationSummary ID="validationSummaryMain" runat="server" DisplayMode="BulletList" Enabled="true" EnableClientScript="true"
                ValidationGroup="OrderApprove"
                CssClass="NullValidator" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow runat="server" >
        <asp:TableCell runat="server" >
            Rate Miles:
        </asp:TableCell>
        <asp:TableCell runat="server" >
            <asp:HiddenField ID="hfID" runat="server" />
            <asp:Literal ID="litActualMiles" runat="server" />
        </asp:TableCell>
        <asp:TableCell runat="server" >
            Override:
        </asp:TableCell>
        <asp:TableCell runat="server" >
            <telerik:RadNumericTextBox ID="rntxActualMiles" runat="server" DataType="Int32" NumberFormat-DecimalDigits="0" Width="80px" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow runat="server" >
        <asp:TableCell runat="server" >
            Origin Minutes:
        </asp:TableCell>
        <asp:TableCell runat="server" >
            <asp:Literal ID="litOriginMin" runat="server" />
        </asp:TableCell>
        <asp:TableCell runat="server" >
            Override:
        </asp:TableCell>
        <asp:TableCell runat="server" >
            <telerik:RadNumericTextBox ID="rntxOriginMin" runat="server" DataType="Int32" NumberFormat-DecimalDigits="0" Width="80px" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow runat="server" >
        <asp:TableCell runat="server" >
            Destination Minutes:
        </asp:TableCell>
        <asp:TableCell runat="server" >
            <asp:Literal ID="litDestMin" runat="server" />
        </asp:TableCell>
        <asp:TableCell runat="server" >
            Override:
        </asp:TableCell>
        <asp:TableCell runat="server" >
            <telerik:RadNumericTextBox ID="rntxDestMin" runat="server" DataType="Int32" NumberFormat-DecimalDigits="0" Width="80px" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow runat="server" >
        <asp:TableCell runat="server" >
            Origin Chain-Up?:
        </asp:TableCell>
        <asp:TableCell runat="server" >
            <asp:CheckBox ID="chkOriginChainup" runat="server" Enabled ="false" />
        </asp:TableCell>
        <asp:TableCell runat="server" >
            Omit?:
        </asp:TableCell>
        <asp:TableCell runat="server" >
            <asp:CheckBox ID="chkOverrideOriginChainup" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow runat="server" >
        <asp:TableCell runat="server" >
            Destination Chain-Up?:
        </asp:TableCell>
        <asp:TableCell runat="server" >
            <asp:CheckBox ID="chkDestChainup" runat="server" Enabled ="false" />
        </asp:TableCell>
        <asp:TableCell runat="server" >
            Omit?:
        </asp:TableCell>
        <asp:TableCell runat="server" >
            <asp:CheckBox ID="chkOverrideDestChainup" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow runat="server" >
        <asp:TableCell runat="server" >
            H2S?:
        </asp:TableCell>
        <asp:TableCell runat="server" >
            <asp:CheckBox ID="chkH2S" runat="server" Enabled ="false" />
        </asp:TableCell>
        <asp:TableCell runat="server" >
            Omit H2S?:
        </asp:TableCell>
        <asp:TableCell runat="server" >
            <asp:CheckBox ID="chkOverrideH2S" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow runat="server" >
        <asp:TableCell runat="server" >
            Rerouted?:
        </asp:TableCell>
        <asp:TableCell runat="server" >
            <asp:CheckBox ID="chkRerouted" runat="server" Enabled ="false" />
        </asp:TableCell>
        <asp:TableCell runat="server" >
            Omit Reroute?:
        </asp:TableCell>
        <asp:TableCell runat="server" >
            <asp:CheckBox ID="chkOverrideReroute" runat="server" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow runat="server" >
        <asp:TableCell runat="server" >
            Transfer Percent:
        </asp:TableCell>
        <asp:TableCell runat="server" >
            <asp:Literal ID="litTransferPercentComplete" runat="server" />
        </asp:TableCell>
        <asp:TableCell runat="server" >
            Override:
        </asp:TableCell>
        <asp:TableCell runat="server" >
            <telerik:RadNumericTextBox ID="rntxTransferPercentComplete" runat="server" DataType="Int32" NumberFormat-DecimalDigits="0" Width="80px" />
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow runat="server" >
        <asp:TableCell runat="server" >
            Carrier Min Settlement (<asp:Literal ID="litCarrierMinSettlementUOM" runat="server" />):
        </asp:TableCell>
        <asp:TableCell runat="server" >
            <asp:Literal ID="litCarrierMinSettlementUnits" runat="server" />
        </asp:TableCell>
        <asp:TableCell runat="server" >
            Override:
        </asp:TableCell>
        <asp:TableCell runat="server" >
            <telerik:RadNumericTextBox ID="rntxCarrierMinSettlementUnits" runat="server" DataType="Decimal" NumberFormat-DecimalDigits="3" Width="80px" />
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow runat="server" >
        <asp:TableCell runat="server" >
            Shipper Min Settlement (<asp:Literal ID="litShipperMinSettlementUOM" runat="server" />):
        </asp:TableCell>
        <asp:TableCell runat="server" >
            <asp:Literal ID="litShipperMinSettlementUnits" runat="server" />
        </asp:TableCell>
        <asp:TableCell runat="server" >
            Override:
        </asp:TableCell>
        <asp:TableCell runat="server" >
            <telerik:RadNumericTextBox ID="rntxShipperMinSettlementUnits" runat="server" DataType="Decimal" NumberFormat-DecimalDigits="3" Width="80px" />
        </asp:TableCell>
    </asp:TableRow>

    
</asp:Table>
<asp:Table id="tblMain2" runat="server" border="0" style="border-collapse: collapse">
    <asp:TableRow>
        <asp:TableCell runat="server" >
            Approval Notes:
        </asp:TableCell>
        <asp:TableCell runat="server">
            <asp:TextBox id="txtApprovalNotes" runat="server" Height="50px" Width="305px" MaxLength="500" TextMode="MultiLine" />
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow runat="server" >
        <asp:TableCell runat="server" style="text-align:left" ColumnSpan="4">
            <asp:Button ID="cmdApprove" runat="server" Text="Approve" CssClass="btn btn-blue shiny" 
                CommandName="Approve" CausesValidation="true" ValidationGroup="OrderApprove"
                OnClick="ApproveDataItem" />
            &nbsp;
            <asp:Button ID="cmdUpdate" runat="server" Text="Update" CssClass="btn btn-blue shiny" 
                CommandName="Update" CausesValidation="true" ValidationGroup="OrderApprove"
                OnClick="UpdateDataItem" />
            &nbsp;
            <asp:Button ID="cmdCancel" runat="server" Text="Cancel" CssClass="btn btn-blue shiny" 
                CommandName="Cancel" CausesValidation="False" ValidationGroup="OrderApprove" />
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>