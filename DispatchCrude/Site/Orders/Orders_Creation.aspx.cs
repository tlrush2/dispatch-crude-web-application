﻿using System;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using Telerik.Web.UI;
using System.Web.UI.WebControls;
using DispatchCrude.App_Code;
using DispatchCrude.Core;
using DispatchCrude.Extensions;
using DispatchCrude.Models;
using AlonsIT;

namespace DispatchCrude.Site.Orders
{
    public partial class Orders_Creation : RadAjaxPage
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            //These two statements are required for personalized grid layout 
            //The Key value must be unique across all site pages
            rpmMain.StorageProviderKey = "TruckCreate_rgMain";
            rpmMain.StorageProvider = new DbStorageProvider();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ConfigureAjax(!Core.Settings.SettingsID.DisableAJAX.AsBool());

            //Print tab navigation html to page
            tabmenu.InnerHtml = NavigationHelper.PrintTabArray(NavigationHelper.TabSet_Dispatch, "Tab_OrderCreate").ToString();

            //Print button navigation html to page
            speedbuttons.InnerHtml = NavigationHelper.PrintButtonArray(NavigationHelper.ButtonSet_TabOrderCreate, "Button_TruckOrders").ToString();

            // Disable contract # (lookup only)
            txtContractNumber.ReadOnly = true;                        
            txtContractNumber.BackColor = ColorTranslator.FromHtml("#f0f0f0");

            if (!IsPostBack)
            {
                this.ddlPriority.SelectedValue = "3";  // default value is "Low" priority

                //This is setting the initially selected date (on load) to "today" plus the number of days in "AddDays()"
                this.rdpDueDate.SelectedDate = DateTime.Now.Date.AddDays(1);
                this.rdpDueDate.MinDate = DateTime.Now.Date;
                this.rdpDueDate.Calendar.ShowRowHeaders = false;

                // default the Region to the default user profile (if available and valid)
                int regionID;
                if (Context.Profile.GetPropertyValue("RegionID") != null && (regionID = DBHelper.ToInt32(Context.Profile.GetPropertyValue("RegionID"))) != 0)
                {
                    this.rcbRegion.DataBind();
                    RadComboBoxHelper.SetSelectedValue(this.rcbRegion, regionID);
                    
                    // DCWEB-1027: if this user has a specific Profile Region specified, and the site is restricting users to that region, make the control readonly
                    if (regionID > 0 && Settings.SettingsID.RestrictUserToProfileRegion.AsBool())
                        rcbRegion.Enabled = false;
                }

                //Default the Terminal to the setting in the current user's profile (if available and valid)
                int terminalID;
                if (Context.Profile.GetPropertyValue("TerminalID") != null && (terminalID = DBHelper.ToInt32(Context.Profile.GetPropertyValue("TerminalID"))) != 0)
                {
                    this.rcbTerminal.DataBind();
                    RadComboBoxHelper.SetSelectedValue(this.rcbTerminal, terminalID);

                    //If the user's terminal is set, filtering is assumed to be "ON" so disable the terminal selection box
                    if (terminalID > 0)
                        rcbTerminal.Enabled = false;
                }

                // if the user has a persisted grid layout value, load it now
                if (DbStorageProvider.HasStateInStorage(rpmMain.StorageProviderKey)) rpmMain.LoadState();

                //Check system setting for display of producer label
                using (SSDB ssdb = new SSDB())
                {
                    if (Settings.SettingsID.DisplayProducerOnTruckOrdersPage.AsBool())
                    {
                        lblProducer.Visible = true;
                        txtProducerName.Visible = true;
                        txtProducerName.ReadOnly = true;                        
                        txtProducerName.BackColor = ColorTranslator.FromHtml("#f0f0f0");
                    }
                    else
                    {
                        lblProducer.Visible = false;
                        txtProducerName.Visible = false;
                    }                    
                }

                //Check system setting for display of Rate miles label
                using (SSDB ssdb = new SSDB())
                {
                    if (Settings.SettingsID.DisplayRateMilesOnTruckOrdersPage.AsBool())
                    {
                        lblRateMiles.Visible = true;
                        txtRateMiles.Visible = true;
                        txtRateMiles.ReadOnly = true;
                        txtRateMiles.BackColor = ColorTranslator.FromHtml("#f0f0f0");
                    }
                    else
                    {
                        lblRateMiles.Visible = false;
                        txtRateMiles.Visible = false;
                    }
                }
            }
        }

        private void ConfigureAjax(bool enableAjax = true)
        {
            if ((RadAjaxManager.GetCurrent(Page).EnableAJAX = enableAjax))
            {
                RadAjaxHelper.AddAjaxSetting(this.Page, ddlPriority, ddlPriority);
                RadAjaxHelper.AddAjaxSetting(this.Page, rgMain, rgMain);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbRegion, rcbOrigin);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbRegion, rcbCustomer);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbRegion, panelTankInfo);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbRegion, panelBOLInfo);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbRegion, rcbCarrier);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbRegion, rcbDriver);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbRegion, rgMain);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbRegion, rcbDestination);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbRegion, cmdCreateLoads, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbTerminal, rcbOrigin);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbTerminal, rcbCustomer);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbTerminal, panelTankInfo);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbTerminal, panelBOLInfo);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbTerminal, rcbCarrier);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbTerminal, rcbDriver);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbTerminal, rgMain);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbTerminal, rcbDestination);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbTerminal, cmdCreateLoads, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbOrigin, rcbOrigin);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbOrigin, txtProducerName);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbOrigin, txtRateMiles);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbOrigin, rcbProduct);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbOrigin, rcbTicketType);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbOrigin, rcbDestination);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbOrigin, panelTankInfo);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbOrigin, panelBOLInfo);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbOrigin, rcbCarrier);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbOrigin, rcbDriver);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbOrigin, rcbCustomer);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbOrigin, rfvDispatchConfirmNum);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbOrigin, cvDispatchConfirmNum);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbOrigin, cmdCreateLoads);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbOrigin, txtContractNumber);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbOrigin, txtDispatchNotes);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbCustomer, rcbProduct);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbCustomer, rcbDestination);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbCustomer, rfvDispatchConfirmNum);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbCustomer, cmdCreateLoads, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbCustomer, txtContractNumber);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbProduct, rcbDestination);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbProduct, rcbCarrier);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbProduct, rcbDriver);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbProduct, cmdCreateLoads);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbProduct, panelTankInfo);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbProduct, panelBOLInfo);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbProduct, rgMain);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbProduct, cmdCreateLoads, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbProduct, rfvDispatchConfirmNum);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbProduct, txtContractNumber);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbDestination, rcbDriver);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbDestination, rfvDispatchConfirmNum);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbDestination, txtRateMiles);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbDestination, rcbConsignee);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbDestination, cmdCreateLoads);
                RadAjaxHelper.AddAjaxSetting(this.Page, panelTankInfo, cmdCreateLoads);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbTicketType, panelTankInfo);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbTicketType, panelBOLInfo);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbTicketType, cmdCreateLoads);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbCarrier, cmdCreateLoads);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbCarrier, rcbDriver);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbCarrier, rfvDispatchConfirmNum);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbDriver, rcbDriver);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbDriver, rcbCarrier);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbDriver, rfvDispatchConfirmNum);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbOriginTank, cmdCreateLoads, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, txtDispatchConfirmNum, cmdCreateLoads, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, txtDispatchConfirmNum, rntxtQty, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, txtDispatchConfirmNum, cvDispatchConfirmNum, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, txtDispatchConfirmNum, rfvDispatchConfirmNum, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, rdpDueDate, rdpDueDate);
                RadAjaxHelper.AddAjaxSetting(this.Page, rdpDueDate, rcbDriver);
                RadAjaxHelper.AddAjaxSetting(this.Page, rdpDueDate, txtContractNumber);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdCreateLoads, rgMain);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdCreateLoads, rfvDispatchConfirmNum, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdCreateLoads, cvDispatchConfirmNum, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdCreateLoads, radWindowManager, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdDelete, rgMain);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdAssignCarrier, rgMain);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdDispatch, rgMain);
            }
        }

        protected void cmdCreateLoads_Click(object source, EventArgs e)
        {
            if (Page.IsValid)
            {
                int count = CreateLoads();

                // refresh the grid
                RebindGrid();

                // provide a dialog to the user indicating success
                AlertUser("created", count);
            }
        }

        private void AlertUser(string action, int count, int width = 250, int height = 75)
        {
            string userMsg = string.Format(@"{0} order{1} {2}."
                , count
                , count == 1 ? "" : "s"
                , action);
            radWindowManager.RadAlert(userMsg, 250, 75, "Action Feedback", null);
        }

        private int CreateLoads()
        {
            int ret = 0;
            bool carrierAssigned = DBHelper.ToInt32(rcbCarrier.SelectedValue) != 0
                , driverAssigned = DBHelper.ToInt32(rcbDriver.SelectedValue) != 0;
            using (SSDB db = new SSDB())
            {
                using (SqlCommand cmd = db.BuildCommand("spCreateLoads"))
                {
                    cmd.Parameters["@UserName"].Value = User.Identity.Name;
                    cmd.Parameters["@OriginID"].Value = DBHelper.ToInt32(rcbOrigin.SelectedValue) == 0 ? DBNull.Value : rcbOrigin.SelectedValue as object;
                    cmd.Parameters["@DestinationID"].Value = DBHelper.ToInt32(rcbDestination.SelectedValue) == 0 ? DBNull.Value : rcbDestination.SelectedValue as object;
                    cmd.Parameters["@TicketTypeID"].Value = rcbTicketType.SelectedValue;
                    cmd.Parameters["@DueDate"].Value = rdpDueDate.SelectedDate.Value.Date;
                    cmd.Parameters["@CustomerID"].Value = DBHelper.ToInt32(rcbCustomer.SelectedValue) == 0 ? DBNull.Value : rcbCustomer.SelectedValue as object;
                    if (RadComboBoxHelper.SelectedValue(rcbConsignee) > 0)
                    {
                        cmd.Parameters["@ConsigneeID"].Value = RadComboBoxHelper.SelectedValue(rcbConsignee);
                    }


                    if (carrierAssigned)
                        cmd.Parameters["@CarrierID"].Value = rcbCarrier.SelectedValue;
                    if (driverAssigned)
                        cmd.Parameters["@DriverID"].Value = rcbDriver.SelectedValue;
                    cmd.Parameters["@Qty"].Value = rntxtQty.Value;
                    cmd.Parameters["@PriorityID"].Value = ddlPriority.SelectedValue;

                    if (driverAssigned)
                        cmd.Parameters["@StatusID"].Value = (int)OrderStatus.STATUS.Dispatched;
                    else if (carrierAssigned)
                        cmd.Parameters["@StatusID"].Value = (int)OrderStatus.STATUS.Assigned;
                    else
                        cmd.Parameters["@StatusID"].Value = (int)OrderStatus.STATUS.Generated;
                    
                    if (txtDispatchConfirmNum.Text.Length > 0)
                        cmd.Parameters["@DispatchConfirmNum"].Value = txtDispatchConfirmNum.Text;

                    if (txtJobNumber.Text.Length > 0)
                        cmd.Parameters["@JobNumber"].Value = txtJobNumber.Text;

                    cmd.Parameters["@ProductID"].Value = rcbProduct.SelectedValue;
                    cmd.Parameters["@DispatchNotes"].Value = txtDispatchNotes.Text;

                    if (panelTankInfo.Visible && RadComboBoxHelper.SelectedValue(rcbOriginTank) > 0)
                    {
                        cmd.Parameters["@OriginTankID"].Value = RadComboBoxHelper.SelectedValue(rcbOriginTank);
                    }
                    else 
                        cmd.Parameters["@OriginBOLNum"].Value = txtOriginBOLNum.Text;
                    try
                    {
                        cmd.ExecuteNonQuery();
                        ret = DBHelper.ToInt32(cmd.Parameters["@count"].Value);
                    }
                    catch (Exception e)
                    {
                        throw new Exception(
                            string.Format("Exception occurred generating orders with parameters: OriginID={0},DestinationID={1},TicketTypeID={2},DueDate={3}"
                                , rcbOrigin.SelectedValue
                                , rcbDestination.SelectedValue
                                , rcbTicketType.SelectedValue
                                , rdpDueDate.SelectedDate.Value.ToShortDateString()), e);
                    }
                }
            }
            return ret;
        }

        protected void cmdDelete_Click(object sender, EventArgs e)
        {
            int count = 0;
            using (SSDB db = new SSDB())
            {
                using (SqlCommand cmd = db.BuildCommand(
                    string.Format("UPDATE tblOrder SET DeleteDateUTC = getutcdate(), DeletedByUser = {0} WHERE ID = (@ID)"
                        , DBHelper.QuoteStr(UserSupport.UserName))))
                {
                    count = ExecuteCommandOnSelection(cmd);
                }
            }
            AlertUser("deleted", count);
        }

        protected void cmdAssignCarrier_Click(object sender, EventArgs e)
        {
            int count = 0;
            using (SSDB db = new SSDB())
            {
                using (SqlCommand cmd = db.BuildCommand(
                    string.Format("UPDATE tblOrder SET StatusID={0}, CarrierID = {1}, LastChangeDateUTC=getutcdate(), LastChangedByUser={2} WHERE ID = (@ID)"
                        , (int)OrderStatus.STATUS.Assigned, rcbCarrier.SelectedValue, DBHelper.QuoteStr(UserSupport.UserName))))
                {
                    count = ExecuteCommandOnSelection(cmd);
                }
            }
            AlertUser("assigned", count);
        }

        protected void cmdDispatch_Click(object sender, EventArgs e)
        {
            int count = 0;
            using (SSDB db = new SSDB())
            {
                using (SqlCommand cmd = db.BuildCommand(
                    string.Format(@"UPDATE tblOrder SET StatusID = CASE WHEN OriginID IS NOT NULL AND DestinationID IS NOT NULL THEN {0} ELSE {1} END"
                        + ", CarrierID = {2}"
                        + ", DriverID = CASE WHEN OriginID IS NOT NULL AND DestinationID IS NOT NULL THEN {3} ELSE NULL END "
                        + ", TruckID = (SELECT min(TruckID) FROM tblDriver WHERE ID = {3}) "
                        + ", TrailerID = (SELECT min(TrailerID) FROM tblDriver WHERE ID = {3}) "
                        + ", Trailer2ID = (SELECT min(Trailer2ID) FROM tblDriver WHERE ID = {3}) "
                        + ", LastChangeDateUTC = getutcdate() "
                        + ", LastChangedByUser = {4} "
                        + "WHERE ID = (@ID)"
                    , (int)OrderStatus.STATUS.Dispatched
                    , (int)OrderStatus.STATUS.Assigned
                    , rcbCarrier.SelectedValue
                    , rcbDriver.SelectedValue
                    , DBHelper.QuoteStr(UserSupport.UserName))))
                {
                    count = ExecuteCommandOnSelection(cmd);
                }
            }
            AlertUser("dispatched", count);
        }

        protected int ExecuteCommandOnSelection(SqlCommand cmd)
        {
            int ret = 0;
            cmd.Parameters.Add("@ID", SqlDbType.Int);

            foreach (GridItem item in rgMain.MasterTableView.Items)
            {
                if (item is GridDataItem && (item.FindControl("chkSel") as CheckBox).Checked)
                {
                    cmd.Parameters["@ID"].Value = (item as GridDataItem).GetDataKeyValue("ID");
                    ret += cmd.ExecuteNonQuery();
                }
            }
            RebindGrid();
            return ret;
        }


        protected void rcbDestination_SelectedIndexChanged(object sender, EventArgs e)
        {
            // get valid consignees
            App_Code.RadComboBoxHelper.Rebind(rcbConsignee);
            rcbConsignee.SelectedValue = "-1";

            //Show get rate miles from route if both origin and destination are selected
            if (RadComboBoxHelper.SelectedValue(rcbOrigin) != 0 && RadComboBoxHelper.SelectedValue(rcbDestination) != 0)
            {
                txtRateMiles.Text = GetRateMiles(RadComboBoxHelper.SelectedValue(rcbOrigin), RadComboBoxHelper.SelectedValue(rcbDestination));
            }

            entryChanged(sender, e);
        }


        protected void rcbOrigin_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (SSDB db = new SSDB())
            {                
                if (RadComboBoxHelper.SelectedValue(rcbOrigin) != 0)
                {
                    txtProducerName.Text = db.QuerySingleValue("SELECT P.Name FROM tblProducer P LEFT JOIN tblOrigin O ON O.ProducerID = P.ID WHERE O.ProducerID = P.ID AND O.ID={0}", RadComboBoxHelper.SelectedValue(rcbOrigin)).ToString();
                }

                //Show get rate miles from route if both origin and destination are selected
                if (RadComboBoxHelper.SelectedValue(rcbOrigin) != 0 && RadComboBoxHelper.SelectedValue(rcbDestination) != 0)
                {                   
                    txtRateMiles.Text = GetRateMiles(RadComboBoxHelper.SelectedValue(rcbOrigin), RadComboBoxHelper.SelectedValue(rcbDestination));
                }

                App_Code.RadComboBoxHelper.Rebind(rcbProduct, true);
                // if only 1 true product is available, just select it
                if (rcbProduct.Items.Count == 2)
                    rcbProduct.SelectedIndex = 1;

                App_Code.RadComboBoxHelper.Rebind(rcbDestination, true);
                if (rcbDestination.Items.Count == 2)
                    rcbDestination.SelectedIndex = 1;

                // Make sure to rebind the tank dropdown if the origin changes.  If this is not done, the generate button may stay
                // activated an allow the user to click generate when no tank is selected.
                App_Code.RadComboBoxHelper.Rebind(rcbOriginTank, true);

                // ensure rcbTicketType is rebound before assigning the default Origin.TicketTypeID value (to ensure it is already present)
                rcbTicketType.DataBind();
                object ticketTypeID = db.QuerySingleValue("SELECT TicketTypeID FROM tblOrigin WHERE ID={0}", RadComboBoxHelper.SelectedValue(rcbOrigin));
                RadComboBoxHelper.SetSelectedValue(rcbTicketType, ticketTypeID ?? 1);
                rcbTicketType_SelectedIndexChanged(rcbTicketType, EventArgs.Empty);
                App_Code.RadComboBoxHelper.Rebind(rcbCustomer, true);
                entryChanged(sender, e);
                //dispatchValueChanged(sender, e);
                FormatRadComboBoxH2S(sender as RadComboBox);
                
                UpdateDispatchConfirmationRequired();

                //Clear dispatch notes if origin has been changed
                txtDispatchNotes.Text = "";

                //JT-2864 (4/26/17) - Clear selected Carrier when the selected origin is changed
                rcbCarrier.SelectedIndex = int.Parse("-1");                
            }
        }

        private bool _dcr_skip = false; // prevent redundant executions on the same postback
        private void UpdateDispatchConfirmationRequired()
        {
            if (!_dcr_skip)
            {
                _dcr_skip = true;
                bool enabled = RadComboBoxHelper.SelectedValue(rcbDriver) > 0
                        && DBHelper.ToBoolean(
                            OrderRuleHelper.GetRuleValue(DateTime.UtcNow.Date
                                , OrderRuleHelper.Type.RequireShipperPO
                                , RadComboBoxHelper.SelectedValue(rcbCustomer)
                                , RadComboBoxHelper.SelectedValue(rcbCarrier)
                                , RadComboBoxHelper.SelectedValue(rcbProduct)
                                , RadComboBoxHelper.SelectedValue(rcbOrigin)
                                , RadComboBoxHelper.SelectedValue(rcbDestination)
                                , false));
                this.rfvDispatchConfirmNum.Enabled = enabled;
            }
        }

        private void UpdateContractNumber()
        {
            if (RadComboBoxHelper.SelectedValue(rcbOrigin) != 0 //producer
                && RadComboBoxHelper.SelectedValue(rcbProduct) != 0 // productgroup
                && RadComboBoxHelper.SelectedValue(rcbCustomer) != 0) // shipper
            {
                using (SSDB db = new SSDB())
                {
                    var ContractNumber = db.QuerySingleValue(@"
                        SELECT ContractNumber FROM tblContract WHERE ID = dbo.fnContractID(
                            {0},
                            (SELECT ProductGroupID FROM tblProduct WHERE ID = {1}),
                            (SELECT ProducerID FROM tblOrigin WHERE ID = {2}),
                            '{3}')",
                        RadComboBoxHelper.SelectedValue(rcbCustomer),
                        RadComboBoxHelper.SelectedValue(rcbProduct),
                        RadComboBoxHelper.SelectedValue(rcbOrigin),
                        rdpDueDate.SelectedDate.Value.Date);
                    txtContractNumber.Text = (ContractNumber ?? "").ToString();
                }
            }
        }

        protected void rcbOrigin_ItemDataBound(object sender, RadComboBoxItemEventArgs e)
        {
            if (DBHelper.ToBoolean((e.Item.DataItem as DataRowView)["H2S"]))
                e.Item.CssClass += " H2S";
        }

        protected void rcbOriginTank_SelectedIndexChanged(object sender, EventArgs e)
        {
            entryChanged(sender, e);
        }

        protected void rcbCarrier_SelectedIndexChanged(object sender, EventArgs e)
        {
            App_Code.RadComboBoxHelper.Rebind(rcbDriver);
            rcbDriver.SelectedValue = "-1";
            entryChanged(sender, e);
        }

        protected void rcbDriver_ItemDataBound(object sender, RadComboBoxItemEventArgs e)
        {
            if (e.Item.DataItem is DataRowView)
            {
                e.Item.Attributes.Add("CarrierID", (e.Item.DataItem as DataRowView)["CarrierID"].ToString());
                if ((e.Item.DataItem as DataRowView)["DriverScore"].ToString() == "0")
                {
                    e.Item.Enabled = false;
                    e.Item.Text += " [UNAVAILABLE]";
                }
            }
        }

        private int _driverID = 0;
        protected void rcbDriver_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ((sender as RadComboBox).SelectedItem != null)
            {
                // "remember" the current selected driver
                _driverID = RadComboBoxHelper.SelectedValue(sender as RadComboBox);
                RadComboBoxHelper.SetSelectedValue(rcbCarrier, (sender as RadComboBox).SelectedItem.Attributes["CarrierID"]);
                _dcr_skip = true;
                rcbCarrier_SelectedIndexChanged(rcbCarrier, EventArgs.Empty);
                // reselect the same driver
                RadComboBoxHelper.SetSelectedValue(sender as RadComboBox, _driverID);
                _dcr_skip = false;
                UpdateDispatchConfirmationRequired();
            }
        }

        protected void rcbRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            App_Code.RadComboBoxHelper.Rebind(rcbOrigin, true);
            rcbOrigin_SelectedIndexChanged(rcbOrigin, EventArgs.Empty);
            App_Code.RadComboBoxHelper.Rebind(rcbDestination, true);
            RebindGrid();
            entryChanged(sender, e);
        }

        protected void rcbTerminal_SelectedIndexChanged(object sender, EventArgs e)
        {
            App_Code.RadComboBoxHelper.Rebind(rcbOrigin, true);
            rcbOrigin_SelectedIndexChanged(rcbOrigin, EventArgs.Empty);
            App_Code.RadComboBoxHelper.Rebind(rcbDestination, true);
            RebindGrid();
            entryChanged(sender, e);
        }

        protected void rcbProduct_SelectedIndexChanged(object sender, EventArgs e)
        {
            App_Code.RadComboBoxHelper.Rebind(rcbDestination, true);
            RebindGrid();
            entryChanged(sender, e);
        }

        private void RebindGrid()
        {
            rgMain.Rebind();
        }

        protected void entryChanged(object sender, EventArgs e)
        {
            UpdateDispatchConfirmationRequired();
            UpdateContractNumber();
            // ensure the destination and origin + shipper are defined before allowing order creation
            // 9/11/15 BB - The origin check has to use text because the new paging functionality breaks the index > 0 check
            this.cmdCreateLoads.Enabled = rcbOrigin.Text != "" && rcbCustomer.SelectedIndex > 0
                && rcbProduct.SelectedIndex > 0 && rcbDestination.SelectedIndex > 0
                && (Settings.SettingsID.RequireTanksDuringCreate.AsBool() == false  // system setting is turned off
                    || !panelTankInfo.Visible // tank info not visible - tanks don't apply to this ticket type
                    || rcbOriginTank.SelectedIndex > 0); // a tank other than default "(Select Tank)" is selected
        }

        protected void txtDispatchConfirmNum_TextChanged(object sender, EventArgs e)
        {
            if (txtDispatchConfirmNum.Text.Trim().Length > 0)
            {
                rntxtQty.Value = 1;
                rntxtQty.Enabled = false;
            }
            else
                rntxtQty.Enabled = true;
            entryChanged(sender, e);
        }

        protected void rdpDueDate_Changed(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            string driver = rcbDriver.SelectedValue;

            RadComboBoxHelper.Rebind(rcbDriver);
            rcbDriver.SelectedValue = driver; // reselect the same driver
            entryChanged(sender, e);
        }

        protected void rcbTicketType_SelectedIndexChanged(object sender, EventArgs e)
        {
            HandleTicketType(RadComboBoxHelper.SelectedValue(rcbTicketType));
            entryChanged(sender, e);
        }

        private void HandleTicketType(int ticketTypeID)
        {
            switch (ticketTypeID)
            {
                case (int)TicketType.TYPE.GaugeRun:
                case (int)TicketType.TYPE.Gauge_Auto_Close:
                case (int)TicketType.TYPE.GaugeNet:
                case (int)TicketType.TYPE.NetVolume:
                case (int)TicketType.TYPE.CAN_MeterRun:
                case (int)TicketType.TYPE.Production_Water_Run:
                    panelBOLInfo.Visible = false;
                    panelTankInfo.Visible = true;
                    break;
                case (int)TicketType.TYPE.BasicRun:
                case (int)TicketType.TYPE.CAN_BasicRun:
                    panelBOLInfo.Visible = true;
                    panelTankInfo.Visible = false;
                    break;
                case (int)TicketType.TYPE.MeterRun:
                    panelBOLInfo.Visible = true;
                    // only displayed (and required) when at least 1 "real" tank is available
                    panelTankInfo.Visible = rcbOriginTank.Items.Count > 1;
                    break;
                case (int)TicketType.TYPE.GrossVolume:
                default:
                    panelBOLInfo.Visible = panelTankInfo.Visible = false;
                    break;
            }
        }

        protected void glblOrigin_PreRender(object sender, EventArgs e)
        {
            if ((sender as Label).Text.StartsWith("H2S-"))
                (sender as Label).CssClass += " H2S";
        }
        
        private void FormatRadComboBoxH2S(RadComboBox rcb)
        {
            if (rcb.Text.StartsWith("H2S-") || (rcb.SelectedItem != null && rcb.SelectedItem.Text.StartsWith("H2S-")))
                rcb.InputCssClass += " H2S";
            else
                rcb.InputCssClass = rcb.InputCssClass.Replace("H2S", "").Trim();
        }

        private void ExportToExcel()
        {
            string filename = string.Format("OrderCreation_{0:yyyy-MM-dd-HH-mm}.xlsx", DateTime.Now);
            MemoryStream ms = new RadGridExcelExporter().ExportSheet(rgMain.MasterTableView);
            Response.ExportExcelStream(ms, filename);
        }

        protected void grid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.ExportToExcelCommandName)
            {
                ExportToExcel();
                e.Canceled = true;
            }
            else if (e.CommandName == "Refresh")
            {
                rgMain.Rebind();
            }
        }

        protected void grid_ItemCreated(object sender, GridItemEventArgs e)
        {
        } 

        protected void grid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            bool h2s = false;
            if (e.Item.DataItem != null && !(e.Item is GridGroupHeaderItem) && e.Item.DataItem is DataRowView)            
                h2s = DBHelper.ToBoolean((e.Item.DataItem as DataRowView)["H2S"]);                            

            if (e.Item is GridDataItem)
            {
                Color backColor = Color.White;
                switch ((RadGridHelper.GetControlByType((e.Item as GridDataItem)["OrderStatusID"], typeof(Label)) as Label).Text)
                {
                    case "Generated":
                        backColor = Color.LightGreen;
                        break;
                    case "Dispatched":
                        backColor = Color.Tan;
                        break;
                    case "Declined":
                        backColor = Color.LightGray;
                        break;
                }
                if (backColor != Color.Transparent)
                    e.Item.BackColor = backColor;

                // H2S alert
                if (h2s)
                {
                    ((Label)e.Item.FindControl("lblH2S")).Text = " H2S ";
                    ((Label)e.Item.FindControl("lblH2S")).CssClass += " H2S";
                }
            }
        }

        protected void cvDispatchConfirmNum_ServerValidate(object source, ServerValidateEventArgs args)
        {
            using (SSDB ssdb = new SSDB())
            {
                args.IsValid = txtDispatchConfirmNum.Text.Trim().Length == 0 || DBHelper.ToInt32(ssdb.QuerySingleValue(
                    string.Format(
                        "SELECT count(*) FROM tblOrder WHERE DeleteDateUTC IS NULL AND CustomerID = {0} AND DispatchConfirmNum = {1}"
                            , RadComboBoxHelper.SelectedValue(rcbCustomer), DBHelper.QuoteStr(txtDispatchConfirmNum.Text)))) == 0;
            }
        }

        protected void cmdSaveLayout_Click(object source, EventArgs e)
        {
            rpmMain.SaveState();
            rgMain.Rebind();
        }

        protected void cmdResetLayout_Click(object source, EventArgs e)
        {
            DbStorageProvider.ResetStateInStorage(rpmMain.StorageProviderKey);
            Response.Redirect(Request.Url.ToString());
        }

        ///Get the rate miles for the specified route (called when
        protected string GetRateMiles(int OriginID, int DestinationID)
        {
            using (SSDB db = new SSDB())
            {
                string result = DBHelper.ToString(db.QuerySingleValue("SELECT ActualMiles FROM tblRoute WHERE OriginID = {0} AND DestinationID = {1}", OriginID, DestinationID));

                return (string.IsNullOrEmpty(result)) ? "N/A" : result;
            }            
        }
    }
}