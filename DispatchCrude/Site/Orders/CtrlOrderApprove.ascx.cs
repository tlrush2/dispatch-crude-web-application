﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using DispatchCrude.App_Code;
using DispatchCrude.Core;
using AlonsIT;

namespace DispatchCrude.Orders
{
    public partial class CtrlOrderApprove : System.Web.UI.UserControl
    {
        private object _dataItem = null;

        // load the popup with the correct values
        protected void Page_Load(object sender, System.EventArgs e)
        {
            if (DataItem != null && DataItem is DataRowView)
            {
                DataRowView dv = DataItem as DataRowView;

                hfID.Value = dv["ID"].ToString();
                litActualMiles.Text = dv["ActualMiles"].ToString();
                if (!DBHelper.IsNull(dv["OverrideActualMiles"]))
                    rntxActualMiles.DbValue =  DBHelper.ToInt32(dv["OverrideActualMiles"]);
                litOriginMin.Text = dv["OriginMinutes"].ToString();
                if (!DBHelper.IsNull(dv["OverrideOriginMinutes"]))
                    rntxOriginMin.DbValue =  DBHelper.ToInt32(dv["OverrideOriginMinutes"]);
                litDestMin.Text = dv["DestMinutes"].ToString();
                if (!DBHelper.IsNull(dv["OverrideDestMinutes"]))
                    rntxDestMin.DbValue =  DBHelper.ToInt32(dv["OverrideDestMinutes"]);
                chkOriginChainup.Checked = DBHelper.ToBoolean(dv["OriginChainup"]);
                chkOverrideOriginChainup.Enabled = chkOriginChainup.Checked;
                chkOverrideOriginChainup.Checked = DBHelper.ToBoolean(dv["OverrideOriginChainup"]);
                chkDestChainup.Checked = DBHelper.ToBoolean(dv["DestChainup"]);
                chkOverrideDestChainup.Enabled = chkDestChainup.Checked;
                chkOverrideDestChainup.Checked = DBHelper.ToBoolean(dv["OverrideDestChainup"]);
                chkH2S.Checked = DBHelper.ToBoolean(dv["H2S"]);
                chkOverrideH2S.Enabled = chkH2S.Checked;
                chkOverrideH2S.Checked = DBHelper.ToBoolean(dv["OverrideH2S"]);
                chkRerouted.Checked = DBHelper.ToBoolean(dv["Rerouted"]);
                chkOverrideReroute.Enabled = chkRerouted.Checked;
                chkOverrideReroute.Checked = DBHelper.ToBoolean(dv["OverrideReroute"]);

                litTransferPercentComplete.Text = dv["TransferPercentComplete"].ToString();
                if (!DBHelper.ToBoolean(dv["IsTransfer"]))
                    rntxTransferPercentComplete.Enabled = false;
                else if (!DBHelper.IsNull(dv["OverrideTransferPercentComplete"]))
                    rntxTransferPercentComplete.DbValue = DBHelper.ToInt32(dv["OverrideTransferPercentComplete"]);

                litCarrierMinSettlementUOM.Text = dv["CarrierMinSettlementUOM"].ToString();
                litCarrierMinSettlementUnits.Text = dv["CarrierMinSettlementUnits"].ToString();
                if (!DBHelper.IsNull(dv["OverrideCarrierMinSettlementUnits"]))
                    rntxCarrierMinSettlementUnits.DbValue = DBHelper.ToDecimal(dv["OverrideCarrierMinSettlementUnits"]);
                litShipperMinSettlementUOM.Text = dv["ShipperMinSettlementUOM"].ToString();
                litShipperMinSettlementUnits.Text = dv["ShipperMinSettlementUnits"].ToString();
                if (!DBHelper.IsNull(dv["OverrideShipperMinSettlementUnits"]))
                    rntxShipperMinSettlementUnits.DbValue = DBHelper.ToDecimal(dv["OverrideShipperMinSettlementUnits"]);
                if (!DBHelper.IsNull(dv["ApprovalNotes"]))
                    txtApprovalNotes.Text = DBHelper.ToString(dv["ApprovalNotes"]);
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///          Required method for Designer support - do not modify
        ///          the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion

        public object DataItem
        {
            get
            {
                return this._dataItem;
            }
            set
            {
                this._dataItem = value;
            }
        }

        protected void UpdateDataItem(object sender, EventArgs e)
        {
            //update, do not approve
            SaveChanges(false);
        }
        protected void ApproveDataItem(object sender, EventArgs e)
        {
            //update and approve
            SaveChanges(true);
        }

        /*  Approves/updates the order in the approval table.  Runs the spUpdateOrderApproval stored procedure to update the database */
        private void SaveChanges(bool approved)
        {
            if (Page.IsValid)
            {
                using (SSDB db = new SSDB())
                {
                    using (System.Data.SqlClient.SqlCommand cmd = db.BuildCommand("spUpdateOrderApproval"))
                    {
                        cmd.Parameters["@ID"].Value = DBHelper.ToInt32(hfID.Value);
                        cmd.Parameters["@UserName"].Value = UserSupport.UserName;
                        cmd.Parameters["@OverrideActualMiles"].Value = rntxActualMiles.DbValue;
                        cmd.Parameters["@OverrideOriginMinutes"].Value = rntxOriginMin.DbValue;
                        cmd.Parameters["@OverrideDestMinutes"].Value = rntxDestMin.DbValue;
                        cmd.Parameters["@OverrideOriginChainup"].Value = chkOverrideOriginChainup.Checked;
                        cmd.Parameters["@OverrideDestChainup"].Value = chkOverrideDestChainup.Checked;
                        cmd.Parameters["@OverrideH2S"].Value = chkOverrideH2S.Checked;
                        cmd.Parameters["@OverrideReroute"].Value = chkOverrideReroute.Checked;
                        cmd.Parameters["@OverrideTransferPercentComplete"].Value = rntxTransferPercentComplete.DbValue;
                        cmd.Parameters["@OverrideCarrierMinSettlementUnits"].Value = rntxCarrierMinSettlementUnits.DbValue;
                        cmd.Parameters["@OverrideShipperMinSettlementUnits"].Value = rntxShipperMinSettlementUnits.DbValue;
                        cmd.Parameters["@Approved"].Value = approved;
                        cmd.Parameters["@ApprovalNotes"].Value = txtApprovalNotes.Text;
                        cmd.ExecuteNonQuery();
                    }
                }
            }
        }

    }
}