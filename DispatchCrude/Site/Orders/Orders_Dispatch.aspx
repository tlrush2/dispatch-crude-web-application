﻿<%@ Page Title="Dispatch" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Orders_Dispatch.aspx.cs" Inherits="DispatchCrude.Site.Orders.Orders_Dispatch" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="contentMain" ContentPlaceHolderID="MainContent" runat="server">
    <style type="text/css">
        .H2S {
            background-color: Red !important;
            color: White !important;
        }
    </style>
    <script>
        $("#ctl00_ctl00_EntityCaption").html("Grid");
    </script>
    <div class="tabbable TabRepaint">
        <ul class="nav nav-tabs" id="tabmenu" runat="server">
            <!--Tabs will print here (see code behind)-->
        </ul>
        <div class="tab-content">
            <div class="tab-pane in active">

                <div id="gridArea" style="height:100%;">

                    <telerik:RadPersistenceManager ID="rpmMain" runat="server">
                        <PersistenceSettings>
                            <telerik:PersistenceSetting ControlID="rgMain" />
                        </PersistenceSettings>
                    </telerik:RadPersistenceManager>
                    <telerik:RadGrid ID="rgMain" runat="server" EnableHeaderContextMenu="true" CellSpacing="0" GridLines="None" ShowStatusBar="false"
                                     AllowSorting="True" AllowFilteringByColumn="true" Height="800" CssClass="GridRepaint" ShowGroupPanel="false" EnableLinqExpressions="false"
                                     DataSourceID="dsMain" OnItemCreated="grid_ItemCreated" OnItemDataBound="grid_ItemDataBound" OnItemCommand="grid_ItemCommand"
                                     AllowPaging="true" EnableEmbeddedSkins="true" Skin="Vista" AlternatingItemStyle-BackColor="#dcf2fc"
                                     PageSize='<%# Settings.DefaultPageSize %>'>
                        <ClientSettings AllowDragToGroup="false" Resizing-AllowColumnResize="true" Resizing-AllowResizeToFit="true" AllowColumnHide="true" AllowColumnsReorder="true">
                            <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                        </ClientSettings>
                        <SortingSettings EnableSkinSortStyles="false" />
                        <GroupingSettings CaseSensitive="False" ShowUnGroupButton="true" />
                        <HeaderStyle Wrap="False" />
                        <PagerStyle AlwaysVisible="True" />

                        <MasterTableView AutoGenerateColumns="False" DataKeyNames="ID" CommandItemDisplay="Top" EditMode="PopUp" GroupsDefaultExpanded="false">
                            <CommandItemTemplate>
                                <div style="padding: 5px;">
                                    <asp:LinkButton ID="cmdExportExcel" Style="vertical-align: middle" runat="server"
                                                    CommandName="ExportToExcel" CssClass="pull-right NOAJAX">
                                        <img style="border:0px" alt="" src="../../images/exportToExcel.png" />
                                        Excel Export &nbsp;&nbsp;
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" Text="Refresh" CommandName="Refresh" CssClass="pull-right" runat="server">Refresh&nbsp;&nbsp;|&nbsp;&nbsp;</asp:LinkButton>
                                    <asp:Button runat="server" ID="buttonRefresh" CommandName="Refresh" CssClass="rgRefresh pull-right" Text="Refresh" Title="Refresh" />
                                    <div style="width:10px" class="pull-right">&nbsp;</div>
                                    <asp:LinkButton ID="cmdResetLayout" Style="vertical-align: middle" runat="server"
                                                    OnClick="cmdResetLayout_Click" Enabled="true" CssClass="pull-right">
                                        <img style="border:0px" alt="" src="../../images/wrench_orange.png" />
                                        Reset Layout
                                    </asp:LinkButton>
                                    <div style="width:10px" class="pull-right">&nbsp;</div>
                                    <asp:LinkButton ID="cmdSaveLayout" Style="vertical-align: middle" runat="server"
                                                    OnClick="cmdSaveLayout_Click" Enabled="true" CssClass="pull-right">
                                        <img style="border:0px" alt="" src="../../images/wrench_orange.png" />
                                        Save Layout
                                    </asp:LinkButton>
                                </div>
                            </CommandItemTemplate>
                            <EditFormSettings UserControlName="CtrlOrderEdit.ascx" EditFormType="WebUserControl"
                                              CaptionDataField="OrderNum" CaptionFormatString="Order # {0}">
                                <PopUpSettings Modal="true" />
                                <EditColumn UniqueName="EditColumn" />
                            </EditFormSettings>

                            <Columns>
                                <telerik:GridTemplateColumn UniqueName="ActionColumn" ReadOnly="true" AllowFiltering="false" Groupable="false">
                                    <ItemTemplate>
                                        <asp:Button CssClass="btn btn-blue btn-xs shiny" ID="cmdEdit" runat="server" Text="Dispatch" CommandName="Edit" />
                                    </ItemTemplate>
                                    <HeaderTemplate>
                                        <asp:Button ID="btnExport" runat="server" ClientIDMode="Static" Text="Export" CssClass="NOAJAX btn btn-blue shiny"
                                                    OnClientClick="WaitForExport" OnClick="cmdExport_Click" Visible="false" />
                                    </HeaderTemplate>
                                    <HeaderStyle Width="70px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="ID" UniqueName="ID" ReadOnly="true" Display="false" Visible="false" ForceExtractValue="Always" />

                                <telerik:GridBoundColumn DataField="OrderStatus" UniqueName="OrderStatus"
                                                         HeaderText="Order Status" SortExpression="OrderStatus" DataType="System.String"
                                                         FilterControlWidth="70%">
                                    <HeaderStyle Width="90px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn HeaderText="Order #" DataField="OrderNum" FilterControlWidth="60%" UniqueName="OrderNum"
                                                         SortExpression="OrderNum" DataType="System.Int32" Display="true" Visible="true" Groupable="false" ReadOnly="true">
                                    <HeaderStyle Width="80px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn DataField="PriorityNum" HeaderText="Priority" SortExpression="PriorityID"
                                                            FilterControlWidth="70%" EditFormColumnIndex="0"
                                                            UniqueName="PriorityID" GroupByExpression="PriorityNum GROUP BY PriorityNum" ReadOnly="true">
                                    <ItemTemplate>
                                        <blac:PriorityLabel ID="lblPriority" runat="server" Text='<%# Eval("PriorityNum") %>' Width="100%" BorderStyle="None" style="text-align:center" />
                                    </ItemTemplate>
                                    <HeaderStyle Width="90px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn DataField="ECOT_Minutes" HeaderText="ECOT" DataType="System.Decimal" FilterControlWidth="60%"
                                                            UniqueName="ECOT_Minutes" SortExpression="ECOT_Minutes" EditFormColumnIndex="2">
                                    <ItemTemplate>
                                        <asp:Label ID="lblECOT_Minutes" runat="server" Text='<%# DateHelper.FormatDurationMinutes(Eval("ECOT_Minutes")) %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="80px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn DataField="DueDate" HeaderText="Due Date"
                                                            GroupByExpression="DueDate GROUP BY DueDate"
                                                            UniqueName="DueDate" SortExpression="DueDate" FilterControlWidth="70%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDueDate" runat="server" Text='<%# Eval("DueDate", "{0:d}") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="90px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="ProductShort" HeaderText="Product" SortExpression="Product" FilterControlWidth="70%"
                                                         UniqueName="Product">
                                    <HeaderStyle Width="110px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="DispatchConfirmNum" UniqueName="DispatchConfirmNum" HeaderText="Shipper PO #" FilterControlWidth="70%"
                                                         HeaderStyle-Width="100px" />
                                <telerik:GridBoundColumn DataField="JobNumber" UniqueName="JobNumber" HeaderText="Job #" FilterControlWidth="70%"
                                                         HeaderStyle-Width="100px" />
                                <telerik:GridBoundColumn DataField="ContractNumber" UniqueName="ContractNumber" HeaderText="Contract #" FilterControlWidth="70%"
                                                         HeaderStyle-Width="100px" />
                                <telerik:GridBoundColumn DataField="OriginRegion" UniqueName="OriginRegion" HeaderText="Origin Region" SortExpression="OriginRegion"
                                                         ReadOnly="true" ForceExtractValue="Always" FilterControlWidth="70%" GroupByExpression="OriginRegion GROUP BY OriginRegion">
                                    <HeaderStyle Width="100px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="OriginStateAbbrev" UniqueName="OriginStateAbbrev" HeaderText="Origin ST" SortExpression="OriginStateAbbrev"
                                                         ReadOnly="true" ForceExtractValue="Always" FilterControlWidth="50%" GroupByExpression="OriginStateAbbrev GROUP BY OriginStateAbbrev">
                                    <HeaderStyle Width="60px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="LeaseNum" UniqueName="LeaseNum" HeaderText="Lease #" FilterControlWidth="70%"
                                                         HeaderStyle-Width="100px" />
                                <telerik:GridTemplateColumn DataField="Origin" HeaderText="Origin" SortExpression="Origin" UniqueName="Origin"
                                                            DataType="System.String" GroupByExpression="Origin GROUP BY Origin"
                                                            FilterControlWidth="70%">
                                    <ItemTemplate>
                                        <asp:Label ID="glblOrigin" runat="server"
                                                   Text='<%# String.IsNullOrEmpty(Eval("Origin").ToString()) ? "(Contact Dispatch)" : Eval("Origin") %>'
                                                   OnPreRender="glblOrigin_PreRender" />
                                        <asp:Label ID="lblH2S" runat="server" Text='' CssClass="label label-danger" />
                                    </ItemTemplate>
                                    <HeaderStyle Width="250px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="OriginTerminal" UniqueName="OriginTerminal" HeaderText="Origin Terminal" SortExpression="OriginTerminal"
                                                         ReadOnly="true" ForceExtractValue="Always" FilterControlWidth="50%" GroupByExpression="OriginTerminal GROUP BY OriginTerminal">
                                    <HeaderStyle Width="150px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="OriginTankText" SortExpression="OriginTankText" FilterControlWidth="65%"
                                                         UniqueName="OriginTankText" HeaderText="Tank #" HeaderStyle-Width="120px" />
                                <telerik:GridBoundColumn DataField="OriginBOLNum" SortExpression="OriginBOLNum" FilterControlWidth="65%"
                                                         UniqueName="OriginBOLNum" HeaderText="BOL #" HeaderStyle-Width="80px" />
                                <telerik:GridTemplateColumn DataField="OriginDrivingDirections" HeaderText="Origin DD" SortExpression="OriginDrivingDirections"
                                                            UniqueName="OriginDrivingDirections" DataType="System.String" FilterControlWidth="70%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblOriginDrivingDirections" runat="server" data-toggle="tooltip"
                                                   class='<%# String.IsNullOrEmpty(Eval("OriginDrivingDirections").ToString()) ? "" : "center glyphicon glyphicon-list-alt" %>'
                                                   title='<%# String.IsNullOrEmpty(Eval("OriginDrivingDirections").ToString()) ? "" : Eval("OriginDrivingDirections") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="80px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="Operator" HeaderText="Operator"
                                                         SortExpression="Operator" FilterControlWidth="70%" UniqueName="Operator">
                                    <HeaderStyle Width="250px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Producer" HeaderText="Producer"
                                                         SortExpression="Producer" FilterControlWidth="70%" UniqueName="Producer">
                                    <HeaderStyle Width="250px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="DestRegion" UniqueName="DestRegion" HeaderText="Dest Region" SortExpression="DestRegion"
                                                         ReadOnly="true" ForceExtractValue="Always" FilterControlWidth="70%" GroupByExpression="DestRegion GROUP BY DestRegion">
                                    <HeaderStyle Width="100px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn DataField="Destination" HeaderText="Destination"
                                                            DataType="System.String" GroupByExpression="Destination GROUP BY Destination"
                                                            UniqueName="Destination" SortExpression="Destination" FilterControlWidth="70%">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="linkDestination" runat="server" Text='<%# Eval("Destination") %>' ForeColor="Blue" Visible="false"
                                                        OnClientClick='<%# String.Format("RerouteOrder({0});",DataBinder.Eval(Container,"DataItem.ID"))%> ' ToolTip="Click to Re-route this order" />
                                        <asp:Label ID="lblDestination" runat="server" Text='<%# String.IsNullOrEmpty(Eval("Destination").ToString()) ? "(Contact Dispatch)" : Eval("Destination") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="250px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="Consignee" UniqueName="Consignee" HeaderText="Consignee" SortExpression="Consignee"
                                                         ReadOnly="true" ForceExtractValue="Always" FilterControlWidth="50%" GroupByExpression="Consignee GROUP BY Consignee">
                                    <HeaderStyle Width="150px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="DestTerminal" UniqueName="DestTerminal" HeaderText="Destination Terminal" SortExpression="DestTerminal"
                                                         ReadOnly="true" ForceExtractValue="Always" FilterControlWidth="50%" GroupByExpression="DestTerminal GROUP BY DestTerminal">
                                    <HeaderStyle Width="150px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="DestinationStation" HeaderText="Destination Station"
                                                         SortExpression="DestinationStation" FilterControlWidth="70%" UniqueName="DestinationStation">
                                    <HeaderStyle Width="150px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn DataField="DestinationDrivingDirections" HeaderText="Dest DD" SortExpression="DestinationDrivingDirections"
                                                            UniqueName="DestinationDrivingDirections" DataType="System.String" FilterControlWidth="70%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDestinationDrivingDirections" runat="server" data-toggle="tooltip"
                                                   class='<%# String.IsNullOrEmpty(Eval("DestinationDrivingDirections").ToString()) ? "" : "center glyphicon glyphicon-list-alt" %>'
                                                   title='<%# String.IsNullOrEmpty(Eval("DestinationDrivingDirections").ToString()) ? "" : Eval("DestinationDrivingDirections") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="80px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="Carrier" HeaderText="Carrier"
                                                         SortExpression="Carrier" FilterControlWidth="70%" UniqueName="Carrier">
                                    <HeaderStyle Width="165px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Customer" HeaderText="Shipper" Display="false" SortExpression="Customer" FilterControlWidth="70%"
                                                         UniqueName="Customer" ReadOnly="true">
                                    <HeaderStyle Width="165px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn DataField="Driver" HeaderText="Driver"
                                                            DataType="System.String" GroupByExpression="Driver GROUP BY Driver"
                                                            UniqueName="Driver" SortExpression="Driver" FilterControlWidth="70%">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="linkTransfer" runat="server" Text='<%# Eval("Driver") %>' ForeColor="Blue" Visible="false"
                                                        OnClientClick='<%# String.Format("TransferOrder({0});",DataBinder.Eval(Container,"DataItem.ID"))%> ' ToolTip="Click to transfer/slip-seat this order" />
                                        <asp:Label ID="lblDriver" runat="server" Text='<%# Eval("Driver") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="195px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="DriverTerminal" UniqueName="DriverTerminal" HeaderText="Driver Terminal" SortExpression="DriverTerminal"
                                                         ReadOnly="true" ForceExtractValue="Always" FilterControlWidth="50%" GroupByExpression="DriverTerminal GROUP BY DriverTerminal">
                                    <HeaderStyle Width="150px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Truck" HeaderText="Truck"
                                                         SortExpression="Truck" FilterControlWidth="70%" UniqueName="Truck">
                                    <HeaderStyle Width="100px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Trailer" HeaderText="Trailer"
                                                         SortExpression="Trailer" FilterControlWidth="70%" UniqueName="Trailer">
                                    <HeaderStyle Width="100px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Trailer2" HeaderText="Trailer 2"
                                                         SortExpression="Trailer2" FilterControlWidth="70%" UniqueName="Trailer2">
                                    <HeaderStyle Width="100px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="TicketType" HeaderText="Ticket Type" FilterControlWidth="70%"
                                                         UniqueName="TicketType">
                                    <HeaderStyle Width="100px" />
                                </telerik:GridBoundColumn>

                                <telerik:GridBoundColumn DataField="OriginUom" HeaderText="Origin UoM"
                                                         SortExpression="OriginUom" FilterControlWidth="70%" UniqueName="OriginUom">
                                    <HeaderStyle Width="100px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="DestUom" HeaderText="Dest UoM"
                                                         SortExpression="DestUom" FilterControlWidth="70%" UniqueName="DestUom">
                                    <HeaderStyle Width="100px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="DispatchNotes" FilterControlWidth="70%" UniqueName="DispatchNotes" HeaderText="Dispatch Notes"
                                                         SortExpression="DispatchNotes">
                                    <HeaderStyle Width="250px" />
                                </telerik:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>
                </div>
            </div>
        </div>
    </div>
    <telerik:RadScriptBlock runat="server">
        <script type="text/javascript">
            // this function invokes the Order_Reroute_Popup page to do the reroute
            function RerouteOrder(id) {
                debugger;
                var wm = GetRadWindowManager();
                wm.open("/Site/Orders/Order_Reroute_Popup.aspx?ID=" + id, null);
                //window.radopen("/Site/Orders/Order_Reroute_Popup.aspx?ID=" + id, null, 385, 305);
            }
            // this function invokes the Order_Transfer_Popup page to do the transfer/slip-seat
            function TransferOrder(id) {
                debugger;
                var wm = GetRadWindowManager();
                wm.open("/Site/Orders/Order_Transfer_Popup.aspx?ID=" + id, null);
            }
            function ReloadPage() {
                window.location.reload();
            };
            function popup_ClientClose(sender, args) {
                ReloadPage();
            };

            var fileDownloadCheckTimer, button;

            function DisableSender(sender) {
                sender.set_enabled(false);
            }

            function WaitForExport(sender, args) {
                //disable button
                DisableSender(button = sender);

                fileDownloadCheckTimer = window.setInterval(function () {
                    //get cookie and compare
                    var cookieValue = readCookie("fileDownloadToken");
                    if (cookieValue == "done") {
                        finishDownload(false);
                    }
                }, 1000);
            }

            function finishDownload(refresh) {
                window.clearInterval(fileDownloadCheckTimer);
                //clear cookie
                eraseCookie("fileDownloadToken");
                //enable button
                button.set_enabled(true);
            }

            function createCookie(name, value, days) {
                if (days) {
                    var date = new Date();
                    var expires;
                    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                    expires = "; expires=" + date.toGMTString();
                }
                else expires = "";
                document.cookie = name + "=" + value + expires + "; path=/";
            }

            function readCookie(name) {
                var nameEQ = name + "=";
                var ca = document.cookie.split(';');
                for (var i = 0; i < ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
                }
                return null;
            }

            function eraseCookie(name) {
                createCookie(name, "", -1);
            };
        </script>
    </telerik:RadScriptBlock>
    <telerik:RadWindowManager ID="rwmSingleton" runat="server" Width="400" Height="315" VisibleStatusbar="false" EnableViewState="false"
                              Behaviors="Close,Move,Resize"
                              EnableShadow="true" OnClientClose="popup_ClientClose" Modal="true">
    </telerik:RadWindowManager>
        
    <blc:RadGridDBCtrl ID="dbcMain" runat="server" ControlID="rgMain" FilterActiveEntities="false" />
    <blac:DBDataSource ID="dsMain" runat="server"
        SelectCommand="SELECT *
            FROM dbo.viewOrder
            WHERE (StatusID IN (1,2,3,7,8) AND PrintStatusID IN (1,2,7,8) 
                    OR @HoursBack > 0 AND StatusID IN (3,4) AND DestDepartTimeUTC >= DATEADD(HOUR, -@HoursBack, GETUTCDATE()))
                AND (@CarrierID=-1 OR CarrierID=@CarrierID)
                AND (@RegionID=0 OR OriginRegionID=@RegionID OR DestRegionID=@RegionID OR DriverRegionID=@RegionID)
                AND (
                      @TerminalID = -1
		              OR (
                            OriginID IN (SELECT ID FROM tblOrigin WHERE TerminalID = @TerminalID OR TerminalID IS NULL)	
			                AND 
                            DestinationID IN (SELECT ID FROM tblDestination WHERE TerminalID = @TerminalID OR TerminalID IS NULL)
                            AND
                            DriverID IN (SELECT ID FROM tblDriver WHERE TerminalID = @TerminalID OR TerminalID IS NULL)
                         )
                    )
                AND DeleteDateUTC IS NULL
                ORDER BY
                    CASE WHEN PrintStatusID IN (2,7) THEN PriorityID ELSE 4 END
                    , StatusNum, Origin, Destination, DueDate">
        <SelectParameters>
            <asp:ProfileParameter Name="CarrierID" PropertyName="CarrierID" ConvertEmptyStringToNull="false" DbType="string" DefaultValue="-1" />
            <asp:ProfileParameter Name="TerminalID" PropertyName="TerminalID" ConvertEmptyStringToNull="true" DbType="string" DefaultValue="-1" />
            <asp:Parameter Name="RegionID" ConvertEmptyStringToNull="false" DbType="Int32" DefaultValue="0" />
            <asp:Parameter Name="HoursBack" ConvertEmptyStringToNull="false" DbType="Int32" DefaultValue="0" />
        </SelectParameters>
    </blac:DBDataSource>
</asp:Content>