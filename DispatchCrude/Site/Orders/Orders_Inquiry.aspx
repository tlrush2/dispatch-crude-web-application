﻿<%@ Page Title="Reports" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Orders_Inquiry.aspx.cs"
    Inherits="DispatchCrude.Site.Orders.Orders_Inquiry" ValidateRequest="false" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="contentMain" ContentPlaceHolderID="MainContent" runat="server">
    <script>
        $("#ctl00_ctl00_EntityCaption").html("Report Center");
    </script>
    <style>
        .btn-narrow {
            padding-left: 6px;
            padding-right: 6px;
        }
    </style>
    <div class="tabbable TabRepaint">
        <ul class="nav nav-tabs" id="tabmenu" runat="server">
            <!--Tabs will print here (see code behind)-->
        </ul>
        <div class="tab-content">
            <div class="tab-pane in active">

                <div class="leftpanel">
                    <asp:Panel ID="panelReport" runat="server" CssClass="well with-header">
                        <div class="header bordered-blue">Reports</div>
                        <div class="Entry">
                            <telerik:RadComboBox ID="rcbUserReport" runat="server" AllowCustomText="false" ShowDropDownOnTextboxClick="true"
                                                 DataSourceID="dsUserReport" DataTextField="Name" DataValueField="ID" MaxLength="30"
                                                 Width="100%" AutoPostBack="true"
                                                 OnItemDataBound="rcbUserReport_ItemDataBound" OnSelectedIndexChanged="rcbUserReport_SelectedIndexChanged" />
                        </div>
                        <div class="spacer5px"></div>
                        <div class="center">
                            <asp:Button ID="cmdLoadReport" runat="server" Text="Load" CssClass="floatLeft btn btn-blue shiny NOAJAX"
                                        UseSubmitBehavior="false" OnClick="cmdLoadReport_Click" />
                            <asp:Button ID="cmdShowAdvancedPanel" runat="server" Text="Advanced" CssClass="btn btn-blue shiny"
                                        UseSubmitBehavior="false" OnClick="cmdShowAdvancedPanel_Click" />
                            <asp:Button ID="cmdDeleteReport" runat="server" Text="Delete" CssClass="floatRight btn btn-blue shiny"
                                        OnClick="cmdDeleteReport_Click" OnClientClicked="cmdDeleteReport_ClientClicked" Enabled="false" />
                        </div>
                        <div>
                            <asp:Label runat="server" ID="lblReportError" CssClass="NullValidator" Visible="false" />
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="panelAdvanced" runat="server" Visible="false" CssClass="well with-header">
                        <div class="header bordered-yellow">Advanced</div>
                        <div class="Entry">
                            <asp:Label ID="lblNewReportHeader" runat="server" Text="New Report Name" AssociatedControlID="txtNewReportName" />
                            <asp:TextBox ID="txtNewReportName" runat="server" MaxLength="50" Width="100%" class="NewReportName" />
                        </div>
                        <div class="spacer5px"></div>
                        <div class="center">
                            <asp:Button ID="cmdCloneReport" runat="server" Text="Clone" CssClass="btn btn-blue shiny NOAJAX"
                                        UseSubmitBehavior="false" OnClick="cmdCloneReport_Click" CausesValidation="true" ValidationGroup="NewReport" />
                            <asp:Button ID="cmdAddNewReport" runat="server" Text="Add New" CssClass="btn btn-blue shiny NOAJAX"
                                        UseSubmitBehavior="false" OnClick="cmdAddNewReport_Click" CausesValidation="true" ValidationGroup="NewReport" />
                            <asp:Button ID="cmdRenameReport" runat="server" Text="Rename" CssClass="btn btn-blue shiny"
                                        UseSubmitBehavior="false" OnClick="cmdRenameReport_Click" CausesValidation="true" ValidationGroup="NewReport" />
                        </div>
                        <div class="spacer10px"></div>
                        <div class="Entry">
                            <asp:Label ID="lblUserNames" runat="server" Text="User Name(s)" AssociatedControlID="rtbUserNames" />
                            <telerik:RadTextBox id="rtbUserNames" runat="server" MaxLength="8000" EmptyMessage="(global if empty)" Width="100%" />
                            <div class="spacer5px"></div>
                            <div class="center">
                                <asp:Button ID="cmdUpdateUserNames" runat="server" text="Update" CssClass="btn btn-blue shiny"
                                            UseSubmitBehavior="false" OnClick="cmdUpdateUserNames_Click" CausesValidation="false" />                                
                            </div>
                            <asp:Panel id="panelXML" runat="server" Visible="false">
                                <div class="spacer20px"></div>
                                <div class="center">
                                    <b>Import/Export Report Design File</b>
                                    <div class="spacer5px"></div>
                                    <asp:Button ID="cmdExportXml" runat="server" text="Export XML" CssClass="btn btn-blue shiny btn-narrow NOAJAX"
                                                UseSubmitBehavior="false" OnClick="cmdExportXml_Click" CausesValidation="false" />
                                    <input type="button" id="cmdImportXmlStart" class="btn btn-blue shiny btn-narrow NOAJAX" value="Import XML" disabled />
                                </div>
                            </asp:Panel>                            
                        </div>
                        <asp:RequiredFieldValidator id="rfvNewReportName" runat="server" ValidationGroup="NewReport" ControlToValidate="txtNewReportName"
                                                    Text="Report Name is required" CssClass="NullValidator" Display="Dynamic" />
                    </asp:Panel>
                    <asp:Panel ID="panelExport" runat="server" DefaultButton="cmdExport" CssClass="well with-header">
                        <div class="header bordered-green">Export</div>
                        <asp:Panel id="panelExportHeader" runat="server" Width="100%" HorizontalAlign="Center">
                            <asp:Label ID="lblNewReport" runat="server" Text="File Export Details" style="font-weight:bold" />
                        </asp:Panel>
                        <div class="spacer5px"></div>
                        <div>
                            <div class="Entry floatLeft-date-row">
                                <asp:Label ID="lblStartDate" runat="server" Text="Start Date" AssociatedControlID="rdpStart" CssClass="Entry" />
                                <telerik:RadDatePicker ID="rdpStart" runat="server" Width="100px">
                                    <DateInput runat="server" DateFormat="M/d/yyyy" />
                                    <DatePopupButton Enabled="true" />
                                </telerik:RadDatePicker>
                            </div>
                            <div class="Entry floatRight-date-row">
                                <asp:Label ID="lblEndDate" runat="server" Text="End Date" AssociatedControlID="rdpEnd" CssClass="Entry" />
                                <telerik:RadDatePicker ID="rdpEnd" runat="server" Width="100px">
                                    <DateInput runat="server" DateFormat="M/d/yyyy" />
                                    <DatePopupButton />
                                </telerik:RadDatePicker>
                            </div>
                        </div>
                        <br /><br /><br />
                        <div style="height: 5px;" class="clear"></div>
                        <div class="Entry">
                            <asp:Label ID="lblFileName" runat="server" Text="Export File Name" AssociatedControlID="rtxFileName" CssClass="Entry" />
                            <telerik:RadTextBox ID="rtxFileName" runat="server" MaxLength="100" Width="100%" ValidationGroup="vgFileName" />
                            <asp:RequiredFieldValidator ID="rfvFileName" runat="server" ControlToValidate="rtxFileName"
                                                        ValidationGroup="vgFileName" Text="Export file name is required." CssClass="NullValidator" Display="Dynamic" />
                            <asp:RegularExpressionValidator ID="revFileName" runat="server" ControlToValidate="rtxFileName"
                                                            ValidationExpression="^[a-zA-Z0-9_\s\-\.]+$"
                                                            ValidationGroup="vgFileName" Text="Invalid file name was provided." CssClass="NullValidator" Display="Dynamic" />
                            <asp:RegularExpressionValidator ID="revFileName_NoExtension" runat="server" ControlToValidate="rtxFileName"
                                                            ValidationExpression="[^\.]+$"
                                                            ValidationGroup="vgFileName" Text="Do not enter a filename extension (.xlsx will automatically be used)." CssClass="NullValidator" Display="Dynamic" />
                        </div>
                        <div class="spacer5px"></div>
                        <div class="center">
                            <asp:Button ID="cmdExport" runat="server" Text="Export" CssClass="btn btn-blue shiny NOAJAX"
                                        CausesValidation="true" ValidationGroup="vgFileName"
                                        UseSubmitBehavior="false" OnClick="cmdExport_Click" />
                        </div>
                        <asp:Panel runat="server" ID="panelExportError" Visible="false">
                            <asp:Label runat="server" ID="lblExportError" CssClass="NullValidator" />
                        </asp:Panel>
                    </asp:Panel>
                </div>
                <div id="gridArea" style="height:100%">
                    <asp:ValidationSummary ID="validationSummaryMain" runat="server" DisplayMode="BulletList" Enabled="true" EnableClientScript="true" CssClass="NullValidator" />
                    <telerik:RadGrid ID="rgMain" runat="server" EnableHeaderContextMenu="false" ClientSettings-AllowColumnsReorder="false"
                                     Height="800" CssClass="GridRepaint" CellSpacing="0" GridLines="None" EnableEmbeddedSkins="true" Skin="Vista" AlternatingItemStyle-BackColor="#dcf2fc"
                                     AllowSorting="True" EnableViewState="true" AllowFilteringByColumn="false"
                                     AllowPaging="True" ShowStatusBar="false" MasterTableView-EditMode="InPlace"
                                     OnItemCommand="grid_ItemCommand" PageSize='<%# Settings.DefaultPageSize %>'
                                     OnDataBound="grid_DataBound" OnItemDataBound="grid_ItemDataBound" OnNeedDataSource="grid_NeedDataSource">
                        <ClientSettings>
                            <Scrolling AllowScroll="True" UseStaticHeaders="True" FrozenColumnsCount="0" />
                        </ClientSettings>
                        <MasterTableView AutoGenerateColumns="False" DataKeyNames="ID" CommandItemDisplay="Top">
                            <CommandItemSettings ShowAddNewRecordButton="true" AddNewRecordText="Add New Report Column" />
                            <RowIndicatorColumn Visible="True">
                                <HeaderStyle Width="20px"></HeaderStyle>
                            </RowIndicatorColumn>
                            <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                <HeaderStyle Width="20px"></HeaderStyle>
                            </ExpandCollapseColumn>
                            <Columns>
                                <telerik:GridTemplateColumn UniqueName="ActionColumn" HeaderStyle-Width="75px" AllowFiltering="false">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" CssClass="btn btn-xs btn-default shiny" CommandName="Edit" ImageUrl="~/images/edit.png" CausesValidation="false" />
                                        <asp:ImageButton runat="server" CssClass="btn btn-xs btn-default shiny" id="cmdDelete" CommandName="Delete" ImageUrl="~/images/delete.png"
                                                         OnClientClick="return confirm('Permanently delete?');" CausesValidation="false" />
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:ImageButton runat="server" CssClass="btn btn-xs btn-default shiny" CommandName="Update" ImageUrl="~/images/apply_imageonly.png" CausesValidation="true" />
                                        <asp:ImageButton runat="server" CssClass="btn btn-xs btn-default shiny" CommandName="Cancel" ImageUrl="~/images/cancel_imageonly.png" CausesValidation="false" />
                                    </EditItemTemplate>
                                    <InsertItemTemplate>
                                        <asp:ImageButton runat="server" CssClass="btn btn-xs btn-default shiny" CommandName="PerformInsert" ImageUrl="~/images/apply_imageonly.png" CausesValidation="true" />
                                        <asp:ImageButton runat="server" CssClass="btn btn-xs btn-default shiny" CommandName="Cancel" ImageUrl="~/images/cancel_imageonly.png" CausesValidation="false" />
                                    </InsertItemTemplate>
                                </telerik:GridTemplateColumn>

                                <telerik:GridTemplateColumn DataField="ReportColumnID" UniqueName="BaseCaption" HeaderText="Data Field"
                                                            SortExpression="BaseCaption" HeaderStyle-Width="150px">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDataField" runat="server" Text='<%# Eval("BaseCaption") %>' />
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <telerik:RadDropDownTree ID="rddtReportColumn" runat="server" Width="98%"
                                                                 DataSourceID="dsReportColumnTree" DataFieldID="ID" DataTextField="Caption" DataFieldParentID="ParentID" DataValueField="ID"
                                                                 SelectedValue='<%#Bind("ReportColumnID") %>' DefaultValue="88"
                                                                 AutoPostBack="true" OnEntryAdded="rddtReportColumn_EntryAdded" OnDataBound="rddtReportColumn_DataBound" OnNodeDataBound="rddtReportColumn_NodeDataBound"
                                                                 OnClientEntryAdding="rddtReportColumn_ClientEntryAdding"
                                                                 ExpandNodeOnSingleClick="true"
                                                                 EnableFiltering="true" FilterSettings-Filter="Contains" FilterSettings-MinFilterLength="0" FilterSettings-Highlight="Matches"
                                                                 DropDownSettings-OpenDropDownOnLoad="false" DropDownSettings-CloseDropDownOnSelection="true"
                                                                 DropDownSettings-AutoWidth="Disabled" DropDownSettings-Width="250px" />
                                    </EditItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="Caption" UniqueName="Caption" HeaderText="Caption"
                                                         HeaderStyle-Width="120px" ItemStyle-Width="120px" />
                                <telerik:GridCheckBoxColumn DataField="Export" UniqueName="Export" HeaderText="Export?" HeaderStyle-Width="50px" DefaultInsertValue="true" />
                                <telerik:GridNumericColumn DataField="SortNum" UniqueName="SortNum" HeaderText="Position" DataType="System.Int32"
                                                           HeaderStyle-Width="60px" />
                                <telerik:GridBoundColumn DataField="DataFormat" UniqueName="DataFormat" HeaderText="Data Format" MaxLength="4000" HtmlEncode="true"
                                                         HeaderStyle-Width="90px" ItemStyle-Width="98%" />
                                <telerik:GridNumericColumn DataField="DataSort" UniqueName="DataSort" HeaderText="Data Sort" DataType="System.Int16" MaxValue="25"
                                                           HeaderStyle-Width="60px" />
                                <telerik:GridDropDownColumn DataField="GroupingFunctionID" UniqueName="GroupingFunctionID" HeaderText="Grouping"
                                                            DataSourceID="dsReportGroupingFunction" ListValueField="ID" ListTextField="Name" DefaultInsertValue="1"
                                                            HeaderStyle-Width="125px" />
                                <telerik:GridTemplateColumn DataField="FilterOperatorID" HeaderText="Filter Type" SortExpression="FilterOperator" DefaultInsertValue="1"
                                                            HeaderStyle-Width="90px">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFilterOperator" runat="server" Text='<%# Eval("FilterOperator") %>' />
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <telerik:RadComboBox ID="rcbFilterOperator" runat="server"
                                                             DataValueField="ID" DataTextField="DataField"
                                                             SelectedValue='<%# Bind("FilterOperatorID") %>'
                                                             OnItemDataBound="rcbFilterOperator_ItemDataBound" Width="95%"
                                                             OnDataBound="rcbFilterOperator_DataBound"
                                                             OnSelectedIndexChanged="rcbFilterOperator_SelectedIndexChanged" AutoPostBack="true">
                                        </telerik:RadComboBox>
                                    </EditItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn DataField="FilterValues" UniqueName="FilterValues" HeaderText="Filter Values" SortExpression="FilterValues" HeaderStyle-Width="200px">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFilterValues" runat="server" Text='<%# Eval("FilterValues") %>' />
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:Panel ID="panelFEV_SingleValue" runat="server" Width="100%" Visible="false">
                                            <telerik:RadComboBox ID="rcbSingleValue1" runat="server" Width="90%" EnableCheckAllItemsCheckBox="true"
                                                                 AllowCustomText="true" DataValueField="ID" DataTextField="Name" />
                                            <asp:CustomValidator ID="cvSingleValue1" runat="server" OnServerValidate="cvFilterValue_ServerValidate"
                                                                 Text="*" ErrorMessage="Invalid Filter value provided" CssClass="NullValidator" />
                                        </asp:Panel>
                                        <asp:Panel ID="panelFEV_TwoValues" runat="server" Width="100%" Visible="false">
                                            <telerik:RadComboBox ID="rcbTwoValues1" runat="server" Width="40%" EnableCheckAllItemsCheckBox="true"
                                                                 AllowCustomText="true" DataValueField="ID" DataTextField="Name" />
                                            <asp:CustomValidator ID="cvTwoValues1" runat="server"
                                                                 ControlToValidate="rcbTwoValues1" OnServerValidate="cvFilterValue_ServerValidate"
                                                                 Text="*" ErrorMessage="Invalid Filter value 1 provided" CssClass="NullValidator" />
                                            <asp:Literal ID="litBetween" runat="server" Text=" - " />
                                            <telerik:RadComboBox ID="rcbTwoValues2" runat="server" Width="40%" EnableCheckAllItemsCheckBox="true"
                                                                 AllowCustomText="true" DataValueField="ID" DataTextField="Name" />
                                            <asp:CustomValidator ID="cvTwoValues2" runat="server"
                                                                 ControlToValidate="rcbTwoValues2" OnServerValidate="cvFilterValue_ServerValidate"
                                                                 Text="*" ErrorMessage="Invalid Filter value 2 provided" CssClass="NullValidator" />
                                        </asp:Panel>
                                    </EditItemTemplate>
                                </telerik:GridTemplateColumn>

                                <telerik:GridBoundColumn DataField="ID" UniqueName="ID" Visible="false" Display="false" DataType="System.Int32"
                                                         ForceExtractValue="Always" ReadOnly="true" />
                            </Columns>
                            <PagerStyle AlwaysVisible="True" />
                        </MasterTableView>
                        <HeaderStyle Wrap="False" />
                        <PagerStyle AlwaysVisible="True" />
                    </telerik:RadGrid>
                </div>
                <telerik:RadScriptBlock runat="server">
                    <script type="text/javascript">
                        // ensure these controls are "wired" at initial page load
                        $(function () {
                            contentResponseEnd();
                        })
                        function contentResponseEnd(source, args) {
                            /* done here since event handlers are "un-wired" on each webform "ajax" postback */
                            $(".NewReportName").keyup(newReportName_KeyUp);
                            $("#cmdImportXmlStart").click(cmdImportXmlStart_Click);
                            /* if the #windowXmlImport kendoWindow is present, then we just returned from a Xml Import operation, so cancel the window now */
                            if ($("#windowXmlImport").data("kendoWindow")) {
                                cmdImportXmlCancel_Click();
                            }
                        }

                        function rddtReportColumn_ClientEntryAdding(sender, eventArgs) {
                            if (eventArgs.get_entry().get_value() < 0) {
                                eventArgs.set_cancel(true);
                            }
                        };
                        function cmdDeleteReport_ClientClicked(button, args) {
                            if (window.confirm("Are you sure you want to delete the report?")) {
                                button.set_autoPostBack(true);
                            }
                            else {
                                button.set_autoPostBack(false);
                            }
                        };
                        function newReportName_KeyUp() {
                            var enabled = $(".NewReportName").val();
                            $("#cmdImportXmlStart").prop("disabled", !enabled);
                        }
                        function cmdImportXmlStart_Click() {
                            debugger;
                            $("#xmlImportFile").kendoUpload({
                                multiple: false
                                , async: {
                                    saveUrl: "/ReportCenter/SaveImportXmlFile"
                                    , autoUpload: true
                                }
                            });
                            $("#cmdImportXmlCancel").click(cmdImportXmlCancel_Click);
                            $("#windowXmlImport").kendoWindow({ modal: true, resizable: false }).data("kendoWindow").title("Import Xml File").center().open();
                        }
                        function cmdImportXmlCancel_Click() {
                            debugger;
                            var ku = $("#xmlImportFile").data("kendoUpload");
                            if (ku) {
                                //ku.clearFile(function () { return true; });
                                ku.destroy(); /* TELERIK: why does this not work!! */
                                // brute force replace of the control - so if the window is re-displayed it won't "double" create the kendoUpload
                                $("#divXmlImportFile").html("<input name='xmlImportFile' id='xmlImportFile' type='file' />");
                            }
                            $("#windowXmlImport").data("kendoWindow").close();
                        }
                    </script>
                </telerik:RadScriptBlock>
                <blc:RadGridDBCtrl ID="dbcMain" runat="server"
                                   ControlID="rgMain"
                                   UpdateTableName="tblUserReportColumnDefinition" OnGetUnboundChangedValues="dbcMain_GetUnboundChangedValues" />
                <blac:DBDataSource ID="dsReportColumnTree" runat="server" SelectCommand="SELECT * FROM dbo.fnReportColumnTree(@UserName)">
                    <SelectParameters>
                        <asp:Parameter Name="UserName" DefaultValue="" DbType="String" />
                    </SelectParameters>
                </blac:DBDataSource>
                <blac:DBDataSource ID="dsUserReport" runat="server"
                                   SelectCommand="SELECT * FROM viewUserReportDefinition WHERE (UserNames IS NULL OR @UserName = 'ALL' OR @UserName IN (SELECT Value FROM dbo.fnSplitCSV(UserNames))) ORDER BY Name">
                    <SelectParameters>
                        <asp:Parameter Name="UserName" DefaultValue="" DbType="String" />
                    </SelectParameters>
                </blac:DBDataSource>
                <blac:DBDataSource ID="dsReportGroupingFunction" runat="server" SelectCommand="SELECT ID, Name FROM tblReportGroupingFunction ORDER BY Name" />
            </div>
        </div>
        <div style="display: none;">
            <div id="windowXmlImport" class="window">
                <table>
                    <tr>
                        <td>
                            <div id="divXmlImportFile">
                                <input name="xmlImportFile" id="xmlImportFile" type="file" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Button ID="cmdImportXml" runat="server" text="Import" CssClass="btn btn-blue shiny"
                                UseSubmitBehavior="false" OnClick="cmdImportXml_Click" CausesValidation="false" />
                            <button id="cmdImportXmlCancel" class="btn btn-blue shiny">Cancel</button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
