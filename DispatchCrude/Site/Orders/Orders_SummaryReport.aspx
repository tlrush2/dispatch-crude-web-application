﻿<%@  Title="Reports" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Orders_SummaryReport.aspx.cs"
    Inherits="DispatchCrude.Site.Orders.Orders_SummaryReport" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script>
        $("#ctl00_ctl00_EntityCaption").html("Order Summary");

        function cmdPopulate_Click() {
            $(".tabbable").removeClass("TabRepaint");
        }
    </script>
    <div class="tabbable TabRepaint">
        <ul class="nav nav-tabs" id="tabmenu" runat="server">
            <!--Tabs will print here (see code behind)-->
        </ul>
        <div class="tab-content">
            <div class="tab-pane in active">

                <div class="leftpanel well with-header">
                    <div class="header bordered-blue">Filters</div>
                    <asp:Panel ID="panelGenerate" runat="server" DefaultButton="cmdPopulate">
                        <div>
                            <div class="Entry floatLeft-date-row">
                                <asp:Label ID="lblStartDate" runat="server" Text="Start Date" AssociatedControlID="rdpStart" CssClass="Entry" />
                                <telerik:RadDatePicker ID="rdpStart" runat="server" Width="100px">
                                    <DateInput runat="server" DateFormat="M/d/yyyy" />
                                    <DatePopupButton Enabled="true" />
                                </telerik:RadDatePicker>
                            </div>
                            <div class="Entry floatRight-date-row">
                                <asp:Label ID="lblEndDate" runat="server" Text="End Date" AssociatedControlID="rdpEnd" CssClass="Entry" />
                                <telerik:RadDatePicker ID="rdpEnd" runat="server" Width="100px">
                                    <DateInput runat="server" DateFormat="M/d/yyyy" />
                                    <DatePopupButton />
                                </telerik:RadDatePicker>
                            </div>
                            <br /><br /><br />
                            <div class="Entry floatLeft">
                                <asp:Label ID="lblUOM" runat="server" Text="UOM" AssociatedControlID="rdpEnd" CssClass="Entry" />
                                <telerik:RadComboBox ID="rcbUOM" runat="server" Width="150px" DataSourceID="dsUOM" DataValueField="ID" DataTextField="Name" />
                            </div>
                        </div>
                        <div class="clear"></div>
                        <div class="spacer5px"></div>
                        <asp:Label ID="lblEntities" runat="server" Text="Check 1 or more (starred) boxes to group/sort by that option" AssociatedControlID="cblEntities" />
                        <asp:CheckBoxList id="cblEntities" runat="server" TextAlign="Right">
                            <asp:ListItem Text="Carrier Type *" Value="CarrierType" />
                            <asp:ListItem Text="Product Group *" Value="ProductGroup" />
                            <asp:ListItem Text="Product *" Value="Product" />
                            <asp:ListItem Text="Carrier *" Value="Carrier" />
                            <asp:ListItem Text="Driver *" Value="Driver" />
                            <asp:ListItem Text="Truck *" Value="Truck" />
                            <asp:ListItem Text="Trailer *" Value="Trailer" />
                            <asp:ListItem Text="UOM" Value="UOM" Selected="True" Enabled="false" />
                            <asp:ListItem Text="Order Qty" Value="OrderQty" Selected="True" Enabled="false" />
                            <asp:ListItem Text="Net Vol" Value="NetVol" Selected="True" Enabled="false" />
                        </asp:CheckBoxList>
                        <div class="spacer5px"></div>
                        <div class="Entry">
                            <asp:Button ID="cmdPopulate" runat="server" Text="Populate" CssClass="btn btn-blue shiny" OnClick="cmdPopulate_Click" />
                            <asp:Button ID="cmdExport" runat="server" Text="Export" CssClass="btn btn-blue shiny" OnClick="cmdExport_Click" style="float:right" />
                        </div>
                    </asp:Panel>
                </div>
                <div id="gridArea" style="height: 100%; min-height: 400px;">
                    <asp:PlaceHolder id="phGrid" runat="server">
                    </asp:PlaceHolder>
                </div>
                <blac:DBDataSource ID="dsUOM" runat="server" SelectCommand="SELECT ID, Name FROM tblUOM ORDER BY ID" />
            </div>
        </div>
    </div>
</asp:Content>