﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CtrlOrderEdit.ascx.cs" Inherits="DispatchCrude.Orders.CtrlOrderEdit" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<style type="text/css">
    .rgEditForm {
            width: auto !important;
    }
    .rgEditForm > div + div,
    .RadGrid .rgEditForm {
            height: auto !important;
    }
    .rgEditForm > div > table{
            height: 100%;
    }
    .rgEditForm > div > table > tbody > tr > td{
            padding: 4px 10px;
    }
    .H2S
    {
        color: red;
    }
    .Deleted
    {
        color: gray;
    }
    .col
    {
            margin: 0;
            padding: 0 5px 0 0;
            line-height: 14px;
            float: left;
    }
    .oCol1
    {
        width: 80px;
    }
    .oCol2 
    {
        width: 130px;
    }
</style>
<telerik:RadScriptBlock runat="server" >
    <script type="text/javascript">
        function validateItemSpecified(sender, args) {
            //invalid if first character is a "("
            debugger;
            args.IsValid = args.Value != null && args.Value != "" && !args.Value.startsWith("(");
        };
        function validateOrderAssignedStatus(sender, args) {
            debugger;
            args.IsValid = true;    // default value
            var status = $find("<%= rcbOrderStatus.ClientID %>").get_value();
            if (status == 1) { // Assigned
                var carrierID = $find("<%= rcbCarrier.ClientID %>").get_value();
                args.IsValid = carrierID > 0;
            }
        };
        function validateOrderDispatchedStatus(sender, args) {
            debugger;
            args.IsValid = true;    // default value
            var status = $find("<%= rcbOrderStatus.ClientID %>").get_value();
            if (status == 2) { // Dispatched
                var carrierID = $find("<%= rcbCarrier.ClientID %>").get_value();
                var driverID = $find("<%= rcbDriver.ClientID %>").get_value();
                args.IsValid = carrierID > 0 && driverID > 0;
            }
        };
    </script>
</telerik:RadScriptBlock>
<asp:Table id="tblMain" runat="server" border="0" style="border-collapse: collapse">
    <asp:TableRow runat="server" >
        <asp:TableCell runat="server" ColumnSpan="2" >
            <asp:ValidationSummary ID="validationSummaryMain" runat="server" DisplayMode="BulletList" Enabled="true" EnableClientScript="true"
                ValidationGroup="OrderEdit"
                CssClass="NullValidator" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow runat="server" >
        <asp:TableCell runat="server" >
            <asp:Table runat="server" id="tblLeft" border="0" style="margin-right: 0px">
                <asp:TableRow runat="server" >
                    <asp:TableCell runat="server" >
                        Priority/Status:
                    </asp:TableCell>
                    <asp:TableCell runat="server" >
                        <asp:HiddenField ID="hfID" runat="server" />
                        <blac:PriorityDropDownList CssClass="btn-xs" ID="ddPriority" runat="server" Width="60px" />&nbsp;
                        <telerik:RadComboBox ID="rcbOrderStatus" runat="server" Width="175px" ClientIDMode="Static" 
                                DataSourceID="dsOrderStatus" DataTextField="OrderStatus" DataValueField="ID" 
                                CausesValidation="true" ValidationGroup="OrderEdit"
                                AutoPostBack="true" OnSelectedIndexChanged="rcbOrderStatus_SelectedIndexChanged" />
                                <%--SelectedValue='<%# Bind("PrintStatusID") %>' />--%>
                        <asp:CustomValidator ID="cvStatusAssign" runat="server" 
                            OnServerValidate="cvStatus_AssignedValidate" ClientValidationFunction="validateOrderAssignedStatus" 
                            ControlToValidate="rcbOrderStatus" ValidationGroup="OrderEdit"
                            Text="*" ErrorMessage="Carrier is required for the ASSIGNED order status" CssClass="NullValidator" />
                        <asp:CustomValidator ID="cvStatusDispatched" runat="server" 
                            OnServerValidate="cvStatus_DispatchedValidate" ClientValidationFunction="validateOrderDispatchedStatus" 
                            ControlToValidate="rcbOrderStatus" ValidationGroup="OrderEdit"
                            Text="*" ErrorMessage="Carrier & Driver are required for the DISPATCHED order status" CssClass="NullValidator" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow runat="server" >
                    <asp:TableCell runat="server" >
                        Origin:
                    </asp:TableCell>
                    <asp:TableCell runat="server" >
                        <telerik:RadComboBox id="rcbOrigin" runat="server" AllowCustomText="false" Filter="Contains" Width="250px" 
                            DataSourceID="dsOrigin" DataTextField="LeaseNum_Name" DataValueField="ID" 
                            CausesValidation="false" ValidationGroup="OrderEdit"
                            DropDownAutoWidth="Enabled" EmptyMessage="(Select Origin)"
                            AutoPostBack="true" OnItemDataBound="rcbOrigin_ItemDataBound" OnSelectedIndexChanged="rcbOrigin_SelectedIndexChanged" >
                            <%--<HeaderTemplate>
                                <ul>
                                    <li class="col oCol1">Lease #</li>
                                    <li class="col oCol2">Name</li>
                                </ul>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <ul>
                                    <li class="col oCol1">
                                        <%# DataBinder.Eval(Container.DataItem, "LeaseNum") %></li>
                                    <li class="col oCol2">
                                        <%# DataBinder.Eval(Container.DataItem, "Name") %></li>
                                </ul>
                            </ItemTemplate>--%>
                        </telerik:RadComboBox>
                        <asp:RequiredFieldValidator ID="rfvOrigin" runat="server" ControlToValidate="rcbOrigin"
                            ValidateEmptyText="true" ValidationGroup="OrderEdit" InitialValue=""
                            Text="*" ErrorMessage="An Origin must be specified" CssClass="NullValidator" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow runat="server" >
                    <asp:TableCell runat="server" >
                        Product:
                    </asp:TableCell>
                    <asp:TableCell runat="server" >
                        <telerik:RadComboBox id="rcbProduct" runat="server" Width="250px"
                            DataSourceID="dsProduct" DataTextField="Name" DataValueField="ID" 
                            CausesValidation="true" ValidationGroup="OrderEdit"
                            AutoPostBack="true" OnSelectedIndexChanged="rcbProduct_SelectedIndexChanged" />
                            <%--SelectedValue='<%# Bind("ProductID") %>' />--%>
                        <asp:CustomValidator ID="cvProduct" runat="server" ControlToValidate="rcbProduct" ValidationGroup="OrderEdit"
                            ValidateEmptyText="true" ClientValidationFunction="validateItemSpecified"
                            Text="*" ErrorMessage="A Product must be specified" CssClass="NullValidator" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow runat="server" >
                    <asp:TableCell runat="server" >
                        Destination:
                    </asp:TableCell>
                    <asp:TableCell runat="server" >
                        <telerik:RadComboBox ID="rcbDestination" runat="server" Width="250px" 
                            DataSourceID="dsDestination" DataTextField="Name" DataValueField="ID"
                            CausesValidation="true" ValidationGroup="OrderEdit"
                            AutoPostBack="true"  />
                            <%--SelectedValue='<%# Bind("DestinationID") %>'/>--%>
                        <asp:CustomValidator ID="cvDestination" runat="server" ControlToValidate="rcbDestination" ValidationGroup="OrderEdit"
                            ValidateEmptyText="true" ClientValidationFunction="validateItemSpecified"
                            Text="*" ErrorMessage="A Destination must be specified" CssClass="NullValidator" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow runat="server" >
                    <asp:TableCell runat="server" >
                        Consignee:
                    </asp:TableCell>
                    <asp:TableCell runat="server" >
                        <telerik:RadComboBox ID="rcbConsignee" runat="server" Width="250px" 
                            DataSourceID="dsConsignee" DataTextField="Name" DataValueField="ID" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow runat="server" >
                    <asp:TableCell runat="server" >
                        <asp:Label ID="lblCustomer" runat="server" Text="Shipper:" />
                    </asp:TableCell>
                    <asp:TableCell runat="server" >
                        <telerik:RadComboBox ID="rcbCustomer" runat="server" Width="250px" 
                            DataSourceID="dsCustomer" DataTextField="FullName" DataValueField="ID"
                            CausesValidation="true" ValidationGroup="OrderEdit"
                            AutoPostBack="true" OnSelectedIndexChanged="rcbCustomer_SelectedIndexChanged" />
                            <%--SelectedValue='<%# Bind("CustomerID") %>' />--%>
                        <asp:CustomValidator ID="cvCustomer" runat="server" ControlToValidate="rcbCustomer" ValidationGroup="OrderEdit"
                            ValidateEmptyText="true" ClientValidationFunction="validateItemSpecified"
                            Text="*" ErrorMessage="A valid Shipper must be selected" CssClass="NullValidator" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow runat="server" >
                    <asp:TableCell runat="server" >
                        Carrier:
                    </asp:TableCell>
                    <asp:TableCell runat="server" >
                        <telerik:RadComboBox ID="rcbCarrier" runat="server" Width="250px" 
                            DataSourceID="dsCarrier" DataTextField="FullName" DataValueField="ID" 
                            CausesValidation="false" ValidationGroup="OrderEdit"
                            AutoPostBack="true" OnSelectedIndexChanged="rcbCarrier_SelectedIndexChanged" />
                            <%--SelectedValue='<%# Bind("CarrierID") %>' />--%>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow runat="server" >
                    <asp:TableCell runat="server" >
                        Driver:
                    </asp:TableCell>
                    <asp:TableCell runat="server" >
                        <telerik:RadComboBox ID="rcbDriver" runat="server" Width="250px" 
                            DataSourceID="dsDriver" DataTextField="FullName" DataValueField="ID" 
                            CausesValidation="true" ValidationGroup="OrderEdit"
                            AutoPostBack="true" OnItemDataBound="rcbDriver_ItemDataBound" OnSelectedIndexChanged="rcbDriver_SelectedIndexChanged" />
                            <%--SelectedValue='<%# Bind("DriverID") %>' />--%>
                    </asp:TableCell>
                </asp:TableRow>                
            </asp:Table>
        </asp:TableCell>
        <asp:TableCell runat="server" >
            <asp:Table runat="server" id="tblRight" BorderStyle="None" >
                <asp:TableRow runat="server" >
                    <asp:TableCell runat="server" >
                        Due Date:
                    </asp:TableCell>
                    <asp:TableCell runat="server" >
                        <telerik:RadDatePicker ID="rdpDueDate" runat="server" Width="150px" AutoPostback="true" OnSelectedDateChanged="rdpDueDate_Changed" />
                            <%--DateInput-DateFormat="M/d/yy" DbSelectedDate='<%# Bind("DueDate") %>' />--%>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow runat="server" >
                    <asp:TableCell runat="server" >
                        Ticket Type:
                    </asp:TableCell>
                    <asp:TableCell runat="server" >
                        <telerik:RadComboBox ID="rcbTicketType" runat="server" 
                            DataSourceID="dsTicketType" DataTextField="TicketType" DataValueField="ID" Width="150px" 
                            AutoPostBack="true" OnSelectedIndexChanged="rcbTicketType_SelectedIndexChanged" />
                            <%--SelectedValue='<%# Bind("TicketTypeID") %>' />--%>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow runat="server" >
                    <asp:TableCell runat="server" >
                        Tank ID:
                    </asp:TableCell>
                    <asp:TableCell runat="server" >
                        <telerik:RadComboBox ID="rcbOriginTank" runat="server" 
                            DataSourceID="dsOriginTank" DataTextField="TankNum" DataValueField="ID" Width="150px" ValidationGroup="OrderEdit"
                            AutoPostBack="true" OnSelectedIndexChanged="rcbOriginTank_SelectedIndexChanged" CausesValidation="false" />
                            <%--SelectedValue='<%# Bind("OriginTankID") %>' />--%>
                        <asp:CustomValidator ID="cvOriginTank" runat="server" ControlToValidate="rcbOriginTank" ValidationGroup="OrderEdit"
                            ValidateEmptyText="true" 
                            OnServerValidate="cvOriginTank_ServerValidate" Display="Dynamic" 
                            Text="*" ErrorMessage="Tank selection must be made" CssClass="NullValidator" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow runat="server" >
                    <asp:TableCell runat="server" >
                        Tank Details:
                    </asp:TableCell>
                    <asp:TableCell runat="server" >
                        <asp:TextBox runat="server" ID="txtOriginTankNum" MaxLength="20" Width="150px" />
                            <%--Text='<%# Bind("OriginTankNum") %>' />--%>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow runat="server" >
                    <asp:TableCell runat="server" >
                        BOL #:
                    </asp:TableCell>
                    <asp:TableCell runat="server" >
                        <asp:TextBox runat="server" ID="txtOriginBOLNum" MaxLength="20" Width="150px" />
                            <%--Text='<%# Bind("OriginBOLNum") %>' />--%>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow runat="server" >
                    <asp:TableCell runat="server" >
                        Origin UOM:
                    </asp:TableCell>
                    <asp:TableCell runat="server" >
                        <telerik:RadComboBox ID="rcbOriginUom" runat="server" 
                            DataSourceID="dsUom" DataTextField="Name" DataValueField="ID" Width="150px" 
                            CausesValidation="true" ValidationGroup="OrderEdit" Enabled="false" />
                            <%--SelectedValue='<%# Bind("OriginUomID") %>' />--%>
                        <asp:CustomValidator ID="cvOriginUom" runat="server" ControlToValidate="rcbOriginUom" ValidationGroup="OrderEdit"
                            ValidateEmptyText="true" 
                            ClientValidationFunction="validateItemSpecified" 
                            Text="*" ErrorMessage="An Origin UOM must be selected" CssClass="NullValidator" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow runat="server" >
                    <asp:TableCell runat="server" >
                        Destination UOM:
                    </asp:TableCell>
                    <asp:TableCell runat="server" >
                        <telerik:RadComboBox ID="rcbDestUom" runat="server" 
                            DataSourceID="dsUom" DataTextField="Name" DataValueField="ID" Width="150px" 
                            CausesValidation="true" ValidationGroup="OrderEdit" Enabled="false" />
                            <%--SelectedValue='<%# Bind("DestUomID") %>' />--%>
                        <asp:CustomValidator ID="cbDestUom" runat="server" ControlToValidate="rcbDestUom" ValidationGroup="OrderEdit"
                            ValidateEmptyText="true" 
                            ClientValidationFunction="validateItemSpecified" 
                            Text="*" ErrorMessage="An Destination UOM must be selected" CssClass="NullValidator" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow runat="server" >
        <asp:TableCell runat="server" ColumnSpan="2" >
            <asp:Table runat="server">
                <asp:TableRow runat="server">
                    <asp:TableCell runat="server" Width="45%">
                        <asp:Table runat="server">
                            <asp:TableRow runat="server">
                                <asp:TableCell runat="server">
                                    Shipper PO:
                                </asp:TableCell>
                                <asp:TableCell runat="server">
                                    <asp:TextBox id="txtDispatchConfirmNum" runat="server" MaxLength="30"  ValidationGroup="OrderEdit" />
                                    <asp:RequiredFieldValidator ID="rfvDispatchConfirmNum" runat="server" 
                                        ControlToValidate="txtDispatchConfirmNum" ValidationGroup="OrderEdit" 
                                        Text="!" ErrorMessage="Dispatch Confirmation is required for DISPATCHED orders" CssClass="NullValidator" />
                                    <asp:CustomValidator ID="cvDispatchConfirmNum" runat="server" ControlToValidate="txtDispatchConfirmNum"
                                        ValidationGroup="OrderEdit"
                                        OnServerValidate="cvDispatchConfirmNum_ServerValidate" Display="Dynamic" 
                                        CssClass="NullValidator" Text="*" ErrorMessage="Dispatch Confirm # already assigned to this Shipper" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow runat="server">
                                <asp:TableCell runat="server">
                                    Job #:
                                </asp:TableCell>
                                <asp:TableCell runat="server">
                                    <asp:TextBox id="txtJobNumber" runat="server" MaxLength="20" />                                    
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow runat="server">
                                <asp:TableCell runat="server">
                                    Contract #:
                                </asp:TableCell>
                                <asp:TableCell runat="server">
                                    <asp:TextBox id="txtContractNumber" runat="server" Enabled="false" MaxLength="20" />                                    
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                    <asp:TableCell runat="server" Width="55%">
                        <asp:Table runat="server">
                            <asp:TableRow runat="server">
                                <asp:TableCell runat="server">
                                    Dispatch Notes:
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow runat="server">
                                <asp:TableCell runat="server">
                                    <asp:TextBox id="txtDispatchNotes" runat="server" Width="450px" Height="50px" MaxLength="500" TextMode="MultiLine" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow runat="server" >
        <asp:TableCell runat="server" style="text-align:left" ColumnSpan="2">
            <asp:Button ID="cmdUpdate" runat="server" Text="Update" CssClass="btn btn-blue shiny" CommandName="Update" CausesValidation="true" ValidationGroup="OrderEdit"
                OnClick="UpdateDataItem" 
                Visible='<%# !(DataItem is Telerik.Web.UI.GridInsertionObject) %>'>
            </asp:Button>
            &nbsp;
            <asp:Button ID="cmdCancel" runat="server" Text="Cancel" CssClass="btn btn-blue shiny" CommandName="Cancel" CausesValidation="False" ValidationGroup="OrderEdit" />
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
<asp:HiddenField ID="hfRestrictedRegionID" ClientIDMode="Static" runat="server" value="0" />
<asp:HiddenField ID="hfRestrictedTerminalID" ClientIDMode="Static" runat="server" value="0" />
<blac:DBDataSource ID="dsOrigin" runat="server" 
    SelectCommand="SELECT ID, LeaseNum = isnull(LeaseNum, 'N/A'), Name, LeaseNum_Name = isnull(LeaseNum + ' - ', '') + Name, H2S, SortNum = CASE WHEN DeleteDateUTC IS NULL THEN 0 ELSE 1 END FROM dbo.viewOrigin WHERE (DeleteDateUTC IS NULL OR ID = @InitialID) AND (ID = @InitialID OR @RegionID = 0 OR RegionID IS NULL OR RegionID = @RegionID) AND (ID = @InitialID OR @TerminalID = 0 OR TerminalID IS NULL OR TerminalID = @TerminalID) AND GaugerRequired=0 ORDER BY SortNum, Name">
    <SelectParameters>
        <asp:Parameter Name="InitialID" DefaultValue="0" />
        <asp:ControlParameter Name="RegionID" ControlID="hfRestrictedRegionID" PropertyName="Value" DefaultValue="0" />
        <asp:ControlParameter Name="TerminalID" ControlID="hfRestrictedTerminalID" PropertyName="Value" DefaultValue="0" />
    </SelectParameters>
</blac:DBDataSource>
<blac:DBDataSource ID="dsProduct" runat="server" SelectCommand="SELECT ID, Name, ShortName FROM dbo.tblProduct WHERE ID IN (SELECT ProductID FROM tblOriginProducts WHERE OriginID = @OriginID) UNION SELECT 0, '(Select Product)', '(Select Product)' ORDER BY Name" >
    <SelectParameters>
        <asp:ControlParameter Name="OriginID" ControlID="rcbOrigin" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
    </SelectParameters>
</blac:DBDataSource>
<blac:DBDataSource ID="dsOriginTank" runat="server"
    SelectCommand="SELECT ID, TankNum_Unstrapped AS TankNum, CASE WHEN TankNum='*' THEN 1 ELSE 0 END AS SortNum FROM viewOriginTank WHERE OriginID=@OriginID AND DeleteDateUTC IS NULL UNION SELECT NULL, '(Select Tank)', 0 ORDER BY SortNum, TankNum" >
    <SelectParameters>
        <asp:ControlParameter Name="OriginID" ControlID="rcbOrigin" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
    </SelectParameters>
</blac:DBDataSource>
<blac:DBDataSource ID="dsDestination" runat="server" 
    SelectCommand="SELECT ID, Name, FullName, DestinationType FROM dbo.fnRetrieveEligibleDestinations(@OriginID, @ShipperID, @ProductID, 0) WHERE (ID = @InitialID OR @RegionID = 0 OR RegionID IS NULL OR RegionID = @RegionID) UNION SELECT 0, '(Select Destination)', '(Select Destination)', NULL ORDER BY Name">
    <SelectParameters>
        <asp:Parameter Name="InitialID" DefaultValue="0" />
        <asp:ControlParameter Name="ProductID" ControlID="rcbProduct" PropertyName="SelectedValue" Type="Int32" DefaultValue="1" />
        <asp:ControlParameter Name="ShipperID" ControlID="rcbCustomer" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
        <asp:ControlParameter Name="OriginID" ControlID="rcbOrigin" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
        <asp:ControlParameter Name="RegionID" ControlID="hfRestrictedRegionID" PropertyName="Value" DefaultValue="0" />
    </SelectParameters>
</blac:DBDataSource>
<blac:DBDataSource ID="dsCustomer" runat="server" SelectCommand="SELECT ID, Name AS FullName FROM dbo.tblCustomer WHERE ID IN (SELECT CustomerID FROM tblOriginCustomers WHERE OriginID=@OriginID) UNION SELECT NULL, '(Select Shipper)' ORDER BY FullName" >
    <SelectParameters>
        <asp:ControlParameter Name="OriginID" ControlID="rcbOrigin" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
    </SelectParameters>
</blac:DBDataSource>

                <blac:DBDataSource ID="dsConsignee" runat="server"
                    SelectCommand="SELECT ID, Name FROM dbo.tblConsignee WHERE DeleteDateUTC IS NULL
                                   AND (@DestinationID = -1 OR ID IN (SELECT ConsigneeID FROM tblDestinationConsignees WHERE DestinationID = @DestinationID))
                                   UNION SELECT NULL, '(Select Consignee)' ORDER BY Name">
                    <SelectParameters>
                        <asp:ControlParameter Name="DestinationID" ControlID="rcbDestination" PropertyName="SelectedValue" Type="Int32" DefaultValue="-1" />
                    </SelectParameters>
                </blac:DBDataSource>

<blac:DBDataSource ID="dsCarrier" runat="server" 
    SelectCommand="SELECT ID, Name AS FullName FROM dbo.viewCarrier WHERE Active = 1 AND (@RegionID = 0 OR ID IN (SELECT DISTINCT CarrierID FROM tblDriver WHERE ID = @InitialDriverID OR RegionID IS NULL OR RegionID = @RegionID AND DeleteDateUTC IS NULL)) UNION SELECT NULL, '(Select Carrier)' ORDER BY FullName" >
    <SelectParameters>
        <asp:Parameter Name="InitialDriverID" DefaultValue="0" />
        <asp:ControlParameter Name="RegionID" ControlID="hfRestrictedRegionID" PropertyName="Value" DefaultValue="0" />
    </SelectParameters>
</blac:DBDataSource>
<blac:DBDataSource ID="dsDriver" runat="server" 
    SelectCommand="SELECT ID, FullName, DriverScore FROM dbo.fnRetrieveEligibleDrivers_NoGPS(@CarrierID, @TerminalID, NULL, @RegionID, NULL, @StartDate, ISNULL((SELECT h2s FROM tblOrigin WHERE ID = @OriginID), 0)) UNION SELECT 0, '(Select Driver)', 100 ORDER BY DriverScore DESC, FullName" >
    <SelectParameters>
        <asp:Parameter Name="InitialID" DefaultValue="0" />
        <asp:ControlParameter Name="CarrierID" ControlID="rcbCarrier" PropertyName="SelectedValue" DbType="Int32" DefaultValue="0" />
        <asp:ControlParameter Name="TerminalID" ControlID="hfRestrictedTerminalID" PropertyName="Value" DbType="Int32" DefaultValue="0" />
        <asp:ControlParameter Name="RegionID" ControlID="hfRestrictedRegionID" PropertyName="Value" DefaultValue="0" />
        <asp:ControlParameter Name="StartDate" ControlID="rdpDueDate" PropertyName="DbSelectedDate" Type="String" DefaultValue="NULL" />
        <asp:ControlParameter Name="OriginID" ControlID="rcbOrigin" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
    </SelectParameters>
</blac:DBDataSource >
<blac:DBDataSource ID="dsTicketType" runat="server" SelectCommand="SELECT ID, Name AS TicketType FROM dbo.tblTicketType ORDER BY Name" />
<blac:DBDataSource ID="dsPriority" runat="server" SelectCommand="SELECT ID, PriorityNum FROM tblPriority ORDER BY PriorityNum" />
<blac:DBDataSource ID="dsOrderStatus" runat="server" SelectCommand="SELECT ID, OrderStatus FROM tblOrderStatus WHERE (@DispatchPage=1 AND ID IN (-10,1,2,7,8,9)) OR ID IN (-10,1,2,9)" >
    <SelectParameters>
        <asp:Parameter Name="DispatchPage" DbType="Int32" DefaultValue="0" />
    </SelectParameters>
</blac:DBDataSource>
<blac:DBDataSource ID="dsUom" runat="server" SelectCommand="SELECT ID, Name FROM tblUom UNION ALL SELECT 0, '(Select UOM)' ORDER BY Name" />
