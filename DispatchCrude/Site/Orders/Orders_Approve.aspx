﻿<%@  Title="Dispatch" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Orders_Approve.aspx.cs"
    Inherits="DispatchCrude.Site.Orders.Orders_Approve" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%-- ------------------------------------------------------------------------------------------ --%>
<%-- Author: Kevin Alons                                                                        --%>
<%-- Date:                                                                                      --%>
<%--                                                                                            --%>
<%-- Summary: Page to approve an order                                                          --%>
<%-- Notes:                                                                                     --%>
<%--   * Hidden fields are used for the IMPORT functionality                                    --%>
<%--                                                                                            --%>
<%-- NAME           DATE        UPDATES                                                         --%>
<%-- =============  ==========  ============================================================    --%>
<%-- J Engler       10/23/2015  Added the transfer % override for order transfers               --%>
<%-- J Engler       12/21/2015  Added the Override Min Settlement Units (carrier and shipper)   --%>
<%--                                                                                            --%>
<%-- ------------------------------------------------------------------------------------------ --%>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script>
        $("#ctl00_ctl00_EntityCaption").html("Order Approval");
    </script>
    <div class="tabbable TabRepaint">
        <ul class="nav nav-tabs" id="tabmenu" runat="server">
            <!--Tabs will print here (see code behind)-->
        </ul>
        <div class="tab-content">
            <div class="tab-pane in active">

                <div class="leftpanel">
                    <div class="tabbable">
                        <ul class="nav nav-tabs">
                            <li class="active tab-blue">
                                <a data-toggle="tab" href="#Filters" aria-expanded="true">Filters</a>
                            </li>
                            <li class="tab-green">
                                <a data-toggle="tab" href="#Export" aria-expanded="true">Export</a>
                            </li>
                        </ul>
                        <div id="leftTabs" class="tab-content">
                            <div id="Filters" class="tab-pane active">
                                <asp:Panel ID="panelFilter" runat="server" DefaultButton="btnRefresh">
                                    <div class="Entry">
                                        <asp:Label ID="lblShipper" runat="server" Text="Shipper" AssociatedControlID="rcbShipper" />
                                        <telerik:RadComboBox Width="100%" runat="server" ID="rcbShipper" DataTextField="Name" DataValueField="ID" DataSourceID="dsShipper" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblCarrier" runat="server" Text="Carrier" AssociatedControlID="rcbCarrier" CssClass="Entry" />
                                        <telerik:RadComboBox Width="100%" ID="rcbCarrier" runat="server" DataTextField="FullName" DataValueField="ID" DataSourceID="dsCarrier" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblProductGroup" runat="server" Text="Product Group" AssociatedControlID="rcbProductGroup" CssClass="Entry" />
                                        <telerik:RadComboBox Width="100%" ID="rcbProductGroup" runat="server" DataTextField="Name" DataValueField="ID" DataSourceID="dsProductGroup"
                                                             AutoPostBack="true" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblRegion" runat="server" Text="Region" AssociatedControlID="rcbRegion" CssClass="Entry" />
                                        <telerik:RadComboBox Width="100%" ID="rcbRegion" runat="server" DataTextField="Name" DataValueField="ID" DataSourceID="dsRegion" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblTerminal" runat="server" Text="Terminal" AssociatedControlID="rcbTerminal" CssClass="Entry" />
                                        <telerik:RadComboBox Width="100%" ID="rcbTerminal" runat="server" DataTextField="Name" DataValueField="ID" DataSourceID="dsTerminal" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblOriginState" runat="server" Text="Origin State" AssociatedControlID="rcbOriginState" CssClass="Entry" />
                                        <telerik:RadComboBox Width="100%" ID="rcbOriginState" runat="server" DataTextField="Name" DataValueField="ID" DataSourceID="dsState" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblDestState" runat="server" Text="Destination State" AssociatedControlID="rcbDestState" CssClass="Entry" />
                                        <telerik:RadComboBox Width="100%" ID="rcbDestState" runat="server" DataTextField="Name" DataValueField="ID" DataSourceID="dsState" />
                                    </div>
                                    <div class="spacer10px"></div>
                                    <div class="Entry">
                                        <asp:CheckBox ID="chkIncludeApproved" runat="server" Text="Include Approved?" Checked="false" />
                                    </div>
                                    <div>
                                        <div class="Entry floatLeft-date-row">
                                            <asp:Label ID="lblStartDate" runat="server" Text="Start Date" AssociatedControlID="rdpStartDate" CssClass="Entry" />
                                            <telerik:RadDatePicker ID="rdpStartDate" runat="server" Width="100px">
                                                <DateInput runat="server" DateFormat="M/d/yyyy" />
                                                <DatePopupButton Enabled="true" />
                                            </telerik:RadDatePicker>
                                        </div>
                                        <div class="Entry floatRight-date-row">
                                            <asp:Label ID="lblEndDate" runat="server" Text="End Date" AssociatedControlID="rdpEndDate" CssClass="Entry" />
                                            <telerik:RadDatePicker ID="rdpEndDate" runat="server" Width="100px">
                                                <DateInput runat="server" DateFormat="M/d/yyyy" />
                                                <DatePopupButton Enabled="true" />
                                            </telerik:RadDatePicker>
                                        </div>                                        
                                    </div>
                                    <div style="height: 5px;" class="clear"></div>                                    
                                    <br /><div class="spacer20px"></div><br />    
                                    <div class="center">
                                            <asp:Button ID="btnRefresh" runat="server" Text="Refresh" CssClass="btn btn-blue shiny" OnClick="btnRefresh_Click" />
                                    </div>
                                </asp:Panel>
                            </div>
                            <div id="Export" class="tab-pane">
                                <asp:Panel ID="panelExcel" runat="server" DefaultButton="cmdExport">
                                    <div>
                                        <div>
                                            <asp:Button ID="cmdExport" runat="server" Text="Export to Excel" CssClass="btn btn-blue shiny" Enabled="true" OnClick="cmdExport_Click" />
                                        </div>
                                        <div class="spacer10px"></div>
                                        <div>
                                            <asp:Button ID="cmdImport" runat="server" ClientIDMode="Static" Text="Import Excel file"
                                                        Enabled="false" OnClick="cmdImport_Click" CssClass="floatRight btn btn-blue shiny" />
                                        </div>
                                    </div>
                                    <div class="spacer10px"></div>
                                    <div class="center">
                                        <asp:FileUpload ID="excelUpload" runat="server" ClientIDMode="Static" CssClass="floatLeft" Width="99%" />
                                        <asp:CustomValidator ID="cvUpload" runat="server" ControlToValidate="excelUpload" CssClass="NullValidator floatLeft" Display="Dynamic"
                                                             Text="*" ErrorMessage="Only Excel (*.xlsx) files allowed)" OnServerValidate="cvUpload_ServerValidate" />
                                    </div>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="gridArea" style="height: 100%; min-height: 500px;">
                    <telerik:RadGrid ID="rgMain" runat="server" EnableHeaderContextMenu="False" CellSpacing="0" GridLines="None" EnableLinqExpressions="false"
                                     AllowSorting="True" AllowFilteringByColumn="false" Height="800" CssClass="GridRepaint" ShowGroupPanel="false" EnableEmbeddedSkins="true" Skin="Vista" AlternatingItemStyle-BackColor="#dcf2fc"
                                     AllowPaging="True" PageSize='<%# Settings.DefaultPageSize %>'
                                     OnNeedDataSource="grid_NeedDataSource" OnItemCreated="grid_ItemCreated" OnItemDataBound="grid_ItemDataBound" OnItemCommand="grid_ItemCommand">
                        <ClientSettings AllowDragToGroup="true">
                            <Scrolling AllowScroll="True" UseStaticHeaders="True" FrozenColumnsCount="1" />
                        </ClientSettings>
                        <SortingSettings EnableSkinSortStyles="false" />
                        <GroupingSettings CaseSensitive="False" ShowUnGroupButton="true" />
                        <MasterTableView AutoGenerateColumns="False" DataKeyNames="ID" CommandItemDisplay="None" AllowMultiColumnSorting="true" EditMode="PopUp">
                            <CommandItemSettings ShowAddNewRecordButton="false" ShowExportToExcelButton="false" ShowRefreshButton="false" />
                            <RowIndicatorColumn Visible="True">
                                <HeaderStyle Width="20px"></HeaderStyle>
                            </RowIndicatorColumn>
                            <EditFormSettings UserControlName="CtrlOrderApprove.ascx" EditFormType="WebUserControl"
                                              CaptionDataField="OrderNum" CaptionFormatString="Order # {0}">
                                <PopUpSettings Modal="true" />
                                <EditColumn UniqueName="EditColumn" />
                            </EditFormSettings>
                            <SortExpressions>
                                <telerik:GridSortExpression FieldName="OrderDate" SortOrder="Ascending" />
                            </SortExpressions>
                            <ColumnGroups>
                                <telerik:GridColumnGroup HeaderText="Order Details" Name="OrderDetails" HeaderStyle-HorizontalAlign="Center" />
                                <telerik:GridColumnGroup HeaderText="Pickup Details" Name="PickupDetails" HeaderStyle-HorizontalAlign="Center" />
                                <telerik:GridColumnGroup HeaderText="Delivery Details" Name="DeliveryDetails" HeaderStyle-HorizontalAlign="Center" />
                            </ColumnGroups>
                            <Columns>
                                <telerik:GridTemplateColumn UniqueName="ActionColumn" HeaderStyle-Width="100px" AllowFiltering="false" AllowSorting="false">
                                    <ItemTemplate>
                                        <telerik:RadButton ID="btnApprove" runat="server" EnableSplitButton="true" Text="Approve"
                                                           AutoPostBack="false" Value='<%# Eval("ID") %>'
                                                           OnClientClicked="btnApprove_ClientClicked">
                                        </telerik:RadButton>
                                        <telerik:RadContextMenu ID="cmApprove" runat="server"
                                                                OnClientItemClicked="cmApprove_ClientItemClicked" OnClientLoad="storeContextMenuReference">
                                            <Items>
                                                <telerik:RadMenuItem Text="Edit" />
                                                <telerik:RadMenuItem Text="Refresh" />
                                            </Items>
                                        </telerik:RadContextMenu>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>

                                <telerik:GridBoundColumn DataField="ID" UniqueName="ID" HeaderText="ID" HeaderStyle-Width="50px" ReadOnly="true" ForceExtractValue="Always" />
                                <telerik:GridCheckBoxColumn DataField="Approved" UniqueName="Approved" HeaderText="A?"
                                                            ReadOnly="true" ForceExtractValue="Always" FilterControlAltText="LightGreen">
                                    <HeaderStyle Width="40px" CssClass="Header-Approved" />
                                </telerik:GridCheckBoxColumn>

                                <telerik:GridTemplateColumn DataField="ApprovalNotes" HeaderText="Approval Notes" SortExpression="ApprovalNotes" 
                                                            UniqueName="ApprovalNotes" DataType="System.String" FilterControlWidth="70%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNotes" runat="server" data-toggle="tooltip"
                                                   class='<%# String.IsNullOrEmpty(Eval("ApprovalNotes").ToString()) ? "" : "center glyphicon glyphicon-list-alt" %>'
                                                   title='<%# String.IsNullOrEmpty(Eval("ApprovalNotes").ToString()) ? "" : Eval("ApprovalNotes") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="100px" />
                                </telerik:GridTemplateColumn>                                
                                <telerik:GridBoundColumn UniqueName="ApprovalNotesExport" DataField="ApprovalNotes" 
                                                         HeaderText="Approval Notes" HeaderStyle-Width="70px" Display="false" Visible="true" ForceExtractValue="Always" DataType="System.String" />


                                <telerik:GridBoundColumn UniqueName="OrderNum" DataField="OrderNum" ReadOnly="true" ColumnGroupName="OrderDetails"
                                                         HeaderText="Order #" HeaderStyle-Width="70px" AllowFiltering="true" FilterControlWidth="60%" />

                                <telerik:GridBoundColumn UniqueName="DispatchConfirmNum" DataField="DispatchConfirmNum" ReadOnly="true" ColumnGroupName="OrderDetails"
                                                         HeaderText="Shipper PO #" HeaderStyle-Width="90px" AllowFiltering="true" FilterControlWidth="60%" />
                                <telerik:GridBoundColumn UniqueName="JobNumber" DataField="JobNumber" ReadOnly="true" ColumnGroupName="OrderDetails"
                                                         HeaderText="Job #" HeaderStyle-Width="70px" AllowFiltering="true" FilterControlWidth="60%" />
                                <telerik:GridBoundColumn UniqueName="ContractNumber" DataField="ContractNumber" ReadOnly="true" ColumnGroupName="OrderDetails"
                                                         HeaderText="Contract #" HeaderStyle-Width="70px" AllowFiltering="true" FilterControlWidth="60%" />

                                <telerik:GridBoundColumn UniqueName="OrderDate" DataField="OrderDate" DataFormatString="{0:M/d/yyyy}" ReadOnly="true" ColumnGroupName="OrderDetails"
                                                         HeaderText="Date" HeaderStyle-Width="80px" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn UniqueName="Shipper" DataField="Shipper" ReadOnly="true" ColumnGroupName="OrderDetails"
                                                         HeaderText="Shipper" HeaderStyle-Width="200px" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn UniqueName="Carrier" DataField="Carrier" ReadOnly="true" ColumnGroupName="OrderDetails"
                                                         HeaderText="Carrier" HeaderStyle-Width="200px" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn UniqueName="ProductGroup" DataField="ProductGroup" ReadOnly="true" ColumnGroupName="OrderDetails"
                                                         HeaderText="Product Group" HeaderStyle-Width="100px" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn UniqueName="Driver" DataField="Driver" ReadOnly="true" ColumnGroupName="OrderDetails"
                                                         HeaderText="Driver" HeaderStyle-Width="150px" AllowFiltering="true" FilterControlWidth="70%" />

                                <telerik:GridBoundColumn UniqueName="ActualMiles" DataField="FinalActualMiles" ReadOnly="true" ColumnGroupName="OrderDetails"
                                                         HeaderText="Rate Miles" HeaderStyle-Width="70px" ForceExtractValue="Always" />
                                <telerik:GridBoundColumn UniqueName="OverrideActualMiles" DataField="OverrideActualMiles" ColumnGroupName="OrderDetails"
                                                         HeaderText="Rate Miles+" HeaderStyle-Width="75px"
                                                         FilterControlAltText="LightGreen" Visible="false" ForceExtractValue="Always" DataType="System.String" />

                                <telerik:GridBoundColumn UniqueName="CarrierMinSettlementUnits" DataField="FinalCarrierMinSettlementUnits" ReadOnly="true" ColumnGroupName="OrderDetails"
                                                         DataType="System.Decimal" DataFormatString="{0:F3}"
                                                         HeaderText="C Min Units" ForceExtractValue="Always">
                                    <HeaderStyle Width="80px" CssClass="Header-CarrierMinSettleUnits" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn UniqueName="OverrideCarrierMinSettlementUnits" DataField="OverrideCarrierMinSettlementUnits" ColumnGroupName="OrderDetails"
                                                         HeaderText="C Min Units+" HeaderStyle-Width="80px"
                                                         FilterControlAltText="LightGreen" Visible="false" ForceExtractValue="Always" DataType="System.String" />
                                <telerik:GridBoundColumn UniqueName="ShipperMinSettlementUnits" DataField="FinalShipperMinSettlementUnits" ReadOnly="true" ColumnGroupName="OrderDetails"
                                                         DataFormatString="{0:F3}"
                                                         HeaderText="S Min Units" HeaderStyle-Width="70px" ForceExtractValue="Always">
                                    <HeaderStyle Width="70px" CssClass="Header-ShipperMinSettleUnits" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn UniqueName="OverrideShipperMinSettlementUnits" DataField="OverrideShipperMinSettlementUnits" ColumnGroupName="OrderDetails"
                                                         HeaderText="S Min Units+" HeaderStyle-Width="70px"
                                                         FilterControlAltText="LightGreen" Visible="false" ForceExtractValue="Always" DataType="System.String" />

                                <telerik:GridBoundColumn UniqueName="Origin" DataField="Origin" ReadOnly="true" ColumnGroupName="PickupDetails"
                                                         HeaderText="Origin" HeaderStyle-Width="200px" AllowFiltering="true" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn UniqueName="OriginMinutes" DataField="FinalOriginMinutes" ReadOnly="true" ColumnGroupName="PickupDetails"
                                                         HeaderText="Total Min." HeaderStyle-Width="70px" AllowFiltering="true" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn UniqueName="OverrideOriginMinutes" DataField="OverrideOriginMinutes" ColumnGroupName="PickupDetails"
                                                         HeaderText="Override Origin Minutes" HeaderStyle-Width="70px" FilterControlAltText="LightGreen"
                                                         Visible="false" ForceExtractValue="Always" DataType="System.String" />
                                <telerik:GridBoundColumn UniqueName="CarrierOriginWaitBillableMinutes" DataField="CarrierOriginWaitBillableMinutes" ReadOnly="true" ColumnGroupName="PickupDetails"
                                                         HeaderText="CBWM" AllowFiltering="true" FilterControlWidth="70%">
                                    <HeaderStyle Width="70px" CssClass="Header-CarrierOriginBillableMinutes" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn UniqueName="ShipperOriginWaitBillableMinutes" DataField="ShipperOriginWaitBillableMinutes" ReadOnly="true" ColumnGroupName="PickupDetails"
                                                         HeaderText="SBWM" AllowFiltering="true" FilterControlWidth="70%">
                                    <HeaderStyle Width="70px" CssClass="Header-ShipperOriginBillableMinutes" />
                                </telerik:GridBoundColumn>

                                <telerik:GridTemplateColumn DataField="OriginWaitNotes" HeaderText="Wait Notes" SortExpression="OriginWaitNotes" ColumnGroupName="PickupDetails"
                                                            UniqueName="OriginWaitNotes" DataType="System.String" FilterControlWidth="70%" >
                                    <ItemTemplate>
                                        <asp:Label ID="lblOriginWaitNotes" runat="server" data-toggle="tooltip"
                                                   class='<%# String.IsNullOrEmpty(Eval("OriginWaitReason").ToString()) 
                                                            && String.IsNullOrEmpty(Eval("OriginWaitNotes").ToString()) ? "" : "center glyphicon glyphicon-list-alt" %>'
                                                   title='<%# String.IsNullOrEmpty(Eval("OriginWaitReason").ToString()) 
                                                            && String.IsNullOrEmpty(Eval("OriginWaitNotes").ToString()) ? "" : String.Concat(Eval("OriginWaitReason"), " | ", Eval("OriginWaitNotes")) %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="80px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn UniqueName="OriginWaitReasonExport" DataField="OriginWaitReason" ColumnGroupName="PickupDetails"
                                                         HeaderText="Origin Wait Reason" HeaderStyle-Width="70px" Display="false" Visible="true" ForceExtractValue="Always" DataType="System.String" />
                                <telerik:GridBoundColumn UniqueName="OriginWaitNotesExport" DataField="OriginWaitNotes" ColumnGroupName="PickupDetails"
                                                         HeaderText="Origin Wait Notes" HeaderStyle-Width="70px" Display="false" Visible="true" ForceExtractValue="Always" DataType="System.String" />

                                <telerik:GridCheckBoxColumn UniqueName="H2S" DataField="FinalH2S" ReadOnly="true" ColumnGroupName="PickupDetails"
                                                            HeaderText="H2S" HeaderStyle-Width="40px" />
                                <telerik:GridCheckBoxColumn UniqueName="OverrideH2S" DataField="OverrideH2S" ColumnGroupName="PickupDetails"
                                                            HeaderText="Override H2S" HeaderStyle-Width="75px"
                                                            FilterControlAltText="LightGreen" Visible="false" ForceExtractValue="Always" />
                                <telerik:GridCheckBoxColumn UniqueName="OriginChainup" DataField="FinalOriginChainup" ReadOnly="true" ColumnGroupName="PickupDetails"
                                                            HeaderText="Chain-Up" HeaderStyle-Width="80px" />
                                <telerik:GridCheckBoxColumn UniqueName="OverrideOriginChainup" DataField="OverrideOriginChainup" ColumnGroupName="PickupDetails"
                                                            HeaderText="Override Chain-Up" HeaderStyle-Width="80px"
                                                            FilterControlAltText="LightGreen" Visible="false" ForceExtractValue="Always" />
                                <telerik:GridCheckBoxColumn UniqueName="Rerouted" DataField="FinalRerouted" ReadOnly="true" ColumnGroupName="PickupDetails"
                                                            HeaderText="Rerouted" HeaderStyle-Width="70px" />
                                <telerik:GridCheckBoxColumn UniqueName="OverrideReroute" DataField="OverrideReroute" ColumnGroupName="PickupDetails"
                                                            HeaderText="Override Reroute" HeaderStyle-Width="75px"
                                                            FilterControlAltText="LightGreen" Visible="false" ForceExtractValue="Always" />
                                <telerik:GridBoundColumn UniqueName="TransferPercentComplete" DataField="FinalTransferPercentComplete" ReadOnly="true" ColumnGroupName="PickupDetails"
                                                         HeaderText="Transfer %" HeaderStyle-Width="70px" ForceExtractValue="Always" />
                                <telerik:GridBoundColumn UniqueName="OverrideTransferPercentComplete" DataField="OverrideTransferPercentComplete" ColumnGroupName="PickupDetails"
                                                         HeaderText="Transfer %+" HeaderStyle-Width="70px"
                                                         FilterControlAltText="LightGreen" Visible="false" ForceExtractValue="Always" DataType="System.String" />                               


                                <telerik:GridTemplateColumn DataField="RejectNotes" HeaderText="Reject Notes" SortExpression="RejectNotes" ColumnGroupName="PickupDetails"
                                                            UniqueName="RejectNotes" DataType="System.String" FilterControlWidth="70%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblRejectNotes" runat="server" data-toggle="tooltip"
                                                   class='<%# String.IsNullOrEmpty(Eval("RejectReason").ToString()) 
                                                            && String.IsNullOrEmpty(Eval("RejectNotes").ToString()) ? "" : "center glyphicon glyphicon-list-alt" %>'
                                                   title='<%# String.IsNullOrEmpty(Eval("RejectReason").ToString()) 
                                                            && String.IsNullOrEmpty(Eval("RejectNotes").ToString()) ? "" : String.Concat(Eval("RejectReason"), " | ", Eval("RejectNotes")) %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="80px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn UniqueName="RejectReasonExport" DataField="RejectReason" ColumnGroupName="PickupDetails"
                                                         HeaderText="Reject Reason" HeaderStyle-Width="70px" Display="false" Visible="true" ForceExtractValue="Always" DataType="System.String" />
                                <telerik:GridBoundColumn UniqueName="RejectNotesExport" DataField="RejectNotes" ColumnGroupName="PickupDetails"
                                                         HeaderText="Reject Notes" HeaderStyle-Width="70px" Display="false" Visible="true" ForceExtractValue="Always" DataType="System.String" />


                                <telerik:GridTemplateColumn DataField="RerouteNotes" HeaderText="Reroute Notes" SortExpression="RerouteNotes" ColumnGroupName="PickupDetails"
                                                            UniqueName="RerouteNotes" DataType="System.String" FilterControlWidth="70%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblRerouteNotes" runat="server" data-toggle="tooltip"
                                                   class='<%# String.IsNullOrEmpty(Eval("RerouteNotes").ToString()) ? "" : "center glyphicon glyphicon-list-alt" %>'
                                                   title='<%# String.IsNullOrEmpty(Eval("RerouteNotes").ToString()) ? "" : Eval("RerouteNotes") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="90px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn UniqueName="RerouteNotesExport" DataField="RerouteNotes" ColumnGroupName="PickupDetails"
                                                         HeaderText="Reroute Notes" HeaderStyle-Width="70px" Display="false" Visible="true" ForceExtractValue="Always" DataType="System.String" />


                                <telerik:GridBoundColumn UniqueName="Destination" DataField="Destination" ReadOnly="true" ColumnGroupName="DeliveryDetails"
                                                         HeaderText="Destination" HeaderStyle-Width="200px" AllowFiltering="true" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn UniqueName="PreviousDestinations" DataField="PreviousDestinations" ReadOnly="true" ColumnGroupName="DeliveryDetails"
                                                         HeaderText="Previous Destinations" HeaderStyle-Width="200px" AllowFiltering="true" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn UniqueName="DestMinutes" DataField="FinalDestMinutes" ReadOnly="true" ColumnGroupName="DeliveryDetails"
                                                         HeaderText="Total Min." HeaderStyle-Width="70px" AllowFiltering="true" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn UniqueName="OverrideDestMinutes" DataField="OverrideDestMinutes" ColumnGroupName="DeliveryDetails"
                                                         HeaderText="Override Dest Minutes" HeaderStyle-Width="70px" FilterControlAltText="LightGreen"
                                                         Visible="false" ForceExtractValue="Always" DataType="System.String" />
                                <telerik:GridBoundColumn UniqueName="CarrierDestinationWaitBillableMinutes" DataField="CarrierDestinationWaitBillableMinutes" ReadOnly="true" ColumnGroupName="DeliveryDetails"
                                                         HeaderText="CBWM" AllowFiltering="true" FilterControlWidth="70%">
                                    <HeaderStyle Width="70px" CssClass="Header-CarrierDestBillableMinutes" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn UniqueName="ShipperDestinationWaitBillableMinutes" DataField="ShipperDestinationWaitBillableMinutes" ReadOnly="true" ColumnGroupName="DeliveryDetails"
                                                         HeaderText="SBWM" AllowFiltering="true" FilterControlWidth="70%">
                                    <HeaderStyle Width="70px" CssClass="Header-ShipperDestBillableMinutes" />
                                </telerik:GridBoundColumn>

                                <telerik:GridTemplateColumn DataField="DestWaitNotes" HeaderText="Wait Notes" SortExpression="DestWaitNotes" ColumnGroupName="DeliveryDetails"
                                                            UniqueName="DestWaitNotes" DataType="System.String" FilterControlWidth="70%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDestWaitNotes" runat="server" data-toggle="tooltip"
                                                   class='<%# String.IsNullOrEmpty(Eval("DestWaitReason").ToString()) 
                                                            && String.IsNullOrEmpty(Eval("DestWaitNotes").ToString()) ? "" : "center glyphicon glyphicon-list-alt" %>'
                                                   title='<%# String.IsNullOrEmpty(Eval("DestWaitReason").ToString()) 
                                                            && String.IsNullOrEmpty(Eval("DestWaitNotes").ToString()) ? "" : String.Concat(Eval("DestWaitReason"), " | ", Eval("DestWaitNotes")) %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="80px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn UniqueName="DestWaitReasonExport" DataField="DestWaitReason" ColumnGroupName="DeliveryDetails"
                                                         HeaderText="Dest Wait Reason" HeaderStyle-Width="70px" Display="false" Visible="true" ForceExtractValue="Always" DataType="System.String" />
                                <telerik:GridBoundColumn UniqueName="DestWaitNotesExport" DataField="DestWaitNotes" ColumnGroupName="DeliveryDetails"
                                                         HeaderText="Dest Wait Notes" HeaderStyle-Width="70px" Display="false" Visible="true" ForceExtractValue="Always" DataType="System.String" />

                                <telerik:GridCheckBoxColumn UniqueName="DestChainup" DataField="FinalDestChainup" ReadOnly="true" ColumnGroupName="DeliveryDetails"
                                                            HeaderText="Chain-Up" HeaderStyle-Width="80px" />
                                <telerik:GridCheckBoxColumn UniqueName="OverrideDestChainup" DataField="OverrideDestChainup" ColumnGroupName="DeliveryDetails"
                                                            HeaderText="Override Chain-Up" HeaderStyle-Width="75px"
                                                            FilterControlAltText="LightGreen" Visible="false" ForceExtractValue="Always" />
                                
                                
                                


                                <telerik:GridTemplateColumn UniqueName="Validation" HeaderStyle-Width="0px" Display="false" EditFormColumnIndex="3" EditFormHeaderTextFormat="">
                                    <EditItemTemplate>
                                        <asp:CustomValidator ID="cvGridError" runat="server" Display="None" ErrorMessage="Generic Message" />
                                        <asp:ValidationSummary ID="validationSummaryMain" runat="server" DisplayMode="BulletList" Enabled="true" EnableClientScript="true"
                                                               CssClass="NullValidator" />
                                    </EditItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="CreateDate" UniqueName="CreateDate" SortExpression="CreateDate" DataType="System.DateTime" DataFormatString="{0:M/d/yy}"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Create Date" HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="CreatedByUser" UniqueName="CreatedByUser" SortExpression="CreatedByUser"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Created By" HeaderStyle-Width="100px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />

                                <telerik:GridDropDownColumn DataField="ImportAction" UniqueName="ImportAction" HeaderText="Import Action"
                                                            ReadOnly="true" Display="false" ForceExtractValue="Always" ItemStyle-BackColor="Olive"
                                                            FilterControlAltText="LightGreen"
                                                            DataSourceID="dsImportAction" ListTextField="Name" ListValueField="Name" />
                                <telerik:GridBoundColumn DataField="ImportOutcome" UniqueName="ImportOutcome" HeaderText="Import Outcome"
                                                         ReadOnly="true" Visible="false" ForceExtractValue="Always" ItemStyle-BackColor="Olive" />

                            </Columns>
                            <EditFormSettings ColumnNumber="4">
                                <EditColumn ButtonType="ImageButton" CancelImageUrl="~/images/cancel.png"
                                            UpdateImageUrl="~/images/apply.png" InsertImageUrl="~/images/apply.png" />
                            </EditFormSettings>
                            <PagerStyle AlwaysVisible="True" />
                        </MasterTableView>
                        <HeaderStyle Wrap="False" />
                        <PagerStyle AlwaysVisible="True" />
                        <FilterMenu EnableImageSprites="False" />
                    </telerik:RadGrid>
                    <telerik:RadScriptBlock ID="rsbApprove" runat="server">
                        <script type="text/javascript">
                            var splitButton = null;
                            var contextMenu = null;

                            function storeContextMenuReference(sender, args) {
                                contextMenu = sender;
                            };
                            function btnApprove_ClientClicked(sender, args) {
                                //debugger;
                                //console.log("btnApprove_Clicked() triggered");
                                // store a reference to this particular btnApprove
                                splitButton = sender;
                                // if the "Split" was clicked, then show the context menu, otherwise do the default behavior
                                if (args.IsSplitButtonClick()) {
                                    var currentLocation = $telerik.getBounds(sender.get_element());
                                    contextMenu.showAt(currentLocation.x, currentLocation.y + currentLocation.height);
                                } else {
                                    SendPostBack("Approve");
                                }
                            };
                            function cmApprove_ClientItemClicked(sender, args) {
                                //console.log("cmApprove_ClientItemClicked(" + args.get_item().get_text() + ") triggered");
                                SendPostBack(args.get_item().get_text());
                            };
                            function SendPostBack(commandName) {
                                console.log("SendPostBack(" + commandName + ") triggered");
                                __doPostBack("<%= rgMain.UniqueID %>", commandName + "|" + splitButton.get_value());
                            };

                            $(function () {
                                $("#excelUpload").change(function () {
                                    var disabled = $("#excelUpload").val().length == 0;
                                    $("#cmdImport").prop("disabled", disabled);
                                });
                            });
                        </script>
                    </telerik:RadScriptBlock>
                </div>
                <blac:DBDataSource ID="dsShipper" runat="server" SelectCommand="SELECT ID, Name FROM dbo.tblCustomer UNION SELECT -1, '(All)' ORDER BY Name" />
                <blac:DBDataSource ID="dsCarrier" runat="server"
                                   SelectCommand="SELECT ID, CASE WHEN Active = 1 THEN '' ELSE 'Deleted: ' END + Name AS FullName, Active FROM dbo.viewCarrier UNION SELECT -1, '(All)', 1 ORDER BY Active DESC, FullName">
                </blac:DBDataSource>
                <blac:DBDataSource ID="dsProductGroup" runat="server" SelectCommand="SELECT ID, Name FROM tblProductGroup UNION SELECT -1, '(All)' ORDER BY Name" />
                <blac:DBDataSource ID="dsState" runat="server" SelectCommand="SELECT ID, Name = FullName FROM dbo.tblState UNION SELECT -1, '(All)' ORDER BY Name" />
                <blac:DBDataSource ID="dsImportAction" runat="server" SelectCommand="SELECT Name = 'None' UNION SELECT 'Update'" />
                <blac:DBDataSource ID="dsRegion" runat="server" SelectCommand="SELECT ID, Name FROM dbo.tblRegion UNION SELECT -1, '(All Regions)' ORDER BY Name" />
                <blac:DBDataSource ID="dsTerminal" runat="server" SelectCommand="SELECT ID, Name FROM dbo.tblTerminal WHERE DeleteDateUTC IS NULL UNION SELECT -1, '(All Terminals)' ORDER BY Name" />
            </div>
        </div>
    </div>
<script>
    $(document).ready(function () {
        //Add custom tooltips to grid headers
        $(".rgHeader.Header-Approved > a").attr("title", "Approved?\nClick to sort");
        $(".rgHeader.Header-CarrierMinSettleUnits > a").attr("title", "Carrier Minimum Settlement Units\nClick to sort");
        $(".rgHeader.Header-ShipperMinSettleUnits > a").attr("title", "Shipper Minimum Settlement Units\nClick to sort");
        $(".rgHeader.Header-CarrierOriginBillableMinutes > a").attr("title", "Carrier Billable Wait Minutes\nClick to sort");
        $(".rgHeader.Header-ShipperOriginBillableMinutes > a").attr("title", "Shipper Billable Wait Minutes\nClick to sort");
        $(".rgHeader.Header-CarrierDestBillableMinutes > a").attr("title", "Carrier Billable Wait Minutes\nClick to sort");
        $(".rgHeader.Header-ShipperDestBillableMinutes > a").attr("title", "Shipper Billable Wait Minutes\nClick to sort");
    });
</script>
</asp:Content>
