﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using DispatchCrude.App_Code;
using DispatchCrude.Core;
using DispatchCrude.Models;
using AlonsIT;
using System.Drawing;

namespace DispatchCrude.Orders
{
    public partial class CtrlOrderEdit : System.Web.UI.UserControl
    {
        private object _dataItem = null;

        protected void Page_Load(object sender, System.EventArgs e)
        {
            this.dsOrderStatus.SelectParameters["DispatchPage"].DefaultValue = (Page is DispatchCrude.Site.Orders.Orders_Dispatch) ? "1" : "0";

            if (DataItem != null && DataItem is DataRowView)
            {
                DataRowView dv = DataItem as DataRowView;

                hfID.Value = dv["ID"].ToString();

                // Disable contract # (lookup only)
                txtContractNumber.ReadOnly = true;                        
                txtContractNumber.BackColor = ColorTranslator.FromHtml("#f0f0f0");

                // DCWEB-1027: if this user has a specific Profile Region specified, and the site is restricting users to that region, make the control readonly
                int regionID;
                if (Context.Profile.GetPropertyValue("RegionID") != null 
                    && (regionID = DBHelper.ToInt32(Context.Profile.GetPropertyValue("RegionID"))) != 0
                    && Settings.SettingsID.RestrictUserToProfileRegion.AsBool())
                {
                    hfRestrictedRegionID.Value = regionID.ToString();
                    // these will ensure that if an inelible entity (based on Region filtering) is currently assigned it will be displayed
                    dsDestination.SelectParameters["InitialID"].DefaultValue = dv["DestinationID"].ToString();
                    dsCarrier.SelectParameters["InitialDriverID"].DefaultValue = dv["DriverID"].ToString();
                    dsDriver.SelectParameters["InitialID"].DefaultValue = dv["DriverID"].ToString();
                }

                // JT-169: if this user has a specific Profile Terminal specified, make the control readonly
                int terminalID;
                if (Context.Profile.GetPropertyValue("TerminalID") != null
                    && (terminalID = DBHelper.ToInt32(Context.Profile.GetPropertyValue("TerminalID"))) != 0)
                {
                    hfRestrictedTerminalID.Value = terminalID.ToString();
                    // these will ensure that if an inelible entity (based on Terminal filtering) is currently assigned it will be displayed
                    dsDestination.SelectParameters["InitialID"].DefaultValue = dv["DestinationID"].ToString();
                    dsCarrier.SelectParameters["InitialDriverID"].DefaultValue = dv["DriverID"].ToString();
                    dsDriver.SelectParameters["InitialID"].DefaultValue = dv["DriverID"].ToString();
                }

                App_Code.DropDownListHelper.Rebind(ddPriority, dv["PriorityID"]);
                App_Code.RadComboBoxHelper.Rebind(rcbOrderStatus, dv["StatusID"]);
                
                /* this will ensure that even if the currently assigned Origin is deleted,  it will be included in the list */
                dsOrigin.SelectParameters["InitialID"].DefaultValue = dv["OriginID"].ToString();

                App_Code.RadComboBoxHelper.Rebind(rcbOrigin, dv["OriginID"]);
                App_Code.RadComboBoxHelper.Rebind(rcbOriginTank, dv["OriginTankID"]);
                App_Code.RadComboBoxHelper.Rebind(rcbOriginUom, dv["OriginUomID"]);
                App_Code.RadComboBoxHelper.Rebind(rcbProduct, dv["ProductID"], true);
                App_Code.RadComboBoxHelper.Rebind(rcbDestination, dv["DestinationID"], true);
                App_Code.RadComboBoxHelper.Rebind(rcbDestUom, dv["DestUomID"]);
                App_Code.RadComboBoxHelper.Rebind(rcbCustomer, dv["CustomerID"]);
                App_Code.RadComboBoxHelper.Rebind(rcbCarrier, dv["CarrierID"], true);
                App_Code.RadComboBoxHelper.Rebind(rcbDriver, dv["DriverID"], true);
                App_Code.RadComboBoxHelper.Rebind(rcbConsignee, dv["ConsigneeID"], true);
                App_Code.RadComboBoxHelper.Rebind(rcbTicketType, dv["TicketTypeID"], true);
                rdpDueDate.DbSelectedDate = DBHelper.ToDateTime(dv["DueDate"]);
                txtOriginBOLNum.Text = dv["OriginBOLNum"].ToString();
                txtOriginTankNum.Text = dv["OriginTankNum"].ToString();
                txtDispatchNotes.Text = dv["DispatchNotes"].ToString();
                txtDispatchConfirmNum.Text = dv["DispatchConfirmNum"].ToString();
                txtJobNumber.Text = dv["JobNumber"].ToString();
                txtContractNumber.Text = dv["ContractNumber"].ToString();

                ddPriority.Enabled =  
                    rcbOrigin.Enabled = 
                    rcbDestination.Enabled = 
                    rcbTicketType.Enabled =
                    rcbCarrier.Enabled =
                    txtOriginBOLNum.Enabled = rcbOriginTank.Enabled = txtOriginTankNum.Enabled = 
                    txtDispatchConfirmNum.Enabled =
                        HttpContext.Current.User.IsInRole("editOrders");

                SetDispatchConfirmationRequired();

                HandleOriginTextFields();
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///          Required method for Designer support - do not modify
        ///          the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion

        public object DataItem
        {
            get
            {
                return this._dataItem;
            }
            set
            {
                this._dataItem = value;
            }
        }

        protected void rcbOrigin_ItemDataBound(object o, RadComboBoxItemEventArgs e)
        {
            if (e.Item.Text.StartsWith("H2S-"))
                e.Item.CssClass += " H2S";
            else if (e.Item.Text.StartsWith("Deleted:"))
                e.Item.CssClass += " Deleted";
        }

        protected void cvStatus_AssignedValidate(object sender, ServerValidateEventArgs args)
        {
            args.IsValid = true; // default value
            int carrierID = RadComboBoxHelper.SelectedValue(rcbCarrier);
            if (RadComboBoxHelper.SelectedValue(rcbOrderStatus) == (int)OrderStatus.STATUS.Assigned && carrierID == 0)
            {
                args.IsValid = false;
            }
        }

        protected void cvStatus_DispatchedValidate(object sender, ServerValidateEventArgs args)
        {
            args.IsValid = true; // default value
            int carrierID = RadComboBoxHelper.SelectedValue(rcbCarrier);
            int driverID = RadComboBoxHelper.SelectedValue(rcbDriver);
            if (RadComboBoxHelper.SelectedValue(rcbOrderStatus) == (int)OrderStatus.STATUS.Dispatched && (carrierID == 0 || driverID == 0))
            {
                args.IsValid = false;
            }
        }

        protected void UpdateDataItem(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                using (DispatchCrudeDB db = new DispatchCrudeDB())
                {
                    int id = DBHelper.ToInt32(hfID.Value);
                    var current = db.Orders.Find(id);

                    if (current.StatusID != DBHelper.ToInt32(rcbOrderStatus.SelectedValue))
                    {
                        // change status, attempt to keep print status in sync
                        current.StatusID = DBHelper.ToInt32(rcbOrderStatus.SelectedValue);
                        if (current.StatusID == (int)OrderStatus.STATUS.Delivered || current.StatusID == (int)OrderStatus.STATUS.Audited)
                        {
                            current.PickupPrintStatusID = (int)PrintStatus.STATUS.Handwritten;
                            current.DeliverPrintStatusID = (int)PrintStatus.STATUS.Handwritten;
                        }
                        else if (current.StatusID == (int)OrderStatus.STATUS.PickedUp)
                        {
                            current.PickupPrintStatusID = (int)PrintStatus.STATUS.Handwritten;
                            current.DeliverPrintStatusID = (int)PrintStatus.STATUS.NotFinalized;
                        }
                        else
                        {
                            current.PickupPrintStatusID = (int)PrintStatus.STATUS.NotFinalized;
                            current.DeliverPrintStatusID = (int)PrintStatus.STATUS.NotFinalized;
                        }
                    }

                    if (HttpContext.Current.User.IsInRole("editOrders"))
                    {
                        current.OriginID = DBHelper.ToInt32(rcbOrigin.SelectedValue);
                        current.OriginTankID = rcbOriginTank.Enabled && RadComboBoxHelper.SelectedValue(rcbOriginTank) > 0 ? RadComboBoxHelper.SelectedValue(rcbOriginTank) : (int?)null;
                        current.OriginUomID = DBHelper.ToInt32(rcbOriginUom.SelectedValue);
                        current.ProductID = DBHelper.ToInt32(rcbProduct.SelectedValue);
                        current.DestinationID = DBHelper.ToInt32(rcbDestination.SelectedValue);
                        current.DestUomID = DBHelper.ToInt32(rcbDestUom.SelectedValue);
                        current.CustomerID = DBHelper.ToInt32(rcbCustomer.SelectedValue);
                        current.CarrierID = DBHelper.TZ_(rcbCarrier.SelectedValue);
                        current.TicketTypeID = DBHelper.ToInt32(rcbTicketType.SelectedValue);
                        current.OriginTankNum = txtOriginTankNum.Text.Length > 0 ? txtOriginTankNum.Text : null;
                        current.OriginBOLNum = txtOriginBOLNum.Text.Length > 0 ? txtOriginBOLNum.Text : null;
                        current.DueDate = rdpDueDate.SelectedDate.Value;
                        current.DispatchConfirmNum = txtDispatchConfirmNum.Text.Length > 0 ? txtDispatchConfirmNum.Text : null;
                        current.PriorityID = Convert.ToByte(ddPriority.SelectedValue);
                        current.ConsigneeID = RadComboBoxHelper.SelectedValue(rcbConsignee) > 0 ? RadComboBoxHelper.SelectedValue(rcbConsignee) : (int?)null;
                    }
                    if (DBHelper.ToInt32(current.DriverID) != DBHelper.ToInt32(rcbDriver.SelectedValue))
                    {
                        Driver driver = db.Drivers.SqlQuery("SELECT * FROM tblDriver WHERE ID={0}", DBHelper.ToInt32(rcbDriver.SelectedValue)).ToList<Driver>().FirstOrDefault();
                        if (driver != null)
                        {
                            current.TruckID = driver.TruckID;
                            current.TrailerID = driver.TrailerID;
                            current.Trailer2ID = driver.Trailer2ID;
                        }
                    }
                    current.DriverID = DBHelper.TZ_(rcbDriver.SelectedValue);
                    current.DispatchNotes = txtDispatchNotes.Text.Length > 0 ? txtDispatchNotes.Text : null;
                    current.JobNumber = txtJobNumber.Text.Length > 0 ? txtJobNumber.Text : null;
                    //current.ContractNumber = txtContractNumber.Text;
                    db.SaveChanges(UserSupport.UserName);
                }
            }
        }

        protected void rcbOrderStatus_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            SetDispatchConfirmationRequired();
            Page.Validate();
        }

        private void SetDispatchConfirmationRequired()
        {
            rfvDispatchConfirmNum.Enabled = RadComboBoxHelper.SelectedValue(rcbOrderStatus) == (int)OrderStatus.STATUS.Dispatched
                && DBHelper.ToBoolean(
                    OrderRuleHelper.GetRuleValue(DateTime.UtcNow.Date
                        , OrderRuleHelper.Type.RequireShipperPO
                        , RadComboBoxHelper.SelectedValue(rcbCustomer)
                        , RadComboBoxHelper.SelectedValue(rcbCarrier)
                        , RadComboBoxHelper.SelectedValue(rcbProduct)
                        , RadComboBoxHelper.SelectedValue(rcbOrigin)
                        , RadComboBoxHelper.SelectedValue(rcbDestination)
                        , false));
        }

        protected void rcbOrigin_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (DBHelper.ToInt32(rcbOrigin.SelectedValue) != 0)
            {
                int _DriverID = RadComboBoxHelper.SelectedValue(rcbDriver);
                App_Code.RadComboBoxHelper.Rebind(rcbProduct, true);
                App_Code.RadComboBoxHelper.Rebind(rcbDestination, true);
                App_Code.RadComboBoxHelper.Rebind(rcbCustomer, true);
                App_Code.RadComboBoxHelper.Rebind(rcbOriginTank, true);
                object uomID = DispatchCrude_ConnFactory.QuerySingleValue("SELECT UomID FROM tblOrigin WHERE ID = {0}", rcbOrigin.SelectedValue);
                App_Code.RadComboBoxHelper.SetSelectedValue(rcbOriginUom, uomID, true);

                RadComboBoxHelper.SetSelectedValue(rcbDriver, _DriverID);
            }
        }

        protected void rcbOriginTank_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            txtOriginTankNum.Enabled = rcbOriginTank.Text == "[Enter Details]";
            if (!txtOriginTankNum.Enabled)
                txtOriginTankNum.Text = "";

            UpdateContractNumber();
        }

        protected void rcbProduct_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            App_Code.RadComboBoxHelper.Rebind(rcbDestination, true);

            UpdateContractNumber();
        }

        protected void rcbCustomer_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            App_Code.RadComboBoxHelper.Rebind(rcbDestination, true);

            UpdateContractNumber();
        }

        protected void rcbCarrier_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            App_Code.RadComboBoxHelper.Rebind(rcbDriver, true);
            RadComboBoxHelper.SetSelectedValue(rcbOrderStatus, 
                DBHelper.ToInt32(rcbCarrier.SelectedValue) > 0
                    ? (int)OrderStatus.STATUS.Assigned
                    : (int)OrderStatus.STATUS.Generated);
        }

        protected void rcbDriver_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            RadComboBoxHelper.SetSelectedValue(rcbOrderStatus, 
                DBHelper.ToInt32(rcbDriver.SelectedValue) > 0
                    ? (int)OrderStatus.STATUS.Dispatched
                    : (int)OrderStatus.STATUS.Assigned);
            SetDispatchConfirmationRequired();
        }

        protected void rcbTicketType_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            HandleOriginTextFields();
        }
        private void HandleOriginTextFields()
        {
            int ticketTypeID = RadComboBoxHelper.SelectedValue(rcbTicketType);
            switch (ticketTypeID)
            {
                case (int)TicketType.TYPE.GaugeRun:
                case (int)TicketType.TYPE.Gauge_Auto_Close:
                case (int)TicketType.TYPE.GaugeNet:
                case (int)TicketType.TYPE.NetVolume:
                case (int)TicketType.TYPE.CAN_MeterRun:
                case (int)TicketType.TYPE.Production_Water_Run:
                    rcbOriginTank.Enabled = cvOriginTank.Enabled = true;
                    rcbOriginTank_SelectedIndexChanged(null, null);
                    txtOriginBOLNum.Enabled = false;
                    txtOriginBOLNum.Text = "";
                    break;
                case (int)TicketType.TYPE.MeterRun:
                case (int)TicketType.TYPE.BasicRun:
                    rcbOriginTank.Enabled = txtOriginTankNum.Enabled = cvOriginTank.Enabled = false;
                    txtOriginTankNum.Text = "";
                    break;
                case (int)TicketType.TYPE.GrossVolume:
                default:
                    rcbOriginTank.Enabled = txtOriginTankNum.Enabled = cvOriginTank.Enabled = txtOriginBOLNum.Enabled = false;
                    break;
            }
        }

        protected void cvDispatchConfirmNum_ServerValidate(object source, ServerValidateEventArgs args)
        {
            using (SSDB ssdb = new SSDB())
            {
                args.IsValid = txtDispatchConfirmNum.Text.Trim().Length == 0 || DBHelper.ToInt32(ssdb.QuerySingleValue(
                    string.Format(
                        "SELECT count(*) FROM tblOrder WHERE DeleteDateUTC IS NULL AND CustomerID = {0} AND DispatchConfirmNum = {1} AND ID <> {2}"
                            , RadComboBoxHelper.SelectedValue(rcbCustomer)
                            , DBHelper.QuoteStr(txtDispatchConfirmNum.Text)
                            , this.hfID.Value))) == 0;
            }
        }

        protected void cvOriginTank_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = Settings.SettingsID.RequireTanksDuringCreate.AsBool() == false  // system setting off
                || RadComboBoxHelper.SelectedValue(rcbOriginTank) > 0; // tank selected
        }

        protected void rcbDriver_ItemDataBound(object sender, RadComboBoxItemEventArgs e)
        {
            if (e.Item.DataItem is DataRowView)
            {
                if ((e.Item.DataItem as DataRowView)["DriverScore"].ToString() == "0")
                {
                    e.Item.Enabled = false;
                    e.Item.Text += " [UNAVAILABLE]";
                }
            }
        }

        protected void rdpDueDate_Changed(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            string driver = rcbDriver.SelectedValue;

            RadComboBoxHelper.Rebind(rcbDriver);
            rcbDriver.SelectedValue = driver; // reselect the same driver

            UpdateContractNumber();
        }


        private void UpdateContractNumber()
        {
            if (RadComboBoxHelper.SelectedValue(rcbOrigin) != 0 //producer
                && RadComboBoxHelper.SelectedValue(rcbProduct) != 0 // productgroup
                && RadComboBoxHelper.SelectedValue(rcbCustomer) != 0) // shipper
            {
                using (SSDB db = new SSDB())
                {
                    var ContractNumber = db.QuerySingleValue(@"
                        SELECT ContractNumber FROM tblContract WHERE ID = dbo.fnContractID(
                            {0},
                            (SELECT ProductGroupID FROM tblProduct WHERE ID = {1}),
                            (SELECT ProducerID FROM tblOrigin WHERE ID = {2}),
                            '{3}')",
                        RadComboBoxHelper.SelectedValue(rcbCustomer),
                        RadComboBoxHelper.SelectedValue(rcbProduct),
                        RadComboBoxHelper.SelectedValue(rcbOrigin),
                        rdpDueDate.SelectedDate.Value.Date);
                    txtContractNumber.Text = (ContractNumber ?? "").ToString();
                }
            }
        }
    }
}