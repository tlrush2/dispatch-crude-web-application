﻿<%@ Page Title="Dispatch" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Orders_Audit.aspx.cs"
    Inherits="DispatchCrude.Site.Orders.Orders_Audit" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .DetailTable caption
        {
            background: #a13b2e;
            text-align: left;
            color: White;
        }
        .DetailTable_Header
        {
            padding-top: 0px !important;
            padding-bottom: 0px !important;
            height: 9px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script>
        $("#ctl00_ctl00_EntityCaption").html("Order Audit");
    </script>
    <div class="tabbable TabRepaint">
        <ul class="nav nav-tabs" id="tabmenu" runat="server">
            <!--Tabs will print here (see code behind)-->
        </ul>
        <div class="tab-content">
            <div class="tab-pane in active">

                <div id="speedbuttons" class="well speedButtonContainer" runat="server">
                    <!--Buttons will print here (see code behind)-->                    
                </div>

                <div id="gridArea" style="height:100%">
                    <asp:ValidationSummary ID="validationSummaryMain" runat="server" DisplayMode="BulletList" Enabled="true" EnableClientScript="true"
                                           CssClass="NullValidator" />
                    <telerik:RadPersistenceManager ID="rpmMain" runat="server">
                        <PersistenceSettings>
                            <telerik:PersistenceSetting ControlID="rgMain" />
                        </PersistenceSettings>
                    </telerik:RadPersistenceManager>
                    <telerik:RadGrid ID="rgMain" runat="server" Height="800"
                                     EnableHeaderContextMenu="True" AllowPaging="True" AllowSorting="True" CellSpacing="0" GridLines="None" CssClass="GridRepaint"
                                     EnableEmbeddedSkins="true" Skin="Vista" AlternatingItemStyle-BackColor="#dcf2fc" AutoGenerateColumns="False" AllowMultiRowEdit="True"
                                     AllowFilteringByColumn="True" ShowGroupPanel="true"
                                     DataSourceID="dsMain" OnDetailTableDataBind="grid_DetailTableDataBind" OnItemCommand="grid_ItemCommand" OnItemDataBound="grid_ItemDataBound"
                                     PageSize='<%# Settings.DefaultPageSize %>'>
                        <ClientSettings AllowDragToGroup="true" Resizing-AllowColumnResize="true" Resizing-AllowResizeToFit="true" AllowColumnHide="true" AllowColumnsReorder="true">
                            <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                        </ClientSettings>
                        <GroupingSettings CaseSensitive="false" ShowUnGroupButton="true" />
                        <MasterTableView DataKeyNames="ID" CommandItemDisplay="Top" EnableViewState="True">
                            <CommandItemTemplate>
                                <div style="padding: 5px;">
                                    <asp:LinkButton ID="btnRefresh" Text="Refresh" CommandName="Refresh" CssClass="pull-right" runat="server">Refresh&nbsp;&nbsp;</asp:LinkButton>
                                    <asp:Button runat="server" ID="buttonRefresh" CommandName="Refresh" CssClass="rgRefresh pull-right" Text="Refresh" Title="Refresh" />
                                    <div style="width:10px" class="pull-right">&nbsp;</div>
                                    <asp:LinkButton ID="cmdResetLayout" Style="vertical-align: middle" runat="server"
                                                    OnClick="cmdResetLayout_Click" Enabled="true" CssClass="pull-right">
                                        <img style="border:0px" alt="" src="../../images/wrench_orange.png" />
                                        Reset Layout
                                    </asp:LinkButton>
                                    <div style="width:10px" class="pull-right">&nbsp;</div>
                                    <asp:LinkButton ID="cmdSaveLayout" Style="vertical-align: middle" runat="server"
                                                    OnClick="cmdSaveLayout_Click" Enabled="true" CssClass="pull-right">
                                        <img style="border:0px" alt="" src="../../images/wrench_orange.png" />
                                        Save Layout
                                    </asp:LinkButton>
                                </div>
                            </CommandItemTemplate>
                            <DetailTables>
                                <telerik:GridTableView runat="server" AllowPaging="False" Name="CarrierTableView" Caption="&nbsp;Order Details" CssClass="DetailTable"
                                                       AllowSorting="False" AllowFilteringByColumn="false" EditMode="InPlace">
                                    <HeaderStyle CssClass="DetailTable_Header" />
                                    <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                    <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                    </RowIndicatorColumn>
                                    <ExpandCollapseColumn Visible="false" />
                                    <Columns>
                                        <telerik:GridTemplateColumn UniqueName="ActionColumn" ReadOnly="true" Visible="false">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="cmdEdit" runat="server" CommandName="EditDetails" ImageUrl="~/images/edit.png" CausesValidation="false" />
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:ImageButton ID="cmdUpdate" runat="server" CommandName="UpdateDetails" ImageUrl="~/images/apply_imageonly.png" CausesValidation="true" />
                                                <asp:ImageButton ID="cmdCancel" runat="server" CommandName="CancelDetails" ImageUrl="~/images/cancel_imageonly.png" CausesValidation="false" />
                                            </EditItemTemplate>
                                            <HeaderStyle Width="35px" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridBoundColumn DataField="DispatchConfirmNum" UniqueName="DispatchConfirmNum" HeaderText="Shipper PO #" HeaderStyle-Width="80px" />
                                        <telerik:GridBoundColumn DataField="JobNumber" UniqueName="JobNumber" HeaderText="Job #" HeaderStyle-Width="80px" />
                                        <telerik:GridBoundColumn DataField="ContractNumber" UniqueName="ContractNumber" HeaderText="Contract #" HeaderStyle-Width="80px" />
                                        <telerik:GridTemplateColumn EditFormColumnIndex="0" DataField="CarrierID" HeaderText="Carrier"
                                                                    SortExpression="Carrier" UniqueName="CarrierID" DataType="System.Int32" Groupable="false">
                                            <EditItemTemplate>
                                                <asp:DropDownList ID="ddlCarrier" runat="server" DataSourceID="dsCarrier" DataTextField="Name" DataValueField="ID" SelectedValue='<%# Bind("CarrierID") %>'
                                                                  Width="100%" AutoPostBack="true" OnSelectedIndexChanged="ddlCarrier_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblCarrier" runat="server" Text='<%# Eval("Carrier") %>' Width="100%" />
                                            </ItemTemplate>
                                            <HeaderStyle Width="100px" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn DataField="CarrierTicketNum" UniqueName="CarrierTicketNum" HeaderText="Ticket # (1st)" Groupable="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCarrierTicketNum" runat="server" Text='<%# Eval("CarrierTicketNum") %>' Width="100%" />
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtCarrierTicketNum" runat="server" Text='<%# Bind("CarrierTicketNum") %>' Width="97%" />
                                            </EditItemTemplate>
                                            <HeaderStyle Width="50px" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn EditFormColumnIndex="0" DataField="OriginDriverID" FilterControlAltText="Filter Driver column" HeaderText="Driver"
                                                                    SortExpression="Driver" UniqueName="DriverID" Groupable="false">
                                            <EditItemTemplate>
                                                <asp:DropDownList ID="ddlDriver" runat="server" DataSourceID="dsDriver" DataTextField="FullName" DataValueField="ID" AutoPostBack="true"
                                                                  OnSelectedIndexChanged="ddlDriver_SelectedIndexChanged" Width="95%">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="rfvDriver" runat="server" ErrorMessage="*" ControlToValidate="ddlDriver" CssClass="NullValidator" />
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblDriver" runat="server" Text='<%# Eval("OriginDriver") %>' Width="100%" />
                                            </ItemTemplate>
                                            <HeaderStyle Width="100px" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridCheckBoxColumn DataField="OriginChainup" HeaderText="Origin Chain-Up" UniqueName="OriginChainup" HeaderStyle-Width="40px" Groupable="false" />
                                        <telerik:GridCheckBoxColumn DataField="DestChainup" HeaderText="Dest Chain-Up" UniqueName="DestChainup" HeaderStyle-Width="40px" Groupable="false" />
                                        <telerik:GridTemplateColumn EditFormColumnIndex="2" DataField="OriginTruckID" FilterControlAltText="Filter Truck column" HeaderText="Truck"
                                                                    SortExpression="Truck" UniqueName="TruckID" Groupable="false">
                                            <EditItemTemplate>
                                                <asp:DropDownList ID="ddlTruck" runat="server" DataSourceID="dsTruck" DataTextField="FullName" DataValueField="ID" Width="100%">
                                                    <%--Manual data binding due to "filtering" based on selected Carrier--%>
                                                </asp:DropDownList>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblTruck" runat="server" Text='<%# Eval("Truck") %>' Width="100%" />
                                            </ItemTemplate>
                                            <HeaderStyle Width="75px" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn EditFormColumnIndex="2" DataField="TrailerID" FilterControlAltText="Filter Trailer column" HeaderText="Trailer"
                                                                    SortExpression="Trailer" UniqueName="TrailerID" Groupable="false">
                                            <EditItemTemplate>
                                                <asp:DropDownList ID="ddlTrailer" runat="server" DataSourceID="dsTrailer" DataTextField="FullName" DataValueField="ID" Width="100%">
                                                    <%--Manual data binding due to "filtering" based on selected Driver--%>
                                                </asp:DropDownList>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblTrailer" runat="server" Text='<%# Eval("Trailer") %>' Width="100%" />
                                            </ItemTemplate>
                                            <HeaderStyle Width="75px" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn EditFormColumnIndex="2" DataField="Trailer2ID" FilterControlAltText="Filter Trailer2 column"
                                                                    HeaderText="Trailer  2" SortExpression="Trailer2" UniqueName="Trailer2ID" Groupable="false">
                                            <EditItemTemplate>
                                                <asp:DropDownList ID="ddlTrailer2" runat="server" DataSourceID="dsTrailer" DataTextField="FullName" DataValueField="ID"
                                                                  Width="100%">
                                                    <%--Manual data binding due to "filtering" based on selected Driver--%>
                                                </asp:DropDownList>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblTrailer2" runat="server" Text='<%# Eval("Trailer2") %>' Width="100%" />
                                            </ItemTemplate>
                                            <HeaderStyle Width="75px" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridBoundColumn DataField="OriginTruckMileage" FilterControlAltText="Filter OriginTruckMileage column"
                                                                 HeaderText="Origin Truck Mileage" SortExpression="OriginTruckMileage" UniqueName="OriginTruckMileage" Groupable="false">
                                            <HeaderStyle Width="75px" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="DestTruckMileage" FilterControlAltText="Filter DestTruckMileage column"
                                                                 HeaderText="Dest Truck Mileage" SortExpression="DestTruckMileage" UniqueName="DestTruckMileage" Groupable="false">
                                            <HeaderStyle Width="75px" />
                                        </telerik:GridBoundColumn>
                                    </Columns>
                                    <EditFormSettings>
                                        <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                        </EditColumn>
                                    </EditFormSettings>
                                    <PagerStyle Visible="false" />
                                </telerik:GridTableView>
                                <telerik:GridTableView runat="server" AllowPaging="False" Name="ProductTableView" AllowSorting="False" EditMode="InPlace"
                                                       AllowFilteringByColumn="false">
                                    <HeaderStyle CssClass="DetailTable_Header" />
                                    <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                    <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                    </RowIndicatorColumn>
                                    <ExpandCollapseColumn Visible="false" />
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="ID" Display="False" FilterControlAltText="Filter ID Column" ReadOnly="True" UniqueName="ID"
                                                                 ForceExtractValue="Always">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridTemplateColumn UniqueName="ActionColumn" ReadOnly="true" Visible="false">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="cmdEdit" runat="server" CommandName="EditDetails" ImageUrl="~/images/edit.png" CausesValidation="false" />
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:ImageButton ID="cmdUpdate" runat="server" CommandName="UpdateDetails" ImageUrl="~/images/apply_imageonly.png" CausesValidation="true" />
                                                <asp:ImageButton ID="cmdCancel" runat="server" CommandName="CancelDetails" ImageUrl="~/images/cancel_imageonly.png" CausesValidation="false" />
                                            </EditItemTemplate>
                                            <HeaderStyle Width="35px" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn EditFormColumnIndex="0" DataField="Origin" HeaderText="Origin"
                                                                    SortExpression="Origin" UniqueName="OriginID" Groupable="false">
                                            <EditItemTemplate>
                                                <asp:DropDownList ID="ddlOrigin" runat="server" DataSourceID="dsOrigin" DataTextField="FullName" DataValueField="ID" AutoPostBack="true" Width="95%" SelectedValue='<%# Bind("OriginID") %>' />
                                                <asp:RequiredFieldValidator ID="rfvOrigin" runat="server" ErrorMessage="*" ControlToValidate="ddlOrigin" CssClass="NullValidator" />
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblOrigin" runat="server" Text='<%# Eval("Origin") %>' Width="100%" />
                                            </ItemTemplate>
                                            <HeaderStyle Width="100px" />
                                        </telerik:GridTemplateColumn>
                                        
                                        <telerik:GridBoundColumn DataField="OriginTimeZone" UniqueName="OriginTimeZone" HeaderText="TZ" SortExpression="OriginTimeZone"
                                                                 ReadOnly="true" ForceExtractValue="Always">
                                            <HeaderStyle Width="35px" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridDateTimeColumn DataField="OriginArriveTime" UniqueName="OriginArriveTime" HeaderText="Origin Arrival"
                                                                    HeaderStyle-Width="145px" DataType="System.DateTime" DataFormatString="{0:M/d/yy hh:mm tt}" EditDataFormatString="M/d/yy hh:mm tt" PickerType=DateTimePicker
                                                                    Groupable="false" />
                                        <telerik:GridDateTimeColumn DataField="OriginDepartTime" UniqueName="OriginDepartTime" HeaderText="Origin Departure"
                                                                    HeaderStyle-Width="145px" DataType="System.DateTime" DataFormatString="{0:M/d/yy hh:mm tt}" EditDataFormatString="M/d/yy hh:mm tt" PickerType=DateTimePicker
                                                                    Groupable="false" />
                                        <telerik:GridTemplateColumn DataField="OriginMinutes" UniqueName="OriginMinutes" HeaderText="Load Min" Groupable="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblOriginMinutes" runat="server" Text='<%# Eval("OriginMinutes") %>' ToolTip='<%# Eval("OriginMinutes") %>'
                                                           Width="100%" />
                                            </ItemTemplate>
                                            <HeaderStyle Width="75px" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn DataField="OriginWaitNotes" UniqueName="OriginWaitNotes" HeaderText="Wait Notes" Groupable="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblOriginWaitNotes" runat="server" Text='<%# Eval("OriginWaitNotes") %>' ToolTip='<%# Eval("OriginWaitNotes") %>'
                                                           Width="100%" />
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtOriginWaitNotes" runat="server" Text='<%# Bind("OriginWaitNotes") %>' ToolTip='<%# Eval("OriginWaitNotes") %>'
                                                             Width="100%" TextMode="MultiLine" Rows="1" />
                                            </EditItemTemplate>
                                            <HeaderStyle Width="85px" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridBoundColumn DataField="TransitMinutes" UniqueName="TransitMinutes" HeaderText="Transit Min" SortExpression="TransitMinutes"
                                                                 ReadOnly="true">
                                            <HeaderStyle Width="80px" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridTemplateColumn EditFormColumnIndex="0" DataField="Destination" FilterControlAltText="Filter Destination column" HeaderText="Destination"
                                                                    SortExpression="Dest" UniqueName="DestinationID" Groupable="false">
                                            <EditItemTemplate>
                                                <asp:DropDownList ID="ddlDestination" runat="server" DataSourceID="dsDestination" DataTextField="FullName" DataValueField="ID" AutoPostBack="true" Width="95%" SelectedValue='<%# Bind("DestinationID") %>' />
                                                <asp:RequiredFieldValidator ID="rfvDestination" runat="server" ErrorMessage="*" ControlToValidate="ddlDestination" CssClass="NullValidator" />
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblDestination" runat="server" Text='<%# Eval("Destination") %>' Width="100%" />
                                            </ItemTemplate>
                                            <HeaderStyle Width="100px" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridBoundColumn DataField="DestTimeZone" UniqueName="DestTimeZone" HeaderText="TZ" SortExpression="DestTimeZone"
                                                                 ReadOnly="true" ForceExtractValue="Always">
                                            <HeaderStyle Width="35px" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridDateTimeColumn DataField="DestArriveTime" UniqueName="DestArriveTime" HeaderText="Dest Arrival"
                                                                    HeaderStyle-Width="145px" DataType="System.DateTime" DataFormatString="{0:M/d/yy hh:mm tt}" EditDataFormatString="M/d/yy hh:mm tt" PickerType=DateTimePicker
                                                                    Groupable="false" />
                                        <telerik:GridDateTimeColumn DataField="DestDepartTime" UniqueName="DestDepartTime" HeaderText="Dest Departure"
                                                                    HeaderStyle-Width="145px" DataType="System.DateTime" DataFormatString="{0:M/d/yy hh:mm tt}" EditDataFormatString="M/d/yy hh:mm tt" PickerType=DateTimePicker
                                                                    Groupable="false" />
                                        <telerik:GridTemplateColumn DataField="DestMinutes" UniqueName="DestMinutes" HeaderText="Unload Min" Groupable="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDestMinutes" runat="server" Text='<%# Eval("DestMinutes") %>' ToolTip='<%# Eval("DestMinutes") %>'
                                                           Width="100%" />
                                            </ItemTemplate>
                                            <HeaderStyle Width="75px" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn DataField="DestWaitNotes" UniqueName="DestWaitNotes" HeaderText="Wait Notes" Groupable="false">
                                            <ItemTemplate>
                                                <asp:Label runat="server" Text='<%# Eval("DestWaitNotes") %>' ToolTip='<%# Eval("DestWaitNotes") %>' Width="100%" />
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtDestWaitNotes" runat="server" Text='<%# Bind("DestWaitNotes") %>' ToolTip='<%# Eval("DestWaitNotes") %>'
                                                             Width="100%" TextMode="MultiLine" Rows="1" />
                                            </EditItemTemplate>
                                            <HeaderStyle Width="85px" />
                                        </telerik:GridTemplateColumn>
                                    </Columns>
                                    <EditFormSettings>
                                        <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                        </EditColumn>
                                    </EditFormSettings>
                                    <PagerStyle Visible="false" />
                                </telerik:GridTableView>
                                <telerik:GridTableView runat="server" AllowPaging="False" Name="TicketTableView" AllowSorting="False" EditMode="InPlace"
                                                       AllowFilteringByColumn="false">
                                    <EditFormSettings>
                                        <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                        </EditColumn>
                                    </EditFormSettings>
                                    <HeaderStyle CssClass="DetailTable_Header" />
                                    <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                    <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                    </RowIndicatorColumn>
                                    <ExpandCollapseColumn Visible="false" />
                                    <Columns>
                                        <telerik:GridTemplateColumn UniqueName="ActionColumn" ReadOnly="true" HeaderText="Tickets" Visible="false">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="cmdEdit" runat="server" CommandName="EditDetails" ImageUrl="~/images/edit.png" CausesValidation="false" />
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:ImageButton ID="cmdUpdate" runat="server" CommandName="UpdateDetails" ImageUrl="~/images/apply_imageonly.png" CausesValidation="true" />
                                                <asp:ImageButton ID="cmdCancel" runat="server" CommandName="CancelDetails" ImageUrl="~/images/cancel_imageonly.png" CausesValidation="false" />
                                            </EditItemTemplate>
                                            <HeaderStyle Width="35px" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridBoundColumn DataField="ID" Display="False" FilterControlAltText="Filter ID Column" ReadOnly="True" UniqueName="ID"
                                                                 ForceExtractValue="Always">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridCheckBoxColumn HeaderText="Deleted?" DataField="IsDeleted" UniqueName="IsDeleted" SortExpression="IsDeleted" GroupByExpression="IsDeleted GROUP BY IsDeleted" ReadOnly="true" HeaderStyle-Width="50px" />
                                        <telerik:GridDateTimeColumn DataField="DeleteDate" UniqueName="DeleteDate" HeaderText="Delete Date"
                                                                    HeaderStyle-Width="90px" DataType="System.DateTime" DataFormatString="{0:M/d/yy hh:mm tt}" EditDataFormatString="M/d/yy hh:mm tt" PickerType=DateTimePicker
                                                                    Groupable="false" />
                                        <telerik:GridTemplateColumn DataField="TicketTypeID" FilterControlAltText="Filter Ticket Type column" HeaderText="Ticket Type"
                                                                    UniqueName="TicketTypeID" Groupable="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTicketType" runat="server" Text='<%# Eval("TicketType") %>' />
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:DropDownList ID="ddlTicketType" runat="server" DataSourceID="dsTicketType" DataTextField="TicketType" DataValueField="ID"
                                                                  SelectedValue='<%# Bind("TicketTypeID") %>' Width="100%" AutoPostBack="true" OnSelectedIndexChanged="ddlTicketType_SelectedIndexChanged" />
                                            </EditItemTemplate>
                                            <HeaderStyle Width="80px" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn DataField="CarrierTicketNum" HeaderText="Ticket #" UniqueName="CarrierTicketNum"
                                                                    SortExpression="CarrierTicketNum" Groupable="false" FilterControlWidth="70%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCarrierTicketNum" runat="server" Text='<%# Eval("CarrierTicketNum") %>' />
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtCarrierTicketNum" runat="server" Text='<%# Bind("CarrierTicketNum") %>' Width="97%" />
                                            </EditItemTemplate>
                                            <HeaderStyle Width="70px" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn DataField="BOLNum" HeaderText="BOL #" UniqueName="BOLNum"
                                                                    SortExpression="BOLNum" Groupable="false" FilterControlWidth="70%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBOLNum" runat="server" Text='<%# Eval("BOLNum") %>' />
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtBOLNum" runat="server" Text='<%# Bind("BOLNum") %>' Width="97%" />
                                            </EditItemTemplate>
                                            <HeaderStyle Width="70px" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn DataField="OriginTankID" HeaderText="Origin Tank"
                                                                    UniqueName="OriginTankID" Groupable="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblOriginTank" runat="server" Text='<%# Eval("OriginTankText") %>' />
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:Label ID="lblOriginTank" runat="server" Text='<%# Eval("OriginTankText") %>' />
                                                <%--<asp:DropDownList ID="ddlOriginTank" runat="server" DataSourceID="dsOriginTank" DataTextField="TankNum" DataValueField="ID"
                                                                      SelectedValue='<%# Bind("OriginTankID") %>' Width="100%" />--%>
                                            </EditItemTemplate>
                                            <HeaderStyle Width="80px" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn DataField="TankNum" HeaderText="Tank Details" UniqueName="TankNum"
                                                                    SortExpression="TankNum" Groupable="false" FilterControlWidth="70%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTankNum" runat="server" Text='<%# Eval("TankNum") %>' />
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtTankNum" runat="server" Text='<%# Bind("TankNum") %>' Width="97%" />
                                            </EditItemTemplate>
                                            <HeaderStyle Width="70px" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn DataField="ProductObsGravity" UniqueName="ProductObsGravity" HeaderText="Gravity" Groupable="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblObsGravity" runat="server" Text='<%# Eval("ProductObsGravity", "{0:#.0}" ) %>' Width="60px" Style="text-align: right" />
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <telerik:RadNumericTextBox ID="txtObsGravity" runat="server" MinValue="25" MaxValue="75" AllowOutOfRangeAutoCorrect="true"
                                                                           AutoCompleteType="None" DataType="System.Decimal" DbValue='<%# Bind("ProductObsGravity") %>' NumberFormat-DecimalDigits="1"
                                                                           Width="100%" Style="text-align: right" AutoPostBack="true" OnTextChanged="CalculateBarrels" />
                                            </EditItemTemplate>
                                            <HeaderStyle Width="60px" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn DataField="ProductObsTemp" UniqueName="ProductObsTemp" HeaderText="Temp" Groupable="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblObsTemp" runat="server" Text='<%# Eval("ProductObsTemp", "{0:#.0}") %>' Width="60px" Style="text-align: right" />
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <telerik:RadNumericTextBox ID="txtObsTemp" runat="server"
                                                                           MinValue='<%# Settings.SettingsID.ProductTempMinID.AsDecimal() %>'
                                                                           MaxValue='<%# Settings.SettingsID.ProductTempMaxID.AsDecimal() %>'
                                                                           AllowOutOfRangeAutoCorrect="true" AutoCompleteType="None"
                                                                           DataType="System.Decimal" DbValue='<%# Bind("ProductObsTemp") %>' NumberFormat-DecimalDigits="1" Width="100%" Style="text-align: right"
                                                                           AutoPostBack="true" OnTextChanged="CalculateBarrels" />
                                            </EditItemTemplate>
                                            <HeaderStyle Width="60px" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn DataField="ProductBSW" UniqueName="ProductBSW" HeaderText="BS&W" Groupable="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBSW" runat="server" Text='<%# Eval("ProductBSW", @"{0:#.000}%") %>' Width="100%" Style="text-align: right" />
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <telerik:RadNumericTextBox ID="txtBSW" runat="server" MinValue="0" MaxValue="100" AllowOutOfRangeAutoCorrect="true" AutoCompleteType="None"
                                                                           DataType="System.Decimal" DbValue='<%# Bind("ProductBSW") %>' NumberFormat-DecimalDigits="3" Width="100%" Style="text-align: right"
                                                                           AutoPostBack="true" OnTextChanged="CalculateBarrels" />
                                            </EditItemTemplate>
                                            <HeaderStyle Width="60px" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn DataField="Rejected" UniqueName="Rejected" HeaderText="Rejected?" Groupable="false">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkRejected" runat="server" Checked='<%# Eval("Rejected") %>' Enabled="false" />
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:CheckBox ID="chkRejected" runat="server" Checked='<%# Bind("Rejected") %>' OnCheckedChanged="chkRejected_CheckChanged"
                                                              AutoPostBack="true" />
                                            </EditItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="60px" />
                                            <HeaderStyle Width="55px" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn DataField="OpeningGaugeFeet" UniqueName="OpeningGaugeFeet" HeaderText="Open FT" Groupable="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblOpeningFeet" runat="server" Text='<%# Eval("OpeningGaugeFeet") %>' />
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <telerik:RadNumericTextBox ID="txtOpeningFeet" runat="server" MinValue="0" MaxValue="20" AllowOutOfRangeAutoCorrect="true"
                                                                           AutoCompleteType="None" DataType="System.Int16" DbValue='<%# Bind("OpeningGaugeFeet") %>' NumberFormat-DecimalDigits="0"
                                                                           Width="100%" Style="text-align: right" AutoPostBack="true" OnTextChanged="GaugeRunValueChanged" />
                                            </EditItemTemplate>
                                            <HeaderStyle Width="40px" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn DataField="OpeningGaugeInch" UniqueName="OpeningGaugeInch" HeaderText="Open IN" Groupable="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblOpeningInch" runat="server" Text='<%# Eval("OpeningGaugeInch") %>' />
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <telerik:RadNumericTextBox ID="txtOpeningInch" runat="server" MinValue="0" MaxValue="11" AllowOutOfRangeAutoCorrect="true"
                                                                           AutoCompleteType="None" DataType="System.Int16" DbValue='<%# Bind("OpeningGaugeInch") %>' NumberFormat-DecimalDigits="0"
                                                                           Width="100%" Style="text-align: right" AutoPostBack="true" OnTextChanged="GaugeRunValueChanged" />
                                            </EditItemTemplate>
                                            <HeaderStyle Width="40px" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn DataField="OpeningGaugeQ" UniqueName="OpeningGaugeQ" HeaderText="Open Q" Groupable="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblOpeningQ" runat="server" Text='<%# Eval("OpeningGaugeQ") %>' />
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <telerik:RadNumericTextBox ID="txtOpeningQ" runat="server" MinValue="0" MaxValue="3" AllowOutOfRangeAutoCorrect="true" AutoCompleteType="None"
                                                                           DataType="System.Int16" DbValue='<%# Bind("OpeningGaugeQ") %>' NumberFormat-DecimalDigits="0" Width="100%" Style="text-align: right"
                                                                           AutoPostBack="true" OnTextChanged="GaugeRunValueChanged" />
                                            </EditItemTemplate>
                                            <HeaderStyle Width="40px" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn DataField="ClosingGaugeFeet" UniqueName="ClosingGaugeFeet" HeaderText="Close FT" Groupable="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblClosingFeet" runat="server" Text='<%# Eval("ClosingGaugeFeet") %>' />
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <telerik:RadNumericTextBox ID="txtClosingFeet" runat="server" MinValue="0" MaxValue="20" AllowOutOfRangeAutoCorrect="true"
                                                                           AutoCompleteType="None" DataType="System.Int16" DbValue='<%# Bind("ClosingGaugeFeet") %>' NumberFormat-DecimalDigits="0"
                                                                           Width="100%" Style="text-align: right" AutoPostBack="true" OnTextChanged="GaugeRunValueChanged" />
                                            </EditItemTemplate>
                                            <HeaderStyle Width="40px" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn DataField="ClosingGaugeInch" UniqueName="ClosingGaugeInch" HeaderText="Close IN" Groupable="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblClosingInch" runat="server" Text='<%# Eval("ClosingGaugeInch") %>' />
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <telerik:RadNumericTextBox ID="txtClosingInch" runat="server" MinValue="0" MaxValue="11" AllowOutOfRangeAutoCorrect="true"
                                                                           AutoCompleteType="None" DataType="System.Int16" DbValue='<%# Bind("ClosingGaugeInch") %>' NumberFormat-DecimalDigits="0"
                                                                           Width="100%" Style="text-align: right" AutoPostBack="true" OnTextChanged="GaugeRunValueChanged" />
                                            </EditItemTemplate>
                                            <HeaderStyle Width="40px" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn DataField="ClosingGaugeQ" UniqueName="ClosingGaugeQ" HeaderText="Close Q" Groupable="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblClosingQ" runat="server" Text='<%# Eval("ClosingGaugeQ") %>' />
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <telerik:RadNumericTextBox ID="txtClosingQ" runat="server" MinValue="0" MaxValue="3" AllowOutOfRangeAutoCorrect="true" AutoCompleteType="None"
                                                                           DataType="System.Int16" DbValue='<%# Bind("ClosingGaugeQ") %>' NumberFormat-DecimalDigits="0" Width="100%" Style="text-align: right"
                                                                           AutoPostBack="true" OnTextChanged="GaugeRunValueChanged" />
                                            </EditItemTemplate>
                                            <HeaderStyle Width="40px" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn DataField="GrossUnits" UniqueName="GrossUnits" HeaderText="Gross Units" Groupable="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblGrossUnits" runat="server" Text='<%# Eval("GrossUnits", "{0:0.000}") %>' />
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:RequiredFieldValidator ID="rfvGrossUnits" runat="server" ControlToValidate="txtGrossUnits" CssClass="NullValidator"
                                                                            Text="*" ErrorMessage="You must calculate/enter the Gross Units" />
                                                <telerik:RadNumericTextBox ID="txtGrossUnits" runat="server" Text='<%# Bind("GrossUnits") %>' Width="50px"
                                                                           Style="text-align: right;" AutoPostBack="true" OnTextChanged="GrossUnitsChanged" NumberFormat-DecimalDigits="3" />
                                            </EditItemTemplate>
                                            <HeaderStyle Width="60px" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn DataField="NetUnits" UniqueName="NetUnits" HeaderText="Net Units" Groupable="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNetUnits" runat="server" Text='<%# Eval("NetUnits", "{0:0.000}") %>' />
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:RequiredFieldValidator ID="rfvNetUnits" runat="server" ControlToValidate="txtNetUnits" CssClass="NullValidator"
                                                                            Text="*" ErrorMessage="You must calculate/enter the Net Units" />
                                                <telerik:RadNumericTextBox ID="txtNetUnits" runat="server" Text='<%# Bind("NetUnits") %>'
                                                                           Width="50px" Style="text-align: right;" NumberFormat-DecimalDigits="3" />
                                            </EditItemTemplate>
                                            <HeaderStyle Width="60px" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn DataField="WeightNetUnits" UniqueName="WeightNetUnits" HeaderText="Net Weight" Groupable="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblWeightNetUnits" runat="server" Text='<%# Eval("WeightNetUnits", "{0:0.000}") %>' />
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:RequiredFieldValidator ID="rfvWeightNetUnits" runat="server" ControlToValidate="txtWeightNetUnits" CssClass="NullValidator"
                                                                            Text="*" ErrorMessage="You must calculate/enter the Net Weight Units" />
                                                <telerik:RadNumericTextBox ID="txtWeightNetUnits" runat="server" Text='<%# Bind("WeightNetUnits") %>'
                                                                           Width="50px" Style="text-align: right;" NumberFormat-DecimalDigits="3" />
                                            </EditItemTemplate>
                                            <HeaderStyle Width="60px" />
                                        </telerik:GridTemplateColumn>
                                    </Columns>
                                </telerik:GridTableView>
                                <telerik:GridTableView runat="server" AllowPaging="False" Name="ReroutesTableView" AllowSorting="False" EditMode="InPlace"
                                                       AllowFilteringByColumn="false">
                                    <EditFormSettings>
                                        <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                        </EditColumn>
                                    </EditFormSettings>
                                    <HeaderStyle CssClass="DetailTable_Header" />
                                    <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                    <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                    </RowIndicatorColumn>
                                    <ExpandCollapseColumn Visible="false" />
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="OrderID" Display="False" Visible="false" FilterControlAltText="Filter OrderID Column" ReadOnly="True"
                                                                 UniqueName="OrderID" ForceExtractValue="Always">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridTemplateColumn HeaderText="Reroutes" HeaderStyle-Width="100px" />
                                        <telerik:GridTemplateColumn DataField="PreviousDestinationFull" UniqueName="PreviousDestinationID" HeaderText="Previous Destination"
                                                                    SortExpression="PreviousDestinationFull" Groupable="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPreviousDestination" runat="server" Width="100%" Text='<%# Eval("PreviousDestinationFull") %>' />
                                            </ItemTemplate>
                                            <HeaderStyle Width="100px" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn DataField="RerouteDate" UniqueName="RerouteDate" HeaderText="Date Rerouted" SortExpression="RerouteDate"
                                                                    Groupable="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRerouteDate" runat="server" Width="100%" Text='<%# Eval("RerouteDate", "{0:M/d/yy h:mm tt}") %>' />
                                            </ItemTemplate>
                                            <HeaderStyle Width="90px" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn DataField="UserName" UniqueName="UserName" HeaderText="User" SortExpression="UserName" Groupable="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblUserName" runat="server" Width="100%" Text='<%# Eval("UserName") %>' />
                                            </ItemTemplate>
                                            <HeaderStyle Width="90px" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn DataField="Notes" UniqueName="Notes" HeaderText="Notes" SortExpression="Notes" Groupable="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNotes" runat="server" Width="100%" Text='<%# Eval("Notes") %>' />
                                            </ItemTemplate>
                                            <HeaderStyle Width="150px" />
                                        </telerik:GridTemplateColumn>
                                    </Columns>
                                </telerik:GridTableView>
                                <telerik:GridTableView runat="server" AllowPaging="False" Name="AuditNotesTableView" CssClass="DetailTable"
                                                       AllowSorting="False" AllowFilteringByColumn="false" EditMode="InPlace">
                                    <HeaderStyle CssClass="DetailTable_Header" />
                                    <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                    <RowIndicatorColumn Visible="True">
                                    </RowIndicatorColumn>
                                    <ExpandCollapseColumn Visible="false" />
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="ID" Display="False" ReadOnly="True" UniqueName="ID"
                                                                 ForceExtractValue="Always">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridTemplateColumn UniqueName="ActionColumn" ReadOnly="true">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="cmdEdit" runat="server" CommandName="EditDetails" ImageUrl="~/images/edit.png" CausesValidation="false" />
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:ImageButton ID="cmdUpdate" runat="server" CommandName="UpdateDetails" ImageUrl="~/images/apply_imageonly.png" CausesValidation="true" />
                                                <asp:ImageButton ID="cmdCancel" runat="server" CommandName="CancelDetails" ImageUrl="~/images/cancel_imageonly.png" CausesValidation="false" />
                                            </EditItemTemplate>
                                            <HeaderStyle Width="35px" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn EditFormColumnIndex="0" DataField="AuditNotes" HeaderText="Correction Notes"
                                                                    UniqueName="AuditNotes" DataType="System.String" Groupable="false">
                                            <EditItemTemplate>
                                                <telerik:RadTextBox ID="txtAuditNotes" runat="server" Text='<%# Bind("AuditNotes") %>' TextMode="MultiLine"
                                                                    MaxLength="255" Rows="2" Width="100%" />
                                                <asp:Label ID="lblErrors" runat="server" Text='<%# Eval("Errors") %>' ForeColor="Red" Width="100%" />
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblAuditNotes" runat="server" Text='<%# Eval("AuditNotes") %>' Width="100%" />
                                                <asp:Label ID="lblErrors" runat="server" Text='<%# Eval("Errors") %>' ForeColor="Red" Width="100%" />
                                            </ItemTemplate>
                                            <HeaderStyle Width="100%" />
                                        </telerik:GridTemplateColumn>
                                    </Columns>
                                </telerik:GridTableView>
                            </DetailTables>
                            <CommandItemSettings ExportToPdfText="Export to PDF" ShowAddNewRecordButton="false" />
                            <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                <HeaderStyle Width="20px"></HeaderStyle>
                            </RowIndicatorColumn>
                            <ExpandCollapseColumn Visible="True">
                                <HeaderStyle Width="20px"></HeaderStyle>
                            </ExpandCollapseColumn>
                            <Columns>
                                <telerik:GridTemplateColumn UniqueName="ActionColumn" ReadOnly="true" HeaderText="" AllowFiltering="false" HeaderStyle-Width="110px" HeaderStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Button ID="btnRevert" runat="server" Text="Go Back" CommandName="Revert" CssClass="btn btn-yellow btn-xs shiny" CausesValidation="false" />
                                        <div class="spacer5px"></div>
                                        <asp:Button CssClass="btn btn-blue btn-xs shiny" ID="btnSave" runat="server" CommandName="SaveAll" Text="Save" CausesValidation="true" />
                                        <asp:Button CssClass="btn btn-success btn-xs shiny" ID="btnDone" runat="server" CommandName="Done" Text="Done" CausesValidation="true" Visible='<%# !Converter.ToBoolean(Eval("HasError")) %>' />
                                        <asp:Button CssClass="btn btn-danger btn-xs shiny" ID="btnErrors" runat="server" CommandName="ExpandAll" Text="Errors" CausesValidation="false"
                                                    ToolTip='<%# Eval("Errors") %>' Visible='<%# Converter.ToBoolean(Eval("HasError")) %>' />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridCheckBoxColumn DataField="HasError" HeaderText="Errors?" ReadOnly="true" UniqueName="HasError" HeaderStyle-Width="60px" />
                                <telerik:GridTemplateColumn HeaderText="Order #" DataField="OrderNum" UniqueName="OrderNum"
                                                            SortExpression="OrderNum" DataType="System.Int32" Display="true" Visible="true" FilterControlWidth="65%" Groupable="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblOrderNum" runat="server" Text='<%# Eval("OrderNum") %>' Width="80px" />
                                    </ItemTemplate>
                                    <HeaderStyle Width="75px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="DispatchConfirmNum" UniqueName="DispatchConfirmNum" HeaderText="Shipper PO #"
                                                         HeaderStyle-Width="80px" FilterControlWidth="60%" />
                                <telerik:GridBoundColumn DataField="JobNumber" UniqueName="JobNumber" HeaderText="Job #"
                                                         HeaderStyle-Width="80px" FilterControlWidth="60%" />
                                <telerik:GridBoundColumn DataField="ContractNumber" UniqueName="ContractNumber" HeaderText="Contract #"
                                                         HeaderStyle-Width="80px" FilterControlWidth="60%" />
                                <telerik:GridBoundColumn HeaderText="Order Date" Datafield="OrderDate" SortExpression="OrderDate" UniqueName="OrderDate"
                                                         HeaderStyle-Width="80px" DataFormatString="{0:M/d/yy}" FilterControlWidth="60%" />
                                <telerik:GridBoundColumn HeaderText="Tickets" DataField="Tickets" SortExpression="Tickets" UniqueName="Tickets" HeaderStyle-Width="70px" FilterControlWidth="70%" Groupable="true" />
                                <telerik:GridCheckBoxColumn DataField="Rejected" HeaderText="Rejected?" SortExpression="Rejected"
                                                            UniqueName="Rejected" FilterControlWidth="50%" HeaderStyle-Width="65px" />
                                <telerik:GridTemplateColumn DataField="Origin" HeaderText="Origin" SortExpression="Origin"
                                                            UniqueName="Origin" FilterControlWidth="70%" GroupByExpression="Origin GROUP BY Origin">
                                    <ItemTemplate>
                                        <asp:Label ID="gridlblOrigin" runat="server" Text='<%# Eval("Origin") %>' Width="100%" />
                                    </ItemTemplate>
                                    <HeaderStyle Width="175px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="OriginTerminal" UniqueName="OriginTerminal" HeaderText="Origin Terminal" SortExpression="OriginTerminal"
                                                                 ReadOnly="true" ForceExtractValue="Always" FilterControlWidth="50%" GroupByExpression="OriginTerminal GROUP BY OriginTerminal">
                                            <HeaderStyle Width="150px" />
                                        </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn DataField="OriginDistanceText" UniqueName="OriginDistance" HeaderText="DTP(m)" HeaderStyle-Width="70px" FilterControlWidth="60%"
                                                            SortExpression="OriginDistance">
                                    <ItemTemplate>
                                        <asp:Label ID="lblOriginDTP" runat="server" Text='<%# Eval("OriginDistanceText") %>'
                                                   ToolTip='<%# string.Format("GPS Lat/Lon: {0}\r\nOrigin Lat/Lon: {1}"
                                , Converter.IfNullOrEmpty(Eval("OriginGpsLatLon"), "N/A")
                                , Converter.IfNullOrEmpty(Eval("OriginLatLon"), "N/A")) %>' />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn HeaderText="Origin BOL #" DataField="OriginBOLNum" UniqueName="OriginBOLNum" SortExpression="OriginBOLNum"
                                                            FilterControlAltText="Filter Origin BOL # column" FilterControlWidth="70%" Groupable="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblOriginBOLNum" runat="server" Text='<%# Eval("OriginBOLNum") %>' Width="100%" />
                                    </ItemTemplate>
                                    <HeaderStyle Width="80px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn HeaderText="Origin Gross" DataField="OriginGrossUnits" UniqueName="OriginGrossUnits" SortExpression="OriginGrossUnits"
                                                            FilterControlAltText="Filter Origin Gross Vol column" FilterControlWidth="70%" Groupable="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblOriginGrossUnits" runat="server" Text='<%# Eval("OriginGrossUnits", "{0:0.000}") %>' Width="100%" />
                                    </ItemTemplate>
                                    <HeaderStyle Width="80px" />
                                    <ItemStyle HorizontalAlign="Right" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn HeaderText="Origin Net" DataField="OriginNetUnits" UniqueName="OriginNetUnits" SortExpression="OriginNetUnits"
                                                            FilterControlAltText="Filter Origin Net Vol column" FilterControlWidth="70%" Groupable="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblOriginNetUnits" runat="server" Text='<%# Eval("OriginNetUnits", "{0:0.000}") %>' Width="100%" />
                                    </ItemTemplate>
                                    <HeaderStyle Width="80px" />
                                    <ItemStyle HorizontalAlign="Right" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn DataField="Destination" HeaderText="Destination"
                                                            SortExpression="Destination" UniqueName="Destination" FilterControlWidth="70%" GroupByExpression="Destination GROUP BY Destination">
                                    <ItemTemplate>
                                        <asp:Label ID="gridlblDestination" runat="server" Text='<%# Eval("Destination") %>' Width="100%" />
                                    </ItemTemplate>
                                    <HeaderStyle Width="165px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="DestTerminal" UniqueName="DestTerminal" HeaderText="Destination Terminal" SortExpression="DestTerminal"
                                                         ReadOnly="true" ForceExtractValue="Always" FilterControlWidth="50%" GroupByExpression="DestTerminal GROUP BY DestTerminal">
                                    <HeaderStyle Width="150px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn DataField="DestDistanceText" UniqueName="DestDistance" HeaderText="DTP(m)" HeaderStyle-Width="70px" FilterControlWidth="60%"
                                                            SortExpression="DestDistance" ItemStyle-Wrap="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDestDTP" runat="server" width="100%"
                                                   Text='<%# Eval("DestDistanceText") %>'
                                                   ToolTip='<%# string.Format("GPS Lat/Lon: {0}\r\nDest Lat/Lon: {1}"
                                , Converter.IfNullOrEmpty(Eval("DestGpsLatLon"), "N/A"), Converter.IfNullOrEmpty(Eval("DestLatLon"), "N/A")) %>' />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn HeaderText="Dest BOL #" DataField="DestBOLNum" UniqueName="DestBOLNum" SortExpression="DestBOLNum"
                                                            FilterControlAltText="Filter Dest BOL # column" FilterControlWidth="70%" Groupable="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDestBOLNum" runat="server" Text='<%# Eval("DestBOLNum") %>' Width="100%" />
                                    </ItemTemplate>
                                    <HeaderStyle Width="80px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn DataField="Customer" HeaderText="Shipper" SortExpression="Customer"
                                                            UniqueName="Customer" FilterControlWidth="70%" GroupByExpression="Customer GROUP BY Customer">
                                    <ItemTemplate>
                                        <asp:Label ID="gridlblCustomer" runat="server" Text='<%# Eval("Customer") %>' Width="100%" />
                                    </ItemTemplate>
                                    <HeaderStyle Width="145px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn DataField="Carrier" HeaderText="Carrier" SortExpression="Carrier"
                                                            UniqueName="Carrier" FilterControlWidth="70%" GroupByExpression="Carrier GROUP BY Carrier">
                                    <ItemTemplate>
                                        <asp:Label ID="gridlblCarrier" runat="server" Text='<%# Eval("Carrier") %>' Width="100%" />
                                    </ItemTemplate>
                                    <HeaderStyle Width="145px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn DataField="OriginDriver" HeaderText="Driver" SortExpression="Driver"
                                                            UniqueName="Driver" FilterControlWidth="70%" GroupByExpression="OriginDriver GROUP BY OriginDriver">
                                    <ItemTemplate>
                                        <asp:Label ID="gridlblDriver" runat="server" Text='<%# Eval("OriginDriver") %>' Width="100%" />
                                    </ItemTemplate>
                                    <HeaderStyle Width="145px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="DriverTerminal" UniqueName="DriverTerminal" HeaderText="Driver Terminal" SortExpression="DriverTerminal"
                                                         ReadOnly="true" ForceExtractValue="Always" FilterControlWidth="50%" GroupByExpression="DriverTerminal GROUP BY DriverTerminal">
                                    <HeaderStyle Width="150px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn UniqueName="OriginGpsLatLon" HeaderText="Origin L/L" HeaderStyle-Width="140px" FilterControlWidth="60%"
                                                            SortExpression="OriginGpsLatLon" ItemStyle-Wrap="true">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDriverOrigGPSHeader" runat="server" width="100%"
                                                   Text="GPS Lat/Lon: " />
                                        <asp:Hyperlink ID="lblOriginGPSLL" runat="server" width="100%"
                                                       Text='<%# string.Format("{0}", Converter.IfNullOrEmpty(Eval("OriginGpsLatLon"), "")) %>'
                                                       NavigateUrl='<%# string.Format("http://maps.google.com/maps?z=12&t=k&q=loc:{0}", Converter.IfNullOrEmpty(Eval("OriginGpsLatLon"), "")) %>'
                                                       Target="_blank" />
                                        <asp:Label ID="lblOriginGPSHeader" runat="server" width="100%"
                                                   Text="Origin Lat/Lon: " />
                                        <asp:Hyperlink ID="lblOriginLL" runat="server" width="100%"
                                                       Text='<%# string.Format("{0}", Converter.IfNullOrEmpty(Eval("OriginLatLon"), "")) %>'
                                                       NavigateUrl='<%# string.Format("http://maps.google.com/maps?z=12&t=k&q=loc:{0}", Converter.IfNullOrEmpty(Eval("OriginLatLon"), "")) %>'
                                                       Target="_blank" />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn UniqueName="DestGpsLatLon" HeaderText="Dest L/L" HeaderStyle-Width="140px" FilterControlWidth="60%"
                                                            SortExpression="DestGpsLatLon" ItemStyle-Wrap="true">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDriverDestGPSHeader" runat="server" width="100%"
                                                   Text="GPS Lat/Lon: " />
                                        <asp:Hyperlink ID="lblDestGPSLL" runat="server" width="100%"
                                                       Text='<%# string.Format("{0}", Converter.IfNullOrEmpty(Eval("DestGpsLatLon"), "")) %>'
                                                       NavigateUrl='<%# string.Format("http://maps.google.com/maps?z=12&t=k&q=loc:{0}", Converter.IfNullOrEmpty(Eval("DestGpsLatLon"), "")) %>'
                                                       Target="_blank" />
                                        <asp:Label ID="lblDestGPSHeader" runat="server" width="100%"
                                                   Text="Dest Lat/Lon: " />
                                        <asp:Hyperlink ID="lblDestLL" runat="server" width="100%"
                                                       Text='<%# string.Format("{0}", Converter.IfNullOrEmpty(Eval("DestLatLon"), "")) %>'
                                                       NavigateUrl='<%# string.Format("http://maps.google.com/maps?z=12&t=k&q=loc:{0}", Converter.IfNullOrEmpty(Eval("DestLatLon"), "")) %>'
                                                       Target="_blank" />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                            </Columns>
                            <EditFormSettings ColumnNumber="3">
                                <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                </EditColumn>
                            </EditFormSettings>
                            <PagerStyle AlwaysVisible="True" />
                        </MasterTableView>
                        <HeaderStyle Wrap="False" />
                        <PagerStyle AlwaysVisible="True" />
                        <FilterMenu EnableImageSprites="False" />
                    </telerik:RadGrid>
                </div>
                <telerik:RadScriptBlock runat="server">
                    <script type="text/javascript">
                    </script>
                </telerik:RadScriptBlock>
                
                <blac:DBDataSource ID="dsMain" runat="server"
                                   SelectCommand="SELECT * FROM fnOrders_AllTickets_Audit(@CarrierID, @DriverID, @TerminalID, @OrderNum, NULL) ORDER BY OrderDate, OrderNum">
                    <SelectParameters>
                        <asp:ProfileParameter Name="CarrierID" PropertyName="CarrierID" ConvertEmptyStringToNull="false" DbType="Int32" DefaultValue="0" />
                        <asp:ProfileParameter Name="TerminalID" PropertyName="TerminalID" ConvertEmptyStringToNull="true" DbType="Int32" DefaultValue="-1" />
                        <asp:Parameter Name="DriverID" DbType="Int32" DefaultValue="-1" />
                        <asp:Parameter Name="OrderNum" DbType="Int32" DefaultValue="0" />
                    </SelectParameters>
                </blac:DBDataSource>
                <blc:RadGridDBCtrl ID="dbcMain" runat="server" ControlID="rgMain" UpdateTableName="tblOrder" />
                <blac:DBDataSource ID="dsCarrier" runat="server"
                                   SelectCommand="SELECT ID, Name FROM dbo.tblCarrier ORDER BY Name">
                </blac:DBDataSource>
                <blac:DBDataSource ID="dsDriver" runat="server"
                                   SelectCommand="SELECT ID, FullName FROM dbo.viewDriver WHERE (@CarrierID=-1 OR CarrierID=@CarrierID) UNION SELECT 0, '(Select Driver)' ORDER BY FullName">
                    <SelectParameters>
                        <%--using a basic "Parameter" since we are using manual data binding (we just use the DefaultValue as the value, which can be assigned in code behind)--%>
                        <asp:Parameter Name="CarrierID" DbType="Int32" DefaultValue="-1" />
                    </SelectParameters>
                </blac:DBDataSource>
                <blac:DBDataSource ID="dsOrigin" runat="server" SelectCommand="SELECT ID, FullName FROM viewOrigin WHERE Active = 1 ORDER BY FullName" />
                <blac:DBDataSource ID="dsDestination" runat="server" SelectCommand="SELECT ID, FullName FROM viewDestination wHERE Active = 1 ORDER BY FullName" />
                <blac:DBDataSource ID="dsTruck" runat="server"
                                   SelectCommand="SELECT ID, FullName FROM dbo.viewTruck WHERE DeleteDateUTC IS NULL AND (@CarrierID=-1 OR CarrierID=@CarrierID) UNION SELECT 0, '(Select Truck)' ORDER BY FullName">
                    <SelectParameters>
                        <%--using a basic "Parameter" since we are using manual data binding (we just use the DefaultValue as the value, which can be assigned in code behind)--%>
                        <asp:Parameter Name="CarrierID" DbType="Int32" DefaultValue="-1" />
                    </SelectParameters>
                </blac:DBDataSource>
                <blac:DBDataSource ID="dsTrailer" runat="server"
                                   SelectCommand="SELECT ID, FullName FROM dbo.viewTrailer WHERE (@CarrierID=-1 OR CarrierID=@CarrierID) UNION SELECT 0, '(Select Trailer)' ORDER BY FullName">
                    <SelectParameters>
                        <%--using a basic "Parameter" since we are using manual data binding (we just use the DefaultValue as the value, which can be assigned in code behind)--%>
                        <asp:Parameter Name="CarrierID" DbType="Int32" DefaultValue="-1" />
                    </SelectParameters>
                </blac:DBDataSource>
                <blac:DBDataSource ID="dsTicketType" runat="server"
                                   SelectCommand="SELECT ID, Name AS TicketType FROM dbo.tblTicketType ORDER BY Name">
                </blac:DBDataSource>
                <blac:DBDataSource ID="dsOriginTank" runat="server"
                                   SelectCommand="SELECT OT.ID, OT.TankNum FROM dbo.tblOriginTank OT JOIN tblOrder O ON OriginID = OT.OriginID JOIN tblOrderTicket OT2 ON OT2.OrderID = O.ID WHERE OT2.ID = @ID ORDER BY OT.TankNum">
                    <SelectParameters>
                        <asp:Parameter Name="ID" DbType="Int32" DefaultValue="0" />
                    </SelectParameters>
                </blac:DBDataSource>
            </div>
        </div>
    </div>
</asp:Content>
