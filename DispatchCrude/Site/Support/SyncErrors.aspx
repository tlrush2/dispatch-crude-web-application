﻿<%@ Page Title="Logs" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SyncErrors.aspx.cs"
    Inherits="DispatchCrude.Site.Support.SyncErrors" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="contentMain" ContentPlaceHolderID="MainContent" runat="server">
    <script>
        $("#ctl00_ctl00_EntityCaption").html("Sync Errors");
    </script>
    <div class="tabbable TabRepaint">
        <ul class="nav nav-tabs" id="tabmenu" runat="server">
            <!--Tabs will print here (see code behind)-->
        </ul>
        <div class="tab-content">
            <div class="tab-pane in active">

                <div class="leftpanel">
                    <div class="well with-header">
                        <div class="header bordered-blue">Filters</div>
                        <asp:Panel ID="panelGenerate" runat="server" DefaultButton="cmdFilter">
                            <div class="Entry">
                                <asp:Label ID="lblDriver" runat="server" Text="Driver" AssociatedControlID="rcbDriver" CssClass="Entry" />
                                <telerik:RadComboBox Width="100%" ID="rcbDriver" runat="server" EmptyMessage="Select..."
                                                     DataSourceID="dsDriver" DataTextField="Name" DataValueField="ID" />
                            </div>
                            <div>
                                <div class="Entry floatLeft">
                                    <asp:Label ID="lblStartDate" runat="server" Text="Start Date" AssociatedControlID="rdpStart" CssClass="Entry" />
                                    <telerik:RadDatePicker ID="rdpStart" runat="server" Width="100px">
                                        <DateInput runat="server" DateFormat="M/d/yyyy" />
                                        <DatePopupButton Enabled="true" />
                                    </telerik:RadDatePicker>
                                </div>
                                <div class="Entry floatRight">
                                    <asp:Label ID="lblEndDate" runat="server" Text="End Date" AssociatedControlID="rdpEnd" CssClass="Entry" />
                                    <telerik:RadDatePicker ID="rdpEnd" runat="server" Width="100px">
                                        <DateInput runat="server" DateFormat="M/d/yyyy" />
                                        <DatePopupButton />
                                    </telerik:RadDatePicker>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="spacer5px"></div>
                            <div style="text-align: center;">
                                <asp:Button ID="cmdFilter" runat="server" Text="Refresh" CssClass="btn btn-blue shiny"
                                            UseSubmitBehavior="true" OnClick="cmdFilter_Click" />
                            </div>
                        </asp:Panel>
                    </div>
                </div>
                <div id="gridArea" style="height:100%">
                    <telerik:RadGrid ID="rgMain" runat="server" EnableHeaderContextMenu="False" Height="800" CssClass="GridRepaint" CellSpacing="0" GridLines="None"
                                     AllowSorting="True" ShowGroupPanel="true" AllowFilteringByColumn="true" EnableViewState="true" AllowMultiRowSelection="true" EnableEmbeddedSkins="true" Skin="Vista" AlternatingItemStyle-BackColor="#dcf2fc"
                                     OnNeedDataSource="grid_NeedDataSource" AllowPaging="True"
                                     PageSize='<%# Settings.DefaultPageSize %>'>
                        <ClientSettings AllowDragToGroup="true">
                            <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                            <Selecting AllowRowSelect="true" />
                            <ClientEvents />
                        </ClientSettings>
                        <GroupingSettings ShowUnGroupButton="true" CaseSensitive="false" />
                        <MasterTableView AutoGenerateColumns="False" DataKeyNames="ID" CommandItemDisplay="None">
                            <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                            <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                <HeaderStyle Width="20px"></HeaderStyle>
                            </RowIndicatorColumn>
                            <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                <HeaderStyle Width="20px"></HeaderStyle>
                            </ExpandCollapseColumn>
                            <Columns>
                                <telerik:GridBoundColumn DataField="Driver" UniqueName="Driver" HeaderText="Driver" SortExpression="Driver"
                                                         ReadOnly="true" AllowFiltering="true">
                                    <HeaderStyle Width="140px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridDateTimeColumn DataField="CreateDate" UniqueName="CreateDate" HeaderText="Sync Date" SortExpression="CreateDate"
                                                            DataFormatString="{0:M/d/yyyy}" ReadOnly="true" FilterControlWidth="70%">
                                    <HeaderStyle Width="120px" />
                                </telerik:GridDateTimeColumn>
                                <telerik:GridBoundColumn DataField="ErrorMsg" UniqueName="ErrorMsg" HeaderText="Sync Error Msg" SortExpression="ErrorMsg"
                                                         ReadOnly="true" AllowFiltering="true" FilterControlWidth="70%">
                                    <HeaderStyle Width="800px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ID" UniqueName="ID" Visible="false" DataType="System.Int32" ForceExtractValue="Always" />
                            </Columns>
                            <PagerStyle AlwaysVisible="True" />
                        </MasterTableView>
                        <HeaderStyle Wrap="False" />
                        <PagerStyle AlwaysVisible="True" />
                        <FilterMenu EnableImageSprites="False" />
                    </telerik:RadGrid>
                </div>
                <telerik:RadScriptBlock runat="server">
                    <script type="text/javascript">
                        $(document).ready(function docReady() {
                            debugger;
                            //$("#cmdExportToExcel").addClass("NOAJAX");
                        });
                    </script>
                </telerik:RadScriptBlock>
                <blac:DBDataSource ID="dsDriver" runat="server" SelectCommand="SELECT ID, Name = FullName FROM viewDriver WHERE DeleteDateUTC IS NULL ORDER BY Name" />
            </div>
        </div>
    </div>
</asp:Content>
