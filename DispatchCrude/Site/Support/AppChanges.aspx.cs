﻿using System;
//add for sql stuff
using System.Data;
using Telerik.Web.UI;
using AlonsIT;
using DispatchCrude.Extensions;

namespace DispatchCrude.Site.Support
{
    public partial class AppChanges : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            App_Code.RadAjaxHelper.AddAjaxSetting(this.Page, this.rgMain, this.rgMain);
            RadAjaxManager.GetCurrent(this.Page).EnableAJAX = true;

            //Print tab navigation html to page
            tabmenu.InnerHtml = NavigationHelper.PrintTabArray(NavigationHelper.TabSet_Logs, "Tab_AppChangeHistory").ToString();
        }

        protected void grid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                if (DBHelper.ToBoolean((e.Item.DataItem as DataRowView)["ForPublic"]))
                    e.Item.Font.Bold = true;
            }
        }

    }
}