﻿<%@ Page Title="Logs" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AppChanges.aspx.cs" Inherits="DispatchCrude.Site.Support.AppChanges" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="headContent" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="mainContent" ContentPlaceHolderID="MainContent" runat="server">
    <script>
        $("#ctl00_ctl00_EntityCaption").html("App Change History");
    </script>
    <div class="tabbable TabRepaint">
        <ul class="nav nav-tabs" id="tabmenu" runat="server">
            <!--Tabs will print here (see code behind)-->
        </ul>
        <div class="tab-content">
            <div class="tab-pane in active">

                <div id="DataEntryFormGrid" style="height:100%">
                    <telerik:RadGrid ID="rgMain" runat="server" CellSpacing="0" GridLines="None" CssClass="GridRepaint" EnableLinqExpressions="false" Height="800"
                                     AutoGenerateColumns="False" ShowGroupPanel="false" AllowFilteringByColumn="true" AllowSorting="True" AllowPaging="True" EnableEmbeddedSkins="true" Skin="Vista" AlternatingItemStyle-BackColor="#dcf2fc"
                                     PageSize='<%# Settings.DefaultPageSize %>'
                                     OnItemDataBound="grid_ItemDataBound">
                        <ClientSettings AllowDragToGroup="True">
                            <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                        </ClientSettings>
                        <SortingSettings SortedBackColor="Transparent" />
                        <GroupingSettings CaseSensitive="false" ShowUnGroupButton="true" />
                        <ExportSettings FileName="AppChanges" ExportOnlyData="true" IgnorePaging="true" HideStructureColumns="true" Excel-FileExtension="xls" Excel-Format="Biff" OpenInNewWindow="true" />
                        <MasterTableView DataKeyNames="ID" CommandItemDisplay="Top">
                            <CommandItemSettings ShowAddNewRecordButton="false" ShowExportToExcelButton="true" />
                            <SortExpressions>
                                <telerik:GridSortExpression FieldName="VersionInt" SortOrder="Descending" />
                            </SortExpressions>
                            <Columns>
                                <telerik:GridBoundColumn DataField="VersionNum" HeaderText="Version #" UniqueName="VersionNum" SortExpression="VersionInt"
                                                         HeaderStyle-Width="100px" FilterControlWidth="70%" AllowSorting="true" />
                                <telerik:GridBoundColumn DataField="VersionInt" UniqueName="VersionInt" Visible="false" />
                                <telerik:GridCheckBoxColumn DataField="ForPublic" UniqueName="ForPublic" HeaderText="Major" HeaderStyle-Width="60px" FilterControlWidth="60%" />
                                <telerik:GridDateTimeColumn DataField="ChangeDate" HeaderText="Date" UniqueName="ChangeDate"
                                                            DataFormatString="{0:M/d/yyyy}" HeaderStyle-Width="95px" FilterControlWidth="70%">
                                </telerik:GridDateTimeColumn>
                                <telerik:GridBoundColumn DataField="ChangeDescription" HeaderText="Description" UniqueName="ChangeDescription" FilterControlWidth="70%" />
                            </Columns>
                            <EditFormSettings ColumnNumber="1">
                                <EditColumn FilterControlAltText="Filter EditCommandColumn column" ButtonType="ImageButton" CancelImageUrl="~/images/cancel.png"
                                            UpdateImageUrl="~/images/apply.png" InsertImageUrl="~/images/apply.png" />
                            </EditFormSettings>
                            <PagerStyle AlwaysVisible="True" />
                        </MasterTableView>
                        <HeaderStyle Wrap="False" />
                        <PagerStyle AlwaysVisible="True" />
                        <FilterMenu EnableImageSprites="False">
                        </FilterMenu>
                    </telerik:RadGrid>
                </div>
                <blc:RadGridDBCtrl ID="dbcMain" runat="server" ControlID="rgMain" SelectCommand="SELECT * FROM dbo.viewAppChanges" />
            </div>
        </div>
    </div>
</asp:Content>