﻿using System;
using System.Web;
//add for sql stuff
using Telerik.Web.UI;
using DispatchCrude.App_Code;
using System.Web.UI;
using System.Web.UI.WebControls;
using AlonsIT;
using DispatchCrude.Core;
using DispatchCrude.Extensions;

namespace DispatchCrude.Site.DecisionSupport
{
    public partial class OdometerLog : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            rdpStartDate.DbSelectedDate = DateTime.Now.Date.AddDays(1-DateTime.Now.Date.Day);
            rdpEndDate.DbSelectedDate = DateTime.Now.Date;
            rdpStartDate.Calendar.ShowRowHeaders = rdpEndDate.Calendar.ShowRowHeaders = false;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ConfigureAjax(!Settings.SettingsID.DisableAJAX.AsBool());

            //Print tab navigation html to page
            tabmenu.InnerHtml = NavigationHelper.PrintTabArray(NavigationHelper.TabSet_Resources, "Tab_OdometerLogs").ToString();
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
            }
        }

        private void ConfigureAjax(bool enabled = true)
        {
            if (enabled)
            {
                RadAjaxHelper.AddAjaxSetting(this.Page, btnRefresh, rgMain, true);
                RadAjaxHelper.AddAjaxSetting(this.Page, rgMain, rgMain);
            }
            RadAjaxManager.GetCurrent(this.Page).EnableAJAX = enabled;
        }

        protected void filterValueChanged(object sender, EventArgs e)
        {
            rgMain.Rebind();
        }

        protected void grid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.EditCommandName)
            {
                if (e.Item is GridEditableItem)
                {
                }
            }
            else if (e.CommandName == RadGrid.UpdateCommandName)
            {
                e.Canceled = true;
                string loadtype = "Loaded";

                if (e.Item is GridEditFormItem)
                {
                    GridEditFormItem item = (GridEditFormItem)e.Item;
                    loadtype = item.ParentItem["Type"].Text;
                }

                using (SSDB ssdb = new SSDB())
                {
                    if (loadtype == "Loaded (1st leg)")
                    {
                        // Update origin truck mileage (transfer)
                        ssdb.ExecuteSql(
                              "UPDATE tblOrder SET OriginTruckMileage = {1}, LastChangeDateUTC = getutcdate(), LastChangedByUser = {3} "
                                + "WHERE ID = {0}"
                            , (object)RadGridHelper.GetGridItemID(e.Item)
                            , ((object)RadGridHelper.GetControlByType(e.Item, "StartOdometer", typeof(TextBox)) as TextBox).Text
                            , ((object)RadGridHelper.GetControlByType(e.Item, "EndOdometer", typeof(TextBox)) as TextBox).Text
                            , DBHelper.QuoteStr(UserSupport.UserName));
                        ssdb.ExecuteSql(
                              "UPDATE tblOrderTransfer SET OriginTruckEndMileage = {2}, LastChangeDateUTC = getutcdate(), LastChangedByUser = {3} "
                                + "WHERE OrderID = {0}"
                            , (object)RadGridHelper.GetGridItemID(e.Item)
                            , ((object)RadGridHelper.GetControlByType(e.Item, "StartOdometer", typeof(TextBox)) as TextBox).Text
                            , ((object)RadGridHelper.GetControlByType(e.Item, "EndOdometer", typeof(TextBox)) as TextBox).Text
                            , DBHelper.QuoteStr(UserSupport.UserName));
                    }
                    else if (loadtype == "Loaded (2nd leg)")
                    {
                        // Update destination truck mileage (transfer)
                        ssdb.ExecuteSql(
                              "UPDATE tblOrderTransfer SET DestTruckStartMileage = {1}, LastChangeDateUTC = getutcdate(), LastChangedByUser = {3} "
                                + "WHERE OrderID = {0}"
                            , (object)RadGridHelper.GetGridItemID(e.Item)
                            , ((object)RadGridHelper.GetControlByType(e.Item, "StartOdometer", typeof(TextBox)) as TextBox).Text
                            , ((object)RadGridHelper.GetControlByType(e.Item, "EndOdometer", typeof(TextBox)) as TextBox).Text
                            , DBHelper.QuoteStr(UserSupport.UserName));
                        ssdb.ExecuteSql(
                              "UPDATE tblOrder SET DestTruckMileage = {2}, LastChangeDateUTC = getutcdate(), LastChangedByUser = {3} "
                                + "WHERE ID = {0}"
                            , (object)RadGridHelper.GetGridItemID(e.Item)
                            , ((object)RadGridHelper.GetControlByType(e.Item, "StartOdometer", typeof(TextBox)) as TextBox).Text
                            , ((object)RadGridHelper.GetControlByType(e.Item, "EndOdometer", typeof(TextBox)) as TextBox).Text
                            , DBHelper.QuoteStr(UserSupport.UserName));
                    }
                    else // type = "Loaded"
                    {
                        // Update truck mileage (non-transfer)
                        ssdb.ExecuteSql(
                              "UPDATE tblOrder SET OriginTruckMileage = {1}, DestTruckMileage = {2}, LastChangeDateUTC = getutcdate(), LastChangedByUser = {3} "
                                + "WHERE ID = {0}"
                            , (object)RadGridHelper.GetGridItemID(e.Item)
                            , ((object)RadGridHelper.GetControlByType(e.Item, "StartOdometer", typeof(TextBox)) as TextBox).Text
                            , ((object)RadGridHelper.GetControlByType(e.Item, "EndOdometer", typeof(TextBox)) as TextBox).Text
                            , DBHelper.QuoteStr(UserSupport.UserName));
                    }
                }
                e.Item.Edit = false;
                rgMain.Rebind();
            }
        }

        protected void cmdExport_Click(object sender, EventArgs e)
        {
            ExportGridToExcel();
        }

        private void ExportGridToExcel()
        {
            string filename = string.Format("Odometer Log as of {0:yyyyMMdd}.xlsx", rdpStartDate.SelectedDate);
            string[] hiddenToInclude = { "ID" };
            rgMain.AllowPaging = false;
            rgMain.Rebind();
            RadGridExcelExporter exporter = new RadGridExcelExporter(hiddenColNamesToInclude: hiddenToInclude);
            Response.ExportExcelStream(exporter.ExportSheet(rgMain.MasterTableView, "Odometer Log"), filename);
        }

        protected void grid_ItemCreated(object sender, GridItemEventArgs e)
        {
        }

        protected void grid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem && !e.Item.IsInEditMode && e.Item.DataItem is System.Data.DataRowView)
            {
                System.Data.DataRowView drv = e.Item.DataItem as System.Data.DataRowView;
                Control ctrl = (e.Item as GridDataItem)["EditColumn"].Controls[0];
                if (ctrl != null)
                    ctrl.Visible = !DBHelper.IsNull(drv["ID"]); // && !DBHelper.ToBoolean(drv["Locked"]);
            }
        }

    }
}