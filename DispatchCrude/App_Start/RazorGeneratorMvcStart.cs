using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;
using RazorGenerator.Mvc;

[assembly: WebActivatorEx.PostApplicationStartMethod(typeof(DispatchCrude.RazorGeneratorMvcStart), "Start")]

namespace DispatchCrude {
    /* see: https://stacktoheap.com/blog/2013/01/19/precompiling-razor-views-in-asp-dot-net-mvc-3/ 
     * and related sections of DispatchCrude Enterprise.csproj */
    public static class RazorGeneratorMvcStart {
        public static void Start() {
            var engine = new PrecompiledMvcEngine(typeof(RazorGeneratorMvcStart).Assembly) {
                UsePhysicalViewsIfNewer = HttpContext.Current.Request.IsLocal
            };

            ViewEngines.Engines.Insert(0, engine);

            // StartPage lookups are done by WebPages. 
            VirtualPathFactoryManager.RegisterVirtualPathFactory(engine);
        }
    }
}
