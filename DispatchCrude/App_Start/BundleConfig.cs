﻿using System.Web.Optimization;

namespace DispatchCrude
{
    public class BundleConfig
    {

        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {   // done to prevent bundling for now (remove when this is all figured out)
            BundleTable.EnableOptimizations = true; // false;

            bundles.Add(new ScriptBundle("~/bundles/jqueryval", @"http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.0/jquery.validate.min.js")
                .Include(
                    "~/Scripts/jquery.unobtrusive*",
                    "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr")
                .Include(
                    "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/dispatchcrude")
                .Include(
                      "~/Scripts/bootstrap.min.js",
                      "~/Scripts/dispatchcrude.js"));

            bundles.Add(new StyleBundle("~/Content/beyond")
                .Include(
                    "~/Content/beyondadmin/beyond.min.css",
                    "~/Content/beyondadmin/demo.min.css",
                    "~/Content/beyondadmin/font-awesome.min.css",
                    "~/Content/beyondadmin/typicons.min.css",
                    "~/Content/beyondadmin/weather-icons.min.css",
                    "~/Content/beyondadmin/animate.min.css"
                ));
            
            bundles.Add(new StyleBundle("~/Content/css")
                .Include(
                    "~/Content/site.css",
                    "~/Content/sitemobile.css"));

            bundles.Add(new StyleBundle("~/Content/bootstrap")
                .Include(
                    "~/Content/bootstrap.min.css",
                    "~/Content/dispatchcrude.css"));

            bundles.Add(new StyleBundle("~/Content/themes/base/css")
                .Include(
                    "~/Content/themes/base/jquery.ui.core.css",
                    "~/Content/themes/base/jquery.ui.resizable.css",
                    "~/Content/themes/base/jquery.ui.selectable.css",
                    "~/Content/themes/base/jquery.ui.accordion.css",
                    "~/Content/themes/base/jquery.ui.autocomplete.css",
                    "~/Content/themes/base/jquery.ui.button.css",
                    "~/Content/themes/base/jquery.ui.dialog.css",
                    "~/Content/themes/base/jquery.ui.slider.css",
                    "~/Content/themes/base/jquery.ui.tabs.css",
                    "~/Content/themes/base/jquery.ui.datepicker.css",
                    "~/Content/themes/base/jquery.ui.progressbar.css",
                    "~/Content/themes/base/jquery.ui.theme.css"));

            bundles.Add(new ScriptBundle("~/bundles/beyond")
                .Include(
                    "~/Content/beyondadmin/beyond.min.js"));
        }
    }
}