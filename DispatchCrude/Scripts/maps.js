﻿var LABEL_WIDTH = 50;
var LABEL_HEIGHT = 20;



function DCMarker(latlng, map, args)
{
    this.latlng = latlng;
    this.args = args;
    this.setMap(map);
}

DCMarker.prototype = new google.maps.OverlayView();

DCMarker.prototype.draw = function () {
    var self = this;

    var div = this.div;

    if (!div) {
        div = this.div = document.createElement('div');

        div.className = 'marker';

        div.style.position = 'absolute';
        div.style.cursor = 'pointer';
        //div.style.width = LABEL_WIDTH + 'px';
        //div.style.height = LABEL_HEIGHT + 'px';

        if (typeof (self.args.marker_id) !== 'undefined') {
            div.dataset.marker_id = self.args.marker_id;
        }

        if (typeof (self.args.status) !== 'undefined') {
            div.dataset.status = self.args.status;
        }

        if (typeof (self.args.content) !== 'undefined') {
            div.innerHTML = self.args.content;
        }

        google.maps.event.addDomListener(div, "click", function (event) {
            $(div).children().popover('show');
        });

        var panes = this.getPanes();
        panes.overlayImage.appendChild(div);
    }

    var point = this.getProjection().fromLatLngToDivPixel(this.latlng);

    if (point) {
        div.style.left = (point.x - LABEL_WIDTH/2) + 'px';
        div.style.top = (point.y - LABEL_HEIGHT) + 'px';
    }
};

DCMarker.prototype.remove = function () {
    if (this.div)
    {
        this.div.parentNode.removeChild(this.div);
        this.div = null;
    }
};

DCMarker.prototype.getPosition = function () {
    return this.latlng;
};

var styles = [
    {
        stylers: [
            { saturation: -20 }, /* slightly gray scale map */
            { weight: 1.4 } /* weight all roads */
        ]
    }, {
        featureType: 'landscape',
        elementType: 'all',
        stylers: [
            { hue: '#0000ff' } /* blue tint the background */
        ]
    }, {
        featureType: 'poi',
        elementType: 'all',
        stylers: [
            //{ hue: '#0000ff' }, /* blue tint the background */
            { saturation: -50 } /* super saturate parks which are green */
        ]
    }, {
        featureType: "poi.business",
        elementType: "labels",
        stylers: [
          { visibility: "off" }
        ]
    }
];


//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//:::                                                                         :::
//:::  This routine calculates the distance between two points (given the     :::
//:::  latitude/longitude of those points). It is being used to calculate     :::
//:::  the distance between two locations using GeoDataSource (TM) prodducts  :::
//:::                                                                         :::
//:::  Definitions:                                                           :::
//:::    South latitudes are negative, east longitudes are positive           :::
//:::                                                                         :::
//:::  Passed to function:                                                    :::
//:::    lat1, lon1 = Latitude and Longitude of point 1 (in decimal degrees)  :::
//:::    lat2, lon2 = Latitude and Longitude of point 2 (in decimal degrees)  :::
//:::    unit = the unit you desire for results                               :::
//:::           where: 'M' is statute miles (default)                         :::
//:::                  'K' is kilometers                                      :::
//:::                  'N' is nautical miles                                  :::
//:::                                                                         :::
//:::  Worldwide cities and other features databases with latitude longitude  :::
//:::  are available at http://www.geodatasource.com                          :::
//:::                                                                         :::
//:::  For enquiries, please contact sales@geodatasource.com                  :::
//:::                                                                         :::
//:::  Official Web site: http://www.geodatasource.com                        :::
//:::                                                                         :::
//:::               GeoDataSource.com (C) All Rights Reserved 2015            :::
//:::                                                                         :::
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

function distance(lat1, lon1, lat2, lon2, unit) {
    if (lat1 == 0 || lon1 == 0 || lat2 ==0 || lon2 == 0)
        return 60; // default to some logical value if parameter is missing 

    var radlat1 = Math.PI * lat1 / 180
    var radlat2 = Math.PI * lat2 / 180
    var theta = lon1 - lon2
    var radtheta = Math.PI * theta / 180
    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    dist = Math.acos(dist)
    dist = dist * 180 / Math.PI
    dist = dist * 60 * 1.1515
    if (unit == "K") { dist = dist * 1.609344 }
    if (unit == "N") { dist = dist * 0.8684 }
    return dist
}