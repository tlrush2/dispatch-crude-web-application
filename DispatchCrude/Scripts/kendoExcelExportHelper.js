﻿/* DEPENDENCIES: this file has a dependency on jsonSupport.js */
// this "class" will handle common export "translations" such as StateID for all grids
var kendoGridExportHelper = {
    // if this is not explicitly defined (on the "grid" table mx page), the logic below will retrieve the page root url value (such as /Operators), trim the leading '/' and the trailing 's' and try to use that
    // NOTE: if that default value is invalid, you can explicitly define this value in javascript on the appropriate page using: kendoGridExportHelper.ObjectName = "<appropriat object name>";
    ObjectName: null
    // to specify an alternate, custom "translation", you can use 1 of 2 override variables below
    // 1) var ColumnTemplates = [ { colID: X, field: '<field name - either/or with colID>', template: '<a template here or don't define to use the column's defined template>' } ] // where X is 1-based
    , ColumnTemplates: []
    // 2) var ColumnDataMappings = [ { field: 'StateID', dataPath: 'State.Abbreviation' } ]
    , ColumnDataMappings: []
    // NOTE: you don't need to define the following, they will be handled automatically [to not process these, re-define them with a null/undefined dataPath value]
    , DefColumnDataMappings: [
        { field: 'StateID', dataPath: 'State.Abbreviation' }
        , { field: 'OriginID', dataPath: 'Origin.Name' }
        , { field: 'DestinationID', dataPath: 'Destination.Name' }
        , { field: 'TruckTrailerMaintenanceTypeID', dataPath: 'TruckTrailerMaintenanceType.Name' }
    ]

    // this is the event handler that you assign to the kendoGrid.ExcelExport event
    , ExcelExport: function (e) {
        me = kendoGridExportHelper;
        debugger;
        // we have to restart the process if there are columns we need to hide only for export, so check that first (which causes a re-entrance into this same function if required)
        if (me._preventColumnWithBlankCaptionExport(e)) {

            // apply any grid defined formatting to the spreadsheet
            me.ApplyFormatting(e);

            var dataToTransform = false;
            var sheet = e.workbook.sheets[0];

            // get the column.template values for all basic [colID only] assignments
            if (me.ColumnTemplates && $.isArray(me.ColumnTemplates)) {
                dataToTransform = me.ColumnTemplates.length > 0;
                for (var i = 0; i < me.ColumnTemplates.length; i++) {
                    var ct = me.ColumnTemplates[i], index;
                    // retrieve the colID value if not specified (& the field was)
                    if (!ct.colID && ct.field && (index = me.findColIndex(this.columns, ct.field)) != -1) ct.colID = index + 1;  // store it as 1-based
                    if (!ct.sColID && ct.field && (index = me.findSheetColIndex(this.columns, ct.field)) != -1) ct.sColID = index;  // store as 0-based
                    if (!ct.template) ct.template = kendo.template(this.columns[ct.colID - 1].template);
                    // if the user specified a template definition, convert it into a template function here
                    if (typeof ct.template !== "function")
                        ct.template = kendo.template(ct.template);
                }
            }

            // update DefColumnDataMappings with the incoming ColumnDataMappings (null for the dataPath if being not used)
            if (me.ColumnDataMappings && $.isArray(me.ColumnDataMappings)) {
                for (var i = 0; i < me.ColumnDataMappings.length; i++) {
                    var dm = me.ColumnDataMappings[i];
                    var pos = me.DefColumnDataMappings.map(function (e) { return e.field; }).indexOf(dm.field);
                    if (pos != -1)
                        me.DefColumnDataMappings[pos].dataPath = dm.dataPath;
                    else
                        me.DefColumnDataMappings.push(dm);
                }
            }
            // if the mapping doesn't exist in this grid, then "skip" the mapping
            for (var i = 0; i < me.DefColumnDataMappings.length; i++) {
                var dm = me.DefColumnDataMappings[i]
                    , index = -1;
                if (dm.dataPath && (index = me.findColIndex(this.columns, dm.field)) != -1) {
                    dm.colID = index + 1;
                    dm.sColID = me.findSheetColIndex(this.columns, dm.field); // store 0-based
                    dataToTransform = true;
                }
            }

            if (dataToTransform) {
                // iterate over the data rows
                for (var r = 0; r < e.data.length; r++) {
                    var dr = e.data[r];
                    // iterate over the ColumnTemplates first
                    if (me.ColumnTemplates && $.isArray(me.ColumnTemplates)) {
                        for (var c = 0; c < me.ColumnTemplates.length; c++) {
                            var ct = ColumnTemplates[c];
                            sheet.rows[r + 1].cells[ct.colID - 1].value = ct.template(dr);
                        }
                    }
                    // iterate over the DefColumnDataMappings next (recursive process)
                    if (me.DefColumnDataMappings && $.isArray(me.DefColumnDataMappings)) {
                        for (var c = 0; c < me.DefColumnDataMappings.length; c++) {
                            var dm = me.DefColumnDataMappings[c];
                            if (dm.colID) {
                                sheet.rows[r + 1].cells[dm.sColID].value = drillToValue(dr, dm.dataPath);
                            }
                        }
                    }
                }
            }

            // attempt to get the Object Name (if not explicitly assigned)
            if (!me.ObjectName && window.location.pathname.length) {
                var pageName = window.location.pathname.split(".", 1)[0].substr(1);
                // trim trailing 's' character (if present)
                if (pageName.slice(-1).toLowerCase() === 's')
                    pageName = pageName.substr(0, pageName.length - 1);
                me.ObjectName = pageName;
            }

            if (me.ObjectName != null && retrieveJSON && e.data && e.data.length > 0 && e.data[0].ID) {
                var fields = retrieveJSON('/objectfield/customobjectfields?objectname=' + me.ObjectName);
                if (fields && fields.length) {
                    // add new columns to the spreadsheet output for these custom data fields
                    var colIndex = sheet.columns.length;
                    for (var c = 0; c < fields.length; c++) {
                        // add a new column for each custom data value
                        sheet.columns.push(cloneObject(sheet.columns[sheet.columns.length - 1]));
                        // set the new column header caption value
                        var headerCell = cloneObject(sheet.rows[0].cells[colIndex - 1], [{ Name: "value", Value: fields[c].Name }]);
                        sheet.rows[0].cells.push(headerCell);
                        // record the ColIndex of this new column (so we can reference it later when we add the data)
                        fields[c].ColIndex = colIndex++;
                    }
                    // get the "recordID" values for the output data
                    var idList = [];
                    for (var r = 0; r < e.data.length; r++) {
                        idList.push(e.data[r].ID);
                    }
                    // retrieve the actual custom data values
                    var fieldData = JSON.parse(retrieveJSON('/objectfield/customobjectfieldvalues', null, $.param({ objectname: me.ObjectName, idList: idList }, true)));
                    if (fieldData && fieldData.length) {
                        // update the output spreadsheet with the custom data
                        for (var r = 0; r < e.data.length; r++) {
                            var dr = e.data[r]
                                , row = sheet.rows[r + 1];
                            for (var c = 0; c < fields.length; c++) {
                                var value = arrayFindValueByKey(fieldData, dr.ID, fields[c].ID, "RecordID")
                                    , newCell = cloneObject(row.cells[row.cells.length - 1], [{ Name: "value", Value: value }]);
                                sheet.rows[r + 1].cells.push(newCell);
                            }
                        }
                    }
                }
            }
        }
    }
    // find the index of the column by the "field" 
    , findColIndex: function (columns, field) {
        return columns.findIndex(function (f) { return f.field === field; });
    }
    , findSheetColIndex: function (columns, field) {
        return columns.filter(function (f) { return f.field !== undefined; }).findIndex(function (f) { return f.field === field; });
    }

    /* prevent exporting of columns with a caption of "&nbsp;" - loosely based on https://stackoverflow.com/questions/38420374/show-hidden-columns-in-kendo-grid-excel-export */
    , _skipExportColumnIDs: []
    , _preventColumnWithBlankCaptionExport: function (e) {
        me = kendoGridExportHelper;
        // if an "Export operation has just been initiated, check to see if any columns should not be exported - if any exist, then restart the export with those columns marked "hidden" 
        if (!me._skipExportColumnIDs.length) {
            var sheet = e.workbook.sheets[0];

            // hide (don't export) any columns with a "&nbsp;" caption
            if (sheet.rows.length > 0) {
                for (var c = 0; c < sheet.columns.length; c++) {
                    var caption = sheet.rows[0].cells[c].value;
                    if (caption && caption.toLowerCase() == "&nbsp;") {
                        // record this column as a "temporarily" hidden column
                        me._skipExportColumnIDs.push(c);
                        // "hide" the column
                        e.sender.hideColumn(c);
                    }
                }
                // if we "temporarily hid" any grid columns (for export), then restart the export process so this process excludes them appropriately
                if (me._skipExportColumnIDs.length) {
                    e.preventDefault();
                    // restart the ExportToExcel process (which will "recursively" calling the this function again (indirectly from the main function above)
                    e.sender.saveAsExcel();
                    // return "false" to instruct the caller to abort the remaining processing (which will be called again recursively)
                    return false;
                }
            }
        }
        else { // reset the grid (un-hide the temporarily hidden columns), and let the operation continue (the columns are skipped based on the config before the "exportToExcel" operation starts)
            for (var c = 0; c < me._skipExportColumnIDs.length; c++) {
                e.sender.showColumn(c);
            }
            // remove the storage of these columns
            me._skipExportColumnIDs = [];
        }
        // return "true" to inform the caller to continue processing
        return true;
    }

    /* loosely based on: https://blog.falafel.com/kendoui-grid-excel-export-datetime/ */
    , ApplyFormatting: function (e) {
        debugger;
        // get all formatted field data
        var formattedColumns = [];
        var sheet = e.workbook.sheets[0];
        //var fields = e.sender.dataSource.options.schema.model.fields;
        var colIndex = 0;
        e.sender.columns.forEach(function (col, index) {
            if (sheet.rows[0].cells.filter(function (cell) { return cell.value == col.title; }).length > 0) {
                // determine if this column has a format we need to apply
                if (col.format) {
                    var colonPos = col.format.indexOf(":");
                    var exportFormat = col.format.substr(colonPos + 1, col.format.length - colonPos - 2);
                    // if the format ends with "tt" then this needs to be replaced with the excel equivalent ("AM/PM")
                    if (exportFormat.slice(-2) == "tt") exportFormat = exportFormat.substr(0, exportFormat.length - 2) + "AM/PM";
                    // give each column a format from the grid settings or a default format
                    formattedColumns.push(
                        {
                            index: colIndex
                            , format: exportFormat
                        }
                    );
                };
                colIndex++; // only advance the colIndex for exported columns
            }
        });
        // apply formatting to the exported excel worksheet
        for (var rowIndex = 1; rowIndex < sheet.rows.length; rowIndex++) {
            var row = sheet.rows[rowIndex];
            // apply the column formats to the appropriate excel data
            for (var i = 0; i < formattedColumns.length; i++) {
                var colIndex = formattedColumns[i].index;
                row.cells[colIndex].format = formattedColumns[i].format;
            }
        }
    }
}