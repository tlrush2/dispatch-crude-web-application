﻿function uploadFileSelected(sender, dateTimeClassName, yearIncrement, monthIncrement, dayIncrement) {
    setDatePickerByClass(dateTimeClassName, null, getDateMoment(yearIncrement, monthIncrement, dayIncrement).toDate());
};
function getDateMoment(yearIncrement, monthIncrement, dayIncrement) {
    if (yearIncrement == null) yearIncrement = 1;
    if (monthIncrement == null) monthIncrement = 0;
    if (dayIncrement == null) dayIncrement = 0;

    // get default new expiration date (1 yr from today)
    return moment().hour(0).minute(0).second(0).millisecond(0).add("years", yearIncrement).add("months", monthIncrement).add("days", dayIncrement);
};
function setDatePickerByClass(className, parent, dateVal) {
    if (dateVal == null) {
        getDatePickerByClass(className, parent).clear();
    } else {
        getDatePickerByClass(className, parent).set_selectedDate(dateVal);
    }
};
function getDatePickerByClass(className, parent) {
    var rdpID = null;
    // get the ID of the actual Telerik RadDatePicker
    if (parent == null) {
        rdpID = $("." + className).attr("ID").replace("_wrapper", "");
    } else {
        var rdpByClass = parent.getElementsByClassName(className)[0].getAttribute("id").replace("_wrapper", "");
    }
    // return the actual Telerik RDP reference
    return $find(rdpID);
};
