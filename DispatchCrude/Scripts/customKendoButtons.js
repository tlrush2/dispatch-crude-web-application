﻿/*
    The following code is specifically designed for the Telerik Kendo MVC grids.  This is for grids with standard functionality.  Grids with
    special button functionality may need manual overrides using template columns for the buttons
    instead of implementing this code.
*/

/*  NOTE: ActiveState() is not technically telerik button related but does provide default functionality for the "Active?" column on the new MVC grids.
  USAGE EXAMPLE: columns.Bound(column => column.Active).Title("Active?").ClientTemplate("#= ActiveState(data.Active) #").Width(85);
  It can also be used to display the checkbox control for any boolean field in the Ajax grids*/
function ActiveState(active) {
    if (active === true) {
        return "<div class='center'><input type='checkbox' checked='checked' disabled></input></div>";
    } else {
        return "<div class='center'><input type='checkbox' disabled></input></div>";
    }
}

/* this handler tells customKendoGridButtons to set the delete button text to 'delete' instead of the default 'deactivate' for grids that actually have deleteable records */
function customKendoGridButtons_OnDataBound_UseDeleteButton(e) {
    customKendoGridButtons(e.sender.element, true);
}

/* this handler can be assigned to a grid DataBound event to "wire up" the customKendoGridButtons() logic */
function customKendoGridButtons_OnDataBound(e) {
    customKendoGridButtons(e.sender.element);
}

function customKendoGridButtons(grid, useDeleteButton)
{
    // if not grid element was passed in, then use the body (to ensure we find all elements)
    if (!grid) grid = $("body");
    // if a selector for the grid was passed (we need to drill into the sender.element properties)
    if (grid.sender && grid.sender.element) grid = grid.sender.element;

    // Find the grid and any default buttons that could be in use
    var addButton = grid.find(".k-grid-toolbar").find(".k-grid-add");
    var excelButton = grid.find(".k-grid-toolbar").find(".k-grid-excel");
    var gridContent = grid.find(".k-grid-content");
    var updateButton = gridContent.find(".k-grid-update");
    var insertButton = gridContent.find(".k-grid-insert");
    var editButton = gridContent.find(".k-grid-edit");
    var cancelButton = gridContent.find(".k-grid-cancel");
    var deleteButton = gridContent.find(".k-grid-delete:not('.k-grid-activation')");  // if k-grid-activation is present, then these buttons are already transformed (see rowCommandsUndelete() below)
    var unauditButton = gridContent.find(".k-grid-Unaudit");
    var undeleteButton = gridContent.find(".k-grid-Undelete");
    var placeholderButton = gridContent.find(".k-grid-placeholder");

    // Disable placeholder button so it's not clickable
    placeholderButton.attr("disabled", "disabled");
    placeholderButton.attr("href", "");
    placeholderButton.css({ "cursor": "default" });

    // Remove Kendo styles from buttons (kendo UI grids have the k-button-icontext class that needs to be removed)
    addButton.removeClass("k-button k-button-icontext");
    excelButton.removeClass("k-button k-button-icontext");
    updateButton.removeClass("k-button k-button-icontext");
    insertButton.removeClass("k-button k-button-icontext");
    editButton.removeClass("k-button k-button-icontext");
    cancelButton.removeClass("k-button k-button-icontext");
    deleteButton.removeClass("k-button k-button-icontext");
    unauditButton.removeClass("k-button k-button-icontext");
    undeleteButton.removeClass("k-button k-button-icontext");
    placeholderButton.removeClass("k-button k-button-icontext");

    // Remove Telerik default text from add record buttons, leaving only the plus sign 
    // but add title text to say the same thing on hover (per Maverick's request)
    addButton.contents().filter(function () { return this.nodeType === 3; }).remove();
    addButton.attr('title', 'Add New Record');

    // Add our own custom styles to the buttons
    var baseImgUrl = document.location.origin + "/images/"
        , baseCss = "btn btn-xs btn-default shiny";
    addButton.addClass(baseCss).addClass("grid-button-right-margin");
    excelButton.addClass(baseCss).addClass("pull-right");
    updateButton.html("<img src='" + baseImgUrl + "apply_imageonly.png' title='Save' />").addClass(baseCss).addClass("grid-button-right-margin");
    insertButton.html("<img src='" + baseImgUrl + "apply_imageonly.png' title='Save' />").addClass(baseCss).addClass("grid-button-right-margin");
    editButton.html("<img src='" + baseImgUrl + "edit.png' title='Edit' />").addClass(baseCss).addClass("grid-button-right-margin");
    cancelButton.html("<img src='" + baseImgUrl + "cancel_imageonly.png' title='Cancel' />").addClass(baseCss);
    if (!useDeleteButton) {  //Check to see if the grid calling this funtion uses the default deactivate button or the delete button and change accordingly
        deleteButton.html("<img src='" + baseImgUrl + "delete.png' title='Deactivate' />").addClass(baseCss);
    } else {
        deleteButton.html("<img src='" + baseImgUrl + "delete.png' title='Delete' />").addClass(baseCss);
    }   
    unauditButton.html("<img src='" + baseImgUrl + "undelete.png' title='Unaudit' />").addClass(baseCss);
    undeleteButton.html("<img src='" + baseImgUrl + "undelete.png' title='Undelete' />").addClass(baseCss);
    placeholderButton.html("<img src='" + baseImgUrl + "placeholder.png' title='' />").addClass(baseCss);

    //if an alternate link has been specified for the add/create button, change the link to the specified alt link
    if ($("#allowCreate").attr("alt")) {
        var altLink = $("#allowCreate").attr("alt");
        var targetGrid = $("#allowCreate").attr("target_grid");

        //(for multi-grid pages)
        //If there is a target grid specified...
        if (typeof targetGrid != undefined && targetGrid.length > 0) {

            //make sure the add button is within the target grid before updating the link
            if (addButton.parent().parent().attr("id") == targetGrid) {
                //Change the link in the button then...
                addButton.attr("href", altLink);
                //...Remove the class that controls the buttons default behavior
                addButton.removeClass("k-grid-add");
            } else {
                //If there was a target grid specified but this is not the correct button
                //Do nothing to this button...
            }

        } else {  //If there is not a target grid specified, this is a single-grid page.
            //Change the link in the button then...
            addButton.attr("href", altLink);
            //...Remove the class that controls the buttons default behavior
            addButton.removeClass("k-grid-add");
        }        
    }
}

function customKendoGridButtons_Delay(gridSelector) {
    setTimeout(function () {
        customKendoGridButtons(gridSelector)
    }, 50);
}

function customKendoPopupButtons()
{
    // Find the pop-up window and any default buttons that could be in use
    var popupEditButtonArea = $(".k-state-default");  // As far as I can tell, this is the DIV class that always holds the buttons in the kendo pop-up editing mode
    var updateButton = popupEditButtonArea.find(".k-grid-update");
    var insertButton = popupEditButtonArea.find(".k-grid-insert");
    var cancelButton = popupEditButtonArea.find(".k-grid-cancel");

    // Remove Kendo styles from buttons
    updateButton.removeClass("k-button k-button-icontext");
    insertButton.removeClass("k-button k-button-icontext");
    cancelButton.removeClass("k-button k-button-icontext");

    // Add our own custom styles to the buttons
    var baseImgUrl = document.location.origin + "/images/"
        , baseCss = "btn btn-xs btn-default shiny";
    updateButton.html("<img src='" + baseImgUrl + "apply_imageonly.png' title='Save' />").addClass(baseCss).addClass("grid-button-right-margin");
    insertButton.html("<img src='" + baseImgUrl + "apply_imageonly.png' title='Save' />").addClass(baseCss).addClass("grid-button-right-margin");
    cancelButton.html("<img src='" + baseImgUrl + "cancel_imageonly.png' title='Cancel' />").addClass(baseCss);
}

function customKendoInlineButtons()
{
    var baseImgUrl = document.location.origin + "/images/"
        , baseCss = "btn btn-xs btn-default shiny";
    // update button
    $(".k-grid-update").removeClass("k-button k-button-icontext").html("<img src='" + baseImgUrl + "apply_imageonly.png' title='Save' />").addClass(baseCss).addClass("grid-button-right-margin");
    // insert Button
    $(".k-grid-insert").removeClass("k-button k-button-icontext").html("<img src='" + baseImgUrl + "apply_imageonly.png' title='Save' />").addClass(baseCss).addClass("grid-button-right-margin");
    // cancel Button 
    $(".k-grid-cancel").removeClass("k-button k-button-icontext").html("<img src='" + baseImgUrl + "cancel_imageonly.png' title='Cancel' />").addClass(baseCss);
}

//*********  NOTE  ***************
// The following was created to replace some of the above code.  Do not delete the above code until the pages using it have been re-written.  
// ALSO, the preview button is not addressed below yet, only the edit, delete, and re-activate buttons.  All buttons need to be addressed before removing any code above this comment.
//********************************


// Call this function for undelete button functionality for the Telerik AJAX grids.  
// If the model for that page extends the AuditModelDeleteBase model, the undelete functionality will be made available. Otherwise the regular delete functionality will be active.
// Ex:  columns.Template(@<text></text>).ClientTemplate("#= rowCommandsUndelete(data) #").Title("&nbsp;").Width(100);
function rowCommandsUndelete(data) {
    //debugger;
    var ret = "";

    if ($('#viewDetails').val()) {
        //Do not create the deactivate button for a grid row if the row is locked
        if (data.allowRowEdit == true) {
            ret += rowCommands_ShowDetailsButton(data.id);
        }
    }

    if ($('#allowEdit').val() && !data.DeleteDate) {
        //Do not create the deactivate button for a grid row if the row is locked
        if (data.allowRowEdit == true) {
            ret += rowCommands_ShowEditButton(data.id);
        }
    }
    //else if ($('#NoEdit').val() == 0) {
    //    //Return nothing - conventional edit is not used on this grid
    //    ret += "";
    //}
    else {
        ret += "<img src='/images/placeholder.png' class='btn-xs grid-button-right-margin' />";
    }

    if ($('#allowDelete').val()) {
        //Do not create the deactivate button for a grid row if the row is locked
        if (data.allowRowDeactivate == true)
        {   
            if (!data.DeleteDate) {
                ret += "<a href='#' class='k-grid-delete k-grid-activation'><img src='/images/delete.png' title='Deactivate' class='btn btn-xs btn-default shiny'/></a>";
            } else {
                ret += "<a href='#' class='k-grid-delete k-grid-activation'><img src='/images/undelete.png' title='Reactivate' class='btn btn-xs btn-default shiny'/></a>";
            }
        }      
    }
    return ret;    
}

function rowCommands_ShowEditButton(id) {

    if ($('#allowEdit').attr("alt")) {
        var altLink = $('#allowEdit').attr("alt") + id;
        //Return alternate edit link specified by the alt text of the allowEdit element on the page
        ret = "<a href='" + altLink + "' ><img src='/images/edit.png' title='Edit' class='btn btn-xs btn-default shiny grid-button-right-margin'/></a>";
    }
    else {
        //Return normal edit link to edit grid content
        ret = "<a href='#' class='k-grid-edit'><img src='/images/edit.png' title='Edit' class='btn btn-xs btn-default shiny grid-button-right-margin'/></a>";
    }

    return ret;
}

// if for a ds assigned to a non-standard named grid (#grid), then invoke this logic 
function onDSRequestEnd(e, gridId) {
    //debugger;
    if (e.type == "destroy") {  
        if (!gridId) gridId = "#grid";
        var grid = $(gridId).data("kendoGrid");
        grid.dataSource.read();
    }
}

// generic popupEditor editor logic
function onPopupEdit(e) {    //Place popup create vs. edit code here
    if (!e.model.isNew()) {
        //Do edit mode stuff:
    }
    else {
        //Do create mode stuff:
        $(".k-window-title").html("Create"); //change the window title to create
    }
}

function rowCommands_ShowDetailsButton(id) {

    var Link = "Details/" + id;
    if ($('#viewDetails').attr("alt")) 
    {
        //use alternate edit link specified by the alt text of the viewDetails element on the page
        Link = $('#viewDetails').attr("alt") + id;
    }
    return "<a href='" + Link + "' class='btn btn-xs btn-default shiny grid-button-right-margin'><i class='glyphicon glyphicon-list-alt' title='Details'></i></a>";
}