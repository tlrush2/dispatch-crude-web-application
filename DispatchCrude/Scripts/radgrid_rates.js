﻿$(function () {
    /* logic for each "rate" page */
    $("#excelUpload").change(function () {
        var disabled = $("#excelUpload").val().length == 0;
        $("#cmdImport").prop("disabled", disabled);
    });
    $("#chkReplaceMatching").click(function () {
        debugger;
        if ($("#chkReplaceMatching").is(':checked')) {
            $("#chkReplaceOverlapping").prop("disabled", false);
        }
        else {
            $("#chkReplaceOverlapping").attr("checked", false).prop("disabled", true);
        }
    });
});
function FormatAllImageButtons(sel) {
    // set default selector if not provided
    if (!sel) sel = "input[type='image']";
    return $(sel).addClass("btn").addClass("btn-xs").addClass("shiny");
};
function CenterPopup(sender, eventArgs) {
    /* based on http://docs.telerik.com/devtools/aspnet-ajax/controls/grid/how-to/data-editing/center-popup-edit-form-in-radgrid */
    var popUp = eventArgs.get_popUp();
    var gridWidth = sender.get_element().offsetWidth;
    var gridHeight = sender.get_element().offsetHeight;
    var popUpWidth = popUp.style.width.substr(0, popUp.style.width.indexOf("px"));
    var popUpHeight = popUp.style.height.substr(0, popUp.style.height.indexOf("px"));
    popUp.style.left = ((gridWidth - popUpWidth) / 2 + sender.get_element().offsetLeft) + "px";
    popUp.style.top = ((gridHeight - popUpHeight) / 3 + sender.get_element().offsetTop) + "px";
};
function GridPopupShowing(sender, args) {
    debugger;
    FormatAllImageButtons(".rgEditPopup input[type='image']").last().addClass("btnLessLeftMargin");
    ReconfigureGridPopupErrors(sender, args);
    CenterPopup(sender, args);
};
function ReconfigureGridPopupErrors(sender, args) {
    debugger;
    // get the current validator html
    var tr = $(".gridPopupErrors").parents("tr").first();
    // remove the first (empty) <td> element
    tr.find('td:first-child').remove();
    // re-configure the remaining <td> element to span both table columns
    tr.children("td").first().attr("colspan", 2).addClass("td_gridPopupErrors");
};

function validateEndDate(source, args) {
    debugger;
    args.isValid = true;
    endDate = moment(args.Value);
    if (endDate.isValid()) {
        var earliest = moment($(".EffectiveDateRDP").length == 1
            ? getDatePickerByClass("EffectiveDateRDP").get_focusedDate()
            : $(".EarliestEndDate").val());
        if (earliest.isValid() && endDate.diff(earliest) < 0) {
            args.isValid = false;
            source.errormessage = "End Date must be after the Effective Date (or empty)";
        }
    }
};