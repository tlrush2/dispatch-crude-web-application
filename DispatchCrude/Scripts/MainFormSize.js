﻿function repaintMainGrid() {
    try {
        if ($(".GridRepaint:not(.rgEditForm)").length) {
            // For old rad grids
            $(".GridRepaint:not(.rgEditForm)").each(function () {
                $find($(this).attr("id")).repaint();
            });
        }
    } catch (err) {
        /* eat these errors */
    }
};
function UsablePageHeight(excludeHeader) {
    // header(85px) + content top padding (20px) + content bottom padding (20px) = 125px
    // -3px removes the barely there space that still creates a vertical scrollbar
    return window.innerHeight - (excludeHeader ? 80 : 0) - 3;
}
function SetElementHeight(elemSelector, height) {
    elemSelector.height(height);
    elemSelector.css("height", height);
}
//Dynamically assign height (for grids and tab controls)
var _inSizeContent = false;
function sizeContent() {
    // prevent re-entrance (which can occur if the content page does something to force the resize() to be fired on a page element)
    if (_inSizeContent) return;
    _inSizeContent = true;
    try {
        var usablePageHeight = UsablePageHeight(true);
        var leftpanel = $(".leftpanel");
        var gridAreaDiv = $(".GridRepaint:not(.rgEditForm)");
        // retrieve the height of the speedButtonContainer (42 if defined, 0 if not defined)
        var speedButtonContainerHeight = $(".speedButtonContainer")[0] ? 42 : 0; 
        var mvcGridAreaDiv = $(".MVCGridRepaint");
        var mvcGridContent = mvcGridAreaDiv.find(".k-grid-content");
        var mvcGridHeader = mvcGridAreaDiv.find(".k-grid-header").height() + $(".k-grouping-header").height();
        var mvcGridPager = mvcGridAreaDiv.find(".k-grid-pager").height();
        
        var tabAreaDiv = $(".TabRepaint");
        var tabContent = $(".tab-content").not("#leftTabs");  //10/24/16 - Added .not("#leftTabs") to allow left side menu tabs to stay the same size when put into a whole page tab
        var tabHeader = $(".nav-tabs").height();
        var staticContent = $(".staticContent").height();

        // Size tab controls to full usable height
        if ($(".tabbable").hasClass("TabRepaint"))
        {
            SetElementHeight(tabAreaDiv, usablePageHeight);
            SetElementHeight(tabContent, tabAreaDiv.height() - tabHeader);
        }

        // resize elements that need to expand to full size/recalculate height (minus fixed content and some padding)
        if ($(".expand").hasClass("Repaint"))
        {
            SetElementHeight($(".expand"), usablePageHeight - tabHeader - staticContent - 30);
        }       

        var innerTabContentHeight = tabContent.height() - speedButtonContainerHeight;

        // Size grids to full usable height
        if (gridAreaDiv.length > 0 && gridAreaDiv.parent().parent().attr("class") == "dual-grid-vertical")
        {
            // these grids are stacked vertically so we have to cut their height in half
            SetElementHeight(gridAreaDiv, innerTabContentHeight / 2);
        }
        else if (gridAreaDiv.length > 0 || (mvcGridAreaDiv.length > 0 && mvcGridContent.length))
        {
            if (mvcGridAreaDiv.parents().hasClass("tab-pane") || gridAreaDiv.parents().hasClass("tab-pane"))
            {
                //resize leftpanel if it exists and doesn't match the height of the tabbed area
                if ($(".leftpanel")[0] && leftpanel.height() != innerTabContentHeight) {
                    SetElementHeight(leftpanel, innerTabContentHeight);
                }

                //Size Rad grids inside tabs
                SetElementHeight(gridAreaDiv, innerTabContentHeight);

                //Size MVC grids inside tabs
                // for new Telerik MVC Grids (-18 is the approximate horizontal scrollbar height and -25 is the approximate height of the command bar)
                // the -3 is to remove the browser scrollbar on the mvcgrid pages.
                SetElementHeight(mvcGridAreaDiv, innerTabContentHeight - 3);
                SetElementHeight(mvcGridContent, innerTabContentHeight - mvcGridHeader - mvcGridPager - 18 - 25);
            }
            else
            {
                //Size Rad grids with no tab container
                SetElementHeight(gridAreaDiv, usablePageHeight);

                //Size MVC grids with no tab container
                SetElementHeight(mvcGridAreaDiv, usablePageHeight - 3);
                SetElementHeight(mvcGridContent, usablePageHeight - mvcGridHeader - mvcGridPager - 18 - 25);
            }
        }

        fillParentVertical();
        fillPageVertical();
        repaintMainGrid();

        /* required so the grid will properly "resize" itself - preventing weird "growth" when an AJAX postback occurs */
        mvcGridAreaDiv.resize();

        call_IntPageResized();
    } catch (err) {
        debugger;
    }
    _inSizeContent = false;
};

function fillParentVertical() {
    $(".fillParentVertical").each(function () {
        var me = $(this);
        var newHeight = me.parent().innerHeight() - (me.position().top - me.parent().position().top);
        SetElementHeight(me, newHeight);
    });
};

function fillPageVertical() {
    $(".fillPageVertical").each(function () {
        debugger;
        var mainDiv = $("div.tab-content");
        var me = $(this);
        var newHeight = mainDiv.height() /*.css("height").replace("px", "")*/ - (me.offset().top - mainDiv.offset().top) + 25;
        SetElementHeight(me, newHeight);
    });
};

function sizeContentReady() {
    sizeContent();
    call_contentDocReady();
}
function resizeContent() {
    sizeContent();
    call_contentResized();
};
function call_contentDocReady() {
    try { if (contentDocReady) contentDocReady(); } catch (err) { /* just eat any exceptions */ }
};
function call_contentResized() {
    try { if (contentResized) contentResized(); } catch (err) { /* just eat any exceptions */ }
};
function call_IntPageResized() {
    try { if (IntPageResized) IntPageResized(); } catch (err) { /* just eat any exceptions */ }
};