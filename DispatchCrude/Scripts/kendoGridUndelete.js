﻿$(function () {
    debugger;
    detectShiftKey();
});

// grid assumes a delete/destroy operation simply removes the record, which might not be the case (since is undeletable), so force "rebind"
function onDSRequestEnd(e) {
    debugger;
    if (e.type == "destroy") {
        var grid = $("#grid").data("kendoGrid");
        grid.dataSource.read();
    }
}

function detectShiftKey() {
    document.ShiftDepressed = false;
    document.onkeydown = function () {
        if (event.shiftKey) document.ShiftDepressed = true;
    }
    document.onkeyup = function () {
        document.ShiftDepressed = false;
    }
}

/* this needs to be assigned to the grid.dataSource.destroy.Action.Data() */
function destroyData() {
    return { _PermanentDelete: document.ShiftDepressed }
}
