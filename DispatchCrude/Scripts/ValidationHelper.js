﻿var HighlightRequiredFields = function () {
    $(".required").each(function () {
        var label = $('label[for="' + $(this).attr('id') + '"]');
        if (undefined != label) {
            var text = label.text();
            if (text.length > 0) {
                label.append('<span style="color:red"> *</span>');
            }
        }
    });
};