﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Welcome to <asp:Literal runat="server" Text="<%$ appSettings:SiteTitle%>" />!
    </h2>
    <p>
        Please choose a menu item.&nbsp; If you need assistance, contact the FuelHelpDesk at 
        <b>1 (812) 303-4357</b>.
    </p>
</asp:Content>