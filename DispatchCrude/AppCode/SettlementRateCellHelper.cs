﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web;
using System.Drawing;
using System.Data;
using Telerik.Web.UI;
using System.Web.UI.WebControls;
using DispatchCrude.Core;
using DispatchCrude.Extensions;
using AlonsIT;

namespace DispatchCrude.App_Code
{
    public class SettlementRateCellHelper
    {
        public const string MANUAL_RATE_TEXT = "Manual"
            , MISSING_RATE_TEXT = "Missing";
        public const string OTHERDETAILS_FIELDNAME = "InvoiceOtherDetailsTSV"
            , OTHERAMOUNTS_FIELDNAME = "InvoiceOtherAmountsTSV";

        // return true if any Other records were MANUAL, otherwise return False
        static public bool HandleOtherDetails(GridDataItem gdi, DataRowView rv, string entityType, bool useHyperLinks = true)
        {
            string[] details = rv[OTHERDETAILS_FIELDNAME].ToString().Split('\r');
            bool[] isManual = new bool[details.Length];
            int i = 0;
            foreach (string detail in details)
            {
                if (!Converter.IsNullOrEmpty(detail))
                {
                    string[] detailList = detail.Split('\t');

                    if (detailList.Length == 3)
                    {
                        string text = string.Format("{0}-{1}", detailList[0].Truncate(10, true), detailList[1]);

                        Control ctrl = null;
                        if (useHyperLinks)
                        {
                            HyperLink hlDetail = new HyperLink();
                            hlDetail.NavigateUrl = string.Format("~/Site/Financials/{2}AccessorialRates.aspx?orderid={0}&typeid={1}", rv["ID"], detailList[2], entityType);
                            hlDetail.Text = text;
                            hlDetail.ForeColor = Color.Blue;
                            hlDetail.Target = "_blank"; // force open in new window
                            ctrl = hlDetail;
                        }
                        else
                        {
                            Label lbl = new Label();
                            lbl.Text = text;
                            ctrl = lbl;
                        }

                        if (gdi[OTHERDETAILS_FIELDNAME].Controls.Count > 0)
                        {
                            Literal br = new Literal();
                            br.Text = "</br>";
                            gdi[OTHERDETAILS_FIELDNAME].Controls.Add(br);
                        }
                        gdi[OTHERDETAILS_FIELDNAME].Controls.Add(ctrl);
                    }
                    else
                    {
                        isManual[i] = true;
                        Label manual = new Label();
                        manual.Text = string.Format("{0}-{1}", detailList[0].Truncate(10, true), MANUAL_RATE_TEXT);
                        if (useHyperLinks)
                        {
                            manual.ForeColor = Color.White;
                            manual.BackColor = Color.Olive;
                        }
                        gdi[OTHERDETAILS_FIELDNAME].Controls.Add(manual);
                    }
                    i++;
                }
            }
            string[] amounts = rv[OTHERAMOUNTS_FIELDNAME].ToString().Split('\r');
            i = 0;
            foreach (string amount in amounts)
            {
                if (!Converter.IsNullOrEmpty(amount))
                {
                    Label lbl = new Label();
                    lbl.Text = string.Format("${0}", amount);
                    if (useHyperLinks && isManual.Length >= i && isManual[i])
                    {
                        lbl.ForeColor = Color.White;
                        lbl.BackColor = Color.Olive;
                    }
                    if (gdi[OTHERAMOUNTS_FIELDNAME].Controls.Count > 0)
                    {
                        Literal br = new Literal();
                        br.Text = "</br>";
                        gdi[OTHERAMOUNTS_FIELDNAME].Controls.Add(br);
                    }
                    gdi[OTHERAMOUNTS_FIELDNAME].Controls.Add(lbl);
                }
                i++;
            }
            foreach (bool manual in isManual)
            {
                if (manual) return true;
            }
            return false;
        }

        // return true if valid, false if invalid (MISSING rates)
        static public bool ValidateRateCells(GridDataItem gdi, string baseColName, bool conditionMet = true)
        {
            bool ret = true;
            string rateCell = "Invoice" + baseColName + "Rate"
                , amountCell = "Invoice" + baseColName + "Amount";
            bool hasAmount = !Converter.IsNullOrEmpty(gdi[amountCell].Text);
            if (conditionMet && !hasAmount)
            {
                MarkCellAsOverride(gdi, rateCell, Color.Red, MISSING_RATE_TEXT);
                MarkCellAsOverride(gdi, amountCell, Color.Red, MISSING_RATE_TEXT);
                ret = false;
            }
            else if (hasAmount && Converter.IsNullOrEmpty(RadGridHelper.GetColumnHyperLinkText(gdi, rateCell)))
            {
                MarkCellAsOverride(gdi, rateCell, Color.Olive);
                MarkCellAsOverride(gdi, amountCell, Color.Olive, null);
            }
            return ret;
        }
        static public void MarkCellAsOverride(GridDataItem gdi, string uniqueName, Color backColor, string msg = MANUAL_RATE_TEXT)
        {
            if (!Converter.IsNullOrEmpty(msg))
            {
                gdi[uniqueName].Text = msg;
                gdi[uniqueName].HorizontalAlign = HorizontalAlign.Center;
            }
            gdi[uniqueName].BackColor = backColor;
            gdi[uniqueName].ForeColor = Color.White;
        }

        static public void exporter_CellValueChanged(GridDataItem gridRow, string colName, ref object value)
        {
            if (colName == OTHERDETAILS_FIELDNAME || colName == OTHERAMOUNTS_FIELDNAME)
            {
                value = string.Empty;
                foreach (Control ctrl in gridRow[colName].Controls)
                {
                    if (ctrl is HyperLink)
                        value += (Converter.IsNullOrEmpty(value) ? "" : "\r\n") + (ctrl as HyperLink).Text;
                    else if (ctrl is Label)
                        value += (Converter.IsNullOrEmpty(value) ? "" : "\r\n") + (ctrl as Label).Text;
                }
            }
        }

        static public void grid_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e, string type = "Carrier")
        {
            //Is it a GridDataItem
            if (e.Item is GridDataItem)
            {
                //Get the instance of the right type
                GridDataItem gdi = e.Item as GridDataItem;
                DataRowView rv = e.Item.DataItem as DataRowView;

                RadGridHelper.GetColumnCheckBox(gdi, "BatchSelColumn", "chkBatch").Attributes["OrderID"] = rv["OrderID"].ToString();
                RadGridHelper.GetColumnCheckBox(gdi, "RateApplySelColumn", "chkRateApply").Attributes["OrderID"] = rv["OrderID"].ToString();

                bool hasError = DBHelper.ToBoolean(rv["HasError"]), hasManualRates = false;
                //Check the formatting condition

                /* ensure the order has already been APPROVED */
                if (!DBHelper.ToBoolean(rv["Approved"]))
                {
                    gdi["Approved"].BackColor = Color.Red;
                }
                HyperLink hlRouteRate = RadGridHelper.GetControlByType(gdi["InvoiceRouteRate"], typeof(HyperLink)) as HyperLink;
                HyperLink hlRateSheetRate = RadGridHelper.GetControlByType(gdi["InvoiceRateSheetRate"], typeof(HyperLink)) as HyperLink;
                bool hasLoadAmount = !Converter.IsNullOrEmpty(gdi["InvoiceLoadAmount"].Text);
                if ((hlRouteRate != null && (hlRouteRate.Text.Length == 0 || DBHelper.ToDecimalSafe(hlRouteRate.Text.Replace("$", "")) == 0M))
                    && (hlRateSheetRate != null && (hlRateSheetRate.Text.Length == 0 || DBHelper.ToDecimalSafe(hlRateSheetRate.Text.Replace("$", "")) == 0M)))
                {
                    if (hasLoadAmount)
                    {
                        hlRouteRate.Enabled = hlRateSheetRate.Enabled = false;
                        hlRouteRate.BackColor = hlRateSheetRate.BackColor = Color.Transparent;
                        gdi["InvoiceRouteRate"].BackColor = gdi["InvoiceRateSheetRate"].BackColor = Color.Olive;
                        hlRouteRate.ForeColor = hlRateSheetRate.ForeColor = Color.White;
                        gdi["InvoiceRouteRate"].HorizontalAlign = gdi["InvoiceRateSheetRate"].HorizontalAlign = HorizontalAlign.Center;
                        hlRouteRate.Text = hlRateSheetRate.Text = MANUAL_RATE_TEXT;
                        hasManualRates = true;
                    }
                    else
                    {
                        MarkCellAsOverride(gdi, "InvoiceRateSheetRate", Color.Red, MISSING_RATE_TEXT);
                        MarkCellAsOverride(gdi, "InvoiceRouteRate", Color.Red, MISSING_RATE_TEXT);
                    }
                }
                ValidateRateCells(gdi, "OriginChainup", DBHelper.ToBoolean(rv["FinalOriginChainup"]));
                ValidateRateCells(gdi, "DestChainup", DBHelper.ToBoolean(rv["FinalDestChainup"]));
                ValidateRateCells(gdi, "Reroute", DBHelper.ToInt32(rv["FinalRerouteCount"]) > 0);
                ValidateRateCells(gdi, "H2S", DBHelper.ToBoolean(rv["FinalH2S"]));
                ValidateRateCells(gdi, "SplitLoad", DBHelper.ToInt32(rv["TicketCount"]) > 1);

                ValidateRateCells(gdi, "OriginWait", DBHelper.ToInt32(rv["InvoiceOriginWaitBillableMinutes"]) > 0);
                ValidateRateCells(gdi, "DestinationWait", DBHelper.ToInt32(rv["InvoiceDestinationWaitBillableMinutes"]) > 0);
                ValidateRateCells(gdi, "OrderReject", DBHelper.ToBoolean(rv["Rejected"]));

                if (Converter.IsNullOrEmpty(rv["InvoiceMinSettlementUnitsID"]))
                    MarkCellAsOverride(gdi, "InvoiceMinSettlementUnits", Color.Red, MISSING_RATE_TEXT);

                if (Converter.IsNullOrEmpty(rv["InvoiceSettlementFactorID"]))
                    MarkCellAsOverride(gdi, "InvoiceSettlementFactor", Color.Red, MISSING_RATE_TEXT);
                else if (!DBHelper.ToBoolean(rv["Rejected"])) // highlight the missing UNITS field (if not REJECTED and appropriate UNITS value is missing)
                {
                    string orderUnitsField = rv["OrderUnitsField"].ToString();
                    if (DBHelper.ToInt32(rv[orderUnitsField]) == 0)
                    {
                        MarkCellAsOverride(gdi, orderUnitsField, Color.Red, MISSING_RATE_TEXT);
                    }
                }
                if (DBHelper.IsNull(rv["InvoiceWaitFeeParameterID"]))
                {
                    MarkCellAsOverride(gdi, "InvoiceOriginWaitBillableMinutes", Color.Red, MISSING_RATE_TEXT);
                    MarkCellAsOverride(gdi, "InvoiceDestinationWaitBillableMinutes", Color.Red, MISSING_RATE_TEXT);
                }
                if (Converter.IsNullOrEmpty(RadGridHelper.GetColumnHyperLinkText(gdi, "InvoiceFuelSurchargeRate")) && !Converter.IsNullOrEmpty(gdi["InvoiceFuelSurchargeAmount"].Text))
                {
                    MarkCellAsOverride(gdi, "InvoiceFuelSurchargeRate", Color.Olive);
                    MarkCellAsOverride(gdi, "InvoiceFuelSurchargeAmount", Color.Olive, null);
                    hasManualRates = true;
                }
                if (HandleOtherDetails(gdi, rv, type)) hasManualRates = true;

                if (Converter.IsNullOrEmpty(gdi["ActualMiles"].Text) || DBHelper.ToDecimalSafe(gdi["ActualMiles"].Text) == 0)
                {
                    MarkCellAsOverride(gdi, "ActualMiles", Color.Red, MISSING_RATE_TEXT);
                }

                // if the rates have been applied, then indicate that certain notable conditions apply (Override or missing rates)
                if (gdi["InvoiceRatesAppliedDate"] != null)
                {
                    if (!hasError && Converter.IsNullOrEmpty(gdi["InvoiceRatesAppliedDate"].Text)) hasError = true;

                    // determine if any Manual Rates are present (that aren't already determined)
                    if (!hasManualRates)
                    {
                        foreach (GridColumn col in gdi.OwnerTableView.Columns)
                        {
                            if (gdi[col].BackColor == Color.Olive)
                            {
                                hasManualRates = true;
                                break;
                            }
                        }
                    }
                    if (hasError || hasManualRates)
                    {
                        Color backColor = hasError ? Color.Red : Color.Olive;
                        MarkCellAsOverride(gdi, "InvoiceRatesAppliedDate", backColor, null);
                    }
                }
            }

        }

    }
}