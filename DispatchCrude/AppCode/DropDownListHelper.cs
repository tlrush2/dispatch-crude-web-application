﻿using System;
using System.Web.UI.WebControls;
using AlonsIT;

namespace DispatchCrude.App_Code
{
	static public class DropDownListHelper
	{
		static public int SelectedValue(DropDownList ddl)
		{
			return ddl.SelectedIndex == -1 ? 0 : DBHelper.ToInt32(ddl.SelectedValue);
		}
        static public int SetSelectedValue(DropDownList ddl, string value, bool defaultToSingleton = false)
        {
            ListItem li = ddl.Items.FindByValue(value);
            if (li != null)
                ddl.SelectedValue = value;
            else if (defaultToSingleton &&ddl.Items.Count == 1)
                ddl.SelectedIndex = 0;
            else if (defaultToSingleton && (ddl.Items.Count == 2 && ddl.Items[0].Text.StartsWith("(")))
                ddl.SelectedIndex = 1;
            else if (ddl.Items.FindByValue("0") != null)
                ddl.SelectedValue = "0";
            else
                ddl.SelectedIndex = -1;

            return ddl.SelectedIndex;
        }

        static public void Rebind(DropDownList ddl, bool defaultToSingleton = false)
        {
            Rebind(ddl, SelectedValue(ddl), defaultToSingleton);
        }
        static public void Rebind(DropDownList ddl, object selectedValue, bool defaultToSingleton = false)
        {
            object currentID = selectedValue;
            ddl.SelectedIndex = -1;
            if (ddl.DataSourceID.Length > 0)
            {
                ddl.DataBind();
            }
            SetSelectedValue(ddl, currentID.ToString(), defaultToSingleton);
        }
    }
}