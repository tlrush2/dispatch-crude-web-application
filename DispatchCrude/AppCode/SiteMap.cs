﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using DispatchCrude.Core;

namespace DispatchCrude.App_Code
{
    public class SiteMap
    {
        // XML to generate site map.  Title and url must be set for each node.  Label will override the page title as the menu text
        public static string XML = File.ReadAllText(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "WebSiteMap.xml")); 

        /**************************************************************************************************/
        /**  FUNCTION: Menu                                                                              **/
        /**  PARAMETERS: curPage - title of the current page, will highlight/expand menu if match        **/
        /**  DESCRIPTION:  Generates a navigation menu using XML (defined above, but could be a          **/
        /**      separate file).  Menu calls the DropDownMenu using the top level, and that function     **/
        /**      uses recursion to generate a multi-level menu.                                          **/
        /**************************************************************************************************/
        public static string Menu(string curPage = "Dashboard")
        {
            return DropDownMenu(XElement.Parse(XML).Elements("SiteMapNode"), curPage);
        }

        /**************************************************************************************************/
        /**  FUNCTION: DropDownMenu                                                                      **/
        /**  PARAMETERS: nodes - set of nodes to generate a multi-level menu (XML)                       **/
        /**              curPage - title of the current page, will highlight/expand menu if match        **/
        /**  DESCRIPTION: Using the BeyondAdmin template, generate menu items for a given set of nodes.  **/
        /**      The function is recursive and can call itself to create drop down elements.             **/
        /**************************************************************************************************/
        public static string DropDownMenu(IEnumerable<XElement> nodes, string curPage)
        {
            string html = "";
            foreach (var x in nodes)
            {
                // skip node (and children) if user does not have access
                if (hasAccess(x.Attribute("roles")) == false)
                    continue;

                string title = x.Attribute("title").Value;
                string url = x.Attribute("url") == null ? "" : x.Attribute("url").Value;
                string icon = (x.Attribute("icon") == null) ? "" : x.Attribute("icon").Value;
                string menuClass = "";
                string label = (x.Attribute("label") == null) ? title : x.Attribute("label").Value;

                if (x.Elements("SiteMapNode").Count() == 0)
                {
                    // single line menu entry
                    if (title == curPage) { menuClass = "active"; }

                    html += "<li class='" + menuClass + "'>";
                    html += "<a href='" + url + "'><i class='menu-icon " + icon + "'></i><span class='menu-text'>" + label + "</span></a>";
                    html += "</li>";
                }
                else
                {
                    // drop down menu entry, loop through children
                    if (title == curPage && !String.IsNullOrEmpty(url)) { menuClass = "open active"; }
                    XElement descendant = x.Descendants().FirstOrDefault(n => n.Attribute("title").Value == curPage && !String.IsNullOrEmpty(n.Attribute("url").Value));
                    if (descendant != null) { menuClass = "open"; }

                    html += "<li class='" + menuClass + "'>";
                    html += "<a class='menu-dropdown' href='#'><i class='menu-icon " + icon + "'></i><span class='menu-text'>" + label + "</span><i class='menu-expand'></i></a>" +                            
                            "<ul class='submenu'>" +
                            DropDownMenu(x.Elements("SiteMapNode"), curPage) +
                            "</ul>";
                    html += "</li>";
                }
            }
            return html;
        }


        /**************************************************************************************************/
        /**  FUNCTION: hasAccess                                                                         **/
        /**  PARAMETERS: roles - comma-separated list of roles (XML attribute)                           **/
        /**  DESCRIPTION: Check if a user has access to a menu item using the roles attribute.  Roles    **/
        /**      is a comma-separated list of valid roles with permission to see the menu item (NOT the  **/
        /**      page itself).  If not explicitly set, all users can see the menu item.                  **/
        /**************************************************************************************************/
        public static bool hasAccess(XAttribute roles)
        {
            // okay if role not explicitly set or if blank or *
            if (roles == null || String.IsNullOrEmpty(roles.Value) || roles.Value == "*")
                return true;
            // otherwise return true user is in one of these roles
            return UserSupport.IsInRoles(roles.Value.Split(new char[] { ',' }));
        }


        /**************************************************************************************************/
        /**  FUNCTION: BreadCrumbs                                                                       **/
        /**  PARAMETERS: curPage - title of the current page, will highlight/expand menu if match        **/
        /**  DESCRIPTION: Create breadcrumbs for a page.  Look through descendents for the current page  **/
        /**      If found, grab the parents back to the root and create links.  Current page is always   **/
        /**      inactive, and the home page (dashboard) is always at the top.                           **/
        /**************************************************************************************************/
        public static string BreadCrumbs(string curPage)
        {
            string html = "<li>" + curPage + "</li>";
            XElement node = XElement.Parse(XML).Descendants().FirstOrDefault(n => n.Attribute("title").Value == curPage);
            if (node != null && node.Parent != null)
            {
                node = node.Parent; // skip last node (current page already set with an inactive link)
                while (node.Parent != null)
                {
                    html = "<li class='active'>" +
                           "<a href='" + node.Attribute("url").Value + "'>" + node.Attribute("title").Value + "</a>" +
                           "</li>" + html;
                    node = node.Parent;
                }
            }
            return "<li class='active'><i class='fa fa-home'></i>" 
                + "<a href='/'>Dashboard</a>" 
                + "</li>" + html;
        }
    }
}
