﻿using System;
using System.Collections.Specialized;
using System.Web;
using Telerik.Web.UI;
using System.Data;
using AlonsIT;
using DispatchCrude.Core;
using DispatchCrude.Extensions;

namespace DispatchCrude.App_Code
{
    public delegate void GetUnboundChangedValues(GridEditableItem item, DataRow values);

    public class DBSave_RadGrid
    {
        private string _tableName = string.Empty;

        public event GetUnboundChangedValues GetUnboundChangedValues;
        private void RaiseGetUnboundChangedValues(GridEditableItem item, DataRow values)
        {
            if (GetUnboundChangedValues != null)
                GetUnboundChangedValues(item, values);
        }

        public DBSave_RadGrid(string tableName) { _tableName = tableName; }

        private DataTable GetDestTable(int id = 0)
        {
            using (SSDB ssdb = new SSDB())
            {
                return ssdb.GetPopulatedDataTable("SELECT * FROM {0} WHERE ID={1}", _tableName, id);
            }
        }

        public int InsertGridRow(GridEditableItem item)
        {
            object ret = null;
            if (item != null)
            {
                DataTable dtDest = GetDestTable();
                DataRow row = dtDest.NewRow();
                row.Table.ColumnChanging += ColumnChangingHandler;
                UpdateValues(item, row);
                //item.UpdateValues(row);
                RaiseGetUnboundChangedValues(item, row);
                // save the new record to the database
                using (SSDB ssdb = new SSDB())
                {
                    ret = SSDBSave.SaveDataRow(ssdb, row, _tableName, "ID", HttpContext.Current.User.Identity.Name);
                }
                // ensure this row is no longer in edit mode
                item.Edit = false;
            }
            return DBHelper.ToInt32(ret);
        }

        private void UpdateValues(GridEditableItem item, DataRow row)
        {
            ListDictionary newValues = new ListDictionary();
            item.ExtractValues(newValues);
            foreach (object key in newValues.Keys)
            {
                if (row.Table.Columns.Contains(key.ToString()))
                    row[key.ToString()] = newValues[key];
            }
        }
        private void UpdateValues(ListDictionary newValues, DataRow row)
        {
            foreach (object key in newValues.Keys)
            {
                if (key.ToString() != "ID" && row.Table.Columns.Contains(key.ToString()))
                {   // ensure the column is not readonly
                    row.Table.Columns[key.ToString()].ReadOnly = false;
                    // write out the data
                    row[key.ToString()] = newValues[key];
                }
            }
        }

        static private ListDictionary ExtractValues(GridEditableItem item)
        {
            ListDictionary ret = new ListDictionary();
            item.ExtractValues(ret);
            return ret;
        }

        static public int ExtractIDValue(GridEditableItem item, ListDictionary dataValues = null)
        {
            int ret = 0;
            if (dataValues == null)
                dataValues = ExtractValues(item);
            if (dataValues.Contains("ID"))
                ret = Converter.ToInt32(dataValues["ID"]);
            else
                ret = Converter.ToInt32(item.GetDataKeyValue("ID"));
            return ret;
        }

        public int UpdateGridRow(GridEditableItem item)
        {
            int id = 0;
            if (item != null)
            {
                ListDictionary dataValues = ExtractValues(item);
                id = ExtractIDValue(item, dataValues);
                DataTable dtDest = GetDestTable(id);
                if (dtDest.Rows.Count != 1)
                    id = InsertGridRow(item);
                else
                {
                    DataRow row = dtDest.Rows[0];
                    row.Table.ColumnChanging += ColumnChangingHandler;
                    UpdateValues(dataValues, row);
                    RaiseGetUnboundChangedValues(item, row);
                    // save the data to the database
                    using (SSDB ssdb = new SSDB())
                    {
                        SSDBSave.SaveDataRow(ssdb, row, _tableName, "ID", HttpContext.Current.User.Identity.Name);
                    }
                }
            }
            return id;
        }
        public void DeleteGridRow(GridItem item)
        {
            // get the record being deleted
            int id = RadGridHelper.GetGridItemID(item);
            using (SSDB ssdb = new SSDB())
            {
                DataTable dt = ssdb.GetPopulatedDataTable("SELECT * FROM {0} WHERE ID={1}", _tableName, id);
                //mark it as deleted
                dt.Rows[0].Delete();
                // let the data library do the Delete
                SSDBSave.SaveDataTable(ssdb, dt, _tableName, "ID", HttpContext.Current.User.Identity.Name);
            }
        }

        public void UndeleteGridRow(GridItem item)
        {
            // get the record being deleted
            int id = RadGridHelper.GetGridItemID(item);
            using (SSDB ssdb = new SSDB())
            {
                DataTable dt = ssdb.GetPopulatedDataTable("SELECT * FROM {0} WHERE ID={1}", _tableName, id);
                //mark it as un-deleted
                if (dt.Columns.Contains("DeleteDateUTC"))
                    dt.Rows[0]["DeleteDateUTC"] = DBNull.Value;
                else if (dt.Columns.Contains("DeleteDate"))
                    dt.Rows[0]["DeleteDate"] = DBNull.Value;
                if (dt.Columns.Contains("DeletedByUser"))
                    dt.Rows[0]["DeletedByUser"] = DBNull.Value;

                // let the data library do the Delete
                SSDBSave.SaveDataTable(ssdb, dt, _tableName, "ID", HttpContext.Current.User.Identity.Name);
            }
        }

        private void ColumnChangingHandler(object sender, DataColumnChangeEventArgs e)
        {
            // DataRow field will not accept null, so translate null -> DBNull.Value (when Nulls allowed)
            if (e.ProposedValue == null && e.Column.AllowDBNull)
            {
                e.ProposedValue = DBNull.Value;
            }
            // if the column is an "ID" column, and the value is not a positive integer, then convert it to DBNULL.Value
            else if (e.Column.ColumnName.EndsWith("ID") && Converter.ToInt32(e.ProposedValue) == 0
                && e.Column.DataType.IsIn(Type.GetType("System.Int32"), Type.GetType("System.Int16"), Type.GetType("System.Byte")))
            {
                e.ProposedValue = DBNull.Value;
            }
        }
    }
}