﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AlonsIT;

namespace DispatchCrude.App_Code
{
    public class StringList: List<string>
    {
        public StringList()
        {
        }
        public StringList(DataTable dtData, string fieldName)
        {
            foreach (DataRow dr in dtData.Rows) this.Add(dr[fieldName].ToString());
        }
        public StringList(string csv, char delimiter = ',')
        {
            foreach (string value in csv.Split(delimiter)) this.Add(value);
        }
        public string CSV(bool quoteValues = false)
        {
            string ret = string.Empty;
            foreach (String value in this)
            {
                ret += "," + (quoteValues ? DBHelper.QuoteStr(value) : value);
            }
            ret = ret.Trim(',');
            return ret;
        }
    }
}