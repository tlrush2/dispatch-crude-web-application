﻿using System;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Collections.Specialized;
using AlonsIT;

namespace DispatchCrude.App_Code
{
    static public class RadGridHelper
    {
        static private int MAX_ATTACHMENT_BYTES = 6291456; // 6MB default size limit

        static public void PreRender_Disable_Edit_Add(RadGrid grid)
        {
            bool inEdit = false;
            foreach (GridItem item in grid.MasterTableView.Items)
            {
                // disable the data row when in edit mode
                if (item is GridDataItem && item.Edit)
                {
                    inEdit = true;
                }
            }
            if (inEdit)
            {
                DisableAddNewRecordButton(grid);
            }
        }

        static public void DisableAddNewRecordButton(RadGrid grid)
        {
            foreach (GridCommandItem cmditm in grid.MasterTableView.GetItems(GridItemType.CommandItem))
            {
                Control ctrl = cmditm.FindControl("AddNewRecordButton");
                if (ctrl != null)
                    ctrl.Visible = false;
                ctrl = (LinkButton)cmditm.FindControl("InitInsertButton");
                if (ctrl != null)
                    ctrl.Visible = false;
            }
        }
        static public void FixEditLeftMargin(GridItem item)
        {
            if (item is GridEditableItem && item.IsInEditMode)
            {
                //find the cancel button in edit section of grid
                ImageButton cancel = item.FindControl("CancelButton") as ImageButton;
                if (cancel != null)
                {
                    //change the left margin so the update/insert and cancel buttons are not squished together and 
                    //subsequently easy to push the wrong button.
                    cancel.Style["margin-left"] = "30px";
                }
            }
        }

        static public int GetGridItemID(GridItem item, string idFieldName = "ID")
        {
            object id = null;
            if (item is GridEditableItem && !(item is GridDataInsertItem) && item.OwnerTableView.DataKeyNames.Contains(idFieldName))
                try { id = ((item as GridEditableItem).GetDataKeyValue(idFieldName) ?? "").ToString(); } catch { /* eat errors */ }
            if (id == null && item is GridDataItem)
                try { id = (item as GridDataItem).GetDataKeyValue(idFieldName); } catch { /* eat errors */ }
            if (id == null && item is GridEditableItem)
            {
                ListDictionary values = new ListDictionary();
                try { 
                    (item as GridEditableItem).ExtractValues(values);
                    if (values.Contains(idFieldName))
                        id = values[idFieldName];
                }
                catch { /* eat these errors */ }
            }
            return DBHelper.ToInt32(id);
        }

        static public Control GetControlByType(GridItem item, string uniqueName, Type controlType, bool recurse = false, string ctrlName = "")
        {
            return GetControlByType((item as GridEditableItem)[uniqueName], controlType, recurse, ctrlName);
        }
        static public Control GetControlByType(Control parent, Type controlType, bool recurse = false, string ctrlName = "")
        {
            Control namedCtrl = ctrlName.Length > 0 ? parent.FindControl(ctrlName) : null;
            if (namedCtrl != null)
                return namedCtrl;
            else
            {
                foreach (Control ctrl in parent.Controls)
                {
                    if ((string.IsNullOrEmpty(ctrlName) || ctrl.ID == null || ctrl.ID.Contains(ctrlName))
                        && (ctrl.GetType() == controlType
                            || (ctrl is IConvertible && Convert.ChangeType(ctrl, controlType) != null)
                            || (controlType.Name == "Label" && ctrl is Label))) // required because Label is not IConvertible
                    {
                        return ctrl;
                    }
                    else if (recurse)
                    {
                        Control rCtrl = GetControlByType(ctrl, controlType, recurse, ctrlName);
                        if (rCtrl != null)
                            return rCtrl;
                    }
                }
            }
            return null;
        }

        static public Control GetColumnControlByType(TableCell cell, Type type, string ctrlName = "")
        {
            Control ret = ctrlName.Length > 0 ? Convert.ChangeType(cell.FindControl(ctrlName), type) as Control : null;
            if (ret == null)
            {
                foreach (Control ctrl in cell.Controls)
                {
                    if (!(ctrl is LiteralControl))
                        ret = Convert.ChangeType(ctrl, type) as Control;
                    if (ret != null) break;
                }
            }
            return ret;
        }

        static public CheckBox GetColumnCheckBox(GridItem item, string uniqueName, string ctrlName = "")
        {
            return GetColumnControlByType((item as GridEditableItem)[uniqueName], typeof(CheckBox), ctrlName) as CheckBox;
        }
        static public RadDatePicker GetColumnDatePicker(GridItem item, string uniqueName, string ctrlName = "")
        {
            return GetColumnDatePicker((item as GridEditableItem)[uniqueName], ctrlName);
        }
        static public RadDatePicker GetColumnDatePicker(TableCell cell, string ctrlName = "")
        {
            RadDatePicker rdp = ctrlName.Length > 0 ? cell.FindControl(ctrlName) as RadDatePicker : null;
            if (rdp == null)
            {
                foreach (Control ctrl in cell.Controls)
                {
                    if (ctrl is RadDatePicker)
                        rdp = ctrl as RadDatePicker;
                }
            }
            return rdp;
        }
        static public DropDownList GetColumnDropDown(TableCell cell, string ctrlName = "")
        {
            DropDownList ddl = ctrlName.Length > 0 ? cell.FindControl(ctrlName) as DropDownList : null;
            if (ddl == null)
            {
                foreach (Control ctrl in cell.Controls)
                {
                    if (ctrl is DropDownList)
                        ddl = ctrl as DropDownList;
                }
            }
            return ddl;
        }

        static public RadComboBox GetColumnRadComboBox(GridItem item, string uniqueName, string ctrlName = "")
        {
            return GetColumnRadComboBox((item as GridEditableItem)[uniqueName], ctrlName);
        }
        static public RadComboBox GetColumnRadComboBox(Control cell, string ctrlName = "")
        {
            RadComboBox ddl = ctrlName.Length > 0 ? cell.FindControl(ctrlName) as RadComboBox : null;
            if (ddl == null)
            {
                foreach (Control ctrl in cell.Controls)
                {
                    if (ctrl is RadComboBox)
                        ddl = ctrl as RadComboBox;
                }
            }
            return ddl;
        }

        static public HyperLink GetColumnHyperLink(GridItem item, string uniqueName, string ctrlName = "")
        {
            return GetColumnHyperLink((item as GridEditableItem)[uniqueName], ctrlName);
        }
        static public HyperLink GetColumnHyperLink(TableCell cell, string ctrlName = "")
        {
            return RadGridHelper.GetControlByType(cell, typeof(HyperLink)) as HyperLink;
        }
        static public string GetColumnHyperLinkText(GridItem item, string uniqueName, string ctrlName = "", string defaultValue = null)
        {
            HyperLink hl = GetColumnHyperLink(item, uniqueName, ctrlName);
            return hl == null ? defaultValue : hl.Text;
        }

        static public object GetColumnDropDownSelectedValue(TableCell cell, string ctrlName = "", object defaultValue = null)
        {
            DropDownList ddl = GetColumnDropDown(cell, ctrlName);
            if (ddl != null && ddl.SelectedIndex != -1)
                return ddl.SelectedValue;
            else
            {
                RadComboBox rcb = GetColumnRadComboBox(cell, ctrlName);
                if (rcb != null && rcb.SelectedIndex != -1)
                    return rcb.SelectedValue;
            }
            return defaultValue;
        }

        static public object GetColumnRadComboBoxSelectedValue(TableCell cell, string ctrlName = "", object defaultValue = null)
        {
            RadComboBox rcb = GetColumnRadComboBox(cell, ctrlName);
            if (rcb != null && rcb.SelectedIndex != -1)
                return rcb.SelectedValue;
            return defaultValue;
        }

        static public void RebindColumnDropDown(TableCell cell, string ctrlName = "", bool eatExceptions = false)
        {
            DropDownList ddl = App_Code.RadGridHelper.GetColumnDropDown(cell, ctrlName);
            if (ddl != null)
            {
                int currentID = DropDownListHelper.SelectedValue(ddl);
                ddl.SelectedIndex = -1;
                if (ddl.DataSourceID.Length > 0)
                {
                    try { ddl.DataBind(); }
                    catch (Exception e) 
                    {
                        if (!eatExceptions)
                            throw e;
                    }
                }
                DropDownListHelper.SetSelectedValue(ddl, currentID.ToString());
            }
        }

        static public void RebindColumnRadComboBox(TableCell cell, string ctrlName = "")
        {
            RadComboBox ddl = App_Code.RadGridHelper.GetColumnRadComboBox(cell, ctrlName);
            if (ddl != null)
            {
                int currentID = App_Code.RadComboBoxHelper.SelectedValue(ddl);
                if (ddl.DataSourceID.Length > 0)
                    ddl.DataBind();
                App_Code.RadComboBoxHelper.SetSelectedValue(ddl, currentID.ToString());
            }
        }

        static public void SetColumnDropDownSelectedValue(TableCell cell, string value, string ctrlName = "")
        {
            DropDownList ddl = GetColumnDropDown(cell, ctrlName);
            if (ddl != null)
                DropDownListHelper.SetSelectedValue(ddl, value);
        }

        static public void SetColumnRadComboBoxSelectedValue(TableCell cell, string value, string ctrlName = "")
        {
            RadComboBox ddl = GetColumnRadComboBox(cell, ctrlName);
            if (ddl != null)
                RadComboBoxHelper.SetSelectedValue(ddl, value);
        }

        static public void SetFilter(GridTableView gridTable, string uniqueName, object filterValue, GridKnownFunction filterFunction = GridKnownFunction.EqualTo)
        {
            string currentFilterExpression = gridTable.FilterExpression;
            GridColumn column = gridTable.Columns.FindByUniqueNameSafe(uniqueName);
            object value = filterValue;
            if (value != null && (filterFunction == GridKnownFunction.Contains || filterFunction == GridKnownFunction.DoesNotContain))
                value = string.Format("%{0}%", value);
            if (value is string)
                value = DBHelper.QuoteStr(value);

            string op = null;
            switch (filterFunction)
            {
                case GridKnownFunction.Contains:
                    op = "LIKE";
                    break;
                case GridKnownFunction.DoesNotContain:
                    op = "NOT LIKE";
                    break;
                case GridKnownFunction.EqualTo:
                    op = "=";
                    break;
                case GridKnownFunction.GreaterThan:
                    op = ">";
                    break;
                case GridKnownFunction.GreaterThanOrEqualTo:
                    op = ">=";
                    break;
                case GridKnownFunction.LessThan:
                    op = "<";
                    break;
                case GridKnownFunction.LessThanOrEqualTo:
                    op = "<=";
                    break;
            }
            if (column != null)
            {
                // see http://www.telerik.com/help/aspnet-ajax/grid-operate-with-filter-expression-manually.html for details
                string filterBase = "";
        
                if (gridTable.OwnerGrid.EnableLinqExpressions)
                    filterBase = @"(it[""{0}""] {1} {2})";
                else
                    filterBase = @"([{0}] {1} {2})";

                gridTable.FilterExpression = string.Format(filterBase, uniqueName, op, value);
                column.CurrentFilterFunction = filterFunction;
                column.CurrentFilterValue = filterValue.ToString();
            }
            if (!string.IsNullOrEmpty(currentFilterExpression))
            {
                gridTable.FilterExpression += string.Format(" AND ({0})", currentFilterExpression);
            }
        }

        // handle updating any binary attachment documents (which aren't handled "natively" with regular "updates" 
        static public bool ValidateAttachmentColumns(GridEditableItem item)
        {
            bool ret = true;
            foreach (GridColumn col in item.OwnerTableView.Columns)
            {
                if (col is GridAttachmentColumn && !ValidateAttachmentColumnDocument(item, col.UniqueName))
                    ret = false;
            }
            return ret;
        }

        static public bool ValidateAttachmentColumnDocument(GridEditableItem item, string attachmentColUniqueName)
        {
            bool ret = true;

            GridAttachmentColumn attachCol = item.OwnerTableView.Columns.FindByUniqueName(attachmentColUniqueName) as GridAttachmentColumn;
            if (attachCol != null)
            {
                string docName = GetAttachmentDocumentName(item, attachmentColUniqueName);

                if (attachCol != null && docName.Length > 0)
                {
                    if (GetAttachmentDocLength(item, attachmentColUniqueName) > MAX_ATTACHMENT_BYTES)  // enforce a 5MB file size limit
                    {
                        CustomValidator cv = new CustomValidator();
                        cv.ID = "cvFileSize_" + attachmentColUniqueName;
                        cv.ErrorMessage = docName + " is too large for upload!";
                        cv.Text = "*";
                        cv.CssClass = "NullValidator";
                        cv.IsValid = false;
                        cv.EnableClientScript = false;
                        item[attachmentColUniqueName].Controls.Add(cv);
                        // return false to indicate not valid
                        ret = false;
                    }
                }
            }
            return ret;
        }

        // handle updating any binary attachment documents (which aren't handled "natively" with regular "updates" 
        static public void UpdateAttachmentColumns(GridEditableItem item, string updateTableName, int? id = null)
        {
            int attachColCount = 0;
            foreach (GridColumn col in item.OwnerTableView.Columns)
            {
                if (col is GridAttachmentColumn) attachColCount++;
            }
            foreach (GridColumn col in item.OwnerTableView.Columns)
            {
                UpdateAttachmentColumnDocument(item, col.UniqueName, updateTableName, id, attachColCount == 1);
            }
        }

        static public void UpdateAttachmentColumnDocument(GridEditableItem item, string attachmentColUniqueName, string updateTableName, int? id = null, bool singleton = false)
        {
            GridAttachmentColumn attachCol = item.OwnerTableView.Columns.FindByUniqueName(attachmentColUniqueName) as GridAttachmentColumn;
            if (attachCol != null)
            {
                string docName = GetAttachmentDocumentName(item, attachmentColUniqueName).Replace(',', '_');
                // require a document to have been provided if only one GridAttachmentColumn belongs to the grid (a valid assumption)
                if (docName.Length == 0)
                {
                    if (singleton)
                        throw new Exception("File was not specified");
                }
                else 
                {
                    using (SSDB db = new SSDB())
                    {
                        using (System.Data.SqlClient.SqlCommand cmd = db.BuildCommand(
                            string.Format("UPDATE dbo.{0} SET {1}=substring(@DocName,1,255), {2}=@Doc WHERE ID=@ID"
                                , updateTableName
                                , attachCol.FileNameTextField
                                , attachCol.AttachmentDataField)))
                        {
                            cmd.Parameters.AddWithValue("@ID", id.HasValue ? id.Value : item.GetDataKeyValue("ID"));
                            cmd.Parameters.AddWithValue("@DocName", docName);
                            cmd.Parameters.AddWithValue("@Doc", GetAttachmentDocument(item, attachmentColUniqueName));
                            cmd.ExecuteNonQuery();
                        }
                    }
                }
            }
        }

        static public string GetAttachmentDocumentName(GridEditableItem item, string attachmentColUniqueName)
        {
            GridAttachmentColumnEditor editor = item.EditManager.GetColumnEditor(attachmentColUniqueName) as GridAttachmentColumnEditor;
            string fileName = editor.RadUploadControl.UploadedFiles.Count == 0 ? "" : editor.RadUploadControl.UploadedFiles[0].FileName;
            return Path.GetFileName(fileName);
        }
        static public int GetAttachmentDocLength(GridEditableItem item, string attachmentColUniqueName)
        {
            return (item.EditManager.GetColumnEditor(attachmentColUniqueName) as GridAttachmentColumnEditor).UploadedFileContent.Length;
        }
        static public byte[] GetAttachmentDocument(GridEditableItem item, string attachmentColUniqueName)
        {
            return (item.EditManager.GetColumnEditor(attachmentColUniqueName) as GridAttachmentColumnEditor).UploadedFileContent;
        }

    }

    public class ItemActionTemplate : ITemplate
    {
        private string _entityCaption = "";
        private bool _includeClone = false;

        public ItemActionTemplate(string entityCaption, bool includeClone = false)
        {
            _entityCaption = entityCaption;
            _includeClone = includeClone;
        }

        public void InstantiateIn(System.Web.UI.Control container)
        {
            ImageButton btnEdit = new ImageButton();
            btnEdit.ID = "cmdEdit";
            btnEdit.ImageUrl = "~/images/edit.png";
            btnEdit.BorderStyle = BorderStyle.None;
            btnEdit.AlternateText = btnEdit.CommandName = "Edit";
            container.Controls.Add(btnEdit);

            Literal spacer = new Literal();
            spacer.Text = "&nbsp;";
            container.Controls.Add(spacer);

            ImageButton btnDelete = new ImageButton();
            btnDelete.ID = "cmdDelete";
            btnDelete.ImageUrl = "~/images/delete.png";
            btnDelete.BorderStyle = BorderStyle.None;
            btnDelete.AlternateText = btnDelete.CommandName = "Delete";
            btnDelete.OnClientClick = "return confirm('Permanently delete this " + _entityCaption + "?')";
            container.Controls.Add(btnDelete);

            if (_includeClone)
            {
                Literal spacer2 = new Literal();
                spacer2.Text = "&nbsp;";
                container.Controls.Add(spacer2);

                ImageButton btnClone = new ImageButton();
                btnClone.ID = "cmdClone";
                btnClone.ImageUrl = "~/images/add.png";
                btnClone.BorderStyle = BorderStyle.None;
                btnClone.AlternateText = btnClone.CommandName = "Clone";
                container.Controls.Add(btnClone);
            }
        }
    }

    public class EditItemActionTemplate : IBindableTemplate
    {
        void ITemplate.InstantiateIn(System.Web.UI.Control container)
        {
            LinkButton btnUpdate = new LinkButton();
            btnUpdate.ID = "cmdUpdate";
            btnUpdate.Text = btnUpdate.CommandName = "Update";
            container.Controls.Add(btnUpdate);

            Literal spacer = new Literal();
            spacer.Text = "&nbsp;";
            container.Controls.Add(spacer);

            LinkButton btnCancel = new LinkButton();
            btnCancel.ID = "cmdCancel";
            btnCancel.Text = btnCancel.CommandName = "Cancel";
            container.Controls.Add(btnCancel);
        }
        IOrderedDictionary IBindableTemplate.ExtractValues(Control container)
        {
            return new OrderedDictionary();
        }
    }

    public class InsertItemActionTemplate : IBindableTemplate
    {
        void ITemplate.InstantiateIn(System.Web.UI.Control container)
        {
            LinkButton btnInsert = new LinkButton();
            btnInsert.ID = "cmdInsert";
            btnInsert.Text = btnInsert.CommandName = "Insert";
            container.Controls.Add(btnInsert);

            Literal spacer = new Literal();
            spacer.Text = "&nbsp;";
            container.Controls.Add(spacer);

            LinkButton btnCancel = new LinkButton();
            btnCancel.ID = "cmdCancel";
            btnCancel.Text = btnCancel.CommandName = "Cancel";
            container.Controls.Add(btnCancel);
        }
        IOrderedDictionary IBindableTemplate.ExtractValues(Control container)
        {
            return new OrderedDictionary();
        }
    }
}