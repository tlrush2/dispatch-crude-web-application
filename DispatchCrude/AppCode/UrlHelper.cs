﻿using System;
using System.Text.RegularExpressions;
using System.Web;
using System.Net;
using System.Net.Sockets;

namespace DispatchCrude.App_Code
{
    public static class UrlHelper
    {
        public static string BaseUrl(HttpContext ctx, string host = null, int port = 0)
        {
            if (ctx == null) ctx = HttpContext.Current;
            var url = ctx.Request.Url;
            if (port == 0) port = url.Port;
            var sPort = port != 80 ? (":" + port) : String.Empty;

            return string.Format("{0}://{1}{2}", url.Scheme, host ?? url.Host, sPort);
        }

        public static string LocalHost()
        {
            IPAddress[] localIPs = Dns.GetHostAddresses(Dns.GetHostName());
            foreach (IPAddress addr in localIPs)
            {
                if (addr.AddressFamily == AddressFamily.InterNetwork)
                {
                    return addr.ToString();
                }
            }
            return "localhost";
        }

        public static string ToAbsoluteUrl(this string relativeUrl, HttpContext ctx = null, string host = null, int port = 0)
        {
            if (string.IsNullOrEmpty(relativeUrl))
                return relativeUrl;

            if (ctx == null && HttpContext.Current != null)
                ctx = HttpContext.Current;
            if (ctx == null)
                return relativeUrl;

            if (relativeUrl.StartsWith("/"))
                relativeUrl = relativeUrl.Insert(0, "~");
            if (!relativeUrl.StartsWith("~/"))
                relativeUrl = relativeUrl.Insert(0, "~/");

            return string.Format("{0}{1}", BaseUrl(ctx, host, port), VirtualPathUtility.ToAbsolute(relativeUrl));
        }

        //public static string HtmlAppRelativeUrlsToAbsoluteUrls(this string html)
        //{
        //    if (string.IsNullOrEmpty(html))
        //        return html;

        //    const string htmlPattern = "(?<attrib>\\shref|\\ssrc|\\sbackground)\\s*?=\\s*?"
        //                              + "(?<delim1>[\"'\\\\]{0,2})(?!#|http|ftp|mailto|javascript)"
        //                              + "/(?<url>[^\"'>\\\\]+)(?<delim2>[\"'\\\\]{0,2})";

        //    var htmlRegex = new Regex(htmlPattern, RegexOptions.IgnoreCase | RegexOptions.Multiline);
        //    html = htmlRegex.Replace(html, m => htmlRegex.Replace(m.Value, "${attrib}=${delim1}" + ("~/" + m.Groups["url"].Value).ToAbsoluteUrl() + "${delim2}"));

        //    const string cssPattern = "@import\\s+?(url)*['\"(]{1,2}"
        //                              + "(?!http)\\s*/(?<url>[^\"')]+)['\")]{1,2}";

        //    var cssRegex = new Regex(cssPattern, RegexOptions.IgnoreCase | RegexOptions.Multiline);
        //    html = cssRegex.Replace(html, m => cssRegex.Replace(m.Value, "@import url(" + ("~/" + m.Groups["url"].Value).ToAbsoluteUrl() + ")"));

        //    return html;
        //}
    }

}