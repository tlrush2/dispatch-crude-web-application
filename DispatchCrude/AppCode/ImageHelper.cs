﻿using System.IO;
using System.Drawing.Imaging;
using System.Drawing;
using System;

namespace DispatchCrude.App_Code
{
    public class ImageHelper
    {
        static public byte[] ConvertImageToByteArray(System.Drawing.Image image)
        {
            return ConvertImageToByteArray(image, image.RawFormat);
        }

        static public byte[] ConvertImageToByteArray(System.Drawing.Image image, ImageFormat format)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                image.Save(ms, format);
                return ms.ToArray();
            }
        }

        public static Image ConvertByteArrayToImage(byte[] byteArray)
        {
            if (byteArray != null)
            {
                MemoryStream ms = new MemoryStream(byteArray, 0, byteArray.Length);
                ms.Write(byteArray, 0, byteArray.Length);
                return Image.FromStream(ms, true);
            }
            return null;
        }

        static public string BlobSrc(byte[] blob, string type)
        {
            return blob == null || blob.Length == 0 
                ? "" 
                : string.Format("data:image/{1};base64,{0}", Convert.ToBase64String(blob), type);
        }

    }
}