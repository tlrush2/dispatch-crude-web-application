﻿using System;
using System.Web;
using System.Web.UI.WebControls;
using System.Collections.Specialized;
using Telerik.Web.UI;
using System.Web.UI;
using System.ComponentModel;
using AlonsIT;
using DispatchCrude.Core;

namespace DispatchCrude.App_Code.Controls
{
    [ParseChildren(true), PersistChildren(false)]
    public class HandleRadGridDefaultValues : System.Web.UI.UserControl
    {
        [PersistenceMode(PersistenceMode.Attribute), DefaultValue(""), MergableProperty(false)]
        public string ControlID;

        [PersistenceMode(PersistenceMode.InnerProperty), DefaultValue(""), MergableProperty(false)]
        public ParameterCollection DefaultParameters { get; private set; }

        [PersistenceMode(PersistenceMode.InnerProperty), DefaultValue(""), MergableProperty(false)]
        public object GetParameterValue(Parameter param)
        {
            //Prepare an IDictionary with the predefined values
            IOrderedDictionary values = DefaultParameters.GetValues(Context, this);
            return values[param.Name];
        }

        public HandleRadGridDefaultValues()
        {   // since this isn't a full blown UserControl (aspx) file, we don't have AutoWireUpEvents, so wire them up in the constructor
            this.Init += Page_Init;
            this.Load += Page_Load;
            // initial this property
            DefaultParameters = new ParameterCollection();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                DisplayEntityInfo();
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (ControlID != null && ControlID.Length > 0)
            {
                Telerik.Web.UI.RadGrid grid = this.Parent.FindControl(ControlID) as Telerik.Web.UI.RadGrid;
                if (grid != null)
                {
                    grid.DataBound += grid_DataBound;
                    grid.ItemCommand += grid_ItemCommand;
                    grid.ItemDataBound += grid_ItemDataBound;
                }
            }
        }

        protected void grid_ItemCommand(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.InitInsertCommandName) //"Add new" button clicked
            {
                // cancel the default operation
                e.Canceled = true;

                //Prepare an IDictionary with the predefined values
                IOrderedDictionary newValues = DefaultParameters.GetValues(Context, this);

                //Insert the item and rebind
                e.Item.OwnerTableView.InsertItem(newValues);
            }
        }

        private void DisplayEntityInfo()
        {
            IOrderedDictionary defaults = DefaultParameters.GetValues(Context, this);
            foreach (Parameter param in DefaultParameters)
            {   // if this is a single entity, and the "Caption" attributes are defined, then retrieve the caption and display it (on the master page field)
                if (Converter.ToInt32(defaults[param.Name]) > 0
                    && Page.Master is DispatchCrude.SiteMaster
                    && DefaultParameters[param.Name] is HasAllChoiceParameter
                    && (DefaultParameters[param.Name] as HasAllChoiceParameter).CaptionTableName != null
                    && (DefaultParameters[param.Name] as HasAllChoiceParameter).CaptionTableName.Length > 0
                    && (DefaultParameters[param.Name] as HasAllChoiceParameter).CaptionFieldName != null
                    && (DefaultParameters[param.Name] as HasAllChoiceParameter).CaptionFieldName.Length > 0)
                {
                    using (SSDB db = new SSDB())
                    {
                        // retrieve the Entity Name for the hidden column
                        string entity = db.QuerySingleValue("SELECT {0} FROM {1} WHERE ID={2}"
                            , (DefaultParameters[param.Name] as HasAllChoiceParameter).CaptionFieldName
                            , (DefaultParameters[param.Name] as HasAllChoiceParameter).CaptionTableName
                            , Converter.ToInt32(defaults[param.Name])).ToString();
                        // retrieve the Entity Caption (optionally provided)
                        string caption = (DefaultParameters[param.Name] as HasAllChoiceParameter).CaptionEntity;
                        // append the Entity "Name"
                        caption += (caption.Length > 0 ? ": " : "") + entity;
                        // append the entire caption to the existing site "caption"
                        (Page.Master as DispatchCrude.RootMaster).EntityCaptionText =
                            ((Page.Master as DispatchCrude.RootMaster).EntityCaptionText += " - " + caption).Trim().Trim('-');
                    }
                }
            }
        }

        protected void grid_DataBound(object sender, EventArgs e)
        {   // hide any "single value" columns that are specified to be hidden
            if (DefaultParameters.Count > 0)
            {
                IOrderedDictionary defaults = DefaultParameters.GetValues(Context, this);
                foreach (Parameter param in DefaultParameters)
                {
                    if (param is HasAllChoiceParameter
                        && (DefaultParameters[param.Name] as HasAllChoiceParameter).HideSingle
                        && Converter.ToInt32(defaults[param.Name]) > 0)
                    {
                        GridColumn col = (sender as RadGrid).Columns.FindByUniqueNameSafe(param.Name);
                        if (col != null)
                            col.Visible = false;
                        if ((DefaultParameters[param.Name] as HasAllChoiceParameter).HideAdditionalColumns.Length > 0)
                        {
                            string[] hideAdditional = (DefaultParameters[param.Name] as HasAllChoiceParameter).HideAdditionalColumns.Split(',');
                            foreach (string colName in hideAdditional)
                            {
                                GridColumn additionalCol = (sender as RadGrid).Columns.FindByUniqueNameSafe(colName);
                                if (additionalCol != null)
                                    additionalCol.Visible = false;
                            }
                        }
                    }
                }
            }
        }

        protected void grid_ItemDataBound(object sender, GridItemEventArgs e)
        {   // disable any "single value" columns that are specified to be "Disabled"
            if (e.Item is GridEditableItem && DefaultParameters.Count > 0)
            {
                IOrderedDictionary defaults = DefaultParameters.GetValues(Context, this);
                foreach (Parameter param in DefaultParameters)
                {
                    if (param is HasAllChoiceParameter 
                        && (DefaultParameters[param.Name] as HasAllChoiceParameter).DisableSingle
                        && Converter.ToInt32(defaults[param.Name]) > 0)
                    {
                        TableCell cell = null;
                        try
                        {
                            cell = (e.Item as GridEditableItem)[param.Name];
                        }
                        catch (Exception)
                        {
                            // just eat these errors, I couldn't find a way to check without potentially throwing an exception
                        }
                        if (cell != null)
                            DisableCellEditControls(cell);
                    }
                }
            }
        }

        private void DisableCellEditControls(TableCell cell)
        {
            foreach (Control ctrl in cell.Controls)
            {
                if (ctrl is WebControl && !(ctrl is BaseValidator))
                    (ctrl as WebControl).Enabled = false;
            }
        }

    }

    [AspNetHostingPermission(System.Security.Permissions.SecurityAction.Demand, Level = AspNetHostingPermissionLevel.Minimal)]
    [ParseChildren(true), PersistChildren(false)]
    public class HasAllChoiceParameter : Parameter
    {
        public HasAllChoiceParameter(): base("") 
        {
            // set default values
            DisableSingle = true;
            HideSingle = false;
            HideAdditionalColumns = string.Empty;
        }

        [PersistenceMode(PersistenceMode.Attribute), DefaultValue(true), MergableProperty(false)]
        public bool DisableSingle { get; set; }

        [PersistenceMode(PersistenceMode.Attribute), DefaultValue(false), MergableProperty(false)]
        public bool HideSingle { get; set; }

        [PersistenceMode(PersistenceMode.Attribute), DefaultValue(false), MergableProperty(false)]
        public string HideAdditionalColumns { get; set; }

        [PersistenceMode(PersistenceMode.Attribute), DefaultValue(""), MergableProperty(false)]
        public string CaptionEntity { get; set; }

        [PersistenceMode(PersistenceMode.Attribute), DefaultValue(""), MergableProperty(false)]
        public string CaptionTableName { get; set; }

        [PersistenceMode(PersistenceMode.Attribute), DefaultValue(""), MergableProperty(false)]
        public string CaptionFieldName { get; set; }
    }

    [AspNetHostingPermission(System.Security.Permissions.SecurityAction.Demand, Level = AspNetHostingPermissionLevel.Minimal)]
    [ParseChildren(true), PersistChildren(false)]
    public class HasAllChoiceProfileParameter : HasAllChoiceParameter
    {
        // The Evaluate method is overridden to return a "computed" Profile value 
        protected override object Evaluate(HttpContext context, Control control)
        {
            if (context.Request == null)
                return DefaultValue;
            // return either the profile ID value, or 0 (for "All" or "None")
            int id = DispatchCrudeHelper.GetProfileValueInt(context, this.Name);
            return id > 0 ? id : 0;
        }
    }

    [AspNetHostingPermission(System.Security.Permissions.SecurityAction.Demand, Level = AspNetHostingPermissionLevel.Minimal)]
    [ParseChildren(true), PersistChildren(false)]
    public class HasAllChoiceQueryStringParameter : HasAllChoiceParameter
    {
        // The Evaluate method is overridden to return a "computed" Profile value 
        protected override object Evaluate(HttpContext context, Control control)
        {
            if (context.Request == null)
                return DefaultValue;
            // return either the querystring ID value, or 0 (for "All" or "None")
            int id = DBHelper.ToInt32(context.Request.QueryString[this.Name] ?? (object)0);
            return id > 0 ? id : 0;
        }
    }

}