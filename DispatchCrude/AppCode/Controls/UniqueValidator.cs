﻿using System;
using System.Web.UI;
using System.ComponentModel;
using Telerik.Web.UI;
using DispatchCrude.Core;
using AlonsIT;

namespace DispatchCrude.App_Code.Controls
{
    public delegate object GetCellValue(GridItem item, string uniqueName);

    [ParseChildren(true)]
    [PersistChildren(false)]
    public class UniqueValidator : System.Web.UI.WebControls.CustomValidator
    {
        public UniqueValidator()
        {
            this.EnableClientScript = false;
            this.OtherUniqueNames_CSV = null;
        }

        [PersistenceMode(PersistenceMode.InnerProperty)]
        public event GetCellValue OnGetOtherCellValue;
        private object RaiseGetOtherCellValue(GridItem item, string uniqueName)
        {
            object ret = null; // default value
            if (OnGetOtherCellValue != null)
                ret = OnGetOtherCellValue(item, uniqueName);
            return ret;
        }

        [PersistenceMode(PersistenceMode.Attribute), DefaultValue(""), MergableProperty(false)]
        public string TableName;

        [PersistenceMode(PersistenceMode.Attribute), DefaultValue(""), MergableProperty(false)]
        public string DataField;

        [PersistenceMode(PersistenceMode.Attribute), DefaultValue(""), MergableProperty(false)]
        public string OtherUniqueNames_CSV;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override bool OnServerValidate(string value)
        {   // attempt to get the ID for the related record (there must be a better way to generalize this)
            int id = App_Code.RadGridHelper.GetGridItemID(this.NamingContainer as GridItem);
            try
            {
                using (SSDB ssdb = new SSDB())
                {
                    string whereOtherFields = "";
                    if (OtherUniqueNames_CSV != null)
                    {
                        foreach (string df in OtherUniqueNames_CSV.Split(new char[] { ',' }))
                        {
                            object val = RaiseGetOtherCellValue(this.NamingContainer as GridItem, df.Trim());
                            if (val != null)
                                whereOtherFields += string.Format(" AND {0} LIKE {1} ", df.Trim(), DBHelper.QuoteStr(val.ToString()));
                        }
                    }

                    IsValid = Converter.ToInt32(
                        ssdb.QuerySingleValue("SELECT count(*) FROM {0} WHERE {1} LIKE {2}{4} AND ID <> {3}"
                            , TableName
                            , DataField
                            , DBHelper.QuoteStr(value)
                            , id
                            , whereOtherFields)) == 0;
                }
            }
            catch (Exception)
            {
                IsValid = false;
            }
            return IsValid;
        }
    }
}