﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using AlonsIT;
using DispatchCrude.Core;

namespace DispatchCrude.App_Code.Controls
{
    public delegate void GetUnboundValues(DataRow values);

    [PersistChildren(false), ParseChildren(true)]
    public class DBDataSource: DataSourceControl
    {
        #region Fields

        protected static readonly string[] _views = { "DefaultView" };
        protected DBDataSourceView _view;

        #endregion

        #region Properties

        [PersistenceMode(PersistenceMode.Attribute), Category("Data"), DefaultValue(""), MergableProperty(false)]
        public string UpdateTableName
        {
            get { return View.UpdateTableName; }
            set { View.UpdateTableName = value; }
        }

        [PersistenceMode(PersistenceMode.Attribute), Category("Data"), DefaultValue(""), MergableProperty(false)]
        public string SelectCommand
        {
            get { return View.SelectCommand; }
            set { View.SelectCommand = value; }
        }

        [PersistenceMode(PersistenceMode.InnerProperty), Category("Data"), DefaultValue((ParameterCollection)null), MergableProperty(false)]
        public ParameterCollection SelectParameters
        {
            get { return View.SelectParameters; }
        }

        [PersistenceMode(PersistenceMode.InnerProperty), Category("Data"), DefaultValue(false), MergableProperty(false)]
        public bool SelectIDNullsToZero
        {
            get { return View.SelectIDNullsToZero; }
            set { View.SelectIDNullsToZero = value; }
        }

        [PersistenceMode(PersistenceMode.InnerProperty), Category("Data"), DefaultValue(30), MergableProperty(false)]
        public int CommandTimeout
        {
            get { return View.CommandTimeout; }
            set { View.CommandTimeout = value; }
        }

        public event GetUnboundValues GetUnboundValues;
        private void RaiseGetUnboundValues(System.Data.DataRow values)
        {
            if (GetUnboundValues != null)
                GetUnboundValues(values);
        }

        protected DBDataSourceView View
        {
            get
            {
                if (_view == null)
                {
                    _view = new DBDataSourceView(this, _views[0]);
                    _view.GetUnboundValues += new GetUnboundValues(this.RaiseGetUnboundValues);
                    if (base.IsTrackingViewState)
                    {
                        ((IStateManager)_view).TrackViewState();
                    }
                }
                return _view;
            }
        }

        #endregion

        #region IDataSource Methods

        protected override DataSourceView GetView(string viewName)
        {
            if ((viewName == null) || ((viewName.Length != 0) && (String.Compare(viewName, _views[0], StringComparison.OrdinalIgnoreCase) != 0)))
            {
                throw new ArgumentException(string.Format("An invalid view was requested: {0}", viewName), "viewName");
            }

            return View;
        }

        protected override ICollection GetViewNames()
        {
            return _views;
        }

        #endregion

        #region ViewState Management

        // We use a pair to store the view state. The first element is used to store the parent's view state
        // The second element is used to store the view's view state

        protected override void LoadViewState(object savedState)
        {
            Pair previousState = (Pair)savedState;

            if (savedState == null)
            {
                base.LoadViewState(null);
            }
            else
            {
                base.LoadViewState(previousState.First);

                if (previousState.Second != null)
                {
                    ((IStateManager)View).LoadViewState(previousState.Second);
                }
            }
        }

        protected override object SaveViewState()
        {
            Pair currentState = new Pair();

            currentState.First = base.SaveViewState();

            if (_view != null)
            {
                currentState.Second = ((IStateManager)View).SaveViewState();
            }

            if ((currentState.First == null) && (currentState.Second == null))
            {
                return null;
            }

            return currentState;
        }

        protected override void TrackViewState()
        {
            base.TrackViewState();

            if (_view != null)
            {
                ((IStateManager)View).TrackViewState();
            }
        }

        #endregion

        #region Life Cycle Related Methods

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            // handle the LoadComplete event to update select parameters
            if (Page != null)
            {
                Page.LoadComplete += new EventHandler(UpdateParameterValues);
            }
        }

        protected virtual void UpdateParameterValues(object sender, EventArgs e)
        {
            SelectParameters.UpdateValues(Context, this);
        }

        #endregion

        #region Select Method

        public IEnumerable Select()
        {
            return View.Select(DataSourceSelectArguments.Empty);
        }

        #endregion

    }

    public class DBDataSourceView: DataSourceView, IStateManager
    {
        #region Fields

		bool _tracking;
		protected DBDataSource _owner;
		protected string _selectCommand;
		protected string _updateTableName;
		protected ParameterCollection _selectParameters;
        protected int _cmdTimeout;
        protected bool _selectIDNullsToZero;

		#endregion

		#region Properties

		#region DataSourceView's CanXXX Properties

		public override bool CanInsert
		{
			get	{ return true; }
		}

		public override bool CanUpdate
		{
			get	{ return true; }
		}

		public override bool CanDelete
		{
			get { return true; }
		}

		public override bool CanRetrieveTotalRowCount
		{
			get	{ return false; }
		}

		public override bool CanSort
		{
			get	{ return false;	}
		}

		public override bool CanPage
		{
			get	{ return false; }
		}

		#endregion

		#region Other Properties

		public string SelectCommand
		{
			get	{
				if (_selectCommand == null) {
					return String.Empty;
				}
				return _selectCommand;
			}
			set	{
				if (SelectCommand != value) {
					_selectCommand = value;
					OnDataSourceViewChanged(EventArgs.Empty);
				}
			}
		}

		public string UpdateTableName
		{
			get	{
				if (_updateTableName == null) {
					return String.Empty;
				}
				return _updateTableName;
			}
			set	{
				if (UpdateTableName != value) {
					_updateTableName = value;
					OnDataSourceViewChanged(EventArgs.Empty);
				}
			}
		}

		public ParameterCollection SelectParameters
		{
			get	{
				if (_selectParameters == null) {
					_selectParameters = new ParameterCollection();
					_selectParameters.ParametersChanged += new EventHandler(ParametersChangedEventHandler);
					if (_tracking) {
						((IStateManager)_selectParameters).TrackViewState();
					}
				}
				return _selectParameters;
			}
		}

        public int CommandTimeout
        {
            get { return _cmdTimeout; }
            set { _cmdTimeout = value; }
        }

        public bool SelectIDNullsToZero
        {
            get { return _selectIDNullsToZero; }
            set { _selectIDNullsToZero = value; }
        }

		#endregion

		#endregion

		#region Methods

		#region Constructors

		public DBDataSourceView(DBDataSource owner, string name) : base(owner, name)
		{
			_owner = owner;
		}

		#endregion

		#region IStateManager Methods

		bool IStateManager.IsTrackingViewState
		{
			get	{ return _tracking; }
		}

		void IStateManager.LoadViewState(object savedState)
		{
			LoadViewState(savedState);
		}

		object IStateManager.SaveViewState()
		{
			return SaveViewState();
		}

		void IStateManager.TrackViewState()
		{
			TrackViewState();
		}

		protected virtual void LoadViewState(object savedState)
		{
			if (savedState != null) {
				if (savedState != null){
					((IStateManager)SelectParameters).LoadViewState(savedState);
				}
			}
		}

		protected virtual object SaveViewState()
		{
			if (_selectParameters != null){
				return ((IStateManager)_selectParameters).SaveViewState();
			} else {
				return null;
			}
		}

		protected virtual void TrackViewState()
		{
			_tracking = true;

			if (_selectParameters != null)	{
				((IStateManager)_selectParameters).TrackViewState();
			}
		}

		#endregion

		#region ExecuteXXX Methods

		protected override IEnumerable ExecuteSelect(DataSourceSelectArguments arguments)
		{
			// if there isn't a select method, error
			if (SelectCommand.Length == 0)
            {
				throw new InvalidOperationException(_owner.ID + ": No SelectCommand is defined");
			}

			// check if we support the capabilities the data bound control expects
			arguments.RaiseUnsupportedCapabilitiesError(this);

            try
            {
                using (SSDB db = new SSDB())
                {
                    using (SqlCommand cmd = db.BuildCommand(SelectCommand))
                    {
                        // if an explicit CommandTimeout value was specified, use it here
                        if (this.CommandTimeout != 0)
                            cmd.CommandTimeout = this.CommandTimeout;

                        IOrderedDictionary selectValues = SelectParameters.GetValues(System.Web.HttpContext.Current, _owner);
                        foreach (object key in selectValues.Keys)
                        {
                            string paramKey = "@" + key.ToString();
                            object paramValue = selectValues[key] ?? SelectParameters[key.ToString()].DefaultValue;
                            if (paramValue == null || (paramValue is string && paramValue.ToString().Equals("null", StringComparison.CurrentCultureIgnoreCase)))
                                paramValue = DBNull.Value;
                            if (cmd.Parameters.IndexOf(paramKey) != -1)
                                cmd.Parameters[paramKey].Value = paramValue;
                            else
                                cmd.Parameters.AddWithValue(paramKey, paramValue);
                        }
                        DataTable data = SSDB.GetPopulatedDataTable(cmd);
                        data = Core.DateHelper.AddLocalRowStateDateFields(data, HttpContext.Current);
                        if (_selectIDNullsToZero)
                            data = DispatchCrude_ConnFactory.SetIDNullsToZero(data);
                        data = DispatchCrude_ConnFactory.AddActiveColumn(data);                        
                        return data.DefaultView;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("SelectCommand = \"{0}\"", SelectCommand), ex);
            }
		}

		protected override int ExecuteInsert(IDictionary values)
		{
            return ExecuteChange(values, GetDestTable().NewRow());
		}

		protected override int ExecuteUpdate(IDictionary keys, IDictionary values, IDictionary oldValues)
		{
            DataTable dt = GetDestTable(Converter.ToInt32(oldValues["ID"] ?? values["ID"]));
            if (dt.Rows.Count == 1)
                return ExecuteChange(values, dt.Rows[0]);
            else
                throw new InvalidOperationException(string.Format("Row not found to update for ID={0}", oldValues["ID"]));
        }

		protected override int ExecuteDelete(IDictionary keys, IDictionary oldValues)
		{
            int id = Convert.ToInt32(oldValues["ID"] ?? 0);
            using (SSDB ssdb = new SSDB())
            {
                return ssdb.ExecuteSql("DELETE FROM dbo.{0} WHERE ID={1}", UpdateTableName, id);
            }
        }

		#endregion

		#region Helpers

        private int ExecuteChange(IDictionary values, DataRow row)
        {
            row.Table.ColumnChanging += ColumnChangingHandler;
            UpdateValues(values, row);
            using (SSDB ssdb = new SSDB())
            {
                return DBHelper.ToInt32(SSDBSave.SaveDataRow(ssdb, row, UpdateTableName, "ID", UserSupport.UserName));
            }
        }

        private DataTable GetDestTable(int id = 0)
        {
            using (SSDB ssdb = new SSDB())
            {
                return ssdb.GetPopulatedDataTable("SELECT * FROM {0} WHERE ID={1}", UpdateTableName, id);
            }
        }

        private void ColumnChangingHandler(object sender, DataColumnChangeEventArgs e)
        {
            // DataRow field will not accept null, so translate null -> DBNull.Value (when Nulls allowed)
            if (e.ProposedValue == null && e.Column.AllowDBNull)
            {
                e.ProposedValue = DBNull.Value;
            }
            // if the column accepts NULL, and is an "ID" column, and the value is not a positive integer, then convert it to DBNULL.Value
            else if (e.Column.AllowDBNull && e.Column.ColumnName.EndsWith("ID") && Converter.ToInt32(e.ProposedValue) == 0
                && DispatchCrude_ConnFactory.IsIntegerType(e.Column.DataType))
            {
                e.ProposedValue = DBNull.Value;
            }
        }

        private void UpdateValues(IDictionary values, DataRow row)
        {
            foreach (object key in values.Keys)
            {
                if (row.Table.Columns.Contains(key.ToString()) && !row.Table.Columns[key.ToString()].ReadOnly)
                    row[key.ToString()] = values[key];
            }
            RaiseGetUnboundValues(row);
        }

        public event GetUnboundValues GetUnboundValues;
        private void RaiseGetUnboundValues(DataRow values)
        {
            if (GetUnboundValues != null)
                GetUnboundValues(values);
        }

		public IEnumerable Select(DataSourceSelectArguments args)
		{
			return ExecuteSelect(args);
		}

		protected void ParametersChangedEventHandler(object o, EventArgs e)
		{
			OnDataSourceViewChanged(EventArgs.Empty);
		}

		#endregion

		#endregion    
    }
}