﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using AlonsIT;
using DispatchCrude.Core;
using DispatchCrude.Extensions;

namespace DispatchCrude.App_Code.Controls
{
    public delegate void GridRecordEvent(GridEditableItem item, int id);
    public delegate void GridErrorMessage(ref string error);

    [ParseChildren(true)]
    [PersistChildren(false)]
    public class RadGridDBCtrl : System.Web.UI.UserControl
    {
        public static String CMD_UNDELETE = "Undelete";

        private int _selID;

        [PersistenceMode(PersistenceMode.InnerProperty)]
        public string ControlID;
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public string SelectCommand;
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public string UpdateTableName;
        [PersistenceMode(PersistenceMode.InnerProperty), DefaultValue(true)]
        public bool FilterActiveEntities;
        [PersistenceMode(PersistenceMode.InnerProperty), DefaultValue(false)]
        public bool AdminNoShowIDColumns;

        [PersistenceMode(PersistenceMode.InnerProperty)]
        public event App_Code.GetUnboundChangedValues GetUnboundChangedValues;
        protected void RaiseGetUnboundChangedValues(GridEditableItem item, DataRow values)
        {
            GetUnboundChangedValues?.Invoke(item, values);
        }

        [PersistenceMode(PersistenceMode.InnerProperty)]
        public event EventHandler OnRowSelected;
        protected void RaiseOnRowSelected(EventArgs e)
        {
            OnRowSelected?.Invoke(this, e);
        }

        [PersistenceMode(PersistenceMode.InnerProperty)]
        public event GridRecordEvent ChangesSaved;
        private void RaiseChangesSaved(GridEditableItem item, int id)
        {
            ChangesSaved?.Invoke(item, id);
        }

        [PersistenceMode(PersistenceMode.InnerProperty)]
        public event GridErrorMessage GetGridErrorMessage;
        private void RaiseGetGridErrorMessage(ref string error)
        {
            // common error "translation" logic
            error = DBSupport.TranslateErrorMessage(error);

            // give the page an opportunity to override the raw error message
            GetGridErrorMessage?.Invoke(ref error);
        }

        public RadGridDBCtrl()
        {
            FilterActiveEntities = true; // default value
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            Initialize();
        }

        protected void Page_Load(object sender, EventArgs e)
        {   // hook up the data update event handlers
            HookupDataEventHandlers(this.Parent.FindControl(ControlID) as Telerik.Web.UI.RadGrid);
        }

        private RadGrid Grid { get { return ControlID == null ? null : this.Parent.FindControl(ControlID) as RadGrid; } }

        public void Initialize()
        {
            if (ControlID != null)
            {
                RadGrid grid = Grid;
                if (grid != null)
                {
                    RemoveRangeFilterMenuItems(grid);
                    grid.DataBound += grid_DataBound;
                    if (HasValidSelect)
                    {
                        grid.NeedDataSource += grid_NeedDataSource;
                    }
                    else if (!IsPostBack && FilterActiveEntities) // else the grid is likely already populate so ensure the Active column filtering is on (if 
                        SetActiveFilter(grid);

                    grid.ItemDataBound += grid_ItemDataBound;
                    grid.PreRender += grid_PreRender;
                    grid.ItemCreated += grid_ItemCreated;

                    GridColumn col = null;
                    if (!AdminNoShowIDColumns && (UserSupport.IsInGroup("_DCAdministrator") || UserSupport.IsInGroup("_DCSystemManager") || UserSupport.IsInGroup("_DCSupport"))
                        && (col = grid.MasterTableView.Columns.FindByDataFieldSafe("ID")) != null
                        && col.HeaderText == "ID" // don't display if headertext value is not "ID"
                        && (!col.Display || !col.Visible))
                    {
                        col.Display = true;
                        col.Visible = true;
                        if (col is GridEditableColumn)
                        {
                            (col as GridEditableColumn).AllowFiltering = false;
                            (col as GridEditableColumn).ReadOnly = true;
                        }
                        if (col.HeaderStyle.Width.Value < 35)
                            col.HeaderStyle.Width = Unit.Pixel(35);
                    }
                }
            }
        }

        private void HookupDataEventHandlers(Telerik.Web.UI.RadGrid grid)
        {
            if (grid != null && UpdateTableName != null && UpdateTableName.Length > 0)
            {
                grid.ItemCommand += grid_ItemCommand;
                grid.CancelCommand += grid_CancelCommand;
                grid.DeleteCommand += grid_DeleteCommand;
                if (grid.MasterTableView.EditMode != GridEditMode.PopUp || Converter.IsNullOrEmpty(grid.MasterTableView.EditFormSettings.UserControlName))
                {
                    grid.InsertCommand += grid_InsertCommand;
                    grid.UpdateCommand += grid_UpdateCommand;
                }
            }
        }

        private bool HasValidSelect { get { return SelectCommand != null && SelectCommand.Length > 0; } }

        public DataTable GetData()
        {
            using (SSDB db = new SSDB())
            {
                using (SqlCommand cmd = db.BuildCommand(SelectCommand))
                {
                    DataTable data = SSDB.GetPopulatedDataTable(cmd);
                    DispatchCrude_ConnFactory.AddActiveColumn(data);
                    return Core.DateHelper.AddLocalRowStateDateFields(data, HttpContext.Current);
                }
            }
        }

        protected void grid_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            if (!e.IsFromDetailTable)
            {
                (sender as RadGrid).DataSource = GetData();
            }
            if (!IsPostBack && FilterActiveEntities) // else the grid is likely already populate so ensure the Active column filtering is on (if 
                SetActiveFilter(sender as RadGrid);
        }

        protected void grid_DataBound(object sender, EventArgs e)
        {
            SetActiveFilter(sender as RadGrid);
        }

        private void SetActiveFilter(RadGrid grid)
        {
            if (FilterActiveEntities && !IsPostBack)
            {
                string currentFilterExpression = grid.MasterTableView.FilterExpression;
                if (grid.MasterTableView.Columns.FindByDataFieldSafe("Active") != null)
                {
                    try
                    {
                        // see http://www.telerik.com/help/aspnet-ajax/grid-operate-with-filter-expression-manually.html for details
                        if (grid.EnableLinqExpressions)
                            grid.MasterTableView.FilterExpression = @"Convert.ToBoolean(it[""Active""]) == True";
                        else
                            grid.MasterTableView.FilterExpression = "([Active] = True)";
                        GridColumn column = grid.MasterTableView.Columns.FindByDataField("Active");
                        column.CurrentFilterFunction = GridKnownFunction.EqualTo;
                        column.CurrentFilterValue = "True";
                    }
                    catch (InvalidCastException)
                    {
                        // if Active has a NULL value, then an invalid cast occurs, just eat that error (no easy way to detect)
                    }
                }
                else if (grid.MasterTableView.Columns.FindByDataFieldSafe("DeleteDate") != null)
                {
                    // see http://www.telerik.com/help/aspnet-ajax/grid-operate-with-filter-expression-manually.html for details
                    if (grid.EnableLinqExpressions)
                        grid.MasterTableView.FilterExpression = @"it[""DeleteDate""] == Convert.DBNull";
                    else
                        grid.MasterTableView.FilterExpression = "([DeleteDate] IS NULL)";
                    grid.MasterTableView.Columns.FindByDataField("DeleteDate").CurrentFilterFunction = GridKnownFunction.IsNull;
                }
                else if (grid.MasterTableView.Columns.FindByDataFieldSafe("DeleteDateUTC") != null)
                {
                    // see http://www.telerik.com/help/aspnet-ajax/grid-operate-with-filter-expression-manually.html for details
                    if (grid.EnableLinqExpressions)
                        grid.MasterTableView.FilterExpression = @"it[""DeleteDateUTC""] == Convert.DBNull";
                    else
                        grid.MasterTableView.FilterExpression = "([DeleteDateUTC] IS NULL)";
                    grid.MasterTableView.Columns.FindByDataField("DeleteDateUTC").CurrentFilterFunction = GridKnownFunction.IsNull;
                }
                if (!string.IsNullOrEmpty(currentFilterExpression))
                {
                    grid.MasterTableView.FilterExpression += string.Format(" AND ({0})", currentFilterExpression);
                }
            }
        }

        protected void grid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            foreach (GridColumn col in e.Item.OwnerTableView.Columns)
            {
                if (e.Item is GridDataItem && e.Item.DataItem is DataRowView)
                {
                    if (col is GridAttachmentColumn && (e.Item as GridDataItem)[col.UniqueName].Controls[0] is LinkButton)
                    {   // disable the "download LinkButton" if a filename is not available
                        ((e.Item as GridDataItem)[col.UniqueName].Controls[0] as LinkButton).Enabled =
                            (e.Item.DataItem as DataRowView)[(col as GridAttachmentColumn).FileNameTextField].ToString().Length > 0;
                    }
                    else if ((col is GridButtonColumn && (col as GridButtonColumn).UniqueName.Contains("Delete"))
                        || (col is GridTemplateColumn && (col as GridTemplateColumn).UniqueName == "ActionColumn"))
                    {
                        if (e.Item.DataItem is DataRowView && (e.Item.DataItem as DataRowView).Row.Table.Columns.Contains("DeleteDateUTC")
                            && !DBHelper.IsNull((e.Item.DataItem as DataRowView)["DeleteDateUTC"]))
                        {
                            GridTableView gtv = e.Item.OwnerTableView;
                            ImageButton btnDelete = null, btnEdit = null;
                            if (gtv != null)
                            {
                                if (gtv.Columns.FindByUniqueNameSafe("ActionColumn") != null)
                                {
                                    btnDelete = (ImageButton)RadGridHelper.GetColumnControlByType((e.Item as GridDataItem)["ActionColumn"], typeof(ImageButton), "btnDelete");
                                    btnEdit = (ImageButton)RadGridHelper.GetColumnControlByType((e.Item as GridDataItem)["ActionColumn"], typeof(ImageButton), "btnEdit");
                                }
                                else if (gtv.Columns.FindByUniqueNameSafe("DeleteColumn") != null)
                                {
                                    btnDelete = (RadGridHelper.GetControlByType((e.Item as GridDataItem)["DeleteColumn"], typeof(ImageButton)) as ImageButton);
                                    btnEdit = (RadGridHelper.GetControlByType((e.Item as GridDataItem)["EditColumn"], typeof(ImageButton)) as ImageButton);
                                }
                            }

                            // change the CommandName to Undelete & change the icon color to undelete icon
                            if (btnDelete != null)
                            {
                                btnDelete.ImageUrl = "~/images/undelete.png";
                                btnDelete.CommandName = CMD_UNDELETE;
                                btnDelete.ToolTip = "Reactivate";
                            }
                            // prevent editing of Deleted records
                            if (btnEdit != null)
                            {
                                btnEdit.Enabled = false;
                            }
                        }
                    }
                    else if (col is GridHyperLinkColumn)
                    {
                        HyperLink hl = RadGridHelper.GetControlByType((e.Item as GridDataItem)[col.UniqueName], typeof(HyperLink)) as HyperLink;
                        if (hl != null && hl.Enabled)
                            hl.ForeColor = System.Drawing.Color.Blue;
                    }
                }
                if (e.Item.OwnerTableView.EditMode != GridEditMode.PopUp
                    && e.Item.IsInEditMode
                    && (col is GridBoundColumn || col is GridNumericColumn || col is GridDropDownColumn)) // handle properly sizing the internal RadNumericTextBox (otherwise always 160px)
                {
                    try
                    {
                        WebControl innerCtrl = null;
                        if ((e.Item is GridDataItem
                            && (e.Item as GridDataItem)[col.UniqueName] != null
                            && (e.Item as GridDataItem)[col.UniqueName].Controls.Count > 0
                            && (innerCtrl = (WebControl)(e.Item as GridDataItem)[col.UniqueName].Controls[0]) != null)
                            ||
                            (e.Item is GridEditableItem
                            && (e.Item as GridEditableItem)[col.UniqueName] != null
                            && (e.Item as GridEditableItem)[col.UniqueName].Controls.Count > 0
                            && (innerCtrl = (WebControl)(e.Item as GridEditableItem)[col.UniqueName].Controls[0]) != null))
                        {
                            try
                            {
                                // only use 95% for GridDataItem controls (inplace editing), otherwise only set the width if an explicit ItemStyle.Width was provided
                                if (e.Item is GridDataItem || !col.ItemStyle.Width.IsEmpty)
                                    innerCtrl.Width = !col.ItemStyle.Width.IsEmpty ? col.ItemStyle.Width : new Unit(95, UnitType.Percentage);
                            }
                            catch (Exception)
                            {
                                // eat these errors
                            }
                        }
                    }
                    catch (Telerik.Web.UI.GridNotSupportedException)
                    {
                        // eat these errors
                    }
                }
            }
        }


        protected void grid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            // stream the stored binary file if the DownloadAttachmentCommandName command is received
            if (e.CommandName == RadGrid.DownloadAttachmentCommandName)
            {
                e.Canceled = true;

                GridDownloadAttachmentCommandEventArgs args = e as GridDownloadAttachmentCommandEventArgs;
                string fileName = App_Code.IOHelper.MakeValidFileName(args.FileName);

                if (args.AttachmentColumn.AttachmentDataField.Length > 0 && fileName.Length > 0)
                {
                    using (SSDB db = new SSDB())
                    {
                        using (SqlCommand cmd = db.BuildCommand(
                            string.Format("SELECT {0} FROM dbo.{1} WHERE ID=@ID", args.AttachmentColumn.AttachmentDataField, UpdateTableName)))
                        {
                            int id = DBHelper.ToInt32(args.AttachmentKeyValues["ID"]);
                            cmd.Parameters.AddWithValue("@ID", id);
                            DataTable data = SSDB.GetPopulatedDataTable(cmd);
                            byte[] binaryData = (byte[])data.Rows[0][0];

                            Response.Clear();
                            Response.ContentType = "application/octet-stream";
                            Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
                            Response.BinaryWrite(binaryData);
                            Response.EndSafe();
                        }
                    }
                }
            }
            else if (e.CommandName == CMD_UNDELETE)
            {
                GetDBSave_RadGrid().UndeleteGridRow(e.Item);
                RebindGrid(sender as RadGrid, id: RadGridHelper.GetGridItemID(e.Item));
            }
        }

        private void grid_InsertCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (!RadGridHelper.ValidateAttachmentColumns(e.Item as GridEditableItem))
                e.Canceled = true;
            else
            {
                try
                {
                    int id = GetDBSave_RadGrid().InsertGridRow(e.Item as Telerik.Web.UI.GridEditableItem);
                    RadGridHelper.UpdateAttachmentColumns(e.Item as GridEditableItem, UpdateTableName, id);
                    RaiseChangesSaved(e.Item as GridEditableItem, id);
                    // set to null to avoid weird insert errors 
                    (sender as RadGrid).DataSource = null;
                    RebindGrid(sender as RadGrid, id: id);
                }
                catch (Exception ex)
                {
                    if (HandleGridError(e.Item, ex))
                        e.Canceled = true;
                    else
                        throw ex;
                }
            }
        }

        // returns true if the error has been handled and the grid operation should be canceled
        private bool HandleGridError(GridItem item, Exception ex)
        {
            bool ret = false;
            if (item is GridEditableItem && (item as GridEditableItem).OwnerTableView.Columns.FindByUniqueNameSafe("Validation") != null)
            {
                Control wc = RadGridHelper.GetControlByType(item as GridEditableItem, typeof(CustomValidator), false, "cvGridError");
                if (wc != null)
                {
                    const string ERR_MARKER = "Error occurred: ";
                    string error = ex.Message;
                    if (error.Contains(ERR_MARKER))
                        error = error.Substring(error.IndexOf(ERR_MARKER) + ERR_MARKER.Length);
                    RaiseGetGridErrorMessage(ref error);
                    (wc as CustomValidator).ErrorMessage = error;
                    (wc as CustomValidator).IsValid = false;
                    ret = true;
                }
            }
            return ret;
        }

        private void grid_DeleteCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            int id = RadGridHelper.GetGridItemID(e.Item as GridEditableItem);
            RaiseChangesSaved(e.Item as GridEditableItem, id);
            GetDBSave_RadGrid().DeleteGridRow(e.Item);
            RebindGrid(sender as RadGrid, id: id);
        }

        private void grid_UpdateCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (UpdateTableName.Length > 0)
            {
                if (!RadGridHelper.ValidateAttachmentColumns(e.Item as GridEditableItem))
                    e.Canceled = true;
                else
                {
                    try
                    {
                        int id = GetDBSave_RadGrid().UpdateGridRow(e.Item as Telerik.Web.UI.GridEditableItem);
                        RadGridHelper.UpdateAttachmentColumns(e.Item as GridEditableItem, UpdateTableName, id);
                        RaiseChangesSaved(e.Item as GridEditableItem, id);
                        RebindGrid(sender as RadGrid, id: id);
                    }
                    catch (Exception ex)
                    {
                        if (HandleGridError(e.Item, ex))
                            e.Canceled = true;
                        else
                            throw ex;
                    }
                }
            }
        }

        private void grid_CancelCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            e.Item.Edit = false;
            int id = RadGridHelper.GetGridItemID(e.Item);
            // if cancelling and on an ADDNEW item that was cancelled and we are rowselecting, then select the first row
            if (id == 0 && (sender as RadGrid).ClientSettings.Selecting.AllowRowSelect && (sender as RadGrid).Items.Count > 0)
                id = RadGridHelper.GetGridItemID((sender as RadGrid).Items[0]);
            RebindGrid(sender as RadGrid, id: id);
        }

        /* see: http://docs.telerik.com/devtools/aspnet-ajax/controls/grid/how-to/Filtering/reduce-the-filter-menu-options (not using grid_Init() event because this is too late to "hook" the event up */
        static public void RemoveRangeFilterMenuItems(RadGrid grid)
        {
            GridFilterMenu menu = grid.FilterMenu;
            if (menu != null)
            {
                for (int i = menu.Items.Count - 1; i >= 0; i--)
                {
                    if (menu.Items[i].Text.ToLower().Contains("between"))
                    {
                        menu.Items.RemoveAt(i);
                    }
                }
            }
        }

        public void RebindGrid()
        {
            object id = Grid.SelectedValue;
            RebindGrid(Grid, id: DBHelper.ToInt32(id));
        }
        public void RebindGrid(RadGrid grid, DataTable data = null, int id = 0)
        {
            // do the rebind action
            if (data == null)
                grid.Rebind();
            else
                grid.DataSource = data;

            // http://www.telerik.com/help/aspnet-ajax/grid-persist-selected-rows-on-sorting.html
            // restore the selected values (if appropriate and possible)
            _selID = 0; // default value
            if (grid.ClientSettings.Selecting.AllowRowSelect && id != 0 && grid.MasterTableView.DataKeyNames[0] == "ID")
            {
                _selID = id;
            }
        }

        private DBSave_RadGrid _DBSave_RadGrid = null;
        private DBSave_RadGrid GetDBSave_RadGrid()
        {
            _DBSave_RadGrid = new DBSave_RadGrid(UpdateTableName);
            _DBSave_RadGrid.GetUnboundChangedValues += new GetUnboundChangedValues(this.RaiseGetUnboundChangedValues);
            return _DBSave_RadGrid;
        }

        protected void grid_PreRender(object sender, EventArgs e)
        {
            RadGridHelper.PreRender_Disable_Edit_Add(sender as Telerik.Web.UI.RadGrid);

            if (_selID != 0 && (sender as RadGrid).ClientSettings.Selecting.AllowRowSelect)
            {
                foreach (GridItem item in (sender as RadGrid).Items)
                {
                    if (RadGridHelper.GetGridItemID(item) == _selID)
                    {
                        item.Selected = true;
                        RaiseOnRowSelected(EventArgs.Empty);
                    }
                }
                // reset the id to 0
                _selID = 0;
            }
        }

        protected void grid_ItemCreated(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            RadGridHelper.FixEditLeftMargin(e.Item);
        }

    }
}
