﻿using System;
using System.Web.UI;
using System.Data;
using System.Data.SqlClient;
using System.ComponentModel;
using DispatchCrude.Core;
using AlonsIT;

namespace DispatchCrude.App_Code.Controls
{
    [ParseChildren(true)]
    [PersistChildren(false)]
    public class SingleRowDataHelper : System.Web.UI.UserControl
    {
        [PersistenceMode(PersistenceMode.Attribute), DefaultValue(""), MergableProperty(false)]
        public string ControlToBind { get; set; }
        [PersistenceMode(PersistenceMode.Attribute), DefaultValue(""), MergableProperty(false)]
        public string DataField { get; set; }
        [PersistenceMode(PersistenceMode.Attribute), DefaultValue(""), MergableProperty(false)]
        public string PropertyName { get; set; }
        [PersistenceMode(PersistenceMode.Attribute), DefaultValue(""), MergableProperty(false)]
        public string FormatString { get; set; }
        [PersistenceMode(PersistenceMode.Attribute), DefaultValue(""), MergableProperty(false)]
        public string DefaultValue { get; set; }
    }
}

namespace DispatchCrude.App_Code
{
    // Assigned means use the value, Skip means that the no value should be assigned
    public enum GetControlValueResult { UseValue, Skip };
    public delegate GetControlValueResult GetControlValue(Control ctrl, ref object value);

    public class SingleRowDataBinder
    {
        private string _selectStatement;
        private string _tableName;
        private int _id;
        private DataRow _drow = null;

        public SingleRowDataBinder(string selectStatement, string tableName, int id)
        {
            _selectStatement = selectStatement;
            _tableName = tableName;
            _id = id;
            _drow = GetDataRow();
        }

        public event GetControlValue GetControlValue;

        public void ControlsToDataRow(Control parent)
        {
            ControlsToDataRow(parent, 0);
            using (SSDB ssdb = new SSDB())
            {
                SSDBSave.SaveDataRow(ssdb, _drow, _tableName, "ID", UserSupport.UserName);
            }
        }

        public void DataRowToControls(Control parent)
        {
            System.ComponentModel.TypeConverter tc = new System.ComponentModel.TypeConverter();
            foreach (Control ctrl in parent.Controls)
            {
                App_Code.Controls.SingleRowDataHelper bindingCtrl = ctrl as App_Code.Controls.SingleRowDataHelper;
                Control boundCtrl = null;

                if (bindingCtrl != null
                    && bindingCtrl.ControlToBind.Length > 0
                    && (boundCtrl = parent.FindControl(bindingCtrl.ControlToBind)) != null
                    && bindingCtrl.DataField.Length > 0
                    && _drow.Table.Columns.Contains(bindingCtrl.DataField)
                    && bindingCtrl.PropertyName.Length > 0)
                {
                    UpdateControl(boundCtrl, bindingCtrl.PropertyName, bindingCtrl.DataField, bindingCtrl.DefaultValue, bindingCtrl.FormatString);
                }
                else // recursive call with grandchildren
                    DataRowToControls(ctrl);
            }
        }

        public void UpdateDataRow(Control ctrl, string propName, string dataField)
        {
            Type t = ctrl.GetType();
            var prop = t.GetProperty(propName);
            object value = prop.GetValue(ctrl, null);
            _drow.Table.Columns[dataField].ReadOnly = false;
            if (GetControlValue == null || GetControlValue(ctrl, ref value) == GetControlValueResult.UseValue)
            {
                if (value == null || value.ToString() == "" && _drow.Table.Columns[dataField].AllowDBNull)
                    value = DBNull.Value;
                _drow[dataField] = value;
            }
        }

        public void UpdateDataRow(object value, string dataField)
        {
            _drow[dataField] = value;
        }

        public void SaveDataRow()
        {
            using (SSDB ssdb = new SSDB())
            {
                SSDBSave.SaveDataRow(ssdb, _drow, _tableName, "ID", UserSupport.UserName);
            }
        }

        public void UpdateControl(Control ctrl, string propName, string dataField, string defaultValue = null, string formatString = null)
        {
            DataRow drow = _drow;
            Type t = ctrl.GetType();
            var prop = t.GetProperty(propName);
            object value = drow[dataField];
            if (DBHelper.IsNull(value) && defaultValue != null)
                value = defaultValue;
            if (formatString != null && formatString.Length > 0)
                value = string.Format("{0:" + formatString + "}", value);
            Type fromType = value.GetType(), toType = prop.PropertyType;
            if (fromType == toType
                || toType == Type.GetType("System.Object")
                || ConvertToString(ref value, toType)
                || ConvertNullableTypes(ref value, fromType, toType)
                || ParseValue(ref value, toType)
                || ParseValue(ref value, GetBaseType(toType)))
            {
                prop.SetValue(ctrl, value, null);
            }
        }

        private bool ConvertToString(ref object value, Type toType)
        {
            bool ret = toType == Type.GetType("System.String");
            if (ret)
                value = value.ToString();
            return ret;
        }

        private System.Data.DataRow GetDataRow()
        {
            using (SSDB db = new SSDB())
            {
                using (SqlCommand cmd = db.BuildCommand(_selectStatement))
                {
                    if (_selectStatement.Contains("@ID"))
                        cmd.Parameters.AddWithValue("@ID", _id);
                    DataTable data = SSDB.GetPopulatedDataTable(cmd);
                    return data.Rows.Count == 0 ? data.NewRow() : data.Rows[0];
                }
            }
        }

        private bool IsNullable(Type type)
        {
            return type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>);
        }

        private bool ConvertNullableTypes(ref object value, Type fromType, Type toType)
        {
            bool ret = false;
            if (IsNullable(toType))
            {
                // if TO a Nullable field, then pass DBNull.Value
                if (value == null || value == DBNull.Value)
                {
                    value = Activator.CreateInstance(toType);
                    ret = true;
                }
                else
                    ret = ConvertTypes(ref value, GetBaseType(fromType), GetBaseType(toType));
            }
            else if (value == DBNull.Value)
            {
                value = "";
                ret = true;
            }
            return ret;
        }

        private Type GetBaseType(Type type)
        {
            return IsNullable(type) ? Nullable.GetUnderlyingType(type) : type;
        }

        private bool ConvertTypes(ref object value, Type fromType, Type toType)
        {
            bool ret = false;
            try
            {
                System.ComponentModel.TypeConverter tc = System.ComponentModel.TypeDescriptor.GetConverter(toType);
                if (ret = (tc != null && tc.CanConvertFrom(fromType)))
                    value = tc.ConvertFrom(value);
            }
            catch (Exception)
            {
                //eat the error
                ret = false;
            }
            return ret;
        }

        private bool ParseValue(ref object value, Type toType)
        {
            bool ret = false;
            try
            {
                System.Reflection.MethodInfo m = toType.GetMethod("Parse", new Type[] { typeof(string) });
                if (ret = m != null)
                    value = m.Invoke(null, new object[] { value.ToString() });
            }
            catch (Exception)
            {
                // eat the error
            }
            return ret;
        }

        private void ControlsToDataRow(Control parent, int recurseLevel)
        {
            foreach (Control ctrl in parent.Controls)
            {
                App_Code.Controls.SingleRowDataHelper bindingCtrl = ctrl as App_Code.Controls.SingleRowDataHelper;
                Control boundCtrl = null;

                if (bindingCtrl != null
                    && bindingCtrl.ControlToBind.Length > 0
                    && (boundCtrl = parent.FindControl(bindingCtrl.ControlToBind)) != null
                    && bindingCtrl.DataField.Length > 0
                    && bindingCtrl.PropertyName.Length > 0)
                {
                    UpdateDataRow(boundCtrl, bindingCtrl.PropertyName, bindingCtrl.DataField);
                }
                else // recursive call with grandchildren
                    ControlsToDataRow(ctrl, recurseLevel++);
            }
        }
    }
}