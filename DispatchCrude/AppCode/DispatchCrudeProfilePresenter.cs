﻿using System;
using System.Web.Profile;
using DispatchCrude.Core;

namespace DispatchCrude.App_Code
{
    public class DispatchCrudeProfilePresenter
    {
        public dynamic GetProfile(string username)
        {
            ProfileBase ret = ProfileBase.Create(username, true);
            if (string.IsNullOrEmpty((ret["TimeZoneID"] ?? "").ToString()))
                ret["TimeZoneID"] = ((int)Models.TimeZone.TYPE.CentralStandardTime).ToString();
            ret["UseDST"] = Converter.ToBoolean(ret["UseDST"], true) ? "true" : "false"; // ensure the returned value is truly a BOOL (defaulting to TRUE)
            return ret;
        }
        public dynamic UpdateProfile(string username, string fullName
            , string carrierID, string driverID, string customerID, string producerID, string regionID
            , string timeZoneID, string useDST, string gaugerID, string terminalID)
        {
            ProfileBase prof = GetProfile(username);
            prof.SetPropertyValue("FullName", fullName);
            prof.SetPropertyValue("CarrierID", carrierID);
            prof.SetPropertyValue("DriverID", driverID);
            prof.SetPropertyValue("CustomerID", customerID);
            prof.SetPropertyValue("ProducerID", producerID);
            prof.SetPropertyValue("RegionID", regionID);
            prof.SetPropertyValue("TimeZoneID", timeZoneID);
            prof.SetPropertyValue("UseDST", useDST);
            prof.SetPropertyValue("GaugerID", gaugerID);
            prof.SetPropertyValue("TerminalID", terminalID);
            prof.Save();
            return prof;
        }
    }

}