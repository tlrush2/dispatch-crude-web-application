﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace DispatchCrude.App_Code
{
    static public class SelectListHelper
    {
        public enum ListPosition
        {
            First = 0,
            Last = -1
        }

        static public SelectList Add(this SelectList list, string text = "(None)", string value = "", ListPosition listPosition = ListPosition.First, bool skipNoneIfSelected = false)
        {
            var listItems = list.ToList();
            int lp = (int)listPosition;
            if (lp == (int)ListPosition.Last)
                lp = list.Count();
            bool itemAlreadySelected = false;
            foreach (SelectListItem item in list)
            {
                if (item.Selected)
                {
                    itemAlreadySelected = true;
                    break;
                }
            }
            if (!skipNoneIfSelected || !itemAlreadySelected)
            {
                listItems.Insert(lp, new SelectListItem { Value = value, Text = text, Selected = !itemAlreadySelected });
            }
            list = new SelectList(listItems, "Value", "Text");
            return list;
        }
    }

    public class SuperSelectListItem : SelectListItem
    {
        public SuperSelectListItem()
        { }

        public SuperSelectListItem(string value, string text, string html)
        {
            Text = text;
            Value = value;
            HTMLContent = html ?? text;
        }

        public SuperSelectListItem(string value, string text) :
            this(value, text, text)
        { }

        public string HTMLContent { get; set; }
        public string SortOrder { get; set; }
    }

    public class SuperSelectList : SelectList
    {
        public SuperSelectList(IEnumerable items)
            : base(items)
        { }

        public SuperSelectList(IEnumerable items, object selectedValue)
            : base(items, selectedValue)
        { }

        public SuperSelectList(IEnumerable items, string dataValueField, string dataTextField)
            : base(items, dataValueField, dataTextField)
        { }

        public SuperSelectList(IEnumerable items, string dataValueField, string dataTextField, object selectedValue)
            : base(items, dataValueField, dataTextField, selectedValue)
        { }

    }
}