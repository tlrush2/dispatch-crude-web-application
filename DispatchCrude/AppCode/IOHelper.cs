﻿using System.IO;
using System.Text.RegularExpressions;

namespace DispatchCrude.App_Code
{
    public class IOHelper
    {
        static public string MakeValidFileName(string filename)
        {
            string regexSearch = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());
            Regex r = new Regex(string.Format("[{0}]", Regex.Escape(regexSearch)));
            return r.Replace(filename, "_");
        }
    }
}