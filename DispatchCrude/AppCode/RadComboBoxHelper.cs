﻿using System;
using Telerik.Web.UI;
using DispatchCrude.Core;

namespace DispatchCrude.App_Code
{
    static public class RadComboBoxHelper
    {
        static public int SelectedValue(RadComboBox rcb, int missingValue = 0)
        {
            return !rcb.EnableAutomaticLoadOnDemand && rcb.SelectedIndex == -1 ? missingValue : Converter.ToInt32(rcb.SelectedValue ?? missingValue.ToString());
        }
        static public object SelectedDbValue(RadComboBox rcb)
        {
            return !rcb.EnableAutomaticLoadOnDemand && rcb.SelectedIndex == -1 || SelectedValue(rcb) == 0 ? DBNull.Value : (object)rcb.SelectedValue;
        }

        static public int SetSelectedValue(RadComboBox rcb, object value, bool defaultToSingleton = false)
        {
            string strValue = (value ?? (object)"").ToString();
            int selIndex = -1; // default value
            RadComboBoxItem li = rcb.Items.FindItemByValue(strValue);
            if (li == null)
            {
                if (defaultToSingleton && rcb.Items.Count == 1)
                    li = rcb.Items[0];
                else if (defaultToSingleton && (rcb.Items.Count == 2 && rcb.Items[0].Text.StartsWith("(")))
                    li = rcb.Items[1];
                else
                    li = rcb.Items.FindItemByValue("0");
            }

            if (li != null)
            {
                if (rcb.EnableAutomaticLoadOnDemand)
                    rcb.Text = li.Text;
                else
                {
                    li.Selected = true;
                    rcb.SelectedValue = li.Value;
                }
                selIndex = li.Index;
            }
            else if (rcb.EnableAutomaticLoadOnDemand)
                rcb.Text = string.Empty;
            else
                rcb.SelectedIndex = selIndex = -1;

            return rcb.SelectedIndex;
        }
        static public void Rebind(RadComboBox rcb, bool defaultToSingleton = false)
        {
            Rebind(rcb, SelectedValue(rcb), defaultToSingleton);
        }
        static public void Rebind(RadComboBox rcb, object selectedValue, bool defaultToSingleton = false)
        {
            object currentID = null;
            if (!rcb.CheckBoxes)
            {
                currentID = selectedValue;
                rcb.SelectedIndex = -1;
            }
            if (rcb.DataSourceID.Length > 0)
            {
                rcb.DataBind();
            }
            if (!rcb.CheckBoxes)
            {
                SetSelectedValue(rcb, (currentID ?? 0).ToString(), defaultToSingleton);
            }
        }
    }
}