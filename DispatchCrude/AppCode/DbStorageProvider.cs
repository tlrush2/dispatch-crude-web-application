﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Telerik.Web.UI.PersistenceFramework;
using AlonsIT;
using DispatchCrude.Core;

namespace DispatchCrude.App_Code
{
    public class DbStorageProvider : IStateStorageProvider
    {
        public string LoadStateFromStorage(string key)
        {
            using (SSDB db = new SSDB())
            {
                return (db.QuerySingleValue("SELECT Value FROM tblUserPersistedValue WHERE UserName = {0} AND [Key] = {1}", DBHelper.QuoteStr(UserSupport.UserName), DBHelper.QuoteStr(key)) ?? "").ToString();
            }
        }

        public void SaveStateToStorage(string key, string serializedState)
        {
            using (SSDB db = new SSDB())
            {
                db.ExecuteSql("DELETE FROM tblUserPersistedValue WHERE UserName = {0} AND [Key] = {1}; INSERT INTO tblUserPersistedValue (UserName, [Key], Value) VALUES ({0}, {1}, {2})"
                    , DBHelper.QuoteStr(UserSupport.UserName), DBHelper.QuoteStr(key), DBHelper.QuoteStr(serializedState));
            }
        }

        static public bool HasStateInStorage(string key)
        {
            using (SSDB db = new SSDB())
            {
                return !DBHelper.IsNull(db.QuerySingleValue("SELECT Value FROM tblUserPersistedValue WHERE UserName = {0} AND [Key] = {1}", DBHelper.QuoteStr(UserSupport.UserName), DBHelper.QuoteStr(key)));
            }
        }
        static public void ResetStateInStorage(string key)
        {
            using (SSDB db = new SSDB())
            {
                db.ExecuteSql("DELETE FROM tblUserPersistedValue WHERE UserName = {0} AND [Key] = {1}"
                    , DBHelper.QuoteStr(UserSupport.UserName), DBHelper.QuoteStr(key));
            }
        }
    }
}