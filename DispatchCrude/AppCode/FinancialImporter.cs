﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using Telerik.Web.UI;
using DispatchCrude.App_Code.Controls;
using DispatchCrude.Core;
using Syncfusion.XlsIO;
using AlonsIT;

namespace DispatchCrude.App_Code
{
    public class FinancialImporter
    {
        public event GridErrorMessage GetErrorMessage;
        private void RaiseGetErrorMessage(ref string error)
        {
            error = DBSupport.TranslateErrorMessage(error);
            if (GetErrorMessage != null) GetErrorMessage(ref error);
        }

        public class FISpec
        {
            public enum FISType { ID = 1, NEW = 2, UPDATE = 4, BOTH = NEW | UPDATE, OUTCOME = 8, USERNAME = 16, NOW = 32, ACTION = 64 }
            public FISType Type { get; set; }
            public Type DataType { get; set; }
            public string FieldName { get; set; }
            public Dictionary<string, string> Data { get; protected set; }
            public string LowerCaption { get; set; }
            public int SheetCol { get; set; }
            public object GetValue(object val)
            {
                string lVal = null;
                if (Type.HasFlag(FISType.NOW))
                    val = DateTime.UtcNow.ToString("M/d/yyyy HH:mm:ss");
                else if (Type.HasFlag(FISType.USERNAME))
                    val = UserSupport.UserName;
                else if (Data != null && Data.ContainsKey((lVal = val.ToString().ToLower())))
                {
                    val = Data.First(x => x.Key == lVal).Value;
                    if (val != null && val.ToString() == "0") val = "NULL";
                }
                else if (DataType == typeof(DateTime))
                {
                    DateTime dVal = DBHelper.ToDateTime(val);
                    if (DBHelper.ToDateTime(val) == new DateTime())
                        val = "NULL";
                    else
                        val = DBHelper.ToDateTime(val).ToShortDateString();
                }
                return val;
            }

            public FISpec() { }
            public FISpec(RadGrid grid, string colUniqueName, FISType type)
            {
                GridColumn col = grid.Columns.FindByUniqueNameSafe(colUniqueName);

                bool isValidDropDownCol = col is GridDropDownColumn && !string.IsNullOrEmpty((col as GridDropDownColumn).DataSourceID);

                FISpecInt(type
                    , col.DataType
                    , col.GetType().GetProperty("DataField").GetValue(col, null).ToString()
                    , col.HeaderText.ToLower()
                    , (isValidDropDownCol ? PageHelper.FindControlRecursive(col.Owner.OwnerGrid.NamingContainer, (col as GridDropDownColumn).DataSourceID) : null) as DBDataSource
                    , isValidDropDownCol ? (col as GridDropDownColumn).ListValueField : null
                    , isValidDropDownCol ? (col as GridDropDownColumn).ListTextField : null);
            }
            public FISpec(string fieldName, Type dataType, FISType type, string gridCaption = null, DBDataSource dbDataSource = null, string dataValueField = null, string dataTextField = null)
            {
                FISpecInt(type, dataType, fieldName, gridCaption, dbDataSource, dataValueField, dataTextField);
            }
            private void FISpecInt(FISType type, Type dataType, string fieldName, string gridCaption = null, DBDataSource dbDataSource = null, string dataValueField = null, string dataTextField = null)
            {
                Type = type;
                DataType = dataType;
                FieldName = fieldName;
                LowerCaption = gridCaption == null ? null : gridCaption.ToLower();
                if (dbDataSource != null && dataValueField != null && dataTextField != null)
                {
                    Data = new Dictionary<string, string>();
                    System.Data.DataView dvData = dbDataSource.Select() as System.Data.DataView;
                    foreach (System.Data.DataRowView drv in dvData)
                    {
                        Data[drv[dataTextField].ToString().ToLower()] = drv[dataValueField].ToString();
                    }
                }
            }
        }
        public class FISpecs : List<FISpec> { }

        public FISpecs Specs { get; set; }
        public FinancialImporter()
        {
            Specs = new FISpecs();
        }
        public void AddSpec(RadGrid grid, string colUniqueName, FISpec.FISType type)
        {
            Specs.Add(new FISpec(grid, colUniqueName, type));
        }
        public void AddSpec(string fieldName, Type dataType, FISpec.FISType type)
        {
            Specs.Add(new FISpec(fieldName, dataType, type));
        }

        // do the actual import here
        public MemoryStream ProcessSP(Stream excelStream, string storedProcedureName)
        {
            // instantiate an excel document
            using (ExcelEngine excel = new ExcelEngine())
            {
                IWorkbook wb = excel.Excel.Workbooks.Open(excelStream);
                IWorksheet sheet = wb.Worksheets[0];
                for (int i = 1; i < sheet.UsedRange.Columns.Length + 1; i++)
                {
                    FISpec spec = Specs.Find(sp => sp.LowerCaption == (sheet.Range[1, i].Text ?? "").ToLower());
                    if (spec != null)
                        spec.SheetCol = i;
                }
                if (Specs.Count(sp => sp.SheetCol != 0) > 0)
                {
                    using (SSDB db = new SSDB())
                    {
                        using (System.Data.SqlClient.SqlCommand cmd = db.BuildCommand(storedProcedureName))
                        {
                            for (int r = 2; r < sheet.UsedRange.Rows.Length + 1; r++)
                            {
                                string actionResult = "Skipped";
                                Color outcomeColor = Color.LightGreen;
                                try
                                {
                                    string action = sheet.Range[r, Specs.First(sp => sp.Type.HasFlag(FISpec.FISType.ACTION)).SheetCol].Value.ToString();
                                    switch (action)
                                    {
                                        case "None":
                                            break;
                                        default:
                                            foreach (FISpec spec in Specs)
                                            {
                                                if (spec.SheetCol != 0 && cmd.Parameters.Contains("@" + spec.FieldName))
                                                {
                                                    object value = sheet.Range[r, spec.SheetCol].Value.Trim();
                                                    if (value.ToString().Length == 0) value = DBNull.Value;
                                                    cmd.Parameters["@" + spec.FieldName].Value = value;
                                                }
                                            }
                                            if (cmd.Parameters.Contains("@UserName")) cmd.Parameters["@UserName"].Value = UserSupport.UserName;
                                            if (cmd.ExecuteNonQuery() == 0)
                                            {
                                                actionResult = "Skipped - record not found";
                                                outcomeColor = Color.Red;
                                            }
                                            else if (action.Substring(action.Length - 1, 1).ToLower() == "e")
                                                actionResult = action + "d";
                                            else
                                                actionResult = action + "ed";
                                            break;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    string msg = ex.Message;
                                    RaiseGetErrorMessage(ref msg);
                                    actionResult = "Failure: " + msg;
                                    outcomeColor = Color.Red;

                                }
                                IRange rng = sheet.Range[r, Specs.Find(sp => sp.Type == FISpec.FISType.OUTCOME).SheetCol];
                                rng.Text = actionResult;
                                rng.CellStyle.Color = outcomeColor;
                                rng.AutofitColumns();
                            }
                        }
                    }
                }
                MemoryStream ms = new MemoryStream();
                wb.SaveAs(ms);
                return ms;
            }
        }

        // do the actual import here
        public MemoryStream ProcessSql(Stream excelStream, string updateTableName)
        {
            // instantiate an excel document
            using (ExcelEngine excel = new ExcelEngine())
            {
                IWorkbook wb = excel.Excel.Workbooks.Open(excelStream);
                IWorksheet sheet = wb.Worksheets[0];
                for (int i = 1; i < sheet.UsedRange.Columns.Length + 1; i++)
                {
                    FISpec spec = Specs.Find(sp => sp.LowerCaption == (sheet.Range[1, i].Text ?? "").ToLower());
                    if (spec != null)
                        spec.SheetCol = i;
                }
                if (Specs.Count(sp => sp.SheetCol == 0 && sp.LowerCaption != null) == 0)
                {
                    using (SSDB db = new SSDB())
                    {
                        for (int r = 2; r < sheet.UsedRange.Rows.Length + 1; r++)
                        {
                            try
                            {
                                string sql = null, action = null, actionResult = "Skipped";
                                action = sheet.Range[r, Specs.First(sp => sp.Type.HasFlag(FISpec.FISType.ACTION)).SheetCol].Value.ToString();
                                int id = DBHelper.ToInt32(sheet.Range[r, Specs.First(spec => spec.Type == FISpec.FISType.ID).SheetCol].Value);
                                if (action.Equals("Update", StringComparison.CurrentCultureIgnoreCase) &&  id > 0)
                                {
                                    actionResult = "Updated";
                                    sql = string.Format("UPDATE {0} SET", updateTableName);
                                    bool pastFirst = false;
                                    foreach (FISpec spec in Specs.FindAll(spec => spec.Type.HasFlag(FISpec.FISType.UPDATE)))
                                    {
                                        object rawValue = spec.SheetCol == 0 ? null : sheet.Range[r, spec.SheetCol].Value;
                                        if (rawValue is string && string.IsNullOrEmpty(rawValue.ToString().Trim())) rawValue = "NULL";
                                        sql += string.Format("{0}{1}={2}", !pastFirst ? " " : ",", spec.FieldName, DBHelper.QuoteStr(spec.GetValue(rawValue)));
                                        pastFirst = true;
                                    }
                                    sql += string.Format(" WHERE ID={0}", id);
                                }
                                else if (action.Equals("Add", StringComparison.CurrentCultureIgnoreCase))
                                {
                                    actionResult = "Added";
                                    sql = string.Format("INSERT INTO {0} ", updateTableName);
                                    string fieldList = string.Empty, valueList = string.Empty;
                                    bool pastFirst = false;
                                    foreach (FISpec spec in Specs.FindAll(spec => spec.Type.HasFlag(FISpec.FISType.NEW)))
                                    {
                                        fieldList += string.Format("{0}{1}", !pastFirst ? "" : ", ", spec.FieldName);
                                        object rawValue = spec.SheetCol == 0 ? null : sheet.Range[r, spec.SheetCol].Value;
                                        if (rawValue is string && string.IsNullOrEmpty(rawValue.ToString().Trim())) rawValue = "NULL";
                                        valueList += string.Format("{0}{1}", !pastFirst ? "" : ", ", DBHelper.QuoteStr(spec.GetValue(rawValue)));
                                        pastFirst = true;
                                    }
                                    sql += string.Format("({0}) VALUES ({1})", fieldList, valueList);
                                }
                                else if (action.Equals("Delete", StringComparison.CurrentCultureIgnoreCase))
                                {
                                    if (id == 0)
                                        actionResult = "Skipped";
                                    else
                                    {
                                        actionResult = "Deleted";
                                        sql = string.Format("DELETE FROM {0} WHERE ID = {1}", updateTableName, id);
                                    }
                                }
                                if (!string.IsNullOrEmpty(sql))
                                    db.ExecuteSql(sql);
                                IRange rng = sheet.Range[r, Specs.Find(sp => sp.Type == FISpec.FISType.OUTCOME).SheetCol];
                                rng.Text = actionResult;
                                rng.CellStyle.Color = System.Drawing.Color.LightGreen;
                            }
                            catch (Exception ex)
                            {
                                string msg = ex.Message;
                                RaiseGetErrorMessage(ref msg);
                                IRange eRng = sheet.Range[r, Specs.Find(sp => sp.Type == FISpec.FISType.OUTCOME).SheetCol];
                                eRng.Text = "Failure: " + msg;
                                eRng.CellStyle.Color = System.Drawing.Color.Red;
                            }
                        }
                    }
                }
                MemoryStream ms = new MemoryStream();
                wb.SaveAs(ms);
                return ms;
            }
        }
    }
}