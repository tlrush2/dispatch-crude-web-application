﻿using System;
using System.Data;
using System.Data.SqlClient;
using DispatchCrude.Extensions;
using AlonsIT;

namespace DispatchCrude.App_Code
{
    class DispatchCrude_ConnFactory : IConnectionFactory
    {
        static private string DATACONNECTIONSTRING = "DispatchCrudeDB";
        private int _cmdTimeout;

        public DispatchCrude_ConnFactory()
        {
            _cmdTimeout = 120;
        }
        public override int CmdTimeout
        {
            get
            {
                return _cmdTimeout;
            }
            set
            {
                _cmdTimeout = value;
            }
        }
        public override System.Data.SqlClient.SqlConnection Connect(bool reset = false)
        {
            SqlConnection ret = new SqlConnection(GetConnectionString(DATACONNECTIONSTRING));
            ret.Open();
            return ret;
        }
        static private string GetConnectionString(string connectionStringName)
        {
            string ret = string.Empty;

            System.Configuration.ConnectionStringSettings connectionStringSettings = System.Configuration.ConfigurationManager.ConnectionStrings[connectionStringName];

            if (connectionStringSettings != null && !string.IsNullOrWhiteSpace(connectionStringSettings.ConnectionString))
                ret = connectionStringSettings.ConnectionString.Trim();
            return ret;
        }
        static public object QuerySingleValue(string sql, params object[] parameters)
        {
            using (SSDB ssdb = new SSDB())
            {
                return ssdb.QuerySingleValue(sql, parameters);
            }
        }

        static public DataTable AddActiveColumn(System.Data.DataTable data)
        {
            bool hasDelete = data.Columns.Contains("DeleteDate")
                , hasDeleteUTC = data.Columns.Contains("DeleteDateUTC")
                , hasActive = data.Columns.Contains("Active");
            if ((hasDelete || hasDeleteUTC) && !hasActive)
            {
                data.Columns.Add(new System.Data.DataColumn("Active", typeof(bool)));
                foreach (System.Data.DataRow row in data.Rows)
                {
                    row["Active"] = DBHelper.IsNull(row[hasDelete ? "DeleteDate" : "DeleteDateUTC"]);
                    row.AcceptChanges();
                }
            }
            return data;
        }

        static public bool IsIntegerType(Type dataType)
        {
            return dataType.IsIn(Type.GetType("System.Int32"), Type.GetType("System.Int16"), Type.GetType("System.Byte"));
        }

        static public DataTable SetIDNullsToZero(DataTable dt)
        {
            foreach (DataRow row in dt.Rows)
            {
                foreach (DataColumn col in dt.Columns)
                {
                    if (col.ColumnName.EndsWith("ID") && IsIntegerType(col.DataType) && DBHelper.IsNull(row[col]))
                        row[col] = 0;
                }
                row.AcceptChanges();
            }
            return dt;
        }
    }

    public class DBSupport
    {
        // public static method that translates common db logic returned error messages into a user-friendly message
        static public string TranslateErrorMessage(string error)
        {
            if (error.Contains("Cannot insert duplicate key row") && new System.Text.RegularExpressions.Regex("udx.*_Main").IsMatch(error))
                error = "Overlapping/duplicate records are not allowed";
            error = error.Replace("The transaction ended in the trigger. The batch has been aborted.", "");
            return error;
        }

    }
}