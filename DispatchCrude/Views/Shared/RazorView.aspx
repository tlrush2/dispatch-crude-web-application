﻿<%------------------------------------------------------------------------------------------------%>
<%-- Generates MVC pages using the old web forms template                                       --%>
<%------------------------------------------------------------------------------------------------%>
<%@ Page Title="" Language="C#" MasterPageFile="~/MVC.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>
<asp:Content ID="contentHeader" ContentPlaceHolderID="HeadContent" runat="server">
    <%--see http://web.archive.org/web/20120526174840/http://www.eworldui.net/blog/post/2011/01/07/Using-Razor-Pages-with-WebForms-Master-Pages.aspx--%>
    <link href="/Content/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/styles/telerik.mvc/kendo.common.min.css?version=2017.1.118" rel="stylesheet" type="text/css" />
    <link href="/styles/telerik.mvc/kendo.dataviz.min.css?version=2017.1.118" rel="stylesheet" type="text/css" />
    <link href="/styles/telerik.mvc/kendo.blueopal.min.css?version=2017.1.118" rel="stylesheet" type="text/css" />
    <link href="/styles/telerik.mvc/kendo.rtl.min.css?version=2017.1.118" rel="stylesheet" type="text/css" />
    <link href="/styles/telerik.mvc/kendo.dataviz.default.min.css?version=2017.1.118" rel="stylesheet" type="text/css" />
    <link href="/styles/telerik.mvc/kendo.mobile.all.min.css?version=2017.1.118" rel="stylesheet" type="text/css" />
    
    <%
        Response.Write("<script type='text/javascript' src='/scripts/beyondadmin/jquery.min.js'></script>");
        Response.Write("<script type='text/javascript' src='/scripts/jquery-migrate-1.2.1.min.js'></script>");
    %>
    <script type="text/javascript" src="/scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="/scripts/telerik.mvc/kendo.web.min.js?version=2017.1.118"></script>
    <script type="text/javascript" src="/scripts/telerik.mvc/kendo.aspnetmvc.min.js?version=2017.1.118"></script>
    <script type="text/javascript" src='/scripts/telerik.mvc/kendo.modernizr.custom.js'></script>
    <script type="text/javascript" src='/scripts/moment.js'></script>
    <script type="text/javascript" src='/scripts/MainFormSize.js?version=1.14'></script>
    <script type="text/javascript" src='/scripts/toDictionary.js'></script>
    <%--<script type="text/javascript" src='@Url.Content("~/scripts/kendo.modernizr.custom.js")'></script>--%>
</asp:Content>
<asp:Content ID="mainContent" ContentPlaceHolderID="MainContent" runat="server">
    <% { Response.Write("<input id='hfPageTitle' type='hidden' value='" + ViewBag.Title + "' />"); } %>
    <div class="headerdivider" ></div>
    <div id="content" runat="server" class="main mainContentX" >
        <% Html.RenderPartial((string) ViewBag._ViewName); %>
        <script type="text/javascript" >
            $(document).ready(function () {
                setTimeout(sizeContentReady, 50);
                //Every resize of window
                $(window).resize(resizeContent);

                // set the Page Title on the Root.Master from the page title stored in the HiddenField element
                $("#lblRootMasterPageTitle").text($("#hfPageTitle").val());
            });
        </script>
    </div>
</asp:Content>
