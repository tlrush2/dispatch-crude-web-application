﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Data.Entity;
using System.Web;
using System.IO;
using PagedList;
using DispatchCrude.Models;
using DispatchCrude.ViewModels;
using DispatchCrude.Core;
using AlonsIT;
using Ionic.Zip;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;
using System.Data;
using System.Linq.Dynamic;
using DispatchCrude.App_Code;
using DispatchCrude.Extensions;

namespace DispatchCrude.Controllers
{
    /**************************************************************************************************/
    /**  CONTORLLER: Orders                                                                          **/
    /**  DESCRIPTION: Anyone with viewOrders access can view the readonly pages.  Updates have       **/
    /**      additional access roles that allow a user to execute a specific task (i.e.              **/
    /**      transferOrders, rejectOrders) but editOrders has the ability to run any function.       **/
    /**************************************************************************************************/

    [Authorize]
    public class OrdersController : Controller
    {
        static public string SESSION_NAME_ORDERIDLIST = "OrderIDList";

        private DispatchCrudeDB db = new DispatchCrudeDB();


        /**************************************************************************************************/
        /**  PAGE: ~/Orders/                                                                             **/
        /**        ~/Orders/Index/                                                                       **/
        /**  DESCRIPTION: Dispalys a list of all orders based on permissions.  Orders can be ordered     **/
        /**      by clicking the column headers, and the quick search box at the top searches order #,   **/
        /**      origin or destination BOL #, origin or destination name, shipper PO, or driver.         **/
        /**      Advanced searches should use the search screen at left (Click button to expand or       **/
        /**      ~/Orders/Search/).  Other pages can define a filter (Dispatch View, Audited) and use    **/
        /**      this page to generate a filtered list.                                                  **/
        /**************************************************************************************************/
        [Authorize(Roles = "viewOrders, searchOrders")]
        public ActionResult Index(OrderSearchViewModel search)
        {
            ViewBag.Title = "Order List";

            ViewBag.Statuses = new MultiSelectList(db.OrderStatus, "ID", "Name", search.Statuses);
            ViewBag.TicketTypeID = new SelectList(db.TicketTypes, "ID", "Name", search.TicketTypeID);
            ViewBag.PriorityID = new SelectList(db.Priorities, "ID", "Name", search.PriorityID);
            ViewBag.ProductID = new SelectList(db.Products.OrderBy(p => p.Name), "ID", "Name", search.ProductID);

            ViewBag.OrderNumSort = search.Sort == "ordernum" ? "ordernum_desc" : "";
            ViewBag.StatusSort = search.Sort == "status" ? "status_desc" : "status";
            ViewBag.PrioritySort = search.Sort == "priority" ? "priority_desc" : "priority";
            ViewBag.DueDateSort = search.Sort == "duedate" ? "duedate_desc" : "duedate";
            ViewBag.ProductSort = search.Sort == "product" ? "product_desc" : "product";
            ViewBag.DispatchConfirmNumSort = search.Sort == "dispatchconfirmnum" ? "dispatchconfirmnum_desc" : "dispatchconfirmnum";
            ViewBag.JobNumberSort = search.Sort == "jobnumber" ? "jobnumber_desc" : "jobnumber";
            ViewBag.ContractNumberSort = search.Sort == "contractnumber" ? "contractnumber_desc" : "contractnumber";
            ViewBag.OriginStateSort = search.Sort == "originstate" ? "originstate_desc" : "originstate";
            ViewBag.OriginSort = search.Sort == "origin" ? "origin_desc" : "origin";
            ViewBag.TankNumSort = search.Sort == "tanknum" ? "tanknum_desc" : "tanknum";
            ViewBag.BOLNumSort = search.Sort == "bolnum" ? "bolnum_desc" : "bolnum";
            ViewBag.DestinationSort = search.Sort == "destination" ? "destination_desc" : "destination";
            ViewBag.CarrierSort = search.Sort == "carrier" ? "carrier_desc" : "carrier";
            ViewBag.DriverSort = search.Sort == "driver" ? "driver_desc" : "driver";
            ViewBag.TruckSort = search.Sort == "truck" ? "truck_desc" : "truck";
            ViewBag.TrailerSort = search.Sort == "trailer" ? "trailer_desc" : "trailer";
            ViewBag.Trailer2Sort = search.Sort == "trailer2" ? "trailer2_desc" : "trailer2";
            ViewBag.TicketTypeSort = search.Sort == "tickettype" ? "tickettype_desc" : "tickettype";

            // Filter orders assigned to me or that use my carrier
            int myDriverID = Converter.ToInt32(Profile.GetPropertyValue("DriverID"));
            int myCarrierID = Converter.ToInt32(Profile.GetPropertyValue("CarrierID"));
            IQueryable<Order> orders = db.Orders.Where(o => myCarrierID == -1 || o.CarrierID == myCarrierID || o.DriverID == myDriverID);

            // filter orders by terminal
            int myTerminalID = Converter.ToInt32(Profile.GetPropertyValue("TerminalID"));
            orders = orders.Where(o => myTerminalID <= 0 // no terminal filtering
                                     ||     (o.Origin == null || o.Origin.TerminalID == null || o.Origin.TerminalID == myTerminalID)
                                         && (o.Destination == null || o.Destination.TerminalID == null || o.Destination.TerminalID == myTerminalID)
                                         && (o.Driver == null || o.Driver.TerminalID == null || o.Driver.TerminalID == myTerminalID));


            if (search.QuickSearch != null)
            {
                // search several fields using the quicksearch string (match any, so use OR)
                orders = orders.Where(o => (o.OrderNum.ToString() == search.QuickSearch)
                                           || (o.OriginBOLNum == search.QuickSearch)
                                           || (o.DispatchConfirmNum == search.QuickSearch)
                                           || (o.DestBOLNum == search.QuickSearch)
                                           || (o.JobNumber == search.QuickSearch)
                                           || (o.Contract.ContractNumber == search.QuickSearch)
                                        );
            }
            else
            {
                // filter list based on search strings (match all, so use AND)
                orders = orders.Where(o => (search.OrderNum == null || o.OrderNum.ToString().Contains(search.OrderNum))
                                           && (search.Statuses.Count == 0 || search.Statuses.Contains(o.StatusID))
                                           && (search.TicketTypeID == null || o.TicketTypeID == search.TicketTypeID)
                                           && (search.PriorityID == null || o.PriorityID == search.PriorityID)
                                           && (search.ProductID == null || o.ProductID == search.ProductID)
                                           && (search.Rejected == null || o.Rejected == search.Rejected)
                                           && (search.Origin == null || o.Origin.Name.Contains(search.Origin) || o.Origin.LeaseNum.Contains(search.Origin))
                                           && (search.OriginTank == null || o.OriginTank.TankNum.Contains(search.OriginTank))
                                           && (search.OriginState == null || o.Origin.State.Abbreviation.Contains(search.OriginState))
                                           && (search.OriginWait == null || (o.OriginWaitReasonID != null) == search.OriginWait)
                                           && (search.OriginBOLNum == null || o.OriginBOLNum.Contains(search.OriginBOLNum))
                                           && (search.DispatchConfirmNum == null || o.DispatchConfirmNum.Contains(search.DispatchConfirmNum))
                                           && (search.Destination == null || o.Destination.Name.Contains(search.Destination))
                                           && (search.Consignee == null || o.Consignee.Name.Contains(search.Consignee))
                                           && (search.DestWait == null || (o.DestWaitReasonID != null) == search.DestWait)
                                           && (search.DestState == null || o.Destination.State.Abbreviation.Contains(search.DestState))
                                           && (search.DestBOLNum == null || o.DestBOLNum.Contains(search.DestBOLNum))
                                           && (search.Driver == null || (o.Driver.FirstName + " " + o.Driver.LastName).Contains(search.Driver))
                                           && (search.Truck == null || o.Truck.IDNumber.Contains(search.Truck))
                                           && (search.Trailer == null || o.Trailer.IDNumber.Contains(search.Trailer) || o.Trailer2.IDNumber.Contains(search.Trailer))
                                           && (search.Operator == null || o.Operator.Name.Contains(search.Operator))
                                           && (search.Carrier == null || o.Carrier.Name.Contains(search.Carrier))
                                           && (search.Producer == null || o.Producer.Name.Contains(search.Producer))
                                           && (search.Customer == null || o.Customer.Name.Contains(search.Customer))
                                           && (search.JobNumber == null || o.JobNumber.Contains(search.JobNumber))
                                           && (search.ContractNumber == null || o.Contract.ContractNumber.Contains(search.ContractNumber))
                                           && (search.Pumper == null || (o.Pumper.FirstName + " " + o.Pumper.LastName).Contains(search.Pumper))
                                           && (search.DueDate == null || o.DueDate == search.DueDate)
                                           && (search.OrderDate == null || o.OrderDate == search.OrderDate)
                                           && (search.Transferred == null || (o.OrderTransfer != null) == search.Transferred)
                                           && (search.Rerouted == null || (o.OrderReroutes.Count > 0) == search.Rerouted)
                                           && (search.Deleted == null || (o.DeleteDateUTC != null) == search.Deleted)
                                        );
            }

            switch (search.Sort)
            {
                case "ordernum_desc":
                    orders = orders.OrderByDescending(o => o.OrderNum);
                    break;
                case "status":
                    orders = orders.OrderBy(o => o.Status.Name);
                    break;
                case "status_desc":
                    orders = orders.OrderByDescending(o => o.Status.Name);
                    break;
                case "priority":
                    orders = orders.OrderBy(o => o.Priority.PriorityNum);
                    break;
                case "priority_desc":
                    orders = orders.OrderByDescending(o => o.Priority.PriorityNum);
                    break;
                case "duedate":
                    orders = orders.OrderBy(o => o.DueDate);
                    break;
                case "duedate_desc":
                    orders = orders.OrderByDescending(o => o.DueDate);
                    break;
                case "product":
                    orders = orders.OrderBy(o => o.Product.Name);
                    break;
                case "product_desc":
                    orders = orders.OrderByDescending(o => o.Product.Name);
                    break;
                case "dispatchconfirmnum":
                    orders = orders.OrderBy(o => o.DispatchConfirmNum);
                    break;
                case "dispatchconfirmnum_desc":
                    orders = orders.OrderByDescending(o => o.DispatchConfirmNum);
                    break;
                case "jobnumber":
                    orders = orders.OrderBy(o => o.JobNumber);
                    break;
                case "jobnumber_desc":
                    orders = orders.OrderByDescending(o => o.JobNumber);
                    break;
                case "contractnumber":
                    orders = orders.OrderBy(o => o.Contract.ContractNumber);
                    break;
                case "contractnumber_desc":
                    orders = orders.OrderByDescending(o => o.Contract.ContractNumber);
                    break;
                case "originstate":
                    orders = orders.OrderBy(o => o.Origin.State.Abbreviation);
                    break;
                case "originstate_desc":
                    orders = orders.OrderByDescending(o => o.Origin.State.Abbreviation);
                    break;
                case "origin":
                    orders = orders.OrderBy(o => o.Origin.Name);
                    break;
                case "origin_desc":
                    orders = orders.OrderByDescending(o => o.Origin.Name);
                    break;
                case "tanknum":
                    orders = orders.OrderBy(o => o.OriginTank.TankNum);
                    break;
                case "tanknum_desc":
                    orders = orders.OrderByDescending(o => o.OriginTank.TankNum);
                    break;
                case "bolnum":
                    orders = orders.OrderBy(o => o.OriginBOLNum);
                    break;
                case "bolnum_desc":
                    orders = orders.OrderByDescending(o => o.OriginBOLNum);
                    break;
                case "destination":
                    orders = orders.OrderBy(o => o.Destination.Name);
                    break;
                case "destination_desc":
                    orders = orders.OrderByDescending(o => o.Destination.Name);
                    break;
                case "carrier":
                    orders = orders.OrderBy(o => o.Carrier.Name);
                    break;
                case "carrier_desc":
                    orders = orders.OrderByDescending(o => o.Carrier.Name);
                    break;
                case "driver":
                    orders = orders.OrderBy(o => o.Driver.LastName).ThenBy(o => o.Driver.FirstName);
                    break;
                case "driver_desc":
                    orders = orders.OrderByDescending(o => o.Driver.LastName).ThenByDescending(o => o.Driver.FirstName);
                    break;
                case "truck":
                    orders = orders.OrderBy(o => o.Truck.IDNumber);
                    break;
                case "truck_desc":
                    orders = orders.OrderByDescending(o => o.Truck.IDNumber);
                    break;
                case "trailer":
                    orders = orders.OrderBy(o => o.Trailer.IDNumber);
                    break;
                case "trailer_desc":
                    orders = orders.OrderByDescending(o => o.Trailer.IDNumber);
                    break;
                case "trailer2":
                    orders = orders.OrderBy(o => o.Trailer2.IDNumber);
                    break;
                case "trailer2_desc":
                    orders = orders.OrderByDescending(o => o.Trailer2.IDNumber);
                    break;
                case "tickettype":
                    orders = orders.OrderBy(o => o.TicketType.Name);
                    break;
                case "tickettype_desc":
                    orders = orders.OrderByDescending(o => o.TicketType.Name);
                    break;
                default:
                    orders = orders.OrderBy(s => s.OrderNum);
                    break;
            }

            search.Orders = orders.ToPagedList(search.Page, Settings.DefaultPageSize); // Cap pages at 500

            return View(search);
        }


        /**************************************************************************************************/
        /**  PAGE: ~/Orders/Search                                                                       **/
        /**  DESCRIPTION: Search for orders based on a variety of fields.  Other pages default here      **/
        //**     when an order is not found                                                              **/
        /**************************************************************************************************/
        [Authorize(Roles = "searchOrders")]
        public ActionResult Search()
        {
            OrderSearchViewModel osv = new OrderSearchViewModel();
            osv.HideSearch = false;

            return RedirectToAction("Index", osv.RouteValues());
        }

        [Authorize(Roles = "Dispatch3P")]
        public ActionResult Dispatch3P()
        {
            /*
            OrderSearchViewModel osv = new OrderSearchViewModel();
            osv.Statuses = new List<int> { (int)OrderStatus.STATUS.Assigned, (int)OrderStatus.STATUS.Dispatched, (int)OrderStatus.STATUS.Accepted, (int)OrderStatus.STATUS.PickedUp };

            return RedirectToAction("Index", osv.RouteValues());
            */
            ViewBag.Title = "Dispatch";
            ViewBag.SubTitle = "Grid 3P";
            ViewBag.Action = "Dispatch3P";

            int deliveredhourslimit = Settings.SettingsID.ShowRecentlyDelivered.AsInt(0);
            DateTime minDeliverDateUTC = DateTime.UtcNow.AddHours(-deliveredhourslimit);

            var orders = db.Orders.Where(o => o.DeleteDateUTC == null 
                                                && (o.StatusID == (int)OrderStatus.STATUS.Assigned 
                                                    || o.StatusID == (int)OrderStatus.STATUS.Dispatched 
                                                    || o.StatusID == (int)OrderStatus.STATUS.Accepted 
                                                    || o.StatusID == (int)OrderStatus.STATUS.PickedUp
                                                    || deliveredhourslimit > 0 && (o.StatusID == (int)OrderStatus.STATUS.Delivered || o.StatusID == (int)OrderStatus.STATUS.Audited) && o.DestDepartTimeUTC > minDeliverDateUTC));

            // Filter orders assigned to me or that use my carrier
            int myDriverID = Converter.ToInt32(Profile.GetPropertyValue("DriverID"));
            int myCarrierID = Converter.ToInt32(Profile.GetPropertyValue("CarrierID"));
            orders = orders.Where(o => myCarrierID == -1 || o.CarrierID == myCarrierID || o.DriverID == myDriverID);

            // filter orders by terminal
            int myTerminalID = Converter.ToInt32(Profile.GetPropertyValue("TerminalID"));
            orders = orders.Where(o => myTerminalID <= 0 // no terminal filtering
                                     ||     (o.Origin == null || o.Origin.TerminalID == null || o.Origin.TerminalID == myTerminalID)
                                         && (o.Destination == null || o.Destination.TerminalID == null || o.Destination.TerminalID == myTerminalID)
                                         && (o.Driver == null || o.Driver.TerminalID == null || o.Driver.TerminalID == myTerminalID));

            return View("IndexTelerik", orders.ToList());
        }

        /**************************************************************************************************/
        /**  PAGE: ~/Orders/OrderList                                                                   **/
        /**  DESCRIPTION: Emulate the ticket list.  Filter on status of Dispatched, Accepted, Picked     **/
        /**         Up, or Delivered and not deleted and send to main page for list                      **/
        /**************************************************************************************************/
        [Authorize(Roles = "viewOrders")]
        public ActionResult OrderList()
        {
            /*
            OrderSearchViewModel osv = new OrderSearchViewModel();
            osv.Statuses = new List<int> { (int)OrderStatus.STATUS.Dispatched, (int)OrderStatus.STATUS.Accepted, (int)OrderStatus.STATUS.PickedUp, (int)OrderStatus.STATUS.Delivered, };
            osv.Deleted = false;

            return RedirectToAction("Index", osv.RouteValues());
            */
            ViewBag.Title = "Dispatch";
            ViewBag.SubTitle = "Order Edit";
            ViewBag.Action = "OrderList";
            var orders = db.Orders.Where(o => o.DeleteDateUTC == null
                                        && (o.StatusID == (int)OrderStatus.STATUS.Dispatched
                                            || o.StatusID == (int)OrderStatus.STATUS.Accepted
                                            || o.StatusID == (int)OrderStatus.STATUS.PickedUp
                                            || o.StatusID == (int)OrderStatus.STATUS.Delivered))
                                            .Include(o => o.Origin.Terminal)
                                            .Include(o => o.Destination.Terminal)
                                            .Include(o => o.Driver.Terminal);

            // Filter orders assigned to me or that use my carrier
            int myDriverID = Converter.ToInt32(Profile.GetPropertyValue("DriverID"));
            int myCarrierID = Converter.ToInt32(Profile.GetPropertyValue("CarrierID"));
            orders = orders.Where(o => myCarrierID == -1 || o.CarrierID == myCarrierID || o.DriverID == myDriverID);

            // filter orders by terminal
            int myTerminalID = Converter.ToInt32(Profile.GetPropertyValue("TerminalID"));
            orders = orders.Where(o => myTerminalID <= 0 // no terminal filtering
                                     ||     (o.Origin == null || o.Origin.TerminalID == null || o.Origin.TerminalID == myTerminalID)
                                         && (o.Destination == null || o.Destination.TerminalID == null || o.Destination.TerminalID == myTerminalID)
                                         && (o.Driver == null || o.Driver.TerminalID == null || o.Driver.TerminalID == myTerminalID));

            return View("IndexTelerik", orders.ToList());
        }

        public ActionResult UndeleteView()
        {
            OrderSearchViewModel osv = new OrderSearchViewModel();
            osv.Deleted = true;

            return RedirectToAction("Index", osv.RouteValues());
        }


        /**************************************************************************************************/
        /**  PAGE: ~/Orders/Details/{id}                                                                 **/
        /**        ~/Orders/Details/?ordernum={ordernum}                                                 **/
        /**  DESCRIPTION: Dispalys all fields for a given order                                          **/
        /**************************************************************************************************/
        [Authorize(Roles = "viewOrders")]
        public ActionResult Details(int? id, int? ordernum)
        {
            ViewBag.Title = "Order Detail";
            Order order = null;

            if (ordernum != null)
                order = db.Orders
                    .Include(o => o.Destination.TicketType)
                    .Include(o => o.Route.Origin)
                    .Include(o => o.Route.Destination)
                    .FirstOrDefault(o => o.OrderNum == ordernum);
            else if (id != null)
                order = db.Orders
                    .Include(o => o.Destination.TicketType)
                    .Include(o => o.Route.Origin)
                    .Include(o => o.Route.Destination)
                    .SingleOrDefault(o => o.ID == id);

            if (order == null)
            {
                TempData["error"] = "Order not found!";
                return RedirectToAction("Search");
            }
            if (hasAccess(order) == false)
            {
                TempData["error"] = "You do not have access to this order!";
                return RedirectToAction("Search");
            }

            ViewBag.OTZ = (order.Origin != null) ? order.Origin.TimeZone.ID : 0;
            ViewBag.DTZ = (order.Destination != null) ? order.Destination.TimeZone.ID : 0;
            ViewBag.OriginUOM = (order.OriginUOM != null) ? order.OriginUOM.Abbrev : "";
            ViewBag.OriginUOMFormat = (order.OriginUOM != null) ? order.OriginUOM.getFormat() : "0.00";
            ViewBag.DestUOM = (order.DestUOM != null) ? order.DestUOM.Abbrev : "";
            ViewBag.DestUOMFormat = (order.DestUOM != null) ? order.DestUOM.getFormat() : "0.00";
            ViewBag.Reassignments = getReassigns(order);

            return View(order);
        }



        /**************************************************************************************************/
        /**  PAGE: ~/Orders/History/{id}                                                                 **/
        /**        ~/Orders/History/?ordernum={ordernum}                                                 **/
        /**  DESCRIPTION:  Displays audit history or the change log for a given order                    **/
        /**************************************************************************************************/
        [Authorize(Roles = "viewOrders")]
        public ActionResult History(int? id, int? ordernum)
        {
            ViewBag.Title = "Order History";
            Order order = null;

            if (ordernum != null)
                order = db.Orders.FirstOrDefault(o => o.OrderNum == ordernum);
            else if (id != null)
                order = db.Orders.Find(id);

            if (order == null)
            {
                TempData["error"] = "Order not found!";
                return RedirectToAction("Search");
            }
            if (hasAccess(order) == false)
            {
                TempData["error"] = "You do not have access to this order!";
                return RedirectToAction("Search");
            }

            return View(order);
        }
        //[Authorize(Roles = "createOrders")]
        //public ActionResult Create() { }

        //[Authorize(Roles = "assignOrders")]
        //public ActionResult Assign() { }


        /**************************************************************************************************/
        /**  PAGE: ~/Orders/Dispatch/{id}                                                                **/
        /**  DESCRIPTION: Dispatch an order to a driver, puts the order is Dispatched status             **/
        /**************************************************************************************************/
        [Authorize(Roles = "viewOrders, Dispatch3P, editOrders")]
        public ActionResult Dispatch(int? id)
        {
            ViewBag.Title = "Order Dispatch";
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                TempData["error"] = "Order not found!";
                return RedirectToAction("Search");
            }
            if (hasAccess(order) == false)
            {
                TempData["error"] = "You do not have access to this order!";
                return RedirectToAction("Search");
            }
            if (order.StatusID != (int)OrderStatus.STATUS.Assigned)
            {
                TempData["error"] = "Cannot dispatch an order unless it is in Assigned status!";
                return RedirectToAction("Details", new { id = id });
            }

            ViewBag.OrderNum = order.OrderNum;
            //ViewBag.DriverID = new SelectList(db.Drivers.Where(d => d.CarrierID == order.CarrierID && d.DeleteDateUTC == null).OrderBy(d => d.LastName).ThenBy(d => d.FirstName), "ID", "FullName");
            ViewBag.DriverID = DriversController.getEligibleDriversSSL(order.CarrierID, null, null, null, null, order.OrderDate ?? order.DueDate, order.Origin.H2S);
            ViewBag.Origin = order.Origin.Name;
            ViewBag.OriginH2S = order.Origin.H2S;
            ViewBag.Destination = order.Destination.Name;

            return View(order);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "viewOrders, Dispatch3P, editOrders")]
        public ActionResult Dispatch([Bind(Include = "ID,DriverID,DispatchNotes")] Order o) // tickettype? tank? bol#? shipperpo?
        {
            if (ModelState.IsValid)
            {
                Order order = db.Orders.Find(o.ID);
                ViewBag.OrderNum = order.OrderNum;
                //ViewBag.DriverID = new SelectList(db.Drivers.Where(d => d.CarrierID == order.CarrierID && d.DeleteDateUTC == null).OrderBy(d => d.LastName).ThenBy(d => d.FirstName), "ID", "FullName");
                ViewBag.DriverID = DriversController.getEligibleDriversSSL(order.CarrierID, null, null, null, null, order.OrderDate ?? order.DueDate, order.Origin.H2S, o.DriverID);
                ViewBag.Origin = order.Origin.Name;
                ViewBag.OriginH2S = order.Origin.H2S;
                ViewBag.Destination = order.Destination.Name;

                DateTime now = DateTime.UtcNow;
                order.StatusID = (int)OrderStatus.STATUS.Dispatched;
                order.DriverID = o.DriverID;
                order.DispatchNotes = o.DispatchNotes;

                db.SaveChanges(User.Identity.Name);

                TempData["success"] = "Order #" + order.OrderNum + " was dispatched";
                return RedirectToAction("Details", new { id = order.ID });
            }

            return View(o);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "viewDispatchMap")]
        public ActionResult DispatchFromMap(int OrderID, int DriverID, string DispatchNotes)
        {
            Order order = db.Orders.Find(OrderID);
            Driver driver = db.Drivers.Find(DriverID);
            if (order == null || hasAccess(order) == false || driver == null)
            {
                TempData["error"] = "Order no longer exists or you do not have access to dispatch it!";
                return Json(null, JsonRequestBehavior.AllowGet); // throw error
            }
            if (order.OriginInfoRequired()) // send warning if pickedup, delivered, or audited
            {
                TempData["error"] = "Cannot dispatch order #" + order.OrderNum + " because it has since been picked up or delivered.";
                return DispatchMap_Orders();
            }
            if (order.StatusID == (int) OrderStatus.STATUS.Declined) // send warning if declined
            {
                TempData["error"] = "Cannot dispatch order #" + order.OrderNum + " because it has since been declined.";
                return DispatchMap_Orders();
            }
            if (order.Deleted) // send warning if deleted
            {
                TempData["error"] = "Cannot dispatch order #" + order.OrderNum + " because it has since been deleted.";
                return DispatchMap_Orders();
            }
            bool reassign = (order.DriverID != null && DriverID != order.DriverID);

            DateTime now = DateTime.UtcNow;
            order.StatusID = (int)OrderStatus.STATUS.Dispatched;
            order.DriverID = DriverID;
            order.DispatchNotes = DispatchNotes;
            order.CarrierID = driver.CarrierID;
            order.TruckID = driver.TruckID;
            order.TrailerID = driver.TrailerID;
            order.Trailer2ID = driver.Trailer2ID;

            db.SaveChanges(User.Identity.Name);

            if (reassign)
            {
                Order neworder = db.Orders.Where(x => x.DriverID == DriverID && x.ReassignKey == order.ReassignKey && x.DeleteDateUTC == null).OrderByDescending(x => x.CreateDateUTC).FirstOrDefault();
                if (neworder == null || neworder.ID == order.ID) // soft reassign, not on mobile yet
                    TempData["success"] = "Order #" + order.OrderNum + " was reassigned to " + order.Driver.FullName + " successfully!";
                else 
                    TempData["success"] = "Order was reassigned to " + neworder.Driver.FullName + " (new order #" + neworder.OrderNum + ")";
            }
            else
            {
                TempData["success"] = "Order #" + order.OrderNum + " was dispatched to " + order.Driver.FullName + " successfully!";
            }
            
            return DispatchMap_Orders();
        }


        /**************************************************************************************************/
        /**  PAGE: ~/Orders/Accept/{id}                                                                  **/
        /**  DESCRIPTION: Accepts an order on behalf of a driver, puts the order in Accepted status      **/
        /**************************************************************************************************/
        [Authorize(Roles = "editOrders")]
        public ActionResult Accept(int? id)
        {
            ViewBag.Title = "Order Accept";
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                TempData["error"] = "Order not found!";
                return RedirectToAction("Search");
            }
            if (hasAccess(order) == false)
            {
                TempData["error"] = "You do not have access to this order!";
                return RedirectToAction("Search");
            }
            if (order.StatusID != (int)OrderStatus.STATUS.Dispatched)
            {
                TempData["error"] = "Cannot accept an order unless it is in Dispatched status!";
                return RedirectToAction("Details", new { id = id });
            }

            ViewBag.OrderNum = order.OrderNum;
            ViewBag.TruckID = new SelectList(db.Trucks.Where(t => t.CarrierID == order.CarrierID && t.DeleteDateUTC == null).OrderBy(t => t.IDNumber), "ID", "IDNumber", order.TruckID);
            ViewBag.TrailerID = new SelectList(db.Trailers.Where(t => t.CarrierID == order.CarrierID && t.DeleteDateUTC == null).OrderBy(t => t.IDNumber), "ID", "IDNumber", order.TrailerID);
            ViewBag.Trailer2ID = new SelectList(db.Trailers.Where(t => t.CarrierID == order.CarrierID && t.DeleteDateUTC == null).OrderBy(t => t.IDNumber), "ID", "IDNumber", order.Trailer2ID);
            ViewBag.Origin = order.Origin.Name;
            ViewBag.OriginH2S = order.Origin.H2S;
            ViewBag.Destination = order.Destination.Name;

            return View(order);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "editOrders")]
        public ActionResult Accept([Bind(Include = "ID,TruckID,TrailerID,Trailer2ID")] Order o)
        {
            if (ModelState.IsValid)
            {
                Order order = db.Orders.Find(o.ID);
                ViewBag.OrderNum = order.OrderNum;
                ViewBag.TruckID = new SelectList(db.Trucks.Where(t => t.CarrierID == order.CarrierID && t.DeleteDateUTC == null).OrderBy(t => t.IDNumber), "ID", "IDNumber", o.TruckID);
                ViewBag.TrailerID = new SelectList(db.Trailers.Where(t => t.CarrierID == order.CarrierID && t.DeleteDateUTC == null).OrderBy(t => t.IDNumber), "ID", "IDNumber", o.TrailerID);
                ViewBag.Trailer2ID = new SelectList(db.Trailers.Where(t => t.CarrierID == order.CarrierID && t.DeleteDateUTC == null).OrderBy(t => t.IDNumber), "ID", "IDNumber", o.Trailer2ID);
                ViewBag.Origin = order.Origin.Name;
                ViewBag.OriginH2S = order.Origin.H2S;
                ViewBag.Destination = order.Destination.Name;

                DateTime now = DateTime.UtcNow;
                order.PickupPrintStatusID = (int) PrintStatus.STATUS.NotFinalized;
                order.StatusID = (int) OrderStatus.STATUS.Accepted;
                order.TruckID = o.TruckID;
                if (o.TrailerID == o.Trailer2ID)
                {
                    ViewBag.Error = "Cannot use the same trailer twice!";
                    return View(o);
                }
                order.TrailerID = o.TrailerID;
                order.Trailer2ID = o.Trailer2ID;
                order.AcceptLastChangeDateUTC = now;

                db.SaveChanges(User.Identity.Name);
                // The order trigger (trigOrder_IU) will set the defaults for the driver app

                TempData["success"] = "Order #" + order.OrderNum + " was accepted";
                return RedirectToAction("Details", new { id = order.ID });
            }

            return View(o);
        }


        /**************************************************************************************************/
        /**  PAGE: ~/Orders/Unaccept/{id}                                                                **/
        /**  DESCRIPTION: Returns an order to Dispatched status                                          **/
        /**************************************************************************************************/
        [Authorize(Roles = "editOrders")]
        public ActionResult Unaccept(int? id)
        {
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                TempData["error"] = "Order not found!";
                return RedirectToAction("Search");
            }
            if (hasAccess(order) == false)
            {
                TempData["error"] = "You do not have access to this order!";
                return RedirectToAction("Search");
            }
            if (order.StatusID != (int)OrderStatus.STATUS.Accepted)
            {
                TempData["error"] = "Invalid status.  Cannot edit acceptance for this order!";
                return RedirectToAction("Details", new { id = id });
            }

            DateTime now = DateTime.UtcNow;
            order.StatusID = (int) OrderStatus.STATUS.Dispatched;

            db.SaveChanges(User.Identity.Name);

            TempData["success"] = "Order #" + order.OrderNum + " successfully returned to Dispatched status";
            return RedirectToAction("Details", new { id = id });
        }


        /**************************************************************************************************/
        /**  PAGE: ~/Orders/Pickup/{id}                                                                  **/
        /**  DESCRIPTION: Picks up a load for an order, puts the order in Picked Up status               **/
        /**************************************************************************************************/
        [Authorize(Roles = "editOrders")]
        public ActionResult Pickup(int? id)
        {
            ViewBag.Title = "Order Pickup";
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                TempData["error"] = "Order not found!";
                return RedirectToAction("Search");
            }
            if (hasAccess(order) == false)
            {
                TempData["error"] = "You do not have access to this order!";
                return RedirectToAction("Search");
            }
            if (order.StatusID != (int)OrderStatus.STATUS.Accepted)
            {
                TempData["error"] = "Cannot pick up an order unless it is in Accepted status!";
                return RedirectToAction("Details", new { id = id });
            }

            ViewBag.OrderNum = order.OrderNum;
            ViewBag.OriginWaitReasonID = new SelectList(db.OriginWaitReasons.Where(r => r.DeleteDateUTC == null).OrderBy(r => r.Num), "ID", "NumDesc", order.OriginWaitReasonID);
            ViewBag.Origin = order.Origin.Name;
            ViewBag.OriginH2S = order.Origin.H2S;
            ViewBag.Destination = order.Destination.Name;

            return View(order);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "editOrders")]
        public ActionResult Pickup([Bind(Include = "ID,OriginArriveTime,ChainUp,OriginDepartTime,OriginTruckMileage,OriginWaitReasonID,OriginWaitNotes,PickupDriverNotes")] Order o)
        {
            // Set page variables in case pickup fails
            Order order = db.Orders.Find(o.ID);
            ViewBag.OrderNum = order.OrderNum;
            ViewBag.OriginWaitReasonID = new SelectList(db.OriginWaitReasons.Where(r => r.DeleteDateUTC == null).OrderBy(r => r.Num), "ID", "NumDesc", o.OriginWaitReasonID);
            ViewBag.Origin = order.Origin.Name;
            ViewBag.OriginH2S = order.Origin.H2S;
            ViewBag.Destination = order.Destination.Name;


            if (ModelState.IsValid)
            {
                if (o.OriginDepartTimeUTC == null || o.OriginArriveTimeUTC == null)
                {
                    ViewBag.Error = "Origin arrive and depart times are required!";
                    return View(o);
                }
                else if (o.OriginArriveTimeUTC > o.OriginDepartTimeUTC)
                {
                    ViewBag.Error = "Invalid arrive and depart times!";
                    return View(o);
                }
                else
                {
                    TimeSpan wait = o.OriginDepartTimeUTC.Value - o.OriginArriveTimeUTC.Value;
                    o.OriginMinutes = DBHelper.ToInt32(wait.TotalMinutes);
                }

                // If wait threshold exceeded during pickup, require origin wait reason and check if notes required
                if (o.OriginWaitReasonRequired)
                {
                    OriginWaitReason reason = db.OriginWaitReasons.Find(o.OriginWaitReasonID);
                    if (reason == null)
                    {
                        ViewBag.Error = "Origin wait reason is required!";
                        return View(o);
                    }
                    if (reason.RequireNotes && String.IsNullOrEmpty(o.OriginWaitNotes))
                    {
                        ViewBag.Error = "Origin wait notes are required!";
                        return View(o);
                    }
                    order.OriginWaitReasonID = o.OriginWaitReasonID;
                    order.OriginWaitNotes = o.OriginWaitNotes;
                }
                else
                {
                    // Clear out reason and notes
                    order.OriginWaitReasonID = null;
                    order.OriginWaitNotes = null;
                }

                int activetickets = db.OrderTickets.Where(t => t.OrderID == o.ID && !t.Rejected && t.DeleteDateUTC == null).Count();
                if (activetickets == 0 && o.Rejected == false)
                {
                    ViewBag.Error = "A valid ticket must accompany an order in Picked Up status!  Please add a ticket or reject this order.";
                    return View(o);
                }

                if (o.MileageRequired() && o.OriginTruckMileage == null)
                {
                    ViewBag.Error = "Mileage (odometer reading) at origin is required";
                    return View(o);
                }

                if (!order.IsOriginQtyValid) // check against existing order tickets
                {
                    ViewBag.Error = "Order exceeds maximum quantity allowed!";
                    return View(o);
                }
                if (!order.IsOriginWeightValid) // check against existing order tickets
                {
                    ViewBag.Error = "Order exceeds maximum weight allowed!";
                    return View(o);
                }

                if (o.OriginPhotoRequired() && order.OriginPhoto == null)
                {
                    TempData["message"] += "An origin photo should accompany this order.  ";
                }
                if (o.OriginPhoto2Required() && order.OriginPhoto2 == null)
                {
                    TempData["message"] += "An origin photo #2 should accompany this order.  ";
                }
                if (o.OriginConditionPhotosRequired() && (order.OriginArrivePhoto == null || order.OriginDepartPhoto == null))
                {
                    TempData["message"] += "Origin arrive and depart photos should accompany this order.  ";
                }
                
                // Copy remaining fields to the existing order and save pickup
                DateTime now = DateTime.UtcNow;
                order.ChainUp = o.ChainUp;

                order.OriginArriveTimeUTC = o.OriginArriveTimeUTC;
                order.OriginDepartTimeUTC = o.OriginDepartTimeUTC;
                // Origin minutes (OriginMinutes) is calculated by the tblOrder trigger (trigOrder_IU)
                order.PickupDriverNotes = o.PickupDriverNotes;


                order.OriginTruckMileage = o.OriginTruckMileage;

                order.Rejected = false;
                order.RejectReasonID = null;
                order.RejectNotes = null;
                

                order.StatusID = (int) OrderStatus.STATUS.PickedUp;
                order.PickupPrintStatusID = (int) PrintStatus.STATUS.Handwritten;
                // set PickupLastChangeDateUTC = LastChangeDateUTC = UtcNow
                db.SaveChanges(User.Identity.Name, order.PickupLastChangeDateUTC = DateTime.UtcNow);

                TempData["success"] = "Order #" + order.OrderNum + " was picked up";
                return RedirectToAction("Details", new { id = order.ID });
            }

            return View(o);
        }


        /**************************************************************************************************/
        /**  PAGE: ~/Orders/Unpickup/{id}                                                                **/
        /**  DESCRIPTION: Returns an order to Accepted status                                            **/
        /**************************************************************************************************/
        [Authorize(Roles = "editOrders")]
        public ActionResult Unpickup(int id)
        {
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                TempData["error"] = "Order not found!";
                return RedirectToAction("Search");
            }
            if (hasAccess(order) == false)
            {
                TempData["error"] = "You do not have access to this order!";
                return RedirectToAction("Search");
            }
            if (order.StatusID != (int)OrderStatus.STATUS.PickedUp)
            {
                TempData["error"] = "Invalid status.  Cannot edit pickup for this order!";
                return RedirectToAction("Details", new { id = id });
            }

            order.PickupPrintStatusID = (int) PrintStatus.STATUS.NotFinalized;
            order.StatusID = (int) OrderStatus.STATUS.Accepted;
            // set AcceptLastChangeDateUTC = LastChangeDateUTC = UtcNow
            db.SaveChanges(User.Identity.Name, order.AcceptLastChangeDateUTC = DateTime.UtcNow);

            TempData["success"] = "Order #" + order.OrderNum + " successfully returned to Accepted status";
            return RedirectToAction("Details", new { id = id });
        }


        /**************************************************************************************************/
        /**  PAGE: ~/Orders/Deliver/{id}                                                                 **/
        /**  DESCRIPTION: Delivers a load for an order, puts the order in Delivered status               **/
        /**************************************************************************************************/
        [Authorize(Roles = "editOrders")]
        public ActionResult Deliver(int? id)
        {
            ViewBag.Title = "Order Deliver";

            //*** This changed so we could get rid of the destination ticket type virtual 9/26/16 ***
            //Order order = db.Orders.Find(id);
            Order order = db.Orders.Include(o => o.Destination.TicketType).SingleOrDefault(o => o.ID == id);  
            
            if (order == null)
            {
                TempData["error"] = "Order not found!";
                return RedirectToAction("Search");
            }
            if (hasAccess(order) == false)
            {
                TempData["error"] = "You do not have access to this order!";
                return RedirectToAction("Search");
            }
            if (order.StatusID != (int)OrderStatus.STATUS.PickedUp)
            {
                TempData["error"] = "Cannot deliver an order unless it is in Picked Up status!";
                return RedirectToAction("Details", new { id = id });
            }

            ViewBag.OrderNum = order.OrderNum;
            ViewBag.DestWaitReasonID = new SelectList(db.DestinationWaitReasons.Where(r => r.DeleteDateUTC == null).OrderBy(r => r.Num), "ID", "NumDesc", order.DestWaitReasonID);
            ViewBag.Origin = order.Origin.Name;
            ViewBag.OriginH2S = order.Origin.H2S;
            ViewBag.Destination = order.Destination.Name;
            ViewBag.UOM = order.DestUOM.Abbrev;
            ViewBag.UOMStep = order.DestUOM.getStep();
            ViewBag.UOMFormat = order.DestUOM.getFormat();
            ViewBag.TicketTypeID = order.Destination.TicketTypeID;
            ViewBag.TicketType = order.Destination.TicketType.Name;

            return View(order);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "editOrders, Administrator")]
        public ActionResult Deliver([Bind(Include = "ID,DestArriveTime,DestDepartTime,DestBOLNum,DestTruckMileage,DestChainup," +
                    "DestOpenMeterUnits,DestCloseMeterUnits,DestGrossUnits,DestNetUnits,DestProductBSW,DestProductGravity,DestProductTemp," + 
                    "DestWeightGrossUnits,DestWeightTareUnits,DestWeightNetUnits," +
                    "DestRailCarNum,DestRackBay,DestTrailerWaterCapacity,DestWaitReasonID,DestWaitNotes,DeliverDriverNotes")] Order o)
        {
            // Set page variables in case deliver fails

            //*** This changed so we could get rid of the destination ticket type virtual 9/26/16 ***
            //Order order = db.Orders.Find(o.ID);
            Order order = db.Orders.Include(ordr => ordr.Destination.TicketType).SingleOrDefault(ordr => ordr.ID == o.ID);
            
            ViewBag.OrderNum = order.OrderNum;
            ViewBag.DestWaitReasonID = new SelectList(db.DestinationWaitReasons.Where(r => r.DeleteDateUTC == null).OrderBy(r => r.Num), "ID", "NumDesc", o.DestWaitReasonID);
            ViewBag.Origin = order.Origin.Name;
            ViewBag.OriginH2S = order.Origin.H2S;
            ViewBag.Destination = order.Destination.Name;
            ViewBag.UOM = order.DestUOM.Abbrev;
            ViewBag.UOMStep = order.DestUOM.getStep();
            ViewBag.UOMFormat = order.DestUOM.getFormat();
            ViewBag.TicketTypeID = order.Destination.TicketTypeID;
            ViewBag.TicketType = order.Destination.TicketType.Name;

            if (ModelState.IsValid)
            {
                // Convert destination arrive and depart back to UTC
                order.DestArriveTimeUTC = o.DestArriveTimeUTC; // automatically converts DestArriveTime
                order.DestDepartTimeUTC = o.DestDepartTimeUTC; // automatically converts DestDepartTime
                // Destination minutes (DestMinutes) are calculated by the Order trigger (trigOrder_IU) based on arrive and depart times; needed here for check on wait time

                order.DestBOLNum = o.DestBOLNum;
                if (order.Destination.TicketTypeID == (int) DestTicketType.TYPE.VisualMeter && o.CalculateVM())
                {
                    // Calculate gross volume from open and close meter readings (global setting)
                    order.DestGrossUnits = o.DestCloseMeterUnits - o.DestOpenMeterUnits;  // may get overwritten by the Order trigger (trigOrder_IU)
                }
                else
                {
                    order.DestGrossUnits = o.DestGrossUnits; // may get overwritten by the Order trigger (trigOrder_IU)
                }
                order.DestNetUnits = o.DestNetUnits; // may get overwritten by the Order trigger (trigOrder_IU)
                order.DestOpenMeterUnits = o.DestOpenMeterUnits;
                order.DestCloseMeterUnits = o.DestCloseMeterUnits;
                order.DestProductBSW = o.DestProductBSW;
                order.DestProductGravity = o.DestProductGravity;
                order.DestProductTemp = o.DestProductTemp;
                order.DestWeightGrossUnits = o.DestWeightGrossUnits;
                order.DestWeightTareUnits = o.DestWeightTareUnits;
                order.DestWeightNetUnits = o.DestWeightNetUnits;
                order.DestRailCarNum = o.DestRailCarNum;
                order.DestRackBay = o.DestRackBay;
                order.DestTrailerWaterCapacity = o.DestTrailerWaterCapacity;
                order.DestTruckMileage = o.DestTruckMileage;
                order.DestChainUp = o.DestChainUp;
                order.DeliverDriverNotes = o.DeliverDriverNotes;


                if (o.DestDepartTimeUTC == null || o.DestArriveTimeUTC == null || o.DestArriveTimeUTC > o.DestDepartTimeUTC)
                {
                    ViewBag.Error = "Valid arrive and depart times are required!";
                    return View(o);
                }
                else if (o.DestArriveTimeUTC < order.OriginDepartTimeUTC)
                {
                    ViewBag.Error = "Arrive time must be greater than origin departure!";
                    return View(o);
                }
                else
                {
                    TimeSpan wait = o.DestDepartTimeUTC.Value - o.DestArriveTimeUTC.Value;
                    o.DestMinutes = DBHelper.ToInt32(wait.TotalMinutes);
                }

                // If wait threshold exceeded during delivery, require destination wait reason and check if notes required
                if (o.DestWaitReasonRequired)
                {
                    DestinationWaitReason reason = db.DestinationWaitReasons.Find(o.DestWaitReasonID);
                    if (reason == null)
                    {
                        ViewBag.Error = "Destination wait reason is required!";
                        return View(o);
                    }
                    if (reason.RequireNotes && String.IsNullOrEmpty(o.DestWaitNotes))
                    {
                        ViewBag.Error = "Destination wait notes are required!";
                        return View(o);
                    }
                    order.DestWaitReasonID = o.DestWaitReasonID;
                    order.DestWaitNotes = o.DestWaitNotes;
                }
                else
                {
                    // Clear out reason and notes
                    order.DestWaitReasonID = null;
                    order.DestWaitNotes = null;
                }

                if (o.DestRackBay == null && o.RackBayRequired())
                {
                    ViewBag.Error = "Rack/bay is required!";
                    return View(o);
                }
                // Execute additional checks based on the destination ticket type
                switch (order.Destination.TicketTypeID)
                {
                    case (int)DestTicketType.TYPE.Basic:
                        if (o.DestGrossUnits == null || o.DestNetUnits == null || o.DestGrossUnits < 0 || o.DestNetUnits < 0)
                        {
                            ViewBag.Error = "Valid GOV and NSV are required";
                            return View(o);
                        }
                        break;

                    case (int)DestTicketType.TYPE.Propane:
                        if (o.DestGrossUnits == null || o.DestNetUnits == null || o.DestGrossUnits < 0 || o.DestNetUnits < 0)
                        {
                            ViewBag.Error = "Valid GOV and NSV are required";
                            return View(o);
                        }
                        break;

                    case (int)DestTicketType.TYPE.BOLAvailable:
                        if (o.DestBOLRequired() && String.IsNullOrEmpty(o.DestBOLNum))
                        {
                            ViewBag.Error = "BOL # is required";
                            return View(o);
                        }
                        if (o.DestGrossUnits == null || o.DestNetUnits == null || o.DestGrossUnits < 0 || o.DestNetUnits < 0)
                        {
                            ViewBag.Error = "Valid GOV and NSV are required";
                            return View(o);
                        }
                        if (o.DestProductTemp == null || o.DestProductGravity == null || o.DestProductBSW == null)
                        {
                            ViewBag.Error = "Product temperature, API density and sediment & water % are required";
                            return View(o);
                        }
                        break;

                    case (int)DestTicketType.TYPE.VisualMeter:
                        if (   o.DestOpenMeterUnits == null || o.DestCloseMeterUnits == null 
                            || o.DestOpenMeterUnits < 0 || o.DestCloseMeterUnits < 0 || o.DestOpenMeterUnits > o.DestCloseMeterUnits)
                        {
                            ViewBag.Error = "Valid open and close meter readings are required";
                            return View(o);
                        }
                        if (o.CalculateVM() == false && (o.DestGrossUnits == null || o.DestGrossUnits < 0)) 
                        {
                            ViewBag.Error = "A valid GOV is required";
                            return View(o);
                        }
                        if (o.DestNetUnits == null || o.DestNetUnits < 0)
                        {
                            ViewBag.Error = "A valid NSV is required";
                            return View(o);
                        }
                        break;

                    case (int)DestTicketType.TYPE.CAN_Basic:
                        if (o.DestProductTemp == null || o.DestProductGravity == null || o.DestProductBSW == null)
                        {
                            ViewBag.Error = "Product temperature, density and water cut are required";
                            return View(o);
                        }
                        break;

                    case (int)DestTicketType.TYPE.MineralRun:
                        if (o.DestWeightNetUnits == null || o.DestWeightNetUnits < 0)
                        {
                            ViewBag.Error = "Valid net weight is required!";
                            return View(o);
                        }
                        if (o.DestBOLRequired() && String.IsNullOrEmpty(o.DestBOLNum))
                        {
                            ViewBag.Error = "BOL # is required";
                            return View(o);
                        }
                        break;

                    case (int)DestTicketType.TYPE.VisualMeterBOL:
                        if (o.DestBOLRequired() && String.IsNullOrEmpty(o.DestBOLNum))
                        {
                            ViewBag.Error = "BOL # is required";
                            return View(o);
                        }
                        if (   o.DestOpenMeterUnits == null || o.DestCloseMeterUnits == null 
                            || o.DestOpenMeterUnits < 0 || o.DestCloseMeterUnits < 0 || o.DestOpenMeterUnits > o.DestCloseMeterUnits)
                        {
                            ViewBag.Error = "Valid open and close meter readings are required";
                            return View(o);
                        }
                        if (o.CalculateVM() == false && (o.DestGrossUnits == null || o.DestGrossUnits < 0))
                        {
                            ViewBag.Error = "A valid GOV is required";
                            return View(o);
                        }
                        if (o.DestProductTemp == null || o.DestProductGravity == null || o.DestProductBSW == null)
                        {
                            ViewBag.Error = "Product temperature, density and BS&W are required";
                            return View(o);
                        }
                        break;
 
                    case (int)DestTicketType.TYPE.BOLGrossOnly:
                        if (o.DestBOLRequired() && String.IsNullOrEmpty(o.DestBOLNum))
                        {
                            ViewBag.Error = "BOL # is required";
                            return View(o);
                        }
                        if (o.DestGrossUnits == null || o.DestGrossUnits < 0 )
                        {
                            ViewBag.Error = "Valid GOV is required";
                            return View(o);
                        }
                        break;

                    case (int)DestTicketType.TYPE.Propane_With_Scale:
                        if (o.DestBOLRequired() && String.IsNullOrEmpty(o.DestBOLNum))
                        {
                            ViewBag.Error = "BOL # is required";
                            return View(o);
                        }
                        if (o.DestGrossUnits == null || o.DestNetUnits == null || o.DestGrossUnits < 0 || o.DestNetUnits < 0)
                        {
                            ViewBag.Error = "Valid GOV and NSV are required";
                            return View(o);
                        }
                        if (o.DestWeightGrossUnits == null || o.DestWeightGrossUnits < 0
                            || o.DestWeightTareUnits == null || o.DestWeightTareUnits < 0
                            || o.DestWeightNetUnits == null || o.DestWeightNetUnits < 0)
                        {
                            ViewBag.Error = "Valid gross, tare, and net weights are required!";
                            return View(o);
                        }
                        break;
                }

                o.OriginGrossUnits = order.OriginGrossUnits; // use current GOV value to compare
                if (!o.DestGOVWithinTolerance())
                {
                    TempData["message"] += "Destination GOV not within tolerance!";
                }

                o.OriginNetUnits = order.OriginNetUnits; // use current NSV value to compare
                if (!o.DestNSVWithinTolerance())
                {
                    TempData["message"] += " Destination NSV not within tolerance!";
                }

                OrderTicket Ticket = new OrderTicket(); // dummy ticket to grab global min/max values
                if (new [] {(int)DestTicketType.TYPE.BOLAvailable, (int)DestTicketType.TYPE.VisualMeterBOL, (int)DestTicketType.TYPE.CAN_Basic }
                            .Contains(order.Destination.TicketTypeID))
                {
                    if (o.DestProductBSW < Ticket.MinBSW || o.DestProductBSW > Ticket.MaxBSW)
                    {
                        TempData["message"] += " Destination BS&W out of range!";
                    }

                    if (o.DestProductGravity < Ticket.MinGravity || o.DestProductGravity > Ticket.MaxGravity)
                    {
                        TempData["message"] += " Destination gravity out of range!";
                    }

                    if (o.DestProductTemp < Ticket.MinTemp || o.DestProductTemp > Ticket.MaxTemp)
                    {
                        TempData["message"] += " Destination temperature out of range!";
                    }
                }

                if (o.DestPhotoRequired() && order.DestinationPhoto == null)
                {
                    TempData["message"] += "A destination photo should accompany this order.  ";
                }
                if (o.DestPhoto2Required() && order.DestinationPhoto2 == null)
                {
                    TempData["message"] += "A destination photo #2 should accompany this order.  ";
                }
                if (o.DestConditionPhotosRequired() && (order.DestArrivePhoto == null || order.DestDepartPhoto == null))
                {
                    TempData["message"] += "Destination arrive and depart photos should accompany this order.  ";
                }

                if (o.MileageRequired())
                {
                    if (o.DestTruckMileage == null)
                    {
                        ViewBag.Error = "Mileage (odometer reading) at destination is required";
                        return View(o);
                    }
                    if (!o.IsTransfer)
                    {
                        if (o.OriginTruckMileage > o.DestTruckMileage) 
                        {
                            TempData["message"] += "Odometer readings invalid (origin > destination)";
                        }
                    }
                    else
                    {
                        if (o.OriginTruckMileage > o.OrderTransfer.OriginTruckEndMileage)
                        {
                            TempData["message"] += "Odometer readings invalid (origin > transfer)";
                        }
                        if (o.TruckID == o.OrderTransfer.OriginTruckID // same truck
                            && o.OrderTransfer.OriginTruckEndMileage > o.OrderTransfer.DestTruckStartMileage)
                        {
                            TempData["message"] += "Odometer readings invalid (transfer > transfer)";
                        }
                        if (o.OrderTransfer.DestTruckStartMileage > o.DestTruckMileage)
                        {
                            TempData["message"] += "Odometer readings invalid (transfer > destination)";
                        }
                    }
                }

                // Save delivery
                order.StatusID = (int) OrderStatus.STATUS.Delivered;
                order.DeliverPrintStatusID = (int) PrintStatus.STATUS.Handwritten;
                if (order.PickupPrintStatus.IsCompleted == false)
                {
                    order.PickupPrintStatusID = (int) PrintStatus.STATUS.Handwritten; // ensure pickup print status is valid
                }
                // set DeliverLastChangeDateUTC = LastChangeDateUTC = UtcNow
                db.SaveChanges(User.Identity.Name, order.DeliverLastChangeDateUTC = DateTime.UtcNow);


                TempData["success"] = "Order #" + order.OrderNum + " was delivered";
                return RedirectToAction("Details", new { id = order.ID });
            }

            return View(o);
        }

        /**************************************************************************************************/
        /**  PAGE: ~/Orders/Undeliver/{id}                                                               **/
        /**  DESCRIPTION: Returns an order to Picked Up status                                           **/
        /**************************************************************************************************/
        [Authorize(Roles = "editOrders")]
        public ActionResult Undeliver(int id)
        {
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                TempData["error"] = "Order not found!";
                return RedirectToAction("Search");
            }
            if (hasAccess(order) == false)
            {
                TempData["error"] = "You do not have access to this order!";
                return RedirectToAction("Search");
            }
            if (order.Rejected)
            {
                TempData["error"] = "Cannot edit delivery for a rejected order!  Please unreject order instead.";
                return RedirectToAction("Details", new { id = id });
            }
            if (order.StatusID != (int)OrderStatus.STATUS.Delivered)
            {
                TempData["error"] = "Invalid status.  Cannot edit delivery for this order!";
                return RedirectToAction("Details", new { id = id });
            }

            order.DeliverPrintStatusID = (int) PrintStatus.STATUS.NotFinalized;
            order.StatusID = (int) OrderStatus.STATUS.PickedUp;
            // set PickupLastChangeDateUTC = LastChangeDateUTC = UtcNow
            db.SaveChanges(User.Identity.Name, order.PickupLastChangeDateUTC = DateTime.UtcNow);

            TempData["success"] = "Order #" + order.OrderNum + " successfully returned to Picked Up status";
            return RedirectToAction("Details", new { id = order.ID });
        }


        /**************************************************************************************************/
        /**  PAGE: ~/Orders/Transfer/{id}                                                                **/
        /**  DESCRIPTION: Transfer an order to a new driver/truck                                        **/
        /**************************************************************************************************/
        [Authorize(Roles = "transferOrders")]
        public ActionResult Transfer(int id) 
        { 
            ViewBag.Title = "Order Transfer";
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                TempData["error"] = "Order not found!";
                return RedirectToAction("Search");
            }
            if (hasAccess(order) == false)
            {
                TempData["error"] = "You do not have access to this order!";
                return RedirectToAction("Search");
            }
            if (order.canEdit == false)
            {
                TempData["error"] = "Cannot edit a settled/audited order!";
                return RedirectToAction("Details", new { id = id });
            }
            if (order.StatusID != (int)OrderStatus.STATUS.PickedUp)
            {
                TempData["error"] = "Order must be in Picked Up status to be transferred!";
                return RedirectToAction("Details", new { id = id });
            }
            if (order.IsTransfer) 
            {
                TempData["error"] = "Cannot transfer an existing transfer!";
                return RedirectToAction("Details", new { id = id });
            }

            ViewBag.OrderNum = order.OrderNum;
            ViewBag.CurrentDriver = order.Driver.FullName;
            ViewBag.CurrentTruck = order.Truck.IDNumber;
            int myTerminalID = Converter.ToInt32(Profile.GetPropertyValue("TerminalID"));
            ViewBag.DestDriverID = DriversController.getEligibleDriversSSL(order.CarrierID, myTerminalID, null, null, null, order.OrderDate ?? order.DueDate, false);

            ViewBag.DestTruckID = new SelectList(db.Trucks.Where(t => t.CarrierID == order.CarrierID && t.DeleteDateUTC == null
                    && (myTerminalID <= 0 || myTerminalID == t.TerminalID || t.TerminalID == null)).OrderBy(t => t.IDNumber), "ID", "IDNumber");

            return View(new OrderTransferViewModel(order.ID));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "transferOrders")]
        public ActionResult Transfer(OrderTransferViewModel transfer)
        {
            Order order = db.Orders.Find(transfer.OrderID);
            ViewBag.OrderNum = order.OrderNum;
            ViewBag.CurrentDriver = order.Driver.FullName;
            ViewBag.CurrentTruck = order.Truck.IDNumber;
            int myTerminalID = Converter.ToInt32(Profile.GetPropertyValue("TerminalID"));
            ViewBag.DestDriverID = DriversController.getEligibleDriversSSL(order.CarrierID, myTerminalID, null, null, null, order.OrderDate ?? order.DueDate, false, transfer.DestDriverID);
            ViewBag.DestTruckID = new SelectList(db.Trucks.Where(t => t.CarrierID == order.CarrierID && t.DeleteDateUTC == null
                        && (myTerminalID <= 0 || myTerminalID == t.TerminalID || t.TerminalID == null)).OrderBy(t => t.IDNumber), "ID", "IDNumber", transfer.DestTruckID);

            if (ModelState.IsValid)
            {
                if (transfer.DestDriverID == order.DriverID)
                {
                    ViewBag.Error = "Cannot set new driver to the existing driver!";
                    return View(transfer);
                }
                if (order.MileageRequired())
                {
                    if (transfer.OriginTruckEndMileage == null)
                    {
                        ViewBag.Error = "Current truck mileage is required";
                        return View(transfer);
                    }
                    // ensure that truck mileage has increased
                    else if (transfer.OriginTruckEndMileage <= order.OriginTruckMileage)
                    {
                        ViewBag.Error = "Invalid mileage (must be greater than current odometer on record)";
                        return View(transfer);
                    }
                }

                if (transfer.PercentComplete == null || transfer.PercentComplete < 0 || transfer.PercentComplete > 100)
                {
                    // require a percentage to be set
                    ViewBag.Error = "Percentage must be set";
                    return View(transfer);
                }

                // Use stored procedure to save transfer
                using (SSDB DCDB = new SSDB())
                {
                    using (System.Data.SqlClient.SqlCommand cmd = DCDB.BuildCommand("spOrderTransfer"))
                    {
                        cmd.Parameters["@OrderID"].Value = transfer.OrderID;
                        cmd.Parameters["@DestDriverID"].Value = transfer.DestDriverID;
                        if (transfer.DestTruckID != null)
                            cmd.Parameters["@DestTruckID"].Value = transfer.DestTruckID;
                        cmd.Parameters["@OriginTruckEndMileage"].Value = transfer.OriginTruckEndMileage ?? 0;
                        cmd.Parameters["@PercentComplete"].Value = transfer.PercentComplete;
                        cmd.Parameters["@Notes"].Value = transfer.Notes ?? "";
                        cmd.Parameters["@UserName"].Value = User.Identity.Name;
                        cmd.ExecuteNonQuery();
                    }
                }

                TempData["success"] = "Order #" + order.OrderNum + " was successfully transferred";
                return RedirectToAction("Details", new { id = transfer.OrderID });
            }
            return View(transfer);
        }

        
        /**************************************************************************************************/
        /**  PAGE: ~/Orders/EditTransfer/{id}                                                            **/
        /**  DESCRIPTION: Edit some details for a transferred order, even after audit                    **/
        /**************************************************************************************************/
        [Authorize(Roles = "transferOrders")]
        public ActionResult EditTransfer(int id) 
        { 
            ViewBag.Title = "Edit Order Transfer";
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                TempData["error"] = "Order not found!";
                return RedirectToAction("Search");
            }
            if (hasAccess(order) == false)
            {
                TempData["error"] = "You do not have access to this order!";
                return RedirectToAction("Search");
            }
            if (order.IsTransfer == false)
            {
                TempData["error"] = "Order is not transferred.  Cannot edit!";
                return RedirectToAction("Details", new { id = id });
            }

            ViewBag.OrderNum = order.OrderNum;
            ViewBag.OriginDriver = order.OrderTransfer.OriginDriver.FullName;
            ViewBag.OriginTruck = order.OrderTransfer.OriginTruck.IDNumber;
            ViewBag.DestDriver = order.Driver.FullName;
            ViewBag.DestTruck = order.Truck.IDNumber;

            OrderTransferViewModel transfer = new OrderTransferViewModel(order.ID);
            transfer.PercentComplete = order.OrderTransfer.PercentComplete;
            transfer.Notes = order.OrderTransfer.Notes;
            return View(transfer);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "transferOrders")]
        public ActionResult EditTransfer(OrderTransferViewModel transfer)
        {
            Order order = db.Orders.Find(transfer.OrderID);
            ViewBag.OrderNum = order.OrderNum;
            ViewBag.OriginDriver = order.OrderTransfer.OriginDriver.FullName;
            ViewBag.OriginTruck = order.OrderTransfer.OriginTruck.IDNumber;
            ViewBag.DestDriver = order.Driver.FullName;
            ViewBag.DestTruck = order.Truck.IDNumber;

            if (ModelState.IsValid)
            {
                order.OrderTransfer.PercentComplete = transfer.PercentComplete;
                order.OrderTransfer.Notes = transfer.Notes;
        /*
                    if (order.MileageRequired())
                    {
                        if (order.OriginTruckMileage == null || order.OriginTruckEndMileage == null) { /* mileage required MUST CHECK STATUS TOO * / }
                        if (order.OriginTruckMileage > order.OriginTruckEndMileage) { /* Odometer invalid * / }
                        // warning if loaded miles warning % threshold exceeded
                    }
                    // should simulate transfer complete too?
                    transfercomplete = true
                        // if transfer check against desttruckstartmileage too 
                        // if ordertransfer origintruckendmiles req
                        // if ordertransfer complete desttruckstartmiles req

         */
                db.SaveChanges(User.Identity.Name);

                TempData["success"] = "Order #" + order.OrderNum + " was successfully updated";
                return RedirectToAction("Details", new { id = transfer.OrderID });
            }
            return View(transfer);
        }

        
        /**************************************************************************************************/
        /**  PAGE: ~/Orders/Untransfer/{id}                                                              **/
        /**  DESCRIPTION: Deletes a transfer for an order.  The order is returned to the original driver **/
        /**          and truck                                                                           **/
        /**************************************************************************************************/
        [Authorize(Roles = "transferOrders")]
        public ActionResult Untransfer(int id)
        {
            Order order = db.Orders.Find(id);

            if (order == null)
            {
                TempData["error"] = "Order not found!";
                return RedirectToAction("Search");
            }
            if (hasAccess(order) == false)
            {
                TempData["error"] = "You do not have access to this order!";
                return RedirectToAction("Search");
            }
            if (order.canEdit == false)
            {
                TempData["error"] = "Cannot edit a settled/audited order!";
                return RedirectToAction("Details", new { id = order.ID });
            }
            if (order.IsTransfer == false)
            {
                TempData["error"] = "Order is not transferred.  Cannot undo!";
                return RedirectToAction("Details", new { id = order.ID });
            }
            // set driver/truck back to origin
            order.DriverID = order.OrderTransfer.OriginDriverID;
            order.TruckID = order.OrderTransfer.OriginTruckID;
            // delete record from tblordertransfer
            order.OrderTransfer = null; 

            db.SaveChanges(User.Identity.Name);


            TempData["success"] = "Order #" + order.OrderNum + " was successfully untransferred and returned to the original driver";
            return RedirectToAction("Details", new { id = order.ID });
        }


        /**************************************************************************************************/
        /**  PAGE: ~/Orders/Reassign/{id}                                                                **/
        /**  DESCRIPTION: Reassign an order to a new driver.  This will clone the ticket for the new     **/
        /**            driver and deactivate the original ticket.                                        **/
        /**************************************************************************************************/
        [Authorize(Roles = "editOrders")]
        public ActionResult Reassign(int? id) 
        { 
            ViewBag.Title = "Reassign Order";

            Order order = db.Orders.Find(id);
            if (order == null)
            {
                TempData["error"] = "Order not found!";
                return RedirectToAction("Search");
            }
            if (hasAccess(order) == false)
            {
                TempData["error"] = "You do not have access to this order!";
                return RedirectToAction("Search");
            }
            if (order.canEdit == false)
            {
                TempData["error"] = "Cannot edit a settled/audited order!";
                return RedirectToAction("Details", new { id = order.ID });
            }
            if (order.StatusID != (int) OrderStatus.STATUS.Dispatched && order.StatusID != (int) OrderStatus.STATUS.Accepted)
            {
                // cannot be picked up otherwise transfer
                TempData["error"] = "Only Dispatched or Accepted orders can be reassigned!";
                return RedirectToAction("Details", new { id = order.ID });
            }

            // set page variables
            ViewBag.OrderNum = order.OrderNum;
            ViewBag.Driver = order.Driver.FullName;
            //ViewBag.DriverID = new SelectList(db.Drivers.Where(d => d.CarrierID == order.CarrierID && d.DeleteDateUTC == null).OrderBy(d => d.LastName).ThenBy(d => d.FirstName), "ID", "FullName");
            ViewBag.DriverID = DriversController.getEligibleDriversSSL(order.CarrierID, null, null, null, null, order.OrderDate ?? order.DueDate, order.Origin.H2S);
            ViewBag.Origin = order.Origin.Name;
            ViewBag.OriginH2S = order.Origin.H2S;
            ViewBag.Destination = order.Destination.Name;

            return View(order);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "editOrders")]
        public ActionResult Reassign([Bind(Include = "ID,DriverID")] Order o)
        {
            // set page variables in case reassign fails
            Order order = db.Orders.Find(o.ID);
            ViewBag.OrderNum = order.OrderNum;
            ViewBag.Driver = order.Driver.FullName;
            //ViewBag.DriverID = new SelectList(db.Drivers.Where(d => d.CarrierID == order.CarrierID && d.DeleteDateUTC == null).OrderBy(d => d.LastName).ThenBy(d => d.FirstName), "ID", "FullName", o.DriverID);
            ViewBag.DriverID = DriversController.getEligibleDriversSSL(order.CarrierID, null, null, null, null, order.OrderDate ?? order.DueDate, order.Origin.H2S, o.DriverID);
            ViewBag.Origin = order.Origin.Name;
            ViewBag.OriginH2S = order.Origin.H2S;
            ViewBag.Destination = order.Destination.Name;

            if (o.DriverID == null || o.DriverID == 0)
            {
                ViewBag.Error = "A new driver must be assigned!";
                return View(o);
            }
            if (o.DriverID == order.DriverID)
            {
                ViewBag.Error = "Cannot set new driver to the existing driver!";
                return View(o);
            }
            order.DriverID = o.DriverID;
            db.SaveChanges(User.Identity.Name);
            // The order trigger (trigOrder_IU) creates a clone and links the old and new with a reassign key.  Need to reload to fetch value
            db.Entry(order).Reload();

            Order neworder = db.Orders.Where(x => x.DriverID == o.DriverID && x.ReassignKey == order.ReassignKey && x.DeleteDateUTC == null).OrderByDescending(x => x.CreateDateUTC).FirstOrDefault();
            TempData["success"] = "The order was reassigned to " + neworder.Driver.FullName + " (new order #" + neworder.OrderNum + ")";
            return RedirectToAction("Details", new { id = neworder.ID });
        }
        // view reassign(s)
        // unreassign? probably only re-reassign
        // what about reassigning a reroute/transfer


        /**************************************************************************************************/
        /**  PAGE: ~/Orders/Reroute/{id}                                                                 **/
        /**  DESCRIPTION: Reroute an order to a new destination.  Can be used multiple times for         **/
        /**            multiple reroutes.                                                                **/
        /**************************************************************************************************/
        [Authorize(Roles = "rerouteOrders")]
        public ActionResult Reroute(int id)
        {
            ViewBag.Title = "Order Edit";
            ViewBag.Subtitle = "Reroute Order";

            Order order = db.Orders.Find(id);
            if (order == null)
            {
                TempData["error"] = "Order not found!";
                return RedirectToAction("Search");
            }
            if (hasAccess(order) == false)
            {
                TempData["error"] = "You do not have access to this order!";
                return RedirectToAction("Search");
            }
            if (order.canEdit == false)
            {
                TempData["error"] = "Cannot edit a settled/audited order!";
                return RedirectToAction("Details", new { id = order.ID });
            }
            if (order.StatusID != (int) OrderStatus.STATUS.PickedUp)
            {
                TempData["error"] = "Order must be in Picked Up status to be rerouted!";
                return RedirectToAction("Details", new { id = order.ID });
            }

            ViewBag.OrderNum = order.OrderNum;
            ViewBag.DestinationID = new SelectList(db.Destinations.Where(d => d.DeleteDateUTC == null).OrderBy(d => d.Name), "ID", "Name");
            ViewBag.Origin = order.Origin.Name;
            ViewBag.OriginH2S = order.Origin.H2S;
            ViewBag.Destination = order.Destination.Name;

            return View(new OrderRerouteViewModel(order.ID));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "rerouteOrders")]
        public ActionResult Reroute(OrderRerouteViewModel reroute)
        {
            Order order = db.Orders.Find(reroute.OrderID);

            ViewBag.OrderNum = order.OrderNum;
            ViewBag.DestinationID = new SelectList(db.Destinations.Where(d => d.DeleteDateUTC == null).OrderBy(d => d.Name), "ID", "Name", reroute.DestinationID);
            ViewBag.Origin = order.Origin.Name;
            ViewBag.OriginH2S = order.Origin.H2S;
            ViewBag.Destination = order.Destination.Name;

            if (ModelState.IsValid)
            {
                if (reroute.DestinationID == order.DestinationID)
                {
                    ViewBag.Error = "Cannot reroute order to its current destination!";
                    return View(reroute);
                }

                // Use stored procedure to save reroute
                using (SSDB DCDB = new SSDB())
                {
                    using (System.Data.SqlClient.SqlCommand cmd = DCDB.BuildCommand("spRerouteOrder"))
                    {
                        cmd.Parameters["@OrderID"].Value = reroute.OrderID;
                        cmd.Parameters["@NewDestinationID"].Value = reroute.DestinationID;
                        cmd.Parameters["@UserName"].Value = User.Identity.Name;
                        cmd.Parameters["@Notes"].Value = reroute.Notes ?? "";
                        cmd.ExecuteNonQuery();
                    }
                }

                TempData["success"] = "Order #" + order.OrderNum + " was successfully rerouted";
                return RedirectToAction("Details", new { id = order.ID });
            }
            return View(reroute);
        }
        // edit reroute? 
        // view reroute(s)?


        /**************************************************************************************************/
        /**  PAGE: ~/Orders/Unreroute/{id}                                                               **/
        /**  DESCRIPTION: Undoes the most recent reroute for a given order and returns it to the         **/
        /**                     previous destination                                                     **/
        /**************************************************************************************************/
        [Authorize(Roles = "rerouteOrders")]
        public ActionResult Unreroute(int id)
        {
            Order order = db.Orders.Find(id);

            if (order == null)
            {
                TempData["error"] = "Order not found!";
                return RedirectToAction("Search");
            }
            if (hasAccess(order) == false)
            {
                TempData["error"] = "You do not have access to this order!";
                return RedirectToAction("Search");
            }
            if (order.canEdit == false)
            {
                TempData["error"] = "Cannot edit a settled/audited order!";
                return RedirectToAction("Details", new { id = order.ID });
            }
            OrderReroute reroute = db.OrderReroutes.Where(r => r.OrderID == order.ID).OrderByDescending(r => r.RerouteDate).FirstOrDefault();
            if (reroute == null)
            {
                TempData["error"] = "There is no reroute for this order!";
                return RedirectToAction("Details", new { id = order.ID });
            }

            // set destination back to previous
            order.DestinationID = reroute.PreviousDestinationID;
            // delete record from tblOrderReroute
            db.OrderReroutes.Remove(reroute);

            db.SaveChanges(User.Identity.Name);


            TempData["success"] = "The reroute for order #" + order.OrderNum + " was successfully removed and routed to the previous destination";
            return RedirectToAction("Details", new { id = order.ID });
        }

        //[Authorize(Roles = "deleteOrders")]
        //public ActionResult Delete() { }

        //[Authorize(Roles = "undeleteOrders")]
        //public ActionResult Undelete() { }

        //[Authorize(Roles = "viewOrderAudit")]
        //public ActionResult Audit() { }

        //[Authorize(Roles = "unauditOrders")]
        //public ActionResult Unaudit() { }

        //[Authorize(Roles = "viewOrderApproval")]
        //public ActionResult Approve() { }


        /**************************************************************************************************/
        /**  PAGE: ~/Orders/Decline/{id}                                                                 **/
        /**  DESCRIPTION: Declines a given order                                                         **/
        /**************************************************************************************************/
        [Authorize(Roles = "editOrders")]
        public ActionResult Decline(int? id)
        {
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                TempData["error"] = "Order not found!";
                return RedirectToAction("Search");
            }
            if (hasAccess(order) == false)
            {
                TempData["error"] = "You do not have access to this order!";
                return RedirectToAction("Search");
            }
            if (order.canEdit == false)
            {
                TempData["error"] = "Audited/Settled orders cannot be edited";
                return RedirectToAction("Details", new { id = order.ID });
            }
            if (   order.StatusID != (int) OrderStatus.STATUS.Generated
                && order.StatusID != (int) OrderStatus.STATUS.Assigned
                && order.StatusID != (int) OrderStatus.STATUS.Dispatched
                && order.StatusID != (int) OrderStatus.STATUS.Accepted)
            {
                TempData["error"] = "Only an order in Accepted status that has not yet been picked up can be declined";
                return RedirectToAction("Details", new { id = order.ID });
            }

            order.StatusID = (int) OrderStatus.STATUS.Declined;

            db.SaveChanges(User.Identity.Name);


            TempData["success"] = "Order #" + order.OrderNum + " was successfully declined.";
            return RedirectToAction("Details", new { id = order.ID });
        }


        /**************************************************************************************************/
        /**  PAGE: ~/Orders/Reject/{id}                                                                  **/
        /**  DESCRIPTION: Reject a given order                                                           **/
        /**************************************************************************************************/
        [Authorize(Roles = "editOrders")]
        public ActionResult Reject(int? id)
        {
            ViewBag.Title = "Reject Order";
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                TempData["error"] = "Order not found!";
                return RedirectToAction("Search");
            }
            if (hasAccess(order) == false)
            {
                TempData["error"] = "You do not have access to this order!";
                return RedirectToAction("Search");
            }
            if (order.canEdit == false)
            {
                TempData["error"] = "Audited/Settled orders cannot be edited";
                return RedirectToAction("Details", new { id = order.ID });
            }
            if (order.StatusID != (int) OrderStatus.STATUS.Accepted)
            {
                TempData["error"] = "Only an order in Accepted status that has not yet been picked up can be rejected";
                return RedirectToAction("Details", new { id = order.ID });
            }
            
            // set page variables
            ViewBag.OrderNum = order.OrderNum;
            ViewBag.Origin = order.Origin.Name;
            ViewBag.OriginH2S = order.Origin.H2S;
            ViewBag.Destination = order.Destination.Name;
            ViewBag.RejectReasonID = new SelectList(db.OrderRejectReasons.Where(r => r.DeleteDateUTC == null).OrderBy(r => r.Num), "ID", "NumDesc", order.RejectReasonID);

            return View(order);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "editOrders")]
        public ActionResult Reject([Bind(Include = "ID,OriginArriveTime,ChainUp,OriginDepartTime,RejectReasonID,RejectNotes")] Order o)
        {
            Order order = db.Orders.Find(o.ID);
            // set page variables if reject fails
            ViewBag.OrderNum = order.OrderNum;
            ViewBag.Origin = order.Origin.Name;
            ViewBag.OriginH2S = order.Origin.H2S;
            ViewBag.Destination = order.Destination.Name;
            ViewBag.RejectReasonID = new SelectList(db.OrderRejectReasons.Where(r => r.DeleteDateUTC == null).OrderBy(r => r.Num), "ID", "NumDesc", o.RejectReasonID);

            int activetickets = db.OrderTickets.Where(t => t.OrderID == order.ID && !t.Rejected && t.DeleteDateUTC == null).Count();
            if (activetickets > 0)
            {
                ViewBag.Error = "A rejected order cannot have a valid ticket!  Please delete/reject ticket(s).";
                return View(o);
            }

            OrderRejectReason reject = db.OrderRejectReasons.Find(o.RejectReasonID);
            if (reject == null)
            {
                ViewBag.Error = "A reject reason must be selected!";
                return View(o);
            }
            else if (reject.RequireNotes == true && String.IsNullOrEmpty(o.RejectNotes))
            {
                ViewBag.Error = "Reject notes are required for this reject reason!";
                return View(o);
            }
            if (o.OriginDepartTimeUTC == null || o.OriginArriveTimeUTC == null || o.OriginArriveTimeUTC > o.OriginDepartTimeUTC)
            {
                ViewBag.Error = "Valid origin arrive and depart times are required!";
            	return View(o);
            }
            if (o.RejectPhotoRequired() && order.RejectPhoto == null)
            {
                TempData["message"] += "A reject photo should accompany this order.  ";
            }

            order.Rejected = true;
            order.RejectReasonID = o.RejectReasonID;
            order.RejectNotes = o.RejectNotes;
            order.OriginArriveTimeUTC = o.OriginArriveTimeUTC;
            order.OriginDepartTimeUTC = o.OriginDepartTimeUTC;

            // ensure the delivery is marked as "completed" if rejected
            order.StatusID = (int) OrderStatus.STATUS.Delivered;
            order.PickupPrintStatusID = (int) PrintStatus.STATUS.Handwritten;
            order.DeliverPrintStatusID = (int) PrintStatus.STATUS.Handwritten;

            // clear out details? or is data ignored? clear/delete tickets?

            db.SaveChanges(User.Identity.Name);


            TempData["success"] = "Order #" + order.OrderNum + " was successfully rejected.";
            return RedirectToAction("Details", new { id = order.ID });
        }

        /**************************************************************************************************/
        /**  PAGE: ~/Orders/Unreject/{id}                                                                **/
        /**  DESCRIPTION: Unreject a given order                                                         **/
        /**************************************************************************************************/
        [Authorize(Roles = "editOrders")]
        public ActionResult Unreject(int id)
        {
            Order order = db.Orders.Find(id);

            if (order == null)
            {
                TempData["error"] = "Order not found!";
                return RedirectToAction("Search");
            }
            if (hasAccess(order) == false)
            {
                TempData["error"] = "You do not have access to this order!";
                return RedirectToAction("Search");
            }
            if (order.canEdit == false)
            {
                TempData["error"] = "Cannot edit a settled/audited order!";
                return RedirectToAction("Details", new { id = order.ID });
            }
            if (order.Rejected == false || order.StatusID != (int) OrderStatus.STATUS.Delivered)
            {
                TempData["error"] = "Only a rejected order in Delivered status can be unrejected!";
                return RedirectToAction("Details", new { id = order.ID });
            }

            order.DeliverPrintStatusID = (int) PrintStatus.STATUS.NotFinalized;
            order.PickupPrintStatusID = (int) PrintStatus.STATUS.NotFinalized;
            order.StatusID = (int) OrderStatus.STATUS.Accepted;

            // clear out reason and notes
            order.Rejected = false;
            order.RejectReasonID = null;
            order.RejectNotes = null;

            db.SaveChanges(User.Identity.Name);


            TempData["success"] = "Order #" + order.OrderNum + " was unrejected";
            return RedirectToAction("Details", new { id = order.ID });
        }


        /**************************************************************************************************/
        /**  PAGE: ~/Orders/Edit/{id}                                                                    **/
        /**  DESCRIPTION: Ability to edit an order.  This is designed from an administrative view,       **/
        /**       with several fields editable.  Specific tasks should be done separately for lower-     **/
        /**       level manipulation of orders (i.e. Accept, Transfer, Reject).                          **/
        /**************************************************************************************************/
        [Authorize(Roles = "editOrders")]
        public ActionResult Edit(int? id)
        {
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                TempData["error"] = "Order not found!";
                return RedirectToAction("Search");
            }
            if (hasAccess(order) == false)
            {
                TempData["error"] = "You do not have access to this order!";
                return RedirectToAction("Search");
            }
            if (order.Deleted)
            {
                TempData["error"] = "Deleted orders cannot be edited!  Please undelete.";
                return RedirectToAction("Details", new { id = order.ID });
            }
            if (order.canEdit == false)
            {
                TempData["error"] = "Audited/Settled orders cannot be edited";
                return RedirectToAction("Details", new { id = order.ID });
            }

            // what data should be filtered if any? carrier? by carrier?
            ViewBag.StatusID = new SelectList(db.OrderStatus, "ID", "Name", order.StatusID);
            ViewBag.TicketTypeID = new SelectList(db.TicketTypes, "ID", "Name", order.TicketTypeID);
            ViewBag.PriorityID = new SelectList(db.Priorities, "ID", "PriorityNum", order.PriorityID);
            ViewBag.ProductID = new SelectList(db.Products.OrderBy(p => p.Name), "ID", "Name", order.ProductID);
            ViewBag.RejectReasonID = new SelectList(db.OrderRejectReasons.Where(r => r.DeleteDateUTC == null || r.ID == order.RejectReasonID).OrderBy(r => r.Num), "ID", "NumDesc", order.RejectReasonID);

            ViewBag.OriginID = new SelectList(db.Origins.Where(o => o.DeleteDateUTC == null || o.ID == order.OriginID).OrderBy(o => o.Name), "ID", "Name", order.OriginID);
            ViewBag.OriginTankID = new SelectList(db.OriginTanks.Where(o => o.DeleteDateUTC == null && o.OriginID == order.OriginID || o.ID == order.OriginTankID).OrderBy(o => o.TankNum), "ID", "TankNumDesc", order.OriginTankID);
            ViewBag.OriginWaitReasonID = new SelectList(db.OriginWaitReasons.Where(r => r.DeleteDateUTC == null || r.ID == order.OriginWaitReasonID).OrderBy(r => r.Num), "ID", "NumDesc", order.OriginWaitReasonID);

            // no way to use LINQ to get valid destinations so use function that uses a database lookup origin->shipper->dest
            ViewBag.DestinationID = OriginsController.getValidDestinations(order.OriginID, order.CustomerID, order.ProductID, order.Destination);
            // dest change allowed, see reroute for rerouting an order
            ViewBag.DestUOM = order.DestUOM.Abbrev;
            ViewBag.DestUOMStep = order.DestUOM.getStep();
            ViewBag.DestUOMFormat = order.DestUOM.getFormat();
            ViewBag.DestWaitReasonID = new SelectList(db.DestinationWaitReasons.Where(r => r.DeleteDateUTC == null || r.ID == order.DestWaitReasonID).OrderBy(r => r.Num), "ID", "NumDesc", order.DestWaitReasonID);
            ViewBag.ConsigneeID = new SelectList(
                        (from c in db.Consignees join dc in db.DestinationConsignees on c.ID equals dc.ConsigneeID where dc.DestinationID == order.DestinationID
                         select c).ToList(), "ID", "Name", order.ConsigneeID);

            ViewBag.Driver = (order.Driver == null) ? "" : order.Driver.FullName; // driver change not allowed, see transfer or reassign
            if (order.IsTransfer)
                ViewBag.OrigDriver = order.OrderTransfer.OriginDriver.FullName;
            ViewBag.TruckID = new SelectList(db.Trucks.Where(t => t.CarrierID == order.CarrierID && t.DeleteDateUTC == null || t.ID == order.TruckID).OrderBy(t => t.IDNumber), "ID", "IDNumber", order.TruckID);
            ViewBag.TrailerID = new SelectList(db.Trailers.Where(t => t.CarrierID == order.CarrierID && t.DeleteDateUTC == null || t.ID == order.TrailerID).OrderBy(t => t.IDNumber), "ID", "IDNumber", order.TrailerID);
            ViewBag.Trailer2ID = new SelectList(db.Trailers.Where(t => t.CarrierID == order.CarrierID && t.DeleteDateUTC == null || t.ID == order.Trailer2ID).OrderBy(t => t.IDNumber), "ID", "IDNumber", order.Trailer2ID);

            ViewBag.OperatorID = new SelectList(db.Operators.Where(o => o.DeleteDateUTC == null || o.ID == order.OperatorID).OrderBy(o => o.Name), "ID", "Name", order.OperatorID);
            //ViewBag.CarrierID = new SelectList(db.Carriers.Where(c => c.DeleteDateUTC == null || c.ID == order.CarrierID).OrderBy(c => c.Name), "ID", "Name", order.CarrierID);
            ViewBag.Carrier = (order.Carrier == null) ? "" : order.Carrier.Name; // disable carrier change for now
            ViewBag.ProducerID = new SelectList(db.Producers.Where(p => p.DeleteDateUTC == null || p.ID == order.ProducerID).OrderBy(p => p.Name), "ID", "Name", order.ProducerID);
            ViewBag.CustomerID = new SelectList(db.Customers.OrderBy(c => c.Name), "ID", "Name", order.CustomerID);
            ViewBag.PumperID = new SelectList(db.Pumpers.OrderBy(c => c.LastName).ThenBy(c => c.FirstName), "ID", "FullName", order.PumperID);

            return View(order);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "editOrders")]
        public ActionResult Edit([Bind(Include = "ID,OrderNum,StatusID,TicketTypeID,DispatchConfirmNum,DueDate,OrderDate,PriorityID,ProductID,Rejected,RejectReasonID,RejectNotes," +
                    "OriginID,OriginTankID,OriginArriveTime,OriginDepartTime,OriginWaitReasonID,OriginWaitNotes,OriginBOLNum,OriginTruckMileage,ChainUp,PickupDriverNotes,OriginSulfurContent," +
                    "DestinationID,ConsigneeID,DestArriveTime,DestDepartTime,DestWaitReasonID,DestWaitNotes,DestBOLNum,DestGrossUnits,DestNetUnits,DestOpenMeterUnits,DestCloseMeterUnits,DestProductTemp,DestProductBSW," + 
                    "DestProductGravity,DestWeightGrossUnits,DestWeightTareUnits,DestWeightNetUnits,DestRailCarNum,DestTrailerWaterCapacity,DestRackBay,DestTruckMileage,DestChainup,DeliverDriverNotes," +
                    "CustomerID,CarrierID,DriverID,TruckID,TrailerID,Trailer2ID,DispatchNotes,AuditNotes,JobNumber,OperatorID,ProducerID")] Order o)
        {
            Order order = db.Orders.Find(o.ID);

            // Set page variables if edit fails
            ViewBag.StatusID = new SelectList(db.OrderStatus, "ID", "Name", o.StatusID);
            ViewBag.TicketTypeID = new SelectList(db.TicketTypes, "ID", "Name", o.TicketTypeID);
            ViewBag.PriorityID = new SelectList(db.Priorities, "ID", "PriorityNum", o.PriorityID);
            ViewBag.ProductID = new SelectList(db.Products.OrderBy(p => p.Name), "ID", "Name", o.ProductID);
            ViewBag.RejectReasonID = new SelectList(db.OrderRejectReasons.Where(r => r.DeleteDateUTC == null || r.ID == o.RejectReasonID).OrderBy(r => r.Num), "ID", "NumDesc", o.RejectReasonID);

            ViewBag.OriginID = new SelectList(db.Origins.Where(x => x.DeleteDateUTC == null || x.ID == o.OriginID).OrderBy(x => x.Name), "ID", "Name", o.OriginID);
            ViewBag.OriginTankID = new SelectList(db.OriginTanks.Where(x => x.DeleteDateUTC == null && x.OriginID == o.OriginID || x.ID == o.OriginTankID).OrderBy(x => x.TankNum), "ID", "TankNumDesc", o.OriginTankID);
            ViewBag.OriginWaitReasonID = new SelectList(db.OriginWaitReasons.Where(r => r.DeleteDateUTC == null || r.ID == o.OriginWaitReasonID).OrderBy(r => r.Num), "ID", "NumDesc", o.OriginWaitReasonID);

            // no way to use LINQ to get valid destinations so use function that uses a database lookup origin->shipper->dest
            ViewBag.DestinationID = OriginsController.getValidDestinations(order.OriginID, order.CustomerID, order.ProductID, order.Destination);
            // dest change allowed, see reroute for rerouting an order
            ViewBag.DestUOM = order.DestUOM.Abbrev;
            ViewBag.DestUOMStep = order.DestUOM.getStep();
            ViewBag.DestUOMFormat = order.DestUOM.getFormat();
            ViewBag.DestWaitReasonID = new SelectList(db.DestinationWaitReasons.Where(r => r.DeleteDateUTC == null || r.ID == o.DestWaitReasonID).OrderBy(r => r.Num), "ID", "NumDesc", o.DestWaitReasonID);
            ViewBag.ConsigneeID = new SelectList(
                        (from c in db.Consignees join dc in db.DestinationConsignees on c.ID equals dc.ConsigneeID where dc.DestinationID == order.DestinationID
                         select c).ToList(), "ID", "Name", o.ConsigneeID);

            ViewBag.Driver = (order.Driver == null) ? "" : order.Driver.FullName; // driver change not allowed, see transfer or reassign
            if (order.IsTransfer)
                ViewBag.OrigDriver = order.OrderTransfer.OriginDriver.FullName;
            ViewBag.TruckID = new SelectList(db.Trucks.Where(t => t.CarrierID == o.CarrierID && t.DeleteDateUTC == null || t.ID == o.TruckID).OrderBy(t => t.IDNumber), "ID", "IDNumber", o.TruckID);
            ViewBag.TrailerID = new SelectList(db.Trailers.Where(t => t.CarrierID == o.CarrierID && t.DeleteDateUTC == null || t.ID == o.TrailerID).OrderBy(t => t.IDNumber), "ID", "IDNumber", o.TrailerID);
            ViewBag.Trailer2ID = new SelectList(db.Trailers.Where(t => t.CarrierID == o.CarrierID && t.DeleteDateUTC == null || t.ID == o.Trailer2ID).OrderBy(t => t.IDNumber), "ID", "IDNumber", o.Trailer2ID);

            ViewBag.OperatorID = new SelectList(db.Operators.Where(x => x.DeleteDateUTC == null || o.ID == o.OperatorID).OrderBy(x => x.Name), "ID", "Name", o.OperatorID);
            //ViewBag.CarrierID = new SelectList(db.Carriers.Where(c => c.DeleteDateUTC == null || c.ID == o.CarrierID).OrderBy(c => c.Name), "ID", "Name", o.CarrierID);
            ViewBag.Carrier = (order.Carrier == null) ? "" : order.Carrier.Name; // carrier change not allowed currently
            ViewBag.ProducerID = new SelectList(db.Producers.Where(p => p.DeleteDateUTC == null || p.ID == o.ProducerID).OrderBy(p => p.Name), "ID", "Name", o.ProducerID);
            ViewBag.CustomerID = new SelectList(db.Customers.OrderBy(c => c.Name), "ID", "Name", o.CustomerID);
            ViewBag.PumperID = new SelectList(db.Pumpers.OrderBy(c => c.LastName).ThenBy(c => c.FirstName), "ID", "FullName", o.PumperID);



            if (ModelState.IsValid)
            {
                // CHECK BASIC INFO
                // ------------------------------------------------------------------------------
                OrderStatus status = db.OrderStatus.Find(o.StatusID);
                if (status == null)
                {
                    ViewBag.Error = "Status is required!";
                	return View("Edit", o);
                }
                order.StatusID = o.StatusID; // may be overwritten by the Order trigger (trigOrder_IU)
                if (o.StatusID == (int)OrderStatus.STATUS.Delivered || o.StatusID == (int)OrderStatus.STATUS.Audited)
                {
                    order.PickupPrintStatusID = (int)PrintStatus.STATUS.Printed;
                    order.DeliverPrintStatusID = (int) PrintStatus.STATUS.Printed;
                }
                else if (o.StatusID == (int)OrderStatus.STATUS.PickedUp)
                {
                    order.PickupPrintStatusID = (int) PrintStatus.STATUS.Printed;
                    order.DeliverPrintStatusID = (int) PrintStatus.STATUS.NotFinalized;
                }
                else
                {
                    order.PickupPrintStatusID = (int) PrintStatus.STATUS.NotFinalized;
                    order.DeliverPrintStatusID = (int) PrintStatus.STATUS.NotFinalized;
                }

                if (o.TicketTypeID == 0)
                {
                    ViewBag.Error = "Ticket type is required!";
                	return View("Edit", o);
                }
                // check if change ticket type allowed global setting NOT REQUIRED
                // Check if change ticket type will affect existing tickets
                if (db.OrderTickets.Where(t => t.OrderID == o.ID && t.DeleteDateUTC == null && t.TicketTypeID != o.TicketTypeID).Count() > 0)
                {
                    ViewBag.Error = "Tickets of a different type exists for this order.  Please delete to allow changing ticket type!";
                	return View("Edit", o);
                }
                order.TicketTypeID = o.TicketTypeID;

                if (o.DispatchConfirmNumRequired() && String.IsNullOrEmpty(o.DispatchConfirmNum))
                {
                    ViewBag.Error = "Shipper PO required for this order!";
                	return View("Edit", o);
                }
                order.DispatchConfirmNum = o.DispatchConfirmNum;
                order.JobNumber = o.JobNumber;
                order.DueDate = o.DueDate;
                order.OrderDate = o.OrderDate; // may get overwritten by the trigger or later changes to the order
                order.PriorityID = o.PriorityID;
                if (o.ProductID == 0)
                {
                    ViewBag.Error = "A product is required!";
                	return View("Edit", o);
                }
                order.ProductID = o.ProductID;

                // If rejected, make sure reason is set and check if notes required
                if (o.Rejected)
                {
                    OrderRejectReason reject = db.OrderRejectReasons.Find(o.RejectReasonID);
                    if (reject == null)
                    {
                        ViewBag.Error = "A reject reason must be selected for rejected orders!";
                    	return View("Edit", o);
                    }
                    else if (reject.RequireNotes == true && String.IsNullOrEmpty(o.RejectNotes))
                    {
                        ViewBag.Error = "Reject notes are required for this reject reason!";
                   		return View("Edit", o);
                    }
                    else if (o.OriginDepartTimeUTC == null || o.OriginArriveTimeUTC == null || o.OriginArriveTimeUTC > o.OriginDepartTimeUTC)
                    {
                        ViewBag.Error = "Valid origin arrive and depart times are required for a rejected order!";
                    	return View("Edit", o);
                    }

                    order.Rejected = true;
                    order.RejectReasonID = o.RejectReasonID;
                    order.RejectNotes = o.RejectNotes;
                    // ensure status?

                    if (o.RejectPhotoRequired() && order.RejectPhoto == null)
                    {
                        TempData["message"] += "A reject photo should accompany this order.  ";
                    }
                }
                else
                {
                    // Clear out reason and notes
                    order.Rejected = false;
                    order.RejectReasonID = null;
                    order.RejectNotes = null;
                }


                // CHECK ORIGIN INFO
                // ------------------------------------------------------------------------------
                Origin origin = db.Origins.Find(o.OriginID);
                if (origin == null)
                {
                    ViewBag.Error = "An origin is required!";
                	return View("Edit", o);
                }
                order.OriginID = o.OriginID;
                // Ticket type (TicketTypeID), producer (ProducerID), operator (OperatorID), pumper (PumperID), origin unit of measure (OriginUomID), (and quantities?) get updated by the Order trigger (trigOrder_IU) based on the origin
                if (o.OriginTankID != null)
                {
                    OriginTank tank = db.OriginTanks.Find(o.OriginTankID);
                    if (tank == null || tank.OriginID != o.OriginID)
                    {
                        ViewBag.Error = "Tank is not valid for this origin!";
                    	return View("Edit", o);
                    }
                }
                order.OriginTankID = o.OriginTankID;
                order.OperatorID = o.OperatorID;
                order.ProducerID = o.ProducerID;
                order.OriginBOLNum = o.OriginBOLNum;
                order.OriginTruckMileage = o.OriginTruckMileage;
                order.ChainUp = o.ChainUp;
                order.OriginSulfurContent = o.OriginSulfurContent;
                order.PickupDriverNotes = o.PickupDriverNotes;

                // Convert origin arrive and depart back to to UTC
                order.OriginArriveTimeUTC = o.OriginArriveTimeUTC; // automatically converts OriginArriveTime back to UTC
                order.OriginDepartTimeUTC = o.OriginDepartTimeUTC; // automatically converts OriginDepartTime back to UTC
                // Origin minutes (OriginMinutes) are calculated by the Order trigger (trigOrder_IU) based on arrive and depart times; needed here for check on wait time
                // OriginGrossUnits, OriginNetUnits, OriginGrossStdUnits, OriginBOLNum, CarrierTicketNum, OriginTankID, and OriginTankNum may be overwritten by the Ticket trigger (trigOrderTicket_IU)


                // Check order status to see if origin fields are required
                if (o.OriginInfoRequired())
                {
                    if (o.OriginDepartTimeUTC == null || o.OriginArriveTimeUTC == null || o.OriginArriveTimeUTC > o.OriginDepartTimeUTC)
                    {
                        ViewBag.Error = "Valid origin arrive and depart times are required for an order in " + status.Name + " status!";
                    	return View("Edit", o);
                    }
                    else
                    {
                        TimeSpan wait = o.OriginDepartTimeUTC.Value - o.OriginArriveTimeUTC.Value;
                        o.OriginMinutes = DBHelper.ToInt32(wait.TotalMinutes);
                    }

                    // If wait threshold exceeded during pickup, require origin wait reason and check if notes required
                    if (o.OriginWaitReasonRequired)
                    {
                        OriginWaitReason reason = db.OriginWaitReasons.Find(o.OriginWaitReasonID);
                        if (reason == null)
                        {
                            ViewBag.Error = "Origin wait reason is required!";
                        	return View("Edit", o);
                        }
                        if (reason.RequireNotes && String.IsNullOrEmpty(o.OriginWaitNotes))
                        {
                            ViewBag.Error = "Origin wait notes are required!";
                        	return View("Edit", o);
                        }
                        order.OriginWaitReasonID = o.OriginWaitReasonID;
                        order.OriginWaitNotes = o.OriginWaitNotes;
                    }
                    else
                    {
                        // Clear out reason and notes
                        order.OriginWaitReasonID = null;
                        order.OriginWaitNotes = null;
                    }

                    int activetickets = db.OrderTickets.Where(t => t.OrderID == o.ID && !t.Rejected && t.DeleteDateUTC == null).Count();
                    if (activetickets == 0 && o.Rejected == false)
                    {
                        ViewBag.Error = "A valid ticket must accompany an order in " + status.Name + " status!  Please add a ticket or reject this order.";
                    	return View("Edit", o);
                    }
                    else if (activetickets > 0 && o.Rejected)
                    {
                        ViewBag.Error = "A rejected order cannot have a valid ticket!  Please delete/reject ticket(s).";
                    	return View("Edit", o);
                    }
                    if (o.TruckID == null || o.TrailerID == null)
                    {
                        ViewBag.Error = "A truck and trailer are required for an order in " + status.Name + " status!";
                        return View("Edit", o);
                    }
                    if (o.MileageRequired() && o.OriginTruckMileage == null)
                    {
                        ViewBag.Error = "Mileage (odometer reading) at origin is required";
                    	return View("Edit", o);
                    }

                    if (!order.IsOriginQtyValid) // check against existing order tickets
                    {
                        ViewBag.Error = "Order exceeds maximum quantity allowed!";
                    	return View("Edit", o);
                    }

                    if (!order.IsOriginWeightValid) // check against existing order tickets
                    {
                        ViewBag.Error = "Order exceeds maximum weight allowed!";
                        return View("Edit", o);
                    }

                    if (o.OriginPhotoRequired() && order.OriginPhoto == null)
                    {
                        TempData["message"] += "An origin photo should accompany this order.  ";
                    }
                    if (o.OriginPhoto2Required() && order.OriginPhoto2 == null)
                    {
                        TempData["message"] += "An origin photo #2 should accompany this order.  ";
                    }
                    if (o.OriginConditionPhotosRequired() && (order.OriginArrivePhoto == null || order.OriginDepartPhoto == null))
                    {
                        TempData["message"] += "Origin arrive and depart photos should accompany this order.  ";
                    }
                }


                // CHECK DESTINATION INFO
                // ------------------------------------------------------------------------------
                Destination destination = db.Destinations.Find(o.DestinationID);
                if (destination == null)
                {
                    ViewBag.Error = "A destination is required!";
                	return View("Edit", o);
                }
                order.DestinationID = o.DestinationID; // will change destination only, use Reroute page for rerouting
                // Destination UOM (DestUomID) gets updated by the Order trigger (trigOrder_IU) based on the destination
                order.ConsigneeID = o.ConsigneeID;
                order.DestBOLNum = o.DestBOLNum;
                if (destination.TicketTypeID == (int)DestTicketType.TYPE.VisualMeter && o.CalculateVM())
                {
                    // Calculate gross volume from open and close meter readings (global setting)
                    order.DestGrossUnits = o.DestCloseMeterUnits - o.DestOpenMeterUnits;  // may get overwritten by the Order trigger (trigOrder_IU)
                }
                else
                {
                    order.DestGrossUnits = o.DestGrossUnits; // may get overwritten by the Order trigger (trigOrder_IU)
                }
                order.DestNetUnits = o.DestNetUnits; // may get overwritten by the Order trigger (trigOrder_IU)
                order.DestOpenMeterUnits = o.DestOpenMeterUnits;
                order.DestCloseMeterUnits = o.DestCloseMeterUnits;
                order.DestProductBSW = o.DestProductBSW;
                order.DestProductGravity = o.DestProductGravity;
                order.DestProductTemp = o.DestProductTemp;
                order.DestWeightGrossUnits = o.DestWeightGrossUnits;
                order.DestWeightTareUnits = o.DestWeightTareUnits;
                order.DestWeightNetUnits = o.DestWeightNetUnits; // may get overwritten by the Order trigger (trigOrder_IU)
                order.DestRailCarNum = o.DestRailCarNum;
                order.DestRackBay = o.DestRackBay;
                order.DestTrailerWaterCapacity = o.DestTrailerWaterCapacity;
                order.DestTruckMileage = o.DestTruckMileage;
                order.DestChainUp = o.DestChainUp;
                order.DeliverDriverNotes = o.DeliverDriverNotes;

                // Check arrive/depart are set if not rejected and Delivered, Audited ? purge otherwise?
                // Convert destination arrive and depart back to UTC
                order.DestArriveTimeUTC = o.DestArriveTimeUTC; // automatically converts DestArriveTime
                order.DestDepartTimeUTC = o.DestDepartTimeUTC; // automatically converts DestDepartTime
                // Destination minutes (DestMinutes) are calculated by the Order trigger (trigOrder_IU) based on arrive and depart times; needed here for check on wait time

                // Check order status to see if origin fields are required
                if (o.DestInfoRequired())
                {
                    //if (order.OriginID != o.OriginID) { /* should not be able to change */ }

                    if (o.DestDepartTimeUTC == null || o.DestArriveTimeUTC == null || o.DestArriveTimeUTC > o.DestDepartTimeUTC)
                    {
                        ViewBag.Error = "Valid destination arrive and depart times are required for an order in " + status.Name + " status!";
                    	return View("Edit", o);
                    }
                    else if (o.DestArriveTimeUTC < order.OriginDepartTimeUTC)
                    {
                        ViewBag.Error = "Arrive time must be greater than origin departure!";
                        return View(o);
                    }
                    else
                    {
                        TimeSpan wait = o.DestDepartTimeUTC.Value - o.DestArriveTimeUTC.Value;
                        o.DestMinutes = DBHelper.ToInt32(wait.TotalMinutes);
                    }

                    // If wait threshold exceeded during delivery, require destination wait reason and check if notes required
                    if (o.DestWaitReasonRequired)
                    {
                        DestinationWaitReason reason = db.DestinationWaitReasons.Find(o.DestWaitReasonID);
                        if (reason == null)
                        {
                            ViewBag.Error = "Destination wait reason is required!";
                        	return View("Edit", o);
                        }
                        if (reason.RequireNotes && String.IsNullOrEmpty(o.DestWaitNotes))
                        {
                            ViewBag.Error = "Destination wait notes are required!";
                        	return View("Edit", o);
                        }
                        order.DestWaitReasonID = o.DestWaitReasonID;
                        order.DestWaitNotes = o.DestWaitNotes;
                    }
                    else
                    {
                        // Clear out reason and notes
                        order.DestWaitReasonID = null;
                        order.DestWaitNotes = null;
                    }

                    if (o.MileageRequired() && o.DestTruckMileage == null)
                    {
                        ViewBag.Error = "Mileage (odometer reading) at destination is required";
                    	return View("Edit", o);
                    }
                    if (o.DestRackBay == null && o.RackBayRequired())
                    {
                        ViewBag.Error = "Rack/bay is required!";
                        return View(o);
                    }

                    switch (destination.TicketTypeID)
                    {
                        case (int)DestTicketType.TYPE.Basic:
                            if (o.DestGrossUnits == null || o.DestNetUnits == null || o.DestGrossUnits < 0 || o.DestNetUnits < 0)
                            {
                                ViewBag.Error = "Valid GOV and NSV are required";
                                return View("Edit", o);
                            }
                            break;

                        case (int)DestTicketType.TYPE.Propane:
                            if (o.DestGrossUnits == null || o.DestNetUnits == null || o.DestGrossUnits < 0 || o.DestNetUnits < 0)
                            {
                                ViewBag.Error = "Valid GOV and NSV are required";
                                return View("Edit", o);
                            }
                            break;

                        case (int)DestTicketType.TYPE.BOLAvailable:
                            if (o.DestBOLRequired() && String.IsNullOrEmpty(o.DestBOLNum))
                            {
                                ViewBag.Error = "Destination BOL # is required";
                                return View("Edit", o);
                            }
                            if (o.DestGrossUnits == null || o.DestNetUnits == null || o.DestGrossUnits < 0 || o.DestNetUnits < 0)
                            {
                                ViewBag.Error = "Valid GOV and NSV are required";
                                return View("Edit", o);
                            }
                            if (o.DestProductTemp == null || o.DestProductGravity == null || o.DestProductBSW == null)
                            {
	                            ViewBag.Error = "Product temperature, API density and sediment & water % are required";
                            	return View("Edit", o);
                            }
                            break;

                        case (int)DestTicketType.TYPE.VisualMeter:
                            if (o.DestOpenMeterUnits == null || o.DestCloseMeterUnits == null
                                || o.DestOpenMeterUnits < 0 || o.DestCloseMeterUnits < 0 || o.DestOpenMeterUnits > o.DestCloseMeterUnits)
                            {
                                ViewBag.Error = "Valid open and close meter readings are required";
                                return View("Edit", o);
                            }
                            if (o.CalculateVM() == false && (o.DestGrossUnits == null || o.DestGrossUnits < 0))
                            {
                                ViewBag.Error = "A valid GOV is required";
                                return View("Edit", o);
                            }
                            if (o.DestNetUnits == null || o.DestNetUnits < 0)
                            {
                                ViewBag.Error = "A valid NSV is required";
                                return View("Edit", o);
                            }
                            break;

                        case (int)DestTicketType.TYPE.CAN_Basic:
                            if (o.DestProductTemp == null || o.DestProductGravity == null || o.DestProductBSW == null)
                            {
                                ViewBag.Error = "Product temperature, density and water cut are required";
                                return View("Edit", o);
                            }
                            break;

                        case (int)DestTicketType.TYPE.MineralRun:
                            if (o.DestWeightNetUnits == null || o.DestWeightNetUnits < 0)
                            {
                                ViewBag.Error = "Valid net weight is required!";
                                return View("Edit", o);
                            }
                            if (o.DestBOLRequired() && String.IsNullOrEmpty(o.DestBOLNum))
                            {
                                ViewBag.Error = "BOL # is required";
                                return View("Edit", o);
                            }
                            break;

                        case (int)DestTicketType.TYPE.VisualMeterBOL:
                            if (o.DestBOLRequired() && String.IsNullOrEmpty(o.DestBOLNum))
                            {
                                ViewBag.Error = "BOL # is required";
                                return View("Edit", o);
                            }
                            if (o.DestOpenMeterUnits == null || o.DestCloseMeterUnits == null
                                || o.DestOpenMeterUnits < 0 || o.DestCloseMeterUnits < 0 || o.DestOpenMeterUnits > o.DestCloseMeterUnits)
                            {
                                ViewBag.Error = "Valid open and close meter readings are required";
                                return View("Edit", o);
                            }
                            if (o.CalculateVM() == false && (o.DestGrossUnits == null || o.DestGrossUnits < 0))
                            {
                                ViewBag.Error = "A valid GOV is required";
                                return View("Edit", o);
                            }
                            if (o.DestProductTemp == null || o.DestProductGravity == null || o.DestProductBSW == null)
                            {
                                ViewBag.Error = "Product temperature, density and BS&W are required!";
                                return View("Edit", o);
                            }
                            break;

                        case (int)DestTicketType.TYPE.BOLGrossOnly:
                            if (o.DestBOLRequired() && String.IsNullOrEmpty(o.DestBOLNum))
                            {
                                ViewBag.Error = "BOL # is required";
                                return View(o);
                            }
                            if (o.DestGrossUnits == null || o.DestGrossUnits < 0)
                            {
                                ViewBag.Error = "Valid GOV is required";
                                return View(o);
                            }
                            break;

                        case (int)DestTicketType.TYPE.Propane_With_Scale:
                            if (o.DestBOLRequired() && String.IsNullOrEmpty(o.DestBOLNum))
                            {
                                ViewBag.Error = "BOL # is required";
                                return View(o);
                            }
                            if (o.DestGrossUnits == null || o.DestNetUnits == null || o.DestGrossUnits < 0 || o.DestNetUnits < 0)
                            {
                                ViewBag.Error = "Valid GOV and NSV are required";
                                return View(o);
                            }
                            if (o.DestWeightGrossUnits == null || o.DestWeightGrossUnits < 0
                                || o.DestWeightTareUnits == null || o.DestWeightTareUnits < 0
                                || o.DestWeightNetUnits == null || o.DestWeightNetUnits < 0)
                            {
                                ViewBag.Error = "Valid gross, tare, and net weights are required!";
                                return View(o);
                            }
                            break;
                    }

                    o.OriginGrossUnits = order.OriginGrossUnits; // use current GOV value to compare
                    if (!o.DestGOVWithinTolerance())
                    {
                        TempData["message"] += "Destination GOV not within tolerance!";
                    }

                    o.OriginNetUnits = order.OriginNetUnits; // use current NSV value to compare
                    if (!o.DestNSVWithinTolerance())
                    {
                        TempData["message"] += " Destination NSV not within tolerance!";
                    }

                    if (new[] { (int)DestTicketType.TYPE.BOLAvailable, (int)DestTicketType.TYPE.VisualMeterBOL, (int)DestTicketType.TYPE.CAN_Basic }
                            .Contains(destination.TicketTypeID))
                    {
                        OrderTicket Ticket = new OrderTicket(); // dummy ticket to grab global min/max values
                        if (o.DestProductBSW < Ticket.MinBSW || o.DestProductBSW > Ticket.MaxBSW)
                        {
                            TempData["message"] += " Destination BS&W out of range!";
                        }

                        if (o.DestProductGravity < Ticket.MinGravity || o.DestProductGravity > Ticket.MaxGravity)
                        {
                            TempData["message"] += " Destination gravity out of range!";
                        }

                        if (o.DestProductTemp < Ticket.MinTemp || o.DestProductTemp > Ticket.MaxTemp)
                        {
                            TempData["message"] += " Destination temperature out of range!";
                        }
                    }

                    if (o.DestPhotoRequired() && order.DestinationPhoto == null)
                    {
                        TempData["message"] += "A destination photo should accompany this order.  ";
                    }
                    if (o.DestPhoto2Required() && order.DestinationPhoto2 == null)
                    {
                        TempData["message"] += "A destination photo #2 should accompany this order.  ";
                    }
                    if (o.DestConditionPhotosRequired() && (order.DestArrivePhoto == null || order.DestDepartPhoto == null))
                    {
                        TempData["message"] += "Destination arrive and depart photos should accompany this order.  ";
                    }
                }


                // CHECK DELIVERY INFO
                // ------------------------------------------------------------------------------
                if (o.CarrierID == null
                    && o.StatusID != (int)OrderStatus.STATUS.Generated
                    && o.StatusID != (int)OrderStatus.STATUS.Gauger)
                {
                    ViewBag.Error = "A carrier is required for an order in " + status.Name + " status!";
                    return View("Edit", o);
                }
                order.CarrierID = o.CarrierID;
                order.DriverID = o.DriverID; // Driver change causes reassign/clone, what about transfers (link to transfer edit screen?)
                order.TruckID = o.TruckID; // will only change truck, what about transfers (link to transfer edit screen?)
                order.TrailerID = o.TrailerID;
                order.Trailer2ID = o.Trailer2ID;
                order.DispatchNotes = o.DispatchNotes;
                // Route (RouteID) and route miles (ActualMiles) are set by the Order trigger (trigOrder_IU) based on the origin and destination
                if (o.CustomerID == null)
                {
                    ViewBag.Error = "A shipper is required";
                    return View("Edit", o);
                }
                order.CustomerID = o.CustomerID;
                //order.PumperID = o.PumperID;
                order.AuditNotes = o.AuditNotes;


                // FINAL CHECKS
                // ------------------------------------------------------------------------------
                if (o.MileageRequired())
                {
                    if (o.DestInfoRequired())
                    {
                        if (!o.IsTransfer)
                        {
                            if (o.OriginTruckMileage > o.DestTruckMileage)
                            {
                                TempData["message"] += "Odometer readings invalid (origin > destination)";
                            }
                        }
                        else
                        {
                            if (o.OriginTruckMileage > o.OrderTransfer.OriginTruckEndMileage)
                            {
                                TempData["message"] += "Odometer readings invalid (origin > transfer)";
                            }
                            if (o.TruckID == o.OrderTransfer.OriginTruckID // same truck
                                && o.OrderTransfer.OriginTruckEndMileage > o.OrderTransfer.DestTruckStartMileage)
                            {
                                TempData["message"] += "Odometer readings invalid (transfer > transfer)";
                            }
                            if (o.OrderTransfer.DestTruckStartMileage > o.DestTruckMileage)
                            {
                                TempData["message"] += "Odometer readings invalid (transfer > destination)";
                            }
                        }
                    }
                }

                //acceptlastchangedateutc should clear if <accepted?
                //pickuplastchangedateutc should clear if <picked up?
                //deliverlastchangedateutc should clear if <delivered?

                // OK SAVE
                db.Entry(order).CurrentValues.SetValues(order);
                db.SaveChanges(User.Identity.Name);

                TempData["success"] = "Order #" + order.OrderNum + " successfully saved";
                return RedirectToAction("Details", new { id = order.ID });
            }
            return View(o);
        }

        /**************************************************************************************************/
        /**  PAGE: ~/Orders/DispatchBoard                                                                **/
        /**  DESCRIPTION: Displays a driver board for assigning orders.  Lists drivers associated with the currently logged in user.   **/
        /**         Use most recent point and only within the last month                                 **/
        /** TODO: Need to carry h2s bit to show h2s orders and only allow assign to h2s compliant drivers **/
        /**************************************************************************************************/
        [Authorize(Roles="viewDispatchBoard")]
        public ActionResult DispatchBoard(FilterViewModel<DriverPlusLoc> fvm)
        {
            if (fvm.EndDate == null)
            {
                fvm.StartDate = fvm.EndDate = DateTime.Today.AddDays(1);
            }

            // filter drivers by driver/carrier/region
            int myCarrierID = Converter.ToInt32(Profile.GetPropertyValue("CarrierID"));
            int myDriverID = Converter.ToInt32(Profile.GetPropertyValue("DriverID"));
            int myRegionID = Converter.ToInt32(Profile.GetPropertyValue("RegionID"));
            int myTerminalID = Converter.ToInt32(Profile.GetPropertyValue("TerminalID"));
            var drivers = db.Drivers.Include(d => d.Terminal)
                                    .Include(d => d.DriverShiftType)
                                    .Where(d => ((myCarrierID == -1 && (fvm.CarrierID == null || fvm.CarrierID == d.CarrierID)) || d.CarrierID == myCarrierID)
                                                && ((myRegionID == 0 && (fvm.RegionID == null || fvm.RegionID == d.RegionID)) || d.RegionID == myRegionID)
                                                && (myDriverID == 0 && (fvm.DriverID == null || fvm.DriverID == d.ID) || d.ID == myDriverID)
                                                && (myTerminalID <= 0 && (fvm.TerminalID == null || fvm.TerminalID == d.TerminalID || d.TerminalID == null) || d.TerminalID == myTerminalID || d.TerminalID == null)
                                                && (string.IsNullOrEmpty(fvm.DriverShiftType) || d.DriverShiftTypeID == null || (d.DriverShiftType.Name).Contains(fvm.DriverShiftType))
                                                && (d.DeleteDateUTC == null)).ToList();

            //get driver score for eligibility
            string sql = string.Format(@"
                DECLARE @CarrierID int = {0};
                DECLARE @TerminalID int = {1};
                DECLARE @RegionID int = {2};    
                SELECT *
                FROM dbo.fnRetrieveEligibleDrivers (@CarrierID, @TerminalID, null, @RegionID, null, '{3}', {4}, {5})", 
                Converter.ToInt32(Profile.GetPropertyValue("CarrierID")),
                Converter.ToInt32(Profile.GetPropertyValue("TerminalID")),
                Converter.ToInt32(Profile.GetPropertyValue("RegionID")),                
                (fvm.EndDate == DateTime.Today) ? DateTime.UtcNow // assume now
                    : Core.DateHelper.ToUTC(fvm.EndDate.Value, DispatchCrudeHelper.GetProfileTimeZone(System.Web.HttpContext.Current)), // use time as midnight
                0, // get all drivers not just h2s-able
                Core.Settings.AsInt(Settings.SettingsID.DriverLocationsLimitHours));
            var driverEligibility = db.Database.SqlQuery<DriverEligibility>(sql).AsQueryable().ToList();

            // location info now comes from eligibility, should eventually eliminate join with drivers 
            var results = from d in drivers
                          join de in driverEligibility on d.ID equals de.ID into y
                          from de in y.DefaultIfEmpty()
                          select new DriverPlusLoc() { driver = d, eligibility = de };

            if (Settings.SettingsID.DispatchPlanner_HideUnavailableDrivers.AsBool())
            {
                // Filter unavailable drivers (Driver score = 0)
                results = results.Where(x => x.eligibility != null && x.eligibility.DriverScore > 0);
            }
            fvm.results = results.OrderByDescending(x => x.eligibility.DriverScore).ThenBy(x => x.driver.FirstName).ThenBy(x => x.driver.LastName);

            ViewBag.RegionID = new SelectList(db.Regions.Where(r => myRegionID == 0 || r.ID == myRegionID).OrderBy(r => r.Name), "ID", "Name", fvm.RegionID);
            ViewBag.CarrierID = new SelectList(db.Carriers.Where(c => c.DeleteDateUTC == null && (myCarrierID == -1 || c.ID == myCarrierID)).OrderBy(c => c.Name), "ID", "Name", fvm.CarrierID);
            ViewBag.DriverID = new SelectList(db.Drivers.Where(d => d.DeleteDateUTC == null && 
                                (fvm.CarrierID == null && (myCarrierID == -1 || d.CarrierID == myCarrierID || d.ID == myDriverID) || fvm.CarrierID == d.CarrierID))
                        .OrderBy(d => d.FirstName).ThenBy(d => d.LastName), "ID", "FullName", fvm.DriverID);
            ViewBag.TerminalID = new SelectList(db.Terminals.Where(t => (myTerminalID <= 0 || t.ID == myTerminalID) && t.DeleteDateUTC == null), "ID", "Name", fvm.TerminalID);
            ViewBag.ProductGroupID = new SelectList(db.ProductGroups, "ID", "Name", fvm.ProductGroupID);

            ViewBag.CustomFlag = fvm.CustomFlag;
            ViewBag.CustomFlag2 = fvm.CustomFlag2;
            List<SelectListItem> dispatchboardoptimization = new List<SelectListItem>()
                {
                    new SelectListItem() { Value = "MinDeadhead", Text = "Minimum Deadhead" },
                    new SelectListItem() { Value = "LoadBalancing", Text = "Load Balancing" }
                };
            ViewBag.CustomFilter = new SelectList(dispatchboardoptimization, "Value", "Text", fvm.CustomFilter);
            List<SelectListItem> dispatchboardsortorders = new List<SelectListItem>()
                {
                    new SelectListItem() { Value = "DueDate+Priority", Text = "Due Date, then Priority" },
                    new SelectListItem() { Value = "LongestOrder", Text = "Longest Order" }
                };
            ViewBag.CustomFilter2 = new SelectList(dispatchboardsortorders, "Value", "Text", fvm.CustomFilter2);

            return View(fvm);
        }


        [Authorize(Roles="viewDispatchBoard")]
        public ActionResult DispatchBoard_DriverOrders(int driverid, int? regionid, int? productgroupid, decimal lat, decimal lon, int driverscore, bool fromTerminal, DateTime? startdate, DateTime? enddate)
        {
            int deliveredhourslimit = Settings.SettingsID.ShowRecentlyDelivered.AsInt(0);
            DateTime minDeliverDateUTC = DateTime.UtcNow.AddHours(-deliveredhourslimit);

            // Get all orders for a driver
            var orders = db.Orders.Where(o => o.DriverID == driverid
                                            && o.DeleteDateUTC == null
                                            && (startdate == null || o.DueDate >= startdate.Value)
                                            && (enddate == null || o.DueDate <= enddate.Value)
                                            && (regionid == null || o.Driver.RegionID == regionid)
                                            && (productgroupid == null || o.Product.ProductGroupID == productgroupid)
                                            && (o.StatusID == (int)OrderStatus.STATUS.Dispatched 
                                                    || o.StatusID == (int)OrderStatus.STATUS.Accepted 
                                                    || o.StatusID == (int)OrderStatus.STATUS.PickedUp
                                                    || deliveredhourslimit > 0 && (o.StatusID == (int)OrderStatus.STATUS.Delivered || o.StatusID == (int)OrderStatus.STATUS.Audited) && o.DestDepartTimeUTC > minDeliverDateUTC))
                                    .Include(o => o.Route.Origin)
                                    .Include(o => o.Route.Destination)
                                    .OrderBy(o => (o.StatusID == (int)OrderStatus.STATUS.Delivered || o.StatusID == (int)OrderStatus.STATUS.Audited) ? 1 // completed first
                                                    : (o.StatusID == (int)OrderStatus.STATUS.PickedUp ? 2 : 3)) // picked up 2nd, then all others
                                    .ThenBy(o => o.DueDate)
                                    .ThenBy(o => o.Priority.PriorityNum)
                                    .ThenBy(o => o.SequenceNum)
                                    .ThenBy(o => o.OrderNum);
            ViewBag.Lat = lat;
            ViewBag.Lon = lon;
            ViewBag.DriverScore = driverscore;
            ViewBag.FromTerminal = fromTerminal;
            return PartialView("_DriverOrders", orders.ToList());
        }

        [Authorize(Roles="viewDispatchBoard")]
        public ActionResult DispatchBoard_AvailableOrders(int? carrierid, int? productgroupid, int? regionid, int? terminalid, DateTime? startdate, DateTime? enddate)
        {
            var orders = db.Orders.Where(o => o.DeleteDateUTC == null 
                                            && (regionid == null || o.Destination.RegionID == regionid)
                                            && (terminalid == null || ((o.Origin.TerminalID == terminalid || o.Origin.TerminalID == null) 
                                                                        && (o.Destination.TerminalID == terminalid || o.Destination.TerminalID == null)))
                                            && (productgroupid == null || o.Product.ProductGroupID == productgroupid)
                                            && (startdate == null || o.DueDate >= startdate.Value)
                                            && (enddate == null || o.DueDate <= enddate.Value)
                                            && (   o.StatusID == (int)OrderStatus.STATUS.Generated 
                                                || o.StatusID == (int)OrderStatus.STATUS.Assigned && (carrierid == null || o.CarrierID == carrierid)))
                                    .Include(o => o.Route.Origin)
                                    .Include(o => o.Route.Destination)
                                    .OrderBy(o => o.DueDate)
                                    .ThenBy(o => o.Priority.PriorityNum)
                                    .ThenBy(o => o.SequenceNum)
                                    .ThenBy(o => o.OrderNum);
            return PartialView("_AvailableOrders", orders.ToList());
        }

        [Authorize(Roles="editDispatchBoard")]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult ProcessBoard(IEnumerable<DispatchViewModel> orders, DateTime? StartDate, DateTime? EndDate, int? RegionID, int? TerminalID, int? ProductGroupID, int? CarrierID, int? DriverID, string DriverShiftType)
        {
            if (orders != null && orders.Any())
            {
                foreach (var dispatch in orders)
                {
                    Order order = db.Orders.Find(dispatch.OrderID);
                    Driver driver = db.Drivers.Find(dispatch.DriverID);
                    if (order == null || hasAccess(order) == false)
                    {
                        continue; // skip order
                    }
                    if (order.OriginInfoRequired())
                    {
                        TempData["warning"] += "Skipping #" + order.OrderNum + " because it has since been picked up or delivered.<br>";
                        continue; // skip order if pickedup, delivered, or audited
                    }
                    if (order.StatusID == (int) OrderStatus.STATUS.Declined) // send warning if declined
                    {
                        TempData["warning"] += "Skipping #" + order.OrderNum + " because it has since been declined.<br>";
                        continue; // skip order if declined
                    }
                    if (order.Deleted)
                    {
                        TempData["warning"] += "Skipping #" + order.OrderNum + " because it has since been deleted.<br>";
                        continue; // skip order if deleted
                    }

                    if (driver == null)
                    {
                        // in unassigned queue, reset to Generated
                        order.StatusID = (int)OrderStatus.STATUS.Generated;
                        order.SequenceNum = null;
                        order.CarrierID = null;
                        order.DriverID = null;
                        order.TruckID = null;
                        order.TrailerID = null;
                        order.Trailer2ID = null;
                    }
                    else
                    { 
                        if (order.DriverID == dispatch.DriverID && order.StatusID == (int)OrderStatus.STATUS.Accepted)
                        {
                            // keep in Accepted status if staying with same driver
                        }
                        else
                        {
                            order.StatusID = (int)OrderStatus.STATUS.Dispatched;
                        }
                        order.DriverID = dispatch.DriverID;
                        //order.DispatchNotes = dispatch.DispatchNotes; need way to send bulk dispatch notes
                        order.SequenceNum = dispatch.SequenceNum;
                        // ensure info associated with the driver gets updated
                        order.CarrierID = driver.CarrierID;
                        order.TruckID = driver.TruckID;
                        order.TrailerID = driver.TrailerID;
                        order.Trailer2ID = driver.Trailer2ID;
                    }

                    order.LastChangeDate = DateTime.Now;
                    order.LastChangedByUser = User.Identity.Name;
                }

                db.SaveChanges(User.Identity.Name);
                TempData["success"] = "All orders were saved!";
            }

            return RedirectToAction("DispatchBoard", new { StartDate = StartDate, EndDate = EndDate, RegionID = RegionID, TerminalID = TerminalID, ProductGroupID = ProductGroupID, CarrierID = CarrierID, DriverID = DriverID, DriverShiftType = DriverShiftType });
        }

        /**************************************************************************************************/
        /**  PAGE: ~/Orders/DispatchMap/                                                                 **/
        /**  DESCRIPTION: Displays a map for the drivers associated with the currently logged in user.   **/
        /**         Use most recent point and only within the last month                                 **/
        /** TODO: Carry h2s bit to allow only h2s orders to be assigned to H2S drivers                   **/
        /**************************************************************************************************/
        [Authorize(Roles="viewDispatchMap")]
        public ActionResult DispatchMap()
        {
            string sql = string.Format(@"
                DECLARE @CarrierID int = {0};
                DECLARE @RegionID int = {1};
                SELECT *
                FROM tblDriverLocation
                WHERE ID IN (
                    SELECT ID = max(ID) 
                    FROM tblDriverLocation 
                    WHERE DriverID IN (
                            SELECT ID FROM tblDriver 
                                WHERE DeleteDateUTC IS NULL 
                                  AND (CarrierID = @CarrierID OR @CarrierID = -1) 
                                  AND (RegionID = @RegionID OR @RegionID = 0))
                      AND ({2} = 0 OR CreateDateUTC > DATEADD(HOUR, -{2}, GETUTCDATE())) 
                    GROUP BY DriverID)", 
                Converter.ToInt32(Profile.GetPropertyValue("CarrierID")),
                Converter.ToInt32(Profile.GetPropertyValue("RegionID")),
                Settings.AsInt(Settings.SettingsID.DriverLocationsLimitHours));
            var driverLocations = db.DriverLocations.SqlQuery(sql).ToList();

            //get driver score for eligibility
            sql = string.Format(@"
                DECLARE @CarrierID int = {0};
                DECLARE @TerminalID int = {1};
                DECLARE @RegionID int = {2};                
                SELECT *
                FROM dbo.fnRetrieveEligibleDrivers (@CarrierID, @TerminalID, null, @RegionID, null, '{3}', {4}, {5})",
                Converter.ToInt32(Profile.GetPropertyValue("CarrierID")),
                Converter.ToInt32(Profile.GetPropertyValue("TerminalID")),
                Converter.ToInt32(Profile.GetPropertyValue("RegionID")),                
                DateTime.Today,
                0, // get all drivers, not just h2s-able
                Settings.AsInt(Settings.SettingsID.DriverLocationsLimitHours));
            var driverEligibility = db.Database.SqlQuery<DriverEligibility>(sql).AsQueryable().ToList();

            // join the drivers with locations (ignore null [aka join])
            var results = from d in db.Drivers.Include(d => d.Truck).Include(d => d.Trailer).ToList()
                          join dl in driverLocations on d.ID equals dl.DriverID
                          join de in driverEligibility on dl.DriverID equals de.ID
                          select new DriverPlusLoc() { driver = d, eligibility = de, location = dl };

            IEnumerable<DriverPlusLoc> drivers = results.OrderBy(d => d.eligibility.DriverScore);

            ViewBag.lat = (drivers.Any()) ? drivers.First().eligibility.Lat : LocationHelper.GEOGRAPHIC_CENTER_USA_LAT;
            ViewBag.lon = (drivers.Any()) ? drivers.First().eligibility.Lon : LocationHelper.GEOGRAPHIC_CENTER_USA_LON;

            return View("DispatchMap", results.ToList());
        }

        /*************************************************************************************************/
        /**  PAGE: [partial]                                                                            **/
        /**  DESCRIPTION: returns list of current "dispatchable" orders associated with the current     **/
        /**         user                                                                                **/
        /*************************************************************************************************/
        [Authorize(Roles="viewDispatchMap")]
		public ActionResult DispatchMap_Orders()
        {
            var orders = db.Orders.Where(o => o.DeleteDateUTC == null && (o.StatusID == (int)OrderStatus.STATUS.Generated || o.StatusID == (int)OrderStatus.STATUS.Assigned || o.StatusID == (int)OrderStatus.STATUS.Dispatched));

            // Filter orders assigned to me or that use my carrier
            int myCarrierID = Converter.ToInt32(Profile.GetPropertyValue("CarrierID"));
            orders = orders.Where(o => myCarrierID == -1 || o.CarrierID == myCarrierID);

            // Filter orders by my terminal
            int myTerminalID = Converter.ToInt32(Profile.GetPropertyValue("TerminalID"));
            if (myTerminalID > 0)
            {
                orders = orders.Include(o => o.Origin.Terminal)
                    .Where(o => ((o.Origin.TerminalID == null || o.Origin.TerminalID == myTerminalID) 
                            && (o.Destination.TerminalID == null || o.Destination.TerminalID == myTerminalID)
                            && (o.DriverID == null || o.Driver.TerminalID == null || o.Driver.TerminalID == myTerminalID)));
            }
            return PartialView("_DispatchOrders", orders.OrderBy(o => o.DueDate).ThenBy(o => o.PriorityID).ToList());
        }


        /**************************************************************************************************/
        /**  PAGE: ~/Orders/Map/{id}                                                                     **/
        /**        ~/Orders/Map/?ordernum={ordernum}                                                     **/
        /**  DESCRIPTION: Displays a map for a given order using the routes in the DriverLocation        **/
        /**       table.                                                                                 **/
        /**************************************************************************************************/
        [Authorize(Roles = "viewOrderMap")]
        public ActionResult Map(int? id, int ordernum = 0)
        {
            ViewBag.Title = "Order Maps";
            Order order = null;

            if (ordernum > 0)
            {
                order = db.Orders.FirstOrDefault(o => o.OrderNum == ordernum);
            }
            else if (id != null)
            {
                order = db.Orders.Find(id);
            }
            else
            {
                // both parameters are null, first load
                return View();
            }

            if (order == null)
            {
                ViewBag.Error = "Order is not a valid order";
                ViewBag.ordernum = ordernum;
                return View();
            }
            if (hasAccess(order) == false)
            {
                TempData["error"] = "You do not have access to this order!";
                return RedirectToAction("Search");
            }
            if (order.OriginID == null || order.DestinationID == null)
            {
                TempData["error"] = "Order does not have a valid route (origin and destination)!";
                return RedirectToAction("Details", new { id = order.ID });
            }

            // Send the relevant driver locations for this order
            string sql = "SELECT * FROM tblDriverLocation " +
                "WHERE OrderID = " + order.ID + " AND SourceAccuracyMeters < 500 " +
                "ORDER BY CreateDateUTC";
            ViewBag.DriverLocations = db.DriverLocations.SqlQuery(sql).ToList();

            ViewBag.ordernum = order.OrderNum;
            return View(order);
        }


        /**************************************************************************************************/
        /**  PAGE: ~/Orders/DemurrageReport/                                                             **/
        /**  DESCRIPTION: Displays a list of orders whose wait times dont match mobile points            **/
        /**************************************************************************************************/
        [Authorize(Roles="viewDemurrageReport")]
        public ActionResult DemurrageReport(FilterViewModel<OrderDemurrage> fvm)
        {
            DateTime now = DateTime.Today;
            if (fvm.StartDate == null) fvm.StartDate = Core.DateHelper.StartOfMonth(now);
            if (fvm.EndDate == null) fvm.EndDate = now;
            if (fvm.TimeLimit == null) fvm.TimeLimit = OrderDemurrage.DEMURRAGE_VARIANCE_ERROR_MIN;
            if (fvm.DistanceLimit == null) fvm.DistanceLimit = OrderDemurrage.DEMURRAGE_VARIANCE_ERROR_MILES;

            int myCarrierID = Converter.ToInt32(Profile.GetPropertyValue("CarrierID"));
            int myDriverID = Converter.ToInt32(Profile.GetPropertyValue("DriverID"));
            int myTerminalID = Converter.ToInt32(Profile.GetPropertyValue("TerminalID"));

            string sql = "SELECT * FROM dbo.fnOrdersBadDemurrage('" + 
                fvm.StartDate.Value.ToString("yyyy-MM-dd") + "', '" +
                fvm.EndDate.Value.ToString("yyyy-MM-dd") + "', " +
                fvm.TimeLimit + ", " +
                fvm.DistanceLimit + ", " +
                (fvm.CustomFlag ? 1 : 0) + ") ORDER BY ID";
            var Orders = db.Database.SqlQuery<OrderDemurrage>(sql);
            fvm.results = Orders.Where(o => (myCarrierID == -1 && (fvm.CarrierID == null || fvm.CarrierID == o.CarrierID) && (fvm.DriverID == null || o.DriverID == fvm.DriverID)) 
                                          || (o.CarrierID == myCarrierID && (myDriverID == -1 && (fvm.DriverID == null || fvm.DriverID == o.DriverID) || o.DriverID == myDriverID)));
//-------------------TODO----------------------
            ViewBag.CarrierID = new SelectList(db.Carriers.Where(c => (myCarrierID == -1 || c.ID == myCarrierID) && c.DeleteDateUTC == null).OrderBy(c => c.Name), "ID", "Name", fvm.CarrierID);
            ViewBag.DriverID = new SelectList(db.Drivers.Where(d => (myCarrierID == -1 && (fvm.CarrierID == null || d.CarrierID == fvm.CarrierID) || (myDriverID == -1 && d.CarrierID == myCarrierID) || myDriverID == d.ID) && d.DeleteDateUTC == null).OrderBy(d => d.FirstName).ThenBy(d => d.LastName), "ID", "FullName", fvm.DriverID);

            return View(fvm);
        }

        /**************************************************************************************************/
        /**  PAGE: [partial view]                                                                        **/
        /**  DESCRIPTION: List all photos tied to an order.                                              **/
        /**************************************************************************************************/
        [Authorize(Roles = "viewOrders")]
        public ActionResult ListPhotos(int id, string photoFilter = "")
        {
            Order order = db.Orders.Find(id);

            if (order == null)
            {
                TempData["error"] = "Order not found!";
                return RedirectToAction("Search");
            }
            if (hasAccess(order) == false)
            {
                TempData["error"] = "You do not have access to this order!";
                return RedirectToAction("Search");
            }

            ViewBag.OrderID = id;
            ViewBag.Filter = photoFilter.ToUpper();

            return PartialView("_ListPhotos", order);
        }

        /**************************************************************************************************/
        /**  PAGE: ~/Orders/ShowPhoto/{id}                                                               **/
        /**  DESCRIPTION: Display the order photo as an image (jpeg)                                     **/
        /**************************************************************************************************/
        [Authorize(Roles = "viewOrders")]
        public ActionResult ShowPhoto(int id)
        {
            OrderPhoto photo = db.OrderPhotos.Find(id);

            if (photo == null)
            {
                TempData["error"] = "Photo not found!";
                return RedirectToAction("Search");
            }

            if (hasAccess(photo.Order) == false)
            {
                TempData["error"] = "You do not have access to this order!";
                return RedirectToAction("Search");
            }

            MemoryStream ms = new MemoryStream(photo.PhotoBlob);
            Image currentImage = new Bitmap(ms);

            byte[] newImage = ImageToByte2(AppendFooter(currentImage, photo.PhotoDateUTC, photo.GPSLocation));
            
            return File(newImage, "image/jpeg");
            //return File(photo.PhotoBlob, "image/jpeg");
        }
        
        public static Bitmap AppendFooter(Image photo, string photoDate, string photoLocation)
        {
            string footerText = "DATE: " + photoDate + "              LOCATION: " + photoLocation;
            //Source credit: http://stackoverflow.com/a/30534887/5152586
            //Create new image with space for footer
            int footerSize = 30;            
            Bitmap newImage = new Bitmap(photo.Width, photo.Height + footerSize);

            Graphics g = Graphics.FromImage(newImage);            
            g.DrawImage(photo, 0, 0, photo.Width, photo.Height);
            g.FillRectangle(new SolidBrush(Color.White), 0, photo.Height, photo.Width, footerSize);
            g.DrawString(footerText, new Font("Arial", 12), new SolidBrush(Color.Black), 20, photo.Height + 5);
            g.Dispose();            

            return newImage;
        }

        public static byte[] ImageToByte2(Image img)
        {
            //Source credit: http://www.vcskicks.com/image-to-byte.php
            byte[] byteArray = new byte[0];
            using (MemoryStream stream = new MemoryStream())
            {
                img.Save(stream, ImageFormat.Jpeg);
                stream.Close();
                byteArray = stream.ToArray();
            }
            return byteArray;
        }

        /**************************************************************************************************/
        /**  PAGE: ~/Orders/AddPhoto/{id}                                                                **/
        /**  DESCRIPTION: Add a photograph to an order.                                                  **/
        /**************************************************************************************************/
        [Authorize(Roles = "editOrders")]
        public ActionResult AddPhoto(int id, string type)
        {
            ViewBag.Title = "Add Order Photo";
            
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                TempData["error"] = "Order not found!";
                return RedirectToAction("Search");
            }
            if (hasAccess(order) == false)
            {
                TempData["error"] = "You do not have access to this order!";
                return RedirectToAction("Search");
            }

            ViewBag.OrderID = id;
            ViewBag.PhotoType = type;
            ViewBag.OrderNum = order.OrderNum;
            if (type == "Origin")
            {
                ViewBag.OriginID = order.OriginID;
                ViewBag.PhotoTypeID = (int)PhotoType.TYPES.Location;
            }
            else if (type == "Origin2")
            {
                ViewBag.OriginID = order.OriginID;
                ViewBag.PhotoTypeID = (int)PhotoType.TYPES.Location2;
            }
            else if (type == "OriginArrive")
            {
                ViewBag.OriginID = order.OriginID;
                ViewBag.PhotoTypeID = (int)PhotoType.TYPES.ConditionBefore;
            }
            else if (type == "OriginDepart")
            {
                ViewBag.OriginID = order.OriginID;
                ViewBag.PhotoTypeID = (int)PhotoType.TYPES.ConditionAfter;
            }
            else if (type == "Destination")
            {
                ViewBag.DestinationID = order.DestinationID;
                ViewBag.PhotoTypeID = (int)PhotoType.TYPES.Location;
            }
            else if (type == "Destination2")
            {
                ViewBag.DestinationID = order.DestinationID;
                ViewBag.PhotoTypeID = (int)PhotoType.TYPES.Location2;
            }
            else if (type == "DestArrive")
            {
                ViewBag.DestinationID = order.DestinationID;
                ViewBag.PhotoTypeID = (int)PhotoType.TYPES.ConditionBefore;
            }
            else if (type == "DestDepart")
            {
                ViewBag.DestinationID = order.DestinationID;
                ViewBag.PhotoTypeID = (int)PhotoType.TYPES.ConditionAfter;
            }
            else if (type == "Reject")
            {
                ViewBag.PhotoTypeID = (int)PhotoType.TYPES.Reject;
            }

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "editOrders")]
        public ActionResult AddPhoto([Bind(Include = "OrderID,OriginID,DestinationID,PhotoTypeID")] OrderPhoto op, HttpPostedFileBase upload)
        {
            Order order = db.Orders.Find(op.OrderID);
            op.DriverID = order.DriverID;
            op.UID = Guid.NewGuid();

            if (ModelState.IsValid)
            {
                if (upload != null && upload.ContentLength > 0)
                {
                    using (var reader = new BinaryReader(upload.InputStream))
                    {
                        op.PhotoBlob = reader.ReadBytes(upload.ContentLength);
                    }
                    db.OrderPhotos.Add(op);
                    db.SaveChanges(User.Identity.Name);
                    return RedirectToAction("Details", new { id = op.OrderID });
                }
            }
            return View(op);
        }

        /**************************************************************************************************/
        /**  PAGE: ~/Orders/DeletePhoto/{id}                                                             **/
        /**  DESCRIPTION: Remove a photograph from an order.                                             **/
        /**************************************************************************************************/
        [Authorize(Roles = "editOrders")]
        public ActionResult DeletePhoto(int id)
        {
            OrderPhoto photo = db.OrderPhotos.Find(id);
            Order order = photo.Order;

            if (photo == null)
            {
                TempData["error"] = "Photo not found!";
                return RedirectToAction("Search");
            }
            if (hasAccess(photo.Order) == false)
            {
                TempData["error"] = "You do not have access to this order!";
                return RedirectToAction("Search");
            }
            if (photo.Order.canEdit == false)
            {
                TempData["error"] = "Cannot edit a settled/audited order!";
                return RedirectToAction("Details", new { id = order.ID });
            }

            // delete record from tblOrderPhoto
            db.OrderPhotos.Remove(photo);
            db.SaveChanges(User.Identity.Name);


            TempData["success"] = "Photo removed from order #" + order.OrderNum;
            return RedirectToAction("Details", new { id = order.ID });
        }

        /**************************************************************************************************/
        /**  PAGE: ~/Orders/PrintBOL/{id}                                                                **/
        /**  DESCRIPTION: Prints an electronic bill of lading (BOL) for a given order                    **/
        /**************************************************************************************************/
        [HttpGet]
        [Authorize(Roles = "viewPrintBOL")]
        public ActionResult PrintBOL(int id)
        {
            byte[] fileBytes = GetPdfFileBytes(id);
            GC.Collect();
            return File(fileBytes, "Application/pdf", GetPdfFileName(id));
        }

        [Authorize(Roles = "viewPrintBOL")]
        private string GetPdfFileName(int id)
        {
            return string.Format("{0}_BOL.pdf", db.Orders.Single(r => r.ID == id).OrderNum);
        }

        [Authorize(Roles = "viewPrintBOL")]
        private byte[] GetPdfFileBytes(int id)
        {
            HiQPdf.HtmlToPdf converter = new HiQPdf.HtmlToPdf();
            {
                converter.BrowserWidth = 900;
                converter.SerialNumber = "ezMSKisf-HTcSGQka-CQJNVUtb-SltJW0NO-SFtISlVK-SVVCQkJC";
                return converter.ConvertHtmlToMemory(GenerateBOLString(id), null);
            }
        }

        [Authorize(Roles = "viewPrintBOL")]
        public string GenerateBOLString(int id)
        {
            Order order = db.Orders.Where(o => o.ID == id)
                .Include(o => o.OrderTickets)
                .Include(o => o.OrderSignatures)
                .Include(o => o.OrderPhotos)
                .Include(o => o.Origin.UOM)
                .Include(o => o.Destination.UOM)
                .Include(o => o.OrderTransfer.OriginDriver)
                .Include(o => o.Origin.TicketType)
                .Include(o => o.Destination.TicketType)
                .FirstOrDefault();

            //Get custom eBOL fields from the database (These display at the bottom of the eBOLs)
            SSDB ssdb = new SSDB();
            DataTable dt = ssdb.GetPopulatedDataTable("SELECT DataLabel, DataField FROM tblCustomBOLFields WHERE DisplayOnBOL = 1 ORDER BY SortNum ASC");
            ViewBag.CustomFields = dt;

            return ControllerExtensions.RenderViewToString(this, "BOL_All", order);
        }

        //[Authorize(Roles = "viewPrintBOL")]
        //public ActionResult GenerateBOL(int id)
        //{
        //    Models.Order order = db.Orders.Where(o => o.ID == id)
        //        .Include(o => o.OrderTickets)
        //        .Include(o => o.OrderSignatures)
        //        .Include(o => o.OrderPhotos)
        //        .Include(o => o.Origin.UOM)
        //        .Include(o => o.Destination.UOM)
        //        .Include(o => o.OrderTransfer.OriginDriver)
        //        .Include(o => o.Origin.TicketType)
        //        .Include(o => o.Destination.TicketType)
        //        .FirstOrDefault();
        //    return View("BOL_All", order);
        //}

        [Authorize(Roles = "viewPrintBOL")]
        private string GetJpegFileName(int id)
        {
            return string.Format("JPG_{0}.pdf", db.Orders.Single(r => r.ID == id).OrderNum);
        }


        [Authorize(Roles = "Administrator, Management, Operations, Logistics, Customer, Producer, Carrier, Dispatch3P, viewPrintBOL")]
        [HttpGet]
        public FileStreamResult PrintBOLs(string fileName, bool includePhotos = true)
        {
            using (ZipFile zf = new ZipFile())
            {
                List<int> orderIDs = (List<int>)this.ControllerContext.HttpContext.Session[SESSION_NAME_ORDERIDLIST];
                foreach (int id in orderIDs)
                {
                    byte[] fileBytes = GetPdfFileBytes(id);
                    string filename = GetPdfFileName(id);
                    zf.AddEntry(filename, fileBytes);
                    if (includePhotos)
                    {
                        // add any available Location + Reject Origin|Destination photos
                        using (SSDB db = new SSDB())
                        {
                            System.Data.DataTable dtPhotos = db.GetPopulatedDataTable(
                                "SELECT O.OrderNum, X.OrderID, X.ID, X.LT "
                                + "FROM tblOrder O "
                                + "JOIN ("
                                /* using nested query to allow UNION between to TOP 1 ... ORDER BY queries */
                                // get the newest Origin LOCATION Photo (if any)
                                    + "SELECT OrderID, ID, LT = 'Origin' FROM ("
                                        + "SELECT TOP 1 OrderID, ID FROM tblOrderPhoto WHERE OrderID = {0} AND OriginID IS NOT NULL AND PhotoTypeID = 1 ORDER BY CreateDateUTC DESC, ID DESC"
                                    + ") X "
                                // get the newest Origin LOCATION #2 Photo (if any)
                                    + "UNION SELECT OrderID, ID, LT = 'Origin2' FROM ("
                                        + "SELECT TOP 1 OrderID, ID FROM tblOrderPhoto WHERE OrderID = {0} AND OriginID IS NOT NULL AND PhotoTypeID = 5 ORDER BY CreateDateUTC DESC, ID DESC"
                                    + ") X "
                                // get the newest Origin Arrive Photo (if any)
                                    + "UNION SELECT OrderID, ID, LT = 'Origin_Arrive' FROM ("
                                        + "SELECT TOP 1 OrderID, ID FROM tblOrderPhoto WHERE OrderID = {0} AND OriginID IS NOT NULL AND PhotoTypeID = 3 ORDER BY CreateDateUTC DESC, ID DESC"
                                    + ") X "
                                // get the newest Origin Depart Photo (if any)
                                    + "UNION SELECT OrderID, ID, LT = 'Origin_Depart' FROM ("
                                        + "SELECT TOP 1 OrderID, ID FROM tblOrderPhoto WHERE OrderID = {0} AND OriginID IS NOT NULL AND PhotoTypeID = 4 ORDER BY CreateDateUTC DESC, ID DESC"
                                    + ") X "
                                // get the newest Reject Photo (if any)
                                    + "UNION SELECT OrderID, ID, LT = 'Reject' FROM ("
                                        + "SELECT TOP 1 OP.OrderID, OP.ID FROM tblOrderPhoto OP LEFT JOIN tblOrder O ON O.ID = OP.OrderID WHERE OP.OrderID = {0} AND O.Rejected = 1 AND OP.PhotoTypeID = 2 ORDER BY OP.CreateDateUTC DESC, ID DESC"
                                    + ") X "
                                // get the newest Destination LOCATION Photo (if any)
                                    + "UNION SELECT OrderID, ID, LT = 'Destination' FROM ("
                                        + "SELECT TOP 1 OrderID, ID FROM tblOrderPhoto WHERE OrderID = {0} AND DestinationID IS NOT NULL AND PhotoTypeID = 1 ORDER BY CreateDateUTC DESC, ID DESC"
                                    + ") X "
                                // get the newest Destination LOCATION #2 Photo (if any)
                                    + "UNION SELECT OrderID, ID, LT = 'Destination2' FROM ("
                                        + "SELECT TOP 1 OrderID, ID FROM tblOrderPhoto WHERE OrderID = {0} AND DestinationID IS NOT NULL AND PhotoTypeID = 5 ORDER BY CreateDateUTC DESC, ID DESC"
                                    + ") X "
                                // get the newest Destination Arrive Photo (if any)
                                    + "UNION SELECT OrderID, ID, LT = 'Destination_Arrive' FROM ("
                                        + "SELECT TOP 1 OrderID, ID FROM tblOrderPhoto WHERE OrderID = {0} AND DestinationID IS NOT NULL AND PhotoTypeID = 3 ORDER BY CreateDateUTC DESC, ID DESC"
                                    + ") X "
                                // get the newest Destination Depart Photo (if any)
                                    + "UNION SELECT OrderID, ID, LT = 'Destination_Depart' FROM ("
                                        + "SELECT TOP 1 OrderID, ID FROM tblOrderPhoto WHERE OrderID = {0} AND DestinationID IS NOT NULL AND PhotoTypeID = 4 ORDER BY CreateDateUTC DESC, ID DESC"
                                    + ") X "
                                + ") X ON X.OrderID = O.ID"
                                , (object)id);
                            // write out each located Photo as a zip file entry (they are assumed to be jpg files)
                            foreach (System.Data.DataRow drPhoto in dtPhotos.Rows)
                            {
                                //MemoryStream pms = new MemoryStream();
                                //db.QuerySingleToStream(pms, "SELECT PhotoBlob FROM tblOrderPhoto WHERE ID = {0}", drPhoto["ID"]);                                

                                //*****DCWEB-1674 Changed the way we had to get the photos for the bol zip
                                OrderPhoto photo = null;
                                using (DispatchCrudeDB dcdb = new DispatchCrudeDB())
                                {
                                    photo = dcdb.OrderPhotos.Find(drPhoto["ID"]);
                                }

                                MemoryStream pms = new MemoryStream(photo.PhotoBlob);
                                Image currentImage = new Bitmap(pms);
                                byte[] newImage = ImageToByte2(AppendFooter(currentImage, photo.PhotoDateUTC, photo.GPSLocation));
                                zf.AddEntry(string.Format("{0}_{1}.jpg", drPhoto["OrderNum"], drPhoto["LT"]), newImage);
                                //******

                                //zf.AddEntry(string.Format("{0}_{1}.jpg", drPhoto["OrderNum"], drPhoto["LT"]), pms.GetBuffer());
                                //pms.Close();
                            }
                        }
                    }
                }
                MemoryStream ms = new MemoryStream();
                zf.Save(ms);
                ms.Seek(0, SeekOrigin.Begin);
                // ensure the filename has a .zip extension
                if (!Path.GetExtension(fileName).Equals(".zip", StringComparison.CurrentCultureIgnoreCase))
                {
                    fileName = Path.GetFileNameWithoutExtension(fileName) + ".zip";
                }
                GC.Collect();
                return File(ms, "Application/zip", fileName);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [Authorize(Roles = "viewOrders")]
        public List<Order> getReassigns(Order order)
        {
            if (order.ReassignKey == null)
                return new List<Order>(); // not a reassign, return empty list

            return db.Orders.Where(o => o.ReassignKey == order.ReassignKey && o.ID != order.ID).ToList();
        }

        public bool hasAccess(Order order)
        {
            // use profile to determine if a user can work with this order
            int myCarrierID = Converter.ToInt32(Profile.GetPropertyValue("CarrierID"));
            int myDriverID = Converter.ToInt32(Profile.GetPropertyValue("DriverID"));
            int myTerminalID = Converter.ToInt32(Profile.GetPropertyValue("TerminalID"));
            return ((myCarrierID == -1) || (myCarrierID == order.CarrierID) || (myDriverID == order.DriverID)) // carrier/driver match
                && (   (myTerminalID <= 0)  // No Terminal filtering
                    || (   (order.Origin == null || order.Origin.TerminalID == null || order.Origin.TerminalID == myTerminalID) // origin terminal match
                        && (order.Destination == null || order.Destination.TerminalID == null || order.Destination.TerminalID == myTerminalID) // dest terminal match
                        && (order.Destination == null || order.Destination.TerminalID == null || order.Destination.TerminalID == myTerminalID))); // driver terminal match
        }

        /**************************************************************************************************/
        /**  PAGE: ~/Destinations/getRouteMiles/?order1={order1}&order2=order2                           **/
        /**  DESCRIPTION: JSON page that returns the Unit of Measure for a given destination             **/
        /**************************************************************************************************/
        public ActionResult getRoute(int order1, int order2)
        {
            Order o1 = db.Orders.Find(order1);
            Order o2 = db.Orders.Find(order2);

            var result = (double?)(from r in db.Routes
                          where r.DestinationID == o1.DestinationID
                          && r.OriginID == o2.OriginID
                          select r.ActualMiles).First();

            if (result == null && o1.Destination.LAT != null && o1.Destination.LAT != null && o2.Origin.LAT != null && o2.Origin.LAT != null)
            {
                result = LocationHelper.getMiles(decimal.Parse(o1.Destination.LAT), decimal.Parse(o1.Destination.LON),
                            decimal.Parse(o2.Origin.LAT), decimal.Parse(o2.Origin.LON));
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        
    }

}

