﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DispatchCrude.DataExchange;
using DispatchCrude.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System.Collections;
using Ionic.Zip;

namespace DispatchCrude.Controllers
{
    [Authorize(Roles = "viewPdfExport")]
    public class PdfExportController : _DBControllerRead
    {
        // This session variable is required for Asyncronous file uploads to work with new records.
        // The file is uploaded here temporarily until the user actually clicks the save button.
        private const string SESSION_PDFEXPORT_WORDFILENAME = "PDFEXPORT_FILENAME"
            , SESSION_PDFEXPORT_WORDFILE = "PDFEXPORT_FILE";

        public ActionResult Index()
        {
            return View();
        }

        /* normally this is the only method that is overridden (defined) in the inheriting controller classes */
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        override protected ContentResult Read([DataSourceRequest] DataSourceRequest request, ModelStateDictionary modelState, int id)
        {
            var data = db.PdfExports.Where(m => m.ID == id || id == 0)
                            .Include(m => m.PdfExportType);
            return ToJsonResult(data, request, modelState, "WordDocument");
        }

        [HttpPost]
        [Authorize(Roles = "createPdfExport")]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, PdfExport model)
        {
            return Update(request, model);
        }

        [HttpPost]
        [Authorize(Roles = "editPdfExport")]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, PdfExport model)
        {
            // push the previously provided document info into the model (if present we need to re-validate the model, done below)
            bool resetValidation = false;
            if (resetValidation = Session[SESSION_PDFEXPORT_WORDFILE] != null) model.WordDocument = Session[SESSION_PDFEXPORT_WORDFILE] as byte[];
            if (resetValidation = Session[SESSION_PDFEXPORT_WORDFILENAME] != null) model.WordDocName = Session[SESSION_PDFEXPORT_WORDFILENAME].ToString();
            // reset the ModelState and re-validate (this is required if the document was required)
            if (resetValidation)
            {
                model.Validated = false;
                ModelState.Clear();
                //CarrierCompliance.Validate(new System.ComponentModel.DataAnnotations.ValidationContext(CarrierCompliance, null, null));
            }

            if (ModelState.IsValid)
            {
                try
                {
                    // don't update the Document properties if not provided (will fail validation above if required and not provided)
                    string[] skipParameters = model.WordDocument != null ? null : new string[] { "WordDocument", "WordDocName" };
                    db.AddOrUpdateSave(User.Identity.Name, model, null, skipParameters);

                    //Clear Carrier compliance session variable after successful add.  Otherwise the next record added will automatically use this file if the user did not select one
                    Session[SESSION_PDFEXPORT_WORDFILE] = null;
                    Session[SESSION_PDFEXPORT_WORDFILENAME] = null;
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { model }.ToDataSourceResult(request, ModelState));
                }
            }

            return Read(request, ModelState, model.ID);
        }


        // Delete (Deactivate)  Product Groups
        [Authorize(Roles = "deactivatePdfExport")]
        public ActionResult Delete([DataSourceRequest] DataSourceRequest request, PdfExport model)
        {
            return DeleteInt(request, model);
        }

        public FilePathResult ExportDriverSettlementZipFile(int pdfExportID, int batchID)
        {
            string tempfileName = Path.GetTempFileName()
                , fileName = "Driver Settlement Statements.zip";
            if (!Path.GetExtension(fileName).Equals(".zip", StringComparison.CurrentCultureIgnoreCase))
            {
                fileName = Path.GetFileNameWithoutExtension(fileName) + ".zip";
            }
            using (ZipFile zf = new ZipFile())
            {
                zf.Save(tempfileName);
                DriverSettlementStatementPdfDocs pdfDocuments = new DriverSettlementStatementPdfDocs(pdfExportID, batchID);
                foreach (DCPdfDocument pdfDoc in pdfDocuments)
                {
                    zf.AddEntry(pdfDoc.FileName, pdfDoc.Bytes);
                }
                zf.Save();
            }
            return File(tempfileName, "Application/zip", fileName);
        }

        private bool ValidateUpload(bool isNew, ModelStateDictionary modelState)
        {
            bool ret = Session[SESSION_PDFEXPORT_WORDFILENAME] != null && Session[SESSION_PDFEXPORT_WORDFILE] != null;
            //if (isNew && !ret)
                //modelState.AddModelError(null, "File must be provided");
            return ret;
        }


        [HttpPost]
        public ActionResult SaveDocument(IEnumerable<HttpPostedFileBase> document)
        {
            // The Name of the Upload component is "files"
            if (document != null)
            {
                foreach (var file in document)
                {
                    Session[SESSION_PDFEXPORT_WORDFILENAME] = Path.GetFileName(file.FileName);
                    Session[SESSION_PDFEXPORT_WORDFILE] = new BinaryReader(file.InputStream).ReadBytes(file.ContentLength);
                    // we only accept a single file so just break
                    break;
                }
            }
            // Return an empty string to signify success
            return Content("");
        }

        public ActionResult ClearDocument()
        {
            Session[SESSION_PDFEXPORT_WORDFILE] = null;
            Session[SESSION_PDFEXPORT_WORDFILENAME] = null;

            return Json(new { status="OK"}, JsonRequestBehavior.AllowGet);
        }

        // Download the Document
        public ActionResult Download(int id)
        {
            PdfExport model = db.PdfExports.Find(id);
            return File(model.WordDocument, "application/octet-stream", model.WordDocName);
        }

    }
}
