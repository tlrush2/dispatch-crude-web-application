﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Syncfusion.XlsIO;
using DispatchCrude.Core;
using DispatchCrude.Extensions;
using DispatchCrude.Models;
using DispatchCrude.Models.ImportCenter;
using AlonsIT;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;

namespace DispatchCrude.Controllers
{
    [Authorize(Roles = "viewImportCenter, editImportCenter")]
    public class ImportCenterController : _DBControllerRead
    {
        static private string SESSION_DT_NAME = "TBL_INPUT"
            , SESSION_INPUTFILE = "IC_INPUTFILE"
            , SESSION_INPUTFILENAME = "IC_INPUTFILENAME"
            , SESSION_DATAFILE = "IC_DATAFILE"
            , SESSION_DATAFILENAME = "IC_DATAFILENAME"
            , SESSION_DATAFIRSTROWHEADER = "IC_DATAFIRSTROWHEADER"
            , SESSION_EXECUTE_DATATABLE = "IC_EXECUTE_DATATABLE"
            , SESSION_ROOTOBJECT = "IC_ROOTOBJECT"
            , SESSION_DEFINITION_XML = "IC_DEFINITION_XML";

        // GET: /ImportCenter/Index/
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        // ImportCenter/Read
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        override protected ContentResult Read([DataSourceRequest] DataSourceRequest request, ModelStateDictionary modelState, int id)
        {
            var data = db.Objects
                // only include RootObjects that have at least 1 IC def defined for it
                .Where(o => o.ImportCenterDefinitions.Count > 0)
                // nested projection to return the data in select2 data format ( optgroup = { text: "<title>", children: [ { id: ID, text: "<title>" } ] } )
                .Select(m => new
                    {
                        text = m.Name
                        , children = m.ImportCenterDefinitions
                            // filter the IC defs to only those available to this user
                            .Where(ic => (ic.UserNames == null || ("," + ic.UserNames + ",").Contains("," + User.Identity.Name + ",")))
                            // inner projection to get the id/text for the importcenter definitions
                            .Select(ic => new { id = ic.ID, text = ic.Name, ic.RootObjectID, RootObject = ic.RootObject.Name, HasSample = ic.InputFile != null })
                            // inner sort on the ic defs
                            .OrderBy(ic => ic.text)
                    })
                .OrderBy(m => m.text);

            return ToJsonResult(data, request, modelState);
        }


        // GET: /ImportCenter/Designer/
        [Authorize(Roles = "editImportCenter")]
        [HttpGet]
        public ActionResult Designer()
        {
            return View();
        }

        [Authorize(Roles = "editImportCenter")]
        [HttpPost]
        public ActionResult ExecuteTest(int importCenterID, int rootObjectID)
        {
            return ExecuteInt(importCenterID, rootObjectID, true);
        }

        private ActionResult ExportDataTable(DataTable dtData, string importFileName, int rootObjectID)
        {
            var rootObjectName = db.Objects.FirstOrDefault(o => o.ID == rootObjectID).Name;
            importFileName = Path.GetFileNameWithoutExtension(importFileName);
            if (dtData != null)
            {
                using (ExcelEngine excelEngine = new ExcelEngine())
                {
                    IApplication application = excelEngine.Excel;
                    application.DefaultVersion = ExcelVersion.Excel2013;
                    IWorkbook workbook = application.Workbooks.Create(1);
                    IWorksheet sheet = workbook.Worksheets[0];
                    sheet.ImportDataTable(dtData, true, 1, 1);
                    MemoryStream ms = new MemoryStream();
                    workbook.SaveAs(ms, ExcelSaveType.SaveAsXLS);
                    string defaultFileName = string.Format("{0}_ImportOutcome - [{1}] ({2:yyyy-MM-dd}).xlsx", importFileName, rootObjectName, DateTime.Now.Date);
                    return File(ms.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", defaultFileName);
                }
            }
            // TODO: return the same page with an error message stating what went wrong?
            return null;
        }

        [Authorize(Roles = "editImportCenter")]
        [HttpPost]
        public ActionResult CloneDefinition(int id, string name, string userNames)
        {
            int ret = 0;

            // only admins can create a spec for another user (than their self)
            //if (!UserSupport.IsAdmin(//)) userNames = UserSupport.UserName;
            
            using (SSDB db = new SSDB())
            {
                using (System.Data.SqlClient.SqlCommand cmd = db.BuildCommand("spCloneImportCenterDefinition"))
                {
                    cmd.Parameters["@importCenterDefinitionID"].Value = id;
                    cmd.Parameters["@Name"].Value = name;
                    cmd.Parameters["@UserNames"].Value = (object)userNames.TZ() ?? DBNull.Value;
                    cmd.Parameters["@UserName"].Value = User.Identity.Name;
                    cmd.ExecuteNonQuery();
                    ret = AlonsIT.DBHelper.ToInt32(cmd.Parameters["@NewID"].Value);
                }
            }
            return ToJsonResult(new { id = ret });
        }

        [Authorize(Roles = "editImportCenter")]
        [HttpPost]
        public ActionResult CreateDefinition(string name, int rootObjectID, string userNames, int? id = null)
        {
            int ret = 0;

            // only admins can create a spec for another user (than their self)
            //if (!UserSupport.IsAdmin(//)) userNames = UserSupport.UserName;

            using (SSDB db = new SSDB())
            {
                ret = DBHelper.ToInt32(db.QuerySingleValue(
                    "INSERT INTO tblImportCenterDefinition (RootObjectID, Name, UserNames, CreatedByUser) VALUES ({0}, {1}, {2}, {3}); SELECT SCOPE_IDENTITY()"
                    , rootObjectID
                    , DBHelper.QuoteStr(name)
                    , DBHelper.QuoteStr(userNames.TZ())
                    , DBHelper.QuoteStr(UserSupport.UserName)));
            }

            return ToJsonResult(new { id = ret });
        }

        [Authorize(Roles = "editImportCenter")]
        [HttpPost]
        public ActionResult DeleteDefinition(int id)
        {
            using (SSDB db = new SSDB())
            {
                return ToJsonResult(
                    new { 
                        success = db.ExecuteSql(
                            // have to explicitly delete the children first because we are prevented from using a CASCADING FK due to a IOD trigger on the child table
                            "DELETE FROM tblImportCenterFieldDefinition WHERE ImportCenterDefinitionID = {0};"
                            + " DELETE FROM tblImportCenterDefinition WHERE ID={0}", id) > 0 
                    });
            }
        }

        [Authorize(Roles = "editImportCenter")]
        [HttpPost]
        public ActionResult UpdateDefinition(int id, string name, string userNames)
        {
            bool ret = false;

            using (SSDB db = new SSDB())
            {
                ret = db.ExecuteSql(
                    "UPDATE tblImportCenterDefinition SET Name = {1}, UserNames = {2} WHERE ID={0}"
                    , id
                    , DBHelper.QuoteStr(name)
                    // if a non-admin is calling - never update the UserNames field (just no-op this part by updating the value to "itself")
                    , DBHelper.QuoteStr(userNames.TZ())  //(!UserSupport.IsAdmin(//) ? "[UserNames]" : DBHelper.QuoteStr(UserNames))
                ) > 0; //});
            }
            return ToJsonResult(new { id = id, success = ret });
        }

        [Authorize(Roles = "editImportCenter")]
        [HttpPost]
        public ActionResult CreateFields(IList<ImportCenterFieldDefinition> importFields)
        {
            //Iterate all created products which are posted by the Kendo Grid
            foreach (var importField in importFields)
            {
                DoFieldUpdate(importField, EntityState.Added);
            }
            return ToJsonResult(importFields);
        }
        [Authorize(Roles = "editImportCenter")]
        [HttpPost]
        public ActionResult CreateField(ImportCenterFieldDefinition importField)
        {
            return ToJsonResult(DoFieldUpdate(importField, EntityState.Added));
        }
        [Authorize(Roles = "editImportCenter")]
        [HttpPost]
        public ActionResult UpdateField(ImportCenterFieldDefinition importField)
        {
            return ToJsonResult(DoFieldUpdate(importField, EntityState.Modified));
        }
        [Authorize(Roles = "editImportCenter")]
        [HttpPost]
        public ActionResult UpdateFields(IList<ImportCenterFieldDefinition> importFields)
        {
            //Iterate all created products which are posted by the Kendo Grid
            foreach (var importField in importFields)
            {
                DoFieldUpdate(importField, EntityState.Modified);
            }
            return ToJsonResult(importFields);
        }
        [Authorize(Roles = "editImportCenter")]
        [HttpPost]
        public ActionResult DestroyFields(IList<ImportCenterFieldDefinition> importFields)
        {
            //Iterate all created products which are posted by the Kendo Grid
            foreach (var field in importFields)
            {
                DoFieldUpdate(field, EntityState.Deleted);
            }
            return null;
        }
        [Authorize(Roles = "editImportCenter")]
        [HttpPost]
        public ActionResult DestroyField(ImportCenterFieldDefinition importField)
        {
            DoFieldUpdate(importField, EntityState.Deleted);
            return null;
        }

        private ImportCenterFieldDefinition DoFieldUpdate(ImportCenterFieldDefinition importField, EntityState state)
        {
            switch (state)
            {
                case EntityState.Deleted:
                    db.ImportCenterFieldDefinitions.Remove(db.ImportCenterFieldDefinitions.First(d => d.ID == importField.ID));
                    break;
                case EntityState.Modified:
                {
                    // ensure any existing Fields are removed (will be "replaced" below)
                    // TODO: would be better to do this removal within EF so the entire update is atomic (currently it is NOT)
                    using (SSDB ssdb = new SSDB())
                    {
                        ssdb.ExecuteSql("DELETE FROM tblImportCenterFieldDefinitionField WHERE ImportCenterFieldID={0}", importField.ID);
                    }

                    var original = db.ImportCenterFieldDefinitions.Where(f => f.ID == importField.ID).First();

                    // ensure we assign an incrementing a Position to the new Fields
                    byte position = 0;
                    foreach (var f in importField.Fields)
                    {
                        f.Position = position++;
                        original.Fields.Add(f);
                    }
                    // update all other values in the Field
                    DateTime createDateUTC = original.CreateDateUTC.Value;
                    string createdByUser = original.CreatedByUser;
                    db.Entry(original).CurrentValues.SetValues(importField);
                    original.CreateDateUTC = createDateUTC;
                    original.CreatedByUser = createdByUser;
                    break;
                }
                case EntityState.Added:
                {
                    db.ImportCenterFieldDefinitions.Add(importField);
                    // ensure we assign an incrementing a Position to the new Fields
                    byte position = 0;
                    foreach (var f in importField.Fields)
                    {
                        if (f != null) f.Position = position++;
                    }
                    break;
                }
            }
            // Insert all created products to the database
            db.SaveChanges(UserSupport.UserName);

            return importField;
        }

        [HttpGet]
        public ContentResult ImportDefinitions(int rootObjectID)
        {
            return ToJsonResult(
                db.ImportCenterDefinitions
                    .Where(
                        d => d.RootObjectID == rootObjectID
                            && (d.UserNames == null || ("," + d.UserNames + ",").Contains("," + User.Identity.Name + ","))
                    )
                    .Include(ic => ic.InputFile)
                    .OrderBy(d => d.Name)
            );
        }

        // GET /ImportCenter/ImportFields
        [HttpGet]
        public ContentResult ImportFields(int id)
        {
            var fields = db.ImportCenterFieldDefinitions
                .Where(t => t.ImportCenterDefinitionID == id)
                // special SELECT projection to ensure the child records are sorted by Position, ImportFieldName
                // see: http://stackoverflow.com/questions/9938956/entity-framework-loading-child-collection-with-sort-order
                .Select(t => new { T = t, Fields = t.Fields.OrderBy(f => f.ImportFieldName).OrderBy(f => f.Position) })
                // these "secondary" steps extract the sorted "inner" data from above list()

                .ToList()
                .Select(g => g.T)
                .ToList();

            return ToJsonResult(fields);
        }

        [HttpGet]
        public ContentResult ObjectsRoot()
        {
            return Content(App_Code.RestControllerHelper.GetJson(
                objectName: "object"
                , where: "SqlTargetName IS NOT NULL"
                , orderBy: "Name"), "application/json");
        }

        [HttpGet]
        public ContentResult ObjectFields()
        {
            return Content(App_Code.RestControllerHelper.GetJson("ObjectField", fieldsCSV: "ID,Name=CASE WHEN ParentObjectID IS NULL THEN Name ELSE '+' + Name END,ObjectID,ParentObjectID,ObjectFieldTypeID", orderBy: "Name"), "application/json");
        }

        [HttpGet]
        public ContentResult ComparisonTypes()
        {
            return Content(App_Code.RestControllerHelper.GetJson("ImportCenterComparisonType", "ID,Name,ObjectFieldTypes=',' + ObjectFieldTypeID_CSV + ','"), "application/json");
        }

        [Authorize(Roles = "editImportCenter")]
        [HttpGet]
        public FileResult ExportDefinition(int id)
        {
            DataSet ds = new DataSet();
            ds.DataSetName = "ImportDS";
            string fileName = "export";
            using (SSDB db = new SSDB())
            {
                DBHelper.AddDsTbl(ds, db.GetPopulatedDataTable("SELECT * FROM tblImportCenterDefinition WHERE ID = {0}", (object)id), "Import");
                DBHelper.AddDsTbl(ds, db.GetPopulatedDataTable("SELECT * FROM tblImportCenterInputFile WHERE ImportCenterDefinitionID = {0}", (object)id), "InputFile");
                DBHelper.AddDsTbl(ds, db.GetPopulatedDataTable("SELECT * FROM tblImportCenterFieldDefinition WHERE ImportCenterDefinitionID = {0}", (object)id), "ImportFields");
                DBHelper.AddDsTbl(ds, db.GetPopulatedDataTable("SELECT * FROM tblImportCenterFieldDefinitionField WHERE ImportCenterFieldID IN (SELECT ID FROM tblImportCenterFieldDefinition WHERE ImportCenterDefinitionID = {0})", (object)id), "ImportField_Fields");
                DBHelper.AddDsTbl(ds, db.GetPopulatedDataTable("SELECT * FROM tblObjectField WHERE IsCustom = 1 AND ID IN (SELECT ObjectFieldID FROM tblImportCenterFieldDefinition WHERE ImportCenterDefinitionID = {0})", (object)id), "ObjectFields");
                fileName = Path.ChangeExtension(db.QuerySingleValue("SELECT (SELECT Name FROM tblObject WHERE ID = X.RootObjectID) + '_' + name FROM tblImportCenterDefinition X WHERE ID = {0}", id).ToString(), "xml");
                // remove any invalid characters
                fileName = Regex.Replace(fileName, @"[\/?:*""><|]+", "", RegexOptions.Compiled);
            }
            MemoryStream ms = new MemoryStream();
            ds.WriteXml(ms, XmlWriteMode.WriteSchema);

            return File(ms.ToArray(), "text/xml", fileName);
        }


        [Authorize(Roles = "viewImportCenter, editImportCenter")]
        [HttpPost]
        public ActionResult UpdateDataFile(IEnumerable<HttpPostedFileBase> dataFile, bool firstRowHeader, string rootObject)
        {
            if (dataFile != null)
            {
                foreach (var file in dataFile)
                {
                    byte[] inArray = new byte[(int)file.InputStream.Length];
                    file.InputStream.Read(inArray, 0, inArray.Length);

                    Session[SESSION_DATAFILE] = Convert.ToBase64String(inArray);
                    Session[SESSION_DATAFILENAME] = file.FileName;
                    Session[SESSION_DATAFIRSTROWHEADER] = firstRowHeader;
                    Session[SESSION_ROOTOBJECT] = rootObject;
                    // we only accept a single file so just break
                    break;
                }
            }
            // Return an empty string to signify success
            return Content("");
        }

        [HttpPost]
        [Authorize(Roles = "viewImportCenter")]
        public ActionResult ExecuteImport(int id, int rootObjectID)
        {
            return ExecuteInt(id, rootObjectID, false);
        }

        protected ActionResult ExecuteInt(int id, int rootObjectID, bool testOnly = false)
        {
            ProcessResult ret = new ProcessResult(rootObjectID, id, null);
            if (Session[SESSION_DATAFILE] != null)
            {
                string fileContents = Session[SESSION_DATAFILE].ToString()
                    , fileName = Session[SESSION_DATAFILENAME].ToString();
                bool firstRowHeader = DBHelper.ToBoolean(Session[SESSION_DATAFIRSTROWHEADER]);
                // convert string -> byte[] -> stream
                MemoryStream inputStream = new MemoryStream(Convert.FromBase64String(fileContents));
                inputStream.Position = 0;
                // convert stream into datatable
                DataTable dtData = ImportCenterInputHelper.ToDataTable(fileName, inputStream, firstRowHeader, SESSION_DT_NAME);

                try
                {
                    ImportCenterEngine logic = new ImportCenterEngine(
                        id
                        , rootObjectID
                        , dtData
                        , fileName);
                    ret = logic.Process(testOnly);
                }
                catch (ImportCenterEngine.ProcessingException_InvalidMapping ex)
                {
                    ret.ImportFileName = fileName;
                    ret.GeneralError = ex.Message;
                }
                // persist the resultant DataTable to SESSION state (for potential download)
                Session[SESSION_EXECUTE_DATATABLE] = ret.DataTable;
            }
            else
            {
                ret.GeneralError = "Missing Input file, re-provide and try again";
            }
            // return null if ret = null (sucess) or the results for display (without the DataTable content) if some type of failure
            return ToJsonResult(ret, ToJSonSettings("DataTable"));
        }

        [HttpGet]
        public ActionResult ExecuteExport()
        {
            DataTable dtResults = Session[SESSION_EXECUTE_DATATABLE] as DataTable;
            string rootObject = Session[SESSION_ROOTOBJECT].ToString();
            string importFileName = Path.GetFileNameWithoutExtension(Session[SESSION_DATAFILENAME].ToString());
            if (dtResults != null)
            {
                using (ExcelEngine excelEngine = new ExcelEngine())
                {
                    IApplication application = excelEngine.Excel;
                    application.DefaultVersion = ExcelVersion.Excel2013;
                    IWorkbook workbook = application.Workbooks.Create(1);
                    IWorksheet sheet = workbook.Worksheets[0];
                    sheet.ImportDataTable(dtResults, true, 1, 1);
                    MemoryStream ms = new MemoryStream();
                    workbook.SaveAs(ms, ExcelSaveType.SaveAsXLS);
                    string defaultFileName = string.Format("{0}_ImportOutcome - [{1}] ({2:yyyy-MM-dd}).xlsx", importFileName, rootObject, DateTime.Now.Date);
                    return File(ms.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", defaultFileName);
                }
            }
            // TODO: return the same page with an error message stating what went wrong?
            return null;
        }

        [Authorize(Roles = "editImportCenter")]
        [HttpPost]
        public ActionResult UpdateInputFile(IEnumerable<HttpPostedFileBase> inputFile)
        {
            // The Name of the Upload component is "inputFile"
            if (inputFile != null)
            {
                foreach (var file in inputFile)
                {
                    byte[] inArray = new byte[(int)file.InputStream.Length];
                    file.InputStream.Read(inArray, 0, inArray.Length);

                    Session[SESSION_INPUTFILE] = Convert.ToBase64String(inArray);
                    Session[SESSION_INPUTFILENAME] = file.FileName;
                    // we only accept a single file so just break
                    break;
                }
            }
            // Return an empty string to signify success
            return Content("");
        }

        [Authorize(Roles = "editImportCenter")]
        [HttpPost]
        public ContentResult SaveInputFile(int importCenterID, bool firstRowHeader)
        {
            if (importCenterID != 0 && Session[SESSION_INPUTFILE] != null && Session[SESSION_INPUTFILENAME] != null)
            {
                string fileContents = Session[SESSION_INPUTFILE].ToString()
                    , fileName = Session[SESSION_INPUTFILENAME].ToString();
                // convert string -> byte[] -> stream
                MemoryStream inputStream = new MemoryStream(Convert.FromBase64String(fileContents));
                inputStream.Position = 0;
                // convert stream into datatable
                DataTable dtInput = ImportCenterInputHelper.ToDataTable(fileName, inputStream, firstRowHeader, SESSION_DT_NAME);
                // generate the fieldListCSV values (from the datatable)
                string[] fieldNames = dtInput.Columns.Cast<DataColumn>().Select(column => column.ColumnName).ToArray();
                // serialize datatable into a stream/string
                MemoryStream outputStream = new MemoryStream();
                dtInput.WriteXml(outputStream);
                // reset the stream to the beginning
                outputStream.Position = 0;
                // convert stream to a serialized string
                string inputXML = new StreamReader(outputStream).ReadToEnd();
                // save the new/updated record to the db
                using (DispatchCrudeDB db = new DispatchCrudeDB())
                {
                    var data = db.Set<ImportCenterInputFile>().FirstOrDefault(icif => icif.ImportCenterDefinitionID == importCenterID);
                    if (data == null) data = db.Set<ImportCenterInputFile>().Add(db.Set<ImportCenterInputFile>().Create());
                    data.ImportCenterDefinitionID = importCenterID;
                    data.OriginalFileName = fileName;
                    data.InputDataTableXml = inputXML;
                    data.FieldNameCSV = string.Join(",", fieldNames);
                    data.CreateDateUTC = DateTime.UtcNow;
                    data.CreatedByUser = User.Identity.Name;
                    db.SaveChanges(User.Identity.Name);
                    // return the new InputFieldNames (which could have changed)
                    return ToJsonResult(new { InputFile = data, Error = "" });
                }
            }
            return ToJsonResult(new { Error = "Invalid input parameters" });
        }

        [HttpGet]
        public FileContentResult InputFileExport(int id)
        {
            var data = db.Set<ImportCenterInputFile>().FirstOrDefault(i => i.ImportCenterDefinitionID == id);
            if (data != null)
            {
                MemoryStream ms;
                // de-serialize the datatable contents of the original input file
                DataSet dsInputFile = new DataSet();
                TextReader tr = new StringReader(data.InputDataTableXml);
                dsInputFile.ReadXml(tr);
                // import the original data into an excel spreadsheet
                using (ExcelEngine excel = new ExcelEngine())
                {
                    IWorkbook wb = excel.Excel.Workbooks.Add(ExcelVersion.Excel2013);
                    IWorksheet sheet = wb.Worksheets[0];
                    sheet.ImportDataTable(dsInputFile.Tables[0], true, 1, 1);
                    ms = new MemoryStream();
                    wb.SaveAs(ms);
                }
                return File(ms.ToArray(), "text/xlsx", Path.ChangeExtension(data.OriginalFileName, "xlsx"));
            }
            return null;
        }

        [Authorize(Roles = "editImportCenter")]
        [HttpPost]
        public ActionResult ImportDefinitionXml(IEnumerable<HttpPostedFileBase> xmlFile)
        {
            // The Name of the Upload component is "xmlFile"
            if (xmlFile != null)
            {
                foreach (var file in xmlFile)
                {
                    Session[SESSION_DEFINITION_XML] = new StreamReader(file.InputStream).ReadToEnd();
                    // we only accept a single file so just break
                    break;
                }
            }
            // Return an empty string to signify success
            return Content("");
        }

        [Authorize(Roles = "editImportCenter")]
        [HttpPost]
        public ContentResult ImportDefinition(string name, string userNames = null)
        {
            int ret = 0;
            DataSet ds = new DataSet();
            TextReader tr = new StringReader(Session[SESSION_DEFINITION_XML].ToString());
            ds.ReadXml(tr, XmlReadMode.ReadSchema);

            ret = new Importer().ImportFromDS(ds, name, userNames);

            return ToJsonResult(new { id = ret });
        }

        internal class Importer
        {
            private class FieldDict : Dictionary<int, int>
            {
                public FieldDict() : base() { }
                public FieldDict(FieldDict dict) : base(dict) { }
            }

            public int ImportFromDS(DataSet ds, string name, string userNames)
            {
                int ret = 0;
                using (SSDB db = new SSDB(true))
                {
                    ret = ImportMain(db, ds.Tables["Import"].Rows[0], name, userNames);

                    ImportInputFile(db, ret, ds.Tables["InputFile"]);

                    FieldDict dlOF = ImportObjectFields(db, ds.Tables["ObjectFields"]);

                    FieldDict dlIF = ImportFields(db, ret, ds.Tables["ImportFields"].Select("ParentFieldID IS NULL"), dlOF);

                    ImportField_Fields(db, ds.Tables["ImportField_Fields"], dlIF);

                    db.CommitTransaction();
                }
                return ret;
            }

            private int ImportMain(SSDB db, DataRow drImport, string name, string userNames)
            {
                int ret = 0;
                drImport["ID"] = 0;
                drImport["Name"] = name;
                drImport["UserNames"] = (userNames as object) ?? DBNull.Value;
                ret = DBHelper.ToInt32(SSDBSave.SaveDataRow(db, drImport, "tblImportCenterDefinition", "ID", UserSupport.UserName));

                return ret;
            }

            private void ImportInputFile(SSDB db, int newImportCenterDefinitionID, DataTable dtIF)
            {
                foreach (DataRow drIF in dtIF.Rows)
                {
                    drIF["ImportCenterDefinitionID"] = newImportCenterDefinitionID;
                    DBHelper.ToInt32(SSDBSave.SaveDataRow(db, drIF, "tblImportCenterInputFile", "ImportCenterDefinitionID", UserSupport.UserName));
                }
            }

            private FieldDict ImportObjectFields(SSDB db, DataTable dtOF)
            {
                FieldDict ret = new FieldDict();
                foreach (DataRow drObjectField in dtOF.Rows)
                {
                    int key = DBHelper.ToInt32(drObjectField["ID"]);
                    int newID = DBHelper.ToInt32(db.QuerySingleValue("SELECT ID FROM tblObjectField WHERE Name LIKE {0}", DBHelper.QuoteStr(drObjectField["Name"])));
                    if (newID == 0)
                    {
                        drObjectField["ID"] = 0;
                        drObjectField.SetAdded();
                        newID = DBHelper.ToInt32(SSDBSave.SaveDataRow(db, drObjectField, "tblObjectField", "ID", UserSupport.UserName));
                    }
                    ret.Add(key, newID);
                }
                return ret;
            }

            private FieldDict ImportFields(SSDB db, int importID, DataRow[] drImportFields, FieldDict dlOF, FieldDict dlIF = null)
            {
                FieldDict ret = dlIF == null ? new FieldDict() : new FieldDict(dlIF);
                
                foreach (DataRow drImportField in drImportFields)
                {
                    int key = DBHelper.ToInt32(drImportField["ID"]);
                    drImportField["ID"] = 0;
                    if (dlOF.ContainsKey(DBHelper.ToInt32(drImportField["ObjectFieldID"])))
                        drImportField["ObjectFieldID"] = dlOF[DBHelper.ToInt32(drImportField["ObjectFieldID"])];
                    if (ret.ContainsKey(DBHelper.ToInt32(drImportField["ParentFieldID"])))
                        drImportField["ParentFieldID"] = ret[DBHelper.ToInt32(drImportField["ParentFieldID"])];
                    drImportField["ImportCenterDefinitionID"] = importID;
                    int newID = DBHelper.ToInt32(SSDBSave.SaveDataRow(db, drImportField, "tblImportCenterFieldDefinition", "ID", UserSupport.UserName));
                    ret.Add(key, newID);
                    ret = ImportFields(db, importID, drImportField.Table.Select(string.Format("ParentFieldID = {0}", key)), dlOF, ret);
                }

                return ret;
            }

            private void ImportField_Fields(SSDB db, DataTable dtFields, FieldDict dlIF)
            {
                foreach (DataRow drIFF in dtFields.Rows)
                {
                    drIFF["ID"] = 0;
                    drIFF["ImportCenterFieldID"] = dlIF[DBHelper.ToInt32(drIFF["ImportCenterFieldID"])];
                }
                SSDBSave.SaveDataTable(db, dtFields, "tblImportCenterFieldDefinitionField", "ID", UserSupport.UserName);
            }
        }

        #region ToJsonResult
        private ContentResult ToJsonResult(object obj, JsonSerializerSettings settings = null)
        {
            if (settings == null) settings = ToJSonSettings();
            string json = JsonConvert.SerializeObject(obj, settings);
            return Content(json, "application/json");
        }

        static private JsonSerializerSettings ToJSonSettings(params string[] skipProperties)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings();
            // assign default skipProperties if not supplied (use default unless a new list is supplied)
            if (skipProperties == null && skipProperties.Length == 0) skipProperties = new string[] { "ImportCenterDefinition", "ImportCenterFieldDefinition", "RootObject", "Object", "ObjectField", "ParentField", "KeyComparisonType", "CreateDateUTC", "CreatedByUser", "LastChangeDateUTC", "LastChangedByUser" };
            settings.ContractResolver = new JsonPropListExcludeContractResolver(skipProperties);
            settings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            settings.Formatting = Formatting.Indented;
            settings.DateFormatHandling = DateFormatHandling.IsoDateFormat;
            return settings;
        }
        #endregion
    }
}
