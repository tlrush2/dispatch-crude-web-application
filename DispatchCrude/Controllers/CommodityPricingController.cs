﻿﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using DispatchCrude.Models;
using DispatchCrude.App_Code;
using DispatchCrude.ViewModels;

namespace DispatchCrude.Controllers
{
    [Authorize(Roles= "viewCommodityPricing")]
    public class CommodityPricingController : Controller
    {
        private DispatchCrudeDB db = new DispatchCrudeDB();

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        #region Indexes

        public ActionResult Indexes()
        {
            ViewBag.Title = "Commodity Pricing";
            var productGroups = db.ProductGroups.Select(c => new { c.ID, c.Name }).OrderBy(c => c.Name);
            ViewData["ddlProductGroupID"] = productGroups;
            var uoms = (from u in db.Uoms
                        select new
                        {
                            ID = u.ID,
                            Name = u.Name
                        }).ToList();
            ViewData["ddlUomID"] = uoms;
            return View();
        }

        public ContentResult CommodityIndexes(DataSourceRequest request)
        {
            var data = db.CommodityIndexes;
            return JsonStringResult.Create(data.ToDataSourceResult(request));
        }

        [Authorize(Roles = "createCommodityPricing")]
        public ActionResult CommodityIndex_Create(DataSourceRequest request, [Bind(Exclude = "ProductGroup,Uom")] CommodityIndex index)
        {
            if (ModelState.IsValid)
            {
                db.CommodityIndexes.Add(index);
                db.SaveChanges(User.Identity.Name);
            }
            //return Json(new[] { index }.ToDataSourceResult(request, ModelState));
            return Json(ModelState.ToDataSourceResult());
        }

        [Authorize(Roles = "editCommodityPricing")]
        public ActionResult CommodityIndex_Update(DataSourceRequest request, CommodityIndex index)
        {
            if (ModelState.IsValid)
            {
                db.CommodityIndexes.Attach(index);
                db.Entry(index).State = EntityState.Modified;
                db.SaveChanges(User.Identity.Name);
                return RedirectToAction("Indexes");
            }
            return Json(new[] { index }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        #region Prices

        public ActionResult Prices()
        {
            //ViewBag.Title = "Commodity Pricing";
            var commodityIndexes = db.CommodityIndexes.Select(i => new { i.ID, Name = i.ShortName }).OrderBy(i => i.Name);
            ViewData["ddlCommodityIndexID"] = commodityIndexes;
            return View();
        }

        //public ContentResult CommodityPrices(DataSourceRequest request)
        //{
        //    var data = db.CommodityPrices;
        //    return JsonStringResult.Create(data.ToDataSourceResult(request));
        //}

        //[HttpPost] //[ValidateAntiForgeryToken] - still trying to figure out a way to send this token along with the dictionary object data
        //[Authorize(Roles = "editCommodityPricing")]
        //public ContentResult Update(CommodityPrice price)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        var errors = ModelState.Values.SelectMany(v => v.Errors);
        //    }
        //    else
        //    {
        //        CommodityPrice existing = null;
        //        if (price.ID != 0)
        //        {
        //            existing = db.CommodityPrices.Find(price.ID);
        //            //db.CopyEntityValues(existing, price);
        //            db.Entry(existing).State = EntityState.Modified;
        //        }
        //        else
        //            existing = db.CommodityPrices.Add(price);
        //        db.SaveChanges(User.Identity.Name);
        //        return JsonStringResult.Create(existing);
        //    }
        //    // TODO: how to handle an invalid model state
        //    return new JsonStringResult(null);
        //}

        public ActionResult CommodityPrices_Read([DataSourceRequest] DataSourceRequest request = null, int id = 0)
        {
            return CommodityPrices_Read(request, null, id);
        }

        private ContentResult CommodityPrices_Read([DataSourceRequest] DataSourceRequest request, ModelStateDictionary modelState, int id = 0)
        {
            // .ToList() is required due to filtering on "Active" field, which is NOT MAPPED
            var data = db.CommodityPrices.Where(m => m.ID == id || id == 0)
                                            .Include(m => m.CommodityIndex).ToList();

            var result = data.ToDataSourceResult(request, modelState);

            return App_Code.JsonStringResult.Create(result);
        }

        [Authorize(Roles = "createCommodityPricing")]
        public ActionResult CommodityPrices_Create(DataSourceRequest request, [Bind(Exclude = "CommodityIndex")] CommodityPrice commodityPrice)
        {
            if (commodityPrice.CommodityIndexID <= 0)
                ModelState.AddModelError("CommodityIndex", "Index is required");

            if (ModelState.IsValid)
            {
                try
                {
                    // Save new record to the database
                    db.CommodityPrices.Add(commodityPrice);
                    db.SaveChanges(User.Identity.Name);  // Create dates & usernames will all be taken care of by this line.
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(null, ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { commodityPrice }.ToDataSourceResult(request, ModelState));
                }
            }

            return CommodityPrices_Read(request, ModelState, commodityPrice.ID);

        }

        [Authorize(Roles = "editCommodityPricing")]
        public ActionResult CommodityPrices_Update(DataSourceRequest request, CommodityPrice commodityPrice)
        {
            //CommodityPrice price = db.CommodityPrices.Find(commodityPrice.ID);
            //if (price.Locked)
            //{
            //    ModelState.AddModelError("PriceDate", "Price is locked");
            //}

            //if (ModelState.IsValid)
            //{
            //    price.PriceDate = commodityPrice.PriceDate;
            //    price.CommodityIndexID = commodityPrice.CommodityIndexID;
            //    price.IndexPrice = commodityPrice.IndexPrice;

            //    db.Entry(price).State = EntityState.Modified;
            //    db.SaveChanges(User.Identity.Name);
            //}

            //return Json(new[] { price }.ToDataSourceResult(request, ModelState));

            if (ModelState.IsValid)
            {
                try
                {
                    // Get the existing record
                    CommodityPrice existing = db.CommodityPrices.Find(commodityPrice.ID);

                    // Specify all fields that will/can be updated by user input on the popup
                    db.CopyEntityValues(existing, commodityPrice);
                    db.SaveChanges(User.Identity.Name);  // Create & Modified dates & usernames will be taken care of by this line.
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(null, ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { commodityPrice }.ToDataSourceResult(request, ModelState));
                }
            }

            return CommodityPrices_Read(request, ModelState, commodityPrice.ID);
        }

        #endregion

        #region Methods

        public ActionResult Methods()
        {
            ViewBag.Title = "Commodity Pricing";
            return View();
        }

        public ContentResult CommodityMethods(DataSourceRequest request)
        {
            var data = db.CommodityMethods;
            return JsonStringResult.Create(data.ToDataSourceResult(request));
        }

        #endregion

        #region Non-Trade Days

        public ActionResult NonTradeDays()
        {
            ViewBag.Title = "Commodity Pricing";
            //var holidays = from h in db.NonTradeDays
            //               orderby h.HolidayDate
            //               select h;
            //return View(holidays);
            return View(db.NonTradeDays.ToList());
        }

        [HttpPost]
        [Authorize(Roles = "createCommodityPricing")]
        public ActionResult NonTradeDays_Create(NonTradeDays NTD)
        {
            if (ModelState.IsValid)
            {
                // Save new record to the database
                db.NonTradeDays.Add(NTD);
                db.SaveChanges(User.Identity.Name);  // Create dates & usernames will all be taken care of by this line.

                return RedirectToAction("NonTradeDays");
            }

            return View("NonTradeDays");
        }

        [HttpPost]
        [Authorize(Roles = "editCommodityPricing")]
        public ActionResult NonTradeDays_Update(NonTradeDays NTD)
        {
            NonTradeDays existing = db.NonTradeDays.Find(NTD.HolidayDate);

            if (ModelState.IsValid)
            {
                // Update the curent record
                existing.HolidayDate = NTD.HolidayDate;
                existing.Name = NTD.Name;

                // Save changes to the database
                db.Entry(existing).State = EntityState.Modified;
                db.SaveChanges(User.Identity.Name);  // Create & Modified dates & usernames will be taken care of by this line.

                return RedirectToAction("NonTradeDays");
            }

            return View("NonTradeDays");
        }

        [Authorize(Roles = "deactivateCommodityPricing")]
        public ActionResult NonTradeDays_ActiveState(NonTradeDays NTD)
        {
            NonTradeDays selected = db.NonTradeDays.Find(NTD.HolidayDate);

            if (selected.DeleteDateUTC == null)
            {
                // If there is no delete date, we're deactivating this Non-Trade Date
                selected.DeleteDateUTC = DateTime.UtcNow;
                selected.DeletedByUser = User.Identity.Name;
            }
            else
            {
                // If there is a delete date, we're reactivating this Non-Trade Date
                selected.DeleteDateUTC = null;
                selected.DeletedByUser = null;
            }

            // Save record
            db.Entry(selected).State = EntityState.Modified;
            db.SaveChanges(User.Identity.Name);

            return RedirectToAction("NonTradeDays");
        }

        public ActionResult IsDateAvailable(string HolidayDate)
        {
            //try
            //{
            //    var date = db.NonTradeDays.Single(d => d.HolidayDate.ToString() == HolidayDate);
            //    return Json(false, JsonRequestBehavior.AllowGet);
            //}
            //catch (Exception)
            //{
            //    return Json(true, JsonRequestBehavior.AllowGet);
            //}

            var model = db.NonTradeDays.Where(x => x.HolidayDate.ToString() != HolidayDate);

            if (model.Count() != 0)
                ModelState.AddModelError("HolidayDate", "That date is already in the list.");

            return Json(model.Count() == 0);
        }

        #endregion

        #region Pricebook

        public ActionResult Pricebook(int? orderid)
        {
            ViewBag.Title = "Commodity Pricing";
            if (orderid != null)
            {
                Order order = db.Orders.Find(orderid);
                var data = db.CommodityPurchasePricebooks.Where(x => (x.ProducerID == null || x.ProducerID == order.ProducerID)
                                                                  && (x.OperatorID == null || x.OperatorID == order.OperatorID)
                                                                  && (x.ProductID == null || x.ProductID == order.ProductID)
                                                                  && (x.ProductGroupID == null || x.ProductGroupID == order.Product.ProductGroupID)
                                                                  && (x.ProductID == null || x.ProductID == order.ProductID)
                                                                  && (x.OriginID == null || x.OriginID == order.OriginID)
                                                                  && (x.OriginRegionID == null || x.OriginRegionID == order.Origin.RegionID)
                                                                  && (x.OriginStateID == null || x.OriginStateID == order.Origin.StateID))
                                                .Include(x => x.Producer).Include(x => x.Operator)
                                                .Include(x => x.Product).Include(x => x.ProductGroup)
                                                .Include(x => x.OriginState).Include(x => x.Origin).Include(x => x.OriginRegion);
                ViewBag.Subtitle = "Pricebook [Matching Order # " + order.OrderNum + "]";
                return View(data);
            }
            else
            {
                var data = db.CommodityPurchasePricebooks
                                                .Include(x => x.Producer).Include(x => x.Operator)
                                                .Include(x => x.Product).Include(x => x.ProductGroup)
                                                .Include(x => x.OriginState).Include(x => x.Origin).Include(x => x.OriginRegion);
                ViewBag.Subtitle = "Pricebook";
                return View(data);
            }
        }

        public ActionResult PurchasePricebooks(DataSourceRequest request)
        {
            var data = db.CommodityPurchasePricebooks.Include(x => x.OriginState).Include(x => x.Origin).Include(x => x.OriginRegion);
            return JsonStringResult.Create(data.ToDataSourceResult(request));
        }

        [Authorize(Roles = "editCommodityPricing")]
        public ActionResult EditPricebook(int? id)
        {
            CommodityPurchasePricebook pricebook = null;
            if (id == null || id == 0)
            {
                // new addition
                pricebook = new CommodityPurchasePricebook();
            }
            else
            {
                // update to existing record
                pricebook = db.CommodityPurchasePricebooks.Find(id);
                if (pricebook == null)
                {
                    TempData["error"] = "Cannot find that pricebook!";
                    return RedirectToAction("Index");
                }
            }
            ViewBag.Locked = pricebook.Locked;
            ViewBag.ProducerID = new SelectList(db.Producers.OrderBy(p => p.Name), "ID", "Name", pricebook.ProducerID);
            ViewBag.OperatorID = new SelectList(db.Operators.OrderBy(o => o.Name), "ID", "Name", pricebook.OperatorID);
            ViewBag.ProductID = new SelectList(db.Products.OrderBy(x => x.Name), "ID", "Name", pricebook.ProductID);
            ViewBag.ProductGroupID = new SelectList(db.ProductGroups.OrderBy(x => x.Name), "ID", "Name", pricebook.ProductGroupID);
            ViewBag.OriginID = new SelectList(db.Origins.OrderBy(x => x.Name), "ID", "Name", pricebook.OriginID);
            ViewBag.OriginStateID = new SelectList(db.States.OrderBy(x => x.FullName), "ID", "FullName", pricebook.OriginStateID);
            ViewBag.OriginRegionID = new SelectList(db.Regions.OrderBy(x => x.Name), "ID", "Name", pricebook.OriginRegionID);
            ViewBag.CommodityIndexID = new SelectList(db.CommodityIndexes.OrderBy(x => x.Name), "ID", "ShortName", pricebook.CommodityIndexID);
            ViewBag.CommodityMethodID = new SelectList(db.CommodityMethods, "ID", "ShortName", pricebook.CommodityMethodID);
            ViewBag.SettlementFactorID = new SelectList(db.SettlementFactors, "ID", "Name", pricebook.SettlementFactorID);

            return View(pricebook);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "editCommodityPricing")]
        public ActionResult EditPricebook(CommodityPurchasePricebook updated)
        {
            CommodityPurchasePricebook existing = db.CommodityPurchasePricebooks.Find(updated.ID);

            if (updated.OrderStartDate > updated.OrderEndDate)
            {
                ModelState.AddModelError("OrderEndDate", "Order end date cannot be before start date!");
            }
            if (updated.CommodityMethodID != (int)CommodityMethod.TYPE.PICK)
            {
                if (updated.IndexEndDate == null)
                {
                    ModelState.AddModelError("IndexEndDate", "The Index End Date field is required.");
                }
                else if (updated.IndexStartDate > updated.IndexEndDate)
                {
                    ModelState.AddModelError("IndexEndDate", "Index end date cannot be before start date!");
                }
            }
            if (existing != null && existing.Locked)
            {
                if (updated.OrderEndDate != existing.OrderEndDate && updated.OrderEndDate < existing.LockDate)
                {
                    ModelState.AddModelError("OrderEndDate", "Order end date cannot be prior to last settled order!");
                }
            }


            if (ModelState.IsValid)
            {
                if (updated.ID == 0)
                {
                    // new addition
                    if (updated.CommodityMethodID == (int)CommodityMethod.TYPE.PICK)
                    {
                        updated.IndexEndDate = updated.IndexStartDate; // Hard code to start date when method is PICK/single day
                    }
                    updated.CreateDateUTC = DateTime.UtcNow;
                    updated.CreatedByUser = User.Identity.Name;
                    db.CommodityPurchasePricebooks.Add(updated);
                }
                else
                {
                    // update
                    existing.OrderEndDate = updated.OrderEndDate;
                    if (!existing.Locked)
                    {
                        existing.OrderStartDate = updated.OrderStartDate;
                        existing.ProducerID = updated.ProducerID;
                        existing.OperatorID = updated.OperatorID;
                        existing.ProductID = updated.ProductID;
                        existing.ProductGroupID = updated.ProductGroupID;
                        existing.OriginID = updated.OriginID;
                        existing.OriginStateID = updated.OriginStateID;
                        existing.OriginRegionID = updated.OriginRegionID;
                        existing.CommodityIndexID = updated.CommodityIndexID;
                        existing.CommodityMethodID = updated.CommodityMethodID;
                        existing.IndexStartDate = updated.IndexStartDate;
                        existing.IndexEndDate = (updated.CommodityMethodID == (int)CommodityMethod.TYPE.PICK) ? updated.IndexStartDate : updated.IndexEndDate; // Hard code to start date when method is PICK/single day
                        existing.SettlementFactorID = updated.SettlementFactorID;
                        existing.Deduct = updated.Deduct;
                        existing.DeductType = updated.DeductType;
                        existing.Premium = updated.Premium;
                        existing.PremiumType = updated.PremiumType;
                        existing.PremiumDesc = updated.PremiumDesc;

                    }
                    existing.LastChangeDateUTC = DateTime.UtcNow;
                    existing.LastChangedByUser = User.Identity.Name;
                    db.Entry(existing).CurrentValues.SetValues(existing);
                }
                db.SaveChanges(User.Identity.Name);
                TempData["success"] = "Pricebook saved successfully";
                return RedirectToAction("Pricebook");
            }

            ViewBag.Locked = updated.Locked;
            ViewBag.ProducerID = new SelectList(db.Producers.OrderBy(p => p.Name), "ID", "Name", updated.ProducerID);
            ViewBag.OperatorID = new SelectList(db.Operators.OrderBy(o => o.Name), "ID", "Name", updated.OperatorID);
            ViewBag.ProductID = new SelectList(db.Products.OrderBy(x => x.Name), "ID", "Name", updated.ProductID);
            ViewBag.ProductGroupID = new SelectList(db.ProductGroups.OrderBy(x => x.Name), "ID", "Name", updated.ProductGroupID);
            ViewBag.OriginID = new SelectList(db.Origins.OrderBy(x => x.Name), "ID", "Name", updated.OriginID);
            ViewBag.OriginStateID = new SelectList(db.States.OrderBy(x => x.FullName), "ID", "FullName", updated.OriginStateID);
            ViewBag.OriginRegionID = new SelectList(db.Regions.OrderBy(x => x.Name), "ID", "Name", updated.OriginRegionID);
            ViewBag.CommodityIndexID = new SelectList(db.CommodityIndexes.OrderBy(x => x.Name), "ID", "ShortName", updated.CommodityIndexID);
            ViewBag.CommodityMethodID = new SelectList(db.CommodityMethods, "ID", "ShortName", updated.CommodityMethodID);
            ViewBag.SettlementFactorID = new SelectList(db.SettlementFactors, "ID", "Name", updated.SettlementFactorID);

            return View(updated);
        }

        [HttpPost]
        [Authorize(Roles = "createCommodityPricing")]
        public ActionResult PurchasePricebook_Create(DataSourceRequest request, CommodityPurchasePricebook purchasePricebook)
        {
            if (ModelState.IsValid)
            {
                db.CommodityPurchasePricebooks.Add(purchasePricebook);
                db.SaveChanges(User.Identity.Name);
            }
            return Json(new[] { purchasePricebook }.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        [Authorize(Roles = "editCommodityPricing")]
        public ContentResult PurchasePricebook_Update(CommodityPurchasePricebook purchasePricebook)
        {
            if (ModelState.IsValid)
            {
                db.CommodityPurchasePricebooks.Attach(purchasePricebook);
                db.Entry(purchasePricebook).State = EntityState.Modified;
                db.SaveChanges(User.Identity.Name);
            }

            object result = new { Data = purchasePricebook, Errors = ModelState.Values.SelectMany(v => v.Errors).ToArray() };
            return JsonStringResult.Create(result);
        }

        #endregion

        #region Support Functions

        [HttpPost]
        public ActionResult Excel_Export_Save(string contentType, string base64, string fileName)
        {
            var fileContents = Convert.FromBase64String(base64);

            return File(fileContents, contentType, fileName);
        }

        [HttpPost]
        public ActionResult Pdf_Export_Save(string contentType, string base64, string fileName)
        {
            var fileContents = Convert.FromBase64String(base64);

            return File(fileContents, contentType, fileName);
        }

        public ContentResult getProducts()
        {
            return Content(App_Code.RestControllerHelper.GetJson(
                    objectName: "Product"
                ), "application/json");
        }
        public ContentResult getProducers()
        {
            return Content(App_Code.RestControllerHelper.GetJson(
                    objectName: "Producer"
                ), "application/json");
        }
        public ContentResult getOperators()
        {
            return Content(App_Code.RestControllerHelper.GetJson(
                    objectName: "Operator"
                ), "application/json");
        }
        public ContentResult getOrigins()
        {
            return Content(App_Code.RestControllerHelper.GetJson(
                    objectName: "Origin"
                ), "application/json");
        }
        public JsonResult getStates()
        {
            var result = (from s in db.States
                          select new
                          {
                              ID = s.ID,
                              Name = s.FullName
                          }).ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ContentResult getRegions()
        {
            return Content(App_Code.RestControllerHelper.GetJson(
                    objectName: "Region"
                ), "application/json");
        }
        public ContentResult getUoms()
        {
            return Content(App_Code.RestControllerHelper.GetJson(
                    objectName: "Uom"
                ), "application/json");
        }

        public ContentResult getSettlementFactors()
        {
            return Content(App_Code.RestControllerHelper.GetJson(
                    objectName: "SettlementFactor"
                ), "application/json");
        }

        //public JsonResult getCommodityIndexes()
        //{
        //    var result = (from i in db.CommodityIndexes
        //                  select new
        //                  {
        //                      ID = i.ID,
        //                      Name = i.ShortName
        //                  }).ToList();

        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}


        public ContentResult getCommodityMethods()
        {
            return Content(App_Code.RestControllerHelper.GetJson(
                    objectName: "CommodityMethod"
                ), "application/json");
        }

        public JsonResult GetProductGroups()
        {
            var result = (from s in db.ProductGroups
                          select new
                          {
                              ID = s.ID,
                              Name = s.Name
                          }).ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #endregion
        
    }

}
