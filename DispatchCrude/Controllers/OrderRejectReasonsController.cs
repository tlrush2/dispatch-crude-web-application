﻿using System;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using DispatchCrude.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;

namespace DispatchCrude.Controllers
{
    [Authorize(Roles = "viewReasonCodes")]
    public class OrderRejectReasonsController : Controller
    {
        private DispatchCrudeDB db = new DispatchCrudeDB();

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        // GET: Read
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest request = null, int id = 0)
        {
            return Read(request, null, id);
        }

        private ContentResult Read([DataSourceRequest] DataSourceRequest request, ModelStateDictionary modelState, int id = 0)
        {
            // .ToList() is required due to filtering on "Active" field, which is NOT MAPPED
            var data = db.OrderRejectReasons.Where(m => m.ID == id || id == 0).ToList();
            var result = data.ToDataSourceResult(request, modelState);
            return App_Code.JsonStringResult.Create(result);
        }

        // POST: Create
        [HttpPost]
        [Authorize(Roles = "createReasonCodes")]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, OrderRejectReason waitReason)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // ensure all new Units of measure are marked "Active" (otherwise they will be created deleted)
                    waitReason.Active = true;

                    // Save new record to the database
                    db.OrderRejectReasons.Add(waitReason);
                    db.SaveChanges(User.Identity.Name);  // Create dates & usernames will all be taken care of by this line.
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(null, ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { waitReason }.ToDataSourceResult(request, ModelState));
                }
            }

            return Read(request, ModelState, waitReason.ID);
        }

        // POST: Update
        [HttpPost]
        [Authorize(Roles = "editReasonCodes")]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, OrderRejectReason waitReason)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // Get the existing record
                    // This allows us to keep the existing file upload if one is not specified in an edit
                    OrderRejectReason existing = db.OrderRejectReasons.Find(waitReason.ID);

                    // Specify all fields that will/can be updated by user input on the popup
                    db.CopyEntityValues(existing, waitReason);

                    db.SaveChanges(User.Identity.Name);  // Create & Modified dates & usernames will be taken care of by this line.
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(null, ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { waitReason }.ToDataSourceResult(request, ModelState));
                }
            }

            return Read(request, ModelState, waitReason.ID);
        }

        // Delete (Deactivate)
        [Authorize(Roles = "deactivateReasonCodes")]
        public ActionResult Delete(OrderRejectReason waitReason)
        {
            db.OrderRejectReasons.Attach(waitReason);
            db.OrderRejectReasons.Remove(waitReason);
            db.SaveChanges(User.Identity.Name);

            return null;
        }
    }
}
