﻿using System.Web.Mvc;

namespace DispatchCrude.Controllers
{
    [Authorize]
    public class LandingPageController : Controller
    {             
        public ActionResult Index()
        {
            return View();
        }        
    }
}
