﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DispatchCrude.Models;
using DispatchCrude.Models.ImportCenter;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;

namespace DispatchCrude.Controllers
{
    [Authorize(Roles = "viewImportCenterGroups")]
    public class ImportCenterGroupController : _DBControllerRead
    {
        static private string SESSION_IMPORTGROUP_EXECUTE_RESULT = "FILE_EXECUTE_RESULT";
        static private string SESSION_IMPORTGROUP_EXECUTE_RESULT_FILENAME = "FILE_EXECUTE_RESULT_FILENAME";

        // GET: ImportCenterGroup
        public ActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = "viewImportCenterGroups")]
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        override protected ContentResult Read([DataSourceRequest] DataSourceRequest request, ModelStateDictionary modelState, int id)
        {
            var data = db.ImportCenterGroups
                .Where(g => ((id == 0 || g.ID == id) && (g.UserNames == null || ("," + g.UserNames + ",").Contains("," + User.Identity.Name + ","))))
                .Include(c => c.Steps.Select(s => s.ImportCenterDefinition));
            return ToJsonResult(data, request, modelState);
        }

        [Authorize(Roles = "viewImportCenterGroups")]
        // GET ImportCenterDefinitions
        public JsonResult ImportCenterGroupSteps()
        {
            var results = db.ImportCenterGroupSteps.Include(s => s.ImportCenterDefinition).ToList();
            return Json(results, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "viewImportCenterGroups")]
        [HttpPost]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, ImportCenterGroup importCenterGroup)
        {
            ImportCenterGroup item = importCenterGroup;
            if (ModelState.IsValid)
            {
                // specialized save logic due to use of multi-select to choose multiple steps (the ID of the Steps is the issue)
                try
                {
                    if (importCenterGroup.ID == 0)
                        db.ImportCenterGroups.Add(item = new ImportCenterGroup(importCenterGroup, db));
                    else
                    {
                        (item = db.ImportCenterGroups.First(g => g.ID == importCenterGroup.ID)).CopyValues(importCenterGroup, db);
                    }
                    db.SaveChanges(User.Identity.Name);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { importCenterGroup }.ToDataSourceResult(request, ModelState));
                }
            }
            return Read(request, ModelState, item.ID);
        }

        // Delete (Deactivate)
        [HttpPost]
        [Authorize(Roles = "viewImportCenterGroups")]
        public ActionResult Delete([DataSourceRequest] DataSourceRequest request, ImportCenterGroup model)
        {
            return DeleteInt(request, model);
        }

        // GET ImportCenterDefinitions
        public JsonResult AllGroupSteps()
        {
            var results = from ic in db.ImportCenterDefinitions
                            select new
                            {
                                ic.ID
                                ,
                                ImportCenterDefinitionID = ic.ID
                                ,
                                ic.Name
                            };
            return Json(results.ToList(), JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult DialogExecute(int id)
        {
            return PartialView("_DialogExecute", id);
        }

        [Authorize(Roles = "viewImportCenterGroups")]
        [HttpPost]
        public ActionResult ExecuteImportGroup(IEnumerable<HttpPostedFileBase> importFiles, int id, string exportFileName)
        {
            string ret = "";
            foreach (var file in importFiles)
            {
                try
                {
                    ImportCenterGroupEngine logic = new ImportCenterGroupEngine();
                    Session.Add(SESSION_IMPORTGROUP_EXECUTE_RESULT, logic.Process(id, file.FileName, file.InputStream, exportFileName));
                    Session.Add(SESSION_IMPORTGROUP_EXECUTE_RESULT_FILENAME, exportFileName);
                }
                catch (Exception ex)
                {
                    ret += ex.Message;
                }
                break;
            }
            // Return an empty string to signify success
            return Content(ret);
        }

        public ActionResult ExecuteResult()
        {
            // TODO: handle situation where the Session variable is not defined (or is invalid?)
            return File(Session[SESSION_IMPORTGROUP_EXECUTE_RESULT] as byte[]
                , "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml"
                , Path.ChangeExtension(Session[SESSION_IMPORTGROUP_EXECUTE_RESULT_FILENAME].ToString(), "xlsx"));
        }

        private static byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
    }
}