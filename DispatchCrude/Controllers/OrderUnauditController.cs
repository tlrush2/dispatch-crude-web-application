﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Data;
using DispatchCrude.Core;
using AlonsIT;

namespace DispatchCrude.Controllers
{
    [Authorize(Roles = "unauditOrders")]
    public class OrderUnauditController : Controller
    {
        //
        // GET: /OrderUnaudit/

        [Authorize(Roles = "unauditOrders")]
        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.Title = "Order Unaudit";
            return View(AuditedData());
        }

        [Authorize(Roles = "unauditOrders")]
        [HttpPost]
        public ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            DataTable auditedData = AuditedData();

            if (request.Aggregates.Any())
            {
                request.Aggregates.Each(agg =>
                {
                    agg.Aggregates.Each(a =>
                    {
                        a.MemberType = auditedData.Columns[agg.Member].DataType;
                    });
                });
            }

            return Json(auditedData.ToDataSourceResult(request));
        }

        [Authorize(Roles = "unauditOrders")]
        [HttpPost]
        public ContentResult Unaudit(int id)
        {
            // mark the record as Audited (Completed) and unapprove
            using (SSDB db = new SSDB())
            {
                // done in multiple steps to avoid the trigger that prevents changes (other than status update) when order in AUDIT status
                db.ExecuteSql("UPDATE tblOrder SET StatusID=3 WHERE ID={0}; "
                    + "UPDATE tblOrder SET DeliverPrintStatusID=0, LastChangeDateUTC=GETUTCDATE(), LastChangedByUser={1} WHERE ID={0}; "
                    + "DELETE FROM tblOrderApproval WHERE OrderID={0};"
                    , id
                    , DBHelper.QuoteStr(UserSupport.UserName));
            }
            return Content("success");
        }

        private System.Data.DataTable AuditedData()
        {
            using (SSDB ssdb = new SSDB())
            {
                return ssdb.GetPopulatedDataTable(
                    "SELECT ID, OrderNum=CAST(OrderNum AS VARCHAR(100)), OrderDate=CAST(OrderDate AS DATE), Status, Rejected, JobNumber, ContractNumber, DispatchConfirmNum, Customer, Carrier, OriginDriver, "
                    + "Origin, OriginGrossUnits, OriginNetUnits, Destination "
                    + "FROM viewOrderUnaudit ORDER BY OrderDate DESC");
            }
        }

    }
}
