﻿using DispatchCrude.Models;
using System.Linq;
using System.Web.Mvc;
using System;
using System.IO;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Net;
using System.Web;
using DispatchCrude.Core;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;

namespace DispatchCrude.Controllers
{
    public class CarriersController : Controller
    {
        private DispatchCrudeDB db = new DispatchCrudeDB();

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        //Get: Carriers        
        public ActionResult Index()
        {
            return View();
        }

        /**************************************************************************************************/
        /**  PAGE: ~/Carriers/getTrucks/?carrierid={carrierid}                                           **/
        /**  DESCRIPTION: JSON page that returns valid trucks for a given carrier                        **/
        /**************************************************************************************************/
        [Authorize(Roles = "viewTrucks")]
        public ActionResult getTrucks(int carrierid)
        {
            var result = (from t in db.Trucks
                          where t.CarrierID == carrierid
                          orderby t.IDNumber
                          select new
                          {
                              id = t.ID,
                              name = t.IDNumber
                          }).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);

        }

        /**************************************************************************************************/
        /**  PAGE: ~/Carriers/getTrailers/?carrierid={carrierid}                                         **/
        /**  DESCRIPTION: JSON page that returns valid trailers for a given carrier                      **/
        /**************************************************************************************************/
        [Authorize(Roles = "viewTrailers")]
        public ActionResult getTrailers(int carrierid)
        {
            var result = (from t in db.Trailers
                          where t.CarrierID == carrierid
                          orderby t.IDNumber
                          select new
                          {
                              id = t.ID,
                              name = t.IDNumber
                          }).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);

        }

        //*****************************
        // CARRIER TYPES
        //*****************************        
        //Get: Carrier Types        
        public ActionResult Types()
        {
            return View();
        }

        public ActionResult Read_CarrierType([DataSourceRequest] DataSourceRequest request = null, int id = 0)
        {
            return Read_CarrierType(request, null, id);
        }

        private ContentResult Read_CarrierType([DataSourceRequest] DataSourceRequest request, ModelStateDictionary modelState, int id = 0)
        {
            // .ToList() is required due to filtering on "Active" field, which is NOT MAPPED
            var data = db.CarrierTypes.Where(c => c.ID == id || id == 0).ToList();
            var result = data.ToDataSourceResult(request, modelState);
            return App_Code.JsonStringResult.Create(result);
        }

        // POST: Create carrier types
        [HttpPost]
        [Authorize(Roles = "createCarrierTypes")]
        public ActionResult Create_CarrierType([DataSourceRequest] DataSourceRequest request, CarrierType carrierType)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // ensure all new carrier types are marked "Active" (otherwise they will be created deleted)
                    carrierType.Active = true;                    

                    // Save new record to the database
                    db.CarrierTypes.Add(carrierType);
                    db.SaveChanges(User.Identity.Name);  // Create dates & usernames will all be taken care of by this line.
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(null, ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { carrierType }.ToDataSourceResult(request, ModelState));
                }
            }
            
            return Read_CarrierType(request, ModelState, carrierType.ID);
        }

        // POST: Update carrier types
        [HttpPost]
        [Authorize(Roles = "editCarrierTypes")]
        public ActionResult Update_CarrierType([DataSourceRequest] DataSourceRequest request, CarrierType carrierType)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // Get the existing record                    
                    CarrierType existing = db.CarrierTypes.Find(carrierType.ID);

                    // Specify all fields that will/can be updated by user input on the popup
                    db.CopyEntityValues(existing, carrierType);
                    db.SaveChanges(User.Identity.Name);  // Create & Modified dates & usernames will be taken care of by this line.
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(null, ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { carrierType }.ToDataSourceResult(request, ModelState));
                }
            }

            return Read_CarrierType(request, ModelState, carrierType.ID);
        }

        // Delete (Deactivate)
        [Authorize(Roles = "deactivateCarrierTypes")]
        public ActionResult Delete_CarrierType(CarrierType carrierType)
        {
            db.CarrierTypes.Attach(carrierType);
            db.CarrierTypes.Remove(carrierType);
            db.SaveChanges(User.Identity.Name);

            return null;
        }

        //Return a list of drivers assigned to teh specified carrier
        public ActionResult getDriversJSON(int? CarrierID)
        {
            Carrier carrier = db.Carriers.Find(CarrierID);
            IQueryable<Driver> drivers;

            if (CarrierID == null || CarrierID == -1)
            {
                drivers = db.Drivers;
            }
            else
            {
                drivers = db.Drivers.Where(d => d.CarrierID == carrier.ID);
            }

            var result = drivers.OrderBy(d => d.FirstName).ThenBy(d => d.LastName)
                            .Select(x => new { id = x.ID, name = (x.FirstName + " " + x.LastName ) })
                            .ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}
