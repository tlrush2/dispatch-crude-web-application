﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using AlonsIT;
using DispatchCrude.Core;

namespace DispatchCrude.Controllers
{
    public class ReportCenterController : Controller
    {
        static public string SESSION_RC_IMPORT_DS = "SESSION_RC_IMPORT_DS";

        static public MemoryStream ExportXmlFile(int id, ref string fileName)
        {
            DataSet ds = new DataSet();
            ds.DataSetName = "ReportCenterDS";
            using (SSDB db = new SSDB())
            {
                DBHelper.AddDsTbl(ds, db.GetPopulatedDataTable("SELECT * FROM tblUserReportDefinition WHERE ID = {0}", (object)id), "UserReportDefinition");
                DBHelper.AddDsTbl(ds, db.GetPopulatedDataTable("SELECT * FROM tblUserReportColumnDefinition WHERE UserReportID = {0}", (object)id), "UserReportColumnDefinition");
                DBHelper.AddDsTbl(ds, db.GetPopulatedDataTable("SELECT * FROM tblReportColumnDefinition WHERE ID >= 100000 AND ID IN (SELECT DISTINCT ReportColumnID FROM tblUserreportColumnDefinition WHERE UserReportID = {0} AND ReportColumnID >= 100000)", (object)id), "ReportColumnDefinition");
                fileName = Path.ChangeExtension(db.QuerySingleValue("SELECT Name FROM tblUserReportDefinition WHERE ID = {0}", id).ToString(), "xml");
                // remove any invalid characters
                fileName = Regex.Replace(fileName, @"[\/?:*""><|]+", "", RegexOptions.Compiled);
            }
            MemoryStream ms = new MemoryStream();
            ds.WriteXml(ms, XmlWriteMode.WriteSchema);
            return ms;
        }

        [HttpPost]
        // POST: ReportCenter/SetImportXmlFile
        public ActionResult SaveImportXmlFile(IEnumerable<HttpPostedFileBase> xmlImportFile)
        {
            // The Name of the Upload component is "files"
            if (xmlImportFile != null)
            {
                foreach (var file in xmlImportFile)
                {
                    DataSet ds = new DataSet();
                    ds.ReadXml(file.InputStream, XmlReadMode.ReadSchema);
                    // write the resultant datatable to session
                    Session[SESSION_RC_IMPORT_DS] = ds;
                    break;
                }
            }

            // Return an empty string to signify success
            return null;
        }

        public JsonResult ImportDefinition(string name, string userNames = null)
        {
            int ret = 0;
            DataSet ds = (DataSet)Session[SESSION_RC_IMPORT_DS];

            ret = new Importer().ImportFromDS(ds, name, userNames);

            return Json(new { id = ret });
        }

        internal class Importer
        {
            private class FieldDict : Dictionary<int, int>
            {
                public FieldDict() : base() { }
                public FieldDict(FieldDict dict) : base(dict) { }
            }

            public int ImportFromDS(DataSet ds, string name, string userNames)
            {
                int ret = 0;
                using (SSDB db = new SSDB(true))
                {
                    ret = ImportMain(db, ds.Tables["UserReportDefinition"].Rows[0], name, userNames);

                    FieldDict dlRC = ImportReportColumns(db, ds.Tables["ReportColumnDefinition"]);

                    ImportUserColumns(db, ret, ds.Tables["UserReportColumnDefinition"], dlRC);

                    db.CommitTransaction();
                }
                return ret;
            }

            private int ImportMain(SSDB db, DataRow drUserReport, string name, string userNames)
            {
                int ret = 0;
                drUserReport["ID"] = 0;
                drUserReport["Name"] = name;
                drUserReport["UserNames"] = (userNames as object) ?? DBNull.Value;
                ret = DBHelper.ToInt32(SSDBSave.SaveDataRow(db, drUserReport, "tblUserReportDefinition", "ID", UserSupport.UserName));

                return ret;
            }

            private FieldDict ImportReportColumns(SSDB db, DataTable dtRC)
            {
                FieldDict ret = new FieldDict();
                foreach (DataRow drRC in dtRC.Rows)
                {
                    int key = DBHelper.ToInt32(drRC["ID"]);
                    int newID = DBHelper.ToInt32(db.QuerySingleValue("SELECT ID FROM tblReportColumnDefinition WHERE DataField LIKE {0}", DBHelper.QuoteStr(drRC["DataField"])));
                    if (newID == 0)
                    {
                        drRC["ID"] = 0;
                        //drRC.SetAdded();
                        newID = DBHelper.ToInt32(SSDBSave.SaveDataRow(db, drRC, "tblReportColumnDefinition", "ID", UserSupport.UserName));
                    }
                    ret.Add(key, newID);
                }
                return ret;
            }

            private FieldDict ImportUserColumns(SSDB db, int userReportID, DataTable dtUC, FieldDict dlRC)
            {
                FieldDict ret = new FieldDict();

                foreach (DataRow drUC in dtUC.Rows)
                {
                    int key = DBHelper.ToInt32(drUC["ID"]);
                    drUC["ID"] = 0;
                    if (dlRC.ContainsKey(DBHelper.ToInt32(drUC["ReportColumnID"])))
                        drUC["ReportColumnID"] = dlRC[DBHelper.ToInt32(drUC["ReportColumnID"])];
                    drUC["UserReportID"] = userReportID;
                    int newID = DBHelper.ToInt32(SSDBSave.SaveDataRow(db, drUC, "tblUserReportColumnDefinition", "ID", UserSupport.UserName));
                    ret.Add(key, newID);
                }

                return ret;
            }

        }
    }
}