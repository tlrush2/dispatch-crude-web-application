﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DispatchCrude.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;

namespace DispatchCrude.Controllers
{
    [Authorize(Roles = "viewCarrierCompliance")]
    public class CarrierComplianceController : _DBControllerRead
    {
        // This session variable is required for Asyncronous file uploads to work with new records.
        // The file is uploaded here temporarily until the user actually clicks the save button.
        private const string SESSION_CARCOMP_FILENAME = "CARCOMP_FILENAME"
            , SESSION_CARCOMP_FILE = "CARCOMP_FILE";

        public ActionResult Index(int? CarrierID)
        {
            //If a carrier has been specified as a starting filter, get its name and pass it to the view
            Carrier c = db.Carriers.Find(CarrierID);

            if (c != null)
            {
                ViewBag.Carrier = c.Name_FlagInactive;
                ViewBag.CarrierID = c.ID;
            
                //We don't want the URL filter to stay after we've filtered the grid so this lets us know on the view to get rid of it.
                ViewBag.ClearUrl = true;
            }
            return View();
        }

        /* normally this is the only method that is overridden (defined) in the inheriting controller classes */
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        override protected ContentResult Read([DataSourceRequest] DataSourceRequest request, ModelStateDictionary modelState, int id)
        {
            var data = db.CarrierCompliances.Where(m => m.ID == id || id == 0)
                            .Include(m => m.Carrier)
                            .Include(m => m.Carrier.CarrierType)
                            .Include(m => m.CarrierComplianceType);
            return ToJsonResult(data, request, modelState, "Document");
        }

        // POST: Create Product Groups
        [HttpPost]
        [Authorize(Roles = "createCarrierCompliance")]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, CarrierCompliance CarrierCompliance)
        {
            return Update(request, CarrierCompliance);
        }

        // POST: Update Product Groups
        [HttpPost]
        [Authorize(Roles = "editCarrierCompliance")]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, CarrierCompliance CarrierCompliance)
        {
            // push the previously provided document info into the model (if present we need to re-validate the model, done below)
            bool resetValidation = false;
            if (resetValidation = Session[SESSION_CARCOMP_FILE] != null) CarrierCompliance.Document = Session[SESSION_CARCOMP_FILE] as byte[];
            if (resetValidation = Session[SESSION_CARCOMP_FILENAME] != null) CarrierCompliance.DocumentName = Session[SESSION_CARCOMP_FILENAME].ToString();
            // reset the ModelState and re-validate (this is required if the document was required)
            if (resetValidation)
            {
                CarrierCompliance.Validated = false;
                ModelState.Clear();
                //CarrierCompliance.Validate(new System.ComponentModel.DataAnnotations.ValidationContext(CarrierCompliance, null, null));
            }

            if (ModelState.IsValid)
            {
                try
                {
                    // don't update the Document properties if not provided (will fail validation above if required and not provided)
                    string[] skipParameters = CarrierCompliance.Document != null ? null : new string[] { "Document", "DocumentName" };
                    db.AddOrUpdate(CarrierCompliance, null, skipParameters);

                    // Save new record to the database
                    db.SaveChanges(User.Identity.Name);  // Create dates & usernames will all be taken care of by this line.

                    //Clear Carrier compliance session variable after successful add.  Otherwise the next record added will automatically use this file if the user did not select one
                    Session[SESSION_CARCOMP_FILE] = null;
                    Session[SESSION_CARCOMP_FILENAME] = null;
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { CarrierCompliance }.ToDataSourceResult(request, ModelState));
                }
            }

            return Read(request, ModelState, CarrierCompliance.ID);
        }


        // Delete (Deactivate)  Product Groups
        [Authorize(Roles = "deactivateCarrierCompliance")]
        public ActionResult Delete([DataSourceRequest] DataSourceRequest request, CarrierCompliance CarrierCompliance)
        {
            return DeleteInt(request, CarrierCompliance);
        }

        private bool ValidateUpload(bool isNew, ModelStateDictionary modelState)
        {
            bool ret = Session[SESSION_CARCOMP_FILENAME] != null && Session[SESSION_CARCOMP_FILE] != null;
            //if (isNew && !ret)
                //modelState.AddModelError(null, "File must be provided");
            return ret;
        }


        [HttpPost]
        public ActionResult SaveDocument(IEnumerable<HttpPostedFileBase> document)
        {
            // The Name of the Upload component is "files"
            if (document != null)
            {
                foreach (var file in document)
                {
                    Session[SESSION_CARCOMP_FILENAME] = Path.GetFileName(file.FileName);
                    Session[SESSION_CARCOMP_FILE] = new BinaryReader(file.InputStream).ReadBytes(file.ContentLength);
                    // we only accept a single file so just break
                    break;
                }
            }
            // Return an empty string to signify success
            return Content("");
        }

        public ActionResult ClearDocument()
        {
            Session[SESSION_CARCOMP_FILE] = null;
            Session[SESSION_CARCOMP_FILENAME] = null;

            return Json(new { status="OK"}, JsonRequestBehavior.AllowGet);
        }

        // Download the Document
        public ActionResult Download(int id)
        {
            CarrierCompliance compliance = db.CarrierCompliances.Find(id);
            return File(compliance.Document, "application/octet-stream", compliance.DocumentName);
        }

    }
}
