﻿using System;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using DispatchCrude.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;

namespace DispatchCrude.Controllers
{
    [Authorize(Roles = "viewCarrierComplianceTypes")]
    public class CarrierComplianceTypesController : _DBController
    {
        // GET: Product Groups
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest request = null, int id = 0)
        {
            return Read(request, null, id);
        }

        private ContentResult Read([DataSourceRequest] DataSourceRequest request, ModelStateDictionary modelState, int id = 0)
        {
            // .ToList() is required due to filtering on "Active" field, which is NOT MAPPED
            var data = db.CarrierComplianceTypes.Where(m => m.ID == id || id == 0).ToList();

            var result = data.ToDataSourceResult(request, modelState);

            return App_Code.JsonStringResult.Create(result);
        }

        // POST: Create Product Groups
        //[HttpPost]
        [Authorize(Roles = "createCarrierComplianceTypes")]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, CarrierComplianceType CarrierComplianceType)
        {            
            if (ModelState.IsValid)
            {
                try
                {
                    // ensure all new records are marked "Active" (otherwise they will be created deleted)
                    CarrierComplianceType.Active = true;

                    // Save new record to the database
                    db.CarrierComplianceTypes.Add(CarrierComplianceType);
                    db.SaveChanges(User.Identity.Name);  // Create dates & usernames will all be taken care of by this line.
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(null, ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { CarrierComplianceType }.ToDataSourceResult(request, ModelState));
                }
            }

            return Read(request, ModelState, CarrierComplianceType.ID);
        }

        // POST: Update Product Groups
        [HttpPost]
        [Authorize(Roles = "editCarrierComplianceTypes")]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, CarrierComplianceType CarrierComplianceType)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // Get the existing record
                    CarrierComplianceType existing = db.CarrierComplianceTypes.Find(CarrierComplianceType.ID);

                    // Specify all fields that will/can be updated by user input on the popup
                    db.CopyEntityValues(existing, CarrierComplianceType);

                    db.SaveChanges(User.Identity.Name);  // Create & Modified dates & usernames will be taken care of by this line.
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(null, ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { CarrierComplianceType }.ToDataSourceResult(request, ModelState));
                }
            }

            return Read(request, ModelState, CarrierComplianceType.ID);
        }


        // Delete (Deactivate)  Product Groups
        [Authorize(Roles = "deactivateCarrierComplianceTypes")]
        public ActionResult Delete(CarrierComplianceType CarrierComplianceType)
        {
            db.CarrierComplianceTypes.Attach(CarrierComplianceType);
            db.CarrierComplianceTypes.Remove(CarrierComplianceType);
            db.SaveChanges(User.Identity.Name);

            return null;
        }
        
        //Returns expiration length (if applicable) and whether or not a document is required (created for/used by Carrier compliance documents page popup)
        public JsonResult GetDocTypeInformation(int TypeID)
        {
            CarrierComplianceType type = db.CarrierComplianceTypes.Find(TypeID);

            return Json(new
            {
                ExpirationLength = type.ExpirationLength,
                DocumentRequired = type.RequiresDocument
            }, JsonRequestBehavior.AllowGet);
        }
    }
}
