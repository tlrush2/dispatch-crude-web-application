﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using AlonsIT;

namespace DispatchCrude.Controllers
{
    // NOTE: this controller is only here to exercie/test RestControllerHelper, but not for production use (to risky to sql injection)
    [Authorize(Roles="Administrator")]
    public class RestController : Controller
    {
        [Authorize(Roles="Administrator")]
        public ContentResult Get(string objectName, string fieldsCSV = "ID,Name"
            , string where = null, string orderBy = null
            , bool includeDeleted = false, string deletedSuffix = "Deleted: ", string deletedNameField = "Name"
            , string allID = null, string[] allTexts = null)
        {
            if (orderBy == null && fieldsCSV.Contains("Name")) orderBy = "Name"; // default sort
            string json = App_Code.RestControllerHelper.GetJson(objectName, fieldsCSV, where, orderBy, includeDeleted, deletedSuffix, deletedNameField, allID, allTexts);
            return Content(json, "application/json");
        }

        [Authorize(Roles="Administrator")]
        public ContentResult GetDDL(string objectName, string valueField = "ID", string textField = "Name"
            , string allID = null, string[] allTexts = null)
        {
            string json = App_Code.RestControllerHelper.GetDDLJson(objectName, valueField, textField, allID: allID, allTexts: allTexts);
            return Content(json, "application/json");
        }
    }
}
