﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Data;
using System.Data.SqlClient;
using System.Web.Hosting;
using AlonsIT;
using System.IO;
using DispatchCrude.Core;
using DispatchCrude.Models;
using DispatchCrude.ViewModels;

namespace DispatchCrude.Controllers
{
    [Authorize(Roles = "viewAllocationDashboard")]
    public class DashboardController : Controller
    {
        private DispatchCrudeDB db = new DispatchCrudeDB();


        public ActionResult Index(FilterViewModel<AllocationDestinationShipperViewModel> fvm)
        {
            ViewBag.Title = "Allocation";

            //if (User.IsInRole("viewAllocationDashboard"))
            //    //|| User.IsInRole("viewDriverDashboard")     //This permission will need to be created when this page actually exists
            //    //|| User.IsInRole("viewDispatchDashboard"))  //This permission will need to be created when this page actually exists
            //{
            //    /* FilterViewModel allows for keeping filter options as you navigate the dashboard */
            //    return View(fvm);
            //}

            //return View("NoDashboard");

            return View(fvm);
        }

        [Authorize(Roles = "viewDriverDashboard")]
        public ActionResult DriverDashboard()
        {
            //var orders = db.Orders.Where(o => o.LastChangedByUser == User.Identity.Name || o.CreatedByUser == User.Identity.Name).OrderByDescending(o => o.LastChangeDateUTC).Take(5);
            return PartialView("Driver");
        }

        [Authorize(Roles = "viewAllocationDashboard")]
        public ActionResult Allocation(FilterViewModel<AllocationDestinationShipperViewModel> fvm)
        {
            // Define the date filters available on this dashboard
            List<DateFilter> dateFilters = new List<DateFilter> {
                DateFilter.CURRENT_MONTH,
                DateFilter.PREVIOUS_MONTH
            };

            // Filter dashboard based on my shipper/region
            int myShipperID = Core.Converter.ToInt32(Profile.GetPropertyValue("CustomerID")); ;
            int myRegionID = Core.Converter.ToInt32(Profile.GetPropertyValue("RegionID"));
            int myTerminalID = Core.Converter.ToInt32(Profile.GetPropertyValue("TerminalID"));

            int? ShipperID = (myShipperID != -1) ? myShipperID : fvm.ShipperID; // hardcode shipper if profile not all
            int? RegionID = (myRegionID != 0) ? myRegionID : fvm.RegionID; // hardcode region if profile not all
            int? TerminalID = (myTerminalID != 0) ? myTerminalID : fvm.TerminalID; // hardcode terminal if profile not all

            ViewBag.ShipperID = new SelectList(db.Customers.Where(x => myShipperID == -1 || x.ID == myShipperID).OrderBy(x => x.Name), "ID", "Name", fvm.ShipperID);
            ViewBag.RegionID = RegionsController.getRegionsSL(ShipperID, RegionID, fvm.RegionID);
            ViewBag.TerminalID = new SelectList(db.Terminals.Where(t => (myTerminalID <= 0 || t.ID == myTerminalID) && t.DeleteDateUTC == null), "ID", "Name", fvm.TerminalID);
            ViewBag.DestinationID = SupportController.getDestinationsSL(null, ShipperID, RegionID, TerminalID, null, fvm.DestinationID);
            ViewBag.ProductGroupID = new SelectList(db.ProductGroups.OrderBy(x => x.Name), "ID", "Name", fvm.ProductGroupID);
            ViewBag.UomID = new SelectList(db.Uoms.Where(x => x.UomTypeID == (int)UomType.Types.VOLUME).OrderBy(x => x.Name), "ID", "Name", fvm.UomID);
            ViewBag.DateFilterName = new SelectList(dateFilters, "Name", "Name", fvm.DateFilterName);


            // Get the date range
            fvm.DateFilter = dateFilters.Where(df => df.Name == fvm.DateFilterName).FirstOrDefault();
            if (fvm.DateFilter == null)
                fvm.DateFilter = DateFilter.CURRENT_MONTH; // default for page load


            // Use the stored procedure to get the allocations and data for the page
            List<AllocationDestinationShipperViewModel> Allocations = new List<AllocationDestinationShipperViewModel>();
            using (SSDB odb = new SSDB())
            {
                using (System.Data.SqlClient.SqlCommand cmd = odb.BuildCommand("spAllocationDestinationShipper"))
                {
                    cmd.Parameters["@RegionID"].Value = RegionID;
                    cmd.Parameters["@TerminalID"].Value = TerminalID;
                    cmd.Parameters["@DestinationID"].Value = fvm.DestinationID;
                    cmd.Parameters["@ShipperID"].Value = ShipperID;
                    cmd.Parameters["@ProductGroupID"].Value = fvm.ProductGroupID;
                    cmd.Parameters["@StartDate"].Value = fvm.DateFilter.StartDate;
                    cmd.Parameters["@EndDate"].Value = fvm.DateFilter.EndDate;
                    cmd.Parameters["@UomID"].Value = fvm.UomID ?? 1; // Default BBLS
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        AllocationDestinationShipperViewModel allocation = new AllocationDestinationShipperViewModel();
                        allocation.TicketCount = dr.GetInt32(dr.GetOrdinal("TicketCount"));
                        allocation.TotalGross = dr.GetDecimal(dr.GetOrdinal("TotalGross"));
                        allocation.DestinationID = dr.GetInt32(dr.GetOrdinal("DestinationID"));
                        allocation.Destination = dr.GetString(dr.GetOrdinal("Destination"));
                        allocation.ShipperID = dr.GetInt32(dr.GetOrdinal("CustomerID"));
                        allocation.Shipper = dr.GetString(dr.GetOrdinal("Customer"));
                        allocation.ProductGroupID = dr.GetInt32(dr.GetOrdinal("ProductGroupID"));
                        allocation.ProductGroup = dr.GetString(dr.GetOrdinal("ProductGroup"));
                        allocation.DailyUnits = dr.IsDBNull(dr.GetOrdinal("DailyUnits")) ? (decimal?)null : dr.GetDecimal(dr.GetOrdinal("DailyUnits"));
                        allocation.AllocationStartDate = dr.GetDateTime(dr.GetOrdinal("AllocationStartDate"));
                        allocation.AllocationEndDate = dr.GetDateTime(dr.GetOrdinal("AllocationEndDate"));
                        allocation.AllocatedUnits = dr.IsDBNull(dr.GetOrdinal("AllocatedUnits")) ? (decimal?)null : dr.GetDecimal(dr.GetOrdinal("AllocatedUnits"));
                        allocation.DaysLeft = dr.GetInt32(dr.GetOrdinal("DaysLeft"));

                        Allocations.Add(allocation);
                    }
                }
            }
            fvm.results = Allocations.OrderBy(a => a.Shipper).ThenBy(a => a.Destination);
            return PartialView("_Allocation", fvm);
        }

        //[Authorize(Roles = "viewAllocationDashboard")]
        //public ActionResult AllocationDashboard()
        //{
        //    //var orders = db.Orders.Where(o => o.LastChangedByUser == User.Identity.Name || o.CreatedByUser == User.Identity.Name).OrderByDescending(o => o.LastChangeDateUTC).Take(5);
        //    return PartialView("Allocation");
        //}

        [Authorize(Roles = "viewDispatchDashboard")]
        public ActionResult DispatchDashboard()
        {
            return PartialView("Dispatch");
        }

        [Authorize(Roles = "viewDispatchDashboard")]
        public ActionResult MyRecentOrders()
        {
            var orders = db.Orders.Where(o => o.LastChangedByUser == User.Identity.Name || o.CreatedByUser == User.Identity.Name).OrderByDescending(o => o.LastChangeDateUTC).Take(5);
            return PartialView("_RecentOrders", orders.ToList());
        }

        [Authorize(Roles = "viewDispatchDashboard")]
        public ActionResult ExportView(string dashboard="DispatchDashboard")
        {
            ViewBag.Dashboard = dashboard;
            return View("Export");
        }
/*
        public ActionResult Export(string dashboard="DispatchDashboard")
        {
            //String content = 
            HiQPdf.HtmlToPdf converter = new HiQPdf.HtmlToPdf();
            //{
            converter.BrowserWidth = 900;
            converter.SerialNumber = "ezMSKisf-HTcSGQka-CQJNVUtb-SltJW0NO-SFtISlVK-SVVCQkJC";
            string content = ControllerExtensions.RenderPartialViewToString(this, dashboard, new DCDashboard());
            //content = ControllerExtensions.RenderViewToString(this, dashboard, new DCDashboard());
            byte[] pdfBuffer = null;
            pdfBuffer = converter.ConvertHtmlToMemory(content, null);
            //}
            //ControllerExtensions.RenderViewToString(this, "BOL_All", order);
            GC.Collect();
            //return File(fileBytes, "Application/pdf", "Dashboard");
            //byte[] jpegBytes = GetJpegFileBytes(id);
            //GC.Collect();
            return File(pdfBuffer, "Application/pdf", "test.pdf");
        }
*/
    }

}
