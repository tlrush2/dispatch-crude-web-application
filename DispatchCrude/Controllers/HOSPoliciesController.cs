﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DispatchCrude.Models;
using DispatchCrude.Core;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using AlonsIT;

namespace DispatchCrude.Controllers
{
    [Authorize(Roles = "viewHOSPolicy")]
    public class HOSPoliciesController : _DBControllerRead
    {        
        public ActionResult Index()
        {
            return View();
        }

        override protected ContentResult Read([DataSourceRequest] DataSourceRequest request, ModelStateDictionary modelState, int id)
        {
            // .ToList() is required due to filtering on "Active" field, which is NOT MAPPED
            var data = db.HosPolicies.Where(r => r.ID == id || id == 0)
                            .ToList();

            var result = data.ToDataSourceResult(request, modelState);
            
            return App_Code.JsonStringResult.Create(result);
        }

        [Authorize(Roles = "createHOSPolicy")]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, HosPolicy policy)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // ensure new records are marked "Active" (otherwise they will be created deleted)
                    policy.Active = true;

                    // Save new record to the database
                    db.HosPolicies.Add(policy);
                    db.SaveChanges(User.Identity.Name);  // Create dates & usernames will all be taken care of by this line.
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(null, ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { policy }.ToDataSourceResult(request, ModelState));
                }
            }


            return Read(request, ModelState, policy.ID);
        }

        [HttpPost]
        [Authorize(Roles = "editHOSPolicy")]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, HosPolicy policy)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // Get the existing record
                    HosPolicy existing = db.HosPolicies.Find(policy.ID);

                    // Specify all fields that will/can be updated by user input on the popup
                    db.CopyEntityValues(existing, policy);

                    db.SaveChanges(User.Identity.Name);  // Create & Modified dates & usernames will be taken care of by this line.
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(null, ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { policy }.ToDataSourceResult(request, ModelState));
                }
            }

            return Read(request, ModelState, policy.ID);
        }

        [Authorize(Roles = "deactivateHOSPolicy")]
        public ActionResult Delete(HosPolicy policy)
        {
            db.HosPolicies.Attach(policy);
            db.HosPolicies.Remove(policy);
            db.SaveChanges(User.Identity.Name);

            return null;
        }
    }
}
