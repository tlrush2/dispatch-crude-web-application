﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DispatchCrude.DataExchange;
using DispatchCrude.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System.Collections;
using AlonsIT;

namespace DispatchCrude.Controllers
{
    [Authorize(Roles = "viewDriverSettlementStatementEmail")]
    public class DriverSettlementStatementEmailController : _DBControllerRead
    {
        public ActionResult Index(int? batchID = null)
        {
            // if a batchID querystring value was supplied then lookup the associated BatchNum and pass it to be used as the default filter criteria
            if (batchID.HasValue)
                ViewData["BatchNum"] = (db.DriverSettlementBatches.Find(batchID.Value) ?? new DriverSettlementBatch()).BatchNum;
            return View();
        }

        /* normally this is the only method that is overridden (defined) in the inheriting controller classes */
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        override protected ContentResult Read([DataSourceRequest] DataSourceRequest request, ModelStateDictionary modelState, int id)
        {
            // use .ToList() to force the query to be generated/executed before referencing [NotMapped] properties, etc
            var data = db.DriverSettlementStatementEmails.Where(m => m.ID == id || id == 0)
                            .Include(m => m.Batch)
                            .Include(m => m.PdfExport)
                            .Include(m => m.DriverSettlementStatementEmailDefinition)
                            .Include(m => m.Driver)
                            .Include(m => m.Driver.Carrier).ToList();
            // use a projection to limit the data returned to the caller (but use ToList() so we can pass anonymous objects to the ToJsonResult() method)
            var result = data.Select(m => new {
                m.ID,
                m.DriverSettlementStatementEmailDefinitionID, m.PdfExportID, m.BatchID, m.DriverID, m.EmailAddress, m.CCEmailAddress, m.QueueEmailSend,
                m.CreateDate, m.CreatedByUser, m.EmailDate, m.EmailedByUser, m.allowRowEdit, m.allowRowDeactivate,
                DriverSettlementStatementEmailDefinition = new { m.DriverSettlementStatementEmailDefinition.ID, m.DriverSettlementStatementEmailDefinition.Name, },
                PdfExport = new { m.PdfExport.ID, m.PdfExport.Name, },
                Batch = new { m.Batch.ID, m.Batch.BatchNum, m.Batch.InvoiceNum, m.Batch.PeriodEndDate, m.Batch.BatchDate, },
                Driver = new { m.Driver.ID, m.Driver.FullName, m.Driver.IDNumber, 
                    Carrier = new { m.Driver.Carrier.ID, m.Driver.Carrier.Name, },
                }
            }).ToList();
            return ToJsonResult(result, request, modelState);
        }

        [HttpPost]
        [Authorize(Roles = "editDriverSettlementStatementEmail")]
        // this method takes the base model to prevent any "child" objects from being validated incorrectly or saved unexpectedly
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, DriverSettlementStatementEmailBase model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // since the incoming model is not the model the DB instance can use, clone it to one that is compatible
                    model = IEDTO.CloneToMe(model, typeof(DriverSettlementStatementEmail));
                    if (model.ID == 0) throw new Exception("Driver Settlement Statement Emails can only be generated via the Driver Batch page");
                    // the person "sending" is the person who queued
                    if (model.QueueEmailSend)
                        model.EmailedByUser = User.Identity.Name;
                    db.OnUpdateExisting += Db_OnUpdateExisting;
                    // never let the use update the skipped properties (the UI doesn't support these fields, but if this method was called manually with these changes we still ignore them)
                    db.AddOrUpdateSave(User.Identity.Name, model, "DriverSettlementStatementEmailDefinitionID", "DriverID", "BatchID");
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { model }.ToDataSourceResult(request, ModelState));
                }
            }

            return Read(request, ModelState, model.ID);
        }

        private bool Db_OnUpdateExisting(IEDTO existing, IEDTO changes)
        {
            // if the user is setting QueueEmailSend = true, then update the EmailedByUser value to the current user (who "queued" the email is the "sender")
            if (!(existing as DriverSettlementStatementEmailBase).QueueEmailSend && (changes as DriverSettlementStatementEmailBase).QueueEmailSend)
                (changes as DriverSettlementStatementEmailBase).EmailedByUser = User.Identity.Name;
            return false;
        }

        [HttpPost]
        public ContentResult UpdateQueueEmailSend(bool queueEmailSend, int[] idList)
        {
            string sql = string.Format(
                @"UPDATE tblDriverSettlementStatementEmail SET QueueEmailSend={0}, EmailedByUser = {1} WHERE ID IN ({2});
                SELECT * FROM tblDriverSettlementStatementEmail WHERE ID IN ({2});"
                , queueEmailSend ? "1" : "0"
                , DBHelper.QuoteStr(User.Identity.Name)
                , string.Join(",", idList));
            var result = db.DriverSettlementStatementEmails.SqlQuery(sql)
                .Select(m => new { m.ID, m.QueueEmailSend, m.EmailedByUser })
                .ToList();
            return ToJsonResult(result);
        }
        public FileResult DownloadEmailBody(int id)
        {
            var data = db.DriverSettlementStatementEmails.Find(id);
            DriverSettlementStatementHtmlDocs htmlDocuments = new DriverSettlementStatementHtmlDocs(data.PdfExportID, data.BatchID, data.DriverID);
            foreach (DCHtmlDocument htmlDoc in htmlDocuments)
            {
                // should always be 1 record so just return it
                return File(htmlDoc.Bytes, "Application/html", htmlDoc.FileName);
            }
            // probably should redirect to this page if we get here, otherwise the user goes to a blank page
            return null;
        }
        public FileResult DownloadAttachment(int id)
        {
            var data = db.DriverSettlementStatementEmails.Find(id);
            DriverSettlementStatementPdfDocs pdfDocuments = new DriverSettlementStatementPdfDocs(data.PdfExportID, data.BatchID, data.DriverID);
            foreach (DCPdfDocument pdfDoc in pdfDocuments)
            {
                // should always be 1 record so just return it
                return File(pdfDoc.Bytes, "Application/pdf", pdfDoc.FileName);
            }
            // probably should redirect to this page if we get here, otherwise the user goes to a blank page
            return null;
        }
    }
}
