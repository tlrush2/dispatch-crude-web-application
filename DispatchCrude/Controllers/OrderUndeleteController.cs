﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Data;
using AlonsIT;

namespace DispatchCrude.Controllers
{
    [Authorize(Roles = "undeleteOrders")]
    public class OrderUndeleteController : Controller
    {
        //
        // GET: /OrderUndelete/

        [Authorize(Roles = "undeleteOrders")]
        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.Title = "Order Undelete";
            return View(DeletedData());
        }

        [Authorize(Roles = "undeleteOrders")]
        [HttpPost]
        public ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            DataTable deletedData = DeletedData();

            if (request.Aggregates.Any())
            {
                request.Aggregates.Each(agg =>
                {
                    agg.Aggregates.Each(a =>
                    {
                        a.MemberType = deletedData.Columns[agg.Member].DataType;
                    });
                });
            }

            return Json(deletedData.ToDataSourceResult(request));
        }

        [Authorize(Roles = "undeleteOrders")]
        [HttpPost]
        public ContentResult Undelete(int id)
        {
            // mark the record as Audited (Completed)
            using (SSDB db = new SSDB())
            {
                db.ExecuteSql("UPDATE tblOrder SET DeleteDateUTC=NULL, DeletedByUser=NULL WHERE ID={0}", id);
            }
            return Content("success");
        }

        private System.Data.DataTable DeletedData()
        {
            using (SSDB ssdb = new SSDB())
            {
                System.Data.DataTable data = ssdb.GetPopulatedDataTable(
                    "SELECT ID, OrderNum=CAST(OrderNum AS VARCHAR(100)), Status = OrderStatus, DeleteDate=CAST(DeleteDateUTC AS DATE), DeletedByUser, Rejected, DispatchConfirmNum, Customer, Carrier, OriginDriver"
                    + ", Origin, OriginGrossUnits, OriginNetUnits, Destination, JobNumber, ContractNumber "
                    + "FROM viewOrderLocalDates "
                    + "WHERE DeleteDateUTC IS NOT NULL "
                    + "ORDER BY DeleteDateUTC DESC");
                return Core.DateHelper.AddLocalRowStateDateFields(data, System.Web.HttpContext.Current);
            }
        }

    }
}
