﻿using System;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using DispatchCrude.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using DispatchCrude.Extensions;
using System.Data.Entity;
using Newtonsoft.Json;
using DispatchCrude.App_Code;
using System.Collections.Generic;


namespace DispatchCrude.Controllers
{
    [Authorize(Roles = "viewImportCenterCustomDataFields")]
    public class ObjectCustomDataController : _DBControllerRead
    {

        public ActionResult Index(int? filterObjectID = null, int? filterObjectFieldID = null, string filterRecordSearch = null)
        {
            //Get data for left-hand filters            
            ViewBag.filterObjectID = new SelectList(db.Objects.OrderBy(m => m.Name), "ID", "Name", filterObjectID);
            ViewBag.filterObjectFieldID = new SelectList(db.ObjectFields.Where(x => x.IsCustom == true && (filterObjectID == null || x.ObjectDefID == filterObjectID)).OrderBy(m => m.Name), "ID", "Name", filterObjectFieldID);
            ViewBag.filterRecordSearch = filterRecordSearch;

            return View();
        }

        public ActionResult ReadWithFilters([DataSourceRequest] DataSourceRequest request, int id = 0,
                            int? ObjectID = null, int? ObjectFieldID = null, String RecordSearch = null)
        {
            return ReadWithFilters(request, null, id, ObjectID, ObjectFieldID, RecordSearch);
        }

        private ContentResult ReadWithFilters([DataSourceRequest] DataSourceRequest request, ModelStateDictionary modelState, int id = 0,
                            int? ObjectID = null, int? ObjectFieldID = null, String RecordSearch = null)
        {
            ObjectDef o = db.Objects.Find(ObjectID);
            if (o == null)
                return Content(JsonConvert.SerializeObject(Json(new { }, JsonRequestBehavior.AllowGet)));

            // use record search to get matching IDs
            IEnumerable<int> matches = new List<int>();
            if (String.IsNullOrEmpty(RecordSearch) == false)
                matches = db.Database.SqlQuery<int>("SELECT ID FROM " + (o.SqlTargetName ?? o.SqlSourceName) + " WHERE CAST(" + o.KeyField + " AS VARCHAR(MAX)) LIKE '%" + RecordSearch + "%'").AsQueryable().ToList();

            var data = db.ObjectCustomData.Where(m => (m.ID == id || id == 0) 
                                    && m.ObjectField.ObjectDefID == ObjectID
                                    && (ObjectFieldID == null || m.ObjectFieldID == ObjectFieldID)
                                    && (String.IsNullOrEmpty(RecordSearch) || matches.Contains(m.RecordID)))
                            .Include(m => m.ObjectField)
                            .Include(m => m.ObjectField.ObjectDef)
                            .Take(500);

            var result = data.ToDataSourceResult(request, modelState);            

            return JsonStringResult.Create(data.ToDataSourceResult(request, modelState));
        }
        
        // POST: Create
        [HttpPost]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, [Bind(Exclude="ObjectField")] ObjectCustomData model) //Bind(Exclude) is here to prevent unwanted validation of these objects
        {
            return Update(request, model);
        }

        // POST: Update
        [HttpPost]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, [Bind(Exclude="ObjectField")] ObjectCustomData model) // Bind(Exclude) is here to prevent unwanted validation of these objects
        {
            if (ModelState.IsValid)
            {
                try
                {
                    db.AddOrUpdateSave(User.Identity.Name, model);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(null, ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { model }.ToDataSourceResult(request, ModelState));
                }
            }

            ObjectField of = db.ObjectFields.Find(model.ObjectFieldID);
            return ReadWithFilters(request, ModelState, model.ID, of.ObjectDefID, model.ObjectFieldID, null);
        }

        // Delete (Deactivate)
        [HttpPost]
        public ActionResult Delete([DataSourceRequest] DataSourceRequest request, [Bind(Exclude="ObjectField")] ObjectCustomData model) // Bind(Exclude) is here to prevent unwanted validation of these objects
        {
            DeleteInt(request, model);

            return null;
        }
    }
}
