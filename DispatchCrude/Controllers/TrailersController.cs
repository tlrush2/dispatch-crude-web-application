﻿using DispatchCrude.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DispatchCrude.Core;

namespace DispatchCrude.Controllers
{
    public class TrailersController : Controller
    {
        static public string SESSION_NAME_TRAILERIDLIST = "TrailerIDList";

        private DispatchCrudeDB db = new DispatchCrudeDB();

        /**************************************************************************************************/
        /**  PAGE: ~/Trailers/QRList/                                                                    **/
        /**  PARAMETERS: id - trailer ID for generating a single QR (optional)                           **/
        /**              SESSION["TrailerIDList"] - session variable of trailers from grid (optional)    **/
        /**  DESCRIPTION: Get a list of QR codes for trailers (filterd by user's carrier)                **/
        /**************************************************************************************************/
        [Authorize(Roles = "viewTrailers")]
        public ActionResult QRList(int? id)
        {
            // Filter trailers that use my carrier and terminal
            int myCarrierID = Converter.ToInt32(Profile.GetPropertyValue("CarrierID"));
            int myTerminalID = Converter.ToInt32(Profile.GetPropertyValue("TerminalID"));
            var trailers = db.Trailers.Where(t => (myCarrierID == -1 || t.CarrierID == myCarrierID)
                                            && (myTerminalID <= 0 || t.TerminalID == myTerminalID || t.TerminalID == null));

            if (id != null)
            {
                // filter to single record
                trailers = trailers.Where(t => t.ID == id);
            }
            else if (this.ControllerContext.HttpContext.Session[SESSION_NAME_TRAILERIDLIST] != null)
            {
                // filter to trailers from previous grid (stored in session variable)
                List<int> trailerIDs = (List<int>)this.ControllerContext.HttpContext.Session[SESSION_NAME_TRAILERIDLIST];
                trailers = trailers.Where(t => trailerIDs.Contains(t.ID));
            }
            return View(trailers);
        }
    }
}