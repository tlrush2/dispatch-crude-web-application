using System.Linq;
using System.Web.Mvc;
using System.Data.Entity;
using DispatchCrude.Models;
using PagedList;
using System;
using AlonsIT;
using DispatchCrude.Common;
using DispatchCrude.App_Code;
using DispatchCrude.Core;

namespace DispatchCrude.Controllers
{
    [HandleError]
    [Authorize(Roles = "viewOrders, Dispatch3P, editOrders")]
    public class TicketsController : Controller
    {
        private DispatchCrudeDB db = new DispatchCrudeDB();

        /**************************************************************************************************/
        /**  PAGE: ~/Tickets/                                                                            **/
        /**        ~/Tickets/Index/                                                                      **/
        /**  DESCRIPTION: Dispalys a list of all order tickets based on permissions.  Tickets can be     **/
        /**      ordered by clicking most column headers, and the list is searchable on the ticket #,    **/
        /**      order #, shipper PO, BOL #, or ticket type.                                             **/
        /**************************************************************************************************/
        [Authorize(Roles = "viewOrders, editOrders")]
        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.Title = "Tickets";
            ViewBag.CurrentSort = sortOrder;

            ViewBag.CarrierTicketNumSort = String.IsNullOrEmpty(sortOrder) ? "carrierticketnum_desc" : "";
            ViewBag.TicketTypeSort = sortOrder == "tickettype" ? "tickettype_desc" : "tickettype";
            ViewBag.OrderNumSort = sortOrder == "ordernum" ? "ordernum_desc" : "ordernum";
            ViewBag.DispatchConfirmNumSort = sortOrder == "dispatchconfirmnum" ? "dispatchconfirmnum_desc" : "dispatchconfirmnum";
            ViewBag.BOLNumSort = sortOrder == "bolnum" ? "bolnum_desc" : "bolnum";

            if (searchString != null)
                page = 1;
            else
                searchString = currentFilter;
            ViewBag.CurrentFilter = searchString;

            // Filter orders assigned to me or that use my carrier
            int myDriverID = Converter.ToInt32(Profile.GetPropertyValue("DriverID"));
            int myCarrierID = Converter.ToInt32(Profile.GetPropertyValue("CarrierID"));
            var tickets = db.OrderTickets.Where(t => myCarrierID == -1 || t.Order.CarrierID == myCarrierID || t.Order.DriverID == myDriverID);

            // filter orders by terminal
            int myTerminalID = Converter.ToInt32(Profile.GetPropertyValue("TerminalID"));
            tickets = tickets.Where(o => myTerminalID <= 0 // no terminal filtering
                                     ||     (o.Order.Origin == null || o.Order.Origin.TerminalID == null || o.Order.Origin.TerminalID == myTerminalID)
                                         && (o.Order.Destination == null || o.Order.Destination.TerminalID == null || o.Order.Destination.TerminalID == myTerminalID)
                                         && (o.Order.Driver == null || o.Order.Driver.TerminalID == null || o.Order.Driver.TerminalID == myTerminalID));

            if (!String.IsNullOrEmpty(searchString))
            {
                tickets = tickets.Where(t => t.Order.OrderNum.ToString().Contains(searchString) 
                                          || t.CarrierTicketNum.Contains(searchString)
                                          || t.DispatchConfirmNum.Contains(searchString)
                                          || t.BOLNum.Contains(searchString)
                                          || t.TicketType.Name.Contains(searchString));

            }

            switch (sortOrder)
            {
                case "carrierticketnum_desc":
                    tickets = tickets.OrderByDescending(t => t.CarrierTicketNum);
                    break;
                case "tickettype":
                    tickets = tickets.OrderBy(t => t.TicketType.Name);
                    break;
                case "tickettype_desc":
                    tickets = tickets.OrderByDescending(t => t.TicketType.Name);
                    break;
                case "ordernum":
                    tickets = tickets.OrderBy(t => t.Order.OrderNum);
                    break;
                case "ordernum_desc":
                    tickets = tickets.OrderByDescending(t => t.Order.OrderNum);
                    break;
                case "dispatchconfirmnum":
                    tickets = tickets.OrderBy(t => t.DispatchConfirmNum);
                    break;
                case "dispatchconfirmnum_desc":
                    tickets = tickets.OrderByDescending(t => t.DispatchConfirmNum);
                    break;
                case "bolnum":
                    tickets = tickets.OrderBy(t => t.BOLNum);
                    break;
                case "bolnum_desc":
                    tickets = tickets.OrderByDescending(t => t.BOLNum);
                    break;
                default:
                    tickets = tickets.OrderBy(s => s.CarrierTicketNum);
                    break;
            }

            int pageSize = 100;
            int pageNumber = (page ?? 1);
            return View((tickets.ToPagedList(pageNumber, pageSize)));
        }

        /**************************************************************************************************/
        /**  PAGE: [partial view]                                                                        **/
        /**  DESCRIPTION: Returns a list of tickets tied to an order as a table.  The function calls     **/
        /**         the appropriate partial view to display the correct fields.  The editable flag is    **/
        /**         used to display or hide the edit buttons                                             **/
        /**************************************************************************************************/
        [Authorize(Roles = "viewOrders, Dispatch3P, editOrders")]
        public ActionResult List(int id, bool editable = false)
        {
            string partialViewName = "";

            Order order = db.Orders.Find(id);
            var tickets = db.OrderTickets.Where(t => t.OrderID == id && t.DeleteDateUTC == null);

            ViewBag.OrderID = id;
            ViewBag.TotalGross = order.OriginGrossUnits ?? 0;
            ViewBag.TotalNet = order.OriginNetUnits ?? 0;
            ViewBag.TotalWeightNet = order.OriginWeightNetUnits ?? 0;
            ViewBag.TicketTypeID = order.TicketTypeID;
            ViewBag.UOM = order.OriginUOM.Abbrev;
            ViewBag.UOMFormat = order.OriginUOM.getFormat();
            ViewBag.Editable = editable;

            switch (order.TicketTypeID)
            {
                case (int)TicketType.TYPE.GaugeRun:
                case (int)TicketType.TYPE.Gauge_Auto_Close:
                case (int)TicketType.TYPE.GaugeNet:
                case (int)TicketType.TYPE.NetVolume:
                case (int)TicketType.TYPE.CAN_MeterRun:
                case (int)TicketType.TYPE.Production_Water_Run:
                    partialViewName = "_OrderTanks";
                    break;
                case (int)TicketType.TYPE.MeterRun:
                case (int)TicketType.TYPE.BasicRun:
                    partialViewName = "_OrderBOLs";
                    break;
                case (int)TicketType.TYPE.GrossVolume:
                case (int)TicketType.TYPE.Propane_With_Scale:
                case (int)TicketType.TYPE.CAN_BasicRun:
                    partialViewName = "_OrderTickets";
                    ViewBag.TotalNet = ""; // no Net is ever available, so just show nothing
                    break;
                case (int)TicketType.TYPE.MineralRun:
                case (int)TicketType.TYPE.Asphalt:
                    partialViewName = "_OrderNet";
                    break;
            }

            return PartialView(partialViewName, tickets);
        }


        /**************************************************************************************************/
        /**  PAGE: ~/Tickets/Details/{id}                                                                **/
        /**  DESCRIPTION: View details of a ticket                                                       **/
        /**************************************************************************************************/
        [Authorize(Roles = "viewOrders, Dispatch3P, editOrders")]
        public ActionResult Details(int? id)
        {
            OrderTicket ticket = db.OrderTickets.Find(id);
            if (ticket == null)
            {
                TempData["error"] = "Ticket not found!";
                return RedirectToAction("Index");
            }
            Order order = db.Orders.Find(ticket.OrderID);
            if (hasAccess(order) == false)
            {
                TempData["error"] = "You do not have access to this order!";
                return RedirectToAction("Index");
            }

            ViewBag.UOM = order.OriginUOM.Abbrev;
            ViewBag.UOMStep = order.OriginUOM.getStep();
            ViewBag.UOMFormat = order.OriginUOM.getFormat();
            return View(ticket);
        }

        
        /**************************************************************************************************/
        /**  PAGE: ~/Tickets/FullEdit/{id}                                                               **/
        /**  DESCRIPTION: Full edit for a ticket.  All fields are editable from this screen, though it   **/
        /**         should be noted only certain fields are relevant/required based on the ticket type.  **/
        /**************************************************************************************************/
        [Authorize(Roles = "editOrders")]
        public ActionResult FullEdit(int? id)
        {
            OrderTicket ticket = db.OrderTickets.Find(id);
            if (hasAccess(ticket.Order) == false)
            {
                TempData["error"] = "You do not have access to this order!";
                return RedirectToAction("Index");
            }
            ViewBag.UOM = ticket.Order.OriginUOM.Abbrev;
            ViewBag.UOMSigFigs = ticket.Order.OriginUOM.SignificantDigits;
            ViewBag.UOMStep = ticket.Order.OriginUOM.getStep();
            ViewBag.UOMFormat = ticket.Order.OriginUOM.getFormat();
            ViewBag.OrderNum = ticket.Order.OrderNum;
            ViewBag.OriginTankID = new SelectList(db.OriginTanks.Where(t => t.OriginID == ticket.Order.OriginID && t.DeleteDateUTC == null).OrderBy(t => t.TankNum), "ID", "TankNumDesc", ticket.OriginTankID);
            ViewBag.TicketTypeID = new SelectList(db.TicketTypes, "ID", "Name", ticket.TicketTypeID);
            ViewBag.RejectReasonID = new SelectList(db.OrderTicketRejectReasons.Where(r => r.DeleteDateUTC == null), "ID", "NumDesc", ticket.RejectReasonID);

            return View(ticket);
        }


        /**************************************************************************************************/
        /**  PAGE: ~/Tickets/Edit/0?orderid={orderid}                                                    **/
        /**        ~/Tickets/Edit/{id}                                                                   **/
        /**  DESCRIPTION: Adds/Edits a ticket for an order.  If id is not set or id = 0 Create,          **/
        /**         otherwise an edit.  An order id is required for new tickets (for templating)         **/
        /**************************************************************************************************/
        [Authorize(Roles = "editOrders")]
        public ActionResult Edit(int? id, int? orderid = 0)
        {
            ViewBag.Title = "Ticket Add/Edit";

            OrderTicket ticket = null;

            if (id == null || id == 0)
            {
                // Create new ticket
                Order order = db.Orders.Find(orderid);
                if (order == null)
                {
                    TempData["message"] = "Order not found!";
                    return RedirectToAction("Search", "Orders");
                }
                if (hasAccess(order) == false)
                {
                    TempData["error"] = "You do not have access to this order!";
                    return RedirectToAction("Index");
                }
                if (order.canEdit == false)
                {
                    TempData["message"] = "Order #" + order.OrderNum + " has been settled and cannot be changed"; ;
                    return RedirectToAction("Details", "Orders", new { id = orderid });
                }
                ticket = new Models.OrderTicket(); 
                ticket.OrderID = order.ID;
                ticket.TicketTypeID = order.TicketTypeID;
                ViewBag.TicketType = order.TicketType.Name;
                ViewBag.UOM = order.OriginUOM.Abbrev;
                ViewBag.UOMSigFigs = order.OriginUOM.SignificantDigits;
                ViewBag.UOMStep = order.OriginUOM.getStep();
                ViewBag.UOMFormat = order.OriginUOM.getFormat();
                ViewBag.OrderNum = order.OrderNum;

                int ticketCount = db.OrderTickets.Count(r => r.OrderID == orderid && r.DeleteDateUTC == null);
                // Default Ticket # to Order # + a letter (A, B, C, ...)
                ticket.CarrierTicketNum = order.OrderNum + ((char)('A' + ticketCount)).ToString();
                // Default the first OrderTicket to the values specified on the Order ("seed" values)
                if (ticketCount == 0)
                {
                    ticket.OriginTankID = order.OriginTankID;
                    ticket.TankNum = order.OriginTankNum ?? "";
                    ticket.BOLNum = order.OriginBOLNum ?? "";
                    ticket.DispatchConfirmNum = order.DispatchConfirmNum;
                }

                ViewBag.OriginTankID = new SelectList(db.OriginTanks.Where(t => t.OriginID == order.OriginID && t.DeleteDateUTC == null).OrderBy(t => t.TankNum), "ID", "TankNumDesc", ticket.OriginTankID);
            }
            else
            {
                // Edit existing ticket
                ticket = db.OrderTickets.Find(id);
                if (ticket == null)
                {
                    TempData["message"] = "Ticket not found!";
                    return RedirectToAction("Index");
                }
                if (hasAccess(ticket.Order) == false)
                {
                    TempData["error"] = "You do not have access to this order!";
                    return RedirectToAction("Index");
                }
                if (ticket.Order.canEdit == false)
                {
                    TempData["message"] = "Order #" + ticket.Order.OrderNum + " has been audited/settled and cannot be changed"; ;
                    return RedirectToAction("Details", "Orders", new { id = ticket.OrderID });
                }

                ViewBag.TicketType = ticket.TicketType.Name;
                ViewBag.OriginTankID = new SelectList(db.OriginTanks.Where(t => t.OriginID == ticket.Order.OriginID && t.DeleteDateUTC == null).OrderBy(t => t.TankNum), "ID", "TankNumDesc", ticket.OriginTankID);
                ViewBag.UOM = ticket.Order.OriginUOM.Abbrev;
                ViewBag.UOMSigFigs = ticket.Order.OriginUOM.SignificantDigits;
                ViewBag.UOMStep = ticket.Order.OriginUOM.getStep();
                ViewBag.UOMFormat = ticket.Order.OriginUOM.getFormat();
                ViewBag.OrderNum = ticket.Order.OrderNum;
            }

            ViewBag.RejectReasonID = new SelectList(db.OrderTicketRejectReasons.Where(r => r.DeleteDateUTC == null), "ID", "NumDesc", ticket.RejectReasonID);

            string viewName = ((TicketType.TYPE) ticket.TicketTypeID).ToString();

            return View(viewName, ticket);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "editOrders")]
        public ActionResult Edit([Bind(Include = "ID,OrderID,UID,TicketTypeID,CarrierTicketNum,BOLNum,TankNum,OriginTankID," + 
                    "ProductObsGravity,ProductObsTemp,ProductBSW,ProductHighTemp,ProductLowTemp,ProductObsSpecificWeight," + 
                    "OpeningGaugeFeet,OpeningGaugeInch,OpeningGaugeQ,ClosingGaugeFeet,ClosingGaugeInch,ClosingGaugeQ,BottomFeet,BottomInches,BottomQ," +
                    "MeterFactor,OpenMeterUnits,CloseMeterUnits,GrossUnits,GrossStdUnits,NetUnits,DispatchConfirmNum,SealOff,SealOn," +
                    "WeightGrossUnits,WeightTareUnits,WeightNetUnits,ScaleTicketNum," +
                    "Rejected,RejectReasonID,RejectNotes")] OrderTicket ticket)
        {
            // editing a ticket for an order after it has been picked up may cause a discrepancy between pickup and delivery, validate? disallow editing when status not in accepted?

            // Set page variables if edit fails
            Order order = db.Orders.Find(ticket.OrderID);
            TicketType ticketType = db.TicketTypes.Find(ticket.TicketTypeID);
            ViewBag.TicketType = ticketType.Name;
            ViewBag.UOM = order.OriginUOM.Abbrev;
            ViewBag.UOMSigFigs = order.OriginUOM.SignificantDigits;
            ViewBag.UOMStep = order.OriginUOM.getStep();
            ViewBag.UOMFormat = order.OriginUOM.getFormat();
            ViewBag.OrderNum = order.OrderNum;
            ViewBag.RejectReasonID = new SelectList(db.OrderTicketRejectReasons.Where(r => r.DeleteDateUTC == null), "ID", "NumDesc", ticket.RejectReasonID);
            ViewBag.OriginTankID = new SelectList(db.OriginTanks.Where(t => t.OriginID == order.OriginID && t.DeleteDateUTC == null), "ID", "TankNumDesc", ticket.OriginTankID);
            string viewName = ((TicketType.TYPE)ticket.TicketTypeID).ToString();
             
            if (ModelState.IsValid)
            {
                if (order.canEdit == false)
                {
                    TempData["message"] = "Cannot edit tickets for this order";
                    return RedirectToAction("Details", "Orders", new { id = ticket.OrderID });
                }
                if (hasAccess(ticket.Order) == false)
                {
                    TempData["error"] = "You do not have access to this order!";
                    return RedirectToAction("Index");
                }

                // Ticket number must be unique for given order (ignore deleted)
                if (db.OrderTickets.Where(t => t.OrderID == ticket.OrderID && t.CarrierTicketNum == ticket.CarrierTicketNum && t.DeleteDateUTC == null && t.ID != ticket.ID).Count() > 0)
                {
                    ViewBag.Error = "Ticket #" + ticket.CarrierTicketNum + " already exists for Order #" + order.OrderNum;
                    return View(viewName, ticket);
                }

                //volume check (see model for TODO) check gross units and net units with trailer capacity possibly at order level **wishlist
                //check if can change ticket type (would need drop down)
                //tanknum? use origintankid? 
                //rejectreasonid, grossunits, tanknum, create/lastchange/delete not updated
                //grossunits, net units may get overwritten by Ticket trigger (trigOrderTicket_IU) when origin uom changes

                // if required, check dispatch conf #
                if (ticket.DispatchConfirmNumRequired() && String.IsNullOrEmpty(ticket.DispatchConfirmNum))
                {
                    ViewBag.Error = "Shipper PO required for this ticket";
                    return View(viewName, ticket);
                }

                if (ticket.Rejected == false)
                {
                    // Execute checks for non-rejected tickets 

                    // Execute additional checks based on the type of ticket
                    switch (ticket.TicketTypeID)
                    {
                        case (int)TicketType.TYPE.GaugeRun:
                            // Tank is required
                            if (ticket.OriginTankID == null)
                            {
                                ViewBag.Error = "Tank is required";
                                return View(viewName, ticket);
                            }
                            // All product measurement values are required
                            if (ticket.ProductBSW == null || ticket.ProductObsGravity == null || ticket.ProductObsTemp == null)
                            {
                                ViewBag.Error = "Observed bs&w, gravity and temperature are required";
                                return View(viewName, ticket);
                            }
                            if (ticket.ProductHighTemp == null || ticket.ProductLowTemp == null)
                            {
                                ViewBag.Error = "Valid open and close temperatures are required";
                                return View(viewName, ticket);
                            }
                            // All opening/closing gauge values are required
                            if (   ticket.OpeningGaugeFeet == null || ticket.OpeningGaugeInch == null || ticket.OpeningGaugeQ == null
                                || ticket.ClosingGaugeFeet == null || ticket.ClosingGaugeInch == null || ticket.ClosingGaugeQ == null
                                || ticket.ClosingGauge > ticket.OpeningGauge)
                            {
                                ViewBag.Error = "Valid opening and closing gauge values are required";
                                return View(viewName, ticket);
                            }
                            // Seal off and seal on are required for gauge run
                            if (String.IsNullOrEmpty(ticket.SealOff) || String.IsNullOrEmpty(ticket.SealOn))
                            {
                                ViewBag.Error = "Seal off and seal on are required";
                                return View(viewName, ticket);
                            }
                            // GOV, GSV, and NSV will get recalculated at the trigger level
                            if (ticket.ProductBSW < ticket.MinBSW || ticket.ProductBSW > ticket.MaxBSW)
                                TempData["message"] += "Observed BS&W out of range";

                            if (ticket.ProductObsGravity < ticket.MinGravity || ticket.ProductObsGravity > ticket.MaxGravity)
                                TempData["message"] += "Observed gravity out of range";

                            if (ticket.ProductObsTemp < ticket.MinTemp || ticket.ProductObsTemp > ticket.MaxTemp)
                                TempData["message"] += "Observed temperature out of range";

                            if (Math.Abs(ticket.ProductHighTemp.Value - ticket.ProductLowTemp.Value) > ticket.ProductTempDeviation)
                                TempData["message"] += "Opening and closing temperature deviation exceeds limit";
                            break;

                        case (int)TicketType.TYPE.Gauge_Auto_Close:
                            // Tank is required
                            if (ticket.OriginTankID == null)
                            {
                                ViewBag.Error = "Tank is required";
                                return View(viewName, ticket);
                            }
                            // All product measurement values are required
                            if (ticket.ProductBSW == null || ticket.ProductObsGravity == null || ticket.ProductObsTemp == null)
                            {
                                ViewBag.Error = "Observed bs&w, gravity and temperature are required";
                                return View(viewName, ticket);
                            }
                            if (ticket.ProductHighTemp == null || ticket.ProductLowTemp == null)
                            {
                                ViewBag.Error = "Valid open and close temperatures are required";
                                return View(viewName, ticket);
                            }
                            // All opening gauge values are required
                            if (ticket.OpeningGaugeFeet == null || ticket.OpeningGaugeInch == null || ticket.OpeningGaugeQ == null)
                            {
                                ViewBag.Error = "Valid opening gauge values are required";
                                return View(viewName, ticket);
                            }
                            //gross volume is required for this ticket type
                            if (ticket.GrossUnits == null)
                            {
                                ViewBag.Error = "GOV value is required";
                                return View(viewName, ticket);
                            }
                            // Seal off and seal on are required for gauge auto close
                            if (String.IsNullOrEmpty(ticket.SealOff) || String.IsNullOrEmpty(ticket.SealOn))
                            {
                                ViewBag.Error = "Seal off and seal on are required";
                                return View(viewName, ticket);
                            }
                            // GSV, and NSV will get recalculated at the trigger level
                            if (ticket.ProductBSW < ticket.MinBSW || ticket.ProductBSW > ticket.MaxBSW)
                                TempData["message"] += "Observed BS&W out of range";

                            if (ticket.ProductObsGravity < ticket.MinGravity || ticket.ProductObsGravity > ticket.MaxGravity)
                                TempData["message"] += "Observed gravity out of range";

                            if (ticket.ProductObsTemp < ticket.MinTemp || ticket.ProductObsTemp > ticket.MaxTemp)
                                TempData["message"] += "Observed temperature out of range";

                            if (Math.Abs(ticket.ProductHighTemp.Value - ticket.ProductLowTemp.Value) > ticket.ProductTempDeviation)
                                TempData["message"] += "Opening and closing temperature deviation exceeds limit";
                            break;

                        case (int)TicketType.TYPE.NetVolume:
                            // Tank is required
                            if (ticket.OriginTankID == null)
                            {
                                ViewBag.Error = "Tank is required";
                                return View(viewName, ticket);
                            }
                            // All product measurement values are required
                            if (ticket.ProductBSW == null || ticket.ProductObsGravity == null || ticket.ProductObsTemp == null)
                            {
                                ViewBag.Error = "Observed bs&w, gravity and temperature are required";
                                return View(viewName, ticket);
                            }
                            if (ticket.ProductHighTemp == null || ticket.ProductLowTemp == null)
                            {
                                ViewBag.Error = "Valid open and close temperatures are required";
                                return View(viewName, ticket);
                            }
                            if (ticket.GrossUnits == null || ticket.GrossUnits < 0)
                            {
                                ViewBag.Error = "GOV is required";
                                return View(viewName, ticket);
                            }
                            // GSV and NSV may be recalculated at the trigger level
                            if (ticket.ProductBSW < ticket.MinBSW || ticket.ProductBSW > ticket.MaxBSW)
                                TempData["message"] += "Observed BS&W out of range";

                            if (ticket.ProductObsGravity < ticket.MinGravity || ticket.ProductObsGravity > ticket.MaxGravity)
                                TempData["message"] += "Observed gravity out of range";

                            if (ticket.ProductObsTemp < ticket.MinTemp || ticket.ProductObsTemp > ticket.MaxTemp)
                                TempData["message"] += "Observed temperature out of range";

                            if (Math.Abs(ticket.ProductHighTemp.Value - ticket.ProductLowTemp.Value) > ticket.ProductTempDeviation)
                                TempData["message"] += "Opening and closing temperature deviation exceeds limit";
                            break;

                        case (int)TicketType.TYPE.MeterRun:
                            if (ticket.MeterRunTankRequired() && ticket.OriginTankID == null)
                            {
                                ViewBag.Error = "Tank is required";
                                return View(viewName, ticket);
                            }
                            if (ticket.BOLRequired() && ticket.BOLNum == null)
                            {
                                ViewBag.Error = "BOL # is required";
                                return View(viewName, ticket);
                            }
                            // Gross and net volume required (require open and close meter to calculate gross)
                            if (ticket.NetUnits == null || ticket.NetUnits < 0)
                            {
                                ViewBag.Error = "A valid NSV is required";
                                return View(viewName, ticket);
                            }
                            if (ticket.OpenMeterUnits == null || ticket.CloseMeterUnits == null
                                || ticket.OpenMeterUnits < 0 || ticket.CloseMeterUnits < 0
                                || ticket.OpenMeterUnits > ticket.CloseMeterUnits)
                            {
                                ViewBag.Error = "Valid open and close meter readings are required";
                                return View(viewName, ticket);
                            }
                            ticket.GrossUnits = ticket.CloseMeterUnits - ticket.OpenMeterUnits; // GOV will get recalculated at the trigger level (required anyway by trigger)
                            // All product measurement values are required
                            if (ticket.ProductBSW == null || ticket.ProductObsGravity == null || ticket.ProductObsTemp == null)
                            {
                                ViewBag.Error = "Observed bs&w, gravity and temperature are required";
                                return View(viewName, ticket);
                            }
                            if (ticket.MeterRunTempsRequired() && (ticket.ProductHighTemp == null || ticket.ProductLowTemp == null))
                            {
                                ViewBag.Error = "Valid open and close temperatures are required";
                                return View(viewName, ticket);
                            }
                            if (ticket.ProductBSW != null && (ticket.ProductBSW < ticket.MinBSW || ticket.ProductBSW > ticket.MaxBSW))
                                TempData["message"] += "Observed BS&W out of range";

                            if (ticket.ProductObsGravity != null && (ticket.ProductObsGravity < ticket.MinGravity || ticket.ProductObsGravity > ticket.MaxGravity))
                                TempData["message"] += "Observed gravity out of range";

                            if (ticket.ProductObsTemp != null && (ticket.ProductObsTemp < ticket.MinTemp || ticket.ProductObsTemp > ticket.MaxTemp))
                                TempData["message"] += "Observed temperature out of range";
                            break;

                        case (int)TicketType.TYPE.BasicRun:
                            if (ticket.BOLRequired() && ticket.BOLNum == null)
                            {
                                ViewBag.Error = "BOL # is required";
                                return View(viewName, ticket);
                            }
                            if (ticket.GrossUnits == null || ticket.NetUnits == null
                                || ticket.GrossUnits < 0 || ticket.NetUnits < 0)
                            {
                                ViewBag.Error = "Valid GOV and NSV are required";
                                return View(viewName, ticket);
                            }
                            break;

                        case (int)TicketType.TYPE.GrossVolume:
                            if (ticket.BOLNum == null)
                            {
                                ViewBag.Error = "BOL # is required";
                                return View(viewName, ticket);
                            }
                            if (ticket.GrossUnits == null || ticket.NetUnits == null
                                || ticket.GrossUnits < 0 || ticket.NetUnits < 0)
                            {
                                ViewBag.Error = "Valid GOV and NSV are required";
                                return View(viewName, ticket);
                            }
                            if (ticket.ProductObsGravity != null && (ticket.ProductObsGravity < ticket.MinGravity || ticket.ProductObsGravity > ticket.MaxGravity))
                                TempData["message"] += "Observed gravity out of range";

                            if (ticket.ProductObsTemp != null && (ticket.ProductObsTemp < ticket.MinTemp || ticket.ProductObsTemp > ticket.MaxTemp))
                                TempData["message"] += "Observed temperature out of range";
                            break;

                        case (int)TicketType.TYPE.CAN_MeterRun:
                            if (ticket.BOLRequired() && ticket.BOLNum == null)
                            {
                                ViewBag.Error = "BOL # is required";
                                return View(viewName, ticket);
                            }
                            // If set, check open and close meter readings and calculate gross
                            if (ticket.OpenMeterUnits != null && ticket.CloseMeterUnits != null)
                            {
                                if (ticket.OpenMeterUnits < 0 || ticket.CloseMeterUnits < 0
                                    || ticket.OpenMeterUnits > ticket.CloseMeterUnits)
                                {
                                    ViewBag.Error = "Valid open and close meter readings are required";
                                    return View(viewName, ticket);
                                }
                                ticket.GrossUnits = ticket.CloseMeterUnits - ticket.OpenMeterUnits;
                            }
                            if (ticket.ProductBSW != null && (ticket.ProductBSW < ticket.MinBSW || ticket.ProductBSW > ticket.MaxBSW))
                                TempData["message"] += "Observed BS&W out of range";

                            if (ticket.ProductObsGravity != null && (ticket.ProductObsGravity < ticket.MinGravity || ticket.ProductObsGravity > ticket.MaxGravity))
                                TempData["message"] += "Observed gravity out of range";

                            if (ticket.ProductObsTemp != null && (ticket.ProductObsTemp < ticket.MinTemp || ticket.ProductObsTemp > ticket.MaxTemp))
                                TempData["message"] += "Observed temperature out of range";
                            break;

                        case (int)TicketType.TYPE.GaugeNet:
                            if (ticket.OriginTankID == null)
                            {
                                ViewBag.Error = "Tank is required";
                                return View(viewName, ticket);
                            }
                            if (ticket.GrossUnits == null || ticket.GrossUnits < 0)
                            {
                                ViewBag.Error = "A valid GOV is required";
                                return View(viewName, ticket);
                            }
                            // All opening/closing gauge values are required
                            if (ticket.OpeningGaugeFeet == null || ticket.OpeningGaugeInch == null || ticket.OpeningGaugeQ == null
                                || ticket.ClosingGaugeFeet == null || ticket.ClosingGaugeInch == null || ticket.ClosingGaugeQ == null)
                            {
                                ViewBag.Error = "Opening and closing gauge values are required";
                                return View(viewName, ticket);
                            }
                            // All product measurement values are required
                            if (ticket.ProductBSW == null || ticket.ProductObsGravity == null || ticket.ProductObsTemp == null)
                            {
                                ViewBag.Error = "Observed bs&w, gravity and temperature are required";
                                return View(viewName, ticket);
                            }
                            if (ticket.ProductHighTemp == null || ticket.ProductLowTemp == null)
                            {
                                ViewBag.Error = "Valid open and close temperatures are required";
                                return View(viewName, ticket);
                            }
                            // GSV and NSV may be recalculated at the trigger level
                            if (ticket.ProductBSW < ticket.MinBSW || ticket.ProductBSW > ticket.MaxBSW)
                                TempData["message"] += "Observed BS&W out of range";

                            if (ticket.ProductObsGravity < ticket.MinGravity || ticket.ProductObsGravity > ticket.MaxGravity)
                                TempData["message"] += "Observed gravity out of range";

                            if (ticket.ProductObsTemp < ticket.MinTemp || ticket.ProductObsTemp > ticket.MaxTemp)
                                TempData["message"] += "Observed temperature out of range";

                            if (Math.Abs(ticket.ProductHighTemp.Value - ticket.ProductLowTemp.Value) > ticket.ProductTempDeviation)
                                TempData["message"] += "Opening and closing temperature deviation exceeds limit";
                            break;

                        case (int)TicketType.TYPE.CAN_BasicRun:
                            break;

                        case (int)TicketType.TYPE.MineralRun:
                            if (ticket.BOLRequired() && ticket.BOLNum == null)
                            {
                                ViewBag.Error = "BOL # is required";
                                return View(viewName, ticket);
                            }
                            if (ticket.WeightGrossUnits == null || ticket.WeightTareUnits == null 
                                    || ticket.WeightGrossUnits < 0 || ticket.WeightTareUnits < 0
                                    || ticket.WeightTareUnits > ticket.WeightGrossUnits)
                            {
                                ViewBag.Error = "Valid gross and tare weights are required";
                                return View(viewName, ticket);
                            }
                            break;

                        case (int)TicketType.TYPE.Propane_With_Scale:
                            if (ticket.BOLNum == null)
                            {
                                ViewBag.Error = "BOL # is required";
                                return View(viewName, ticket);
                            }
                            if (ticket.GrossUnits == null || ticket.NetUnits == null
                                || ticket.GrossUnits < 0 || ticket.NetUnits < 0)
                            {
                                ViewBag.Error = "Valid GOV and NSV are required";
                                return View(viewName, ticket);
                            }
                            if (ticket.GrossStdUnits == null || ticket.GrossStdUnits < 0)
                            {
                                ViewBag.Error = "GOV (in gallons) is required";
                                return View(viewName, ticket);
                            }
                            if (ticket.ProductObsGravity != null && (ticket.ProductObsGravity < ticket.MinGravity || ticket.ProductObsGravity > ticket.MaxGravity))
                                TempData["message"] += "Observed gravity out of range";

                            if (ticket.ProductObsTemp != null && (ticket.ProductObsTemp < ticket.MinTemp || ticket.ProductObsTemp > ticket.MaxTemp))
                                TempData["message"] += "Observed temperature out of range";
                            break;

                        case (int)TicketType.TYPE.Production_Water_Run:
                            if (ticket.OriginTankID == null)
                            {
                                ViewBag.Error = "Tank is required";
                                return View(viewName, ticket);
                            }
                            // All opening/closing gauge values are required
                            if (ticket.OpeningGaugeFeet == null || ticket.OpeningGaugeInch == null || ticket.OpeningGaugeQ == null
                                || ticket.ClosingGaugeFeet == null || ticket.ClosingGaugeInch == null || ticket.ClosingGaugeQ == null)
                            {
                                ViewBag.Error = "Opening and closing gauge values are required";
                                return View(viewName, ticket);
                            }
                            // Defaulting to "Standard" temps & gravity (trying to force GOV=GSV=NSV)
                            ticket.ProductHighTemp = 60;
                            ticket.ProductLowTemp = 60;
                            ticket.ProductObsTemp = 60;
                            ticket.ProductObsGravity = 1;
                            ticket.ProductBSW = 0;
                            break;

                        case (int)TicketType.TYPE.Asphalt:
                            if (ticket.BOLNum == null)
                            {
                                ViewBag.Error = "BOL # is required";
                                return View(viewName, ticket);
                            }
                            if (ticket.WeightGrossUnits == null || ticket.WeightTareUnits == null 
                                    || ticket.WeightGrossUnits < 0 || ticket.WeightTareUnits < 0
                                    || ticket.WeightTareUnits > ticket.WeightGrossUnits)
                            {
                                ViewBag.Error = "Valid gross and tare weights are required";
                                return View(viewName, ticket);
                            }
                            /*if (ticket.PPG == null)
                            {
                                ViewBag.Error = "PPG";
                                return View(viewName, ticket);
                            }*/
                            if (ticket.NetUnits == null)
                            {
                                ViewBag.Error = "Net Gallons are required";
                                return View(viewName, ticket);
                            }
                            break;

                    }
                    // clear out reject reason and notes
                    ticket.RejectReasonID = null;
                    ticket.RejectNotes = null;
                }
                else
                {
                    // Rejected, make sure reason is set and check if notes required
                    OrderTicketRejectReason reject = db.OrderTicketRejectReasons.Find(ticket.RejectReasonID);
                    if (reject == null)
                    {
                        ViewBag.Error = "A reject reason must be selected for rejected tickets";
                        return View(viewName, ticket);
                    }
                    else if (reject.RequireNotes == true && String.IsNullOrEmpty(ticket.RejectNotes))
                    {
                        ViewBag.Error = "Reject notes are required for this reject reason";
                        return View(viewName, ticket);
                    }

                    // Execute additional checks for rejects based on the type of ticket
                    switch (ticket.TicketTypeID)
                    {
                        case (int)TicketType.TYPE.GaugeRun:                        
                        case (int)TicketType.TYPE.GaugeNet:
                            if (ticket.OriginTankID == null)
                            {
                                ViewBag.Error = "Tank is required";
                                return View(viewName, ticket);
                            }
                            if (ticket.RejectInfoRequired())
                            {
                                if (ticket.OpeningGauge == null || ticket.OpeningGauge < 0
                                    || ticket.ClosingGauge == null || ticket.ClosingGauge < 0
                                    || ticket.OpeningGauge != ticket.ClosingGauge)
                                {
                                    ViewBag.Error = "Opening and closing gauge values are required and must be equal";
                                    return View(viewName, ticket);
                                }
                            }
                            break;
                        case (int)TicketType.TYPE.Gauge_Auto_Close:
                            if (ticket.OriginTankID == null)
                            {
                                ViewBag.Error = "Tank is required";
                                return View(viewName, ticket);
                            }
                            if (ticket.RejectInfoRequired())
                            {
                                if (ticket.OpeningGauge == null || ticket.OpeningGauge < 0)
                                {
                                    ViewBag.Error = "Opening gauge values are required";
                                    return View(viewName, ticket);
                                }
                            }
                            break;
                        case (int)TicketType.TYPE.NetVolume:
                        case (int)TicketType.TYPE.Production_Water_Run:
                            if (ticket.OriginTankID == null)
                            {
                                ViewBag.Error = "Tank is required";
                                return View(viewName, ticket);
                            }
                            break;

                        case (int)TicketType.TYPE.MeterRun:
                            if (ticket.MeterRunTankRequired() && ticket.OriginTankID == null)
                            {
                                ViewBag.Error = "Tank is required";
                                return View(viewName, ticket);
                            }
                            break;

                        case (int)TicketType.TYPE.CAN_MeterRun:
                            break;

                        case (int)TicketType.TYPE.Asphalt:
                            break;

                        case (int)TicketType.TYPE.BasicRun:
                        case (int)TicketType.TYPE.GrossVolume:
                        case (int)TicketType.TYPE.Propane_With_Scale:
                        case (int)TicketType.TYPE.CAN_BasicRun:
                            // cannot reject, throw error?
                            break;
                    }

                    if (ticket.RejectInfoRequired() == false)
                    {
                        // clear out any measurable values
                        ticket.GrossUnits = null;
                        ticket.GrossStdUnits = null;
                        ticket.NetUnits = null;
                        ticket.OpeningGaugeFeet = null;
                        ticket.OpeningGaugeInch = null;
                        ticket.OpeningGaugeQ = null;
                        ticket.ClosingGaugeFeet = null;
                        ticket.ClosingGaugeInch = null;
                        ticket.ClosingGaugeQ = null;
                        ticket.OpenMeterUnits = null;
                        ticket.CloseMeterUnits = null;
                        ticket.ProductHighTemp = null;
                        ticket.ProductLowTemp = null;
                        ticket.ProductBSW = null;
                        ticket.ProductObsGravity = null;
                        ticket.ProductObsTemp = null;
                        ticket.WeightGrossUnits = null;
                        ticket.WeightTareUnits = null;
                        ticket.WeightNetUnits = null;
                    }
                }

                String me = User.Identity.Name;
                DateTime now = DateTime.UtcNow;
                if (ticket.ID == 0)
                {
                    ticket.UID = Guid.NewGuid();
                    ticket.FromMobileApp = false;
                    ticket.CreateDateUTC = now;
                    ticket.CreatedByUser = me;
                    db.OrderTickets.Add(ticket);
                }
                else
                {
                    ticket.LastChangeDateUTC = now; // may get overwritten by Order trigger (trigOrder_IU) for dispatched tickets
                    ticket.LastChangedByUser = me;
                    OrderTicket original = db.OrderTickets.Find(ticket.ID);
                    ticket.FromMobileApp = original.FromMobileApp;
                    ticket.CreateDateUTC = original.CreateDateUTC;
                    ticket.CreatedByUser = original.CreatedByUser;
                    db.Entry(original).CurrentValues.SetValues(ticket);
                }
                db.SaveChanges(me, now);

                TempData["success"] = "Ticket #" + ticket.CarrierTicketNum + " successfully saved";
                return RedirectToAction("Edit", "Orders", new { id = ticket.OrderID });
            }


            return View(viewName, ticket);
            //return RedirectToAction("Edit", "Ticket", new { id = ticket.ID });
        }


        /**************************************************************************************************/
        /**  PAGE: ~/Tickets/Delete/{id}                                                                 **/
        /**  DESCRIPTION: Logically deletes a ticket for an order by setting delete date and user        **/
        /**************************************************************************************************/
        [Authorize(Roles = "editOrders")]
        public ActionResult Delete(int? id)
        {
            // deleting a ticket for an order after it has been picked up may cause a discrepancy between pickup and delivery, validate? disallow delete when status not in accepted?

            OrderTicket ticket = db.OrderTickets.Find(id);
            if (ticket == null)
            {
                TempData["error"] = "Ticket not found!";
                return RedirectToAction("Index");
            }
            if (hasAccess(ticket.Order) == false)
            {
                TempData["error"] = "You do not have access to this order!";
                return RedirectToAction("Index");
            }
            if (ticket.Order.canEdit == false)
            {
                TempData["error"] = "Order #" + ticket.Order.OrderNum + " has been settled and cannot be changed";
                return RedirectToAction("Details", "Orders", new { id = ticket.OrderID });
            }

            // check that this is not the only valid ticket (required for orders Picked Up or greater)
            int activetickets = db.OrderTickets.Where(t => t.OrderID == ticket.OrderID && !t.Rejected && t.DeleteDateUTC == null).Count();
            if (activetickets == 1 && ticket.Order.OriginInfoRequired() && ticket.Rejected == false)
            {
                TempData["error"] = "Deleting this ticket would invalidate the order!  Please create a valid ticket";
                return RedirectToAction("Edit", "Orders", new { id = ticket.OrderID });
            }

            return View(ticket);
        }

        [HttpPost,ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "editOrders")]
        public ActionResult DeleteConfirmed(int id)
        {
            OrderTicket ticket = db.OrderTickets.Find(id);

            // Do not remove, just set delete date/user
            //db.OrderTickets.Remove(ticket);
            //zero out any totals, looks like code checks for deleted?
            ticket.DeleteDateUTC = DateTime.UtcNow;
            ticket.DeletedByUser = User.Identity.Name;
            db.Entry(ticket).CurrentValues.SetValues(ticket);

            db.SaveChanges(User.Identity.Name);
            return RedirectToAction("Edit", "Orders", new { id = ticket.OrderID });
        }

        /**************************************************************************************************/
        /**  FUNCTION:                                                                                   **/
        /**  DESCRIPTION:                                                                                **/
        /**************************************************************************************************/
        [HttpPost]
        public JsonResult IsCarrierTicketUnique(int id, int orderID, string carrierTicketNum)
        {
            var model = db.OrderTickets.Where(x => x.ID != id && x.OrderID == orderID && x.DeleteDateUTC == null && x.CarrierTicketNum.Equals(carrierTicketNum, StringComparison.CurrentCultureIgnoreCase));
            return Json(model.Count() == 0);
        }

        /**************************************************************************************************/
        /**  FUNCTION:                                                                                   **/
        /**  DESCRIPTION:                                                                                **/
        /**************************************************************************************************/
        [HttpPost]
        public JsonResult IsGrossQtyValid(int uomID, decimal? GrossUnits)
        {
            return Json(IsQtyValid(uomID, GrossUnits));
        }

        /**************************************************************************************************/
        /**  FUNCTION:                                                                                   **/
        /**  DESCRIPTION:                                                                                **/
        /**************************************************************************************************/
        [HttpPost]
        public JsonResult IsNetQtyValid(int uomID, decimal? NetUnits)
        {
            return Json(IsQtyValid(uomID, NetUnits));
        }

        /**************************************************************************************************/
        /**  FUNCTION:                                                                                   **/
        /**  DESCRIPTION:                                                                                **/
        /**************************************************************************************************/
        private bool IsQtyValid(int uomID, decimal? qty)
        {
            const decimal MAXBARRELS = 320; // this should not be used, instead check global DB setting #6 if at all (check order instead of ticket)
            bool ret = false; // default value
            if (qty.HasValue)
            {
                using (SSDB ssdb = new SSDB())
                {
                    using (System.Data.SqlClient.SqlCommand cmd = ssdb.BuildCommand("SELECT dbo.fnConvertUom(@Units, @OldUomID, @newUomID)"))
                    {
                        cmd.Parameters.AddWithValue("@Units", qty.Value);
                        cmd.Parameters.AddWithValue("@OldUomID", uomID);
                        cmd.Parameters.AddWithValue("@NewUomID", 1); // barrels
                        ret = DBHelper.ToInt32(cmd.ExecuteScalar()) <= MAXBARRELS;
                    }
                }
            }
            return ret;
        }

        /**************************************************************************************************/
        /**  FUNCTION:                                                                                   **/
        /**  DESCRIPTION:                                                                                **/
        /**************************************************************************************************/
        public JsonResult GetGSV_NSV(decimal? grossUnits, decimal? temp, decimal? opentemp, decimal? closetemp, decimal? grav, decimal? bsw, int? sigfig)
        {
            decimal? gsv = new decimal?(), nsv = new decimal?();
            using (SSDB ssdb = new SSDB())
            {
                if (grossUnits.HasValue && temp.HasValue && grav.HasValue && bsw.HasValue)
                {
                    if (opentemp.HasValue == false) opentemp = temp;
                    if (closetemp.HasValue == false) closetemp = temp;
                    gsv = DBHelper.ToDecimalSafe(ssdb.QuerySingleValue("SELECT dbo.fnCrudeNetCalculator2004api({0},{1},{2},{3},{4},{5},{6})"
                        , new object[] { grossUnits.Value, temp.Value, opentemp.Value, closetemp.Value, grav.Value, 0, sigfig }));
                    nsv = DBHelper.ToDecimalSafe(ssdb.QuerySingleValue("SELECT dbo.fnCrudeNetCalculator2004api({0},{1},{2},{3},{4},{5},{6})"
                        , new object[] { grossUnits.Value, temp.Value, opentemp.Value, closetemp.Value, grav.Value, bsw.Value, sigfig }));
                }
            }
            return Json(new { gsv = gsv.HasValue ? gsv.Value.ToString("0.000") : "", nsv = nsv.HasValue ? nsv.Value.ToString("0.000") : "" },
                JsonRequestBehavior.AllowGet);
        }

        
        public bool hasAccess(Order order)
        {
            // use profile to determine if a user can work with this ticket/order
            int myCarrierID = Converter.ToInt32(Profile.GetPropertyValue("CarrierID"));
            int myDriverID = Converter.ToInt32(Profile.GetPropertyValue("DriverID"));
            int myTerminalID = Converter.ToInt32(Profile.GetPropertyValue("TerminalID"));

            return ((myCarrierID == -1) || (myCarrierID == order.CarrierID) || (myDriverID == order.DriverID))
                && (   (myTerminalID <= 0)  // No Terminal filtering
                    || (   (order.Origin == null || order.Origin.TerminalID == null || order.Origin.TerminalID == myTerminalID) // origin terminal match
                        && (order.Destination == null || order.Destination.TerminalID == null || order.Destination.TerminalID == myTerminalID) // dest terminal match
                        && (order.Destination == null || order.Destination.TerminalID == null || order.Destination.TerminalID == myTerminalID))); // driver terminal match
        }
    }
}
