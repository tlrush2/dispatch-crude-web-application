﻿using System;
using System.Linq;
using System.Web.Mvc;

namespace DispatchCrude.Controllers
{
    // see http://devstuffs.wordpress.com/2010/12/12/how-to-use-customerrors-in-asp-net-mvc-2/ for details

    public class ErrorController : Controller
    {
        public ActionResult Index()
        {
            return View("Error");
        }

        public ActionResult NotFound(string errorUrl)
        {
            ViewBag.ErrorUrl = errorUrl;

            return View();
        }

        public ActionResult AccessDenied(string action)
        {
            ViewData["action"] = action;

            return View();
        }
    }
}
