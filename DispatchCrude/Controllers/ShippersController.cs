﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DispatchCrude.Models;
using DispatchCrude.Core;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using DispatchCrude.Extensions;

namespace DispatchCrude.Controllers
{
    [Authorize(Roles = "viewShippers")]
    public class ShippersController : Controller
    {
        // This session variable is required for Asyncronous file uploads to work with new records.
        // The file is uploaded here temporarily until the user actually clicks the save button.
        private const string SESSION_BOL_FILENAME = "SB_FILENAME"
            , SESSION_BOL_FILE = "SB_FILE";

        private DispatchCrudeDB db = new DispatchCrudeDB();

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        // GET: Shippers
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest request = null, int id = 0)
        {
            return Read(request, null, id);
        }

        private ContentResult Read([DataSourceRequest] DataSourceRequest request, ModelStateDictionary modelState, int id = 0)
        {
            // .ToList() is required due to filtering on "Active" field, which is NOT MAPPED
            var data = db.Customers.Where(s => s.ID == id || id == 0).Include(c => c.State).ToList();
            var result = data.ToDataSourceResult(request, modelState);
            // exclude these large fields since they are not needed and can be quite large
            return App_Code.JsonStringResult.Create(result, "BOLHeaderImageSrc", "BOLHeaderBlob", "ZPLHeaderBlob");
        }

        [HttpPost]
        public ActionResult SaveBOLHeaderBlob(IEnumerable<HttpPostedFileBase> bolHeaderBlob)
        {
            // The Name of the Upload component is "files"
            if (bolHeaderBlob != null)
            {
                foreach (var file in bolHeaderBlob)
                {
                    Session[SESSION_BOL_FILENAME] = Path.GetFileName(file.FileName);
                    Session[SESSION_BOL_FILE] = new BinaryReader(file.InputStream).ReadBytes(file.ContentLength);
                    // we only accept a single file so just break
                    break;
                }
            }
            // Return an empty string to signify success
            return Content("");
        }

        public ActionResult ClearDocument()
        {
            Session[SESSION_BOL_FILE] = null;
            Session[SESSION_BOL_FILENAME] = null;

            return Json(new { status="OK"}, JsonRequestBehavior.AllowGet);
        }

        private bool ValidateBOLFile(ModelStateDictionary modelState = null)
        {
            bool ret = Session[SESSION_BOL_FILENAME] != null && Session[SESSION_BOL_FILE] != null;
            if (!ret && modelState != null)
                modelState.AddModelError(null, "BOL File must be provided");
            return ret;
        }

        // POST: Create Shippers
        [HttpPost]
        [Authorize(Roles = "createShippers")]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, Customer shipper)
        {
            // require a valid BOLHeaderBlob for new records
            if (ModelState.IsValid && ValidateBOLFile(ModelState))
            {
                try
                {
                    // ensure all new Shippers are marked "Active" (otherwise they will be created deleted)
                    shipper.Active = true;

                    //Ensure non-numeric characters from Masked textboxes do not make it to the database
                    shipper.ContactPhone = NumericUtils.RemoveNonNumeric(shipper.ContactPhone);
                    shipper.HelpDeskPhone = NumericUtils.RemoveNonNumeric(shipper.HelpDeskPhone);

                    // Save the new file into the database
                    shipper.BOLHeaderBlob = Session[SESSION_BOL_FILE] as byte[];
                    shipper.BOLHeaderFileName = Session[SESSION_BOL_FILENAME].ToString();

                    // Save new record to the database
                    db.Customers.Add(shipper);
                    db.SaveChanges(User.Identity.Name);  // Create dates & usernames will all be taken care of by this line.

                    //Clear bol header session variable after successful save.  Otherwise the next shipper added will automatically use this file if the user did not select one
                    //This is behavior is undesireable because the user could forget to choose the correct image and end up with an image for another shipper by accident.
                    if (Session[SESSION_BOL_FILE] != null)
                    {
                        Session[SESSION_BOL_FILE] = null;
                        Session[SESSION_BOL_FILENAME] = null;
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(null, ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { shipper }.ToDataSourceResult(request, ModelState));
                }
            }

            return Read(request, ModelState, shipper.ID);
        }

        // POST: Update Shippers
        [HttpPost]
        [Authorize(Roles = "editShippers")]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, Customer shipper)
        {
            if (ModelState.IsValid)
            {
                try
                { 
                    // Get the existing record
                    // This allows us to keep the existing file upload if one is not specified in an edit
                    Customer existing = db.Customers.Find(shipper.ID);

                    //Ensure non-numeric characters from Masked textboxes do not make it to the database
                    shipper.ContactPhone = NumericUtils.RemoveNonNumeric(shipper.ContactPhone);
                    shipper.HelpDeskPhone = NumericUtils.RemoveNonNumeric(shipper.HelpDeskPhone);

                    // Specify all fields that will/can be updated by user input on the popup
                    db.CopyEntityValues(existing, shipper, "BOLHeaderFileName", "BOLHeaderBlob");

                    if (ValidateBOLFile())  // If a file was specified and it is not zero-length
                    {
                        existing.BOLHeaderBlob = Session[SESSION_BOL_FILE] as byte[];
                        existing.BOLHeaderFileName = Session[SESSION_BOL_FILENAME].ToString();
                    }
                    // else - no new image was specified, keep the original image and update the rest of the fields

                    db.SaveChanges(User.Identity.Name);  // Create & Modified dates & usernames will be taken care of by this line.

                    //Clear bol header session variable after successful save.  Otherwise the next shipper added will automatically use this file if the user did not select one
                    //This is behavior is undesireable because the user could forget to choose the correct image and end up with an image for another shipper by accident.
                    if (Session[SESSION_BOL_FILE] != null)
                    {
                        Session[SESSION_BOL_FILE] = null;
                        Session[SESSION_BOL_FILENAME] = null;
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(null, ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { shipper }.ToDataSourceResult(request, ModelState));
                }
            }

            return Read(request, ModelState, shipper.ID);
        }

        // Delete (Deactivate)
        [Authorize(Roles = "deactivateShippers")]
        public ActionResult Delete(Customer shipper)
        {
            db.Customers.Attach(shipper);
            db.Customers.Remove(shipper);
            db.SaveChanges(User.Identity.Name);

            return null;
        }

        // Download the BOL header image
        public ActionResult downloadImage(int id)
        {
            Customer customer = db.Customers.Find(id);
            return File(customer.BOLHeaderBlob, "image/png", customer.BOLHeaderFileName);
        }

        // View the BOL header image (in browser)
        public ActionResult viewImage(int id)
        {
            Customer customer = db.Customers.Find(id);
            return File(customer.BOLHeaderBlob, "image/png");
        }

        // Remote validation for shipper name
        //public JsonResult IsNameAvailable(string name, int id)
        //{
        //    if (db.Customers.Where(s => s.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase) && (id == 0 || s.ID != id)).Count() == 0)
        //        return Json("That shipper already exists", JsonRequestBehavior.AllowGet);
        //    else
        //        return Json(true, JsonRequestBehavior.AllowGet);            
        //}
    }
}
