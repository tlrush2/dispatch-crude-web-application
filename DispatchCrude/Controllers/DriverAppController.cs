﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.Profile;
using Newtonsoft.Json;
using DispatchCrude.Core;
using DispatchCrude.Models;
using DispatchCrude.Models.Sync;
using DispatchCrude.Models.Sync.DriverApp;
using AlonsIT;
using Newtonsoft.Json.Linq;

namespace DispatchCrude.Controllers
{
    public class DriverAppController : Controller
    {
        private DispatchCrudeDB db = new DispatchCrudeDB();
        //
        // GET: /Account/Index

        [AllowAnonymous]
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        //
        // POST: /DriverApp/Login

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(string userName, string password)
        {
            Outcome outcome = new Outcome();
            if (string.IsNullOrEmpty(userName) || !Membership.ValidateUser(userName, password))
                outcome.Message = "User Authentication failed";
            else
            {
                int driverID = 0;
                if ((outcome = GetDriverID(userName, out driverID)).Success)
                {   // valid outcome occurred, so return the MasterData instance in the LoginResult
                    return JsonResult(new LoginResult(outcome, MasterData.Init(userName, driverID, ref outcome)));
                }
            }
            // an error occurred, so just return the Outcome
            return JsonResult(new LoginResult(outcome));
        }

        private Outcome GetDriverID(string userName, out int driverID)
        {
            Outcome ret = new Outcome();
            driverID = 0; // default value
            if (!Roles.IsUserInRole(userName, "Driver"))
                ret.Message = "User is not assigned the 'Driver' role";
            else if ((driverID = DBHelper.ToInt32(ProfileBase.Create(userName).GetPropertyValue("DriverID"))) <= 0)
                ret.Message = "User is not assigned to a Carrier Driver account";
            else if (!GetDriverMobileAppEnabled(driverID))
                ret.Message = "Your driver account is not configured for Mobile App access";
            else
                ret.Success = true;
            return ret;
        }
        private bool GetDriverMobileAppEnabled(int driverID)
        {
            using (SSDB ssdb = new SSDB())
            {
                return DBHelper.ToBoolean(ssdb.QuerySingleValue("SELECT MobileApp FROM dbo.tblDriver WHERE ID = {0}", driverID));
            }
        }

        [HttpGet]
        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ChangePassword(string userName, string oldPassword, string newPassword)
        {
            Outcome outcome = new Outcome();
            if (!Membership.ValidateUser(userName, oldPassword))
                outcome.Message = "Old Password was invalid";
            else if (!Membership.GetUser(userName).ChangePassword(oldPassword, newPassword))
                outcome.Message = "Invalid new password was provided";
            else
            {
                int driverID = 0;
                if ((outcome = GetDriverID(userName, out driverID)).Success)
                    return JsonResult(new LoginResult(outcome, MasterData.ResetPasswordHash(userName, driverID, ref outcome)));
            }
            // an error occurred, so just return the Outcome
            return JsonResult(new LoginResult(outcome));
        }

        [AllowAnonymous]
        public ActionResult Sync()
        {
            return View();
        }

        // support method
        private ActionResult JsonResult(object data)
        {
            return Content(JsonConvert.SerializeObject(data, SyncDataInput.JSonSettings()));
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Sync(string json)
        {
            Outcome outcome = new Outcome();

            DateTime syncStart = DateTime.Now;

            try
            {
                // deserialize the incoming JSON into a SyncDataInput object
                if (json.Length == 0)
                    outcome.Message = "No JSON was received";
                else
                {
                    SyncDataInput incomingSD = null;
                    try
                    {
                        incomingSD = SyncDataInput.GetFromJSON(json);
                    }
                    catch (Exception e)
                    {
                        throw new Exception("JSON Deserialize error occurred", e);
                    }
                    // if the object is valid, then continue processing
                    if (incomingSD == null)
                        outcome.Message = "Invalid JSON was received";
                    else if (string.IsNullOrEmpty(incomingSD.Master.UserName))
                        outcome.Message = "UserName was missing or invalid";
                    else if (incomingSD.Master != null)
                    {
                        MasterData outgoingMD = MasterData.Sync(incomingSD.Master, ref outcome);
                        if (outcome.Success)
                        {
                            try
                            {
                                // if this is an initial (full) sync, we need to ensure that any "VirtualDelete" records are first cleared (not needed)
                                if (incomingSD.Master.LastSyncUTC.HasValue)
                                {   // do the PUSH synchronization

                                    // get the driver local time zone
                                    byte? TimeZoneID = null;
                                    if (string.IsNullOrEmpty(incomingSD.CurrentAppTzDstOffsets) == false)
                                    {
                                        try
                                        {
                                            int TZoffset = -99;
                                            int.TryParse(incomingSD.CurrentAppTzDstOffsets.Substring(0, incomingSD.CurrentAppTzDstOffsets.IndexOf(",")), out TZoffset);
                                            Models.TimeZone DriverTZ = db.TimeZones.Where(t => t.StandardOffsetHours == TZoffset).FirstOrDefault();
                                            if (DriverTZ != null) TimeZoneID = DriverTZ.ID;
                                        }
                                        catch { }
                                    }

                                    // only update the order if the driver matches (it returns TRUE if something actually changed)
                                    SyncOrders_Tickets(incomingSD, ref outcome, outgoingMD.UserName);

                                    SyncOrderTransfers(incomingSD, ref outcome, outgoingMD.UserName);

                                    SyncOrderSignatures(incomingSD, ref outcome, outgoingMD.UserName);

                                    SyncDriverLocations(incomingSD, ref outcome, outgoingMD.UserName);

                                    SyncHos(incomingSD, ref outcome, outgoingMD.UserName, TimeZoneID);

                                    SyncHosSignatures(incomingSD, ref outcome, outgoingMD.UserName);

                                    SyncExceptions(incomingSD, ref outcome, outgoingMD.UserName);

                                    SyncOrderPhotos(incomingSD, ref outcome, outgoingMD.UserName);

                                    SyncQuestionnaires(incomingSD, ref outcome, outgoingMD.UserName, TimeZoneID);

                                    SyncDispatchMessages(incomingSD, ref outcome, outgoingMD.UserName);

                                }
                            }
                            catch (Exception ex)
                            {
                                outcome.Success = false;
                                outcome.Message = ex.Message;
                            }

                            SyncDataOutput output = new SyncDataOutput(incomingSD.Master.LastSyncUTC, outcome, outgoingMD, incomingSD.OrdersPresent);

                            MarkOrdersDriverSynced(output.Orders);

                            // log the incoming JSON if this associated system setting is enabled
                            if (Settings.SettingsID.DA_DEBUG_SYNC.AsBool())
                            {
                                using (SSDB ssdb = new SSDB())
                                {
                                    ssdb.ExecuteSql("INSERT INTO tblDriverSyncLog(DriverID, IncomingJSON, OutgoingJSON, SyncStart, SyncDate) VALUES ({0}, {1},{2}, {3}, {4})"
                                        , incomingSD.Master.DriverID
                                        , DBHelper.QuoteStr(incomingSD.SerializeAsJson(true)) // suppress non-logged fields
                                        , DBHelper.QuoteStr(output.SerializeAsJson(true)) // suppress non-logged fields
                                        , DBHelper.QuoteStr(syncStart.ToString("yyyy/MM/dd HH:mm:ss.fff"))
                                        , DBHelper.QuoteStr(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.fff")));
                                }
                            }

                            return Content(output.SerializeAsJson());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("DriverApp.Sync server exception occurred, JSON={0}", json), ex);
            }
            // just return the failure outcome
            return JsonResult(new SyncDataOutput(outcome));
        }

        private string LogSyncError(int driverID, string error)
        {
            error = error.Replace("The transaction ended in the trigger. ", "");
            error = error.Replace("The batch has been aborted.", "");
            error = error.Replace("FIRED", "");
            error = error.Replace("COMPLETE", "");
            error = error.Replace("trigOrderTicket_IU", "trigOT_IU");
            error = error.Replace("  ", " ").Trim();
            using (SSDB ssdb = new SSDB())
            {
                using (System.Data.SqlClient.SqlCommand cmd = ssdb.BuildCommand("spLogSyncError"))
                {
                    cmd.Parameters["@driverID"].Value = driverID;
                    cmd.Parameters["@errorMsg"].Value = error;
                    cmd.ExecuteNonQuery();
                }
            }
            return error;
        }

        private void SyncOrders_Tickets(SyncDataInput incomingSD, ref Outcome outcome, string userName)
        {
            List<string> processedTicketUIDs = new List<string>();

            using (DispatchCrudeDB db = new DispatchCrudeDB())
            {
                if (incomingSD.OrderEdits != null)
                {
                    // process any changed orders from the Driver App
                    foreach (OrderEdit incomingOrder in incomingSD.OrderEdits)
                    {
                        Order currentOrder = db.Orders.Where(r => r.ID == incomingOrder.ID)
                            .Include(r => r.OrderTickets)
                            .SingleOrDefault();

                        if (incomingSD.Master.DriverID == currentOrder.DriverID)
                        {
                            bool changed = incomingOrder.UpdateValues(currentOrder);
                            if (incomingSD.OrderTickets != null)
                            {
                                foreach (OrderTicketDA ticket in incomingSD.OrderTickets.Where(ot => ot.OrderID == incomingOrder.ID))
                                {
                                    OrderTicket currentTicket = currentOrder.OrderTickets.FirstOrDefault(ot => ot.UID == ticket.UID);
                                    if (currentTicket == null)
                                    {
                                        currentTicket = new OrderTicket(ticket.UID, ticket.OrderID);
                                        currentOrder.OrderTickets.Add(currentTicket);
                                    }
                                    processedTicketUIDs.Add(ticket.UID.ToString());
                                    if (ticket.UpdateValues(currentTicket))
                                        changed = true;
                                }
                            }
                            if (changed)
                            {
                                try
                                {
                                    db.SaveChanges(userName);
                                }
                                catch (Exception e)
                                {
                                    outcome.Success = false;
                                    outcome.Message += LogSyncError(incomingSD.Master.DriverID, currentOrder.OrderNum + ": " + e.Message);
                                }
                            }
                        }
                    }
                }

                // process any incoming ticket changes that did not have an associated changed (order)
                if (incomingSD.OrderTickets != null)
                {
                    foreach (OrderTicketDA ticket in incomingSD.OrderTickets)
                    {
                        if (!processedTicketUIDs.Contains(ticket.UID.ToString()))
                        {
                            OrderTicket currentTicket = db.OrderTickets.FirstOrDefault(ot => ot.UID == ticket.UID);
                            if (currentTicket == null)
                            {
                                currentTicket = new OrderTicket(ticket.UID, ticket.OrderID);
                                db.OrderTickets.Add(currentTicket);
                            }
                            if (ticket.UpdateValues(currentTicket))
                            {
                                try
                                {
                                    db.SaveChanges(userName);
                                }
                                catch (Exception e)
                                {
                                    outcome.Success = false;
                                    outcome.Message += LogSyncError(incomingSD.Master.DriverID, currentTicket.CarrierTicketNum + ": " + e.Message);
                                }
                            }
                        }
                    }
                }
            }
        }
        private void SyncOrderTransfers(SyncDataInput incomingSD, ref Outcome outcome, string userName)
        {
            if (incomingSD.OrderTransfers != null)
            {
                System.Data.DataTable dtTransfers = SyncHelper.PopulateDataTableFromList(incomingSD.OrderTransfers);
                using (SSDB ssdb = new SSDB(false))
                {
                    foreach (System.Data.DataRow row in dtTransfers.Rows)
                    {
                        row.AcceptChanges();
                        row.SetModified();
                        try
                        {
                            SSDBSave.SaveDataRow(ssdb, row, "tblOrderTransfer", "OrderID"
                                , SSDBSave.UpdateMode.IgnoreConcurrencyExceptions
                                , userName);
                            ssdb.ExecuteSql("UPDATE tblTruck SET LastOdometer = {1}, LastOdometerDateUTC = '{2}' " +
                                "WHERE ID = (SELECT TruckID FROM tblOrder WHERE ID = {0}) " +
                                "AND '{2}' > LastOdometerDateUTC",
                                row["OrderID"],
                                row["DestTruckStartMileage"],
                                row["LastChangeDateUTC"] ?? DateTime.UtcNow);
                        }
                        catch (Exception e)
                        {
                            // record the failure but continue processing
                            outcome.Success = false;
                            outcome.Message += e.Message;
                        }
                    }
                }
            }
        }
        private void SyncOrderSignatures(SyncDataInput incomingSD, ref Outcome outcome, string userName)
        {
            if (incomingSD.OrderSignatures != null)
            {
                System.Data.DataTable dtSignatures = SyncHelper.PopulateDataTableFromList(incomingSD.OrderSignatures);
                // since UID is the key, but an ID (identity) column exists, remove it so 
                //it doesn't try to insert into an identity column (which will fail)
                dtSignatures.Columns.Remove("ID");
                using (SSDB ssdb = new SSDB(false))
                {
                    foreach (System.Data.DataRow row in dtSignatures.Rows)
                    {
                        try
                        {
                            object uid = SSDBSave.SaveDataRow(ssdb, row, "tblOrderSignature", "UID"
                                , SSDBSave.UpdateMode.IgnoreConcurrencyExceptions | SSDBSave.UpdateMode.IncludeBlobFields
                                , userName);
                        }
                        catch (Exception e)
                        {
                            // record the failure but continue processing
                            outcome.Success = false;
                            outcome.Message += e.Message;
                        }
                    }
                }
            }
        }
        private void SyncDriverLocations(SyncDataInput incomingSD, ref Outcome outcome, string userName)
        {
            if (incomingSD.DriverLocations != null)
            {
                System.Data.DataTable dtDL = SyncHelper.PopulateDataTableFromList(incomingSD.DriverLocations);
                // since UID is the key, but an ID (identity) column exists, remove it so 
                //it doesn't try to insert into an identity column (which will fail)
                dtDL.Columns.Remove("ID");
                using (SSDB ssdb = new SSDB(false))
                {
                    foreach (System.Data.DataRow row in dtDL.Rows)
                    {
                        try
                        {
                            object uid = SSDBSave.SaveDataRow(ssdb, row, "tblDriverLocation", "UID"
                                , SSDBSave.UpdateMode.IgnoreConcurrencyExceptions
                                , userName);
                        }
                        catch (Exception e)
                        {
                            // record the failure but continue processing
                            outcome.Success = false;
                            outcome.Message += e.Message;
                        }
                    }
                }
            }
        }

        private void SyncHos(SyncDataInput incomingSD, ref Outcome outcome, string userName, byte? driverTimeZoneID)
        {
            if (incomingSD.Hos != null)
            {
                System.Data.DataTable dtDL = SyncHelper.PopulateDataTableFromList(incomingSD.Hos);
                // since UID is the key, but an ID (identity) column exists, remove it so 
                //it doesn't try to insert into an identity column (which will fail)
                dtDL.Columns.Remove("ID");
                // Add column (if it does not exist) to capture driver local time zone
                if (dtDL.Columns.Contains("LogTimeZoneID") == false)
                    dtDL.Columns.Add("LogTimeZoneID", typeof(Byte));

                using (SSDB ssdb = new SSDB(false))
                {
                    foreach (System.Data.DataRow row in dtDL.Rows)
                    {
                        try
                        {
                            // Attempt to get nearest city/state from gps coordinates
                            GoogleLocation location = LocationHelper.getLocation(Convert.ToDecimal(row["Lat"]), Convert.ToDecimal(row["Lon"]));
                            if (location.City != null)
                                row["LocationDescription"] = location.City + ", " + location.StateAbbrev;

                            // Capture driver local time zone
                            row["LogTimeZoneID"] = Converter.ToDBNullFromEmpty(driverTimeZoneID);

                            // Default newly required fields with ELD expansion
                            if (row["EventRecordOriginID"] == null || row["EventRecordOriginID"].ToString() == "0") { row["EventRecordOriginID"] = ELDEventRecordOrigin.Type.DRIVER; }
                            if (row["EventRecordStatusID"] == null || row["EventRecordStatusID"].ToString() == "0") { row["EventRecordStatusID"] = ELDEventRecordStatus.Type.ACTIVE; }

                            object uid = SSDBSave.SaveDataRow(ssdb, row, "tblHos", "UID"
                                , SSDBSave.UpdateMode.IgnoreConcurrencyExceptions
                                , userName);
                        }
                        catch (Exception e)
                        {
                            // record the failure but continue processing
                            outcome.Success = false;
                            outcome.Message += e.Message;
                        }
                    }

                    // JT-2570: If a driver has edited an HOS record AND someone on the server has created a change request for that same record,
                    // the mobile app (driver change) will win.  We need to deactivate/invalidate any web originated records that fit this scenario
                    // here before they ever make it down to the mobile app.
                    try
                    {
                        string sql = @"UPDATE H1 " +
                                "SET H1.EventRecordStatusID = 2 " +
                                "FROM tblHos H1 " +
                                "WHERE H1.EventRecordStatusID = 3 " +
                                "AND EXISTS ( SELECT 1 " +
                                            "FROM tblHos H2 " +
                                            "WHERE H2.EventRecordStatusID = 2 " +
                                            "AND H2.UID = H1.HosParentUID )";
                        ssdb.ExecuteSql(sql);
                    }
                    catch (Exception e)
                    {
                        // record the failure but continue processing
                        outcome.Success = false;
                        outcome.Message += e.Message;
                    }
                }
            }
        }
        private void SyncHosSignatures(SyncDataInput incomingSD, ref Outcome outcome, string userName)
        {
            if (incomingSD.HosSignatures != null)
            {
                System.Data.DataTable dtSignatures = SyncHelper.PopulateDataTableFromList(incomingSD.HosSignatures);
                // since UID is the key, but an ID (identity) column exists, remove it so 
                //it doesn't try to insert into an identity column (which will fail)
                dtSignatures.Columns.Remove("ID");
                using (SSDB ssdb = new SSDB(false))
                {
                    foreach (System.Data.DataRow row in dtSignatures.Rows)
                    {
                        try
                        {
                            object uid = SSDBSave.SaveDataRow(ssdb, row, "tblHosSignature", "UID"
                                , SSDBSave.UpdateMode.IgnoreConcurrencyExceptions | SSDBSave.UpdateMode.IncludeBlobFields
                                , userName);
                        }
                        catch (Exception e)
                        {
                            // record the failure but continue processing
                            outcome.Success = false;
                            outcome.Message += e.Message;
                        }
                    }
                }
            }
        }

        private void SyncExceptions(SyncDataInput incomingSD, ref Outcome outcome, string userName)
        {
            if (incomingSD.Exceptions != null)
            {
                System.Data.DataTable dtExceptions = SyncHelper.PopulateDataTableFromList(incomingSD.Exceptions);
                using (SSDB ssdb = new SSDB(false))
                {
                    foreach (System.Data.DataRow row in dtExceptions.Rows)
                    {
                        try
                        {
                            object uid = SSDBSave.SaveDataRow(ssdb, row, "tblDriverAppExceptionLog", "UID"
                                , SSDBSave.UpdateMode.IgnoreConcurrencyExceptions
                                , userName);
                        }
                        catch (Exception e)
                        {
                            // record the failure but continue processing
                            outcome.Success = false;
                            outcome.Message += e.Message;
                        }
                    }
                }
            }
        }
        private void SyncOrderPhotos(SyncDataInput incomingSD, ref Outcome outcome, string userName)
        {
            if (incomingSD.OrderPhotos != null)
            {
                System.Data.DataTable dtOrderPhotos = SyncHelper.PopulateDataTableFromList(incomingSD.OrderPhotos);
                // since UID is the key, but an ID (identity) column exists, remove it so 
                //it doesn't try to insert into an identity column (which will fail)
                dtOrderPhotos.Columns.Remove("ID");
                using (SSDB ssdb = new SSDB(false))
                {
                    foreach (System.Data.DataRow row in dtOrderPhotos.Rows)
                    {
                        try
                        {
                            object uid = SSDBSave.SaveDataRow(ssdb, row, "tblOrderPhoto", "UID"
                                , SSDBSave.UpdateMode.IgnoreConcurrencyExceptions | SSDBSave.UpdateMode.IncludeBlobFields
                                , userName);
                        }
                        catch (Exception e)
                        {
                            // record the failure but continue processing
                            outcome.Success = false;
                            outcome.Message += e.Message;
                        }
                    }
                }
            }
        }

        private void SyncQuestionnaires(SyncDataInput incomingSD, ref Outcome outcome, string userName, byte? driverTimeZoneID)
        {
            if (incomingSD.QuestionnaireSubmissions != null)
            {
                using (DispatchCrudeDB db = new DispatchCrudeDB())
                {
                    foreach (QuestionnaireSubmissionDA qSubmission in incomingSD.QuestionnaireSubmissions)
                    {
                        Driver driver = db.Drivers.Find(incomingSD.Master.DriverID);
                        var template = db.QuestionnaireTemplates.Find(qSubmission.QuestionnaireTemplateID);
                        // skip submission if driver does not have permission to submit this form (carrier doesn't match)
                        if (template.CarrierID != null && template.CarrierID != driver.CarrierID)
                            continue;

                        try
                        {
                            QuestionnaireSubmission submission = new QuestionnaireSubmission()
                            {
                                QuestionnaireTemplateID = qSubmission.QuestionnaireTemplateID,
                                DriverID = qSubmission.DriverID,
                                TruckID = qSubmission.TruckID,
                                TrailerID = qSubmission.TrailerID,
                                Trailer2ID = qSubmission.Trailer2ID,
                                SubmissionDateUTC = qSubmission.SubmissionDateUTC,
                                SubmissionTimeZoneID = driverTimeZoneID
                            };
                            db.QuestionnaireSubmissions.Add(submission);
                            db.SaveChanges(userName);

                            if (incomingSD.QuestionnaireResponses != null)
                            {
                                foreach (QuestionnaireResponseDA qResponse in incomingSD.QuestionnaireResponses.Where(r => r.QuestionnaireSubmissionID == qSubmission.ID))
                                {
                                    QuestionnaireResponse response = new QuestionnaireResponse()
                                    {
                                        Answer = (qResponse.Answer != null && qResponse.Answer.Length > 0) ? qResponse.Answer : null,  //  If the answer were not sent use null, not blank
                                        Notes = (qResponse.Notes != null && qResponse.Notes.Length > 0) ? qResponse.Notes : null,     //  If the notes were not sent user null, not blank
                                        QuestionnaireQuestionID = qResponse.QuestionnaireQuestionID,
                                        QuestionnaireSubmissionID = submission.ID, // get submission ID generated by the database (not from the mobile app)
                                    };
                                    db.QuestionnaireResponses.Add(response);
                                    db.SaveChanges(userName);

                                    if (incomingSD.QuestionnaireResponseBlobs != null)
                                    {
                                        foreach (QuestionnaireResponseBlobDA qResponseBlob in incomingSD.QuestionnaireResponseBlobs.Where(r => r.QuestionnaireResponseID == qResponse.ID))
                                        {
                                            QuestionnaireResponseBlob responseBlob = new QuestionnaireResponseBlob()
                                            {
                                                AnswerBlob = qResponseBlob.AnswerBlob,
                                                QuestionnaireResponseID = response.ID, // get response ID generated by the database (not from the mobile app)
                                            };
                                            db.QuestionnaireResponseBlobs.Add(responseBlob);
                                            db.SaveChanges(userName);
                                        }
                                    }
                                }
                            }

                            // Email questionnaire if exception
                            if (!submission.Passing)
                            {
                                // Send function uses request variables so need to pass context into "new" controller
                                // new QuestionnairesController().Send(submission.ID);
                                var controller = DependencyResolver.Current.GetService<QuestionnairesController>();
                                controller.ControllerContext = new ControllerContext(Request.RequestContext, controller);
                                controller.Send(submission.ID);
                            }
                        }
                        catch (Exception e)
                        {
                            // record the failure but continue processing
                            outcome.Success = false;
                            outcome.Message += e.Message;
                        }
                    }
                }
            }
        }

        private void SyncDispatchMessages(SyncDataInput incomingSD, ref Outcome outcome, string userName)
        {
            if (incomingSD.DispatchMessages != null)
            {
                System.Data.DataTable dtDispatchMessages = SyncHelper.PopulateDataTableFromList(incomingSD.DispatchMessages);
                // since UID is the key, but an ID (identity) column exists, remove it so 
                //it doesn't try to insert into an identity column (which will fail)
                dtDispatchMessages.Columns.Remove("ID");
                using (SSDB ssdb = new SSDB(false))
                {
                    foreach (System.Data.DataRow row in dtDispatchMessages.Rows)
                    {
                        try
                        {
                            object uid = SSDBSave.SaveDataRow(ssdb, row, "tblDispatchMessage", "UID"
                                , SSDBSave.UpdateMode.IgnoreConcurrencyExceptions | SSDBSave.UpdateMode.IncludeBlobFields
                                , userName);
                        }
                        catch (Exception e)
                        {
                            // record the failure but continue processing
                            outcome.Success = false;
                            outcome.Message += e.Message;
                        }
                    }
                }
            }
        }


        // mark the outgoing orders as DRIVER SYNCED
        private void MarkOrdersDriverSynced(OrderReadOnlyList orders)
        {
            if (orders == null) return;  //JAE 4/4/17 - Added to resolve sync error

            DataTable dtIDs = new DataTable();
            dtIDs.Columns.Add("ID", typeof(int));
            foreach (var order in orders)
            {
                dtIDs.Rows.Add(order.ID);
            }
            using (SSDB db = new SSDB())
            {
                using (SqlCommand cmd = db.BuildCommand("spMarkOrdersDriverSynced"))
                {
                    SqlParameter paramIDs = cmd.Parameters["@IDs"];
                    paramIDs.Value = dtIDs;
                    paramIDs.SqlDbType = SqlDbType.Structured;
                    paramIDs.TypeName = "dbo.IDTABLE";
                    cmd.ExecuteNonQuery();
                }
            }
        }

        [Authorize(Roles = "viewDriverAppSyncLog")]
        public ActionResult DriverLog(int? driverid, string count)
        {
            Driver driver = db.Drivers.Find(driverid);
            ViewBag.DriverID = new SelectList(db.Drivers.OrderBy(d => d.LastName).ThenBy(d => d.FirstName), "ID", "FullNameLF", driverid);
            ViewBag.Count = count;
            return View("Log", driver);
        }

        [Authorize(Roles = "viewDriverAppSyncLog")]
        public ActionResult OutgoingJSON(int id)
        {
            return getJson(id, "OutgoingJson");
        }
        [Authorize(Roles = "viewDriverAppSyncLog")]
        public ActionResult IncomingJSON(int id)
        {
            return getJson(id, "IncomingJson");
        }

        private ActionResult getJson(int id, string field = " OutgoingJson")
        {
            using (SSDB ssdb = new SSDB())
            {
                string json = ssdb.QuerySingleValue("SELECT " + field + " FROM tblDriverSyncLog WHERE ID = " + id).ToString();
                return new ContentResult { Content = json, ContentType = "application/json" };
            }
        }
    }

}