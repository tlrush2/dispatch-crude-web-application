﻿using System;
using System.Web.Mvc;

namespace DispatchCrude.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return RedirectToAction("Index", "LandingPage");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        #region MENU REDIRECTS BASED ON PERMISSIONS
        //MENU ITEM REDIRECTS
        //This region is for checking users' permission to the "first" 
        //tab in a tabbed menu page.  These redirects ensure that the user
        //will be sent to the first page they have access to.
        //
        // The naming convention is TopLevelMenuItem_SubMenuItem()

        //The ActionResult items below work with the Tab and Button sets in NavigationHelper to produce the tab and button structure underneath the main menu items

        //MENU ITEM: Dispatch
        public ActionResult Dispatch()
        {
            if (User.IsInRole("viewTruckOrders"))
            {
                return Redirect("/Site/Orders/Orders_Creation.aspx");
            }
            else if (User.IsInRole("viewGaugerOrders"))
            {
                return Redirect("/Site/Orders/GaugerOrders_Creation.aspx");
            }
            else if (User.IsInRole("viewDispatch"))
            {
                return Redirect("/Site/Orders/Orders_Dispatch.aspx");
            }
            else if (User.IsInRole("Dispatch3P"))
            {
                return Redirect("/Orders/Dispatch3P");
            }
            else if (User.IsInRole("viewDispatchBoard"))
            {
                return Redirect("/Orders/Dispatchboard");
            }
            else if (User.IsInRole("viewDispatchMap"))
            {
                return Redirect("/Orders/DispatchMap");
            }
            else if (User.IsInRole("viewOrders"))
            {
                return Redirect("/Orders/OrderList");
            }
            else if (User.IsInRole("viewOrderAudit"))
            {
                return Redirect("/Home/Audit");
            }
            else if (User.IsInRole("unauditOrders"))
            {
                return Redirect("/Home/Audit");
            }
            else if (User.IsInRole("viewOrderApproval"))
            {
                return Redirect("/Site/Orders/Orders_Approve.aspx");
            }
            else if (User.IsInRole("searchOrders"))
            {
                return Redirect("/Orders?Page=1&Sort=OrderNum&HideSearch=False");
            }
            else
            {
                //If we somehow make it this far, go to default homepage
                return RedirectToAction("Index");
            }
        }

        //MENU ITEM: Dispatch > Audit
        public ActionResult Audit()
        {
            if (User.IsInRole("viewOrderAudit"))
            {
                return Redirect("/Site/Orders/Orders_Audit.aspx");
            }
            else if (User.IsInRole("unauditOrders"))
            {
                return Redirect("/OrderUnaudit");
            }
            else
            {
                //If we somehow make it this far, go to default homepage
                return RedirectToAction("Index");
            }
        }

        //MENU ITEM: Assets > Resources > Equipment
        public ActionResult Equipment()
        {
            if (User.IsInRole("viewDriverEquipment"))
            {
                return Redirect("/DriverEquipment");
            }
            else if (User.IsInRole("viewDriverEquipmentManufacturers"))
            {
                return Redirect("/DriverEquipmentManufacturers");
            }
            else if (User.IsInRole("viewDriverAppHistory"))
            {
                return Redirect("/Site/TableMaint/DriverUpdate.aspx");
            }
            else
            {
                //If we somehow make it this far, go to default homepage
                return RedirectToAction("Index");
            }
        }

        //MENU ITEM: Configuration > Print Configuration
        public ActionResult Configuration_Printing()
        {
            if (User.IsInRole("viewDriverPrintConfig"))
            {
                return Redirect("/Site/PrintBM/DAHeaderImage.aspx");
            }
            else if (User.IsInRole("viewGaugerPrintConfig"))
            {
                return Redirect("/Site/PrintBM/GAHeaderImage.aspx");
            }
            else if (User.IsInRole("viewBOLFieldMaintenance"))
            {
                return Redirect("/BOLFields");
            }
            else if (User.IsInRole("viewTicketTypes"))
            {
                return Redirect("/TicketTypes");
            }
            else
            {
                //If we somehow make it this far, go to default homepage
                return RedirectToAction("Index");
            }            
        }

        //MENU ITEM: Help > Logs
        public ActionResult Help_Logs()
        {
            if (User.IsInRole("viewAppSyncErrors"))
            {
                return Redirect("/Site/Support/SyncErrors.aspx");
            }
            else if (User.IsInRole("viewDriverAppSyncLog"))
            {
                return Redirect("/DriverApp/DriverLog");
            }
            else if (User.IsInRole("viewGaugerAppSyncLog"))
            {
                return Redirect("/GaugerApp/GaugerLog");
            }
            else if (User.IsInRole("viewAppChangeHistory"))
            {
                return Redirect("/Site/Support/AppChanges.aspx");
            }
            else
            {
                //If we somehow make it this far, go to default homepage
                return RedirectToAction("Index");
            }
        }

        //MENU ITEM: Help > Tools
        public ActionResult Help_Tools()
        {
            if (User.IsInRole("viewRecalculateVolumes"))
            {
                return Redirect("/RecalculateVolumes");
            }
            else if (User.IsInRole("undeleteOrders"))
            {
                return Redirect("/OrderUndelete");
            }
            else
            {
                //If we somehow make it this far, go to default homepage
                return RedirectToAction("Index");
            }
        }

        //MENU ITEM: Business Rules
        public ActionResult BusinessRules()
        {
            if (User.IsInRole("manageCarrierRules"))
            {
                return Redirect("/Site/Compliance/CarrierRules.aspx");
            }
            else if (User.IsInRole("viewOrderRules"))
            {
                return Redirect("/Site/Orders/OrderRules.aspx");
            }
            else if (User.IsInRole("viewHOS"))
            {
                return Redirect("/HOSPolicies");
            }
            else if (User.IsInRole("viewContracts"))
            {
                return Redirect("/Contracts");
            }
            else if (User.IsInRole("viewRegionMaintenance"))
            {
                return Redirect("/Regions");
            }            
            else
            {
                //If we somehow make it this far, go to default homepage
                return RedirectToAction("Index");
            }
        }

        //MENU ITEM: Security
        public ActionResult Security()
        {
            if (User.IsInRole("viewUsers"))
            {
                return Redirect("/DCUsers");
            }
            else if (User.IsInRole("viewGroups"))
            {
                return Redirect("/DCGroups");
            }
            else
            {
                //If we somehow make it this far, go to default homepage
                return RedirectToAction("Index");
            }
        }

        //MENU ITEM: Data Exchange > Reports
        public ActionResult DataExchange_Reports()
        {
            if (User.IsInRole("viewReportCenter"))
            {
                return Redirect("/Site/Orders/Orders_Inquiry.aspx");
            }
            else if (User.IsInRole("viewEmailSubscription"))
            {
                return Redirect("/ReportEmailSubscription");
            }
            else if (User.IsInRole("viewDemurrageReport"))
            {
                return Redirect("/Orders/DemurrageReport");
            }
            else if (User.IsInRole("viewPrintBOL"))
            {
                return Redirect("/Site/Orders/Orders_BOLPrint.aspx");
            }
            else if (User.IsInRole("viewOrderSummary"))
            {
                return Redirect("/Site/Orders/Orders_SummaryReport.aspx");
            }
            else if (User.IsInRole("viewVolumeAnalysis"))
            {
                return Redirect("/Site/Orders/Orders_VolAnalysisReport.aspx");
            }
            else if (User.IsInRole("viewOrderMap"))
            {
                return Redirect("/Orders/Map");
            }
            else if (User.IsInRole("viewDriverMap"))
            {
                return Redirect("/Drivers/Map");
            }
            else
            {
                //If we somehow make it this far, go to default homepage
                return RedirectToAction("Index");
            }
        }

        //MENU ITEM: Data Exchange > Imports
        public ActionResult DataExchange_Imports()
        {
            if (User.IsInRole("viewImportCenter"))
            {
                return Redirect("/ImportCenter/Index");
            }
            else if (User.IsInRole("viewImportCenterGroups"))
            {
                return Redirect("/ImportCenterGroup");
            }
            else
            {
                //If we somehow make it this far, go to default homepage
                return RedirectToAction("Index");
            }
        }

        //MENU ITEM: Data Exchange > Imports
        public ActionResult DataExchange_CustomData()
        {
            if (User.IsInRole("viewImportCenterCustomDataFields"))
            {
                return Redirect("/ObjectField");
            }
            else
            {
                //If we somehow make it this far, go to default homepage
                return RedirectToAction("Index");
            }
        }

        //MENU ITEM: Data Exchange > Allocation
        public ActionResult DataExchange_Allocation()
        {
            if (User.IsInRole("viewAllocationDashboard"))
            {
                return Redirect("/Dashboard");
            }
            if (User.IsInRole("viewDestinationShipperAllocation"))
            {
                return Redirect("/Site/Allocation/DestinationShipperAllocations.aspx");
            }            
            else
            {
                //If we somehow make it this far, go to default homepage
                return RedirectToAction("Index");
            }
        }

        //MENU ITEM: Assets > Locations
        public ActionResult Assets_Locations()
        {
            if (User.IsInRole("viewOriginMaintenance"))
            {
                return Redirect("/Site/TableMaint/Origins.aspx");
            }
            if (User.IsInRole("viewOriginTypes"))
            {
                return Redirect("/OriginTypes");
            }
            if (User.IsInRole("viewOriginTanks"))
            {
                return Redirect("/OriginTanks");
            }
            if (User.IsInRole("viewOriginTankDefinitions"))
            {
                return Redirect("/OriginTankDefinitions");
            }
            if (User.IsInRole("viewProducerMaintenance"))
            {
                return Redirect("/Producers");
            }
            if (User.IsInRole("viewOperatorMaintenance"))
            {
                return Redirect("/Operators");
            }
            if (User.IsInRole("viewPumperMaintenance"))
            {
                return Redirect("/Site/TableMaint/Pumpers.aspx");
            }
            if (User.IsInRole("viewDestinationMaintenance"))
            {
                return Redirect("/Site/TableMaint/Destinations.aspx");
            }
            if (User.IsInRole("viewDestinationTypes"))
            {
                return Redirect("/DestinationTypes");
            }
            if (User.IsInRole("viewConsignees"))
            {
                return Redirect("/Consignees");
            }
            if (User.IsInRole("viewTerminals"))
            {
                return Redirect("/Terminals");
            }
            if (User.IsInRole("viewProducts"))
            {
                return Redirect("/Site/TableMaint/Products.aspx");
            }
            if (User.IsInRole("viewProductGroups"))
            {
                return Redirect("/ProductGroups");
            }
            if (User.IsInRole("viewUnitsOfMeasure"))
            {
                return Redirect("/Uoms");
            }
            else
            {
                //If we somehow make it this far, go to default homepage
                return RedirectToAction("Index");
            }
        }

        //MENU ITEM: Assets > Resources
        public ActionResult Assets_Resources()
        {
            if (User.IsInRole("viewDriverMaintenance"))
            {
                return Redirect("/Drivers");
            }
            if (User.IsInRole("viewDriverScheduling"))
            {
                return Redirect("/DriverScheduling");
            }
            if (User.IsInRole("viewDriverEquipment"))
            {
                return Redirect("/Home/Equipment");
            }
            if (User.IsInRole("viewDriverEquipmentManufacturers"))
            {
                return Redirect("/Home/Equipment");
            }
            if (User.IsInRole("viewDriverAppHistory"))
            {
                return Redirect("/Home/Equipment");
            }
            if (User.IsInRole("viewTrucks"))
            {
                return Redirect("/Site/TableMaint/Trucks.aspx");
            }
            if (User.IsInRole("viewTruckTypes"))
            {
                return Redirect("/TruckTypes");
            }
            if (User.IsInRole("viewTrailers"))
            {
                return Redirect("/Site/TableMaint/Trailers.aspx");
            }
            if (User.IsInRole("viewTrailerTypes"))
            {
                return Redirect("/TrailerTypes");
            }
            if (User.IsInRole("viewCarrierMaintenance"))
            {
                return Redirect("/Site/TableMaint/Carriers.aspx");
            }
            if (User.IsInRole("viewCarrierTypes"))
            {
                return Redirect("/Carriers/Types");
            }
            if (User.IsInRole("viewTruckMaintenance"))
            {
                return Redirect("/TruckMaintenance");
            }
            if (User.IsInRole("viewTrailerMaintenance"))
            {
                return Redirect("/TrailerMaintenance");
            }
            if (User.IsInRole("viewTruckTrailerMaintenanceTypes"))
            {
                return Redirect("/TruckTrailerMaintenanceTypes");
            }
            if (User.IsInRole("viewOdometerLog"))
            {
                return Redirect("/Site/DecisionSupport/OdometerLog.aspx");
            }
            if (User.IsInRole("viewGaugerMaintenance"))
            {
                return Redirect("/Site/TableMaint/Gaugers.aspx");
            }
            if (User.IsInRole("viewShippers"))
            {
                return Redirect("/Shippers");
            }
            else
            {
                //If we somehow make it this far, go to default homepage
                return RedirectToAction("Index");
            }
        }

        //MENU ITEM: Assets > Compliance
        public ActionResult Assets_Compliance()
        {
            if (User.IsInRole("viewComplianceReport"))
            {
                return Redirect("/Compliance");
            }
            if (User.IsInRole("viewCarrierCompliance"))
            {
                return Redirect("/CarrierCompliance");
            }
            if (User.IsInRole("viewCarrierComplianceTypes"))
            {
                return Redirect("/CarrierComplianceTypes");
            }
            if (User.IsInRole("viewDriverCompliance"))
            {
                return Redirect("/DriverCompliance");
            }
            if (User.IsInRole("viewDriverComplianceTypes"))
            {
                return Redirect("/DriverComplianceTypes");
            }
            if (User.IsInRole("viewTruckCompliance"))
            {
                return Redirect("/TruckCompliance");
            }
            if (User.IsInRole("viewTruckComplianceTypes"))
            {
                return Redirect("/TruckComplianceTypes");
            }
            if (User.IsInRole("viewTrailerCompliance"))
            {
                return Redirect("/TrailerCompliance");
            }
            if (User.IsInRole("viewTrailerComplianceTypes"))
            {
                return Redirect("/TrailerComplianceTypes");
            }            
            if (User.IsInRole("viewQuestionnaires"))
            {
                return Redirect("/Questionnaires");
            }
            if (User.IsInRole("viewHOS"))
            {
                return Redirect("/HOS/Daily");
            }
            else
            {
                //If we somehow make it this far, go to default homepage
                return RedirectToAction("Index");
            }
        }

        //MENU ITEM: Financials > Demurrage
        public ActionResult Financials_Demurrage()
        {
            if (User.IsInRole("viewReasonCodes"))
            {
                return Redirect("/OriginWaitReasons");
            }
            if (User.IsInRole("viewShipperOriginWaitRates"))
            {
                return Redirect("/Site/Financials/ShipperOriginWaitRates.aspx");
            }
            if (User.IsInRole("viewCarrierOriginWaitRates"))
            {
                return Redirect("/Site/Financials/CarrierOriginWaitRates.aspx");
            }
            if (User.IsInRole("viewDriverOriginWaitRates"))
            {
                return Redirect("/Site/Financials/DriverOriginWaitRates.aspx");
            }
            if (User.IsInRole("viewShipperDestinationWaitRates"))
            {
                return Redirect("/Site/Financials/ShipperDestinationWaitRates.aspx");
            }
            if (User.IsInRole("viewCarrierDestinationWaitRates"))
            {
                return Redirect("/Site/Financials/CarrierDestinationWaitRates.aspx");
            }
            if (User.IsInRole("viewDriverDestinationWaitRates"))
            {
                return Redirect("/Site/Financials/DriverDestinationWaitRates.aspx");
            }
            if (User.IsInRole("viewShipperWaitFeeParameters"))
            {
                return Redirect("/Site/Financials/ShipperWaitFeeParameters.aspx");
            }
            if (User.IsInRole("viewCarrierWaitFeeParameters"))
            {
                return Redirect("/Site/Financials/CarrierWaitFeeParameters.aspx");
            }
            else
            {
                //If we somehow make it this far, go to default homepage
                return RedirectToAction("Index");
            }
        }

        //MENU ITEM: Financials > Accessorial
        public ActionResult Financials_Accessorial()
        {            
            if (User.IsInRole("viewShipperAssessorialRates"))
            {
                return Redirect("/Site/Financials/ShipperAccessorialRates.aspx");
            }
            if (User.IsInRole("viewCarrierAssessorialRates"))
            {
                return Redirect("/Site/Financials/CarrierAccessorialRates.aspx");
            }
            if (User.IsInRole("viewDriverAssessorialRates"))
            {
                return Redirect("/Site/Financials/DriverAccessorialRates.aspx");
            }
            if (User.IsInRole("viewAssessorialRateTypes"))
            {
                return Redirect("/Site/Financials/AccessorialRateTypes.aspx");
            }
            if (User.IsInRole("viewReasonCodes"))
            {
                return Redirect("/OrderRejectReasons");
            }
            if (User.IsInRole("viewShipperOrderRejectRates"))
            {
                return Redirect("/Site/Financials/ShipperOrderRejectRates.aspx");
            }
            if (User.IsInRole("viewCarrierOrderRejectRates"))
            {
                return Redirect("/Site/Financials/CarrierOrderRejectRates.aspx");
            }
            if (User.IsInRole("viewDriverOrderRejectRates"))
            {
                return Redirect("/Site/Financials/DriverOrderRejectRates.aspx");
            }
            if (User.IsInRole("viewShipperFuelSurchargeRates"))
            {
                return Redirect("/Site/Financials/ShipperFuelSurchargeRates.aspx");
            }
            if (User.IsInRole("viewCarrierFuelSurchargeRates"))
            {
                return Redirect("/Site/Financials/CarrierFuelSurchargeRates.aspx");
            }
            if (User.IsInRole("viewDriverFuelSurchargeRates"))
            {
                return Redirect("/Site/Financials/DriverFuelSurchargeRates.aspx");
            }
            if (User.IsInRole("viewPADDFuelPrices"))
            {
                return Redirect("/Site/Financials/FuelPrices.aspx");
            }
            else
            {
                //If we somehow make it this far, go to default homepage
                return RedirectToAction("Index");
            }
        }

        //MENU ITEM: Financials > Route Rates
        public ActionResult Financials_RouteRates()
        {
            if (User.IsInRole("viewShipperRateSheets"))
            {
                return Redirect("/Site/Financials/ShipperRateSheets.aspx");
            }
            if (User.IsInRole("viewCarrierRateSheets"))
            {
                return Redirect("/Site/Financials/CarrierRateSheets.aspx");
            }
            if (User.IsInRole("viewDriverRateSheets"))
            {
                return Redirect("/Site/Financials/DriverRateSheets.aspx");
            }
            if (User.IsInRole("viewShipperRouteRates"))
            {
                return Redirect("/Site/Financials/ShipperRouteRates.aspx");
            }
            if (User.IsInRole("viewCarrierRouteRates"))
            {
                return Redirect("/Site/Financials/CarrierRouteRates.aspx");
            }
            if (User.IsInRole("viewDriverRouteRates"))
            {
                return Redirect("/Site/Financials/DriverRouteRates.aspx");
            }
            if (User.IsInRole("viewRouteMaintenance"))
            {
                return Redirect("/Routes");
            }
            else
            {
                //If we somehow make it this far, go to default homepage
                return RedirectToAction("Index");
            }
        }

        //MENU ITEM: Financials > Settlement
        public ActionResult Financials_Settlement()
        {
            if (User.IsInRole("viewShipperSettlement"))
            {
                return Redirect("/Site/Financials/ShipperInvoicing.aspx");
            }
            if (User.IsInRole("viewCarrierSettlement"))
            {
                return Redirect("/Site/Financials/CarrierInvoicing.aspx");
            }
            if (User.IsInRole("viewDriverSettlement"))
            {
                return Redirect("/Site/Financials/DriverInvoicing.aspx");
            }
            if (User.IsInRole("viewShipperSettlementBatches"))
            {
                return Redirect("/Site/Financials/ShipperBatches.aspx");
            }
            if (User.IsInRole("viewCarrierSettlementBatches"))
            {
                return Redirect("/Site/Financials/CarrierBatches.aspx");
            }
            if (User.IsInRole("viewDriverSettlementBatches"))
            {
                return Redirect("/Site/Financials/DriverBatches.aspx");
            }
            if (User.IsInRole("viewShipperSettlementUnits"))
            {
                return Redirect("/Site/Financials/ShipperSettlementFactor.aspx");
            }
            if (User.IsInRole("viewCarrierSettlementUnits"))
            {
                return Redirect("/Site/Financials/CarrierSettlementFactor.aspx");
            }
            if (User.IsInRole("viewShipperMinSettlementUnits"))
            {
                return Redirect("/Site/Financials/ShipperMinSettlementUnits.aspx");
            }
            if (User.IsInRole("viewCarrierMinSettlementUnits"))
            {
                return Redirect("/Site/Financials/CarrierMinSettlementUnits.aspx");
            }
            if (User.IsInRole("viewCarrierDriverSettlementGroups"))
            {
                return Redirect("/Site/Financials/DriverGroups.aspx");
            }
            if (User.IsInRole("viewDriverSettlementGroupAssignments"))
            {
                return Redirect("/Site/Financials/Drivers.aspx");
            }
            else
            {
                //If we somehow make it this far, go to default homepage
                return RedirectToAction("Index");
            }
        }

        //MENU ITEM: Financials > Commodities
        public ActionResult Financials_Commodities()
        {
            if (User.IsInRole("viewCommodityPricing"))
            {
                return Redirect("/CommodityPricing/Indexes");
            }
            if (User.IsInRole("viewProducerSettlement"))
            {
                return Redirect("/Site/Financials/ProducerInvoicing.aspx");
            }
            if (User.IsInRole("viewProducerSettlementBatches"))
            {
                return Redirect("/Site/Financials/ProducerBatches.aspx");
            }
            else
            {
                //If we somehow make it this far, go to default homepage
                return RedirectToAction("Index");
            }
        }
        #endregion
    }
}
