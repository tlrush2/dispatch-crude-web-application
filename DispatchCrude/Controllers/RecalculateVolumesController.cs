﻿using DispatchCrude.Models;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System;
using DispatchCrude.ViewModels;
using DispatchCrude.App_Code;
using System.Data.Entity;
using AlonsIT;

namespace DispatchCrude.Controllers
{
    [Authorize(Roles = "viewRecalculateVolumes")]
    public class RecalculateVolumesController : Controller
    {
        private DispatchCrudeDB db = new DispatchCrudeDB();

        //Only show/work with Delivered and Audited statuses on this page
        int[] allowedStatuses = new int[] { 3, 4 };

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        // GET:
        public ActionResult Index(int? OriginID = null
                                , int? DestinationID = null
                                , int? StatusID = null
                                , int? TicketTypeID = null
                                , int? DriverID = null
                                , int? ProducerID = null
                                , int? OrderNum = null
                                , DateTime? StartDate = null
                                , DateTime? EndDate = null
                                )
        {            

            //Get data for the dropdownlist filters            
            ViewBag.StatusID = new SelectList(db.OrderStatus
                                                    .Where(m => allowedStatuses.Contains(m.ID))
                                                    .OrderBy(m => m.Name)
                                                    , "ID", "Name", StatusID);

            ViewBag.OriginID = new SelectList(db.Origins.ToList()
                                                        .OrderByDescending(m => m.Active)
                                                        .ThenBy(m => m.Name)
                                                        , "ID", "NameWithStatus", OriginID);

            ViewBag.DestinationID = new SelectList(db.Destinations.ToList()
                                                        .OrderByDescending(m => m.Active)
                                                        .ThenBy(m => m.Name)
                                                        , "ID", "NameWithStatus", DestinationID);

            ViewBag.TicketTypeID = new SelectList(db.TicketTypes.ToList()
                                                        .OrderByDescending(m => m.Active)
                                                        .ThenBy(m => m.Name)
                                                        , "ID", "NameWithStatus", TicketTypeID);

            ViewBag.DriverID = new SelectList(db.Drivers.ToList()
                                                    .OrderByDescending(m => m.Active)
                                                    .ThenBy(m => m.FirstName)
                                                    , "ID", "FullName", DriverID);

            ViewBag.ProducerID = new SelectList(db.Producers.ToList()
                                                    .OrderByDescending(m => m.Active)
                                                    .ThenBy(m => m.Name)
                                                    , "ID", "NameWithStatus", ProducerID);

            //Get system setting value for recalculate tab info box
            TempData["systemsetting"] = "";
            using (SSDB ssdb = new SSDB())
            {
                TempData["systemsetting"] = DBHelper.ToString(ssdb.QuerySingleValue("SELECT Value FROM tblSetting WHERE ID = 35"));
            }


            return View();
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest request = null, int id = 0
                                , int? OriginID = null
                                , int? DestinationID = null
                                , int? StatusID = null
                                , int? TicketTypeID = null
                                , int? DriverID = null
                                , int? ProducerID = null
                                , int? OrderNum = null
                                , DateTime? StartDate = null
                                , DateTime? EndDate = null
                                )
        {
            return Read(request, null, id
                        , OriginID
                        , DestinationID
                        , StatusID
                        , TicketTypeID
                        , DriverID
                        , ProducerID
                        , OrderNum
                        , StartDate
                        , EndDate
                        );
        }

        private ContentResult Read([DataSourceRequest] DataSourceRequest request, ModelStateDictionary modelState, int id = 0
                                    , int? OriginID = null
                                    , int? DestinationID = null
                                    , int? StatusID = null
                                    , int? TicketTypeID = null
                                    , int? DriverID = null
                                    , int? ProducerID = null
                                    , int? OrderNum = null
                                    , DateTime? StartDate = null
                                    , DateTime? EndDate = null
                                    )
        {
            //set default date range limit for initial page load (start date)
            var startDate = (StartDate == null) ? DateTime.UtcNow.AddDays(-30) : StartDate;

            IEnumerable<RecalculateVolumesViewModel> data = db.Orders
                                                                .Where(m => m.ID == id || id == 0)
                                                                .Where(m => m.OrderDate >= startDate)
                                                                .Where(m => allowedStatuses.Contains(m.StatusID))                                                                
                                                                .Where(m => m.Rejected == false)
                                                    .Select(x => new RecalculateVolumesViewModel()
                                                    {
                                                        OrderId = x.ID                                                        
                                                        , Date = x.OrderDate
                                                        , OrderNumber = x.OrderNum
                                                        , GOV = x.OriginGrossUnits
                                                        , GSV = x.OriginGrossStdUnits
                                                        , NSV = x.OriginNetUnits
                                                        , OriginName = x.Origin.Name
                                                        , OriginID = x.OriginID
                                                        , DestinationName = x.Destination.Name
                                                        , DestinationID = x.DestinationID
                                                        , OrderStatus = x.Status.Name
                                                        , StatusID = x.StatusID
                                                        , TicketTypeName = x.TicketType.Name
                                                        , TicketTypeID = x.TicketTypeID
                                                        , DriverName = x.Driver.FirstName + " " + x.Driver.LastName
                                                        , DriverID = x.DriverID
                                                        , ProducerName = x.Producer.Name
                                                        , ProducerID = x.ProducerID
                                                    }
                                                    ).ToList();

            //Filters
            if (OriginID != null)
                data = data.Where(m => m.OriginID == OriginID);

            if (DestinationID != null)
                data = data.Where(m => m.DestinationID == DestinationID);

            if (StatusID != null)
                data = data.Where(m => m.StatusID == StatusID);

            if (TicketTypeID != null)
                data = data.Where(m => m.TicketTypeID == TicketTypeID);

            if (DriverID != null)
                data = data.Where(m => m.DriverID == DriverID);

            if (ProducerID != null)
                data = data.Where(m => m.ProducerID == ProducerID);

            if (OrderNum != null)
                data = data.Where(m => m.OrderNumber.Value == OrderNum);

            //Start date will always be filtered by either the default set above (startDate) or whatever is passed in.
            //End date needs to be filtered here (default is "Now").  EndDate+1 ensures "EndDate 23:59" is still
            //included if the dates ever become time sensitive on this page
            data = data.Where(m => m.Date < ((DateTime)EndDate).AddDays(1));                        

            var result = data.ToDataSourceResult(request, modelState);            

            return JsonStringResult.Create(data.ToDataSourceResult(request, modelState));
        }

        public ActionResult Filter(RecalculateVolumesViewModel rcv, [DataSourceRequest] DataSourceRequest request = null, int id = 0
                                    , DateTime? StartDate = null
                                    , DateTime? EndDate = null)
        {
            int? originId = rcv.OriginID;
            int? destId = rcv.DestinationID;
            int? statusId = rcv.StatusID;
            int? ticketTypeId = rcv.TicketTypeID;
            int? driverId = rcv.DriverID;
            int? producerId = rcv.ProducerID;            
            int? orderNum = rcv.OrderNum;            

            return RedirectToAction("Index", new { OriginID = originId
                                                    , DestinationID = destId
                                                    , StatusID = statusId
                                                    , TicketTypeID = ticketTypeId
                                                    , DriverId = driverId
                                                    , ProducerID = producerId
                                                    , OrderNum = orderNum
                                                    , StartDate
                                                    , EndDate
                                                    });
        }

        public ActionResult Recalculate(string orderIdCSV, RecalculateVolumesViewModel rcv, [DataSourceRequest] DataSourceRequest request = null, int id = 0
                                    , DateTime? StartDate = null
                                    , DateTime? EndDate = null)
        {
            int? originId = rcv.OriginID;
            int? destId = rcv.DestinationID;
            int? statusId = rcv.StatusID;
            int? ticketTypeId = rcv.TicketTypeID;
            int? driverId = rcv.DriverID;
            int? producerId = rcv.ProducerID;
            int? orderNum = rcv.OrderNum;
                        
            //Recalculate the specified orders (the tickets attached to the submitted order id's)
            using (SSDB ssdb = new SSDB())
            {
                ssdb.ExecuteSql("EXECUTE spRecalculateOrderTicketVolumes '{0}', '{1}'", orderIdCSV, User.Identity.Name);

                //Success message to return to user
                TempData["success"] = "Order volumes successfully recalculated.";
            }            

            return RedirectToAction("Index", new { OriginID = originId
                                                    , DestinationID = destId
                                                    , StatusID = statusId
                                                    , TicketTypeID = ticketTypeId
                                                    , DriverId = driverId
                                                    , ProducerID = producerId
                                                    , OrderNum = orderNum
                                                    , StartDate
                                                    , EndDate
                                                    });
        }
    }
}
