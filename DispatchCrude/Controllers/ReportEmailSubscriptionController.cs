﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using DispatchCrude.App_Code;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using DispatchCrude.Core;
using DispatchCrude.Extensions;
using DispatchCrude.Models;

namespace DispatchCrude.Controllers
{
    [Authorize(Roles = "viewEmailSubscription")]
    public class ReportEmailSubscriptionController : _DBControllerRead
    {
        //
        // GET: /ReportEmailSubscription/
        public ActionResult Index()
        {
            ViewBag.Title = "Report Email Subscriptions";
            return View();
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        override protected ContentResult Read([DataSourceRequest] DataSourceRequest request, ModelStateDictionary modelState, int id)
        {
            bool viewAll = UserSupport.IsInGroup("_DCAdministrator") || UserSupport.IsInGroup("_DCSystemManager") || UserSupport.IsInGroup("_DCSupport");
            var data = db.UserReportEmailSubscriptions
                    .Where(s => ((id == 0 || s.ID == id) && (s.UserName.ToUpper() == User.Identity.Name.ToUpper() || viewAll)))
                    .OrderBy(s => s.Name);
            return ToJsonResult(data, request, modelState, "CreateDateUTC", "LastChangeDateUTC", "ExecuteHourUTC");
        }

        // GET: return the Subscriptions currently defined for this user
        public ContentResult UserReports()
        {
            bool viewAll = UserSupport.IsInGroup("_DCAdministrator") || UserSupport.IsInGroup("_DCSystemManager") || UserSupport.IsInGroup("_DCSupport");
            return Content(App_Code.RestControllerHelper.GetJson(
                    objectName: "UserReportDefinition"
                    , where: viewAll ? null : string.Format("UserNames IS NULL OR ','+UserNames+',' LIKE '%,{0},%'", User.Identity.Name)
                    , orderBy: "Name"
                ), "application/json");
        }

        //
        // POST: /ReportEmailSubscription/Edit/5
        [HttpPost]
        [ValidateHeaderAntiForgeryToken]
        public ContentResult Update([DataSourceRequest] DataSourceRequest request, UserReportEmailSubscription subscription)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    db.AddOrUpdateSave(User.Identity.Name, subscription);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { subscription }.ToDataSourceResult(request, ModelState));
                }
            }

            return Read(request, ModelState, subscription.ID);
        }

        //
        // GET: /ReportEmailSubscription/Delete/5
        [HttpPost]
        [ValidateHeaderAntiForgeryToken]
        public ActionResult Delete([DataSourceRequest] DataSourceRequest request, UserReportEmailSubscription model)
        {
            return DeleteInt(request, model);
        }

    }
}
