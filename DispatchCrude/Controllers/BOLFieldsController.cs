﻿using System;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using DispatchCrude.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;

namespace DispatchCrude.Controllers
{
    [Authorize(Roles = "viewBOLFieldMaintenance")]
    public class BOLFieldsController : Controller
    {
        private DispatchCrudeDB db = new DispatchCrudeDB();

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        // GET: Read
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest request = null, int id = 0)
        {
            return Read(request, null, id);
        }

        private ContentResult Read([DataSourceRequest] DataSourceRequest request, ModelStateDictionary modelState, int id = 0)
        {
            // .ToList() is required due to filtering on "Active" field, which is NOT MAPPED
            var data = db.BOLFields.Where(m => m.ID == id || id == 0).ToList();
            var result = data.ToDataSourceResult(request, modelState);            
            return App_Code.JsonStringResult.Create(result);  
        }
        
        
        // POST: Update
        [HttpPost]
        [Authorize(Roles = "editBOLFieldMaintenance")]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, BOLField bolfield)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // Get the existing record                    
                    BOLField existing = db.BOLFields.Find(bolfield.ID);

                    // Specify all fields that will/can be updated by user input on the popup
                    db.CopyEntityValues(existing, bolfield);

                    db.SaveChanges(User.Identity.Name);  // Create & Modified dates & usernames will be taken care of by this line.
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(null, ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { bolfield }.ToDataSourceResult(request, ModelState));
                }
            }

            return Read(request, ModelState, bolfield.ID);
        }

        // Delete (Deactivate)
        //[Authorize(Roles = "deactivateBOLFieldMaintenance")]
        //public ActionResult Delete(BOLField bolfield)
        //{
        //    db.BOLFields.Attach(bolfield);
        //    db.BOLFields.Remove(bolfield);
        //    db.SaveChanges(User.Identity.Name);

        //    return null;
        //}
    }
}
