﻿using DispatchCrude.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DispatchCrude.Core;

namespace DispatchCrude.Controllers
{
    public class TrucksController : Controller
    {
        static public string SESSION_NAME_TRUCKIDLIST = "TruckIDList";

        private DispatchCrudeDB db = new DispatchCrudeDB();

        /**************************************************************************************************/
        /**  PAGE: ~/Trucks/getTrucks/?carrierid={carrierid}                                             **/
        /**  DESCRIPTION: JSON page that returns valid trucks for a given carrier                        **/
        /**************************************************************************************************/
        [Authorize(Roles = "viewTrucks")]
        public ActionResult getTrucks(int carrierid)
        {
            var result = (from t in db.Trucks
                          where t.CarrierID == carrierid
                          orderby t.IDNumber
                          select new
                          {
                              id = t.ID,
                              name = t.IDNumber
                          }).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);

        }

        /**************************************************************************************************/
        /**  PAGE: ~/Trucks/QRList/                                                                      **/
        /**  PARAMETERS: id - truck ID for generating a single QR (optional)                             **/
        /**              SESSION["TruckIDList"] - session variable of trucks from grid (optional)        **/
        /**  DESCRIPTION: Generate a list of QR codes for all trucks (filtered by my carrier)            **/
        /**************************************************************************************************/
        [Authorize(Roles = "viewTrucks")]
        public ActionResult QRList(int? id)
        {
            // Filter trucks that use my carrier and terminal
            int myCarrierID = Core.Converter.ToInt32(Profile.GetPropertyValue("CarrierID"));
            int myTerminalID = Converter.ToInt32(Profile.GetPropertyValue("TerminalID"));
            var trucks = db.Trucks.Where(t => (myCarrierID == -1 || t.CarrierID == myCarrierID) 
                                            && (myTerminalID <= 0 || t.TerminalID == myTerminalID || t.TerminalID == null));

            if (id != null)
            {
                // filter to single record
                trucks = trucks.Where(t => t.ID == id);
            }
            else if (this.ControllerContext.HttpContext.Session[SESSION_NAME_TRUCKIDLIST] != null)
            {
                // filter to trucks from previous grid (stored in session variable)
                List<int> truckIDs = (List<int>)this.ControllerContext.HttpContext.Session[SESSION_NAME_TRUCKIDLIST];
                trucks = trucks.Where(t => truckIDs.Contains(t.ID));
            }
            return View(trucks);
        }
    }
}