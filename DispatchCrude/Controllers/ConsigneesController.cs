﻿using System;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using DispatchCrude.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using DispatchCrude.Extensions;

namespace DispatchCrude.Controllers
{
    [Authorize(Roles = "viewConsignees")]
    public class ConsigneesController : Controller
    {
        private DispatchCrudeDB db = new DispatchCrudeDB();

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        // GET: Read
        public ActionResult Index(int? id)
        {
            //If a consignee has been specified as a starting filter in the url, get it and pass it to the view
            Consignee c = db.Consignees.Find(id);

            if (c != null)
            {
                ViewBag.ConsigneeName = c.Name;

                //We don't want the URL filter to stay after we've filtered the grid so this lets us know on the view to get rid of it.
                ViewBag.ClearUrl = true;
            }

            return View();
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest request = null, int id = 0)
        {
            return Read(request, null, id);
        }

        private ContentResult Read([DataSourceRequest] DataSourceRequest request, ModelStateDictionary modelState, int id = 0)
        {
            // .ToList() is required due to filtering on "Active" field, which is NOT MAPPED
            var data = db.Consignees.Where(m => m.ID == id || id == 0).ToList();
            var result = data.ToDataSourceResult(request, modelState);            
            return App_Code.JsonStringResult.Create(result);  
        }
        
        // POST: Create
        [HttpPost]
        [Authorize(Roles = "createConsignees")]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, Consignee consignee)
        {            
            if (ModelState.IsValid)
            {
                try
                {
                    // ensure all new Units of measure are marked "Active" (otherwise they will be created deleted)
                    consignee.Active = true;

                    //Ensure non-numeric characters from Masked textboxes do not make it to the database
                    consignee.ContactPhone = NumericUtils.RemoveNonNumeric(consignee.ContactPhone);

                    // Save new record to the database
                    db.Consignees.Add(consignee);
                    db.SaveChanges(User.Identity.Name);  // Create dates & usernames will all be taken care of by this line.
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(null, ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { consignee }.ToDataSourceResult(request, ModelState));
                }
            }
            
            return Read(request, ModelState, consignee.ID);
        }

        // POST: Update
        [HttpPost]
        [Authorize(Roles = "editConsignees")]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, Consignee consignee)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // Get the existing record
                    // This allows us to keep the existing file upload if one is not specified in an edit
                    Consignee existing = db.Consignees.Find(consignee.ID);

                    //Ensure non-numeric characters from Masked textboxes do not make it to the database
                    consignee.ContactPhone = NumericUtils.RemoveNonNumeric(consignee.ContactPhone);

                    // Specify all fields that will/can be updated by user input on the popup
                    db.CopyEntityValues(existing, consignee);

                    db.SaveChanges(User.Identity.Name);  // Create & Modified dates & usernames will be taken care of by this line.
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(null, ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { consignee }.ToDataSourceResult(request, ModelState));
                }
            }

            return Read(request, ModelState, consignee.ID);
        }

        // Delete (Deactivate)
        [Authorize(Roles = "deactivateConsignees")]
        public ActionResult Delete(Consignee consignee)
        {
            db.Consignees.Attach(consignee);
            db.Consignees.Remove(consignee);
            db.SaveChanges(User.Identity.Name);

            return null;
        }
    }
}
