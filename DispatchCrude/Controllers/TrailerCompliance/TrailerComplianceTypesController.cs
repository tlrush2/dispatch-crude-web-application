﻿using System;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using DispatchCrude.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;

namespace DispatchCrude.Controllers
{
    [Authorize(Roles = "viewTrailerComplianceTypes")]
    public class TrailerComplianceTypesController : _DBController
    {
        // GET: Product Groups
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest request = null, int id = 0)
        {
            return Read(request, null, id);
        }

        private ContentResult Read([DataSourceRequest] DataSourceRequest request, ModelStateDictionary modelState, int id = 0)
        {
            // .ToList() is required due to filtering on "Active" field, which is NOT MAPPED
            var data = db.TrailerComplianceTypes.Where(m => m.ID == id || id == 0).ToList();

            var result = data.ToDataSourceResult(request, modelState);

            return App_Code.JsonStringResult.Create(result);
        }

        // POST: Create Product Groups
        //[HttpPost]
        [Authorize(Roles = "createTrailerComplianceTypes")]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, TrailerComplianceType TrailerComplianceType)
        {            
            if (ModelState.IsValid)
            {
                try
                {
                    // ensure all new records are marked "Active" (otherwise they will be created deleted)
                    TrailerComplianceType.Active = true;

                    // Save new record to the database
                    db.TrailerComplianceTypes.Add(TrailerComplianceType);
                    db.SaveChanges(User.Identity.Name);  // Create dates & usernames will all be taken care of by this line.
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(null, ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { TrailerComplianceType }.ToDataSourceResult(request, ModelState));
                }
            }

            return Read(request, ModelState, TrailerComplianceType.ID);
        }

        // POST: Update Product Groups
        [HttpPost]
        [Authorize(Roles = "editTrailerComplianceTypes")]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, TrailerComplianceType TrailerComplianceType)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // Get the existing record
                    TrailerComplianceType existing = db.TrailerComplianceTypes.Find(TrailerComplianceType.ID);

                    // Specify all fields that will/can be updated by user input on the popup
                    db.CopyEntityValues(existing, TrailerComplianceType);

                    db.SaveChanges(User.Identity.Name);  // Create & Modified dates & usernames will be taken care of by this line.
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(null, ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { TrailerComplianceType }.ToDataSourceResult(request, ModelState));
                }
            }

            return Read(request, ModelState, TrailerComplianceType.ID);
        }


        // Delete (Deactivate)  Product Groups
        [Authorize(Roles = "deactivateTrailerComplianceTypes")]
        public ActionResult Delete(TrailerComplianceType TrailerComplianceType)
        {
            db.TrailerComplianceTypes.Attach(TrailerComplianceType);
            db.TrailerComplianceTypes.Remove(TrailerComplianceType);
            db.SaveChanges(User.Identity.Name);

            return null;
        }
        
        //Returns expiration length (if applicable) and whether or not a document is required (created for/used by Trailer compliance documents page popup)
        public JsonResult GetDocTypeInformation(int TypeID)
        {
            TrailerComplianceType type = db.TrailerComplianceTypes.Find(TypeID);

            return Json(new
            {
                ExpirationLength = type.ExpirationLength,
                DocumentRequired = type.RequiresDocument
            }, JsonRequestBehavior.AllowGet);
        }
    }
}
