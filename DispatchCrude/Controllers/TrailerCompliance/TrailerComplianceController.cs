﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DispatchCrude.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;

namespace DispatchCrude.Controllers
{
    [Authorize(Roles = "viewTrailerCompliance")]
    public class TrailerComplianceController : _DBControllerRead
    {
        // This session variable is required for Asyncronous file uploads to work with new records.
        // The file is uploaded here temporarily until the user actually clicks the save button.
        private const string SESSION_TRLCOMP_FILENAME = "TRLCOMP_FILENAME"
            , SESSION_TRLCOMP_FILE = "TRLCOMP_FILE";

        public ActionResult Index(int? TrailerID)
        {
            //If a trailer has been specified as a starting filter, get its name and pass it to the view
            Trailer t = db.Trailers.Find(TrailerID);

            if (t != null)
            {
                ViewBag.Trailer = t.IDNumber_FlagInactive;
                ViewBag.TrailerID = t.ID;
            }
            
            //We don't want the URL filter to stay after we've filtered the grid so this lets us know on the view to get rid of it.
            ViewBag.ClearUrl = true;
            return View();
        }

        /* normally this is the only method that is overridden (defined) in the inheriting controller classes */
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        override protected ContentResult Read([DataSourceRequest] DataSourceRequest request, ModelStateDictionary modelState, int id)
        {
            var data = db.TrailerCompliances.Where(m => m.ID == id || id == 0)
                            .Include(dc => dc.Trailer)
                            .Include(dc => dc.Trailer.Carrier)
                            .Include(dc => dc.Trailer.Carrier.CarrierType)
                            .Include(dc => dc.TrailerComplianceType);
            return ToJsonResult(data, request, modelState, "Document");
        }

        // POST: Create Product Groups
        [HttpPost]
        [Authorize(Roles = "createTrailerCompliance")]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, TrailerCompliance TrailerCompliance)
        {
            return Update(request, TrailerCompliance);
        }

        // POST: Update Product Groups
        [HttpPost]
        [Authorize(Roles = "editTrailerCompliance")]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, TrailerCompliance TrailerCompliance)
        {
            // push the previously provided document info into the model (if present we need to re-validate the model, done below)
            bool resetValidation = false;
            if (resetValidation = Session[SESSION_TRLCOMP_FILE] != null) TrailerCompliance.Document = Session[SESSION_TRLCOMP_FILE] as byte[];
            if (resetValidation = Session[SESSION_TRLCOMP_FILENAME] != null) TrailerCompliance.DocumentName = Session[SESSION_TRLCOMP_FILENAME].ToString();
            // reset the ModelState and re-validate (this is required if the document was required)
            if (resetValidation)
            {
                TrailerCompliance.Validated = false;
                ModelState.Clear();
                //TrailerCompliance.Validate(new System.ComponentModel.DataAnnotations.ValidationContext(TrailerCompliance, null, null));
            }

            if (ModelState.IsValid)
            {
                try
                {
                    // don't update the Document properties if not provided (will fail validation above if required and not provided)
                    string[] skipParameters = TrailerCompliance.Document != null ? null : new string[] { "Document", "DocumentName" };
                    db.AddOrUpdate(TrailerCompliance, null, skipParameters);

                    // Save new record to the database
                    db.SaveChanges(User.Identity.Name);  // Create dates & usernames will all be taken care of by this line.

                    //Clear Trailer compliance session variable after successful add.  Otherwise the next record added will automatically use this file if the user did not select one
                    Session[SESSION_TRLCOMP_FILE] = null;
                    Session[SESSION_TRLCOMP_FILENAME] = null;
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { TrailerCompliance }.ToDataSourceResult(request, ModelState));
                }
            }

            return Read(request, ModelState, TrailerCompliance.ID);
        }


        // Delete (Deactivate)  Product Groups
        [Authorize(Roles = "deactivateTrailerCompliance")]
        public ActionResult Delete([DataSourceRequest] DataSourceRequest request, TrailerCompliance TrailerCompliance)
        {
            return DeleteInt(request, TrailerCompliance);
        }

        private bool ValidateUpload(bool isNew, ModelStateDictionary modelState)
        {
            bool ret = Session[SESSION_TRLCOMP_FILENAME] != null && Session[SESSION_TRLCOMP_FILE] != null;
            //if (isNew && !ret)
                //modelState.AddModelError(null, "File must be provided");
            return ret;
        }


        [HttpPost]
        public ActionResult SaveDocument(IEnumerable<HttpPostedFileBase> document)
        {
            // The Name of the Upload component is "files"
            if (document != null)
            {
                foreach (var file in document)
                {
                    Session[SESSION_TRLCOMP_FILENAME] = Path.GetFileName(file.FileName);
                    Session[SESSION_TRLCOMP_FILE] = new BinaryReader(file.InputStream).ReadBytes(file.ContentLength);
                    // we only accept a single file so just break
                    break;
                }
            }
            // Return an empty string to signify success
            return Content("");
        }

        public ActionResult ClearDocument()
        {
            Session[SESSION_TRLCOMP_FILE] = null;
            Session[SESSION_TRLCOMP_FILENAME] = null;

            return Json(new { status="OK"}, JsonRequestBehavior.AllowGet);
        }

        // Download the Document
        public ActionResult Download(int id)
        {
            TrailerCompliance compliance = db.TrailerCompliances.Find(id);
            return File(compliance.Document, "application/octet-stream", compliance.DocumentName);
        }

    }
}
