﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DispatchCrude.Models;
using DispatchCrude.Core;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using AlonsIT;

namespace DispatchCrude.Controllers
{
    [Authorize(Roles = "viewContracts")]
    public class ContractsController : Controller
    {
        private DispatchCrudeDB db = new DispatchCrudeDB();

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest request = null, int id = 0)
        {
            return Read(request, null, id);
        }

        private ContentResult Read([DataSourceRequest] DataSourceRequest request, ModelStateDictionary modelState, int id)
        {
            // .ToList() is required due to filtering on "Active" field, which is NOT MAPPED
            var data = db.Contracts.Where(c => c.ID == id || id == 0)
                            .Include(c => c.Shipper)
                            .Include(c => c.ProductGroup)
                            .Include(c => c.Producer)
                            .Include(c => c.Uom)
                            .ToList();

            var result = data.ToDataSourceResult(request, modelState);
            
            return App_Code.JsonStringResult.Create(result);
        }

        [Authorize(Roles = "createContracts")]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, [Bind(Exclude="Shipper,ProductGroup,Producer,Uom")] Contract contract)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // ensure new records are marked "Active" (otherwise they will be created deleted)
                    contract.Active = true;

                    // Save new record to the database
                    db.Contracts.Add(contract);
                    db.SaveChanges(User.Identity.Name);  // Create dates & usernames will all be taken care of by this line.
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(null, ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { contract }.ToDataSourceResult(request, ModelState));
                }
            }


            return Read(request, ModelState, contract.ID);
        }

        [HttpPost]
        [Authorize(Roles = "editContracts")]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, [Bind(Exclude="Shipper,ProductGroup,Producer,Uom")] Contract contract) //Bind(Exclude) is here to prevent unwanted validation of these objects
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // Get the existing record
                    Contract existing = db.Contracts.Find(contract.ID);

                    // Specify all fields that will/can be updated by user input on the popup
                    db.CopyEntityValues(existing, contract);

                    db.SaveChanges(User.Identity.Name);  // Create & Modified dates & usernames will be taken care of by this line.
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(null, ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { contract }.ToDataSourceResult(request, ModelState));
                }
            }

            return Read(request, ModelState, contract.ID);
        }

        [Authorize(Roles = "deactivateContracts")]
        public ActionResult Delete([Bind(Exclude = "Shipper,ProductGroup,Producer,Uom")] Contract contract) //Bind(Exclude) is here to prevent unwanted validation of these objects
        {
            db.Contracts.Attach(contract);
            db.Contracts.Remove(contract);
            db.SaveChanges(User.Identity.Name);

            return null;
        }
    }
}
