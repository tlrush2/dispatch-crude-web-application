﻿using AlonsIT;
using DispatchCrude.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DispatchCrude.Controllers
{
    [Authorize]
    public class DestinationsController : Controller
    {
        private DispatchCrudeDB db = new DispatchCrudeDB();

        /**************************************************************************************************/
        /**  PAGE: ~/Destinations/getUOM/?destid={destid}                                                **/
        /**  DESCRIPTION: JSON page that returns the Unit of Measure for a given destination             **/
        /**************************************************************************************************/
        public ActionResult getUOM(int destid)
        {
            var result = (from d in db.Destinations
                          from u in db.Uoms
                          where d.ID == destid
                          && d.UomID == u.ID
                          select u).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /**************************************************************************************************/
        /**  PAGE: ~/Destinations/getConsignees/?destid={destid}                                         **/
        /**  DESCRIPTION: JSON page that returns the Consignees for a given destination                  **/
        /**************************************************************************************************/
        public ActionResult getConsignees(int destid)
        {
            var result = (from c in db.Consignees
                          from dc in db.DestinationConsignees
                          where destid == dc.DestinationID
                          && c.ID == dc.ConsigneeID
                          && c.DeleteDateUTC == null
                          select c).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }        
    }
}
