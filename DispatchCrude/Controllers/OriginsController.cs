﻿using AlonsIT;
using DispatchCrude.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DispatchCrude.Controllers
{
    [Authorize]
    public class OriginsController : Controller
    {
        //
        // GET: /Origins/
        private DispatchCrudeDB db = new DispatchCrudeDB();

        /**************************************************************************************************/
        /**  PAGE: ~/Origins/getUOM/?originid={originid}                                                 **/
        /**  DESCRIPTION: JSON page that returns the Unit of Measure for a given origin                  **/
        /**************************************************************************************************/
        public ActionResult getUOM(int originid)
        {
            var result = (from o in db.Origins
                          from u in db.Uoms
                          where o.ID == originid
                          && o.UomID == u.ID
                          select u).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /**************************************************************************************************/
        /**  PAGE: ~/Origins/getTanks/?originid={originid}                                               **/
        /**  DESCRIPTION: JSON page that returns valid tanks for a given origin                          **/
        /**************************************************************************************************/
        public ActionResult getTanks(int originid)
        {
            var result = (from t in db.OriginTanks
                          where t.OriginID == originid
                          && t.DeleteDateUTC == null
                          orderby t.TankNum
                          select new
                          {
                              id = t.ID,
                              name = t.TankNum + t.TankDescription
                          }).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /**************************************************************************************************/
        /**  PAGE: ~/Origins/getOrigins/                                                                 **/
        /**  DESCRIPTION: JSON page that returns valid origins                                           **/
        /**************************************************************************************************/
        public ActionResult getOrigins()
        {
            var result = (from o in db.Origins
                          where o.DeleteDateUTC == null
                          orderby o.Name
                          select new
                          {
                              id = o.ID,
                              name = o.LeaseName + o.Name
                          }).ToList(); 

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /**************************************************************************************************/
        /**  PAGE: ~/Origins/getProducts/?originid={originid}                                            **/
        /**  DESCRIPTION: JSON page that returns valid products for a given origin                       **/
        /**************************************************************************************************/
        public ActionResult getProducts(int originid)
        {
            using (SSDB ssdb = new SSDB())
            {
                string query = "SELECT ID as id, Name as name FROM tblProduct " +
                                "WHERE ID IN (SELECT Productid FROM tblOriginProducts WHERE originid = " + originid + ")" +
                                "ORDER BY Name";

                DataTable dt = ssdb.GetPopulatedDataTable(query);
                string json = JsonConvert.SerializeObject(dt);
                return this.Content(json, "application/json");
            }
        }

        /**************************************************************************************************/
        /**  PAGE: ~/Origins/getShippers/?originid={originid}                                            **/
        /**  DESCRIPTION: JSON page that returns valid shippers (customer) for a given origin            **/
        /**************************************************************************************************/
        public ActionResult getShippers(int originid)
        {
            using (SSDB ssdb = new SSDB())
            {
                string query = "SELECT C.ID as id, C.Name as name FROM tblCustomer C, tblOriginCustomers OC " +
                                "WHERE OC.CustomerID = C.ID AND OC.OriginID = " + originid + " ORDER BY Name";
                DataTable dt = ssdb.GetPopulatedDataTable(query);
                string json = JsonConvert.SerializeObject(dt);
                return this.Content(json, "application/json");
            }
        }

        /**************************************************************************************************/
        /**  PAGE: ~/Origins/getDestinations/?originid={originid}                                        **/
        /**  DESCRIPTION: JSON page that returns valid destinations for a given origin                   **/
        /**************************************************************************************************/
        public ActionResult getDestinations(int originid, int shipperid = 0, int? productid=0, int requireroute = 0)
        {
            // should probably use origincustomer and destinationcustomer tables (need model)
            using (SSDB ssdb = new SSDB())
            {
                string query = "SELECT DISTINCT ID, Name FROM dbo.fnRetrieveEligibleDestinations (" + originid + ", " + shipperid + ", " + productid + ", " + requireroute + ") ORDER BY Name";
                DataTable dt = ssdb.GetPopulatedDataTable(query);
                string json = JsonConvert.SerializeObject(dt);

                return this.Content(json, "application/json");
            }
        }

        
        /**************************************************************************************************/
        /**  DESCRIPTION: Datatable to get valid destinations for an origin.  Returns an empty list      **/
        /**         if fields are not set.  Could default to list all, but the intent is an origin       **/ 
        /**         should be selected first.  (Currently only routes defined by model)                  **/
        /**************************************************************************************************/
        public static SelectList getValidDestinations(int? originid, int? shipperid, int? productid, Destination selected = null)
        {
            // should probably use origincustomer and destinationcustomer tables (need model)
            int requireroute = 0;
            List<SelectListItem> list = new List<SelectListItem>();

            using (SSDB ssdb = new SSDB())
            {
                string query = "SELECT DISTINCT ID, Name FROM dbo.fnRetrieveEligibleDestinations (" + 
                                    ((originid == null) ? "NULL" : originid.ToString()) + ", " + 
                                    ((shipperid == null) ? "NULL" : shipperid.ToString()) + ", " + 
                                    ((productid == null) ? "NULL" : productid.ToString()) + ", " + 
                                    requireroute + ") ORDER BY Name";
                DataTable dt = ssdb.GetPopulatedDataTable(query);

                foreach (DataRow row in dt.Rows)
                {
                    list.Add(new SelectListItem()
                    {
                        Value = row["ID"].ToString(),
                        Text = row["Name"].ToString()
                    });
                }
            }

            // Add the current destination if it is a deleted record
            if (selected != null && selected.Deleted)
            {
                list.Add(new SelectListItem()
                {
                    Value = selected.ID.ToString(),
                    Text = selected.Name
                });
            }

            return new SelectList(list, "Value", "Text", selected.ID);
        }
    }
}
