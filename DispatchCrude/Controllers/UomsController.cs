﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DispatchCrude.Models;
using DispatchCrude.Core;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;

namespace DispatchCrude.Controllers
{
    [Authorize(Roles = "viewUnitsOfMeasure")]
    public class UomsController : Controller
    {
        private DispatchCrudeDB db = new DispatchCrudeDB();

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        // GET: Read
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest request = null, int id = 0)
        {
            return Read(request, null, id);
        }

        private ContentResult Read([DataSourceRequest] DataSourceRequest request, ModelStateDictionary modelState, int id = 0)
        {
            // .ToList() is required due to filtering on "Active" field, which is NOT MAPPED
            var data = db.Uoms.Where(u => u.ID == id || id == 0).Include(t => t.UomType).ToList();
            var result = data.ToDataSourceResult(request, modelState);            
            return App_Code.JsonStringResult.Create(result);  
        }
        
        // POST: Create
        [HttpPost]
        [Authorize(Roles = "createUnitsOfMeasure")]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, Uom uom)
        {            
            if (ModelState.IsValid)
            {
                try
                {
                    // ensure all new Units of measure are marked "Active" (otherwise they will be created deleted)
                    uom.Active = true;                    

                    // Save new record to the database
                    db.Uoms.Add(uom);
                    db.SaveChanges(User.Identity.Name);  // Create dates & usernames will all be taken care of by this line.
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(null, ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { uom }.ToDataSourceResult(request, ModelState));
                }
            }
            
            return Read(request, ModelState, uom.ID);
        }

        // POST: Update
        [HttpPost]
        [Authorize(Roles = "editUnitsOfMeasure")]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, Uom uom)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // Get the existing record
                    // This allows us to keep the existing file upload if one is not specified in an edit
                    Uom existing = db.Uoms.Find(uom.ID);

                    // Specify all fields that will/can be updated by user input on the popup
                    db.CopyEntityValues(existing, uom);

                    db.SaveChanges(User.Identity.Name);  // Create & Modified dates & usernames will be taken care of by this line.
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(null, ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { uom }.ToDataSourceResult(request, ModelState));
                }
            }

            return Read(request, ModelState, uom.ID);
        }

        // Delete (Deactivate)
        [Authorize(Roles = "deactivateUnitsOfMeasure")]
        public ActionResult Delete(Uom uom)
        {
            db.Uoms.Attach(uom);
            db.Uoms.Remove(uom);
            db.SaveChanges(User.Identity.Name);

            return null;
        }
    }
}
