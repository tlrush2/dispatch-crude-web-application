﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DispatchCrude.App_Code;
using AlonsIT;

namespace DispatchCrude.Controllers
{
    public class JsonDataController : Controller
    {
        //
        // POST: /DDData/
        [HttpPost]
        public ActionResult DDData(string table, bool includeAll = true
            , string allID = "-1", string allText = "(All)", string textCol = "Name", string textName = "name"
            , bool omitDeleted = false, string deletedSuffix = "Deleted:")
        {
            return new JsonStringResult(new JSON_Serializer().Serialize(
                GetData(table, includeAll, allID, allText, textCol, textName, omitDeleted, deletedSuffix)));
        }

        static private DataTable GetData(string table, bool includeAll
            , string allID, string allText, string textCol, string textName
            , bool omitDeleted, string deletedSuffix)
        {
            table = table.ToLower();
            if (table.Equals("shipper", StringComparison.CurrentCultureIgnoreCase)) table = "customer";
            if (table.StartsWith("SELECT", StringComparison.CurrentCultureIgnoreCase))
                table = string.Format("({0}) X", table);
            else if (!table.StartsWith("tbl") && !table.StartsWith("view")) 
                table = "tbl" + table;

            string whereClause = "";
            using (SSDB ssdb = new SSDB())
            {
                DataTable dtEmpty = ssdb.GetPopulatedDataTable("SELECT * FROM {0} WHERE 1=0;", (object)table);
                if (dtEmpty.Columns.Contains("DeleteDateUTC"))
                    whereClause = "DeleteDateUTC IS NULL";
                else if (dtEmpty.Columns.Contains("Active"))
                    whereClause = "Active=1";

                string sql = string.Format(
                    "SELECT id=ID, [{2}] = {1} FROM [{0}] {4} {3}"
/* 0 */             , table
/* 1 */             , (whereClause.Length == 0 ? textCol : string.Format("CASE WHEN {0} THEN '' ELSE {1} END + ' ' + [{2}]", whereClause, DBHelper.QuoteStr(deletedSuffix.Trim()), textName))
/* 2 */             , textName
/* 3 */             , (includeAll ? string.Format(" UNION SELECT {0}, {1}", allID, DBHelper.QuoteStr(allText)) : "")
/* 4 */             , (omitDeleted && whereClause.Length > 0 ? string.Format("WHERE {0}", whereClause) : ""));
                return ssdb.GetPopulatedDataTable(sql);
            }
        }

        static public string GetOptionListData(string table, int selID = 0, bool includeAll = true
            , string allID = "-1", string allText = "(All)", string textCol = "Name", string textName = "Name"
            , bool omitDeleted = false, string deletedSuffix = "Deleted: ")
        {
            string ret = string.Empty;
            if (table.Equals("shipper", StringComparison.CurrentCultureIgnoreCase)) table = "customer";
            if (!table.StartsWith("tbl")) table = "tbl" + table;

            System.Data.DataTable dt = GetData(table, includeAll, allID, allText, textCol, textName, omitDeleted, deletedSuffix);
            foreach (System.Data.DataRow dr in dt.Rows)
            {
                ret += string.Format("<option value='{0}' {2}>{1}</option>"
                    , dr["id"]
                    , dr["name"]
                    , DBHelper.ToInt32(dr["id"]) == selID ? "selected='selected'" : "");
            }
            return ret;
        }

        [Authorize(Roles = "Administrator, Management")]
        [HttpGet]
        public ActionResult Test()
        {
            return View("Test");
        }

    }
}
