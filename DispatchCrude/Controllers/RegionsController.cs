﻿using AlonsIT;
using DispatchCrude.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;

namespace DispatchCrude.Controllers
{
    [Authorize(Roles = "viewRegionMaintenance")]
    public class RegionsController : Controller
    {
        private DispatchCrudeDB db = new DispatchCrudeDB();

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        // GET: Product Groups
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest request = null, int id = 0)
        {
            return Read(request, null, id);
        }

        private ContentResult Read([DataSourceRequest] DataSourceRequest request, ModelStateDictionary modelState, int id = 0)
        {
            // .ToList() is required due to filtering on "Active" field, which is NOT MAPPED
            var data = db.Regions.Where(m => m.ID == id || id == 0).ToList();

            var result = data.ToDataSourceResult(request, modelState);

            return App_Code.JsonStringResult.Create(result);
        }

        // POST: Create Product Groups
        //[HttpPost]
        [Authorize(Roles = "createRegionMaintenance")]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, Region region)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // ensure all new product groups are marked "Active" (otherwise they will be created deleted)
                    region.Active = true;

                    // Save new record to the database
                    db.Regions.Add(region);
                    db.SaveChanges(User.Identity.Name);  // Create dates & usernames will all be taken care of by this line.
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(null, ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { region }.ToDataSourceResult(request, ModelState));
                }
            }


            return Read(request, ModelState, region.ID);
        }

        // POST: Update Product Groups
        [HttpPost]
        [Authorize(Roles = "editRegionMaintenance")]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, Region region)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // Get the existing record
                    Region existing = db.Regions.Find(region.ID);

                    // Specify all fields that will/can be updated by user input on the popup
                    db.CopyEntityValues(existing, region);

                    db.SaveChanges(User.Identity.Name);  // Create & Modified dates & usernames will be taken care of by this line.
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(null, ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { region }.ToDataSourceResult(request, ModelState));
                }
            }

            return Read(request, ModelState, region.ID);
        }


        // Delete (Deactivate)  Product Groups
        [Authorize(Roles = "deactivateRegionMaintenance")]
        public ActionResult Delete(Region region)
        {
            db.Regions.Attach(region);
            db.Regions.Remove(region);
            db.SaveChanges(User.Identity.Name);

            return null;
        }



        /**************************************************************************************************/
        /**  DESCRIPTION: Returns select list for popuplating regions in an MVC dropdown.  Since         **/
        /**       function is static, it assumes that any profile filters have already been applied.     **/
        /**************************************************************************************************/
        public static SelectList getRegionsSL(int? ShipperID, int? RegionID, int? selected = null)
        {
            List<SelectListItem> list = new List<SelectListItem>();

            foreach (Region region in getValidRegions(ShipperID, RegionID))
            {
                list.Add(new SelectListItem()
                {
                    Value = region.ID.ToString(),
                    Text = region.Name
                });
            }

            return new SelectList(list, "Value", "Text", selected);
        }


        /**************************************************************************************************/
        /**  PAGE: ~/Regions/getRegionsJSON/?ShipperID={shipperid}                                       **/
        /**  DESCRIPTION: JSON page that returns regions for a shipper                                   **/
        /**************************************************************************************************/
        public ActionResult getRegionsJSON(int? ShipperID)
        {
            // Filter dashboard based on my shipper/region
            int myShipperID = Core.Converter.ToInt32(Profile.GetPropertyValue("CustomerID"));
            int myRegionID = Core.Converter.ToInt32(Profile.GetPropertyValue("RegionID"));

            if (ShipperID == null)
            {
                // Show all but use profile to determine what "ALL" means
                ShipperID = myShipperID;
            }
            else if (ShipperID != myShipperID && myShipperID != -1)
            {
                return Json(null, JsonRequestBehavior.AllowGet); // not allowed to see destinations for this shipper
            }

            var result = getValidRegions(ShipperID, myRegionID)
                .Select(x => new { ID = x.ID, Name = x.Name })
                .OrderBy(x => x.Name)
                .ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /**************************************************************************************************/
        /**  DESCRIPTION: Retrieve valid regions based on the search criteria.  Used to populate         **/
        /**       MVC drop downs and for JSON requests.  Since Assumes that any profile filters have     **/
        /**       already been applied.                                                                  **/
        /**************************************************************************************************/
        private static List<Region> getValidRegions(int? ShipperID, int? RegionID)
        {
            using (DispatchCrudeDB db = new DispatchCrudeDB())
            {
                IQueryable<Region> regions = db.Regions;
                if (ShipperID != null && ShipperID != -1)
                {
                    IQueryable<Region> shipperRegions = db.Destinations.Where(d => d.Shippers.Any(s => s.ID == ShipperID))
                        .Select(d => d.Region).Distinct();
                    regions = regions.Where(r => shipperRegions.Any(sr => sr.ID == r.ID));
                }
                if (RegionID != null && RegionID != 0)
                {
                    regions = regions.Where(r => r.ID == RegionID);
                }
                return regions.OrderBy(r => r.Name).ToList();
            }
        }
    }
}
