﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DispatchCrude.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;

namespace DispatchCrude.Controllers
{
    // base class that has a protected DispatchCrudeDB instance (that is cleaned up appropriately)
    public class _DBController : Controller
    {
        protected DispatchCrudeDB db = new DispatchCrudeDB();

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }

    // base class that has a protected DispatchCrudeDB instance + plus some template Read() methods (mostly to assist common syntax)
    public class _DBControllerRead : _DBController
    {
        /* this implementation normally does not need to be overridden */
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        virtual public ContentResult Read([DataSourceRequest] DataSourceRequest request = null, int id = 0)
        {
            return Read(request, null, id);
        }

        // used internally by Create/Update/Delete controller methods (only)
        /* normally this is the only method that is overridden (defined) in the inheriting controller classes */
        virtual protected ContentResult Read([DataSourceRequest] DataSourceRequest request, ModelStateDictionary modelState, int id)
        {
            throw new NotImplementedException();
        }

        // basic method that takes a data object and optional skipProperties (as individual strings)
        protected ContentResult ToJsonResult(object data, params string[] skipProperties)
        {
            return App_Code.JsonStringResult.Create(data, skipProperties);
        }
        // method that takes an IQuerable<T> (such as the result of an EF.Entity.Select() call), and the objects available in a .Read() call + optional skipProperties string list
        protected ContentResult ToJsonResult<T>(IQueryable<T> data, DataSourceRequest request, ModelStateDictionary modelState, params string[] skipProperties)
        {
            // ToList() required due to [NotMapped] properties (such as [Active]) - why??
            return ToJsonResult(data.ToList(), request, modelState, skipProperties);
        }
        // method that takes a List<T> (such as a .ToList() result of an EF.Entity.Select() call with .ToList()), and the objects available in a .Read() call + optional skipProperties string list
        protected ContentResult ToJsonResult<T>(List<T> dataList, DataSourceRequest request, ModelStateDictionary modelState, params string[] skipProperties)
        {
            var result = modelState == null ? dataList.ToDataSourceResult(request) : dataList.ToDataSourceResult(request, modelState);
            return App_Code.JsonStringResult.Create(result, skipProperties);
        }

        protected ActionResult DeleteInt<T>([DataSourceRequest] DataSourceRequest request, T model, bool permanentDelete = false)
        {
            return DeleteInt(request, model, model.GetType(), permanentDelete);
        }

        // generic support Delete implementation (invoke this in a "regular" controller Delete method)
        protected ActionResult DeleteInt<T>([DataSourceRequest] DataSourceRequest request, T model, Type actualType, bool permanentDelete = false)
        {
            try
            {
                var dbSet = db.Set(actualType);
                var existing = dbSet.Find(AlonsIT.DBHelper.ToInt32(actualType.GetProperty("ID").GetValue(model)));
                // if the item being deleted is an AuditModelDeleteBase & the incoming _permanentDelete = TRUE, then record this before .Remove operation
                if (existing is AuditModelDeleteBase && !(existing as AuditModelDeleteBase).Deleted)
                    (existing as AuditModelDeleteBase).PermanentDelete = permanentDelete;
                dbSet.Remove(existing);
                // pass the ModelState to the SaveChanges call to so we get the Field + error values added to the ModelState (which is serialized below)
                db.SaveChanges(User.Identity.Name, null, ModelState);
            }
            catch (Exception)
            {
                return App_Code.JsonStringResult.Create(new[] { model }.ToDataSourceResult(request, ModelState));
            }

            return null;
        }

    }
}