﻿using DispatchCrude.Core;
using DispatchCrude.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DispatchCrude.ViewModels;
using System.Data;
using Syncfusion.XlsIO;
using HiQPdf;
using System.Web.UI;
using System.Text;
using Ionic.Zip;
using System.Data.Entity;

namespace DispatchCrude.Controllers
{
    [Authorize(Roles = "viewHOS")]
    public class HOSController : Controller
    {
        private DispatchCrudeDB db = new DispatchCrudeDB();


        public ActionResult Index()
        {
            return RedirectToAction("Daily");
        }


        /// --------------------------------------------------------------------------------------------
        /// <summary>
        ///     View one-day entry for HOS for a given driver
        /// </summary>
        /// <param name="fvm">Filter view model makes use of these fields for matching data
        ///     * StartDate - one-day (normalized to localtime), EndDate is always 24 hours later
        ///     * DriverID
        /// </param>
        /// --------------------------------------------------------------------------------------------
        public ActionResult Daily(FilterViewModel<Hos> fvm)
        {
            if (fvm.StartDate == null) // used for filtering
            {
                fvm.StartDate = DateTime.UtcNow;
            }
            DateTime StartDate = DateHelper.ToUTC(fvm.StartDate ?? DateTime.UtcNow, DispatchCrudeHelper.GetProfileTimeZone(System.Web.HttpContext.Current));
            DateTime EndDate = StartDate.AddDays(1);

            int myCarrierID = Converter.ToInt32(Profile.GetPropertyValue("CarrierID"));
            int myDriverID = Converter.ToInt32(Profile.GetPropertyValue("DriverID"));
            ViewBag.DriverID = new SelectList(db.Drivers.Where(d => d.DeleteDateUTC == null && (myCarrierID == -1 || (myDriverID == 0 && d.CarrierID == myCarrierID) || myDriverID == d.ID)).OrderBy(d => d.FirstName).ThenBy(d => d.LastName), "ID", "FullName", fvm.DriverID);

            ViewBag.LogDate = StartDate.Date;
            //ViewBag.showOriginal = fvm.CustomFlag;
            if (fvm.DriverID != null)
                ViewBag.Driver = db.Drivers.Find(fvm.DriverID).FullName;
            ViewBag.ID = fvm.DriverID;

            Hos previousHos = db.Hoss.Where(x => x.DriverID == fvm.DriverID && x.LogDateUTC <= StartDate && x.EventRecordStatusID == (int)ELDEventRecordStatus.Type.ACTIVE).OrderByDescending(x => x.LogDateUTC).FirstOrDefault();
            ViewBag.StartStatus = (previousHos != null) ? previousHos.HosDriverStatusID : (int)HosDriverStatus.Status.OFFDUTY;

            return View(fvm);
        }


        /// --------------------------------------------------------------------------------------------
        /// <summary>
        ///     Get HOS violations for the current or previous month
        /// </summary>
        /// <param name="fvm">Filter view model makes use of these fields for matching data
        ///     * DateFilter - Current or previous month
        ///     * CarrierID
        ///     * DriverID
        ///     * ShowAll - show all drivers or only those with violations
        /// </param>
        /// --------------------------------------------------------------------------------------------
        public ActionResult Violations(FilterViewModel<HOSViolationSummaryViewModel> fvm)
        {
            // Define the date filters available on this dashboard
            List<DateFilter> dateFilters = new List<DateFilter> {
                DateFilter.CURRENT_MONTH,
                DateFilter.PREVIOUS_MONTH
            };

            ViewBag.DateFilterName = new SelectList(dateFilters, "Name", "Name", fvm.DateFilterName);


            // Get the date range
            fvm.DateFilter = dateFilters.Where(df => df.Name == fvm.DateFilterName).FirstOrDefault();
            if (fvm.DateFilter == null)
                fvm.DateFilter = DateFilter.CURRENT_MONTH; // default for page load

            // Filter dashboard based on my carrier/driver
            int myCarrierID = Converter.ToInt32(Profile.GetPropertyValue("CarrierID"));
            int myDriverID = Converter.ToInt32(Profile.GetPropertyValue("DriverID"));

            ViewBag.CarrierID = new SelectList(db.Carriers.Where(c => (myCarrierID == -1 || c.ID == myCarrierID) && c.DeleteDateUTC == null).OrderBy(c => c.Name), "ID", "Name", fvm.CarrierID);
            ViewBag.DriverID = new SelectList(db.Drivers.Where(d => (myCarrierID == -1 && (fvm.CarrierID == null || d.CarrierID == fvm.CarrierID) || (myDriverID == -1 && d.CarrierID == myCarrierID) || myDriverID == d.ID) && d.DeleteDateUTC == null).OrderBy(d => d.FirstName).ThenBy(d => d.LastName), "ID", "FullName", fvm.DriverID);


            int? CarrierID = (myCarrierID != -1) ? myCarrierID : fvm.CarrierID; // hardcode carrier if profile not all
            int? DriverID = (myDriverID != 0) ? myDriverID : fvm.DriverID; // hardcode driver if profile not all

            //get most recent HOS Summary record
            string sql = string.Format(@"
                DECLARE @StartDate DATETIME = '{0}'
                DECLARE @EndDate DATETIME = '{1}'
                DECLARE @OnlyViol INT = {2}
                SELECT DriverID, Driver = FullName, CarrierID, Carrier,
                		UniqueViolations = SUM(CASE WHEN WeeklyOnDutyViolation = 1
										OR WeeklyDrivingViolation = 1
										OR OnDutyViolation = 1
										OR DrivingViolation = 1
										OR BreakViolation = 1 THEN 1 ELSE 0 END),
                        WeeklyOnDutyViolations = SUM(CASE WHEN WeeklyOnDutyViolation = 1 THEN 1 ELSE 0 END), 
                        WeeklyDrivingViolations = SUM(CASE WHEN WeeklyDrivingViolation = 1 THEN 1 ELSE 0 END), 
                        OnDutyViolations = SUM(CASE WHEN OnDutyViolation = 1 THEN 1 ELSE 0 END), 
                        DrivingViolations = SUM(CASE WHEN DrivingViolation = 1 THEN 1 ELSE 0 END),
                        BreakViolations = SUM(CASE WHEN BreakViolation = 1 THEN 1 ELSE 0 END)
                FROM viewDriverBase d
                CROSS APPLY fnHosViolationDetail(d.id, @StartDate, @EndDate) hos
                WHERE d.DeleteDateUTC IS NULL
                AND (@onlyviol = 0 OR WeeklyOnDutyViolation = 1 OR WeeklyDrivingViolation = 1 OR OnDutyViolation = 1 OR DrivingViolation = 1 OR BreakViolation = 1)
                GROUP BY DriverID, FullName, CarrierID, Carrier",
                DateHelper.ToUTC(fvm.DateFilter.StartDate, DispatchCrudeHelper.GetProfileTimeZone(System.Web.HttpContext.Current)),
                DateHelper.ToUTC(fvm.DateFilter.EndDate, DispatchCrudeHelper.GetProfileTimeZone(System.Web.HttpContext.Current)),
                (fvm.ShowAll ? 0 : 1));

            fvm.results = db.Database.SqlQuery<HOSViolationSummaryViewModel>(sql).AsQueryable().ToList();
            fvm.results = fvm.results.Where(d => (fvm.CarrierID == null || d.CarrierID == fvm.CarrierID) && (fvm.DriverID == null || d.DriverID == fvm.DriverID));
            return View(fvm);
        }

  
        /// --------------------------------------------------------------------------------------------
        /// <summary>
        ///     Get HOS violations for the current or previous month
        /// </summary>
        /// <param name="fvm">Filter view model makes use of these fields for matching data
        ///     * DateFilter - Current or previous month
        ///     * CarrierID
        ///     * DriverID
        ///     * ShowAll - show all personal conveyance totals or only overages
        /// </param>
        /// --------------------------------------------------------------------------------------------
        public ActionResult PersonalConveyance(FilterViewModel<HOSPersonalConveyanceViewModel> fvm)
        {
            // Define the date filters available on this dashboard
            List<DateFilter> dateFilters = new List<DateFilter> {
                DateFilter.CURRENT_MONTH,
                DateFilter.PREVIOUS_MONTH
            };

            ViewBag.DateFilterName = new SelectList(dateFilters, "Name", "Name", fvm.DateFilterName);


            // Get the date range
            fvm.DateFilter = dateFilters.Where(df => df.Name == fvm.DateFilterName).FirstOrDefault();
            if (fvm.DateFilter == null)
                fvm.DateFilter = DateFilter.CURRENT_MONTH; // default for page load

            // Filter dashboard based on my carrier/driver
            int myCarrierID = Converter.ToInt32(Profile.GetPropertyValue("CarrierID"));
            int myDriverID = Converter.ToInt32(Profile.GetPropertyValue("DriverID"));

            ViewBag.CarrierID = new SelectList(db.Carriers.Where(c => (myCarrierID == -1 || c.ID == myCarrierID) && c.DeleteDateUTC == null).OrderBy(c => c.Name), "ID", "Name", fvm.CarrierID);
            ViewBag.DriverID = new SelectList(db.Drivers.Where(d => (myCarrierID == -1 && (fvm.CarrierID == null || d.CarrierID == fvm.CarrierID) || (myDriverID == -1 && d.CarrierID == myCarrierID) || myDriverID == d.ID) && d.DeleteDateUTC == null).OrderBy(d => d.FirstName).ThenBy(d => d.LastName), "ID", "FullName", fvm.DriverID);


            int? CarrierID = (myCarrierID != -1) ? myCarrierID : fvm.CarrierID; // hardcode carrier if profile not all
            int? DriverID = (myDriverID != 0) ? myDriverID : fvm.DriverID; // hardcode driver if profile not all

            //get most recent HOS Summary record
            string sql = string.Format(@"
                DECLARE @StartDate DATETIME = '{0}'
                DECLARE @EndDate DATETIME = '{1}'
                DECLARE @ShowAll INT = {2}
                SELECT * FROM dbo.fnHosPersonalConveyance(@StartDate, @EndDate, @ShowAll)",
                DateHelper.ToUTC(fvm.DateFilter.StartDate, DispatchCrudeHelper.GetProfileTimeZone(System.Web.HttpContext.Current)),
                DateHelper.ToUTC(fvm.DateFilter.EndDate, DispatchCrudeHelper.GetProfileTimeZone(System.Web.HttpContext.Current)),
                (fvm.ShowAll ? 1 : 0));

            fvm.results = db.Database.SqlQuery<HOSPersonalConveyanceViewModel>(sql).AsQueryable().ToList();
            fvm.results = fvm.results.Where(d => (fvm.CarrierID == null || d.CarrierID == fvm.CarrierID) && (fvm.DriverID == null || d.DriverID == fvm.DriverID));
            return View(fvm);
        }

        /// --------------------------------------------------------------------------------------------
        /// <summary>
        ///     Partial view for displaying the HOS graph and data online
        /// </summary>
        /// <param name="DriverID">Database ID of the driver</param>
        /// <param name="logDate">Log date to view (normalized to local time)</param>
        /// <param name="ShowOriginal">Flag to show the current record or original data.  Default is false **NO LONGER USED**</param>
        /// <param name="ShowDetails">Flag to show or hide table of details with the graph.  Default is false</param>
        /// <param name="ShowNavigation">Flag to show or hide speed arrows at the top for quick navigation.  Default is true</param>
        /// <remarks>[Partial view]</remarks>
        /// --------------------------------------------------------------------------------------------
        public ActionResult eLog(int DriverID, DateTime? logDate, bool ShowOriginal = false, bool ShowDetails = false, bool ShowNavigation = true)
        {
            DateTime StartDate = DateHelper.ToUTC(logDate ?? DateTime.Today, DispatchCrudeHelper.GetProfileTimeZone(System.Web.HttpContext.Current));
            DateTime EndDate = new DateTime(Math.Min(StartDate.AddDays(1).Ticks, DateTime.UtcNow.Ticks));

            ViewBag.LogDate = StartDate.Date;
            ViewBag.DriverID = DriverID;
            ViewBag.ShowDetails = ShowDetails;
            ViewBag.ShowNavigation = ShowNavigation;
            ViewBag.ShowOriginal = ShowOriginal;

            Hos previousHos = db.Hoss.Where(x => x.DriverID == DriverID && x.LogDateUTC <= StartDate && x.EventRecordStatusID == (int)ELDEventRecordStatus.Type.ACTIVE).OrderByDescending(x => x.LogDateUTC).FirstOrDefault();
            ViewBag.StartStatus = (previousHos != null) ? previousHos.HosDriverStatusID : (int)HosDriverStatus.Status.OFFDUTY;
            ViewBag.StartDate = StartDate;
            ViewBag.EndDate = EndDate;


            var hos = db.Hoss.Where(x => x.DriverID == DriverID 
                                    && x.LogDateUTC >= StartDate && x.LogDateUTC < EndDate && x.EventRecordStatusID == (int)ELDEventRecordStatus.Type.ACTIVE);

            string sql2 = string.Format(@"
                SELECT *
                FROM fnHosViolationDetail({0}, '{1}', '{2}') hos
                WHERE (WeeklyOnDutyViolation = 1 OR WeeklyDrivingViolation = 1 OR OnDutyViolation = 1 OR DrivingViolation = 1 OR BreakViolation = 1)",
                    Converter.ToInt32(DriverID),
                    Converter.ToDateTime(StartDate),
                    Converter.ToDateTime(EndDate));

            var violations = db.Database.SqlQuery<HOSViolationDetailViewModel>(sql2).AsQueryable();
            ViewBag.Violations = violations;

            // Get most recent HOS signature
            // TODO: will eventually want to see if that signature is valid (may have signed but logged back in)
            sql2 = string.Format(@"
                    SELECT TOP 1 * FROM tblHOSSignature
                        WHERE HOSDate = '{0}' AND DriverID = {1}
                        ORDER BY CreateDateUTC DESC", 
                    StartDate.Date.ToString("yyyy-MM-dd"),
                    DriverID);

            ViewBag.HosSignature = db.Database.SqlQuery<HosSignature>(sql2).AsQueryable().FirstOrDefault();

            return PartialView("_eLog", hos.OrderBy(x => x.LogDateUTC).ToList());
        }

        
        /// --------------------------------------------------------------------------------------------
        /// <summary>
        ///     Suggests an edit for the HOS entry.  Record is not edited but a clone is created in 
        ///         "change requested" status.  The new record works with the mobile app and pushes 
        ///         down the change to be approved or rejected by the driver.
        /// </summary>
        /// <param name="id">Database ID of the HOS record</param>
        /// <remarks>~/HOS/Edit/?ID={id}</remarks>
        /// --------------------------------------------------------------------------------------------
        [Authorize(Roles="editHOS")]
        public ActionResult Edit(int id)
        {
            Hos hos = db.Hoss.Find(id);

            if (hos == null)
            {
                TempData["error"] = "That is not a valid HOS entry!";
                return RedirectToAction("Daily");
            }
            if (hasAccess(hos.Driver) == false)
            {
                TempData["error"] = "You do not have access to the driver in this HOS record!";
                return RedirectToAction("Daily");
            }
            if (hos.isEditable == false)
            {
                TempData["error"] = "You cannot edit this HOS record!"; // Per ELD Mandate 4.4.4.2.5
                return RedirectToAction("Daily");
            }
            if (hos.hasPendingChange)
            {
                TempData["error"] = "There is a pending change for this HOS record!";
                return RedirectToAction("Daily");
            }

            ViewBag.HosEventTypeID = new SelectList(db.HosEventTypes.ToList().Where(e => e.isEditable).OrderBy(e => e.Name), "ID", "Name", hos.HosEventTypeID);
            ViewBag.HosDriverStatusID = new SelectList(db.HosDriverStatuses.OrderBy(ds => ds.Name), "ID", "Name", hos.HosDriverStatusID);
            ViewBag.LogTimeZoneID = new SelectList(db.TimeZones, "ID", "Name", hos.LogTimeZoneID);
            ViewBag.Driver = hos.Driver.FullName;

            return View(hos);
        }

        [Authorize(Roles="editHOS")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Exclude="Driver,HosDriverStatus,HosEventType,LogTimeZone,CoDriver,EventRecordOrigin,EventRecordStatus,DiagnosticCode,EventDataCheckValue")] Hos updated)
        {
            if (ModelState.IsValid)
            {
                // Not a true edit, just a support suggest requiring driver approval
                Hos newHOS = new Hos();

                // Specify all fields that will/can be updated by user input on the popup
                newHOS.DriverID = updated.DriverID;
                newHOS.Lat = updated.Lat;
                newHOS.Lon = updated.Lon;
                newHOS.PersonalUse = updated.PersonalUse;
                newHOS.YardMove = updated.YardMove;
                newHOS.LogTimeZoneID = updated.LogTimeZoneID;
                newHOS.ELDUserName = updated.ELDUserName;
                newHOS.DriverTimeZoneOffset = updated.DriverTimeZoneOffset;
                newHOS.CoDriverID = updated.CoDriverID;
                newHOS.HosEventCode = updated.HosEventCode;
                newHOS.DistanceSinceLastValidCoordinates = updated.DistanceSinceLastValidCoordinates;
                newHOS.LocationDescription = updated.LocationDescription;
                newHOS.Geolocation = updated.Geolocation;
                newHOS.VIN = updated.VIN;
                newHOS.EngineHours = updated.EngineHours;
                newHOS.VehicleMiles = updated.VehicleMiles;
                newHOS.DataDiagnosticEventIndicator = updated.DataDiagnosticEventIndicator;
                newHOS.MalfunctionIndicator = updated.MalfunctionIndicator;
                newHOS.DiagnosticCodeID = updated.DiagnosticCodeID;
                newHOS.ELDIdentifier = updated.ELDIdentifier;
                newHOS.ELDAuthenticationValue = updated.ELDAuthenticationValue;

                newHOS.Notes = updated.Notes;
                newHOS.HosDriverStatusID = updated.HosDriverStatusID;
                newHOS.HosEventTypeID = updated.HosEventTypeID;
                newHOS.LogDateUTC = updated.LogDateUTC;
                newHOS.EventDataCheckValue = newHOS.getEventCode();


                newHOS.HosParentUID = updated.UID;
                newHOS.EventRecordOriginID = (int)ELDEventRecordOrigin.Type.SUPPORT_OTHER;
                newHOS.EventRecordStatusID = (int)ELDEventRecordStatus.Type.INACTIVE_CHANGE_REQUESTED;


                DateTime now = DateTime.UtcNow;
                newHOS.CreateDateUTC = now;
                newHOS.CreatedByUser = User.Identity.Name;

/*
                // Auto approve if own order
                int myDriverID = Converter.ToInt32(Profile.GetPropertyValue("DriverID"));
                if (updated.DriverID == myDriverID)
                {
                    newHOS.EventRecordOriginID = (int)ELDEventRecordOrigin.Type.DRIVER;
                    newHOS.EventRecordStatusID = (int)ELDEventRecordStatus.Type.ACTIVE;

                    // reset original record
                    Hos existingHOS = db.Hoss.Find(updated.ID);
                    existingHOS.EventRecordStatusID = (int)ELDEventRecordStatus.Type.INACTIVE_CHANGED;
                    existingHOS.LastChangedByUser = User.Identity.Name;
                    existingHOS.LastChangeDateUTC = now;
                }
*/
                db.Hoss.Add(newHOS);
                db.SaveChanges(User.Identity.Name);

                TempData["success"] = "Hours of Service change request was submitted successfully!";
                return RedirectToAction("Daily", new { driverid = updated.DriverID, startdate=updated.LogDate.Date });
            }

            //If there was a model error make sure the propose button does not get deactivated when we return due to the form passing a zero here by default
            //updated.EventRecordStatusID = (int)ELDEventRecordStatus.Type.ACTIVE;

            ViewBag.HosEventTypeID = new SelectList(db.HosEventTypes.OrderBy(s => s.Name), "ID", "Name", updated.HosEventTypeID);
            ViewBag.HosDriverStatusID = new SelectList(db.HosDriverStatuses.OrderBy(ds => ds.Name), "ID", "Name", updated.HosDriverStatusID);
            ViewBag.LogTimeZoneID = new SelectList(db.TimeZones, "ID", "Name", updated.LogTimeZoneID);
            //ViewBag.Driver = updated.Driver.FullName;

            return View(updated);
        }


        /// --------------------------------------------------------------------------------------------
        /// <summary>
        ///     Exports all hours of service (HOS) entries for a given driver for a give date range 
        ///         as an Excel download.
        /// </summary>
        /// <param name="fvm">Filter view model makes use of these fields for matching data
        ///     * DriverID
        ///     * StartDate
        ///     * EndDate
        /// </param>
        /// <remarks>~/HOS/Export/?DriverID={driverID}&startdate={startdate}&enddate={enddate}</remarks>
        /// --------------------------------------------------------------------------------------------
        public ActionResult Export(FilterViewModel<Hos> fvm)
        {
            if (fvm.StartDate == null) fvm.StartDate = DateTime.Today.AddDays(-1);
            if (fvm.EndDate == null) fvm.EndDate = DateTime.Today.AddDays(-1);

            Driver driver = db.Drivers.Find(fvm.DriverID);
            if (driver == null)
            {
                TempData["error"] = "Please select a valid driver!";
                return RedirectToAction("Daily", fvm);
            }
            if (fvm.StartDate == null || fvm.EndDate == null)
            {
                TempData["error"] = "Please select a valid date range!";
                return RedirectToAction("Daily", fvm);
            }
            if (hasAccess(driver) == false)
            {
                // check driver? can see others' submissions
                TempData["error"] = "You do not have access to that driver!";
                return RedirectToAction("Daily", fvm);
            }
            return File(ExportToExcel(fvm.DriverID.Value, fvm.StartDateTime.Value, fvm.EndDateTime.Value),
                    "Application/excel", string.Format("{0}_HOS_Logs.xlsx", driver.FullName));
        }

        /// --------------------------------------------------------------------------------------------
        /// <summary>
        ///     Called by the export page, this function actually creates the excel file.
        /// </summary>
        /// <param name="driverID">Database ID of the driver</param>
        /// <param name="startdate">The starting date range to query</param>
        /// <param name="enddate">The ending date range to query</param>
        /// --------------------------------------------------------------------------------------------
        private Stream ExportToExcel(int driverID, DateTime startdate, DateTime enddate)
        {
            MemoryStream ret = null;

            using (AlonsIT.SSDB ssdb = new AlonsIT.SSDB())
            {

                // Get HOS summary info (aggregate hours)
                DataTable dtSummary = ssdb.GetPopulatedDataTable(
                           @"SELECT hds.Name, s.TotalHours AS 'Total Hours'
                                FROM tblHosDriverStatus hds
                                LEFT JOIN (
                                        SELECT SUM(TotalHours) AS TotalHours, HosDriverStatusID 
                                            FROM dbo.fnHosSummary({0}, '{1}', '{2}')
                                            GROUP BY HosDriverStatusID
                                ) s ON hds.ID = s.HosDriverStatusID", 
                            driverID, startdate, enddate);

                // Get HOS line entries
                DataTable dtHOS = ssdb.GetPopulatedDataTable(
                           @"SELECT d.LastName, d.FirstName, hds.Name AS 'Driver Status', hs.Name AS 'HOS Status',
                                    dbo.fnUTC_to_Local(LogDateUTC, 1, 1) AS 'Log Date (CST)',
                                    dbo.fnUTC_to_Local(LogDateUTC, h.LogTimeZoneID, 1) AS 'Log Date (Driver)',
                                    h.Lat, h.Lon, h.LocationDescription AS 'Nearest City',
                                    h.PersonalUse as 'Personal Use', h.YardMove AS 'Yard Move',
                                    h.Notes, 
                                    h.ApprovedByUser AS 'Approved By', dbo.fnUTC_to_Local(h.ApproveDateUTC, 1, 1) AS 'Approve Date (CST)',
                                    h.CreatedByUser AS 'Created By', dbo.fnUTC_to_Local(h.CreateDateUTC, 1, 1) AS 'Create Date (CST)',
                                    h.LastChangedByUser AS 'Last Changed By', dbo.fnUTC_to_Local(h.LastChangeDateUTC, 1, 1) AS 'Last Change Date (CST)',
                                    h.DeletedByUser AS 'Deleted By', dbo.fnUTC_to_Local(h.DeleteDateUTC, 1, 1) AS 'Delete Date (CST)'
                               FROM tblHOS h
                          LEFT JOIN tblDriver d ON d.id = h.driverid
                          LEFT JOIN tblHosDriverStatus hds ON hds.ID = h.HOSDriverStatusID 
                          LEFT JOIN tblHosStatus hs ON hs.ID = h.HOSStatusID
                              WHERE DriverID = {0}
                                AND LogDateUTC BETWEEN '{1}' AND '{2}'
                                AND HosEventRecordStatusID = {3}
                           ORDER BY LogDateUTC",
                                driverID, startdate, enddate, ELDEventRecordStatus.Type.ACTIVE);

                DataTable dtHOSHistory = ssdb.GetPopulatedDataTable(
                           @"SELECT ha.id,
                                    dbo.fnUTC_to_Local(ha.DBAuditDate, 1, 1) AS 'Audit/Change Date (CST)',
                                    d.LastName, d.FirstName, hds.Name AS 'Driver Status', hs.Name AS 'HOS Status',
                                    dbo.fnUTC_to_Local(LogDateUTC, 1, 1) AS 'Log Date (CST)',
                                    dbo.fnUTC_to_Local(LogDateUTC, ha.LogTimeZoneID, 1) AS 'Log Date (Driver)',
                                    ha.Lat, ha.Lon, ha.LocationDescription AS 'Nearest City',
                                    ha.PersonalUse as 'Personal Use', ha.YardMove AS 'Yard Move',
                                    ha.Notes, 
                                    ha.ApprovedByUser AS 'Approved By', dbo.fnUTC_to_Local(ha.ApproveDateUTC, 1, 1) AS 'Approve Date (CST)'
                               FROM tblHOS ha
                          LEFT JOIN tblDriver d ON d.id = ha.driverid
                          LEFT JOIN tblHosDriverStatus hds ON hds.ID = ha.HOSDriverStatusID 
                          LEFT JOIN tblHosStatus hs ON hs.ID = ha.HOSStatusID
                              WHERE DriverID = {0}
                                AND LogDateUTC BETWEEN '{1}' AND '{2}'
                                AND HosEventRecordStatusID = {3}
                           ORDER BY LogDateUTC",
                                driverID, startdate, enddate, ELDEventRecordStatus.Type.INACTIVE_CHANGED);

                // Grids populated, begin export
                using (ExcelEngine excelEngine = new ExcelEngine())
                {
                    IApplication excelApp = null;
                    IWorkbook wb = null;
                    IWorksheet ws = null;
                    int sheetID = 0;
                    try
                    {
                        excelApp = excelEngine.Excel;
                        wb = excelApp.Workbooks.Add(ExcelVersion.Excel2010);

                        // Create summary tab
                        ws = wb.Worksheets[sheetID++];
                        ws.Name = "Summary";
                        AddDataTableToWorksheet(ws, dtSummary, 1, 1, true);
                        ws.Columns[1].NumberFormat = "0.00"; // Format total hours
                        ws.UsedRange.AutofitColumns();

                        // Create HOS tab
                        ws = wb.Worksheets[sheetID++];
                        ws.Name = "Hours of Service";
                        AddDataTableToWorksheet(ws, dtHOS, 1, 1, true);
                        ws.Columns[4].NumberFormat = "M/d/yyyy h:mm:ss AM/PM"; // Format log date
                        ws.Columns[5].NumberFormat = "M/d/yyyy h:mm:ss AM/PM"; // Format driver log date
                        ws.Columns[13].NumberFormat = "M/d/yyyy h:mm:ss AM/PM"; // Format approve date
                        ws.Columns[15].NumberFormat = "M/d/yyyy h:mm:ss AM/PM"; // Format create date
                        ws.Columns[17].NumberFormat = "M/d/yyyy h:mm:ss AM/PM"; // Format last change date
                        ws.Columns[19].NumberFormat = "M/d/yyyy h:mm:ss AM/PM"; // Format delete date
                        ws.UsedRange.AutofitColumns();

                        // Create HOS history tab
                        ws = wb.Worksheets[sheetID++];
                        ws.Name = "History";
                        AddDataTableToWorksheet(ws, dtHOSHistory, 1, 1, true);
                        ws.Columns[1].NumberFormat = "M/d/yyyy h:mm:ss AM/PM"; // Format audit date
                        ws.Columns[6].NumberFormat = "M/d/yyyy h:mm:ss AM/PM"; // Format log date
                        ws.Columns[7].NumberFormat = "M/d/yyyy h:mm:ss AM/PM"; // Format driver log date
                        ws.Columns[15].NumberFormat = "M/d/yyyy h:mm:ss AM/PM"; // Format approve date
                        ws.UsedRange.AutofitColumns();
                    }
                    catch (Exception)
                    {
                        // TODO: handle these errors
                    }
                    ret = new MemoryStream();
                    wb.SaveAs(ret, ExcelSaveType.SaveAsXLS);
                    wb.Close();
                }
            }
            ret.Position = 0;
            return ret;
        }

        /// --------------------------------------------------------------------------------------------
        /// <summary>
        ///     Inserts DataTable into a worksheet looping through columns and rows
        /// </summary>
        /// <param name="sheet">Worksheet to add data</param>
        /// <param name="dt">Data table containing the content of the spreadsheet</param>
        /// <param name="startRow">Start row if offset is used.  Default is 1 (first row)</param>
        /// <param name="startCol">Start column if offset is used.  Default is 1 (first column)</param>
        /// <param name="addHeader">Flag whether to add a header row using the column caption.  Default is true</param>
        /// --------------------------------------------------------------------------------------------
        private void AddDataTableToWorksheet(IWorksheet sheet, DataTable dt, int startRow = 1, int startCol = 1, bool addHeader = true)
        {
            if (addHeader)
            {
                int col = startCol;
                foreach (DataColumn dc in dt.Columns)
                {
                    sheet.Range[startRow, col++].Value = dc.Caption;
                }
                startRow++;
            }
            foreach (DataRow dr in dt.Rows)
            {
                int col = startCol;
                foreach (DataColumn dc in dt.Columns)
                {
                    sheet.Range[startRow, col++].Value = dr[dc].ToString();
                }
                startRow++;
            }
        }


        /// --------------------------------------------------------------------------------------------
        /// <summary>
        ///     Checks the user profile to see confirm if a user has access to the driver and the 
        ///         corresponding HOS entry
        /// </summary>
        /// <param name="Driver">Driver to check</param>
        /// --------------------------------------------------------------------------------------------
        private bool hasAccess(Driver driver)
        {
            // use profile to determine if a user can work with this driver
            int myCarrierID = Converter.ToInt32(Profile.GetPropertyValue("CarrierID"));
            int myDriverID = Converter.ToInt32(Profile.GetPropertyValue("DriverID"));
            return (myCarrierID == -1) || (myCarrierID == driver.CarrierID && myDriverID == 0) || (myDriverID == driver.ID);
        }



        /// --------------------------------------------------------------------------------------------
        /// <summary>
        ///     Used for print (PDF export) of HOS Daily Log, but functions as a preview
        /// </summary>
        /// <param name="DriverID">Database ID of the driver</param>
        /// <param name="Date">Log date to view (normalized to local time)</param>
        /// <remarks>~/HOS/PreviewLog/?DriverID={driverid}&Date={date}</remarks>
        /// <returns>HTML preview of the HOS Daily Log</returns>
        /// --------------------------------------------------------------------------------------------
        [Authorize(Roles = "viewPrintBOL")]
        public string PreviewLog(int DriverID, DateTime? Date)
        {
            Driver driver = db.Drivers.Where(d => d.ID == DriverID)
                .Include(d => d.Carrier)
                .Include(d => d.Truck)
                .Include(d => d.Trailer)
                .Include(d => d.Trailer2).First();
            if (driver == null)
            {
                return "That is not a valid driver!";
            }
            if (hasAccess(driver) == false)
            {
                return "You are not allowed to view that driver!";
            }

            HOSReportViewModel report = getReport(driver, Date ?? DateTime.Today);

            Hos previousHos = db.Hoss.Where(x => x.DriverID == DriverID && x.LogDateUTC <= Date && x.EventRecordStatusID == (int)ELDEventRecordStatus.Type.ACTIVE).OrderByDescending(x => x.LogDateUTC).FirstOrDefault();
            ViewBag.StartStatus = (previousHos != null) ? previousHos.HosDriverStatusID : (int)HosDriverStatus.Status.OFFDUTY;

            return ControllerExtensions.RenderViewToString(this, "Log", report);
        }


        /// --------------------------------------------------------------------------------------------
        /// <summary>
        ///     Return an HOS Daily Log report data in a view model
        /// </summary>
        /// <param name="Driver">Driver record</param>
        /// <param name="Date">Date of report</param>
        /// <returns>HOS Report view model</returns>
        /// --------------------------------------------------------------------------------------------
        private HOSReportViewModel getReport(Driver Driver, DateTime Date)
        {
            HOSReportViewModel report = new HOSReportViewModel(Driver, Date);

            // Get HOS policy
            var PolicyName = CarrierRuleHelper.GetRuleValue(DateTime.Now, CarrierRuleHelper.Type.HOS_POLICY, Driver.ID, Driver.CarrierID, Driver.TerminalID, Driver.OperatingStateID, Driver.RegionID);
            if (PolicyName != null)
                report.HosPolicy = db.HosPolicies.Where(p => p.Name == PolicyName.ToString()).FirstOrDefault();
            else
                report.HosPolicy = db.HosPolicies.Find(HosPolicy.DEFAULT_HOS_POLICY);

            // Get base HOS information
            string sql = string.Format(@"
                SELECT * FROM fnHosSummary({0}, '{1}', '{2}') 
                    ORDER BY [LogDateUTC]",
                    Converter.ToInt32(Driver.ID),
                    Converter.ToDateTime(report.StartDate),
                    Converter.ToDateTime(report.EndDate));
            var hoss = db.Database.SqlQuery<HOSSummary>(sql).AsQueryable();
            report.Hoss = hoss;

            // Get HOS violations
            sql = string.Format(
                "SELECT * FROM fnHosViolationDetail({0}, '{1}', '{2}') hos",
                    Converter.ToInt32(Driver.ID),
                    Converter.ToDateTime(report.StartDate),
                    Converter.ToDateTime(report.EndDate));
            var violations = db.Database.SqlQuery<HOSViolationDetailViewModel>(sql).AsQueryable();
            report.Violations = violations;

            // Get most recent HOS signature
            // TODO: will eventually want to see if that signature is valid (may have signed but logged back in)
            sql = string.Format(@"
                    SELECT TOP 1 * FROM tblHOSSignature
                        WHERE HOSDate = '{0}' AND DriverID = {1}
                        ORDER BY CreateDateUTC DESC", 
                    Date.ToString("yyyy-MM-dd"),
                    Driver.ID);

            report.HosSignature = db.Database.SqlQuery<HosSignature>(sql).AsQueryable().FirstOrDefault();

            // Get recap data (most recent HOS Summary records)
            sql = string.Format(@"
                SELECT *
                    FROM fnHosDriverViolationSummary({0}, DATEADD(DAY, -{2}+1, '{1}'), '{1}', default)
                    ORDER BY [Date]",
                Driver.ID,
                report.StartDate,
                report.HosPolicy.ShiftIntervalDays);
            report.ViolationSummary = db.Database.SqlQuery<HOSViolationSummaryViewModel>(sql).AsQueryable();

            // Get related orders
            report.Orders = db.Orders.Where(o => o.DriverID == Driver.ID 
                                && (   o.OriginArriveTimeUTC > report.StartDate && o.OriginDepartTimeUTC < report.EndDate
                                    || o.DestArriveTimeUTC > report.StartDate && o.DestDepartTimeUTC < report.EndDate)
                                ).ToList();

            return report;
        }

        /// --------------------------------------------------------------------------------------------
        /// <summary>
        ///     Exports an HOS Daily Log as a PDF file
        /// </summary>
        /// <param name="DriverID">Database ID of the driver</param>
        /// <param name="Date">Date of report (normalized to local time)</param>
        /// <remarks>~/HOS/PrintLog/?DriverID={driverid}&Date={date}</remarks>
        /// <returns>HOS Report view model</returns>
        /// --------------------------------------------------------------------------------------------
        [HttpGet]
        public ActionResult PrintLog(int DriverID, DateTime Date)
        {
            Driver Driver = db.Drivers.Find(DriverID);
            byte[] fileBytes = GetPdfFileBytes(DriverID, Date);
            GC.Collect();
            return File(fileBytes, "Application/pdf", GetPdfFileName(Driver.FullNameLF, Date));
        }


        /// --------------------------------------------------------------------------------------------
        /// <summary>
        ///     Returns a standardized file name for the HOS PDF reports
        /// </summary>
        /// <param name="Driver">Driver name (last name first)</param>
        /// <param name="Date">date of report</param>
        /// <returns>Filename of report</returns>
        /// --------------------------------------------------------------------------------------------
        private string GetPdfFileName(string Driver, DateTime Date)
        {
            return string.Format("{0}_HOS_{1}.pdf", Driver, Date.ToString("yyyy-MM-dd"));
        }


        /// --------------------------------------------------------------------------------------------
        /// <summary>
        ///     Used for print (PDF export), converts a page to a byte array using the HiQPdf plugin
        /// </summary>
        /// <param name="DriverID">database id of the driver</param>
        /// <param name="Date">date of report</param>
        /// --------------------------------------------------------------------------------------------
        private byte[] GetPdfFileBytes(int DriverID, DateTime Date)
        {
            HiQPdf.HtmlToPdf converter = new HiQPdf.HtmlToPdf();
            {
                converter.BrowserWidth = 900;
                converter.SerialNumber = "ezMSKisf-HTcSGQka-CQJNVUtb-SltJW0NO-SFtISlVK-SVVCQkJC";
                return converter.ConvertHtmlToMemory(PreviewLog(DriverID, Date), null);
            }
        }


        /// --------------------------------------------------------------------------------------------
        /// <summary>
        ///     Export daily HOS entries for a given driver and date range into one zip file.  Date range 
        ///         is limited to 31 days.
        /// </summary>
        /// <param name="DriverID">Database ID of the driver</param>
        /// <param name="StartDate">Start date to export</param>
        /// <param name="EndDate">End date to export</param>
        /// <remarks>~/HOS/ExportLogs/?DriverID={driverid}&StartDate={startdate}&EndDate={enddate}</remarks>
        /// <returns>HOS export zip file</returns>
        /// --------------------------------------------------------------------------------------------
        public FileStreamResult ExportLogs(int DriverID, DateTime Start, DateTime End)
        {
            Driver Driver = db.Drivers.Find(DriverID);
            End = new DateTime(Math.Min(Start.AddDays(31).Ticks, End.Ticks)); // Ensure 1-month max
            string fileName = string.Format("HOS_{0}_{1}-{2}", Driver.FullNameLF, Start.ToString("yyyyMMdd"), End.ToString("yyyyMMdd"));

            using (ZipFile zf = new ZipFile())
            {
                for (DateTime d = Start; d < End.AddDays(1); d = d.AddDays(1))
                {
                    byte[] fileBytes = GetPdfFileBytes(DriverID, d);
                    string filename = GetPdfFileName(Driver.FullNameLF, d);
                    zf.AddEntry(filename, fileBytes);
                }
                MemoryStream ms = new MemoryStream();
                zf.Save(ms);
                ms.Seek(0, SeekOrigin.Begin);
                // ensure the filename has a .zip extension
                if (!Path.GetExtension(fileName).Equals(".zip", StringComparison.CurrentCultureIgnoreCase))
                {
                    fileName = Path.GetFileNameWithoutExtension(fileName) + ".zip";
                }
                GC.Collect();
                return File(ms, "Application/zip", fileName);
            }
        }
    }
}