﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DispatchCrude.Models;
using DispatchCrude.Core;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;

namespace DispatchCrude.Controllers
{
    [Authorize(Roles = "viewTicketTypes")]
    public class TicketTypesController : Controller
    {
        private DispatchCrudeDB db = new DispatchCrudeDB();

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        // GET: Read
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest request = null, int id = 0)
        {
            return Read(request, null, id);
        }

        private ContentResult Read([DataSourceRequest] DataSourceRequest request, ModelStateDictionary modelState, int id = 0)
        {
            // .ToList() is required due to filtering on "Active" field, which is NOT MAPPED
            var data = db.TicketTypes.Where(t => t.ID == id || id == 0).ToList();
            var result = data.ToDataSourceResult(request, modelState);            
            return App_Code.JsonStringResult.Create(result);  
        }
        
        
        // POST: Update
        [HttpPost]
        [Authorize(Roles = "editTicketTypes")]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, TicketType tickettype)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // Get the existing record                    
                    TicketType existing = db.TicketTypes.Find(tickettype.ID);

                    // Specify all fields that will/can be updated by user input on the popup
                    db.CopyEntityValues(existing, tickettype);

                    db.SaveChanges(User.Identity.Name);  // Create & Modified dates & usernames will be taken care of by this line.
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(null, ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { tickettype }.ToDataSourceResult(request, ModelState));
                }
            }

            return Read(request, ModelState, tickettype.ID);
        }

        // Delete (Deactivate)
        [Authorize(Roles = "deactivateTicketTypes")]
        public ActionResult Delete(TicketType tickettype)
        {
            db.TicketTypes.Attach(tickettype);
            db.TicketTypes.Remove(tickettype);
            db.SaveChanges(User.Identity.Name);

            return null;
        }
    }
}
