﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DispatchCrude.Models;
using DispatchCrude.Core;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using AlonsIT;

namespace DispatchCrude.Controllers
{
    [Authorize(Roles = "viewRouteMaintenance")]
    public class RoutesController : Controller
    {        
        private DispatchCrudeDB db = new DispatchCrudeDB();

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        // GET: Routes
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest request = null, int id = 0)
        {
            return Read(request, null, id);
        }

        private ContentResult Read([DataSourceRequest] DataSourceRequest request, ModelStateDictionary modelState, int id = 0)
        {
            // .ToList() is required due to filtering on "Active" field, which is NOT MAPPED
            var data = db.Routes.Where(r => r.ID == id || id == 0)
                            .Include(r => r.Origin)
                            .Include(r => r.Destination)
                            .Include(r => r.WRMSVUom)
                            .ToList();

            var result = data.ToDataSourceResult(request, modelState);
            
            return App_Code.JsonStringResult.Create(result);
        }

        // POST: Create Routes
        //[HttpPost]
        [Authorize(Roles = "createRouteMaintenance")]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, Route route)
        {
            //Unique route Validation for the popup.  This doesn't actually DO the validation but ensures a validation message is produced on the popup.
            if (DBHelper.ToBoolean(db.Routes.Where(o => o.OriginID == route.OriginID).Where(d => d.DestinationID == route.DestinationID).Count()))
                ModelState.AddModelError(string.Empty, "This route already exists");
            
            if (ModelState.IsValid)
            {
                try
                {
                    // ensure all new routes are marked "Active" (otherwise they will be created deleted)
                    route.Active = true;

                    // Save new record to the database
                    db.Routes.Add(route);
                    db.SaveChanges(User.Identity.Name);  // Create dates & usernames will all be taken care of by this line.
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(null, ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { route }.ToDataSourceResult(request, ModelState));
                }
            }


            return Read(request, ModelState, route.ID);
        }

        // POST: Update Routes
        [HttpPost]
        [Authorize(Roles = "editRouteMaintenance")]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, Route route)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // Get the existing record
                    Route existing = db.Routes.Find(route.ID);

                    // Specify all fields that will/can be updated by user input on the popup
                    db.CopyEntityValues(existing, route);

                    db.SaveChanges(User.Identity.Name);  // Create & Modified dates & usernames will be taken care of by this line.
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(null, ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { route }.ToDataSourceResult(request, ModelState));
                }
            }

            return Read(request, ModelState, route.ID);
        }

        //***************
        // Per Maverick, this functionality should be tied to the activation/deactivation 
        // of the origins and destinations not managed manually on the routes page
        //***************
        // Delete (Deactivate)      
        //[Authorize(Roles = "deactivateRouteMaintenance")]
        //public ActionResult Delete(Route route)
        //{
        //    db.Routes.Attach(route);
        //    db.Routes.Remove(route);
        //    db.SaveChanges(User.Identity.Name);

        //    return null;
        //}
    }
}
