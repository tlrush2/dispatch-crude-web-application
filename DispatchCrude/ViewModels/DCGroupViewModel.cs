﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DispatchCrude.Models;
using DispatchCrude.Core;
using DispatchCrude.Extensions;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace DispatchCrude.ViewModels
{
    public class DCGroupViewModel : AuditModelDeleteBase
    {
        public Guid Id { get; set; }

        [Required]
        [DisplayName("Name")]
        [StringLength(256, MinimumLength = 3, ErrorMessage = "Name must be at least 3 characters long")]
        public string GroupName { get; set; }

        public int UserCount { get; set; }

        public int PermissionCount { get; set; }

        [Required]
        [DisplayName("Description")]
        [StringLength(256, MinimumLength = 20, ErrorMessage = "Please be more descriptive")]
        public string Description { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!ValidateDuplicateName(Id, GroupName))
                yield return new ValidationResult("Group name is already in use");
            Validated = true;
        }

        private bool ValidateDuplicateName(Guid id, string name)
        {
            using (DispatchCrudeDB db = new DispatchCrudeDB())
            {
                return db.DCGroups.Where(u => u.GroupName.Equals(name, StringComparison.CurrentCultureIgnoreCase) && (id == null || u.GroupId != id)).Count() == 0;
            }
        }

        override public bool allowRowEdit  //works with customKendoButtons.js to get rid of the edit button for locked rows
        {
            get
            {
                if (
                        //If the group is the admin group and you are not an admin, you cannot edit this group
                        //(NOTE: group details page will also look at this and ensure that the admin group is not editable by anyone on the front end)
                        (GroupName == "_DCAdministrator" && !UserSupport.IsInGroup("_DCAdministrator"))
                        //If the group is the manager group and you are not an admin, you cannot edit this group
                        || (GroupName == "_DCSystemManager" && !UserSupport.IsInGroup("_DCAdministrator"))
                        //If the group is the support group and you are not an admin or manager, you cannot edit this group
                        || (GroupName == "_DCSupport" && (!UserSupport.IsInGroup("_DCAdministrator") && !UserSupport.IsInGroup("_DCSystemManager")))
                    )
                    return false;

                return true;
            }
        }

        override public bool allowRowDeactivate  //works with customKendoButtons.js to get rid of the deactivate button for locked rows
        {
            get
            {
                //The groups ("_DCAdministrator", "_DCSystemManager", "_DCSupport", "_Driver", "_Gauger") are system groups and cannot be deactivated by anyone.
                //Also, groups with users assigned to them cannot be deactivated.
                return !GroupName.IsIn("_DCAdministrator", "_DCSystemManager", "_DCSupport", "_Driver", "_Gauger") && UserCount == 0;
            }
        }
    }
}