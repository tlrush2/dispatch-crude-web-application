﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DispatchCrude.ViewModels
{
    public class DriverScheduleViewModel
    {
        public DateTime DayDate { get; set; }
        //ID
        public int? DriverID { get; set; }

        public short? StatusID { get; set; }
        public byte? StartHours { get; set; }
        public byte? DurationHours { get; set; }
        public bool? ManualAssignment { get; set; }

        public string Status { get; set; }
        public string StatusAbbrev { get; set; }
        public bool? OnDuty { get; set; }
        public bool? Utilized { get; set; }

        public DateTime? StartDateTime {get; set;}
        public DateTime? StartDateTimeUTC {get; set;}
        public DateTime? EndDateTime { get; set; }
        public DateTime? EndDateTimeUTC { get; set; }
    }
}