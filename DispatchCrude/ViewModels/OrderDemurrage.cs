﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DispatchCrude.Models;

namespace DispatchCrude.ViewModels
{
    public class OrderDemurrage
    {
        // system settings?
        //public static int DEMURRAGE_VARIANCE_WARNING_MIN = 15;
        public static int DEMURRAGE_VARIANCE_ERROR_MIN = 10;

        //public static int DEMURRAGE_VARIANCE_WARNING_MILES = 5;
        public static int DEMURRAGE_VARIANCE_ERROR_MILES = 5;


        public enum STATUS { OK = 0, Warning = 1, EarlyArrival = 2, LateArrival = 3, EarlyDeparture = 4, LateDeparture = 5, NotAtLocation = 6 }


        public int ID { get; set; }
        public int? OrderNum { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}"), UIHint("Date")]
        public DateTime? OrderDate { get; set; }

        public int CarrierID { get; set; }
        //public virtual Carrier Carrier { get; set; }
        public int DriverID { get; set; }
        //public virtual Driver Driver { get; set; }
        public string Driver { get; set; }

        public DateTime? AcceptActualTimeUTC { get; set; }
        public decimal? AcceptActualDist { get; set; }

        public DateTime? OriginArriveTimeUTC { get; set; }
        public DateTime? OriginArriveActualTimeUTC { get; set; }
        public int? OriginArriveDelta { get; set; }
        public decimal? OriginArriveActualDist { get; set; }

        public DateTime? OriginDepartTimeUTC { get; set; }
        public DateTime? OriginDepartActualTimeUTC { get; set; }
        public int? OriginDepartDelta { get; set; }
        public decimal? OriginDepartActualDist { get; set; }
        public DateTime? OriginPickupActualTimeUTC { get; set; }
        public int? OriginPickupDelta { get; set; }
        public decimal? OriginPickupActualDist { get; set; }

        public DateTime? DestArriveTimeUTC { get; set; }
        public DateTime? DestArriveActualTimeUTC { get; set; }
        public int? DestArriveDelta { get; set; }
        public decimal? DestArriveActualDist { get; set; }

        public DateTime? DestDepartTimeUTC { get; set; }
        public DateTime? DestDepartActualTimeUTC { get; set; }
        public int? DestDepartDelta { get; set; }
        public decimal? DestDepartActualDist { get; set; }
        public DateTime? DestDeliverActualTimeUTC { get; set; }
        public int? DestDeliverDelta { get; set; }
        public decimal? DestDeliverActualDist { get; set; }

        public bool OriginArriveTimeOK
        {
            get { return TimeInRange(OriginArriveDelta); }
        }
        public bool OriginDepartTimeOK
        {
            get { return TimeInRange(OriginDepartDelta) || TimeInRange(OriginPickupDelta); }
        }
        public bool DestArriveTimeOK
        {
            get { return TimeInRange(DestArriveDelta); }
        }
        public bool DestDepartTimeOK
        {
            get { return TimeInRange(DestDepartDelta) || TimeInRange(DestDeliverDelta); }
        }

        public bool TimeInRange(int? delta)
        {
            if (delta == null)
                return true;

            return -DEMURRAGE_VARIANCE_ERROR_MIN < delta && delta < DEMURRAGE_VARIANCE_ERROR_MIN;
        }

        public bool OriginArriveEarly
        {
            get { return TimeEarly(OriginArriveDelta); }
        }
        public bool OriginDepartEarly
        {
            get { return TimeEarly(OriginDepartDelta) && TimeInRange(OriginPickupDelta) == false; }
        }
        public bool DestArriveEarly
        {
            get { return TimeEarly(DestArriveDelta); }
        }
        public bool DestDepartEarly
        {
            get { return TimeEarly(DestDepartDelta) && TimeInRange(DestDeliverDelta) == false; }
        }
        public bool TimeEarly(int? delta)
        {
            if (delta == null)
                return false; // nothing to validate so cannot assume

            return (delta > DEMURRAGE_VARIANCE_ERROR_MIN);
        }

        public bool OriginArriveLate
        {
            get { return TimeLate(OriginArriveDelta); }
        }
        public bool OriginDepartLate
        {
            get { return TimeLate(OriginDepartDelta) && TimeInRange(OriginPickupDelta) == false; }
        }
        public bool DestArriveLate
        {
            get { return TimeLate(DestArriveDelta); }
        }
        public bool DestDepartLate
        {
            get { return TimeLate(DestDepartDelta) && TimeInRange(DestDeliverDelta) == false; }
        }
        public bool TimeLate(int? delta)
        {
            if (delta == null)
                return false; // nothing to validate so cannot assume

            return (delta < -DEMURRAGE_VARIANCE_ERROR_MIN);
        }

        public bool OriginArriveTooFar
        {
            get { return DistTooFar(OriginArriveActualDist); }
        }
        public bool OriginDepartTooFar
        {
            get { return DistTooFar(OriginDepartActualDist) && DistTooFar(OriginPickupActualDist); }
        }
        public bool DestArriveTooFar
        {
            get { return DistTooFar(DestArriveActualDist); }
        }
        public bool DestDepartTooFar
        {
            get { return DistTooFar(DestDepartActualDist) && DistTooFar(DestDeliverActualDist); }
        }
        public bool DistTooFar(decimal? dist)
        {
            if (dist == null)
                return false; // nothing to validate so cannot assume

            return (Math.Abs(dist.Value) > DEMURRAGE_VARIANCE_ERROR_MILES);
        }
    }
}