﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Routing;
using DispatchCrude.Models;
using PagedList;

namespace DispatchCrude.ViewModels
{
    public class OrderSearchViewModel
    {
        public OrderSearchViewModel()
        {
            this.Page = 1;
            this.PageSize = 100;
            this.Sort = "ordernum";
            this.Statuses = new List<int>() { };
            this.HideSearch = true;
        }

        public int Page { get; set; }
        public int PageSize { get; set; }
        public string Sort { get; set; }
        public bool HideSearch { get; set; }
        public IPagedList<Order> Orders { get; set; }


        public string QuickSearch { get; set; }

        [DisplayName("Order #")]
        public string OrderNum { get; set; }

        [DisplayName("Job #")]
        public string JobNumber { get; set; }

        [DisplayName("Contract #")]
        public string ContractNumber { get; set; }

        [DisplayName("Status")]
        public List<int> Statuses { get; set; }

        [DisplayName("Ticket Type")]
        public int? TicketTypeID { get; set; }

        [Required]
        [DisplayName("Due Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}"), UIHint("Date")]
        public DateTime? DueDate { get; set; }

        [DisplayName("Order Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}"), UIHint("Date")]
        public DateTime? OrderDate { get; set; }

        [DisplayName("Priority")]
        public byte? PriorityID { get; set; }

        [DisplayName("Product")]
        public int? ProductID { get; set; }

        [DisplayName("Shipper PO"), StringLength(25)]
        public string DispatchConfirmNum { get; set; }


        public string Origin { get; set; }

        [DisplayName("Origin State"), StringLength(2)]
        public string OriginState { get; set; }

        [DisplayName("Tank #")]
        public string OriginTank { get; set; }

        [DisplayName("Origin BOL #"), StringLength(15)]
        public string OriginBOLNum { get; set; }

        [DisplayName("Origin Wait"), UIHint("BoolNullable2")]
        public bool? OriginWait { get; set; }


        [DisplayName("Destination")]
        public string Destination { get; set; }

        [DisplayName("Consignee")]
        public string Consignee { get; set; }

        [DisplayName("Dest. State"), StringLength(2)]
        public string DestState { get; set; }

        [DisplayName("Dest BOL #"), StringLength(30)]
        public string DestBOLNum { get; set; }

        [DisplayName("Destination Wait"), UIHint("BoolNullable2")]
        public bool? DestWait { get; set; }


        public string Carrier { get; set; }

        public string Driver { get; set; }

        public string Truck { get; set; }

        public string Trailer { get; set; }

        [DisplayName("Shipper")]
        public string Customer { get; set; }

        public string Operator { get; set; }

        public string Producer { get; set; }

        public string Pumper { get; set; }


        [DisplayName("Rejected?"), UIHint("BoolNullable2")]
        public bool? Rejected { get; set; }

        [DisplayName("Reroute?"), UIHint("BoolNullable2")]
        public bool? Rerouted { get; set; }

        //        [DisplayName("Chain-up?"), UIHint("BoolNullable2")]
        //        public bool? ChainUp { get; set; }

        [DisplayName("Transfer?"), UIHint("BoolNullable2")]
        public bool? Transferred { get; set; }

        [DisplayName("Deleted?"), UIHint("BoolNullable2")]
        public bool? Deleted { get; set; }

        //        [DisplayName("Settled?"), UIHint("BoolNullable2")]
        //        public bool? Settled { get; set; }


        // Route values is a shortcut for setting id/value pairs when calling a new page
        public RouteValueDictionary RouteValues(int page = 1, string sort = "OrderNum")
        {
            var rvd = new RouteValueDictionary();

            rvd["Page"] = page;
            rvd["Sort"] = sort;
            rvd["HideSearch"] = HideSearch;
            rvd["QuickSearch"] = QuickSearch;

            rvd["OrderNum"] = OrderNum;
            for (int i = 0; i < Statuses.Count; i++)
                rvd["Statuses[" + i + "]"] = Statuses[i];
            rvd["TicketTypeID"] = TicketTypeID;
            rvd["DueDate"] = DueDate;
            rvd["OrderDate"] = OrderDate;
            rvd["PriorityID"] = PriorityID;
            rvd["ProductID"] = ProductID;
            rvd["DispatchConfirmNum"] = DispatchConfirmNum;
            rvd["Origin"] = Origin;
            rvd["OriginState"] = OriginState;
            rvd["OriginTank"] = OriginTank;
            rvd["OriginBOLNum"] = OriginBOLNum;
            rvd["OriginWait"] = OriginWait;
            rvd["Destination"] = Destination;
            rvd["DestState"] = DestState;
            rvd["DestBOLNum"] = DestBOLNum;
            rvd["DestWait"] = DestWait;
            rvd["Carrier"] = Carrier;
            rvd["Driver"] = Driver;
            rvd["Truck"] = Truck;
            rvd["Trailer"] = Trailer;
            rvd["Customer"] = Customer;
            rvd["Operator"] = Operator;
            rvd["Producer"] = Producer;
            rvd["Pumper"] = Pumper;
            rvd["Rejected"] = Rejected;
            rvd["Rerouted"] = Rerouted;
            rvd["Transferred"] = Transferred;
            rvd["Deleted"] = Deleted;

            return rvd;
        }
    }
}