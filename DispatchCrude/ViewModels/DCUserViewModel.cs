﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DispatchCrude.Models;
using DispatchCrude.Core;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Profile;
using DispatchCrude.App_Code;

namespace DispatchCrude.ViewModels
{
    public class DCUserViewModel : AuditModelDeleteBase
    {
        public Guid Id { get; set; }

        [Required]
        [DisplayName("Username")]
        [StringLength(256, MinimumLength = 3, ErrorMessage = "Name must be at least 3 characters long")]
        public string UserName { get; set; }

        public int GroupCount { get; set; }
        //public DateTime? LastActivityDate { get; set; }
        //public DateTime? LastActivityDateDisplay { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]        
        [Display(Name = "Password")]
        public string Password { get; set; }

        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        public string Password1 { get; set; }

        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        public string Password2 { get; set; }

        [DisplayName("Contact Email")]
        [StringLength(256)]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Please enter a valid email address.")]
        public string Email { get; set; }

        [DisplayName("Locked Out?")]
        [UIHint("Switch")]
        public bool IsLockedOut { get; set; }

        [DisplayName("Active User?")]
        [UIHint("Switch")]
        public bool IsApproved { get; set; }

        [StringLength(256)]
        public string Comment { get; set; }
        
        //[DisplayName("Create Date")]  //NOTE: 7/22/16 - Discovered this is not needed as it is passed through, will delete entirely if testing passes on all pages
        //public DateTime CreateDateUTC { get; set; }  //from aspnet_membership

        [DisplayName("Last Activity Date")]
        public DateTime LastActivityDateUTC { get; set; } //from aspnet_users

        [DisplayName("Last Login Date")]
        public DateTime LastLoginDateUTC { get; set; } //membership

        [DisplayName("Last Lockout Date")]
        public DateTime LastLockoutDateUTC { get; set; } //membership

        [DisplayName("Last Password Changed Date")]
        public DateTime LastPasswordChangedDateUTC { get; set; } //membership

        // User Profile Placeholders
        #region Not Mapped Profile Data...
        [NotMapped]
        [DisplayName("Full Name")]
        public string FullName { get; set; }

        [NotMapped]
        public string CarrierID { get; set; }

        [NotMapped]
        public string DriverID { get; set; }

        [NotMapped]
        public string GaugerID { get; set; }

        [NotMapped]
        public string ShipperID { get; set; }

        [NotMapped]
        public string ProducerID { get; set; }

        [NotMapped]
        public string RegionID { get; set; }

        [NotMapped]
        public string TimeZoneID { get; set; }

        [NotMapped]
        public string TerminalID { get; set; }
        
        [NotMapped]
        [UIHint("Switch")]
        public bool UseDST { get; set; }
        #endregion

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!ValidateDuplicateName(Id, UserName))
                yield return new ValidationResult("Username is already in use");
            Validated = true;
        }

        private bool ValidateDuplicateName(Guid id, string name)
        {
            using (DispatchCrudeDB db = new DispatchCrudeDB())
            {
                return db.DCUsers.Where(u => u.UserName.Equals(name, StringComparison.CurrentCultureIgnoreCase) && (id == null || u.UserId != id)).Count() == 0;
            }
        }

        static private bool BlockElevatedChange(string UserName)
        {
            // check if the user has elevated permissions, but "you" don't - then prevent Edit|Deactivate operations
            return
                //If this user is an admin and "you" are not, then you cannot edit the user
                (UserSupport.IsInGroup("_DCAdministrator", UserName) && !UserSupport.IsInGroup("_DCAdministrator"))
                //If this user is a Manager and "you" are not an admin or system manager, then you cannot edit the user
                || (UserSupport.IsInGroup("_DCSystemManager", UserName) && !UserSupport.IsInGroup("_DCAdministrator"))
                //If this user is a Support user and "you" are not a Manager OR an Admin, then you cannot edit the user
                || (UserSupport.IsInGroup("_DCSupport", UserName) && (!UserSupport.IsInGroup("_DCAdministrator") && !UserSupport.IsInGroup("_DCSystemManager")));
        }

        //Note: to add this to regular models (not viewmodels) this may need to be a NotMapped field
        override public bool allowRowEdit  //works with customKendoButtons.js to get rid of the edit button for locked rows
        {
            get
            {
                return !BlockElevatedChange(this.UserName);
            }
        }

        //Note: to add this to regular models (not viewmodels) this may need to be a NotMapped field
        override public bool allowRowDeactivate  //works with customKendoButtons.js to get rid of the deactivate button for locked rows
        {
            get
            {
                return !BlockElevatedChange(this.UserName);
            }
        }

        static public string getFullName(string UserName)
        {
                ProfileBase UserProfile = new DispatchCrudeProfilePresenter().GetProfile(UserName);
                return (UserProfile.GetPropertyValue("FullName") != null) ? UserProfile.GetPropertyValue("FullName").ToString() : "";

        }
    }
}