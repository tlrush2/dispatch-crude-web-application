﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using DispatchCrude.Core;
using DispatchCrude.Models;

namespace DispatchCrude.ViewModels
{
    public class HOSViolationDetailViewModel
    {
        public int DriverID { get; set; }
        public int HOSPolicyID { get; set; }
	    public DateTime LogDateUTC { get; set; } 
	    public DateTime EndDateUTC { get; set; } 
	    public double TotalHours { get; set; }  
	    public double? TotalHoursSleeping { get; set; } 
	    public double? TotalHoursDriving { get; set; } 
	    public int SleeperStatusID { get; set; } 
	    public bool WeeklyReset { get; set; } 
	    public bool DailyReset { get; set; } 
	    public bool SleepBreak { get; set; }  
	    public bool DriverBreak { get; set; } 
	    public string Status { get; set; } 
	    public bool SleeperReset { get; set; } 
	    public DateTime? LastWeeklyReset { get; set; } 
	    public DateTime? LastDailyReset { get; set; } 
	    public DateTime? LastBreak { get; set; } 
	    public DateTime? LastSleeperBerthReset { get; set; } 
	    public double HoursSinceWeeklyReset { get; set; } 
        public double OnDutyHoursSinceWeeklyReset { get; set; }
	    public double DrivingHoursSinceWeeklyReset { get; set; } 
	    public double OnDutyHoursSinceDailyReset { get; set; }    
	    public double DrivingHoursSinceDailyReset { get; set; } 
	    public double OnDutyDailyLimit { get; set; } 
	    public double HoursSinceBreak { get; set; } 
	    public double OnDutyHoursLeft { get; set; } 
	    public bool OnDutyViolation { get; set; } 
	    public double DrivingDailyLimit { get; set; } 
	    public double DrivingHoursLeft { get; set; } 
	    public bool DrivingViolation { get; set; }  
        public double BreakLimit { get; set; }
	    public double HoursTilBreak { get; set; } 
	    public bool BreakViolation { get; set; } 
	    public double WeeklyOnDutyLimit { get; set; } 
	    public double WeeklyOnDutyHoursLeft { get; set; } 
	    public bool WeeklyOnDutyViolation { get; set; } 
	    public double WeeklyDrivingLimit { get; set; } 
	    public double WeeklyDrivingHoursLeft { get; set; } 
	    public bool WeeklyDrivingViolation { get; set; } 
        public DateTime? OnDutyViolationDateUTC { get; set; }
	    public DateTime? DrivingViolationDateUTC { get; set; }  
	    public DateTime? BreakViolationDateUTC { get; set; } 
	    public DateTime? WeeklyDrivingViolationDateUTC { get; set; } 
	    public DateTime? WeeklyOnDutyViolationDateUTC { get; set; } 

        [NotMapped]
        public double DrivingPercent { get { return 100 * DrivingHoursSinceDailyReset / DrivingDailyLimit; } }
        [NotMapped]
        public double OnDutyPercent { get { return 100 * OnDutyHoursSinceDailyReset / OnDutyDailyLimit; } }
        [NotMapped]
        public double WeeklyOnDutyPercent { get { return 100 * OnDutyHoursSinceWeeklyReset / WeeklyOnDutyLimit; } }

        public static double ERROR_LIMIT = 100;
        public static double WARNING_LIMIT = 75;

        [NotMapped]
        public string DrivingStatus { get { return App_Code.PageHelper.getStatus(DrivingPercent, ERROR_LIMIT, WARNING_LIMIT); } }
        [NotMapped]
        public string OnDutyStatus { get { return App_Code.PageHelper.getStatus(OnDutyPercent, ERROR_LIMIT, WARNING_LIMIT); } }
        [NotMapped]
        public string WeeklyOnDutyStatus { get { return App_Code.PageHelper.getStatus(WeeklyOnDutyPercent, ERROR_LIMIT, WARNING_LIMIT); } }

        public List<HOSViolation> Violations {
            get {
                List<HOSViolation> v = new List<HOSViolation>();
                if (WeeklyOnDutyViolation && WeeklyOnDutyViolationDateUTC.HasValue)
                {
                    v.Add(new HOSViolation() {
                        ViolationTime = WeeklyOnDutyViolationDateUTC.Value,
                        Name = "Weekly Violation" });
                }
                if (WeeklyDrivingViolation && WeeklyDrivingViolationDateUTC.HasValue)
                {
                    v.Add(new HOSViolation() {
                        ViolationTime = WeeklyDrivingViolationDateUTC.Value,
                        Name = "Weekly Driving Violation" });
                }
                if (OnDutyViolation && OnDutyViolationDateUTC.HasValue)
                {
                    v.Add(new HOSViolation() {
                        ViolationTime = OnDutyViolationDateUTC.Value,
                        Name = "On Duty Violation"
                    });
                }
                if (DrivingViolation && DrivingViolationDateUTC.HasValue)
                {
                    v.Add(new HOSViolation() {
                        ViolationTime = DrivingViolationDateUTC.Value,
                        Name = "Driving Violation"
                    });
                }
                if (BreakViolation && BreakViolationDateUTC.HasValue)
                {
                    v.Add(new HOSViolation() {
                        ViolationTime = BreakViolationDateUTC.Value,
                        Name = "Break Violation"
                    });
                }
                return v;
            }
        }

        public DateTime LogDate { get { return DateHelper.ToLocal(LogDateUTC, DispatchCrudeHelper.GetProfileTimeZone(HttpContext.Current)); } }
        public DateTime EndDate { get { return DateHelper.ToLocal(EndDateUTC, DispatchCrudeHelper.GetProfileTimeZone(HttpContext.Current)); } }

        public string DriverStatus
        {
            get
            {
                switch (SleeperStatusID)
                {
                    case (int)HosDriverStatus.Status.DRIVING:
                        return "Driving";
                    case (int)HosDriverStatus.Status.ONDUTY:
                        return "On Duty (Not Driving)";
                    default:
                        if (TotalHoursSleeping == TotalHours)
                            return "Sleeper"; // all sleep
                        else if (TotalHoursSleeping > 0)
                            return "Off Duty/Sleeper"; // sleep/off duty mix
                        else
                            return "Off Duty"; // all off duty
                }
            }
        }
    }

    public class HOSViolation
    {
        public DateTime ViolationTime { get; set; }
        public string Name { get; set; }
    }



    public class HOSViolationSummaryViewModel
    {
        public DateTime Date { get; set; }
        public int DriverID { get; set; }
        public string Driver { get; set; }
        public int CarrierID { get; set; }
        public string Carrier { get; set; }
        public double DrivingHours { get; set; }
		public double OnDutyHours { get; set; }
		public double SBHours { get; set; }
		public double OffDutyHours {get; set;}
        public int UniqueViolations { get; set; } 
	    public int WeeklyOnDutyViolations { get; set; } 
	    public int WeeklyDrivingViolations { get; set; } 
	    public int OnDutyViolations { get; set; } 
	    public int DrivingViolations { get; set; }  
	    public int BreakViolations { get; set; } 
/*
        [ForeignKey("DriverID")]
        public DispatchCrude.Models.Driver Driver {
            get {
                using (Models.DispatchCrudeDB db = new Models.DispatchCrudeDB())
                {
                    return db.Drivers.Find(DriverID);
                }
            }
        }
*/
        public int Violations { get { return WeeklyOnDutyViolations + WeeklyDrivingViolations + OnDutyViolations + DrivingViolations + BreakViolations; } }
    }

    public class HOSSummary
    {
        public int ID { get; set; }
        public DateTime LogDateUTC { get; set; }
        public DateTime EndDateUTC { get; set; }
        public decimal TotalHours { get; set; }
        public int HosDriverStatusID { get; set; }

        public Hos HOS
        {
            get
            {
                using (DispatchCrudeDB db = new DispatchCrudeDB())
                {
                    return db.Hoss.Find(ID);
                }
            }
        }


        [NotMapped, DisplayName("Log Date")]
        public DateTime LogDate
        {
            get
            {
                return DateHelper.ToLocal(LogDateUTC, DispatchCrudeHelper.GetProfileTimeZone(HttpContext.Current));
            }
            set
            {
                LogDateUTC = DateHelper.ToUTC(value, DispatchCrudeHelper.GetProfileTimeZone(HttpContext.Current));
            }
        }
        [NotMapped, DisplayName("End Date")]
        public DateTime EndDate
        {
            get
            {
                return DateHelper.ToLocal(EndDateUTC, DispatchCrudeHelper.GetProfileTimeZone(HttpContext.Current));
            }
            set
            {
                EndDateUTC = DateHelper.ToUTC(value, DispatchCrudeHelper.GetProfileTimeZone(HttpContext.Current));
            }
        }

    }
}