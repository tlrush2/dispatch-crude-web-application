﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using DispatchCrude.Core;
using DispatchCrude.Models;

namespace DispatchCrude.ViewModels
{
    public class HOSReportViewModel
    {
        public HOSReportViewModel()
        {
        }

        public HOSReportViewModel(Driver driver, DateTime date)
        {
            Driver = driver;
            Date = date;
            StartDate = DateHelper.ToUTC(Date, DispatchCrudeHelper.GetProfileTimeZone(System.Web.HttpContext.Current));
            EndDate = new DateTime(Math.Min(StartDate.AddDays(1).Ticks, DateTime.UtcNow.Ticks));
            Orders = new List<Order>();
        }

        public Driver Driver { get; set; }
        public DateTime Date { get; set; }

        public IEnumerable<HOSSummary> Hoss { get; set; }
        public IEnumerable<HOSViolationDetailViewModel> Violations { get; set; }
        public IEnumerable<HOSViolationSummaryViewModel> ViolationSummary { get; set; }
        public HosPolicy HosPolicy { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public HosSignature HosSignature { get; set; }

        public List<Order> Orders { get; set; }
        public string OrderNums
        {
            get
            {
                return string.Join(",", Orders.Select(o => o.OrderNum).ToList()); // comma-separated list of ordernumbers
            }
        }
        public string Products
        {
            get
            {
                return string.Join(",", Orders.Select(o => o.Product.Name).Distinct().ToList()); // comma-separated list of distinct products
            }
        }
        public string ShippingDocs
        {
            get { return Orders.Any() ? (OrderNums + "; " + Products) : ""; }
        }

        public bool Signed
        {
            get { return HosSignature != null; }
        }
        
    }
}