﻿using System;

namespace DispatchCrude.ViewModels
{
    public class HOSPersonalConveyanceViewModel
    {
        public DateTime Date { get; set; }
        public int DriverID { get; set; }
        public string Driver { get; set; }
        public int CarrierID { get; set; }
        public string Carrier { get; set; }
        public int? HOSPolicyID { get; set; }
        public decimal TotalHours { get; set; }
        public decimal? PersonalDailyLimitHR { get; set; }
        public bool Overage { get; set; }
    }
}