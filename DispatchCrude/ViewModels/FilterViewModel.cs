﻿using DispatchCrude.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DispatchCrude.ViewModels
{
    public class FilterViewModel<T>
    {
        public FilterViewModel()
        {
            this.Month = DateTime.Now.Month;
            this.Year = DateTime.Now.Year;
        }

        [DisplayName("Start Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}"), UIHint("Date")]
        public DateTime? StartDate { get; set; }

        [DisplayName("End Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}"), UIHint("Date")]
        public DateTime? EndDate { get; set; }

        [DisplayName("Start Date")]
        public DateTime? StartDateTime { get; set; }

        [DisplayName("End Date")]
        public DateTime? EndDateTime { get; set; }

        [DisplayName("Month")]
        public int? Month { get; set; }
        [DisplayName("Year")]
        public int? Year { get; set; }

        [DisplayName("Date Range")]
        public string DateFilterName { get; set; }
        public DateFilter DateFilter { get; set; }


        public IEnumerable<T> results { get; set; }


        [DisplayName("Batch #")]
        public int? BatchNum { get; set; }

        [DisplayName("Carrier")]
        public int? CarrierID { get; set; }

        [DisplayName("Destination")]
        public int? DestinationID { get; set; }

        [DisplayName("Driver")]
        public int? DriverID { get; set; }

        [DisplayName("Driver Group")]
        public int? DriverGroupID { get; set; }

        [DisplayName("Driver Shift Type")]
        public string DriverShiftType { get; set; }

        [DisplayName("Order #")]
        public int? OrderNum { get; set; }

        [DisplayName("Priority")]
        public byte? PriorityID { get; set; }

        [DisplayName("Product")]
        public int? ProductID { get; set; }

        [DisplayName("Product Group")]
        public int? ProductGroupID { get; set; }

        [DisplayName("Questionnaire")]
        public int? QuestionnaireTemplateID { get; set; }

        [DisplayName("Questionnaire Type")]
        public int? QuestionnaireTemplateTypeID { get; set; }

        [DisplayName("Region")]
        public int? RegionID { get; set; }

        [DisplayName("Shipper")]
        public int? ShipperID { get; set; }

        [DisplayName("State")]
        public int? StateID { get; set; }

        [DisplayName("Terminal")]
        public int? TerminalID { get; set; }

        [DisplayName("Truck")]
        public int? TruckID { get; set; }

        [DisplayName("Trailer")]
        public int? TrailerID { get; set; }

        [DisplayName("Unit of Measure")]
        public int? UomID { get; set; }

        public int? DistanceLimit { get; set; }
        public int? TimeLimit { get; set; }

        [DisplayName("Search String")]
        public string SearchString { get; set; }

        // generic string filters
        public string CustomFilter { get; set; }
        public string CustomFilter2 { get; set; }
        public string CustomFilter3 { get; set; }
        public string CustomFilter4 { get; set; }
        public string CustomFilter5 { get; set; }
        // generic true false
        public bool CustomFlag { get; set; } 
        public bool CustomFlag2 { get; set; } 
        public bool CustomFlag3 { get; set; } 
        public bool CustomFlag4 { get; set; } 
        public bool CustomFlag5 { get; set; } 

        public bool ShowAll { get; set; }
    }
}