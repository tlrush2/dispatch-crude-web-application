﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DispatchCrude.ViewModels
{
    public class CommodityMethodViewModel
    {
        public int ID { get; set; }
        [Required, StringLength(100)]
        public string Name { get; set; }

        [Required, StringLength(15)]
        [DisplayName("Short Name")]
        public string ShortName { get; set; }

        [StringLength(255)]
        public string Description { get; set; }

        [DisplayName("Trade Days Only?")]
        public bool TradeDaysOnly { get; set; }
    }
}