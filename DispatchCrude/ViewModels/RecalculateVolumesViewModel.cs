﻿using System;
using DispatchCrude.Models;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace DispatchCrude.ViewModels
{
    public class RecalculateVolumesViewModel : Order
    {
        public int OrderId { get; set; }

        [DisplayName("Order #")]
        public int? OrderNumber { get; set; }        

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yy}")]
        public DateTime? Date { get; set; }

        public decimal? GOV { get; set; }

        public decimal? GSV { get; set; }

        public decimal? NSV { get; set; }

        [DisplayName("Origin")]
        public string OriginName { get; set; }

        [DisplayName("Destination")]
        public string DestinationName { get; set; }

        [DisplayName("Status")]
        public string OrderStatus { get; set; }

        [DisplayName("Ticket Type")]
        public string TicketTypeName { get; set; }

        [DisplayName("Driver")]
        public string DriverName { get; set; }          

        [DisplayName("Producer")]
        public string ProducerName { get; set; }
    }
}