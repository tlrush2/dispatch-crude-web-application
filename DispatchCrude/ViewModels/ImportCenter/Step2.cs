﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Newtonsoft.Json;

namespace DispatchCrude.ViewModels.ImportCenter
{
    public class Step2
    {
        public int RootObjectID { get; set; }
        public string RootObjectName { get; set; }
        public string ImportFieldsJson { get; set; }
        public string ImportFileName { get; set; }

        public Step2()
        {
            // this allows a blank Step2 model instance to be created
        }
        public Step2(int rootObjectID, DataTable dtInputData, string importFileName)
        {
            RootObjectID = rootObjectID;
            ImportFileName = importFileName;

            using (AlonsIT.SSDB db = new AlonsIT.SSDB())
            {
                RootObjectName = db.QuerySingleValue("SELECT Name FROM tblObject WHERE ID = {0}", (object)rootObjectID).ToString();
            }
            // store the InputFields as a JSON array (how it will be used later anyway - more efficient than storing an entire DataTable)
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.ContractResolver = new Core.JsonPropListExcludeContractResolver(Models.ImportCenter.ImportCenterEngine.IMPORT_FIELDNAMES);
            ImportFieldsJson = JsonConvert.SerializeObject(InputFields(dtInputData), settings);
        }
        private DataTable InputFields(DataTable dtInputData)
        {
            DataTable ret = new DataTable("InputFields");
            ret.Columns.Add("ImportFieldName");
            foreach (DataColumn col in dtInputData.Columns)
            {
                ret.Rows.Add(new object[] { col.Caption });
            }
            return ret;
        }
    }
}