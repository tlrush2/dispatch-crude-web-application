﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Newtonsoft.Json;
using DispatchCrude.Models.ImportCenter;

namespace DispatchCrude.ViewModels.ImportCenter
{
    public class Step3
    {
        public int RootObjectID { get; set; }
        public string RootObjectName { get; set; }
        public int ImportCenterID { get; set; }
        public string ImportCenterName { get; set; }
        public string ImportFileName { get; set; }
        public int SuccessCount { get; set; }
        public int WarningCount { get; set; }
        public int ErrorCount { get; set; }
        //public string ResultsJson { get; set; }

        public Step3() { }
        public Step3(ProcessResult processResult)
        {
            ImportFileName = processResult.ImportFileName;
            SuccessCount = processResult.SuccessCount;
            WarningCount = processResult.WarningCount;
            ErrorCount = processResult.ErrorCount;

            RootObjectID = processResult.RootObjectID;
            ImportCenterID = processResult.ImportCenterID;
            using (AlonsIT.SSDB db = new AlonsIT.SSDB())
            {
                RootObjectName = db.QuerySingleValue("SELECT Name FROM tblObject WHERE ID = {0}", (object)RootObjectID).ToString();
                ImportCenterName = db.QuerySingleValue("SELECT Name FROM tblImportCenterDefinition WHERE ID = {0}", (object)ImportCenterID).ToString();
            }
            //ResultsJson = JsonConvert.SerializeObject(processResult.Data);
        }
    }
}