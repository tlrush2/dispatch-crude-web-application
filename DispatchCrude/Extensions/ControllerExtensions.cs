﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace DispatchCrude.Extensions
{
    public static class ControllerExtensions
    {
        public static ViewResult RazorView(this Controller controller)
        {
            return RazorView(controller, null, null);
        }

        public static ViewResult RazorView(this Controller controller, object model)
        {
            return RazorView(controller, null, model);
        }

        public static ViewResult RazorView(this Controller controller, string viewName)
        {
            return RazorView(controller, viewName, null);
        }

        public static ViewResult RazorView(this Controller controller, string viewName, object model)
        {
            if (model != null)
                controller.ViewData.Model = model;

            controller.ViewBag._ViewName = GetViewName(controller, viewName);

            return new ViewResult
            {
                ViewName = "RazorView",
                ViewData = controller.ViewData,
                TempData = controller.TempData
            };
        }

        static string GetViewName(Controller controller, string viewName)
        {
            return !string.IsNullOrEmpty(viewName)
                ? viewName
                : controller.RouteData.GetRequiredString("action");
        }

        public static int IndexOf<T>(this IEnumerable<T> source, Func<T, bool> predicate)
        {
            if (source == null) throw new ArgumentNullException("source");
            if (predicate == null) throw new ArgumentNullException("predicate");

            var i = 0;
            foreach (var item in source)
            {
                if (predicate(item)) return i;
                i++;
            }

            return -1;
        }

    }
}