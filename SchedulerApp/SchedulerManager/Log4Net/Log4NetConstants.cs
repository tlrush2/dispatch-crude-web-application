﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlonsIT;
using System.Data.SqlClient;

namespace SchedulerManager.Log4Net
{
    public class Log4NetConstants
    {
        public const String SCHEDULER_LOGGER = "SchedulerLogger";

        private Log4NetConstants()
        {
        }
    }
}
