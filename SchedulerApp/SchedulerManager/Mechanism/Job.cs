﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using DispatchCrude.Models;

namespace SchedulerManager.Mechanism
{
    public abstract class Job
    {
        
        public abstract String GetName();
        public abstract void DoJob();
        public abstract bool IsRepeatable();
        public abstract int GetRepititionIntervalTime();

        public void ExecuteJob()
        {
            if (IsRepeatable())
            {
                while (true)
                {
                    DoJob();
                    Thread.Sleep(GetRepititionIntervalTime());
                }
            }
            else
            {
                DoJob();
            }
        }
        public virtual Object GetParameters()
        {
            return null;
        }
    }
}
