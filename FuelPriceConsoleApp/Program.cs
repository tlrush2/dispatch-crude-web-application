﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuelPriceConsoleApp
{
    class Program
    {
        static int Main(string[] args)
        {
            AlonsIT.SSDB.ConnectionFactory = new AlonsIT.ConnFactory_WinForm();
            string result = new FuelPrices().Retrieve(GetLastMonday());
            Console.WriteLine(result);
            return result.StartsWith("Success") ? 0 : 1;
        }

        static private DateTime GetLastMonday()
        {
            DateTime ret = DateTime.Now.Date;
            while (ret.DayOfWeek != DayOfWeek.Monday)
                ret = ret.AddDays(-1);
            return ret;
        }
    }

}
