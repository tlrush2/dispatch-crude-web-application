﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlonsIT;

namespace DispatchCrude.EmailSubscriptionEngine.Models
{
    class EmailStatement
    {
        public EmailStatement()
        {
            Errors = new List<string>();
        }
        public EmailStatement(DataRow drDefinition): this()
        {
            ID = DBHelper.ToInt32(drDefinition["ID"]);
            DefinitionID = DBHelper.ToInt32(drDefinition["DriverSettlementStatementEmailDefinitionID"]);
            DBName = drDefinition["DBName"].ToString();
            EmailSubject = drDefinition["EmailSubject"].ToString();
            EmailAddress = drDefinition["EmailAddress"].ToString();
            CCEmailAddress = drDefinition["CCEmailAddress"].ToString();
            PdfExportID = DBHelper.ToInt32(drDefinition["PdfExportID"]);
            BatchID = DBHelper.ToInt32(drDefinition["BatchID"]);
            DriverID = DBHelper.ToInt32(drDefinition["DriverID"]);
        }
        public int ID { get; private set; }
        public int DefinitionID { get; private set; }
        public string DBName { get; private set; }
        public string EmailSubject { get; private set; }
        public string EmailAddress { get; private set; }
        public string CCEmailAddress { get; private set; }
        public int PdfExportID { get; private set; }
        public int BatchID { get; private set; }
        public int DriverID { get; private set; }

        public string HtmlBody { get; set; }
        public Stream AttachmentStream { get; set; }
        public string AttachmentFileName { get; set; }

        public DateTime? StartUTC { get; set; }
        public DateTime? EndUTC { get; set; }

        public double? DurationSeconds { get { return new DateDifferential(StartUTC.Value, EndUTC.Value).Milliseconds / 1000.0; } }
        public List<string> Errors { get; private set; }
        public void AddResult(string error) { if (error != null) Errors.Add(error); }
        public string ErrorCSV { get { return Errors.Count == 0 ? null : string.Join(", ", Errors); } }
    }
}
