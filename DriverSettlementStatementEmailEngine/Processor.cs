﻿using System;
using System.IO;
using System.Net;
using System.Net.Mail;
using AlonsIT;
using System.Data;
using DispatchCrude.DataExchange;
using DispatchCrude.EmailSubscriptionEngine.Models;
using DispatchCrude.Extensions;
using System.Linq;

namespace DispatchCrude.DriverSettlementStatementEmailEngine
{
    public class Processor
    {
        public void Process()
        {
            DataTable dtEmailStatement = SupportEngine.RetrieveData(SupportEngine.RetrieveType.rtDriverSettlementStatementEmail);
            foreach (DataRow drEmailStatement in dtEmailStatement.Rows)
            {
                EmailStatement statement = new EmailStatement(drEmailStatement);
                Process(statement);
            }
        }

        private void Process(EmailStatement statement)
        {
            string location = "Process(definition)";

            statement.StartUTC = DateTime.UtcNow;
            try
            {
                location = "Setting DBName";
                // change the connection string to the specified database so the report will be generated properly
                (SSDB.ConnectionFactory as ConnFactory_WinForm).DBName = statement.DBName;

                location = "Instantiating Body Html generator";
                DriverSettlementStatementHtmlDocs htmlDocs = new DriverSettlementStatementHtmlDocs(statement.DefinitionID, statement.BatchID, statement.DriverID);
                location = "Generating Body Html Content";
                foreach (DCHtmlDocument htmlDoc in htmlDocs)
                {
                    byte[] htmlBytes = htmlDoc.Bytes;
                    statement.HtmlBody = System.Text.Encoding.UTF8.GetString(htmlBytes, 0, htmlBytes.Length);
                    break; // when specifying the optional "DriverID" parameter there will never be more than 1 document
                }

                location = "Instantiating Pdf Statement generator";
                DriverSettlementStatementPdfDocs attachPdfs = new DriverSettlementStatementPdfDocs(statement.PdfExportID, statement.BatchID, statement.DriverID);
                location = "Generating Pdf Statement Attachment";
                foreach (DCPdfDocument pdfDoc in attachPdfs)
                {
                    statement.AttachmentStream = pdfDoc.Stream;
                    statement.AttachmentStream.Seek(0, SeekOrigin.Begin);
                    statement.AttachmentFileName = pdfDoc.FileName;
                    break; // when specifying the optional "DriverID" parameter there will never be more than 1 document
                }

                // generate and send the email
                location = "Sending Email";
                statement.AddResult(SendEmail(statement));

                if (!statement.Errors.Any())
                {
                    // mark the record as processed so we know when it happened and to prevent duplicate processing
                    MarkSent(statement);
                }
            }
            catch (Exception ex)
            {
                statement.AddResult("Error @" + location + ": " + ex.Message);
            }
            statement.EndUTC = DateTime.UtcNow;
        }

         //return null if successful(no error) or an error string if failure occurred
        private string SendEmail(EmailStatement emailStatement)
        {
            string ret = null;

            try
            {
                new CTSEmail(emailStatement.EmailAddress, emailStatement.EmailSubject, emailStatement.HtmlBody, true)
                    .AddCC(emailStatement.CCEmailAddress)
                    .Attach(emailStatement.AttachmentStream, emailStatement.AttachmentFileName)
                    .Send();
            }
            catch (Exception ex)
            {
                ret = ex.Message;
            }
            return ret; // NULL indicates success
        }
 
        private void MarkSent(EmailStatement definition)
        {
            using (SSDB db = new SSDB())
            {
                db.ExecuteSql(@"UPDATE tblDriverSettlementStatementEmail
                    SET QueueEmailSend = 0, EmailDateUTC = '{1}'
                    WHERE ID = {0}"
                    , definition.ID
                    , DateTime.UtcNow.ToString("yyyy/MM/dd HH:mm:ss"));
            }
        }
        

    }
}
