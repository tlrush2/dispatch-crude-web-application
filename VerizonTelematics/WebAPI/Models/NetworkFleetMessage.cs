﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Reflection;
using System.Xml.Serialization;
using AlonsIT;

namespace VerizonTelematicsWebAPI.Models
{
    public partial class NetworkfleetMessage
    {
        static public NetworkfleetMessage Create(string xml)
        {
            NetworkfleetMessage ret = null;

            XmlRootAttribute xRoot = new XmlRootAttribute();
            xRoot.ElementName = "NetworkfleetMessage";
            // xRoot.Namespace = "http://www.networkfleet.com/dtd/dataconnect/DataConnect.v2.2.dtd";
            xRoot.IsNullable = true;

            using (TextReader reader = new StringReader(xml))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(NetworkfleetMessage), xRoot);
                ret = (NetworkfleetMessage)serializer.Deserialize(reader);
            }
            return ret;
        }

        private Dictionary<NetworkfleetMessageType, SqlCommand> _commands = new Dictionary<NetworkfleetMessageType, SqlCommand>((int)NetworkfleetMessageType._Size);
        private SqlCommand GetSqlCommand(SSDB db, NetworkfleetMessageType type)
        {
            if (_commands.ContainsKey(type))
                return _commands[type];
            else
            {
                SqlCommand cmd = null;
                try
                {
                    cmd = db.BuildCommand("spInsert_NFM_" + type.ToString());
                }
                catch (Exception)
                {
                    cmd = db.BuildCommand("spInsert_NFM_Other");
                }
                return (_commands[type] = cmd);
            }
        }
        public bool Save(string xml)
        {
            bool ret = false;

            using (SSDB db = new SSDB(true))
            {
                SqlCommand cmd = GetSqlCommand(db, this.Type);
                if (cmd != null)
                {
                    UpdateCommandParameters(cmd, this);

                    cmd.Parameters["@XML"].Value = xml;
                    cmd.ExecuteNonQuery();
                    if (this.Type == NetworkfleetMessageType.GPS && !DBHelper.IsNull(cmd.Parameters["@MsgId"].Value) && this.GPSFixes.GPSFix.Length > 0)
                    {
                        using (SqlCommand cmdFix = db.BuildCommand("spInsert_NFM_GPS_Fix"))
                        {
                            cmdFix.Parameters["@MsgId"].Value = cmd.Parameters["@MsgId"].Value;
                            foreach (GPSFix fix in this.GPSFixes.GPSFix)
                            {
                                UpdateCommandParameters(cmdFix, fix);
                                foreach (Speed speed in fix.Speed)
                                {
                                    cmdFix.Parameters["@Speed_Mph_" + speed.Type].Value = speed.Value;
                                }
                                cmdFix.ExecuteNonQuery();
                            }
                        }
                    }
                }
                db.CommitTransaction();
            }

            return ret;
        }

        private void UpdateCommandParameters(SqlCommand cmd, object obj)
        {
            // note: this uses reflection which has performance ramifications
            Type sourceItemType = obj.GetType();

            // iterate over all public instance properties of the destination object and update those values available in the source object
            foreach (PropertyInfo pi in sourceItemType.GetProperties(BindingFlags.Instance | BindingFlags.Public))
            {
                if (cmd.Parameters.Contains("@" + pi.Name))
                {
                    PropertyInfo spi = sourceItemType.GetProperty(pi.Name);
                    if (spi != null)
                    {
                        cmd.Parameters["@" + pi.Name].Value = spi.GetValue(obj);
                    }

                }
            }
        }

        [XmlAttribute()]
        public NetworkfleetMessageType Type { get; set; }

        [XmlElement("AgeInMiles", typeof(string))]
        public string AgeInMiles { get; set; }

        [XmlElement("VIN", typeof(string))]
        public string VIN { get; set; }

        [XmlElement("GPSFixes", typeof(GPSFixes))]
        public GPSFixes GPSFixes { get; set; }

        [XmlElement("BatteryVoltage", typeof(string))]
        public string BatteryVoltage { get; set; }

        [XmlElement("BatteryVoltageOff", typeof(string))]
        public string BatteryVoltageOff { get; set; }

        [XmlElement("BeginTime", typeof(string))]
        public string BeginTime { get; set; }

        [XmlElement("BufferType", typeof(string))]
        public string BufferType { get; set; }

        [XmlElement("Current", typeof(DataGroup))]
        public DataGroup Current { get; set; }

        [XmlElement("CurrentQueueSize", typeof(string))]
        public string CurrentQueueSize { get; set; }

        [XmlElement("CurrentTemp", typeof(string))]
        public string CurrentTemp { get; set; }

        [XmlElement("Delayed", typeof(DataGroup))]
        public DataGroup Delayed { get; set; }

        [XmlElement("DeliveryStatus", typeof(string))]
        public string DeliveryStatus { get; set; }

        [XmlElement("EndTime", typeof(string))]
        public string EndTime { get; set; }

        //[XmlElement("Engine-Hours", typeof(string))]
        [XmlElement("EngineHours", typeof(string))]
        public string EngineHours { get; set; }

        [XmlElement("EventId", typeof(string))]
        public string EventId { get; set; }

        [XmlElement("EventTime", typeof(string))]
        public string EventTime { get; set; }

        [XmlElement("EventTimeUTF", typeof(string))]
        public string EventTimeUTF { get; set; }

        [XmlElement("EventType", typeof(string))]
        public string EventType { get; set; }

        [XmlElement("FixTime", typeof(string))]
        public string FixTime { get; set; }

        [XmlElement("FleetId", typeof(string))]
        public string FleetId { get; set; }

        [XmlElement("GForce", typeof(string))]
        public string GForce { get; set; }

        [XmlElement("GPSAntenna", typeof(string))]
        public string GPSAntenna { get; set; }

        [XmlElement("Heading", typeof(string))]
        public string Heading { get; set; }

        [XmlElement("HistogramStatistics", typeof(HistogramStatistics))]
        public HistogramStatistics HistogramStatistics { get; set; }

        [XmlElement("Ignition", typeof(string))]
        public string Ignition { get; set; }

        [XmlElement("KeyOffTime", typeof(string))]
        public string KeyOffTime { get; set; }

        [XmlElement("KeyOnTime", typeof(string))]
        public string KeyOnTime { get; set; }

        [XmlElement("Latitude", typeof(string))]
        public string Latitude { get; set; }

        [XmlElement("Longitude", typeof(string))]
        public string Longitude { get; set; }

        [XmlElement("MaximumTemp", typeof(string))]
        public string MaximumTemp { get; set; }

        [XmlElement("MessageTime", typeof(string))]
        public string MessageTime { get; set; }

        [XmlElement("MessageTimeUTF", typeof(string))]
        public string MessageTimeUTF { get; set; }

        [XmlElement("MinimumTemp", typeof(string))]
        public string MinimumTime { get; set; }

        [XmlElement("MldList", typeof(MldList))]
        public MldList MldList { get; set; }

        [XmlElement("MsgId", typeof(string))]
        public string MsgId { get; set; }

        [XmlElement("NewVin", typeof(string))]
        public string NewVin { get; set; }

        [XmlElement("OdoBase", typeof(string))]
        public string OdoBase { get; set; }

        [XmlElement("Odometer", typeof(string))]
        public string Odometer { get; set; }

        [XmlElement("OffTime", typeof(string))]
        public string OffTime { get; set; }

        [XmlElement("OnTime", typeof(string))]
        public string OnTime { get; set; }

        [XmlElement("RegType", typeof(string))]
        public string RegType { get; set; }

        [XmlElement("Resets", typeof(string))]
        public string Resets { get; set; }

        [XmlElement("Retrans", typeof(DataGroup))]
        public DataGroup Retrans { get; set; }

        [XmlElement("Run", typeof(string))]
        public string Run { get; set; }

        [XmlElement("Sensors", typeof(Sensors))]
        public Sensors Sensors { get; set; }

        [XmlElement("SerialNum", typeof(string))]
        public string SerialNum { get; set; }

        [XmlElement("Speed", typeof(Speed))]
        public Speed Speed { get; set; }

        [XmlElement("State", typeof(string))]
        public string State { get; set; }

        [XmlElement("TotRunTimeMinutes", typeof(string))]
        public string TotRunTimeMinutes { get; set; }

        [XmlElement("TotalGallons", typeof(string))]
        public string TotalGallons { get; set; }

        [XmlElement("TotalTrips", typeof(string))]
        public string TotalTrips { get; set; }

        [XmlElement("TransitionCount", typeof(string))]
        public string TransitionCount { get; set; }

        [XmlElement("Trips", typeof(string))]
        public string Trips { get; set; }

        [XmlElement("UpTime", typeof(string))]
        public string UpTime { get; set; }

        [XmlElement("Vehicle", typeof(Vehicle))]
        public Vehicle Vehicle { get; set; }

        [XmlElement("Xrssi", typeof(string))]
        public string Xrssi { get; set; }
    }

    public partial class DataEntry
    {
        [XmlAttributeAttribute(DataType = "NMTOKEN")]
        public string NumDelivered { get; set; }

        [XmlAttributeAttribute(DataType = "NMTOKEN")]
        public string NumErrored { get; set; }

        [XmlAttributeAttribute(DataType = "NMTOKEN")]
        public string MinMsgId { get; set; }

        [XmlAttributeAttribute(DataType = "NMTOKEN")]
        public string MaxMsgId { get; set; }
    }

    public partial class DataGroup
    {
        public DataEntry GPS { get; set; }

        public DataEntry Diagnostic { get; set; }

        public DataEntry Histogram { get; set; }

        public DataEntry VehicleReg { get; set; }

        public DataEntry Sensor { get; set; }

        public DataEntry Seatbelt { get; set; }

        public DataEntry LastBuffer { get; set; }

        public DataEntry HealthBuffer { get; set; }

        public DataEntry HardBrakeAccel { get; set; }
    }

    public partial class GPSFixes
    {
        [XmlElement("GPSFix")]
        public GPSFix[] GPSFix { get; set; }

        [XmlAttributeAttribute(DataType = "NMTOKEN")]
        public string NumFixes { get; set; }
    }

    public partial class GPSFix
    {
        public string FixTime { get; set; }

        public string FixTimeUTF { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public string Ignition { get; set; }

        [XmlElement("Speed")]
        public Speed[] Speed { get; set; }

        public string Heading { get; set; }

        public string Odometer { get; set; }

        public string AgeInMiles { get; set; }

        [XmlAttributeAttribute(DataType = "NMTOKEN")]
        public string FromSatellite { get; set; }
    }

    public partial class Speed
    {
        [XmlAttribute(DataType = "NMTOKEN")]
        public string Type { get; set; }

        [XmlAttribute(DataType = "NMTOKEN")]
        public string Units { get; set; }

        [XmlText()]
        public string Value { get; set; }
    }

    public partial class HistogramStatistics
    {
        public Bins Bins { get; set; }

        [XmlAttributeAttribute(DataType = "NMTOKEN")]
        public string Type { get; set; }
    }

    public partial class Bins
    {
        [XmlElement("Bin")]
        public Bin[] Bin { get; set; }

        [XmlAttributeAttribute(DataType = "NMTOKEN")]
        public string Count { get; set; }
    }

    public partial class Bin
    {
        public string Range { get; set; }

        public string Count { get; set; }

        public string Percent { get; set; }
    }

    public partial class MldList
    {
        [XmlElement("Mld")]
        public Mld[] Mld { get; set; }

        [XmlAttributeAttribute(DataType = "NMTOKEN")]
        public string NumItems { get; set; }
    }

    public partial class Mld
    {
        public string MldCode { get; set; }

        public string MldValue { get; set; }

        public string MldUnit { get; set; }

        public string Description { get; set; }
    }

    public partial class Sensors
    {

        [XmlElement("SensorFix")]
        public SensorFix[] SensorFix { get; set; }

        [XmlAttribute()]
        public string NumFixes { get; set; }
    }

    public partial class SensorFix
    {
        public string FixTime { get; set; }

        public string FixTimeUTF { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public string Heading { get; set; }

        public string AgeInMiles { get; set; }

        public string Ignition { get; set; }

        public string Odometer { get; set; }

        public Speed Speed { get; set; }

        public string SensorName { get; set; }

        public string SensorNumber { get; set; }

        public string State { get; set; }

        public string TransitionCount { get; set; }

        public string OnTime { get; set; }

        public string OffTime { get; set; }

        public string KeyOnTime { get; set; }
    }

    public partial class Vehicle
    {
        public string Label { get; set; }

        public string Year { get; set; }

        public string Make { get; set; }

        public string Model { get; set; }

        public string Trim { get; set; }

        public string License { get; set; }

        public string LicenseState { get; set; }

        public string LicenseCntry { get; set; }

        public string FuelType { get; set; }
    }

    public enum NetworkfleetMessageType
    {
        GPS, Diagnostic, Histogram, Sensor, SeatBelt, LastBuffer, HealthBuffer, HardBrakeAccel, VehicleReg, Status, Heartbeat, Other, _Size = Heartbeat
    }
}
