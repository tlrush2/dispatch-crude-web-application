﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;

namespace VerizonTelematicsWebAPI
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AlonsIT.SSDB.ConnectionFactory = new App_Code.SSDB_ConnFactory();

            GlobalConfiguration.Configure(WebApiConfig.Register);
        }
    }
}
