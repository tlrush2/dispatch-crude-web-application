﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using AlonsIT;

namespace VerizonTelematicsWebAPI.App_Code
{
    class SSDB_ConnFactory : IConnectionFactory
    {
        static private string DATACONNECTIONSTRING = "VerizonTelematicsDB";
        private int _cmdTimeout;

        public SSDB_ConnFactory()
        {
            _cmdTimeout = 120;
        }
        public override int CmdTimeout
        {
            get
            {
                return _cmdTimeout;
            }
            set
            {
                _cmdTimeout = value;
            }
        }
        public override SqlConnection Connect(bool reset = false)
        {
            SqlConnection ret = new SqlConnection(GetConnectionString(DATACONNECTIONSTRING));
            ret.Open();
            return ret;
        }
        static private string GetConnectionString(string connectionStringName)
        {
            string ret = string.Empty;

            System.Configuration.ConnectionStringSettings connectionStringSettings = System.Configuration.ConfigurationManager.ConnectionStrings[connectionStringName];

            if (connectionStringSettings != null && !string.IsNullOrWhiteSpace(connectionStringSettings.ConnectionString))
                ret = connectionStringSettings.ConnectionString.Trim();
            return ret;
        }
    }
}