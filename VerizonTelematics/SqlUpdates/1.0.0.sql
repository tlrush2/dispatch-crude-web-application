BEGIN TRAN x
GO

CREATE TABLE DeliveryStatus
(
  ID tinyint identity(1, 1) NOT NULL CONSTRAINT PK_DeliveryStatus PRIMARY KEY
, Name varchar(25) NOT NULL CONSTRAINT UC_DeliveryStatus_Name UNIQUE
)
GO
CREATE TABLE NFM_Error
(
  [Type] varchar(25) NOT NULL
, MsgId bigint NOT NULL
, VIN varchar(18) NOT NULL
, FleetId varchar(28) NULL
, MessageTime datetime NOT NULL
, XML varchar(max) NOT NULL
, CONSTRAINT PK_NFM_Error PRIMARY KEY (Type, MsgId)
)
GO
CREATE TABLE NFM_Other
(
  [Type] varchar(25) NOT NULL
, MsgId bigint NOT NULL
, VIN varchar(18) NOT NULL
, FleetId varchar(28) NULL
, MessageTime datetime NOT NULL
, XML varchar(max) NOT NULL
, CONSTRAINT PK_NFM_Other PRIMARY KEY (Type, MsgId)
)
GO

CREATE TABLE NFM_GPS
(
  MsgId bigint NOT NULL CONSTRAINT PK_NFM_GPS PRIMARY KEY
, VIN varchar(18) NOT NULL
, FleetId varchar(28) NULL
, MessageTime datetime NULL
, DeliveryStatusID tinyint NULL CONSTRAINT FK_NFM_GPS_DeliveryStatus FOREIGN KEY REFERENCES DeliveryStatus(ID)
)
GO

CREATE TABLE NFM_GPS_Fix
(
  ID bigint identity(1, 1) CONSTRAINT PK_NFM_GPS_Fix PRIMARY KEY
, MsgId bigint NOT NULL CONSTRAINT FK_NFM_GPS_Fix_GPS FOREIGN KEY REFERENCES NFM_GPS(MsgId)
, FixTime datetime NOT NULL
, Latitude decimal(8, 6) NULL
, Longitude decimal(9, 6) NULL
, Ignition bit NOT NULL
, Speed_Mph_Max tinyint NULL
, Speed_Mph_Inst tinyint NULL
, Speed_Mph_Avg tinyint NULL
, Odometer int NULL
, Heading smallint NULL
, AgeInFeet int NULL
)
GO
CREATE UNIQUE INDEX UDX_NFM_GPS_Fix ON NFM_GPS_Fix(MsgId, FixTime)
GO

CREATE PROCEDURE spInsert_NFM_Error
(
  @Type varchar(25)
, @MsgId bigint 
, @VIN varchar(18) 
, @FleetId varchar(28) 
, @MessageTime datetime 
, @XML varchar(max) 
)
AS BEGIN
	BEGIN TRY
	IF NOT EXISTS (SELECT * FROM NFM_Error WHERE Type = @Type AND MsgId = @MsgId)
		INSERT INTO NFM_Error (Type, MsgId, VIN, FleetId, MessageTime, XML)
			VALUES (@Type, @MsgId, @VIN, @FleetId, @MessageTime, @XML)
	END TRY
	BEGIN CATCH
		-- JUST EAT ANY EXCEPTIONS HERE
	END CATCH
END
GO
GRANT EXECUTE ON spInsert_NFM_Error TO dispatchcrude_iis_acct
GO

CREATE PROCEDURE spInsert_NFM_Other
(
  @Type varchar(25)
, @MsgId bigint 
, @VIN varchar(18) 
, @FleetId varchar(28) 
, @MessageTime datetime 
, @XML varchar(max) 
)
AS BEGIN
	BEGIN TRY
		IF NOT EXISTS (SELECT * FROM NFM_Other WHERE Type = @Type AND MsgId = @MsgId)
			INSERT INTO NFM_Other (Type, MsgId, VIN, FleetId, MessageTime, XML)
				VALUES (@Type, @MsgId, @VIN, @FleetId, @MessageTime, @XML)
	END TRY
	BEGIN CATCH
		EXEC spInsert_NFM_Error @Type, @MsgId, @VIN, @FleetId, @MessageTime, @XML
	END CATCH
END
GO
GRANT EXECUTE ON spInsert_NFM_Other TO dispatchcrude_iis_acct
GO

CREATE PROCEDURE spInsert_NFM_GPS 
(
  @MsgId bigint OUTPUT
, @VIN varchar(18) 
, @FleetId varchar(28) 
, @MessageTime datetime 
, @DeliveryStatus varchar(12) 
, @XML varchar(max) 
)
AS BEGIN
	DECLARE @DSID tinyint = (SELECT ID FROM DeliveryStatus WHERE Name = @DeliveryStatus)
	IF (@DSID IS NULL)
	BEGIN
		INSERT INTO DeliveryStatus (Name) VALUES (@DeliveryStatus)
		SET @DSID = SCOPE_IDENTITY()
	END

	BEGIN TRY
		IF EXISTS (SELECT * FROM NFM_GPS WHERE MsgId = @MsgID)
			SET @MsgId = NULL
		ELSE
			INSERT INTO NFM_GPS (MsgId, VIN, FleetId, MessageTime, DeliveryStatusID) VALUES (@MsgId, @VIN, @FleetId, @MessageTime, @DSID)
	END TRY
	BEGIN CATCH
		EXEC spInsert_NFM_Error 'GPS', @MsgId, @VIN, @FleetId, @MessageTime, @XML
	END CATCH
END
GO
GRANT EXECUTE ON spInsert_NFM_GPS TO dispatchcrude_iis_acct
GO

/**********************************************************
-- Date Created: 10/17/2014
-- Author: Kevin Alons
-- Purpose: take a string and convert it to a bit (boolean)
***********************************************************/
CREATE FUNCTION fnToBool(@val varchar(255)) RETURNS bit AS
BEGIN
	DECLARE @ret bit
	SELECT @ret = CASE WHEN upper(ltrim(rtrim(@val))) IN ('1', 'TRUE', 'YES', 'T', 'Y') THEN 1 ELSE 0 END
	RETURN (@ret)
END
GO
GRANT EXECUTE ON dbo.fnToBool TO dispatchcrude_iis_acct
GO

CREATE PROCEDURE spInsert_NFM_GPS_Fix
(
  @MsgId bigint 
, @FixTime datetime 
, @Latitude decimal(8, 6)
, @Longitude decimal(9, 6) 
, @Ignition varchar(10)
, @Speed_Mph_Max tinyint 
, @Speed_Mph_Inst tinyint 
, @Speed_Mph_Avg tinyint 
, @Odometer decimal(9, 2) 
, @Heading smallint 
, @AgeInMiles decimal(9, 3) 
, @ID bigint = NULL OUTPUT

)
AS BEGIN
	INSERT INTO NFM_GPS_Fix (MsgId, FixTime, Latitude, Longitude, Ignition, Speed_Mph_Max, Speed_Mph_Inst, Speed_Mph_Avg, Odometer, Heading, AgeInFeet)
		VALUES (@MsgID, @FixTime, @Latitude, @Longitude, dbo.fnToBool(@Ignition), @Speed_Mph_Max, @Speed_Mph_Inst, @Speed_Mph_Avg, round(@Odometer, 0), @Heading, round(@AgeInMiles * 5280, 0))
END
GO
GRANT EXECUTE ON spInsert_NFM_GPS_Fix TO dispatchcrude_iis_acct
GO