SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.0.6'
SELECT  @NewVersion = '4.0.7'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1672: Add ability to capture arrive/depart photos'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

INSERT INTO tblPhotoType VALUES
(3, 'Condition Before', getutcdate(), 'System'),
(4, 'Condition After', GETUTCDATE(), 'System')

GO

INSERT INTO tblOrderRuleType VALUES
(18, 'Require Arrive/Depart Photos at Origin', 2, 1),
(19, 'Require Arrive/Depart Photos at Destination?', 2, 1)

GO

COMMIT
SET NOEXEC OFF