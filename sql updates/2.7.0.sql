/*  
	-- add new Product Temp min/max system settings
*/ 
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.6.29'
SELECT  @NewVersion = '2.7.0'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

INSERT INTO tblSetting (ID, Name, SettingTypeID, Value, Category, CreateDateUTC, CreatedByUser)
	SELECT 19, 'Obs Temp Minimum valid value', 4, '-50', 'Product Acceptance Criteria', GETUTCDATE(), 'System'
	UNION SELECT 20, 'Obs Temp Maximum valid value', 4, '150', 'Product Acceptance Criteria', GETUTCDATE(), 'System'
	
COMMIT
SET NOEXEC OFF