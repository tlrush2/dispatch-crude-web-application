/* add Driver Availability logic
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.2.3'
SELECT  @NewVersion = '2.2.4'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

CREATE TABLE tblDriverAvailability
(
  ID int identity(1, 1) not null constraint PK_DriverAvailability PRIMARY KEY
, DriverID int not null CONSTRAINT FK_DriverAvailability_Driver FOREIGN KEY REFERENCES tblDriver(ID)
, AvailDateTime smalldatetime not null
)
GO
GRANT SELECT, UPDATE, INSERT, DELETE ON tblDriverAvailability TO dispatchcrude_iis_acct
GO

/***********************************************************/
-- Date Created: 3 Nov 2013
-- Author: Kevin Alons
-- Purpose: add "friendly" names to the DriverAvailability table
/***********************************************************/
CREATE VIEW viewDriverAvailability AS
	SELECT DA.*
		, dbo.fnDateOnly(AvailDateTime) AS AvailDate
		, D.FullName AS Driver
		, C.Name AS Carrier
	FROM tblDriverAvailability DA
	JOIN viewDriver D ON D.ID = DA.DriverID
	JOIN tblCarrier C ON C.ID = D.CarrierID

GO
GRANT SELECT ON viewDriverAvailability TO dispatchcrude_iis_acct
GO

/***********************************************************/
-- Date Created: 3 Nov 2013
-- Author: Kevin Alons
-- Purpose: return all "potential" Driver Availability records for a date range
/***********************************************************/
CREATE PROCEDURE spDriverAvailability(@CarrierID int, @StartDate smalldatetime, @EndDate smalldatetime) AS BEGIN
	-- get a table with the entire specified date range (date only values)
	DECLARE @AvailDate smalldatetime
	SELECT @AvailDate = @StartDate, @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	CREATE TABLE #DateRange (AvailDate smalldatetime)
	WHILE (@AvailDate <= @EndDate) BEGIN
		INSERT INTO #DateRange VALUES (@AvailDate)
		SET @AvailDate = DATEADD(day, 1, @AvailDate)
	END
	
	SELECT dbo.fnDateMdYY(AvailDateOnly) AS AvailDate, AvailDateTime, CarrierID, Carrier, x.DriverID, Driver
		, MobilePhone, isnull(TOO.OpenOrders, 0) AS OpenOrders, isnull(DOO.OpenOrders, 0) AS DailyOpenOrders
	FROM (
		SELECT DA.ID
			, dbo.fnDateOnly(DR.AvailDate) AS AvailDateOnly, DA.AvailDateTime
			, D.CarrierID, D.Carrier, D.ID AS DriverID, D.FullName AS Driver
			, D.MobilePhone
		FROM (#DateRange DR
		CROSS JOIN viewDriver D)
		LEFT JOIN tblDriverAvailability DA ON DA.DriverID = D.ID AND dbo.fnDateOnly(DA.AvailDateTime) = DR.AvailDate
		WHERE (@CarrierID = -1 OR D.CarrierID = @CarrierID)
	) x
	LEFT JOIN (
		SELECT DriverID, count(*) AS OpenOrders 
		FROM tblOrder 
		WHERE StatusID IN (2,7,8) 
		GROUP BY DriverID
	) TOO ON TOO.DriverID = x.DriverID
	LEFT JOIN (
		SELECT DriverID, dbo.fnDateOnly(DueDate) AS DueDate, COUNT(*) AS OpenOrders
		FROM tblOrder 
		WHERE StatusID IN (2,7,8) 
		GROUP BY DriverID, dbo.fnDateOnly(DueDate)
	) DOO ON DOO.DriverID = x.DriverID AND DOO.DueDate = x.AvailDateOnly
	ORDER BY Carrier, Driver, AvailDateOnly
	
END

GO
GRANT EXECUTE ON spDriverAvailability TO dispatchcrude_iis_acct
GO

COMMIT
SET NOEXEC OFF