SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.12.7.6'
SELECT  @NewVersion = '3.12.7.7'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1497 - Clean up date column parameters in ReportCenter'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


UPDATE tblReportColumnDefinition
   SET DataFormat = 'm/d/yyyy'
 WHERE ID = 14 -- DueDate

UPDATE tblReportColumnDefinition
   SET FilterTypeID = 6
 WHERE ID = 241 -- PickupPrintDateUTC 

UPDATE tblReportColumnDefinition
   SET FilterTypeID = 6
 WHERE ID = 242 -- DeliverPrintDateUTC

UPDATE tblReportColumnDefinition
  SET FilterTypeID = 6,
      DataFormat = '[$-409]m/d/yy hh:mm AM/PM;@'
 WHERE ID = 90027 -- OrderCreateDate

GO


COMMIT
SET NOEXEC OFF