SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.5.2'
SELECT  @NewVersion = '4.5.3'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'JT-1493 - Convert trailer compliance pages to MVC'	
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

-- -------------------------------------------------------------
-- Create Trailer Compliance Permissions
-- -------------------------------------------------------------

-- Update existing Trailer compliance page view permission to fit with new permissions being added below 
UPDATE aspnet_Roles
SET FriendlyName = 'Compliance Docs - View'
	, RoleName = 'viewTrailerCompliance'
	, LoweredRoleName = 'viewtrailercompliance'
	, Category = 'Assets - Compliance - Trailers'
	, Description = 'Allow user to view the Trailer Compliance page'
WHERE RoleName = 'viewComplianceTrailer'
GO

-- Add new Trailer compliance permissions
EXEC spAddNewPermission 'createTrailerCompliance', 'Allow user to create Trailer Compliance Documents', 'Assets - Compliance - Trailers', 'Compliance Docs - Create'
GO
EXEC spAddNewPermission 'editTrailerCompliance', 'Allow user to edit Trailer Compliance Documents', 'Assets - Compliance - Trailers', 'Compliance Docs - Edit'
GO
EXEC spAddNewPermission 'deactivateTrailerCompliance', 'Allow user to deactivate Trailer Compliance Documents', 'Assets - Compliance - Trailers', 'Compliance Docs - Deactivate'
GO

-- Add new Trailer compliance permissions to any groups that already had the "view" permission 
EXEC spAddNewPermissionToGroupsWithExistingRole 'viewTrailerCompliance', 'createTrailerCompliance'
GO
EXEC spAddNewPermissionToGroupsWithExistingRole 'viewTrailerCompliance', 'editTrailerCompliance'
GO
EXEC spAddNewPermissionToGroupsWithExistingRole 'viewTrailerCompliance', 'deactivateTrailerCompliance'
GO

-- Add new Trailer compliance type permissions ("Types" permissions do not get added to users by default when they are new permissions.  They will be added on request when needed by a user.)
EXEC spAddNewPermission 'viewTrailerComplianceTypes', 'Allow user to view the Trailer Compliance Types page', 'Assets - Compliance - Trailers', 'Compliance Types - View'
GO
EXEC spAddNewPermission 'createTrailerComplianceTypes', 'Allow user to create Trailer Compliance Types', 'Assets - Compliance - Trailers', 'Compliance Types - Create'
GO
EXEC spAddNewPermission 'editTrailerComplianceTypes', 'Allow user to edit Trailer Compliance Types', 'Assets - Compliance - Trailers', 'Compliance Types - Edit'
GO
EXEC spAddNewPermission 'deactivateTrailerComplianceTypes', 'Allow user to deactivate Trailer Compliance Types', 'Assets - Compliance - Trailers', 'Compliance Types - Deactivate'
GO

-- Add new Trailer compliance type permissions to any groups that already had the "view" permission 
EXEC spAddNewPermissionToGroupsWithExistingRole 'viewComplianceInspectionTypes', 'viewTrailerComplianceTypes'
GO
EXEC spAddNewPermissionToGroupsWithExistingRole 'viewComplianceInspectionTypes', 'createTrailerComplianceTypes'
GO
EXEC spAddNewPermissionToGroupsWithExistingRole 'viewComplianceInspectionTypes', 'editTrailerComplianceTypes'
GO
EXEC spAddNewPermissionToGroupsWithExistingRole 'viewComplianceInspectionTypes', 'deactivateTrailerComplianceTypes'
GO

--REMOVE NOW UNUSED PERMISSION FOR TRUCK/TRAILER COMPLIANCE INSPECTION TYPES PAGES
DELETE FROM aspnet_UsersInRoles WHERE RoleId = (SELECT RoleId FROM aspnet_Roles WHERE RoleName = 'viewComplianceInspectionTypes')
GO
DELETE FROM aspnet_RolesInGroups WHERE RoleId = (SELECT RoleId FROM aspnet_Roles WHERE RoleName = 'viewComplianceInspectionTypes')
GO
DELETE FROM aspnet_Roles WHERE RoleId = (SELECT RoleId FROM aspnet_Roles WHERE RoleName = 'viewComplianceInspectionTypes')
GO


-- Update descriptions on a few existing permissions
UPDATE aspnet_Roles SET Description = 'Allow user to view the Driver Compliance page' WHERE RoleName = 'viewDriverCompliance'
GO
UPDATE aspnet_Roles SET Description = 'Allow user to view the Driver Compliance Types page' WHERE RoleName = 'viewDriverComplianceTypes'
GO

-- -------------------------------------------------------------
-- Update Trailer Compliance Type (formerly Trailer Inspection Type) - (Change tblTrailerInspectionType to tblTrailerComplianceType)
-- -------------------------------------------------------------
EXEC sp_rename N'dbo.tblTrailerInspectionType', N'tblTrailerComplianceType', 'OBJECT'
GO
EXEC sp_rename 'PK_TrailerInspectionType', 'PK_TrailerComplianceType'
GO
EXEC sp_rename 'DF_TrailerInspectionType_CreateDateUTC', 'DF_TrailerComplianceType_CreateDateUTC'
GO
EXEC sp_rename 'DF_TrailerInspectionType_Required', 'DF_TrailerComplianceType_Required'
GO

ALTER TABLE tblTrailerComplianceType ADD IsSystem BIT NOT NULL CONSTRAINT DF_TrailerComplianceType_IsSystem DEFAULT 0
GO
--ALTER TABLE tblTrailerComplianceType ADD IsRequired BIT NOT NULL CONSTRAINT DF_TrailerComplianceType_IsRequired DEFAULT 0
EXEC sp_rename 'dbo.tblTrailerComplianceType.Required', 'IsRequired', 'COLUMN'
GO
ALTER TABLE tblTrailerComplianceType DROP CONSTRAINT DF_TrailerComplianceType_Required
GO
ALTER TABLE tblTrailerComplianceType ADD CONSTRAINT DF_TrailerComplianceType_IsRequired DEFAULT 0 FOR IsRequired
GO
ALTER TABLE tblTrailerComplianceType ADD RequiresDocument BIT NOT NULL CONSTRAINT DF_TrailerComplianceType_RequiresDocument DEFAULT 0
GO
--ALTER TABLE tblTrailerComplianceType ADD ExpirationLength INT NULL
EXEC sp_rename 'dbo.tblTrailerComplianceType.ExpirationMonths', 'ExpirationLength', 'COLUMN'
GO
ALTER TABLE tblTrailerComplianceType ALTER COLUMN ExpirationLength INT NULL
GO
ALTER TABLE tblTrailerComplianceType ADD TrailerTypeID INT NULL CONSTRAINT FK_TrailerComplianceType_TrailerType REFERENCES tblTrailerType(ID)
GO



-- -------------------------------------------------------------
-- Update Trailer Compliance (formerly Trailer Inspection)
-- -------------------------------------------------------------
EXEC sp_rename N'dbo.tblTrailerInspection', N'tblTrailerCompliance', 'OBJECT'
GO
EXEC sp_rename 'PK_TrailerInspection', 'PK_TrailerCompliance'
GO
EXEC sp_rename 'FK_TrailerInspection_Trailer', 'FK_TrailerCompliance_Trailer'
GO
EXEC sp_rename 'FK_TrailerInspection_Type', 'FK_TrailerCompliance_Type'
GO
EXEC sp_rename 'DF_TrailerInspection_CreateDateUTC', 'DF_TrailerCompliance_CreateDateUTC'
GO
EXEC sp_rename 'DF_TrailerInspection_InspectionDate', 'DF_TrailerCompliance_InspectionDate'
GO
EXEC sp_rename 'dbo.tblTrailerCompliance.TrailerInspectionTypeID', 'TrailerComplianceTypeID', 'COLUMN'
GO


--ALTER TABLE tblTrailerCompliance ADD DocumentName VARCHAR(255) NULL
EXEC sp_rename 'dbo.tblTrailerCompliance.DocName', 'DocumentName', 'COLUMN'
GO
--ALTER TABLE tblTrailerCompliance ADD DocumentDate SMALLDATETIME NULL CONSTRAINT DF_TrailerCompliance_DocumentDate DEFAULT GETDATE()
EXEC sp_rename 'dbo.tblTrailerCompliance.InspectionDate', 'DocumentDate', 'COLUMN'
GO
ALTER TABLE tblTrailerCompliance ALTER COLUMN DocumentDate SMALLDATETIME NULL
GO
EXEC sp_rename 'DF_TrailerCompliance_InspectionDate', 'PK_TrailerCompliance_DocumentDate'
GO
ALTER TABLE tblTrailerCompliance ADD DeleteDateUTC DATETIME NULL
GO
ALTER TABLE tblTrailerCompliance ADD DeletedByUser VARCHAR(100) NULL
GO


/*************************************/
-- Date Created: 08 Feb 2017 - 4.5.3
-- Author: Joe Engler/Ben Bloodworth
-- Purpose: ensure only one active compliance record exists
/*************************************/
CREATE TRIGGER trigTrailerCompliance_IU ON tblTrailerCompliance AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	UPDATE tblTrailerCompliance
	SET DeleteDateUTC = ISNULL(i.LastChangeDateUTC, i.CreateDateUTC),
	    DeletedByUser = ISNULL(i.LastChangedByUser, i.CreatedByUser)
	FROM tblTrailerCompliance TC
		LEFT JOIN inserted i ON i.TrailerID = TC.TrailerID AND i.TrailerComplianceTypeID = TC.TrailerComplianceTypeID
	WHERE i.ID IS NOT NULL AND i.ID <> TC.ID
		AND TC.DeleteDateUTC IS NULL
END
GO

-- Insert new carrier rule type
INSERT INTO tblCarrierRuleType 
VALUES (25, 'Enforce Trailer Compliance', 2, 0, 'Flag to check trailer compliance when dispatching orders')
GO


-- ----------------------------------------------------------------
-- Update compliance type table
-- ----------------------------------------------------------------

-- Update existing records
ALTER TABLE tblTrailerCompliance NOCHECK CONSTRAINT FK_TrailerCompliance_Type
GO

UPDATE tblTrailerCompliance 
SET TrailerComplianceTypeID = TrailerComplianceTypeID + 100
GO

SET IDENTITY_INSERT tblTrailerComplianceType ON
INSERT INTO tblTrailerComplianceType (ID, Name, ExpirationLength, IsRequired, IsSystem, RequiresDocument, TrailerTypeID,
		CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser)
SELECT ID+100, Name, ExpirationLength, IsRequired, IsSystem, RequiresDocument, TrailerTypeID,
		CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser 
FROM tblTrailerComplianceType
	
--Delete existing low numbered records
DELETE FROM tblTrailerComplianceType WHERE ID < 100
GO

INSERT INTO tblTrailerComplianceType (ID, Name, TrailerTypeID, IsSystem, IsRequired, RequiresDocument, ExpirationLength, CreatedByUser)
VALUES (1, 'Registration', NULL, 1, 1, 1, 365, 'System'),
	   (2, 'Insurance ID Card', NULL, 1, 1, 1, 365, 'System')
SET IDENTITY_INSERT tblTrailerComplianceType OFF	
GO

ALTER TABLE tblTrailerCompliance WITH CHECK CHECK CONSTRAINT FK_TrailerCompliance_Type
GO

-- Reseed the Identity #
DBCC CHECKIDENT (tblTrailerComplianceType, RESEED, 1001)
GO


-- ----------------------------------------------------------------
-- Copy existing data into new compliance table
-- ----------------------------------------------------------------
INSERT INTO tblTrailerCompliance (TrailerID, TrailerComplianceTypeID, Document, DocumentName, ExpirationDate)
SELECT ID, 1, RegistrationDocument, RegistrationDocName, RegistrationExpiration FROM tblTrailer
WHERE RegistrationDocName IS NOT NULL
GO

INSERT INTO tblTrailerCompliance (TrailerID, TrailerComplianceTypeID, Document, DocumentName, DocumentDate, ExpirationDate)
SELECT ID, 2, InsuranceIDCardDocument, InsuranceIDCardDocName, InsuranceIDCardIssue, InsuranceIDCardExpiration FROM tblTrailer
WHERE InsuranceIDCardDocName IS NOT NULL
GO


-- ----------------------------------------------------------------
-- Purge old columns from Trailer table
-- ----------------------------------------------------------------
ALTER TABLE tblTrailer DROP COLUMN RegistrationDocument
GO
ALTER TABLE tblTrailer DROP COLUMN RegistrationDocName
GO
ALTER TABLE tblTrailer DROP COLUMN RegistrationExpiration
GO
ALTER TABLE tblTrailer DROP COLUMN InsuranceIDCardDocName
GO
ALTER TABLE tblTrailer DROP COLUMN InsuranceIDCardDocument
GO
ALTER TABLE tblTrailer DROP COLUMN InsuranceIDCardExpiration
GO
ALTER TABLE tblTrailer DROP COLUMN InsuranceIDCardIssue
GO


-- ----------------------------------------------------------------
-- Refresh views after table changes
-- ----------------------------------------------------------------
EXEC sp_refreshview viewTrailer
GO
EXEC sp_refreshview viewTrailerMaintenance
GO
EXEC sp_refreshview viewDriverBase
GO
EXEC sp_refreshview viewOrder
GO



-- ----------------------------------------------------------------
-- Views
-- ----------------------------------------------------------------
GO

DROP VIEW viewTrailerInspection
GO
/***********************************/
-- Date Created: 5 Aug 2013 
-- Author: Kevin Alons
-- Purpose: return Trailer Inspection records with translated "friendly" values + missing required records
-- Changes:
--		4.5.3		2017/02/08		JAE/BSB			(Changed name - originally viewTrailerInspection) Reworked using the new trailer compliance fields added some helper logic (missing, expired, etc)
/***********************************/
CREATE VIEW viewTrailerCompliance AS
SELECT TC.ID, 
	TC.TrailerID, 
	Trailer = T.IDNumber,
	T.CarrierID,
	T.Carrier,
	TC.TrailerComplianceTypeID, 
	TrailerComplianceType = TCT.Name,
	TC.DocumentName,
	TC.DocumentDate,
	TC.ExpirationDate,
	TC.Notes,
	TC.CreateDateUTC,
	TC.CreatedByuser,
	TC.LastChangeDateUTC,
	TC.LastChangedByUser,
	TC.DeleteDateUTC,
	TC.DeletedByUser,
	MissingDocument = CAST(CASE WHEN Document IS NULL AND TCT.RequiresDocument = 1 THEN 1 ELSE 0 END AS BIT),
	ExpiredNow = CAST(CASE WHEN ExpirationDate < CAST(GETDATE() AS DATE) THEN 1 ELSE 0 END AS BIT),
	ExpiredNext30 = CAST(CASE WHEN ExpirationDate < CAST(DATEADD(MONTH, 1, GETDATE()) AS DATE) AND ExpirationDate >= CAST(GETDATE() AS DATE) THEN 1 ELSE 0 END AS BIT),
	ExpiredNext60 = CAST(CASE WHEN ExpirationDate < CAST(DATEADD(MONTH, 2, GETDATE()) AS DATE) AND ExpirationDate >= CAST(DATEADD(MONTH, 1, GETDATE()) AS DATE) THEN 1 ELSE 0 END AS BIT),
	ExpiredNext90 = CAST(CASE WHEN ExpirationDate < CAST(DATEADD(MONTH, 3, GETDATE()) AS DATE) AND ExpirationDate >= CAST(DATEADD(MONTH, 2, GETDATE()) AS DATE) THEN 1 ELSE 0 END AS BIT)
 FROM tblTrailerCompliance TC
 JOIN tblTrailerComplianceType TCT ON TC.TrailerComplianceTypeID = TCT.ID
 JOIN viewTrailer T ON TC.TrailerID = T.ID AND (TCT.TrailerTypeID IS NULL OR TCT.TrailerTypeID = T.TrailerTypeID)
GO


-- ----------------------------------------------------------------
-- Functions
-- ----------------------------------------------------------------
GO
/****************************************************
 Date Created: 2017/02/08 - 4.5.3
 Author: Joe Engler/Ben Bloodworth
 Purpose: return all trailer compliance with non compliant and missing records
 Changes:
****************************************************/
CREATE FUNCTION fnTrailerComplianceSummary(@showCompliant INT = 0, @showUnenforced INT = 0)
RETURNS @ret TABLE
(
	ID INT,
	TrailerID INT,
	Trailer VARCHAR(40),
	CarrierID INT,
	Carrier VARCHAR(40),
	ComplianceTypeID INT,
	ComplianceType VARCHAR(100),
	MissingDocument BIT,
	ExpiredNow BIT,
	ExpiredNext30 BIT,
	ExpiredNext60 BIT,
	ExpiredNext90 BIT,
	Missing BIT
)
AS BEGIN
 	DECLARE @__ENFORCE_TRAILER_COMPLIANCE__ INT = 24

	INSERT INTO @ret
	SELECT  ID, 
		TrailerID, 
		Trailer, 
		CarrierID, 
		Carrier, 
		TrailerComplianceTypeID, 
		TrailerComplianceType, 
		MissingDocument, 
		ExpiredNow, 
		ExpiredNext30, 
		ExpiredNext60, 
		ExpiredNext90, 
		Missing = CAST(0 AS BIT)
	  FROM viewTrailerCompliance
	 WHERE DeleteDateUTC IS NULL
	   AND (@showCompliant = 1 OR MissingDocument = 1 OR ExpiredNow = 1 OR ExpiredNext30 = 1 OR ExpiredNext60 = 1 OR ExpiredNext90 = 1)
	UNION 
	SELECT ID = NULL,
		TrailerID = Q.ID,
		Trailer = Q.IDNumber,
		CarrierID = Q.ID,
		CarrierName = Q.Carrier,
		ComplianceTypeID = DT.ID,
		ComplianceType = DT.Name,
		MissingDocument = CAST(0 AS BIT),
		ExpiredNow = CAST(0 AS BIT),
		ExpiredNext30 = CAST(0 AS BIT),
		ExpiredNext60 = CAST(0 AS BIT),
		ExpiredNext90 = CAST(0 AS BIT),
		Missing = DT.IsRequired
	FROM ( -- Get active trailers with compliance enforced
			SELECT T.* 
				FROM viewTrailer T
				OUTER APPLY dbo.fnCarrierRules(GETUTCDATE(), NULL, @__ENFORCE_TRAILER_COMPLIANCE__, NULL, T.CarrierID, NULL, NULL, NULL, 1) CR_C

				WHERE T.DeleteDateUTC IS NULL --active trailers
				AND (@showUnenforced = 1 OR dbo.fnToBool(CR_C.Value) = 1) -- compliance enforced
		) Q,
		tblTrailerComplianceType DT
	WHERE (@showCompliant = 1 OR DT.IsRequired = 1)
		AND NOT EXISTS -- no active record in the trailer compliance table
		(SELECT  1
			FROM viewTrailerCompliance
			WHERE DeleteDateUTC IS NULL AND TrailerID = Q.ID and TrailerComplianceTypeID = DT.ID
		)

	RETURN
END
GO


/****************************************************
 Date Created: 2016/12/22 - 4.4.14
 Author: Joe Engler
 Purpose: return carrier, driver, truck and trailer compliance
 Changes:
	- 4.5.2		2017-02-02		JAE			Integrate Truck compliance
	- 4.5.3		2017-02-08		BSB			Integrate Trailer compliance
****************************************************/
ALTER FUNCTION fnComplianceReport(@showCompliant INT = 0, @showUnenforced INT = 0)
RETURNS TABLE AS
RETURN
	SELECT Category = 'Carrier',
			ItemID = CarrierID, 
			ItemName = Carrier, 
			CarrierID, 
			Carrier,
			ComplianceTypeID, 
			ComplianceType,
			MissingDocument, 
			ExpiredNow, 
			ExpiredNext30, 
			ExpiredNext60, 
			ExpiredNext90, 
			Missing
	FROM fnCarrierComplianceSummary(@showCompliant, @showUnenforced)
	UNION
	SELECT Category = 'Driver',
			ItemID = DriverID, 
			ItemName = FullName,
			CarrierID, 
			Carrier,
			ComplianceTypeID, 
			ComplianceType,
			MissingDocument, 
			ExpiredNow, 
			ExpiredNext30, 
			ExpiredNext60, 
			ExpiredNext90, 
			Missing
	FROM fnDriverComplianceSummary(@showCompliant, @showUnenforced)
	UNION
	SELECT Category = 'Truck',
			ItemID = TruckID, 
			ItemName = Truck,
			CarrierID, 
			Carrier,
			ComplianceTypeID, 
			ComplianceType,
			MissingDocument, 
			ExpiredNow, 
			ExpiredNext30, 
			ExpiredNext60, 
			ExpiredNext90, 
			Missing
	FROM fnTruckComplianceSummary(@showCompliant, @showUnenforced)
	UNION
	SELECT Category = 'Trailer',
			ItemID = TrailerID, 
			ItemName = Trailer,
			CarrierID, 
			Carrier,
			ComplianceTypeID, 
			ComplianceType,
			MissingDocument, 
			ExpiredNow, 
			ExpiredNext30, 
			ExpiredNext60, 
			ExpiredNext90, 
			Missing
	FROM fnTrailerComplianceSummary(@showCompliant, @showUnenforced)
GO


-- ----------------------------------------------------------------
-- Refresh all
-- ----------------------------------------------------------------
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRebuildAllObjects
GO
EXEC _spRebuildAllObjects
GO


COMMIT
SET NOEXEC OFF