SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.11.17'
SELECT  @NewVersion = '3.11.17.1'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1117: Create core Producer Settlement tables/views/functions'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

-- Create producer settlement pages

CREATE TABLE tblProducerSettlementBatch(
	ID int IDENTITY(1,1) NOT NULL CONSTRAINT PK_ProducerSettlementBatch PRIMARY KEY CLUSTERED,
	BatchNum int NOT NULL,
	ProducerID int NOT NULL CONSTRAINT FK_ProducerSettlementBatch_Producer REFERENCES tblProducer(ID),
	Notes varchar(255) NULL,
	CreateDateUTC smalldatetime NOT NULL CONSTRAINT DF_ProducerSettlementBatch_CreateDateUTC DEFAULT (getutcdate()),
	CreatedByUser varchar(100) NULL,
	LastChangeDateUTC smalldatetime NULL,
	LastChangedByUser varchar(100) NULL,
	BatchDate smalldatetime NOT NULL CONSTRAINT DF_ProducerSettlementBatch_BatchDate DEFAULT (getdate()),
	InvoiceNum varchar(50) NULL,
)

GO


CREATE TABLE tblOrderSettlementProducer
(
	OrderID int NOT NULL CONSTRAINT PK_OrderSettlementProducer PRIMARY KEY CLUSTERED 
						 CONSTRAINT FK_OrderSettlementProducer_Order REFERENCES tblOrder(ID),
	OrderDate date NOT NULL,
	BatchID int NULL CONSTRAINT FK_OrderSettlementProducer_Batch REFERENCES tblProducerSettlementBatch(ID),
	SettlementFactorID int NULL CONSTRAINT FK_OrderSettlementProducer_SettlementFactor REFERENCES tblSettlementFactor(ID),
	SettlementUomID int NOT NULL CONSTRAINT FK_OrderSettlementProducer_Uom REFERENCES tblUom(ID),
	SettlementUnits decimal(18, 10) NOT NULL,
	CommodityPurchasePricebookID INT NOT NULL CONSTRAINT FK_OrderSettlementProducer_CommodityPurchasePricebook REFERENCES tblCommodityPurchasePricebook(ID),
	CommodityPurchasePricebookPriceAmount money NULL,
	CommodityPurchasePricebookDeductAmount money NULL,
	CommodityPurchasePricebookPremiumAmount money NULL,
	CommodityPurchasePricebookTotalAmount money null, --AS CommodityPurchasePricebookPriceAmount - CommodityPurchasePricebookDeductAmount + CommodityPurchasePricebookPremiumAmount,
	CreateDateUTC datetime NOT NULL CONSTRAINT DF_OrderSettlementProducer_CreateDateUTC  DEFAULT (getutcdate()),
	CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_OrderSettlementProducer_CreatedByUser  DEFAULT (suser_name()),
)

GO


EXEC _spDropIndex 'idxOrderSettlementProducer_Batch'
GO
CREATE NONCLUSTERED INDEX idxOrderSettlementProducer_Batch ON tblOrderSettlementProducer
(
	BatchID ASC
)
GO

EXEC _spDropIndex 'idxOrderSettlementProducer_Order'
GO
CREATE NONCLUSTERED INDEX idxOrderSettlementProducer_Order ON tblOrderSettlementProducer
(
	OrderID ASC
)
GO

EXEC _spDropIndex 'idxOrderSettlementProducer_OrderDate'
GO
CREATE NONCLUSTERED INDEX idxOrderSettlementProducer_OrderDate ON tblOrderSettlementProducer
(
	OrderDate ASC
)
GO




/******************************************************
-- Date Created: 2016-03-15
-- Author: Joe Engler
-- Purpose: Add computed fields to the CommodityPurchasePricebook table data
-- Changes: 
******************************************************/
CREATE VIEW viewCommodityPurchasePricebook AS
	SELECT X.*
		, MaxEffectiveDate = (SELECT MIN(OrderDate) FROM tblOrderSettlementProducer WHERE CommodityPurchasePricebookID = X.ID AND BatchID IS NOT NULL)
		, MinEndDate = (SELECT MAX(OrderDate) FROM tblOrderSettlementProducer WHERE CommodityPurchasePricebookID = X.ID AND BatchID IS NOT NULL)
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementProducer WHERE CommodityPurchasePricebookID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.OrderStartDate) 
			FROM tblCommodityPurchasePricebook XN 
			WHERE isnull(XN.ProductGroupID, 0) = isnull(X.ProductGroupID, 0) 
			  AND isnull(XN.ProductID, 0) = isnull(X.ProductID, 0) 
			  AND isnull(XN.ProducerID, 0) = isnull(X.ProducerID, 0) 
			  AND isnull(XN.OperatorID, 0) = isnull(X.OperatorID, 0) 
			  AND isnull(XN.OriginRegionID, 0) = isnull(X.OriginRegionID, 0)
			  AND isnull(XN.OriginID, 0) = isnull(X.OriginID, 0) 
			  AND isnull(XN.OriginStateID, 0) = isnull(X.OriginStateID, 0) 
			  AND XN.OrderStartDate > X.OrderStartDate)
		, PriorEndDate = (
			SELECT max(XN.OrderEndDate) 
			FROM tblCommodityPurchasePricebook XN 
			WHERE isnull(XN.ProductGroupID, 0) = isnull(X.ProductGroupID, 0) 
			  AND isnull(XN.ProductID, 0) = isnull(X.ProductID, 0) 
			  AND isnull(XN.ProducerID, 0) = isnull(X.ProducerID, 0) 
			  AND isnull(XN.OperatorID, 0) = isnull(X.OperatorID, 0) 
			  AND isnull(XN.OriginRegionID, 0) = isnull(X.OriginRegionID, 0)
			  AND isnull(XN.OriginID, 0) = isnull(X.OriginID, 0) 
			  AND isnull(XN.OriginStateID, 0) = isnull(X.OriginStateID, 0) 
			  AND XN.OrderEndDate < X.OrderStartDate)
	FROM tblCommodityPurchasePricebook X

GO

/******************************************************
-- Date Created: 2016-03-15
-- Author: Joe Engler
-- Purpose: Add computed fields to the CommodityPurchasePricebook table data
-- Changes: 
******************************************************/
CREATE VIEW viewCommodityPrice AS
	SELECT CP.*
		 , Locked = cast(CASE WHEN CPP.ID IS NULL THEN 1 ELSE 0 END as bit)
	FROM tblCommodityPrice CP
	LEFT JOIN viewCommodityPurchasePricebook CPP ON CPP.CommodityIndexID = CP.CommodityIndexID AND CP.PriceDate BETWEEN CPP.IndexStartDate AND CPP.IndexEndDate AND CPP.Locked = 1
GO

/*************************************
-- Date Created: 04/22/2016
-- Author: Joe Engler
-- Purpose: prevent deletion of Locked records
-- Changes:
*************************************/
CREATE TRIGGER trigCommodityPrice_IOD ON tblCommodityPrice INSTEAD OF DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewCommodityPrice X ON X.id = d.ID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('Locked (in use) records cannot be deleted', 16, 1)
		RETURN
	END
END

GO

/***********************************************
-- Date Created: 2016-03-05
-- Author: Joe Engler
-- Purpose: prevent editing of locked records
-- Changes:
***********************************************/
CREATE TRIGGER trigCommodityPrice_IU ON tblCommodityPrice AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewCommodityPrice X ON d.ID = X.ID AND X.Locked = 1
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END

	IF @error IS NOT NULL 
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
END

GO

/*************************************
-- Date Created: 04/18/2016
-- Author: Joe Engler
-- Purpose: prevent deletion of Locked records
-- Changes:
*************************************/
CREATE TRIGGER trigCommodityPurchasePricebook_IOD ON tblCommodityPurchasePricebook INSTEAD OF DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewCommodityPurchasePricebook X ON X.id = d.ID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('Locked (in use) records cannot be deleted', 16, 1)
		RETURN
	END
	ELSE
	BEGIN
		-- remove all references to the deleted rates
		UPDATE tblOrderSettlementProducer
		  SET CommodityPurchasePricebookID = NULL, 
		      CommodityPurchasePricebookPriceAmount = NULL,
			  CommodityPurchasePricebookDeductAmount = NULL,
			  CommodityPurchasePricebookPremiumAmount = NULL
		WHERE BatchID IS NULL
		  AND CommodityPurchasePricebookID IN (SELECT ID FROM deleted)
		-- do the actual delete now
		DELETE FROM tblCommodityPurchasePricebook WHERE ID IN (SELECT ID FROM deleted)
	END
END

GO


/***********************************************
-- Date Created: 2016-03-15
-- Author: Joe Engler
-- Purpose: prevent overlapping records (based on StartDate | EndDate)
			prevent changes to Locked (in use) records
			un-associate changed rates from previously rated orders
-- Changes:
***********************************************/
CREATE TRIGGER trigCommodityPurchasePricebook_IU ON tblCommodityPurchasePricebook AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblCommodityPurchasePricebook X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductID, X.ProductID) = 1
			  AND dbo.fnCompareNullableInts(i.ProducerID, X.ProducerID) = 1
			  AND dbo.fnCompareNullableInts(i.OperatorID, X.OperatorID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginRegionID, X.OriginRegionID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginStateID, X.OriginStateID) = 1
		WHERE i.OrderStartDate BETWEEN X.OrderStartDate AND X.OrderEndDate 
			OR i.OrderEndDate BETWEEN X.OrderStartDate AND X.OrderEndDate
			OR X.OrderStartDate BETWEEN i.OrderStartDate AND i.OrderEndDate
	)
	BEGIN
		SET @error = 'Overlapping Commodity Purchase Pricebook records are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewCommodityPurchasePricebook X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
		  OR dbo.fnCompareNullableInts(d.ProductID, X.ProductID) = 0
		  OR dbo.fnCompareNullableInts(d.ProducerID, X.ProducerID) = 0
		  OR dbo.fnCompareNullableInts(d.OperatorID, X.OperatorID) = 0
		  OR dbo.fnCompareNullableInts(d.OriginRegionID, X.OriginRegionID) = 0
		  OR dbo.fnCompareNullableInts(d.OriginID, X.OriginID) = 0
		  OR dbo.fnCompareNullableInts(d.OriginStateID, X.OriginStateID) = 0
		  OR d.OrderStartDate <> X.OrderStartDate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	IF @error IS NOT NULL 
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		DELETE FROM tblOrderSettlementProducer
		WHERE BatchID IS NULL AND CommodityPurchasePricebookID IN (SELECT ID FROM inserted)
END

GO

/***********************************/
-- Date Created: 13 Apr 2016
-- Author: Joseph Engler
-- Purpose: return Order JOIN OrderTicket + FINANCIAL INFO into a single view
-- Changes:
/***********************************/
CREATE VIEW viewOrderSettlementProducer AS 
	SELECT OSP.*
		, BatchNum = SB.BatchNum
		, SettlementUom = UOM.Name
		, SettlementUomShort = UOM.Abbrev
		, SettlementFactor = SF.Name
		, CommodityIndex = CI.ShortName
		, CommodityIndexID = CPP.CommodityIndexID
		, CommodityMethod = CM.ShortName
		, CommodityMethodID = CPP.CommodityMethodID
		, Price = dbo.fnCalculateCommodityPriceFromMethod(CPP.IndexStartDate, CPP.IndexEndDate, CPP.CommodityIndexID, CM.TradeDaysOnly)
		, CPP.OrderStartDate
		, CPP.OrderEndDate
		, CPP.IndexStartDate
		, CPP.IndexEndDate
		, CPP.Deduct
		, CPP.DeductType
		, CPP.Premium
		, CPP.PremiumType
		, CPP.PremiumDesc
	FROM tblOrderSettlementProducer OSP 
	LEFT JOIN tblProducerSettlementBatch SB ON SB.ID = OSP.BatchID
	LEFT JOIN tblUom UOM ON UOM.ID = OSP.SettlementUomID
	LEFT JOIN tblSettlementFactor SF ON SF.ID = OSP.SettlementFactorID
	LEFT JOIN tblCommodityPurchasePricebook CPP ON CPP.ID = OSP.CommodityPurchasePricebookID
	LEFT JOIN tblCommodityMethod CM ON CM.ID = CPP.CommodityMethodID
	LEFT JOIN tblCommodityIndex CI ON CI.ID = CPP.CommodityIndexID

GO




/***********************************/
-- Date Created: 4/1/2016
-- Author: Joe Engler
-- Purpose: return Order JOIN OrderTicket + FINANCIAL INFO into a single view
-- Changes:
/***********************************/
CREATE VIEW viewOrder_Financial_Producer AS 
	SELECT O.* 
		, TicketNums = dbo.fnOrderTicketDetails(O.ID, 'TicketNums', '<br/>') 
		, TankNums = dbo.fnOrderTicketDetails(O.ID, 'TankNums', '<br/>') 
		, InvoiceRatesAppliedDate = dbo.fnUTC_To_Local(OSP.CreateDateUTC, O.OriginTimeZoneID, O.OriginUseDST)
		, OSP.BatchID
		, InvoiceBatchNum = OSP.BatchNum 
		, InvoiceIndexStartDate = OSP.IndexStartDate
		, InvoiceIndexEndDate = OSP.IndexEndDate
		, InvoiceSettlementUom = OSP.SettlementUom
		, InvoiceSettlementUomShort = OSP.SettlementUomShort
		, InvoiceSettlementUnits = OSP.SettlementUnits
		, InvoiceSettlementFactorID = OSP.SettlementFactorID
		, InvoiceSettlementFactor = OSP.SettlementFactor
		, InvoiceIndexPrice = OSP.Price
		, InvoiceCommodityPurchasePricebookID = OSP.CommodityPurchasePricebookID
		, InvoiceCommodityPurchasePricebookPriceAmount = OSP.CommodityPurchasePricebookPriceAmount
		, InvoiceCommodityPurchasePricebookDeductAmount = OSP.CommodityPurchasePricebookDeductAmount
		, InvoiceCommodityPurchasePricebookPremiumAmount = OSP.CommodityPurchasePricebookPremiumAmount
		, InvoiceCommodityPurchasePricebookTotalAmount = OSP.CommodityPurchasePricebookTotalAmount
		, InvoiceCommodityIndex = OSP.CommodityIndex
		, InvoiceCommodityMethod = OSP.CommodityMethod
		, InvoiceDeduct = CASE WHEN OSP.DeductType = '$' THEN OSP.DeductType + CAST(OSP.Deduct AS VARCHAR)
								WHEN OSP.DeductType = '%' THEN CAST(OSP.Deduct AS VARCHAR) + OSP.DeductType 
								ELSE null END
		, InvoicePremium = CASE WHEN OSP.PremiumType = '$' THEN OSP.PremiumType + CAST(OSP.Premium AS VARCHAR)
								WHEN OSP.PremiumType = '%' THEN CAST(OSP.Premium AS VARCHAR) + OSP.PremiumType 
								ELSE null END
		, InvoicePremiumDesc = OSP.PremiumDesc								
	FROM dbo.viewOrderLocalDates O
	LEFT JOIN viewOrderSettlementProducer OSP ON OSP.OrderID = O.ID
	WHERE O.StatusID NOT IN (-10) -- don't include "Generated" orders (only Assigned+)

GO


/***********************************/
-- Date Created: 14 Apr 2016
-- Author: Joe Engler
-- Purpose: return the ProducerSettlementBatch table contents with the CreateDate formatted DateOnly (for the BatchDate)
/***********************************/
CREATE VIEW viewProducerSettlementBatch AS
SELECT X.*
	, '#' + ltrim(BatchNum) + ' [' + dbo.fnDateMdYY(BatchDate) + '] ' + ' / ' + ltrim(OrderCount) + isnull(' | ' + InvoiceNum, '') AS FullName
FROM (
	SELECT SB.*
		, Producer = P.Name 
		, OrderCount = (SELECT COUNT(*) FROM tblOrderSettlementShipper WHERE BatchID = SB.ID) 
	FROM dbo.tblProducerSettlementBatch SB
	JOIN dbo.tblProducer P ON P.ID = SB.ProducerID
) X


GO


/***********************************
-- Date Created: 3/31/2016
-- Author: Joe Engler
-- Purpose: Retrieve and return the Commodity Pricebook info for the specified criteria
-- NOTE!!!!: The ranking uses a decimal for each column so if a column is added the index at the bottom must be incremented
-- Changes:
***********************************/
CREATE FUNCTION fnProducerPurchasePricebook
(
  @StartDate date
, @EndDate date
, @ProducerID int
, @OperatorID int
, @ProductID int
, @ProductGroupID int
, @OriginID int
, @OriginStateID int
, @OriginRegionID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID
			, Ranking =	dbo.fnRateRanking(@ProducerID, R.ProducerID, 64, 0)
				  + dbo.fnRateRanking(@OperatorID, R.OperatorID, 32, 0)
				  + dbo.fnRateRanking(@ProductID, R.ProductID, 16, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 8, 0)
				  + dbo.fnRateRanking(@OriginID, R.OriginID, 4, 0)
				  + dbo.fnRateRanking(@OriginStateID, R.OriginStateID, 2, 0)
				  + dbo.fnRateRanking(@OriginRegionID, R.OriginRegionID, 1, 0)
		 FROM viewCommodityPurchasePricebook R
		WHERE coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(ProducerID, nullif(@ProducerID, 0), 0)
		  AND coalesce(nullif(@OperatorID, 0), R.OperatorID, 0) = coalesce(OperatorID, nullif(@OperatorID, 0), 0)
		  AND coalesce(nullif(@ProductID, 0), R.ProductID, 0) = coalesce(ProductID, nullif(@ProductID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@OriginID, 0), R.OriginID, 0) = coalesce(OriginID, nullif(@OriginID, 0), 0)
		  AND coalesce(nullif(@OriginStateID, 0), R.OriginStateID, 0) = coalesce(OriginStateID, nullif(@OriginStateID, 0), 0)
		  AND coalesce(nullif(@OriginRegionID, 0), R.OriginRegionID, 0) = coalesce(OriginRegionID, nullif(@OriginRegionID, 0), 0)
		  AND (@StartDate BETWEEN OrderStartDate AND OrderEndDate
			 OR @EndDate BETWEEN OrderStartDate AND OrderEndDate
			 OR OrderStartDate BETWEEN @StartDate AND @EndDate)
	)
	SELECT R.ID, ProducerID, OperatorID, ProductID, ProductGroupID, OriginID, OriginStateID, OriginRegionID, OrderStartDate, OrderEndDate
	  , MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate
	  , BestMatch, Ranking
	  , Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewCommodityPurchasePricebook R
	JOIN (
		SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 7  -- ensure some type of match occurred on all criteria choices, must match number of columns above
		) X ON X.Ranking = S.Ranking
	) X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)

GO


/***********************************
-- Date Created: 3/31/2016
-- Author: Joe Engler
-- Purpose:  Retrieve and return the Commodity Pricebook rows for the specified criteria
-- Changes:
***********************************/
CREATE FUNCTION fnProducerPurchasePricebookDisplay
(
  @StartDate date
, @EndDate date
, @ProducerID int
, @OperatorID int
, @ProductID int
, @ProductGroupID int
, @OriginID int
, @OriginStateID int
, @OriginRegionID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ProducerID, R.OperatorID, R.ProductID, R.ProductGroupID, R.OriginID, R.OriginStateID, R.OriginRegionID, R.OrderStartDate, R.OrderEndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Producer = PP.Name
		, Operator = OP.Name
		, Product = P.Name
		, ProductGroup = PG.Name
		, Origin = O.Name
		, OriginState = S.FullName
		, OriginRegion = RR.Name
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch, Ranking
		, CPP.SettlementFactorID
	FROM dbo.fnProducerPurchasePricebook(@StartDate, @EndDate, @ProducerID, @OperatorID, @ProductID, @ProductGroupID, @OriginID, @OriginStateID, @OriginRegionID, 0) R
	LEFT JOIN tblProducer PP ON PP.ID = R.ProducerID
	LEFT JOIN tblOperator OP ON OP.ID = R.OperatorID
	LEFT JOIN tblProduct P ON P.ID = R.ProductID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblOrigin O ON O.ID = R.OriginID
	LEFT JOIN tblState S ON S.ID = R.OriginStateID
	LEFT JOIN tblRegion RR ON RR.ID = R.OriginRegionID
	LEFT JOIN tblCommodityPurchasePricebook CPP ON CPP.ID = R.ID
	ORDER BY OrderStartDate

GO


/***********************************
-- Date Created: 3/31/2016
-- Author: Joe Engler
-- Purpose: Retrieve and return the Commodity Purchase Pricebook info for the specified order
-- Changes:
***********************************/
CREATE FUNCTION fnOrderProducerPurchasePricebook(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, A.Price, A.Deduct, A.Premium, A.UomID
	FROM dbo.viewOrderBase O
	CROSS APPLY dbo.fnProducerPurchasePricebook(isnull(O.OrderDate, O.DueDate), null, O.ProducerID, O.OperatorID, O.ProductID, O.ProductGroupID, O.OriginID, O.OriginStateID, O.OriginRegionID, 1) R
	CROSS APPLY dbo.fnCalculateCommodityPrice(R.ID) A

	WHERE O.ID = @ID 

GO


/***********************************/
-- Date Created: 4/1/2016
-- Author: Joe Engler
-- Purpose: return Order JOIN OrderTicket + FINANCIAL INFO into a single view
-- Changes:
/***********************************/
CREATE VIEW viewOrderSettlementUnitsProducer AS
	SELECT OrderID
		 , CarrierID
		 , SettlementUomID
		 , SettlementFactorID
		 , SettlementUnits = CASE WHEN SettlementUnits IS NULL THEN 0 ELSE SettlementUnits END
		 , CommodityPurchasePricebookID
	FROM (
			SELECT OrderID = O.ID
				, O.CarrierID
				, SettlementUomID = CASE WHEN CPP.SettlementFactorID IN (1,2,3) THEN O.OriginUomID ELSE O.DestUomID END
				, CPP.SettlementFactorID
				, SettlementUnits = CASE CPP.SettlementFactorID
							WHEN 1 THEN O.OriginGrossUnits 
							WHEN 3 THEN O.OriginGrossStdUnits 
							WHEN 2 THEN O.OriginNetUnits 
							WHEN 4 THEN O.DestGrossUnits
							WHEN 5 THEN O.DestNetUnits END
				, CommodityPurchasePricebookID = CPP.ID
			FROM dbo.tblOrder O
			OUTER APPLY dbo.fnOrderProducerPurchasePricebook(O.ID) OPPP
			LEFT JOIN tblCommodityPurchasePricebook CPP ON CPP.ID = OPPP.ID
--			JOIN tblCarrier C ON C.ID = O.CarrierID
	) X

GO

/*************************************************
-- Date Created: 3/31/2016
-- Author: Joe Engler
-- Purpose: Compute and add the various Producer "Settlement" $$ values to a Delivered/Audited order 
-- Changes:
*************************************************/
CREATE PROCEDURE spApplyRatesProducer
(
  @ID int
, @UserName varchar(100)
, @AllowReApplyPostBatch bit = 0
) AS BEGIN

	SET NOCOUNT ON
	
	IF (@AllowReApplyPostBatch = 0) AND EXISTS (SELECT * FROM tblOrderSettlementProducer WHERE OrderID = @ID AND BatchID IS NOT NULL)
	BEGIN
		RAISERROR('Invoice has already been Producer Settled', 16, 1)
		RETURN
	END
	
	/* compute the basic settlement units from the Order being invoices */
	DECLARE @SettlementUnits TABLE
	(
		OrderID int
	  , SettlementUomID int
	  , SettlementFactorID int
	  , SettlementUnits decimal(18, 10)
	)
	INSERT INTO @SettlementUnits (OrderID, SettlementUomID, SettlementFactorID, SettlementUnits)
		SELECT OSU.OrderID, OSU.SettlementUomID, OSU.SettlementFactorID, OSU.SettlementUnits
		FROM dbo.viewOrderSettlementUnitsProducer OSU
		LEFT JOIN tblOrderApproval OA ON OA.OrderID = OSU.OrderID
		WHERE OSU.OrderID = @ID

	/* compute the order level settlement rateIDs & settlement amounts - into temp table #I */
	SELECT OrderID = @ID
		, OrderDate
		, SettlementFactorID, SettlementUomID, SettlementUnits
		, CommodityPurchasePricebookID
		, CommodityPurchasePricebookPrice = ISNULL(CommodityPurchasePricebookPrice, 0)
		, CommodityPurchasePricebookDeduct = ISNULL(CommodityPurchasePricebookDeduct, 0)
		, CommodityPurchasePricebookPremium = ISNULL(CommodityPurchasePricebookPremium, 0)
		, CommodityPurchasePricebookUomID
		, CreatedByUser = @UserName
	INTO #I
	FROM (
		SELECT OrderDate
			, SettlementFactorID, SettlementUomID, SettlementUnits
			, CommodityPurchasePricebookID = OPPP.ID
			, CommodityPurchasePricebookPrice = OPPP.Price
			, CommodityPurchasePricebookDeduct = OPPP.Deduct
			, CommodityPurchasePricebookPremium = OPPP.Premium
            , CommodityPurchasePricebookUomID = OPPP.UomID
		FROM (
			SELECT O.ID
				, O.OriginID
				, O.CarrierID
				, O.RouteID
				, SU.SettlementFactorID
				, SU.SettlementUomID
				, SettlementUnits = CASE WHEN O.Rejected = 1 THEN 0 ELSE SU.SettlementUnits END
				, OrderDate = isnull(O.OrderDate, O.DueDate)
				, O.Rejected
			FROM dbo.viewOrder O
			JOIN @SettlementUnits SU ON SU.OrderID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
			WHERE O.ID = @ID
		) X
		OUTER APPLY dbo.fnOrderProducerPurchasePricebook(@ID) OPPP
	) X2

	DECLARE @CreatedTran bit = CASE WHEN @@TRANCOUNT = 0 THEN 1 ELSE 0 END
	BEGIN TRY
		IF (@CreatedTran = 1)
			BEGIN TRAN

		-- remove the existing settlment record (if any)
		DELETE FROM tblOrderSettlementProducer WHERE OrderID = @ID
	
		-- all Units and Rates are first normalized to the Order.OriginUOM then consistent processing
		-- and persisted in the Invoice (Settlement) record in this Order.OriginUOM 
		INSERT INTO tblOrderSettlementProducer 
			(OrderID
			, OrderDate
			, SettlementFactorID 
			, SettlementUomID 
			, SettlementUnits
			, CommodityPurchasePricebookID
			, CommodityPurchasePricebookPriceAmount
			, CommodityPurchasePricebookDeductAmount
			, CommodityPurchasePricebookPremiumAmount
			, CommodityPurchasePricebookTotalAmount
			, CreatedByUser)
		SELECT OrderID
			, OrderDate
			, SettlementFactorID 
			, SettlementUomID 
			, SettlementUnits
			, CommodityPurchasePricebookID
			, CommodityPurchasePricebookPriceAmount = CommodityPurchasePricebookPrice * dbo.fnConvertUOM(SettlementUnits, SettlementUomID, CommodityPurchasePricebookUomID)
			, CommodityPurchasePricebookDeductAmount = CommodityPurchasePricebookDeduct * dbo.fnConvertUOM(SettlementUnits, SettlementUomID, CommodityPurchasePricebookUomID)
			, CommodityPurchasePricebookPremiumAmount = CommodityPurchasePricebookPremium * dbo.fnConvertUOM(SettlementUnits, SettlementUomID, CommodityPurchasePricebookUomID)
			, CommodityPurchasePricebookTotalAmount = CommodityPurchasePricebookPrice * dbo.fnConvertUOM(SettlementUnits, SettlementUomID, CommodityPurchasePricebookUomID)
													+ CommodityPurchasePricebookDeduct * dbo.fnConvertUOM(SettlementUnits, SettlementUomID, CommodityPurchasePricebookUomID)
													+ CommodityPurchasePricebookPremium * dbo.fnConvertUOM(SettlementUnits, SettlementUomID, CommodityPurchasePricebookUomID)
			, CreatedByUser
		FROM #I

		IF (@CreatedTran = 1)
			COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @msg varchar(max), @severity int
		SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY()
		ROLLBACK
		RAISERROR(@msg, @severity, 1)
		RETURN
	END CATCH	
	
END

GO



/****************************************/
-- Date Created: 31 Dec 2014
-- Author: Kevin Alons
-- Purpose: apply both Shipper & Carrier rates (in the appropriate order) 
-- Changes:
--		- 3.11.17.1 - JAE - 3/31/2016 - Added producer (should probably be renamed)
/****************************************/
ALTER PROCEDURE spApplyRatesBoth(@ID int, @UserName varchar(255), @ResetOverrides bit = 0) AS
BEGIN
	BEGIN TRY
		EXEC spApplyRatesShipper @ID = @ID, @UserName = @UserName, @ResetOverrides = @ResetOverrides
		EXEC spApplyRatesCarrier @ID = @ID, @UserName = @UserName, @ResetOverrides = @ResetOverrides
		EXEC spApplyRatesProducer @ID = @ID, @UserName = @UserName
	END TRY
	BEGIN CATCH
		DECLARE @msg varchar(max), @severity int
		SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY()
		RAISERROR(@msg, @severity, 1)
	END CATCH
END

GO


-- there is a function spReRateXXXBatch but doesn't seem to be a need for it, omitting for producer

GO


/***********************************
-- Date Created: 3/31/2016
-- Author: Joe Engler
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
--          fields passed in are web filters, not best match criteria
-- Changes:
***********************************/
CREATE PROCEDURE spRetrieveOrdersFinancialProducer
(
  @StartDate datetime = NULL
, @EndDate datetime = NULL
, @ShipperID int = -1 -- all
, @CarrierID int = -1 -- all
, @ProductGroupID int = -1 -- all 
, @TruckTypeID int = -1 -- all
, @DriverGroupID int = -1 -- all
, @OriginStateID int = -1 -- all 
, @DestStateID int = -1 -- all
, @ProducerID int = -1 -- all
, @BatchID int = NULL  -- either show unbatched (NULL), or the specified batch orders
) AS BEGIN

	SELECT @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	
	SELECT DISTINCT OE.* 
		, Approved = cast(ISNULL(OA.Approved, 0) as bit)
	FROM dbo.viewOrder_Financial_Producer OE
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = OE.ID
	WHERE ID IN (
		SELECT O.ID
		FROM viewOrder O
		JOIN tblProduct P ON P.ID = O.ProductID
		JOIN tblTruck T ON T.ID = O.OriginTruckID
		JOIN tblOrigin OO ON OO.ID = O.OriginID
		JOIN tblDestination D ON D.ID = O.DestinationID
		LEFT JOIN tblOrderSettlementProducer OSP ON OSP.OrderID = O.ID
		WHERE O.StatusID IN (4)  
		  AND O.DeleteDateUTC IS NULL  -- 3.9.34
		  AND (@ShipperID=-1 OR O.CustomerID=@ShipperID) 
		  AND (@CarrierID=-1 OR O.CarrierID=@CarrierID) 
		  AND (@ProductGroupID=-1 OR P.ProductGroupID=@ProductGroupID) 
		  AND (@TruckTypeID=-1 OR T.TruckTypeID=@TruckTypeID)
		  AND (@DriverGroupID=-1 OR O.OriginDriverGroupID=@DriverGroupID) 
		  AND (@OriginStateID=-1 OR OO.StateID=@OriginStateID) 
		  AND (@DestStateID=-1 OR D.StateID=@DestStateID) 
		  AND (@ProducerID=-1 OR O.ProducerID=@ProducerID) 
		  AND (@StartDate IS NULL OR O.OrderDate >= @StartDate) 
		  AND (@EndDate IS NULL OR O.OrderDate <= @EndDate)
		  AND ((@BatchID IS NULL AND OSP.BatchID IS NULL) OR OSP.BatchID = @BatchID)
	)
	ORDER BY OE.OriginDepartTimeUTC
END

GO


/***********************************/
-- Date Created: 3/31/2016
-- Author: Joe Engler
-- Purpose: Compute and add the various Commodity "Settlement" $$ values to an Audited order
/***********************************/
CREATE PROCEDURE spCreateProducerSettlementBatch
(
  @ProducerID int
, @InvoiceNum varchar(50)
, @Notes varchar(255)
, @UserName varchar(255)
, @BatchID int = NULL output
, @BatchNum int = NULL output
) AS BEGIN
	INSERT INTO dbo.tblProducerSettlementBatch(ProducerID, InvoiceNum, BatchNum, BatchDate, Notes, CreateDateUTC, CreatedByUser)
		SELECT @ProducerID
			, @InvoiceNum
			, isnull(max(BatchNum), 0) + 1
			, dbo.fnDateOnly(getdate()), @Notes, getutcdate(), @UserName 
		FROM dbo.tblProducerSettlementBatch
		
		SELECT @BatchID = scope_identity()
		SELECT @BatchNum = BatchNum FROM dbo.tblProducerSettlementBatch WHERE ID = @BatchID
END

GO

/***********************************/
-- Date Created: 14 Apr 2016
-- Author: Joe Engler (happy birthday)
-- Purpose: compute and add the various Producer "Settlement" $$ values to an Audited order
/***********************************/
CREATE PROCEDURE spOrderSettlementProducerAddToBatch
(
  @OrderID int
, @BatchID int
) AS BEGIN
	-- this will only update an order that has had prices applied
	UPDATE tblOrderSettlementProducer SET BatchID = @BatchID WHERE OrderID = @OrderID
END


GO

--exec _spRebuildAllObjects
GO



COMMIT 
SET NOEXEC OFF