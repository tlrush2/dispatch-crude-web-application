SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.5.23'
SELECT  @NewVersion = '4.5.23.1'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Fix to origin first arrive (suppress accepts) and other wait time fixes'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/**********************************************************/
-- Author: Kevin Alons
-- Date Created: 8 Oct 2014
-- Purpose: JOIN the DriverLocation table with the Order Table to find the most suitable Origin Arrival GPS record
-- Changes:
-- 3.11.14 - 2016/04/18 - JAE		- Added top 1 to ensure only one record is selected (sync can sometimes send duplicates)
-- 3.11.15.1 - 2016/04/20 - JAE	- Undo top 1 --NEED TO INVESTIGATE WHY THIS WOULD BE AN ISSUE
-- 3.11.18.1 - 2016/05/03 - KDA	- stop using UID but instead use ID for performance reasons
-- 3.11.20.1 - 2016/05/04 - JAE & BB - undo last change of UID > ID due to timeout/syncing errors
-- 4.1.20.1	- 2016.10.15 - KDA	- add NOLOCK query hint to avoid db contention (this is a WRITE ONLY table anyway)
-- 4.5.23.1	- 2017/04/18 - JAE	- Explicitly list valid origin location types
/***********************************************************/
ALTER VIEW viewDriverLocation_OriginFirstArrive AS
SELECT DL3.*
FROM tblDriverLocation DL3
JOIN (
	-- get the first matching record (by lowest UID)
	SELECT DL2.OrderID, DL2.OriginID, UID = min(cast(DL2.UID as varchar(100)))
	FROM tblDriverLocation DL2
	JOIN (
		-- get the best (lowest) SourceAccuracyMeters
		SELECT DL.OrderID, DL.OriginID, X.SourceDateUTC, SourceAccuracyMeters = min(DL.SourceAccuracyMeters)
		FROM tblDriverLocation DL
		JOIN (
			-- get the first SourceDateUTC
			SELECT OrderID, OriginID, SourceDateUTC = MIN(SourceDateUTC)
			FROM tblDriverLocation WITH (NOLOCK)
			WHERE OriginID IS NOT NULL AND LocationTypeID IN (2, 3, 6, 8)
			GROUP BY OrderID, OriginID
		) X ON X.OrderID = DL.OrderID
			AND X.OriginID = DL.OriginID
			AND X.SourceDateUTC = DL.SourceDateUTC
		GROUP BY DL.OrderID, DL.OriginID, X.SourceDateUTC
	) X2 ON X2.OrderID = DL2.OrderID AND X2.OriginID = DL2.OriginID 
		AND X2.SourceDateUTC = DL2.SourceDateUTC AND X2.SourceAccuracyMeters = DL2.SourceAccuracyMeters
	GROUP BY DL2.OrderID, DL2.OriginID
) X3 ON X3.OrderID = DL3.OrderID AND X3.OriginID = DL3.OriginID AND X3.UID = DL3.UID


GO


/**********************************************************/
-- Author: Kevin Alons
-- Date Created: 8 Oct 2014
-- Purpose: JOIN the DriverLocation table with the Order Table to find the most suitable Destination Arrival GPS record
-- Changes:
-- 3.11.14 - 2016/04/18 - JAE		- Added top 1 to ensure only one record is selected (sync can sometimes send duplicates)
-- 3.11.15.1 - 2016/04/20 - JAE	- Undo top 1
-- 3.11.18.1 - 2016/05/03 - KDA	- stop using UID but instead use ID for performance reasons
-- 3.11.20.1 - 2016/05/04 - JAE & BB - undo last change of UID > ID due to timeout/syncing errors
-- 4.1.20.1	- 2016.10.15 - KDA	- add NOLOCK query hint to avoid db contention (this is a WRITE ONLY table anyway)
-- 4.5.23.1	- 2017/04/18 - JAE	- Explicitly list valid destination location types
/***********************************************************/
ALTER VIEW viewDriverLocation_DestinationFirstArrive AS
SELECT DL3.*
FROM tblDriverLocation DL3
JOIN (
	-- get the first matching record (by lowest UID)
	SELECT DL2.OrderID, DL2.DestinationID, UID = min(cast(DL2.UID as varchar(100)))
	FROM tblDriverLocation DL2
	JOIN (
		-- get the best (lowest) SourceAccuracyMeters
		SELECT DL.OrderID, DL.DestinationID, X.SourceDateUTC, SourceAccuracyMeters = min(DL.SourceAccuracyMeters)
		FROM tblDriverLocation DL
		JOIN (
			-- get the first SourceDateUTC
			SELECT OrderID, DestinationID, SourceDateUTC = MIN(SourceDateUTC)
			FROM tblDriverLocation WITH (NOLOCK)
			WHERE DestinationID IS NOT NULL AND LocationTypeID IN (2, 3, 7, 8)
			GROUP BY OrderID, DestinationID
		) X ON X.OrderID = DL.OrderID
			AND X.DestinationID = DL.DestinationID
			AND X.SourceDateUTC = DL.SourceDateUTC
		GROUP BY DL.OrderID, DL.DestinationID, X.SourceDateUTC
	) X2 ON X2.OrderID = DL2.OrderID AND X2.DestinationID = DL2.DestinationID 
		AND X2.SourceDateUTC = DL2.SourceDateUTC AND X2.SourceAccuracyMeters = DL2.SourceAccuracyMeters
	GROUP BY DL2.OrderID, DL2.DestinationID
) X3 ON X3.OrderID = DL3.OrderID AND X3.DestinationID = DL3.DestinationID AND X3.UID = DL3.UID


GO


/********************************************
-- Author:		Joe Engler
-- Create date: 8/1/2016
-- Description:	Return suspicious orders with a billable wait time (for carrier, at both origin and destination) that exceed a 
					given minute or mile limit
-- Changes:
--		4.5.23.1		JAE			2017-04-19			Added flag to include/supress orders without billable waits
*********************************************/
ALTER FUNCTION fnOrdersBadDemurrage (@StartDate DATETIME, @EndDate DATETIME, @LIMIT_MINUTES INT = 15, @LIMIT_MILES INT = 10, @INCLUDE_NONBILLABLE_WAITS INT = 0) 
RETURNS TABLE AS RETURN
SELECT

-- negative is entered early, positive is entered later
OriginArriveDelta = datediff(minute,OriginArriveTimeUTC,OriginArriveActualTimeUTC),
OriginDepartDelta = datediff(minute,OriginDepartTimeUTC, OriginDepartActualTimeUTC),
OriginPickupDelta = datediff(minute, origindeparttimeutc, OriginPickupActualTimeUTC),
DestArriveDelta = datediff(minute,DestArriveTimeUTC,DestArriveActualTimeUTC),
DestDepartDelta = datediff(minute,DestDepartTimeUTC, DestDepartActualTimeUTC),
DestDeliverDelta = datediff(minute,DestDepartTimeUTC, DestDeliverActualTimeUTC),
q.*
FROM (
	SELECT o.ID, OrderNum, o.DriverID, o.CarrierID, o.OrderDate, Driver = dr.LastName+ ', ' + dr.FirstName,

		AcceptActualTimeUTC = a.CreateDateUTC,
		AcceptActualDist = dbo.fnDistance(dbo.fnPointFromLatLon(a.Lat, a.Lon), dbo.fnPointFromLatLon(oo.Lat, oo.Lon), 'MILE'),

		OriginArriveTimeUTC, 
		OriginArriveActualTimeUTC = oa.CreateDateUTC,
		OriginArriveActualDist = dbo.fnDistance(dbo.fnPointFromLatLon(oa.Lat, oa.Lon), dbo.fnPointFromLatLon(oo.Lat, oo.Lon), 'MILE'),

		OriginDepartTimeUTC,
		OriginDepartActualTimeUTC = od.CreateDateUTC,
		OriginDepartActualDist = dbo.fnDistance(dbo.fnPointFromLatLon(od.Lat, od.Lon), dbo.fnPointFromLatLon(oo.Lat, oo.Lon), 'MILE'),

		OriginPickupActualTimeUTC = op.CreateDateUTC,
		OriginPickupActualDist = dbo.fnDistance(dbo.fnPointFromLatLon(op.Lat, op.Lon), dbo.fnPointFromLatLon(oo.Lat, oo.Lon), 'MILE'),

		DestArriveTimeUTC, 
		DestArriveActualTimeUTC = da.CreateDateUTC,
		DestArriveActualDist = dbo.fnDistance(dbo.fnPointFromLatLon(da.Lat, da.Lon), dbo.fnPointFromLatLon(d.Lat, d.Lon), 'MILE'),

		DestDepartTimeUTC,
		DestDepartActualTimeUTC = dd.CreateDateUTC,
		DestDepartActualDist = dbo.fnDistance(dbo.fnPointFromLatLon(dd.Lat, dd.Lon), dbo.fnPointFromLatLon(d.Lat, d.Lon), 'MILE'),

		DestDeliverActualTimeUTC = ddel.CreateDateUTC,
		DestDeliverActualDist = dbo.fnDistance(dbo.fnPointFromLatLon(ddel.Lat, ddel.Lon), dbo.fnPointFromLatLon(d.Lat, d.Lon), 'MILE')

	FROM tblOrder o
	JOIN tblOrigin oo ON o.OriginID = oo.ID
	JOIN tblDestination d ON o.DestinationID = d.ID
	JOIN tblDriver dr ON o.DriverID = dr.ID
	OUTER APPLY (select top 1 * from tbldriverlocation dl where o.id=dl.orderid and LocationTypeID = 9 order by SourceDateUTC DESC, SourceAccuracyMeters) as a
	OUTER APPLY (select top 1 * from tbldriverlocation dl where o.id=dl.orderid and dl.originid=o.originid and LocationTypeID = 2 order by SourceDateUTC DESC, SourceAccuracyMeters) as oa
	OUTER APPLY (select top 1 * from tbldriverlocation dl where o.id=dl.orderid and dl.originid=o.originid and LocationTypeID = 3 order by SourceDateUTC, SourceAccuracyMeters) as od
	OUTER APPLY (select top 1 * from tbldriverlocation dl where o.id=dl.orderid and dl.originid=o.originid and LocationTypeID = 6 order by SourceDateUTC, SourceAccuracyMeters) as op 
	OUTER APPLY (select top 1 * from tbldriverlocation dl where o.id=dl.orderid and dl.destinationid=o.destinationid and LocationTypeID = 2 order by SourceDateUTC DESC, SourceAccuracyMeters) as da
	OUTER APPLY (select top 1 * from tbldriverlocation dl where o.id=dl.orderid and dl.destinationid=o.destinationid and LocationTypeID = 3 order by SourceDateUTC, SourceAccuracyMeters) as dd
	OUTER APPLY (select top 1 * from tbldriverlocation dl where o.id=dl.orderid and dl.destinationid=o.destinationid and LocationTypeID = 7 order by SourceDateUTC, SourceAccuracyMeters) as ddel
	WHERE o.OrderDate BETWEEN @StartDate AND @EndDate
) q
LEFT JOIN tblOrderSettlementCarrier OSC ON OSC.OrderID = q.ID

WHERE (     (OSC.OriginWaitAmount > 0 OR @INCLUDE_NONBILLABLE_WAITS = 1)
		AND (  ABS(DATEDIFF(MINUTE,OriginArriveTimeUTC,OriginArriveActualTimeUTC) ) > @LIMIT_MINUTES
			OR (ABS(DATEDIFF(MINUTE,OriginDepartTimeUTC, OriginDepartActualTimeUTC)) > @LIMIT_MINUTES 
					AND ABS(DATEDIFF(MINUTE,OriginDepartTimeUTC, OriginPickupActualTimeUTC)) > @LIMIT_MINUTES)
			OR OriginArriveActualDist > @LIMIT_MILES
			OR (OriginDepartActualDist > @LIMIT_MILES AND OriginPickupActualDist > @LIMIT_MILES)))
	OR (    (OSC.DestinationWaitAmount > 0 OR @INCLUDE_NONBILLABLE_WAITS = 1)
		AND (ABS(DATEDIFF(MINUTE,DestArriveTimeUTC,DestArriveActualTimeUTC) ) > @LIMIT_MINUTES
			OR (ABS(DATEDIFF(MINUTE,DestDepartTimeUTC, DestDepartActualTimeUTC)) > @LIMIT_MINUTES
					AND ABS(DATEDIFF(MINUTE,DestDepartTimeUTC, DestDeliverActualTimeUTC)) > @LIMIT_MINUTES) 
			OR DestArriveActualDist > @LIMIT_MILES
			OR (DestDepartActualDist > @LIMIT_MILES AND DestDeliverActualDist > @LIMIT_MILES)))


GO


COMMIT
SET NOEXEC OFF