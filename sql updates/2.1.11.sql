/* update Tax Forms queries to be dynamic & to allow state/product filtering
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.1.10'
SELECT  @NewVersion = '2.1.11'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

-- DO THE ACTUAL UPDATE HERE
/***********************************/

CREATE TABLE tblCustomerTaxReport
(
  ID int identity (1, 1) NOT NULL CONSTRAINT PK_CustomerTaxReport PRIMARY KEY
, Name varchar(25) NOT NULL
, StateID int NULL CONSTRAINT FK_CustomerTaxReport_State FOREIGN KEY REFERENCES tblState(ID)
, ReportSource varchar(255) NOT NULL
)
GO

GRANT SELECT, UPDATE, INSERT, DELETE ON tblCustomerTaxReport TO dispatchcrude_iis_acct
GO

CREATE UNIQUE INDEX udxCustomerTaxReport_Name ON tblCustomerTaxReport(Name)
GO

INSERT INTO tblCustomerTaxReport (Name, StateID, ReportSource)
	SELECT 'ND 10A', ID, 'spReport_Tax_ND10A' FROM tblState WHERE Abbreviation = 'ND'
	UNION SELECT 'ND 10B', ID, 'spReport_Tax_ND10B' FROM tblState WHERE Abbreviation = 'ND'
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 20 Mar 2013
-- Description:	return the data for the TAX_ND_10A report (for the specified date range)
-- =============================================
ALTER PROCEDURE [dbo].[spReport_Tax_ND10A]
(
  @CustomerID int = -1
, @ProductID int = -1
, @StateID int = -1
, @StartDate datetime
, @EndDate datetime
, @IncludeUninvoiced bit = 0
)
AS BEGIN
	SELECT @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	
	SELECT O.FieldName
		, O.Operator
		, O.LeaseNum AS LeaseNumber
		, O.Name AS [Well Name and Number]
		, O.CTBNum AS [NDIC CTB No.]
		, cast(round(SUM(OD.OriginNetBarrels), 0) as int) AS [Lease Total (Bbls)]
	FROM viewOrigin O
	JOIN viewOrder OD ON OD.OriginID = O.ID
	JOIN tblOrderInvoiceCustomer IC ON IC.OrderID = OD.ID AND (@IncludeUninvoiced = 1 OR IC.BatchID IS NOT NULL)
	WHERE O.OriginTypeID IN (2) -- WELLs ONLY
		AND OD.DeleteDateUTC IS NULL
		AND (@CustomerID = -1 OR OD.CustomerID = @CustomerID)
		AND (@ProductID = -1 OR OD.ProductID = @ProductID)
		AND (@StateID = -1 OR O.StateID = @StateID)
		AND OD.OrderDate BETWEEN @StartDate AND @EndDate
	GROUP BY O.FieldName, O.Operator, O.LeaseNum, O.Name, O.CTBNum
	ORDER BY O.FieldName, O.Operator, O.LeaseNum, O.Name, O.CTBNum
	END

GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 20 Mar 2013
-- Description:	return the data for the TAX_ND_10B report (for the specified date range)
-- =============================================
ALTER PROCEDURE [dbo].[spReport_Tax_ND10B]
(
  @CustomerID int = -1
, @ProductID int = -1
, @StateID int = -1
, @StartDate datetime
, @EndDate datetime
, @IncludeUninvoiced bit = 0
)
AS BEGIN
	SELECT @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	
	SELECT O.Name AS [Point Received]
		, OD.Customer AS [Purchaser]
		, OD.Destination
		, cast(round(SUM(OD.OriginNetBarrels), 0) as int) AS [Lease Total (Bbls)]
	FROM viewOrder OD
	JOIN tblOrigin O ON O.ID = OD.OriginID
	JOIN tblOrderInvoiceCustomer IC ON IC.OrderID = OD.ID AND (@IncludeUninvoiced = 1 OR IC.BatchID IS NOT NULL)
	WHERE O.OriginTypeID IN (2) -- WELLs ONLY
		AND OD.DeleteDateUTC IS NULL
		AND (@CustomerID = -1 OR OD.CustomerID = @CustomerID)
		AND (@ProductID = -1 OR OD.ProductID = @ProductID)
		AND (@StateID = -1 OR O.StateID = @StateID)
		AND OD.OrderDate BETWEEN @StartDate AND @EndDate
	GROUP BY O.Name, OD.Customer, OD.Destination
	ORDER BY O.Name, OD.Customer, OD.Destination
	END

GO

COMMIT
SET NOEXEC OFF