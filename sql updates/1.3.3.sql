BEGIN TRAN

UPDATE tblSetting SET Value = '1.3.3' WHERE ID=0
GO

/***********************************/
-- Date Created: 26 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Drivers table records with "friendly" translated values included
/***********************************/
ALTER VIEW [dbo].[viewDriver] AS
SELECT *
	, CASE WHEN DeleteDate IS NULL THEN '' ELSE 'Deleted: ' END + FullName AS FullNameD
	, CASE WHEN DeleteDate IS NULL THEN 1 ELSE 0 END AS Active
FROM (
	SELECT D.*
	, D.FirstName + ' ' + D.LastName As FullName
	, D.LastName + ', ' + D.FirstName AS FullNameLF
	, C.Name AS Carrier
	, S.Abbreviation AS StateAbbrev 
	, T.FullName AS Truck
	, T1.FullName AS Trailer
	, T2.FullName AS Trailer2
	FROM dbo.tblDriver D 
	JOIN dbo.tblCarrier C ON C.ID = D.CarrierID 
	LEFT JOIN tblState S ON S.ID = D.StateID
	LEFT JOIN viewTruck T ON T.ID = D.TruckID
	LEFT JOIN viewTrailer T1 ON T1.ID = D.TrailerID
	LEFT JOIN viewTrailer T2 ON T2.ID = D.Trailer2ID
) X

GO

COMMIT