/* fix DriverApp.Sync errors
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.1.2', @NewVersion = '2.1.3'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

/*******************************************/
-- Date Created: 25 Apr 2013
-- Author: Kevin Alons
-- Purpose: return OrderTicket data for Driver App sync
/*******************************************/
ALTER FUNCTION [dbo].[fnOrderTicket_DriverApp]( @DriverID int, @LastChangeDateUTC datetime ) RETURNS  TABLE AS
RETURN 
	SELECT OT.ID
		, OT.UID
		, OT.OrderID
		, OT.CarrierTicketNum
		, OT.TankNum
		, OT.TicketTypeID
		, OT.TankTypeID
		, OT.ProductObsGravity
		, OT.ProductObsTemp
		, OT.ProductHighTemp
		, OT.ProductLowTemp
		, OT.ProductBSW
		, cast(OT.OpeningGaugeFeet as int) AS OpeningGaugeFeet
		, cast(OT.OpeningGaugeInch as int) AS OpeningGaugeInch
		, cast(OT.OpeningGaugeQ as int) AS OpeningGaugeQ
		, cast(OT.ClosingGaugeFeet as int) AS ClosingGaugeFeet
		, cast(OT.ClosingGaugeInch as int) AS ClosingGaugeInch
		, cast(OT.ClosingGaugeQ as int) AS ClosingGaugeQ
		, OT.GrossBarrels
		, OT.NetBarrels
		, OT.Rejected
		, OT.RejectNotes
		, OT.SealOff
		, OT.SealOn
		, OT.BOLNum
		, OT.BarrelsPerInch
		, OT.CreateDateUTC
		, OT.CreatedByUser
		, OT.LastChangeDateUTC
		, OT.LastChangedByUser
		, OT.DeleteDateUTC
		, OT.DeletedByUser
		, cast(CASE WHEN OT.DeleteDateUTC IS NULL THEN 0 ELSE 1 END as bit) AS Deleted
	FROM dbo.tblOrderTicket OT
	JOIN dbo.tblOrder O ON O.ID = OT.OrderID
	WHERE (StatusID IN (2, 7, 8) -- Dispatched, Accepted, Picked Up
		-- or status = Delivered and the driver still has access to the record
		OR (StatusID = 3 AND O.DestDepartTimeUTC < DATEADD(hour, (SELECT cast(value as int) FROM tblSetting WHERE ID = 10), GETUTCDATE())))
	  AND O.DriverID = @DriverID
	  AND (@LastChangeDateUTC IS NULL OR O.LastChangeDateUTC >= @LastChangeDateUTC)

GO

COMMIT
SET NOEXEC OFF