DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '1.3.5', @NewVersion = '1.3.6'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRAN X

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

/***********************************/
-- Date Created: 1 Jun 2013
-- Author: Kevin Alons
-- Purpose: add a new CustomerRouteRate
/***********************************/
ALTER PROCEDURE [dbo].[spCustomerRouteRate_Add]
(
  @CustomerID int
, @RouteID int
, @Rate smallmoney
, @EffectiveDate smalldatetime
, @ReplaceMatching bit
, @ReplaceAllOverlapping bit
, @UserName varchar(100)
, @Success bit = NULL out
, @Message varchar(255) = NULL out
) AS 
BEGIN
	DECLARE @removed int

	IF (@ReplaceMatching = 0)
	BEGIN
		IF ((SELECT count(1) FROM tblCustomerRouteRates WHERE CustomerID=@CustomerID AND RouteID=@RouteID AND EffectiveDate=@EffectiveDate) > 0)
		BEGIN
			SELECT @Success = 0, @Message = 'Skipped: Existing Route Rate already exists'
			RETURN
		END
	END
	ELSE
	BEGIN
		-- remove any matching/overlapping rate records
		IF (@ReplaceAllOverlapping = 1)
		BEGIN
			SELECT @removed = count(1) FROM tblCustomerRouteRates WHERE CustomerID=@CustomerID AND RouteID=@RouteID AND EffectiveDate>@EffectiveDate
			DELETE FROM tblCustomerRouteRates WHERE ID IN (
				SELECT ID FROM viewCustomerRouteRates WHERE CustomerID=@CustomerID AND RouteID=@RouteID AND EffectiveDate>@EffectiveDate)
		END
		ELSE
		BEGIN
			SELECT @removed = count(1) FROM tblCustomerRouteRates WHERE CustomerID=@CustomerID AND RouteID=@RouteID AND EffectiveDate=@EffectiveDate
			DELETE FROM tblCustomerRouteRates WHERE CustomerID=@CustomerID AND RouteID=@RouteID AND EffectiveDate=@EffectiveDate
		END
	END
	
	-- do the actual insert now
	INSERT INTO tblCustomerRouteRates (CustomerID, RouteID, Rate, EffectiveDate, CreateDate, CreatedByUser) 
		VALUES (@CustomerID, @RouteID, @Rate, @EffectiveDate, GETDATE(), @UserName)
	SELECT @success = 1, @Message = 'Rate added successfully' + isnull(' [' + ltrim(@removed) + ' replaced]', '')
END

GO

/***********************************/
-- Date Created: 1 Jun 2013
-- Author: Kevin Alons
-- Purpose: add a new CarrierRouteRate
/***********************************/
ALTER PROCEDURE [dbo].[spCarrierRouteRate_Add]
(
  @CarrierID int
, @RouteID int
, @Rate smallmoney
, @EffectiveDate smalldatetime
, @ReplaceMatching bit
, @ReplaceAllOverlapping bit
, @UserName varchar(100)
, @Success bit = NULL out
, @Message varchar(255) = NULL out
) AS 
BEGIN
	DECLARE @removed int

	IF (@ReplaceMatching = 0)
	BEGIN
		IF ((SELECT count(1) FROM tblCarrierRouteRates WHERE CarrierID=@CarrierID AND RouteID=@RouteID AND EffectiveDate=@EffectiveDate) > 0)
		BEGIN
			SELECT @Success = 0, @Message = 'Skipped: Existing Route Rate already exists'
			RETURN
		END
	END
	ELSE
	BEGIN
		-- remove any matching/overlapping rate records
		IF (@ReplaceAllOverlapping = 1)
		BEGIN
			SELECT @removed = count(1) FROM tblCarrierRouteRates WHERE CarrierID=@CarrierID AND RouteID=@RouteID AND EffectiveDate>@EffectiveDate
			DELETE FROM tblCarrierRouteRates WHERE ID IN (
				SELECT ID FROM viewCarrierRouteRates WHERE CarrierID=@CarrierID AND RouteID=@RouteID AND EffectiveDate>@EffectiveDate)
		END
		ELSE
		BEGIN
			SELECT @removed = count(1) FROM tblCarrierRouteRates WHERE CarrierID=@CarrierID AND RouteID=@RouteID AND EffectiveDate=@EffectiveDate
			DELETE FROM tblCarrierRouteRates WHERE CarrierID=@CarrierID AND RouteID=@RouteID AND EffectiveDate=@EffectiveDate
		END
	END
	
	-- do the actual insert now
	INSERT INTO tblCarrierRouteRates (CarrierID, RouteID, Rate, EffectiveDate, CreateDate, CreatedByUser) 
		VALUES (@CarrierID, @RouteID, @Rate, @EffectiveDate, GETDATE(), @UserName)
	SELECT @success = 1, @Message = 'Rate added successfully' + isnull(' [' + ltrim(@removed) + ' replaced]', '')
END

GO

COMMIT
GO

SET NOEXEC OFF