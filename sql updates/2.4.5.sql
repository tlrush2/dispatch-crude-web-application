/* 
	minor fixes to spCreateLoads and tblOrder trigger error
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.4.4'
SELECT  @NewVersion = '2.4.5'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

/***********************************/
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: create new Order (loads) for the specified criteria
/***********************************/
ALTER PROCEDURE [dbo].[spCreateLoads]
(
  @OriginID int
, @DestinationID int
, @TicketTypeID int
, @DueDate datetime
, @CustomerID int
, @CarrierID int = NULL
, @DriverID int = NULL
, @Qty int
, @UserName varchar(100)
, @TankNum varchar(20) = NULL
, @PriorityID int = 3 -- LOW priority
, @StatusID smallint = -10 -- GENERATED
, @ProductID int = 1 -- basic Crude Oil
) AS
BEGIN
	DECLARE @PumperID int, @OperatorID int, @ProducerID int, @OriginUomID int, @DestUomID int
	SELECT @PumperID = PumperID, @OperatorID = OperatorID, @ProducerID = ProducerID, @OriginUomID = UomID
	FROM tblOrigin 
	WHERE ID = @OriginID
	SELECT @DestUomID = UomID FROM tblDestination WHERE ID = @DestinationID

	DECLARE @i int
	SELECT @i = 0
	
	WHILE @i < @Qty BEGIN
		
		INSERT INTO dbo.tblOrder (OriginID, OriginUomID, DestinationID, DestUomID, TicketTypeID, DueDate, CustomerID
				, CarrierID, DriverID, StatusID, PriorityID, OriginTankNum
				, OrderNum, ProductID, PumperID, OperatorID, ProducerID, CreateDateUTC, CreatedByUser)
			VALUES (@OriginID, @OriginUomID, @DestinationID, @DestUomID, @TicketTypeID, @DueDate, @CustomerID
				, @CarrierID, @DriverID, @StatusID, @PriorityID, @TankNum
				, (SELECT isnull((SELECT max(OrderNum) FROM tblOrder), 100000) + 1), @ProductID
				, @PumperID, @OperatorID, @ProducerID, GETUTCDATE(), @UserName)
		
		IF (@DriverID IS NOT NULL)
		BEGIN
			UPDATE tblOrder SET TruckID = D.TruckID, TrailerID = D.TrailerID, Trailer2ID = D.Trailer2ID
			FROM tblOrder O
			JOIN tblDriver D ON D.ID = O.DriverID
			WHERE O.ID = SCOPE_IDENTITY()
		END
		
		SET @i = @i + 1
	END
END

GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 19 Dec 2012
-- Description:	trigger to add a unique, incrementing OrderNum to each new Order (manual Identity column)
--				and other supporting/coordinating logic
-- =============================================
ALTER TRIGGER [dbo].[trigOrder_IU] ON [dbo].[tblOrder] AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	WHILE (SELECT count(*) FROM inserted i JOIN tblOrder O ON O.ID = i.ID WHERE O.OrderNum IS NULL) > 0
	BEGIN
		UPDATE tblOrder 
		  SET OrderNum = isnull((SELECT max(OrderNum) FROM tblOrder), 100000) + 1
			, CreateDateUTC = getutcdate()
		WHERE ID = (SELECT min(O.ID) FROM tblOrder O JOIN inserted i ON i.ID = O.ID WHERE O.OrderNum IS NULL)
	END
	
	-- re-compute the OriginMinutes (in case the website failed to compute it properly)
	IF UPDATE(OriginArriveTimeUTC) OR UPDATE(OriginDepartTimeUTC)
	BEGIN
		UPDATE tblOrder SET OriginMinutes = datediff(minute, OriginArriveTimeUTC, OriginDepartTimeUTC)
	END
	-- re-compute the DestWaitMinutes (in case the website failed to compute it properly)
	IF UPDATE(DestArriveTimeUTC) OR UPDATE(DestDepartTimeUTC)
	BEGIN
		UPDATE tblOrder SET DestMinutes = datediff(minute, DestArriveTimeUTC, DestDepartTimeUTC)
	END
	
	-- ensure missing Routes are created and assigned for the specified Origin/Destination combination
	IF UPDATE(OriginID) OR UPDATE(DestinationID)
	BEGIN
		-- create any missing Route records
		INSERT INTO tblRoute (OriginID, DestinationID, CreateDateUTC, CreatedByUser)
			SELECT i.OriginID, i.DestinationID, GETUTCDATE(), ISNULL(i.LastChangedByUser, i.CreatedByUser)
			FROM inserted i
			LEFT JOIN tblRoute R ON R.OriginID = i.OriginID AND R.DestinationID = i.DestinationID
			WHERE i.OriginID IS NOT NULL AND i.DestinationID IS NOT NULL AND R.ID IS NULL
		-- ensure the Order records refer to the correct Route (ID)
		UPDATE tblOrder SET RouteID = R.ID
		FROM tblOrder O
		JOIN inserted i ON i.ID = O.ID
		LEFT JOIN tblRoute R ON R.OriginID = O.OriginID AND R.DestinationID = O.DestinationID

		-- update the ActualMiles from the related Route
		UPDATE tblOrder SET ActualMiles = R.ActualMiles
		FROM tblOrder O
		JOIN inserted i ON i.ID = O.ID
		JOIN tblRoute R ON R.ID = O.RouteID
	END
	
	IF (UPDATE(OriginID))
	BEGIN
		-- update Order.ProducerID to match what is assigned to the new Origin
		UPDATE tblOrder SET ProducerID = OO.ProducerID
		FROM tblOrder O
		JOIN deleted d ON d.ID = O.ID
		JOIN tblOrigin OO ON OO.ID = O.OriginID
		WHERE d.OriginID <> O.OriginID

		-- update Order.OriginUomID to match what is assigned to the new Origin (quantities are updated below)
		UPDATE tblOrder SET OriginUomID = OO.UomID
		FROM tblOrder O
		JOIN deleted d ON d.ID = O.ID
		JOIN tblOrigin OO ON OO.ID = O.OriginID
		WHERE d.OriginID <> O.OriginID AND d.OriginUomID <> OO.UomID
	END
	
	-- keep the DestUomID in sync with the Destination (units are updated below)
	IF (UPDATE(DestinationID))
	BEGIN
		-- update Order.DestUomID to match what is assigned to the new Destination
		UPDATE tblOrder 
		  SET DestUomID = DD.UomID
		FROM tblOrder O
		JOIN deleted d ON d.ID = O.ID
		JOIN tblDestination DD ON DD.ID = O.DestinationID
		WHERE d.DestinationID <> O.DestinationID AND d.DestUomID <> DD.UomID
	END
	
	-- just updating the tickets here, trigOrderTicket_IU will in turn update this table
	-- Note: the TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU') ) = 1 statement prevents recursion
	IF (TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU')) = 1)
	BEGIN
		-- ensure any existing OrderTickets are recomputed with the new OriginUOM
		UPDATE tblOrderTicket
		  SET GrossUnits = dbo.fnConvertUOM(GrossUnits, d.OriginUomID, i.OriginUomID)
			, NetUnits = dbo.fnConvertUOM(NetUnits, d.OriginUomID, i.OriginUomID)
		FROM tblOrderTicket OT
		JOIN tblOrder i ON i.ID = OT.OrderID
		JOIN deleted d ON d.ID = i.ID
		WHERE i.OriginUomID <> d.OriginUomID

	END
	
	-- recalculate the Destination Unit values if only if the DestUomID changed but the values didn't
	BEGIN
		UPDATE tblOrder
		  SET DestGrossUnits = dbo.fnConvertUOM(O.DestGrossUnits, d.DestUomID, O.DestUomID)
			, DestNetUnits = dbo.fnConvertUOM(O.DestNetUnits, d.DestUomID, O.DestUomID)
		FROM tblOrder O
		JOIN deleted d ON d.ID = O.ID
		WHERE d.DestUomID <> O.DestUomID 
		  AND d.DestGrossUnits = O.DestGrossUnits
		  AND d.DestNetUnits = O.DestNetUnits
	END
	--NOTE: we do not update the DestOpenMeterUnits/DestClosignMeterUnits since they don't auto update
	--  (and they simply represent a reading that is assumed to in the DestUomID UOM at entry)
	--  (if they are updated in the future [manually] the will calculate again using the DestUomID at that time)
END

GO

COMMIT
SET NOEXEC OFF