-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.11.20.4'
SELECT  @NewVersion = '3.12.1'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'DCWEB-1314:  Implement Carrier Rules'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

--------------------------------------------------------------------------------

CREATE TABLE tblCarrierRuleType
(
	ID int NOT NULL CONSTRAINT PK_CarrierRuleType PRIMARY KEY CLUSTERED,
	Name varchar(50) NOT NULL CONSTRAINT udxCarrierRuleType_Name UNIQUE,
	RuleTypeID int NOT NULL CONSTRAINT FK_CarrierRuleType_RuleType REFERENCES tblRuleType(ID),
	ForDriverApp bit NULL CONSTRAINT DF_CarrierRuleType_ForDriverApp DEFAULT 1,
)

GO

INSERT INTO tblCarrierRuleType VALUES
(1, 'Require e-Logs/Shift', 2, 1),
(2, 'Truck Selection Source', 6, 1),
(3, 'Trailer Selection Source', 6, 1),
(4, 'Require DVIR', 2, 1)

GO


CREATE TABLE tblCarrierRuleTypeDropDownValues
(
	CarrierRuleTypeID int NOT NULL CONSTRAINT FK_CarrierRuleTypeDropDownValues_CarrierRuleType REFERENCES tblCarrierRuleType(ID),
	Value varchar(255) NOT NULL,
	SortNum int NOT NULL,
	IsDefault bit NOT NULL,
	InternalData varchar(255) NULL,
	CONSTRAINT PK_CarrierRuleTypeDropDownValues PRIMARY KEY CLUSTERED (
		CarrierRuleTypeID ASC,
		Value ASC
	)
)

GO


INSERT INTO tblCarrierRuleTypeDropDownValues VALUES
(2, 'Use driver defaults', 1, 1, null),
(2, 'Require select', 2, 0, null),
(2, 'Require scan (QR)', 3, 0, null),
(3, 'Use driver defaults', 1, 1, null),
(3, 'Require select', 2, 0, null),
(3, 'Require scan (QR)', 3, 0, null),
(4, 'None', 1, 1, null),
(4, 'Pre', 2, 1, null),
(4, 'Post', 3, 0, null),
(4, 'Weekly', 4, 0, null),
(4, 'Monthly', 5, 0, null),
(4, 'Annually', 6, 0, null)

GO

CREATE TABLE tblCarrierRule(
	ID int IDENTITY(1,1) NOT NULL CONSTRAINT PK_CarrierRule PRIMARY KEY NONCLUSTERED,
	TypeID int NOT NULL CONSTRAINT FK_CarrierRule_Type REFERENCES tblCarrierRuleType(ID),
	CarrierID int NULL CONSTRAINT FK_CarrierRule_Carrier REFERENCES tblCarrier(ID),
	DriverID int NULL CONSTRAINT FK_CarrierRule_Driver REFERENCES tblDriver(ID),
	StateID int NULL CONSTRAINT FK_CarrierRule_State REFERENCES tblState(ID),
	RegionID int NULL CONSTRAINT FK_CarrierRule_DriverRegion REFERENCES tblRegion(ID),
	EffectiveDate date NOT NULL,
	EndDate date NOT NULL,
	Value varchar(255) NULL,
	CreateDateUTC smalldatetime NOT NULL CONSTRAINT DF_CarrierRule_CreateDateUTC DEFAULT (GETUTCDATE()),
	CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_CarrierRule_CreatedByUser DEFAULT (SUSER_NAME()),
	LastChangeDateUTC smalldatetime NULL,
	LastChangedByUser varchar(100) NULL
) 

GO


ALTER TABLE dbo.tblCarrierRule WITH CHECK ADD CONSTRAINT CK_CarrierRule_EndDate_Greater CHECK (EndDate>=EffectiveDate)
GO
ALTER TABLE dbo.tblCarrierRule CHECK CONSTRAINT CK_CarrierRule_EndDate_Greater
GO


/**********************************************
-- Date Created: 2016-05-03
-- Author: Joe Engler
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
-- Changes:
***********************************************/
CREATE TRIGGER trigCarrierRule_IU ON tblCarrierRule AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblCarrierRule X 
			ON i.ID <> X.ID
			  AND i.TypeID = X.TypeID
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.DriverID, X.DriverID) = 1
			  AND dbo.fnCompareNullableInts(i.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping carrier rules are not allowed'
	END

	IF @error IS NOT NULL 
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
END


GO

/******************************************************
-- Date Created: 2016-05-03
-- Author: Joe Engler
-- Purpose: add a computed "EndDate" value to all Carrier rules
******************************************************/
CREATE VIEW viewCarrierRule AS
	SELECT X.*
		, RT.RuleTypeID
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblCarrierRule XN 
			WHERE XN.TypeID = X.TypeID
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverID, X.DriverID) = 1
			  AND dbo.fnCompareNullableInts(XN.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT max(XN.EndDate) 
			FROM tblCarrierRule XN 
			WHERE XN.TypeID = X.TypeID
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverID, X.DriverID) = 1
			  AND dbo.fnCompareNullableInts(XN.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND XN.EndDate < X.EffectiveDate)
	FROM tblCarrierRule X
	JOIN tblCarrierRuleType RT ON RT.ID = X.TypeID

GO


/***************************************************
-- Date Created: 2016-05-03
-- Author: Joe Engler
-- Purpose: return all CarrierRule Value Dropdown values for the specified CarrierRuleID (no records if not relevant)
-- Changes:
***************************************************/
CREATE PROCEDURE spCarrierRuleDropDownValues(@CarrierRuleTypeID int, @defaultValue varchar(255) = NULL OUTPUT) AS
BEGIN
	/* this statement (using SET ) will ensure @defaultValue IS NULL if no default is found */
	SET @defaultValue = (SELECT TOP 1 Value FROM tblCarrierRuleTypeDropDownValues WHERE CarrierRuleTypeID = @CarrierRuleTypeID AND IsDefault = 1)
	SELECT Value
	FROM (
		SELECT Value, SortNum FROM tblCarrierRuleTypeDropDownValues WHERE CarrierRuleTypeID = @CarrierRuleTypeID
		AND EXISTS (SELECT * FROM tblCarrierRuleType WHERE ID = @CarrierRuleTypeID AND RuleTypeID = 6) -- Drop down
		UNION 
		SELECT Value, SortNum FROM (
			SELECT Value = 'True', SortNum = 0 UNION SELECT 'False', 1
		) X WHERE EXISTS (SELECT * FROM tblCarrierRuleType WHERE ID = @CarrierRuleTypeID AND RuleTypeID = 2) -- Boolean
	) X
	ORDER BY SortNum
END

GO


/***********************************
-- Date Created: 2016-05-03
-- Author: Joe Engler
-- Purpose: retrieve and return the Carrier Rule info for the specified order
-- Changes:
***********************************/
CREATE FUNCTION fnCarrierRules(@StartDate date, @EndDate date, @TypeID int, @CarrierID int, @DriverID int, @StateID int, @RegionID int, @BestMatchOnly bit = 0)
RETURNS 
	@ret TABLE (
		ID int
	  , TypeID int
	  , CarrierID int
	  , DriverID int
	  , StateID int
	  , RegionID int
	  , Value varchar(255)
	  , RuleTypeID int
	  , EffectiveDate date
	  , EndDate date
	  , MaxEffectiveDate date
	  , MinEndDate date
	  , NextEffectiveDate date
	  , PriorEndDate date
	  , BestMatch bit
	  , Ranking smallmoney
	  , CreateDateUTC datetime
	  , CreatedByUser varchar(100)
	  , LastChangeDateUTC datetime
	  , LastChangedByUser varchar(100)
	)
AS BEGIN
	-- get the raw data (all matched rows)
	DECLARE @src TABLE (ID int, TypeID int, Ranking smallmoney)
	INSERT INTO @src (ID, TypeID, Ranking)
		SELECT ID, TypeID
			, Ranking =	dbo.fnRateRanking(@CarrierID, R.CarrierID, 8, 1)
					  + dbo.fnRateRanking(@DriverID, R.DriverID, 4, 0)
					  + dbo.fnRateRanking(@StateID, R.StateID, 2, 1)
					  + dbo.fnRateRanking(@RegionID, R.RegionID, 1, 1)
		FROM dbo.viewCarrierRule R
		WHERE coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@DriverID, 0), R.DriverID, 0) = coalesce(R.DriverID, nullif(@DriverID, 0), 0)
		  AND coalesce(nullif(@StateID, 0), R.StateID, 0) = coalesce(R.StateID, nullif(@StateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(R.RegionID, nullif(@RegionID, 0), 0)
		  AND (nullif(@TypeID, 0) IS NULL OR TypeID = @TypeID)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)

	-- return the rate data for the best-match rates 
	INSERT INTO @ret (ID, TypeID, CarrierID, DriverID, StateID, RegionID, Value, RuleTypeID, EffectiveDate, EndDate
			, NextEffectiveDate, PriorEndDate, BestMatch, Ranking, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser)
		SELECT R.ID, TypeID, CarrierID, DriverID, StateID, RegionID, R.Value, RuleTypeID, EffectiveDate, EndDate
			, NextEffectiveDate, PriorEndDate, BestMatch, Ranking, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser
		FROM viewCarrierRule R
		JOIN (
			SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.TypeID IS NULL THEN 0 ELSE 1 END as bit)
			FROM @src S
			LEFT JOIN (
				SELECT TypeID, Ranking = MAX(Ranking)
				FROM @src
				WHERE @StartDate = ISNULL(@EndDate, @StartDate)
				  AND Ranking % 1 = 0.01 * 4 -- ensure a complete match is found for BESTMATCH
				GROUP BY TypeID
			) X ON X.TypeID = S.TypeID AND X.Ranking = S.Ranking
		) X ON X.ID = R.ID
		WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
		
	RETURN
END

GO



/***********************************/
-- Date Created: 2016-05-03
-- Author: Joe Engler
-- Purpose: retrieve and return the Carrier Rule rows for the specified criteria
/***********************************/
CREATE FUNCTION fnCarrierRulesDisplay(@StartDate date, @EndDate date, @TypeID int, @CarrierID int, @DriverID int, @StateID int, @RegionID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.CarrierID, R.DriverID, R.StateID, R.RegionID, R.TypeID
		, R.Value, R.RuleTypeID, R.EffectiveDate, R.EndDate, R.NextEffectiveDate, R.PriorEndDate
		, Type = CRT.Name
		, Carrier = C.Name
		, Driver = D.FullName
		, Region = D.Region
		, State = D.StateAbbrev
		, RuleType = RT.Name
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch, Ranking
	FROM dbo.fnCarrierRules(@StartDate, @EndDate, @TypeID, @CarrierID, @DriverID, @StateID, @RegionID, 0) R
	JOIN tblCarrierRuleType CRT ON CRT.ID = R.TypeID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN viewDriver D ON D.ID = R.DriverID
	LEFT JOIN tblRuleType RT ON RT.ID = R.RuleTypeID
	ORDER BY EffectiveDate

GO

--------------------------------------------------------------------------------

COMMIT 
SET NOEXEC OFF