-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.3.4'
SELECT  @NewVersion = '3.3.5'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'fix duplicate Route Rate issue'
GO

/***********************************/
-- Date Created: 9 Jun 2013
-- Author: Kevin Alons
-- Purpose: return Carrier route rates with translated "friendly" values + "missing route rates", computed Earliest & End Date values)
/***********************************/
ALTER VIEW [dbo].[viewCarrierRouteRates] AS
SELECT ID, RouteID, UomID, Rate, EffectiveDate, Active, OpenOrderCount = SUM(OpenOrderCount), ActualMiles
	, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, EndDate, EarliestEffectiveDate
	, CarrierID, Carrier, OriginID, Origin, OriginFull, DestinationID, Destination, DestinationFull
	, Status, Uom, UomShort
	, RouteInUse = cast(CASE WHEN sum(OpenOrderCount) = 0 THEN 0 ELSE 1 END AS bit) 
FROM (
	SELECT RB.ID, RouteID = R.ID
		, O.UomID 
		, Rate = isnull(RB.Rate, 0) 
		, EffectiveDate = isnull(RB.EffectiveDate, RM.MinOrderDate) 
		, Active = cast(CASE WHEN RB.Active = 0 THEN 0 ELSE 1 END as bit) 
		, OpenOrderCount = isnull(RM.OrderCount, 0)
		, ActualMiles = isnull(R.ActualMiles, 0)
		, RB.CreateDateUTC, RB.CreatedByUser, RB.LastChangeDateUTC, RB.LastChangedByUser
		, RB.EndDate
		, RB.EarliestEffectiveDate
		, C.ID AS CarrierID, C.Name AS Carrier
		, R.OriginID, Origin = O.Name, OriginFull = O.FullName
		, R.DestinationID, Destination = D.Name, DestinationFull = D.FullName
		, Status = CASE WHEN RB.ID IS NULL THEN 'Missing' ELSE 'Active' END 
		, O.UOM
		, O.UomShort
	FROM viewCarrier C CROSS JOIN viewOrigin O 
	JOIN tblRoute R ON R.OriginID = O.ID
	JOIN viewDestination D ON D.ID = R.DestinationID
	LEFT JOIN (
		SELECT CarrierID
			, RouteID
			, OrderCount = sum(OrderCount)
			, MinOrderDate = MIN(MinOrderDate)
			, IsSettled
		FROM viewCarrier_RouteMetrics 
		GROUP BY CarrierID, RouteID, IsSettled
	) RM ON RM.CarrierID = C.ID AND RM.RouteID = R.ID
	LEFT JOIN viewCarrierRouteRatesBase RB ON RB.CarrierID = C.ID AND RB.RouteID = R.ID
	WHERE (RB.CarrierID IS NOT NULL OR (RM.IsSettled IS NOT NULL AND RM.IsSettled = 0))
) X
GROUP BY ID, RouteID, UomID, Rate, EffectiveDate, Active, ActualMiles
	, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, EndDate, EarliestEffectiveDate
	, CarrierID, Carrier, OriginID, Origin, OriginFull, DestinationID, Destination, DestinationFull
	, Status, Uom, UomShort

GO

/***********************************/
-- Date Created: 9 Jun 2013
-- Author: Kevin Alons
-- Purpose: return Customer route rates with translated "friendly" values + "missing route rates", computed Earliest & End Date values)
/***********************************/
ALTER VIEW [dbo].[viewCustomerRouteRates] AS
SELECT ID, RouteID, UomID, Rate, EffectiveDate, Active, OpenOrderCount = SUM(OpenOrderCount), ActualMiles
	, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, EndDate, EarliestEffectiveDate
	, CustomerID, Customer, OriginID, Origin, OriginFull, DestinationID, Destination, DestinationFull
	, Status, Uom, UomShort
	, RouteInUse = cast(CASE WHEN sum(OpenOrderCount) = 0 THEN 0 ELSE 1 END AS bit) 
FROM (
	SELECT RB.ID, RouteID = R.ID
		, O.UomID 
		, Rate = isnull(RB.Rate, 0) 
		, EffectiveDate = isnull(RB.EffectiveDate, RM.MinOrderDate) 
		, Active = cast(CASE WHEN RB.Active = 0 THEN 0 ELSE 1 END as bit) 
		, OpenOrderCount = isnull(RM.OrderCount, 0)
		, ActualMiles = isnull(R.ActualMiles, 0)
		, RB.CreateDateUTC, RB.CreatedByUser, RB.LastChangeDateUTC, RB.LastChangedByUser
		, RB.EndDate
		, RB.EarliestEffectiveDate
		, C.ID AS CustomerID, C.Name AS Customer
		, R.OriginID, Origin = O.Name, OriginFull = O.FullName
		, R.DestinationID, Destination = D.Name, DestinationFull = D.FullName
		, Status = CASE WHEN RB.ID IS NULL THEN 'Missing' ELSE 'Active' END 
		, O.UOM
		, O.UomShort
	FROM viewCustomer C 
	JOIN viewOrigin O ON O.CustomerID = C.ID
	JOIN tblRoute R ON R.OriginID = O.ID
	JOIN viewDestination D ON D.ID = R.DestinationID
	LEFT JOIN (
		SELECT CustomerID
			, RouteID
			, OrderCount = sum(CASE WHEN IsSettled = 1 THEN 0 ELSE OrderCount END)
			, MinOrderDate = MIN(MinOrderDate)
			, IsSettled
		FROM viewCustomer_RouteMetrics 
		GROUP BY CustomerID, RouteID, IsSettled
	) RM ON RM.CustomerID = C.ID AND RM.RouteID = R.ID
	LEFT JOIN viewCustomerRouteRatesBase RB ON RB.CustomerID = C.ID AND RB.RouteID = R.ID
	-- either a RouteRateBase record exists, or this is for at least 1 unsettled order
	WHERE (RB.CustomerID IS NOT NULL OR (RM.IsSettled IS NOT NULL AND RM.IsSettled = 0))
) X
GROUP BY ID, RouteID, UomID, Rate, EffectiveDate, Active, ActualMiles
	, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, EndDate, EarliestEffectiveDate
	, CustomerID, Customer, OriginID, Origin, OriginFull, DestinationID, Destination, DestinationFull
	, Status, Uom, UomShort

GO

COMMIT
SET NOEXEC OFF