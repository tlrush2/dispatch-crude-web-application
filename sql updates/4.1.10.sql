SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.1.9.5'
SELECT  @NewVersion = '4.1.10'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1738 - Rewrite Routes maintenance page in MVC'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO



-- Add Delete columns to tblRoute
ALTER TABLE tblRoute
	ADD DeleteDateUTC SMALLDATETIME NULL
		, DeletedByUser VARCHAR(100) NULL
GO



-- Update existing route view permission to fit with new permissions below
UPDATE aspnet_Roles
SET Category = 'Routes'
	, FriendlyName = 'View'
WHERE RoleName = 'viewRouteMaintenance'
GO



-- Add new permissions for route page
EXEC spAddNewPermission 'createRouteMaintenance', 'Allow user to create Routes', 'Routes', 'Create'
GO
EXEC spAddNewPermission 'editRouteMaintenance', 'Allow user to edit Routes', 'Routes', 'Edit'
GO
-- Per Maverick, deactivation should be controled by origins and destinations so a deactivate permission will not be created.



/* Deactivate routes that should be deactivated currently due to their origin/destination active status.
   This will start the page at a good baseline. If we do not do this you'll see routes that have an inactive
   origin or destination but still show up incorrectly as an active route.
*/
UPDATE tblRoute
SET DeleteDateUTC = GETUTCDATE()
, DeletedByUser = 'bbloodworth'
WHERE DeleteDateUTC IS NULL
	AND OriginID IN
	(
		SELECT ID 
		FROM tblOrigin
		WHERE DeleteDateUTC IS NOT NULL
	)
GO

UPDATE tblRoute
SET DeleteDateUTC = GETUTCDATE()
, DeletedByUser = 'bbloodworth'
WHERE DeleteDateUTC IS NULL
AND DestinationID IN
(
	SELECT ID 
	FROM tblDestination
	WHERE DeleteDateUTC IS NOT NULL
)
GO


-- =============================================
-- Author:		Kevin Alons
-- Create date: 19 Jun 2014
-- Description:	deactivate any currently active Origins that haven't had any traffic since the Stale days threshold
-- Changes:
--			4.1.10 - 2016/09/23 - BB - Add code to additionlly deactivate the routes associated with the origins that get deactivated
-- =============================================
ALTER PROCEDURE [dbo].[spDeactivateStaleOrigins]
(
  @UserName varchar(100)
, @affectedRoutes int = null output
, @affectedOrigins int = null output
) 
AS BEGIN
	DECLARE @staleDays int
	SELECT @staleDays = Value FROM tblSetting WHERE ID = 21
	
	--Deactivate routes
	UPDATE tblRoute
		SET DeleteDateUTC = GETUTCDATE(), DeletedByUser = @UserName
	WHERE ID IN 
	(
		SELECT ID 
		FROM tblRoute
		WHERE DeleteDateUTC IS NULL			
			AND CreateDateUTC < DATEADD(day, -@staleDays, getdate())
			AND OriginID NOT IN (SELECT OriginID FROM viewOrder WHERE OrderDate > DATEADD(day, -@staleDays, getdate()))
	)

	SET @affectedRoutes = @@ROWCOUNT

	--Deactivate origins
	UPDATE tblOrigin 
		SET DeleteDateUTC = GETUTCDATE(), DeletedByUser = @UserName
	WHERE DeleteDateUTC IS NULL
		AND CreateDateUTC < DATEADD(day, -@staleDays, getdate())
		AND ID NOT IN (SELECT OriginID FROM viewOrder WHERE OrderDate > DATEADD(day, -@staleDays, getdate()))

	SET @affectedOrigins = @@ROWCOUNT
END
GO



-- =============================================
-- Author:		Kevin Alons
-- Create date: 19 Jun 2014
-- Description:	deactivate any currently active Destinations that haven't had any traffic since the Stale days threshold
-- Changes:
--			4.1.10 - 2016/09/23 - BB - Add code to additionlly deactivate the routes associated with the destinations that get deactivated
--									- Also corrected setting id to #22 (Destination Stale) because it was checking origin stale (#21)
-- =============================================
ALTER PROCEDURE [dbo].[spDeactivateStaleDestinations]
(
  @UserName varchar(100)
, @affectedRoutes int = null output
, @affectedDestinations int = null output
) 
AS BEGIN
	DECLARE @staleDays int
	SELECT @staleDays = Value FROM tblSetting WHERE ID = 22
	
	--Deactivate routes
	UPDATE tblRoute
		SET DeleteDateUTC = GETUTCDATE(), DeletedByUser = @UserName
	WHERE ID IN 
	(
		SELECT ID 
		FROM tblRoute
		WHERE DeleteDateUTC IS NULL
			AND CreateDateUTC < DATEADD(day, -@staleDays, getdate())
			AND DestinationID NOT IN (SELECT DestinationID FROM viewOrder WHERE OrderDate > DATEADD(day, -@staleDays, getdate()))
	)

	SET @affectedRoutes = @@ROWCOUNT

	--Deactivate destinations
	UPDATE tblDestination
		SET DeleteDateUTC = GETUTCDATE(), DeletedByUser = @UserName
	WHERE DeleteDateUTC IS NULL
		AND CreateDateUTC < DATEADD(day, -@staleDays, getdate())
		AND ID NOT IN (SELECT DestinationID FROM viewOrder WHERE OrderDate > DATEADD(day, -@staleDays, getdate()))

	SET @affectedDestinations = @@ROWCOUNT
END
GO



COMMIT
SET NOEXEC OFF