SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.5.5'
SELECT  @NewVersion = '4.5.6'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'JT-219 - Compliance Report - final'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO



/****************************************************
 Date Created: 2016/12/22 - 4.4.14
 Author: Joe Engler
 Purpose: return all carrier compliance with non compliant and missing records
 Changes:
	4.5.4	- 2017/02/10	- JAE/BSB	- Reworked using the new carrier compliance fields
	4.5.6	- 2016/02/14	- JAE		- Add enforcement check to existing records
											added extra carrier type filter to ensure missing only applied to appropriate types
****************************************************/
ALTER FUNCTION fnCarrierComplianceSummary(@showCompliant INT = 0, @showUnenforced INT = 0)
RETURNS @ret TABLE
(
	ID INT,
	CarrierID INT,
	Carrier VARCHAR(40),
	ComplianceTypeID INT,
	ComplianceType VARCHAR(100),
	MissingDocument BIT,
	ExpiredNow BIT,
	ExpiredNext30 BIT,
	ExpiredNext60 BIT,
	ExpiredNext90 BIT,
	Missing BIT
)
AS BEGIN
 	DECLARE @__ENFORCE_CARRIER_COMPLIANCE__ INT = 23

	INSERT INTO @ret
	SELECT C.ID, C.CarrierID, Carrier, CarrierComplianceTypeID, CarrierComplianceType, MissingDocument, ExpiredNow, ExpiredNext30, ExpiredNext60, ExpiredNext90, Missing = CAST(0 AS BIT)
	  FROM viewCarrierCompliance C
	 OUTER APPLY dbo.fnCarrierRules(GETUTCDATE(), NULL, @__ENFORCE_CARRIER_COMPLIANCE__, NULL, C.CarrierID, NULL, NULL, NULL, 1) CR_C
	 WHERE C.DeleteDateUTC IS NULL --active carriers
	   AND (@showUnenforced = 1 OR dbo.fnToBool(CR_C.Value) = 1) -- compliance enforced
	   AND (@showCompliant = 1 OR MissingDocument = 1 OR ExpiredNow = 1 OR ExpiredNext30 = 1 OR ExpiredNext60 = 1 OR ExpiredNext90 = 1)

	UNION
 
	SELECT ID = NULL,
		CarrierID = Q.ID,
		CarrierName = Q.Name,
		ComplianceTypeID = CCT.ID,
		ComplianceType = CCT.Name,
		MissingDocument = CAST(0 AS BIT),
		ExpiredNow = CAST(0 AS BIT),
		ExpiredNext30 = CAST(0 AS BIT),
		ExpiredNext60 = CAST(0 AS BIT),
		ExpiredNext90 = CAST(0 AS BIT),
		Missing = CCT.IsRequired
	FROM ( -- Get active drivers with compliance enforced
			SELECT C.* 
				FROM viewCarrier C
				OUTER APPLY dbo.fnCarrierRules(GETUTCDATE(), NULL, @__ENFORCE_CARRIER_COMPLIANCE__, NULL, C.ID, NULL, NULL, NULL, 1) CR_C

				WHERE c.DeleteDateUTC IS NULL --active carriers
				AND (@showUnenforced = 1 OR dbo.fnToBool(CR_C.Value) = 1) -- compliance enforced
		) Q,
		tblCarrierComplianceType CCT
	WHERE (@showCompliant = 1 OR CCT.IsRequired = 1)
	 AND (CCT.CarrierTypeID IS NULL OR CCT.CarrierTypeID = Q.CarrierTypeID)
	 AND NOT EXISTS -- no active record in the carrier document table
		(SELECT  1
			FROM viewCarrierCompliance
			WHERE DeleteDateUTC IS NULL AND CarrierID = Q.ID AND CarrierComplianceTypeID = CCT.ID
		)

	RETURN
END
GO



/****************************************************
 Date Created: 2016/12/22
 Author: Joe Engler
 Purpose: return all driver compliance with non compliant and missing records
 Changes:
--	4.5.0		BSB		2017-01-25		Add Terminal
--	4.5.4		JAE		2017-02-13		Add expiring soon records to display
 -- 4.5.6		JAE		2017-02-14		Add enforcement check to existing records
****************************************************/
ALTER FUNCTION fnDriverComplianceSummary(@showCompliant INT = 0, @showUnenforced INT = 0)
RETURNS @ret TABLE
(
	ID INT,
	DriverID INT,
	FullName VARCHAR(41),
	CarrierID INT,
	Carrier VARCHAR(40),
	ComplianceTypeID INT,
	ComplianceType VARCHAR(100),
	MissingDocument BIT,
	ExpiredNow BIT,
	ExpiredNext30 BIT,
	ExpiredNext60 BIT,
	ExpiredNext90 BIT,
	Missing BIT
)
AS BEGIN
 	DECLARE @__ENFORCE_DRIVER_COMPLIANCE__ INT = 14

	INSERT INTO @ret
	SELECT d.ID, d.DriverID, d.FullName, d.CarrierID, d.Carrier, ComplianceTypeID, ComplianceType, MissingDocument, ExpiredNow, ExpiredNext30, ExpiredNext60, ExpiredNext90, Missing = CAST(0 AS BIT)
	  FROM viewDriverCompliance d
	  LEFT JOIN viewDriverBase db ON d.DriverID = db.ID
	 OUTER APPLY dbo.fnCarrierRules(GETUTCDATE(), NULL, @__ENFORCE_DRIVER_COMPLIANCE__, d.ID, d.CarrierID, db.TerminalID, db.OperatingStateID, db.RegionID, 1) CR_C
	 WHERE d.DeleteDateUTC IS NULL --active drivers
	   AND (@showUnenforced = 1 OR dbo.fnToBool(CR_C.Value) = 1) -- compliance enforced
	   AND (@showCompliant = 1 OR MissingDocument = 1 OR ExpiredNow = 1 OR ExpiredNext30 = 1 OR ExpiredNext60 = 1 OR ExpiredNext90 = 1)

	UNION 

	SELECT ID = NULL,
		DriverID = q.ID,
		q.FullName,
		q.CarrierID,
		q.Carrier,
		ComplianceTypeID = ct.id,
		ComplianceType = ct.Name,
		MissingDocument = CAST(0 AS BIT),
		ExpiredNow = CAST(0 AS BIT),
		ExpiredNext30 = CAST(0 AS BIT),
		ExpiredNext60 = CAST(0 AS BIT),
		ExpiredNext90 = CAST(0 AS BIT),
		Missing = ct.IsRequired
	FROM ( -- Get active drivers with compliance enforced
			SELECT d.* 
				FROM viewDriverBase d
				OUTER APPLY dbo.fnCarrierRules(GETUTCDATE(), null, @__ENFORCE_DRIVER_COMPLIANCE__, d.ID, d.CarrierID, d.TerminalID, d.OperatingStateID, d.RegionID, 1) cr_c
				WHERE d.DeleteDateUTC is null --active drivers
				AND (@showUnenforced = 1 OR dbo.fnToBool(cr_c.Value) = 1) -- compliance enforced
		) q,
		tbldrivercompliancetype ct
	WHERE (@showCompliant = 1 OR ct.IsRequired = 1)
	 AND NOT EXISTS -- no active record in the driver compliance table
		(SELECT  1
			FROM viewDriverCompliance 
			WHERE DeleteDateUTC IS NULL AND DriverID = q.ID and ComplianceTypeID = ct.id
		)
	RETURN
END
GO


/****************************************************
 Date Created: 2017/02/02 - 4.5.2
 Author: Joe Engler
 Purpose: return all carrier compliance with non compliant and missing records
 Changes:
 -- 4.5.6		JAE		2017-02-14		Add enforcement check to existing records
										 added extra truck type filter to ensure missing only applied to appropriate types
****************************************************/
ALTER FUNCTION fnTruckComplianceSummary(@showCompliant INT = 0, @showUnenforced INT = 0)
RETURNS @ret TABLE
(
	ID INT,
	TruckID INT,
	Truck VARCHAR(40),
	CarrierID INT,
	Carrier VARCHAR(40),
	ComplianceTypeID INT,
	ComplianceType VARCHAR(100),
	MissingDocument BIT,
	ExpiredNow BIT,
	ExpiredNext30 BIT,
	ExpiredNext60 BIT,
	ExpiredNext90 BIT,
	Missing BIT
)
AS BEGIN
 	DECLARE @__ENFORCE_TRUCK_COMPLIANCE__ INT = 24

	INSERT INTO @ret
	SELECT T.ID, 
		TruckID, 
		Truck, 
		T.CarrierID, 
		Carrier, 
		TruckComplianceTypeID, 
		TruckComplianceType, 
		MissingDocument, 
		ExpiredNow, 
		ExpiredNext30, 
		ExpiredNext60, 
		ExpiredNext90, 
		Missing = CAST(0 AS BIT)
	  FROM viewTruckCompliance T
	 OUTER APPLY dbo.fnCarrierRules(GETUTCDATE(), NULL, @__ENFORCE_TRUCK_COMPLIANCE__, NULL, T.CarrierID, NULL, NULL, NULL, 1) CR_C
	 WHERE T.DeleteDateUTC IS NULL --active trucks
	   AND (@showUnenforced = 1 OR dbo.fnToBool(CR_C.Value) = 1) -- compliance enforced
	   AND (@showCompliant = 1 OR MissingDocument = 1 OR ExpiredNow = 1 OR ExpiredNext30 = 1 OR ExpiredNext60 = 1 OR ExpiredNext90 = 1)

	UNION
 
	SELECT ID = NULL,
		TruckID = Q.ID,
		Truck = Q.IDNumber,
		CarrierID = Q.ID,
		CarrierName = Q.Carrier,
		ComplianceTypeID = DT.ID,
		ComplianceType = DT.Name,
		MissingDocument = CAST(0 AS BIT),
		ExpiredNow = CAST(0 AS BIT),
		ExpiredNext30 = CAST(0 AS BIT),
		ExpiredNext60 = CAST(0 AS BIT),
		ExpiredNext90 = CAST(0 AS BIT),
		Missing = DT.IsRequired
	FROM ( -- Get active trucks with compliance enforced
			SELECT T.* 
				FROM viewTruck T
				OUTER APPLY dbo.fnCarrierRules(GETUTCDATE(), NULL, @__ENFORCE_TRUCK_COMPLIANCE__, NULL, T.CarrierID, NULL, NULL, NULL, 1) CR_C

				WHERE T.DeleteDateUTC IS NULL --active trucks
				AND (@showUnenforced = 1 OR dbo.fnToBool(CR_C.Value) = 1) -- compliance enforced
		) Q,
		tblTruckComplianceType DT
	WHERE (@showCompliant = 1 OR DT.IsRequired = 1)
  	  AND (DT.TruckTypeID IS NULL OR DT.TruckTypeID = Q.TruckTypeID)
	  AND NOT EXISTS -- no active record in the truck compliance table
		(SELECT  1
			FROM viewTruckCompliance
			WHERE DeleteDateUTC IS NULL AND TruckID = Q.ID and TruckComplianceTypeID = DT.ID
		)

	RETURN
END
GO


/****************************************************
 Date Created: 2017/02/08 - 4.5.3
 Author: Joe Engler/Ben Bloodworth
 Purpose: return all trailer compliance with non compliant and missing records
 Changes:
 -- 4.5.6		JAE		2017-02-14		Add enforcement check to existing records
										 added extra trailer type filter to ensure missing only applied to appropriate types
 ****************************************************/
ALTER FUNCTION fnTrailerComplianceSummary(@showCompliant INT = 0, @showUnenforced INT = 0)
RETURNS @ret TABLE
(
	ID INT,
	TrailerID INT,
	Trailer VARCHAR(40),
	CarrierID INT,
	Carrier VARCHAR(40),
	ComplianceTypeID INT,
	ComplianceType VARCHAR(100),
	MissingDocument BIT,
	ExpiredNow BIT,
	ExpiredNext30 BIT,
	ExpiredNext60 BIT,
	ExpiredNext90 BIT,
	Missing BIT
)
AS BEGIN
 	DECLARE @__ENFORCE_TRAILER_COMPLIANCE__ INT = 24

	INSERT INTO @ret
	SELECT T.ID, 
		TrailerID, 
		Trailer, 
		T.CarrierID, 
		Carrier, 
		TrailerComplianceTypeID, 
		TrailerComplianceType, 
		MissingDocument, 
		ExpiredNow, 
		ExpiredNext30, 
		ExpiredNext60, 
		ExpiredNext90, 
		Missing = CAST(0 AS BIT)
	  FROM viewTrailerCompliance T
	 OUTER APPLY dbo.fnCarrierRules(GETUTCDATE(), NULL, @__ENFORCE_TRAILER_COMPLIANCE__, NULL, T.CarrierID, NULL, NULL, NULL, 1) CR_C
	 WHERE T.DeleteDateUTC IS NULL --active trailers
	   AND (@showUnenforced = 1 OR dbo.fnToBool(CR_C.Value) = 1) -- compliance enforced
	   AND (@showCompliant = 1 OR MissingDocument = 1 OR ExpiredNow = 1 OR ExpiredNext30 = 1 OR ExpiredNext60 = 1 OR ExpiredNext90 = 1)

	UNION 

	SELECT ID = NULL,
		TrailerID = Q.ID,
		Trailer = Q.IDNumber,
		CarrierID = Q.ID,
		CarrierName = Q.Carrier,
		ComplianceTypeID = DT.ID,
		ComplianceType = DT.Name,
		MissingDocument = CAST(0 AS BIT),
		ExpiredNow = CAST(0 AS BIT),
		ExpiredNext30 = CAST(0 AS BIT),
		ExpiredNext60 = CAST(0 AS BIT),
		ExpiredNext90 = CAST(0 AS BIT),
		Missing = DT.IsRequired
	FROM ( -- Get active trailers with compliance enforced
			SELECT T.* 
				FROM viewTrailer T
				OUTER APPLY dbo.fnCarrierRules(GETUTCDATE(), NULL, @__ENFORCE_TRAILER_COMPLIANCE__, NULL, T.CarrierID, NULL, NULL, NULL, 1) CR_C

				WHERE T.DeleteDateUTC IS NULL --active trailers
				AND (@showUnenforced = 1 OR dbo.fnToBool(CR_C.Value) = 1) -- compliance enforced
		) Q,
		tblTrailerComplianceType DT
	WHERE (@showCompliant = 1 OR DT.IsRequired = 1)
	  AND (DT.TrailerTypeID IS NULL OR DT.TrailerTypeID = Q.TrailerTypeID)
      AND NOT EXISTS -- no active record in the trailer compliance table
		(SELECT  1
			FROM viewTrailerCompliance
			WHERE DeleteDateUTC IS NULL AND TrailerID = Q.ID and TrailerComplianceTypeID = DT.ID
		)

	RETURN
END
GO



COMMIT
SET NOEXEC OFF