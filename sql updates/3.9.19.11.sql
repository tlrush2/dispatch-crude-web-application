-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.9.19.10.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.9.19.10.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0

SET NOEXEC OFF
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.9.19.10'
SELECT  @NewVersion = '3.9.19.11'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'revamp Shipper export logic to be more modular and maintainable'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

EXEC _spDropProcedure 'spOrderExportFinalCustomer_Sunoco_Sundex'
GO
/*****************************************
Author: Kevin Alons
Date Created: 2015/10/08 - 3.9.19.11
Purpose: implement the Sunoco Sundex Shipper export
******************************************/
CREATE PROCEDURE spOrderExportFinalCustomer_Sunoco_Sundex (@OrderIDs IDTABLE READONLY) AS
BEGIN
	SELECT E.* 
	FROM dbo.viewSunocoSundex E
	JOIN @orderIDs IDs ON IDs.ID = E._ID
	ORDER BY _OrderNum
END
GO

EXEC _spDropFunction 'fnIsnumeric'
GO
/***********************************************
Author: Kevin Alons
Date Created: 2015/10/08- 3.9.19.11
Purpose: workaround issue with built-in ISNUMERIC function where a string containing , is considered NUMERIC
***********************************************/
CREATE FUNCTION fnIsNumeric(@expression varchar(max)) RETURNS bit AS
BEGIN
	DECLARE @ret bit
	SET @ret = CASE WHEN @ret IS NULL OR @ret LIKE '%,%' THEN 0 ELSE ISNUMERIC(@expression) END
	RETURN (@ret)
END
GO
GRANT EXECUTE ON fnIsNumeric TO role_iis_acct
GO

/*=============================================
	Author:			Ben Bloodworth
	Create date:	5 Jun 2015
	Description:	Displays the base information for the CODE export. Please note that some values could change per DispatchCrude customer and thus it may
					be better to retrieve these settings from the Data Exchange settings instead of this view.
					
	Updates:		8/21/15 - BB: Added CASE statments to account for Nulls in gravity readings and added additional filters for rejects and ticket types.
					8/26/15 - BB: Altered tank number field to use OriginTankText instead of TankNum.  Altered Property_Code per instructions from customer.
					9/24/15 - BB: Added Net Volume tickets to allowed ticket types list in where clause.
				   10/05/15 - BB: Changed export to use Gross Volume instead of Net Volume (Changed per Cash Rainer at CW2).
		- 3.9.19.11 - 2015/10/08 - KDA - fix several varchar -> numeric conversions that could fail with entered data 
							(CarrierTicketNum too long, LeaseNum not numeric, T.OriginTankText not numeric, use of fnIsNumeric() function)
=============================================*/
ALTER VIEW viewOrderExport_CODE_Base AS
SELECT 
	OrderID = O.ID
	,OrderTicketID = T.ID
	,Record_ID = '2'
	,Company_ID = C.CodeDXCode
	,Ticket_Type = TT.CodeDXCode
	,Transmit_Ind = '2'  -- Hard coded per Angie Toews (PDS Energy) but may need to be changed to dynamic if the customer says so
	,Run_Type = 1
	,Ticket_Number = CASE WHEN dbo.fnIsNumeric(RIGHT(T.CarrierTicketNum, 7)) = 0 THEN dbo.fnFixedLenStr(T.CarrierTicketNum, 7) 
						  ELSE dbo.fnFixedLenNum(RIGHT(T.CarrierTicketNum, 7), 7, 0, 0) 
					 END  
	,Run_Ticket_Date = dbo.fnDateMMDDYY(O.OrderDate)
	
	--Property code is a non-standard CODE field for Plains (per Steve Carver).  It is a mixture of origin leasenum and destination station num
	,Property_Code = (CASE WHEN O.LeaseNum IS NULL OR dbo.fnIsNumeric(O.LeaseNum) = 0 THEN '000000' ELSE CAST(dbo.fnFixedLenNum(O.LeaseNum,6,0,0) AS VARCHAR(6)) END)
					+ '000'
					+ (CASE WHEN O.DestStation IS NULL THEN '00000' ELSE dbo.fnFixedLenStr(O.DestStation,5) END)
	,Gathering_Charge = '0'  -- Hard coded per Angie Toews (PDS Energy) but may need to be changed to dynamic if the customer says so
	,Tank_Meter_Num = CASE WHEN dbo.fnIsNumeric(T.OriginTankText) = 0 THEN '000000' ELSE CAST(dbo.fnFixedLenNum(RIGHT(T.OriginTankText, 6), 6, 0, 0) AS VARCHAR(6)) END + '0'	
	,Adjustment_Ind = '0' -- Hard coded per Angie Toews (PDS Energy) but may need to be changed to dynamic if the customer says so
	,Opening_Reading = dbo.fnOpenCloseReadingCODEFormat(T.ID,TT.CodeDXCode,0)
	,Opening_Date = dbo.fnDateMMDD(O.OrderDate)
	,Closing_Reading = dbo.fnOpenCloseReadingCODEFormat(T.ID,TT.CodeDXCode,1)
	,Closing_Date = dbo.fnDateMMDD(O.DestDepartTime)
	,Meter_Factor = CASE WHEN T.MeterFactor IS NULL OR dbo.fnIsNumeric(T.MeterFactor) = 0 THEN '00000000' 
						 ELSE dbo.fnFixedLenNum(RIGHT(T.MeterFactor, 11), 2, 6, 1) 
					END 
	,Shrinkage_Incrustation = '1000000' -- Hard coded per Angie Toews (PDS Energy) but may need to be changed to dynamic if the customer says so
	,Opening_Temperature = CASE WHEN T.ProductHighTemp IS NULL THEN '0000' 
								ELSE dbo.fnFixedLenNum(T.ProductHighTemp, 3, 1, 1) 
						   END 
	,Closing_Temperature = CASE WHEN T.ProductLowTemp IS NULL THEN '0000' 
								ELSE dbo.fnFixedLenNum(T.ProductLowTemp, 3, 1, 1) 
						   END
	,BSW = CASE WHEN TT.ID IN (1,3) THEN dbo.fnFixedLenNum(T.ProductBSW, 2, 2, 1) --Non-Estmated ticket types (Gauge Run, Meter Run)					
				ELSE '0000'
		   END	
	,Observed_Gravity =	CASE WHEN T.ProductObsGravity IS NULL THEN '000'
							 ELSE dbo.fnFixedLenNum(T.ProductObsGravity, 2, 1, 1)
						END
	,Observed_Temperature = dbo.fnFixedLenNum(T.ProductObsTemp, 3, 1, 1)
	,Pos_Neg_Code = '0' -- Hard coded per Angie Toews (PDS Energy) but may need to be changed to dynamic if the customer says so
	,Total_Net_Volume = dbo.fnFixedLenNum(T.GrossUnits, 7, 2, 1) -- BB 10/7/15 this sends GROSS on purpose. this change is per Plains Marketing
	,Shippers_Net_Volume = dbo.fnFixedLenNum(T.GrossUnits, 7, 2, 1) -- BB 10/7/15 this sends GROSS on purpose. this change is per Plains Marketing
	,Corrected_Gravity = CASE WHEN T.Gravity60F IS NULL THEN '000'
							  ELSE dbo.fnFixedLenNum(T.Gravity60F, 2, 1, 1)
						 END
	,Product_Code = dbo.fnFixedLenStr('',3)	--Leave 3 blank spaces unless product is not crude oil. For others lookup PETROEX code	
	,Transmission_Date = dbo.fnFixedLenStr('',3) --Leave 3 blank spaces. Filled in by export reciever.
FROM viewOrderLocalDates O
	JOIN dbo.viewOrderCustomerFinalExportPending EP ON EP.ID = O.ID  -- **** NOTE **** This will become obsolete with the new changes to the export process
	LEFT JOIN viewOrderTicket T ON T.OrderID = O.ID AND T.DeleteDateUTC IS NULL
	LEFT JOIN tblTicketType TT ON TT.ID = T.TicketTypeID
	LEFT JOIN tblCustomer C ON C.ID = O.CustomerID
	LEFT JOIN tblProduct P ON P.ID = O.ProductID
WHERE O.DeleteDateUTC IS NULL
	AND O.Rejected = 0 -- No rejected orders
	AND T.Rejected = 0 -- No rejected tickets
	AND O.StatusID = 4 -- Only Audited orders
	AND TT.ID IN (1,2,7) -- Only Gauge Run, Gauge Net, and Net Volume tickets
GO

EXEC _spDropProcedure 'spOrderExportFinalCustomer_CODE_Standard'
GO
/*****************************************
Author: Kevin Alons
Date Created: 2015/10/08 - 3.9.19.11
Purpose: implement the CODE Standard Shipper export
******************************************/
CREATE PROCEDURE spOrderExportFinalCustomer_CODE_Standard (@OrderIDs IDTABLE READONLY) AS
BEGIN
	-- retrieve the basic data
	SELECT E.ExportText, E.CompanyID
	INTO #output
	FROM dbo.viewOrderExport_CODE E
	JOIN @orderIDs IDs ON IDs.ID = E.OrderID

	IF (SELECT COUNT(DISTINCT CompanyID) FROM #output) <> 1 OR (SELECT COUNT(*) FROM #output WHERE LEN(isnull(CompanyID, '')) <> 2) > 0
	BEGIN /* invalid input data was specified, so just return an error message */
		RAISERROR('Error in export data. Check Shipper CODE ID.', 16, 1)
		RETURN
	END 
		
	/* Add summary record to output*/
	INSERT INTO #output (ExportText)
	SELECT
		'4' -- Summary Record Type RecordID (Hard Coded)
		+ X.Company_ID 
		+ X.Record_Count 
		+ X.Pos_Neg_Code 
		+ X.Total_Net
		+ X.Shipper_Net
		+ dbo.fnFixedLenStr('',89)
		+ dbo.fnFixedLenStr('',3)
	FROM (
		SELECT
			ECB.Company_ID
			,CAST(dbo.fnFixedLenNum((COUNT(*) + 1), 10, 0, 1) AS VARCHAR)	AS Record_Count
			,ECB.Pos_Neg_Code
			,CAST(dbo.fnFixedLenNum(SUM(CAST(ECB.Total_Net_Volume AS DECIMAL)), 13, 0, 1) AS VARCHAR) Total_Net
			,CAST(dbo.fnFixedLenNum(SUM(CAST(ECB.Shippers_Net_Volume AS DECIMAL)), 13, 0, 1) AS VARCHAR) Shipper_Net
		FROM dbo.viewOrderExport_CODE_Base ECB			
		JOIN @orderIDs IDs ON IDs.ID = ECB.OrderID			
		GROUP BY ECB.Company_ID,ECB.Pos_Neg_Code) X			
	
	/* return the final export data */
	SELECT ExportText FROM #output ORDER BY ExportText --Sorted so that the summary record always sits at the bottom
END
GO

EXEC _spDropFunction 'fnOrderExport_ShipXpress'
GO
/*****************************************
Author: Kevin Alons
Date Created: 2015/10/08 - 3.9.19.11
Purpose:	Turns the information collected by viewOrderExport_ShipXpress_Base into XML in the format specified by shipXpress 					
				-- using a function as a DYNAMIC VIEW so we can filter on OrderIDs but not return them in the resultant XML data
******************************************/
CREATE FUNCTION fnOrderExport_ShipXpress(@orderIDs IDTABLE READONLY) RETURNS TABLE AS RETURN
SELECT 
(
	SELECT 
		[@Client] = SXB.Client
		, SXB.TMWOrderNumber
		, SXB.ClientOrderNumber
		, SXB.TicketStatus
		, SXB.LeaseID
		, SXB.LeaseName
		, SXB.IPID
		, SXB.IPName
		, SXB.DriverID
		, SXB.DriverName
		, SXB.TrailerID
		, SXB.TruckID
		, SXB.CarrierID
		, SXB.CarrierName
		, SXB.TicketDate
		, SXB.TankID
		, SXB.TankName
		, SXB.MeterID
		, SXB.MeterName
		, SXB.GrossVolume
		, SXB.NetVolume
		, SXB.ObsGravity
		, SXB.ObsTemp
		, SXB.BSW
		, SXB.BeginReadingDate
		, SXB.EndReadingDate
		, SXB.TopGauge
		, SXB.BottomGauge
		, SXB.TopTemp
		, SXB.BotTemp
		, SXB.MeterStart
		, SXB.MeterEnd
		, SXB.Comments
		, SXB.RejectReason
		, SXB.SealOn
		, SXB.SealOff
		, SXB.StationLogMeterGross
		, SXB.StationLogMeterOpen
		, SXB.StationLogMeterClose
		, SXB.StationLogComments
		, SXB.StationLogDate
		, SXB.Product
		, SXB.CallinDate
		, SXB.RelatedOrderNumber		
	FROM viewOrderExport_ShipXpress_Base SXB
	JOIN @orderIDs O ON O.ID = SXB.ExportedOrderID
	FOR XML PATH('Order'), ROOT('Orders'), TYPE
) XML

GO
GRANT SELECT ON fnOrderExport_ShipXpress TO role_iis_acct
GO

EXEC _spDropProcedure 'spOrderExportFinalCustomer_ShipXpress'
GO
/*****************************************
Author: Kevin Alons
Date Created: 2015/10/08 - 3.9.19.11
Purpose: implement the SHIPXPRESS Shipper export
******************************************/
CREATE PROCEDURE spOrderExportFinalCustomer_ShipXpress (@OrderIDs IDTABLE READONLY) AS
BEGIN
	SELECT E.XML 
	FROM dbo.fnOrderExport_ShipXpress(@orderIDs) E
END
GO

EXEC _spDropFunction 'fnShipXpressDXExportCustomerIDs'
GO
/***********************************************
-- Author:		Kevin Alons
-- Create date: 2015/10/09
-- Description:	return Customer IDs where ShipXpressCXExport = 1
-- Changes:
***********************************************/
CREATE FUNCTION fnShipXpressDXExportCustomerIDs() RETURNS varchar(1000) AS BEGIN
	DECLARE @ret varchar(max)
	SELECT @ret = COALESCE(@ret + ',', '') + LTRIM(ID) FROM tblCustomer WHERE ShipXpressDXExport = 1
	RETURN (isnull(@ret, 0))
END
GO
GRANT EXECUTE ON fnShipXpressDXExportCustomerIDs To role_iis_acct
GO

EXEC _spDropFunction 'fnOrderCustomerFinalExportPending'
GO
/*****************************************************************************************
-- Author: Kevin Alons
-- Date Created: 27 Feb 2014 - 3.9.19.11
-- Purpose: with the provided parameters, export the data and mark done (if doing finalExport)
-- Changes:
*****************************************************************************************/
CREATE FUNCTION fnOrderCustomerFinalExportPending(@customerID_CSV varchar(max)) RETURNS TABLE AS RETURN
	SELECT ID
	FROM dbo.viewOrderCustomerFinalExportPending 
	WHERE CustomerID IN (SELECT ID FROM dbo.fnSplitCSVIDs(@customerID_CSV))
GO

EXEC _spDropProcedure 'spOrderExportFinalCustomer3'
GO
/*****************************************************************************************
-- Author: Kevin Alons
-- Date Created: 27 Feb 2014 - 3.9.19.11
-- Purpose: with the provided parameters, export the data and mark done (if doing finalExport)
-- Changes:
*****************************************************************************************/
CREATE PROCEDURE spOrderExportFinalCustomer3
( 
  @customerID_CSV varchar(max)
, @exportedByUser varchar(100) = NULL -- will default to SUSER_NAME() -- default value for now
, @exportFormat varchar(100) -- Options are: 'SUNOCO_SUNDEX' or 'CODE_STANDARD' or 'SHIPXPRESS'
, @finalExport bit = 1 -- default to TRUE
) 
AS BEGIN
	SET NOCOUNT ON
	-- default to the current user if not supplied
	IF @exportedByUser IS NULL SET @exportedByUser = SUSER_NAME()
	
	-- retrieve the order records to export
	DECLARE @orderIDs IDTABLE
	INSERT INTO @orderIDs (ID)
		SELECT ID FROM dbo.fnOrderCustomerFinalExportPending(@customerID_CSV)

	-- retrieve and return the requested data
	DECLARE @sql nvarchar(max)
	SELECT @sql = 'EXEC spOrderExportFinalCustomer_' + @exportFormat + ' @orderIDs=@orderIDs'
	EXEC sp_executesql @sql, N'@orderIDs IDTABLE READONLY', @orderIDs = @orderIDs

	-- mark the orders as exported FINAL (if instructed to do so)
	IF (@finalExport = 1) BEGIN
		-- This is the original (and preferred) way this is done
		EXEC spMarkOrderExportFinalCustomer @orderIDs, @exportedByUser
	END
	
END
GO

COMMIT
SET NOEXEC OFF

/* example of how to do SHIPXPRESS export
DECLARE @customerID_CSV varchar(max)
SET @customerID_CSV = dbo.fnShipXpressDXExportCustomerIDs()
EXEC spOrderExportFinalCustomer3 @customerID_CSV, 'system', 'ShipXpress', 0
*/