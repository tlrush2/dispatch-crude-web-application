-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.7.14.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.7.14.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.7.14'
SELECT  @NewVersion = '3.7.15'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Mobile App Support: Add tbl[Driver|Gauger]AppExceptionLog table & supporting sync logic to better track app abnormalities'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

CREATE TABLE tblDriverAppExceptionLog
(
  ID int identity(1, 1) NOT NULL 
, UID uniqueidentifier NOT NULL CONSTRAINT DF_DriverAppExceptionLog_UID default (newid()) constraint PK_DriverAppExceptionLog primary key
, DriverID int NOT NULL
, OrderID int NULL
, ExceptionDateUTC datetime NULL
, ExceptionLocation Text NULL
, ExceptionName TEXT NULL
, ExceptionDetails Text NULL
, CreateDateUTC datetime NOT NULL constraint DF_DriverAppExceptionLog_CreateDateUTC default (getutcdate())
, CreatedByUser varchar(255) NULL
)
GO
GRANT SELECT, INSERT ON tblDriverAppExceptionLog TO role_iis_acct
GO

CREATE TABLE tblGaugerAppExceptionLog
(
  ID int identity(1, 1) NOT NULL 
, UID uniqueidentifier NOT NULL CONSTRAINT DF_GaugerAppExceptionLog_UID default (newid()) constraint PK_GaugerAppExceptionLog primary key
, GaugerID int NOT NULL
, OrderID int NULL
, ExceptionDateUTC datetime NULL
, ExceptionLocation Text NULL
, ExceptionName TEXT NULL
, ExceptionDetails Text NULL
, CreateDateUTC datetime NOT NULL constraint DF_GaugerAppExceptionLog_CreateDateUTC default (getutcdate())
, CreatedByUser varchar(255) NULL
)
GO
GRANT SELECT, INSERT ON tblGaugerAppExceptionLog TO role_iis_acct
GO

COMMIT
SET NOEXEC OFF