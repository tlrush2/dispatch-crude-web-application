SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.7.2'
SELECT  @NewVersion = '4.7.2.1'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'JT-4512 - Cleanup to settlement log/pending status work'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/***********************************/
-- Created: ?.?.? - 2013/06/02 - Kevin Alons
-- Purpose: compute and add the various Carrier "Settlement" $$ values to an Audited order
-- Changes:
-- 4.1.0	- 2016/08/21 - KDA	- use new @SessionID parameter to create the entire settlement record in a single invocation
-- 4.1.8.6	- 2016.09.24 - KDA	- optimize data retrieval into 2 separate steps (MAJOR performance improvement workaround)
-- 4.3.0.2	- 2016.11.11 - KDA	- prevent execution if any orders in the session are already Settled
--								- remove related tblOrderSettlementSessionCarrier record at end of settlement if all records are settled
-- 4.6.2	- 2017.04.18 - KDA	- add @PeriodEndDate parameter and add to tblCarrierSettlementBatch on SettlementBatch creation
-- 4.7.0	- 2017/05/12 - JAE	- Add isfinal option to parameters
-- 4.7.2.1	- 2017/06/09 - JAE	- Add check for next batch num to look at audit table (don't reuse a batch #)
/***********************************/
ALTER PROCEDURE spCreateCarrierSettlementBatch
(
  @SessionID varchar(100)
, @CarrierID int
, @PeriodEndDate date
, @InvoiceNum varchar(50)
, @Notes varchar(255)
, @UserName varchar(255)
, @IsFinal bit = 1
, @BatchID int = NULL output
, @BatchNum int = NULL output
, @Outcome varchar(25) = 'Success' output
, @Msg varchar(255) = NULL output
) AS BEGIN
	IF @PeriodEndDate IS NULL SET @PeriodEndDate = cast(getdate() as date)

	-- retrieve the basic session order data first
	SELECT OS.OrderID, IsRated = cast(0 as bit), HasError = cast(0 as bit), IsSettled = cast(0 as bit)
	INTO #data
	FROM tblOrderSettlementSelectionCarrier OS 
	WHERE OS.SessionID = @SessionID AND OS.BatchSel = 1
	-- add the Financial information next (done in 2 steps for MAJOR performance reasons - a workaround but a more intuitive way was not identified)
	UPDATE #data
	  SET IsRated = cast(CASE WHEN OFB.InvoiceRatesAppliedDateUTC IS NULL THEN 0 ELSE 1 END as bit)
		, HasError = OFB.HasError
		, IsSettled = CASE WHEN BatchID IS NULL THEN 0 ELSE 1 END
	FROM #data O
	JOIN (
		SELECT OrderID, InvoiceRatesAppliedDateUTC, HasError , BatchID
		FROM viewOrder_Financial_Base_Carrier 
		WHERE OrderID IN (SELECT OrderID FROM #data)
	) OFB ON OFB.OrderID = O.OrderID

	-- validate the data next
	IF NOT EXISTS (SELECT * FROM #data)
	BEGIN
		SELECT @Outcome = 'Error', @Msg = 'At least order must be selected to complete settlement!'
		RETURN
	END
	ELSE IF EXISTS (SELECT * FROM #data WHERE IsRated = 0 OR HasError <> 0)
	BEGIN
		SELECT @Outcome = 'Error', @Msg = 'All selected orders must be fully rated with no errors to complete Settlement!'
		RETURN
	END
	ELSE IF EXISTS (SELECT * FROM #data WHERE IsSettled = 1)
	BEGIN
		SELECT @Outcome = 'Error', @Msg = 'At least 1 order is already settled - Refresh to re-query and continue!'
		RETURN
	END

	-- if validation passed, complete the settlement process below
	SET XACT_ABORT ON
	BEGIN TRAN CarrierSettlement

	INSERT INTO dbo.tblCarrierSettlementBatch(CarrierID, PeriodEndDate, InvoiceNum, BatchNum, BatchDate, Notes, IsFinal, CreateDateUTC, CreatedByUser)
		SELECT @CarrierID
			, @PeriodEndDate
			, @InvoiceNum
			, isnull(dbo.fnMaxInt(max(BatchNum), (SELECT max(BatchNum) FROM tblCarrierSettlementBatchDBAudit)), 0) + 1
			, dbo.fnDateOnly(getdate()), @Notes, @IsFinal, getutcdate(), @UserName 
		FROM dbo.tblCarrierSettlementBatch
	SELECT @BatchID = scope_identity()
	SELECT @BatchNum = BatchNum FROM dbo.tblCarrierSettlementBatch WHERE ID = @BatchID

	UPDATE tblOrderSettlementCarrier 
		SET BatchID = @BatchID
	FROM tblOrderSettlementCarrier OSC
	JOIN #data D ON D.OrderID = OSC.OrderID
	WHERE BatchID IS NULL  -- double-check that we aren't re-settling the order

	SELECT @Outcome = 'Success', @Msg = 'Settlement process complete: ' + ltrim(count(1)) + ' record(s) were added to batch: ' + ltrim(@BatchNum)
	FROM #data

	DELETE FROM tblOrderSettlementSelectionCarrier WHERE SessionID = @SessionID AND BatchSel = 1
	IF NOT EXISTS (SELECT SessionID FROM tblOrderSettlementSelectionCarrier WHERE SessionID = @SessionID)
		DELETE FROM tblOrderSettlementSessionCarrier WHERE ID = @SessionID

	COMMIT TRAN
END

GO

/***********************************/
-- Created: ?.?.? - 2013/06/02 - Kevin Alons
-- Purpose: compute and add the various Driver "Settlement" $$ values to an Audited order
-- Changes:
-- 4.1.8.6	- 2016.09.24 - KDA	- optimize data retrieval into 2 separate steps (MAJOR performance improvement workaround)
-- 4.3.0.2	- 2016.11.11 - KDA	- prevent execution if any orders in the session are already Settled
--								- remove related tblOrderSettlementSessionDriver record at end of settlement if all records are settled
-- 4.6.2	- 2017.04.18 - KDA	- add @PeriodEndDate parameter and add to tblDriverSettlementBatch on SettlementBatch creation
-- 4.7.0	- 2017/05/12 - JAE	- Add isfinal option to parameters
-- 4.7.2.1	- 2017/06/09 - JAE	- Add check for next batch num to look at audit table (don't reuse a batch #)
/***********************************/
ALTER PROCEDURE spCreateDriverSettlementBatch
(
  @SessionID varchar(100)
, @CarrierID int
, @DriverGroupID int = NULL
, @DriverID int = NULL
, @PeriodEndDate date
, @InvoiceNum varchar(50) = NULL
, @Notes varchar(255) = NULL
, @UserName varchar(255) 
, @IsFinal bit = 1
, @BatchID int = NULL output
, @BatchNum int = NULL output
, @Outcome varchar(25) = 'Success' output
, @Msg varchar(255) = NULL output
) AS BEGIN
	IF (@DriverGroupID = 0) SET @DriverGroupID = NULL
	IF (@DriverID = 0) SET @DriverID = NULL
	IF @PeriodEndDate IS NULL SET @PeriodEndDate = cast(getdate() as date)

	PRINT 'spCreateDriverSettlementBatch(' + ltrim(@SessionID) + ').Retrieve START ' + (CONVERT(VARCHAR(24), GETDATE(), 121))

	-- retrieve the basic session order data first
	SELECT OS.OrderID, IsRated = cast(0 as bit), HasError = cast(0 as bit), IsSettled = cast(0 as bit)
	INTO #data
	FROM tblOrderSettlementSelectionDriver OS 
	WHERE OS.SessionID = @SessionID AND OS.BatchSel = 1
	-- add the Financial information next (done in 2 steps for MAJOR performance reasons - a workaround but a more intuitive way was not identified)
	UPDATE #data
	  SET IsRated = cast(CASE WHEN OFB.InvoiceRatesAppliedDateUTC IS NULL THEN 0 ELSE 1 END as bit)
		, HasError = OFB.HasError
		, IsSettled = CASE WHEN BatchID IS NULL THEN 0 ELSE 1 END
	FROM #data O
	JOIN (
		SELECT OrderID, InvoiceRatesAppliedDateUTC, HasError, BatchID
		FROM viewOrder_Financial_Base_Driver 
		WHERE OrderID IN (SELECT OrderID FROM #data)
	) OFB ON OFB.OrderID = O.OrderID

	PRINT 'spCreateDriverSettlementBatch(' + ltrim(@SessionID) + ').Retrieve DONE ' + (CONVERT(VARCHAR(24), GETDATE(), 121))

	-- validate the data next
	IF NOT EXISTS (SELECT * FROM #data)
	BEGIN
		SELECT @Outcome = 'Error', @Msg = 'At least 1 order must be selected to complete settlement!'
		RETURN
	END
	ELSE IF EXISTS (SELECT * FROM #data WHERE IsRated = 0 OR HasError <> 0)
	BEGIN
		SELECT @Outcome = 'Error', @Msg = 'All selected orders must be fully rated with no errors to complete Settlement!'
		RETURN
	END
	ELSE IF EXISTS (SELECT * FROM #data WHERE IsSettled = 1)
	BEGIN
		SELECT @Outcome = 'Error', @Msg = 'At least 1 order is already settled - Refresh to re-query and continue!'
		RETURN
	END

	-- if validation passed, complete the settlement process below
	SET XACT_ABORT ON
	BEGIN TRAN DriverSettlement

	INSERT INTO dbo.tblDriverSettlementBatch(CarrierID, DriverGroupID, DriverID, PeriodEndDate, InvoiceNum, BatchNum, BatchDate, Notes, IsFinal, CreateDateUTC, CreatedByUser)
		SELECT @CarrierID, @DriverGroupID, @DriverID
			, @PeriodEndDate
			, @InvoiceNum
			, isnull(dbo.fnMaxInt(max(BatchNum), (SELECT max(BatchNum) FROM tblDriverSettlementBatchDBAudit)), 0) + 1
			, dbo.fnDateOnly(getdate()), @Notes, @IsFinal, getutcdate(), @UserName 
		FROM dbo.tblDriverSettlementBatch
	SELECT @BatchID = scope_identity()
	SELECT @BatchNum = BatchNum FROM dbo.tblDriverSettlementBatch WHERE ID = @BatchID

	UPDATE tblOrderSettlementDriver 
		SET BatchID = @BatchID
	FROM tblOrderSettlementDriver OSC
	JOIN #data D ON D.OrderID = OSC.OrderID
	WHERE BatchID IS NULL  -- double-check that we aren't re-settling the order

	SELECT @Outcome = 'Success', @Msg = 'Settlement process complete: ' + ltrim(count(1)) + ' record(s) were added to batch: ' + ltrim(@BatchNum)
	FROM #data

	DELETE FROM tblOrderSettlementSelectionDriver WHERE SessionID = @SessionID AND BatchSel = 1
	IF NOT EXISTS (SELECT SessionID FROM tblOrderSettlementSelectionDriver WHERE SessionID = @SessionID)
		DELETE FROM tblOrderSettlementSessionDriver WHERE ID = @SessionID

	COMMIT TRAN
END

GO

/***********************************/
-- Created: ?.?.? - 2013/06/02 - Kevin Alons
-- Purpose: compute and add the various Shipper "Settlement" $$ values to an Audited order
-- Changes:
-- 4.1.0 - 2016/08/21 - KDA - use new @SessionID parameter to create the entire settlement record in a single invocation
-- 4.1.8.6	- 2016.09.24 - KDA	- optimize data retrieval into 2 separate steps (MAJOR performance improvement workaround)
-- 4.3.0.2	- 2016.11.11 - KDA	- prevent execution if any orders in the session are already Settled
--								- remove related tblOrderSettlementSessionShipper record at end of settlement if all records are settled
-- 4.6.2	- 2017.04.18 - KDA	- add @PeriodEndDate parameter and add to tblShipperSettlementBatch on SettlementBatch creation
-- 4.7.0	- 2017/05/12 - JAE	- Add isfinal option to parameters
-- 4.7.2.1	- 2017/06/09 - JAE	- Add check for next batch num to look at audit table (don't reuse a batch #)
/***********************************/
ALTER PROCEDURE spCreateShipperSettlementBatch
(
  @SessionID varchar(100)
, @ShipperID int
, @PeriodEndDate date
, @InvoiceNum varchar(50)
, @Notes varchar(255)
, @UserName varchar(255)
, @BatchID int = NULL output
, @BatchNum int = NULL output
, @IsFinal bit = 1
, @Outcome varchar(25) = 'Success' output
, @Msg varchar(255) = NULL output
) AS BEGIN
	IF @PeriodEndDate IS NULL SET @PeriodEndDate = cast(getdate() as date)

	-- retrieve the basic session order data first
	SELECT OS.OrderID, IsRated = cast(0 as bit), HasError = cast(0 as bit), IsSettled = cast(0 as bit)
	INTO #data
	FROM tblOrderSettlementSelectionShipper OS 
	WHERE OS.SessionID = @SessionID AND OS.BatchSel = 1
	-- add the Financial information next (done in 2 steps for MAJOR performance reasons - a workaround but a more intuitive way was not identified)
	UPDATE #data
	  SET IsRated = cast(CASE WHEN OFB.InvoiceRatesAppliedDateUTC IS NULL THEN 0 ELSE 1 END as bit)
		, HasError = OFB.HasError
		, IsSettled = CASE WHEN BatchID IS NULL THEN 0 ELSE 1 END
	FROM #data O
	JOIN (
		SELECT OrderID, InvoiceRatesAppliedDateUTC, HasError, BatchID
		FROM viewOrder_Financial_Base_Shipper 
		WHERE OrderID IN (SELECT OrderID FROM #data)
	) OFB ON OFB.OrderID = O.OrderID

	-- validate the data next
	IF NOT EXISTS (SELECT * FROM #data)
	BEGIN
		SELECT @Outcome = 'Error', @Msg = 'At least order must be selected to complete settlement!'
		RETURN
	END
	ELSE IF EXISTS (SELECT * FROM #data WHERE IsRated = 0 OR HasError <> 0)
	BEGIN
		SELECT @Outcome = 'Error', @Msg = 'All selected orders must be fully rated with no errors to complete Settlement!'
		RETURN
	END
	ELSE IF EXISTS (SELECT * FROM #data WHERE IsSettled = 1)
	BEGIN
		SELECT @Outcome = 'Error', @Msg = 'At least 1 order is already settled - Refresh to re-query and continue!'
		RETURN
	END

	-- if validation passed, complete the settlement process below
	SET XACT_ABORT ON
	BEGIN TRAN ShipperSettlement

	INSERT INTO dbo.tblShipperSettlementBatch(ShipperID, PeriodEndDate, InvoiceNum, BatchNum, BatchDate, Notes, IsFinal, CreateDateUTC, CreatedByUser)
		SELECT @ShipperID
			, @PeriodEndDate
			, @InvoiceNum
			, isnull(dbo.fnMaxInt(max(BatchNum), (SELECT max(BatchNum) FROM tblShipperSettlementBatchDBAudit)), 0) + 1
			, dbo.fnDateOnly(getdate()), @Notes, @IsFinal, getutcdate(), @UserName 
		FROM dbo.tblShipperSettlementBatch
	SELECT @BatchID = scope_identity()
	SELECT @BatchNum = BatchNum FROM dbo.tblShipperSettlementBatch WHERE ID = @BatchID

	UPDATE tblOrderSettlementShipper 
		SET BatchID = @BatchID
	FROM tblOrderSettlementShipper OSC
	JOIN #data D ON D.OrderID = OSC.OrderID
	WHERE BatchID IS NULL  -- double-check that we aren't re-settling the order

	SELECT @Outcome = 'Success', @Msg = 'Settlement process complete: ' + ltrim(count(1)) + ' record(s) were added to batch: ' + ltrim(@BatchNum)
	FROM #data

	DELETE FROM tblOrderSettlementSelectionShipper WHERE SessionID = @SessionID AND BatchSel = 1
	IF NOT EXISTS (SELECT SessionID FROM tblOrderSettlementSelectionShipper WHERE SessionID = @SessionID)
		DELETE FROM tblOrderSettlementSessionShipper WHERE ID = @SessionID

	COMMIT TRAN
END

GO


/*************************************************************/
-- Date Created: 2017/05/19 (4.7.1)
-- Author: Joe Engler
-- Purpose: Show Carrier settlement history
-- Changes:
--		4.7.2.1		JAE			2017-06-07		Fix to Order Date
/*************************************************************/
ALTER FUNCTION fnCarrierSettlementHistory (@BatchID INT, @OrderID INT) RETURNS TABLE AS
RETURN
	SELECT Description = 'Settled'
		, IsCurrent = 1
		, OSC.OrderID
		, OSC.BatchNum
		, O.OrderNum
		, JobNumber = NULLIF(O.JobNumber, '')
		, ContractNumber = NULLIF(O.ContractNumber, '')
		, O.OrderDate
		, Shipper = O.Customer
		, Carrier = O.Carrier
		, O.Origin
		, O.Destination
		, PreviousDestinations = dbo.fnRerouteDetails(O.ID, 'PreviousDestinations', '<br/>') 
		, O.ActualMiles
		, SettlementVolume = OSC.SettlementUnits
		, OSC.SettlementUom
		, OSC.OriginChainupAmount
		, OSC.DestChainupAmount
		, OSC.H2SAmount
		, OSC.RerouteAmount
		, OSC.OrderRejectAmount
		, OSC.OriginWaitAmount
		, OSC.DestinationWaitAmount
		, OSC.TotalWaitAmount
		, OSC.SplitLoadAmount
		, MiscDetails = dbo.fnOrderShipperAssessorialDetailsTSV(O.ID, 0)
		, MiscAmount = OSC.OtherAmount
		, OSC.FuelSurchargeAmount
		, OSC.OriginTaxRate
		, OSC.RouteRate
		, OSC.RateSheetRate
		, OSC.LoadAmount
		, OSC.TotalAmount
		, CSB.BatchDate
		, CSB.PeriodEndDate
		, CSB.InvoiceNum
		, OrderNotes = OSC.Notes
		, BatchNotes = CSB.Notes
		, SettledByUser = ISNULL(CSB.LastChangedByUser, CSB.CreatedByUser)
		, SettlementTimestamp = ISNULL(CSB.LastChangeDateUTC, CSB.CreateDateUTC)
	FROM viewOrderSettlementCarrier OSC 
	JOIN viewOrder O ON OSC.OrderID = O.ID
	JOIN tblCarrierSettlementBatch CSB ON CSB.ID = OSC.BatchID
	WHERE CSB.IsFinal = 1
  	AND (@BatchID = -1 OR Batchid = @BatchID)
	AND (@OrderID = -1 OR OrderID=@OrderID)

	UNION

	SELECT Description = 'Settled', IsCurrent=0
		, OrderID
		, BatchNum
		, OrderNum
		, JobNumber = NULLIF(JobNumber, '')
		, ContractNumber = NULLIF(ContractNumber, '')
		, OrderDate
		, Shipper
		, Carrier
		, Origin
		, Destination
		, PreviousDestinations
		, ActualMiles
		, InvoiceUnits
		, InvoiceSettlementUom
		, InvoiceOriginChainupAmount
		, InvoiceDestChainupAmount
		, InvoiceH2SAmount
		, InvoiceRerouteAmount
		, InvoiceOrderRejectAmount
		, InvoiceOriginWaitAmount
		, InvoiceDestinationWaitAmount
		, InvoiceTotalWaitAmount
		, InvoiceSplitLoadAmount
		, InvoiceOtherDetailsTSV
		, InvoiceOtherAmount
		, InvoiceFuelSurchargeAmount
		, InvoiceTaxRate
		, InvoiceRouteRate
		, InvoiceRateSheetRate
		, InvoiceLoadAmount
		, InvoiceTotalAmount
		, BatchDate
		, InvoicePeriodEnd
		, BatchInvoiceNum
		, OrderNotes = AuditNotes
		, BatchNotes = InvoiceNotes
		, SettledByUser = ISNULL(BatchLastChangedByUser, BatchCreatedByUser)
		, SettlementTimestamp = ISNULL(BatchLastChangeDateUTC, BatchCreateDateUTC)
	FROM tblOrderSettlementCarrierDbAudit
	WHERE (@BatchID = -1 OR Batchid = @BatchID)
	AND (@OrderID = -1 OR OrderID=@OrderID)

	UNION

	SELECT Description = 'Unsettled'
		, IsCurrent=0
		, OrderID
		, BatchNum
		, OrderNum
		, JobNumber = NULLIF(JobNumber, '')
		, ContractNumber = NULLIF(ContractNumber, '')
		, OrderDate
		, Shipper
		, Carrier
		, Origin
		, Destination
		, PreviousDestinations
		, ActualMiles
		, -InvoiceUnits
		, InvoiceSettlementUom
		, -InvoiceOriginChainupAmount
		, -InvoiceDestChainupAmount
		, -InvoiceH2SAmount
		, -InvoiceRerouteAmount
		, -InvoiceOrderRejectAmount
		, -InvoiceOriginWaitAmount
		, -InvoiceDestinationWaitAmount
		, -InvoiceTotalWaitAmount
		, -InvoiceSplitLoadAmount
		, InvoiceOtherDetailsTSV
		, -InvoiceOtherAmount
		, -InvoiceFuelSurchargeAmount
		, InvoiceTaxRate
		, InvoiceRouteRate
		, InvoiceRateSheetRate
		, -InvoiceLoadAmount
		, -InvoiceTotalAmount
		, BatchDate
		, InvoicePeriodEnd
		, BatchInvoiceNum
		, OrderNotes = AuditNotes
		, BatchNotes = InvoiceNotes
		, SettledByUser = ISNULL(BatchLastChangedByUser, BatchCreatedByUser)
		, SettlementTimestamp = DbAuditDate
	FROM tblOrderSettlementCarrierDbAudit
	WHERE (@BatchID = -1 OR Batchid = @BatchID)
	AND (@OrderID = -1 OR OrderID=@OrderID)

GO


-- Create indexes 
CREATE NONCLUSTERED INDEX idxOrderSettlementCarrierDbAudit_OrderID ON tblOrderSettlementCarrierDbAudit(OrderID ASC)
CREATE NONCLUSTERED INDEX idxOrderSettlementCarrierDbAudit_BatchID ON tblOrderSettlementCarrierDbAudit(BatchID ASC)
CREATE NONCLUSTERED INDEX idxOrderSettlementCarrierDbAudit_CarrierID ON tblOrderSettlementCarrierDbAudit(CarrierID ASC)
GO

CREATE NONCLUSTERED INDEX idxOrderSettlementDriverDbAudit_OrderID ON tblOrderSettlementDriverDbAudit(OrderID ASC)
CREATE NONCLUSTERED INDEX idxOrderSettlementDriverDbAudit_BatchID ON tblOrderSettlementDriverDbAudit(BatchID ASC)
CREATE NONCLUSTERED INDEX idxOrderSettlementDriverDbAudit_DriverID ON tblOrderSettlementDriverDbAudit(DriverID ASC)
GO

CREATE NONCLUSTERED INDEX idxOrderSettlementShipperDbAudit_OrderID ON tblOrderSettlementShipperDbAudit(OrderID ASC)
CREATE NONCLUSTERED INDEX idxOrderSettlementShipperDbAudit_BatchID ON tblOrderSettlementShipperDbAudit(BatchID ASC)
CREATE NONCLUSTERED INDEX idxOrderSettlementShipperDbAudit_ShipperID ON tblOrderSettlementShipperDbAudit(ShipperID ASC)
GO


COMMIT
SET NOEXEC OFF
