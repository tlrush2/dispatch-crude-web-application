/*
	-- add Reason codes to ReportCenter.Order report
	-- add Reason fields to viewOrder (and related)
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.1.2'
SELECT  @NewVersion = '3.1.3'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

INSERT tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText) 
	VALUES (114, 1, N'OriginWaitReason', N'Origin Wait Reason', NULL, 'OriginWaitReasonID', 2, 'SELECT ID, Name=NumDesc FROM viewOriginWaitReason WHERE DeleteDateUTC IS NULL ORDER BY NumDesc', 0)
INSERT tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText) 
	VALUES (115, 1, N'DestWaitReason', N'Dest Wait Reason', NULL, 'DestWaitReasonID', 2, 'SELECT ID, Name=NumDesc FROM viewDestinationWaitReason WHERE DeleteDateUTC IS NULL ORDER BY NumDesc', 0)
INSERT tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText) 
	VALUES (116, 1, N'RejectReason', N'Order Reject Reason', NULL, 'RejectReasonID', 2, 'SELECT ID, Name=NumDesc FROM viewOrderRejectReason WHERE DeleteDateUTC IS NULL ORDER BY NumDesc', 0)
GO

/***********************************/
-- Date Created: 7 Sep 014
-- Author: Kevin Alons
-- Purpose: return Reason records with NumDesc "computed" column
/***********************************/
CREATE VIEW viewOriginWaitReason AS
SELECT *
	, NumDesc = Num + ': ' + Description
FROM tblOriginWaitReason

GO
GRANT SELECT ON viewOriginWaitReason TO dispatchcrude_iis_acct
GO
/***********************************/
-- Date Created: 7 Sep 014
-- Author: Kevin Alons
-- Purpose: return Reason records with NumDesc "computed" column
/***********************************/
CREATE VIEW viewDestinationWaitReason AS
SELECT *
	, NumDesc = Num + ': ' + Description
FROM tblDestinationWaitReason

GO
GRANT SELECT ON viewDestinationWaitReason TO dispatchcrude_iis_acct
GO
/***********************************/
-- Date Created: 7 Sep 014
-- Author: Kevin Alons
-- Purpose: return Reason records with NumDesc "computed" column
/***********************************/
CREATE VIEW viewOrderRejectReason AS
SELECT *
	, NumDesc = Num + ': ' + Description
FROM tblOrderRejectReason

GO
GRANT SELECT ON viewOrderRejectReason TO dispatchcrude_iis_acct
GO
/***********************************/
-- Date Created: 7 Sep 014
-- Author: Kevin Alons
-- Purpose: return Reason records with NumDesc "computed" column
/***********************************/
CREATE VIEW viewOrderTicketRejectReason AS
SELECT *
	, NumDesc = Num + ': ' + Description
FROM tblOrderTicketRejectReason

GO
GRANT SELECT ON viewOrderTicketRejectReason TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Order records with "translated friendly" values for FK relationships
/***********************************/
ALTER VIEW [dbo].[viewOrder] AS
SELECT O.*
	, OPS.OrderStatus AS PrintStatus
FROM (
	SELECT O.*
	, OrderDate = dbo.fnDateOnly(dbo.fnUTC_to_Local(O.OriginDepartTimeUTC, vO.TimeZoneID, vO.UseDST)) 
	, Origin = vO.Name 
	, OriginFull = vO.FullName 
	, OriginState = vO.State 
	, OriginStateAbbrev = vO.StateAbbrev 
	, OriginStation = vO.Station 
	, vO.LeaseName
	, vO.LeaseNum
	, OriginLegalDescription = vO.LegalDescription 
	, OriginNDIC = vO.NDICFileNum 
	, OriginNDM = vO.NDM 
	, OriginCA = vO.CA 
	, OriginTimeZoneID = vO.TimeZoneID 
	, OriginUseDST = vO.UseDST 
	, OriginStateID = vO.StateID
	, vO.H2S
	, Destination = vD.Name 
	, DestinationFull = vD.FullName 
	, DestinationState = vD.State 
	, DestinationStateAbbrev = vD.StateAbbrev 
	, DestinationTypeID = vD.ID
	, vD.DestinationType
	, DestStation = vD.Station 
	, DestTimeZoneID = vD.TimeZoneID 
	, DestUseDST = vD.UseDST 
	, DestStateID = vD.StateID
	, Customer = C.Name 
	, Carrier = CA.Name 
	, CarrierType = CT.Name 
	, OS.OrderStatus
	, OS.StatusNum
	, Driver = D.FullName 
	, DriverFirst = D.FirstName 
	, DriverLast = D.LastName 
	, Truck = TRU.FullName 
	, Trailer = TR1.FullName 
	, Trailer2 = TR2.FullName 
	, P.PriorityNum
	, TicketType = TT.Name 
	, DestTicketTypeID = vD.TicketTypeID 
	, DestTicketType = vD.TicketType 
	, Operator = OP.Name 
	, Producer = PR.Name 
	, Pumper = PU.FullName 
	, DriverNumber = D.IDNumber
	, CarrierNumber = CA.IDNumber
	, CarrierTypeID = CA.CarrierTypeID
	, TruckNumber = TRU.IDNumber 
	, TrailerNumber = TR1.IDNumber 
	, Trailer2Number = TR2.IDNumber 
	, Product = PRO.Name
	, ProductShort = PRO.ShortName 
	, ProductGroup = isnull(PRO.ProductGroup, PRO.ShortName)
	, OriginUOM = OUom.Name 
	, OriginUomShort = OUom.Abbrev 
	, OriginTankID_Text = CASE WHEN ORT.TankNum = '*' THEN '*' + isnull(O.OriginTankNum, '?') ELSE ORT.TankNum END 
	, DestUOM = DUom.Name 
	, DestUomShort = DUom.Abbrev 
	, Active = cast((CASE WHEN O.DeleteDateUTC IS NULL THEN 1 ELSE 0 END) as bit) 
	, IsDeleted = cast((CASE WHEN O.DeleteDateUTC IS NOT NULL THEN 1 ELSE 0 END) as bit) 
	, PickupPrintStatus = PPS.Name 
	, PickupCompleted = PPS.IsCompleted 
	, DeliverPrintStatus = DPS.Name 
	, DeliverCompleted = DPS.IsCompleted 
	, PrintStatusID = CASE WHEN StatusID = 8 AND PPS.IsCompleted = 0 THEN 7
						   WHEN StatusID = 3 AND DPS.IsCompleted = 0 THEN 8
						ELSE StatusID END 
	, OriginTankText = CASE WHEN O.OriginTankID IS NULL OR ORT.TankNum = '*' THEN O.OriginTankNum ELSE ORT.TankNum END
	, OriginWaitReason = OWR.NumDesc
	, DestWaitReason = DWR.NumDesc
	, RejectReason = ORR.NumDesc
	FROM dbo.tblOrder O
	LEFT JOIN dbo.viewOrigin vO ON vO.ID = O.OriginID
	LEFT JOIN dbo.viewDestination vD ON vD.ID = O.DestinationID
	LEFT JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
	JOIN dbo.tblOrderStatus AS OS ON OS.ID = O.StatusID
	LEFT JOIN dbo.tblCarrier CA ON CA.ID = O.CarrierID
	LEFT JOIN dbo.tblCarrierType CT ON CT.ID = CA.CarrierTypeID
	LEFT JOIN dbo.viewDriver D ON D.ID = O.DriverID
	LEFT JOIN dbo.viewTruck TRU ON TRU.ID = O.TruckID
	LEFT JOIN dbo.viewTrailer TR1 ON TR1.ID = O.TrailerID
	LEFT JOIN dbo.viewTrailer TR2 ON TR2.ID = O.Trailer2ID
	LEFT JOIN dbo.tblPriority P ON P.ID = O.PriorityID
	LEFT JOIN dbo.tblTicketType TT ON TT.ID = O.TicketTypeID
	LEFT JOIN dbo.tblOriginTank ORT ON ORT.ID = O.OriginTankID
	LEFT JOIN dbo.tblOperator OP ON OP.ID = O.OperatorID
	LEFT JOIN dbo.viewPumper PU ON PU.ID = O.PumperID
	LEFT JOIN dbo.tblProducer PR ON PR.ID = O.ProducerID
	LEFT JOIN dbo.tblProduct PRO ON PRO.ID = O.ProductID
	LEFT JOIN dbo.tblUom OUom ON OUom.ID = O.OriginUomID
	LEFT JOIN dbo.tblUom DUom ON DUom.ID = O.DestUomID
	LEFT JOIN dbo.tblPrintStatus PPS ON PPS.ID = O.PickupPrintStatusID
	LEFT JOIN dbo.tblPrintStatus DPS ON DPS.ID = O.DeliverPrintStatusID
	LEFT JOIN dbo.viewOriginWaitReason OWR ON OWR.ID = O.OriginWaitReasonID
	LEFT JOIN dbo.viewDestinationWaitReason DWR ON DWR.ID = O.DestWaitReasonID
	LEFT JOIN dbo.viewOrderRejectReason ORR ON ORR.ID = O.RejectReasonID
) O
LEFT JOIN dbo.tblOrderStatus OPS ON OPS.ID = O.PrintStatusID

GO

CREATE INDEX idxOrder_Status_DeleteDateUTC
ON [dbo].[tblOrder] ([StatusID],[DeleteDateUTC])
GO

EXEC _spRebuildAllObjects
GO

EXEC _spRefreshAllViews
EXEC _spRefreshAllViews
EXEC _spRefreshAllViews

COMMIT
SET NOEXEC OFF