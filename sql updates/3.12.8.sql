SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.12.7.8'
SELECT  @NewVersion = '3.12.8'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Import Center: providing input string longer than import field max length silently fails'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/************************************************
 Creation Info: 3.10.1 - 2015/11/13
 Author: Kevin Alons
 Purpose: return the tblObjectField data + relevant parent tblObject data
 Changes:
	-- 3.12.8	- 2016/06/24	- KDA	- add MaxStringLength field (for string fields only, others are NULL)
************************************************/
ALTER VIEW viewObjectField AS
	SELECT ObF.*
		, AllowNull = cast(AllowNullID as bit) -- this cast will convert 1 and 2 to TRUE, leaving 0 as FALSE
		, Object = O.Name
		, ObjectSqlSourceName = O.SqlSourceName
		, ObjectSqlTargetName = O.SqlTargetName
		, MaxStringLength = (SELECT TOP 1 character_maximum_length FROM information_schema.columns WHERE table_name = O.SqlTargetName AND column_name = Obf.FieldName)
	FROM tblObjectField ObF
	JOIN tblObject O ON O.ID = ObF.ObjectID

GO

/*******************************************************
 Creation Info:	3.10.1 - 2016/01/04
 Author:		Kevin Alons
 Purpose:		return all fields + some support fields for tblImportCenterFieldDefinition
 Changes:
- 3.10.12	- 2016/02/06	- KDA	- add IsCustom field (from tblObjectField)
- 3.11.8	- 2016/02/04	- KDA	- add InputFieldsCSV field
									- add OBF.ReadOnly field
									- add ObjectReadOnly field
- 3.12.8	- 2016/06/24	- KDA	- add new MaxStringLength field
*******************************************************/
ALTER VIEW viewImportCenterFieldDefinition AS
	SELECT ICDF.* 
		, HasChildren = CASE WHEN EXISTS (SELECT ID FROM tblImportCenterFieldDefinition WHERE ParentFieldID = ICDF.ID) THEN 1 ELSE 0 END
		, Name = OBF.Name , ObjectName = OBF.Object, OBF.FieldName, OBF.ObjectFieldTypeID, OBF.ObjectID, OBF.ObjectSqlSourceName, OBF.ObjectSqlTargetName
		, ObjectReadOnly = CASE WHEN OBF.ObjectSqlTargetName IS NULL THEN 1 ELSE 0 END
		, IDFieldName = (SELECT TOP 1 FieldName FROM tblObjectField WHERE ObjectID = OBF.ObjectID AND IsKey = 1)
		, OBF.AllowNull, OBF.IsCustom
		, OBF.ReadOnly
		, OBF.ParentObjectID
		, OBF.DefaultValue
		, InputFieldsCSV = dbo.fnICF_FieldCSV(ICDF.ID)
		, NestLevel = dbo.fnICFNestLevel(ICDF.ID) 
		, OBF.MaxStringLength
	FROM tblImportCenterFieldDefinition ICDF
	JOIN viewObjectField OBF ON OBF.ID = ICDF.ObjectFieldID
	LEFT JOIN tblImportCenterFieldDefinition PICDF ON PICDF.ID = ICDF.ParentFieldID
	LEFT JOIN viewObjectField POBF ON POBF.ID = PICDF.ObjectFieldID

GO

COMMIT
SET NOEXEC OFF