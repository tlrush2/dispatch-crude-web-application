DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '1.4.5', @NewVersion = '1.4.6'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

EXECUTE sp_rename N'dbo.tblCarrier.InsuranceCertificateDocName', N'LiabilityInsuranceDocName', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.tblCarrier.InsuranceCertificateDocument', N'LiabilityInsuranceDocument', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.tblCarrier.InsuranceExpiration', N'LiabilityInsuranceExpiration', 'COLUMN' 
GO
ALTER TABLE tblCarrier ADD WorkersCompDocName varchar(255) null
GO
ALTER TABLE tblCarrier ADD WorkersCompDocument varbinary(max) null
GO
ALTER TABLE tblCarrier ADD WorkersCompExpiration smalldatetime null
GO

EXEC _spRefreshAllViews
GO

COMMIT TRANSACTION
SET NOEXEC OFF

