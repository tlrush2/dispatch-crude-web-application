SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.0.10'
SELECT  @NewVersion = '4.0.11'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1689 Add origin/destination arrive/depart photo columns to ebol page'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/***********************************
 Date Created: 28 Feb 2013
 Author: Kevin Alons
 Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc for 3rd parties (Customer/Carrier)
 Changes:
	??		- 8/18/15  - BB		- Made delivered orders possible to print eBOLs page
	3.9.31	- 12/04/15 - JAE&BB - Join tblOrderPhoto for access to most recent photos on eBOL print page
								- convert @StartDate & @EndDate to date data type (eliminates need to use fnDateOnly() )
			- 12/09/15 - BB     - Made reject photo accessible separately
	4.0.5	- 08/24/16 - BB		- DCWEB-1655: Added filters for Job#, Contract#, origin, destination, driver, and rejected status
	4.0.11	- 09/01/16 - BB		- DCWEB-1689: Add origin/destination arrive/depart photos
***********************************/
ALTER PROCEDURE [dbo].[spOrdersBasicExport]
(
  @StartDate date
, @EndDate date
, @CarrierID int = 0 -- -1 = all carriers
, @CustomerID int = 0 -- -1 = all customers
, @ProducerID int = 0
, @OrderNum varchar(20) = NULL
, @JobNumber varchar(20) = NULL
, @ContractNumber varchar(20) = NULL
, @DriverID int = 0
, @OriginID int = 0
, @DestinationID int = 0
, @Rejected int = 0
, @StatusID_CSV varchar(max) = '3,4'
) AS BEGIN	
	SELECT OE.*
		, OriginPhotoID = (SELECT TOP 1 OP.ID FROM tblOrderPhoto OP WHERE OrderID = OE.ID AND OP.OriginID IS NOT NULL AND OP.PhotoTypeID = 1 ORDER BY OP.CreateDateUTC DESC)
		, OriginArrivePhotoID = (SELECT TOP 1 OP.ID FROM tblOrderPhoto OP WHERE OrderID = OE.ID AND OP.OriginID IS NOT NULL AND OP.PhotoTypeID = 3 ORDER BY OP.CreateDateUTC DESC)
		, OriginDepartPhotoID = (SELECT TOP 1 OP.ID FROM tblOrderPhoto OP WHERE OrderID = OE.ID AND OP.OriginID IS NOT NULL AND OP.PhotoTypeID = 4 ORDER BY OP.CreateDateUTC DESC)
		, RejectPhotoID = (SELECT TOP 1 OP.ID FROM tblOrderPhoto OP WHERE OrderID = OE.ID AND OE.Rejected = 1 AND OP.PhotoTypeID = 2 ORDER BY OP.CreateDateUTC DESC)
		, DestPhotoID = (SELECT TOP 1 OP.ID FROM tblOrderPhoto OP WHERE OrderID = OE.ID AND OP.DestinationID IS NOT NULL AND OP.PhotoTypeID = 1 ORDER BY OP.CreateDateUTC DESC)		
		, DestArrivePhotoID = (SELECT TOP 1 OP.ID FROM tblOrderPhoto OP WHERE OrderID = OE.ID AND OP.DestinationID IS NOT NULL AND OP.PhotoTypeID = 3 ORDER BY OP.CreateDateUTC DESC)		
		, DestDepartPhotoID = (SELECT TOP 1 OP.ID FROM tblOrderPhoto OP WHERE OrderID = OE.ID AND OP.DestinationID IS NOT NULL AND OP.PhotoTypeID = 4 ORDER BY OP.CreateDateUTC DESC)		
	FROM dbo.viewOrderExportFull_Reroute OE
	WHERE (@ProducerID=0 OR OE.ProducerID=@ProducerID)
	  AND (@CarrierID=-1 OR @CarrierID=0 OR CarrierID=@CarrierID) 
	  AND (@CustomerID=-1 OR @CustomerID=0 OR CustomerID=@CustomerID)
	  AND OE.OrderDate BETWEEN @StartDate AND @EndDate
	  AND OE.DeleteDateUTC IS NULL
	  AND (OE.StatusID IN (SELECT ID FROM dbo.fnSplitCSVIDs(@StatusID_CSV)))
	  AND (@OrderNum IS NULL OR OE.OrderNum LIKE @OrderNum)
	  AND (@JobNumber IS NULL OR OE.JobNumber LIKE @JobNumber)
	  AND (@ContractNumber IS NULL OR OE.ContractNumber LIKE @ContractNumber)
	  AND (@DriverID=-1 OR @DriverID=0 OR OE.DriverID=@DriverID)
	  AND (@OriginID=-1 OR @OriginID=0 OR OE.OriginID=@OriginID)
	  AND (@DestinationID=-1 OR @DestinationID=0 OR OE.DestinationID=@DestinationID)	  
	  AND (@Rejected=0 OR OE.Rejected=@Rejected)
	ORDER BY OE.OrderDate, OE.OrderNum
END
GO


COMMIT
SET NOEXEC OFF