-- rollback
-- select value from tblsetting where id = 0
/*
	-- add Audit Error fields to the Audit Source
	-- add GPS fields to Order Audit Source
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.1.16'
SELECT  @NewVersion = '3.2.0'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

CREATE NONCLUSTERED INDEX idxDriverLocation_Covering1
ON [dbo].[tblDriverLocation] ([OrderID],[OriginID],[SourceDateUTC])
INCLUDE ([ID],[UID],[DriverID],[DestinationID],[LocationTypeID],[Lat],[Lon],[DistanceToPoint],[SourceAccuracyMeters],[CreateDateUTC],[CreatedByUser])
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[viewDriverLocation_FirstArrive]'))
	DROP VIEW [dbo].[viewDriverLocation_FirstArrive]
GO 
/**********************************************************
-- Author: Kevin Alons
-- Date Created: 8 Oct 2014
-- Purpose: JOIN the DriverLocation table with the Order Table to find the most suitable Origin Arrival GPS record
***********************************************************/
CREATE VIEW viewDriverLocation_OriginFirstArrive AS
SELECT DL3.*
FROM tblDriverLocation DL3
JOIN (
	-- get the first matching record (by lowest UID)
	SELECT DL2.OrderID, DL2.OriginID, UID = min(cast(DL2.UID as varchar(100)))
	FROM tblDriverLocation DL2
	JOIN (
		-- get the best (lowest) SourceAccuracyMeters
		SELECT DL.OrderID, DL.OriginID, X.SourceDateUTC, SourceAccuracyMeters = min(DL.SourceAccuracyMeters)
		FROM tblDriverLocation DL
		JOIN (
			-- get the first SourceDateUTC
			SELECT OrderID, OriginID, SourceDateUTC = MIN(SourceDateUTC)
			FROM tblDriverLocation 
			WHERE OriginID IS NOT NULL
			GROUP BY OrderID, OriginID
		) X ON X.OrderID = DL.OrderID
			AND X.OriginID = DL.OriginID
			AND X.SourceDateUTC = DL.SourceDateUTC
		GROUP BY DL.OrderID, DL.OriginID, X.SourceDateUTC
	) X2 ON X2.OrderID = DL2.OrderID AND X2.OriginID = DL2.OriginID 
		AND X2.SourceDateUTC = DL2.SourceDateUTC AND X2.SourceAccuracyMeters = DL2.SourceAccuracyMeters
	GROUP BY DL2.OrderID, DL2.OriginID
) X3 ON X3.OrderID = DL3.OrderID AND X3.OriginID = DL3.OriginID  AND X3.UID = DL3.UID
GO
GRANT SELECT ON viewDriverLocation_OriginFirstArrive TO dispatchcrude_iis_acct
GO

/**********************************************************
-- Author: Kevin Alons
-- Date Created: 8 Oct 2014
-- Purpose: JOIN the DriverLocation table with the Order Table to find the most suitable Destination Arrival GPS record
***********************************************************/
CREATE VIEW viewDriverLocation_DestinationFirstArrive AS
SELECT DL3.*
FROM tblDriverLocation DL3
JOIN (
	-- get the first matching record (by lowest UID)
	SELECT DL2.OrderID, DL2.DestinationID, UID = min(cast(DL2.UID as varchar(100)))
	FROM tblDriverLocation DL2
	JOIN (
		-- get the best (lowest) SourceAccuracyMeters
		SELECT DL.OrderID, DL.DestinationID, X.SourceDateUTC, SourceAccuracyMeters = min(DL.SourceAccuracyMeters)
		FROM tblDriverLocation DL
		JOIN (
			-- get the first SourceDateUTC
			SELECT OrderID, DestinationID, SourceDateUTC = MIN(SourceDateUTC)
			FROM tblDriverLocation 
			WHERE DestinationID IS NOT NULL
			GROUP BY OrderID, DestinationID
		) X ON X.OrderID = DL.OrderID
			AND X.DestinationID = DL.DestinationID
			AND X.SourceDateUTC = DL.SourceDateUTC
		GROUP BY DL.OrderID, DL.DestinationID, X.SourceDateUTC
	) X2 ON X2.OrderID = DL2.OrderID AND X2.DestinationID = DL2.DestinationID 
		AND X2.SourceDateUTC = DL2.SourceDateUTC AND X2.SourceAccuracyMeters = DL2.SourceAccuracyMeters
	GROUP BY DL2.OrderID, DL2.DestinationID
) X3 ON X3.OrderID = DL3.OrderID AND X3.DestinationID = DL3.DestinationID  AND X3.UID = DL3.UID
GO
GRANT SELECT ON viewDriverLocation_DestinationFirstArrive TO dispatchcrude_iis_acct
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 3 Oct 2014
-- Description:	return Tickets [CSV] field for non-deleted CarrierTicketNum values
-- =============================================
CREATE FUNCTION fnTicketsCSV(@OrderID int) RETURNS varchar(1000) AS BEGIN
	DECLARE @ret varchar(max)
	SELECT @ret = 
	  STUFF(
		(
		  SELECT ',' + CarrierTicketNum
		  FROM tblOrderTicket OT
		  WHERE OT.OrderID = @OrderID 
		    AND OT.DeleteDateUTC IS NULL
		  FOR XML PATH(''),TYPE
		  ).value('.','VARCHAR(MAX)'
		), 1, 1, '')

	RETURN (@ret)
END
GO
GRANT EXECUTE ON fnTicketsCSV TO dispatchcrude_iis_acct
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 20 Mar 2013
-- Description:	return all CarrierTicketNums for an Order (separated by ",")
-- =============================================
ALTER VIEW [dbo].[viewOrder_AllTickets] AS 
SELECT O.*
	, Tickets = dbo.fnTicketsCSV(O.ID) 
FROM viewOrderLocalDates O
GO

/**********************************************************
-- Author: Kevin Alons
-- Date Created: 8 Oct 2014
-- Purpose: return the Audit table records (optionally filtered to just the specified driver and/or ordernum)
***********************************************************/
CREATE FUNCTION [dbo].[fnOrders_AllTickets_Audit](@carrierID int, @driverID int, @orderNum int, @id int) RETURNS TABLE AS RETURN
SELECT *
	, HasError = cast(CASE WHEN Errors IS NULL THEN 0 ELSE 1 END as bit)
FROM (
	SELECT O.* 
		, OriginGpsLatLon = ltrim(DLO.Lat) + ',' + ltrim(DLO.Lon)
		, OriginLatLon = ltrim(OO.LAT) + ',' + ltrim(OO.LON)
		, OriginAccuracyMeters = DLO.SourceAccuracyMeters
		, OriginDistance = DLO.DistanceToPoint
		, OriginGpsArrived = CASE WHEN DLO.DistanceToPoint IS NOT NULL AND DLO.DistanceToPoint <= OO.GeoFenceRadiusMeters THEN 1 ELSE 0 END
		, DestGpsLatLon = ltrim(DLD.Lat) + ',' + ltrim(DLD.Lon)
		, DestLatLon = ltrim(D.LAT) + ',' + ltrim(D.LON)
		, DestAccuracyMeters = DLD.SourceAccuracyMeters
		, DestDistance = DLD.DistanceToPoint
		, DestGpsArrived = CASE WHEN DLD.DistanceToPoint IS NOT NULL AND DLD.DistanceToPoint <= OO.GeoFenceRadiusMeters THEN 1 ELSE 0 END
		, Errors = 
			nullif(
				SUBSTRING(
					CASE WHEN O.OriginArriveTimeUTC IS NULL THEN ',Missing Pickup Arrival' ELSE '' END
				  + CASE WHEN O.OriginDepartTimeUTC IS NULL THEN ',Missing Pickup Departure' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND O.DestArriveTimeUTC IS NULL THEN ',Missing Delivery Arrival' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND O.DestDepartTimeUTC IS NULL THEN ',Missing Delivery Departure' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND dbo.fnConvertUOM(O.OriginGrossUnits, O.OriginUomID, 1) NOT BETWEEN 100 AND 320 THEN ',Units out of limits' ELSE '' END
				  + CASE WHEN C.RequireDispatchConfirmation = 1 AND nullif(rtrim(O.DispatchConfirmNum), '') IS NULL THEN ',Missing Dispatch Confirm #' ELSE '' END
				  + CASE WHEN O.TruckID IS NULL THEN ',Truck Missing' ELSE '' END
				  + CASE WHEN O.TrailerID IS NULL THEN ',Trailer 1 Missing' ELSE '' END
				, 2, 8000)
			, '')
		, IsEditable = cast(CASE WHEN O.StatusID = 3 THEN 1 ELSE 0 END as bit)
	FROM viewOrder_AllTickets O
	JOIN tblOrigin OO ON OO.ID = O.OriginID
	JOIN tblCustomer C ON C.ID = OO.CustomerID
	JOIN tblDestination D ON D.ID = O.DestinationID
	LEFT JOIN tblOrderInvoiceCustomer IOC ON IOC.OrderID = O.ID
	LEFT JOIN viewDriverLocation_OriginFirstArrive DLO ON DLO.OrderID = O.ID AND DLO.OriginID = O.OriginID
	LEFT JOIN viewDriverLocation_DestinationFirstArrive DLD ON DLD.OrderID = O.ID AND DLD.DestinationID = O.DestinationID
	WHERE IOC.BatchID IS NULL /* don't even show SETTLED orders on the AUDIT page ever */
	  AND (isnull(@carrierID, 0) = -1 OR O.CarrierID = @carrierID)
	  AND (isnull(@driverID, 0) = -1 OR O.DriverID = @driverID)
	  AND ((nullif(@orderNum, 0) IS NULL AND O.DeleteDateUTC IS NULL AND (O.StatusID = 3 AND DeliverPrintStatusID IN (SELECT ID FROM tblPrintStatus WHERE IsCompleted = 1))) OR O.OrderNum = @orderNum)
	  AND (nullif(@id, 0) IS NULL OR O.ID LIKE @id)
) X
GO
GRANT SELECT ON fnOrders_AllTickets_Audit TO dispatchcrude_iis_acct
GO 

EXEC _spRebuildAllObjects
GO

COMMIT
SET NOEXEC OFF

/*
select * from viewOrder_AllTickets WHERE DeleteDateUTC is not null
select * from fnOrders_AllTickets_Audit(-1, -1, null, null)
select * from tblOrder where DispatchConfirmNum is not null and StatusID = 3
select O.*, Tickets = dbo.fnTicketsCSV(O.ID) 
from viewOrderLocalDates O
WHERE O.StatusID IN (3, 4)

select producerid from viewOrder_OrderTicket_Full
select * from tblReportColumnDefinition where DataField like 'producer%'
select * from tblReportColumnDefinition where DataField like 'carrier%'
select * from tblReportColumnDefinition where DataField like 'customer%'
select * from tblReportColumnDefinition where DataField like 't_%' and OrderSingleExport=1

SELECT * FROM fnOrders_AllTickets_Audit(-1, -1, 0, NULL) ORDER BY DueDate, Origin, Destination
*/