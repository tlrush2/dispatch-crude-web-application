-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.10.18'
SELECT  @NewVersion = '3.11.1'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'Driver App Printing: Add support for Driver App Print FOOTER template (best match)'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

CREATE TABLE tblDriverAppPrintFooterTemplate
(
  ID int IDENTITY(1,1) NOT NULL CONSTRAINT PK_DriverAppPrintFooterTemplate PRIMARY KEY
, TicketTypeID int NULL CONSTRAINT FK_DriverAppPrintFooterTemplate_TicketType FOREIGN KEY REFERENCES dbo.tblTicketType (ID)
, ShipperID int NULL CONSTRAINT FK_DriverAppPrintFooterTemplate_Shipper FOREIGN KEY REFERENCES dbo.tblCustomer (ID)
, ProductGroupID int NULL CONSTRAINT FK_DriverAppPrintFooterTemplate_ProductGroup FOREIGN KEY REFERENCES tblProductGroup(ID)
, ProducerID int NULL CONSTRAINT FK_DriverAppPrintFooterTemplate_Producer FOREIGN KEY REFERENCES dbo.tblProducer (ID)
, TemplateText text NULL
, CreateDateUTC smalldatetime NOT NULL CONSTRAINT DF_DriverAppPrintFooterTemplate_CreateDateUTC  DEFAULT (getutcdate())
, CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_DriverAppPrintFooterTemplate_CreatedByUser  DEFAULT ('N/A')
, LastChangeDateUTC smalldatetime NULL
, LastChangedByUser varchar(100) NULL
) 
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblDriverAppPrintFooterTemplate TO role_iis_acct
GO

CREATE TABLE tblDriverAppPrintFooterTemplateSync
(
  OrderID int NOT NULL CONSTRAINT FK_DriverAppPrintFooterTemplateSync_Order FOREIGN KEY REFERENCES dbo.tblOrder (ID) ON DELETE CASCADE
, DriverID int NOT NULL CONSTRAINT FK_DriverAppPrintFooterTemplateSync_Driver FOREIGN KEY REFERENCES dbo.tblDriver (ID) ON DELETE CASCADE
, RecordID int NOT NULL CONSTRAINT FK_DriverAppPrintFooterTemplateSync_Record FOREIGN KEY REFERENCES dbo.tblDriverAppPrintFooterTemplate (ID) ON DELETE CASCADE
, CONSTRAINT PK_DriverAppPrintFooterTemplateSync PRIMARY KEY CLUSTERED (OrderID, DriverID, RecordID)
)
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblDriverAppPrintFooterTemplateSync TO role_iis_acct
GO

/***********************************
-- Date Created: 2016/02/15
-- Author: Kevin Alons
-- Purpose: retrieve and return the matching DriverAppPrintFooterTemplate record
-- Changes:
***********************************/
CREATE FUNCTION fnBestMatchDriverAppPrintFooterTemplate
(
  @TicketTypeID int
, @ShipperID int
, @ProductGroupID int
, @ProducerID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID
			, Ranking = dbo.fnRateRanking(@TicketTypeID, R.TicketTypeID, 8, 1)
				  + dbo.fnRateRanking(@ShipperID, R.ShipperID, 4, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 2, 0)
				  + dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM  dbo.tblDriverAppPrintFooterTemplate R
		WHERE coalesce(nullif(@TicketTypeID, 0), R.TicketTypeID, 0) = coalesce(TicketTypeID, nullif(@TicketTypeID, 0), 0)
		  AND coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(ProducerID, nullif(@ProducerID, 0), 0)
	)
	SELECT R.ID, TicketTypeID, ShipperID, ProductGroupID, ProducerID
		, TemplateText
		, BestMatch, Ranking
		, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM tblDriverAppPrintFooterTemplate R
	JOIN (
		SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE Ranking % 1 = 0.01 * 4  -- ensure some type of match occurred on all criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)
GO
GRANT SELECT ON fnBestMatchDriverAppPrintFooterTemplate TO role_iis_acct
GO

/***********************************
-- Date Created: 2015/07/02
-- Author: Kevin Alons
-- Purpose: retrieve and return the matching DriverAppPrintFooterTemplate record + friendly translated data
-- Changes:
***********************************/
CREATE FUNCTION fnBestMatchDriverAppPrintFooterTemplateDisplay
(
  @TicketTypeID int
, @ShipperID int
, @ProductGroupID int
, @ProducerID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.TicketTypeID, R.ShipperID, R.ProductGroupID, R.ProducerID
		, R.TemplateText
		, TicketType = TT.Name
		, Shipper = SH.Name
		, ProductGroup = PG.Name
		, Producer = P.Name
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
		, Ranking
	FROM dbo.fnBestMatchDriverAppPrintFooterTemplate(@TicketTypeID, @ShipperID, @ProductGroupID, @ProducerID, 0) R
	LEFT JOIN tblTicketType TT ON TT.ID = R.TicketTypeID
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblProducer P ON P.ID = R.ProducerID
GO
GRANT SELECT ON fnBestMatchDriverAppPrintFooterTemplateDisplay TO role_iis_acct
GO

/***********************************
-- Date Created: 2016/02/15
-- Author: Kevin Alons
-- Purpose: retrieve and return the Best Match DriverAppPrintFooterTemplate info for the specified order
-- Changes:
***********************************/
CREATE FUNCTION fnOrderBestMatchDriverAppPrintFooterTemplate(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, R.TemplateText, R.LastChangeDateUTC
	FROM dbo.tblOrder O
	JOIN tblProduct PRO ON PRO.ID = O.ProductID
	CROSS APPLY dbo.fnBestMatchDriverAppPrintFooterTemplate(O.TicketTypeID, O.CustomerID, PRO.ProductGroupID, O.ProducerID, 1) R
	WHERE O.ID = @ID 
GO
GRANT SELECT ON fnOrderBestMatchDriverAppPrintFooterTemplate TO role_iis_acct
GO

/*******************************************
Date Created: 2013/04/25
Author: Kevin Alons
Purpose: return readonly Order data for Driver App sync
Changes:
- 3.8.1		- 2015/07/02 - KDA	- add new split DriverApp printing capability (HeaderImageLeft|Top|Width|Height & Pickup|Ticket|Deliver TemplateText)
- 3.9.19	- 2015/07/23 - KDA	- remove TicketTemplate column (it will be provided as a separate TicketTemplates sync table)
- 3.9.20	- 2015/10/22 - KDA	- return OriginDriver | DestDriver fields (from OrderTransfer logic)
- 3.10.2.1	- 2015/01/28 - JAE	- Added clause to always send dispatched orders
- 3.10.2.3	- 2015/01/29 - JAE	- Removed clause to always send dispatched orders
- 3.10.5	- 2015/01/29 - KDA	- remove redundant Order.ID filtering outside of CTE (performance optimization)
								- use fnSyncLCDOffset() instead of hard-coded 5 second offset
								- don't send the HeaderBlob or any TemplateText records for Deleted records
- 3.10.5.1	- 2016/02/02 - BB	- Hotfix for reversed logic on print templates that was accidentally published.
- 3.10.9	- 2016/02/18 - JAE	- Add driving directions for destination
- 3.11.1	- 2016/02/15 - KDA	- Add support for tblDriverAppPrintFooterTemplate
*******************************************/
ALTER FUNCTION fnOrderReadOnly_DriverApp(@DriverID INT, @LastChangeDateUTC DATETIME) RETURNS TABLE AS
RETURN 
      WITH cteBase AS
      (
            SELECT O.ID
                  , O.OrderNum
                  , O.StatusID
                  , O.TicketTypeID
                  , PriorityNum = CAST(P.PriorityNum AS INT) 
                  , Product = PRO.Name
                  , O.DueDate
                  , Origin = OO.Name
                  , OriginFull = OO.FullName
                  , OO.OriginType
                  , O.OriginUomID
                  , OriginStation = OO.Station 
                  , OriginLeaseNum = OO.LeaseNum 
                  , OriginCounty = OO.County 
                  , OriginLegalDescription = OO.LegalDescription 
                  , OriginNDIC = OO.NDICFileNum
                  , OriginNDM = OO.NDM
                  , OriginCA = OO.CA
                  , OriginState = OO.State
                  , OriginAPI = OO.WellAPI 
                  , OriginLat = OO.LAT 
                  , OriginLon = OO.LON 
                  , OriginGeoFenceRadiusMeters = OO.GeoFenceRadiusMeters 
                  , Destination = D.Name
                  , DestinationFull = D.FullName
                  , DestType = D.DestinationType 
                  , O.DestUomID
                  , DestLat = D.LAT 
                  , DestLon = D.LON 
                  , DestGeoFenceRadiusMeters = D.GeoFenceRadiusMeters 
                  , DestinationStation = D.Station 
                  , O.CreateDateUTC
                  , O.CreatedByUser
                  , LastChangeDateUTC = OAC.ReadOnlyChangeDateUTC -- only show changed when the ReadOnly portion of the table changed
                  , O.LastChangedByUser
                  , DeleteDateUTC = ISNULL(ODAVD.VirtualDeleteDateUTC, O.DeleteDateUTC) 
                  , DeletedByUser = ISNULL(ODAVD.VirtualDeletedByUser, O.DeletedByUser) 
                  , O.OriginID
                  , O.DestinationID
                  , PriorityID = CAST(O.PriorityID AS INT) 
                  , Operator = OO.Operator
                  , O.OperatorID
                  , Pumper = OO.Pumper
                  , O.PumperID
                  , Producer = OO.Producer
                  , O.ProducerID
                  , Customer = C.Name
                  , O.CustomerID
                  , Carrier = CA.Name
                  , O.CarrierID
                  , O.ProductID
                  , TicketType = OO.TicketType
                  , EmergencyInfo = ISNULL(C.EmergencyInfo, 'For an emergency (spill, leak, fire, or accident) contact CHEMTREC @ 800.424.9300 (toll free)') 
                  , DestTicketTypeID = D.TicketTypeID
                  , DestTicketType = D.TicketType
                  , O.OriginTankNum
                  , O.OriginTankID
                  , O.DispatchNotes
                  , O.DispatchConfirmNum
                  , RouteActualMiles = ISNULL(R.ActualMiles, 0)
                  , CarrierAuthority = CA.Authority 
                  , OriginTimeZone = OO.TimeZone
                  , DestTimeZone = D.TimeZone
                  , OCTM.OriginThresholdMinutes
                  , OCTM.DestThresholdMinutes
                  , ShipperHelpDeskPhone = C.HelpDeskPhone
                  , OriginDrivingDirections = OO.DrivingDirections
				  , DestDrivingDirections = D.DrivingDirections
                  , LCD.LCD
                  , CustomerLastChangeDateUTC = C.LastChangeDateUTC 
                  , CarrierLastChangeDateUTC = CA.LastChangeDateUTC 
                  , OriginLastChangeDateUTC = OO.LastChangeDateUTC 
                  , DestLastChangeDateUTC = D.LastChangeDateUTC
                  , RouteLastChangeDateUTC = R.LastChangeDateUTC
                  , DriverID = @DriverID
                  , OriginDriver = OD.FullName
                  , OriginTruckID = ISNULL(OTR.OriginTruckID, O.TruckID)
                  , DestDriver = DD.FullName
            FROM dbo.tblOrder O
            JOIN dbo.tblPriority P ON P.ID = O.PriorityID
            JOIN dbo.viewOrigin OO ON OO.ID = O.OriginID
            JOIN dbo.viewDestination D ON D.ID = O.DestinationID
            JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
            JOIN dbo.tblRoute R ON R.ID = O.RouteID
            JOIN dbo.tblCarrier CA ON CA.ID = O.CarrierID
            JOIN dbo.tblProduct PRO ON PRO.ID = O.ProductID
			LEFT JOIN tblOrderAppChanges OAC ON OAC.OrderID = O.ID
            LEFT JOIN dbo.tblOrderTransfer OTR ON OTR.OrderID = O.ID
            LEFT JOIN dbo.viewDriver OD ON OD.ID = ISNULL(OTR.OriginDriverID, O.DriverID)
            LEFT JOIN dbo.viewDriver DD ON DD.ID = O.DriverID
            OUTER APPLY dbo.fnOrderBestMatchDriverAppPrintHeaderImage(O.ID) DAHI
            LEFT JOIN tblOrderDriverAppVirtualDelete ODAVD ON ODAVD.OrderID = O.ID AND ODAVD.DriverID = @DriverID
            CROSS JOIN dbo.fnSyncLCDOffset(@LastChangeDateUTC) LCD
            OUTER APPLY dbo.fnOrderCombinedThresholdMinutes(O.ID) OCTM
			WHERE O.ID IN (
				SELECT ID FROM tblOrder WHERE DriverID = @driverID AND StatusID IN (2,7,8,3) 
				UNION 
				SELECT OrderID FROM tblOrderDriverAppVirtualDelete WHERE DriverID = @driverID
			)
      )

      SELECT O.*
            , HeaderImageID = DAHI.ID
            , PrintHeaderBlob = CASE WHEN O.DeleteDateUTC IS NOT NULL THEN NULL ELSE DAHI.ImageBlob END
            , HeaderImageLeft = DAHI.ImageLeft
            , HeaderImageTop = DAHI.ImageTop
            , HeaderImageWidth = DAHI.ImageWidth
            , HeaderImageHeight = DAHI.ImageHeight
            , PickupTemplateID = DAPT.ID
            , PickupTemplateText = CASE WHEN O.DeleteDateUTC IS NOT NULL THEN NULL ELSE DAPT.TemplateText END
            , DeliverTemplateID = DADT.ID
            , DeliverTemplateText = CASE WHEN O.DeleteDateUTC IS NOT NULL THEN NULL ELSE DADT.TemplateText END
            , FooterTemplateID = DAFT.ID
            , FooterTemplateText = CASE WHEN O.DeleteDateUTC IS NOT NULL THEN NULL ELSE DAFT.TemplateText END
      FROM cteBase O
      OUTER APPLY dbo.fnOrderBestMatchDriverAppPrintHeaderImage(O.ID) DAHI
      LEFT JOIN tblDriverAppPrintHeaderImageSync DAHIS ON DAHIS.OrderID = O.ID AND DAHIS.DriverID = O.DriverID AND DAHIS.RecordID <> DAHI.ID
      OUTER APPLY dbo.fnOrderBestMatchDriverAppPrintPickupTemplate(O.ID) DAPT
      LEFT JOIN tblDriverAppPrintPickupTemplateSync DAPTS ON DAPTS.OrderID = O.ID AND DAPTS.DriverID = O.DriverID AND DAPTS.RecordID <> DAPT.ID
      OUTER APPLY dbo.fnOrderBestMatchDriverAppPrintDeliverTemplate(O.ID) DADT
      LEFT JOIN tblDriverAppPrintDeliverTemplateSync DADTS ON DADTS.OrderID = O.ID AND DADTS.DriverID = O.DriverID AND DADTS.RecordID <> DADT.ID
      OUTER APPLY dbo.fnOrderBestMatchDriverAppPrintFooterTemplate(O.ID) DAFT
      LEFT JOIN tblDriverAppPrintFooterTemplateSync DAFTS ON DADTS.OrderID = O.ID AND DADTS.DriverID = O.DriverID AND DADTS.RecordID <> DADT.ID
      WHERE @LastChangeDateUTC IS NULL 
        OR CreateDateUTC >= LCD
        OR O.LastChangeDateUTC >= LCD
        OR O.DeleteDateUTC >= LCD
        OR CustomerLastChangeDateUTC >= LCD
        OR CarrierLastChangeDateUTC >= LCD
        OR OriginLastChangeDateUTC >= LCD
        OR DestLastChangeDateUTC >= LCD
        OR RouteLastChangeDateUTC >= LCD
        -- if any print related record was changed or a different template/image is now valid
        OR DAHI.LastChangeDateUTC >= LCD
        OR DAHIS.RecordID IS NOT NULL
        OR DAPT.LastChangeDateUTC >= LCD
        OR DAPTS.RecordID IS NOT NULL
        OR DADT.LastChangeDateUTC >= LCD
        OR DADTS.RecordID IS NOT NULL
        OR DAFT.LastChangeDateUTC >= LCD
        OR DAFTS.RecordID IS NOT NULL
GO

/*******************************************
 Date Created: 2013/04/25
 Author: Kevin Alons
 Purpose: return readonly Order data for Driver App sync (and log the Print XXX records so we can faithfully re-sync them when a change occurrs)
 Changes:
  - 3.8.1	- 2015/07/02	- KDA	- ADDED
  - 3.9.19	- 2015/09/23	- KDA	- remove references to tblDriverAppPrintTicketTemplateSync
  - 3.10.5	- 2016/01/29	- KDA	- add logic to purge all ImageSync tables on a FULL sync operation (before data retrieval)
  - 3.11.1	- 2016/02/15	- KDA	- support for Footer ZPL template
*******************************************/
ALTER PROCEDURE spOrderReadOnly_DriverApp(@DriverID int, @LastChangeDateUTC datetime = NULL) AS
BEGIN
	BEGIN TRY	
		BEGIN TRAN
		-- purge all of these records on a FULL sync
		IF (@LastChangeDateUTC IS NULL)
		BEGIN
			DELETE FROM tblDriverAppPrintHeaderImageSync WHERE DriverID = @DriverID 
			DELETE FROM tblDriverAppPrintPickupTemplateSync WHERE DriverID = @DriverID 
			DELETE FROM tblDriverAppPrintDeliverTemplateSync WHERE DriverID = @DriverID 
			DELETE FROM tblDriverAppPrintFooterTemplateSync WHERE DriverID = @DriverID 
		END

		-- retrieve the data eligible records to be returned
		SELECT * 
		INTO #d 
		FROM dbo.fnOrderReadOnly_DriverApp(@DriverID, @LastChangeDateUTC)
		
		-- forget the last sync records for this driver (if we didn't do it earlier before retrieving records)
		IF (@LastChangeDateUTC IS NOT NULL)
		BEGIN
			DELETE FROM tblDriverAppPrintHeaderImageSync
			WHERE DriverID = @DriverID 
			  AND OrderID IN (SELECT DISTINCT ID FROM #d)
			DELETE FROM tblDriverAppPrintPickupTemplateSync
			WHERE DriverID = @DriverID 
			  AND OrderID IN (SELECT DISTINCT ID FROM #d)
			DELETE FROM tblDriverAppPrintDeliverTemplateSync
			WHERE DriverID = @DriverID 
			  AND OrderID IN (SELECT DISTINCT ID FROM #d)
			DELETE FROM tblDriverAppPrintFooterTemplateSync
			WHERE DriverID = @DriverID 
			  AND OrderID IN (SELECT DISTINCT ID FROM #d)
		END

		-- re-cache the last sync records for the sync orders
		INSERT INTO tblDriverAppPrintHeaderImageSync (OrderID, DriverID, RecordID)
			SELECT DISTINCT ID, @DriverID, HeaderImageID FROM #d WHERE HeaderImageID IS NOT NULL
		INSERT INTO tblDriverAppPrintPickupTemplateSync (OrderID, DriverID, RecordID)
			SELECT DISTINCT ID, @DriverID, PickupTemplateID FROM #d WHERE PickupTemplateID IS NOT NULL
		INSERT INTO tblDriverAppPrintDeliverTemplateSync (OrderID, DriverID, RecordID)
			SELECT DISTINCT ID, @DriverID, DeliverTemplateID FROM #d WHERE DeliverTemplateID IS NOT NULL
		INSERT INTO tblDriverAppPrintFooterTemplateSync (OrderID, DriverID, RecordID)
			SELECT DISTINCT ID, @DriverID, FooterTemplateID FROM #d WHERE FooterTemplateID IS NOT NULL
		
		COMMIT TRAN
		SELECT * FROM #d
	END TRY
	BEGIN CATCH
		DECLARE @msg varchar(255), @severity int
		SELECT @msg = left(ERROR_MESSAGE(), 255), @severity = ERROR_SEVERITY()
		IF @@TRANCOUNT > 0
			ROLLBACK TRAN 
		RAISERROR(@msg, @severity, 1)
	END CATCH
END
GO

/************************************************
 Author:		Kevin Alons
 Create date: 19 Dec 2012
 Description:	trigger to 
			1) validate any changes, and fail the update if invalid changes are submitted
			2) add a unique, incrementing OrderNum to each new Order (manual Identity column)
			3) recompute wait times (origin|destination based on times provided)
			4) generate route table entry for any newly used origin-destination combination
			5) keep Order consistent with Origin.Producer|Operator|UOM & Dest.UOM then the Origin|Dest change
			6) roll the PrintStatus back to NOTFINALIZED (0) when status is rolled back
			7) when DriverID changes, mark those orders AS DELETED and clone the order with the new DriverID and associated records
			8) update any ticket quantities for open orders when the UOM is changed for the Origin
			9) update the Driver.Truck\Trailer\Trailer2 defaults & related DISPATCHED orders when driver updates them on an order
-REMOVED	10) update the Pickup/DeliverLastChangeDateUTC when Origin/Destination is changed (respectively)
			11) (re) apply Settlement Amounts to orders not yet fully settled when status is changed to DELIVERED
			12) if DBAudit is turned on, save an audit record for this Order change
 Changes: 
  - 3.7.4 05/08/15 GSM Added Rack/Bay field to DBAudit logic
  - 3.7.7 - 15 May 2015 - KDA - REMOVE #10 above, instead update the LastChangeDateUTC whenever an Origin or Destination is changed
  - 3.7.11 - 2015/05/18 - KDA - generally use GETUTCDATE for all LastChangeDateUTC revisions (instead of related Order.LastChangeDateUTC, etc)
  - 3.7.12 - 5/19/2015  - KDA - update any existing OrderTicket records when the Order is DISPATCHED to a driver (so the Driver App will ALWAYS receive the existing TICKETS from the GAUGER)
  - 3.7.23 - 06/05/2015 - GSM - DCDRV-154: Ticket Type following Origin Change
  - 3.7.23 - 06/05/2015 - GSM - DCWEB-530 - ensure ASSIGNED orders with a driver defined are updated to DISPATCHED
  - 3.8.1  - 2015/07/04 - KDA - purge any Driver|Gauger App ZPL related SYNC change records when order is AUDITED
  - 3.9.0  - 2015/08/20 - KDA - add invocation of spAutoAuditOrder for DELIVERED orders (that qualify) when the Auto-Audit global setting value is TRUE
							  - add invocation of spAutoApproveOrder for AUDITED orders (that qualify)
  - 3.9.2  - 2015/08/25 - KDA - appropriately use new tblOrder.OrderDate date column
  - 3.9.4  - 2015/08/30 - KDA - performanc optimization to prevent Orderdate computation if no timestamp fields are yet populated
  - 3.9.5  - 2015/08/31 - KDA - more extensive performance optimizations to minimize performance ramifications or computing OrderDate dynamically from OrderRule
  - 3.9.13 - 2015/09/01 - KDA - add (but commented out) some code to validate Arrive|Depart time being present when required
							  - allow orders to be rolled back from AUDITED to DELIVERED when in AUTO-AUDIT mode
							  - always AUTO UNAPPROVE orders when the status is reverted from AUDITED to DELIVERED 
  - 3.9.19 - 2015/09/23 - KDA - remove obsolete reference to tblDriverAppPrintTicketTemplate
  - 3.9.20 - 2015/09/23 - KDA - add support for tblOrderTransfer (do not accomplish #7 above - cloning/delete orders that are driver re-assigned)
  - 3.9.29.5 - 2015/12/03 - JAE - added DestTrailerWaterCapacity and DestRailCarNum to dbaudit trigger
  - 3.9.38 - 2015/12/21 - BB - Add WeightNetUnits (DCWEB-972)
  - 3.9.38 - 2016/01/11 - JAE - Recompute net for destination weights
  - 3.10.5 - 2016/01/29 - KDA - fix to DriverApp/Gauger App purge sync records for ineligible orders [was order.StatusID IN (4), now NOT IN (valid statuses)]
  - 3.10.5.2 - 2016/02/09 - KDA - preserve OrderDate on OrderTransfer (driver transfer)
								- ensure Accept | Pickup | Deliver LastChangeDateUTC is updated on relevant update
			- 2016/02/11 - JAE  - Added DispatchNotes since mobile screens display that data and any changes should trigger the view to refresh
  - 3.10.10.1 - 2016/02/20	- KDA	- fix to Driver Transfer logic to prevent losing carrier if setting DriverID to NULL (not immediately assigning to another Driver)
  - 3.10.10.3	- 2016/02/27	- KDA	- only update Accept|Pickup|Deliver LastChangeDate values when update NOT done by DriverApp
  - 3.11.1	  - 2016/02/21 - KDA	- add support for tblDriverAppPrintFooterTemplate table
************************************************/
ALTER TRIGGER trigOrder_IU ON tblOrder AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @changesFound bit
	
		-- Note: the TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU') ) = 1 statement prevents recursion
	IF (TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU')) = 1 AND TRIGGER_NESTLEVEL( OBJECT_ID('trigOrderTicket_IU')) = 0) BEGIN

		/**********  START OF VALIDATION SECTION ************************/

		IF (UPDATE(OrderNum) 
			OR UPDATE(PriorityID) 
			OR UPDATE(DueDate) 
			OR UPDATE(RouteID) 
			OR UPDATE(OriginID) 
			OR UPDATE(OriginArriveTimeUTC) 
			OR UPDATE(OriginDepartTimeUTC) 
			OR UPDATE(OriginMinutes) 
			OR UPDATE(OriginWaitNotes) 
			OR UPDATE(OriginBOLNum) 
			OR UPDATE(OriginGrossUnits) 
			OR UPDATE(OriginNetUnits) 
			--OR UPDATE(OriginWeightGrossUnits) -- 3.9.38  Per Maverick 12/21/15 we do not need gross on the order level.
			OR UPDATE(OriginWeightNetUnits) -- 3.9.38
			OR UPDATE(DestinationID) 
			OR UPDATE(DestArriveTimeUTC) 
			OR UPDATE(DestDepartTimeUTC) 
			OR UPDATE(DestMinutes) 
			OR UPDATE(DestWaitNotes) 
			OR UPDATE(DestBOLNum) 
			OR UPDATE(DestGrossUnits) 
			OR UPDATE(DestNetUnits) 
			OR UPDATE(CustomerID) 
			OR UPDATE(CarrierID) 
			OR UPDATE(DriverID) 
			OR UPDATE(TruckID)
			OR UPDATE(TrailerID) 
			OR UPDATE(Trailer2ID) 
			OR UPDATE(OperatorID) 
			OR UPDATE(PumperID) 
			OR UPDATE(TicketTypeID) 
			OR UPDATE(Rejected) 
			OR UPDATE(RejectNotes) 
			OR UPDATE(ChainUp) 
			-- allow this to be changed even in audit status
			--OR UPDATE(OriginTruckMileage) 
			OR UPDATE(OriginTankNum) 
			-- allow this to be changed even in audit status
			--OR UPDATE(DestTruckMileage) 
			OR UPDATE(CarrierTicketNum) 
			OR UPDATE(AuditNotes) 
			OR UPDATE(CreateDateUTC) 
			-- it is permissible to change the ActualMiles value on Audited orders up to the point the Order has been Shipper-Settled
			OR (UPDATE(ActualMiles) AND EXISTS (SELECT * FROM deleted i JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = i.ID WHERE OSS.BatchID IS NOT NULL))
			OR UPDATE(ProducerID) 
			OR UPDATE(CreatedByUser) 
			OR UPDATE(LastChangeDateUTC) 
			OR UPDATE(LastChangedByUser) 
			OR UPDATE(DeleteDateUTC) 
			OR UPDATE(DeletedByUser) 
			OR UPDATE(DestProductBSW)
			OR UPDATE(DestProductGravity) 
			OR UPDATE(DestProductTemp) 
			OR UPDATE(ProductID) 
			OR UPDATE(AcceptLastChangeDateUTC) 
			OR UPDATE(PickupLastChangeDateUTC) 
			OR UPDATE(DeliverLastChangeDateUTC) 
			OR UPDATE(OriginUomID) 
			OR UPDATE(DestUomID) 
			OR UPDATE(PickupPrintStatusID) 
			OR UPDATE(DeliverPrintStatusID)
			OR UPDATE(PickupPrintDateUTC) 
			OR UPDATE(DeliverPrintDateUTC) 
			OR UPDATE(OriginTankID) 
			OR UPDATE(OriginGrossStdUnits) 
			OR UPDATE(DispatchConfirmNum) 
			OR UPDATE(DispatchNotes)
			OR UPDATE(PickupDriverNotes)
			OR UPDATE(DeliverDriverNotes)
			OR UPDATE(OriginWaitReasonID)
			OR UPDATE(DestWaitReasonID)
			OR UPDATE(RejectReasonID) 
			OR UPDATE(DestOpenMeterUnits) 
			OR UPDATE(DestCloseMeterUnits)
			OR UPDATE(DestRackBay)
			OR UPDATE(OrderDate)
			OR UPDATE(DestWeightGrossUnits) 
			OR UPDATE(DestWeightTareUnits)
		)
			--OR UPDATE(ReassignKey)) we don't do any work if this value changes so just ignore it
		BEGIN
			-- only allow the StatusID value to be changed on an audited order
			IF EXISTS (SELECT * FROM deleted WHERE StatusID = 4)
			BEGIN
				-- if the "Allow Audited Order Update (temp only)" setting is TRUE then only warn when this occurs
				IF (dbo.fnToBool(dbo.fnSettingValue(35)) = 1)
					PRINT 'AUDITED orders is being modified - please investigate why!'
				ELSE BEGIN
					-- otherwise (normal behavior) - prevent AUDITED orders from being modified (except to UN-AUDIT them)
					RAISERROR('AUDITED orders cannot be modified!', 16, 1)
					IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
					RETURN
				END
			END
		END
		ELSE IF NOT UPDATE(StatusID) -- NO CHANGES DETECTED SO JUST QUIT
			RETURN

		PRINT 'trigOrder_IU FIRED'
					
		-- ensure the Origin and Destinations are both specified unless the Status is:
		--   (Generated, Assigned, Dispatched or Declined)
		IF  EXISTS(SELECT * FROM inserted O WHERE (O.OriginID IS NULL OR O.DestinationID IS NULL) AND O.StatusID NOT IN (-10,1,2,9))
		BEGIN
			RAISERROR('Invalid Order Status when the Origin and/or Or Destination is not specified', 16, 1)
			IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
			RETURN
		END
		/* this is commented out until we get the Android issues closer to resolved 
		IF EXISTS (SELECT * FROM inserted WHERE StatusID IN (8, 3, 4) AND OriginArriveTimeUTC IS NULL OR OriginDepartTimeUTC IS NULL)
		BEGIN
			RAISERROR('OriginArriveTimeUTC and/or OriginDepartTimeUTC missing when required', 16, 1)
			IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
			RETURN
		END
		IF EXISTS (SELECT * FROM inserted WHERE StatusID IN (3, 4) AND DestArriveTimeUTC IS NULL OR DestDepartTimeUTC IS NULL)
		BEGIN
			RAISERROR('DestArriveTimeUTC and/or DestDepartTimeUTC missing when required', 16, 1)
			IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
			RETURN
		END
		******************************************************************************/
		
		/**********  END OF VALIDATION SECTION ************************/

		WHILE (SELECT count(*) FROM inserted i JOIN tblOrder O ON O.ID = i.ID WHERE O.OrderNum IS NULL) > 0
		BEGIN
			UPDATE tblOrder 
			  SET OrderNum = isnull((SELECT max(OrderNum) FROM tblOrder), 100000) + 1
				, CreateDateUTC = getutcdate()
			WHERE ID = (SELECT min(O.ID) FROM tblOrder O JOIN inserted i ON i.ID = O.ID WHERE O.OrderNum IS NULL)
		END
		
		-- re-compute the OriginMinutes (in case the website failed to compute it properly)
		IF UPDATE(OriginArriveTimeUTC) OR UPDATE(OriginDepartTimeUTC)
		BEGIN
			UPDATE tblOrder SET OriginMinutes = datediff(minute, i.OriginArriveTimeUTC, i.OriginDepartTimeUTC)
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
		END
		-- re-compute the DestMinutes (in case the website failed to compute it properly)
		IF UPDATE(DestArriveTimeUTC) OR UPDATE(DestDepartTimeUTC)
		BEGIN
			UPDATE tblOrder SET DestMinutes = datediff(minute, i.DestArriveTimeUTC, i.DestDepartTimeUTC)
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
		END
		
		/* ensure any changes to the order always update the Order.OrderDate field */
		IF (UPDATE(CustomerID) 
			OR UPDATE(CarrierID) 
			OR UPDATE(ProductID) 
			OR UPDATE(OriginID) 
			OR UPDATE(DestinationID) 
			OR UPDATE(ProducerID) 
			OR UPDATE(OriginArriveTimeUTC) OR UPDATE(OriginDepartTimeUTC)
			OR UPDATE(DestArriveTimeUTC) OR UPDATE(DestDepartTimeUTC))
		BEGIN
			UPDATE tblOrder 
			  SET OrderDate = dbo.fnOrderDate(O.ID)
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
			JOIN deleted d ON d.ID = i.ID
			LEFT JOIN tblOrderSettlementCarrier OSC ON OSC.OrderID = i.ID
			LEFT JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = i.ID
			WHERE i.StatusID <> 4 
			  -- the order has at least 1 valid timestamp
			  AND (i.OriginArriveTimeUTC IS NOT NULL OR i.OriginDepartTimeUTC IS NOT NULL OR i.DestArriveTimeUTC IS NOT NULL OR i.DestDepartTimeUTC IS NOT NULL)
			  -- the order is not yet settled
			  AND OSC.BatchID IS NULL
			  AND OSS.BatchID IS NULL
		END
		
		-- ensure missing Routes are created and assigned for the specified Origin/Destination combination
		IF UPDATE(OriginID) OR UPDATE(DestinationID)
		BEGIN
			-- create any missing Route records
			INSERT INTO tblRoute (OriginID, DestinationID, CreateDateUTC, CreatedByUser)
				SELECT i.OriginID, i.DestinationID, GETUTCDATE(), ISNULL(i.LastChangedByUser, i.CreatedByUser)
				FROM inserted i
				LEFT JOIN tblRoute R ON R.OriginID = i.OriginID AND R.DestinationID = i.DestinationID
				WHERE i.OriginID IS NOT NULL AND i.DestinationID IS NOT NULL AND R.ID IS NULL
			
			-- ensure the Order records refer to the correct Route (ID)
			UPDATE tblOrder SET RouteID = R.ID
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
			LEFT JOIN tblRoute R ON R.OriginID = O.OriginID AND R.DestinationID = O.DestinationID
			WHERE O.RouteID IS NULL OR O.RouteID <> R.ID
			
			-- update the ActualMiles from the related Route
			UPDATE tblOrder SET ActualMiles = R.ActualMiles
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
			JOIN tblRoute R ON R.ID = O.RouteID
			WHERE O.ActualMiles <> R.ActualMiles
		END
		
		IF (UPDATE(OriginID))
		BEGIN
			-- update Order.ProducerID/OperatorID/PumperID to match what is assigned to the new Origin
			UPDATE tblOrder 
				SET TicketTypeID = OO.TicketTypeID
					, ProducerID = OO.ProducerID
					, OperatorID = OO.OperatorID
					, PumperID = OO.PumperID
					, LastChangeDateUTC = GETUTCDATE() 
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			WHERE d.OriginID <> O.OriginID

			-- update Order.OriginUomID to match what is assigned to the new Origin (quantities are updated below)
			UPDATE tblOrder SET OriginUomID = OO.UomID
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			WHERE d.OriginID <> O.OriginID AND d.OriginUomID <> OO.UomID
		END
		
		-- keep the DestUomID in sync with the Destination (units are updated below)
		IF (UPDATE(DestinationID))
		BEGIN
			-- update Order.DestUomID to match what is assigned to the new Destination
			UPDATE tblOrder 
			  SET DestUomID = DD.UomID
				, LastChangeDateUTC = GETUTCDATE()
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID
			JOIN tblDestination DD ON DD.ID = O.DestinationID
			WHERE d.DestinationID <> O.DestinationID AND d.DestUomID <> DD.UomID
		END
		
		-- ensure that any set PrintStatusID values are reset to 0 when backing up the general StatusID value
		IF (UPDATE(StatusID))
		BEGIN
			UPDATE tblOrder 
			  SET DeliverPrintStatusID = 0 
				, LastChangeDateUTC = GETUTCDATE()
			  FROM tblOrder O
			  JOIN deleted d ON d.ID = O.ID
			WHERE O.StatusID <> d.StatusID AND O.StatusID IN (-10, 1, 2, 7, 8) -- Generated, Assigned, Dispatched, Accepted, Picked-Up

			UPDATE tblOrder 
			  SET PickupPrintStatusID = 0 
				, LastChangeDateUTC = GETUTCDATE()
			  FROM tblOrder O
			  JOIN deleted d ON d.ID = O.ID
			WHERE O.StatusID <> d.StatusID AND O.StatusID IN (-10, 1, 2, 7) -- Generated, Assigned, Dispatched, Accepted
			
			/* ensure that any orders that are DISPATCHED - any existing orders are TOUCHED so they are syncable to the Driver App */
			UPDATE tblOrderTicket
				SET LastChangeDateUTC = GETUTCDATE()
			WHERE OrderID IN (
				SELECT i.ID 
				FROM inserted i
				JOIN deleted d ON d.ID = i.ID
				WHERE i.StatusID <> d.StatusID AND i.StatusID IN (2) -- DISPATCHED
			)
		END

		-- just updating the tickets here, trigOrderTicket_IU will in turn update this table
		-- ensure any existing OrderTickets are recomputed with the new OriginUOM
		UPDATE tblOrderTicket
		  SET GrossUnits = dbo.fnConvertUOM(GrossUnits, d.OriginUomID, O.OriginUomID)
			, NetUnits = dbo.fnConvertUOM(NetUnits, d.OriginUomID, O.OriginUomID)
			-- 3.9.38 - added to also force the WeightNetUnits be recomputed
			, WeightGrossUnits = dbo.fnConvertUOM(WeightGrossUnits, d.OriginUomID, O.OriginUomID)
			, LastChangeDateUTC = GETUTCDATE()
		FROM tblOrderTicket OT
		JOIN tblOrder O ON O.ID = OT.OrderID
		JOIN deleted d ON d.ID = O.ID
		WHERE O.OriginUomID <> d.OriginUomID
		
		-- recalculate the Destination Unit values if only if the DestUomID changed but the values didn't
		UPDATE tblOrder
		  SET DestGrossUnits = dbo.fnConvertUOM(O.DestGrossUnits, d.DestUomID, O.DestUomID)
			, DestNetUnits = dbo.fnConvertUOM(O.DestNetUnits, d.DestUomID, O.DestUomID)
			, LastChangeDateUTC = GETUTCDATE()
		FROM tblOrder O
		JOIN deleted d ON d.ID = O.ID
		WHERE d.DestUomID <> O.DestUomID 
		  AND d.DestGrossUnits = O.DestGrossUnits
		  AND d.DestNetUnits = O.DestNetUnits

		/* DCWEB-530 - ensure any ASSIGNED orders with a DRIVER assigned is set to DISPATCHED */
		UPDATE tblOrder SET StatusID = 2 /*DISPATCHED*/
		FROM tblOrder O
		JOIN inserted i ON I.ID = O.ID
		WHERE i.StatusID = 1 /*ASSIGNED*/ AND i.CarrierID IS NOT NULL AND i.DriverID IS NOT NULL AND i.DeleteDateUTC IS NULL

		/**************************************************************************************************************/
		/* 3.10.5.2 - ensure the Accept|Pickup|Deliver LastChangeDateUTC values are updated when any relevant data field changes */
		UPDATE tblOrder 
		  SET AcceptLastChangeDateUTC = LastChangeDateUTC
		WHERE ID IN (
			SELECT ID FROM (
				SELECT ID, TruckID, TrailerID, Trailer2ID, DispatchNotes FROM inserted 
				EXCEPT 
				SELECT ID, TruckID, TrailerID, Trailer2ID, DispatchNotes FROM deleted
			) X
		-- only include records that didn't have the AcceptLastChangeDateUTC explicitly updated (like from the DriverApp) - 3.10.10.3
		) AND ID IN (
			SELECT ID FROM (
				SELECT ID, AcceptLastChangeDateUTC FROM inserted INTERSECT SELECT ID, AcceptLastChangeDateUTC FROM deleted
			) X
		)
		UPDATE tblOrder 
		  SET PickupLastChangeDateUTC = LastChangeDateUTC
		WHERE ID IN (
			SELECT ID FROM (
				SELECT ID, OriginArriveTimeUTC, OriginDepartTimeUTC, OriginMinutes, OriginWaitReasonID, OriginWaitNotes
                    , CarrierTicketNum, OriginBOLNum, Rejected, RejectReasonID, RejectNotes, Chainup, OriginTruckMileage
                    , PickupPrintStatusID, PickupPrintDateUTC, OriginGrossUnits, OriginGrossStdUnits, OriginNetUnits, OriginWeightNetUnits
                    , PickupDriverNotes, DispatchNotes
                    , TicketTypeID 
				FROM inserted 
				EXCEPT 
				SELECT ID, OriginArriveTimeUTC, OriginDepartTimeUTC, OriginMinutes, OriginWaitReasonID, OriginWaitNotes
                    , CarrierTicketNum, OriginBOLNum, Rejected, RejectReasonID, RejectNotes, Chainup, OriginTruckMileage
                    , PickupPrintStatusID, PickupPrintDateUTC, OriginGrossUnits, OriginGrossStdUnits, OriginNetUnits, OriginWeightNetUnits
                    , PickupDriverNotes, DispatchNotes
                    , TicketTypeID 
				FROM deleted
			) X
		-- only include records that didn't have the PickupLastChangeDateUTC explicitly updated (like from the DriverApp) - 3.10.10.3
		) AND ID IN (
			SELECT ID FROM (
				SELECT ID, PickupLastChangeDateUTC FROM inserted INTERSECT SELECT ID, PickupLastChangeDateUTC FROM deleted
			) X
		)
		UPDATE tblOrder 
		  SET DeliverLastChangeDateUTC = LastChangeDateUTC
		WHERE ID IN (
			SELECT ID FROM (
				SELECT ID, DeliverLastChangeDateUTC, DestArriveTimeUTC, DestDepartTimeUTC, DestMinutes, DestWaitReasonID, DestWaitNotes
                    , DestNetUnits, DestGrossUnits
                    , DestBOLNum, DestRailcarNum, DestTrailerWaterCapacity, DestOpenMeterUnits, DestCloseMeterUnits, DestProductBSW
                    , DestProductGravity, DestProductTemp, DestTruckMileage 
                    , DeliverPrintStatusID, DeliverPrintDateUTC, DeliverDriverNotes, DestRackBay
                    , DestWeightGrossUnits, DestWeightTareUnits, DestWeightNetUnits
                    , DispatchNotes 
				FROM inserted
				EXCEPT
				SELECT ID, DeliverLastChangeDateUTC, DestArriveTimeUTC, DestDepartTimeUTC, DestMinutes, DestWaitReasonID, DestWaitNotes
                    , DestNetUnits, DestGrossUnits
                    , DestBOLNum, DestRailcarNum, DestTrailerWaterCapacity, DestOpenMeterUnits, DestCloseMeterUnits, DestProductBSW
                    , DestProductGravity, DestProductTemp, DestTruckMileage 
                    , DeliverPrintStatusID, DeliverPrintDateUTC, DeliverDriverNotes, DestRackBay
                    , DestWeightGrossUnits, DestWeightTareUnits, DestWeightNetUnits 
                    , DispatchNotes
				FROM deleted
			) X
		-- only include records that didn't have the DeliverLastChangeDateUTC explicitly updated (like from the DriverApp) - 3.10.10.3
		) AND ID IN (
			SELECT ID FROM (
				SELECT ID, DeliverLastChangeDateUTC FROM inserted INTERSECT SELECT ID, DeliverLastChangeDateUTC FROM deleted
			) X
		)
		/**************************************************************************************************************/

		-- 3.9.38 - 2016/01/11 - JAE - recompute the Net Weight of any changed Mineral Run orders
		IF UPDATE(DestWeightGrossUnits) OR UPDATE(DestWeightTareUnits) OR UPDATE(DestUomID) BEGIN
			UPDATE tblOrder
				SET DestWeightNetUnits = O.DestWeightGrossUnits - O.DestWeightTareUnits
			FROM tblOrder O
			JOIN tblDestination D ON D.ID = O.DestinationID
			JOIN inserted i on i.ID = O.ID
			WHERE D.TicketTypeID = 9 -- Mineral Run tickets only
		END 			

		/*************************************************************************************************************/
		/* handle driver changes - "orphan" the order/ticket (with a new ORDERNUM) - and clone it for the new driver */
		IF (UPDATE(DriverID))
		BEGIN
			-- create cloned order/ticket records (these records will be cloned then the original reset to orphaned status)
			SELECT O.*, NewOrderNum = CAST(NULL as int)
			INTO #clone
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID AND d.DriverID IS NOT NULL AND d.DriverID <> isnull(O.DriverID, 0)
			/* JOIN to tblOrderTransfer so we can prevent treating an OrderTransfer "driver change" as a Orphaned Order */
			LEFT JOIN tblOrderTransfer OTR ON OTR.OrderID = O.ID  /* 3.9.20 - added */
			WHERE OTR.OrderID IS NULL

			/* generate the next OrderNum values for these new records */
			DECLARE @newOrderNum int; SET @newOrderNum = isnull((SELECT max(OrderNum) FROM tblOrder), 100000) + 1;
			DECLARE @newReassignKey int; SET @newReassignKey = isnull((SELECT max(ReassignKey) FROM tblOrder), 0) + 1;
			WHILE (SELECT count(*) FROM #clone WHERE NewOrderNum IS NULL) > 0
			BEGIN
				UPDATE #clone
				  SET NewOrderNum = @newOrderNum, ReassignKey = isnull(ReassignKey, @newReassignKey)
					, CreateDateUTC = getutcdate()
				WHERE ID = (SELECT min(ID) FROM #clone WHERE NewOrderNum IS NULL)
				-- if the @newReassignKey wasn't used above, then that # will be skipped, but this should not be an issue
				SELECT @newOrderNum = @newOrderNum + 1, @newReassignKey = @newReassignKey + 1
			END
			
			-- reset the existing records as orphaned
			UPDATE tblOrder
				SET DeleteDateUTC = isnull(O.DeleteDateUTC, GETUTCDATE()), DeletedByUser = isnull(O.DeletedByUser, O.LastChangedByUser)
					, CarrierID = d.CarrierID, DriverID = d.DriverID, TruckID = d.TruckID, TrailerID = d.TrailerID, Trailer2ID = d.Trailer2ID
					, DispatchConfirmNum = NULL
					, ReassignKey = C.ReassignKey
			FROM tblOrder O
			JOIN #clone C ON C.ID = O.ID
			JOIN deleted d on D.ID = O.id

			-- create the cloned records (which is essentially the original with a new ID and the Carrrier & default Truck|Trailers of the new driver)
			INSERT INTO tblOrder (OrderDate, OrderNum, StatusID, PriorityID, DueDate, RouteID, OriginID, OriginArriveTimeUTC, OriginDepartTimeUTC
				, OriginMinutes, OriginWaitNotes, OriginBOLNum, OriginGrossUnits, OriginNetUnits, DestinationID, DestArriveTimeUTC, DestDepartTimeUTC
				, DestMinutes, DestWaitNotes, DestBOLNum, DestGrossUnits, DestNetUnits, CustomerID, CarrierID, DriverID, TruckID, TrailerID
				, Trailer2ID, OperatorID, PumperID, TicketTypeID, Rejected, RejectNotes, ChainUp, OriginTruckMileage, OriginTankNum, DestTruckMileage
				, CarrierTicketNum, AuditNotes, CreateDateUTC, ActualMiles, ProducerID, CreatedByUser, LastChangeDateUTC, LastChangedByUser
				, DeleteDateUTC, DeletedByUser, DestProductBSW, DestProductGravity, DestProductTemp, ProductID, AcceptLastChangeDateUTC
				, PickupLastChangeDateUTC, DeliverLastChangeDateUTC, OriginUomID, DestUomID, PickupPrintStatusID, DeliverPrintStatusID
				, PickupPrintDateUTC, DeliverPrintDateUTC, OriginTankID, OriginGrossStdUnits, DispatchNotes, PickupDriverNotes, DeliverDriverNotes
				, DispatchConfirmNum, OriginWaitReasonID, DestWaitReasonID, RejectReasonID, DestOpenMeterUnits, DestCloseMeterUnits
				, OriginWeightNetUnits, ReassignKey)
				SELECT OrderDate, NewOrderNum, StatusID, PriorityID, DueDate, RouteID, OriginID, OriginArriveTimeUTC, OriginDepartTimeUTC, OriginMinutes, OriginWaitNotes
					, OriginBOLNum, OriginGrossUnits, OriginNetUnits, DestinationID, DestArriveTimeUTC, DestDepartTimeUTC, DestMinutes, DestWaitNotes
					, DestBOLNum, DestGrossUnits, DestNetUnits, CustomerID, isnull(D.CarrierID, O.CarrierID), DriverID, D.TruckID, D.TrailerID, D.Trailer2ID, OperatorID
					, PumperID, TicketTypeID, Rejected, RejectNotes, ChainUp, OriginTruckMileage, OriginTankNum, DestTruckMileage, CarrierTicketNum
					, AuditNotes, O.CreateDateUTC, ActualMiles, ProducerID, O.CreatedByUser, GETUTCDATE(), O.LastChangedByUser, O.DeleteDateUTC, O.DeletedByUser
					, DestProductBSW, DestProductGravity, DestProductTemp, ProductID, AcceptLastChangeDateUTC, PickupLastChangeDateUTC, DeliverLastChangeDateUTC
					, OriginUomID, DestUomID, PickupPrintStatusID, DeliverPrintStatusID, PickupPrintDateUTC, DeliverPrintDateUTC, OriginTankID
					, OriginGrossStdUnits, DispatchNotes, PickupDriverNotes, DeliverDriverNotes, DispatchConfirmNum, OriginWaitReasonID, DestWaitReasonID
					, RejectReasonID, DestOpenMeterUnits, DestCloseMeterUnits, OriginWeightNetUnits, ReassignKey
				FROM #clone O
				LEFT JOIN tblDriver D ON D.ID = O.DriverID
		
			-- cache the affected tickets
			SELECT OT.*, C.NewOrderNum, OrderDeletedByUser = C.LastChangedByUser
			INTO #cloneTicket
			FROM tblOrderTicket OT
			JOIN #clone C ON C.ID = OT.OrderID
			WHERE OT.DeleteDateUTC IS NULL
			
			-- create the cloned Order Ticket records (copy of existing ticket(s) pointed at new "original" Order)
			INSERT INTO tblOrderTicket (OrderID, CarrierTicketNum, TicketTypeID, TankNum, ProductObsGravity, ProductObsTemp, ProductBSW, OpeningGaugeFeet
				, OpeningGaugeInch, OpeningGaugeQ, ClosingGaugeFeet, ClosingGaugeInch, ClosingGaugeQ, GrossUnits, NetUnits, Rejected, RejectNotes
				, SealOff, SealOn, BOLNum, ProductHighTemp, ProductLowTemp, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser
				, DeleteDateUTC, DeletedByUser, UID, FromMobileApp, OriginTankID, BottomFeet, BottomInches, BottomQ, GrossStdUnits, RejectReasonID
				, MeterFactor, OpenMeterUnits, CloseMeterUnits, WeightTareUnits, WeightGrossUnits, WeightNetUnits)
				SELECT O.ID, CT.CarrierTicketNum, CT.TicketTypeID, CT.TankNum, CT.ProductObsGravity, CT.ProductObsTemp, CT.ProductBSW, CT.OpeningGaugeFeet
					, CT.OpeningGaugeInch, CT.OpeningGaugeQ, CT.ClosingGaugeFeet, CT.ClosingGaugeInch, CT.ClosingGaugeQ, CT.GrossUnits, CT.NetUnits
					, CT.Rejected, CT.RejectNotes, CT.SealOff, CT.SealOn, CT.BOLNum, CT.ProductHighTemp, CT.ProductLowTemp, CT.CreateDateUTC, CT.CreatedByUser
					, GETUTCDATE(), CT.LastChangedByUser, CT.DeleteDateUTC, CT.DeletedByUser, newid(), CT.FromMobileApp, CT.OriginTankID, CT.BottomFeet
					, CT.BottomInches, CT.BottomQ, CT.GrossStdUnits, CT.RejectReasonID, CT.MeterFactor, CT.OpenMeterUnits, CT.CloseMeterUnits, WeightTareUnits
					, WeightGrossUnits, WeightNetUnits
				FROM #cloneTicket CT
				JOIN tblOrder O ON O.OrderNum = CT.NewOrderNum
		END
		/*************************************************************************************************************/
		
		/* HANDLE TRUCK | TRAILER | TRAILER2 default changes when the driver updates his Truck | Trailer | Trailer2 on ACCEPTANCE */
		-- TRUCK
		IF (UPDATE(TruckID) AND EXISTS (
			SELECT i.* 
			FROM inserted i 
			JOIN deleted d ON d.ID = i.ID
			  AND i.DriverID = d.DriverID 
			  AND i.TruckID <> d.TruckID
			WHERE i.StatusID IN (7, 8, 3) -- ACCEPTED, PICKEDUP, DELIVERED
			  AND d.StatusID IN (2))) -- DISPATCHED
		BEGIN
			UPDATE tblDriver
			  SET TruckID = i.TruckID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblDriver DR
			JOIN inserted i ON i.DriverID = DR.ID
			JOIN deleted d ON d.DriverID = DR.ID
			WHERE i.TruckID <> d.TruckID
			
			UPDATE tblOrder
			  SET TruckID = i.TruckID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblOrder O
			JOIN inserted i ON i.DriverID = O.DriverID 
			  AND O.ID <> i.ID
			  AND O.StatusID IN (2) -- DISPATCHED
			  AND i.TruckID <> O.TruckID
			  AND O.DeleteDateUTC IS NULL
		END
		-- TRAILER
		IF (UPDATE(TrailerID) AND EXISTS (
			SELECT i.* 
			FROM inserted i 
			JOIN deleted d ON d.ID = i.ID
			  AND i.DriverID = d.DriverID 
			  AND i.TrailerID <> d.TrailerID
			WHERE i.StatusID IN (7, 8, 3) -- ACCEPTED, PICKEDUP, DELIVERED
			  AND d.StatusID IN (2))) -- DISPATCHED
		BEGIN
			UPDATE tblDriver
			  SET TrailerID = i.TrailerID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblDriver DR
			JOIN inserted i ON i.DriverID = DR.ID
			JOIN deleted d ON d.DriverID = DR.ID
			WHERE i.TrailerID <> d.TrailerID
			
			UPDATE tblOrder
			  SET TrailerID = i.TrailerID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblOrder O
			JOIN inserted i ON i.DriverID = O.DriverID 
			  AND O.ID <> i.ID
			  AND O.StatusID IN (2) -- DISPATCHED
			  AND i.TrailerID <> O.TrailerID
			  AND O.DeleteDateUTC IS NULL
		END
		-- TRAILER 2
		IF (UPDATE(Trailer2ID) AND EXISTS (
			SELECT i.* 
			FROM inserted i 
			JOIN deleted d ON d.ID = i.ID
			  AND i.DriverID = d.DriverID 
			  AND isnull(i.Trailer2ID, 0) <> isnull(d.Trailer2ID, 0)
			WHERE i.StatusID IN (7, 8, 3) -- ACCEPTED, PICKEDUP, DELIVERED
			  AND d.StatusID IN (2))) -- DISPATCHED
		BEGIN
			UPDATE tblDriver
			  SET TrailerID = i.TrailerID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblDriver DR
			JOIN inserted i ON i.DriverID = DR.ID
			JOIN deleted d ON d.DriverID = DR.ID
			WHERE isnull(i.Trailer2ID, 0) <> isnull(d.Trailer2ID, 0)
			
			UPDATE tblOrder
			  SET Trailer2ID = i.Trailer2ID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblOrder O
			JOIN inserted i ON i.DriverID = O.DriverID 
			  AND O.ID <> i.ID
			  AND O.StatusID IN (2) -- DISPATCHED
			  AND isnull(i.Trailer2ID, 0) <> isnull(O.Trailer2ID, 0)
			  AND O.DeleteDateUTC IS NULL
		END
		/*************************************************************************************************************/
	
		DECLARE @deliveredIDs TABLE (ID int)
		DECLARE @auditedIDs TABLE (ID int)
		DECLARE @id int
		DECLARE @autoAudit bit; SET @autoAudit = dbo.fnToBool(dbo.fnSettingValue(54))
		DECLARE @toAudit bit
		-- Auto-Audit/Apply Settlement Rates/Amounts/Auto-Approve to Order when STATUS is changed to DELIVERED (until it is FINAL settled)
		IF (UPDATE(StatusID) OR UPDATE(DeliverPrintStatusID))
		BEGIN
			INSERT INTO @deliveredIDs (ID)
				SELECT i.ID 
				FROM inserted i
				JOIN tblPrintStatus iPS ON iPS.ID = i.DeliverPrintStatusID
				JOIN deleted d ON d.ID = i.ID
				JOIN tblPrintStatus dPS ON dPS.ID = d.DeliverPrintStatusID
				WHERE i.StatusID = 3 AND d.StatusID <> 4 AND iPS.IsCompleted = 1 AND i.StatusID + iPS.IsCompleted <> d.StatusID + dPS.IsCompleted
			
			SELECT @id = MIN(ID) FROM @deliveredIDs
			WHILE @id IS NOT NULL
			BEGIN
				-- attempt to AUTO-AUDIT the order if this feature is enabled (global setting)
				IF (@autoAudit = 1)
				BEGIN
					EXEC spAutoAuditOrder @ID, 'System', @toAudit OUTPUT
					/* if the order was updated to AUDITED status, then we need to attempt also to auto-approve [below] */
					IF (@toAudit = 1)
						INSERT INTO @auditedIDs VALUES (@id)
				END
				-- attempt to apply rates to all newly DELIVERED orders
				EXEC spApplyRatesBoth @id, 'System' 
				SET @id = (SELECT MIN(id) FROM @deliveredIDs WHERE ID > @id)
			END
		END

		-- Auto-Approve any un-approved orders when STATUS changed to AUDIT
		IF (UPDATE(StatusID) OR EXISTS (SELECT * FROM @auditedIDs))
		BEGIN
			INSERT INTO @auditedIDs (ID)
				SELECT i.ID 
				FROM inserted i
				JOIN deleted d ON d.ID = i.ID
				LEFT JOIN tblOrderApproval OA ON OA.OrderID = i.ID
				LEFT JOIN @auditedIDs AID ON AID.ID = i.ID
				WHERE i.StatusID = 4 AND d.StatusID <> 4 AND OA.OrderID IS NULL AND AID.ID IS NULL

			SELECT @id = MIN(ID) FROM @auditedIDs
			WHILE @id IS NOT NULL
			BEGIN
				EXEC spAutoApproveOrder @id, 'System'
				SET @id = (SELECT MIN(id) FROM @auditedIDs WHERE ID > @id)
			END
			
		END
		
		/* auto UNAPPROVE any orders that have their status reverted from AUDITED */
		IF (UPDATE(StatusID))
		BEGIN
			DELETE FROM tblOrderApproval
			WHERE OrderID IN (
				SELECT i.ID
				FROM inserted i
				JOIN deleted d ON d.ID = i.ID
				JOIN tblOrderApproval OA ON OA.OrderID = i.ID
				WHERE d.StatusID = 4 AND i.StatusID <> 4
			)
		END

		-- purge any DriverApp/Gauger App sync records for any orders that are not in an DRIVER APP eligible status
		IF UPDATE(StatusID) AND EXISTS (SELECT ID FROM inserted WHERE StatusID NOT IN (2,7,8,3))
		BEGIN
			DELETE FROM tblDriverAppPrintHeaderImageSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID NOT IN (2,7,8,3))
			DELETE FROM tblDriverAppPrintPickupTemplateSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID NOT IN (2,7,8,3))
			DELETE FROM tblDriverAppPrintDeliverTemplateSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID NOT IN (2,7,8,3))
			DELETE FROM tblDriverAppPrintFooterTemplateSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID NOT IN (2,7,8,3))
			DELETE FROM tblOrderAppChanges WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID NOT IN (2,7,8,3))
		END
		-- purge any DriverApp/Gauger App sync records for any orders that are not in a GAUGER APP eligible status
		IF UPDATE(StatusID) AND EXISTS (SELECT ID FROM inserted WHERE StatusID NOT IN (-9,-11))
		BEGIN
			DELETE FROM tblGaugerAppPrintHeaderImageSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID NOT IN (-9,-11))
			DELETE FROM tblGaugerAppPrintPickupTemplateSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID NOT IN (-9,-11))
			DELETE FROM tblGaugerAppPrintTicketTemplateSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID NOT IN (-9,-11))
		END
				
		--NOTE: we do not update the DestOpenMeterUnits/DestCloseMeterUnits since they don't auto update
		--  (and they simply represent a reading that is assumed to in the DestUomID UOM at entry)
		--  (if they are updated in the future [manually] the will calculate again using the DestUomID at that time)

		IF EXISTS (SELECT * FROM inserted EXCEPT SELECT * FROM deleted)
		BEGIN
			-- optionally add tblOrderDBAudit records
			BEGIN TRY
				IF EXISTS(SELECT * FROM tblSetting WHERE ID = 32 AND (Value LIKE 'true' OR Value LIKE 'yes'))
					INSERT INTO tblOrderDbAudit (DBAuditDate, ID, OrderNum, StatusID, PriorityID, DueDate, RouteID, OriginID
						, OriginArriveTimeUTC, OriginDepartTimeUTC, OriginMinutes, OriginWaitNotes, OriginBOLNum, OriginGrossUnits
						, OriginNetUnits, DestinationID, DestArriveTimeUTC, DestDepartTimeUTC, DestMinutes, DestWaitNotes, DestBOLNum
						, DestGrossUnits, DestNetUnits, CustomerID, CarrierID, DriverID, TruckID, TrailerID, Trailer2ID, OperatorID
						, PumperID, TicketTypeID, Rejected, RejectNotes, ChainUp, OriginTruckMileage, OriginTankNum, DestTruckMileage
						, CarrierTicketNum, AuditNotes, CreateDateUTC, ActualMiles, ProducerID, CreatedByUser, LastChangeDateUTC
						, LastChangedByUser, DeleteDateUTC, DeletedByUser, DestProductBSW, DestProductGravity, DestProductTemp
						, ProductID, AcceptLastChangeDateUTC, PickupLastChangeDateUTC, DeliverLastChangeDateUTC, OriginUomID, DestUomID
						, PickupPrintStatusID, DeliverPrintStatusID, PickupPrintDateUTC, DeliverPrintDateUTC, OriginTankID
						, OriginGrossStdUnits, DispatchNotes, PickupDriverNotes, DeliverDriverNotes, DispatchConfirmNum, OriginWaitReasonID
						, DestWaitReasonID, RejectReasonID, DestOpenMeterUnits, DestCloseMeterUnits, ReassignKey, DestRackBay, OrderDate
						, DestTrailerWaterCapacity, DestRailCarNum, /*OriginWeightGrossUnits,*/ OriginWeightNetUnits, DestWeightGrossUnits
						, DestWeightTareUnits, DestWeightNetUnits)
						SELECT GETUTCDATE(), ID, OrderNum, StatusID, PriorityID, DueDate, RouteID, OriginID, OriginArriveTimeUTC
							, OriginDepartTimeUTC, OriginMinutes, OriginWaitNotes, OriginBOLNum, OriginGrossUnits, OriginNetUnits
							, DestinationID, DestArriveTimeUTC, DestDepartTimeUTC, DestMinutes, DestWaitNotes, DestBOLNum, DestGrossUnits
							, DestNetUnits, CustomerID, CarrierID, DriverID, TruckID, TrailerID, Trailer2ID, OperatorID, PumperID
							, TicketTypeID, Rejected, RejectNotes, ChainUp, OriginTruckMileage, OriginTankNum, DestTruckMileage
							, CarrierTicketNum, AuditNotes, CreateDateUTC, ActualMiles, ProducerID, CreatedByUser, LastChangeDateUTC
							, LastChangedByUser, DeleteDateUTC, DeletedByUser, DestProductBSW, DestProductGravity, DestProductTemp
							, ProductID, AcceptLastChangeDateUTC, PickupLastChangeDateUTC, DeliverLastChangeDateUTC, OriginUomID
							, DestUomID, PickupPrintStatusID, DeliverPrintStatusID, PickupPrintDateUTC, DeliverPrintDateUTC, OriginTankID
							, OriginGrossStdUnits, DispatchNotes, PickupDriverNotes, DeliverDriverNotes, DispatchConfirmNum
							, OriginWaitReasonID, DestWaitReasonID, RejectReasonID, DestOpenMeterUnits, DestCloseMeterUnits, ReassignKey
							, DestRackBay, OrderDate, DestTrailerWaterCapacity, DestRailCarNum, /*OriginWeightGrossUnits,*/ OriginWeightNetUnits
							, DestWeightGrossUnits, DestWeightTareUnits, DestWeightNetUnits
						FROM deleted d
			END TRY
			BEGIN CATCH
				PRINT 'trigOrder_IU.DBAUDIT FAILURE: ' + ERROR_MESSAGE()
			END CATCH
		END
		
	PRINT 'trigOrder_IU COMPLETE'

	END
END
GO
EXEC sp_settriggerorder @triggername=N'[dbo].[trigOrder_IU]', @order=N'First', @stmttype=N'INSERT'
GO
EXEC sp_settriggerorder @triggername=N'[dbo].[trigOrder_IU]', @order=N'First', @stmttype=N'UPDATE'
GO

COMMIT
SET NOEXEC OFF