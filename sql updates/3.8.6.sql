-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.8.5.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.8.5.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0

DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.8.5'
SELECT  @NewVersion = '3.8.6'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Driver | Gauger App Header Image fixes'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

ALTER TABLE dbo.tblDriverAppPrintHeaderImage
	DROP CONSTRAINT FK_DriverAppPrintHeaderImage_Producer
GO
ALTER TABLE dbo.tblProducer SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblDriverAppPrintHeaderImage
	DROP CONSTRAINT FK_DriverAppPrintHeaderImage_ProductGroup
GO
ALTER TABLE dbo.tblProductGroup SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblDriverAppPrintHeaderImage
	DROP CONSTRAINT FK_DriverAppPrintHeaderImage_Shipper
GO
ALTER TABLE dbo.tblCustomer SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblDriverAppPrintHeaderImage
	DROP CONSTRAINT FK_DriverAppPrintHeaderImage_TicketType
GO
ALTER TABLE dbo.tblTicketType SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblDriverAppPrintHeaderImage
	DROP CONSTRAINT DF_DriverAppPrintHeaderImage_PrintHeaderImageLeft
GO
ALTER TABLE dbo.tblDriverAppPrintHeaderImage
	DROP CONSTRAINT DF_DriverAppPrintHeaderImage_PrintHeaderImageTop
GO
ALTER TABLE dbo.tblDriverAppPrintHeaderImage
	DROP CONSTRAINT DF_DriverAppPrintHeaderImage_PrintHeaderImageWidth
GO
ALTER TABLE dbo.tblDriverAppPrintHeaderImage
	DROP CONSTRAINT DF_DriverAppPrintHeaderImage_PrintHeaderImageHeight
GO
ALTER TABLE dbo.tblDriverAppPrintHeaderImage
	DROP CONSTRAINT DF_DriverAppPrintHeaderImage_CreateDateUTC
GO
ALTER TABLE dbo.tblDriverAppPrintHeaderImage
	DROP CONSTRAINT DF_DriverAppPrintHeaderImage_CreatedByUser
GO
CREATE TABLE dbo.Tmp_tblDriverAppPrintHeaderImage
	(
	ID int NOT NULL IDENTITY (1, 1),
	TicketTypeID int NULL,
	ShipperID int NULL,
	ProductGroupID int NULL,
	ProducerID int NULL,
	ImageBlob varbinary(MAX) NULL,
	ImageFileName varchar(255) NULL,
	ImageLeft int NOT NULL,
	ImageTop int NOT NULL,
	ImageWidth int NOT NULL,
	ImageHeight int NOT NULL,
	CreateDateUTC smalldatetime NULL,
	CreatedByUser varchar(100) NOT NULL,
	LastChangeDateUTC smalldatetime NULL,
	LastChangedByUser varchar(100) NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_tblDriverAppPrintHeaderImage SET (LOCK_ESCALATION = TABLE)
GO
GRANT DELETE ON dbo.Tmp_tblDriverAppPrintHeaderImage TO role_iis_acct  AS dbo
GO
GRANT INSERT ON dbo.Tmp_tblDriverAppPrintHeaderImage TO role_iis_acct  AS dbo
GO
GRANT SELECT ON dbo.Tmp_tblDriverAppPrintHeaderImage TO role_iis_acct  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_tblDriverAppPrintHeaderImage TO role_iis_acct  AS dbo
GO
ALTER TABLE dbo.Tmp_tblDriverAppPrintHeaderImage ADD CONSTRAINT
	DF_DriverAppPrintHeaderImage_PrintHeaderImageLeft DEFAULT ((0)) FOR ImageLeft
GO
ALTER TABLE dbo.Tmp_tblDriverAppPrintHeaderImage ADD CONSTRAINT
	DF_DriverAppPrintHeaderImage_PrintHeaderImageTop DEFAULT ((0)) FOR ImageTop
GO
ALTER TABLE dbo.Tmp_tblDriverAppPrintHeaderImage ADD CONSTRAINT
	DF_DriverAppPrintHeaderImage_PrintHeaderImageWidth DEFAULT ((576)) FOR ImageWidth
GO
ALTER TABLE dbo.Tmp_tblDriverAppPrintHeaderImage ADD CONSTRAINT
	DF_DriverAppPrintHeaderImage_PrintHeaderImageHeight DEFAULT ((300)) FOR ImageHeight
GO
ALTER TABLE dbo.Tmp_tblDriverAppPrintHeaderImage ADD CONSTRAINT
	DF_DriverAppPrintHeaderImage_CreateDateUTC DEFAULT (getutcdate()) FOR CreateDateUTC
GO
ALTER TABLE dbo.Tmp_tblDriverAppPrintHeaderImage ADD CONSTRAINT
	DF_DriverAppPrintHeaderImage_CreatedByUser DEFAULT ('N/A') FOR CreatedByUser
GO
SET IDENTITY_INSERT dbo.Tmp_tblDriverAppPrintHeaderImage ON
GO
IF EXISTS(SELECT * FROM dbo.tblDriverAppPrintHeaderImage)
	 EXEC('INSERT INTO dbo.Tmp_tblDriverAppPrintHeaderImage (ID, TicketTypeID, ShipperID, ProductGroupID, ProducerID, ImageBlob, ImageFileName, ImageLeft, ImageTop, ImageWidth, ImageHeight, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser)
		SELECT ID, TicketTypeID, ShipperID, ProductGroupID, ProducerID, ImageBlob, ImageFileName, ImageLeft, ImageTop, ImageWidth, ImageHeight, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser FROM dbo.tblDriverAppPrintHeaderImage WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_tblDriverAppPrintHeaderImage OFF
GO
ALTER TABLE dbo.tblDriverAppPrintHeaderImageSync
	DROP CONSTRAINT FK_DriverAppPrintHeaderImageSync_Record
GO
DROP TABLE dbo.tblDriverAppPrintHeaderImage
GO
EXECUTE sp_rename N'dbo.Tmp_tblDriverAppPrintHeaderImage', N'tblDriverAppPrintHeaderImage', 'OBJECT' 
GO
ALTER TABLE dbo.tblDriverAppPrintHeaderImage ADD CONSTRAINT
	PK_DriverAppPrintHeaderImage PRIMARY KEY NONCLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE UNIQUE CLUSTERED INDEX udxDriverAppPrintHeaderImage_Main ON dbo.tblDriverAppPrintHeaderImage
	(
	TicketTypeID,
	ShipperID,
	ProductGroupID,
	ProducerID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.tblDriverAppPrintHeaderImage ADD CONSTRAINT
	FK_DriverAppPrintHeaderImage_TicketType FOREIGN KEY
	(
	TicketTypeID
	) REFERENCES dbo.tblTicketType
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblDriverAppPrintHeaderImage ADD CONSTRAINT
	FK_DriverAppPrintHeaderImage_Shipper FOREIGN KEY
	(
	ShipperID
	) REFERENCES dbo.tblCustomer
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblDriverAppPrintHeaderImage ADD CONSTRAINT
	FK_DriverAppPrintHeaderImage_ProductGroup FOREIGN KEY
	(
	ProductGroupID
	) REFERENCES dbo.tblProductGroup
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblDriverAppPrintHeaderImage ADD CONSTRAINT
	FK_DriverAppPrintHeaderImage_Producer FOREIGN KEY
	(
	ProducerID
	) REFERENCES dbo.tblProducer
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblDriverAppPrintHeaderImageSync ADD CONSTRAINT
	FK_DriverAppPrintHeaderImageSync_Record FOREIGN KEY
	(
	RecordID
	) REFERENCES dbo.tblDriverAppPrintHeaderImage
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
GO
ALTER TABLE dbo.tblDriverAppPrintHeaderImageSync SET (LOCK_ESCALATION = TABLE)
GO

------
ALTER TABLE dbo.tblGaugerAppPrintHeaderImage
	DROP CONSTRAINT FK_GaugerAppPrintHeaderImage_Producer
GO
ALTER TABLE dbo.tblProducer SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblGaugerAppPrintHeaderImage
	DROP CONSTRAINT FK_GaugerAppPrintHeaderImage_ProductGroup
GO
ALTER TABLE dbo.tblProductGroup SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblGaugerAppPrintHeaderImage
	DROP CONSTRAINT FK_GaugerAppPrintHeaderImage_Shipper
GO
ALTER TABLE dbo.tblCustomer SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblGaugerAppPrintHeaderImage
	DROP CONSTRAINT FK_GaugerAppPrintHeaderImage_TicketType
GO
ALTER TABLE dbo.tblTicketType SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblGaugerAppPrintHeaderImage
	DROP CONSTRAINT DF_GaugerAppPrintHeaderImage_PrintHeaderImageLeft
GO
ALTER TABLE dbo.tblGaugerAppPrintHeaderImage
	DROP CONSTRAINT DF_GaugerAppPrintHeaderImage_PrintHeaderImageTop
GO
ALTER TABLE dbo.tblGaugerAppPrintHeaderImage
	DROP CONSTRAINT DF_GaugerAppPrintHeaderImage_PrintHeaderImageWidth
GO
ALTER TABLE dbo.tblGaugerAppPrintHeaderImage
	DROP CONSTRAINT DF_GaugerAppPrintHeaderImage_PrintHeaderImageHeight
GO
ALTER TABLE dbo.tblGaugerAppPrintHeaderImage
	DROP CONSTRAINT DF_GaugerAppPrintHeaderImage_CreateDateUTC
GO
ALTER TABLE dbo.tblGaugerAppPrintHeaderImage
	DROP CONSTRAINT DF_GaugerAppPrintHeaderImage_CreatedByUser
GO
CREATE TABLE dbo.Tmp_tblGaugerAppPrintHeaderImage
	(
	ID int NOT NULL IDENTITY (1, 1),
	TicketTypeID int NULL,
	ShipperID int NULL,
	ProductGroupID int NULL,
	ProducerID int NULL,
	ImageBlob varbinary(MAX) NULL,
	ImageFileName varchar(255) NULL,
	ImageLeft int NOT NULL,
	ImageTop int NOT NULL,
	ImageWidth int NOT NULL,
	ImageHeight int NOT NULL,
	CreateDateUTC smalldatetime NULL,
	CreatedByUser varchar(100) NOT NULL,
	LastChangeDateUTC smalldatetime NULL,
	LastChangedByUser varchar(100) NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_tblGaugerAppPrintHeaderImage SET (LOCK_ESCALATION = TABLE)
GO
GRANT DELETE ON dbo.Tmp_tblGaugerAppPrintHeaderImage TO role_iis_acct  AS dbo
GO
GRANT INSERT ON dbo.Tmp_tblGaugerAppPrintHeaderImage TO role_iis_acct  AS dbo
GO
GRANT SELECT ON dbo.Tmp_tblGaugerAppPrintHeaderImage TO role_iis_acct  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_tblGaugerAppPrintHeaderImage TO role_iis_acct  AS dbo
GO
ALTER TABLE dbo.Tmp_tblGaugerAppPrintHeaderImage ADD CONSTRAINT
	DF_GaugerAppPrintHeaderImage_PrintHeaderImageLeft DEFAULT ((0)) FOR ImageLeft
GO
ALTER TABLE dbo.Tmp_tblGaugerAppPrintHeaderImage ADD CONSTRAINT
	DF_GaugerAppPrintHeaderImage_PrintHeaderImageTop DEFAULT ((0)) FOR ImageTop
GO
ALTER TABLE dbo.Tmp_tblGaugerAppPrintHeaderImage ADD CONSTRAINT
	DF_GaugerAppPrintHeaderImage_PrintHeaderImageWidth DEFAULT ((576)) FOR ImageWidth
GO
ALTER TABLE dbo.Tmp_tblGaugerAppPrintHeaderImage ADD CONSTRAINT
	DF_GaugerAppPrintHeaderImage_PrintHeaderImageHeight DEFAULT ((300)) FOR ImageHeight
GO
ALTER TABLE dbo.Tmp_tblGaugerAppPrintHeaderImage ADD CONSTRAINT
	DF_GaugerAppPrintHeaderImage_CreateDateUTC DEFAULT (getutcdate()) FOR CreateDateUTC
GO
ALTER TABLE dbo.Tmp_tblGaugerAppPrintHeaderImage ADD CONSTRAINT
	DF_GaugerAppPrintHeaderImage_CreatedByUser DEFAULT ('N/A') FOR CreatedByUser
GO
SET IDENTITY_INSERT dbo.Tmp_tblGaugerAppPrintHeaderImage ON
GO
IF EXISTS(SELECT * FROM dbo.tblGaugerAppPrintHeaderImage)
	 EXEC('INSERT INTO dbo.Tmp_tblGaugerAppPrintHeaderImage (ID, TicketTypeID, ShipperID, ProductGroupID, ProducerID, ImageBlob, ImageFileName, ImageLeft, ImageTop, ImageWidth, ImageHeight, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser)
		SELECT ID, TicketTypeID, ShipperID, ProductGroupID, ProducerID, ImageBlob, ImageFileName, ImageLeft, ImageTop, ImageWidth, ImageHeight, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser FROM dbo.tblGaugerAppPrintHeaderImage WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_tblGaugerAppPrintHeaderImage OFF
GO
ALTER TABLE dbo.tblGaugerAppPrintHeaderImageSync
	DROP CONSTRAINT FK_GaugerAppPrintHeaderImageSync_Record
GO
DROP TABLE dbo.tblGaugerAppPrintHeaderImage
GO
EXECUTE sp_rename N'dbo.Tmp_tblGaugerAppPrintHeaderImage', N'tblGaugerAppPrintHeaderImage', 'OBJECT' 
GO
ALTER TABLE dbo.tblGaugerAppPrintHeaderImage ADD CONSTRAINT
	PK_GaugerAppPrintHeaderImage PRIMARY KEY NONCLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE UNIQUE CLUSTERED INDEX udxGaugerAppPrintHeaderImage_Main ON dbo.tblGaugerAppPrintHeaderImage
	(
	TicketTypeID,
	ShipperID,
	ProductGroupID,
	ProducerID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.tblGaugerAppPrintHeaderImage ADD CONSTRAINT
	FK_GaugerAppPrintHeaderImage_TicketType FOREIGN KEY
	(
	TicketTypeID
	) REFERENCES dbo.tblTicketType
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblGaugerAppPrintHeaderImage ADD CONSTRAINT
	FK_GaugerAppPrintHeaderImage_Shipper FOREIGN KEY
	(
	ShipperID
	) REFERENCES dbo.tblCustomer
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblGaugerAppPrintHeaderImage ADD CONSTRAINT
	FK_GaugerAppPrintHeaderImage_ProductGroup FOREIGN KEY
	(
	ProductGroupID
	) REFERENCES dbo.tblProductGroup
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblGaugerAppPrintHeaderImage ADD CONSTRAINT
	FK_GaugerAppPrintHeaderImage_Producer FOREIGN KEY
	(
	ProducerID
	) REFERENCES dbo.tblProducer
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblGaugerAppPrintHeaderImageSync ADD CONSTRAINT
	FK_GaugerAppPrintHeaderImageSync_Record FOREIGN KEY
	(
	RecordID
	) REFERENCES dbo.tblGaugerAppPrintHeaderImage
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
GO
ALTER TABLE dbo.tblGaugerAppPrintHeaderImageSync SET (LOCK_ESCALATION = TABLE)
GO

COMMIT
SET NOEXEC OFF