SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.10.13.1'
SELECT  @NewVersion = '3.10.13.2'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Truck Type - Update settlement table indexes'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


-- PART 3
-- UPDATE THE CORE SETTLEMENT INDEXES
------------------------------------------------------------------------------------------

-- Update carrier indexes

EXEC _spDropIndex 'udxCarrierAssessorialRate_Main'
GO
CREATE UNIQUE CLUSTERED INDEX udxCarrierAssessorialRate_Main ON tblCarrierAssessorialRate
(
	TypeID ASC,
	TruckTypeID ASC,
	ShipperID ASC,
	CarrierID ASC,
	ProductGroupID ASC,
	DriverGroupID ASC,
	OriginID ASC,
	DestinationID ASC,
	OriginStateID ASC,
	DestStateID ASC,
	RegionID ASC,
	ProducerID ASC,
	EffectiveDate ASC
)
GO

EXEC _spDropIndex 'idxCarrierAssessorialRate_TruckType'
GO
CREATE NONCLUSTERED INDEX idxCarrierAssessorialRate_TruckType ON tblCarrierAssessorialRate
(
	TruckTypeID ASC
)
GO


EXEC _spDropIndex 'udxCarrierDestinationWaitRate_Main'
GO
CREATE UNIQUE NONCLUSTERED INDEX udxCarrierDestinationWaitRate_Main ON tblCarrierDestinationWaitRate
(
	ReasonID ASC,
	TruckTypeID ASC,
	ShipperID ASC,
	CarrierID ASC,
	ProductGroupID ASC,
	DriverGroupID ASC,
	DestinationID ASC,
	StateID ASC,
	RegionID ASC,
	ProducerID ASC,
	EffectiveDate ASC
)
GO

EXEC _spDropIndex 'idxCarrierDestinationWaitRate_TruckType'
GO
CREATE NONCLUSTERED INDEX idxCarrierDestinationWaitRate_TruckType ON tblCarrierDestinationWaitRate
(
	TruckTypeID ASC
)
GO


EXEC _spDropIndex 'udxCarrierFuelSurchargeRate_Main'
GO
CREATE UNIQUE CLUSTERED INDEX udxCarrierFuelSurchargeRate_Main ON tblCarrierFuelSurchargeRate
(
	TruckTypeID ASC,
	ShipperID ASC,
	CarrierID ASC,
	ProductGroupID ASC,
	DriverGroupID ASC,
	EffectiveDate ASC
)
GO

EXEC _spDropIndex 'idxCarrierFuelSurchargeRate_TruckType'
GO
CREATE NONCLUSTERED INDEX idxCarrierFuelSurchargeRate_TruckType ON tblCarrierFuelSurchargeRate
(
	TruckTypeID ASC
)
GO


EXEC _spDropIndex 'udxCarrierMinSettlementUnits_Main'
GO
CREATE UNIQUE CLUSTERED INDEX udxCarrierMinSettlementUnits_Main ON tblCarrierMinSettlementUnits
(
	TruckTypeID ASC,
	ShipperID ASC,
	CarrierID ASC,
	ProductGroupID ASC,
	OriginID ASC,
	OriginStateID ASC,
	DestinationID ASC,
	DestinationStateID ASC,
	ProducerID ASC,
	EffectiveDate ASC
)
GO

EXEC _spDropIndex 'idxCarrierMinSettlementUnits_TruckType'
GO
CREATE NONCLUSTERED INDEX idxCarrierMinSettlementUnits_TruckType ON tblCarrierMinSettlementUnits
(
	TruckTypeID ASC
)
GO


EXEC _spDropIndex 'udxCarrierOrderRejectRate_Main'
GO
CREATE UNIQUE NONCLUSTERED INDEX udxCarrierOrderRejectRate_Main ON tblCarrierOrderRejectRate
(
	ReasonID ASC,
	TruckTypeID ASC,
	ShipperID ASC,
	CarrierID ASC,
	ProductGroupID ASC,
	DriverGroupID ASC,
	OriginID ASC,
	StateID ASC,
	RegionID ASC,
	ProducerID ASC,
	EffectiveDate ASC
)
GO

EXEC _spDropIndex 'idxCarrierOrderRejectRate_TruckType'
GO
CREATE NONCLUSTERED INDEX idxCarrierOrderRejectRate_TruckType ON tblCarrierOrderRejectRate
(
	TruckTypeID ASC
)
GO


EXEC _spDropIndex 'udxCarrierOriginWaitRate_Main'
GO
CREATE UNIQUE NONCLUSTERED INDEX udxCarrierOriginWaitRate_Main ON tblCarrierOriginWaitRate
(
	ReasonID ASC,
	TruckTypeID ASC,
	ShipperID ASC,
	CarrierID ASC,
	ProductGroupID ASC,
	DriverGroupID ASC,
	OriginID ASC,
	StateID ASC,
	RegionID ASC,
	ProducerID ASC,
	EffectiveDate ASC
)
GO
EXEC _spDropIndex 'idxCarrierOriginWaitRate_TruckType'
GO
CREATE NONCLUSTERED INDEX idxCarrierOriginWaitRate_TruckType ON tblCarrierOriginWaitRate
(
	TruckTypeID ASC
)
GO


EXEC _spDropIndex 'udxCarrierRateSheet_Main'
GO
CREATE UNIQUE CLUSTERED INDEX udxCarrierRateSheet_Main ON tblCarrierRateSheet
(
	TruckTypeID ASC,
	ShipperID ASC,
	CarrierID ASC,
	ProductGroupID ASC,
	DriverGroupID ASC,
	RegionID ASC,
	OriginStateID ASC,
	DestStateID ASC,
	ProducerID ASC,
	EffectiveDate ASC
)
GO

EXEC _spDropIndex 'idxCarrierRateSheet_TruckType'
GO
CREATE NONCLUSTERED INDEX idxCarrierRateSheet_TruckType ON tblCarrierRateSheet
(
	TruckTypeID ASC
)
GO


EXEC _spDropIndex 'udxCarrierRouteRate_Main'
GO
CREATE UNIQUE CLUSTERED INDEX udxCarrierRouteRate_Main ON tblCarrierRouteRate
(
	TruckTypeID ASC,
	ShipperID ASC,
	CarrierID ASC,
	ProductGroupID ASC,
	DriverGroupID ASC,
	RouteID ASC,
	EffectiveDate ASC
)
GO
EXEC _spDropIndex 'idxCarrierRouteRate_TruckType'
GO
CREATE NONCLUSTERED INDEX idxCarrierRouteRate_TruckType ON tblCarrierRouteRate
(
	TruckTypeID ASC
)
GO



EXEC _spDropIndex 'udxCarrierSettlementFactor_Main'
GO
CREATE UNIQUE CLUSTERED INDEX udxCarrierSettlementFactor_Main ON tblCarrierSettlementFactor
(
	TruckTypeID ASC,
	ShipperID ASC,
	CarrierID ASC,
	ProductGroupID ASC,
	OriginID ASC,
	OriginStateID ASC,
	DestinationID ASC,
	DestinationStateID ASC,
	ProducerID ASC,
	EffectiveDate ASC
)
GO
EXEC _spDropIndex 'idxCarrierSettlementFactor_TruckType'
GO
CREATE NONCLUSTERED INDEX idxCarrierSettlementFactor_TruckType ON tblCarrierSettlementFactor
(
	TruckTypeID ASC
)
GO


EXEC _spDropIndex 'udxCarrierWaitFeeParameter_Main'
GO
CREATE UNIQUE CLUSTERED INDEX udxCarrierWaitFeeParameter_Main ON tblCarrierWaitFeeParameter
(
	TruckTypeID ASC,
	ShipperID ASC,
	CarrierID ASC,
	ProductGroupID ASC,
	OriginStateID ASC,
	DestStateID ASC,
	RegionID ASC,
	ProducerID ASC,
	EffectiveDate ASC
)
GO
EXEC _spDropIndex 'idxCarrierWaitFeeParameter_TruckType'
GO
CREATE NONCLUSTERED INDEX idxCarrierWaitFeeParameter_TruckType ON tblCarrierWaitFeeParameter
(
	TruckTypeID ASC
)
GO



-- Update shipper indexes


EXEC _spDropIndex 'udxShipperAssessorialRate_Main'
GO
CREATE UNIQUE CLUSTERED INDEX udxShipperAssessorialRate_Main ON tblShipperAssessorialRate
(
	TypeID ASC,
	TruckTypeID ASC,
	ShipperID ASC,
	ProductGroupID ASC,
	OriginID ASC,
	DestinationID ASC,
	OriginStateID ASC,
	DestStateID ASC,
	RegionID ASC,
	ProducerID ASC,
	EffectiveDate ASC
)
GO

EXEC _spDropIndex 'idxShipperAssessorialRate_TruckType'
GO
CREATE NONCLUSTERED INDEX idxShipperAssessorialRate_TruckType ON tblShipperAssessorialRate
(
	TruckTypeID ASC
)
GO


EXEC _spDropIndex 'udxShipperDestinationWaitRate_Main'
GO
CREATE UNIQUE NONCLUSTERED INDEX udxShipperDestinationWaitRate_Main ON tblShipperDestinationWaitRate
(
	ReasonID ASC,
	TruckTypeID ASC,
	ShipperID ASC,
	ProductGroupID ASC,
	DestinationID ASC,
	StateID ASC,
	RegionID ASC,
	ProducerID ASC,
	EffectiveDate ASC
)
GO

EXEC _spDropIndex 'idxShipperDestinationWaitRate_TruckType'
GO
CREATE NONCLUSTERED INDEX idxShipperDestinationWaitRate_TruckType ON tblShipperDestinationWaitRate
(
	TruckTypeID ASC
)
GO



EXEC _spDropIndex 'udxShipperFuelSurchargeRate_Main'
GO
CREATE UNIQUE CLUSTERED INDEX udxShipperFuelSurchargeRate_Main ON tblShipperFuelSurchargeRate
(
	TruckTypeID ASC,
	ShipperID ASC,
	ProductGroupID ASC,
	EffectiveDate ASC
)
GO

EXEC _spDropIndex 'idxShipperFuelSurchargeRate_TruckType'
GO
CREATE NONCLUSTERED INDEX idxShipperFuelSurchargeRate_TruckType ON tblShipperFuelSurchargeRate
(
	TruckTypeID ASC
)
GO



EXEC _spDropIndex 'udxShipperMinSettlementUnits_Main'
GO
CREATE UNIQUE CLUSTERED INDEX udxShipperMinSettlementUnits_Main ON tblShipperMinSettlementUnits
(
	TruckTypeID ASC,
	ShipperID ASC,
	ProductGroupID ASC,
	OriginID ASC,
	OriginStateID ASC,
	DestinationID ASC,
	DestinationStateID ASC,
	ProducerID ASC,
	EffectiveDate ASC
)
GO

EXEC _spDropIndex 'idxShipperMinSettlementUnits_TruckType'
GO
CREATE NONCLUSTERED INDEX idxShipperMinSettlementUnits_TruckType ON tblShipperMinSettlementUnits
(
	TruckTypeID ASC
)
GO



EXEC _spDropIndex 'udxShipperOrderRejectRate_Main'
GO
CREATE UNIQUE NONCLUSTERED INDEX udxShipperOrderRejectRate_Main ON tblShipperOrderRejectRate
(
	ReasonID ASC,
	TruckTypeID ASC,
	ShipperID ASC,
	ProductGroupID ASC,
	OriginID ASC,
	StateID ASC,
	RegionID ASC,
	ProducerID ASC,
	EffectiveDate ASC
)
GO

EXEC _spDropIndex 'idxShipperOrderRejectRate_TruckType'
GO
CREATE NONCLUSTERED INDEX idxShipperOrderRejectRate_TruckType ON tblShipperOrderRejectRate
(
	TruckTypeID ASC
)
GO



EXEC _spDropIndex 'udxShipperOriginWaitRate_Main'
GO
CREATE UNIQUE NONCLUSTERED INDEX udxShipperOriginWaitRate_Main ON tblShipperOriginWaitRate
(
	ReasonID ASC,
	TruckTypeID ASC,
	ShipperID ASC,
	ProductGroupID ASC,
	OriginID ASC,
	StateID ASC,
	RegionID ASC,
	ProducerID ASC,
	EffectiveDate ASC
)
GO

EXEC _spDropIndex 'idxShipperOriginWaitRate_TruckType'
GO
CREATE NONCLUSTERED INDEX idxShipperOriginWaitRate_TruckType ON tblShipperOriginWaitRate
(
	TruckTypeID ASC
)
GO



EXEC _spDropIndex 'udxShipperRateSheet_Main'
GO
CREATE UNIQUE CLUSTERED INDEX udxShipperRateSheet_Main ON tblShipperRateSheet
(
	TruckTypeID ASC,
	ShipperID ASC,
	ProductGroupID ASC,
	ProducerID ASC,
	RegionID ASC,
	OriginStateID ASC,
	DestStateID ASC,
	EffectiveDate ASC
)
GO

EXEC _spDropIndex 'idxShipperRateSheet_TruckType'
GO
CREATE NONCLUSTERED INDEX idxShipperRateSheet_TruckType ON tblShipperRateSheet
(
	TruckTypeID ASC
)
GO



EXEC _spDropIndex 'udxShipperRouteRate_Main'
GO
CREATE UNIQUE CLUSTERED INDEX udxShipperRouteRate_Main ON tblShipperRouteRate
(
	TruckTypeID ASC,
	ShipperID ASC,
	ProductGroupID ASC,
	RouteID ASC,
	EffectiveDate ASC
)
GO

EXEC _spDropIndex 'idxShipperRouteRate_TruckType'
GO
CREATE NONCLUSTERED INDEX idxShipperRouteRate_TruckType ON tblShipperRouteRate
(
	TruckTypeID ASC
)
GO



EXEC _spDropIndex 'udxShipperSettlementFactor_Main'
GO
CREATE UNIQUE CLUSTERED INDEX udxShipperSettlementFactor_Main ON tblShipperSettlementFactor
(
	TruckTypeID ASC,
	ShipperID ASC,
	ProductGroupID ASC,
	OriginID ASC,
	OriginStateID ASC,
	DestinationID ASC,
	DestinationStateID ASC,
	ProducerID ASC,
	EffectiveDate ASC
)
GO

EXEC _spDropIndex 'idxShipperSettlementFactor_TruckType'
GO
CREATE NONCLUSTERED INDEX idxShipperSettlementFactor_TruckType ON tblShipperSettlementFactor
(
	TruckTypeID ASC
)
GO



EXEC _spDropIndex 'udxShipperWaitFeeParameter_Main'
GO
CREATE UNIQUE CLUSTERED INDEX udxShipperWaitFeeParameter_Main ON tblShipperWaitFeeParameter
(
	TruckTypeID ASC,
	ShipperID ASC,
	ProductGroupID ASC,
	OriginStateID ASC,
	DestStateID ASC,
	RegionID ASC,
	ProducerID ASC,
	EffectiveDate ASC
)
GO

EXEC _spDropIndex 'idxShipperWaitFeeParameter_TruckType'
GO
CREATE NONCLUSTERED INDEX idxShipperWaitFeeParameter_TruckType ON tblShipperWaitFeeParameter
(
	TruckTypeID ASC
)
GO



COMMIT
SET NOEXEC OFF