SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.5.3'
SELECT  @NewVersion = '4.5.4'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'JT-1521 - Convert carrier compliance pages to MVC'
	UNION
	SELECT @NewVersion, 0, 'JT-219 - Compliance Report'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO



-- -------------------------------------------------------------
-- Create Carrier Compliance Permissions
-- -------------------------------------------------------------

-- Update existing carrier compliance page view permission to fit with new permissions being added below 
UPDATE aspnet_Roles
SET FriendlyName = 'Compliance Docs - View'
	, RoleName = 'viewCarrierCompliance'
	, LoweredRoleName = 'viewCarriercompliance'
	, Category = 'Assets - Compliance - Carriers'
	, Description = 'Allow user to view the Carrier Compliance page'
WHERE RoleName = 'viewComplianceCarrierDocuments'
GO

-- Update existing carrier compliance types page view permission to fit with new permissions being added below 
UPDATE aspnet_Roles
SET FriendlyName = 'Compliance Types - View'
	, RoleName = 'viewCarrierComplianceTypes'
	, LoweredRoleName = 'viewcarriercompliancetypes'
	, Category = 'Assets - Compliance - Carriers'
	, Description = 'Allow user to view the Carrier Compliance Types page'
WHERE RoleName = 'viewComplianceCarrierDocumentTypes'
GO

-- Add new Carrier compliance permissions
EXEC spAddNewPermission 'createCarrierCompliance', 'Allow user to create Carrier Compliance Documents', 'Assets - Compliance - Carriers', 'Compliance Docs - Create'
GO
EXEC spAddNewPermission 'editCarrierCompliance', 'Allow user to edit Carrier Compliance Documents', 'Assets - Compliance - Carriers', 'Compliance Docs - Edit'
GO
EXEC spAddNewPermission 'deactivateCarrierCompliance', 'Allow user to deactivate Carrier Compliance Documents', 'Assets - Compliance - Carriers', 'Compliance Docs - Deactivate'
GO

-- Add new Carrier compliance permissions to any groups that already had the "view" permission 
EXEC spAddNewPermissionToGroupsWithExistingRole 'viewCarrierCompliance', 'createCarrierCompliance'
GO
EXEC spAddNewPermissionToGroupsWithExistingRole 'viewCarrierCompliance', 'editCarrierCompliance'
GO
EXEC spAddNewPermissionToGroupsWithExistingRole 'viewCarrierCompliance', 'deactivateCarrierCompliance'
GO

-- Add new Carrier compliance type permissions ("Types" permissions do not get added to users by default when they are new permissions.  They will be added on request when needed by a user.)
EXEC spAddNewPermission 'createCarrierComplianceTypes', 'Allow user to create Carrier Compliance Types', 'Assets - Compliance - Carriers', 'Compliance Types - Create'
GO
EXEC spAddNewPermission 'editCarrierComplianceTypes', 'Allow user to edit Carrier Compliance Types', 'Assets - Compliance - Carriers', 'Compliance Types - Edit'
GO
EXEC spAddNewPermission 'deactivateCarrierComplianceTypes', 'Allow user to deactivate Carrier Compliance Types', 'Assets - Compliance - Carriers', 'Compliance Types - Deactivate'
GO

-- Add new Carrier compliance type permissions to any groups that already had the "view" permission 
EXEC spAddNewPermissionToGroupsWithExistingRole 'viewCarrierComplianceTypes', 'createCarrierComplianceTypes'
GO
EXEC spAddNewPermissionToGroupsWithExistingRole 'viewCarrierComplianceTypes', 'editCarrierComplianceTypes'
GO
EXEC spAddNewPermissionToGroupsWithExistingRole 'viewCarrierComplianceTypes', 'deactivateCarrierComplianceTypes'
GO


-- ----------------------------------------------------------------
-- Tables
-- ----------------------------------------------------------------
GO


-- Rename tblCarrierDocumentType to match naming of new truck and trailer compliance types tables
EXEC sp_rename N'dbo.tblCarrierDocumentType', N'tblCarrierComplianceType', 'OBJECT'
GO
EXEC sp_rename 'PK_CarrierDocumentType', 'PK_CarrierComplianceType'
GO
EXEC sp_rename 'DF_CarrierDocumentType_CreateDateUTC', 'DF_CarrierComplianceType_CreateDateUTC'
GO
EXEC sp_rename 'dbo.tblCarrierComplianceType.udxCarrierDocumentType_Name', 'udxCarrierComplianceType_Name', 'INDEX'
GO

-- Add new fields to renamed table
ALTER TABLE tblCarrierComplianceType ADD IsSystem BIT NOT NULL CONSTRAINT DF_CarrierComplianceType_IsSystem DEFAULT 0
GO
ALTER TABLE tblCarrierComplianceType ADD IsRequired BIT NOT NULL CONSTRAINT DF_CarrierComplianceType_IsRequired DEFAULT 0
GO
ALTER TABLE tblCarrierComplianceType ADD RequiresDocument BIT NOT NULL CONSTRAINT DF_CarrierComplianceType_RequiresDocument DEFAULT 0
GO
ALTER TABLE tblCarrierComplianceType ADD ExpirationLength INT NULL
GO
ALTER TABLE tblCarrierComplianceType ADD CarrierTypeID INT NULL CONSTRAINT FK_CarrierComplianceType_tblCarrierType REFERENCES tblCarrierType(ID)
GO

-- Drop view that is no longer needed since it was only used for the asp.net page
DROP VIEW viewCarrierDocumentTypes
GO


-- Rename tblCarrierDocument to match naming of new truck and trailer compliance types tables
EXEC sp_rename N'dbo.tblCarrierDocument', N'tblCarrierCompliance', 'OBJECT'
GO
EXEC sp_rename 'PK_CarrierDocument', 'PK_CarrierCompliance'
GO
EXEC sp_rename 'FK_CarrierDocument_Carrier', 'FK_CarrierCompliance_Carrier'
GO
EXEC sp_rename 'FK_CarrierDocument_DocumentType', 'FK_CarrierCompliance_ComplianceType'
GO
EXEC sp_rename 'DF_CarrierDocument_CreateDateUTC', 'DF_CarrierCompliance_CreateDateUTC'
GO
EXEC sp_rename 'DF_CarrierDocument_CreatedByUser', 'DF_CarrierCompliance_CreatedByUser'
GO
EXEC sp_rename 'DF_CarrierDocument_DocumentDate', 'DF_CarrierCompliance_DocumentDate'
GO
EXEC sp_rename 'dbo.tblCarrierCompliance.DocName', 'DocumentName', 'COLUMN'
GO
EXEC sp_rename 'dbo.tblCarrierCompliance.DocumentTypeID', 'CarrierComplianceTypeID', 'COLUMN'
GO



/*************************************/
-- Date Created: 26 Dec 2016
-- Author: Joe Engler
-- Purpose: ensure only one active compliance record exists
/*************************************/
CREATE TRIGGER trigCarrierCompliance_IU ON tblCarrierCompliance AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	UPDATE tblCarrierCompliance
	SET DeleteDateUTC = ISNULL(i.LastChangeDateUTC, i.CreateDateUTC),
	    DeletedByUser = ISNULL(i.LastChangedByUser, i.CreatedByUser)
	FROM tblCarrierCompliance C
	LEFT JOIN inserted i ON i.CarrierID = C.CarrierID AND i.CarrierComplianceTypeID = C.CarrierComplianceTypeID
	WHERE i.ID IS NOT NULL AND i.ID <> C.ID
	AND C.DeleteDateUTC IS NULL

	SET NOCOUNT OFF;
END
GO


INSERT INTO tblCarrierRuleType VALUES
(23, 'Enforce Carrier Compliance', 2, 0, 'Flag to check carrier compliance when dispatching orders')
GO



-- ----------------------------------------------------------------
-- Views
-- ----------------------------------------------------------------
GO

DROP VIEW viewCarrierDocument
GO
/******************************************
-- Date Created: 2015/07/22
-- Author: Kevin Alons
-- Purpose: return Carrier Compliance Document records with translated "friendly" values
-- Changes:
--	4.5.4	- 2017/02/10	- BSB	- (Changed name - originally viewCarrierDocument) Reworked using the new carrier compliance fields added some helper logic (missing, expired, etc)
******************************************/
CREATE VIEW viewCarrierCompliance AS
	SELECT CC.*
		, Carrier = C.Name
		, CarrierComplianceType = CCT.Name
		, MissingDocument = CAST(CASE WHEN Document IS NULL AND CCT.RequiresDocument = 1 THEN 1 ELSE 0 END AS BIT)
		, ExpiredNow = CAST(CASE WHEN ExpirationDate < CAST(GETDATE() AS DATE) THEN 1 ELSE 0 END AS BIT)
		, ExpiredNext30 = CAST(CASE WHEN ExpirationDate < CAST(DATEADD(MONTH, 1, GETDATE()) AS DATE) AND ExpirationDate >= CAST(GETDATE() AS DATE) THEN 1 ELSE 0 END AS BIT)
		, ExpiredNext60 = CAST(CASE WHEN ExpirationDate < CAST(DATEADD(MONTH, 2, GETDATE()) AS DATE) AND ExpirationDate >= CAST(DATEADD(MONTH, 1, GETDATE()) AS DATE) THEN 1 ELSE 0 END AS BIT)
		, ExpiredNext90 = CAST(CASE WHEN ExpirationDate < CAST(DATEADD(MONTH, 3, GETDATE()) AS DATE) AND ExpirationDate >= CAST(DATEADD(MONTH, 2, GETDATE()) AS DATE) THEN 1 ELSE 0 END AS BIT)
	FROM tblCarrierCompliance CC
	JOIN tblCarrierComplianceType CCT ON CCT.ID = CC.CarrierComplianceTypeID
	JOIN tblCarrier C ON C.ID = CC.CarrierID AND (CCT.CarrierTypeID IS NULL OR CCT.CarrierTypeID = C.CarrierTypeID)
GO



-- ----------------------------------------------------------------
-- Functions
-- ----------------------------------------------------------------
GO
/****************************************************
 Date Created: 2016/12/22 - 4.4.14
 Author: Joe Engler
 Purpose: return all carrier compliance with non compliant and missing records
 Changes:
	4.5.4	- 2017/02/10	- JAE/BSB	- Reworked using the new carrier compliance fields
****************************************************/
ALTER FUNCTION fnCarrierComplianceSummary(@showCompliant INT = 0, @showUnenforced INT = 0)
RETURNS @ret TABLE
(
	ID INT,
	CarrierID INT,
	Carrier VARCHAR(40),
	ComplianceTypeID INT,
	ComplianceType VARCHAR(100),
	MissingDocument BIT,
	ExpiredNow BIT,
	ExpiredNext30 BIT,
	ExpiredNext60 BIT,
	ExpiredNext90 BIT,
	Missing BIT
)
AS BEGIN
 	DECLARE @__ENFORCE_CARRIER_COMPLIANCE__ INT = 23

	INSERT INTO @ret
	SELECT  ID, CarrierID, Carrier, CarrierComplianceTypeID, CarrierComplianceType, MissingDocument, ExpiredNow, ExpiredNext30, ExpiredNext60, ExpiredNext90, Missing = CAST(0 AS BIT)
	  FROM viewCarrierCompliance
	 WHERE DeleteDateUTC IS NULL
	   AND (@showCompliant = 1 OR MissingDocument = 1 OR ExpiredNow = 1 OR ExpiredNext30 = 1 OR ExpiredNext60 = 1 OR ExpiredNext90 = 1)

	UNION
 
	SELECT ID = NULL,
		CarrierID = Q.ID,
		CarrierName = Q.Name,
		ComplianceTypeID = CCT.ID,
		ComplianceType = CCT.Name,
		MissingDocument = CAST(0 AS BIT),
		ExpiredNow = CAST(0 AS BIT),
		ExpiredNext30 = CAST(0 AS BIT),
		ExpiredNext60 = CAST(0 AS BIT),
		ExpiredNext90 = CAST(0 AS BIT),
		Missing = CCT.IsRequired
	FROM ( -- Get active drivers with compliance enforced
			SELECT C.* 
				FROM viewCarrier C
				OUTER APPLY dbo.fnCarrierRules(GETUTCDATE(), NULL, @__ENFORCE_CARRIER_COMPLIANCE__, NULL, C.ID, NULL, NULL, NULL, 1) CR_C

				WHERE c.DeleteDateUTC IS NULL --active carriers
				AND (@showUnenforced = 1 OR dbo.fnToBool(CR_C.Value) = 1) -- compliance enforced
		) Q,
		tblCarrierComplianceType CCT
	WHERE (@showCompliant = 1 OR CCT.IsRequired = 1)
	 AND NOT EXISTS -- no active record in the carrier document table
		(SELECT  1
			FROM viewCarrierCompliance
			WHERE DeleteDateUTC IS NULL AND CarrierID = Q.ID AND CarrierComplianceTypeID = CCT.ID
		)

	RETURN
END
GO



/****************************************************
 Date Created: 2016/12/22
 Author: Joe Engler
 Purpose: return all driver compliance with non compliant and missing records
 Changes:
--	4.5.0		BSB		2017-01-25		Add Terminal
--	4.5.4		JAE		2017-02-13		Add expiring soon records to display
****************************************************/
ALTER FUNCTION fnDriverComplianceSummary(@showCompliant INT = 0, @showUnenforced INT = 0)
RETURNS @ret TABLE
(
	ID INT,
	DriverID INT,
	FullName VARCHAR(41),
	CarrierID INT,
	Carrier VARCHAR(40),
	ComplianceTypeID INT,
	ComplianceType VARCHAR(100),
	MissingDocument BIT,
	ExpiredNow BIT,
	ExpiredNext30 BIT,
	ExpiredNext60 BIT,
	ExpiredNext90 BIT,
	Missing BIT
)
AS BEGIN
 	DECLARE @__ENFORCE_DRIVER_COMPLIANCE__ INT = 14

	INSERT INTO @ret
	SELECT  ID, DriverID, FullName, CarrierID, Carrier, ComplianceTypeID, ComplianceType, MissingDocument, ExpiredNow, ExpiredNext30, ExpiredNext60, ExpiredNext90, Missing = CAST(0 AS BIT)
	  FROM viewDriverCompliance
	 WHERE DeleteDateUTC IS NULL
	   AND (@showCompliant = 1 OR MissingDocument = 1 OR ExpiredNow = 1 OR ExpiredNext30 = 1 OR ExpiredNext60 = 1 OR ExpiredNext90 = 1)

	UNION 

	SELECT ID = NULL,
		DriverID = q.ID,
		q.FullName,
		q.CarrierID,
		q.Carrier,
		ComplianceTypeID = ct.id,
		ComplianceType = ct.Name,
		MissingDocument = CAST(0 AS BIT),
		ExpiredNow = CAST(0 AS BIT),
		ExpiredNext30 = CAST(0 AS BIT),
		ExpiredNext60 = CAST(0 AS BIT),
		ExpiredNext90 = CAST(0 AS BIT),
		Missing = ct.IsRequired
	FROM ( -- Get active drivers with compliance enforced
			SELECT d.* 
				FROM viewDriverBase d
				OUTER APPLY dbo.fnCarrierRules(GETUTCDATE(), null, @__ENFORCE_DRIVER_COMPLIANCE__, d.ID, d.CarrierID, d.TerminalID, d.OperatingStateID, d.RegionID, 1) cr_c
				WHERE d.DeleteDateUTC is null --active drivers
				AND (@showUnenforced = 1 OR dbo.fnToBool(cr_c.Value) = 1) -- compliance enforced
		) q,
		tbldrivercompliancetype ct
	WHERE (@showCompliant = 1 OR ct.IsRequired = 1)
	 AND NOT EXISTS -- no active record in the driver compliance table
		(SELECT  1
			FROM viewDriverCompliance 
			WHERE DeleteDateUTC IS NULL AND DriverID = q.ID and ComplianceTypeID = ct.id
		)
	RETURN
END
GO


-- ----------------------------------------------------------------
-- Refresh all
-- ----------------------------------------------------------------
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRebuildAllObjects
GO
EXEC _spRebuildAllObjects
GO


COMMIT
SET NOEXEC OFF