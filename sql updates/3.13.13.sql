SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20) = '3.13.12.2'
	, @NewVersion varchar(20) = '3.13.13'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1505 - Dispatch Card Planner'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO
ALTER TABLE tblOrder ADD SequenceNum INT CONSTRAINT DF_Order_SequenceNum DEFAULT 1
GO
ALTER TABLE tblOrderDbAudit ADD SequenceNum INT
GO


-- Update existing orders with a sequence number
UPDATE tblorder 
set sequencenum = orders.row
from tblorder o
INNER JOIN 	
(
		--get ordinal number for drivers active orders
		select row_number() OVER (PARTITION BY DriverID ORDER BY statusid desc, duedate, priorityid) AS ROW,
					id, driverid, priorityid, duedate, statusid 
			from tblorder 
			where statusid in (2,7,8)
			 and DeleteDateUTC is null
	) orders
	on o.ID = orders.ID

GO
/*
-- =============================================
-- Author:		Kevin Alons
-- Create date: 23 Jul 2014
-- Description:	trigger to advance any duplicated SequenceNums in a UserReport to make reordering easier
-- =============================================
CREATE TRIGGER trigOrder_IU_SequenceNum ON tblOrder FOR INSERT, UPDATE AS
BEGIN
	IF (trigger_nestlevel() < 2)  -- logic below will recurse unless this is used to prevent it
	BEGIN

		IF (UPDATE(SequenceNum))
		BEGIN
			SELECT i.DriverID, i.SequenceNum, ID = MAX(i2.ID)
			INTO #new
			FROM (
				SELECT DriverID, SequenceNum = min(SequenceNum)
				FROM inserted
				GROUP BY DriverID
			) i
			JOIN inserted i2 ON i2.DriverID = i.DriverID AND i2.SequenceNum = i.SequenceNum
			GROUP BY i.DriverID, i.SequenceNum
			
			DECLARE @DriverID int, @SequenceNum int, @ID int
			SELECT TOP 1 @DriverID = DriverID, @SequenceNum = SequenceNum, @ID = ID FROM #new 

			WHILE (@DriverID IS NOT NULL)
			BEGIN
				UPDATE tblOrder
					SET SequenceNum = N.NewSequenceNum
				FROM tblOrder o
				JOIN (
					SELECT ID, DriverID, NewSequenceNum = @SequenceNum + ROW_NUMBER() OVER (ORDER BY SequenceNum) 
					FROM tblOrder
					WHERE DriverID = @DriverID AND SequenceNum >= @SequenceNum AND ID <> @ID
						AND statusid in (2,7,8) and DeleteDateUTC is null
				) N ON N.ID = o.ID
				
				DELETE FROM #new WHERE ID = @ID
				SET @DriverID = NULL
				SELECT TOP 1 @DriverID = DriverID, @SequenceNum = SequenceNum, @ID = ID FROM #new
			END	
		END
	END
END
*/
GO

/****************************************************
 Date Created: 2016/02/24
 Author: Kevin Alons
 Purpose: return all currently eligible DriverApp OrderReadOnly records (for all Drivers) 
 Changes:
	-	3.13.13		2016/08/04		JAE		Add Sequence # for sorting orders
****************************************************/
ALTER VIEW dbo.viewOrderReadOnlyAllEligible_DriverApp AS
SELECT O.ID
	, O.OrderNum
	, O.StatusID
	, O.TicketTypeID
	, PriorityNum = CAST(P.PriorityNum AS INT) 
	, Product = PRO.Name
	, O.DueDate
	, Origin = OO.Name
	, OriginFull = OO.FullName
	, OO.OriginType
	, O.OriginUomID
	, OriginStation = OO.Station 
	, OriginLeaseNum = OO.LeaseNum 
	, OriginCounty = OO.County 
	, OriginLegalDescription = OO.LegalDescription 
	, OriginNDIC = OO.NDICFileNum
	, OriginNDM = OO.NDM
	, OriginCA = OO.CA
	, OriginState = OO.State
	, OriginAPI = OO.WellAPI 
	, OriginLat = OO.LAT 
	, OriginLon = OO.LON 
	, OriginGeoFenceRadiusMeters = OO.GeoFenceRadiusMeters 
	, Destination = D.Name
	, DestinationFull = D.FullName
	, DestType = D.DestinationType 
	, O.DestUomID
	, DestLat = D.LAT 
	, DestLon = D.LON 
	, DestGeoFenceRadiusMeters = D.GeoFenceRadiusMeters 
	, DestinationStation = D.Station 
	, O.CreateDateUTC
	, O.CreatedByUser
	, O.DeleteDateUTC 
	, O.DeletedByUser 
	, O.OriginID
	, O.DestinationID
	, PriorityID = CAST(O.PriorityID AS INT) 
	, Operator = OO.Operator
	, O.OperatorID
	, Pumper = OO.Pumper
	, O.PumperID
	, Producer = OO.Producer
	, O.ProducerID
	, Customer = C.Name
	, O.CustomerID
	, Carrier = CA.Name
	, O.CarrierID
	, O.ProductID
	, TicketType = OO.TicketType
	, EmergencyInfo = ISNULL(C.EmergencyInfo, 'For an emergency (spill, leak, fire, or accident) contact CHEMTREC @ 800.424.9300 (toll free)') 
	, DestTicketTypeID = D.TicketTypeID
	, DestTicketType = D.TicketType
	, O.OriginTankNum
	, O.OriginTankID
	, O.DispatchNotes
	, O.DispatchConfirmNum
	, RouteActualMiles = ISNULL(R.ActualMiles, 0)
	, CarrierAuthority = CA.Authority 
	, OriginTimeZone = OO.TimeZone
	, DestTimeZone = D.TimeZone
	, OCTM.OriginThresholdMinutes
	, OCTM.DestThresholdMinutes
	, ShipperHelpDeskPhone = C.HelpDeskPhone
	, OriginDrivingDirections = OO.DrivingDirections
	, DestDrivingDirections = D.DrivingDirections
	, O.AcceptLastChangeDateUTC
	, O.PickupLastChangeDateUTC
	, O.DeliverLastChangeDateUTC
    , CustomerLastChangeDateUTC = C.LastChangeDateUTC 
    , CarrierLastChangeDateUTC = CA.LastChangeDateUTC 
    , OriginLastChangeDateUTC = OO.LastChangeDateUTC 
    , DestLastChangeDateUTC = D.LastChangeDateUTC
    , RouteLastChangeDateUTC = R.LastChangeDateUTC
    , DriverID = O.DriverID
    , OriginDriver = OD.FullName
    , OriginTruckID = ISNULL(OTR.OriginTruckID, O.TruckID)
    , DestDriver = DD.FullName
	, O.SequenceNum
FROM dbo.tblOrder O
JOIN dbo.tblPriority P ON P.ID = O.PriorityID
JOIN dbo.viewOrigin OO ON OO.ID = O.OriginID
JOIN dbo.viewDestination D ON D.ID = O.DestinationID
JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
JOIN dbo.tblRoute R ON R.ID = O.RouteID
JOIN dbo.tblCarrier CA ON CA.ID = O.CarrierID
JOIN dbo.tblProduct PRO ON PRO.ID = O.ProductID		
LEFT JOIN dbo.tblOrderTransfer OTR ON OTR.OrderID = O.ID
LEFT JOIN dbo.viewDriver OD ON OD.ID = ISNULL(OTR.OriginDriverID, O.DriverID)
LEFT JOIN dbo.viewDriver DD ON DD.ID = O.DriverID
OUTER APPLY dbo.fnOrderBestMatchDriverAppPrintHeaderImage(O.ID) DAHI
OUTER APPLY dbo.fnOrderCombinedThresholdMinutes(O.ID) OCTM
WHERE StatusID IN (2,7,8,3) AND O.DeleteDateUTC IS NULL

GO

exec sp_refreshview viewOrderBase
exec sp_refreshview viewOrder
exec sp_refreshview viewOrderLocalDates
exec sp_refreshview viewOrderApprovalSource
exec sp_refreshview viewOrder_Financial_Carrier
go 
exec _spRecompileAllStoredProcedures
GO

COMMIT
SET NOEXEC OFF