/*
	-- more Report Center functionality (sub filtering based on user Profile data)
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.0.2'
SELECT  @NewVersion = '3.0.3'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

/***********************************/
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Order records with "translated friendly" values for FK relationships
/***********************************/
ALTER VIEW [dbo].[viewOrder] AS
SELECT O.*
	, OPS.OrderStatus AS PrintStatus
FROM (
	SELECT O.*
	, OrderDate = dbo.fnDateOnly(dbo.fnUTC_to_Local(O.OriginDepartTimeUTC, vO.TimeZoneID, vO.UseDST)) 
	, Origin = vO.Name 
	, OriginFull = vO.FullName 
	, OriginState = vO.State 
	, OriginStateAbbrev = vO.StateAbbrev 
	, OriginStation = vO.Station 
	, vO.LeaseName
	, vO.LeaseNum
	, OriginLegalDescription = vO.LegalDescription 
	, OriginNDIC = vO.NDICFileNum 
	, OriginNDM = vO.NDM 
	, OriginCA = vO.CA 
	, OriginTimeZoneID = vO.TimeZoneID 
	, OriginUseDST = vO.UseDST 
	, OriginStateID = vO.StateID
	, vO.H2S
	, Destination = vD.Name 
	, DestinationFull = vD.FullName 
	, DestinationState = vD.State 
	, DestinationStateAbbrev = vD.StateAbbrev 
	, DestinationTypeID = vD.ID
	, vD.DestinationType
	, DestStation = vD.Station 
	, DestTimeZoneID = vD.TimeZoneID 
	, DestUseDST = vD.UseDST 
	, DestStateID = vD.StateID
	, Customer = C.Name 
	, Carrier = CA.Name 
	, CarrierType = CT.Name 
	, OS.OrderStatus
	, OS.StatusNum
	, Driver = D.FullName 
	, DriverFirst = D.FirstName 
	, DriverLast = D.LastName 
	, Truck = TRU.FullName 
	, Trailer = TR1.FullName 
	, Trailer2 = TR2.FullName 
	, P.PriorityNum
	, TicketType = TT.Name 
	, DestTicketTypeID = vD.TicketTypeID 
	, DestTicketType = vD.TicketType 
	, Operator = OP.Name 
	, Producer = PR.Name 
	, Pumper = PU.FullName 
	, DriverNumber = D.IDNumber
	, CarrierNumber = CA.IDNumber
	, CarrierTypeID = CA.CarrierTypeID
	, TruckNumber = TRU.IDNumber 
	, TrailerNumber = TR1.IDNumber 
	, Trailer2Number = TR2.IDNumber 
	, Product = PRO.Name
	, ProductShort = PRO.ShortName 
	, ProductGroup = isnull(PRO.ProductGroup, PRO.ShortName)
	, OriginUOM = OUom.Name 
	, OriginUomShort = OUom.Abbrev 
	, OriginTankID_Text = CASE WHEN ORT.TankNum = '*' THEN '*' + isnull(O.OriginTankNum, '?') ELSE ORT.TankNum END 
	, DestUOM = DUom.Name 
	, DestUomShort = DUom.Abbrev 
	, Active = cast((CASE WHEN O.DeleteDateUTC IS NULL THEN 1 ELSE 0 END) as bit) 
	, IsDeleted = cast((CASE WHEN O.DeleteDateUTC IS NOT NULL THEN 1 ELSE 0 END) as bit) 
	, PickupPrintStatus = PPS.Name 
	, PickupCompleted = PPS.IsCompleted 
	, DeliverPrintStatus = DPS.Name 
	, DeliverCompleted = DPS.IsCompleted 
	, PrintStatusID = CASE WHEN StatusID = 8 AND PPS.IsCompleted = 0 THEN 7
						   WHEN StatusID = 3 AND DPS.IsCompleted = 0 THEN 8
						ELSE StatusID END 
	, OriginTankText = CASE WHEN O.OriginTankID IS NULL OR ORT.TankNum = '*' THEN O.OriginTankNum ELSE ORT.TankNum END
	FROM dbo.tblOrder O
	LEFT JOIN dbo.viewOrigin vO ON vO.ID = O.OriginID
	LEFT JOIN dbo.viewDestination vD ON vD.ID = O.DestinationID
	LEFT JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
	JOIN dbo.tblOrderStatus AS OS ON OS.ID = O.StatusID
	LEFT JOIN dbo.tblCarrier CA ON CA.ID = O.CarrierID
	LEFT JOIN dbo.tblCarrierType CT ON CT.ID = CA.CarrierTypeID
	LEFT JOIN dbo.viewDriver D ON D.ID = O.DriverID
	LEFT JOIN dbo.viewTruck TRU ON TRU.ID = O.TruckID
	LEFT JOIN dbo.viewTrailer TR1 ON TR1.ID = O.TrailerID
	LEFT JOIN dbo.viewTrailer TR2 ON TR2.ID = O.Trailer2ID
	LEFT JOIN dbo.tblPriority P ON P.ID = O.PriorityID
	LEFT JOIN dbo.tblTicketType TT ON TT.ID = O.TicketTypeID
	LEFT JOIN dbo.tblOriginTank ORT ON ORT.ID = O.OriginTankID
	LEFT JOIN dbo.tblOperator OP ON OP.ID = O.OperatorID
	LEFT JOIN dbo.viewPumper PU ON PU.ID = O.PumperID
	LEFT JOIN dbo.tblProducer PR ON PR.ID = O.ProducerID
	LEFT JOIN dbo.tblProduct PRO ON PRO.ID = O.ProductID
	LEFT JOIN dbo.tblUom OUom ON OUom.ID = O.OriginUomID
	LEFT JOIN dbo.tblUom DUom ON DUom.ID = O.DestUomID
	LEFT JOIN dbo.tblPrintStatus PPS ON PPS.ID = O.PickupPrintStatusID
	LEFT JOIN dbo.tblPrintStatus DPS ON DPS.ID = O.DeliverPrintStatusID
) O
LEFT JOIN dbo.tblOrderStatus OPS ON OPS.ID = O.PrintStatusID

GO

EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO

------------------------------------
-- MAKE tblReportColumnDefinition NOT use an IDENTIY PK
------------------------------------
GO

ALTER TABLE dbo.tblReportColumnDefinition
	DROP CONSTRAINT FK_ReportColumnDefinition_FilterType
GO
ALTER TABLE dbo.tblReportFilterType SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblReportColumnDefinition
	DROP CONSTRAINT FK_ReportColumnDefinition_ReportDefinition
GO
ALTER TABLE dbo.tblReportDefinition SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblReportColumnDefinition
	DROP CONSTRAINT DF_ReportColumnDefinition_FilterAllowCustomText
GO
CREATE TABLE dbo.Tmp_tblReportColumnDefinition
	(
	ID int NOT NULL,
	ReportID int NOT NULL,
	DataField varchar(50) NOT NULL,
	Caption varchar(50) NULL,
	DataFormat varchar(50) NULL,
	FilterDataField varchar(50) NULL,
	FilterTypeID int NOT NULL,
	FilterDropDownSql varchar(255) NULL,
	FilterAllowCustomText bit NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_tblReportColumnDefinition SET (LOCK_ESCALATION = TABLE)
GO
GRANT SELECT ON dbo.Tmp_tblReportColumnDefinition TO dispatchcrude_iis_acct  AS dbo
GO
ALTER TABLE dbo.Tmp_tblReportColumnDefinition ADD CONSTRAINT
	DF_ReportColumnDefinition_FilterAllowCustomText DEFAULT ((0)) FOR FilterAllowCustomText
GO
IF EXISTS(SELECT * FROM dbo.tblReportColumnDefinition)
	 EXEC('INSERT INTO dbo.Tmp_tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText)
		SELECT ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText FROM dbo.tblReportColumnDefinition WITH (HOLDLOCK TABLOCKX)')
GO
ALTER TABLE dbo.tblReportColumnDefinitionBaseFilter
	DROP CONSTRAINT FK_ReportColumnDefinitionBaseFilter_ReportColumn
GO
ALTER TABLE dbo.tblUserReportColumnDefinition
	DROP CONSTRAINT FK_UserReportColumnDefinition_ReportColumn
GO
DROP TABLE dbo.tblReportColumnDefinition
GO
EXECUTE sp_rename N'dbo.Tmp_tblReportColumnDefinition', N'tblReportColumnDefinition', 'OBJECT' 
GO
ALTER TABLE dbo.tblReportColumnDefinition ADD CONSTRAINT
	PK_ReportColumnDefinition PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE UNIQUE NONCLUSTERED INDEX udxReportColumnDefinition_Report_DataField ON dbo.tblReportColumnDefinition
	(
	ReportID,
	DataField
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.tblReportColumnDefinition ADD CONSTRAINT
	FK_ReportColumnDefinition_ReportDefinition FOREIGN KEY
	(
	ReportID
	) REFERENCES dbo.tblReportDefinition
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblReportColumnDefinition ADD CONSTRAINT
	FK_ReportColumnDefinition_FilterType FOREIGN KEY
	(
	FilterTypeID
	) REFERENCES dbo.tblReportFilterType
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblUserReportColumnDefinition ADD CONSTRAINT
	FK_UserReportColumnDefinition_ReportColumn FOREIGN KEY
	(
	ReportColumnID
	) REFERENCES dbo.tblReportColumnDefinition
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblUserReportColumnDefinition SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblReportColumnDefinitionBaseFilter ADD CONSTRAINT
	FK_ReportColumnDefinitionBaseFilter_ReportColumn FOREIGN KEY
	(
	ReportColumnID
	) REFERENCES dbo.tblReportColumnDefinition
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblReportColumnDefinitionBaseFilter SET (LOCK_ESCALATION = TABLE)
GO

INSERT INTO tblReportFilterType (ID, Name, FilterOPeratorID_CSV)
	SELECT 0, 'DateTime [No filtering]', '1'
	
------------------------------------
-- additional tblReportColumnDefinition data
------------------------------------
GO

UPDATE tblReportColumnDefinition set FilterDropDownSql = 'SELECT ID, Name=OrderStatus FROM tblOrderStatus ORDER BY StatusNum' WHERE ID = 12
GO

INSERT INTO tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterAllowCustomText, FilterDropDownSql)
	--	 id, reportid, datafield			, caption			, dataformat, fdf				, ft ac sql
		  SELECT 13, 1, 'Priority'			, 'Priority'		, NULL		, 'PriorityID'		, 2, 0, 'SELECT ID, Name FROM tblPriority ORDER BY Name'
	UNION SELECT 14, 1, 'DueDate'			, 'Due Date'		, NULL		, NULL				, 6, 1, NULL
	UNION SELECT 15, 1, 'OriginArriveTime'	, 'Origin Arrival'	, NULL		, NULL				, 0, 1, NULL
	UNION SELECT 16, 1, 'OriginDepartTime'	, 'Origin Departure', NULL		, NULL				, 0, 1, NULL
	UNION SELECT 17, 1, 'OriginMinutes'		, 'Origin Minutes'	, NULL		, NULL				, 4, 1, NULL
	UNION SELECT 18, 1, 'OriginWaitNotes'	, 'Origin Wait Notes', NULL		, NULL				, 1, 1, NULL
	UNION SELECT 19, 1, 'OriginBOLNum'		, 'Origin BOL #'	, NULL		, NULL				, 1, 1, NULL
	UNION SELECT 20, 1, 'DestArriveTime'	, 'Dest Arrival'	, NULL		, NULL				, 0, 1, NULL
	UNION SELECT 21, 1, 'DestDepartTime'	, 'Dest Departure'	, NULL		, NULL				, 0, 1, NULL
	UNION SELECT 22, 1, 'DestMinutes'		, 'Dest Minutes'	, NULL		, NULL				, 4, 1, NULL
	UNION SELECT 23, 1, 'DestWaitNotes'		, 'Dest Wait Notes'	, NULL		, NULL				, 1, 1, NULL
	UNION SELECT 24, 1, 'DestBOLNum'		, 'Dest BOL #'		, NULL		, NULL				, 1, 1, NULL
	UNION SELECT 25, 1, 'DestGrossUnits'	, 'Dest GOV'		, NULL		, NULL				, 4, 1, NULL
	UNION SELECT 26, 1, 'DestNetUnits'		, 'Dest GSV'		, NULL		, NULL				, 4, 1, NULL
	UNION SELECT 27, 1, 'Driver'			, 'Driver'			, NULL		, 'DriverID'		, 2, 0, 'SELECT ID, Name = FullName FROM viewDriver ORDER BY FullName'
	UNION SELECT 28, 1, 'Truck'				, 'Truck'			, NULL		, 'TruckID'			, 2, 0, 'SELECT ID, Name = FullName FROM viewTruck ORDER BY FullName'
	UNION SELECT 29, 1, 'Trailer'			, 'Trailer 1'		, NULL		, 'TrailerID'		, 2, 0, 'SELECT ID, Name = FullName FROM viewTrailer ORDER BY FullName'
	UNION SELECT 30, 1, 'Trailer2'			, 'Trailer 2'		, NULL		, 'Trailer2ID'		, 2, 0, 'SELECT ID, Name = FullName FROM viewTrailer ORDER BY FullName'
	UNION SELECT 31, 1, 'Operator'			, 'Operator'		, NULL		, 'OperatorID'		, 2, 0, 'SELECT ID, Name FROM tblOperator ORDER BY Name'
	UNION SELECT 32, 1, 'Pumper'			, 'Pumper'			, NULL		, 'PumperID'		, 2, 0, 'SELECT ID, Name FROM tblPumper ORDER BY Name'
	UNION SELECT 33, 1, 'TicketType'		, 'Order Type'		, NULL		, 'TicketTypeID'	, 2, 0, 'SELECT ID, Name FROM tblTicketType ORDER BY Name'
	UNION SELECT 34, 1, 'RejectNotes'		, 'Reject Notes'	, NULL		, NULL				, 1, 1, NULL
	UNION SELECT 35, 1, 'ChainUp'			, 'Chain-Up'		, NULL		, NULL				, 5, 0, 'SELECT ID=1, Name=''Yes'' UNION SELECT 2, ''No'''
	UNION SELECT 36, 1, 'OriginTruckMileage', 'Origin Truck Mileage', NULL	, NULL				, 4, 1, NULL
	UNION SELECT 37, 1, 'OriginTankText'	, 'Origin Tank #'	, NULL		, NULL				, 1, 1, NULL
	UNION SELECT 38, 1, 'DestTruckMileage'	, 'Dest Truck Mileage', NULL	, NULL				, 4, 1, NULL
	UNION SELECT 39, 1, 'CarrierTicketNum'	, 'Origin Tank #'	, NULL		, NULL				, 1, 1, NULL
	UNION SELECT 40, 1, 'AuditNotes'		, 'Audit Notes'		, NULL		, NULL				, 1, 1, NULL
	UNION SELECT 41, 1, 'ActualMiles'		, 'Actual Miles'	, NULL		, NULL				, 4, 1, NULL
	UNION SELECT 42, 1, 'Producer'			, 'Producer'		, NULL		, 'ProducerID'		, 2, 0, 'SELECT ID, Name FROM tblProducer ORDER BY Name'
	UNION SELECT 43, 1, 'DestProductBSW'	, 'Dest BS&W'		, NULL		, NULL				, 4, 1, NULL
	UNION SELECT 44, 1, 'DestProductGravity', 'Dest Gravity'	, NULL		, NULL				, 4, 1, NULL
	UNION SELECT 45, 1, 'DestProductTemp'	, 'Dest Temp'		, NULL		, NULL				, 4, 1, NULL
	UNION SELECT 46, 1, 'DestOpenMeterUnits', 'Dest Open Meters', NULL		, NULL				, 4, 1, NULL
	UNION SELECT 47, 1, 'DestCloseMeterUnits', 'Dest Close Meters', NULL	, NULL				, 4, 1, NULL
	UNION SELECT 48, 1, 'OriginUom'			, 'Origin UOM'		, NULL		, 'OriginUOMID'		, 2, 0, 'SELECT ID, Name FROM tblUOM ORDER BY Name'
	UNION SELECT 49, 1, 'DestUom'			, 'Dest UOM'		, NULL		, 'DestUOMID'		, 2, 0, 'SELECT ID, Name FROM tblUOM ORDER BY Name'
	UNION SELECT 50, 1, 'DispatchNotes'		, 'Dispatch Notes'	, NULL		, NULL				, 1, 1, NULL
	UNION SELECT 51, 1, 'DriverNotes'		, 'Driver Notes'	, NULL		, NULL				, 1, 1, NULL
	UNION SELECT 52, 1, 'DispatchConfirmNum', 'Dispatch Confirm #', NULL		, NULL			, 1, 1, NULL
	UNION SELECT 53, 1, 'OriginState'		, 'Origin State'	, NULL		, 'OriginStateID'	, 2, 0, 'SELECT ID, Name=FullName FROM tblState ORDER BY FullName'
	UNION SELECT 54, 1, 'OriginStateAbbrev'	, 'Origin ST'		, NULL		, 'OriginStateID'	, 2, 0, 'SELECT ID, Name=Abbreviation FROM tblState ORDER BY Abbreviation'
	UNION SELECT 55, 1, 'OriginStation'		, 'Origin Station'	, NULL		, NULL				, 1, 1, NULL
	UNION SELECT 56, 1, 'LeaseName'			, 'Origin Lease Name', NULL		, NULL				, 1, 1, NULL
	UNION SELECT 57, 1, 'LeaseNum'			, 'Origin Lease #', NULL		, NULL				, 1, 1, NULL
	UNION SELECT 58, 1, 'OriginLegalDescription', 'Origin Legal Desc', NULL		, NULL			, 1, 1, NULL
	UNION SELECT 59, 1, 'OriginNDIC'		, 'Origin NDIC'		, NULL		, NULL				, 1, 1, NULL
	UNION SELECT 60, 1, 'OriginNDM'			, 'Origin NDM'		, NULL		, NULL				, 1, 1, NULL
	UNION SELECT 61, 1, 'OriginCA'			, 'Origin CA'		, NULL		, NULL				, 1, 1, NULL
	UNION SELECT 62, 1, 'OriginTimeZone'	, 'Origin TZ'		, NULL		, 'OriginTimeZoneID', 2, 0, 'SELECT ID, Name FROM tblTimeZone ORDER BY StandardOffsetHours DESC'
	UNION SELECT 63, 1, 'OriginUseDST'		, 'Origin Use DST'	, NULL		, NULL				, 5, 0, 'SELECT ID=1, Name=''Yes'' UNION SELECT 2, ''No'''
	UNION SELECT 64, 1, 'H2S'				, 'H2S'				, NULL		, NULL				, 5, 0, 'SELECT ID=1, Name=''Yes'' UNION SELECT 2, ''No'''
	UNION SELECT 65, 1, 'DestinationState'	, 'Dest State'		, NULL		, 'DestStateID'		, 2, 0, 'SELECT ID, Name=FullName FROM tblState ORDER BY FullName'
	UNION SELECT 66, 1, 'DestinationStateAbbrev', 'Destination ST', NULL	, 'DestStateID'		, 2, 0, 'SELECT ID, Name=Abbreviation FROM tblState ORDER BY Abbreviation'
	UNION SELECT 67, 1, 'DestStation'		, 'Dest Station'	, NULL		, NULL				, 1, 1, NULL
	UNION SELECT 68, 1, 'DestTimeZone'		, 'Dest TZ'			, NULL		, 'DestTimeZoneID'	, 2, 0, 'SELECT ID, Name FROM tblTimeZone ORDER BY StandardOffsetHours DESC'
	UNION SELECT 69, 1, 'DestUseDST'		, 'Dest Use DST'	, NULL		, NULL				, 5, 0, 'SELECT ID=1, Name=''Yes'' UNION SELECT 2, ''No'''
	UNION SELECT 70, 1, 'CarrierType'		, 'Carrier Type'	, NULL		, 'CarrierTypeID'	, 2, 0, 'SELECT ID, Name FROM tblCarrierType ORDER BY Name'
	UNION SELECT 71, 1, 'DriverFirst'		, 'Driver First'	, NULL		, NULL				, 1, 1, NULL
	UNION SELECT 72, 1, 'DriverLast'		, 'Driver Last'		, NULL		, NULL				, 1, 1, NULL
	UNION SELECT 73, 1, 'DestTicketType'	, 'Dest Ticket Type', NULL		, 'DestTicketTypeID', 2, 0, 'SELECT ID, Name FROM tblDestTicketType ORDER BY Name'
	UNION SELECT 74, 1, 'DriverNumber'		, 'Driver #'		, NULL		, NULL				, 1, 1, NULL
	UNION SELECT 75, 1, 'CarrierNumber'		, 'Carrier #'		, NULL		, NULL				, 1, 1, NULL
	UNION SELECT 76, 1, 'ProductGroup'		, 'Product Group'	, NULL		, NULL				, 1, 1, NULL
	UNION SELECT 77, 1, 'OriginUomShort'	, 'Origin UOM Short', NULL		, 'OriginUOMID'		, 2, 0, 'SELECT ID, Name=Abbrev FROM tblUOM ORDER BY Abbrev'
	UNION SELECT 78, 1, 'DestUomShort'		, 'Dest UOM Short'	, NULL		, 'DestUOMID'		, 2, 0, 'SELECT ID, Name=Abbrev FROM tblUOM ORDER BY Abbrev'
	UNION SELECT 79, 1, 'TransitMinutes'	, 'Transit Min'		, NULL		, NULL				, 4, 1, NULL
	UNION SELECT 80, 1, 'TicketCount'		, 'Ticket Count'	, NULL		, NULL				, 4, 1, NULL
	UNION SELECT 81, 1, 'RerouteCount'		, 'Reroute Count'	, NULL		, NULL				, 4, 1, NULL
	UNION SELECT 82, 1, 'TotalMinutes'		, 'Total Minutes'	, NULL		, NULL				, 4, 1, NULL
	UNION SELECT 83, 1, 'TotalWaitMinutes'	, 'Total Wait Minutes', NULL	, NULL				, 4, 1, NULL
	UNION SELECT 84, 1, 'T_TicketType'		, 'Ticket Type'		, NULL		, NULL				, 1, 1, NULL
	UNION SELECT 85, 1, 'T_CarrierTicketNum', 'Carrier Ticket #', NULL		, NULL				, 1, 1, NULL
	UNION SELECT 86, 1, 'T_BOLNum'			, 'Ticket BOL #'	, NULL		, NULL				, 1, 1, NULL
	UNION SELECT 87, 1, 'T_TankNum'			, 'Ticket Tank #'	, NULL		, NULL				, 1, 1, NULL
	UNION SELECT 88, 1, 'T_IsStrappedTank'	, 'Ticket - Strapped?', NULL	, NULL				, 5, 0, 'SELECT ID=1, Name=''Yes'' UNION SELECT 2, ''No'''
	UNION SELECT 89, 1, 'T_BottomFeet'		, 'Ticket Bottom FT', NULL		, NULL				, 4, 1, NULL
	UNION SELECT 90, 1, 'T_BottomInches'	, 'Ticket Bottom IN', NULL		, NULL				, 4, 1, NULL
	UNION SELECT 91, 1, 'T_BottomQ'			, 'Ticket Bottom Q'	, NULL		, NULL				, 4, 1, NULL
	UNION SELECT 92, 1, 'T_OpeningGaugeFeet', 'Ticket Open FT'	, NULL		, NULL				, 4, 1, NULL
	UNION SELECT 93, 1, 'T_OpeningGaugeInches', 'Ticket Open IN', NULL		, NULL				, 4, 1, NULL
	UNION SELECT 94, 1, 'T_OpeningGaugeQ'	, 'Ticket Open Q'	, NULL		, NULL				, 4, 1, NULL
	UNION SELECT 95, 1, 'T_ClosingGaugeFeet', 'Ticket Close FT'	, NULL		, NULL				, 4, 1, NULL
	UNION SELECT 96, 1, 'T_ClosingGaugeInches', 'Ticket Close IN', NULL		, NULL				, 4, 1, NULL
	UNION SELECT 97, 1, 'T_ClosingGaugeQ'	, 'Ticket Close Q'	, NULL		, NULL				, 4, 1, NULL
	UNION SELECT 98, 1, 'T_OpenReading'		, 'Ticket Open Reading', NULL	, NULL				, 1, 1, NULL
	UNION SELECT 99, 1, 'T_CloseReading'	, 'Ticket Close Reading', NULL	, NULL				, 1, 1, NULL
	UNION SELECT 100, 1, 'T_CorrectedAPIGravity'	, 'Ticket Corr. API Gravity', NULL	, NULL		, 4, 1, NULL
	UNION SELECT 101, 1, 'T_GrossStdUnits'	, 'Ticket GSV'		, NULL		, NULL				, 4, 1, NULL
	UNION SELECT 102, 1, 'T_GrossUnits'		, 'Ticket GOV'		, NULL		, NULL				, 4, 1, NULL
	UNION SELECT 103, 1, 'T_NetUnits'		, 'Ticket NSV'		, NULL		, NULL				, 4, 1, NULL
	UNION SELECT 104, 1, 'T_SealOff'		, 'Ticket Seal Off'	, NULL		, NULL				, 1, 1, NULL
	UNION SELECT 105, 1, 'T_SealOn'			, 'Ticket Seal On'	, NULL		, NULL				, 1, 1, NULL
	UNION SELECT 106, 1, 'T_ProductObsTemp'	, 'Ticket Obs Temp'	, NULL		, NULL				, 4, 1, NULL
	UNION SELECT 107, 1, 'T_ProductObsGravity', 'Ticket Obs Gravity', NULL	, NULL				, 4, 1, NULL
	UNION SELECT 108, 1, 'T_ProductBSW'		, 'Ticket BS&W'		, NULL		, NULL				, 4, 1, NULL
	UNION SELECT 109, 1, 'T_Rejected'		, 'Ticket Rejected'	, NULL		, NULL				, 5, 0, 'SELECT ID=1, Name=''Yes'' UNION SELECT 2, ''No'''
	UNION SELECT 110, 1, 'T_RejectNotes'	, 'Ticket Reject Notes'	, NULL		, NULL				, 1, 1, NULL
	UNION SELECT 111, 1, 'PreviousDestinations'	, 'Prev Destinations', NULL		, NULL				, 1, 1, NULL
	UNION SELECT 112, 1, 'RerouteUsers'	, 'Reroute Users'		, NULL		, NULL				, 1, 1, NULL
	UNION SELECT 113, 1, 'RerouteNotes'	, 'Reroute Notes'		, NULL		, NULL				, 1, 1, NULL

GO

------------------------------------
-- NEW tblReportColumnDefinitionBaseFilter records
------------------------------------
INSERT INTO tblReportColumnDefinitionBaseFilter (ReportColumnID, BaseFilterTypeID, ProfileField, IncludeWhereClause_ProfileTemplate, IncludeWhereClause)
		  SELECT 1, 2, 'CarrierID', '@CarrierID', '(@CarrierID = -1 OR ID = @CarrierID)'
	UNION SELECT 1, 2, 'CustomerID', '@CustomerID', '(@CustomerID = -1 OR ID IN (SELECT DISTINCT CarrierID FROM tblOrder WHERE CustomerID = @CustomerID))'
	UNION SELECT 2, 2, 'CarrierID', '@CarrierID', '(@CarrierID = -1 OR ID IN (SELECT DISTINCT CustomerID FROM tblOrder WHERE CarrierID = @CarrierID))'
	UNION SELECT 2, 2, 'CustomerID', '@CustomerID', '(@CustomerID = -1 OR ID = @CustomerID)'
	UNION SELECT 3, 2, 'CarrierID', '@CarrierID', '(@CarrierID = -1 OR ID IN (SELECT DISTINCT DestinationID FROM tblOrder WHERE CarrierID = @CarrierID))'
	UNION SELECT 3, 2, 'CustomerID', '@CustomerID', '(@CustomerID = -1 OR ID IN (SELECT DestinationID FROM tblDestinationCustomers WHERE CustomerID = @CustomerID UNION SELECT DISTINCT DestinationID FROM tblOrder WHERE CustomerID = @CustomerID))'
	UNION SELECT 6, 2, 'CarrierID', '@CarrierID', '(@CarrierID = -1 OR ID IN (SELECT DISTINCT OriginID FROM tblOrder WHERE CarrierID = @CarrierID))'
	UNION SELECT 6, 2, 'CustomerID', '@CustomerID', '(@CustomerID = -1 OR ID IN (SELECT ID FROM tblOrigin WHERE CustomerID = @CustomerID UNION SELECT DISTINCT OriginID FROM tblOrder WHERE CustomerID = @CustomerID))'
	UNION SELECT 10, 2, 'CarrierID', '@CarrierID', '(@CarrierID = -1 OR ID IN (SELECT DISTINCT ProductID FROM tblOrder WHERE CarrierID = @CarrierID))'
	UNION SELECT 10, 2, 'CustomerID', '@CustomerID', '(@CustomerID = -1 OR ID IN (SELECT ProductID FROM tblOrigin O JOIN tblOriginProducts OP ON OP.OriginID = O.ID WHERE O.CustomerID = @CustomerID UNION SELECT DISTINCT ProductID FROM tblOrder WHERE CustomerID = @CustomerID))'
	UNION SELECT 31, 2, 'CarrierID', '@CarrierID', '(@CarrierID = -1 OR ID IN (SELECT DISTINCT OperatorID FROM tblOrder WHERE CarrierID = @CarrierID))'
	UNION SELECT 31, 2, 'CustomerID', '@CustomerID', '(@CustomerID = -1 OR ID IN (SELECT OperatorID FROM tblOrigin WHERE CustomerID = @CustomerID UNION SELECT DISTINCT OperatorID FROM tblOrder WHERE CustomerID = @CustomerID))'
	UNION SELECT 32, 2, 'CarrierID', '@CarrierID', '(@CarrierID = -1 OR ID IN (SELECT DISTINCT PumperID FROM tblOrder WHERE CarrierID = @CarrierID))'
	UNION SELECT 32, 2, 'CustomerID', '@CustomerID', '(@CustomerID = -1 OR ID IN (SELECT PumperID FROM tblOrigin WHERE CustomerID = @CustomerID UNION SELECT DISTINCT PumperID FROM tblOrder WHERE CustomerID = @CustomerID))'
	UNION SELECT 42, 2, 'CarrierID', '@CarrierID', '(@CarrierID = -1 OR ID IN (SELECT DISTINCT ProducerID FROM tblOrder WHERE CarrierID = @CarrierID))'
	UNION SELECT 42, 2, 'CustomerID', '@CustomerID', '(@CustomerID = -1 OR ID IN (SELECT ProducerID FROM tblOrigin WHERE CustomerID = @CustomerID UNION SELECT DISTINCT ProducerID FROM tblOrder WHERE CustomerID = @CustomerID))'
GO

------------------------------------
-- REVISED TRIGGER
------------------------------------
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 23 Jul 2014
-- Description:	trigger to advance any duplicated SortNums in a UserReport to make reordering easier
-- =============================================
ALTER TRIGGER [dbo].[trigUserReportColumnDefinition_IU_SortNum] ON [dbo].[tblUserReportColumnDefinition] FOR INSERT, UPDATE AS
BEGIN
	IF (trigger_nestlevel() < 2)  -- logic below will recurse unless this is used to prevent it
	BEGIN
		IF (UPDATE(SortNum))
		BEGIN
			SELECT i.UserReportID, i.SortNum, ID = MAX(i2.ID)
			INTO #new
			FROM (
				SELECT UserReportID, SortNum = min(SortNum)
				FROM inserted
				GROUP BY UserReportID
			) i
			JOIN inserted i2 ON i2.UserReportID = i.UserReportID AND i2.SortNum = i.SortNum
			GROUP BY i.UserReportID, i.SortNum
			
			DECLARE @UserReportID int, @SortNum int, @ID int
			SELECT TOP 1 @UserReportID = UserReportID, @SortNum = SortNum, @ID = ID FROM #new 

			WHILE (@UserReportID IS NOT NULL)
			BEGIN
				UPDATE tblUserReportColumnDefinition 
					SET SortNum = N.NewSortNum
				FROM tblUserReportColumnDefinition URCD
				JOIN (
					SELECT ID, UserReportID, NewSortNum = @SortNum + ROW_NUMBER() OVER (ORDER BY SortNum) 
					FROM tblUserReportColumnDefinition
					WHERE UserReportID = @UserReportID AND SortNum >= @SortNum AND ID <> @ID
				) N ON N.ID = URCD.ID
				
				DELETE FROM #new WHERE ID = @ID
				SET @UserReportID = NULL
				SELECT TOP 1 @UserReportID = UserReportID, @SortNum = SortNum, @ID = ID FROM #new
			END	
		END
	END
END

GO

------------------------------------
-- NEW TABLES
------------------------------------
CREATE TABLE tblReportBaseFilterType
(
  ID int NOT NULL CONSTRAINT PK_ReportBaseFilterType PRIMARY KEY
, Name varchar(25) NOT NULL
)
GO
CREATE UNIQUE INDEX udxReportBaseFilterType_Name ON tblReportBaseFilterType(Name)
GO
GRANT SELECT ON tblReportBaseFilterType TO dispatchcrude_iis_acct
GO
INSERT INTO tblReportBaseFilterType (ID, Name)
	SELECT 1, 'None'
	UNION SELECT 2, 'Profile Value'
GO

CREATE TABLE [dbo].[tblReportColumnDefinitionBaseFilter](
	[ID] [int] IDENTITY(1,1) NOT NULL CONSTRAINT PK_ReportColumnDefinitionBaseFilter PRIMARY KEY
	, [ReportColumnID] [int] NOT NULL CONSTRAINT FK_ReportColumnDefinitionBaseFilter_ReportColumn FOREIGN KEY REFERENCES tblReportColumnDefinition(ID)
	, [BaseFilterTypeID] [int] NOT NULL CONSTRAINt FK_ReportColumnDefinitionBaseFilter_BaseFilterType FOREIGN KEY REFERENCES tblReportBaseFilterType(ID)
	, [ProfileField] [varchar](100) NULL
	, [IncludeWhereClause] [varchar](max) NULL
	, [IncludeWhereClause_ProfileTemplate] [varchar](100) NULL
 )
GO
GRANT SELECT ON tblReportColumnDefinitionBaseFilter TO dispatchcrude_iis_acct
GO

ALTER VIEW [dbo].[viewReportColumnDefinition] AS
	SELECT RCD.ID
		, RCD.ReportID
		, RCD.DataField
		, Caption = ISNULL(RCD.Caption, RCD.DataField)
		, RCD.DataFormat
		, RCD.FilterDataField
		, RCD.FilterTypeID
		, RCD.FilterDropDownSql
		, RCD.FilterAllowCustomText
		, RFT.FilterOperatorID_CSV
		, BaseFilterCount = (SELECT COUNT(*) FROM tblReportColumnDefinitionBaseFilter WHERE ReportColumnID = RCD.ID)
		, FilterOperatorID_CSV_Delim = ',' + RFT.FilterOperatorID_CSV + ','
	FROM tblReportColumnDefinition RCD
	JOIN tblReportFilterType RFT ON RFT.ID = RCD.FilterTypeID

GO

COMMIT
SET NOEXEC OFF

EXEC _spRefreshAllViews
GO