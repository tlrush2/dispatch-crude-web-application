SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.3.0.1'
SELECT  @NewVersion = '4.3.0.2'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'Settlement - improved Session logic to mitigate overlapping settlement session contention issues'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

create table tblOrderSettlementSessionCarrier
(
  ID int not null identity(1, 1) constraint PK_OrderSettlementSessionCarrier primary key
, UserName varchar(100) not null
, CreateDateUTC datetime not null constraint DF_OrderSettlementSessionCarrier_CreateDateUTC default (getutcdate())
)
go
select * into #ss from tblOrderSettlementSelectionCarrier 
drop table tblOrderSettlementSelectionCarrier
create table tblOrderSettlementSelectionCarrier
(
  SessionID int not null constraint FK_OrderSettlementSelectionCarrier_SessionID foreign key references tblOrderSettlementSessionCarrier(ID) on delete cascade
, OrderID int not null constraint FK_OrderSettlementSelectionCarrier_OrderID foreign key references tblOrder(ID) on delete cascade
, RateApplySel bit not null constraint DF_OrderSettlementSelectionCarrier_RateApplySel default (1)
, BatchSel bit not null constraint DF_OrderSettlementSelectionCarrier_BatchSel default (1)
, constraint PK_OrderSettlementSelectionCarrier primary key (SessionID, OrderID)
)
set identity_insert tblOrderSettlementSessionCarrier on
insert into tblOrderSettlementSessionCarrier (id, username)
	select distinct cast(SessionID as int), 'System' from #ss
set identity_insert tblOrderSettlementSessionCarrier off
insert into tblOrderSettlementSelectionCarrier
	select cast(SessionID as int), OrderID, RateApplySel, BatchSel from #ss
drop table #ss
go

create table tblOrderSettlementSessionDriver
(
  ID int not null identity(1, 1) constraint PK_OrderSettlementSessionDriver primary key
, UserName varchar(100) not null
, CreateDateUTC datetime not null constraint DF_OrderSettlementSessionDriver_CreateDateUTC default (getutcdate())
)
go
select * into #ss from tblOrderSettlementSelectionDriver 
drop table tblOrderSettlementSelectionDriver
create table tblOrderSettlementSelectionDriver
(
  SessionID int not null constraint FK_OrderSettlementSelectionDriver_SessionID foreign key references tblOrderSettlementSessionDriver(ID) on delete cascade
, OrderID int not null constraint FK_OrderSettlementSelectionDriver_OrderID foreign key references tblOrder(ID) on delete cascade
, RateApplySel bit not null constraint DF_OrderSettlementSelectionDriver_RateApplySel default (1)
, BatchSel bit not null constraint DF_OrderSettlementSelectionDriver_BatchSel default (1)
, constraint PK_OrderSettlementSelectionDriver primary key (SessionID, OrderID)
)
set identity_insert tblOrderSettlementSessionDriver on
insert into tblOrderSettlementSessionDriver (id, username)
	select distinct cast(SessionID as int), 'System' from #ss
set identity_insert tblOrderSettlementSessionDriver off
insert into tblOrderSettlementSelectionDriver
	select cast(SessionID as int), OrderID, RateApplySel, BatchSel from #ss
drop table #ss
go

create table tblOrderSettlementSessionShipper
(
  ID int not null identity(1, 1) constraint PK_OrderSettlementSessionShipper primary key
, UserName varchar(100) not null
, CreateDateUTC datetime not null constraint DF_OrderSettlementSessionShipper_CreateDateUTC default (getutcdate())
)
go
select * into #ss from tblOrderSettlementSelectionShipper 
drop table tblOrderSettlementSelectionShipper
create table tblOrderSettlementSelectionShipper
(
  SessionID int not null constraint FK_OrderSettlementSelectionShipper_SessionID foreign key references tblOrderSettlementSessionShipper(ID) on delete cascade
, OrderID int not null constraint FK_OrderSettlementSelectionShipper_OrderID foreign key references tblOrder(ID) on delete cascade
, RateApplySel bit not null constraint DF_OrderSettlementSelectionShipper_RateApplySel default (1)
, BatchSel bit not null constraint DF_OrderSettlementSelectionShipper_BatchSel default (1)
, constraint PK_OrderSettlementSelectionShipper primary key (SessionID, OrderID)
)
set identity_insert tblOrderSettlementSessionShipper on
insert into tblOrderSettlementSessionShipper (id, username)
	select distinct cast(SessionID as int), 'System' from #ss
set identity_insert tblOrderSettlementSessionShipper off
insert into tblOrderSettlementSelectionShipper
	select cast(SessionID as int), OrderID, RateApplySel, BatchSel from #ss
drop table #ss
go

/**********************************/
-- Created: 2013/03/09 - ?.?.? - Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
-- Changes:
-- 3.7.31	- 2015/06/19 - KDA	- reorder input parameters (move ProviderID down) and add @DriverGroupID parameter
-- 3.9.0	- 2/15/08/14 - KDA	- return Approved column
-- 3.9.19.6	- 2015/09/30 - KDA	- add OverrideXXX fields from OrderApproval table
-- 3.9.21	- 2015/10/03 - KDA	- use [viewOrder] O.OriginDriverGroupID instead of O.DriverGroupID (which could be DestDriver.DriverGroupID)
-- 3.9.34	- 2015/12/16 - BB	- Remove deleted orders from the results (DCWEB-851)
-- 3.10.13.6 - 2015/03/01 - JAE	- add TruckTypeID
-- 3.11.3.1	- 2016/03/22 - KDA	- fix to no-op when @BatchID = 0
-- 3.11.19	- 2016/05/02 - BB	- Add Job number and Contract number
-- 3.11.20	- 2016/05/04 - JAE	- 3 month limit was always being applied, skip if batch is provided
-- 3.11.20.1 - 2016/05/04 - JAE	- Undoing change as timeouts reoccurring, need to investigate
-- 3.11.20.3 - 2016/05/11 - KDA	- optimize @BatchID filtering to query directly from tblOrderSettlementCarrier table
--								- use tblOrder and minimal JOINs intead of expensive viewOrder
-- 4.1.0	- 2016/08/08 - KDA	- use new @StartSession & @SessionID parameters to persist the retrieved records as a "Session"
--								- stop defaulting @StartDate to 3 months prior if not supplied (null parameter value provided)
--								- use simplified viewOrder_Financial_Carrier for return results - which is now again optimized for "reasonable" performance
-- 4.1.3.5	- 2016/09/17 - BB	- DCWEB-1770: Add reject filter
-- 4.1.8.6	- 2016.09.21 - KDA	- remove dynamic sql and retrieve data in 2 stages (like before) getting the orderID first, then the final data (expensive query)
--								- use new fnTrimToNull() to ensure optional string parameters are trimmed to NULL
-- 4.1.9.3	- 2026.09.28 - KDA	- ensure @StartSession = 0 when @BatchID is not null
-- 4.1.25.3	- 2016.10.28 - KDA	- ensure we don't reuse a sessionID when generating a new session
-- 4.3.0.2	- 2016.11.11 - KDA	- prevent including the same order multiple times (error in LEFT JOIN)
--								- don't remove existing session on overlapping creation, only "purge" after 1 week (obsolete)
/***********************************/
ALTER PROCEDURE spRetrieveOrdersFinancialCarrier
(
  @StartDate date = NULL -- will default to first day of 3rd prior month for efficiency sake
, @EndDate date = NULL
, @ShipperID int = -1 -- all
, @CarrierID int = -1 -- all
, @ProductGroupID int = -1 -- all 
, @TruckTypeID int = -1 -- all
, @DriverGroupID int = -1 -- all
, @OriginStateID int = -1 -- all 
, @DestStateID int = -1 -- all
, @ProducerID int = -1 -- all
, @BatchID int = NULL -- either show unbatched (NULL), or the specified batch orders
, @OnlyShipperSettled bit = 0 
, @JobNumber varchar(20) = NULL -- 3.11.19
, @ContractNumber varchar(20) = NULL -- 3.11.19
, @Rejected int = -1 -- all (rejected + not rejected) -- 4.1.3.5
, @UserName varchar(100)
, @StartSession bit = 1 -- when set to 1, assign the retrieved records to a new Session (ID), when 0 and a sessionID was assigned, then retrieve it
, @SessionID varchar(100) = NULL OUTPUT -- if @startSession = 1, then this return a new value, otherwise will retrieve the existing records in that session
) AS 
BEGIN
	IF (@BatchID IS NOT NULL) SET @StartSession = 0  -- if selecting orders by BatchID, do NOT start a session
	IF (@StartSession IS NULL) SET @StartSession = 0  -- default to FALSE if not supplied
	IF (@SessionID IS NULL) SET @SessionID = 0 -- default value

	-- if an empty/blank string parameter is provided, trim it to NULL
	SELECT @JobNumber = dbo.fnTrimToNull(@JobNumber)
		, @ContractNumber = dbo.fnTrimToNull(@ContractNumber)

	-- validate the incoming parameters
	IF (@BatchID IS NOT NULL
		AND (@ShipperID <> -1 
			OR @CarrierID <> -1 
			OR @ProductGroupID <> -1
			OR @TruckTypeID <> -1
			OR @DriverGroupID <> -1
			OR @OriginStateID <> -1
			OR @DestStateID <> -1
			OR @ProducerID <> -1))
	BEGIN
		RAISERROR('@BatchID parameter cannot be specified with other parameters', 16, 1)
		RETURN
	END

	DECLARE @IDs IDTABLE

	-- if a @BatchID value was specified then just use this criteria alone
    IF @BatchID IS NOT NULL
	BEGIN
		INSERT INTO @IDs SELECT OrderID FROM tblOrderSettlementCarrier WHERE BatchID = @BatchID
	END
	ELSE IF (@StartSession = 0 AND @SessionID IS NOT NULL)
	BEGIN
		INSERT INTO @IDs SELECT OrderID FROM tblOrderSettlementSelectionCarrier WHERE SessionID = @SessionID
	END
	ELSE
	BEGIN
		INSERT INTO @IDs 
			SELECT O.ID
			FROM tblOrder O
			JOIN tblProduct P ON P.ID = O.ProductID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			JOIN tblDestination D ON D.ID = O.DestinationID
			JOIN tblTruck T1 ON T1.ID = O.TruckID
			LEFT JOIN dbo.tblOrderTransfer OTR ON OTR.OrderID = O.ID
			LEFT JOIN tblTruck T2 ON T2.ID = OTR.OriginTruckID
			LEFT JOIN dbo.viewDriverBase vODR ON vODR.ID = OTR.OriginDriverID
			LEFT JOIN dbo.viewDriverBase vDDR ON vDDR.ID = O.DriverID
			LEFT JOIN tblOrderSettlementCarrier OS ON OS.OrderID = O.ID
			LEFT JOIN tblOrderSettlementShipper OSP ON OSP.OrderID = O.ID
			WHERE O.StatusID IN (4)  
			  AND O.DeleteDateUTC IS NULL  -- 3.9.34
			  AND OS.BatchID IS NULL -- only get order if it's not part of a batch
			  AND (@ShipperID=-1 OR O.CustomerID=@ShipperID) 
			  AND (@CarrierID=-1 OR O.CarrierID=@CarrierID) 
			  AND (@ProductGroupID=-1 OR P.ProductGroupID=@ProductGroupID) 
			  AND (@TruckTypeID=-1 OR T1.TruckTypeID=@TruckTypeID OR T2.TruckTypeID=@TruckTypeID)
			  AND (@DriverGroupID=-1 OR vODR.DriverGroupID = @DriverGroupID OR vDDR.DriverGroupID = @DriverGroupID) 
			  AND (@ProducerID=-1 OR O.ProducerID=@ProducerID) 
			  AND (@OriginStateID=-1 OR OO.StateID=@OriginStateID) 
			  AND (@DestStateID=-1 OR D.StateID=@DestStateID) 
			  AND (@StartDate IS NULL OR O.OrderDate >= @StartDate) 
			  AND (@EndDate IS NULL OR O.OrderDate <= @EndDate)
			  AND (@OnlyShipperSettled = 0 OR OSP.BatchID IS NOT NULL)
			  AND (@JobNumber IS NULL OR O.JobNumber = @JobNumber)
			  AND (@ContractNumber IS NULL OR O.ContractNumber = @ContractNumber)
			  AND (@Rejected=-1 OR O.Rejected=@Rejected) -- 4.1.3.5
	END

	-- create the temp table to store the data
	SELECT *, RateApplySel = cast(1 as bit), BatchSel = cast(1 as bit), SessionID = cast(NULL AS varchar(100))
	INTO #ret 
	FROM viewOrder_Financial_Carrier
	WHERE 1 = 0
	
	-- get the data into the temp table
	INSERT INTO #ret
		SELECT O.*
			, OS.RateApplySel, OS.BatchSel, OS.SessionID
		FROM viewOrder_Financial_Carrier O
		LEFT JOIN tblOrderSettlementSelectionCarrier OS ON OS.OrderID = O.ID AND SessionID = @sessionID -- 4.3.0.2 - added @sessionID criteria
		WHERE O.OrderID IN (SELECT ID FROM @IDs)
	
	-- do the SessionID logic
	IF (@StartSession = 1 AND EXISTS (SELECT * FROM #ret))
	BEGIN
		BEGIN TRAN RetrieveOrdersFinancialCarrier
	
		/* 4.3.0.2 - generate the "next" sessionID record */
		INSERT INTO tblOrderSettlementSessionCarrier(UserName) VALUES (@UserName)
		SELECT @sessionID = SCOPE_IDENTITY()

		UPDATE #ret SET SessionID = @sessionID, RateApplySel = 1, BatchSel = CASE WHEN HasError = 1 THEN 0 ELSE 1 END
		INSERT INTO tblOrderSettlementSelectionCarrier (SessionID, OrderID, RateApplySel, BatchSel)
			SELECT @sessionID, ID, RateApplySel, BatchSel FROM #ret

		COMMIT TRAN RetrieveOrdersFinancialCarrier

		/* "purge" the tblOrderSettlementSessionShipper table of "obsolete" records (older than a week old)
			NOTE: if this ends up being a performance hit, we should instead execute this via a scheduled task */
		DELETE FROM tblOrderSettlementSessionCarrier WHERE CreateDateUTC < dateadd(week, -1, getdate())
	END

	-- return the data to the caller
	SELECT * FROM #ret
END

GO

/**********************************/
-- Created: 2013/03/09 - ?.?.? - Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
-- Changes:
-- 4.1.0.2	- 2016.08.28 - KDA	- add some PRINT instrumentation statements for performance debugging purposes
-- 4.1.3.5	- 2016/09/17 - BB	- DCWEB-1770: Add reject filter
-- 4.1.8.6	- 2016.09.21 - KDA	- remove dynamic sql and retrieve data in 2 stages (like before) getting the orderID first, then the final data (expensive query)
--								- use new fnTrimToNull() to ensure optional string parameters are trimmed to NULL
-- 4.1.9.3	- 2026.09.28 - KDA	- ensure @StartSession = 0 when @BatchID is not null
-- 4.1.9.4	- 2016.09.28 - JAE	- (omitted from prev update) use new fnTrimToNull() to ensure optional string parameters are trimmed to NULL 
-- 4.1.25.3	- 2016.10.28 - KDA	- ensure we don't reuse a sessionID when generating a new session
-- 4.3.0.2	- 2016.11.11 - KDA	- prevent including the same order multiple times (error in LEFT JOIN)
--								- don't remove existing session on overlapping creation, only "purge" after 1 week (obsolete)
/***********************************/
ALTER PROCEDURE spRetrieveOrdersFinancialDriver
(
  @StartDate date = NULL -- will default to first day of 3rd prior month for efficiency sake
, @EndDate date = NULL
, @ShipperID int = -1 -- all
, @CarrierID int = -1 -- all
, @ProductGroupID int = -1 -- all 
, @TruckTypeID int = -1 -- all
, @DriverGroupID int = -1 -- all
, @DriverID int = -1 -- all
, @OriginStateID int = -1 -- all 
, @DestStateID int = -1 -- all
, @ProducerID int = -1 -- all
, @BatchID int = NULL -- either show unbatched (NULL), or the specified batch orders
, @OnlyCarrierSettled bit = 0 
, @JobNumber varchar(20) = NULL -- 3.11.19
, @ContractNumber varchar(20) = NULL -- 3.11.19
, @Rejected int = -1 -- all (rejected + not rejected) -- 4.1.3.5
, @UserName varchar(100)
, @StartSession bit = 1 -- when set to 1, assign the retrieved records to a new Session (ID), when 0 and a sessionID was assigned, then retrieve it
, @SessionID varchar(100) = NULL OUTPUT -- if @startSession = 1, then this return a new value, otherwise will retrieve the existing records in that session
) AS 
BEGIN
	PRINT 'Start=' + CONVERT(VARCHAR(24), GETDATE(), 121)

	IF (@BatchID IS NOT NULL) SET @StartSession = 0  -- if selecting orders by BatchID, do NOT start a session
	IF (@StartSession IS NULL) SET @StartSession = 0  -- default to FALSE if not supplied
	IF (@SessionID IS NULL) SET @SessionID = 0 -- default value

	-- if an empty/blank string parameter is provided, trim it to NULL
	SELECT @JobNumber = dbo.fnTrimToNull(@JobNumber)
		, @ContractNumber = dbo.fnTrimToNull(@ContractNumber)

	-- validate the incoming parameters
	IF (@BatchID IS NOT NULL
		AND (@ShipperID <> -1 
			OR @CarrierID <> -1 
			OR @ProductGroupID <> -1
			OR @TruckTypeID <> -1
			OR @DriverGroupID <> -1
			OR @DriverID <> -1
			OR @OriginStateID <> -1
			OR @DestStateID <> -1
			OR @ProducerID <> -1))
	BEGIN
		RAISERROR('@BatchID parameter cannot be specified with other parameters', 16, 1)
		RETURN
	END

	DECLARE @IDs IDTABLE

	-- if a @BatchID value was specified then just use this criteria alone
    IF @BatchID IS NOT NULL
	BEGIN
		INSERT INTO @IDs SELECT OrderID FROM tblOrderSettlementDriver WHERE BatchID = @BatchID
	END
	ELSE IF (@StartSession = 0 AND @SessionID IS NOT NULL)
	BEGIN
		INSERT INTO @IDs SELECT OrderID FROM tblOrderSettlementSelectionDriver WHERE SessionID = @SessionID 
	END
	ELSE
	BEGIN
		INSERT INTO @IDs 
			SELECT O.ID
			FROM tblOrder O
			JOIN tblProduct P ON P.ID = O.ProductID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			JOIN tblDestination D ON D.ID = O.DestinationID
			JOIN tblTruck T1 ON T1.ID = O.TruckID
			LEFT JOIN dbo.tblOrderTransfer OTR ON OTR.OrderID = O.ID
			LEFT JOIN tblTruck T2 ON T2.ID = OTR.OriginTruckID
			LEFT JOIN dbo.viewDriverBase vODR ON vODR.ID = OTR.OriginDriverID
			LEFT JOIN dbo.viewDriverBase vDDR ON vDDR.ID = O.DriverID
			LEFT JOIN tblOrderSettlementDriver OS ON OS.OrderID = O.ID
			LEFT JOIN tblOrderSettlementCarrier OSP ON OSP.OrderID = O.ID
			WHERE O.StatusID IN (4)  
			  AND O.DeleteDateUTC IS NULL  -- 3.9.34
			  AND OS.BatchID IS NULL -- only get order if it's not part of a batch
			  AND (@ShipperID=-1 OR O.CustomerID=@ShipperID) 
			  AND (@CarrierID=-1 OR O.CarrierID=@CarrierID) 
			  AND (@ProductGroupID=-1 OR P.ProductGroupID=@ProductGroupID) 
			  AND (@TruckTypeID=-1 OR T1.TruckTypeID=@TruckTypeID OR T2.TruckTypeID=@TruckTypeID)
			  AND (@DriverGroupID=-1 OR vODR.DriverGroupID = @DriverGroupID OR vDDR.DriverGroupID = @DriverGroupID) 
			  AND (@DriverID=-1 OR vODR.ID = @DriverID OR vDDR.ID = @DriverID) 
			  AND (@OriginStateID=-1 OR OO.StateID=@OriginStateID) 
			  AND (@DestStateID=-1 OR D.StateID=@DestStateID) 
			  AND (@ProducerID=-1 OR O.ProducerID=@ProducerID) 
			  AND (@StartDate IS NULL OR O.OrderDate >= @StartDate) 
			  AND (@EndDate IS NULL OR O.OrderDate <= @EndDate)
			  AND (@OnlyCarrierSettled = 0 OR OSP.BatchID IS NOT NULL)
			  AND (@JobNumber IS NULL OR O.JobNumber = @JobNumber)
			  AND (@ContractNumber IS NULL OR O.ContractNumber = @ContractNumber)
			  AND (@Rejected=-1 OR O.Rejected=@Rejected)  -- 4.1.3.5
	END

	-- create the temp table to store the data
	SELECT *, RateApplySel = cast(1 as bit), BatchSel = cast(1 as bit), SessionID = cast(NULL AS varchar(100))
	INTO #ret 
	FROM viewOrder_Financial_Driver
	WHERE 1 = 0
	
	-- get the data into the temp table
	INSERT INTO #ret
		SELECT O.*
			, OS.RateApplySel, OS.BatchSel, OS.SessionID
		FROM viewOrder_Financial_Driver O
		LEFT JOIN tblOrderSettlementSelectionDriver OS ON OS.OrderID = O.ID AND SessionID = @sessionID -- 4.3.0.2 - added @sessionID criteria
		WHERE O.OrderID IN (SELECT ID FROM @IDs)

	-- do the SessionID logic
	IF (@StartSession = 1 AND EXISTS (SELECT * FROM #ret))
	BEGIN
		PRINT 'SessionCreate:Start=' + CONVERT(VARCHAR(24), GETDATE(), 121)
		BEGIN TRAN RetrieveOrdersFinancialDriver

		/* 4.3.0.2 - generate the "next" sessionID record */
		INSERT INTO tblOrderSettlementSessionDriver(UserName) VALUES (@UserName)
		SELECT @sessionID = SCOPE_IDENTITY()

		UPDATE #ret SET SessionID = @sessionID, RateApplySel = 1, BatchSel = CASE WHEN HasError = 1 THEN 0 ELSE 1 END
		INSERT INTO tblOrderSettlementSelectionDriver (SessionID, OrderID, RateApplySel, BatchSel)
			SELECT @sessionID, ID, RateApplySel, BatchSel FROM #ret
	
		COMMIT TRAN RetrieveOrdersFinancialDriver
		PRINT 'SessionCreate:Finish=' + CONVERT(VARCHAR(24), GETDATE(), 121)

		/* "purge" the tblOrderSettlementSessionDriver table of "obsolete" records (older than a week old)
			NOTE: if this ends up being a performance hit, we should instead execute this via a scheduled task */
		DELETE FROM tblOrderSettlementSessionDriver WHERE CreateDateUTC < dateadd(week, -1, getdate())
	END

	-- return the data to the caller
	SELECT * FROM #ret

	PRINT 'Finish=' + CONVERT(VARCHAR(24), GETDATE(), 121)
END

GO

/***********************************/
-- Created: ?.?.? - 2013/03/09 - Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
-- Changes:
-- 3.8.11	- 2015/07/28 - KDA	- remove OriginShipperRegion field
-- 3.9.0	- 2/15/08/14 - KDA	- return Approved column
-- 3.9.19.6	- 2015/09/30 - KDA	- add OverrideXXX fields from OrderApproval table
-- 3.9.34	- 2015/12/16 - BB	- Remove deleted orders from the results (DCWEB-851)
-- 3.10.13.6 - 2015/03/01 - JAE	- add TruckTypeID
-- 3.11.3.1	- 2016/03/22 - KDA	- fix to no-op when @BatchID = 0
-- 3.11.19	- 2016/05/02 - BB	- Add Job number and Contract number
-- 3.11.20.3 - 2016/05/11 - KDA	- optimize @BatchID filtering to query directly from tblOrderSettlementShipper table
--								- use tblOrder and minimal JOINs intead of expensive viewOrder
-- 4.1.0	- 2016/08/08 - KDA	- use new @StartSession & @SessionID parameters to persist the retrieved records as a "Session"
--								- stop defaulting @StartDate to 3 months prior if not supplied (null parameter value provided)
--								- use simplified viewOrder_Financial_Shipper for return results - which is now again optimized for "reasonable" performance
-- 4.1.3.5	- 2016/09/17 - BB	- DCWEB-1770: Add reject filter
-- 4.1.8.6	- 2016.09.21 - KDA	- remove dynamic sql and retrieve data in 2 stages (like before) getting the orderID first, then the final data (expensive query)
--								- use new fnTrimToNull() to ensure optional string parameters are trimmed to NULL
-- 4.1.9.3	- 2026.09.28 - KDA	- ensure @StartSession = 0 when @BatchID is not null
-- 4.1.25.3	- 2016.10.28 - KDA	- ensure we don't reuse a sessionID when generating a new session
-- 4.3.0.2	- 2016.11.11 - KDA	- prevent including the same order multiple times (error in LEFT JOIN)
--								- don't remove existing session on overlapping creation, only "purge" after 1 week (obsolete)
/***********************************/
ALTER PROCEDURE spRetrieveOrdersFinancialShipper
(
  @StartDate date = NULL -- will default to first day of 3rd prior month for efficiency sake
, @EndDate date = NULL
, @ShipperID int = -1 -- all
, @ProductGroupID int = -1 -- all 
, @TruckTypeID int = -1 -- all
, @OriginStateID int = -1 -- all 
, @DestStateID int = -1 -- all
, @ProducerID int = -1 -- all
, @BatchID int = NULL -- either show unbatched (NULL), or the specified batch orders
, @JobNumber varchar(20) = NULL -- 3.11.19
, @ContractNumber varchar(20) = NULL -- 3.11.19
, @Rejected int = -1 -- all (rejected + not rejected) -- 4.1.3.5
, @UserName varchar(100)
, @StartSession bit = 1 -- when set to 1, assign the retrieved records to a new Session (ID), when 0 and a sessionID was assigned, then retrieve it
, @SessionID varchar(100) = NULL OUTPUT -- if @startSession = 1, then this return a new value, otherwise will retrieve the existing records in that session
) AS 
BEGIN
	IF (@BatchID IS NOT NULL) SET @StartSession = 0  -- if selecting orders by BatchID, do NOT start a session
	IF (@StartSession IS NULL) SET @StartSession = 0  -- default to FALSE if not supplied
	IF (@SessionID IS NULL) SET @SessionID = 0 -- default value

	-- if an empty/blank string parameter is provided, trim it to NULL
	SELECT @JobNumber = dbo.fnTrimToNull(@JobNumber)
		, @ContractNumber = dbo.fnTrimToNull(@ContractNumber)

	-- validate the incoming parameters
	IF (@BatchID IS NOT NULL
		AND (@ShipperID <> -1 
			OR @ProductGroupID <> -1
			OR @TruckTypeID <> -1
			OR @OriginStateID <> -1
			OR @DestStateID <> -1
			OR @ProducerID <> -1))
	BEGIN
		RAISERROR('@BatchID parameter cannot be specified with other parameters', 16, 1)
		RETURN
	END

	DECLARE @IDs IDTABLE

	-- if a @BatchID value was specified then just use this criteria alone
    IF @BatchID IS NOT NULL
	BEGIN
		INSERT INTO @IDs SELECT OrderID FROM tblOrderSettlementShipper WHERE BatchID = @BatchID
	END
	ELSE IF (@StartSession = 0 AND @SessionID IS NOT NULL)
	BEGIN
		INSERT INTO @IDs SELECT OrderID FROM tblOrderSettlementSelectionShipper WHERE SessionID = @SessionID
	END
	ELSE
	BEGIN
		INSERT INTO @IDs 
			SELECT O.ID
			FROM tblOrder O
			JOIN tblProduct P ON P.ID = O.ProductID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			JOIN tblDestination D ON D.ID = O.DestinationID
			JOIN tblTruck T1 ON T1.ID = O.TruckID
			LEFT JOIN dbo.tblOrderTransfer OTR ON OTR.OrderID = O.ID
			LEFT JOIN tblTruck T2 ON T2.ID = OTR.OriginTruckID
			LEFT JOIN tblOrderSettlementShipper OS ON OS.OrderID = O.ID
			WHERE O.StatusID IN (4)  
			  AND O.DeleteDateUTC IS NULL  -- 3.9.34
			  AND OS.BatchID IS NULL -- only get order if it's not part of a batch
			  AND (@ShipperID=-1 OR O.CustomerID=@ShipperID) 
			  AND (@ProductGroupID=-1 OR P.ProductGroupID=@ProductGroupID) 
			  AND (@TruckTypeID=-1 OR T1.TruckTypeID=@TruckTypeID OR T2.TruckTypeID=@TruckTypeID)
			  AND (@ProducerID=-1 OR O.ProducerID=@ProducerID) 
			  AND (@OriginStateID=-1 OR OO.StateID=@OriginStateID) 
			  AND (@DestStateID=-1 OR D.StateID=@DestStateID) 
			  AND (@StartDate IS NULL OR O.OrderDate >= @StartDate) 
			  AND (@EndDate IS NULL OR O.OrderDate <= @EndDate)
			  AND (@JobNumber IS NULL OR O.JobNumber = @JobNumber)
			  AND (@ContractNumber IS NULL OR O.ContractNumber = @ContractNumber)
			  AND (@Rejected=-1 OR O.Rejected=@Rejected) -- 4.1.3.5
	END
	
	-- create the temp table to store the data
	SELECT *, RateApplySel = cast(1 as bit), BatchSel = cast(1 as bit), SessionID = cast(NULL AS varchar(100))
	INTO #ret 
	FROM viewOrder_Financial_Shipper 
	WHERE 1 = 0
	
	-- get the data into the temp table
	INSERT INTO #ret
		SELECT O.*
			, OS.RateApplySel, OS.BatchSel, OS.SessionID
		FROM viewOrder_Financial_Shipper O
		LEFT JOIN tblOrderSettlementSelectionShipper OS ON OS.OrderID = O.ID AND SessionID = @sessionID -- 4.3.0.2 - added @sessionID criteria
		WHERE O.OrderID IN (SELECT ID FROM @IDs)

	-- do the SessionID logic
	IF (@StartSession = 1 AND EXISTS (SELECT * FROM #ret))
	BEGIN
		BEGIN TRAN RetrieveOrdersFinancialShipper
	
		/* 4.3.0.2 - generate the "next" sessionID record */
		INSERT INTO tblOrderSettlementSessionShipper(UserName) VALUES (@UserName)
		SELECT @sessionID = SCOPE_IDENTITY()

		UPDATE #ret SET SessionID = @sessionID, RateApplySel = 1, BatchSel = CASE WHEN HasError = 1 THEN 0 ELSE 1 END
		INSERT INTO tblOrderSettlementSelectionShipper (SessionID, OrderID, RateApplySel, BatchSel)
			SELECT @sessionID, ID, RateApplySel, BatchSel FROM #ret
	
		COMMIT TRAN RetrieveOrdersFinancialShipper

		/* "purge" the tblOrderSettlementSessionShipper table of "obsolete" records (older than a week old)
			NOTE: if this ends up being a performance hit, we should instead execute this via a scheduled task */
		DELETE FROM tblOrderSettlementSessionShipper WHERE CreateDateUTC < dateadd(week, -1, getdate())
	END
	
	-- return the data to the caller
	SELECT * FROM #ret
END

GO

/***********************************/
-- Created: ?.?.? - 2013/06/02 - Kevin Alons
-- Purpose: compute and add the various Carrier "Settlement" $$ values to an Audited order
-- Changes:
-- 4.1.0	- 2016/08/21 - KDA	- use new @SessionID parameter to create the entire settlement record in a single invocation
-- 4.1.8.6	- 2016.09.24 - KDA	- optimize data retrieval into 2 separate steps (MAJOR performance improvement workaround)
-- 4.3.0.2	- 2016.11.11 - KDA	- prevent execution if any orders in the session are already Settled
--								- remove related tblOrderSettlementSessionCarrier record at end of settlement if all records are settled
/***********************************/
ALTER PROCEDURE spCreateCarrierSettlementBatch
(
  @SessionID varchar(100)
, @CarrierID int
, @InvoiceNum varchar(50)
, @Notes varchar(255)
, @UserName varchar(255)
, @BatchID int = NULL output
, @BatchNum int = NULL output
, @Outcome varchar(25) = 'Success' output
, @Msg varchar(255) = NULL output
) AS BEGIN
	-- retrieve the basic session order data first
	SELECT OS.OrderID, IsRated = cast(0 as bit), HasError = cast(0 as bit), IsSettled = cast(0 as bit)
	INTO #data
	FROM tblOrderSettlementSelectionCarrier OS 
	WHERE OS.SessionID = @SessionID AND OS.BatchSel = 1
	-- add the Financial information next (done in 2 steps for MAJOR performance reasons - a workaround but a more intuitive way was not identified)
	UPDATE #data
	  SET IsRated = cast(CASE WHEN OFB.InvoiceRatesAppliedDateUTC IS NULL THEN 0 ELSE 1 END as bit)
		, HasError = OFB.HasError
		, IsSettled = CASE WHEN BatchID IS NULL THEN 0 ELSE 1 END
	FROM #data O
	JOIN (
		SELECT OrderID, InvoiceRatesAppliedDateUTC, HasError , BatchID
		FROM viewOrder_Financial_Base_Carrier 
		WHERE OrderID IN (SELECT OrderID FROM #data)
	) OFB ON OFB.OrderID = O.OrderID

	-- validate the data next
	IF NOT EXISTS (SELECT * FROM #data)
	BEGIN
		SELECT @Outcome = 'Error', @Msg = 'At least order must be selected to complete settlement!'
		RETURN
	END
	ELSE IF EXISTS (SELECT * FROM #data WHERE IsRated = 0 OR HasError <> 0)
	BEGIN
		SELECT @Outcome = 'Error', @Msg = 'All selected orders must be fully rated with no errors to complete Settlement!'
		RETURN
	END
	ELSE IF EXISTS (SELECT * FROM #data WHERE IsSettled = 1)
	BEGIN
		SELECT @Outcome = 'Error', @Msg = 'At least 1 order is already settled - Refresh to re-query and continue!'
		RETURN
	END

	-- if validation passed, complete the settlement process below
	SET XACT_ABORT ON
	BEGIN TRAN CarrierSettlement

	INSERT INTO dbo.tblCarrierSettlementBatch(CarrierID, InvoiceNum, BatchNum, BatchDate, Notes, CreateDateUTC, CreatedByUser)
		SELECT @CarrierID
			, @InvoiceNum
			, isnull(max(BatchNum), 0) + 1
			, dbo.fnDateOnly(getdate()), @Notes, getutcdate(), @UserName 
		FROM dbo.tblCarrierSettlementBatch
	SELECT @BatchID = scope_identity()
	SELECT @BatchNum = BatchNum FROM dbo.tblCarrierSettlementBatch WHERE ID = @BatchID

	UPDATE tblOrderSettlementCarrier 
		SET BatchID = @BatchID
	FROM tblOrderSettlementCarrier OSC
	JOIN #data D ON D.OrderID = OSC.OrderID
	WHERE BatchID IS NULL  -- double-check that we aren't re-settling the order

	SELECT @Outcome = 'Success', @Msg = 'Settlement process complete: ' + ltrim(count(1)) + ' record(s) were added to batch: ' + ltrim(@BatchNum)
	FROM #data

	DELETE FROM tblOrderSettlementSelectionCarrier WHERE SessionID = @SessionID AND BatchSel = 1
	IF NOT EXISTS (SELECT SessionID FROM tblOrderSettlementSelectionCarrier WHERE SessionID = @SessionID)
		DELETE FROM tblOrderSettlementSessionCarrier WHERE ID = @SessionID

	COMMIT TRAN
END

GO

/***********************************/
-- Created: ?.?.? - 2013/06/02 - Kevin Alons
-- Purpose: compute and add the various Driver "Settlement" $$ values to an Audited order
-- Changes:
-- 4.1.8.6	- 2016.09.24 - KDA	- optimize data retrieval into 2 separate steps (MAJOR performance improvement workaround)
-- 4.3.0.2	- 2016.11.11 - KDA	- prevent execution if any orders in the session are already Settled
--								- remove related tblOrderSettlementSessionDriver record at end of settlement if all records are settled
/***********************************/
ALTER PROCEDURE spCreateDriverSettlementBatch
(
  @SessionID varchar(100)
, @CarrierID int
, @DriverGroupID int = NULL
, @DriverID int = NULL
, @InvoiceNum varchar(50) = NULL
, @Notes varchar(255) = NULL
, @UserName varchar(255) 
, @BatchID int = NULL output
, @BatchNum int = NULL output
, @Outcome varchar(25) = 'Success' output
, @Msg varchar(255) = NULL output
) AS BEGIN
	IF (@DriverGroupID = 0) SET @DriverGroupID = NULL
	IF (@DriverID = 0) SET @DriverID = NULL

	PRINT 'spCreateDriverSettlementBatch(' + ltrim(@SessionID) + ').Retrieve START ' + (CONVERT(VARCHAR(24), GETDATE(), 121))

	-- retrieve the basic session order data first
	SELECT OS.OrderID, IsRated = cast(0 as bit), HasError = cast(0 as bit), IsSettled = cast(0 as bit)
	INTO #data
	FROM tblOrderSettlementSelectionDriver OS 
	WHERE OS.SessionID = @SessionID AND OS.BatchSel = 1
	-- add the Financial information next (done in 2 steps for MAJOR performance reasons - a workaround but a more intuitive way was not identified)
	UPDATE #data
	  SET IsRated = cast(CASE WHEN OFB.InvoiceRatesAppliedDateUTC IS NULL THEN 0 ELSE 1 END as bit)
		, HasError = OFB.HasError
		, IsSettled = CASE WHEN BatchID IS NULL THEN 0 ELSE 1 END
	FROM #data O
	JOIN (
		SELECT OrderID, InvoiceRatesAppliedDateUTC, HasError, BatchID
		FROM viewOrder_Financial_Base_Driver 
		WHERE OrderID IN (SELECT OrderID FROM #data)
	) OFB ON OFB.OrderID = O.OrderID

	PRINT 'spCreateDriverSettlementBatch(' + ltrim(@SessionID) + ').Retrieve DONE ' + (CONVERT(VARCHAR(24), GETDATE(), 121))

	-- validate the data next
	IF NOT EXISTS (SELECT * FROM #data)
	BEGIN
		SELECT @Outcome = 'Error', @Msg = 'At least 1 order must be selected to complete settlement!'
		RETURN
	END
	ELSE IF EXISTS (SELECT * FROM #data WHERE IsRated = 0 OR HasError <> 0)
	BEGIN
		SELECT @Outcome = 'Error', @Msg = 'All selected orders must be fully rated with no errors to complete Settlement!'
		RETURN
	END
	ELSE IF EXISTS (SELECT * FROM #data WHERE IsSettled = 1)
	BEGIN
		SELECT @Outcome = 'Error', @Msg = 'At least 1 order is already settled - Refresh to re-query and continue!'
		RETURN
	END

	-- if validation passed, complete the settlement process below
	SET XACT_ABORT ON
	BEGIN TRAN DriverSettlement

	INSERT INTO dbo.tblDriverSettlementBatch(CarrierID, DriverGroupID, DriverID, InvoiceNum, BatchNum, BatchDate, Notes, CreateDateUTC, CreatedByUser)
		SELECT @CarrierID, @DriverGroupID, @DriverID
			, @InvoiceNum
			, isnull(max(BatchNum), 0) + 1
			, dbo.fnDateOnly(getdate()), @Notes, getutcdate(), @UserName 
		FROM dbo.tblDriverSettlementBatch
	SELECT @BatchID = scope_identity()
	SELECT @BatchNum = BatchNum FROM dbo.tblDriverSettlementBatch WHERE ID = @BatchID

	UPDATE tblOrderSettlementDriver 
		SET BatchID = @BatchID
	FROM tblOrderSettlementDriver OSC
	JOIN #data D ON D.OrderID = OSC.OrderID
	WHERE BatchID IS NULL  -- double-check that we aren't re-settling the order

	SELECT @Outcome = 'Success', @Msg = 'Settlement process complete: ' + ltrim(count(1)) + ' record(s) were added to batch: ' + ltrim(@BatchNum)
	FROM #data

	DELETE FROM tblOrderSettlementSelectionDriver WHERE SessionID = @SessionID AND BatchSel = 1
	IF NOT EXISTS (SELECT SessionID FROM tblOrderSettlementSelectionDriver WHERE SessionID = @SessionID)
		DELETE FROM tblOrderSettlementSessionDriver WHERE ID = @SessionID

	COMMIT TRAN
END

GO

/***********************************/
-- Created: ?.?.? - 2013/06/02 - Kevin Alons
-- Purpose: compute and add the various Shipper "Settlement" $$ values to an Audited order
-- Changes:
-- 4.1.0 - 2016/08/21 - KDA - use new @SessionID parameter to create the entire settlement record in a single invocation
-- 4.1.8.6	- 2016.09.24 - KDA	- optimize data retrieval into 2 separate steps (MAJOR performance improvement workaround)
-- 4.3.0.2	- 2016.11.11 - KDA	- prevent execution if any orders in the session are already Settled
--								- remove related tblOrderSettlementSessionShipper record at end of settlement if all records are settled
/***********************************/
ALTER PROCEDURE spCreateShipperSettlementBatch
(
  @SessionID varchar(100)
, @ShipperID int
, @InvoiceNum varchar(50)
, @Notes varchar(255)
, @UserName varchar(255)
, @BatchID int = NULL output
, @BatchNum int = NULL output
, @Outcome varchar(25) = 'Success' output
, @Msg varchar(255) = NULL output
) AS BEGIN
	-- retrieve the basic session order data first
	SELECT OS.OrderID, IsRated = cast(0 as bit), HasError = cast(0 as bit), IsSettled = cast(0 as bit)
	INTO #data
	FROM tblOrderSettlementSelectionShipper OS 
	WHERE OS.SessionID = @SessionID AND OS.BatchSel = 1
	-- add the Financial information next (done in 2 steps for MAJOR performance reasons - a workaround but a more intuitive way was not identified)
	UPDATE #data
	  SET IsRated = cast(CASE WHEN OFB.InvoiceRatesAppliedDateUTC IS NULL THEN 0 ELSE 1 END as bit)
		, HasError = OFB.HasError
		, IsSettled = CASE WHEN BatchID IS NULL THEN 0 ELSE 1 END
	FROM #data O
	JOIN (
		SELECT OrderID, InvoiceRatesAppliedDateUTC, HasError, BatchID
		FROM viewOrder_Financial_Base_Shipper 
		WHERE OrderID IN (SELECT OrderID FROM #data)
	) OFB ON OFB.OrderID = O.OrderID

	-- validate the data next
	IF NOT EXISTS (SELECT * FROM #data)
	BEGIN
		SELECT @Outcome = 'Error', @Msg = 'At least order must be selected to complete settlement!'
		RETURN
	END
	ELSE IF EXISTS (SELECT * FROM #data WHERE IsRated = 0 OR HasError <> 0)
	BEGIN
		SELECT @Outcome = 'Error', @Msg = 'All selected orders must be fully rated with no errors to complete Settlement!'
		RETURN
	END
	ELSE IF EXISTS (SELECT * FROM #data WHERE IsSettled = 1)
	BEGIN
		SELECT @Outcome = 'Error', @Msg = 'At least 1 order is already settled - Refresh to re-query and continue!'
		RETURN
	END

	-- if validation passed, complete the settlement process below
	SET XACT_ABORT ON
	BEGIN TRAN ShipperSettlement

	INSERT INTO dbo.tblShipperSettlementBatch(ShipperID, InvoiceNum, BatchNum, BatchDate, Notes, CreateDateUTC, CreatedByUser)
		SELECT @ShipperID
			, @InvoiceNum
			, isnull(max(BatchNum), 0) + 1
			, dbo.fnDateOnly(getdate()), @Notes, getutcdate(), @UserName 
		FROM dbo.tblShipperSettlementBatch
	SELECT @BatchID = scope_identity()
	SELECT @BatchNum = BatchNum FROM dbo.tblShipperSettlementBatch WHERE ID = @BatchID

	UPDATE tblOrderSettlementShipper 
		SET BatchID = @BatchID
	FROM tblOrderSettlementShipper OSC
	JOIN #data D ON D.OrderID = OSC.OrderID
	WHERE BatchID IS NULL  -- double-check that we aren't re-settling the order

	SELECT @Outcome = 'Success', @Msg = 'Settlement process complete: ' + ltrim(count(1)) + ' record(s) were added to batch: ' + ltrim(@BatchNum)
	FROM #data

	DELETE FROM tblOrderSettlementSelectionShipper WHERE SessionID = @SessionID AND BatchSel = 1
	IF NOT EXISTS (SELECT SessionID FROM tblOrderSettlementSelectionShipper WHERE SessionID = @SessionID)
		DELETE FROM tblOrderSettlementSessionShipper WHERE ID = @SessionID

	COMMIT TRAN
END

GO

COMMIT
SET NOEXEC OFF