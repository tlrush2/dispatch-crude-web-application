SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20) = '3.13.13.1'
	, @NewVersion varchar(20) = '3.13.13.2'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Update to rebuild all objects script'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/***********************************************************/
-- Created: 9/1/2014 - Kevin Alons
-- Purpose: rebuild all Views, Procedures, Functions, Synonyms in the database
-- Changes:
-- 3.13.13.2 - 2016/08/08 - KDA		- some cleanup - add @debug parameter
/***********************************************************/
ALTER PROCEDURE _spRebuildAllObjects
(
  @objectName varchar(255) = NULL
, @printNames bit = 0
, @printSql bit = 0
, @debug bit = 0
) AS
BEGIN
	SET NOCOUNT OFF

	SELECT dcount = CAST(NULL as int), *
	INTO #data
	FROM (
		SELECT ObjectName = TABLE_NAME, AlterSql = 'EXEC sp_refreshview ' + TABLE_NAME --replace(OBJECT_DEFINITION(OBJECT_ID(TABLE_NAME)), 'CREATE VIEW', 'ALTER VIEW')
		FROM INFORMATION_SCHEMA.VIEWS
		UNION 
		SELECT ROUTINE_Name, replace(OBJECT_DEFINITION(object_id(ROUTINE_NAME)), 'CREATE PROCEDURE', 'ALTER PROCEDURE') 
		FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND ROUTINE_DEFINITION LIKE '%CREATE PROCEDURE%'
		UNION 
		SELECT ROUTINE_Name, replace(OBJECT_DEFINITION(object_id(ROUTINE_NAME)), 'CREATE FUNCTION', 'ALTER FUNCTION') 
		FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'FUNCTION' AND ROUTINE_DEFINITION LIKE '%CREATE FUNCTION%'
		UNION  
		SELECT name, 'DROP SYNONYM ' + name + '; CREATE SYNONYM ' + name + ' FOR ' + DB_NAME() + '.dbo.' + name + ';'
		FROM sys.objects where type = 'sn'
	) X1
	WHERE ObjectName NOT LIKE '%aspnet%' AND ObjectName NOT LIKE '[_]%' AND ObjectName NOT LIKE 'ELMAH%'
		AND @objectName IS NULL OR ObjectName LIKE @ObjectName

	-- mark the dcount = 0 for objects that aren't dependent on any other object
	UPDATE #data SET dcount = 0 WHERE ObjectName NOT IN (SELECT objectname = OBJECT_NAME(id) FROM sysdepends)

	-- walk the dependencies and mark the dcount (dependency count)
	DECLARE @dcount int; SET @dcount = 1
	WHILE EXISTS (SELECT * FROM #data WHERE dcount IS NULL) BEGIN
		UPDATE #data SET dcount = @dcount WHERE ObjectName IN (
			SELECT DISTINCT dependentname = D.objectname --, ND.name
			FROM (
				SELECT name  
				FROM sys.objects 
				WHERE @dcount = 1
				 AND type IN ('U','V', 'P', 'FN', 'IF', 'TF')
				 AND name NOT LIKE '[_]%'
				 AND object_id NOT IN (SELECT id FROM sysdepends) 
				UNION 
				SELECT ObjectName FROM #data WHERE dcount = @dcount - 1
			) ND
			JOIN (
				SELECT objectname = OBJECT_NAME(id), dep_objectname = OBJECT_NAME(depid) 
				FROM sysdepends
			) D ON D.dep_objectname = ND.name)
			
		SET @dcount = @dcount + 1
	END
	
	-- save the data ordered by dependency count, object name
	SELECT id = row_number() over (order by dcount, ObjectName), done = CAST(0 as bit), * INTO #update FROM #data
	
	IF (@debug = 1)
	BEGIN
		SELECT Name = ObjectName, DependencyCount = dcount, Sql = AlterSql FROM #update ORDER BY id
	END
	ELSE
	BEGIN
		DECLARE @id int, @sql nvarchar(max), @name varchar(255)
		SELECT TOP 1 @id = id, @sql = altersql, @name = @objectName FROM #update WHERE done = 0 ORDER BY id

		WHILE (@id IS NOT NULL) BEGIN
			IF (@printNames = 1) PRINT 'NAME=' + @Name
			IF (@printSql = 1) PRINT 'SQL=' + @sql
			EXEC sp_executesql @sql
			UPDATE #update SET done = 1 WHERE id = @id
			SET @id = null
			SELECT TOP 1 @id = id, @sql = altersql, @name = ObjectName FROM #update WHERE done = 0 ORDER BY id
		END

		IF (@printNames = 0 AND @printSql = 0)
			SELECT ltrim(COUNT(*)) + ' objects were REBUILT' FROM #data
	END
END
GO

-- Execute a handful of times to ensure database changes are working
EXEC _spRebuildAllObjects
GO


-- Add trigger commands for trigOrder_IU to enforce sequence order
EXEC sp_settriggerorder @triggername=N'[dbo].[trigOrder_IU]', @order=N'First', @stmttype=N'INSERT'
GO
EXEC sp_settriggerorder @triggername=N'[dbo].[trigOrder_IU]', @order=N'First', @stmttype=N'UPDATE'
GO


COMMIT
SET NOEXEC OFF