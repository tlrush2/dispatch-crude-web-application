/* fix erroneous date fields on tblProduct
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.1.0', @NewVersion = '2.1.1'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

EXEC sp_rename 'tblProduct.CreateDate', 'CreateDateUTC', 'COLUMN'
GO
EXEC sp_rename 'tblProduct.LastChangeDate', 'LastChangeDateUTC', 'COLUMN'
GO

EXEC _spRefreshAllViews
GO

COMMIT
SET NOEXEC OFF