SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.5.15'
SELECT  @NewVersion = '4.5.15.1'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'JT-2597 - Fix ZPL ticket type dropdown options source.'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

-- Drop incorrect foreign key
ALTER TABLE tblDriverAppPrintDeliverTemplate DROP CONSTRAINT FK_DriverAppPrintDeliverTemplate_TicketType
GO

-- Create the correct foreign key
ALTER TABLE tblDriverAppPrintDeliverTemplate  WITH CHECK ADD  CONSTRAINT FK_DriverAppPrintDeliverTemplate_DestTicketType FOREIGN KEY(TicketTypeID)
REFERENCES tblDestTicketType(ID)
GO
ALTER TABLE tblDriverAppPrintDeliverTemplate CHECK CONSTRAINT FK_DriverAppPrintDeliverTemplate_DestTicketType
GO


/***********************************
-- Date Created: 2015/07/02
-- Author: Kevin Alons
-- Purpose: retrieve and return the matching DriverAppPrintDeliverTemplate record + friendly translated data
-- Changes:
	4.5.15.1 -	2017/03/14 -	BSB	-	Fix Destination Ticket Type join issue
***********************************/
ALTER FUNCTION fnBestMatchDriverAppPrintDeliverTemplateDisplay
(
  @TicketTypeID int
, @ShipperID int
, @ProductGroupID int
, @ProducerID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.TicketTypeID, R.ShipperID, R.ProductGroupID, R.ProducerID
		, R.TemplateText
		, TicketType = DTT.Name
		, Shipper = SH.Name
		, ProductGroup = PG.Name
		, Producer = P.Name
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
		, Ranking
	FROM dbo.fnBestMatchDriverAppPrintDeliverTemplate(@TicketTypeID, @ShipperID, @ProductGroupID, @ProducerID, 0) R
	LEFT JOIN tblDestTicketType DTT ON DTT.ID = R.TicketTypeID  -- 4.5.15.1 - Changed to destination ticket type (was ticket type)
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblProducer P ON P.ID = R.ProducerID
GO



/***********************************
-- Date Created: 2015/07/02
-- Author: Kevin Alons
-- Purpose: retrieve and return the Best Match DriverAppPrintDeliverTemplate info for the specified order
-- Changes:
	4.5.15.1 -	2017/03/14 -	BSB	-	Fix Destination Ticket Type join issue
***********************************/
ALTER FUNCTION fnOrderBestMatchDriverAppPrintDeliverTemplate(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, R.TemplateText, R.LastChangeDateUTC
	FROM dbo.tblOrder O
	JOIN tblProduct PRO ON PRO.ID = O.ProductID
	JOIN tblDestination D ON D.ID = DestinationID  -- 4.5.15.1 - Added destination to get ticket type (was using origin ticket type)
	CROSS APPLY dbo.fnBestMatchDriverAppPrintDeliverTemplate(D.TicketTypeID, O.CustomerID, PRO.ProductGroupID, O.ProducerID, 1) R
	WHERE O.ID = @ID 
GO



EXEC _spRecompileAllStoredProcedures
GO
EXEC _spRebuildAllObjects
GO
EXEC _spRebuildAllObjects
GO
EXEC _spRefreshAllViews
GO



COMMIT
SET NOEXEC OFF