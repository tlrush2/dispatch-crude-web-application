/*
	-- revise T_CorrectedAPIGravity field to use a numeric type (so report formatting will work as expected)
	-- add _spRebuildAllObjects if not present
	-- 
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.0.9'
SELECT  @NewVersion = '3.0.10'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details into a single view
/***********************************/
ALTER VIEW [dbo].[viewOrder_OrderTicket_Full] AS 
	SELECT OE.* 
		, T_TicketType = CASE WHEN OE.TicketCount = 0 THEN OE.TicketType ELSE OT.TicketType END 
		, T_CarrierTicketNum = CASE WHEN OE.TicketCount = 0 THEN OE.CarrierTicketNum ELSE OT.CarrierTicketNum END 
		, T_BOLNum = CASE WHEN OE.TicketCount = 0 THEN OE.OriginBOLNum ELSE OT.BOLNum END 
		, T_TankNum = OT.OriginTankText 
		, T_IsStrappedTank = OT.IsStrappedTank 
		, T_BottomFeet = OT.BottomFeet
		, T_BottomInches = OT.BottomInches
		, T_BottomQ = OT.BottomQ 
		, T_OpeningGaugeFeet = OT.OpeningGaugeFeet 
		, T_OpeningGaugeInch = OT.OpeningGaugeInch 
		, T_OpeningGaugeQ = OT.OpeningGaugeQ 
		, T_ClosingGaugeFeet = OT.ClosingGaugeFeet 
		, T_ClosingGaugeInch = OT.ClosingGaugeInch 
		, T_ClosingGaugeQ = OT.ClosingGaugeQ 
		, T_OpenTotalQ = dbo.fnGaugeQtrInches(OT.OpeningGaugeFeet, OT.OpeningGaugeInch, OT.OpeningGaugeQ) 
		, T_CloseTotalQ = dbo.fnGaugeQtrInches(OT.ClosingGaugeFeet, OT.ClosingGaugeInch, ot.ClosingGaugeQ) 
		, T_OpenReading = ltrim(OT.OpeningGaugeFeet) + 'ft ' + ltrim(OT.OpeningGaugeInch) + 'in ' + ltrim(OT.OpeningGaugeQ) + 'q' 
		, T_CloseReading = ltrim(OT.ClosingGaugeFeet) + 'ft ' + ltrim(OT.ClosingGaugeInch) + 'in ' + ltrim(OT.ClosingGaugeQ) + 'q' 
		, T_CorrectedAPIGravity = round(cast(OT.Gravity60F as decimal(9,4)), 9, 4) 
		, T_GrossStdUnits = OT.GrossStdUnits
		, T_SealOff = ltrim(OT.SealOff) 
		, T_SealOn = ltrim(OT.SealOn) 
		, T_ProductObsTemp = OT.ProductObsTemp 
		, T_ProductObsGravity = OT.ProductObsGravity 
		, T_ProductBSW = OT.ProductBSW 
		, T_Rejected = OT.Rejected 
		, T_RejectNotes = OT.RejectNotes 
		, T_GrossUnits = OT.GrossUnits 
		, T_NetUnits = OT.NetUnits 
		, PreviousDestinations = dbo.fnRerouteDetails(OE.ID, 'PreviousDestinations', '<br/>') 
		, RerouteUsers = dbo.fnRerouteDetails(OE.ID, 'RerouteUsers', '<br/>') 
		, RerouteDates = dbo.fnRerouteDetails(OE.ID, 'RerouteDates', '<br/>') 
		, RerouteNotes = dbo.fnRerouteDetails(OE.ID, 'RerouteNotes', '<br/>') 
	FROM dbo.viewOrderExportFull OE
	LEFT JOIN viewOrderTicket OT ON OT.OrderID = OE.ID AND OT.DeleteDateUTC IS NULL
	WHERE OE.DeleteDateUTC IS NULL

GO

ALTER TABLE tblUserReportColumnDefinition ADD Export bit NOT NULL
	CONSTRAINT DF_UserReportColumnDefinition_Export DEFAULT (1)
GO

/***********************************************/
-- Date Created: 27 Jul 2014
-- Author: Kevin Alons
-- Purpose: add related JOINed fields to the tblUserReportColumnDefinition table results
/***********************************************/
ALTER VIEW [dbo].[viewUserReportColumnDefinition] AS
	SELECT URCD.ID
		, URCD.UserReportID
		, URCD.ReportColumnID
		, URCD.Caption
		, DataFormat = isnull(URCD.DataFormat, RCD.DataFormat)
		, URCD.SortNum
		, URCD.FilterOperatorID
		, URCD.FilterValue1
		, URCD.FilterValue2
		, URCD.Export
		, URCD.CreateDateUTC, URCD.CreatedByUser
		, URCD.LastChangeDateUTC, URCD.LastChangedByUser
		, RCD.DataField
		, BaseCaption = RCD.Caption
		, OutputCaption = coalesce(URCD.Caption, RCD.Caption, RCD.DataField)
		, RCD.FilterTypeID
		, FilterDataField = isnull(RCD.FilterDataField, RCD.DataField)
		, RCD.FilterDropDownSql
		, RCD.FilterAllowCustomText
		, RCD.AllowedRoles
		, FilterOperator = RFO.Name
		, FilterValues = CASE 
				WHEN RFO.ID IN (1,4,5) OR RCD.FilterAllowCustomText = 1 THEN FilterValue1 + ISNULL(' - ' + FilterValue2, '')
				ELSE 
					CASE WHEN FilterValue1 IS NOT NULL THEN '&lt;filtered&gt;' 
						 ELSE NULL 
					END 
			END
	FROM tblUserReportColumnDefinition URCD
	JOIN viewReportColumnDefinition RCD ON RCD.ID = URCD.ReportColumnID
	JOIN tblReportFilterOperator RFO ON RFO.ID = URCD.FilterOperatorID

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[_spRebuildAllObjects]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[_spRebuildAllObjects]
GO

CREATE PROCEDURE _spRebuildAllObjects AS
BEGIN
	SELECT id = row_number() over (order by ObjectName), done = CAST(0 as bit), *
	INTO #update
	FROM (
		SELECT ObjectName = TABLE_NAME, AlterSql = replace(OBJECT_DEFINITION(OBJECT_ID(TABLE_NAME)), 'CREATE VIEW', 'ALTER VIEW')
		FROM INFORMATION_SCHEMA.VIEWS
		UNION 
		SELECT ROUTINE_Name, replace(OBJECT_DEFINITION(object_id(ROUTINE_NAME)), 'CREATE PROCEDURE', 'ALTER PROCEDURE') 
		FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND ROUTINE_DEFINITION LIKE '%CREATE PROCEDURE%'
		UNION 
		SELECT ROUTINE_Name, replace(OBJECT_DEFINITION(object_id(ROUTINE_NAME)), 'CREATE FUNCTION', 'ALTER FUNCTION') 
		FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'FUNCTION' AND ROUTINE_DEFINITION LIKE '%CREATE FUNCTION%'
		UNION 
		SELECT name, 'DROP SYNONYM ' + name + '; CREATE SYNONYM ' + name + ' FOR allegroprod.dbo.' + name + ';'
		FROM sys.objects where type = 'sn'
	) X1
	WHERE ObjectName NOT LIKE '%aspnet%' AND ObjectName NOT LIKE '[_]%' AND ObjectName NOT LIKE 'ELMAH%'
	
	DECLARE @id int, @sql nvarchar(max)
	SELECT TOP 1 @id = id, @sql = altersql FROM #update WHERE done = 0

	WHILE (@id IS NOT NULL) BEGIN
		EXEC sp_executesql @sql
		UPDATE #update SET done = 1 WHERE id = @id
		SET @id = null
		SELECT TOP 1 @id = id, @sql = altersql FROM #update WHERE done = 0
	END

END

GO

EXEC _spRebuildAllObjects
GO

EXEC _spRefreshAllViews
GO

COMMIT
SET NOEXEC OFF