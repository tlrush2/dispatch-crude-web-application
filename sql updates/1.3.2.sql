BEGIN TRANSACTION
GO

UPDATE tblSetting SET Value = '1.3.2' WHERE ID=0
GO

DROP INDEX [idxOrder_DeleteDate_StatusID_Covering] ON [dbo].[tblOrder]
GO

CREATE NONCLUSTERED INDEX [idxOrder_DeleteDate_StatusID_Covering] ON [dbo].[tblOrder]
(
	[DeleteDate] ASC, [StatusID] ASC
)
INCLUDE ([ID],[OrderNum],[PriorityID],[DueDate],[RouteID],[OriginID],[OriginArriveTime],[OriginDepartTime],[OriginMinutes],[OriginWaitNotes],[OriginBOLNum],[OriginGrossBarrels],[OriginNetBarrels],[DestinationID],[DestArriveTime],[DestDepartTime],[DestMinutes],[DestWaitNotes],[DestBOLNum],[DestGrossBarrels],[DestNetBarrels],[CustomerID],[CarrierID],[DriverID],[TruckID],[TrailerID],[Trailer2ID],[OperatorID],[PumperID],[TicketTypeID],[Rejected],[RejectNotes],[ChainUp],[OriginTruckMileage],[OriginTankNum],[DestTruckMileage],[CarrierTicketNum],[AuditNotes],[CreateDate],[ActualMiles],[ProducerID],[CarrierRateMiles],[CustomerRateMiles],[CreatedByUser],[LastChangeDate],[LastChangedByUser],[DeletedByUser],[DestProductBSW],[DestProductGravity],[DestProductTemp])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
GO


COMMIT TRANSACTION
GO