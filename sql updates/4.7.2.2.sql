-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.7.2.1'
SELECT  @NewVersion = '4.7.2.2'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'JT-4512 - Use UNION ALL to keep repeated log entries'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

ALTER TABLE tblCarrierSettlementBatch DROP CONSTRAINT DF_CarrierSettlementBatch_CreateDateUTC
ALTER TABLE tblCarrierSettlementBatch ALTER COLUMN CreateDateUTC DATETIME NOT NULL
ALTER TABLE tblCarrierSettlementBatch ADD CONSTRAINT DF_CarrierSettlementBatch_CreateDateUTC DEFAULT GETUTCDATE() FOR CreateDateUTC

ALTER TABLE tblCarrierSettlementBatch ALTER COLUMN LastChangeDateUTC DATETIME NULL

ALTER TABLE tblCarrierSettlementBatchDbAudit ALTER COLUMN CreateDateUTC DATETIME NOT NULL

ALTER TABLE tblCarrierSettlementBatchDbAudit ALTER COLUMN LastChangeDateUTC DATETIME NULL
GO



ALTER TABLE tblDriverSettlementBatch DROP CONSTRAINT DF_DriverSettlementBatch_CreateDateUTC
ALTER TABLE tblDriverSettlementBatch ALTER COLUMN CreateDateUTC DATETIME NOT NULL
ALTER TABLE tblDriverSettlementBatch ADD CONSTRAINT DF_DriverSettlementBatch_CreateDateUTC DEFAULT GETUTCDATE() FOR CreateDateUTC

ALTER TABLE tblDriverSettlementBatch ALTER COLUMN LastChangeDateUTC DATETIME NULL

ALTER TABLE tblDriverSettlementBatchDbAudit ALTER COLUMN CreateDateUTC DATETIME NOT NULL

ALTER TABLE tblDriverSettlementBatchDbAudit ALTER COLUMN LastChangeDateUTC DATETIME NULL
GO


ALTER TABLE tblShipperSettlementBatch DROP CONSTRAINT DF_CustomerSettlementBatch_CreateDateUTC
ALTER TABLE tblShipperSettlementBatch ALTER COLUMN CreateDateUTC DATETIME NOT NULL
ALTER TABLE tblShipperSettlementBatch ADD CONSTRAINT DF_CustomerSettlementBatch_CreateDateUTC DEFAULT GETUTCDATE() FOR CreateDateUTC

ALTER TABLE tblShipperSettlementBatch ALTER COLUMN LastChangeDateUTC DATETIME NULL

ALTER TABLE tblShipperSettlementBatchDbAudit ALTER COLUMN CreateDateUTC DATETIME NOT NULL

ALTER TABLE tblShipperSettlementBatchDbAudit ALTER COLUMN LastChangeDateUTC DATETIME NULL
GO


/*************************************************************/
-- Date Created: 2017/05/19 (4.7.1)
-- Author: Joe Engler
-- Purpose: Show Carrier settlement history
-- Changes:
--		4.7.2.1		JAE			2017-06-07		Fix to Order Date
--		4.7.2.2		JAE			2017-06-09		Use UNION ALL to keep repeated log entries
/*************************************************************/
ALTER FUNCTION fnCarrierSettlementHistory (@BatchID INT, @OrderID INT) RETURNS TABLE AS
RETURN
	SELECT Description = 'Settled'
		, IsCurrent = 1
		, OSC.OrderID
		, OSC.BatchNum
		, O.OrderNum
		, JobNumber = NULLIF(O.JobNumber, '')
		, ContractNumber = NULLIF(O.ContractNumber, '')
		, O.OrderDate
		, Shipper = O.Customer
		, Carrier = O.Carrier
		, O.Origin
		, O.Destination
		, PreviousDestinations = dbo.fnRerouteDetails(O.ID, 'PreviousDestinations', '<br/>') 
		, O.ActualMiles
		, SettlementVolume = OSC.SettlementUnits
		, OSC.SettlementUom
		, OSC.OriginChainupAmount
		, OSC.DestChainupAmount
		, OSC.H2SAmount
		, OSC.RerouteAmount
		, OSC.OrderRejectAmount
		, OSC.OriginWaitAmount
		, OSC.DestinationWaitAmount
		, OSC.TotalWaitAmount
		, OSC.SplitLoadAmount
		, MiscDetails = dbo.fnOrderShipperAssessorialDetailsTSV(O.ID, 0)
		, MiscAmount = OSC.OtherAmount
		, OSC.FuelSurchargeAmount
		, OSC.OriginTaxRate
		, OSC.RouteRate
		, OSC.RateSheetRate
		, OSC.LoadAmount
		, OSC.TotalAmount
		, CSB.BatchDate
		, CSB.PeriodEndDate
		, CSB.InvoiceNum
		, OrderNotes = OSC.Notes
		, BatchNotes = CSB.Notes
		, SettledByUser = ISNULL(CSB.LastChangedByUser, CSB.CreatedByUser)
		, SettlementTimestamp = ISNULL(CSB.LastChangeDateUTC, CSB.CreateDateUTC)
	FROM viewOrderSettlementCarrier OSC 
	JOIN viewOrder O ON OSC.OrderID = O.ID
	JOIN tblCarrierSettlementBatch CSB ON CSB.ID = OSC.BatchID
	WHERE CSB.IsFinal = 1
  	AND (@BatchID = -1 OR Batchid = @BatchID)
	AND (@OrderID = -1 OR OrderID=@OrderID)

	UNION ALL

	SELECT Description = 'Settled', IsCurrent=0
		, OrderID
		, BatchNum
		, OrderNum
		, JobNumber = NULLIF(JobNumber, '')
		, ContractNumber = NULLIF(ContractNumber, '')
		, OrderDate
		, Shipper
		, Carrier
		, Origin
		, Destination
		, PreviousDestinations
		, ActualMiles
		, InvoiceUnits
		, InvoiceSettlementUom
		, InvoiceOriginChainupAmount
		, InvoiceDestChainupAmount
		, InvoiceH2SAmount
		, InvoiceRerouteAmount
		, InvoiceOrderRejectAmount
		, InvoiceOriginWaitAmount
		, InvoiceDestinationWaitAmount
		, InvoiceTotalWaitAmount
		, InvoiceSplitLoadAmount
		, InvoiceOtherDetailsTSV
		, InvoiceOtherAmount
		, InvoiceFuelSurchargeAmount
		, InvoiceTaxRate
		, InvoiceRouteRate
		, InvoiceRateSheetRate
		, InvoiceLoadAmount
		, InvoiceTotalAmount
		, BatchDate
		, InvoicePeriodEnd
		, BatchInvoiceNum
		, OrderNotes = AuditNotes
		, BatchNotes = InvoiceNotes
		, SettledByUser = ISNULL(BatchLastChangedByUser, BatchCreatedByUser)
		, SettlementTimestamp = ISNULL(BatchLastChangeDateUTC, BatchCreateDateUTC)
	FROM tblOrderSettlementCarrierDbAudit
	WHERE (@BatchID = -1 OR Batchid = @BatchID)
	AND (@OrderID = -1 OR OrderID=@OrderID)

	UNION ALL

	SELECT Description = 'Unsettled'
		, IsCurrent=0
		, OrderID
		, BatchNum
		, OrderNum
		, JobNumber = NULLIF(JobNumber, '')
		, ContractNumber = NULLIF(ContractNumber, '')
		, OrderDate
		, Shipper
		, Carrier
		, Origin
		, Destination
		, PreviousDestinations
		, ActualMiles
		, -InvoiceUnits
		, InvoiceSettlementUom
		, -InvoiceOriginChainupAmount
		, -InvoiceDestChainupAmount
		, -InvoiceH2SAmount
		, -InvoiceRerouteAmount
		, -InvoiceOrderRejectAmount
		, -InvoiceOriginWaitAmount
		, -InvoiceDestinationWaitAmount
		, -InvoiceTotalWaitAmount
		, -InvoiceSplitLoadAmount
		, InvoiceOtherDetailsTSV
		, -InvoiceOtherAmount
		, -InvoiceFuelSurchargeAmount
		, InvoiceTaxRate
		, InvoiceRouteRate
		, InvoiceRateSheetRate
		, -InvoiceLoadAmount
		, -InvoiceTotalAmount
		, BatchDate
		, InvoicePeriodEnd
		, BatchInvoiceNum
		, OrderNotes = AuditNotes
		, BatchNotes = InvoiceNotes
		, SettledByUser = ISNULL(BatchLastChangedByUser, BatchCreatedByUser)
		, SettlementTimestamp = DbAuditDate
	FROM tblOrderSettlementCarrierDbAudit
	WHERE (@BatchID = -1 OR Batchid = @BatchID)
	AND (@OrderID = -1 OR OrderID=@OrderID)

GO


/*************************************************************/
-- Date Created: 2017/05/19 (4.7.1)
-- Author: Joe Engler
-- Purpose: Show driver settlement history
-- Changes:
--		4.7.2.2		JAE			2017-06-09		Use UNION ALL to keep repeated log entries
/*************************************************************/
ALTER FUNCTION fnDriverSettlementHistory (@BatchID INT, @OrderID INT) RETURNS TABLE AS
RETURN
	SELECT Description = 'Settled'
		, IsCurrent = CAST(1 AS BIT)
		, OSD.OrderID
		, OSD.BatchNum
		, O.OrderNum
		, JobNumber = NULLIF(O.JobNumber, '')
		, ContractNumber = NULLIF(O.ContractNumber, '')
		, OrderDate = O.OrderDate
		, Shipper = O.Customer
		, O.Carrier
		, Driver = O.OriginDriver
		, DriverFirst = OriginDriverFirst
		, DriverLast = OriginDriverLast
		, DriverGroup = OriginDriverGroup
		, O.Origin
		, O.Destination
		, PreviousDestinations = dbo.fnRerouteDetails(O.ID, 'PreviousDestinations', '<br/>')
		, O.ActualMiles
		, OSD.SettlementUnits
		, OSD.SettlementUom
		, OSD.OriginChainupAmount
		, OSD.DestChainupAmount
		, OSD.H2SAmount
		, OSD.RerouteAmount
		, OSD.OrderRejectAmount
		, OSD.OriginWaitAmount
		, OSD.DestinationWaitAmount
		, OSD.TotalWaitAmount
		, OSD.SplitLoadAmount
		, MiscDetails = dbo.fnOrderShipperAssessorialDetailsTSV(O.ID, 0)
		, MiscAmount = OSD.OtherAmount
		, OSD.FuelSurchargeAmount
		, OSD.OriginTaxRate
		, OSD.RouteRate
		, OSD.RateSheetRate
		, OSD.LoadAmount
		, OSD.TotalAmount
		, DSB.BatchDate
		, DSB.PeriodEndDate
		, DSB.InvoiceNum
		, OrderNotes = OSD.Notes
		, BatchNotes = DSB.Notes
		, SettledByUser = ISNULL(DSB.LastChangedByUser, DSB.CreatedByUser)
		, SettlementTimestamp = ISNULL(DSB.LastChangeDateUTC, DSB.CreateDateUTC)
	FROM viewOrderSettlementDriver OSD 
	JOIN viewOrder O ON OSD.OrderID = O.ID
	JOIN tblDriverSettlementBatch DSB ON DSB.ID = OSD.BatchID
	WHERE DSB.IsFinal = 1
  	AND (@BatchID = -1 OR Batchid = @BatchID)
	AND (@OrderID = -1 OR OrderID=@OrderID)

	UNION ALL

	SELECT Description = 'Settled', IsCurrent=0
		, OrderID
		, BatchNum
		, OrderNum
		, JobNumber = NULLIF(JobNumber, '')
		, ContractNumber = NULLIF(ContractNumber, '')
		, OrderDate
		, Shipper
		, Carrier
		, Driver
		, DriverFirst
		, DriverLast
		, DriverGroup
		, Origin
		, Destination
		, PreviousDestinations
		, ActualMiles
		, InvoiceUnits
		, InvoiceSettlementUom
		, InvoiceOriginChainupAmount
		, InvoiceDestChainupAmount
		, InvoiceH2SAmount
		, InvoiceRerouteAmount
		, InvoiceOrderRejectAmount
		, InvoiceOriginWaitAmount
		, InvoiceDestinationWaitAmount
		, InvoiceTotalWaitAmount
		, InvoiceSplitLoadAmount
		, InvoiceOtherDetailsTSV
		, InvoiceOtherAmount
		, InvoiceFuelSurchargeAmount
		, InvoiceTaxRate
		, InvoiceRouteRate
		, InvoiceRateSheetRate
		, InvoiceLoadAmount
		, InvoiceTotalAmount
		, BatchDate
		, InvoicePeriodEnd
		, BatchInvoiceNum
		, OrderNotes = AuditNotes
		, BatchNotes = InvoiceNotes
		, SettledByUser = ISNULL(BatchLastChangedByUser, BatchCreatedByUser)
		, SettlementTimestamp = ISNULL(BatchLastChangeDateUTC, BatchCreateDateUTC)
	FROM tblOrderSettlementDriverDbAudit 
	WHERE (@BatchID = -1 OR Batchid = @BatchID)
	AND (@OrderID = -1 OR OrderID=@OrderID)

	UNION ALL

	SELECT Description = 'Unsettled'
		, IsCurrent=0
		, OrderID
		, BatchNum
		, OrderNum
		, JobNumber = NULLIF(JobNumber, '')
		, ContractNumber = NULLIF(ContractNumber, '')
		, OrderDate
		, Shipper
		, Carrier
		, Driver
		, DriverFirst
		, DriverLast
		, DriverGroup
		, Origin
		, Destination
		, PreviousDestinations
		, ActualMiles
		, -InvoiceUnits
		, InvoiceSettlementUom
		, -InvoiceOriginChainupAmount
		, -InvoiceDestChainupAmount
		, -InvoiceH2SAmount
		, -InvoiceRerouteAmount
		, -InvoiceOrderRejectAmount
		, -InvoiceOriginWaitAmount
		, -InvoiceDestinationWaitAmount
		, -InvoiceTotalWaitAmount
		, -InvoiceSplitLoadAmount
		, InvoiceOtherDetailsTSV
		, -InvoiceOtherAmount
		, -InvoiceFuelSurchargeAmount
		, InvoiceTaxRate
		, InvoiceRouteRate
		, InvoiceRateSheetRate
		, -InvoiceLoadAmount
		, -InvoiceTotalAmount
		, BatchDate
		, InvoicePeriodEnd
		, BatchInvoiceNum
		, OrderNotes = AuditNotes
		, BatchNotes = InvoiceNotes
		, SettledByUser = ISNULL(BatchLastChangedByUser, BatchCreatedByUser)
		, SettlementTimestamp = DbAuditDate
	FROM tblOrderSettlementDriverDbAudit OSDa 
	WHERE (@BatchID = -1 OR Batchid = @BatchID)
	AND (@OrderID = -1 OR OrderID=@OrderID)

GO

/*************************************************************/
-- Date Created: 2017/05/19 (4.7.1)
-- Author: Joe Engler
-- Purpose: Show Shipper settlement history
-- Changes:
--		4.7.2.2		JAE			2017-06-09		Use UNION ALL to keep repeated log entries
/*************************************************************/
ALTER FUNCTION fnShipperSettlementHistory (@BatchID INT, @OrderID INT) RETURNS TABLE AS
RETURN
	SELECT Description = 'Settled'
		, IsCurrent = CAST(1 AS BIT)
		, OSS.OrderID
		, OSS.BatchNum
		, O.OrderNum
		, JobNumber = NULLIF(O.JobNumber, '')
		, ContractNumber = NULLIF(O.ContractNumber, '')
		, OrderDate = O.OrderDate
		, Shipper = O.Customer
		, O.Origin
		, O.Destination
		, PreviousDestinations = dbo.fnRerouteDetails(O.ID, 'PreviousDestinations', '<br/>') 
		, O.ActualMiles
		, OSS.SettlementUnits
		, OSS.SettlementUom
		, OSS.OriginChainupAmount
		, OSS.DestChainupAmount
		, OSS.H2SAmount
		, OSS.RerouteAmount
		, OSS.OrderRejectAmount
		, OSS.OriginWaitAmount
		, OSS.DestinationWaitAmount
		, OSS.TotalWaitAmount
		, OSS.SplitLoadAmount
		, MiscDetails = dbo.fnOrderShipperAssessorialDetailsTSV(O.ID, 0)
		, MiscAmount = OSS.OtherAmount
		, OSS.FuelSurchargeAmount
		, OSS.OriginTaxRate
		, OSS.RouteRate
		, OSS.RateSheetRate
		, OSS.LoadAmount
		, OSS.TotalAmount
		, SSB.BatchDate
		, SSB.PeriodEndDate
		, SSB.InvoiceNum
		, OrderNotes = OSS.Notes
		, BatchNotes = SSB.Notes
		, SettledByUser = ISNULL(SSB.LastChangedByUser, SSB.CreatedByUser)
		, SettlementTimestamp = ISNULL(SSB.LastChangeDateUTC, SSB.CreateDateUTC)
	FROM viewOrderSettlementShipper OSS 
	JOIN viewOrder O ON OSS.OrderID = O.ID
	JOIN tblShipperSettlementBatch SSB ON SSB.ID = OSS.BatchID
	WHERE SSB.IsFinal = 1
  	AND (@BatchID = -1 OR Batchid = @BatchID)
	AND (@OrderID = -1 OR OrderID=@OrderID)

	UNION ALL

	SELECT Description = 'Settled', IsCurrent=0
		, OrderID
		, BatchNum
		, OrderNum
		, JobNumber = NULLIF(JobNumber, '')
		, ContractNumber = NULLIF(ContractNumber, '')
		, OrderDate
		, Shipper
		, Origin
		, Destination
		, PreviousDestinations
		, ActualMiles
		, InvoiceUnits
		, InvoiceSettlementUom
		, InvoiceOriginChainupAmount
		, InvoiceDestChainupAmount
		, InvoiceH2SAmount
		, InvoiceRerouteAmount
		, InvoiceOrderRejectAmount
		, InvoiceOriginWaitAmount
		, InvoiceDestinationWaitAmount
		, InvoiceTotalWaitAmount
		, InvoiceSplitLoadAmount
		, InvoiceOtherDetailsTSV
		, InvoiceOtherAmount
		, InvoiceFuelSurchargeAmount
		, InvoiceTaxRate
		, InvoiceRouteRate
		, InvoiceRateSheetRate
		, InvoiceLoadAmount
		, InvoiceTotalAmount
		, BatchDate
		, InvoicePeriodEnd
		, BatchInvoiceNum
		, OrderNotes = AuditNotes
		, BatchNotes = InvoiceNotes
		, SettledByUser = ISNULL(BatchLastChangedByUser, BatchCreatedByUser)
		, SettlementTimestamp = ISNULL(BatchLastChangeDateUTC, BatchCreateDateUTC)
	FROM tblOrderSettlementShipperDbAudit
	WHERE (@BatchID = -1 OR Batchid = @BatchID)
	AND (@OrderID = -1 OR OrderID=@OrderID)

	UNION ALL

	SELECT Description = 'Unsettled'
		, IsCurrent=0
		, OrderID
		, BatchNum
		, OrderNum
		, JobNumber = NULLIF(JobNumber, '')
		, ContractNumber = NULLIF(ContractNumber, '')
		, OrderDate
		, Shipper
		, Origin
		, Destination
		, PreviousDestinations
		, ActualMiles
		, -InvoiceUnits
		, InvoiceSettlementUom
		, -InvoiceOriginChainupAmount
		, -InvoiceDestChainupAmount
		, -InvoiceH2SAmount
		, -InvoiceRerouteAmount
		, -InvoiceOrderRejectAmount
		, -InvoiceOriginWaitAmount
		, -InvoiceDestinationWaitAmount
		, -InvoiceTotalWaitAmount
		, -InvoiceSplitLoadAmount
		, InvoiceOtherDetailsTSV
		, -InvoiceOtherAmount
		, -InvoiceFuelSurchargeAmount
		, InvoiceTaxRate
		, InvoiceRouteRate
		, InvoiceRateSheetRate
		, -InvoiceLoadAmount
		, -InvoiceTotalAmount
		, BatchDate
		, InvoicePeriodEnd
		, BatchInvoiceNum
		, OrderNotes = AuditNotes
		, BatchNotes = InvoiceNotes
		, SettledByUser = ISNULL(BatchLastChangedByUser, BatchCreatedByUser)
		, SettlementTimestamp = DbAuditDate
	FROM tblOrderSettlementShipperDbAudit
	WHERE (@BatchID = -1 OR Batchid = @BatchID)
	AND (@OrderID = -1 OR OrderID=@OrderID)

GO


COMMIT
SET NOEXEC OFF
