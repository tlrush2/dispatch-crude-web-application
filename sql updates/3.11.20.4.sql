-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.11.20.3'
SELECT  @NewVersion = '3.11.20.4'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Fix 90-day window for settlement orders'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/***********************************
Date Created: 9 Mar 2013
Author: Kevin Alons
Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
Changes:
- 3.7.31	- 2015/06/19 - KDA	- reorder input parameters (move ProviderID down) and add @DriverGroupID parameter
- 3.9.0		- 2/15/08/14 - KDA	- return Approved column
- 3.9.19.6	- 2015/09/30 - KDA	- add OverrideXXX fields from OrderApproval table
- 3.9.21	- 2015/10/03 - KDA	- use [viewOrder] O.OriginDriverGroupID instead of O.DriverGroupID (which could be DestDriver.DriverGroupID)
- 3.9.34	- 2015/12/16 - BB	- Remove deleted orders from the results (DCWEB-851)
- 3.10.13.6 - 2015/03/01 - JAE	- add TruckTypeID
- 3.11.3.1	- 2016/03/22 - KDA	- fix to no-op when @BatchID = 0
- 3.11.19	- 2016/05/02 - BB	- Add Job number and Contract number
- 3.11.20	- 2016/05/04 - JAE	- 3 month limit was always being applied, skip if batch is provided
- 3.11.20.1 - 2016/05/04 - JAE	- Undoing change as timeouts reoccurring, need to investigate
- 3.11.20.3 - 2016/05/11 - KDA	- optimize @BatchID filtering to query directly from tblOrderSettlementCarrier table
								- use tblOrder and minimal JOINs intead of expensive viewOrder
***********************************/
ALTER PROCEDURE spRetrieveOrdersFinancialCarrier
(
  @StartDate date = NULL -- will default to first day of 3rd prior month for efficiency sake
, @EndDate date = NULL
, @ShipperID int = -1 -- all
, @CarrierID int = -1 -- all
, @ProductGroupID int = -1 -- all 
, @TruckTypeID int = -1 -- all
, @DriverGroupID int = -1 -- all
, @OriginStateID int = -1 -- all 
, @DestStateID int = -1 -- all
, @ProducerID int = -1 -- all
, @BatchID int = NULL -- either show unbatched (NULL), or the specified batch orders
, @OnlyShipperSettled bit = 0 
, @JobNumber varchar(20) = NULL -- 3.11.19
, @ContractNumber varchar(20) = NULL -- 3.11.19
) AS 
BEGIN
	-- validate the incoming parameters
	IF (@BatchID IS NOT NULL
		AND (@ShipperID <> -1 
			OR @CarrierID <> -1 
			OR @ProductGroupID <> -1
			OR @TruckTypeID <> -1
			OR @DriverGroupID <> -1
			OR @OriginStateID <> -1
			OR @DestStateID <> -1
			OR @ProducerID <> -1))
	BEGIN
		RAISERROR('@BatchID parameter cannot be specified with other parameters', 16, 1)
		RETURN
	END

	-- table variable to store the Order.ID values to return
	DECLARE @IDs IDTABLE

	-- if a @BatchID value was specified then just use this criteria alone
    IF @BatchID IS NOT NULL
	BEGIN
		INSERT INTO @IDs (ID)
			SELECT OrderID FROM tblOrderSettlementCarrier WHERE BatchID = @BatchID
	END
	ELSE
	BEGIN
		-- if no Start was specified, never return data earlier than 3 months ago
		IF (@StartDate IS NULL) SET @StartDate = dateadd(month, -3, dbo.fnFirstDOM(getdate()))
		-- retrieve the Order.ID values for the specified parameters
		INSERT INTO @IDs (ID)
			SELECT O.ID
			FROM tblOrder O
			LEFT JOIN dbo.tblOrderTransfer OTR ON OTR.OrderID = O.ID
			LEFT JOIN dbo.viewDriver vODR ON vODR.ID = OTR.OriginDriverID
			LEFT JOIN dbo.viewDriver vDDR ON vDDR.ID = O.DriverID
			LEFT JOIN dbo.tblDriverGroup ODG ON ODG.ID = vODR.DriverGroupID
			JOIN tblProduct P ON P.ID = O.ProductID
			JOIN tblTruck T ON T.ID = ISNULL(OTR.OriginTruckID, O.TruckID)
			JOIN viewOrigin OO ON OO.ID = O.OriginID
			JOIN tblDestination D ON D.ID = O.DestinationID
			LEFT JOIN tblOrderSettlementCarrier OSC ON OSC.OrderID = O.ID
			WHERE O.StatusID IN (4)  
			  AND O.DeleteDateUTC IS NULL  -- 3.9.34
			  AND OSC.BatchID IS NULL -- only get order if it's not part of a batch
			  AND (@ShipperID=-1 OR O.CustomerID=@ShipperID) 
			  AND (@CarrierID=-1 OR O.CarrierID=@CarrierID) 
			  AND (@ProductGroupID=-1 OR P.ProductGroupID=@ProductGroupID) 
			  AND (@TruckTypeID=-1 OR T.TruckTypeID=@TruckTypeID)
			  AND (@DriverGroupID=-1 OR ISNULL(vODR.DriverGroupID, vDDR.DriverGroupID) = @DriverGroupID) 
			  AND (@OriginStateID=-1 OR OO.StateID=@OriginStateID) 
			  AND (@DestStateID=-1 OR D.StateID=@DestStateID) 
			  AND (@ProducerID=-1 OR O.ProducerID=@ProducerID) 
			  AND (@StartDate IS NULL OR O.OrderDate >= @StartDate) 
			  AND (@EndDate IS NULL OR O.OrderDate <= @EndDate)
			  AND (ISNULL(@JobNumber,'') = '' OR O.JobNumber = @JobNumber)  -- 3.11.19
			  AND (ISNULL(@ContractNumber,'') = '' OR O.ContractNumber = @ContractNumber)  -- 3.11.19
	END

	-- retrieve/return the order financial data (done in 2 stages to reduce the load imposed by the "expensive" viewOrder_Financial_Carrier VIEW)
	SELECT DISTINCT OE.* 
		--, ShipperSettled = cast(CASE WHEN OSS.BatchID IS NOT NULL THEN 1 ELSE 0 END as bit)
		, InvoiceOtherDetailsTSV = dbo.fnOrderCarrierAssessorialDetailsTSV(OE.ID, 0)
		, InvoiceOtherAmountsTSV = dbo.fnOrderCarrierAssessorialAmountsTSV(OE.ID, 0)
		, Approved = cast(ISNULL(OA.Approved, 0) as bit)
		, FinalActualMiles = ISNULL(OA.OverrideActualMiles, OE.ActualMiles)
		, FinalOriginMinutes = ISNULL(OA.OverrideOriginMinutes, OE.OriginMinutes)
		, FinalDestMinutes = ISNULL(OA.OverrideDestMinutes, OE.DestMinutes)
		, FinalH2S = cast(CASE WHEN isnull(OA.OverrideH2S, 0) = 1 THEN 0 ELSE OE.H2S END as bit)
		, FinalChainup = cast(CASE WHEN isnull(OA.OverrideChainup, 0) = 1 THEN 0 ELSE OE.Chainup END as bit)
		, FinalRerouteCount = CASE WHEN isnull(OA.OverrideReroute, 0) = 1 THEN 0 ELSE OE.RerouteCount END
	FROM dbo.viewOrder_Financial_Carrier OE
	LEFT JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = OE.ID AND OSS.BatchID IS NOT NULL
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = OE.ID
	WHERE OE.ID IN (SELECT ID FROM @IDs)
	  AND (@OnlyShipperSettled = 0 OR OSS.BatchID IS NOT NULL)
	ORDER BY OE.OriginDepartTimeUTC

END

GO

/***********************************
Date Created: 9 Mar 2013
Author: Kevin Alons
Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
Changes
- 3.8.11	- 2015/07/28 - KDA	- remove OriginShipperRegion field
- 3.9.0		- 2/15/08/14 - KDA	- return Approved column
- 3.9.19.6	- 2015/09/30 - KDA	- add OverrideXXX fields from OrderApproval table
- 3.9.34	- 2015/12/16 - BB	- Remove deleted orders from the results (DCWEB-851)
- 3.10.13.6 - 2015/03/01 - JAE	- add TruckTypeID
- 3.11.3.1	- 2016/03/22 - KDA	- fix to no-op when @BatchID = 0
- 3.11.19	- 2016/05/02 - BB	- Add Job number and Contract number
- 3.11.20.3 - 2016/05/11 - KDA	- optimize @BatchID filtering to query directly from tblOrderSettlementShipper table
								- use tblOrder and minimal JOINs intead of expensive viewOrder
***********************************/
ALTER PROCEDURE spRetrieveOrdersFinancialShipper
(
  @StartDate date = NULL
, @EndDate date = NULL
, @ShipperID int = -1 -- all customers
, @ProductGroupID int = -1 -- all product groups
, @TruckTypeID int = -1 -- all truck types
, @ProducerID int = -1 -- all
, @OriginStateID int = -1 -- all 
, @DestStateID int = -1 -- all
, @BatchID int = NULL  -- either show unbatched (NULL), or the specified batch orders
, @JobNumber varchar(20) = NULL -- 3.11.19
, @ContractNumber varchar(20) = NULL -- 3.11.19
) AS 
BEGIN

	-- validate the incoming parameters
	IF (@BatchID IS NOT NULL
		AND (@ShipperID <> -1 
			OR @ProductGroupID <> -1
			OR @TruckTypeID <> -1
			OR @ProducerID <> -1
			OR @OriginStateID <> -1
			OR @DestStateID <> -1))
	BEGIN
		RAISERROR('@BatchID parameter cannot be specified with other parameters', 16, 1)
		RETURN
	END

	-- table variable to store the Order.ID values to return
	DECLARE @IDs IDTABLE

	-- if a @BatchID value was specified then just use this criteria alone
    IF @BatchID IS NOT NULL
	BEGIN
		INSERT INTO @IDs (ID)
			SELECT OrderID FROM tblOrderSettlementShipper WHERE BatchID = @BatchID
	END
	ELSE
	BEGIN
		-- if no Start was specified, never return data earlier than 3 months ago
		IF (@StartDate IS NULL) SET @StartDate = dateadd(month, -3, dbo.fnFirstDOM(getdate()))
		-- retrieve the Order.ID values for the specified parameters
		INSERT INTO @IDs (ID)
			SELECT O.ID
			FROM tblOrder O
			JOIN tblProduct P ON P.ID = O.ProductID
			LEFT JOIN dbo.tblOrderTransfer OTR ON OTR.OrderID = O.ID
			JOIN tblTruck T ON T.ID = ISNULL(OTR.OriginTruckID, O.TruckID)
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			JOIN tblDestination D ON D.ID = O.DestinationID
			LEFT JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = O.ID
			WHERE O.StatusID IN (4)  
			  AND O.DeleteDateUTC IS NULL  -- 3.9.34
			  AND OSS.BatchID IS NULL -- only get order if it's not part of a batch
			  AND (@ShipperID=-1 OR O.CustomerID=@ShipperID) 
			  AND (@ProductGroupID=-1 OR P.ProductGroupID=@ProductGroupID) 
			  AND (@TruckTypeID=-1 OR T.TruckTypeID=@TruckTypeID)
			  AND (@ProducerID=-1 OR O.ProducerID=@ProducerID) 
			  AND (@OriginStateID=-1 OR OO.StateID=@OriginStateID) 
			  AND (@DestStateID=-1 OR D.StateID=@DestStateID) 
			  AND (@StartDate IS NULL OR O.OrderDate >= @StartDate) 
			  AND (@EndDate IS NULL OR O.OrderDate <= @EndDate)
			  AND (ISNULL(@JobNumber,'') = '' OR O.JobNumber = @JobNumber)  -- 3.11.19
			  AND (ISNULL(@ContractNumber,'') = '' OR O.ContractNumber = @ContractNumber)  -- 3.11.19
	END

	-- retrieve/return the order financial data (done in 2 stages to reduce the load imposed by the "expensive" viewOrder_Financial_Shipper VIEW)
	SELECT DISTINCT OE.* 
		, InvoiceOtherDetailsTSV = dbo.fnOrderShipperAssessorialDetailsTSV(OE.ID, 0)
		, InvoiceOtherAmountsTSV = dbo.fnOrderShipperAssessorialAmountsTSV(OE.ID, 0)
		, Approved = cast(ISNULL(OA.Approved, 0) as bit)
		, FinalActualMiles = ISNULL(OA.OverrideActualMiles, OE.ActualMiles)
		, FinalOriginMinutes = ISNULL(OA.OverrideOriginMinutes, OE.OriginMinutes)
		, FinalDestMinutes = ISNULL(OA.OverrideDestMinutes, OE.DestMinutes)
		, FinalH2S = cast(CASE WHEN isnull(OA.OverrideH2S, 0) = 1 THEN 0 ELSE OE.H2S END as bit)
		, FinalChainup = cast(CASE WHEN isnull(OA.OverrideChainup, 0) = 1 THEN 0 ELSE OE.Chainup END as bit)
		, FinalRerouteCount = CASE WHEN isnull(OA.OverrideReroute, 0) = 1 THEN 0 ELSE OE.RerouteCount END
	FROM viewOrder_Financial_Shipper OE
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = OE.ID
	WHERE ID IN (SELECT ID FROM @IDs)
	ORDER BY OE.OriginDepartTimeUTC
END 

GO

/***********************************
Date Created: 3/31/2016
Author: Joe Engler
Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
			fields passed in are web filters, not best match criteria
Changes:
- 3.11.20.3 - 2016/05/11 - KDA	- optimize @BatchID filtering to query directly from tblOrderSettlementCarrier table
								- use tblOrder and minimal JOINs intead of expensive viewOrder
***********************************/
ALTER PROCEDURE spRetrieveOrdersFinancialProducer
(
  @StartDate date = NULL -- will default to first day of 6th prior month for efficiency sake
, @EndDate date = NULL
, @ShipperID int = -1 -- all
, @CarrierID int = -1 -- all
, @ProductGroupID int = -1 -- all 
, @TruckTypeID int = -1 -- all
, @DriverGroupID int = -1 -- all
, @OriginStateID int = -1 -- all 
, @DestStateID int = -1 -- all
, @ProducerID int = -1 -- all
, @BatchID int = NULL  -- either show unbatched (NULL), or the specified batch orders
) AS 
BEGIN
	-- validate the incoming parameters
	IF (@BatchID IS NOT NULL
		AND (@ShipperID <> -1 
			OR @CarrierID <> -1 
			OR @ProductGroupID <> -1
			OR @TruckTypeID <> -1
			OR @DriverGroupID <> -1
			OR @OriginStateID <> -1
			OR @DestStateID <> -1
			OR @ProducerID <> -1))
	BEGIN
		RAISERROR('@BatchID parameter cannot be specified with other parameters', 16, 1)
		RETURN
	END

	-- table variable to store the Order.ID values to return
	DECLARE @IDs IDTABLE

	-- if a @BatchID value was specified then just use this criteria alone
    IF @BatchID IS NOT NULL
	BEGIN
		INSERT INTO @IDs (ID)
			SELECT OrderID FROM tblOrderSettlementProducer WHERE BatchID = @BatchID
	END
	ELSE
	BEGIN
		-- if no Start was specified, never return data earlier than 6 months ago
		IF (@StartDate IS NULL) SET @StartDate = dateadd(month, -6, dbo.fnFirstDOM(getdate()))
		-- retrieve the Order.ID values for the specified parameters
		INSERT INTO @IDs (ID)
			SELECT O.ID
			FROM tblOrder O
			LEFT JOIN dbo.tblOrderTransfer OTR ON OTR.OrderID = O.ID
			LEFT JOIN dbo.viewDriver vODR ON vODR.ID = OTR.OriginDriverID
			LEFT JOIN dbo.viewDriver vDDR ON vDDR.ID = O.DriverID
			LEFT JOIN tblOrderSettlementProducer OSP ON OSP.OrderID = O.ID
			JOIN tblProduct P ON P.ID = O.ProductID
			JOIN tblTruck T ON T.ID = ISNULL(OTR.OriginTruckID, O.TruckID)
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			JOIN tblDestination D ON D.ID = O.DestinationID
			WHERE O.StatusID IN (4)  
			  AND O.DeleteDateUTC IS NULL  -- 3.9.34
			  AND OSP.BatchID IS NULL -- only get order if it's not part of a batch
			  AND (@ShipperID=-1 OR O.CustomerID=@ShipperID) 
			  AND (@CarrierID=-1 OR O.CarrierID=@CarrierID) 
			  AND (@ProductGroupID=-1 OR P.ProductGroupID=@ProductGroupID) 
			  AND (@TruckTypeID=-1 OR T.TruckTypeID=@TruckTypeID)
			  AND (@DriverGroupID=-1 OR ISNULL(vODR.DriverGroupID, vDDR.DriverGroupID) = @DriverGroupID) 
			  AND (@OriginStateID=-1 OR OO.StateID=@OriginStateID) 
			  AND (@DestStateID=-1 OR D.StateID=@DestStateID) 
			  AND (@ProducerID=-1 OR O.ProducerID=@ProducerID) 
			  AND (@StartDate IS NULL OR O.OrderDate >= @StartDate) 
			  AND (@EndDate IS NULL OR O.OrderDate <= @EndDate)
	END

	SELECT DISTINCT OE.* 
		, Approved = cast(ISNULL(OA.Approved, 0) as bit)
	FROM dbo.viewOrder_Financial_Producer OE
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = OE.ID
	WHERE ID IN (SELECT ID FROM @IDs)
	ORDER BY OE.OriginDepartTimeUTC
END

GO

COMMIT 
SET NOEXEC OFF