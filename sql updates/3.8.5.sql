-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.8.4.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.8.4.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0

DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.8.4'
SELECT  @NewVersion = '3.8.5'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Report Center: Added "Ticket Open Reading Combined (Gauge-Meter)" and "Ticket Close Reading Combined (Gauge-Meter)" columns.'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/*
	BB 7/10/15 - Add "Ticket Open Reading Combined (Gauge-Meter)" and "Ticket Close Reading Combined (Gauge-Meter)" columns
*/
SET IDENTITY_INSERT tblReportColumnDefinition ON
  INSERT tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport)
  SELECT 90024,1,'CASE WHEN TicketTypeID IN (1,7) THEN (T_OpenTotalQ/4.0) WHEN TicketTypeID IN (3,6) THEN T_OpenMeterUnits ELSE NULL END','TICKET | GENERAL | Ticket Open Reading Combined (Gauge-Meter)',NULL,NULL,0,NULL,1,'*',0  
  UNION
  SELECT 90025,1,'CASE WHEN TicketTypeID IN (1,7) THEN (T_CloseTotalQ/4.0) WHEN TicketTypeID IN (3,6) THEN T_CloseMeterUnits ELSE NULL END','TICKET | GENERAL | Ticket Close Reading Combined (Gauge-Meter)',NULL,NULL,0,NULL,1,'*',0  
  EXCEPT SELECT ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport FROM tblReportColumnDefinition
SET IDENTITY_INSERT tblReportColumnDefinition OFF
GO


COMMIT
SET NOEXEC OFF