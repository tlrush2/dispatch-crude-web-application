BEGIN TRANSACTION

UPDATE tblSetting SET Value = '1.1.20' WHERE ID=0
GO

UPDATE aspnet_Applications 
SET ApplicationName = 'DispatchCrude', LoweredApplicationName = 'dispatchcrude'
WHERE ApplicationName = 'Badlands_Enterprise'

/******************************************************/
-- Date Created: 16 May 2013
-- Author:		 Kevin Alons
-- Purpose:		 return the translated "friendly" info for a Reroute
/******************************************************/
CREATE VIEW viewReroute AS
SELECT ORR.ID
	, ORR.OrderID
	, PD.Name AS Destination
	, PD.DestinationType
	, ORR.Notes
	, ORR.UserName
	, ORR.RerouteDate
FROM tblOrderReroute  ORR
JOIN viewDestination PD ON PD.ID = ORR.PreviousDestinationID
GO
GRANT SELECT ON viewReroute TO dispatchcrude_iis_acct
GO

COMMIT