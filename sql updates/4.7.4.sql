SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.7.3'
SELECT  @NewVersion = '4.7.4'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'JT-3851 & JT-4142 - DVIR 2.1 Changes'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/* Create post-trip DVIR carrier rule */
INSERT INTO tblCarrierRuleType
	SELECT 27, 'Require Post-Trip DVIR', 2, 1, 'Flag to require driver to fill out a DVIR when going off duty for the day (or caught the next time driver goes on duty)'
GO

/* Add new fields to Questionnaire tables */
ALTER TABLE tblQuestionnaireTemplate ADD LoadDefaults BIT NOT NULL CONSTRAINT DF_QuestionnaireTemplate_LoadDefaults DEFAULT 0
GO
ALTER TABLE tblQuestionnaireQuestion ADD IsRequired BIT NOT NULL CONSTRAINT DF_QuestionnaireQuestion_IsRequired DEFAULT 1
GO
ALTER TABLE tblQuestionnaireQuestion ADD DefaultAnswer VARCHAR(255) NULL
GO
ALTER TABLE tblQuestionnaireQuestion ADD IncludeNotes BIT NOT NULL CONSTRAINT DF_QuestionnaireQuestion_IncludeNotes DEFAULT 0
GO
ALTER TABLE tblQuestionnaireResponse ADD Notes VARCHAR(255) NULL
GO

/* Make signature blobs not required for now (This solves a sync issue that happens when the signature/blob field is not required since it will now potentially be optional) */
ALTER TABLE tblQuestionnaireResponseBlob ALTER COLUMN AnswerBlob VARBINARY(MAX) NULL
GO


COMMIT
SET NOEXEC OFF