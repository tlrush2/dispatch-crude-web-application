-- select * from tblsetting where id = 0
/* 
	-- add new tblOrderTicket.OpenMeter | .CloseMeter | .MeterFactor fields
*/ 
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.1.8'
SELECT  @NewVersion = '3.1.9'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

ALTER TABLE tblOrderTicket ADD MeterFactor varchar(15)
ALTER TABLE tblOrderTicket ADD OpenMeterUnits decimal(9, 3)
ALTER TABLE tblOrderTicket ADD CloseMeterUnits decimal(9, 3)
GO

/***********************************************************/
-- Date Created: 9/1/2014
-- Author: Kevin Alons
-- Purpose: rebuild all Views, Procedures, Functions, Synonyms in the database
/***********************************************************/
ALTER PROCEDURE [dbo].[_spRebuildAllObjects] 
(
  @objectName varchar(255) = NULL
, @printNames bit = 0
, @printSql bit = 0
) AS
BEGIN
	SET NOCOUNT OFF

	SELECT dcount = CAST(NULL as int), *
	INTO #data
	FROM (
		SELECT ObjectName = TABLE_NAME, AlterSql = replace(OBJECT_DEFINITION(OBJECT_ID(TABLE_NAME)), 'CREATE VIEW', 'ALTER VIEW')
		FROM INFORMATION_SCHEMA.VIEWS
		UNION 
		SELECT ROUTINE_Name, replace(OBJECT_DEFINITION(object_id(ROUTINE_NAME)), 'CREATE PROCEDURE', 'ALTER PROCEDURE') 
		FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND ROUTINE_DEFINITION LIKE '%CREATE PROCEDURE%'
		UNION 
		SELECT ROUTINE_Name, replace(OBJECT_DEFINITION(object_id(ROUTINE_NAME)), 'CREATE FUNCTION', 'ALTER FUNCTION') 
		FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'FUNCTION' AND ROUTINE_DEFINITION LIKE '%CREATE FUNCTION%'
		UNION  
		SELECT name, 'DROP SYNONYM ' + name + '; CREATE SYNONYM ' + name + ' FOR ' + DB_NAME() + '.dbo.' + name + ';'
		FROM sys.objects where type = 'sn'
	) X1
	WHERE ObjectName NOT LIKE '%aspnet%' AND ObjectName NOT LIKE '[_]%' AND ObjectName NOT LIKE 'ELMAH%'
		AND @objectName IS NULL OR ObjectName LIKE @ObjectName

	-- mark the dcount = 0 for objects that aren't dependent on any other object
	UPDATE #data SET dcount = 0 WHERE ObjectName NOT IN (SELECT objectname = OBJECT_NAME(id) FROM sysdepends)

	-- walk the dependencies and mark the dcount (dependency count)
	DECLARE @dcount int; SET @dcount = 1
	WHILE EXISTS (SELECT * FROM #data WHERE dcount IS NULL) BEGIN
		UPDATE #data SET dcount = @dcount WHERE ObjectName IN (
			SELECT DISTINCT dependentname = D.objectname --, ND.name
			FROM (
				SELECT name  
				FROM sys.objects 
				WHERE @dcount = 1
				  AND type IN ('U','V', 'P', 'FN', 'IF', 'TF')
				  AND name NOT LIKE '[_]%'
				  AND object_id NOT IN (SELECT id FROM sysdepends) 
				UNION 
				SELECT ObjectName FROM #data WHERE dcount = @dcount - 1
			) ND
			JOIN (
				SELECT objectname = OBJECT_NAME(id), dep_objectname = OBJECT_NAME(depid) 
				FROM sysdepends
			) D ON D.dep_objectname = ND.name)
			
		SET @dcount = @dcount + 1
	END
	
	-- save the data ordered by dependency count, object name
	SELECT id = row_number() over (order by dcount, ObjectName), done = CAST(0 as bit), * INTO #update FROM #data
	
	DECLARE @id int, @sql nvarchar(max), @name varchar(255)
	SELECT TOP 1 @id = id, @sql = altersql, @name = @objectName FROM #update WHERE done = 0

	WHILE (@id IS NOT NULL) BEGIN
		IF (@printNames = 1) PRINT 'NAME=' + @Name
		IF (@printSql = 1) PRINT 'SQL=' + @sql
		EXEC sp_executesql @sql
		UPDATE #update SET done = 1 WHERE id = @id
		SET @id = null
		SELECT TOP 1 @id = id, @sql = altersql, @name = ObjectName FROM #update WHERE done = 0
	END

	IF (@printNames = 0 AND @printSql = 0)
		SELECT ltrim(COUNT(*)) + ' objects were REBUILT' FROM #data
END

GO

EXEC _spRebuildAllObjects
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 6 Feb 2013
-- Description:	trigger to ensure the entered values for an OrderTicket are actually valid
-- =============================================
ALTER TRIGGER [dbo].[trigOrderTicket_IU_Validate] ON [dbo].[tblOrderTicket] AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	PRINT 'trigOrderTicket_IU_Validate FIRED'
	
	IF (SELECT COUNT(*) FROM (
			SELECT OT.OrderID
			FROM tblOrderTicket OT
			JOIN inserted i ON i.OrderID = OT.OrderID AND i.CarrierTicketNum = OT.CarrierTicketNum
			WHERE OT.DeleteDateUTC IS NULL
			GROUP BY OT.OrderID, OT.CarrierTicketNum
			HAVING COUNT(*) > 1
		) v) > 0
	BEGIN
		RAISERROR('Duplicate Ticket Numbers are not allowed', 16, 1)
		RETURN
	END
	
	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (3) AND BOLNum IS NULL AND DeleteDateUTC IS NULL) > 0
	BEGIN
		RAISERROR('BOL # value is required for Meter Run tickets', 16, 1)
		RETURN
	END
	
	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1, 2) AND OriginTankID IS NULL AND TankNum IS NULL AND DeleteDateUTC IS NULL) > 0
	BEGIN
		RAISERROR('Tank ID value is required for Gauge Run & Net Volume tickets', 16, 1)
		RETURN
	END

	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1) AND CarrierTicketNum IS NULL AND DeleteDateUTC IS NULL) > 0
	BEGIN
		RAISERROR('Ticket # value is required for Gauge Run tickets', 16, 1)
		RETURN
	END

	IF (SELECT COUNT(*) FROM inserted WHERE DeleteDateUTC IS NULL
			AND (TicketTypeID IN (1, 2) AND (ProductObsTemp IS NULL OR ProductObsGravity IS NULL OR ProductBSW IS NULL))
				OR (TicketTypeID IN (1) AND (ProductHighTemp IS NULL OR ProductLowTemp IS NULL))
		) > 0
	BEGIN
		RAISERROR('All Product Measurement values are required for Gauge Run & Net Volume tickets', 16, 1)
		RETURN
	END
	
	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1) AND Rejected = 0 AND DeleteDateUTC IS NULL
		AND (OpeningGaugeFeet IS NULL OR OpeningGaugeInch IS NULL OR OpeningGaugeQ IS NULL)) > 0
	BEGIN
		RAISERROR('All Opening Gauge values are required for Gauge Run tickets', 16, 1)
		RETURN
	END
	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1) AND Rejected = 0 AND DeleteDateUTC IS NULL
		AND (ClosingGaugeFeet IS NULL OR ClosingGaugeInch IS NULL OR ClosingGaugeQ IS NULL)) > 0
	BEGIN
		RAISERROR('All Closing Gauge values are required for Gauge Run tickets', 16, 1)
		RETURN
	END

	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (2) AND Rejected = 0 AND DeleteDateUTC IS NULL
		AND (GrossUnits IS NULL)) > 0
	BEGIN
		RAISERROR('Gross Volume value is required for Net Volume tickets', 16, 1)
		RETURN
	END

	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (3) AND Rejected = 0 AND DeleteDateUTC IS NULL
		AND (GrossUnits IS NULL OR NetUnits IS NULL)) > 0
	BEGIN
		RAISERROR('Gross & Net Volume values are required for Meter Run tickets', 16, 1)
		RETURN
	END

	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1) AND Rejected = 0 AND DeleteDateUTC IS NULL
		AND (SealOff IS NULL OR SealOn IS NULL)) > 0
	BEGIN
		RAISERROR('All Seal Off & Seal On values are required for Gauge Run tickets', 16, 1)
		RETURN
	END

	PRINT 'trigOrderTicket_IU_Validate COMPLETE'

END

GO
EXEC sp_settriggerorder @triggername=N'[dbo].[trigOrderTicket_IU_Validate]', @order=N'First', @stmttype=N'INSERT'
GO
EXEC sp_settriggerorder @triggername=N'[dbo].[trigOrderTicket_IU_Validate]', @order=N'First', @stmttype=N'UPDATE'
GO
-- =============================================
-- Author:		Kevin Alons
-- Create date: 6 Feb 2013
-- Description:	trigger to ensure the GrossBarrels and NetBarrels are computed for valid, entered data
-- =============================================
ALTER TRIGGER [dbo].[trigOrderTicket_IU] ON [dbo].[tblOrderTicket] AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	-- Note: the TRIGGER_NESTLEVEL( OBJECT_ID('trigOrderTicket_IU') ) = 1 statement prevents recursion
	IF (TRIGGER_NESTLEVEL( OBJECT_ID('trigOrderTicket_IU')) = 1) BEGIN

		PRINT 'trigOrderTicket_IU FIRED'
		
		-- re-compute GaugeRun ticket Gross barrels
		IF UPDATE(OriginTankID) OR UPDATE(OpeningGaugeFeet) OR UPDATE(OpeningGaugeInch) OR UPDATE(OpeningGaugeQ)
			OR UPDATE (ClosingGaugeFeet) OR UPDATE(ClosingGaugeInch) OR UPDATE(ClosingGaugeQ) 
			OR UPDATE (GrossUnits)
		BEGIN
			UPDATE tblOrderTicket
			  SET GrossUnits = dbo.fnConvertUom(dbo.fnOriginTankStrappingLevelDeltaBarrels(
					OT.OriginTankID
				  , OpeningGaugeFeet
				  , OpeningGaugeInch
				  , OpeningGaugeQ
				  , ClosingGaugeFeet
				  , ClosingGaugeInch
				  , ClosingGaugeQ), 1, O.OriginUomID) -- ensure this value is always normalized from BARRELS
			FROM tblOrderTicket OT
			JOIN tblOrder O ON O.ID = OT.OrderID
			WHERE OT.ID IN (SELECT ID FROM inserted) 
			  AND OT.TicketTypeID = 1 -- Gauge Run Ticket
			  AND OT.OpeningGaugeFeet IS NOT NULL
			  AND OT.OpeningGaugeInch IS NOT NULL
			  AND OT.OpeningGaugeQ IS NOT NULL
			  AND OT.ClosingGaugeFeet IS NOT NULL
			  AND OT.ClosingGaugeInch IS NOT NULL
			  AND OT.ClosingGaugeQ IS NOT NULL
		END
		-- recompute the GrossUnits of any changed Meter Run tickets
		IF UPDATE(OpenMeterUnits) OR UPDATE(CloseMeterUnits) BEGIN
			UPDATE tblOrderTicket 
				SET GrossUnits = CloseMeterUnits - OpenMeterUnits
			WHERE OpenMeterUnits IS NOT NULL AND CloseMeterUnits IS NOT NULL
		END
		-- re-compute GaugeRun | Net Barrel | Meter Run tickets NetUnits
		IF UPDATE(OpeningGaugeFeet) OR UPDATE(OpeningGaugeInch) OR UPDATE(OpeningGaugeQ)
			OR UPDATE (ClosingGaugeFeet) OR UPDATE(ClosingGaugeInch) OR UPDATE(ClosingGaugeQ) 
			OR UPDATE(OriginTankID) OR UPDATE(GrossUnits) OR UPDATE(GrossStdUnits) OR UPDATE(NetUnits)
			OR UPDATE(ProductObsTemp) OR UPDATE(ProductObsGravity) OR UPDATE(ProductBSW)
			OR UPDATE(OpenMeterUnits) OR UPDATE(CloseMeterUnits)
		BEGIN
			UPDATE tblOrderTicket
			  SET NetUnits = dbo.fnCrudeNetCalculator(GrossUnits, ProductObsTemp, ProductObsGravity, ProductBSW)
				, GrossStdUnits = dbo.fnCrudeNetCalculator(GrossUnits, ProductObsTemp, ProductObsGravity, 0)
			WHERE ID IN (SELECT ID FROM inserted)
			  AND TicketTypeID IN (1,2)
			  AND GrossUnits IS NOT NULL
			  AND ProductObsTemp IS NOT NULL
			  AND ProductObsGravity IS NOT NULL
			  AND ProductBSW IS NOT NULL
		END
		
		-- ensure the Order record is in-sync with the Tickets
		UPDATE tblOrder 
		SET OriginGrossUnits = (SELECT sum(GrossUnits) FROM tblOrderTicket WHERE OrderID = O.ID AND Rejected = 0 AND DeleteDateUTC IS NULL)
		  , OriginNetUnits = (SELECT sum(NetUnits) FROM tblOrderTicket WHERE OrderID = O.ID AND Rejected = 0 AND DeleteDateUTC IS NULL)
		  , OriginGrossStdUnits = (SELECT sum(GrossStdUnits) FROM tblOrderTicket WHERE OrderID = O.ID AND Rejected = 0 AND DeleteDateUTC IS NULL)

			-- use the first MeterRun/BasicRun BOLNum as the Order BOL Num (favor the first un-Rejected)
		  , OriginBOLNum = (
				SELECT TOP 1 BOLNum FROM (
					SELECT TOP 1 BOLNum FROM tblOrderTicket 
					WHERE OrderID = O.ID AND TicketTypeID NOT IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
					UNION SELECT TOP 1 BOLNum FROM tblOrderTicket 
					WHERE OrderID = O.ID AND TicketTypeID NOT IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X)
			-- use the first GaugeRun|NetVolume CarrierTicketNum as the Order.CarrierTicketNum (favor first un-Rejected)
		  , CarrierTicketNum = (
				SELECT TOP 1 CarrierTicketNum FROM (
					SELECT TOP 1 CarrierTicketNum FROM tblOrderTicket 
					WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
					UNION SELECT TOP 1 CarrierTicketNum FROM tblOrderTicket 
					WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X)
			-- use the first GaugeRun|NetVolume OriginTankID for the Order (favor first un-Rejected ticket)
		  , OriginTankID = (
				SELECT TOP 1 OriginTankID FROM (
					SELECT TOP 1 OriginTankID FROM tblOrderTicket 
					WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
					UNION SELECT TOP 1 OriginTankID FROM tblOrderTicket 
					WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X)
			-- use the first GaugeRun|NetVolume OriginTankNum for the Order (favor first un-Rejected ticket)
		  , OriginTankNum = (
				SELECT TOP 1 OriginTankNum FROM (
					SELECT TOP 1 OriginTankNum FROM tblOrderTicket 
					WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
					UNION SELECT TOP 1 OriginTankNum FROM tblOrderTicket 
					WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X)
		  , LastChangeDateUTC = (SELECT MAX(isnull(LastChangeDateUTC, CreateDateUTC)) FROM inserted WHERE OrderID = O.ID)
		  , LastChangedByUser = (SELECT MIN(isnull(LastChangedByUser, CreatedByUser)) FROM inserted WHERE OrderID = O.ID)
		FROM tblOrder O
		WHERE ID IN (SELECT DISTINCT OrderID FROM inserted)

		PRINT 'trigOrderTicket_IU COMPLETE'
		
	END
	
END

GO

COMMIT TRAN
SET NOEXEC OFF