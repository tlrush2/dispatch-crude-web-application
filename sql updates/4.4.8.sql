SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20) 
SELECT @CurrVersion = '4.4.7'
SELECT  @NewVersion = '4.4.8'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'DCWEB-1973 Implement HOS Policy'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


CREATE TABLE tblHosPolicy
(
	/* converted from carrier rules, old CR # listed for each column */

	ID INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_HosPolicy PRIMARY KEY,
	Name VARCHAR(100) NOT NULL,
	Description VARCHAR(800) NULL,

	/* 8 */ OnDutyWeeklyLimit INT NOT NULL CONSTRAINT DF_HOSPolicy_OnDutyWeeklyLimit DEFAULT 60 * 60,
	/* 6 */ DrivingWeeklyLimit INT NOT NULL CONSTRAINT DF_HOSPolicy_DrivingWeeklyLimit DEFAULT 60 * 60,
	/* 7 */ OnDutyDailyLimit INT NOT NULL CONSTRAINT DF_HOSPolicy_OnDutyDailyLimit DEFAULT 14 * 60,
	/* 5 */ DrivingDailyLimit INT NOT NULL CONSTRAINT DF_HOSPolicy_DrivingDailyLimit DEFAULT 11 * 60,
	/* 18 */ DrivingBreakLimit INT NOT NULL CONSTRAINT DF_HOSPolicy_DrivingBreakLimit DEFAULT 8 * 60,
	PersonalDailyLimit INT NOT NULL CONSTRAINT DF_HOSPolicy_PersonalDailyLimit DEFAULT 0, -- personal conveyance

	/* 16 */ ShiftIntervalDays INT NOT NULL CONSTRAINT DF_HOSPolicy_ShiftIntervalDays DEFAULT 7, -- "work week"
	/* 12 */ WeeklyReset INT NOT NULL CONSTRAINT DF_HOSPolicy_WeeklyReset DEFAULT 34 * 60,
	/* 15 */ DailyReset INT NOT NULL CONSTRAINT DF_HOSPolicy_DailyReset DEFAULT 10 * 60,
	/* 17 */ SleeperBreak INT NOT NULL CONSTRAINT DF_HOSPolicy_SleeperBreak DEFAULT 8 * 60,
	/* 19 */ DrivingBreak INT NOT NULL CONSTRAINT DF_HOSPolicy_DrivingBreak DEFAULT 0.5 * 60,
	/* 23 */ SleeperBerthSplitTotal INT NOT NULL CONSTRAINT DF_HOSPolicy_SBSplitTotal DEFAULT 10 * 60, -- sleeper reset
	/* 24 */ SleeperBerthSplitMinimum INT NOT NULL CONSTRAINT DF_HOSPolicy_SBSplitMinimum DEFAULT 2 * 60, -- Qualified rest

	/* 9 */ WarnLogoff INT NOT NULL CONSTRAINT DF_HOSPolicy_WarnLogoff DEFAULT 0,
	/* 10 */ ForceLogoff INT NOT NULL CONSTRAINT DF_HOSPolicy_ForceLogoff DEFAULT 0,

	/* 11 */ DriverAppLogRetentionDays INT NOT NULL CONSTRAINT DF_HOSPolicy_LogRetention DEFAULT 30 * 6, -- 6 months

	CreateDateUTC DATETIME NOT NULL CONSTRAINT DF_HOSPolicy_CreateDateUTC DEFAULT GETUTCDATE(),
	CreatedByUser VARCHAR(100) NULL CONSTRAINT DF_HOSPolicy_CreatedByUser DEFAULT 'System',
	LastChangeDateUTC DATETIME NULL,
	LastChangedByUser VARCHAR(100) NULL,
	DeleteDateUTC DATETIME NULL,
	DeletedByUser VARCHAR(100) NULL,
)

-- Define custom default HOS policies
INSERT INTO tblHosPolicy (Name)
VALUES 
('DEFAULT HOS')

--INSERT INTO tblHOSPolicy VALUES
--('70-7 Texas', '', 60 * 60, 60 * 60, 14 * 60, 11 * 60, 8 * 60, 34 * 60, 7, 10 * 60, 8 * 60, 0.5 * 60, 8 * 60, 2 * 60, 0, 0, 30 * 6, 0, GETUTCDATE(), 'System', null, null, null, null)

GO

/***************************************************
-- Date Created: 2016-05-03
-- Author: Joe Engler
-- Purpose: Return HOS policies with easy to read hour formats
-- Changes:
***************************************************/
CREATE VIEW viewHosPolicy AS
SELECT *
      ,OnDutyWeeklyLimitHR = CAST(OnDutyWeeklyLimit / 60.0 AS FLOAT)
      ,DrivingWeeklyLimitHR = CAST(DrivingWeeklyLimit / 60.0 AS FLOAT)
      ,OnDutyDailyLimitHR = CAST(OnDutyDailyLimit / 60.0 AS FLOAT)
      ,DrivingDailyLimitHR = CAST(DrivingDailyLimit / 60.0 AS FLOAT)
      ,DrivingBreakLimitHR = CAST(DrivingBreakLimit / 60.0 AS FLOAT)

      ,WeeklyResetHR = CAST(WeeklyReset / 60.0 AS FLOAT)
      ,DailyResetHR = CAST(DailyReset / 60.0 AS FLOAT)
      ,SleeperBreakHR = CAST(SleeperBreak / 60.0 AS FLOAT)
      ,DrivingBreakHR = CAST(DrivingBreak / 60.0 AS FLOAT)
      ,SleeperBerthSplitTotalHR = CAST(SleeperBerthSplitTotal / 60.0 AS FLOAT)
      ,SleeperBerthSplitMinimumHR = CAST(SleeperBerthSplitMinimum / 60.0 AS FLOAT)
      ,WarnLogoffHR = CAST(WarnLogoff / 60.0 AS FLOAT)
      ,ForceLogoffHR = CAST(ForceLogoff / 60.0 AS FLOAT)

      ,PersonalDailyLimitHR = CAST(PersonalDailyLimit / 60.0 AS FLOAT)
  FROM tblHOSPolicy

GO


--Create a new SQL type for rules
INSERT INTO tblRuleType VALUES (7, 'SQL')

GO

/***************************************************
-- Date Created: 2016-05-03
-- Author: Joe Engler
-- Purpose: return all CarrierRule Value Dropdown values for the specified CarrierRuleID (no records if not relevant)
-- Changes:
--		x.x.x		JAE			2016-12-13		Added capability to do SQL filtering.  
													Currently uses value and not ID.  Trigger on policy table should prevent discrepancies when renamed
***************************************************/
ALTER PROCEDURE spCarrierRuleDropDownValues(@CarrierRuleTypeID int, @defaultValue varchar(255) = NULL OUTPUT) AS
BEGIN
	/* this statement (using SET ) will ensure @defaultValue IS NULL if no default is found */
	SET @defaultValue = (SELECT TOP 1 Value FROM tblCarrierRuleTypeDropDownValues WHERE CarrierRuleTypeID = @CarrierRuleTypeID AND IsDefault = 1)
	DECLARE @T1 TABLE (Value VARCHAR(255))

	DECLARE @sql VARCHAR(255)
	SELECT @sql = value + ' ORDER BY Value' FROM tblCarrierRuleTypeDropDownValues WHERE CarrierRuleTypeID = @CarrierRuleTypeID
		AND EXISTS (SELECT * FROM tblCarrierRuleType WHERE ID = @CarrierRuleTypeID AND RuleTypeID = 7) -- SQL
	INSERT INTO @T1 EXECUTE (@sql) -- SQL
	INSERT INTO @T1
	SELECT Value
	FROM (
		SELECT Value, SortNum FROM tblCarrierRuleTypeDropDownValues WHERE CarrierRuleTypeID = @CarrierRuleTypeID
		AND EXISTS (SELECT * FROM tblCarrierRuleType WHERE ID = @CarrierRuleTypeID AND RuleTypeID = 6) -- Drop down
		UNION 
		SELECT Value, SortNum FROM (
			SELECT Value = 'True', SortNum = 0 UNION SELECT 'False', 1
		) X WHERE EXISTS (SELECT * FROM tblCarrierRuleType WHERE ID = @CarrierRuleTypeID AND RuleTypeID = 2) -- Boolean
	) X
	ORDER BY SortNum

	SELECT Value FROM @T1
END

GO


-- =============================================
-- Author:		Joe Engler
-- Create date: 13 Dec 2016
-- Description:	trigger to ensure the HOS policies get updated in the carrier rules
-- =============================================
CREATE TRIGGER trigHosPolicy_IU ON tblHosPolicy AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	IF (UPDATE(Name))
	BEGIN
		-- update matching CarrierRule.Value to match what is assigned to the new Policy Name
		UPDATE tblCarrierRule 
		  SET Value = i.Name, LastChangeDateUTC = i.LastChangeDateUTC, LastChangedByUser = i.LastChangedByUser
		FROM tblCarrierRule CR
		JOIN deleted d ON d.Name = CR.Value
		JOIN inserted i ON d.ID = i.ID
		WHERE d.Name <> i.Name AND CR.TypeID = 1
	END
END

GO


-- Delete old carrier rules
DELETE FROM tblCarrierRule WHERE TypeID IN (SELECT ID FROM tblCarrierRuleType WHERE Name LIKE '%HOS%')

-- Delete old carrier rule definitions
DELETE FROM tblCarrierRuleType WHERE Name LIKE '%HOS - %'

GO


-- Update Current HOS Rule to be of new SQL type
UPDATE tblCarrierRuleType
SET Name = 'HOS Policy',
	RuleTypeID = 7,
	Description = 'Policy used to enable Hours Of Service (HOS) module'
 WHERE ID = 1

-- Insert SQL entry into Carrier Rule Drop Down table
INSERT INTO tblCarrierRuleTypeDropDownValues VALUES (1, 'SELECT Value=Name FROM tblHosPolicy', 0, 0, null)

GO


-- Insert an enforcement rule for driver scheduling (not truly part of a policy)
INSERT INTO tblCarrierRuleType 
VALUES
(15, 'Enforce HOS Hours', 2, 0, 'Flag to check driver schedule when dispatching orders')

GO



/********************************************
-- Date Created: 2016 Sep 21
-- Author: Joe Engler
-- Purpose: Get summary counts from HOS records and determine if violations occur.  Use passed date minus a shift for a starting point
-- Changes:
--		4.2.5		2016-10-27		JAE			Added Weekly OnDuty fields
--		4.3.1		2016-11-04		JAE			Fixed on duty daily hours to only include on duty (breaks should not be included)
--		4.4.6.3		2016-12-02		JAE			Alter how Sleeper Berth is calculated
--		x.x.x		2016-12-13		JAE			******************CHECK OTHER BRANCHES************** Updated script to read individual carrier rules from new policy table
********************************************/
ALTER FUNCTION fnHosViolationDetail(@DriverID INT, @StartDate DATETIME, @EndDate DATETIME)
RETURNS @ret TABLE
(
	DriverID INT,
	HosPolicyID INT,
	LogDateUTC DATETIME, 
	EndDateUTC DATETIME, 
	TotalHours FLOAT, 
	TotalHoursSleeping FLOAT,
	TotalHoursDriving FLOAT,
	SleeperStatusID INT,
	WeeklyReset BIT,
	DailyReset BIT,
	SleepBreak BIT, 
	DriverBreak BIT,
	Status VARCHAR(20),
	SleeperReset BIT,
	LastWeeklyReset DATETIME,
	LastDailyReset DATETIME,
	LastBreak DATETIME,
	LastSleeperBerthReset DATETIME,
	HoursSinceWeeklyReset FLOAT,
	OnDutyHoursSinceWeeklyReset FLOAT,
	DrivingHoursSinceWeeklyReset FLOAT,
	OnDutyHoursSinceDailyReset FLOAT,
	DrivingHoursSinceDailyReset FLOAT,
	OnDutyDailyLimit FLOAT,
	HoursSinceBreak FLOAT,
	OnDutyHoursLeft FLOAT,
	OnDutyViolation BIT,
	DrivingDailyLimit FLOAT,
	DrivingHoursLeft FLOAT,
	DrivingViolation BIT, 
	BreakLimit FLOAT,
	HoursTilBreak FLOAT,
	BreakViolation BIT,
	WeeklyOnDutyLimit FLOAT,
	WeeklyOnDutyHoursLeft FLOAT,
	WeeklyOnDutyViolation BIT,
	WeeklyDrivingLimit FLOAT,
	WeeklyDrivingHoursLeft FLOAT,
	WeeklyDrivingViolation BIT,
	OnDutyViolationDateUTC DATETIME,
	DrivingViolationDateUTC DATETIME,
	BreakViolationDateUTC DATETIME,
	WeeklyOnDutyViolationDateUTC DATETIME,
	WeeklyDrivingViolationDateUTC DATETIME
)
AS BEGIN


	DECLARE @HOSPolicyID INT 
	SELECT @HOSPolicyID = ISNULL(
		(SELECT p.ID FROM tblHosPolicy p
			WHERE Name = (SELECT cr.Value
					FROM viewDriverBase d
					CROSS APPLY dbo.fnCarrierRules(@EndDate, null, 1 /* HOS Policy Carrier Rule */, d.CarrierID, d.ID, d.OperatingStateID, d.RegionID, 1) cr
					WHERE d.id = @DriverID))
	, 1)

	DECLARE @WEEKLYRESET FLOAT
	DECLARE @DAILYRESET FLOAT
	DECLARE @SLEEPERBREAK FLOAT
	DECLARE @BREAK FLOAT
	DECLARE @SLEEPERRESET FLOAT
	DECLARE @QUALIFIEDREST FLOAT
	DECLARE @WEEK FLOAT
	DECLARE @ONDUTY_WEEKLY_LIMIT FLOAT
	DECLARE @DRIVING_WEEKLY_LIMIT FLOAT
	DECLARE @ONDUTY_DAILY_LIMIT FLOAT
	DECLARE @DRIVING_DAILY_LIMIT FLOAT
	DECLARE @DRIVING_BREAK_LIMIT FLOAT

	SELECT @WEEKLYRESET = WeeklyReset / 60.0,
			@DAILYRESET = DailyReset / 60.0,
			@SLEEPERBREAK = SleeperBreak / 60.0,
			@BREAK = DrivingBreak / 60.0,
			@SLEEPERRESET = SleeperBerthSplitTotal / 60.0,
			@QUALIFIEDREST = SleeperBerthSplitMinimum / 60.0,
			@WEEK = ShiftIntervalDays,
			@ONDUTY_WEEKLY_LIMIT = OnDutyWeeklyLimit / 60.0,
			@DRIVING_WEEKLY_LIMIT = DrivingWeeklyLimit / 60.0,
			@ONDUTY_DAILY_LIMIT = OnDutyDailyLimit / 60.0,
			@DRIVING_DAILY_LIMIT = DrivingDailyLimit / 60.0,
			@DRIVING_BREAK_LIMIT = DrivingBreakLimit / 60.0
			FROM tblHosPolicy
			WHERE ID = @HOSPolicyID

	IF @StartDate IS NULL
		SELECT @StartDate = DATEADD(DAY, -1, @EndDate) -- one day

	-- Get records up to previous shift for calculating violations, these older records will get filtered out later
	DECLARE @StartDateX DATETIME = DATEADD(DAY, -@WEEK, @StartDate) -- one shift


	-- Core HOS data with the basic info filled in (i.e. is this a weekly reset, daily reset, etc)
	-- Sleeper and Off Duty are grouped together for summing off duty blocks
	DECLARE @tmpHOS TABLE 
	(
		LogDateUTC DATETIME, 
		EndDateUTC DATETIME, 
		TotalHours FLOAT, 
		TotalHoursSleeping FLOAT,
		TotalHoursDriving FLOAT,
		SleeperStatusID INT,
		WeeklyReset INT,
		DailyReset INT,
		SleepBreak INT, -- As INT for multiplying as factor 
		DriverBreak INT,
		Status VARCHAR(20)
	)

	INSERT INTO @tmpHOS
	SELECT qq.*,
		WeeklyReset = CASE WHEN SleeperStatusID = 1 AND TotalHours >= @WEEKLYRESET THEN 1 ELSE 0 END,
		DailyReset = CASE WHEN SleeperStatusID = 1 AND TotalHours >= @DAILYRESET THEN 1 ELSE 0 END,
		SleepBreak = CASE WHEN SleeperStatusID = 2 AND TotalHours >= @SLEEPERBREAK THEN 1 
						 WHEN SleeperStatusID = 1 AND TotalHours >= @DAILYRESET THEN 1 ELSE 0 END,
		DriverBreak = CASE WHEN SleeperStatusID IN (1,2) AND TotalHours >= @BREAK THEN 1 ELSE 0 END,
		Status = CASE WHEN SleeperStatusID = 1 AND TotalHours >= @WEEKLYRESET THEN 'WEEKLY RESET'
							WHEN SleeperStatusID = 1 AND TotalHours >= @DAILYRESET THEN 'DAILY RESET' 
							WHEN SleeperStatusID = 2 AND TotalHours < @DAILYRESET AND TotalHours >= @SLEEPERBREAK THEN 'SLEEP BREAK'
							WHEN SleeperStatusID = 2 AND TotalHours < @DAILYRESET AND TotalHours >= @BREAK THEN 'BREAK' ELSE '' END
	FROM (
		SELECT LogDateUTC = MIN(LogDateUTC), EndDateUTC = MAX(EndDateUTC), TotalHours = SUM(TotalHours), 
			TotalHoursSleeping = SUM(CASE WHEN HosDriverStatusID = 2 THEN TotalHours ELSE NULL END),
			TotalHoursDriving = SUM(CASE WHEN HosDriverStatusID = 3 THEN TotalHours ELSE NULL END),
			SleeperStatusID = CASE WHEN SleeperStatusID = 1 AND SUM(TotalHours) < @DAILYRESET THEN 2 --break
								ELSE SleeperStatusID END
		FROM
		(
			SELECT LogDateUTC, EndDateUTC, 
				TotalHours, 
				SleeperStatusID, HosDriverStatusID,
				Streak = (SELECT COUNT(*) FROM dbo.fnHosSummary(@driverid, @StartDateX, @EndDate) x
								WHERE x.SleeperStatusID <> y.SleeperStatusID AND x.LogDateUTC <= y.LogDateUTC) -- used just to group streaks
			FROM dbo.fnHosSummary(@DriverID, @StartDateX, @EndDate) y
		) Q
		GROUP BY Streak, SleeperStatusID
	) QQ
	WHERE TotalHours > 0 -- Ignore any "quick" changes
	ORDER BY LogDateUTC


	------------------------------------------

	-- Get core HOS data with "last" dates and sleeper reset information
	DECLARE @tmpHOSwithSleeperReset TABLE 
	(
		LogDateUTC DATETIME, 
		EndDateUTC DATETIME, 
		TotalHours FLOAT, 
		TotalHoursSleeping FLOAT,
		TotalHoursDriving FLOAT,
		SleeperStatusID INT,
		WeeklyReset INT,
		DailyReset INT,
		SleepBreak INT, -- As INT for multiplying as factor 
		DriverBreak INT,
		Status VARCHAR(20),
		SleeperReset INT,
		LastWeeklyReset DATETIME,
		LastDailyReset DATETIME,
		LastBreak DATETIME
	)

	INSERT INTO @tmpHOSwithSleeperReset 
	(
		LogDateUTC,
		EndDateUTC, 
		TotalHours, 
		TotalHoursSleeping,
		TotalHoursDriving,
		SleeperStatusID,
		WeeklyReset,
		DailyReset,
		SleepBreak, 
		DriverBreak,
		Status,
		SleeperReset,
		LastWeeklyReset,
		LastDailyReset,
		LastBreak
	)
	SELECT hos.*, 
		SleeperReset = CASE WHEN lsb.Hrs >= @SLEEPERRESET OR hos.DailyReset = 1 THEN 1 ELSE 0 END,

		LastWeeklyReset = ISNULL(lr.EndDateUTC, @StartDateX),
		LastDailyReset = COALESCE(ldr.EndDateUTC, lr.EndDateUTC, @StartDateX),
		LastBreak = COALESCE(lb.EndDateUTC, ldr.EndDateUTC, lr.EndDateUTC, @StartDateX)

	FROM @tmphos hos
	-- Last Weekly Reset
	OUTER APPLY (SELECT TOP 1 EndDateUTC 
					FROM @tmphos
					WHERE WeeklyReset = 1 AND LogDateUTC < hos.LogDateUTC 
					ORDER BY LogDateUTC DESC) lr
	-- Last Daily Reset
	OUTER APPLY (SELECT TOP 1 EndDateUTC 
					FROM @tmphos
					WHERE DailyReset = 1 AND LogDateUTC < hos.LogDateUTC 
					ORDER BY LogDateUTC DESC) ldr
	-- Last Break
	OUTER APPLY (SELECT TOP 1 EndDateUTC 
					FROM @tmphos
					WHERE DriverBreak = 1 AND LogDateUTC < hos.LogDateUTC 
					ORDER BY LogDateUTC DESC) lb
	-- Last Sleeper Berth Reset (used to see if a reset, date is grabbed later since you use the previous break)
	OUTER APPLY (SELECT EndDateUTC = MIN(EndDateUTC), 
						Hrs = SUM(ISNULL(TotalHoursSleeping,0)) --*MAX(ISNULL(SleepBreak,0)) -- Multiply by sleep break to ensure one of the entries is an 8 hr block
					FROM @tmphos
					WHERE DriverBreak = 1  AND logDateUTC < hos.EndDateUTC 
					AND TotalHoursSleeping >= @QUALIFIEDREST -- Both must be a qualified rest break to count
					AND EndDateUTC >= COALESCE(lb.EndDateUTC, ldr.EndDateUTC)) lsb -- Since Last Daily Reset


	------------------------------------------


	-- Get detail HOS information with total hours and violations
	INSERT INTO @ret (
		DriverID,
		HOSPolicyID,
		LogDateUTC, 
		EndDateUTC, 
		TotalHours, 
		TotalHoursSleeping,
		TotalHoursDriving,
		SleeperStatusID,
		WeeklyReset,
		DailyReset,
		SleepBreak, 
		DriverBreak,
		Status,
		SleeperReset,
		LastWeeklyReset,
		LastDailyReset,
		LastBreak,
		LastSleeperBerthReset,
		HoursSinceWeeklyReset,
		OnDutyHoursSinceWeeklyReset,
		DrivingHoursSinceWeeklyReset,
		OnDutyHoursSinceDailyReset,
		DrivingHoursSinceDailyReset,
		HoursSinceBreak,
		OnDutyDailyLimit,
		OnDutyHoursLeft,
		OnDutyViolation,
		DrivingDailyLimit,
		DrivingHoursLeft,
		DrivingViolation,
		BreakLimit,
		HoursTilBreak,
		BreakViolation,
		WeeklyOnDutyLimit,
		WeeklyOnDutyHoursLeft,
		WeeklyOnDutyViolation,
		WeeklyDrivingLimit,
		WeeklyDrivingHoursLeft,
		WeeklyDrivingViolation,
		OnDutyViolationDateUTC,
		DrivingViolationDateUTC,
		BreakViolationDateUTC,
		WeeklyOnDutyViolationDateUTC,
		WeeklyDrivingViolationDateUTC
	)
	SELECT @DriverID,
		@HOSPolicyID,
		*,
		OnDutyViolationDateUTC = CASE WHEN OnDutyViolation = 0 THEN NULL
				ELSE DATEADD(MINUTE, 60*(TotalHours - (OnDutyHoursSinceDailyReset - OnDutyDailyLimit)), LogDateUTC) END,
		DrivingViolationDateUTC = CASE WHEN DrivingViolation = 0 THEN NULL
				ELSE dbo.fnMaxDateTime(DATEADD(MINUTE, 60*(TotalHours - (DrivingHoursSinceDailyReset - DrivingDailyLimit)), LogDateUTC), LogDateUTC) END, -- max of time or start of driving time (violation doesnt occur until you start driving)
		BreakViolationDateUTC = CASE WHEN BreakViolation = 0 THEN NULL
				ELSE dbo.fnMaxDateTime(DATEADD(MINUTE, 60*(TotalHours + HoursTilBreak), LogDateUTC), LogDateUTC) END, -- max of time or start of driving time (violation doesnt occur until you start driving)
		WeeklyOnDutyViolationDateUTC = CASE WHEN WeeklyOnDutyViolation = 0 THEN NULL
				ELSE DATEADD(MINUTE, 60*(TotalHours - (OnDutyHoursSinceWeeklyReset - WeeklyOnDutyLimit)), LogDateUTC) END,
		WeeklyDrivingViolationDateUTC = CASE WHEN WeeklyDrivingViolation = 0 THEN NULL
				ELSE DATEADD(MINUTE, 60*(TotalHours - (DrivingHoursSinceWeeklyReset - WeeklyDrivingLimit)), LogDateUTC) END
	FROM 
	(
		SELECT hos.*,
			OnDutyDailyLimit = @ONDUTY_DAILY_LIMIT,
			OnDutyHoursLeft = CASE WHEN @ONDUTY_DAILY_LIMIT < OnDutyHoursSinceDailyReset THEN 0 ELSE @ONDUTY_DAILY_LIMIT - OnDutyHoursSinceDailyReset END,
			OnDutyViolation = CASE WHEN @ONDUTY_DAILY_LIMIT < OnDutyHoursSinceDailyReset AND SleeperStatusID IN (3,4) THEN 1 ELSE 0 END,

			DrivingDailyLimit = @DRIVING_DAILY_LIMIT,
			DrivingHoursLeft = CASE WHEN @DRIVING_DAILY_LIMIT < DrivingHoursSinceDailyReset THEN 0 ELSE @DRIVING_DAILY_LIMIT - DrivingHoursSinceDailyReset END,
			DrivingViolation = CASE WHEN @DRIVING_DAILY_LIMIT < DrivingHoursSinceDailyReset AND SleeperStatusID = 3 THEN 1 ELSE 0 END,

			BreakLimit = @DRIVING_BREAK_LIMIT,
			HoursTilBreak = @DRIVING_BREAK_LIMIT - HoursSinceBreak,
			BreakViolation = CASE WHEN @DRIVING_BREAK_LIMIT < HoursSinceBreak AND SleeperStatusID = 3 THEN 1 ELSE 0 END,

			WeeklyOnDutyLimit = @ONDUTY_WEEKLY_LIMIT,
			WeeklyOnDutyHoursLeft = CASE WHEN @ONDUTY_WEEKLY_LIMIT < OnDutyHoursSinceWeeklyReset THEN 0 ELSE @ONDUTY_WEEKLY_LIMIT - OnDutyHoursSinceWeeklyReset END,
			WeeklyOnDutyViolation = CASE WHEN @ONDUTY_WEEKLY_LIMIT < OnDutyHoursSinceWeeklyReset AND SleeperStatusID IN (3,4) THEN 1 ELSE 0 END,

			WeeklyDrivingLimit = @DRIVING_WEEKLY_LIMIT,
			WeeklyDrivingHoursLeft = CASE WHEN @DRIVING_WEEKLY_LIMIT < DrivingHoursSinceWeeklyReset THEN 0 ELSE @DRIVING_WEEKLY_LIMIT - DrivingHoursSinceWeeklyReset END,
			WeeklyDrivingViolation = CASE WHEN @DRIVING_WEEKLY_LIMIT < DrivingHoursSinceWeeklyReset AND SleeperStatusID = 3 THEN 1 ELSE 0 END
		FROM
		(
			SELECT hos.*,
				HoursSinceWeeklyReset = 
							CASE WHEN WeeklyReset = 1 THEN 0
							ELSE ISNULL((SELECT SUM(TotalHours) FROM @tmphos WHERE LogDateUTC >= LastWeeklyReset AND LogDateUTC < hos.EndDateUTC), 0) END,

				OnDutyHoursSinceWeeklyReset = 
							CASE WHEN WeeklyReset = 1 THEN 0
							ELSE ISNULL((SELECT SUM(TotalHours) FROM @tmphos WHERE LogDateUTC >= LastWeeklyReset AND LogDateUTC < hos.EndDateUTC 
																			AND SleeperStatusID IN (3,4)), 0) END,
				DrivingHoursSinceWeeklyReset = 
							CASE WHEN WeeklyReset = 1 THEN 0
							ELSE ISNULL((SELECT SUM(TotalHours) FROM @tmphos WHERE LogDateUTC >= LastWeeklyReset AND LogDateUTC < hos.EndDateUTC 
																			AND SleeperStatusID = 3), 0) END,
				OnDutyHoursSinceDailyReset = 
							CASE WHEN DailyReset = 1 THEN 0
							ELSE ISNULL((SELECT SUM(TotalHours) FROM @tmphos WHERE LogDateUTC >= LastSleeperBerthReset AND LogDateUTC < hos.EndDateUTC 
																			--AND SleepBreak = 0 -- Skip sleeper block
																			AND SleeperStatusID IN (3,4)), 0) END, 
				DrivingHoursSinceDailyReset = 
							CASE WHEN DailyReset = 1 THEN 0
							ELSE ISNULL((SELECT SUM(TotalHours) FROM @tmphos WHERE LogDateUTC >= LastSleeperBerthReset AND LogDateUTC < hos.EndDateUTC 
																			AND SleeperStatusID = 3), 0) END,
				HoursSinceBreak = 
							CASE WHEN DriverBreak = 1 THEN 0
							ELSE ISNULL((SELECT SUM(TotalHours) FROM @tmphos WHERE SleepBreak = 0 AND LogDateUTC >= LastBreak AND LogDateUTC < hos.EndDateUTC),0) END
			FROM 
			(
				SELECT hos.*,
					LastSleeperBerthReset = dbo.fnMaxDateTime(lb.LastBreak, LastDailyReset) -- get more recent between sleeper reset or daily reset

				FROM @tmpHOSwithSleeperReset hos
				-- Last Sleeper Berth Reset
				OUTER APPLY (SELECT TOP 1 LastBreak
									FROM @tmpHOSwithSleeperReset
									WHERE SleeperReset = 1 AND LogDateUTC < hos.LogDateUTC
									ORDER BY LogDateUTC DESC) lb
			) hos
		) hos
	) Q
	WHERE EndDateUTC > @StartDate -- Filter older records used for calculating violations

	RETURN
END

GO


-- Add new permissions for the HOS Policy page
EXEC spAddNewPermission 'viewHOSPolicy', 'Allow user to view HOS policiess', 'Compliance', 'HOS - View Policies'
GO
EXEC spAddNewPermission 'createHOSPolicy', 'Allow user to create HOS policiess', 'Compliance', 'HOS - Create Policies'
GO
EXEC spAddNewPermission 'editHOSPolicy', 'Allow user to edit HOS policiess', 'Compliance', 'HOS - Edit Policies'
GO
EXEC spAddNewPermission 'deactivateHOSPolicy', 'Allow user to deactivate HOS policiess', 'Compliance', 'HOS - Deactivate Policies'
GO



COMMIT
SET NOEXEC OFF
