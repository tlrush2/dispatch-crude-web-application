BEGIN TRANSACTION
GO

UPDATE tblSetting SET Value = '1.1.21' WHERE ID=0
GO

UPDATE aspnet_Roles
SET RoleName = 'Management', LoweredRoleName = 'management'
WHERE RoleName = 'BadlandsManagement'

UPDATE aspnet_Roles
SET RoleName = 'Logistics', LoweredRoleName = 'logistics'
WHERE RoleName = 'BadlandsLogistics'

COMMIT