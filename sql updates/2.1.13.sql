/* add MobileApp.'Enforce Password Hash Validation' setting value
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.1.12'
SELECT  @NewVersion = '2.1.13'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

-- DO THE ACTUAL UPDATE HERE
/***********************************/

INSERT INTO tblSetting (ID, Name, SettingTypeID, Value, Category, CreateDateUTC, CreatedByUser)
	SELECT 13, 'Enforce Password Hash Validation (Single Login)', 2, 'false', 'Driver App Settings', GETUTCDATE(), 'System'
GO

/*******************************************/
-- Date Created: 22 Apr 2013
-- Author: Kevin Alons
-- Purpose: validate parameters, if valid insert/update the tblDriver_Sync table for the specified DriverID
/*******************************************/
ALTER PROCEDURE [dbo].[spDriver_Sync]
(
  @UserName varchar(100)
, @DriverID int
, @SyncDateUTC datetime = NULL
, @PasswordHash varchar(25) = NULL
, @Valid bit = NULL output
, @Message varchar(255) = NULL output
) AS
BEGIN
	-- if resetting, delete the entire record (it will be recreated below)
	IF (@SyncDateUTC IS NULL)
	BEGIN
		DELETE FROM tblDriver_Sync WHERE DriverID = @DriverID
		-- result of count(*) query will be 1 or 0 (true or false respectively)
		SELECT @Valid = (SELECT COUNT(*) FROM tblDriver WHERE ID = @DriverID)
		IF (@Valid = 0)
			SELECT @Message = 'DriverID was not valid'
	END
	ELSE
	BEGIN
		-- query the ValidatePasswordHash setting parameter
		DECLARE @ValidatePasswordHash bit
		SELECT @ValidatePasswordHash = CASE WHEN Value IN ('1', 'true', 'yes') THEN 1 ELSE 0 END
		FROM tblSetting
		WHERE ID = 13

		-- result of count(*) query will be 1 or 0 (true or false respectively)
		SELECT @Valid = (SELECT count(*) FROM tblDriver_Sync WHERE DriverID = @DriverID 
			AND (@ValidatePasswordHash = 0 OR PasswordHash = @PasswordHash))
		IF (@Valid = 0)
			SELECT @Message = 'PasswordHash was not valid'
	END
	
	IF (@Valid = 1)
	BEGIN
		-- if a sync record already exists, just update the new LastSync value
		UPDATE tblDriver_Sync SET LastSyncUTC = @SyncDateUTC WHERE DriverID = @DriverID
		-- otherwise insert a new record with a new passwordhash value
		INSERT INTO tblDriver_Sync (DriverID, LastSyncUTC, PasswordHash)
			SELECT @DriverID, NULL, dbo.fnGeneratePasswordHash()
			FROM tblDriver D
			LEFT JOIN tblDriver_Sync DS ON DS.DriverID = D.ID
			WHERE D.ID = @DriverID AND DS.DriverID IS NULL

		-- return the current "Master" data
		SELECT * FROM dbo.fnDriverMasterData(@Valid, @DriverID, @UserName)
	END
	ELSE
	BEGIN
		SELECT @Valid AS Valid, @UserName AS UserName, 0 AS DriverID, NULL AS DriverName, 0 AS MobilePrint
			, NULL AS LastSyncUTC
			, (SELECT cast(Value as int) FROM tblSetting WHERE ID = 11) AS SyncMinutes
			, (SELECT Value FROM tblSetting WHERE ID = 0) AS SchemaVersion
			, (SELECT Value FROM tblSetting WHERE ID = 12) AS LatestAppVersion
			, NULL AS PasswordHash
		FROM tblSetting S WHERE S.ID = 0
	END 
END

GO

COMMIT
SET NOEXEC OFF