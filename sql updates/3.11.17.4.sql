SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.11.17.3'
SELECT  @NewVersion = '3.11.17.4'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1117: Correct commodity price view'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/******************************************************
-- Date Created: 2016-03-15
-- Author: Joe Engler
-- Purpose: Add computed fields to the CommodityPurchasePricebook table data
-- Changes: 
-          3.11.17.4 - JAE - 4/26/2016 - Had bits backward
******************************************************/
ALTER VIEW viewCommodityPrice AS
	SELECT CP.*
		 , Locked = cast(CASE WHEN CPP.ID IS NULL THEN 0 ELSE 1 END as bit)
	FROM tblCommodityPrice CP
	LEFT JOIN viewCommodityPurchasePricebook CPP ON CPP.CommodityIndexID = CP.CommodityIndexID AND CP.PriceDate BETWEEN CPP.IndexStartDate AND CPP.IndexEndDate AND CPP.Locked = 1
GO


/* Terminology fix on tblCommodityMethod */
UPDATE tblCommodityMethod 
SET Description = 'Calendar Month Average' 
WHERE ID = 1
GO


/* Increase precision on two columns tblCommodityPurchasePricebook */
ALTER TABLE tblCommodityPurchasePricebook ALTER COLUMN Deduct DECIMAL(8,3)
GO
ALTER TABLE tblCommodityPurchasePricebook ALTER COLUMN Premium DECIMAL(8,3)
GO


-- Add new roles for commodity pricing and producer settlement
INSERT INTO ASPNET_ROLES
select applicationid, NEWID(), 'manageCommodityPricing', LOWER('manageCommodityPricing'), 'View and edit commodity pricing pages' from aspnet_applications 
where not exists (select 1 from aspnet_roles where rolename = 'manageCommodityPricing')

INSERT INTO ASPNET_ROLES
select applicationid, NEWID(), 'settleProducer', LOWER('settleProducer'), 'Settle orders for producers' from aspnet_applications 
where not exists (select 1 from aspnet_roles where rolename = 'settleProducer')


/* Add new permissions to current Administrator users on each site */
EXEC spAddUserPermissionsByRole 'Administrator', 'manageCommodityPricing'
GO
EXEC spAddUserPermissionsByRole 'Administrator', 'settleProducer'
GO


COMMIT 
SET NOEXEC OFF