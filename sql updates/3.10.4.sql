SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.10.3'
SELECT  @NewVersion = '3.10.4'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Add VIEW viewDriverSyncLog'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

EXEC _spDropView 'viewDriverSyncLog'
GO
/*************************************
 Creation Info: 3.10.4 - 2016/01/29
 Author: Kevin Alons
 Purpose: return tblDriverSyncLog records with computed DurationSeconds field
 Changes:
*************************************/
CREATE VIEW viewDriverSyncLog AS
SELECT *, DurationSeconds = cast(datediff(ms, SyncStart, SyncDate) * 1.0 / 1000 as decimal(18, 3))
FROM tblDriverSyncLog
GO

COMMIT
SET NOEXEC OFF