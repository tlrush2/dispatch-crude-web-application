/* revise the DriverApp table-functions to split order into OrderReadOnly & OrderEdit
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.0.4', @NewVersion = '2.0.5'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

/***********************************/
-- Date Created: 28 Feb 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc for 3rd parties (Customer/Carrier)
/***********************************/
ALTER PROCEDURE [dbo].[spOrderTicketFullExport]
(
  @StartDate datetime
, @EndDate datetime
, @CarrierID int = 0 -- -1 = all carriers
, @CustomerID int = 0 -- -1 = all customers
) AS BEGIN
	SELECT @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	
	SELECT OT.ID
		, O.OrderNum
		, O.OrderStatus
		, O.OrderDate
		, OT.TicketType 
		, OO.WellAPI
		, O.OriginDepartTimeUTC
		, OT.CarrierTicketNum AS TicketNum
		, OO.Name AS Origin
		, OO.LeaseName
		, O.Operator
		, O.Destination
		, OT.NetBarrels
		, OT.GrossBarrels
		, TT.HeightFeet AS TankHeight
		, TT.CapacityBarrels AS TankBarrels
		, OT.TankNum
		, OT.ProductObsGravity
		, OT.ProductObsTemp
		, OT.ProductBSW
		, isnull(OT.ProductHighTemp, OT.ProductObsTemp) AS ProductHighTemp
		, OT.OpeningGaugeFeet
		, OT.OpeningGaugeInch
		, OT.OpeningGaugeQ
		, OT.ClosingGaugeFeet
		, OT.ClosingGaugeInch
		, OT.ClosingGaugeQ
		, isnull(OT.ProductLowTemp, OT.ProductObsTemp) AS ProductLowTemp
		, dbo.fnGaugeQtrInches(OT.OpeningGaugeFeet, OT.OpeningGaugeInch, OT.OpeningGaugeQ) AS OpenTotalQ
		, dbo.fnGaugeQtrInches(OT.ClosingGaugeFeet, OT.ClosingGaugeInch, ot.ClosingGaugeQ) AS CloseTotalQ
	FROM dbo.viewOrderTicket OT
	JOIN dbo.viewOrder O ON O.ID = OT.OrderID
	JOIN dbo.tblOrigin OO ON OO.ID = O.OriginID
	LEFT JOIN dbo.tblTankType TT ON TT.ID = OT.TankTypeID
	WHERE O.StatusID IN (3, 4) -- Delivered & Audited
	  AND (@CarrierID=-1 OR @CustomerID=-1 OR O.CarrierID=@CarrierID OR O.CustomerID=@CustomerID) 
	  AND OrderDate BETWEEN @StartDate AND @EndDate
	  AND O.DeleteDateUTC IS NULL AND OT.DeleteDateUTC IS NULL
	ORDER BY O.OrderDate
END

GO

COMMIT
SET NOEXEC OFF