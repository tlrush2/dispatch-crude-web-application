SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.12.8'
SELECT  @NewVersion = '3.12.8.1'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Import Center: providing input string longer than import field max length silently fails'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/***********************************
-- Date Created: 2016/06/25
-- Author: Kevin Alons
-- Purpose: return tblOrder + local dates + any other tblObject required "computed" fields
-- Changes:
***********************************/
CREATE VIEW viewObjectData_Order AS
SELECT O.*
	, OriginArriveTime = dbo.fnUTC_to_Local(O.OriginArriveTimeUTC, OO.TimeZoneID, OO.UseDST) 
	, OriginDepartTime = dbo.fnUTC_to_Local(O.OriginDepartTimeUTC, OO.TimeZoneID, OO.UseDST) 
	, DestArriveTime = dbo.fnUTC_to_Local(O.DestArriveTimeUTC, D.TimeZoneID, D.UseDST) 
	, DestDepartTime = dbo.fnUTC_to_Local(O.DestDepartTimeUTC, D.TimeZoneID, D.UseDST) 
	, OriginTimeZone = dbo.fnTimeZoneAbbrev(O.OriginArriveTimeUTC, OO.TimeZoneID, OO.UseDST) 
	, DestTimeZone = dbo.fnTimeZoneAbbrev(O.DestArriveTimeUTC, D.TimeZoneID, D.UseDST) 
	, TransitMinutes = DATEDIFF(minute, O.OriginDepartTimeUTC, O.DestArriveTimeUTC) 
	, PickupPrintDate = dbo.fnUTC_to_Local(O.PickupPrintDateUTC, OO.TimeZoneID, OO.UseDST) 
	, DeliverPrintDate = dbo.fnUTC_to_Local(O.DeliverPrintDateUTC, D.TimeZoneID, D.UseDST) 
	FROM tblOrder O
	LEFT JOIN tblOrigin OO ON OO.ID = O.OriginID
	LEFT JOIN tblDestination D ON D.ID = O.DestinationID

GO

-- update the ORDER object definition to use the new viewObjectData_Order for its data source
UPDATE tblObject SET SqlSourceName = 'viewObjectData_Order' WHERE ID = 1

-- add the new "special" ORDER.Local date fields (for UTC fields) - these will be handled "special" in code in ImportCenterEngine
DECLARE @id_base int = 409
SET IDENTITY_INSERT tblObjectField ON 
INSERT INTO tblObjectField (ID, ObjectID, FieldName, Name, ObjectFieldTypeID, ReadOnly, AllowNullID, IsCustom, CreateDateUTC, CreatedByUser)
	SELECT @id_base + ROW_NUMBER() OVER (ORDER BY ID), ObjectID, replace(FieldName, 'UTC', ''), replace(Name, ' UTC', ''), ObjectFieldTypeID, ReadOnly, AllowNullID, IsCustom, GETUTCDATE(), 'System'
	FROM tblObjectField
	WHERE ObjectID = 1 AND FieldName LIKE '%UTC'
SET IDENTITY_INSERT tblObjectField OFF

COMMIT
SET NOEXEC OFF