SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.5.20'
SELECT  @NewVersion = '4.5.21'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'JT-2814 - New system setting to show recently delivered orders on dispatch pages'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


INSERT INTO tblSetting (id, Name, SettingTypeID, Value, Category, CreatedByUser, CreateDateUTC, ReadOnly, Description, Hidden)
values
(71, 'Show recently delivered tickets on dispatch (hours)', 3, '0', 'Order Entry Rules', 'System', getutcdate(), 0, 'If set dispatch pages will include recently delivered orders up to the hours set by this variable.  Default is 0 - do not show delivered', 0)

GO


COMMIT
SET NOEXEC OFF