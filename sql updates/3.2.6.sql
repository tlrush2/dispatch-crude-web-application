-- rollback
-- select value from tblsetting where id = 0
/*
	- add new Carrier Financial fields to Report Center - Order History 
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.2.5'
SELECT  @NewVersion = '3.2.6'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Report Center Order data
/***********************************/
ALTER VIEW [dbo].[viewReportCenter_Orders] AS
	SELECT O.*
		, ShipperH2SRate = IOCS.H2SRate
		, ShipperH2SFee = IOCS.H2SFee
		, ShipperChainupFee = IOCS.ChainupFee
		, ShipperTaxRate = IOCS.TaxRate
		, ShipperRerouteFee = IOCS.RerouteFee
		, ShipperRejectFee = IOCS.RejectionFee
		, ShipperOriginWaitRate = IOCS.OriginWaitRate
		, ShipperOriginWaitFee = IOCS.OriginWaitFee
		, ShipperDestWaitRate = IOCS.DestinationWaitRate
		, ShipperDestWaitFee = IOCS.DestWaitFee
		, ShipperTotalWaitFee = cast(IOCS.OriginWaitFee as int) + cast(IOCS.DestWaitFee as int)
		, ShipperTotalFee = IOCS.TotalFee
		, ShipperSettlementUomID = IOCS.UomID
		, ShipperSettlementUom = SUOM.Abbrev
		, ShipperMinSettlementUnits = IOCS.MinSettlementUnits
		, ShipperRouteRate = IOCS.RouteRate
		, ShipperRouteFee = IOCS.LoadFee
		, ShipperBatchNum = SSB.BatchNum
		, ShipperFuelCharge = IOCS.FuelSurcharge
		, CarrierH2SRate = IOCC.H2SRate
		, CarrierH2SFee = IOCC.H2SFee
		, CarrierChainupFee = IOCC.ChainupFee
		, CarrierTaxRate = IOCC.TaxRate
		, CarrierRerouteFee = IOCC.RerouteFee
		, CarrierRejectFee = IOCC.RejectionFee
		, CarrierOriginWaitRate = IOCC.OriginWaitRate
		, CarrierOriginWaitFee = IOCC.OriginWaitFee
		, CarrierDestWaitRate = IOCC.DestinationWaitRate
		, CarrierDestWaitFee = IOCC.DestWaitFee
		, CarrierTotalWaitFee = cast(IOCC.OriginWaitFee as int) + cast(IOCC.DestWaitFee as int)
		, CarrierTotalFee = IOCC.TotalFee
		, CarrierSettlementUomID = IOCC.UomID
		, CarrierSettlementUom = CUOM.Abbrev
		, CarrierMinSettlementUnits = IOCC.MinSettlementUnits
		, CarrierRouteRate = IOCC.RouteRate
		, CarrierRouteFee = IOCC.LoadFee
		, CarrierBatchNum = CSB.BatchNum
		, CarrierFuelCharge = IOCC.FuelSurcharge
		, OriginGpsLatLon = ltrim(DLO.Lat) + ',' + ltrim(DLO.Lon)
		, OriginLatLon = ltrim(OO.LAT) + ',' + ltrim(OO.LON)
		, OriginAccuracyMeters = DLO.SourceAccuracyMeters
		, OriginDistance = isnull(cast(DLO.DistanceToPoint as int), 99999)
		, OriginGpsArrived = CASE WHEN DLO.DistanceToPoint IS NOT NULL AND DLO.DistanceToPoint <= OO.GeoFenceRadiusMeters THEN 1 ELSE 0 END
		, DestGpsLatLon = ltrim(DLD.Lat) + ',' + ltrim(DLD.Lon)
		, DestLatLon = ltrim(D.LAT) + ',' + ltrim(D.LON)
		, DestAccuracyMeters = DLD.SourceAccuracyMeters
		, DestDistance = isnull(cast(DLD.DistanceToPoint as int), 99999)
		, DestGpsArrived = CASE WHEN DLD.DistanceToPoint IS NOT NULL AND DLD.DistanceToPoint <= OO.GeoFenceRadiusMeters THEN 1 ELSE 0 END
	FROM viewOrder_OrderTicket_Full O
	JOIN tblOrigin OO ON OO.ID = O.OriginID
	JOIN tblDestination D ON D.ID = O.DestinationID
	LEFT JOIN tblOrderInvoiceCustomer IOCS ON IOCS.OrderID = O.ID
	LEFT JOIN tblCustomerSettlementBatch SSB ON SSB.ID = IOCS.BatchID
	LEFT JOIN tblUom SUOM ON SUOM.ID = IOCS.UomID
	LEFT JOIN tblOrderInvoiceCarrier IOCC ON IOCC.OrderID = O.ID
	LEFT JOIN tblCarrierSettlementBatch CSB ON CSB.ID = IOCC.BatchID
	LEFT JOIN tblUom CUOM ON CUOM.ID = IOCC.UomID
	LEFT JOIN viewDriverLocation_OriginFirstArrive DLO ON DLO.OrderID = O.ID AND DLO.OriginID = O.OriginID
	LEFT JOIN viewDriverLocation_DestinationFirstArrive DLD ON DLD.OrderID = O.ID AND DLD.DestinationID = O.DestinationID

GO

INSERT INTO tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport)
	SELECT 164, 1, 'CarrierH2SRate', 'Carrier H2S Rate', '#0.0000', null, 4, null, 1, 'Administrator,Management', 1
	UNION
	SELECT 165, 1, 'CarrierH2SFee', 'Carrier H2S $$', '#0.00', null, 4, null, 1, 'Administrator,Management', 1
	UNION
	SELECT 166, 1, 'CarrierChainupFee', 'Carrier Chainup $$', '#0.00', null, 4, null, 1, 'Administrator,Management', 1
	UNION
	SELECT 167, 1, 'CarrierTaxRate', 'Carrier Tax Rate', '#0.0000', null, 4, null, 1, 'Administrator,Management', 1
	UNION
	SELECT 168, 1, 'CarrierRerouteFee', 'Carrier Reroute $$', '#0.00', null, 4, null, 1, 'Administrator,Management', 1
	UNION
	SELECT 169, 1, 'CarrierRejectFee', 'Carrier Reroute $$', '#0.00', null, 4, null, 1, 'Administrator,Management', 1
	UNION
	SELECT 170, 1, 'CarrierOriginWaitRate', 'Carrier Origin Wait Rate', '#0.0000', null, 4, null, 1, 'Administrator,Management', 1
	UNION
	SELECT 171, 1, 'CarrierOriginWaitFee', 'Carrier Origin Wait Fee', '#0.00', null, 4, null, 1, 'Administrator,Management', 1
	UNION
	SELECT 172, 1, 'CarrierDestWaitRate', 'Carrier Dest Wait Rate', '#0.0000', null, 4, null, 1, 'Administrator,Management', 1
	UNION
	SELECT 173, 1, 'CarrierDestWaitFee', 'Carrier Dest Wait Fee', '#0.00', null, 4, null, 1, 'Administrator,Management', 1
	UNION
	SELECT 174, 1, 'CarrierTotalWaitFee', 'Carrier Total Wait Fee', '#0.00', null, 4, null, 1, 'Administrator,Management', 1
	UNION
	SELECT 175, 1, 'CarrierTotalFee', 'Carrier Total Fee', '#0.00', null, 4, null, 1, 'Administrator,Management', 1
	UNION
	SELECT 176, 1, 'CarrierSettlementUom', 'Carrier Settlement UOM', null, 'CarrierSettlementUomID', 2, 'SELECT ID, Name = Abbrev FROM tblUom ORDER BY Abbrev', 1, 'Administrator,Management', 1
	UNION
	SELECT 177, 1, 'CarrierMinSettlementUnits', 'Carrier Min Settle Units', '#0.00', null, 4, null, 1, 'Administrator,Management', 1
	UNION
	SELECT 178, 1, 'CarrierRouteRate', 'Carrier Route Rate', '#0.0000', null, 4, null, 1, 'Administrator,Management', 1
	UNION
	SELECT 179, 1, 'CarrierRouteFee', 'Carrier Route $$', '#0.00', null, 4, null, 1, 'Administrator,Management', 1
	UNION
	SELECT 180, 1, 'CarrierBatchNum', 'Carrier Batch #', null, null, 4, null, 1, 'Administrator,Management', 1
	UNION
	SELECT 181, 1, 'CarrierFuelCharge', 'Carrier Fuel Charge', '#0.00', null, 4, null, 1, 'Administrator,Management', 1

COMMIT
SET NOEXEC OFF