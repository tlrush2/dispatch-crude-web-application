SET NOEXEC OFF  -- since this is 
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.4.1.4'
SELECT  @NewVersion = '4.4.1.5'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1957 Update subscription log to include report name'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

-- drop existing constraint so foreign key doesn't prevent deletion of report
ALTER TABLE tblUserReportEmailSubscriptionLog DROP CONSTRAINT FK_UserReportEmailSubscriptionLog_UserReport
GO

-- Add new column to grab the report name
ALTER TABLE tblUserReportEmailSubscriptionLog ADD Name VARCHAR(100) NULL
GO

UPDATE tblUserReportEmailSubscriptionLog 
SET Name = ISNULL(urd.Name, '-')
FROM tblUserReportEmailSubscriptionLog uresl
LEFT JOIN tblUserReportDefinition urd ON urd.ID = uresl.UserReportID
GO

ALTER TABLE tblUserReportEmailSubscriptionLog ALTER COLUMN Name VARCHAR(100) NULL
GO



COMMIT
SET NOEXEC OFF