--rollback
BEGIN TRANSACTION
GO

UPDATE tblSetting SET Value = '1.1.18' WHERE ID=0

EXEC sp_rename 
	@objname = 'tblCarrier.LastUpdateDate',
    @newname = 'LastChangeDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblCarrier.LastUpdatedByUser',
    @newname = 'LastChangedByUser',
    @objtype = 'COLUMN'
GO
EXEC sp_rename 
	@objname = 'tblCarrierRangeRates.LastUpdateDate',
    @newname = 'LastChangeDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblCarrierRangeRates.LastUpdatedByUser',
    @newname = 'LastChangedByUser',
    @objtype = 'COLUMN'
GO
EXEC sp_rename 
	@objname = 'tblCarrierRates.LastUpdateDate',
    @newname = 'LastChangeDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblCarrierRates.LastUpdatedByUser',
    @newname = 'LastChangedByUser',
    @objtype = 'COLUMN'
GO
EXEC sp_rename 
	@objname = 'tblCarrierType.LastUpdateDate',
    @newname = 'LastChangeDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblCarrierType.LastUpdatedByUser',
    @newname = 'LastChangedByUser',
    @objtype = 'COLUMN'
GO
EXEC sp_rename 
	@objname = 'tblCustomer.LastUpdateDate',
    @newname = 'LastChangeDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblCustomer.LastUpdatedByUser',
    @newname = 'LastChangedByUser',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblCustomerRangeRates.LastUpdateDate',
    @newname = 'LastChangeDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblCustomerRangeRates.LastUpdatedByUser',
    @newname = 'LastChangedByUser',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblCustomerRates.LastUpdateDate',
    @newname = 'LastChangeDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblCustomerRates.LastUpdatedByUser',
    @newname = 'LastChangedByUser',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblDestination.LastUpdateDate',
    @newname = 'LastChangeDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblDestination.LastUpdatedByUser',
    @newname = 'LastChangedByUser',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblDestinationType.LastUpdateDate',
    @newname = 'LastChangeDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblDestinationType.LastUpdatedByUser',
    @newname = 'LastChangedByUser',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblDriver.LastUpdateDate',
    @newname = 'LastChangeDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblDriver.LastUpdatedByUser',
    @newname = 'LastChangedByUser',
    @objtype = 'COLUMN'
GO
EXEC sp_rename 
	@objname = 'tblOperator.LastUpdateDate',
    @newname = 'LastChangeDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblOperator.LastUpdatedByUser',
    @newname = 'LastChangedByUser',
    @objtype = 'COLUMN'
GO        
EXEC sp_rename 
	@objname = 'tblOrder.LastUpdateDate',
    @newname = 'LastChangeDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblOrder.LastUpdatedByUser',
    @newname = 'LastChangedByUser',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblOrderInvoiceCarrier.LastUpdateDate',
    @newname = 'LastChangeDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblOrderInvoiceCarrier.LastUpdatedByUser',
    @newname = 'LastChangedByUser',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblOrderInvoiceCustomer.LastUpdateDate',
    @newname = 'LastChangeDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblOrderInvoiceCustomer.LastUpdatedByUser',
    @newname = 'LastChangedByUser',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblOrderStatus.LastUpdateDate',
    @newname = 'LastChangeDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblOrderStatus.LastUpdatedByUser',
    @newname = 'LastChangedByUser',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblOrderTicket.LastUpdateDate',
    @newname = 'LastChangeDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblOrderTicket.LastUpdatedByUser',
    @newname = 'LastChangedByUser',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblOrigin.LastUpdateDate',
    @newname = 'LastChangeDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblOrigin.LastUpdatedByUser',
    @newname = 'LastChangedByUser',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblOriginType.LastUpdateDate',
    @newname = 'LastChangeDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblOriginType.LastUpdatedByUser',
    @newname = 'LastChangedByUser',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblPriority.LastUpdateDate',
    @newname = 'LastChangeDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblPriority.LastUpdatedByUser',
    @newname = 'LastChangedByUser',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblProducer.LastUpdateDate',
    @newname = 'LastChangeDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblProducer.LastUpdatedByUser',
    @newname = 'LastChangedByUser',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblPumper.LastUpdateDate',
    @newname = 'LastChangeDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblPumper.LastUpdatedByUser',
    @newname = 'LastChangedByUser',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblRateTableRange.LastUpdateDate',
    @newname = 'LastChangeDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblRateTableRange.LastUpdatedByUser',
    @newname = 'LastChangedByUser',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblRegion.LastUpdateDate',
    @newname = 'LastChangeDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblRegion.LastUpdatedByUser',
    @newname = 'LastChangedByUser',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblRoute.LastUpdateDate',
    @newname = 'LastChangeDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblRoute.LastUpdatedByUser',
    @newname = 'LastChangedByUser',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblSetting.LastUpdateDate',
    @newname = 'LastChangeDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblSetting.LastUpdatedByUser',
    @newname = 'LastChangedByUser',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblSettingType.LastUpdateDate',
    @newname = 'LastChangeDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblSettingType.LastUpdatedByUser',
    @newname = 'LastChangedByUser',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblState.LastUpdateDate',
    @newname = 'LastChangeDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblState.LastUpdatedByUser',
    @newname = 'LastChangedByUser',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblTankType.LastUpdateDate',
    @newname = 'LastChangeDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblTankType.LastUpdatedByUser',
    @newname = 'LastChangedByUser',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblTicketType.LastUpdateDate',
    @newname = 'LastChangeDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblTicketType.LastUpdatedByUser',
    @newname = 'LastChangedByUser',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblTrailer.LastUpdateDate',
    @newname = 'LastChangeDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblTrailer.LastUpdatedByUser',
    @newname = 'LastChangedByUser',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblTrailerType.LastUpdateDate',
    @newname = 'LastChangeDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblTrailerType.LastUpdatedByUser',
    @newname = 'LastChangedByUser',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblTruck.LastUpdateDate',
    @newname = 'LastChangeDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblTruck.LastUpdatedByUser',
    @newname = 'LastChangedByUser',
    @objtype = 'COLUMN'
GO    
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRecompileAllStoredProcedures
GO
/***********************************/
-- Date Created: 24 Jan 2013
-- Author: Kevin Alons
-- Purpose: accomplish an Order Redirect
/***********************************/
ALTER PROCEDURE [dbo].[spRerouteOrder]
(
  @OrderID int
, @NewDestinationID int
, @UserName varchar(255)
, @Notes varchar(255)
)
AS BEGIN
	INSERT INTO tblOrderReroute (OrderID, PreviousDestinationID, UserName, Notes, RerouteDate)
		SELECT ID, DestinationID, @UserName, @Notes, getdate() FROM tblOrder WHERE ID = @OrderID
	
	UPDATE tblOrder 
		SET DestinationID = R.DestinationID, RouteID = R.ID, ActualMiles = R.ActualMiles
			, LastChangeDate=GETDATE(), LastChangedByUser=@UserName
	FROM tblOrder O
	JOIN tblRoute R ON R.OriginID = O.OriginID AND R.DestinationID = @NewDestinationID
	WHERE O.ID = @OrderID
END

GO
/*******************************************/
-- Date Created: 25 Apr 2013
-- Author: Kevin Alons
-- Purpose: return Order data for Driver App sync
/*******************************************/
ALTER FUNCTION [dbo].[fnOrder_DriverApp]( @DriverID int, @LastChangeDate datetime ) RETURNS  TABLE AS
RETURN 
	SELECT O.ID
		, O.OrderNum
		, O.TicketTypeID
		, O.StatusID
		, P.PriorityNum
		, O.DueDate
		, O.TruckID
		, O.TrailerID
		, O.Trailer2ID
		, O.Origin
		, O.OriginFull
		, O.OriginArriveTime
		, O.OriginDepartTime
		, O.OriginMinutes
		, O.OriginWaitNotes
		, O.OriginTruckMileage
		, O.OriginGrossBarrels
		, O.OriginNetBarrels
		, O.ChainUp
		, O.Rejected
		, O.RejectNotes
		, O.OriginTankNum
		, O.Destination
		, O.DestinationFull
		, O.DestArriveTime
		, O.DestDepartTime
		, O.DestMinutes
		, O.DestWaitNotes
		, O.DestTruckMileage
		, O.DestGrossBarrels
		, O.DestNetBarrels
		, O.CarrierTicketNum
		, D.BOLAvailable AS DestBOLAvailable
		, OO.TankTypeID
		, OO.LAT AS OriginLAT
		, OO.LON AS OriginLON
		, OO.BarrelsPerInch AS OriginBarrelsPerInch
		, O.CreateDate
		, O.CreatedByUser
		, O.LastChangeDate
		, O.LastChangedByUser
		, O.DeleteDate
		, O.DeletedByUser
	FROM dbo.viewOrder O
	JOIN dbo.tblPriority P ON P.ID = O.PriorityID
	JOIN dbo.tblOrigin OO ON OO.ID = O.OriginID
	JOIN dbo.tblDestination D ON D.ID = O.DestinationID
	WHERE StatusID IN (2, 7, 8) -- Dispatched, Accepted, Picked Up
	  AND O.DriverID = @DriverID
	  AND (@LastChangeDate IS NULL OR O.LastChangeDate >= @LastChangeDate)


GO

/*******************************************/
-- Date Created: 25 Apr 2013
-- Author: Kevin Alons
-- Purpose: return OrderTicket data for Driver App sync
/*******************************************/
ALTER FUNCTION [dbo].[fnOrderTicket_DriverApp]( @DriverID int, @LastChangeDate datetime ) RETURNS  TABLE AS
RETURN 
	SELECT OT.ID
		, OT.UID
		, OT.OrderID
		, OT.CarrierTicketNum
		, OT.TankNum
		, OT.TicketTypeID
		, OT.TankTypeID
		, OT.ProductObsGravity
		, OT.ProductObsTemp
		, OT.ProductHighTemp
		, OT.ProductLowTemp
		, OT.ProductBSW
		, OT.OpeningGaugeFeet
		, OT.OpeningGaugeInch
		, OT.OpeningGaugeQ
		, OT.ClosingGaugeFeet
		, OT.ClosingGaugeInch
		, OT.ClosingGaugeQ
		, OT.GrossBarrels
		, OT.NetBarrels
		, OT.Rejected
		, OT.RejectNotes
		, OT.SealOff
		, OT.SealOn
		, OT.BOLNum
		, OT.BarrelsPerInch
		, OT.CreateDate
		, OT.CreatedByUser
		, OT.LastChangeDate
		, OT.LastChangedByUser
		, OT.DeleteDate
		, OT.DeletedByUser
	FROM dbo.tblOrderTicket OT
	JOIN dbo.tblOrder O ON O.ID = OT.OrderID
	WHERE StatusID IN (2, 7, 8) -- Dispatched, Accepted, Picked Up
	  AND O.DriverID = @DriverID
	  AND (@LastChangeDate IS NULL OR O.LastChangeDate >= @LastChangeDate)

GO
-- =============================================
-- Author:		Kevin Alons
-- Create date: 6 Feb 2013
-- Description:	trigger to ensure the GrossBarrels and NetBarrels are computed for valid, entered data
-- =============================================
ALTER TRIGGER [dbo].[trigOrderTicket_IU] ON [dbo].[tblOrderTicket] AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	-- re-compute GaugeRun ticket Gross barrels
	IF UPDATE(OpeningGaugeFeet) OR UPDATE(OpeningGaugeInch) OR UPDATE(OpeningGaugeQ)
		OR UPDATE (ClosingGaugeFeet) OR UPDATE(ClosingGaugeInch) OR UPDATE(ClosingGaugeQ) OR UPDATE(BarrelsPerInch)
		OR UPDATE (GrossBarrels)
	BEGIN
		UPDATE tblOrderTicket
		  SET GrossBarrels = dbo.fnTankQtyBarrelsDelta(
				OpeningGaugeFeet
			  , OpeningGaugeInch
			  , OpeningGaugeQ
			  , ClosingGaugeFeet
			  , ClosingGaugeInch
			  , ClosingGaugeQ
			  , BarrelsPerInch)
		WHERE ID IN (SELECT ID FROM inserted) 
		  AND TicketTypeID = 1 -- Gauge Run Ticket
		  AND OpeningGaugeFeet IS NOT NULL
		  AND OpeningGaugeInch IS NOT NULL
		  AND OpeningGaugeQ IS NOT NULL
		  AND ClosingGaugeFeet IS NOT NULL
		  AND ClosingGaugeInch IS NOT NULL
		  AND ClosingGaugeQ IS NOT NULL
		  AND BarrelsPerInch IS NOT NULL
	END
	-- re-compute GaugeRun ticket Net Barrels
	IF UPDATE(OpeningGaugeFeet) OR UPDATE(OpeningGaugeInch) OR UPDATE(OpeningGaugeQ)
		OR UPDATE (ClosingGaugeFeet) OR UPDATE(ClosingGaugeInch) OR UPDATE(ClosingGaugeQ) 
		OR UPDATE(BarrelsPerInch) OR UPDATE(GrossBarrels) OR UPDATE(NetBarrels)
		OR UPDATE(ProductObsTemp) OR UPDATE(ProductObsGravity) OR UPDATE(ProductBSW)
	BEGIN
		UPDATE tblOrderTicket
		  SET NetBarrels = dbo.fnCrudeNetCalculator(GrossBarrels, ProductObsTemp, ProductObsGravity, ProductBSW)
		WHERE ID IN (SELECT ID FROM inserted)
		  AND TicketTypeID IN (1,2)
		  AND GrossBarrels IS NOT NULL
		  AND ProductObsTemp IS NOT NULL
		  AND ProductObsGravity IS NOT NULL
		  AND ProductBSW IS NOT NULL
	END
	-- ensure the Order record is in-sync with the Tickets
	UPDATE tblOrder 
	SET OriginGrossBarrels = (SELECT sum(GrossBarrels) FROM tblOrderTicket WHERE OrderID = O.ID AND Rejected = 0 AND DeleteDate IS NULL)
	  , OriginNetBarrels = (SELECT sum(NetBarrels) FROM tblOrderTicket WHERE OrderID = O.ID AND Rejected = 0 AND DeleteDate IS NULL)
		-- use the first MeterRun CarrierTicketNum num as the Order BOL Num
	  , OriginBOLNum = (SELECT min(BOLNum) FROM tblOrderTicket WHERE OrderID = O.ID AND TicketTypeID = 3 AND DeleteDate IS NULL)
	  , CarrierTicketNum = (SELECT min(CarrierTicketNum) FROM tblOrderTicket WHERE ID IN 
		(SELECT min(ID) FROM tblOrderTicket WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND Rejected = 0 AND DeleteDate IS NULL))
	  , LastChangeDate = (SELECT MAX(LastChangeDate) FROM inserted WHERE OrderID = O.ID)
	  , LastChangedByUser = (SELECT MIN(LastChangedByUser) FROM inserted WHERE OrderID = O.ID)
	FROM tblOrder O
	WHERE ID IN (SELECT DISTINCT OrderID FROM inserted)
END

GO

COMMIT
GO