/* fix a BillableWaitMinutes computation bug, and add RoundingType = None option
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.1.7'
SELECT  @NewVersion = '2.1.8'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

INSERT INTO tblWaitFeeRoundingType (ID, Name) VALUES (0, 'None')
GO

UPDATE tblCarrierRates SET WaitFeeRoundingTypeID = 0 WHERE WaitFeeSubUnitID = 0
UPDATE tblCustomerRates SET WaitFeeRoundingTypeID = 0 WHERE WaitFeeSubUnitID = 0
GO

/***********************************************************/
--  Date Created: 22 June 2013
--  Author: Kevin Alons
--  Purpose: Round the Billable Wait minutes into a decimal hour value (based on rounding parameters)
/***********************************************************/
ALTER FUNCTION [dbo].[fnComputeBillableWaitHours](@min int, @SubUnitID int, @RoundingTypeID int) RETURNS decimal (9,4) AS
BEGIN
	DECLARE @ret decimal(9,4) 
	SELECT @ret = @min
	
	IF (@SubUnitID <> 0 AND @RoundingTypeID <> 0)
	BEGIN
		SELECT @ret = @ret / @SubUnitID
		IF (@RoundingTypeID = 1)
			SELECT @ret = floor(@ret)
		ELSE IF (@RoundingTypeID = 2)
			SELECT @ret = round(@ret - 0.01, 0)
		ELSE IF (@RoundingTypeID = 3)
			SELECT @ret = round(@ret, 0)
		ELSE IF (@RoundingTypeID = 4)
			SELECT @ret = ceiling(@ret)
		ELSE
			SELECT @ret = @min
	END
	
	RETURN @ret / (60.0 / CASE WHEN (@SubUnitID = 0 OR @RoundingTypeID = 0) THEN 1 ELSE @SubUnitID END)
END

GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Carrier "Settlement" $$ values to an Audited order
/***********************************/
ALTER PROCEDURE [dbo].[spProcessCarrierInvoice]
(
  @ID int
, @UserName varchar(100)
, @ChainupFee smallmoney = NULL
, @WaitFee smallmoney = NULL
, @RerouteFee smallmoney = NULL
, @RejectionFee smallmoney = NULL
, @H2SFee smallmoney = NULL
, @LoadFee smallmoney = NULL
) AS BEGIN
	DELETE FROM tblOrderInvoiceCarrier WHERE OrderID = @ID
	
	INSERT INTO tblOrderInvoiceCarrier (OrderID, ChainupFee, RerouteFee, WaitFeeSubUnitID, WaitFeeRoundingTypeID, BillableWaitMinutes, WaitRate, WaitFee
		, RejectionFee, RouteRate, LoadFee, H2SRate, H2SFee, FuelSurcharge, TaxRate
		, TotalFee, CreateDateUTC, CreatedByUser)
	SELECT D.ID, ChainupFee, RerouteFee, WaitFeeSubUnitID, WaitFeeRoundingTypeID, BillableWaitMinutes, WaitRate, WaitFee
		, RejectionFee, RouteRate, LoadFee, H2SRate, H2SFee, FuelSurcharge, TaxRate
		, ChainupFee + RerouteFee + WaitFee + H2SFee + LoadFee AS TotalFee, GETUTCDATE(), @UserName
	FROM (
		SELECT ID
			, coalesce(@ChainupFee, Chainup * ChainupFee, 0) AS ChainupFee
			, coalesce(@RerouteFee, RerouteFee * RerouteCount, 0) AS RerouteFee
			, WaitFeeSubUnitID
			, WaitFeeRoundingTypeID
			, cast(round(BillableWaitHours * 60, 0) as int) AS BillableWaitMinutes
			, WaitFee AS WaitRate
			, coalesce(@WaitFee, BillableWaitHours * WaitFee, 0) AS WaitFee
			, coalesce(@RejectionFee, Rejected * RejectionFee, 0) AS RejectionFee
			, isnull(H2SRate, 0) AS H2SRate
			, coalesce(@H2SFee, H2S * dbo.fnMaxDecimal(MinSettlementBarrels, ActualBarrels) * H2SRate, 0) AS H2SFee
			, isnull(TaxRate, 0) AS TaxRate
			, isnull(Rate, 0) AS RouteRate
			, round(coalesce(@LoadFee, dbo.fnMaxDecimal(MinSettlementBarrels, ActualBarrels) * Rate, 0), 3) AS LoadFee
			, isnull(FuelSurcharge, 0) AS FuelSurcharge
		FROM (
			SELECT S.ID
				, S.ChainUp
				, CR.ChainupFee
				, CR.RerouteFee
				, S.RerouteCount
				, S.TotalWaitMinutes
				, CR.WaitFeeSubUnitID
				, CR.WaitFeeRoundingTypeID
				, dbo.fnComputeBillableWaitHours(S.TotalWaitMinutes, CR.WaitFeeSubUnitID, CR.WaitFeeRoundingTypeID) AS BillableWaitHours
				, CR.WaitFee
				, S.Rejected
				, CR.RejectionFee
				, S.H2S
				, isnull(S.H2S * CR.H2SRate, 0) AS H2SRate
				, S.TaxRate
				, dbo.fnCarrierRouteRate(S.CarrierID, S.RouteID, S.OrderDate) AS Rate
				, S.MinSettlementBarrels
				, isnull(S.ActualBarrels, 0) AS ActualBarrels
				, CR.FuelSurcharge
			FROM (
				SELECT O.ID
					, O.CarrierID
					, O.RouteID
					, O.ChainUp
					, CASE WHEN C.SettlementFactorID = 1 THEN O.OriginGrossBarrels ELSE O.OriginNetBarrels END AS ActualBarrels
					, O.RerouteCount
					, O.TotalWaitMinutes
					, O.Rejected
					, OO.H2S
					, O.OrderDate
					, coalesce(CRXX.ID, CRXA.ID, CRAX.ID, CRAA.ID) AS CRID
					, isnull(C.MinSettlementBarrels, 0) AS MinSettlementBarrels
					, OO.TaxRate
				FROM dbo.viewOrderExportFull O
				JOIN tblOrigin OO ON OO.ID = O.OriginID
				JOIN tblCarrier C ON C.ID = O.CarrierID
				LEFT JOIN tblCarrierRates CRXX ON CRXX.CarrierID = O.CarrierID AND CRXX.RegionID = OO.RegionID
				LEFT JOIN tblCarrierRates CRXA ON CRXA.CarrierID = O.CarrierID AND CRXA.RegionID = -1
				LEFT JOIN tblCarrierRates CRAX ON CRAX.CarrierID = -1 AND CRAX.RegionID = OO.RegionID
				LEFT JOIN tblCarrierRates CRAA ON CRAA.CarrierID = -1 AND CRAA.RegionID = -1
				WHERE O.ID = @ID
			) S
			LEFT JOIN tblCarrierRates CR ON CR.ID = S.CRID
		) SS
	) D
END

GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Customer "Settlement" $$ values to an Audited order
/***********************************/
ALTER PROCEDURE [dbo].[spProcessCustomerInvoice]
(
  @ID int
, @UserName varchar(100)
, @ChainupFee smallmoney = NULL
, @WaitFee smallmoney = NULL
, @RerouteFee smallmoney = NULL
, @RejectionFee smallmoney = NULL
, @H2SFee smallmoney = NULL
, @LoadFee smallmoney = NULL
) AS BEGIN
	DELETE FROM tblOrderInvoiceCustomer WHERE OrderID = @ID
	
	INSERT INTO tblOrderInvoiceCustomer (OrderID, ChainupFee, RerouteFee, WaitFeeSubUnitID, WaitFeeRoundingTypeID, BillableWaitMinutes, WaitRate, WaitFee
		, RejectionFee, RouteRate, LoadFee, H2SRate, H2SFee, FuelSurcharge, TaxRate
		, TotalFee, CreateDateUTC, CreatedByUser)
	SELECT D.ID, ChainupFee, RerouteFee, WaitFeeSubUnitID, WaitFeeRoundingTypeID, BillableWaitMinutes, WaitRate, WaitFee
		, RejectionFee, RouteRate, LoadFee, H2SRate, H2SFee, FuelSurcharge, TaxRate
		, ChainupFee + RerouteFee + WaitFee + H2SFee + LoadFee AS TotalFee, GETUTCDATE(), @UserName
	FROM (
		SELECT ID
			, coalesce(@ChainupFee, Chainup * ChainupFee, 0) AS ChainupFee
			, coalesce(@RerouteFee, RerouteFee * RerouteCount, 0) AS RerouteFee
			, WaitFeeSubUnitID
			, WaitFeeRoundingTypeID
			, cast(round(BillableWaitHours * 60, 0) as int) AS BillableWaitMinutes
			, WaitFee AS WaitRate
			, coalesce(@WaitFee, BillableWaitHours * WaitFee, 0) AS WaitFee
			, coalesce(@RejectionFee, Rejected * RejectionFee, 0) AS RejectionFee
			, isnull(H2SRate, 0) AS H2SRate
			, coalesce(@H2SFee, H2S * dbo.fnMaxDecimal(MinSettlementBarrels, ActualBarrels) * H2SRate, 0) AS H2SFee
			, isnull(TaxRate, 0) AS TaxRate
			, isnull(Rate, 0) AS RouteRate
			, round(coalesce(@LoadFee, dbo.fnMaxDecimal(MinSettlementBarrels, ActualBarrels) * Rate, 0), 3) AS LoadFee
			, isnull(FuelSurcharge, 0) AS FuelSurcharge
		FROM (
			SELECT S.ID
				, S.ChainUp
				, CR.ChainupFee
				, CR.RerouteFee
				, S.RerouteCount
				, S.TotalWaitMinutes
				, CR.WaitFeeSubUnitID
				, CR.WaitFeeRoundingTypeID
				, dbo.fnComputeBillableWaitHours(S.TotalWaitMinutes, CR.WaitFeeSubUnitID, CR.WaitFeeRoundingTypeID) AS BillableWaitHours
				, CR.WaitFee
				, S.Rejected
				, CR.RejectionFee
				, S.H2S
				, isnull(S.H2S * CR.H2SRate, 0) AS H2SRate
				, S.TaxRate
				, dbo.fnCustomerRouteRate(S.CustomerID, S.RouteID, S.OrderDate) AS Rate
				, S.MinSettlementBarrels
				, isnull(S.ActualBarrels, 0) AS ActualBarrels
				, CR.FuelSurcharge
			FROM (
				SELECT O.ID
					, O.CustomerID
					, O.RouteID
					, O.ChainUp
					, CASE WHEN C.SettlementFactorID = 1 THEN O.OriginGrossBarrels ELSE O.OriginNetBarrels END AS ActualBarrels
					, O.RerouteCount
					, O.TotalWaitMinutes
					, O.Rejected
					, OO.H2S
					, O.OrderDate
					, coalesce(CRXX.ID, CRXA.ID, CRAX.ID, CRAA.ID) AS CRID
					, isnull(C.MinSettlementBarrels, 0) AS MinSettlementBarrels
					, OO.TaxRate
				FROM dbo.viewOrderExportFull O
				JOIN tblOrigin OO ON OO.ID = O.OriginID
				JOIN tblCustomer C ON C.ID = O.CustomerID
				LEFT JOIN tblCustomerRates CRXX ON CRXX.CustomerID = O.CustomerID AND CRXX.RegionID = OO.RegionID
				LEFT JOIN tblCustomerRates CRXA ON CRXA.CustomerID = O.CustomerID AND CRXA.RegionID = -1
				LEFT JOIN tblCustomerRates CRAX ON CRAX.CustomerID = -1 AND CRAX.RegionID = OO.RegionID
				LEFT JOIN tblCustomerRates CRAA ON CRAA.CustomerID = -1 AND CRAA.RegionID = -1
				WHERE O.ID = @ID
			) S
			LEFT JOIN tblCustomerRates CR ON CR.ID = S.CRID
		) SS
	) D
END

GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 4 Oct 2013
-- Description:	if either WaitFee SubUnit or RoundingType = None, ensure they both are
-- =============================================
CREATE TRIGGER trigCarrierRates_UI_WaitFee_None ON tblCarrierRates AFTER INSERT, UPDATE AS BEGIN
	SET NOCOUNT ON;

	UPDATE tblCarrierRates SET WaitFeeRoundingTypeID = 0, WaitFeeSubUnitID = 0
	WHERE ID IN (SELECT ID FROM inserted WHERE WaitFeeRoundingTypeID = 0 OR WaitFeeSubUnitID = 0)

END
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 4 Oct 2013
-- Description:	if either WaitFee SubUnit or RoundingType = None, ensure they both are
-- =============================================
CREATE TRIGGER trigCustomerRates_UI_WaitFee_None ON tblCustomerRates AFTER INSERT, UPDATE AS BEGIN
	SET NOCOUNT ON;

	UPDATE tblCustomerRates SET WaitFeeRoundingTypeID = 0, WaitFeeSubUnitID = 0
	WHERE ID IN (SELECT ID FROM inserted WHERE WaitFeeRoundingTypeID = 0 OR WaitFeeSubUnitID = 0)

END
GO

COMMIT
SET NOEXEC OFF