-- backup database [dispatchcrude.dev] to disk = 'C:\files\Consulting\cts\dc_backups\dispatchcrude.dev.3.7.2.bak'
-- restore database [DispatchCrude.Dev] from disk = 'C:\files\Consulting\cts\dc_backups\dispatchcrude.dev.3.7.2.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.7.2'
SELECT  @NewVersion = '3.7.3'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'GaugerApp: honor Origin.Default Gauger for Gauger Order Creation'
	UNION
	SELECT @NewVersion, 1, 'GaugerApp: Completed Rejected orders will advance to final GAUGER REJECTED status'
	UNION
	SELECT @NewVersion, 1, 'GaugerApp: add Gauger Rejected Orders page to un-reject orders when required'
	UNION
	SELECT @NewVersion, 0, 'GaugerApp: fix bug in Order Rule BestMatch logic that required a Carrier to be specified (which might not be supplied)'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/***********************************/
-- Date Created: 28 Feb 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OrderRule info for the specified order
/***********************************/
ALTER FUNCTION fnOrderRules(@StartDate date, @EndDate date, @TypeID int, @ShipperID int, @CarrierID int, @ProductGroupID int, @OriginID int, @DestinationID int, @OriginStateID int, @DestStateID int, @RegionID int, @ProducerID int, @BestMatchOnly bit = 0)
RETURNS 
	@ret TABLE (
		ID int
	  , TypeID int
	  , ShipperID int
	  , CarrierID int
	  , ProductGroupID int
	  , OriginID int
	  , DestinationID int
	  , OriginStateID int
	  , DestStateID int
	  , RegionID int
	  , ProducerID int
	  , Value varchar(255)
	  , RuleTypeID int
	  , EffectiveDate date
	  , EndDate date
	  , MaxEffectiveDate date
	  , MinEndDate date
	  , NextEffectiveDate date
	  , PriorEndDate date
	  , BestMatch bit
	  , Ranking smallmoney
	  , CreateDateUTC datetime
	  , CreatedByUser varchar(100)
	  , LastChangeDateUTC datetime
	  , LastChangedByUser varchar(100)
	)
AS BEGIN
	-- get the raw data (all matched rows)
	DECLARE @src TABLE (ID int, TypeID int, Ranking smallmoney)
	INSERT INTO @src (ID, TypeID, Ranking)
		SELECT ID, TypeID
			, Ranking =	dbo.fnRateRanking(@ShipperID, R.ShipperID, 256, 0)
					  + dbo.fnRateRanking(@CarrierID, R.CarrierID, 128, 1)
					  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 64, 0)
					  + dbo.fnRateRanking(@OriginID, R.OriginID, 32, 0)
					  + dbo.fnRateRanking(@DestinationID, R.DestinationID, 16, 0)
					  + dbo.fnRateRanking(@OriginStateID, R.OriginStateID, 8, 0)
					  + dbo.fnRateRanking(@DestStateID, R.DestStateID, 4, 0)
					  + dbo.fnRateRanking(@RegionID, R.RegionID, 2, 0)
					  + dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM dbo.viewOrderRule R
		WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(R.ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@OriginID, 0), R.OriginID, 0) = coalesce(R.OriginID, nullif(@OriginID, 0), 0)
		  AND coalesce(nullif(@DestinationID, 0), R.DestinationID, 0) = coalesce(R.DestinationID, nullif(@DestinationID, 0), 0)
		  AND coalesce(nullif(@OriginStateID, 0), R.OriginStateID, 0) = coalesce(R.OriginStateID, nullif(@OriginStateID, 0), 0)
		  AND coalesce(nullif(@DestStateID, 0), R.DestStateID, 0) = coalesce(R.DestStateID, nullif(@DestStateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(R.RegionID, nullif(@RegionID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(R.ProducerID, nullif(@ProducerID, 0), 0)
		  AND (nullif(@TypeID, 0) IS NULL OR TypeID = @TypeID)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)

	-- return the rate data for the best-match rates 
	INSERT INTO @ret (ID, TypeID, ShipperID, CarrierID, ProductGroupID, OriginID, DestinationID, OriginStateID, DestStateID, RegionID, ProducerID, Value, RuleTypeID, EffectiveDate, EndDate
			, NextEffectiveDate, PriorEndDate, BestMatch, Ranking, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser)
		SELECT R.ID, TypeID, ShipperID, CarrierID, ProductGroupID, OriginID, DestinationID, OriginStateID, DestStateID, RegionID, ProducerID, Value, RuleTypeID, EffectiveDate, EndDate
			, NextEffectiveDate, PriorEndDate, BestMatch, Ranking, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser
		FROM viewOrderRule R
		JOIN (
			SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.TypeID IS NULL THEN 0 ELSE 1 END as bit)
			FROM @src S
			LEFT JOIN (
				SELECT TypeID, Ranking = MAX(Ranking)
				FROM @src
				WHERE @StartDate = ISNULL(@EndDate, @StartDate)
				  AND Ranking % 1 = 0.01 * 9 -- ensure a complete match is found for BESTMATCH
				GROUP BY TypeID
			) X ON X.TypeID = S.TypeID AND X.Ranking = S.Ranking
		) X ON X.ID = R.ID
		WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
		
	RETURN
END
GO

/***********************************/
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Order records with "translated friendly" values for FK relationships
/***********************************/
ALTER VIEW viewOrder AS
SELECT O.*
	-- GENERATED orders that were GAUGED to COMPLETION or SKIPPED will show "Gauger [gauger status]"
	, PrintStatus = CASE WHEN O.StatusID = -10 AND GOS.IsComplete = 1 THEN 'Gauger ' + GOS.Name ELSE OPS.OrderStatus END
FROM (
	SELECT O.*
	, Origin = vO.Name 
	, OriginFull = vO.FullName 
	, OriginState = vO.State 
	, OriginStateAbbrev = vO.StateAbbrev 
	, OriginStation = vO.Station
	, OriginCounty = vO.County 
	, vO.LeaseName
	, vO.LeaseNum
	, OriginRegion = vO.Region
	, OriginRegionID = vO.RegionID
	, OriginCountryID = vO.CountryID
	, OriginCountry = vO.Country
	, OriginCountryShort = vO.CountryShort
	, OriginLegalDescription = vO.LegalDescription 
	, OriginNDIC = vO.NDICFileNum 
	, OriginNDM = vO.NDM 
	, OriginCA = vO.CA 
	, OriginTimeZoneID = vO.TimeZoneID 
	, OriginUseDST = vO.UseDST 
	, OriginStateID = vO.StateID
	, vO.H2S
	, Destination = vD.Name 
	, DestinationFull = vD.FullName 
	, DestinationState = vD.State 
	, DestinationStateAbbrev = vD.StateAbbrev 
	, DestinationTypeID = vD.ID
	, vD.DestinationType
	, DestStation = vD.Station 
	, DestTimeZoneID = vD.TimeZoneID 
	, DestUseDST = vD.UseDST 
	, DestStateID = vD.StateID
	, DestRegion = vO.Region
	, DestRegionID = vO.RegionID
	, DestCountryID = vD.CountryID
	, DestCountry = vD.Country
	, DestCountryShort = vD.CountryShort
	, Customer = C.Name 
	, Carrier = CA.Name 
	, CarrierType = CT.Name 
	, OS.OrderStatus
	, OS.StatusNum
	, Driver = D.FullName 
	, DriverFirst = D.FirstName 
	, DriverLast = D.LastName 
	, Truck = TRU.FullName 
	, Trailer = TR1.FullName 
	, Trailer2 = TR2.FullName 
	, P.PriorityNum
	, TicketType = TT.Name 
	, DestTicketTypeID = vD.TicketTypeID 
	, DestTicketType = vD.TicketType 
	, Operator = OP.Name 
	, Producer = PR.Name 
	, Pumper = PU.FullName 
	, DriverNumber = D.IDNumber
	, CarrierNumber = CA.IDNumber
	, CarrierTypeID = CA.CarrierTypeID
	, TruckNumber = TRU.IDNumber 
	, TrailerNumber = TR1.IDNumber 
	, Trailer2Number = TR2.IDNumber 
	, Product = PRO.Name
	, ProductShort = PRO.ShortName 
	, PRO.ProductGroupID
	, PRO.ProductGroup
	, OriginUOM = OUom.Name 
	, OriginUomShort = OUom.Abbrev 
	, OriginTankID_Text = CASE WHEN ORT.TankNum = '*' THEN '*' + isnull(O.OriginTankNum, '?') ELSE ORT.TankNum END 
	, DestUOM = DUom.Name 
	, DestUomShort = DUom.Abbrev 
	, Active = cast((CASE WHEN O.DeleteDateUTC IS NULL THEN 1 ELSE 0 END) as bit) 
	, IsDeleted = cast((CASE WHEN O.DeleteDateUTC IS NOT NULL THEN 1 ELSE 0 END) as bit) 
	, PickupPrintStatus = PPS.Name 
	, PickupCompleted = PPS.IsCompleted 
	, DeliverPrintStatus = DPS.Name 
	, DeliverCompleted = DPS.IsCompleted 
	, PrintStatusID = CASE WHEN O.StatusID = -9 THEN ISNULL(-GAO.StatusID, O.StatusID)
						   WHEN O.StatusID = 8 AND PPS.IsCompleted = 0 THEN 7
						   WHEN O.StatusID = 3 AND DPS.IsCompleted = 0 THEN 8
						ELSE O.StatusID END 
	, OriginTankText = CASE WHEN O.OriginTankID IS NULL OR ORT.TankNum = '*' THEN O.OriginTankNum ELSE ORT.TankNum END
	, OriginWaitNum = OWR.Num
	, OriginWaitDesc = OWR.Description
	, OriginWaitNumDesc = OWR.NumDesc
	, DestWaitNum = DWR.Num
	, DestWaitDesc = DWR.Description
	, DestWaitNumDesc = DWR.NumDesc
	, RejectNum = ORR.Num
	, RejectDesc = ORR.Description
	, RejectNumDesc = ORR.NumDesc
	, RerouteCount = (SELECT COUNT(1) FROM tblOrderReroute ORE WHERE ORE.OrderID = O.ID)
	, TicketCount = (SELECT COUNT(1) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND OT.DeleteDateUTC IS NULL)
	, TotalMinutes = isnull(OriginMinutes, 0) + ISNULL(DestMinutes, 0)
	, GaugerStatusID = GAO.StatusID
	, GaugerID = GAO.GaugerID
	FROM dbo.viewOrderBase O
	LEFT JOIN dbo.viewOrigin vO ON vO.ID = O.OriginID
	LEFT JOIN dbo.viewDestination vD ON vD.ID = O.DestinationID
	LEFT JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
	JOIN dbo.tblOrderStatus AS OS ON OS.ID = O.StatusID
	LEFT JOIN dbo.tblCarrier CA ON CA.ID = O.CarrierID
	LEFT JOIN dbo.tblCarrierType CT ON CT.ID = CA.CarrierTypeID
	LEFT JOIN dbo.viewDriverBase D ON D.ID = O.DriverID
	LEFT JOIN dbo.viewTruck TRU ON TRU.ID = O.TruckID
	LEFT JOIN dbo.viewTrailer TR1 ON TR1.ID = O.TrailerID
	LEFT JOIN dbo.viewTrailer TR2 ON TR2.ID = O.Trailer2ID
	LEFT JOIN dbo.tblPriority P ON P.ID = O.PriorityID
	LEFT JOIN dbo.tblTicketType TT ON TT.ID = O.TicketTypeID
	LEFT JOIN dbo.tblOriginTank ORT ON ORT.ID = O.OriginTankID
	LEFT JOIN dbo.tblOperator OP ON OP.ID = O.OperatorID
	LEFT JOIN dbo.viewPumper PU ON PU.ID = O.PumperID
	LEFT JOIN dbo.tblProducer PR ON PR.ID = O.ProducerID
	LEFT JOIN dbo.viewProduct PRO ON PRO.ID = O.ProductID
	LEFT JOIN dbo.tblUom OUom ON OUom.ID = O.OriginUomID
	LEFT JOIN dbo.tblUom DUom ON DUom.ID = O.DestUomID
	LEFT JOIN dbo.tblPrintStatus PPS ON PPS.ID = O.PickupPrintStatusID
	LEFT JOIN dbo.tblPrintStatus DPS ON DPS.ID = O.DeliverPrintStatusID
	LEFT JOIN dbo.viewOriginWaitReason OWR ON OWR.ID = O.OriginWaitReasonID
	LEFT JOIN dbo.viewDestinationWaitReason DWR ON DWR.ID = O.DestWaitReasonID
	LEFT JOIN dbo.viewOrderRejectReason ORR ON ORR.ID = O.RejectReasonID
	LEFT JOIN dbo.tblGaugerOrder GAO ON GAO.OrderID = O.ID
	LEFT JOIN dbo.viewGauger G ON G.ID = GAO.GaugerID
) O
LEFT JOIN dbo.viewOrderPrintStatus OPS ON OPS.ID = O.PrintStatusID
LEFT JOIN dbo.tblGaugerOrderStatus GOS ON GOS.ID = O.GaugerStatusID
GO

EXEC _spRebuildAllObjects
GO

INSERT INTO tblOrderStatus (ID, OrderStatus, StatusNum, ActionText, CreateDateUTC, CreatedByUser, NextID, EntrySortNum)
	VALUES (-11, 'Gauger Rejected', -11, 'Un-reject', GETUTCDATE(), 'System', NULL, NULL)
GO

/* =============================================
-- Author:		Kevin Alons
-- Create date: 19 Dec 2012
-- Description:	trigger to 
				1) validate any changes, and fail the update if invalid changes are submitted
				2) update the tblOrder table with revelant and appropriate changes
				3) if DBAudit is turned on, save an audit record for this Order change
-- =============================================*/
ALTER TRIGGER trigGaugerOrder_U ON tblGaugerOrder AFTER UPDATE AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @changesFound bit
		-- Note: the TRIGGER_NESTLEVEL( OBJECT_ID('trigGaugerOrder_U') ) = 1 statement prevents recursion
	IF (TRIGGER_NESTLEVEL( OBJECT_ID('trigGaugerOrder_U')) = 1) BEGIN

		/**********  START OF VALIDATION SECTION ************************/
		IF (
			   UPDATE(TicketTypeID)
			OR UPDATE(StatusID)
			OR UPDATE(OriginTankID)
			OR UPDATE(OriginTankNum)
			OR UPDATE(ArriveTimeUTC)
			OR UPDATE(DepartTimeUTC)
			OR UPDATE(GaugerID)
			OR UPDATE(Rejected)
			OR UPDATE(RejectReasonID)
			OR UPDATE(RejectNotes)
			OR UPDATE(PrintDateUTC)
			OR UPDATE(GaugerNotes)
			OR UPDATE(DueDate)
			OR UPDATE(PriorityID)
			OR UPDATE(CreateDateUTC)
			OR UPDATE(CreatedByUser)
			OR UPDATE(LastChangeDateUTC)
			OR UPDATE(LastChangedByUser)
			OR UPDATE(DispatchNotes) 
		) 
		BEGIN
			IF EXISTS ( -- if any affected orders are in AUDITED status
				SELECT * 
				FROM tblOrder O
				JOIN inserted i ON i.OrderID = O.ID
				WHERE O.StatusID IN (4) -- AUDITED
			)
			BEGIN				
				-- if the "Allow Audited Order Update (temp only)" setting is TRUE then only warn when this occurs
				IF (dbo.fnToBool(dbo.fnSettingValue(35)) = 1)
					PRINT 'Gauger data for AUDITED order(s) are being modified - please investigate why!'
				ELSE BEGIN
					-- otherwise (normal behavior) - prevent AUDITED orders from being modified (except to UN-AUDIT them)
					RAISERROR('Gauger data for AUDITED orders cannot be modified!', 16, 1)
					IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
					RETURN
				END
			END
		END
		ELSE -- NO CHANGES DETECTED SO JUST QUIT
			RETURN

		PRINT 'trigGaugerOrder_U FIRED'
					
		/**********  END OF VALIDATION SECTION ************************/

		-- update the associated, relevant fields on the underlying order when base order is still in GENERATED status
		UPDATE tblOrder
			SET OriginTankID = i.OriginTankID
				, OriginTankNum = i.OriginTankNum
				, Rejected = i.Rejected
				, RejectReasonID = i.RejectReasonID
				, RejectNotes = i.RejectNotes
				, LastChangeDateUTC = i.LastChangeDateUTC
				, LastChangedByUser = i.LastChangedByUser
				-- advance the order to GENERATED when the Gauger Status is COMPLETED (or GAUGER REJECTED if order was rejected)
				, StatusID = CASE WHEN GOS.IsComplete = 1 THEN CASE WHEN i.Rejected = 1 THEN -11 ELSE -10 END ELSE O.StatusID END
		FROM tblOrder O
		JOIN inserted i ON i.OrderID = O.ID
		JOIN deleted d ON d.OrderID = i.OrderID
		JOIN tblGaugerOrderStatus GOS ON GOS.ID = i.StatusID
		WHERE O.StatusID IN (-9, -10) -- GAUGER, GENERATED
		  AND (i.StatusID <> d.StatusID
			OR isnull(i.OriginTankID, 0) <> isnull(d.OriginTankID, 0)
			OR isnull(i.OriginTankNum, '') <> isnull(d.OriginTankNum, '')
			OR i.Rejected <> d.Rejected
			OR ISNULL(i.RejectReasonID, 0) <> ISNULL(d.RejectReasonID, 0)
			OR ISNULL(i.RejectNotes, '') <> ISNULL(d.RejectNotes, ''))
		
		-- delete any records that no longer apply because now the record is valid for the new gauger/status
		DELETE FROM tblGaugerOrderVirtualDelete
		FROM tblGaugerOrderVirtualDelete GOVD
		JOIN tblOrder O ON O.ID = GOVD.OrderID
		JOIN inserted i ON i.OrderID = GOVD.OrderID AND i.GaugerID = GOVD.GaugerID
		WHERE O.StatusID IN (-9, -10) -- GAUGER, GENERATED

		-- record that the gauger order used to belong to the former Gauger (show it as deleted for this user)
		INSERT INTO tblGaugerOrderVirtualDelete (OrderID, GaugerID, VirtualDeleteDateUTC, VirtualDeletedByUser)
			SELECT d.OrderID, d.GaugerID, i.LastChangeDateUTC, i.LastChangedByUser
			FROM deleted d
			JOIN inserted i ON i.OrderID = d.OrderID
			WHERE d.GaugerID <> i.GaugerID
			
		IF EXISTS (SELECT * FROM inserted EXCEPT SELECT * FROM deleted)
		BEGIN
			-- optionally add tblGaugerOrderDBAudit records
			BEGIN TRY
				IF EXISTS(SELECT * FROM tblSetting WHERE ID = 32 AND (Value LIKE 'true' OR Value LIKE 'yes'))
					INSERT INTO tblGaugerOrderDbAudit (DBAuditDate, OrderID, StatusID, OriginTankID, OriginTankNum, ArriveTimeUTC, DepartTimeUTC, GaugerID, TicketTypeID, Rejected, RejectReasonID, RejectNotes, Handwritten, PrintDateUTC, GaugerNotes, DueDate, PriorityID, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DispatchNotes)
						SELECT GETUTCDATE(), OrderID, StatusID, OriginTankID, OriginTankNum, ArriveTimeUTC, DepartTimeUTC, GaugerID, TicketTypeID, Rejected, RejectReasonID, RejectNotes, Handwritten, PrintDateUTC, GaugerNotes, DueDate, PriorityID, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DispatchNotes
						FROM deleted d
			END TRY
			BEGIN CATCH
				PRINT 'trigGaugerOrder_U.DBAUDIT FAILURE: ' + ERROR_MESSAGE()
			END CATCH
		END
		
	PRINT 'trigGaugerOrder_U COMPLETE'

	END
END


GO
EXEC sp_settriggerorder @triggername=N'[dbo].[trigGaugerOrder_U]', @order=N'First', @stmttype=N'UPDATE'
GO

COMMIT
SET NOEXEC OFF