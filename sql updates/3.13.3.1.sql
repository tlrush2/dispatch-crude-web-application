SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20) = '3.13.3'
	, @NewVersion varchar(20) = '3.13.3.1'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1597 - Update Corrected Gravity formula to accurately display the Gravity @ 60F'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/***********************************************************************/
-- Date Created: 18 Apr 2013
-- Author: Kevin Alons
-- Purpose: given the parameters, convert UncorrectedAPIGravity to CorrectedAPIGravity
-- Source: http://ioilfield.com/units_correl/o_api_co.html
-- Changes:
--	x.x.x	07/20/2016	JAE		Corrected formula to match the 11.1 API documentation
/***********************************************************************/
ALTER FUNCTION fnCorrectedAPIGravity (@ObsGravity decimal(18,8), @ObsTemp decimal(8,3))
RETURNS DECIMAL(18,1)
BEGIN
	DECLARE @WATER_DENSITY_60F FLOAT = 999.012
	DECLARE @K0 FLOAT = 341.0957
	DECLARE @K1 FLOAT = 0.0000

	--calculate difference in observed and base temps
	DECLARE @delta FLOAT
	SELECT @delta = @obsTemp - 60.0

	--compute hydrometer correction term
	DECLARE @hyc FLOAT = 1.0 - ROUND(0.00001278 * @delta,9) - ROUND(0.0000000062 * @delta * @delta,9)

	--convert API gravity to density
	DECLARE @rho FLOAT = ROUND((141.5 * @WATER_DENSITY_60F)/(131.5 + @obsGravity), 2)

    -- Apply hydrometer correction
	DECLARE @rhoT FLOAT = ROUND(@rho * @hyc, 2)

	-- Initialize density @ 60F
	DECLARE @rho60 FLOAT = @rhoT

	DECLARE @rho60prev FLOAT = @rho60 -1 -- something to trigger the first loop
	DECLARE @alpha FLOAT
	DECLARE @vcf FLOAT

	WHILE ABS(@rho60 - @rho60prev) >= .05
	BEGIN
		-- calculate coefficient of thermal expansion
		SELECT @alpha = ROUND(@K0 / @rho60 / @rho60 + @K1 / @rho60, 7)

		-- calculate volume correction factor
		SELECT @vcf = ROUND(EXP(ROUND(-@alpha * @delta - 0.8 * @alpha * @alpha * @delta * @delta, 8)),6)

		SELECT @rho60prev = @rho60
		SELECT @rho60 = ROUND(@rhoT/@vcf, 3, 1)
	END

	RETURN ROUND(141.5 * @WATER_DENSITY_60F / @rho60 - 131.5, 1)
END

GO


/***********************************************************************/
-- Date Created: 20 Jul 2016
-- Author: Joe Engler
-- Purpose: given the parameters, calculate the Volume Correction Factor
-- Changes:
/***********************************************************************/
CREATE FUNCTION fnVCF (@Gravity60F decimal(18,8), @ObsTemp decimal(8,3))
RETURNS FLOAT
BEGIN
	DECLARE @WATER_DENSITY_60F FLOAT = 999.012
	DECLARE @K0 FLOAT = 341.0957
	DECLARE @K1 FLOAT = 0.0000

	--calculate difference in observed and base temps
	DECLARE @delta FLOAT = @obsTemp - 60.0

	--convert API gravity to density
	DECLARE @rho FLOAT = ROUND((141.5 * @WATER_DENSITY_60F)/(131.5 + @gravity60f), 2)

	-- calculate coefficient of thermal expansion
	DECLARE @alpha FLOAT = ROUND(@K0 / @rho / @rho + @K1 / @rho, 7)
	--SELECT @factor10 = (341.0957/((1-(0.00001278*(@ambientTemp-60))-((0.0000000062)*((@ambientTemp-60)*(@ambientTemp-60))))*(141360.198/(@const5+@CorrGravity))))/((1-(@const3*(@ambientTemp-60))-((@const6)*((@ambientTemp-60)*(@ambientTemp-60))))*(141360.198/(@const5+@CorrGravity)))

	-- calculate volume correction factor
	DECLARE @vcf FLOAT = ROUND(EXP(ROUND(-@alpha * @delta - 0.8 * @alpha * @alpha * @delta * @delta, 8)),6)
	IF @vcf < 1 
		SELECT @vcf = ROUND(@vcf, 5)
	ELSE
		SELECT @vcf = ROUND(@vcf, 4)

	RETURN @vcf
END

GO


/***********************************/
-- Date Created: 20 Jul 2016
-- Author: Joe Engler
-- Purpose: New attempt at calculating NSV to appease the Devon gods
/***********************************/
CREATE FUNCTION dbo.fnCrudeNetCalculatorNEW
(
  @Gross float
, @ObsTemp float
, @OpenTemp float
, @CloseTemp float
, @ObsGravity float
, @BSW float
, @SigFig int = 2
) RETURNS FLOAT AS
BEGIN
	-- Round significant digits
	SET @Gross = ROUND(@Gross, @SigFig);
	SET @ObsTemp = ROUND(@ObsTemp, 1);
	SET @OpenTemp = ROUND(@OpenTemp, 1);
	SET @CloseTemp = ROUND(@CloseTemp, 1);
	SET @ObsGravity = ROUND(@ObsGravity, 1);
	SET @BSW = ROUND(@BSW, 3);

	DECLARE @ambientTemp FLOAT

	--calculate the ambient temp
	IF (@OpenTemp IS NULL OR @OpenTemp = 0 OR @CloseTemp IS NULL OR @CloseTemp = 0)
		SELECT @ambientTemp = @ObsTemp
	ELSE
		SELECT @ambientTemp = ( (@OpenTemp + @CloseTemp) / 2 ); /* Calculate the mean ambient temperature (primary temp operand) */

	-- calculate volume correction factor
	DECLARE @vcf FLOAT = dbo.fnVCF(dbo.fnCorrectedAPIGravity(@obsGravity, @ambientTemp), @ambientTemp)


	RETURN ROUND(@gross * @vcf * (1 - @bsw/100), @SigFig)
END


GO



COMMIT
SET NOEXEC OFF