SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.5.14.2'
SELECT  @NewVersion = '4.5.15'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'JT-595 & JT-656 - Add several notes fields to approval page.  Also add approval notes to approval page, order details page, and Report Center.'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO



-- Add notes column to order approval table
ALTER TABLE tblOrderApproval
	ADD Notes VARCHAR(500) NULL
GO



-- ---------------------------
-- Add field to ReportCenter
-- --------------------------
SET IDENTITY_INSERT tblReportColumnDefinition ON

INSERT INTO tblReportColumnDefinition 
	(ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport, IsTicketField)
SELECT 374, 1, 'ApprovalNotes', 'SETTLEMENT | Approval Notes', NULL, NULL, 1, NULL, 1, '*', 0, 0

SET IDENTITY_INSERT tblReportColumnDefinition OFF
GO

-- Fix the filter option on the existing order approved? field
UPDATE tblReportColumnDefinition
	SET FilterTypeID = 5 
		, FilterDropDownSql = 'SELECT ID=1, Name=''TRUE'' UNION SELECT 0, ''FALSE'''
		, FilterAllowCustomText = 0
WHERE ID = 90028
GO


EXEC sp_refreshview viewReportColumnDefinition
GO


/*************************************************************
-- Date Created: 15 Jun 2015
-- Author: Kevin Alons
-- Purpose: return the base data for the Order Approval page
-- Changes:
	- 3.9.2		- 2015/08/25 - KDA - remove obsolete OrderDateUTC & DeliverDateUTC fields (which were used to optimize date searches when OrderDate was virtual)
	- 3.9.20	- 2015/10/26 - JAE - Add columns for transfer percentage and override percentage
	- 3.9.25	- 2015/11/10 - JAE - Use origin driver for approval page
	- 3.9.36	- 2015/12/21 - JAE - Add columns for min settlement units (carrier and shipper)
	- 3.10.13	- 2016/02/29 - JAE - Add Truck Type
	- 3.11.17.2	- 2016/04/18 - JAE - Filter producer settled orders
	- 3.11.19	- 2016/05/02 - BSB - Add Job number and Contract number
	- 4.4.1		- 2016/11/04 - JAE - Add Destination Chainup
	- 4.5.0		- 2017/02/03 - JAE - Add Terminal fields
	- 4.5.15		- 2017/03/03 - BSB - Add Reroute Notes and Previous Destinations columns
*************************************************************/
ALTER VIEW viewOrderApprovalSource
AS
	SELECT O.ID
		, O.StatusID
		, Status = O.OrderStatus
		, O.OrderNum
		, O.JobNumber
		, O.ContractNumber
		, O.OrderDate
		, DeliverDate = ISNULL(O.DeliverDate, O.OrderDate)
		, ShipperID = O.CustomerID
		, O.CarrierID
		, DriverID = O.OriginDriverID
		, TruckID = O.OriginTruckID
		, O.TrailerID
		, O.Trailer2ID
		, O.ProducerID
		, O.OperatorID
		, O.PumperID
		, Shipper = O.Customer
		, O.Carrier
		, Driver = O.OriginDriver
		, O.Truck
		, O.Trailer
		, O.Trailer2
		, O.Producer
		, O.Operator
		, O.Pumper
		, O.TicketTypeID
		, O.TicketType
		, O.ProductID
		, O.Product
		, O.ProductGroupID
		, O.ProductGroup
		, O.TruckTypeID
		, O.TruckType
    
		, O.Rejected
		, RejectReason = O.RejectNumDesc
		, O.RejectNotes
		, O.OriginChainUp
		, OA.OverrideOriginChainup
		, O.DestChainUp
		, OA.OverrideDestChainup
		, O.H2S
		, OA.OverrideH2S
		, O.DispatchConfirmNum
		, O.DispatchNotes
		, O.PickupDriverNotes
		, O.DeliverDriverNotes
		, Approved = CAST(ISNULL(OA.Approved, 0) AS BIT)
		, OSS.WaitFeeParameterID, O.AuditNotes
		
		, O.OriginID
		, O.Origin
		, O.OriginRegionID		-- 4.5.15
		, O.OriginStateID
		, O.OriginArriveTime
		, O.OriginDepartTime
		, O.OriginMinutes
		, O.OriginUomID
		, O.OriginUOM
		, O.OriginGrossUnits
		, O.OriginGrossStdUnits
		, O.OriginNetUnits
		, OriginWaitReason = OWR.NumDesc
		, O.OriginWaitNotes
		, ShipperOriginWaitBillableMinutes = OSS.OriginWaitBillableMinutes
		, CarrierOriginWaitBillableMinutes = OSC.OriginWaitBillableMinutes
		, OA.OverrideOriginMinutes 
		
		, O.DestinationID
		, O.Destination
		, O.DestRegionID		-- 4.5.15
		, O.DestStateID
		, O.DestArriveTime
		, O.DestDepartTime
		, O.DestMinutes
		, O.DestUomID
		, O.DestUOM
		, O.DestGrossUnits
		, O.DestNetUnits
		, O.DestOpenMeterUnits
		, O.DestCloseMeterUnits
		, DestWaitReason = OWR.NumDesc
		, O.DestWaitNotes
		, ShipperDestinationWaitBillableMinutes = OSS.DestinationWaitBillableMinutes
		, CarrierDestinationWaitBillableMinutes = OSC.DestinationWaitBillableMinutes
		, OA.OverrideDestMinutes
	
		, Rerouted = CAST(CASE WHEN ORD.OrderID IS NULL THEN 0 ELSE 1 END AS BIT)
		, RerouteNotes = ORE.Notes		-- 4.5.15
		, ORD.PreviousDestinations		-- 4.5.15
		, OA.OverrideReroute
		, O.ActualMiles
		, RerouteMiles = ROUND(ORD.RerouteMiles, 0)
		, OA.OverrideActualMiles
		, IsTransfer = CAST(CASE WHEN OT.OrderID IS NULL THEN 0 ELSE 1 END AS BIT)
		, TransferPercentComplete = ISNULL(OT.PercentComplete,100)

		, OA.OverrideTransferPercentComplete
		, CarrierMinSettlementUnits = OSC.MinSettlementUnits
		, CarrierMinSettlementUOM = CUOM.Abbrev
		, OA.OverrideCarrierMinSettlementUnits
		, ShipperMinSettlementUnits = OSS.MinSettlementUnits
		, ShipperMinSettlementUOM = CUOM.Abbrev
		, OA.OverrideShipperMinSettlementUnits
		
		, OA.CreateDateUTC
		, OA.CreatedByUser
		, ApprovalNotes = OA.Notes		-- 4.5.15
		
		, OrderID = OSC.OrderID | OSS.OrderID -- NULL if either are NULL - if NULL indicates that the order is not yet rates (used below to rate these)
		, O.OriginTerminalID
		, O.DestTerminalID
		, O.DriverTerminalID
	FROM viewOrderLocalDates O
	OUTER APPLY dbo.fnOrderRerouteData(O.ID) ORD
	LEFT JOIN tblOrderReroute ORE ON ORE.OrderID = O.ID
	LEFT JOIN tblOrderTransfer OT ON OT.OrderID = O.ID
	LEFT JOIN tblOrderSettlementCarrier OSC ON OSC.OrderID = O.ID
	LEFT JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = O.ID
	LEFT JOIN tblOrderSettlementProducer OSP ON OSP.OrderID = O.ID
	LEFT JOIN viewOriginWaitReason OWR ON OWR.ID = O.OriginWaitReasonID
	LEFT JOIN viewDestinationWaitReason DWR ON DWR.ID = O.DestWaitReasonID
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
	LEFT JOIN viewOrderSettlementUnitsCarrier OSUC ON OSUC.OrderID = O.ID
	LEFT JOIN viewOrderSettlementUnitsShipper OSUS ON OSUS.OrderID = O.ID
	LEFT JOIN tblUom CUOM ON CUOM.ID = OSUC.MinSettlementUomID
	LEFT JOIN tblUom SUOM ON SUOM.ID = OSUS.MinSettlementUomID
	WHERE O.StatusID IN (4) /* only include orders that are marked AUDITED */
	  AND OSC.BatchID IS NULL AND OSS.BatchID IS NULL AND OSP.BatchID IS NULL /* don't show orders that have been settled */
GO


EXEC sp_refreshview viewOrderApprovalSource
GO



/***********************************/
-- Created: ?.?.? - 2016/07/08 - Kevin Alons
-- Purpose: return Report Center Order data without GPS data
-- Changes:
	-- 4.4.1	2016/11/04 - JAE - Add Destination Chainup
	-- 4.4.17	2017/01/10 - BSB - Add Settlement Batch Date and Invoice Num for Driver, Carrier, and Producer Settlement + Batch Date for Shipper
	-- 4.5.15	2017/03/06 - BSB - Add order approval notes
/***********************************/
ALTER VIEW viewReportCenter_Orders_NoGPS AS
	SELECT X.*
	FROM (
		SELECT O.*
			--
			, ShipperBatchNum = SS.BatchNum
			, ShipperBatchDate = SSB.BatchDate		-- 4.4.17
			, ShipperBatchInvoiceNum = SSB.InvoiceNum
			, ShipperSettlementUomID = SS.SettlementUomID
			, ShipperSettlementUom = SS.SettlementUom
			, ShipperMinSettlementUnits = SS.MinSettlementUnits
			, ShipperSettlementUnits = SS.SettlementUnits
			, ShipperRateSheetRate = SS.RateSheetRate
			, ShipperRateSheetRateType = SS.RateSheetRateType
			, ShipperRouteRate = SS.RouteRate
			, ShipperRouteRateType = SS.RouteRateType
			, ShipperLoadRate = ISNULL(SS.RouteRate, SS.RateSheetRate)
			, ShipperLoadRateType = ISNULL(SS.RouteRateType, SS.RateSheetRateType)
			, ShipperLoadAmount = SS.LoadAmount
			, ShipperOrderRejectRate = SS.OrderRejectRate 
			, ShipperOrderRejectRateType = SS.OrderRejectRateType 
			, ShipperOrderRejectAmount = SS.OrderRejectAmount 
			, ShipperWaitFeeSubUnit = SS.WaitFeeSubUnit  
			, ShipperWaitFeeRoundingType = SS.WaitFeeRoundingType  
			, ShipperOriginWaitBillableHours = SS.OriginWaitBillableHours  
			, ShipperOriginWaitBillableMinutes = SS.OriginWaitBillableMinutes  
			, ShipperOriginWaitRate = SS.OriginWaitRate
			, ShipperOriginWaitAmount = SS.OriginWaitAmount
			, ShipperDestinationWaitBillableHours = SS.DestinationWaitBillableHours 
			, ShipperDestinationWaitBillableMinutes = SS.DestinationWaitBillableMinutes   
			, ShipperDestinationWaitRate = SS.DestinationWaitRate  
			, ShipperDestinationWaitAmount = SS.DestinationWaitAmount  
			, ShipperTotalWaitAmount = SS.TotalWaitAmount
			, ShipperTotalWaitBillableMinutes = NULLIF(ISNULL(SS.OriginWaitBillableMinutes, 0) + ISNULL(SS.DestinationWaitBillableMinutes, 0), 0)
			, ShipperTotalWaitBillableHours = NULLIF(ISNULL(SS.OriginWaitBillableHours, 0) + ISNULL(SS.DestinationWaitBillableHours, 0), 0)		
			, ShipperFuelSurchargeRate = SS.FuelSurchargeRate
			, ShipperFuelSurchargeAmount = SS.FuelSurchargeAmount
			, ShipperOriginChainupRate = SS.OriginChainupRate
			, ShipperOriginChainupRateType = SS.OriginChainupRateType  
			, ShipperOriginChainupAmount = SS.OriginChainupAmount
			, ShipperDestChainupRate = SS.DestChainupRate
			, ShipperDestChainupRateType = SS.DestChainupRateType  
			, ShipperDestChainupAmount = SS.DestChainupAmount
			, ShipperRerouteRate = SS.RerouteRate
			, ShipperRerouteRateType = SS.RerouteRateType  
			, ShipperRerouteAmount = SS.RerouteAmount
			, ShipperSplitLoadRate = SS.SplitLoadRate
			, ShipperSplitLoadRateType = SS.SplitLoadRateType  
			, ShipperSplitLoadAmount = SS.SplitLoadAmount
			, ShipperH2SRate = SS.H2SRate
			, ShipperH2SRateType = SS.H2SRateType  
			, ShipperH2SAmount = SS.H2SAmount
			, ShipperTaxRate = SS.OriginTaxRate
			, ShipperTotalAmount = SS.TotalAmount
			, ShipperDestCode = CDC.Code
			, ShipperTicketType = STT.TicketType
			--
			, CarrierBatchNum = SC.BatchNum			
			, CarrierBatchDate = CSB.BatchDate		-- 4.4.17
			, CarrierBatchInvoiceNum = CSB.InvoiceNum		-- 4.4.17
			, CarrierSettlementUomID = SC.SettlementUomID
			, CarrierSettlementUom = SC.SettlementUom
			, CarrierMinSettlementUnits = SC.MinSettlementUnits
			, CarrierSettlementUnits = SC.SettlementUnits
			, CarrierRateSheetRate = SC.RateSheetRate
			, CarrierRateSheetRateType = SC.RateSheetRateType
			, CarrierRouteRate = SC.RouteRate
			, CarrierRouteRateType = SC.RouteRateType
			, CarrierLoadRate = ISNULL(SC.RouteRate, SC.RateSheetRate)
			, CarrierLoadRateType = ISNULL(SC.RouteRateType, SC.RateSheetRateType)
			, CarrierLoadAmount = SC.LoadAmount
			, CarrierOrderRejectRate = SC.OrderRejectRate 
			, CarrierOrderRejectRateType = SC.OrderRejectRateType 
			, CarrierOrderRejectAmount = SC.OrderRejectAmount 
			, CarrierWaitFeeSubUnit = SC.WaitFeeSubUnit  
			, CarrierWaitFeeRoundingType = SC.WaitFeeRoundingType  
			, CarrierOriginWaitBillableHours = SC.OriginWaitBillableHours  
			, CarrierOriginWaitBillableMinutes = SC.OriginWaitBillableMinutes  
			, CarrierOriginWaitRate = SC.OriginWaitRate
			, CarrierOriginWaitAmount = SC.OriginWaitAmount
			, CarrierDestinationWaitBillableHours = SC.DestinationWaitBillableHours 
			, CarrierDestinationWaitBillableMinutes = SC.DestinationWaitBillableMinutes  
			, CarrierDestinationWaitRate = SC.DestinationWaitRate 
			, CarrierDestinationWaitAmount = SC.DestinationWaitAmount  
			, CarrierTotalWaitAmount = SC.TotalWaitAmount
			, CarrierTotalWaitBillableMinutes = NULLIF(ISNULL(SC.OriginWaitBillableMinutes, 0) + ISNULL(SC.DestinationWaitBillableMinutes, 0), 0)
			, CarrierTotalWaitBillableHours = NULLIF(ISNULL(SC.OriginWaitBillableHours, 0) + ISNULL(SC.DestinationWaitBillableHours, 0), 0)		
			, CarrierFuelSurchargeRate = SC.FuelSurchargeRate
			, CarrierFuelSurchargeAmount = SC.FuelSurchargeAmount
			, CarrierOriginChainupRate = SC.OriginChainupRate
			, CarrierOriginChainupRateType = SC.OriginChainupRateType  
			, CarrierOriginChainupAmount = SC.OriginChainupAmount
			, CarrierDestChainupRate = SC.DestChainupRate
			, CarrierDestChainupRateType = SC.DestChainupRateType  
			, CarrierDestChainupAmount = SC.DestChainupAmount
			, CarrierRerouteRate = SC.RerouteRate
			, CarrierRerouteRateType = SC.RerouteRateType  
			, CarrierRerouteAmount = SC.RerouteAmount
			, CarrierSplitLoadRate = SC.SplitLoadRate
			, CarrierSplitLoadRateType = SC.SplitLoadRateType  
			, CarrierSplitLoadAmount = SC.SplitLoadAmount
			, CarrierH2SRate = SC.H2SRate
			, CarrierH2SRateType = SC.H2SRateType  
			, CarrierH2SAmount = SC.H2SAmount
			, CarrierTaxRate = SC.OriginTaxRate
			, CarrierTotalAmount = SC.TotalAmount
			--
			, DriverBatchNum = SD.BatchNum
			, DriverBatchDate = DSB.BatchDate		-- 4.4.17
			, DriverBatchInvoiceNum = DSB.InvoiceNum		-- 4.4.17
			, DriverSettlementUomID = SD.SettlementUomID
			, DriverSettlementUom = SD.SettlementUom
			, DriverMinSettlementUnits = SD.MinSettlementUnits
			, DriverSettlementUnits = SD.SettlementUnits
			, DriverRateSheetRate = SD.RateSheetRate
			, DriverRateSheetRateType = SD.RateSheetRateType
			, DriverRouteRate = SD.RouteRate
			, DriverRouteRateType = SD.RouteRateType
			, DriverLoadRate = ISNULL(SD.RouteRate, SD.RateSheetRate)
			, DriverLoadRateType = ISNULL(SD.RouteRateType, SD.RateSheetRateType)
			, DriverLoadAmount = SD.LoadAmount
			, DriverOrderRejectRate = SD.OrderRejectRate 
			, DriverOrderRejectRateType = SD.OrderRejectRateType 
			, DriverOrderRejectAmount = SD.OrderRejectAmount 
			, DriverWaitFeeSubUnit = SD.WaitFeeSubUnit  
			, DriverWaitFeeRoundingType = SD.WaitFeeRoundingType  
			, DriverOriginWaitBillableHours = SD.OriginWaitBillableHours  
			, DriverOriginWaitBillableMinutes = SD.OriginWaitBillableMinutes  
			, DriverOriginWaitRate = SD.OriginWaitRate
			, DriverOriginWaitAmount = SD.OriginWaitAmount
			, DriverDestinationWaitBillableHours = SD.DestinationWaitBillableHours 
			, DriverDestinationWaitBillableMinutes = SD.DestinationWaitBillableMinutes  
			, DriverDestinationWaitRate = SD.DestinationWaitRate 
			, DriverDestinationWaitAmount = SD.DestinationWaitAmount  
			, DriverTotalWaitAmount = SD.TotalWaitAmount
			, DriverTotalWaitBillableMinutes = NULLIF(ISNULL(SD.OriginWaitBillableMinutes, 0) + ISNULL(SD.DestinationWaitBillableMinutes, 0), 0)
			, DriverTotalWaitBillableHours = NULLIF(ISNULL(SD.OriginWaitBillableHours, 0) + ISNULL(SD.DestinationWaitBillableHours, 0), 0)		
			, DriverFuelSurchargeRate = SD.FuelSurchargeRate
			, DriverFuelSurchargeAmount = SD.FuelSurchargeAmount
			, DriverOriginChainupRate = SD.OriginChainupRate
			, DriverOriginChainupRateType = SD.OriginChainupRateType  
			, DriverOriginChainupAmount = SD.OriginChainupAmount
			, DriverDestChainupRate = SD.DestChainupRate
			, DriverDestChainupRateType = SD.DestChainupRateType  
			, DriverDestChainupAmount = SD.DestChainupAmount
			, DriverRerouteRate = SD.RerouteRate
			, DriverRerouteRateType = SD.RerouteRateType  
			, DriverRerouteAmount = SD.RerouteAmount
			, DriverSplitLoadRate = SD.SplitLoadRate
			, DriverSplitLoadRateType = SD.SplitLoadRateType  
			, DriverSplitLoadAmount = SD.SplitLoadAmount
			, DriverH2SRate = SD.H2SRate
			, DriverH2SRateType = SD.H2SRateType  
			, DriverH2SAmount = SD.H2SAmount
			, DriverTaxRate = SD.OriginTaxRate
			, DriverTotalAmount = SD.TotalAmount
			--
			, ProducerBatchNum = SP.BatchNum
			, ProducerBatchDate = PSB.BatchDate		-- 4.4.17
			, ProducerBatchInvoiceNum = PSB.InvoiceNum		-- 4.4.17
			, ProducerSettlementUomID = SP.SettlementUomID
			, ProducerSettlementUom = SP.SettlementUom
			, ProducerSettlementUnits = SP.SettlementUnits
			, ProducerCommodityIndex = SP.CommodityIndex
			, ProducerCommodityIndexID = SP.CommodityIndexID
			, ProducerCommodityMethod = SP.CommodityMethod
			, ProducerCommodityMethodID = SP.CommodityMethodID
			, ProducerIndexPrice = SP.Price
			, ProducerIndexStartDate = SP.IndexStartDate
			, ProducerIndexEndDate = SP.IndexEndDate
			, ProducerPriceAmount = SP.CommodityPurchasePricebookPriceAmount
			, ProducerDeduct = SP.Deduct
			, ProducerDeductType = SP.Deduct
			, ProducerDeductAmount = SP.CommodityPurchasePricebookDeductAmount
			, ProducerPremium = SP.PremiumType
			, ProducerPremiumType = SP.PremiumType
			, ProducerPremiumAmount = SP.CommodityPurchasePricebookPremiumAmount
			, ProducerPremiumDesc = SP.PremiumDesc
			, ProducerTotalAmount = SP.CommodityPurchasePricebookTotalAmount
			--
			, OriginLatLon = LTRIM(OO.LAT) + ',' + LTRIM(OO.LON)
			, OriginGeoFenceRadiusMeters = OO.GeoFenceRadiusMeters
			, OriginCTBNum = OO.CTBNum
			, OriginFieldName = OO.FieldName
			, OriginCity = OO.City																				
			, OriginCityState = OO.City + ', ' + (SELECT Abbreviation FROM tblState WHERE ID = OO.StateID)		
			--
			, DestLatLon = LTRIM(D.LAT) + ',' + LTRIM(D.LON)
			, DestGeoFenceRadiusMeters = D.GeoFenceRadiusMeters
			, DestCity = D.City
			, DestCityState = D.City + ', ' + (SELECT Abbreviation FROM tblState WHERE ID = D.StateID) -- 4.1.0 - fix (was pointing to Origin.StateID)
			--
			, Gauger = GAO.Gauger						
			, GaugerIDNumber = GA.IDNumber
			, GaugerFirstName = GA.FirstName
			, GaugerLastName = GA.LastName
			, GaugerRejected = GAO.Rejected
			, GaugerRejectReasonID = GAO.RejectReasonID
			, GaugerRejectNotes = GAO.RejectNotes
			, GaugerRejectNumDesc = GORR.NumDesc
			, GaugerPrintDateUTC = GAO.PrintDateUTC -- 4.1.0 - added for use by GaugerPrintDate EXPRESSION column for effiency reasons (this is already queried, avoid doing the calculation unless needed)
			--, GaugerPrintDate = dbo.fnUTC_to_Local(GAO.PrintDateUTC, O.OriginTimeZoneID, O.OriginUseDST)
			--
			, T_GaugerCarrierTicketNum = CASE WHEN GAO.TicketCount = 0 THEN LTRIM(GAO.OrderNum) + CASE WHEN GAO.Rejected = 1 THEN 'X' ELSE '' END ELSE GOT.CarrierTicketNum END 
			, T_GaugerTankNum = ISNULL(GOT.OriginTankText, GAO.OriginTankText)
			, T_GaugerIsStrappedTank = GOT.IsStrappedTank 
			, T_GaugerProductObsTemp = GOT.ProductObsTemp
			, T_GaugerProductObsGravity = GOT.ProductObsGravity
			, T_GaugerProductBSW = GOT.ProductBSW		
			, T_GaugerOpeningGaugeFeet = GOT.OpeningGaugeFeet
			, T_GaugerOpeningGaugeInch = GOT.OpeningGaugeInch		
			, T_GaugerOpeningGaugeQ = GOT.OpeningGaugeQ			
			, T_GaugerBottomFeet = GOT.BottomFeet
			, T_GaugerBottomInches = GOT.BottomInches		
			, T_GaugerBottomQ = GOT.BottomQ		
			, T_GaugerRejected = GOT.Rejected
			, T_GaugerRejectReasonID = GOT.RejectReasonID
			, T_GaugerRejectNumDesc = GOT.RejectNumDesc
			, T_GaugerRejectNotes = GOT.RejectNotes	
			--
			--, OrderCreateDate = dbo.fnUTC_to_Local(O.CreateDateUTC, O.OriginTimeZoneID, O.OriginUseDST) -- replaced 4.1.0 with an expression type value (more efficient, only executed when needed)
			, OrderApproved = CAST(ISNULL(OA.Approved, 0) AS BIT)
			, ApprovalNotes = OA.Notes		-- 4.5.15
		FROM viewOrder_OrderTicket_Full O
		JOIN tblOrigin OO ON OO.ID = O.OriginID
		JOIN tblDestination D ON D.ID = O.DestinationID
		--
		LEFT JOIN viewGaugerOrder GAO ON GAO.OrderID = O.ID			            
		LEFT JOIN viewGaugerOrderTicket GOT ON GOT.UID = O.T_UID	            
		LEFT JOIN viewGauger GA ON GA.ID = GAO.GaugerID				            
		LEFT JOIN viewOrderRejectReason GORR ON GORR.ID = GAO.RejectReasonID 
		--
		LEFT JOIN tblCustomerDestinationCode CDC ON CDC.CustomerID = O.CustomerID AND CDC.DestinationID = O.DestinationID
		--
		LEFT JOIN viewOrderSettlementCarrier SC ON SC.OrderID = O.ID
		LEFT JOIN tblCarrierSettlementBatch CSB ON CSB.ID = SC.BatchID		-- 4.4.17
		--
		LEFT JOIN viewOrderSettlementShipper SS ON SS.OrderID = O.ID
		LEFT JOIN tblShipperSettlementBatch SSB ON SSB.ID = SS.BatchID
		--
		LEFT JOIN viewOrderSettlementDriver SD ON SD.OrderID = O.ID
		LEFT JOIN tblDriverSettlementBatch DSB ON DSB.ID = SD.BatchID		-- 4.4.17
		--
		LEFT JOIN viewOrderSettlementProducer SP ON SP.OrderID = O.ID
		LEFT JOIN tblProducerSettlementBatch PSB ON PSB.ID = SP.BatchID		-- 4.4.17
		--
		LEFT JOIN tblShipperTicketType STT ON STT.CustomerID = O.CustomerID AND STT.TicketTypeID = O.TicketTypeID
		--
		LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
	) X
GO



/*************************************************************
-- Date Created: 15 Jun 2015
-- Author: Kevin Alons
-- Purpose: return the data for the Order Approval page
-- Changes:
	- 3.9.2  - 2015/08/26 - KDA - remove obsolete performance optimization using OrderDateUTC to force use of index (now OrderDate is indexed)
	- 3.9.3  - 2015/08/29 - KDA - add new FinalXXX fields
	- 3.9.20 - 2015/10/26 - JAE - update stored procedure to save transfer override values
	- 3.9.37 - 2015/12/21 - JAE - Added carrier and shipper min settlement units
	- 4.4.1	 - 2016/11/04 - JAE	- Add Destination Chainup
	- 4.5.0  - 2017-02-02 - JAE - Add Terminal filter
	- 4.5.15  - 2017-03-03 - BSB - Add Region filter
*************************************************************/
ALTER PROCEDURE spOrderApprovalSource
(
  @userName VARCHAR(100)
, @ShipperID INT = NULL
, @CarrierID INT = NULL
, @ProductGroupID INT = NULL
, @OriginStateID INT = NULL
, @DestStateID INT = NULL
, @TerminalID INT = NULL
, @RegionID INT = NULL
, @Start DATE = NULL
, @End DATE = NULL
, @IncludeApproved BIT = 0
) AS BEGIN
	SET NOCOUNT ON;
	
	SELECT *
		, FinalActualMiles = ISNULL(OverrideActualMiles, ActualMiles)
		, FinalOriginMinutes = ISNULL(OverrideOriginMinutes, OriginMinutes)
		, FinalDestMinutes = ISNULL(OverrideDestMinutes, DestMinutes)
		, FinalOriginChainup = CAST(ISNULL(1-NULLIF(OverrideOriginChainup, 0), OriginChainup) AS BIT)
		, FinalDestChainup = CAST(ISNULL(1-NULLIF(OverrideDestChainup, 0), DestChainup) AS BIT)
		, FinalH2S = CAST(ISNULL(1-NULLIF(OverrideH2S, 0), H2S) AS BIT)
		, FinalRerouted = CAST(ISNULL(1-NULLIF(OverrideReroute, 0), Rerouted) AS BIT)
		, FinalTransferPercentComplete = ISNULL(OverrideTransferPercentComplete, TransferPercentComplete)
		, FinalCarrierMinSettlementUnits = ISNULL(OverrideCarrierMinSettlementUnits, CarrierMinSettlementUnits)
		, FinalShipperMinSettlementUnits = ISNULL(OverrideShipperMinSettlementUnits, ShipperMinSettlementUnits)
	INTO #X
	FROM viewOrderApprovalSource
	WHERE (NULLIF(@ShipperID, -1) IS NULL OR ShipperID = @ShipperID)
	  AND (NULLIF(@CarrierID, -1) IS NULL OR CarrierID = @CarrierID)
	  AND (NULLIF(@TerminalID, -1) IS NULL 
			OR (    (@TerminalID = OriginTerminalID OR OriginTerminalID IS NULL)
			    AND (@TerminalID = DestTerminalID OR DestTerminalID IS NULL)
				AND (@TerminalID = DriverTerminalID OR DriverTerminalID IS NULL)))
	  AND (NULLIF(@RegionID, -1) IS NULL		-- 4.5.15
			OR (	(@RegionID = OriginRegionID OR OriginRegionID IS NULL)
				AND (@RegionID = DestRegionID OR DestRegionID IS NULL)))
	  AND (NULLIF(@ProductGroupID, -1) IS NULL OR ProductGroupID = @ProductGroupID)
	  AND (NULLIF(@OriginStateID, -1) IS NULL OR OriginStateID = @OriginStateID)
	  AND (NULLIF(@DestStateID, -1) IS NULL OR DestStateID = @DestStateID)
	  AND OrderDate BETWEEN ISNULL(@Start, '1/1/1900') AND ISNULL(@End, '1/1/2200')
	  AND (@IncludeApproved = 1 OR ISNULL(Approved, 0) = 0) 
	  
	DECLARE @id int
	WHILE EXISTS (SELECT * FROM #x WHERE OrderID IS NULL)
	BEGIN
		SELECT @id = min(ID) FROM #x WHERE OrderID IS NULL
		EXEC spApplyRatesBoth @id, @userName
		UPDATE #x SET OrderID = @id, Approved = (SELECT Approved FROM tblOrderApproval WHERE OrderID = @id) WHERE ID = @id
	END
	ALTER TABLE #X DROP COLUMN OrderID
	
	SELECT * FROM #x WHERE (@IncludeApproved = 1 OR ISNULL(Approved, 0) = 0) 
END
GO



/***********************************
-- Date Created: 2015/08/14
-- Author: Kevin Alons
-- Purpose: create/update APPROVAL on an individual order (and re-rate the order)
-- Changes:
	3.9.12		- 2015/09/01 - KDA - add SET NOCOUNT ON to statement
				                   - RAISE an ERROR if no record is updated (either due to target record not in valid state or not found
	3.9.20		- 2015/10/03 - JAE - add OverrideTransferPercentComplete parameters/logic
	3.9.37		- 2015/12/21 - JAE - add overrides for carrier and shipper min settlement units
	3.11.17.2	- 2016/04/18 - JAE - add spUpdateOrderApproval
	4.4.1		- 2016/11/04 - JAE - Add Destination Chainup
	4.5.15		- 2017/03/06 - BSB - Add approval notes
***********************************/
ALTER PROCEDURE spUpdateOrderApproval
(
  @ID INT
, @UserName VARCHAR(100)
, @OverrideActualMiles INT = NULL
, @OverrideOriginMinutes INT = NULL
, @OverrideDestMinutes INT = NULL
, @OverrideOriginChainup BIT = NULL
, @OverrideDestChainup BIT = NULL
, @OverrideH2S BIT = NULL
, @OverrideReroute BIT = NULL
, @Approved BIT = 1
, @OverrideTransferPercentComplete INT = NULL
, @OverrideCarrierMinSettlementUnits DECIMAL = NULL
, @OverrideShipperMinSettlementUnits DECIMAL = NULL
, @ApprovalNotes VARCHAR(500) = NULL
)
AS BEGIN
	SET NOCOUNT ON;
	
	/* only allowed AUDITED | non-settled orders from being APPROVED */
	IF EXISTS (
		SELECT O.* 
		FROM tblOrder O 
		LEFT JOIN tblOrderSettlementCarrier OSC ON OSC.OrderID = O.ID
		LEFT JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = O.ID
		LEFT JOIN tblOrderSettlementProducer OSP ON OSP.OrderID = O.ID
		WHERE O.ID = @ID AND O.StatusID IN (4) /* 4 = AUDITED */
		  AND OSC.BatchID IS NULL AND OSS.BatchID IS NULL AND OSP.BatchID IS NULL /* this order is not yet SETTLED */
	)
	BEGIN
		DECLARE @startedTX BIT
		BEGIN TRY
			IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRAN
				SET @startedTX = 1
			END
			UPDATE tblOrderApproval 
				SET OverrideActualMiles = @OverrideActualMiles
					, OverrideOriginMinutes = @OverrideOriginMinutes
					, OverrideDestMinutes = @OverrideDestMinutes
					, OverrideOriginChainup = @OverrideOriginChainup
					, OverrideDestChainup = @OverrideDestChainup
					, OverrideH2S = @OverrideH2S
					, OverrideReroute = @OverrideReroute
					, OverrideTransferPercentComplete = @OverrideTransferPercentComplete
					, OverrideCarrierMinSettlementUnits = @OverrideCarrierMinSettlementUnits 
					, OverrideShipperMinSettlementUnits = @OverrideShipperMinSettlementUnits 
					, Approved = @Approved
					, Notes = @ApprovalNotes
			WHERE OrderID = @ID
			IF (@@ROWCOUNT = 0)
			BEGIN
				INSERT INTO tblOrderApproval 
					(OrderID, OverrideActualMiles, OverrideOriginMinutes, OverrideDestMinutes, OverrideOriginChainup, OverrideDestChainup, OverrideH2S, OverrideReroute
						, OverrideTransferPercentComplete, OverrideCarrierMinSettlementUnits, OverrideShipperMinSettlementUnits
						, Approved, CreatedByUser, CreateDateUTC, Notes) 
				VALUES 
					(@ID, @OverrideActualMiles, @OverrideOriginMinutes, @OverrideDestMinutes, @OverrideOriginChainup, @OverrideDestChainup, @OverrideH2S, @OverrideReroute
						, @OverrideTransferPercentComplete, @OverrideCarrierMinSettlementUnits, @OverrideShipperMinSettlementUnits
						, @Approved, @UserName, GETUTCDATE(), @ApprovalNotes)
			END
			/* ensure any changes to this Approval are applied - which will affect applied rates */
			EXEC spApplyRatesBoth @ID, @UserName
			
			IF (@startedTX = 1)
				COMMIT TRAN
		END TRY
		BEGIN CATCH
			DECLARE @msg VARCHAR(MAX), @severity INT
			SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY()
			IF (@startedTX = 1)
				ROLLBACK TRAN
			RAISERROR(@msg, @severity, 1)
		END CATCH
	END
	ELSE
	BEGIN
		RAISERROR('Record not valid for Approval or not found', 16, 1)
	END
END
GO



EXEC _spRecompileAllStoredProcedures
GO
EXEC _spRebuildAllObjects
GO
EXEC _spRefreshAllViews
GO



COMMIT
SET NOEXEC OFF