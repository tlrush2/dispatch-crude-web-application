/* 
	add DISTINCT to settlement retrieval stored procedures (an attempt to prevent occasional dupes)
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.5.7'
SELECT  @NewVersion = '2.5.8'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
/***********************************/
ALTER PROCEDURE [dbo].[spOrdersExportCarrier]
(
  @StartDate datetime = NULL
, @EndDate datetime = NULL
, @CarrierID int = -1 -- all carriers
, @ProductID int = -1 -- all products
, @BatchID int = NULL  -- either show unbatched (NULL), or the specified batch orders
) AS BEGIN
	SELECT @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	
	SELECT DISTINCT OE.* 
	FROM dbo.viewOrder_Financial_Carrier OE
	WHERE OE.StatusID IN (4)  
	  AND (@CarrierID=-1 OR OE.CarrierID=@CarrierID) 
	  AND (@ProductID=-1 OR OE.ProductID=@ProductID) 
	  AND (@StartDate IS NULL OR OE.OrderDate >= @StartDate) 
	  AND (@EndDate IS NULL OR OE.OrderDate <= @EndDate)
	  AND ((@BatchID IS NULL AND OE.BatchID IS NULL) OR OE.BatchID = @BatchID)
	ORDER BY OE.OriginDepartTimeUTC
	
END

GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
/***********************************/
ALTER PROCEDURE [dbo].[spOrdersExportCarrier]
(
  @StartDate datetime = NULL
, @EndDate datetime = NULL
, @CustomerID int = -1 -- all customers
, @ProductID int = -1 -- all products
, @BatchID int = NULL  -- either show unbatched (NULL), or the specified batch orders
) AS BEGIN
	SELECT @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	
	SELECT DISTINCT OE.* 
	FROM dbo.viewOrder_Financial_Customer OE
	WHERE OE.StatusID IN (4)  
	  AND (@CustomerID=-1 OR OE.CustomerID=@CustomerID) 
	  AND (@ProductID=-1 OR OE.ProductID=@ProductID) 
	  AND (@StartDate IS NULL OR OE.OrderDate >= @StartDate) 
	  AND (@EndDate IS NULL OR OE.OrderDate <= @EndDate)
	  AND ((@BatchID IS NULL AND OE.BatchID IS NULL) OR OE.BatchID = @BatchID)
	ORDER BY OE.OriginDepartTimeUTC
	
END

GO

COMMIT
SET NOEXEC OFF