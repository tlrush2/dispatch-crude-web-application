-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.9.11.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.9.11.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0

SET NOEXEC OFF
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.9.11'
SELECT  @NewVersion = '3.9.12'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Load Creation: add new spCreateLoads.@DispatchNotes varchar(500) parameter'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/***********************************
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: create new Order (loads) for the specified criteria
-- Changes:
	3.9.12 - 2015/09/01 - KDA - adding in @DispatchNotes parameter (this must have been placed here previously by Joe?)
***********************************/
ALTER PROCEDURE spCreateLoads
(
  @OriginID int
, @DestinationID int
, @TicketTypeID int
, @DueDate datetime
, @CustomerID int
, @CarrierID int = NULL
, @DriverID int = NULL
, @Qty int
, @UserName varchar(100)
, @OriginTankID int = NULL
, @OriginTankNum varchar(20) = NULL
, @OriginBOLNum varchar(15) = NULL
, @PriorityID int = 3 -- LOW priority
, @StatusID smallint = -10 -- GENERATED
, @ProductID int = 1 -- basic Crude Oil
, @DispatchConfirmNum varchar(30) = NULL
, @DispatchNotes varchar(500) = NULL
, @IDs_CSV varchar(max) = NULL OUTPUT
, @count int = 0 OUTPUT
) AS
BEGIN

	DECLARE @PumperID int, @OperatorID int, @ProducerID int, @OriginUomID int, @DestUomID int
	SELECT @PumperID = PumperID, @OperatorID = OperatorID, @ProducerID = ProducerID, @OriginUomID = UomID, @count = 0
	FROM tblOrigin 
	WHERE ID = @OriginID
	SELECT @DestUomID = UomID FROM tblDestination WHERE ID = @DestinationID

	-- ensure that a driver is never assigned without a CarrierID
	IF (@DriverID IS NOT NULL AND @CarrierID IS NULL)
		SELECT @CarrierID = CarrierID FROM tblDriver WHERE ID = @DriverID
		
	DECLARE @incrementBOL bit
	SELECT @incrementBOL = CASE WHEN @OriginBOLNum LIKE '%+' THEN 1 ELSE 0 END
	IF (@incrementBOL = 1) SELECT @OriginBOLNum = left(@OriginBOLNum, len(@OriginBOLNum) - 1)
	
	DECLARE @i int, @id int
	SELECT @i = 0, @IDs_CSV = ''
	
	WHILE @i < @Qty BEGIN
		
		INSERT INTO dbo.tblOrder (OriginID, OriginUomID, DestinationID, DestUomID, TicketTypeID, DueDate, CustomerID
				, CarrierID, DriverID, StatusID, PriorityID, OriginTankID, OriginTankNum, OriginBOLNum
				, OrderNum, ProductID, PumperID, OperatorID, ProducerID, DispatchConfirmNum, DispatchNotes
				, CreateDateUTC, CreatedByUser)
			VALUES (@OriginID, @OriginUomID, @DestinationID, @DestUomID, @TicketTypeID, @DueDate, @CustomerID
				, @CarrierID, @DriverID, @StatusID, @PriorityID, @OriginTankID, @OriginTankNum, @OriginBOLNum
				, (SELECT isnull((SELECT max(OrderNum) FROM tblOrder), 100000) + 1), @ProductID
				, @PumperID, @OperatorID, @ProducerID, @DispatchConfirmNum, @DispatchNotes
				, GETUTCDATE(), @UserName)
		
		SELECT @IDs_CSV = @IDs_CSV + ',' + LTRIM(SCOPE_IDENTITY()), @count = @count + 1
		
		IF (@incrementBOL = 1)
		BEGIN
			IF (isnumeric(@OriginBOLNum) = 1) SELECT @OriginBOLNum = rtrim(cast(@OriginBOLNum as bigint) + 1)
		END

		SET @i = @i + 1
	END
	IF (@DriverID IS NOT NULL)
	BEGIN
		UPDATE tblOrder SET TruckID = D.TruckID, TrailerID = D.TrailerID, Trailer2ID = D.Trailer2ID
		FROM tblOrder O
		JOIN tblDriver D ON D.ID = O.DriverID
		WHERE O.ID IN (SELECT ID FROM dbo.fnSplitCSVIDs(@IDs_CSV))
	END
	SET @IDs_CSV = substring(@IDs_CSV, 2, 8000)
END
GO

COMMIT
SET NOEXEC OFF