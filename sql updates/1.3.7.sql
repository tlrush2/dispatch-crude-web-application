DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '1.3.6', @NewVersion = '1.3.7'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

ALTER TABLE [dbo].[tblOrderInvoiceCustomer] ADD [BillableWaitMinutes] [int] NULL
GO

ALTER TABLE [dbo].[tblOrderInvoiceCarrier] ADD [BillableWaitMinutes] [int] NULL
GO

/***********************************/
-- Date Created: 28 Feb 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
/***********************************/
ALTER VIEW [dbo].[viewOrderExportFull] AS
SELECT *
  , OriginMinutes + DestMinutes AS TotalMinutes
  , isnull(OriginWaitMinutes, 0) + isnull(DestWaitMinutes, 0) AS TotalWaitMinutes
FROM (
	SELECT O.*
	  , dbo.fnMaxInt(0, isnull(OriginMinutes, 0) - cast(S.Value as int)) AS OriginWaitMinutes
	  , dbo.fnMaxInt(0, isnull(DestMinutes, 0) - cast(S.Value as int)) AS DestWaitMinutes
	  , (SELECT count(*) FROM tblOrderTicket WHERE OrderID = O.ID) AS TicketCount
	  , (SELECT count(*) FROM tblOrderReroute WHERE OrderID = O.ID) AS RerouteCount
	FROM dbo.viewOrder O
	JOIN dbo.tblSetting S ON S.ID = 7 -- the Unbillable Wait Time threshold minutes 
WHERE O.StatusID NOT IN (-10) -- don't include "Generated" orders (only Assigned+)
  AND O.DeleteDate IS NULL
) v



GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Carrier "Settlement" $$ values to an Audited order
/***********************************/
ALTER PROCEDURE [dbo].[spProcessCarrierInvoice]
(
  @ID int
, @UserName varchar(100)
, @ChainupFee money = null
, @RerouteFee money = null
, @WaitFee money = null
, @RejectionFee money = null
, @LoadFee money = null
) AS BEGIN
	EXEC dbo.spSyncProducer @ID
	EXEC dbo.spSyncActualMiles @ID
	
	DELETE FROM tblOrderInvoiceCarrier WHERE OrderID = @ID
	INSERT INTO tblOrderInvoiceCarrier (OrderID, ChainupFee, RerouteFee, BillableWaitMinutes, WaitRate, WaitFee, RejectionFee, RouteRate, LoadFee, TotalFee, CreateDate, CreatedByUser)
	SELECT D.ID, ChainupFee, RerouteFee, WaitRate, BillableWaitMinutes, WaitFee, RejectionFee, RouteRate, LoadFee
		, ChainupFee + RerouteFee + WaitFee + LoadFee AS TotalFee
		, GETDATE(), @UserName
	FROM (
		SELECT S.ID
			, isnull(S.Chainup, 0) * coalesce(@ChainupFee, CR.ChainupFee, 0) AS ChainupFee
			, coalesce(@RerouteFee, CR.RerouteFee * S.RerouteCount, 0) AS RerouteFee
			, dbo.fnFullQtrHour(S.TotalWaitMinutes) AS BillableWaitMinutes
			, CR.WaitFee AS WaitRate
			, coalesce(@WaitFee, dbo.fnFullQtrHour(S.TotalWaitMinutes) * 1.0 / 60 * CR.WaitFee, 0) AS WaitFee
			, coalesce(@RejectionFee, S.Rejected * CR.RejectionFee, 0) AS RejectionFee
			, coalesce(@LoadFee, CRR.Rate) AS RouteRate
			, coalesce(@LoadFee, dbo.fnMaxDecimal(S.MinSettlementBarrels, S.ActualBarrels) * CRR.Rate, 0) AS LoadFee
		FROM (
			SELECT O.ID
				, O.CarrierID
				, O.RouteID
				, O.ChainUp
				, CASE WHEN C.SettlementFactorID = 1 THEN O.OriginGrossBarrels ELSE O.OriginNetBarrels END AS ActualBarrels
				, O.RerouteCount
				, O.TotalWaitMinutes
				, O.Rejected
				, coalesce(CRXX.ID, CRXA.ID, CRAX.ID, CRAA.ID) AS CRID
				, isnull(C.MinSettlementBarrels, 0) AS MinSettlementBarrels
			FROM dbo.viewOrderExportFull O
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			JOIN tblCarrier C ON C.ID = O.CarrierID
			LEFT JOIN tblCarrierRates CRXX ON CRXX.CarrierID = O.CarrierID AND CRXX.RegionID = OO.RegionID
			LEFT JOIN tblCarrierRates CRXA ON CRXA.CarrierID = O.CarrierID AND CRXA.RegionID = -1
			LEFT JOIN tblCarrierRates CRAX ON CRAX.CarrierID = -1 AND CRAX.RegionID = OO.RegionID
			LEFT JOIN tblCarrierRates CRAA ON CRAA.CarrierID = -1 AND CRAA.RegionID = -1
			WHERE O.ID = @ID
		) S
		LEFT JOIN tblCarrierRates CR ON CR.ID = S.CRID
		LEFT JOIN tblCarrierRouteRates CRR ON CRR.CarrierID = S.CarrierID AND CRR.RouteID = S.RouteID 
	) D
END




GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Customer "Settlement" $$ values to an Audited order
/***********************************/
ALTER PROCEDURE [dbo].[spProcessCustomerInvoice] 
(
  @ID int
, @UserName varchar(100)
, @ChainupFee money = null
, @RerouteFee money = null
, @WaitFee money = null
, @RejectionFee money = null
, @LoadFee money = null
) AS BEGIN
	DELETE FROM tblOrderInvoiceCustomer WHERE OrderID = @ID
	INSERT INTO tblOrderInvoiceCustomer (OrderID, ChainupFee, RerouteFee, BillableWaitMinutes, WaitRate, WaitFee, RejectionFee, RouteRate, LoadFee, TotalFee, CreateDate, CreatedByUser)
	SELECT D.ID, ChainupFee, RerouteFee, BillableWaitMinutes, WaitRate, WaitFee, RejectionFee, RouteRate, LoadFee
		, ChainupFee + RerouteFee + WaitFee + LoadFee AS TotalFee
		, GETDATE(), @UserName
	FROM (
		SELECT S.ID
			, isnull(S.Chainup, 0) * coalesce(@ChainupFee, CR.ChainupFee, 0) AS ChainupFee
			, coalesce(@RerouteFee, CR.RerouteFee * S.RerouteCount, 0) AS RerouteFee
			, dbo.fnFullQtrHour(S.TotalWaitMinutes) AS BillableWaitMinutes
			, CR.WaitFee AS WaitRate
			, coalesce(@WaitFee, dbo.fnFullQtrHour(S.TotalWaitMinutes) * 1.0 / 60 * CR.WaitFee, 0) AS WaitFee
			, coalesce(@RejectionFee, S.Rejected * CR.RejectionFee, 0) AS RejectionFee
			, coalesce(@LoadFee, CRR.Rate) AS RouteRate
			, coalesce(@LoadFee, dbo.fnMaxDecimal(S.MinSettlementBarrels, S.ActualBarrels) * CRR.Rate, 0) AS LoadFee
		FROM (
			SELECT O.ID
				, O.CustomerID
				, O.RouteID
				, O.ChainUp
				, CASE WHEN C.SettlementFactorID = 1 THEN O.OriginGrossBarrels ELSE O.OriginNetBarrels END AS ActualBarrels
				, O.RerouteCount
				, O.TotalWaitMinutes
				, O.Rejected
				, coalesce(CRXX.ID, CRXA.ID, CRAX.ID, CRAA.ID) AS CRID
				, isnull(C.MinSettlementBarrels, 0) AS MinSettlementBarrels
			FROM dbo.viewOrderExportFull O
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			JOIN tblCustomer C ON C.ID = O.CustomerID
			LEFT JOIN tblCustomerRates CRXX ON CRXX.CustomerID = O.CustomerID AND CRXX.RegionID = OO.RegionID
			LEFT JOIN tblCustomerRates CRXA ON CRXA.CustomerID = O.CustomerID AND CRXA.RegionID = -1
			LEFT JOIN tblCustomerRates CRAX ON CRAX.CustomerID = -1 AND CRAX.RegionID = OO.RegionID
			LEFT JOIN tblCustomerRates CRAA ON CRAA.CustomerID = -1 AND CRAA.RegionID = -1
			WHERE O.ID = @ID
		) S
		LEFT JOIN tblCustomerRates CR ON CR.ID = S.CRID
		LEFT JOIN tblCustomerRouteRates CRR ON CRR.CustomerID = S.CustomerID AND CRR.RouteID = S.RouteID
	) D
END




GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
/***********************************/
ALTER PROCEDURE [dbo].[spOrdersExportCarrier]
(
  @StartDate datetime
, @EndDate datetime
, @CarrierID int = -1 -- all carriers
, @IncludeInvoiced bit = 0  -- don't include already invoiced Orders
) AS BEGIN
	SELECT cast(CASE WHEN isnull(OIC.Invoiced, 0) = 1 THEN 0 ELSE 1 END as bit) as Selected
		, OE.* 
		, isnull(OIC.Invoiced, 0) AS Invoiced
		, OIC.ChainupFee
		, OIC.RerouteFee
		, OIC.BillableWaitMinutes
		, OIC.WaitRate
		, OIC.WaitFee
		, OIC.RejectionFee
		, OIC.RouteRate
		, OIC.LoadFee
		, OIC.TotalFee
		, cast(CASE WHEN OE.TicketCount = 0 THEN OE.TicketType ELSE NULL END as varchar(max)) AS TicketTypes
		, cast(CASE WHEN OE.TicketCount = 0 THEN OE.CarrierTicketNum ELSE NULL END as varchar(max)) AS TicketNums
		, cast(NULL as varchar(max)) AS ProductObsTemps
		, cast(NULL as varchar(max)) AS ProductObsGravities
		, cast(NULL as varchar(max)) AS ProductBSWs
		, cast(NULL as varchar(max)) AS TicketsRejected
		, cast(NULL as varchar(max)) AS TicketsRejectNotes
		, cast(NULL as varchar(max)) AS TicketsGrossBBLS
		, cast(NULL as varchar(max)) AS TicketsNetBBLS
		, cast(NULL as varchar(max)) AS PreviousDestinations
		, cast(NULL as varchar(max)) AS RerouteUsers
		, cast(NULL as varchar(max)) AS RerouteDates
	INTO #Orders
	FROM dbo.viewOrderExportFull OE
	LEFT JOIN dbo.tblOrderInvoiceCarrier OIC ON OIC.OrderID = OE.ID
	WHERE OE.StatusID IN (4)  
	  AND (@CarrierID=-1 OR OE.CarrierID=@CarrierID) 
	  AND OriginDepartTime >= @StartDate AND OE.OriginDepartTime < dateadd(day, 1, @EndDate) 
	  AND (@IncludeInvoiced = 1 OR OIC.Invoiced IS NULL OR OIC.Invoiced = 0) 
	  AND OE.DeleteDate IS NULL
	ORDER BY OE.OriginDepartTime
	
	DECLARE @OrderID int, @TicketID int, @RerouteID int
	SELECT @OrderID = min(ID) FROM #Orders WHERE ID > 0
	PRINT isnull(@OrderID, -1)
	
	WHILE @OrderID IS NOT NULL
	BEGIN
		-- add the Tickets information to the order (done this way since there could be multiple)
		SELECT @TicketID = min(ID) FROM dbo.tblOrderTicket WHERE OrderID = @OrderID AND DeleteDate IS NULL AND ID > 0
		WHILE @TicketID IS NOT NULL
		BEGIN
			UPDATE #Orders 
				SET TicketTypes = isnull(O.TicketTypes + '<br/>', '') + OT.TicketType
					, TicketNums = isnull(O.TicketNums + '<br/>', '') + OT.CarrierTicketNum
					, ProductObsTemps = isnull(O.ProductObsTemps + '<br/>', '') + isnull(ltrim(OT.ProductObsTemp), '')
					, ProductObsGravities = isnull(O.ProductObsGravities + '<br/>', '') + isnull(ltrim(OT.ProductObsGravity), '')
					, ProductBSWs = isnull(O.ProductBSWs + '<br/>', '') + isnull(ltrim(OT.ProductBSW), '')
					, TicketsRejected = isnull(O.TicketsRejected + '<br/>', '') + (CASE WHEN OT.Rejected = 0 THEN 'Valid' ELSE 'Rejected' END)
					, TicketsRejectNotes = isnull(O.TicketsRejectNotes + '<br/>', '') + isnull(OT.RejectNotes, '')
					, TicketsGrossBBLS = isnull(O.TicketsGrossBBLS + '<br/>', '') + isnull(ltrim(GrossBarrels), '')
					, TicketsNetBBLS = isnull(O.TicketsNetBBLS + '<br/>', '') + isnull(ltrim(NetBarrels), '')
			FROM #Orders O, viewOrderTicket OT
			WHERE O.ID = @OrderID AND OT.ID = @TicketID
			SELECT @TicketID = min(ID) FROM tblOrderTicket WHERE OrderID = @OrderID AND ID > @TicketID AND DeleteDate IS NULL
		END	
		-- add the Reroutes information to the order (done this way since there could be multiple)
		SELECT @RerouteID = min(ID) FROM dbo.tblOrderReroute WHERE OrderID = @OrderID AND ID > 0
		WHILE @RerouteID IS NOT NULL
		BEGIN
			UPDATE #Orders
				SET PreviousDestinations = O.PreviousDestinations + '<br/>' + O_R.PreviousDestinationFull
					, RerouteUsers = O.RerouteUsers + '<br/>' + O_R.UserName
					, RerouteDates = O.RerouteDates + '<br/>' + dbo.fnDateMdYY(O_R.RerouteDate)
			FROM #Orders O, viewOrderReroute O_R
			WHERE O.ID = @OrderID AND O_R.ID = @RerouteID
			SELECT @RerouteID = min(ID) FROM tblOrderReroute WHERE OrderID = @OrderID AND ID > @RerouteID
		END
		SELECT @OrderID = min(ID) FROM tblOrder WHERE ID > @OrderID
	END
	SELECT * FROM #Orders
END



GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
/***********************************/
ALTER PROCEDURE [dbo].[spOrdersExportCustomer]
(
  @StartDate datetime
, @EndDate datetime
, @CustomerID int = -1 -- all carriers
, @IncludeInvoiced bit = 0  -- don't include already invoiced Orders
) AS BEGIN
	SELECT cast(CASE WHEN isnull(OIC.Invoiced, 0) = 1 THEN 0 ELSE 1 END as bit) as Selected
		, OE.* 
		, isnull(OIC.Invoiced, 0) AS Invoiced
		, OIC.ChainupFee
		, OIC.RerouteFee
		, OIC.BillableWaitMinutes
		, OIC.WaitRate
		, OIC.WaitFee
		, OIC.RejectionFee
		, OIC.RouteRate
		, OIC.LoadFee
		, OIC.TotalFee
		, cast(CASE WHEN OE.TicketCount = 0 THEN OE.TicketType ELSE NULL END as varchar(max)) AS TicketTypes
		, cast(CASE WHEN OE.TicketCount = 0 THEN OE.CarrierTicketNum ELSE NULL END as varchar(max)) AS TicketNums
		, cast(NULL as varchar(max)) AS ProductObsTemps
		, cast(NULL as varchar(max)) AS ProductObsGravities
		, cast(NULL as varchar(max)) AS ProductBSWs
		, cast(NULL as varchar(max)) AS TicketsRejected
		, cast(NULL as varchar(max)) AS TicketsRejectNotes
		, cast(NULL as varchar(max)) AS TicketsGrossBBLS
		, cast(NULL as varchar(max)) AS TicketsNetBBLS
		, cast(NULL as varchar(max)) AS PreviousDestinations
		, cast(NULL as varchar(max)) AS RerouteUsers
		, cast(NULL as varchar(max)) AS RerouteDates
	INTO #Orders
	FROM dbo.viewOrderExportFull OE
	LEFT JOIN dbo.tblOrderInvoiceCustomer OIC ON OIC.OrderID = OE.ID
	WHERE OE.StatusID IN (4)  
	  AND (@CustomerID=-1 OR OE.CustomerID=@CustomerID) 
	  AND OriginDepartTime >= @StartDate AND OE.OriginDepartTime < dateadd(day, 1, @EndDate) 
	  AND (@IncludeInvoiced = 1 OR OIC.Invoiced IS NULL OR OIC.Invoiced = 0) 
	  AND OE.DeleteDate IS NULL
	ORDER BY OE.OriginDepartTime
	
	DECLARE @OrderID int, @TicketID int, @RerouteID int
	SELECT @OrderID = min(ID) FROM #Orders WHERE ID > 0
	PRINT isnull(@OrderID, -1)
	
	WHILE @OrderID IS NOT NULL
	BEGIN
		-- add the Tickets information to the order (done this way since there could be multiple)
		SELECT @TicketID = min(ID) FROM dbo.tblOrderTicket WHERE OrderID = @OrderID AND DeleteDate IS NULL AND ID > 0
		WHILE @TicketID IS NOT NULL
		BEGIN
			UPDATE #Orders 
				SET TicketTypes = isnull(O.TicketTypes + '<br/>', '') + OT.TicketType
					, TicketNums = isnull(O.TicketNums + '<br/>', '') + OT.CarrierTicketNum
					, ProductObsTemps = isnull(O.ProductObsTemps + '<br/>', '') + isnull(ltrim(OT.ProductObsTemp), '')
					, ProductObsGravities = isnull(O.ProductObsGravities + '<br/>', '') + isnull(ltrim(OT.ProductObsGravity), '')
					, ProductBSWs = isnull(O.ProductBSWs + '<br/>', '') + isnull(ltrim(OT.ProductBSW), '')
					, TicketsRejected = isnull(O.TicketsRejected + '<br/>', '') + (CASE WHEN OT.Rejected = 0 THEN 'Valid' ELSE 'Rejected' END)
					, TicketsRejectNotes = isnull(O.TicketsRejectNotes + '<br/>', '') + isnull(OT.RejectNotes, '')
					, TicketsGrossBBLS = isnull(O.TicketsGrossBBLS + '<br/>', '') + isnull(ltrim(GrossBarrels), '')
					, TicketsNetBBLS = isnull(O.TicketsNetBBLS + '<br/>', '') + isnull(ltrim(NetBarrels), '')
			FROM #Orders O, viewOrderTicket OT
			WHERE O.ID = @OrderID AND OT.ID = @TicketID
			SELECT @TicketID = min(ID) FROM tblOrderTicket WHERE OrderID = @OrderID AND ID > @TicketID AND DeleteDate IS NULL
		END	
		-- add the Reroutes information to the order (done this way since there could be multiple)
		SELECT @RerouteID = min(ID) FROM dbo.tblOrderReroute WHERE OrderID = @OrderID AND ID > 0
		WHILE @RerouteID IS NOT NULL
		BEGIN
			UPDATE #Orders
				SET PreviousDestinations = O.PreviousDestinations + '<br/>' + O_R.PreviousDestinationFull
					, RerouteUsers = O.RerouteUsers + '<br/>' + O_R.UserName
					, RerouteDates = O.RerouteDates + '<br/>' + dbo.fnDateMdYY(O_R.RerouteDate)
			FROM #Orders O, viewOrderReroute O_R
			WHERE O.ID = @OrderID AND O_R.ID = @RerouteID
			SELECT @RerouteID = min(ID) FROM tblOrderReroute WHERE OrderID = @OrderID AND ID > @RerouteID
		END
		SELECT @OrderID = min(ID) FROM tblOrder WHERE ID > @OrderID
	END
	SELECT * FROM #Orders
END



GO

EXEC _spRefreshAllViews
EXEC _spRecompileAllStoredProcedures
GO

COMMIT