-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.6.3'
SELECT  @NewVersion = '3.6.4'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'Add Truck | Trailer Meter attribute'
	UNION SELECT @NewVersion, 0, 'Driver Sync: support syncing of Truck | Trailer Meter attribute (Push Only)'
	UNION SELECT @NewVersion, 0, 'Settings: simplify multiple settings into one: "Arrive | Depart Entry tolerance (min)"'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

ALTER TABLE tblTruck ADD MeterType varchar(25) NULL
ALTER TABLE tblTrailer ADD MeterType varchar(25) NULL
GO

EXEC _spRebuildAllObjects
GO

DELETE FROM tblSetting WHERE ID IN (40, 41)
-- add the new single setting that determines when the Unloaded miles entered by the driver seem erroneous
INSERT INTO tblSetting (ID, Category, Name, SettingTypeID, Value, CreateDateUTC, CreatedByUser, ReadOnly)
	SELECT 40, 'Order Entry Rules', 'Unloaded Miles Warning Threshold', 3, 10, GETUTCDATE(), 'System', 0
-- add the new single setting that controls (in minutes) the entry tolerance (from NOW) in minutes
INSERT INTO tblSetting (ID, Category, Name, SettingTypeID, Value, CreateDateUTC, CreatedByUser, ReadOnly)
	SELECT 41, 'Order Entry Rules', 'Arrive | Depart Entry Range From NOW (+/- min)', 3, 10, GETUTCDATE(), 'System', 0
-- remove the now unused settings (were only used by the MVC pages)
DELETE FROM tblSetting WHERE ID IN (8,9)
GO


/***********************************/
-- Date Created: 12 Feb 2015
-- Author: Kevin Alons
-- Purpose: return last truck mileage for each truck (from order table)
/***********************************/
ALTER VIEW [dbo].[viewOrderLastTruckMileage] AS
	SELECT O.TruckID, Odometer = DestTruckMileage, OdometerDate = OrderDate, OdometerDateUTC = DeliverLastChangeDateUTC
	FROM viewOrderBase O
	JOIN ( 
		SELECT TruckID, ID = MAX(id) FROM tblOrder WHERE DestTruckMileage IS NOT NULL GROUP BY TruckID
	) T ON T.ID = O.ID
GO

/***********************************/
-- Date Created: 26 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Truck info with Last Odometer entry 
/***********************************/
ALTER VIEW [dbo].[viewTruckWithLastOdometer] AS
	SELECT T.ID
		, CarrierID
		, IDNumber
		, DOTNumber
		, VIN
		, Make
		, Model
		, Year
		, CreateDateUTC
		, CreatedByUser
		, LastChangeDateUTC = dbo.fnMaxDateTime(LastChangeDateUTC, OdometerDateUTC)
		, LastChangedByUser
		, LicenseNumber
		, AxleCount
		, MeterType
		, HasSleeper
		, PurchasePrice
		, GarageCity
		, GarageStateID
		, RegistrationDocument
		, RegistrationDocName
		, RegistrationExpiration
		, DeleteDateUTC
		, DeletedByUser
		, InsuranceIDCardDocument
		, InsuranceIDCardDocName
		, InsuranceIDCardExpiration
		, InsuranceIDCardIssue
		, CIDNumber
		, SIDNumber
		, GPSUnit
		, OwnerInfo
		, TireSize
		, Active
		, FullName
		, CarrierType
		, Carrier
		, LastOdometer = TM.Odometer, LastOdometerDate = TM.OdometerDate
	FROM dbo.viewTruck T
	LEFT JOIN viewOrderLastTruckMileage TM ON TM.TruckID = T.ID
GO

COMMIT
SET NOEXEC OFF