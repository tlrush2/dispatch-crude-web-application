-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.9.29.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.9.29.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0

SET NOEXEC OFF  -- since this is 
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.9.29'
SELECT  @NewVersion = '3.9.29.1'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'ReportCenter: Add "Ticket BS&W Decimal" & rename "Ticket BS&W" to "Ticket BS&W Percent"'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


-- Primary: Rename field to include the word "Percent" per Maverick's instructions
UPDATE tblReportColumnDefinition
SET Caption = 'TICKET | PRODSPECS | ORIGIN | Ticket BS&W Percent'
WHERE ID = 108
GO

-- Secondary:  Also renaming this field to fit with above convention
UPDATE tblReportColumnDefinition
SET Caption = 'TICKET | PRODSPECS | DESTINATION | Dest BS&W Percent'
WHERE ID = 43
GO

-- Secondary:  Also renaming this field to fit with above convention
UPDATE tblReportColumnDefinition
SET Caption = 'GAUGER | TICKETS | Gauger Ticket BS&W Percent'
WHERE ID = 254
GO

-- Insert the following new report center fields: "Ticket BS&W Decimal", "Ticket BS&W Decimal", and "Ticket BS&W Decimal"
SET IDENTITY_INSERT tblReportColumnDefinition ON
  INSERT tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport)
	SELECT 90035,1,'T_ProductBSW/100','TICKET | PRODSPECS | ORIGIN | Ticket BS&W Decimal',NULL,NULL,4,NULL,1,'*',0
	UNION
	SELECT 90036,1,'DestProductBSW/100','TICKET | PRODSPECS | DESTINATION | Dest BS&W Decimal',NULL,NULL,4,NULL,1,'*',0
	UNION
	SELECT 90037,1,'T_GaugerProductBSW/100','GAUGER | TICKETS | Gauger Ticket BS&W Decimal',NULL,NULL,4,NULL,1,'*',0
  EXCEPT SELECT ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport FROM tblReportColumnDefinition
SET IDENTITY_INSERT tblReportColumnDefinition OFF
GO


COMMIT
SET NOEXEC OFF