SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.5.19.1'
SELECT  @NewVersion = '4.5.19.2'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'JT-2832 - Changes to Sunoco Sundex export'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/*****************************************************************************************/
-- Author: Kevin Alons
-- Date Created: 27 Feb 2014
-- Purpose: return export final orders in the SunocoSundex
-- Changes:
	-- 3.7.21		- 06/03/2015	- BSB		- Ticket_Source_Code is now a dynamic system setting value
	-- 3.10.14		- 02/25/2016	- KDA & BSB	- Added custom data field functionality to the view.
	-- 3.11.9.1		- 04/04/2016	- BSB		- Change tank number to always use newly added "AltTankNum" field
	-- 4.4.6.2		- 12/02/2016	- BSB		- Add where clause to honor new system setting to include/exclude rejected tickets
	-- 4.5.9		- 02/17/2017	- BSB		- Add numeric output for meter fields per a new person at sunoco	
	-- 4.5.19.2		- 03/29/2017	- BSB		- Add origin level reject checks so rejects with no tickets will also output reject code and notes. Also made confirmation number '' if null
/*****************************************************************************************/
ALTER VIEW viewSunocoSundex AS
SELECT
	_ID = O.ID
	, _CustomerID = O.CustomerID
	, _OrderNum = O.OrderNum
	, Request_Code = CASE WHEN EP.IsNew = 1 THEN 'A' ELSE 'C' END
	, Company_Code = ISNULL((SELECT Value FROM tblObjectCustomData WHERE ObjectFieldID = 90003 AND RecordID = O.DestinationID), (SELECT Value FROM tblSetting WHERE ID = 59))
	, Ticket_Type = UPPER(LEFT(O.TicketType, 1))
	/* Changed ticket source code 3.10.14 */
	, Ticket_Source_Code = ISNULL((SELECT Value FROM tblObjectCustomData WHERE ObjectFieldID = 90002 AND RecordID = O.DestinationID), (SELECT Value FROM tblSetting WHERE ID = 53))
	, Ticket_Number = ISNULL(T.CarrierTicketNum, LTRIM(O.OrderNum) + 'X')
	, Ticket_Date = dbo.fnDateMMddYYYY(O.OrderDate)
	, SXL_Property_Code = ISNULL(O.LeaseNum, '')
	, TP_Property_Code = ''
	, Lease_Company_Name = O.Origin
	, Destination = ISNULL(OCD.Value, '')
	, Tank_Meter_Number = COALESCE((SELECT MIN(AltTankNum) FROM tblOriginTank WHERE DeleteDateUTC IS NULL AND ID = O.OriginTankID), T.OriginTankText, O.OriginTankNum)  -- 3.11.9.1
	, Open_Date = dbo.fnDateMMddYYYY(O.OriginArriveTime)
	, Open_Time = dbo.fnTimeOnly(O.OriginArriveTime)
	, Close_Date = dbo.fnDateMMddYYYY(O.OriginDepartTime)
	, Close_Time = dbo.fnTimeOnly(O.OriginDepartTime)
	, Estimated_Volume = CAST(ROUND(T.GrossUnits, 2) AS DECIMAL(18, 2))
	, Gross_Volume = CAST(ROUND(T.GrossUnits, 2) AS DECIMAL(18, 2))
	, Net_Volume = CAST(ROUND(T.NetUnits, 2) AS DECIMAL(18, 2))
	, Observed_Gravity = ISNULL(LTRIM(T.ProductObsGravity), '')
	, Observed_Temperature = ISNULL(LTRIM(T.ProductObsTemp), '')
	, Observed_BSW = ISNULL(LTRIM(T.ProductBSW), '')
	, Corrected_Gravity_API = 0
	, Purchaser = 'Sonoco Logistics'
	, First_Reading_Gauge_Ft = ISNULL(LTRIM(T.OpeningGaugeFeet), '')
	, First_Reading_Gauge_In = ISNULL(LTRIM(T.OpeningGaugeInch), '')
	, First_Reading_Gauge_Nu = ISNULL(LTRIM(T.OpeningGaugeQ), '')
	, First_Reading_Gauge_De = 4
	, First_Temperature = ISNULL(LTRIM(T.ProductHighTemp), '')
	, First_Bottom_Ft = ISNULL(LTRIM(T.BottomFeet), '')
	, First_Bottom_In = ISNULL(LTRIM(T.BottomInches), '')
	, First_Bottom_Nu = ISNULL(LTRIM(T.BottomQ), '')
	, First_Bottom_De = 4
	, Second_Reading_Gauge_Ft = ISNULL(LTRIM(T.ClosingGaugeFeet), '')
	, Second_Reading_Gauge_In = ISNULL(LTRIM(T.ClosingGaugeInch), '')
	, Second_Reading_Gauge_Nu = ISNULL(LTRIM(T.ClosingGaugeQ), '')
	, Second_Reading_Gauge_De = 4
	, Second_Temperature = ISNULL(LTRIM(T.ProductLowTemp), '')
	, Second_Bottom_Ft = 0
	, Second_Bottom_In = 0
	, Second_Bottom_Nu = 0
	, Second_Bottom_De = 4
	, Shrinkage_Incrustation_Factor = 1
	, First_Reading_Meter = T.OpenMeterUnits
	, Second_Reading_Meter = T.CloseMeterUnits
	, Meter_Factor = ISNULL(T.MeterFactor, 0) -- 2017-02-17 Antonio Abreu AABREU@sunocologistics.com said this needs to be a numeric value so it was change to send 0 if null
	, Temp_Comp_Meter = 0 -- 2017-02-17 Antonio Abreu AABREU@sunocologistics.com said this needs to be a numeric value so it was changed from '' to 0
	, Avg_Line_Temp = T.ProductObsTemp
	, Truck_ID = ''
	, Trailer_ID = ''
	, Driver_ID =''
	, Miles = ISNULL(O.ActualMiles, 0)
	, County = ''
	, State = ''
	, Invoice_Number = ''
	, Invoice_Date = ''
	, Remarks = CASE WHEN LEN(ISNULL(T.RejectNotes, O.RejectNotes)) > 0 
					 THEN ISNULL(T.RejectNumDesc, O.RejectNumDesc) + ' | ' + ISNULL(T.RejectNotes, O.RejectNotes) 
					 ELSE ISNULL(T.RejectNumDesc, O.RejectNumDesc) END
	, API_Compliant_Chapter = ''
	, Use_SXL_Calculation = 'Y'
	, Seal_On = dbo.fnTrimSealValue(T.SealOn, '0')
	, Seal_Off = dbo.fnTrimSealValue(T.SealOff, '0')
	, Ticket_Exclusion_Cd = CASE WHEN ISNULL(T.Rejected, O.Rejected) = 1 THEN 'RF' ELSE '' END
	, Confirmation_Number = ISNULL(ISNULL(T.DispatchConfirmNum, O.DispatchConfirmNum), '')
	, Split_Flag = CASE WHEN (SELECT COUNT(*) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND DeleteDateUTC IS NULL) > 1 THEN 'Y' ELSE 'N' END
	, Paired_Ticket_Number = ISNULL((SELECT MIN(CarrierTicketNum) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND DeleteDateUTC IS NULL AND OT.CarrierTicketNum <> T.CarrierTicketNum), '')
	, Bobtail_Flag = 'N'
	, Ticket_Extra_Info_Flag = CASE WHEN ISNULL(T.Rejected, O.Rejected) = 1 THEN ISNULL(T.RejectNum, O.RejectNum) ELSE '' END
FROM viewOrderLocalDates O
JOIN dbo.viewOrderCustomerFinalExportPending EP ON EP.ID = O.ID
LEFT JOIN viewOrderTicket T ON T.OrderID = O.ID AND T.DeleteDateUTC IS NULL
/* Changed join to get destination code from tblCustomerDestinationCode to tblObjectCustomData 3.10.14 */
LEFT JOIN dbo.tblObjectCustomData OCD ON OCD.ObjectFieldID = 90001 AND OCD.RecordID = O.DestinationID
/* Added setting to include/exclude reject tickets (for customer Regal) on 4.4.6.2 */
WHERE (T.Rejected = 0 AND O.Rejected = 0) -- Always include not rejected orders/tickets
	OR (UPPER((SELECT Value FROM tblSetting WHERE ID = 68)) = 'TRUE')  -- Include rejected orders/tickets when setting is on
GO


EXEC _spRefreshAllViews
GO


COMMIT
SET NOEXEC OFF