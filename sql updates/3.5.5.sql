-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.5.4'
SELECT  @NewVersion = '3.5.5'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Issue #624: add Shipper as new match criteria for Carrier Wait Fee Parameters'
	UNION SELECT @NewVersion, 0, 'Issue #604: add Shipper Region to Origins for filtering on Shipper Settlement'
	UNION SELECT @NewVersion, 0, 'Issue #636: add Route.WRMSVUnits, WRMSVUomID fields'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

ALTER TABLE tblCarrierWaitFeeParameter ADD ShipperID int NULL CONSTRAINT FK_CarrierWaitFeeParameter FOREIGN KEY REFERENCES tblCustomer(ID)
GO

/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all CarrierWaitFeeParameter records
******************************************************/
ALTER VIEW viewCarrierWaitFeeParameter AS
	SELECT X.*
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementCarrier WHERE WaitFeeParameterID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblCarrierWaitFeeParameter XN
			WHERE isnull(XN.ShipperID, 0) = isnull(X.ShipperID, 0) 
			  AND isnull(XN.CarrierID, 0) = isnull(X.CarrierID, 0) 
			  AND isnull(XN.ProductGroupID, 0) = isnull(X.ProductGroupID, 0) 
			  AND isnull(XN.OriginStateID, 0) = isnull(X.OriginStateID, 0) 
			  AND isnull(XN.DestStateID, 0) = isnull(X.DestStateID, 0)
			  AND isnull(XN.RegionID, 0) = isnull(X.RegionID, 0)
			  AND XN.EffectiveDate > X.EffectiveDate)
	FROM tblCarrierWaitFeeParameter X
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier WaitFeeParameter info for the specified order
/***********************************/
ALTER FUNCTION fnCarrierWaitFeeParameter(@StartDate date, @EndDate date, @ShipperID int, @CarrierID int, @ProductGroupID int, @OriginStateID int, @DestStateID int, @RegionID int, @bestMatchOnly bit = 0)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID
			, Ranking = dbo.fnRateRanking(@ShipperID, R.ShipperID, 32, 0)
				  + dbo.fnRateRanking(@CarrierID, R.CarrierID, 16, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 8, 0)
				  + dbo.fnRateRanking(@OriginStateID, R.OriginStateID, 4, 0)
				  + dbo.fnRateRanking(@DestStateID, R.DestStateID, 2, 0)
				  + dbo.fnRateRanking(@RegionID, R.RegionID, 1, 0)
		FROM  dbo.viewCarrierWaitFeeParameter R
		WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@OriginStateID, 0), R.OriginStateID, 0) = coalesce(OriginStateID, nullif(@OriginStateID, 0), 0)
		  AND coalesce(nullif(@DestStateID, 0), R.DestStateID, 0) = coalesce(DestStateID, nullif(@DestStateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	)
	
	SELECT R.ID, ShipperID, CarrierID, ProductGroupID, OriginStateID, DestStateID, RegionID, SubUnitID, RoundingTypeID, ThresholdMinutes, EffectiveDate, EndDate, NextEffectiveDate
	  , BestMatch, Ranking
	  , Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewCarrierWaitFeeParameter R
	JOIN (
		SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 6  -- ensure some type of match occurred on all 6 criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier WaitFeeParameter rows for the specified criteria
/***********************************/
ALTER FUNCTION [dbo].[fnCarrierWaitFeeParametersDisplay](@StartDate date, @EndDate date, @ShipperID int, @CarrierID int, @ProductGroupID int, @OriginStateID int, @DestStateID int, @RegionID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.OriginStateID, R.DestStateID, R.RegionID, R.SubUnitID, R.RoundingTypeID, R.ThresholdMinutes, R.EffectiveDate, R.EndDate, R.NextEffectiveDate
		, Shipper = S.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, OriginState = OS.FullName
		, OriginStateAbbrev = OS.Abbreviation
		, DestinationState = DS.FullName
		, DestinationStateAbbrev = DS.Abbreviation
		, Region = REG.Name
		, RoundingType = WFRT.Name
		, SubUnit = WFSU.Name
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch, Ranking
	FROM dbo.fnCarrierWaitFeeParameter(@StartDate, @EndDate, @ShipperID, @CarrierID, @ProductGroupID, @OriginStateID, @DestStateID, @RegionID, 0) R 
	JOIN tblWaitFeeRoundingType WFRT ON WFRT.ID = R.RoundingTypeID
	JOIN tblWaitFeeSubUnit WFSU ON WFSU.ID = R.SubUnitID
	LEFT JOIN tblCustomer S ON S.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblState OS ON OS.ID = R.OriginStateID
	LEFT JOIN tblState DS ON DS.ID = R.DestStateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	ORDER BY EffectiveDate
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier WaitFeeParameter info for the specified order
/***********************************/
ALTER FUNCTION [dbo].[fnOrderCarrierWaitFeeParameter](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, SubUnitID, RoundingTypeID, ThresholdMinutes
	FROM dbo.viewOrder O
	CROSS APPLY dbo.fnCarrierWaitFeeParameter(O.OrderDate, null, O.CustomerID, O.CarrierID, O.ProductGroupID, O.OriginStateID, O.DestStateID, O.OriginRegionID, 1) R
	WHERE O.ID = @ID 
GO

ALTER TABLE tblOrigin ADD ShipperRegion varchar(50) NULL
GO

/*****************************************
-- Date Created: 19 Jan 2015
-- Author: Kevin Alons
-- Purpose: return the tblOrder table witha Local OrderDate field added
*****************************************/
ALTER VIEW [dbo].[viewOrderBase] AS
	SELECT O.*
		, OriginShipperRegion = OO.ShipperRegion
		, OrderDate = cast(dbo.fnUTC_to_Local(O.OriginDepartTimeUTC, OO.TimeZoneID, OO.UseDST) as date) 
	FROM tblOrder O
	JOIN tblOrigin OO ON OO.ID = O.OriginID

GO

EXEC _spRebuildAllObjects
GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
/***********************************/
ALTER PROCEDURE [dbo].[spRetrieveOrdersFinancialShipper]
(
  @StartDate datetime = NULL
, @EndDate datetime = NULL
, @ShipperID int = -1 -- all customers
, @OriginShipperRegion varchar(50) = NULL -- all origin.ShipperRegions
, @ProductGroupID int = -1 -- all product groups
, @ProductID int = -1 -- all products
, @BatchID int = NULL  -- either show unbatched (NULL), or the specified batch orders
) AS BEGIN

	SELECT @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	
	SELECT DISTINCT OE.* 
	FROM viewOrder_Financial_Shipper OE
	WHERE ID IN (
		SELECT O.ID
		FROM viewOrderBase O
		JOIN tblProduct P ON P.ID = O.ProductID
		LEFT JOIN tblOrderSettlementShipper ISC ON ISC.OrderID = O.ID
		WHERE O.StatusID IN (4)  
		  AND (@ShipperID=-1 OR O.CustomerID=@ShipperID) 
		  AND (@OriginShipperRegion IS NULL OR OriginShipperRegion = @OriginShipperRegion)
		  AND (@ProductGroupID=-1 OR P.ProductGroupID=@ProductGroupID) 
		  AND (@ProductID=-1 OR O.ProductID=@ProductID) 
		  AND (@StartDate IS NULL OR O.OrderDate >= @StartDate) 
		  AND (@EndDate IS NULL OR O.OrderDate <= @EndDate)
		  AND ((@BatchID IS NULL AND ISC.BatchID IS NULL) OR ISC.BatchID = @BatchID)
	)
	ORDER BY OE.OriginDepartTimeUTC
END 
GO

ALTER TABLE tblRoute ADD WRMSVUnits int NULL
ALTER TABLE tblRoute ADD WRMSVUomID int CONSTRAINT FK_Route_Uom FOREIGN KEY REFERENCES tblUom(ID)
GO

/**********************************/
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Route records with translated Origin/Destination values
/***********************************/
ALTER VIEW [dbo].[viewRoute] AS
SELECT R.*
, Origin = O.Name, OriginFull = O.FullName 
, Destination = D.Name, DestinationFull = D.FullName 
, Active = cast(CASE WHEN O.Active = 1 AND D.Active = 1 THEN 1 ELSE 0 END as bit) 
FROM dbo.tblRoute R
JOIN dbo.viewOrigin O ON O.ID = R.OriginID
JOIN dbo.viewDestination D ON D.ID = R.DestinationID
GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Carrier "Settlement" $$ values to an Delivered/Audited order
/***********************************/
ALTER VIEW [dbo].[viewOrderSettlementUnitsCarrier] AS
	SELECT X.*
		, SettlementUnits = dbo.fnMaxDecimal(X.ActualUnits, dbo.fnMinDecimal(isnull(X.WRMSVUnits, X.MinSettlementUnits), X.MinSettlementUnits))
	FROM (
		SELECT OrderID = O.ID
			, O.CarrierID
			, SettlementUomID = O.OriginUomID
			, C.SettlementFactorID
			, WRMSVUnits = dbo.fnConvertUOM(R.WRMSVUnits, R.WRMSVUomID, O.OriginUomID)
			, MinSettlementUnits = isnull(dbo.fnConvertUOM(C.MinSettlementUnits, C.MinSettlementUomID, O.OriginUomID), 0) 
			, ActualUnits = CASE C.SettlementFactorID	WHEN 1 THEN O.OriginGrossUnits 
														WHEN 3 THEN isnull(O.OriginGrossStdUnits, O.OriginGrossUnits) 
														ELSE coalesce(O.OriginNetUnits, O.OriginGrossStdUnits, O.OriginGrossUnits, 0) END 
		FROM dbo.tblOrder O
		JOIN dbo.tblRoute R ON R.ID = O.RouteID
		JOIN tblCarrier C ON C.ID = O.CarrierID
	) X
GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Shipper "Settlement" $$ values to an Delivered/Audited order
/***********************************/
ALTER VIEW [dbo].[viewOrderSettlementUnitsShipper] AS
	SELECT X.*
		, SettlementUnits = dbo.fnMaxDecimal(X.ActualUnits, dbo.fnMinDecimal(isnull(X.WRMSVUnits, X.MinSettlementUnits), X.MinSettlementUnits))
	FROM (
		SELECT OrderID = O.ID
			, O.CustomerID
			, SettlementUomID = O.OriginUomID
			, C.SettlementFactorID
			, WRMSVUnits = dbo.fnConvertUOM(R.WRMSVUnits, R.WRMSVUomID, O.OriginUomID)
			, MinSettlementUnits = isnull(dbo.fnConvertUOM(C.MinSettlementUnits, C.MinSettlementUomID, O.OriginUomID), 0) 
			, ActualUnits = CASE C.SettlementFactorID	WHEN 1 THEN O.OriginGrossUnits 
														WHEN 3 THEN isnull(O.OriginGrossStdUnits, O.OriginGrossUnits) 
														ELSE coalesce(O.OriginNetUnits, O.OriginGrossStdUnits, O.OriginGrossUnits, 0) END 
		FROM dbo.tblOrder O
		JOIN dbo.tblRoute R ON R.ID = O.RouteID
		JOIN tblCustomer C ON C.ID = O.CustomerID
	) X
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 11 Sep 2013
-- Description:	trigger to ensure changes to Routes also update related (relevant) orders
-- =============================================
ALTER TRIGGER [dbo].[trigRoute_U] ON [dbo].[tblRoute] AFTER UPDATE AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE tblOrder
	  SET ActualMiles = R.ActualMiles
	FROM tblOrder O
	JOIN inserted R ON R.ID = O.RouteID
	LEFT JOIN tblOrderSettlementCarrier OSC ON OSC.OrderID = O.ID
	LEFT JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = O.ID
	WHERE OSC.BatchID IS NULL AND OSS.BatchID IS NULL
END
GO

EXEC _spRebuildAllObjects
GO

COMMIT
SET NOEXEC OFF
