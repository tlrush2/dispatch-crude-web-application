SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.1.25'
SELECT  @NewVersion = '4.1.25.1'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1874 - Add sort number to custom eBOL fields table so the sort order can be user defined.'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO



/* Add new sort number column for eBOL custom fields */
ALTER TABLE tblCustomBOLFields
	ADD SortNum INT NULL
GO


/* Give each existing custom eBOL field a sort number to begin with */
UPDATE tblCustomBOLFields SET SortNum=7 WHERE InternalLabel='Destination Rack/Bay'
GO
UPDATE tblCustomBOLFields SET SortNum=4 WHERE InternalLabel='Dest Truck Mileage'
GO
UPDATE tblCustomBOLFields SET SortNum=3 WHERE InternalLabel='Origin Truck Mileage'
GO
UPDATE tblCustomBOLFields SET SortNum=6 WHERE InternalLabel='Actual Miles'
GO
UPDATE tblCustomBOLFields SET SortNum=5 WHERE InternalLabel='Trip Miles'
GO
UPDATE tblCustomBOLFields SET SortNum=1 WHERE InternalLabel='GOV Variance'
GO
UPDATE tblCustomBOLFields SET SortNum=2 WHERE InternalLabel='NSV Variance'
GO



-- =============================================
-- Author:		Kevin Alons
-- Create date: 19 Oct 2016
-- Description:	trigger to advance any duplicated SortNums in custom bol fields to make reordering easier 
--				(BB 10/20/16) - This was taken/edited from trigUserReportColumnDefinition_IU_SortNum
-- =============================================
CREATE TRIGGER trigCustomBOLFields_IU_SortNum ON tblCustomBOLFields FOR INSERT, UPDATE AS
BEGIN
	IF (trigger_nestlevel() < 2)  -- logic below will recurse unless this is used to prevent it
	BEGIN
		IF (UPDATE(SortNum))
		BEGIN
			SELECT i.SortNum, ID = MAX(i2.ID)
			INTO #new
			FROM (
				SELECT SortNum = min(SortNum)
				FROM inserted
			) i
			JOIN inserted i2 ON i2.SortNum = i.SortNum
			GROUP BY i.SortNum
			
			DECLARE @SortNum int, @ID int
			SELECT TOP 1 @SortNum = SortNum, @ID = ID FROM #new 

			WHILE (@SortNum IS NOT NULL)
			BEGIN
				UPDATE tblCustomBOLFields 
					SET SortNum = N.NewSortNum
				FROM tblCustomBOLFields CBF
				JOIN (
					SELECT ID, NewSortNum = @SortNum + ROW_NUMBER() OVER (ORDER BY SortNum) 
					FROM tblCustomBOLFields
					WHERE SortNum >= @SortNum AND ID <> @ID
				) N ON N.ID = CBF.ID
				
				DELETE FROM #new WHERE ID = @ID			
				SET @SortNum = NULL	
				SELECT TOP 1 @SortNum = SortNum, @ID = ID FROM #new
			END	
		END
	END
END
GO



COMMIT
SET NOEXEC OFF