/* revise the DriverApp table-functions to split order into OrderReadOnly & OrderEdit
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.0.2', @NewVersion = '2.0.3'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

CREATE FUNCTION [dbo].[fnMaxDateTime](@first datetime, @second datetime) RETURNS datetime AS BEGIN
	SELECT @first = isnull(@first, 0), @second = isnull(@second, 0)
	RETURN (CASE WHEN @first > @second THEN @first ELSE @second END)
END

GO
GRANT EXECUTE ON dbo.fnMaxDateTime TO dispatchcrude_iis_acct
GO

/***********************************************************************/
-- Date Created: 18 Apr 2013
-- Author: Kevin Alons
-- Purpose: return the DriverApp.MasterData for a single driver/login
/***********************************************************************/
ALTER FUNCTION [dbo].[fnDriverMasterData](@Valid bit, @DriverID int, @UserName varchar(100)) RETURNS TABLE AS 
RETURN
	SELECT CASE WHEN DS.DriverID IS NULL THEN 0 ELSE @Valid END AS Valid
		, @UserName AS UserName
		, @DriverID AS DriverID
		, D.FullName AS DriverName
		, D.MobilePrint
		, DS.LastSyncUTC
		, cast(SSF.Value as int) AS SyncMinutes
		, SSV.Value AS SchemaVersion
		, LAV.Value AS LatestAppVersion
		, DS.PasswordHash
	FROM viewDriver D
	LEFT JOIN tblDriver_Sync DS ON DS.DriverID = D.ID
	JOIN tblSetting SSV ON SSV.ID = 0  -- Schema Version
	JOIN tblSetting SSF ON SSF.ID = 11 -- sync frequency (in minutes)
	JOIN tblSetting LAV ON LAV.ID = 12 -- LatestAppVersion
	WHERE D.ID = @DriverID

GO

/*******************************************/
-- Date Created: 31 Aug 2013
-- Author: Kevin Alons
-- Purpose: return driver editable Order data for Driver App sync
/*******************************************/
CREATE FUNCTION [dbo].[fnOrderEdit_DriverApp]( @DriverID int, @LastChangeDateUTC datetime ) RETURNS  TABLE AS
RETURN 
	SELECT O.ID
		, O.TruckID
		, O.TrailerID
		, O.Trailer2ID
		, O.OriginBOLNum
		, O.OriginArriveTimeUTC
		, O.OriginDepartTimeUTC
		, O.OriginMinutes
		, O.OriginWaitNotes
		, O.OriginTruckMileage
		, O.OriginGrossBarrels
		, O.OriginNetBarrels
		, O.ChainUp
		, O.Rejected
		, O.RejectNotes
		, O.OriginTankNum
		, O.DestArriveTimeUTC
		, O.DestDepartTimeUTC
		, O.DestMinutes
		, O.DestWaitNotes
		, O.DestTruckMileage
		, O.DestGrossBarrels
		, O.DestNetBarrels
		, O.DestProductTemp
		, O.DestProductBSW
		, O.DestProductGravity
		, O.DestOpenMeterBarrels
		, O.DestCloseMeterBarrels
		, O.CarrierTicketNum
		, O.AcceptLastChangeDateUTC
		, O.PickupLastChangeDateUTC
		, O.DeliverLastChangeDateUTC
	FROM dbo.tblOrder O
	WHERE (O.StatusID IN (2, 7, 8) -- Dispatched, Accepted, Picked Up
		-- or status = Delivered and the driver still has access to the record
		OR (StatusID = 3 AND O.DestDepartTimeUTC < DATEADD(hour, (SELECT cast(value as int) FROM tblSetting WHERE ID = 10), GETUTCDATE())))
	  AND O.DriverID = @DriverID
	  AND (@LastChangeDateUTC IS NULL OR O.LastChangeDateUTC >= @LastChangeDateUTC)

GO
GRANT SELECT ON [dbo].[fnOrderEdit_DriverApp] TO dispatchcrude_iis_acct
GO

/*******************************************/
-- Date Created: 25 Apr 2013
-- Author: Kevin Alons
-- Purpose: return readonly Order data for Driver App sync
/*******************************************/
CREATE FUNCTION [dbo].[fnOrderReadOnly_DriverApp]( @DriverID int, @LastChangeDateUTC datetime ) RETURNS  TABLE AS
RETURN 
	SELECT O.ID
		, O.OrderNum
		, O.StatusID
		, O.TicketTypeID
		, OO.TankTypeID
		, cast(P.PriorityNum as int) AS PriorityNum
		, O.Product
		, O.DueDate
		, O.Origin
		, O.OriginFull
		, OO.Station AS OriginStation
		, OO.County AS OrigionCounty
		, O.OriginState
		, OO.WellAPI AS OriginAPI
		, O.Destination
		, O.DestinationFull
		, D.Station AS DestinationStation
		, cast(CASE WHEN D.TicketTypeID = 2 THEN 1 ELSE 0 END as bit) AS DestBOLAvailable
		, OO.BarrelsPerInch AS OriginBarrelsPerInch
		, O.CreateDateUTC
		, O.CreatedByUser
		, O.LastChangeDateUTC
		, O.LastChangedByUser
		, O.DeleteDateUTC
		, O.DeletedByUser
		, O.OriginID
		, O.DestinationID
		, cast(O.PriorityID AS int) AS PriorityID
		, O.Operator
		, O.OperatorID
		, O.Pumper
		, O.PumperID
		, O.Producer
		, O.ProducerID
		, O.Customer
		, O.CustomerID
		, O.Carrier
		, O.CarrierID
		, O.ProductID
		, O.TicketType
		, C.PrintHeaderBlob
	FROM dbo.viewOrder O
	JOIN dbo.tblPriority P ON P.ID = O.PriorityID
	JOIN dbo.viewOrigin OO ON OO.ID = O.OriginID
	JOIN dbo.tblDestination D ON D.ID = O.DestinationID
	JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
	WHERE (StatusID IN (2, 7, 8) -- Dispatched, Accepted, Picked Up
		-- or status = Delivered and the driver still has access to the record
		OR (StatusID = 3 AND O.DestDepartTimeUTC < DATEADD(hour, (SELECT cast(value as int) FROM tblSetting WHERE ID = 10), GETUTCDATE())))
	  AND O.DriverID = @DriverID
	  AND (@LastChangeDateUTC IS NULL OR O.LastChangeDateUTC >= @LastChangeDateUTC)
GO
GRANT SELECT ON [dbo].[fnOrderReadOnly_DriverApp] TO dispatchcrude_iis_acct
GO

DROP FUNCTION [dbo].[fnOrder_DriverApp]
GO

COMMIT
SET NOEXEC OFF