-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.5.10'
SELECT  @NewVersion = '3.5.11'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'Deleted rates now downgrade to a MISSING order rate (vs a MANUAL rate)'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

ALTER TABLE dbo.tblOrderSettlementCarrier
	DROP CONSTRAINT FK_OrderSettlementCarrier_OrderRejectRate
GO
ALTER TABLE dbo.tblCarrierOrderRejectRate SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderSettlementCarrier
	DROP CONSTRAINT FK_OrderSettlementCarrier_DestinationWaitRate
GO
ALTER TABLE dbo.tblCarrierDestinationWaitRate SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderSettlementCarrier
	DROP CONSTRAINT FK_OrderSettlementCarrier_OriginWaitRate
GO
ALTER TABLE dbo.tblCarrierOriginWaitRate SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderSettlementCarrier
	DROP CONSTRAINT FK_OrderSettlementCarrier_RangeRate
GO
ALTER TABLE dbo.tblCarrierRangeRate SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderSettlementCarrier
	DROP CONSTRAINT FK_OrderSettlmentCarrier_RouteRate
GO
ALTER TABLE dbo.tblCarrierRouteRate SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderSettlementCarrierAssessorialCharge
	DROP CONSTRAINT FK_OrderSettlementCarrierOtherAssessorialCharge_Settlement
GO
ALTER TABLE dbo.tblOrderSettlementCarrier ADD CONSTRAINT
	FK_OrderSettlmentCarrier_RouteRate FOREIGN KEY
	(
	RouteRateID
	) REFERENCES dbo.tblCarrierRouteRate
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderSettlementCarrier ADD CONSTRAINT
	FK_OrderSettlementCarrier_RangeRate FOREIGN KEY
	(
	RangeRateID
	) REFERENCES dbo.tblCarrierRangeRate
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderSettlementCarrier ADD CONSTRAINT
	FK_OrderSettlementCarrier_OriginWaitRate FOREIGN KEY
	(
	OriginWaitRateID
	) REFERENCES dbo.tblCarrierOriginWaitRate
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderSettlementCarrier ADD CONSTRAINT
	FK_OrderSettlementCarrier_DestinationWaitRate FOREIGN KEY
	(
	DestinationWaitRateID
	) REFERENCES dbo.tblCarrierDestinationWaitRate
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderSettlementCarrier ADD CONSTRAINT
	FK_OrderSettlementCarrier_OrderRejectRate FOREIGN KEY
	(
	OrderRejectRateID
	) REFERENCES dbo.tblCarrierOrderRejectRate
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderSettlementCarrier SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderSettlementCarrierAssessorialCharge ADD CONSTRAINT
	FK_OrderSettlementCarrierOtherAssessorialCharge_Settlement FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.tblOrderSettlementCarrier
	(
	OrderID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
GO
ALTER TABLE dbo.tblOrderSettlementCarrierAssessorialCharge SET (LOCK_ESCALATION = TABLE)
GO

ALTER TABLE dbo.tblOrderSettlementShipper
	DROP CONSTRAINT FK_OrderSettlementShipper_OrderRejectRate
GO
ALTER TABLE dbo.tblShipperOrderRejectRate SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderSettlementShipper
	DROP CONSTRAINT FK_OrderSettlementShipper_DestinationWaitRate
GO
ALTER TABLE dbo.tblShipperDestinationWaitRate SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderSettlementShipper
	DROP CONSTRAINT FK_OrderSettlementShipper_OriginWaitRate
GO
ALTER TABLE dbo.tblShipperOriginWaitRate SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderSettlementShipper
	DROP CONSTRAINT FK_OrderSettlementShipper_RangeRate
GO
ALTER TABLE dbo.tblShipperRangeRate SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderSettlementShipper
	DROP CONSTRAINT FK_OrderSettlmentShipper_RouteRate
GO
ALTER TABLE dbo.tblShipperRouteRate SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderSettlementShipperAssessorialCharge
	DROP CONSTRAINT FK_OrderSettlementShipperOtherAssessorialCharge_Settlement
GO
ALTER TABLE dbo.tblOrderSettlementShipper ADD CONSTRAINT
	FK_OrderSettlmentShipper_RouteRate FOREIGN KEY
	(
	RouteRateID
	) REFERENCES dbo.tblShipperRouteRate
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderSettlementShipper ADD CONSTRAINT
	FK_OrderSettlementShipper_RangeRate FOREIGN KEY
	(
	RangeRateID
	) REFERENCES dbo.tblShipperRangeRate
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderSettlementShipper ADD CONSTRAINT
	FK_OrderSettlementShipper_OriginWaitRate FOREIGN KEY
	(
	OriginWaitRateID
	) REFERENCES dbo.tblShipperOriginWaitRate
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderSettlementShipper ADD CONSTRAINT
	FK_OrderSettlementShipper_DestinationWaitRate FOREIGN KEY
	(
	DestinationWaitRateID
	) REFERENCES dbo.tblShipperDestinationWaitRate
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderSettlementShipper ADD CONSTRAINT
	FK_OrderSettlementShipper_OrderRejectRate FOREIGN KEY
	(
	OrderRejectRateID
	) REFERENCES dbo.tblShipperOrderRejectRate
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderSettlementShipper SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderSettlementShipperAssessorialCharge ADD CONSTRAINT
	FK_OrderSettlementShipperOtherAssessorialCharge_Settlement FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.tblOrderSettlementShipper
	(
	OrderID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
GO
ALTER TABLE dbo.tblOrderSettlementShipperAssessorialCharge SET (LOCK_ESCALATION = TABLE)
GO

ALTER TABLE dbo.tblOrderSettlementCarrier
	DROP CONSTRAINT FK_OrderSettlementCarrier_OrderRejectRate
GO
ALTER TABLE dbo.tblCarrierOrderRejectRate SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderSettlementCarrier
	DROP CONSTRAINT FK_OrderSettlementCarrier_DestinationWaitRate
GO
ALTER TABLE dbo.tblCarrierDestinationWaitRate SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderSettlementCarrier
	DROP CONSTRAINT FK_OrderSettlementCarrier_OriginWaitRate
GO
ALTER TABLE dbo.tblCarrierOriginWaitRate SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderSettlementCarrier
	DROP CONSTRAINT FK_OrderSettlementCarrier_RangeRate
GO
ALTER TABLE dbo.tblCarrierRangeRate SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderSettlementCarrier
	DROP CONSTRAINT FK_OrderSettlmentCarrier_RouteRate
GO
ALTER TABLE dbo.tblCarrierRouteRate SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderSettlementCarrierAssessorialCharge
	DROP CONSTRAINT FK_OrderSettlementCarrierOtherAssessorialCharge_Settlement
GO
ALTER TABLE dbo.tblOrderSettlementCarrier ADD CONSTRAINT
	FK_OrderSettlmentCarrier_RouteRate FOREIGN KEY
	(
	RouteRateID
	) REFERENCES dbo.tblCarrierRouteRate
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderSettlementCarrier ADD CONSTRAINT
	FK_OrderSettlementCarrier_RangeRate FOREIGN KEY
	(
	RangeRateID
	) REFERENCES dbo.tblCarrierRangeRate
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderSettlementCarrier ADD CONSTRAINT
	FK_OrderSettlementCarrier_OriginWaitRate FOREIGN KEY
	(
	OriginWaitRateID
	) REFERENCES dbo.tblCarrierOriginWaitRate
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderSettlementCarrier ADD CONSTRAINT
	FK_OrderSettlementCarrier_DestinationWaitRate FOREIGN KEY
	(
	DestinationWaitRateID
	) REFERENCES dbo.tblCarrierDestinationWaitRate
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderSettlementCarrier ADD CONSTRAINT
	FK_OrderSettlementCarrier_OrderRejectRate FOREIGN KEY
	(
	OrderRejectRateID
	) REFERENCES dbo.tblCarrierOrderRejectRate
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderSettlementCarrier SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderSettlementCarrierAssessorialCharge ADD CONSTRAINT
	FK_OrderSettlementCarrierOtherAssessorialCharge_Settlement FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.tblOrderSettlementCarrier
	(
	OrderID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
GO
ALTER TABLE dbo.tblOrderSettlementCarrierAssessorialCharge SET (LOCK_ESCALATION = TABLE)
GO

ALTER TABLE dbo.tblOrderSettlementShipper
	DROP CONSTRAINT FK_OrderSettlementShipper_OrderRejectRate
GO
ALTER TABLE dbo.tblShipperOrderRejectRate SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderSettlementShipper
	DROP CONSTRAINT FK_OrderSettlementShipper_DestinationWaitRate
GO
ALTER TABLE dbo.tblShipperDestinationWaitRate SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderSettlementShipper
	DROP CONSTRAINT FK_OrderSettlementShipper_OriginWaitRate
GO
ALTER TABLE dbo.tblShipperOriginWaitRate SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderSettlementShipper
	DROP CONSTRAINT FK_OrderSettlementShipper_RangeRate
GO
ALTER TABLE dbo.tblShipperRangeRate SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderSettlementShipper
	DROP CONSTRAINT FK_OrderSettlmentShipper_RouteRate
GO
ALTER TABLE dbo.tblShipperRouteRate SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderSettlementShipperAssessorialCharge
	DROP CONSTRAINT FK_OrderSettlementShipperOtherAssessorialCharge_Settlement
GO
ALTER TABLE dbo.tblOrderSettlementShipper ADD CONSTRAINT
	FK_OrderSettlmentShipper_RouteRate FOREIGN KEY
	(
	RouteRateID
	) REFERENCES dbo.tblShipperRouteRate
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderSettlementShipper ADD CONSTRAINT
	FK_OrderSettlementShipper_RangeRate FOREIGN KEY
	(
	RangeRateID
	) REFERENCES dbo.tblShipperRangeRate
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderSettlementShipper ADD CONSTRAINT
	FK_OrderSettlementShipper_OriginWaitRate FOREIGN KEY
	(
	OriginWaitRateID
	) REFERENCES dbo.tblShipperOriginWaitRate
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderSettlementShipper ADD CONSTRAINT
	FK_OrderSettlementShipper_DestinationWaitRate FOREIGN KEY
	(
	DestinationWaitRateID
	) REFERENCES dbo.tblShipperDestinationWaitRate
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderSettlementShipper ADD CONSTRAINT
	FK_OrderSettlementShipper_OrderRejectRate FOREIGN KEY
	(
	OrderRejectRateID
	) REFERENCES dbo.tblShipperOrderRejectRate
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderSettlementShipper SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderSettlementShipperAssessorialCharge ADD CONSTRAINT
	FK_OrderSettlementShipperOtherAssessorialCharge_Settlement FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.tblOrderSettlementShipper
	(
	OrderID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
GO
ALTER TABLE dbo.tblOrderSettlementShipperAssessorialCharge SET (LOCK_ESCALATION = TABLE)
GO

DROP TRIGGER trigCarrierAssessorialRate_D
GO
/*************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: handle specialized logic related to Effective/End dates (all handled through the associated view)
/*************************************/
CREATE TRIGGER trigCarrierAssessorialRate_IOD ON tblCarrierAssessorialRate INSTEAD OF DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewCarrierAssessorialRate X ON X.ID = d.ID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('Locked (in use) records cannot be deleted', 16, 1)
		ROLLBACK
	END
	ELSE
	BEGIN
		-- remove all Un-Settled referencss to this deleted rate (both Rate + Amount)
		UPDATE tblOrderSettlementCarrierAssessorialCharge 
		  SET AssessorialRateID = NULL, Amount = NULL
		FROM tblOrderSettlementCarrierAssessorialCharge SCAC
		JOIN tblOrderSettlementCarrier SC ON SC.OrderID = SCAC.OrderID
		JOIN deleted d ON d.ID = SCAC.AssessorialRateID
		WHERE SC.BatchID IS NULL
		-- do the actual delete action
		DELETE FROM tblCarrierAssessorialRate WHERE ID IN (SELECT ID FROM deleted)
	END
END
GO

DROP TRIGGER [trigShipperAssessorialRate_D]
GO
/*************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: handle specialized logic related to Effective/End dates (all handled through the associated view)
/*************************************/
CREATE TRIGGER [dbo].[trigShipperAssessorialRate_IOD] ON [dbo].[tblShipperAssessorialRate] INSTEAD OF DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewShipperAssessorialRate X ON X.ID = d.ID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('Locked (in use) records cannot be deleted', 16, 1)
		ROLLBACK
	END
	ELSE
	BEGIN
		-- remove all Un-Settled referencss to this deleted rate (both Rate + Amount)
		UPDATE tblOrderSettlementShipperAssessorialCharge 
		  SET AssessorialRateID = NULL, Amount = NULL
		FROM tblOrderSettlementShipperAssessorialCharge SCAC
		JOIN tblOrderSettlementShipper SC ON SC.OrderID = SCAC.OrderID
		JOIN deleted d ON d.ID = SCAC.AssessorialRateID
		WHERE SC.BatchID IS NULL
		-- do the actual delete action now
		DELETE FROM tblShipperAssessorialRate WHERE ID IN (SELECT ID FROM deleted)
	END
END
GO

DROP TRIGGER trigCarrierDestinationWaitRate_D
GO
/*************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent deletion of Locked records
/*************************************/
CREATE TRIGGER trigCarrierDestinationWaitRate_IOD ON tblCarrierDestinationWaitRate INSTEAD OF DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewCarrierDestinationWaitRate X ON X.id = d.ID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('Locked (in use) records cannot be deleted', 16, 1)
		RETURN
	END
	ELSE
	BEGIN
		-- remove all references to the deleted rates
		UPDATE tblOrderSettlementCarrier
		  SET DestinationWaitRateID = NULL, DestinationWaitAmount = NULL
		WHERE BatchID IS NULL
		  AND DestinationWaitRateID IN (SELECT ID FROM deleted)
		-- do the actual delete now
		DELETE FROM tblCarrierDestinationWaitRate WHERE ID IN (SELECT ID FROM deleted)
	END
END
GO
DROP TRIGGER trigShipperDestinationWaitRate_D
GO
/*************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent deletion of Locked records
/*************************************/
CREATE TRIGGER trigShipperDestinationWaitRate_IOD ON tblShipperDestinationWaitRate INSTEAD OF DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewShipperDestinationWaitRate X ON X.id = d.ID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('Locked (in use) records cannot be deleted', 16, 1)
		RETURN
	END
	ELSE
	BEGIN
		-- remove all references to the deleted rates
		UPDATE tblOrderSettlementShipper
		  SET DestinationWaitRateID = NULL, DestinationWaitAmount = NULL
		WHERE BatchID IS NULL
		  AND DestinationWaitRateID IN (SELECT ID FROM deleted)
		-- do the actual delete now
		DELETE FROM tblShipperDestinationWaitRate WHERE ID IN (SELECT ID FROM deleted)
	END
END
GO

/*************************************/
-- Date Created: 19 Feb 2015
-- Author: Kevin Alons
-- Purpose: prevent deletion of Locked records
/*************************************/
CREATE TRIGGER trigCarrierFuelSurchargeRate_IOD ON tblCarrierFuelSurchargeRate INSTEAD OF DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewCarrierFuelSurchargeRate X ON X.id = d.ID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('Locked (in use) records cannot be deleted', 16, 1)
		RETURN
	END
	ELSE
	BEGIN
		-- ensure all references to the deleted rates are fully removed
		UPDATE tblOrderSettlementCarrier
		  SET FuelSurchargeRateID = NULL, FuelSurchargeAmount = NULL
		WHERE BatchID IS NULL
		  AND FuelSurchargeRateID IN (SELECT ID FROM deleted)
		-- do the actual update here
		DELETE FROM tblCarrierDestinationWaitRate WHERE ID IN (SELECT ID FROM deleted)
	END
END
GO

/*************************************/
-- Date Created: 19 Feb 2015
-- Author: Kevin Alons
-- Purpose: prevent deletion of Locked records
/*************************************/
CREATE TRIGGER trigShipperFuelSurchargeRate_IOD ON tblShipperFuelSurchargeRate INSTEAD OF DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewShipperFuelSurchargeRate X ON X.id = d.ID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('Locked (in use) records cannot be deleted', 16, 1)
		RETURN
	END
	ELSE
	BEGIN
		-- ensure all references to the deleted rates are fully removed
		UPDATE tblOrderSettlementShipper
		  SET FuelSurchargeRateID = NULL, FuelSurchargeAmount = NULL
		WHERE BatchID IS NULL
		  AND FuelSurchargeRateID IN (SELECT ID FROM deleted)
		-- do the actual update here
		DELETE FROM tblShipperDestinationWaitRate WHERE ID IN (SELECT ID FROM deleted)
	END
END
GO

DROP TRIGGER trigCarrierOrderRejectRate_D
GO
/*************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent deletion of Locked records
/*************************************/
CREATE TRIGGER trigCarrierOrderRejectRate_IOD ON tblCarrierOrderRejectRate INSTEAD OF DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewCarrierOrderRejectRate X ON X.id = d.ID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('Locked (in use) records cannot be deleted', 16, 1)
		RETURN
	END
	ELSE
	BEGIN
		-- remove all references to the deleted rates
		UPDATE tblOrderSettlementShipper
		  SET OrderRejectRateID = NULL, OrderRejectAmount = NULL
		WHERE BatchID IS NULL
		  AND OrderRejectRateID IN (SELECT ID FROM deleted)
		-- do the actual delete now
		DELETE FROM tblCarrierOrderRejectRate WHERE ID IN (SELECT ID FROM deleted)
	END
END
GO
DROP TRIGGER trigShipperOrderRejectRate_D
GO
/*************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent deletion of Locked records
/*************************************/
CREATE TRIGGER trigShipperOrderRejectRate_IOD ON tblShipperOrderRejectRate INSTEAD OF DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewShipperOrderRejectRate X ON X.id = d.ID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('Locked (in use) records cannot be deleted', 16, 1)
		RETURN
	END
	ELSE
	BEGIN
		-- remove all references to the deleted rates
		UPDATE tblOrderSettlementShipper
		  SET OrderRejectRateID = NULL, OrderRejectAmount = NULL
		WHERE BatchID IS NULL
		  AND OrderRejectRateID IN (SELECT ID FROM deleted)
		-- do the actual delete now
		DELETE FROM tblShipperOrderRejectRate WHERE ID IN (SELECT ID FROM deleted)
	END
END
GO

DROP TRIGGER trigCarrierOriginWaitRate_D
GO
/*************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent deletion of Locked records
/*************************************/
CREATE TRIGGER trigCarrierOriginWaitRate_IOD ON tblCarrierOriginWaitRate INSTEAD OF DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewCarrierOriginWaitRate X ON X.id = d.ID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('Locked (in use) records cannot be deleted', 16, 1)
		RETURN
	END
	ELSE
	BEGIN
		-- remove any references to the deleted rates
		UPDATE tblOrderSettlementShipper
		  SET OriginWaitRateID = NULL, OriginWaitAmount = NULL
		WHERE BatchID IS NULL
		  AND OriginWaitRateID IN (SELECT ID FROM deleted)
		DELETE FROM tblCarrierOriginWaitRate WHERE ID IN (SELECT ID FROM deleted)
	END
END
GO
DROP TRIGGER trigShipperOriginWaitRate_D
GO
/*************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent deletion of Locked records
/*************************************/
CREATE TRIGGER trigShipperOriginWaitRate_IOD ON tblShipperOriginWaitRate INSTEAD OF DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewShipperOriginWaitRate X ON X.id = d.ID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('Locked (in use) records cannot be deleted', 16, 1)
		RETURN
	END
	ELSE
	BEGIN
		-- remove any references to the deleted rates
		UPDATE tblOrderSettlementShipper
		  SET OriginWaitRateID = NULL, OriginWaitAmount = NULL
		WHERE BatchID IS NULL
		  AND OriginWaitRateID IN (SELECT ID FROM deleted)
		DELETE FROM tblShipperOriginWaitRate WHERE ID IN (SELECT ID FROM deleted)
	END
END
GO

ALTER TABLE dbo.tblCarrierRangeRate
	DROP CONSTRAINT FK_CarrierRangeRate_CarrierRateSheet
GO
ALTER TABLE dbo.tblCarrierRateSheet SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblCarrierRangeRate ADD CONSTRAINT
	FK_CarrierRangeRate_RateSheet FOREIGN KEY
	(
	RateSheetID
	) REFERENCES dbo.tblCarrierRateSheet
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION
	
GO
ALTER TABLE dbo.tblCarrierRangeRate SET (LOCK_ESCALATION = TABLE)
GO
DROP TRIGGER trigCarrierRangeRate_D
GO
/*************************************/
-- Date Created: 19 Feb 2015
-- Author: Kevin Alons
-- Purpose: manually do CASCADING Deletion of RangeRates (since using an INSTEAD OF Trigger there)
/*************************************/
CREATE TRIGGER trigCarrierRateSheet_IOD ON tblCarrierRateSheet INSTEAD OF DELETE AS
BEGIN
	SET NOCOUNT ON;
	-- manually do cascading delete
	DELETE FROM tblCarrierRangeRate WHERE RateSheetID IN (SELECT ID FROM deleted)
	-- do the actual delete now
	DELETE FROM tblCarrierRateSheet WHERE ID IN (SELECT ID FROM deleted)
END
GO
/*************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent deletion of RangeRate records that belong to a Locked RateSheet
/*************************************/
CREATE TRIGGER trigCarrierRangeRate_IOD ON tblCarrierRangeRate INSTEAD OF DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewCarrierRateSheet X ON X.ID = d.RateSheetID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('RangeRate records for a Locked Rate Sheet cannot be deleted', 16, 1)
		ROLLBACK
	END
	ELSE
	BEGIN
		-- remove any references to the deleted rates
		UPDATE tblOrderSettlementShipper
		  SET RangeRateID = NULL, LoadAmount = NULL
		WHERE BatchID IS NULL
		  AND RangeRateID IN (SELECT ID FROM deleted)
		DELETE FROM tblCarrierRangeRate WHERE ID IN (SELECT ID FROM deleted)
	END
END
GO
ALTER TABLE dbo.tblShipperRangeRate
	DROP CONSTRAINT FK_ShipperRangeRate_ShipperRateSheet
GO
ALTER TABLE dbo.tblShipperRateSheet SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblShipperRangeRate ADD CONSTRAINT
	FK_ShipperRangeRate_RateSheet FOREIGN KEY
	(
	RateSheetID
	) REFERENCES dbo.tblShipperRateSheet
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION
	
GO
ALTER TABLE dbo.tblShipperRangeRate SET (LOCK_ESCALATION = TABLE)
GO
DROP TRIGGER trigShipperRangeRate_D
GO
/*************************************/
-- Date Created: 19 Feb 2015
-- Author: Kevin Alons
-- Purpose: manually do CASCADING Deletion of RangeRates (since using an INSTEAD OF Trigger there)
/*************************************/
CREATE TRIGGER trigShipperRateSheet_IOD ON tblShipperRateSheet INSTEAD OF DELETE AS
BEGIN
	SET NOCOUNT ON;
	-- manually do cascading delete
	DELETE FROM tblShipperRangeRate WHERE RateSheetID IN (SELECT ID FROM deleted)
	-- do the actual delete now
	DELETE FROM tblShipperRateSheet WHERE ID IN (SELECT ID FROM deleted)
END
GO
/*************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent deletion of RangeRate records that belong to a Locked RateSheet
/*************************************/
CREATE TRIGGER trigShipperRangeRate_IOD ON tblShipperRangeRate INSTEAD OF DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewShipperRateSheet X ON X.ID = d.RateSheetID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('RangeRate records for a Locked Rate Sheet cannot be deleted', 16, 1)
		ROLLBACK
	END
	ELSE
	BEGIN
		-- remove any references to the deleted rates
		UPDATE tblOrderSettlementShipper
		  SET RangeRateID = NULL, LoadAmount = NULL
		WHERE BatchID IS NULL
		  AND RangeRateID IN (SELECT ID FROM deleted)
		DELETE FROM tblShipperRangeRate WHERE ID IN (SELECT ID FROM deleted)
	END
END
GO

ALTER TABLE dbo.tblCarrierRouteRate
	DROP CONSTRAINT FK_CarrierRouteRate_Route
GO
ALTER TABLE dbo.tblRoute SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblCarrierRouteRate ADD CONSTRAINT
	FK_CarrierRouteRate_Route FOREIGN KEY
	(
	RouteID
	) REFERENCES dbo.tblRoute
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblCarrierRouteRate SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblCarrierRouteRate
	DROP CONSTRAINT FK_CarrierRouteRate_Carrier
GO
ALTER TABLE dbo.tblCarrier SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblCarrierRouteRate ADD CONSTRAINT
	FK_CarrierRouteRate_Carrier FOREIGN KEY
	(
	CarrierID
	) REFERENCES dbo.tblCarrier
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblCarrierRouteRate SET (LOCK_ESCALATION = TABLE)
GO
DROP TRIGGER trigCarrierRouteRate_D
GO
/*************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: handle specialized logic related to Effective/End dates (all handled through the associated view)
/*************************************/
CREATE TRIGGER trigCarrierRouteRate_IOD ON tblCarrierRouteRate INSTEAD OF DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewCarrierRouteRate X ON X.ID = d.ID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('Locked (in use) records cannot be deleted', 16, 1)
		RETURN
	END
	ELSE
	BEGIN
		-- remove any references to the deleted rates
		UPDATE tblOrderSettlementShipper
		  SET RouteRateID = NULL, LoadAmount = NULL
		WHERE BatchID IS NULL
		  AND RouteRateID IN (SELECT ID FROM deleted)
		-- do the actual delete now
		DELETE FROM tblCarrierRouteRate WHERE ID IN (SELECT ID FROM deleted)
	END
END
GO
ALTER TABLE dbo.tblShipperRouteRate
	DROP CONSTRAINT FK_ShipperRouteRate_Route
GO
ALTER TABLE dbo.tblRoute SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblShipperRouteRate ADD CONSTRAINT
	FK_ShipperRouteRate_Route FOREIGN KEY
	(
	RouteID
	) REFERENCES dbo.tblRoute
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblShipperRouteRate SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblShipperRouteRate
	DROP CONSTRAINT FK_ShipperRouteRate_Shipper
GO
ALTER TABLE dbo.tblCustomer SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblShipperRouteRate ADD CONSTRAINT
	FK_ShipperRouteRate_Shipper FOREIGN KEY
	(
	ShipperID
	) REFERENCES dbo.tblCustomer
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblShipperRouteRate SET (LOCK_ESCALATION = TABLE)
GO
DROP TRIGGER trigShipperRouteRate_D
GO
/*************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: handle specialized logic related to Effective/End dates (all handled through the associated view)
/*************************************/
CREATE TRIGGER trigShipperRouteRate_IOD ON tblShipperRouteRate INSTEAD OF DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewShipperRouteRate X ON X.ID = d.ID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('Locked (in use) records cannot be deleted', 16, 1)
		RETURN
	END
	ELSE
	BEGIN
		-- remove any references to the deleted rates
		UPDATE tblOrderSettlementShipper
		  SET RouteRateID = NULL, LoadAmount = NULL
		WHERE BatchID IS NULL
		  AND RouteRateID IN (SELECT ID FROM deleted)
		-- do the actual delete now
		DELETE FROM tblShipperRouteRate WHERE ID IN (SELECT ID FROM deleted)
	END
END
GO
/*************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: manual CASCADE delete (since tblCarrier|Shipper RouteRate table also uses INSTEAD OF DELETE trigger)
/*************************************/
CREATE TRIGGER trigRoute_IOD ON tblRoute INSTEAD OF DELETE AS
BEGIN
	SET NOCOUNT ON;
	-- do cascading delete
	DELETE FROM tblCarrierRouteRate WHERE RouteID IN (SELECT ID FROM deleted)
	DELETE FROM tblShipperRouteRate WHERE RouteID IN (SELECT ID FROM deleted)
	-- do the actual delete action
	DELETE FROM tblRoute WHERE ID IN (SELECT ID FROM deleted)
END
GO

DROP TRIGGER trigCarrierWaitFeeParameter_D
GO
/*************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent deletion of Locked (in use) records
/*************************************/
CREATE TRIGGER trigCarrierWaitFeeParameter_IOD ON tblCarrierWaitFeeParameter INSTEAD OF DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewCarrierWaitFeeParameter X ON X.ID = d.ID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('Locked (in use) records cannot be deleted', 16, 1)
		RETURN
	END
	ELSE
	BEGIN
		-- remove any references to the deleted rates
		UPDATE tblOrderSettlementShipper
		  SET WaitFeeParameterID = NULL, DestinationWaitAmount = NULL
		WHERE BatchID IS NULL
		  AND WaitFeeParameterID IN (SELECT ID FROM deleted)
		-- do the actual delete action now
		DELETE FROM tblCarrierWaitFeeParameter WHERE ID IN (SELECT ID FROM deleted)
	END
END
GO
DROP TRIGGER trigShipperWaitFeeParameter_D
GO
/*************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent deletion of Locked (in use) records
/*************************************/
CREATE TRIGGER trigShipperWaitFeeParameter_IOD ON tblShipperWaitFeeParameter INSTEAD OF DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewShipperWaitFeeParameter X ON X.ID = d.ID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('Locked (in use) records cannot be deleted', 16, 1)
		RETURN
	END
	ELSE
	BEGIN
		-- remove any references to the deleted rates
		UPDATE tblOrderSettlementShipper
		  SET WaitFeeParameterID = NULL, DestinationWaitAmount = NULL
		WHERE BatchID IS NULL
		  AND WaitFeeParameterID IN (SELECT ID FROM deleted)
		-- do the actual delete action now
		DELETE FROM tblShipperWaitFeeParameter WHERE ID IN (SELECT ID FROM deleted)
	END
END
GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details + FINANCIAL INFO into a single view
/***********************************/
ALTER VIEW [dbo].[viewOrder_Financial_Carrier] AS 
	SELECT O.* 
		, TicketNums = dbo.fnOrderTicketDetails(O.ID, 'TicketNums', '<br/>') 
		, TankNums = dbo.fnOrderTicketDetails(O.ID, 'TankNums', '<br/>') 
		, PreviousDestinations = dbo.fnRerouteDetails(O.ID, 'PreviousDestinations', '<br/>') 
		, RerouteUsers = dbo.fnRerouteDetails(O.ID, 'RerouteUsers', '<br/>') 
		, RerouteDates = dbo.fnRerouteDetails(O.ID, 'RerouteDates', '<br/>') 
		, RerouteNotes = dbo.fnRerouteDetails(O.ID, 'RerouteNotes', '<br/>') 
		, InvoiceRatesAppliedDate = dbo.fnUTC_To_Local(OIC.CreateDateUTC, O.OriginTimeZoneID, O.OriginUseDST)
		, OIC.BatchID
		, InvoiceBatchNum = OIC.BatchNum 
		, InvoiceOriginWaitBillableMinutes = OIC.OriginWaitBillableMinutes
		, InvoiceDestinationWaitBillableMinutes = OIC.DestinationWaitBillableMinutes
		, InvoiceTotalWaitBillableMinutes = isnull(OIC.OriginWaitBillableMinutes, 0) + ISNULL(OIC.DestinationWaitBillableMinutes, 0) 
		, InvoiceWaitFeeParameterID = WaitFeeParameterID
		, InvoiceWaitFeeSubUnit = WaitFeeSubUnit
		, InvoiceWaitFeeRoundingType = WaitFeeRoundingType
		, InvoiceOriginWaitRate = OIC.OriginWaitRate 
		, InvoiceOriginWaitAmount = OIC.OriginWaitAmount 		
		, InvoiceDestinationWaitRate = OIC.DestinationWaitRate 
		, InvoiceDestinationWaitAmount = OIC.DestinationWaitAmount 
		, InvoiceTotalWaitAmount = OIC.TotalWaitAmount
		, InvoiceOrderRejectRate = OIC.OrderRejectRate	-- changed
		, InvoiceOrderRejectRateType = OIC.OrderRejectRateType  -- changed
		, InvoiceOrderRejectAmount = OIC.OrderRejectAmount  -- changed
		, InvoiceChainupRate = OIC.ChainupRate
		, InvoiceChainupRateType = OIC.ChainupRateType
		, InvoiceChainupAmount = OIC.ChainupAmount 
		, InvoiceRerouteRate = OIC.RerouteRate
		, InvoiceRerouteRateType = OIC.RerouteRateType
		, InvoiceRerouteAmount = OIC.RerouteAmount 
		, InvoiceH2SRate = OIC.H2SRate
		, InvoiceH2SRateType = OIC.H2SRateType
		, InvoiceH2SAmount = OIC.H2SAmount
		, InvoiceSplitLoadRate = OIC.SplitLoadRate
		, InvoiceSplitLoadRateType = OIC.SplitLoadRateType
		, InvoiceSplitLoadAmount = OIC.SplitLoadAmount
		, InvoiceTaxRate = OIC.OriginTaxRate
		, InvoiceSettlementUom = OIC.SettlementUom -- changed
		, InvoiceSettlementUomShort = OIC.SettlementUomShort -- changed
		, InvoiceMinSettlementUnits = OIC.MinSettlementUnits
		, InvoiceUnits = OIC.SettlementUnits
		, InvoiceRouteRate = OIC.RouteRate
		, InvoiceRouteRateType = OIC.RouteRateType
		, InvoiceRateSheetRate = OIC.RateSheetRate
		, InvoiceRateSheetRateType = OIC.RateSheetRateType
		, InvoiceFuelSurchargeRate = OIC.FuelSurchargeRate
		, InvoiceFuelSurchargeAmount = OIC.FuelSurchargeAmount
		, InvoiceLoadAmount = OIC.LoadAmount
		, InvoiceTotalAmount = OIC.TotalAmount
	FROM dbo.viewOrderLocalDates O
	LEFT JOIN viewOrderSettlementCarrier OIC ON OIC.OrderID = O.ID
	WHERE O.StatusID NOT IN (-10) -- don't include "Generated" orders (only Assigned+)

GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details + FINANCIAL INFO into a single view
/***********************************/
ALTER VIEW [dbo].[viewOrder_Financial_Shipper] AS 
	SELECT O.* 
		, Shipper = Customer
		, TicketNums = dbo.fnOrderTicketDetails(O.ID, 'TicketNums', '<br/>') 
		, TankNums = dbo.fnOrderTicketDetails(O.ID, 'TankNums', '<br/>') 
		, PreviousDestinations = dbo.fnRerouteDetails(O.ID, 'PreviousDestinations', '<br/>') 
		, RerouteUsers = dbo.fnRerouteDetails(O.ID, 'RerouteUsers', '<br/>') 
		, RerouteDates = dbo.fnRerouteDetails(O.ID, 'RerouteDates', '<br/>') 
		, RerouteNotes = dbo.fnRerouteDetails(O.ID, 'RerouteNotes', '<br/>') 
		, InvoiceRatesAppliedDate = dbo.fnUTC_To_Local(OIC.CreateDateUTC, O.OriginTimeZoneID, O.OriginUseDST)
		, OIC.BatchID
		, InvoiceBatchNum = OIC.BatchNum 
		, InvoiceOriginWaitBillableMinutes = OIC.OriginWaitBillableMinutes
		, InvoiceDestinationWaitBillableMinutes = OIC.DestinationWaitBillableMinutes
		, InvoiceTotalWaitBillableMinutes = isnull(OIC.OriginWaitBillableMinutes, 0) + ISNULL(OIC.DestinationWaitBillableMinutes, 0) 
		, InvoiceWaitFeeParameterID = WaitFeeParameterID
		, InvoiceWaitFeeSubUnit = WaitFeeSubUnit
		, InvoiceWaitFeeRoundingType = WaitFeeRoundingType
		, InvoiceOriginWaitRate = OIC.OriginWaitRate 
		, InvoiceOriginWaitAmount = OIC.OriginWaitAmount 		
		, InvoiceDestinationWaitRate = OIC.DestinationWaitRate 
		, InvoiceDestinationWaitAmount = OIC.DestinationWaitAmount 
		, InvoiceTotalWaitAmount = OIC.TotalWaitAmount
		, InvoiceOrderRejectRate = OIC.OrderRejectRate  -- changed
		, InvoiceOrderRejectRateType = OIC.OrderRejectRateType -- changed
		, InvoiceOrderRejectAmount = OIC.OrderRejectAmount  -- changed
		, InvoiceChainupRate = OIC.ChainupRate
		, InvoiceChainupRateType = OIC.ChainupRateType
		, InvoiceChainupAmount = OIC.ChainupAmount 
		, InvoiceRerouteRate = OIC.RerouteRate
		, InvoiceRerouteRateType = OIC.RerouteRateType
		, InvoiceRerouteAmount = OIC.RerouteAmount 
		, InvoiceH2SRate = OIC.H2SRate
		, InvoiceH2SRateType = OIC.H2SRateType
		, InvoiceH2SAmount = OIC.H2SAmount
		, InvoiceSplitLoadRate = OIC.SplitLoadRate
		, InvoiceSplitLoadRateType = OIC.SplitLoadRateType
		, InvoiceSplitLoadAmount = OIC.SplitLoadAmount
		, InvoiceTaxRate = OIC.OriginTaxRate
		, InvoiceSettlementUom = OIC.SettlementUom -- changed
		, InvoiceSettlementUomShort = OIC.SettlementUomShort -- changed
		, InvoiceMinSettlementUnits = OIC.MinSettlementUnits
		, InvoiceUnits = OIC.SettlementUnits
		, InvoiceRouteRate = OIC.RouteRate
		, InvoiceRouteRateType = OIC.RouteRateType
		, InvoiceRateSheetRate = OIC.RateSheetRate
		, InvoiceRateSheetRateType = OIC.RateSheetRateType
		, InvoiceFuelSurchargeRate = OIC.FuelSurchargeRate
		, InvoiceFuelSurchargeAmount = OIC.FuelSurchargeAmount
		, InvoiceLoadAmount = OIC.LoadAmount
		, InvoiceTotalAmount = OIC.TotalAmount
	FROM dbo.viewOrderLocalDates O
	LEFT JOIN viewOrderSettlementShipper OIC ON OIC.OrderID = O.ID
	WHERE O.StatusID NOT IN (-10) -- don't include "Generated" orders (only Assigned+)
GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Carrier "Settlement" $$ values to an Delivered/Audited order
/***********************************/
ALTER PROCEDURE [dbo].[spApplyRatesCarrier]
(
  @ID int
, @UserName varchar(100)
, @OriginWaitAmount money = NULL
, @DestWaitAmount money = NULL
, @RejectionAmount money = NULL
, @FuelSurchargeAmount money = NULL
, @LoadAmount money = NULL
, @AssessorialRateTypeID_CSV varchar(max) = NULL
, @AssessorialAmount_CSV varchar(max) = null
, @ResetOverrides bit = 0
) AS BEGIN

	SET NOCOUNT ON
	
	-- ensure this order hasn't yet been fully settled
	IF EXISTS (SELECT * FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND BatchID IS NOT NULL)
	BEGIN
		RAISERROR('Invoice has already been fully settled', 16, 1)
		RETURN
	END

	-- ensure that Shipper Rates have been applied prior to applying Carrier rates (since they could be dependent on Shipper rates)
	IF NOT EXISTS (SELECT * FROM tblOrderSettlementShipper WHERE OrderID = @ID)
		EXEC spApplyRatesShipper @ID, @UserName

	/*
drop table #i
drop table #ia

declare @id int, @userName varchar(100)
, @OriginWaitAmount money 
, @DestWaitAmount money 
, @RejectionAmount money 
, @FuelSurchargeAmount money 
, @LoadAmount money 
, @AssessorialRateTypeID_CSV varchar(max) 
, @AssessorialAmount_CSV varchar(max) 
, @ResetOverrides bit 
select @ID=62210,@UserName='kalons'
--select @OriginWaitAmount=1.0000,@DestWaitAmount=2.0000,@RejectionAmount=3.0000,@FuelSurchargeAmount=4.0000,@LoadAmount=5.0000,@AssessorialRateTypeID_CSV='1',@AssessorialAmount_CSV='99'
select @ResetOverrides=0
--	*/
	-- get the provided manual Assessorial Rates (if any) into a usable able
	DECLARE @ManualAssessorialRates TABLE (ID int primary key, Amount money)
	INSERT INTO @ManualAssessorialRates (ID, Amount) 
		SELECT ID.ID, R.Amount
		FROM (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), ID FROM dbo.fnSplitCSVIDs(@AssessorialRateTypeID_CSV)) ID
		JOIN (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), amount = CAST(value as money) FROM dbo.fnSplitCSV(@AssessorialAmount_CSV)) R ON R.sortid = ID.sortid

	-- add in an existing manual Assessorial Rates from the existing Order Settlement (if any)
	INSERT INTO @ManualAssessorialRates (ID, Amount)
		SELECT AssessorialRateTypeID, Amount 
		FROM dbo.tblOrderSettlementCarrierAssessorialCharge 
		WHERE OrderID = @ID AND AssessorialRateID IS NULL 
			AND ID NOT IN (SELECT ID FROM @ManualAssessorialRates)
			AND @ResetOverrides = 0
--select * from @ManualAssessorialRates

	IF (@ResetOverrides = 0)
	BEGIN 
		SELECT @OriginWaitAmount = OriginWaitAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @OriginWaitAmount IS NULL AND OriginWaitRateID IS NULL
		SELECT @DestWaitAmount = DestinationWaitAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @DestWaitAmount IS NULL AND DestinationWaitRateID IS NULL
		SELECT @RejectionAmount = OrderRejectAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @RejectionAmount IS NULL AND OrderRejectRateID IS NULL
		SELECT @FuelSurchargeAmount = FuelSurchargeAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @FuelSurchargeAmount IS NULL AND FuelSurchargeRateID IS NULL
		SELECT @LoadAmount = LoadAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @LoadAmount IS NULL AND RouteRateID IS NULL AND RangeRateID IS NULL
	END
--select OverrideOriginWaitAmount = @OriginWaitAmount, OverrideDestWaitAmount = @DestWaitAmount, OverrideRejectionAmount = @RejectionAmount, OverrideFuelSurchargeAmount = @FuelSurchargeAmount, OverrideLoadAmount = @LoadAmount

	DECLARE @SettlementUnits TABLE
	(
		OrderID int
	  , SettlementUomID int
	  , SettlementFactorID int
	  , MinSettlementUnits decimal(18, 10)
	  , ActualUnits decimal(18, 10)
	  , SettlementUnits decimal(18, 10)
	)
	INSERT INTO @SettlementUnits (OrderID, SettlementUomID, SettlementFactorID, MinSettlementUnits, ActualUnits, SettlementUnits)
		SELECT OrderID, SettlementUomID, SettlementFactorID, MinSettlementUnits, ActualUnits, SettlementUnits
		FROM dbo.viewOrderSettlementUnitsCarrier 
		WHERE OrderID = @ID

	SELECT OrderID = @ID
		, OrderDate
		, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
		, RouteRateID, RangeRateID, LoadAmount
		, WaitFeeParameterID
		, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount 
		, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount
		, OrderRejectRateID, OrderRejectAmount
		, FuelSurchargeRateID, FuelSurchargeRate, FuelSurchargeAmount
		, OriginTaxRate
		, TotalAmount = ISNULL(LoadAmount, 0) 
			+ ISNULL(OriginWaitAmount, 0) 
			+ ISNULL(DestWaitAmount, 0) 
			+ ISNULL(OrderRejectAmount, 0) 
			+ ISNULL(FuelSurchargeAmount, 0)
		, CreatedByUser = @UserName
	INTO #I
	FROM (
		SELECT OrderDate
			, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
			, RouteRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RouteRateID ELSE NULL END
			, RangeRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RangeRateID ELSE NULL END
			, LoadAmount = ISNULL(@LoadAmount, RR.Amount)
			, WaitFeeParameterID
			, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount
			, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount
			, OrderRejectRateID = CASE WHEN @RejectionAmount IS NULL THEN RD.RateID ELSE NULL END
			, OrderRejectAmount = isnull(@RejectionAmount, RD.Amount)
			, FuelSurchargeRateID = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.RateID ELSE NULL END
			, FuelSurchargeRate = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.Rate ELSE NULL END
			, FuelSurchargeAmount = isnull(@FuelSurchargeAmount, FSR.Amount)
			, OriginTaxRate
		FROM (
			SELECT O.ID
				, O.OriginID
				, O.CarrierID
				, O.RouteID
				, SU.SettlementFactorID
				, SU.SettlementUomID
				, SU.ActualUnits
				, SU.MinSettlementUnits
				, SU.SettlementUnits
				, RouteMiles = O.ActualMiles
				, O.OrderDate
				, O.ChainUp
				, O.H2S
				, O.Rejected
				, O.RerouteCount
				, IsSplit = CASE WHEN O.Rejected = 0 AND O.TicketCount > 1 THEN 1 ELSE 0 END
				, WaitFeeParameterID = WFP.ID
				, OriginWaitRateID = CASE WHEN @OriginWaitAmount IS NULL THEN OWD.RateID ELSE NULL END
				, OriginWaitBillableMinutes = OWD.BillableMinutes
				, OriginWaitBillableHours = OWD.BillableHours
				, OriginWaitAmount = isnull(@OriginWaitAmount, OWD.Amount)
				, DestWaitRateID = CASE WHEN @DestWaitAmount IS NULL THEN DWD.RateID ELSE NULL END
				, DestWaitBillableMinutes = DWD.BillableMinutes
				, DestWaitBillableHours = DWD.BillableHours
				, DestWaitAmount = isnull(@DestWaitAmount, DWD.Amount)
				, OriginTaxRate = OO.TaxRate
			FROM dbo.viewOrder O
			JOIN @SettlementUnits SU ON SU.OrderID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			OUTER APPLY dbo.fnOrderCarrierOriginWaitData(@ID) OWD 
			OUTER APPLY dbo.fnOrderCarrierDestinationWaitData(@ID) DWD 
			OUTER APPLY dbo.fnOrderCarrierWaitFeeParameter(@ID) WFP
			WHERE O.ID = @ID
		) X
		OUTER APPLY dbo.fnOrderCarrierLoadAmount(@ID, SettlementUnits, SettlementUomID) RR 
		OUTER APPLY dbo.fnOrderCarrierOrderRejectData(@ID, SettlementUnits, SettlementUomID) RD
		OUTER APPLY dbo.fnOrderCarrierFuelSurchargeData(@ID) FSR
	) X2
--select * from #I
select * from #i
	SELECT OrderID = @ID, AssessorialRateTypeID, AssessorialRateID = RateID, Amount, CreatedByUser = @UserName 
	INTO #IA
	FROM @SettlementUnits SU
	CROSS APPLY dbo.fnOrderCarrierAssessorialAmounts(@ID, SU.SettlementUnits, SU.SettlementUomID) CAA
	WHERE CAA.AssessorialRateTypeID NOT IN (SELECT ID FROM @ManualAssessorialRates)
/*
select OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser from #IA
union
select @ID, ID, NULL, Amount, @UserName from @ManualAssessorialRates
--*/
	DECLARE @CreatedTran bit; SET @CreatedTran = CASE WHEN @@TRANCOUNT = 0 THEN 1 ELSE 0 END
	BEGIN TRY
		IF (@CreatedTran = 1)
			BEGIN TRAN

		-- remove the existing settlment record (if any)
		DELETE FROM tblOrderSettlementCarrierAssessorialCharge WHERE OrderID = @ID
		DELETE FROM tblOrderSettlementCarrier WHERE OrderID = @ID
	
		-- all Units and Rates are first normalized to the Order.OriginUOM then consistent processing
		-- and persisted in the Invoice (Settlement) record in this Order.OriginUOM 
		INSERT INTO tblOrderSettlementCarrier (
			OrderID
			, OrderDate
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestinationWaitRateID 
			, DestinationWaitBillableMinutes 
			, DestinationWaitBillableHours
			, DestinationWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser)
		SELECT OrderID
			, OrderDate
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestWaitRateID 
			, DestWaitBillableMinutes 
			, DestWaitBillableHours
			, DestWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser
		FROM #I

		INSERT INTO tblOrderSettlementCarrierAssessorialCharge (OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser)
			SELECT OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser FROM #IA
			UNION
			SELECT @ID, ID, NULL, Amount, @UserName FROM @ManualAssessorialRates
	
		IF (@CreatedTran = 1)
			COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @msg varchar(max), @severity int
		SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY()
		ROLLBACK
		RAISERROR(@msg, @severity, 1)
		RETURN
	END CATCH	
	
END

GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Shipper "Settlement" $$ values to an Delivered/Audited order
/***********************************/
ALTER PROCEDURE [dbo].[spApplyRatesShipper]
(
  @ID int
, @UserName varchar(100)
, @OriginWaitAmount money = NULL
, @DestWaitAmount money = NULL
, @RejectionAmount money = NULL
, @FuelSurchargeAmount money = NULL
, @LoadAmount money = NULL
, @AssessorialRateTypeID_CSV varchar(max) = NULL
, @AssessorialAmount_CSV varchar(max) = null
, @ResetOverrides bit = 0
) AS BEGIN

	SET NOCOUNT ON
	
	-- ensure this order hasn't yet been fully settled
	IF EXISTS (SELECT * FROM tblOrderSettlementShipper WHERE OrderID = @ID AND BatchID IS NOT NULL)
	BEGIN
		RAISERROR('Invoice has already been fully settled', 16, 1)
		RETURN
	END

	-- get the provided manual Assessorial Rates (if any) into a usable able
	DECLARE @ManualAssessorialRates TABLE (ID int primary key, Amount money)
	INSERT INTO @ManualAssessorialRates (ID, Amount) 
		SELECT ID.ID, R.Amount
		FROM (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), ID FROM dbo.fnSplitCSVIDs(@AssessorialRateTypeID_CSV)) ID
		JOIN (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), amount = CAST(value as money) FROM dbo.fnSplitCSV(@AssessorialAmount_CSV)) R ON R.sortid = ID.sortid

	-- add in an existing manual Assessorial Rates from the existing Order Settlement (if any)
	INSERT INTO @ManualAssessorialRates (ID, Amount)
		SELECT AssessorialRateTypeID, Amount 
		FROM dbo.tblOrderSettlementShipperAssessorialCharge 
		WHERE OrderID = @ID AND AssessorialRateID IS NULL 
			AND ID NOT IN (SELECT ID FROM @ManualAssessorialRates)
			AND @ResetOverrides = 0

	IF (@ResetOverrides = 0)
	BEGIN 
		SELECT @OriginWaitAmount = OriginWaitAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @OriginWaitAmount IS NULL AND OriginWaitRateID IS NULL
		SELECT @DestWaitAmount = DestinationWaitAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @DestWaitAmount IS NULL AND DestinationWaitRateID IS NULL
		SELECT @RejectionAmount = OrderRejectAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @RejectionAmount IS NULL AND OrderRejectRateID IS NULL
		SELECT @FuelSurchargeAmount = FuelSurchargeAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @FuelSurchargeAmount IS NULL AND FuelSurchargeRateID IS NULL
		SELECT @LoadAmount = LoadAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @LoadAmount IS NULL AND RouteRateID IS NULL AND RangeRateID IS NULL
	END

	DECLARE @SettlementUnits TABLE
	(
		OrderID int
	  , SettlementUomID int
	  , SettlementFactorID int
	  , MinSettlementUnits decimal(18, 10)
	  , ActualUnits decimal(18, 10)
	  , SettlementUnits decimal(18, 10)
	)
	INSERT INTO @SettlementUnits (OrderID, SettlementUomID, SettlementFactorID, MinSettlementUnits, ActualUnits, SettlementUnits)
		SELECT OrderID, SettlementUomID, SettlementFactorID, MinSettlementUnits, ActualUnits, SettlementUnits
		FROM dbo.viewOrderSettlementUnitsShipper 
		WHERE OrderID = @ID

	SELECT OrderID = @ID
		, OrderDate
		, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
		, RouteRateID, RangeRateID, LoadAmount
		, WaitFeeParameterID
		, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount 
		, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount
		, OrderRejectRateID, OrderRejectAmount
		, FuelSurchargeRateID, FuelSurchargeRate, FuelSurchargeAmount
		, OriginTaxRate
		, TotalAmount = ISNULL(LoadAmount, 0) 
			+ ISNULL(OriginWaitAmount, 0) 
			+ ISNULL(DestWaitAmount, 0) 
			+ ISNULL(OrderRejectAmount, 0) 
			+ ISNULL(FuelSurchargeAmount, 0)
		, CreatedByUser = @UserName
	INTO #I
	FROM (
		SELECT OrderDate
			, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
			, RouteRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RouteRateID ELSE NULL END
			, RangeRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RangeRateID ELSE NULL END
			, LoadAmount = ISNULL(@LoadAmount, RR.Amount)
			, WaitFeeParameterID
			, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount
			, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount
			, OrderRejectRateID = CASE WHEN @RejectionAmount IS NULL THEN RD.RateID ELSE NULL END
			, OrderRejectAmount = isnull(@RejectionAmount, RD.Amount)
			, FuelSurchargeRateID = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.RateID ELSE NULL END
			, FuelSurchargeRate = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.Rate ELSE NULL END
			, FuelSurchargeAmount = isnull(@FuelSurchargeAmount, FSR.Amount)
			, OriginTaxRate
		FROM (
			SELECT O.ID
				, O.OriginID
				, O.CustomerID
				, O.RouteID
				, SU.SettlementFactorID
				, SU.SettlementUomID
				, SU.ActualUnits
				, SU.MinSettlementUnits
				, SU.SettlementUnits
				, RouteMiles = O.ActualMiles
				, O.OrderDate
				, O.ChainUp
				, O.H2S
				, O.Rejected
				, O.RerouteCount
				, IsSplit = CASE WHEN O.Rejected = 0 AND O.TicketCount > 1 THEN 1 ELSE 0 END
				, WaitFeeParameterID = WFP.ID
				, OriginWaitRateID = CASE WHEN @OriginWaitAmount IS NULL THEN OWD.RateID ELSE NULL END
				, OriginWaitBillableMinutes = OWD.BillableMinutes
				, OriginWaitBillableHours = OWD.BillableHours
				, OriginWaitAmount = isnull(@OriginWaitAmount, OWD.Amount)
				, DestWaitRateID = CASE WHEN @DestWaitAmount IS NULL THEN DWD.RateID ELSE NULL END
				, DestWaitBillableMinutes = DWD.BillableMinutes
				, DestWaitBillableHours = DWD.BillableHours
				, DestWaitAmount = isnull(@DestWaitAmount, DWD.Amount)
				, OriginTaxRate = OO.TaxRate
			FROM dbo.viewOrder O
			JOIN @SettlementUnits SU ON SU.OrderID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			OUTER APPLY dbo.fnOrderShipperOriginWaitData(@ID) OWD 
			OUTER APPLY dbo.fnOrderShipperDestinationWaitData(@ID) DWD 
			OUTER APPLY dbo.fnOrderShipperWaitFeeParameter(@ID) WFP
			WHERE O.ID = @ID
		) X
		OUTER APPLY dbo.fnOrderShipperLoadAmount(@ID, SettlementUnits, SettlementUomID) RR 
		OUTER APPLY dbo.fnOrderShipperOrderRejectData(@ID, SettlementUnits, SettlementUomID) RD
		OUTER APPLY dbo.fnOrderShipperFuelSurchargeData(@ID) FSR
	) X2

	SELECT OrderID = @ID, AssessorialRateTypeID, AssessorialRateID = RateID, Amount, CreatedByUser = @UserName 
	INTO #IA
	FROM @SettlementUnits SU
	CROSS APPLY dbo.fnOrderShipperAssessorialAmounts(@ID, SU.SettlementUnits, SU.SettlementUomID) CAA
	WHERE CAA.AssessorialRateTypeID NOT IN (SELECT ID FROM @ManualAssessorialRates)

	DECLARE @CreatedTran bit; SET @CreatedTran = CASE WHEN @@TRANCOUNT = 0 THEN 1 ELSE 0 END
	BEGIN TRY
		IF (@CreatedTran = 1)
			BEGIN TRAN

		-- remove the existing settlment record (if any)
		DELETE FROM tblOrderSettlementShipperAssessorialCharge WHERE OrderID = @ID
		DELETE FROM tblOrderSettlementShipper WHERE OrderID = @ID
	
		-- all Units and Rates are first normalized to the Order.OriginUOM then consistent processing
		-- and persisted in the Invoice (Settlement) record in this Order.OriginUOM 
		INSERT INTO tblOrderSettlementShipper (
			OrderID
			, OrderDate
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestinationWaitRateID 
			, DestinationWaitBillableMinutes 
			, DestinationWaitBillableHours
			, DestinationWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser)
		SELECT OrderID
			, OrderDate
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestWaitRateID 
			, DestWaitBillableMinutes 
			, DestWaitBillableHours
			, DestWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser
		FROM #I

		INSERT INTO tblOrderSettlementShipperAssessorialCharge (OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser)
			SELECT OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser FROM #IA
			UNION
			SELECT @ID, ID, NULL, Amount, @UserName FROM @ManualAssessorialRates
	
		IF (@CreatedTran = 1)
			COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @msg varchar(max), @severity int
		SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY()
		ROLLBACK
		RAISERROR(@msg, @severity, 1)
		RETURN
	END CATCH	
	
END
GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details + FINANCIAL INFO into a single view
/***********************************/
ALTER VIEW [dbo].[viewOrder_Financial_Carrier] AS 
	SELECT O.* 
		, TicketNums = dbo.fnOrderTicketDetails(O.ID, 'TicketNums', '<br/>') 
		, TankNums = dbo.fnOrderTicketDetails(O.ID, 'TankNums', '<br/>') 
		, PreviousDestinations = dbo.fnRerouteDetails(O.ID, 'PreviousDestinations', '<br/>') 
		, RerouteUsers = dbo.fnRerouteDetails(O.ID, 'RerouteUsers', '<br/>') 
		, RerouteDates = dbo.fnRerouteDetails(O.ID, 'RerouteDates', '<br/>') 
		, RerouteNotes = dbo.fnRerouteDetails(O.ID, 'RerouteNotes', '<br/>') 
		, InvoiceRatesAppliedDate = dbo.fnUTC_To_Local(OIC.CreateDateUTC, O.OriginTimeZoneID, O.OriginUseDST)
		, OIC.BatchID
		, InvoiceBatchNum = OIC.BatchNum 
		, InvoiceOriginWaitBillableMinutes = OIC.OriginWaitBillableMinutes
		, InvoiceDestinationWaitBillableMinutes = OIC.DestinationWaitBillableMinutes
		, InvoiceTotalWaitBillableMinutes = isnull(OIC.OriginWaitBillableMinutes, 0) + ISNULL(OIC.DestinationWaitBillableMinutes, 0) 
		, InvoiceWaitFeeParameterID = WaitFeeParameterID
		, InvoiceWaitFeeSubUnit = WaitFeeSubUnit
		, InvoiceWaitFeeRoundingType = WaitFeeRoundingType
		, InvoiceOriginWaitRate = OIC.OriginWaitRate 
		, InvoiceOriginWaitAmount = OIC.OriginWaitAmount 		
		, InvoiceDestinationWaitRate = OIC.DestinationWaitRate 
		, InvoiceDestinationWaitAmount = OIC.DestinationWaitAmount 
		, InvoiceTotalWaitAmount = OIC.TotalWaitAmount
		, InvoiceOrderRejectRate = OIC.OrderRejectRate	-- changed
		, InvoiceOrderRejectRateType = OIC.OrderRejectRateType  -- changed
		, InvoiceOrderRejectAmount = OIC.OrderRejectAmount  -- changed
		, InvoiceChainupRate = OIC.ChainupRate
		, InvoiceChainupRateType = OIC.ChainupRateType
		, InvoiceChainupAmount = OIC.ChainupAmount 
		, InvoiceRerouteRate = OIC.RerouteRate
		, InvoiceRerouteRateType = OIC.RerouteRateType
		, InvoiceRerouteAmount = OIC.RerouteAmount 
		, InvoiceH2SRate = OIC.H2SRate
		, InvoiceH2SRateType = OIC.H2SRateType
		, InvoiceH2SAmount = OIC.H2SAmount
		, InvoiceSplitLoadRate = OIC.SplitLoadRate
		, InvoiceSplitLoadRateType = OIC.SplitLoadRateType
		, InvoiceSplitLoadAmount = OIC.SplitLoadAmount
		, InvoiceTaxRate = OIC.OriginTaxRate
		, InvoiceSettlementUom = OIC.SettlementUom -- changed
		, InvoiceSettlementUomShort = OIC.SettlementUomShort -- changed
		, InvoiceMinSettlementUnits = OIC.MinSettlementUnits
		, InvoiceUnits = OIC.SettlementUnits
		, InvoiceRouteRate = OIC.RouteRate
		, InvoiceRouteRateType = OIC.RouteRateType
		, InvoiceRateSheetRate = OIC.RateSheetRate
		, InvoiceRateSheetRateType = OIC.RateSheetRateType
		, InvoiceFuelSurchargeRate = OIC.FuelSurchargeRate
		, InvoiceFuelSurchargeAmount = OIC.FuelSurchargeAmount
		, InvoiceLoadAmount = OIC.LoadAmount
		, InvoiceTotalAmount = OIC.TotalAmount
	FROM dbo.viewOrderLocalDates O
	LEFT JOIN viewOrderSettlementCarrier OIC ON OIC.OrderID = O.ID
	WHERE O.StatusID NOT IN (-10) -- don't include "Generated" orders (only Assigned+)

GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details + FINANCIAL INFO into a single view
/***********************************/
ALTER VIEW [dbo].[viewOrder_Financial_Shipper] AS 
	SELECT O.* 
		, Shipper = Customer
		, TicketNums = dbo.fnOrderTicketDetails(O.ID, 'TicketNums', '<br/>') 
		, TankNums = dbo.fnOrderTicketDetails(O.ID, 'TankNums', '<br/>') 
		, PreviousDestinations = dbo.fnRerouteDetails(O.ID, 'PreviousDestinations', '<br/>') 
		, RerouteUsers = dbo.fnRerouteDetails(O.ID, 'RerouteUsers', '<br/>') 
		, RerouteDates = dbo.fnRerouteDetails(O.ID, 'RerouteDates', '<br/>') 
		, RerouteNotes = dbo.fnRerouteDetails(O.ID, 'RerouteNotes', '<br/>') 
		, InvoiceRatesAppliedDate = dbo.fnUTC_To_Local(OIC.CreateDateUTC, O.OriginTimeZoneID, O.OriginUseDST)
		, OIC.BatchID
		, InvoiceBatchNum = OIC.BatchNum 
		, InvoiceOriginWaitBillableMinutes = OIC.OriginWaitBillableMinutes
		, InvoiceDestinationWaitBillableMinutes = OIC.DestinationWaitBillableMinutes
		, InvoiceTotalWaitBillableMinutes = isnull(OIC.OriginWaitBillableMinutes, 0) + ISNULL(OIC.DestinationWaitBillableMinutes, 0) 
		, InvoiceWaitFeeParameterID = WaitFeeParameterID
		, InvoiceWaitFeeSubUnit = WaitFeeSubUnit
		, InvoiceWaitFeeRoundingType = WaitFeeRoundingType
		, InvoiceOriginWaitRate = OIC.OriginWaitRate 
		, InvoiceOriginWaitAmount = OIC.OriginWaitAmount 		
		, InvoiceDestinationWaitRate = OIC.DestinationWaitRate 
		, InvoiceDestinationWaitAmount = OIC.DestinationWaitAmount 
		, InvoiceTotalWaitAmount = OIC.TotalWaitAmount
		, InvoiceOrderRejectRate = OIC.OrderRejectRate  -- changed
		, InvoiceOrderRejectRateType = OIC.OrderRejectRateType -- changed
		, InvoiceOrderRejectAmount = OIC.OrderRejectAmount  -- changed
		, InvoiceChainupRate = OIC.ChainupRate
		, InvoiceChainupRateType = OIC.ChainupRateType
		, InvoiceChainupAmount = OIC.ChainupAmount 
		, InvoiceRerouteRate = OIC.RerouteRate
		, InvoiceRerouteRateType = OIC.RerouteRateType
		, InvoiceRerouteAmount = OIC.RerouteAmount 
		, InvoiceH2SRate = OIC.H2SRate
		, InvoiceH2SRateType = OIC.H2SRateType
		, InvoiceH2SAmount = OIC.H2SAmount
		, InvoiceSplitLoadRate = OIC.SplitLoadRate
		, InvoiceSplitLoadRateType = OIC.SplitLoadRateType
		, InvoiceSplitLoadAmount = OIC.SplitLoadAmount
		, InvoiceTaxRate = OIC.OriginTaxRate
		, InvoiceSettlementUom = OIC.SettlementUom -- changed
		, InvoiceSettlementUomShort = OIC.SettlementUomShort -- changed
		, InvoiceMinSettlementUnits = OIC.MinSettlementUnits
		, InvoiceUnits = OIC.SettlementUnits
		, InvoiceRouteRate = OIC.RouteRate
		, InvoiceRouteRateType = OIC.RouteRateType
		, InvoiceRateSheetRate = OIC.RateSheetRate
		, InvoiceRateSheetRateType = OIC.RateSheetRateType
		, InvoiceFuelSurchargeRate = OIC.FuelSurchargeRate
		, InvoiceFuelSurchargeAmount = OIC.FuelSurchargeAmount
		, InvoiceLoadAmount = OIC.LoadAmount
		, InvoiceTotalAmount = OIC.TotalAmount
	FROM dbo.viewOrderLocalDates O
	LEFT JOIN viewOrderSettlementShipper OIC ON OIC.OrderID = O.ID
	WHERE O.StatusID NOT IN (-10) -- don't include "Generated" orders (only Assigned+)
GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Carrier "Settlement" $$ values to an Delivered/Audited order
/***********************************/
ALTER PROCEDURE [dbo].[spApplyRatesCarrier]
(
  @ID int
, @UserName varchar(100)
, @OriginWaitAmount money = NULL
, @DestWaitAmount money = NULL
, @RejectionAmount money = NULL
, @FuelSurchargeAmount money = NULL
, @LoadAmount money = NULL
, @AssessorialRateTypeID_CSV varchar(max) = NULL
, @AssessorialAmount_CSV varchar(max) = null
, @ResetOverrides bit = 0
) AS BEGIN

	SET NOCOUNT ON
	
	-- ensure this order hasn't yet been fully settled
	IF EXISTS (SELECT * FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND BatchID IS NOT NULL)
	BEGIN
		RAISERROR('Invoice has already been fully settled', 16, 1)
		RETURN
	END

	-- ensure that Shipper Rates have been applied prior to applying Carrier rates (since they could be dependent on Shipper rates)
	IF NOT EXISTS (SELECT * FROM tblOrderSettlementShipper WHERE OrderID = @ID)
		EXEC spApplyRatesShipper @ID, @UserName

	/*
drop table #i
drop table #ia

declare @id int, @userName varchar(100)
, @OriginWaitAmount money 
, @DestWaitAmount money 
, @RejectionAmount money 
, @FuelSurchargeAmount money 
, @LoadAmount money 
, @AssessorialRateTypeID_CSV varchar(max) 
, @AssessorialAmount_CSV varchar(max) 
, @ResetOverrides bit 
select @ID=62210,@UserName='kalons'
--select @OriginWaitAmount=1.0000,@DestWaitAmount=2.0000,@RejectionAmount=3.0000,@FuelSurchargeAmount=4.0000,@LoadAmount=5.0000,@AssessorialRateTypeID_CSV='1',@AssessorialAmount_CSV='99'
select @ResetOverrides=0
--	*/
	-- get the provided manual Assessorial Rates (if any) into a usable able
	DECLARE @ManualAssessorialRates TABLE (ID int primary key, Amount money)
	INSERT INTO @ManualAssessorialRates (ID, Amount) 
		SELECT ID.ID, R.Amount
		FROM (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), ID FROM dbo.fnSplitCSVIDs(@AssessorialRateTypeID_CSV)) ID
		JOIN (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), amount = CAST(value as money) FROM dbo.fnSplitCSV(@AssessorialAmount_CSV)) R ON R.sortid = ID.sortid

	-- add in an existing manual Assessorial Rates from the existing Order Settlement (if any)
	INSERT INTO @ManualAssessorialRates (ID, Amount)
		SELECT AssessorialRateTypeID, Amount 
		FROM dbo.tblOrderSettlementCarrierAssessorialCharge 
		WHERE OrderID = @ID AND AssessorialRateID IS NULL 
			AND ID NOT IN (SELECT ID FROM @ManualAssessorialRates)
			AND @ResetOverrides = 0
--select * from @ManualAssessorialRates

	IF (@ResetOverrides = 0)
	BEGIN 
		SELECT @OriginWaitAmount = OriginWaitAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @OriginWaitAmount IS NULL AND OriginWaitRateID IS NULL
		SELECT @DestWaitAmount = DestinationWaitAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @DestWaitAmount IS NULL AND DestinationWaitRateID IS NULL
		SELECT @RejectionAmount = OrderRejectAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @RejectionAmount IS NULL AND OrderRejectRateID IS NULL
		SELECT @FuelSurchargeAmount = FuelSurchargeAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @FuelSurchargeAmount IS NULL AND FuelSurchargeRateID IS NULL
		SELECT @LoadAmount = LoadAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @LoadAmount IS NULL AND RouteRateID IS NULL AND RangeRateID IS NULL
	END
--select OverrideOriginWaitAmount = @OriginWaitAmount, OverrideDestWaitAmount = @DestWaitAmount, OverrideRejectionAmount = @RejectionAmount, OverrideFuelSurchargeAmount = @FuelSurchargeAmount, OverrideLoadAmount = @LoadAmount

	DECLARE @SettlementUnits TABLE
	(
		OrderID int
	  , SettlementUomID int
	  , SettlementFactorID int
	  , MinSettlementUnits decimal(18, 10)
	  , ActualUnits decimal(18, 10)
	  , SettlementUnits decimal(18, 10)
	)
	INSERT INTO @SettlementUnits (OrderID, SettlementUomID, SettlementFactorID, MinSettlementUnits, ActualUnits, SettlementUnits)
		SELECT OrderID, SettlementUomID, SettlementFactorID, MinSettlementUnits, ActualUnits, SettlementUnits
		FROM dbo.viewOrderSettlementUnitsCarrier 
		WHERE OrderID = @ID

	SELECT OrderID = @ID
		, OrderDate
		, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
		, RouteRateID, RangeRateID, LoadAmount
		, WaitFeeParameterID
		, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount 
		, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount
		, OrderRejectRateID, OrderRejectAmount
		, FuelSurchargeRateID, FuelSurchargeRate, FuelSurchargeAmount
		, OriginTaxRate
		, TotalAmount = ISNULL(LoadAmount, 0) 
			+ ISNULL(OriginWaitAmount, 0) 
			+ ISNULL(DestWaitAmount, 0) 
			+ ISNULL(OrderRejectAmount, 0) 
			+ ISNULL(FuelSurchargeAmount, 0)
		, CreatedByUser = @UserName
	INTO #I
	FROM (
		SELECT OrderDate
			, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
			, RouteRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RouteRateID ELSE NULL END
			, RangeRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RangeRateID ELSE NULL END
			, LoadAmount = ISNULL(@LoadAmount, RR.Amount)
			, WaitFeeParameterID
			, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount
			, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount
			, OrderRejectRateID = CASE WHEN @RejectionAmount IS NULL THEN RD.RateID ELSE NULL END
			, OrderRejectAmount = isnull(@RejectionAmount, RD.Amount)
			, FuelSurchargeRateID = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.RateID ELSE NULL END
			, FuelSurchargeRate = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.Rate ELSE NULL END
			, FuelSurchargeAmount = isnull(@FuelSurchargeAmount, FSR.Amount)
			, OriginTaxRate
		FROM (
			SELECT O.ID
				, O.OriginID
				, O.CarrierID
				, O.RouteID
				, SU.SettlementFactorID
				, SU.SettlementUomID
				, SU.ActualUnits
				, SU.MinSettlementUnits
				, SU.SettlementUnits
				, RouteMiles = O.ActualMiles
				, O.OrderDate
				, O.ChainUp
				, O.H2S
				, O.Rejected
				, O.RerouteCount
				, IsSplit = CASE WHEN O.Rejected = 0 AND O.TicketCount > 1 THEN 1 ELSE 0 END
				, WaitFeeParameterID = WFP.ID
				, OriginWaitRateID = CASE WHEN @OriginWaitAmount IS NULL THEN OWD.RateID ELSE NULL END
				, OriginWaitBillableMinutes = OWD.BillableMinutes
				, OriginWaitBillableHours = OWD.BillableHours
				, OriginWaitAmount = isnull(@OriginWaitAmount, OWD.Amount)
				, DestWaitRateID = CASE WHEN @DestWaitAmount IS NULL THEN DWD.RateID ELSE NULL END
				, DestWaitBillableMinutes = DWD.BillableMinutes
				, DestWaitBillableHours = DWD.BillableHours
				, DestWaitAmount = isnull(@DestWaitAmount, DWD.Amount)
				, OriginTaxRate = OO.TaxRate
			FROM dbo.viewOrder O
			JOIN @SettlementUnits SU ON SU.OrderID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			OUTER APPLY dbo.fnOrderCarrierOriginWaitData(@ID) OWD 
			OUTER APPLY dbo.fnOrderCarrierDestinationWaitData(@ID) DWD 
			OUTER APPLY dbo.fnOrderCarrierWaitFeeParameter(@ID) WFP
			WHERE O.ID = @ID
		) X
		OUTER APPLY dbo.fnOrderCarrierLoadAmount(@ID, SettlementUnits, SettlementUomID) RR 
		OUTER APPLY dbo.fnOrderCarrierOrderRejectData(@ID, SettlementUnits, SettlementUomID) RD
		OUTER APPLY dbo.fnOrderCarrierFuelSurchargeData(@ID) FSR
	) X2
--select * from #I
select * from #i
	SELECT OrderID = @ID, AssessorialRateTypeID, AssessorialRateID = RateID, Amount, CreatedByUser = @UserName 
	INTO #IA
	FROM @SettlementUnits SU
	CROSS APPLY dbo.fnOrderCarrierAssessorialAmounts(@ID, SU.SettlementUnits, SU.SettlementUomID) CAA
	WHERE CAA.AssessorialRateTypeID NOT IN (SELECT ID FROM @ManualAssessorialRates)
/*
select OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser from #IA
union
select @ID, ID, NULL, Amount, @UserName from @ManualAssessorialRates
--*/
	DECLARE @CreatedTran bit; SET @CreatedTran = CASE WHEN @@TRANCOUNT = 0 THEN 1 ELSE 0 END
	BEGIN TRY
		IF (@CreatedTran = 1)
			BEGIN TRAN

		-- remove the existing settlment record (if any)
		DELETE FROM tblOrderSettlementCarrierAssessorialCharge WHERE OrderID = @ID
		DELETE FROM tblOrderSettlementCarrier WHERE OrderID = @ID
	
		-- all Units and Rates are first normalized to the Order.OriginUOM then consistent processing
		-- and persisted in the Invoice (Settlement) record in this Order.OriginUOM 
		INSERT INTO tblOrderSettlementCarrier (
			OrderID
			, OrderDate
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestinationWaitRateID 
			, DestinationWaitBillableMinutes 
			, DestinationWaitBillableHours
			, DestinationWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser)
		SELECT OrderID
			, OrderDate
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestWaitRateID 
			, DestWaitBillableMinutes 
			, DestWaitBillableHours
			, DestWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser
		FROM #I

		INSERT INTO tblOrderSettlementCarrierAssessorialCharge (OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser)
			SELECT OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser FROM #IA
			UNION
			SELECT @ID, ID, NULL, Amount, @UserName FROM @ManualAssessorialRates
	
		IF (@CreatedTran = 1)
			COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @msg varchar(max), @severity int
		SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY()
		ROLLBACK
		RAISERROR(@msg, @severity, 1)
		RETURN
	END CATCH	
	
END

GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Shipper "Settlement" $$ values to an Delivered/Audited order
/***********************************/
ALTER PROCEDURE [dbo].[spApplyRatesShipper]
(
  @ID int
, @UserName varchar(100)
, @OriginWaitAmount money = NULL
, @DestWaitAmount money = NULL
, @RejectionAmount money = NULL
, @FuelSurchargeAmount money = NULL
, @LoadAmount money = NULL
, @AssessorialRateTypeID_CSV varchar(max) = NULL
, @AssessorialAmount_CSV varchar(max) = null
, @ResetOverrides bit = 0
) AS BEGIN

	SET NOCOUNT ON
	
	-- ensure this order hasn't yet been fully settled
	IF EXISTS (SELECT * FROM tblOrderSettlementShipper WHERE OrderID = @ID AND BatchID IS NOT NULL)
	BEGIN
		RAISERROR('Invoice has already been fully settled', 16, 1)
		RETURN
	END

	-- get the provided manual Assessorial Rates (if any) into a usable able
	DECLARE @ManualAssessorialRates TABLE (ID int primary key, Amount money)
	INSERT INTO @ManualAssessorialRates (ID, Amount) 
		SELECT ID.ID, R.Amount
		FROM (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), ID FROM dbo.fnSplitCSVIDs(@AssessorialRateTypeID_CSV)) ID
		JOIN (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), amount = CAST(value as money) FROM dbo.fnSplitCSV(@AssessorialAmount_CSV)) R ON R.sortid = ID.sortid

	-- add in an existing manual Assessorial Rates from the existing Order Settlement (if any)
	INSERT INTO @ManualAssessorialRates (ID, Amount)
		SELECT AssessorialRateTypeID, Amount 
		FROM dbo.tblOrderSettlementShipperAssessorialCharge 
		WHERE OrderID = @ID AND AssessorialRateID IS NULL 
			AND ID NOT IN (SELECT ID FROM @ManualAssessorialRates)
			AND @ResetOverrides = 0

	IF (@ResetOverrides = 0)
	BEGIN 
		SELECT @OriginWaitAmount = OriginWaitAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @OriginWaitAmount IS NULL AND OriginWaitRateID IS NULL
		SELECT @DestWaitAmount = DestinationWaitAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @DestWaitAmount IS NULL AND DestinationWaitRateID IS NULL
		SELECT @RejectionAmount = OrderRejectAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @RejectionAmount IS NULL AND OrderRejectRateID IS NULL
		SELECT @FuelSurchargeAmount = FuelSurchargeAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @FuelSurchargeAmount IS NULL AND FuelSurchargeRateID IS NULL
		SELECT @LoadAmount = LoadAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @LoadAmount IS NULL AND RouteRateID IS NULL AND RangeRateID IS NULL
	END

	DECLARE @SettlementUnits TABLE
	(
		OrderID int
	  , SettlementUomID int
	  , SettlementFactorID int
	  , MinSettlementUnits decimal(18, 10)
	  , ActualUnits decimal(18, 10)
	  , SettlementUnits decimal(18, 10)
	)
	INSERT INTO @SettlementUnits (OrderID, SettlementUomID, SettlementFactorID, MinSettlementUnits, ActualUnits, SettlementUnits)
		SELECT OrderID, SettlementUomID, SettlementFactorID, MinSettlementUnits, ActualUnits, SettlementUnits
		FROM dbo.viewOrderSettlementUnitsShipper 
		WHERE OrderID = @ID

	SELECT OrderID = @ID
		, OrderDate
		, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
		, RouteRateID, RangeRateID, LoadAmount
		, WaitFeeParameterID
		, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount 
		, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount
		, OrderRejectRateID, OrderRejectAmount
		, FuelSurchargeRateID, FuelSurchargeRate, FuelSurchargeAmount
		, OriginTaxRate
		, TotalAmount = ISNULL(LoadAmount, 0) 
			+ ISNULL(OriginWaitAmount, 0) 
			+ ISNULL(DestWaitAmount, 0) 
			+ ISNULL(OrderRejectAmount, 0) 
			+ ISNULL(FuelSurchargeAmount, 0)
		, CreatedByUser = @UserName
	INTO #I
	FROM (
		SELECT OrderDate
			, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
			, RouteRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RouteRateID ELSE NULL END
			, RangeRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RangeRateID ELSE NULL END
			, LoadAmount = ISNULL(@LoadAmount, RR.Amount)
			, WaitFeeParameterID
			, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount
			, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount
			, OrderRejectRateID = CASE WHEN @RejectionAmount IS NULL THEN RD.RateID ELSE NULL END
			, OrderRejectAmount = isnull(@RejectionAmount, RD.Amount)
			, FuelSurchargeRateID = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.RateID ELSE NULL END
			, FuelSurchargeRate = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.Rate ELSE NULL END
			, FuelSurchargeAmount = isnull(@FuelSurchargeAmount, FSR.Amount)
			, OriginTaxRate
		FROM (
			SELECT O.ID
				, O.OriginID
				, O.CustomerID
				, O.RouteID
				, SU.SettlementFactorID
				, SU.SettlementUomID
				, SU.ActualUnits
				, SU.MinSettlementUnits
				, SU.SettlementUnits
				, RouteMiles = O.ActualMiles
				, O.OrderDate
				, O.ChainUp
				, O.H2S
				, O.Rejected
				, O.RerouteCount
				, IsSplit = CASE WHEN O.Rejected = 0 AND O.TicketCount > 1 THEN 1 ELSE 0 END
				, WaitFeeParameterID = WFP.ID
				, OriginWaitRateID = CASE WHEN @OriginWaitAmount IS NULL THEN OWD.RateID ELSE NULL END
				, OriginWaitBillableMinutes = OWD.BillableMinutes
				, OriginWaitBillableHours = OWD.BillableHours
				, OriginWaitAmount = isnull(@OriginWaitAmount, OWD.Amount)
				, DestWaitRateID = CASE WHEN @DestWaitAmount IS NULL THEN DWD.RateID ELSE NULL END
				, DestWaitBillableMinutes = DWD.BillableMinutes
				, DestWaitBillableHours = DWD.BillableHours
				, DestWaitAmount = isnull(@DestWaitAmount, DWD.Amount)
				, OriginTaxRate = OO.TaxRate
			FROM dbo.viewOrder O
			JOIN @SettlementUnits SU ON SU.OrderID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			OUTER APPLY dbo.fnOrderShipperOriginWaitData(@ID) OWD 
			OUTER APPLY dbo.fnOrderShipperDestinationWaitData(@ID) DWD 
			OUTER APPLY dbo.fnOrderShipperWaitFeeParameter(@ID) WFP
			WHERE O.ID = @ID
		) X
		OUTER APPLY dbo.fnOrderShipperLoadAmount(@ID, SettlementUnits, SettlementUomID) RR 
		OUTER APPLY dbo.fnOrderShipperOrderRejectData(@ID, SettlementUnits, SettlementUomID) RD
		OUTER APPLY dbo.fnOrderShipperFuelSurchargeData(@ID) FSR
	) X2

	SELECT OrderID = @ID, AssessorialRateTypeID, AssessorialRateID = RateID, Amount, CreatedByUser = @UserName 
	INTO #IA
	FROM @SettlementUnits SU
	CROSS APPLY dbo.fnOrderShipperAssessorialAmounts(@ID, SU.SettlementUnits, SU.SettlementUomID) CAA
	WHERE CAA.AssessorialRateTypeID NOT IN (SELECT ID FROM @ManualAssessorialRates)

	DECLARE @CreatedTran bit; SET @CreatedTran = CASE WHEN @@TRANCOUNT = 0 THEN 1 ELSE 0 END
	BEGIN TRY
		IF (@CreatedTran = 1)
			BEGIN TRAN

		-- remove the existing settlment record (if any)
		DELETE FROM tblOrderSettlementShipperAssessorialCharge WHERE OrderID = @ID
		DELETE FROM tblOrderSettlementShipper WHERE OrderID = @ID
	
		-- all Units and Rates are first normalized to the Order.OriginUOM then consistent processing
		-- and persisted in the Invoice (Settlement) record in this Order.OriginUOM 
		INSERT INTO tblOrderSettlementShipper (
			OrderID
			, OrderDate
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestinationWaitRateID 
			, DestinationWaitBillableMinutes 
			, DestinationWaitBillableHours
			, DestinationWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser)
		SELECT OrderID
			, OrderDate
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestWaitRateID 
			, DestWaitBillableMinutes 
			, DestWaitBillableHours
			, DestWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser
		FROM #I

		INSERT INTO tblOrderSettlementShipperAssessorialCharge (OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser)
			SELECT OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser FROM #IA
			UNION
			SELECT @ID, ID, NULL, Amount, @UserName FROM @ManualAssessorialRates
	
		IF (@CreatedTran = 1)
			COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @msg varchar(max), @severity int
		SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY()
		ROLLBACK
		RAISERROR(@msg, @severity, 1)
		RETURN
	END CATCH	
	
END

GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RateSheetRangeRate info for the specified criteria
/***********************************/
ALTER FUNCTION [dbo].[fnCarrierRateSheetRangeRate](@StartDate date, @EndDate date, @RouteMiles int, @ShipperID int, @CarrierID int, @ProductGroupID int, @OriginStateID int, @DestStateID int, @RegionID int, @BestMatchOnly bit = 0)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT RateSheetID = R.ID, RangeRateID = RR.ID
			-- the manually added .01 is normally added via the fnRateRanking routine
			, Ranking = CASE WHEN @RouteMiles BETWEEN RR.MinRange AND RR.MaxRange THEN 64.01 ELSE 0 END
				+ dbo.fnRateRanking(@ShipperID, R.ShipperID, 32, 0)
				+ dbo.fnRateRanking(@CarrierID, R.CarrierID, 16, 0)
				+ dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 8, 0)
				+ dbo.fnRateRanking(@OriginStateID, R.OriginStateID, 4, 0)
				+ dbo.fnRateRanking(@DestStateID, R.DestStateID, 2, 0)
				+ dbo.fnRateRanking(@RegionID, R.RegionID, 1, 0)
		FROM dbo.viewCarrierRateSheet R
		JOIN dbo.tblCarrierRangeRate RR ON RR.RateSheetID = R.ID AND isnull(@RouteMiles, RR.MinRange) BETWEEN RR.MinRange AND RR.MaxRange
		WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@OriginStateID, 0), R.OriginStateID, 0) = coalesce(OriginStateID, nullif(@OriginStateID, 0), 0)
		  AND coalesce(nullif(@DestStateID, 0), R.DestStateID, 0) = coalesce(DestStateID, nullif(@DestStateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	)
	
	SELECT RR.ID, RateSheetID = R.ID, ShipperID, CarrierID, ProductGroupID, OriginStateID, DestStateID, RegionID, Rate, MinRange, MaxRange, RateTypeID, UomID, EffectiveDate, EndDate, NextEffectiveDate
	  , BestMatch, Ranking
	  , Locked, R.CreateDateUTC, R.CreatedByUser, R.LastChangeDateUTC, R.LastChangedByUser 
	FROM dbo.viewCarrierRateSheet R
	JOIN dbo.tblCarrierRangeRate RR ON RR.RateSheetID = R.ID
	JOIN (
		SELECT RateSheetID, RangeRateID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 7  -- ensure some type of match occurred on all 6 criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.RateSheetID = R.ID AND X.RangeRateID = RR.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)
GO
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper RateSheetRangeRate info for the specified criteria
/***********************************/
ALTER FUNCTION [dbo].[fnShipperRateSheetRangeRate](@StartDate date, @EndDate date, @RouteMiles int, @ShipperID int, @ProductGroupID int, @OriginStateID int, @DestStateID int, @RegionID int, @BestMatchOnly bit = 0)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT RateSheetID = R.ID, RangeRateID = RR.ID
			-- the manually added .01 is normally added via the fnRateRanking routine
			, Ranking = CASE WHEN @RouteMiles BETWEEN RR.MinRange AND RR.MaxRange THEN 32.01 ELSE 0 END
				+ dbo.fnRateRanking(@ShipperID, R.ShipperID, 16, 0)
				+ dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 8, 0)
				+ dbo.fnRateRanking(@OriginStateID, R.OriginStateID, 4, 0)
				+ dbo.fnRateRanking(@DestStateID, R.DestStateID, 2, 0)
				+ dbo.fnRateRanking(@RegionID, R.RegionID, 1, 0)
		FROM dbo.viewShipperRateSheet R
		JOIN dbo.tblShipperRangeRate RR ON RR.RateSheetID = R.ID AND isnull(@RouteMiles, RR.MinRange) BETWEEN RR.MinRange AND RR.MaxRange
		WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@OriginStateID, 0), R.OriginStateID, 0) = coalesce(OriginStateID, nullif(@OriginStateID, 0), 0)
		  AND coalesce(nullif(@DestStateID, 0), R.DestStateID, 0) = coalesce(DestStateID, nullif(@DestStateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	)
	
	SELECT RR.ID, RateSheetID = R.ID, ShipperID, ProductGroupID, OriginStateID, DestStateID, RegionID, Rate, MinRange, MaxRange, RateTypeID, UomID, EffectiveDate, EndDate, NextEffectiveDate
	  , BestMatch, Ranking
	  , Locked, R.CreateDateUTC, R.CreatedByUser, R.LastChangeDateUTC, R.LastChangedByUser 
	FROM dbo.viewShipperRateSheet R
	JOIN dbo.tblShipperRangeRate RR ON RR.RateSheetID = R.ID
	JOIN (
		SELECT RateSheetID, RangeRateID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 6  -- ensure some type of match occurred on all 6 criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.RateSheetID = R.ID AND X.RangeRateID = RR.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)
GO

EXEC _spRebuildAllObjects
GO

COMMIT
GO

/**************************************************************************
-- -Generated by xSQL Schema Compare
-- -Date/Time: Feb 23 2015, 01:11:20 PM

-- -Summary:
    T-SQL script that makes the schema of the database [localhost\sqlexpr2008r2].[DispatchCrude.Badlands] 
    the same as the schema of the database [localhost\sqlexpr2008r2].[DispatchCrude.Dev].

-- -Action:
    Execute this script on [localhost\sqlexpr2008r2].[DispatchCrude.Badlands].

    **** BACKUP the database [localhost\sqlexpr2008r2].[DispatchCrude.Badlands] before running this script ****

-- -Source SQL Server version: 10.50.4033.0
-- -Target SQL Server version: 10.50.4033.0
**************************************************************************/

/**************************************************************************
Drops:
	Table [dbo].[tblCarrierRates] will be dropped.
**************************************************************************/

SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET XACT_ABORT ON
GO


BEGIN TRANSACTION
GO

DROP TRIGGER [dbo].[trigCarrierRates_UI_WaitFee_None]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblOrderSettlementCarrier] DROP CONSTRAINT [FK_OrderSettlementCarrier_WaitFeeParameter]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblOrderSettlementShipper] DROP CONSTRAINT [FK_OrderSettlementShipper_WaitFeeParameter]
GO

DROP PROCEDURE [dbo].[spCloneCarrierAccessorialRates]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblCarrierWaitFeeParameter] DROP CONSTRAINT [DF_CarrierWaitFeeParameter_OriginThresholdMinutes]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblShipperWaitFeeParameter] DROP CONSTRAINT [DF_ShipperWaitFeeParameter_OriginThresholdMinutes]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblCarrierWaitFeeParameter] DROP CONSTRAINT [DF_tblCarrierWaitFeeParameter_DestThresholdMinutes]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblShipperWaitFeeParameter] DROP CONSTRAINT [DF_tblShipperWaitFeeParameter_DestThresholdMinutes]
GO

DROP TABLE [dbo].[tblCarrierRates]
GO

DROP PROCEDURE [dbo].[spCloneCustomerAccessorialRates]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/************************************************
-- Date Created: 13 Nov 2014
-- Author: Kevin Alons
-- Purpose: strip any leading xxx | xxx | ... from caption values
************************************************/
ALTER FUNCTION fnCaptionRoot(@caption varchar(255)) RETURNS varchar(255) AS
BEGIN
	DECLARE @newPos int, @pos int; SELECT @pos = 0, @newPos = 1
	WHILE (@newPos > 0)
	BEGIN
		SET @pos = @newPos + CASE WHEN @pos = 0 THEN 0 ELSE 1 END
		SET @newPos = CHARINDEX('|', @caption, @pos)
	END
	RETURN rtrim(ltrim(substring(@caption, @pos, 255)))
END

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/************************************************
-- Date Created: 18 Jan 2015
-- Author: Kevin Alons
-- Purpose: return combined RateSheet + RangeRate data + friendly translated values
			allow updating of both RateSheet & RangeRate data together
************************************************/
ALTER VIEW [dbo].[viewCarrierRateSheetRangeRate] AS
	SELECT RR.ID, RateSheetID = R.ID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.OriginStateID, R.DestStateID, R.RegionID, RR.Rate, RR.MinRange, RR.MaxRange, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate, R.NextEffectiveDate
		, Shipper = SH.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, OriginState = OS.FullName
		, OriginStateAbbrev = OS.Abbreviation
		, DestinationState = DS.FullName
		, DestinationStateAbbrev = DS.Abbreviation
		, Region = REG.Name
		, RateType = RT.Name
		, Uom = U.Name
		, UomShort = U.Abbrev
		, Locked
		, RR.CreateDateUTC, RR.CreatedByUser
		, RR.LastChangeDateUTC, RR.LastChangedByUser
	FROM dbo.viewCarrierRateSheet R
	JOIN dbo.tblCarrierRangeRate RR ON RR.RateSheetID = R.ID
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblState OS ON OS.ID = R.OriginStateID
	LEFT JOIN tblState DS ON DS.ID = R.DestStateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblRateType RT ON RT.ID = R.RateTypeID
	LEFT JOIN tblUom U ON U.ID = R.UomID

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Carrier "Settlement" $$ values to an Delivered/Audited order
/***********************************/
ALTER VIEW viewOrderSettlementUnitsCarrier AS
	SELECT X2.*
		, SettlementUnits = dbo.fnMaxDecimal(X2.ActualUnits, MinSettlementUnits)
	FROM (
		SELECT OrderID
			, CarrierID
			, SettlementUomID
			, SettlementFactorID
			, MinSettlementUnits = dbo.fnMinDecimal(isnull(X.WRMSVUnits, X.MinSettlementUnits), X.MinSettlementUnits)
			, ActualUnits
		FROM (
			SELECT OrderID = O.ID
				, O.CarrierID
				, SettlementUomID = O.OriginUomID
				, C.SettlementFactorID
				, WRMSVUnits = dbo.fnConvertUOM(R.WRMSVUnits, R.WRMSVUomID, O.OriginUomID)
				, MinSettlementUnits = isnull(dbo.fnConvertUOM(C.MinSettlementUnits, C.MinSettlementUomID, O.OriginUomID), 0) 
				, ActualUnits = CASE C.SettlementFactorID	WHEN 1 THEN O.OriginGrossUnits 
															WHEN 3 THEN isnull(O.OriginGrossStdUnits, O.OriginGrossUnits) 
															ELSE coalesce(O.OriginNetUnits, O.OriginGrossStdUnits, O.OriginGrossUnits, 0) END 
			FROM dbo.tblOrder O
			JOIN dbo.tblRoute R ON R.ID = O.RouteID
			JOIN tblCarrier C ON C.ID = O.CarrierID
		) X
	) X2

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Shipper "Settlement" $$ values to an Delivered/Audited order
/***********************************/
ALTER VIEW viewOrderSettlementUnitsShipper AS
	SELECT X2.*
		, SettlementUnits = dbo.fnMaxDecimal(X2.ActualUnits, MinSettlementUnits)
	FROM (
		SELECT OrderID
			, ShipperID = CustomerID
			, CustomerID
			, SettlementUomID
			, SettlementFactorID
			, MinSettlementUnits = dbo.fnMinDecimal(isnull(X.WRMSVUnits, X.MinSettlementUnits), X.MinSettlementUnits)
			, ActualUnits
		FROM (
			SELECT OrderID = O.ID
				, O.CustomerID
				, SettlementUomID = O.OriginUomID
				, C.SettlementFactorID
				, WRMSVUnits = dbo.fnConvertUOM(R.WRMSVUnits, R.WRMSVUomID, O.OriginUomID)
				, MinSettlementUnits = isnull(dbo.fnConvertUOM(C.MinSettlementUnits, C.MinSettlementUomID, O.OriginUomID), 0) 
				, ActualUnits = CASE C.SettlementFactorID	WHEN 1 THEN O.OriginGrossUnits 
															WHEN 3 THEN isnull(O.OriginGrossStdUnits, O.OriginGrossUnits) 
															ELSE coalesce(O.OriginNetUnits, O.OriginGrossStdUnits, O.OriginGrossUnits, 0) END 
			FROM dbo.tblOrder O
			JOIN dbo.tblRoute R ON R.ID = O.RouteID
			JOIN tblCustomer C ON C.ID = O.CustomerID
		) X
	) X2

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/************************************************
-- Date Created: 13 Nov 2014
-- Author: Kevin Alons
-- Purpose: recursively process the tblReportColumnDefinition table into a "tree" parent-child "table" for use for a tree control
************************************************/
ALTER FUNCTION [dbo].[fnReportColumnTree](@UserName varchar(100)) RETURNS @ret TABLE (
  ID int not null
, Caption varchar(255)
, ParentID int null
) AS 
BEGIN
	DECLARE @data TABLE 
	(
	  ID int
	, Caption varchar(255)
	, sortid int
	, parentid int
	, processed bit not null 
	, parents varchar(255)
	)

	INSERT INTO @data (ID, Caption, sortid, processed)
		SELECT ID, Caption
			--, DataField + char(9) + isnull(DataFormat, '') + char(9) + isnull(FilterDataField, '') + char(9) + isnull(ltrim(FilterTypeID), '') + char(9) + ISNULL(FilterDropDownSql, '')
			, sortid = ROW_NUMBER() over (order by caption), 0
		FROM tblReportColumnDefinition
		WHERE dbo.fnUserInRoles(@UserName, AllowedRoles) = 1 
		ORDER BY caption

	DECLARE @sortid int, @caption varchar(255), @seg varchar(100), @pos int, @parentID int, @parents varchar(255), @id int, @newID int, @newRunID int; SET @newRunID = 0
	WHILE EXISTS (SELECT * FROM @data WHERE processed = 0)
	BEGIN
		SELECT TOP 1 @sortid = sortid, @caption = caption from @data where processed = 0 order by sortid

		SELECT @newID = NULL, @parentID = NULL, @parents = '', @pos = CHARINDEX('|', @caption, 1)
		WHILE @pos > 0
		BEGIN
			SELECT @seg = rtrim(ltrim(SUBSTRING(@caption, 1, @pos - 1)))
			SET @caption = rtrim(ltrim(SUBSTRING(@caption, @pos + 1, 255)))

			SET @parentID = @newID
			SET @parents = isnull(@parents + '|' + LTRIM(@parentID), '')
			
			SET @id = (SELECT id FROM @data WHERE ID < 0 AND Caption = @seg AND parents = @parents)
			IF (@id IS NOT NULL)
				SET @newID = @id
			ELSE BEGIN
				SET @newRunID = @newRunID - 1
				SET @newID = @newRunID
				
				INSERT INTO @data (ID, Caption, sortid, parentid, parents, processed)
					SELECT @newID, @seg, -@sortid, @parentid, @parents, 1
			END
			
			SELECT @pos = CHARINDEX('|', @caption, 1)
		END
		
		UPDATE @data SET Caption = @caption, parentid = @newID, processed = 1 where sortid = @sortid
	END

	INSERT INTO @ret (ID, Caption, ParentID)
		SELECT ID, Caption, ParentID FROM @data ORDER BY ABS(sortid), ABS(id)
	
	RETURN
END


GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper RouteRate info for the specified criteria
/***********************************/
ALTER FUNCTION [dbo].[fnShipperRouteRate](@StartDate date, @EndDate date, @OriginID int, @DestinationID int, @ShipperID int, @ProductGroupID int, @BestMatchOnly bit = 0) 
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT R.ID
			, Ranking =	dbo.fnRateRanking(@ShipperID, R.ShipperID, 2, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 1, 0)
		FROM dbo.viewShipperRouteRate R
		JOIN tblRoute RO ON RO.ID = R.RouteID
		WHERE (nullif(@OriginID, 0) IS NULL OR @OriginID = RO.OriginID)
		  AND (nullif(@DestinationID, 0) IS NULL OR @DestinationID = RO.DestinationID)
		  AND coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	)
	
	SELECT R.ID, RouteID, OriginID, DestinationID, ShipperID, ProductGroupID, Rate, RateTypeID, UomID, EffectiveDate, EndDate, NextEffectiveDate
	  , BestMatch, Ranking
	  , Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewShipperRouteRate R
	JOIN (
		SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 2  -- ensure some type of match occurred on all 6 criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************/
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Order records with "translated friendly" values for FK relationships
/***********************************/
ALTER VIEW [dbo].[viewOrder] AS
SELECT O.*
	, OPS.OrderStatus AS PrintStatus
FROM (
	SELECT O.*
	, Origin = vO.Name 
	, OriginFull = vO.FullName 
	, OriginState = vO.State 
	, OriginStateAbbrev = vO.StateAbbrev 
	, OriginStation = vO.Station
	, OriginCounty = vO.County 
	, vO.LeaseName
	, vO.LeaseNum
	, OriginRegion = vO.Region
	, OriginRegionID = vO.RegionID
	, OriginCountryID = vO.CountryID
	, OriginCountry = vO.Country
	, OriginCountryShort = vO.CountryShort
	, OriginLegalDescription = vO.LegalDescription 
	, OriginNDIC = vO.NDICFileNum 
	, OriginNDM = vO.NDM 
	, OriginCA = vO.CA 
	, OriginTimeZoneID = vO.TimeZoneID 
	, OriginUseDST = vO.UseDST 
	, OriginStateID = vO.StateID
	, vO.H2S
	, Destination = vD.Name 
	, DestinationFull = vD.FullName 
	, DestinationState = vD.State 
	, DestinationStateAbbrev = vD.StateAbbrev 
	, DestinationTypeID = vD.ID
	, vD.DestinationType
	, DestStation = vD.Station 
	, DestTimeZoneID = vD.TimeZoneID 
	, DestUseDST = vD.UseDST 
	, DestStateID = vD.StateID
	, DestRegion = vO.Region
	, DestRegionID = vO.RegionID
	, DestCountryID = vD.CountryID
	, DestCountry = vD.Country
	, DestCountryShort = vD.CountryShort
	, Customer = C.Name 
	, Carrier = CA.Name 
	, CarrierType = CT.Name 
	, OS.OrderStatus
	, OS.StatusNum
	, Driver = D.FullName 
	, DriverFirst = D.FirstName 
	, DriverLast = D.LastName 
	, Truck = TRU.FullName 
	, Trailer = TR1.FullName 
	, Trailer2 = TR2.FullName 
	, P.PriorityNum
	, TicketType = TT.Name 
	, DestTicketTypeID = vD.TicketTypeID 
	, DestTicketType = vD.TicketType 
	, Operator = OP.Name 
	, Producer = PR.Name 
	, Pumper = PU.FullName 
	, DriverNumber = D.IDNumber
	, CarrierNumber = CA.IDNumber
	, CarrierTypeID = CA.CarrierTypeID
	, TruckNumber = TRU.IDNumber 
	, TrailerNumber = TR1.IDNumber 
	, Trailer2Number = TR2.IDNumber 
	, Product = PRO.Name
	, ProductShort = PRO.ShortName 
	, PRO.ProductGroupID
	, PRO.ProductGroup
	, OriginUOM = OUom.Name 
	, OriginUomShort = OUom.Abbrev 
	, OriginTankID_Text = CASE WHEN ORT.TankNum = '*' THEN '*' + isnull(O.OriginTankNum, '?') ELSE ORT.TankNum END 
	, DestUOM = DUom.Name 
	, DestUomShort = DUom.Abbrev 
	, Active = cast((CASE WHEN O.DeleteDateUTC IS NULL THEN 1 ELSE 0 END) as bit) 
	, IsDeleted = cast((CASE WHEN O.DeleteDateUTC IS NOT NULL THEN 1 ELSE 0 END) as bit) 
	, PickupPrintStatus = PPS.Name 
	, PickupCompleted = PPS.IsCompleted 
	, DeliverPrintStatus = DPS.Name 
	, DeliverCompleted = DPS.IsCompleted 
	, PrintStatusID = CASE WHEN StatusID = 8 AND PPS.IsCompleted = 0 THEN 7
						   WHEN StatusID = 3 AND DPS.IsCompleted = 0 THEN 8
						ELSE StatusID END 
	, OriginTankText = CASE WHEN O.OriginTankID IS NULL OR ORT.TankNum = '*' THEN O.OriginTankNum ELSE ORT.TankNum END
	, OriginWaitNum = OWR.Num
	, OriginWaitDesc = OWR.Description
	, OriginWaitNumDesc = OWR.NumDesc
	, DestWaitNum = DWR.Num
	, DestWaitDesc = DWR.Description
	, DestWaitNumDesc = DWR.NumDesc
	, RejectNum = ORR.Num
	, RejectDesc = ORR.Description
	, RejectNumDesc = ORR.NumDesc
	, RerouteCount = (SELECT COUNT(1) FROM tblOrderReroute ORE WHERE ORE.OrderID = O.ID)
	, TicketCount = (SELECT COUNT(1) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND OT.DeleteDateUTC IS NULL)
	, TotalMinutes = isnull(OriginMinutes, 0) + ISNULL(DestMinutes, 0)
	FROM dbo.viewOrderBase O
	LEFT JOIN dbo.viewOrigin vO ON vO.ID = O.OriginID
	LEFT JOIN dbo.viewDestination vD ON vD.ID = O.DestinationID
	LEFT JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
	JOIN dbo.tblOrderStatus AS OS ON OS.ID = O.StatusID
	LEFT JOIN dbo.tblCarrier CA ON CA.ID = O.CarrierID
	LEFT JOIN dbo.tblCarrierType CT ON CT.ID = CA.CarrierTypeID
	LEFT JOIN dbo.viewDriverBase D ON D.ID = O.DriverID
	LEFT JOIN dbo.viewTruck TRU ON TRU.ID = O.TruckID
	LEFT JOIN dbo.viewTrailer TR1 ON TR1.ID = O.TrailerID
	LEFT JOIN dbo.viewTrailer TR2 ON TR2.ID = O.Trailer2ID
	LEFT JOIN dbo.tblPriority P ON P.ID = O.PriorityID
	LEFT JOIN dbo.tblTicketType TT ON TT.ID = O.TicketTypeID
	LEFT JOIN dbo.tblOriginTank ORT ON ORT.ID = O.OriginTankID
	LEFT JOIN dbo.tblOperator OP ON OP.ID = O.OperatorID
	LEFT JOIN dbo.viewPumper PU ON PU.ID = O.PumperID
	LEFT JOIN dbo.tblProducer PR ON PR.ID = O.ProducerID
	LEFT JOIN dbo.viewProduct PRO ON PRO.ID = O.ProductID
	LEFT JOIN dbo.tblUom OUom ON OUom.ID = O.OriginUomID
	LEFT JOIN dbo.tblUom DUom ON DUom.ID = O.DestUomID
	LEFT JOIN dbo.tblPrintStatus PPS ON PPS.ID = O.PickupPrintStatusID
	LEFT JOIN dbo.tblPrintStatus DPS ON DPS.ID = O.DeliverPrintStatusID
	LEFT JOIN dbo.viewOriginWaitReason OWR ON OWR.ID = O.OriginWaitReasonID
	LEFT JOIN dbo.viewDestinationWaitReason DWR ON DWR.ID = O.DestWaitReasonID
	LEFT JOIN dbo.viewOrderRejectReason ORR ON ORR.ID = O.RejectReasonID
) O
LEFT JOIN dbo.tblOrderStatus OPS ON OPS.ID = O.PrintStatusID


GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details + FINANCIAL INFO into a single view
/***********************************/
ALTER VIEW [dbo].[viewOrderSettlementCarrier] AS 
	SELECT OSC.*
		, BatchNum = SB.BatchNum
		, SettlementUom = UOM.Name
		, SettlementUomShort = UOM.Abbrev
		, RouteRate = RR.Rate
		, RouteRateType = RRT.Name
		, RateSheetRate = RSRR.Rate
		, RateSheetRateType = RSRT.Name
		, OrderRejectRate = ORR.Rate
		, OrderRejectRateType = ORRT.Name
		, OriginWaitRate = OWR.Rate
		, DestinationWaitRate = DWR.Rate
		, TotalWaitAmount = cast(isnull(OSC.OriginWaitAmount, 0) as money) + cast(isnull(OSC.DestinationWaitAmount, 0) as money)
		, ChainupRate = CAR.Rate
		, ChainupRateType = CART.Name
		, ChainupAmount = CA.Amount
		, RerouteRate = RAR.Rate
		, RerouteRateType = RART.Name
		, RerouteAmount = RA.Amount
		, SplitLoadRate = SAR.Rate
		, SplitLoadRateType = SART.Name
		, SplitLoadAmount = SA.Amount
		, H2SRate = HAR.Rate
		, H2SRateType = HART.Name
		, H2SAmount = HA.Amount
		, OtherAmount = OA.Amount
		, WaitFeeSubUnit = isnull(WFSU.Name, 'None') 
		, WaitFeeRoundingType = isnull(WFRT.Name, 'None') 
	FROM tblOrderSettlementCarrier OSC 
	LEFT JOIN tblCarrierSettlementBatch SB ON SB.ID = OSC.BatchID
	LEFT JOIN tblUom UOM ON UOM.ID = OSC.SettlementUomID
	LEFT JOIN tblCarrierOriginWaitRate OWR ON OWR.ID = OSC.OriginWaitRateID
	LEFT JOIN tblCarrierDestinationWaitRate DWR ON DWR.ID = OSC.DestinationWaitRateID
	LEFT JOIN tblCarrierOrderRejectRate ORR ON ORR.ID = OSC.OrderRejectRateID
	LEFT JOIN tblRateType ORRT ON ORRT.ID = ORR.RateTypeID
	-- route rate/ratesheet JOINs
	LEFT JOIN tblCarrierRouteRate RR ON RR.ID = OSC.RouteRateID
	LEFT JOIN tblRateType RRT ON RRT.ID = RR.RateTypeID
	LEFT JOIN viewCarrierRateSheetRangeRate RSRR ON RSRR.ID = OSC.RangeRateID
	LEFT JOIN tblRateType RSRT ON RSRT.ID = RSRR.RateTypeID
	-- chainup
	LEFT JOIN tblOrderSettlementCarrierAssessorialCharge CA ON CA.OrderID = OSC.OrderID AND CA.AssessorialRateTypeID = 1
	LEFT JOIN tblCarrierAssessorialRate CAR ON CAR.ID = CA.AssessorialRateID
	LEFT JOIN tblRateType CART ON CART.ID = CAR.RateTypeID
	-- Reroute
	LEFT JOIN tblOrderSettlementCarrierAssessorialCharge RA ON RA.OrderID = OSC.OrderID AND RA.AssessorialRateTypeID = 2
	LEFT JOIN tblCarrierAssessorialRate RAR ON RAR.ID = RA.AssessorialRateID
	LEFT JOIN tblRateType RART ON RART.ID = RAR.RateTypeID
	-- Split Load
	LEFT JOIN tblOrderSettlementCarrierAssessorialCharge SA ON SA.OrderID = OSC.OrderID AND SA.AssessorialRateTypeID = 3
	LEFT JOIN tblCarrierAssessorialRate SAR ON SAR.ID = SA.AssessorialRateID
	LEFT JOIN tblRateType SART ON SART.ID = SAR.RateTypeID
	-- H2S
	LEFT JOIN tblOrderSettlementCarrierAssessorialCharge HA ON HA.OrderID = OSC.OrderID AND HA.AssessorialRateTypeID = 4
	LEFT JOIN tblCarrierAssessorialRate HAR ON HAR.ID = HA.AssessorialRateID
	LEFT JOIN tblRateType HART ON HART.ID = HAR.RateTypeID
	-- Other Assessorial Charges (combined)
	LEFT JOIN (
		SELECT OrderID, Amount = SUM(Amount) FROM tblOrderSettlementCarrierAssessorialCharge WHERE AssessorialRateTypeID NOT IN (1,2,3,4) GROUP BY OrderID
	) OA ON OA.OrderID = OSC.OrderID 
	
	LEFT JOIN tblCarrierWaitFeeParameter WFP ON WFP.ID = OSC.WaitFeeParameterID
	LEFT JOIN dbo.tblWaitFeeSubUnit WFSU ON WFSU.ID = WFP.SubUnitID
	LEFT JOIN dbo.tblWaitFeeRoundingType WFRT ON WFRT.ID = WFP.RoundingTypeID


GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details + FINANCIAL INFO into a single view
/***********************************/
ALTER VIEW [dbo].[viewOrderSettlementShipper] AS 
	SELECT OSC.*
		, BatchNum = SB.BatchNum
		, SettlementUom = UOM.Name
		, SettlementUomShort = UOM.Abbrev
		, RouteRate = RR.Rate
		, RouteRateType = RRT.Name
		, RateSheetRate = RSRR.Rate
		, RateSheetRateType = RSRT.Name
		, OrderRejectRate = ORR.Rate
		, OrderRejectRateType = ORRT.Name
		, OriginWaitRate = OWR.Rate
		, DestinationWaitRate = DWR.Rate
		, TotalWaitAmount = cast(isnull(OSC.OriginWaitAmount, 0) as money) + cast(isnull(OSC.DestinationWaitAmount, 0) as money)
		, ChainupRate = CAR.Rate
		, ChainupRateType = CART.Name
		, ChainupAmount = CA.Amount
		, RerouteRate = RAR.Rate
		, RerouteRateType = RART.Name
		, RerouteAmount = RA.Amount
		, SplitLoadRate = SAR.Rate
		, SplitLoadRateType = SART.Name
		, SplitLoadAmount = SA.Amount
		, H2SRate = HAR.Rate
		, H2SRateType = HART.Name
		, H2SAmount = HA.Amount
		, OtherAmount = OA.Amount
		, WaitFeeSubUnit = isnull(WFSU.Name, 'None') 
		, WaitFeeRoundingType = isnull(WFRT.Name, 'None') 
	FROM tblOrderSettlementShipper OSC 
	LEFT JOIN tblShipperSettlementBatch SB ON SB.ID = OSC.BatchID
	LEFT JOIN tblUom UOM ON UOM.ID = OSC.SettlementUomID
	LEFT JOIN tblShipperOriginWaitRate OWR ON OWR.ID = OSC.OriginWaitRateID
	LEFT JOIN tblShipperDestinationWaitRate DWR ON DWR.ID = OSC.DestinationWaitRateID
	LEFT JOIN tblShipperOrderRejectRate ORR ON ORR.ID = OSC.OrderRejectRateID
	LEFT JOIN tblRateType ORRT ON ORRT.ID = ORR.RateTypeID
	-- route rate/ratesheet JOINs
	LEFT JOIN tblShipperRouteRate RR ON RR.ID = OSC.RouteRateID
	LEFT JOIN tblRateType RRT ON RRT.ID = RR.RateTypeID
	LEFT JOIN viewShipperRateSheetRangeRate RSRR ON RSRR.ID = OSC.RangeRateID
	LEFT JOIN tblRateType RSRT ON RSRT.ID = RSRR.RateTypeID
	-- chainup
	LEFT JOIN tblOrderSettlementShipperAssessorialCharge CA ON CA.OrderID = OSC.OrderID AND CA.AssessorialRateTypeID = 1
	LEFT JOIN tblShipperAssessorialRate CAR ON CAR.ID = CA.AssessorialRateID
	LEFT JOIN tblRateType CART ON CART.ID = CAR.RateTypeID
	-- Reroute
	LEFT JOIN tblOrderSettlementShipperAssessorialCharge RA ON RA.OrderID = OSC.OrderID AND RA.AssessorialRateTypeID = 2
	LEFT JOIN tblShipperAssessorialRate RAR ON RAR.ID = RA.AssessorialRateID
	LEFT JOIN tblRateType RART ON RART.ID = RAR.RateTypeID
	-- Split Load
	LEFT JOIN tblOrderSettlementShipperAssessorialCharge SA ON SA.OrderID = OSC.OrderID AND SA.AssessorialRateTypeID = 3
	LEFT JOIN tblShipperAssessorialRate SAR ON SAR.ID = SA.AssessorialRateID
	LEFT JOIN tblRateType SART ON SART.ID = SAR.RateTypeID
	-- H2S
	LEFT JOIN tblOrderSettlementShipperAssessorialCharge HA ON HA.OrderID = OSC.OrderID AND HA.AssessorialRateTypeID = 4
	LEFT JOIN tblShipperAssessorialRate HAR ON HAR.ID = HA.AssessorialRateID
	LEFT JOIN tblRateType HART ON HART.ID = HAR.RateTypeID
	-- Other Assessorial Charges (combined)
	LEFT JOIN (
		SELECT OrderID, Amount = SUM(Amount) FROM tblOrderSettlementShipperAssessorialCharge WHERE AssessorialRateTypeID NOT IN (1,2,3,4) GROUP BY OrderID
	) OA ON OA.OrderID = OSC.OrderID 
	
	LEFT JOIN tblShipperWaitFeeParameter WFP ON WFP.ID = OSC.WaitFeeParameterID
	LEFT JOIN dbo.tblWaitFeeSubUnit WFSU ON WFSU.ID = WFP.SubUnitID
	LEFT JOIN dbo.tblWaitFeeRoundingType WFRT ON WFRT.ID = WFP.RoundingTypeID


GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/***********************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper OrderRejectRate rows for the specified criteria
/***********************************/
ALTER FUNCTION [dbo].[fnShipperOrderRejectRatesDisplay]
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @ProductGroupID int
, @OriginID int
, @StateID int
, @RegionID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ReasonID, R.ShipperID, R.ProductGroupID, R.OriginID, R.StateID, R.RegionID, R.Rate, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate, R.NextEffectiveDate
		, Shipper = C.Name
		, ProductGroup = PG.Name
		, Origin = D.Name
		, OriginFull = D.FullName
		, State = S.FullName
		, StateAbbrev = S.Abbreviation
		, Region = REG.Name
		, Reason = REA.Description
		, RateType = RT.Name
		, Uom = U.Name
		, UomShort = U.Abbrev
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
	FROM dbo.fnShipperOrderRejectRate(@StartDate, @EndDate, @ReasonID, @ShipperID, @ProductGroupID, @OriginID, @StateID, @RegionID, 0) R
	LEFT JOIN tblCustomer C ON C.ID = R.ShipperID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN viewOrigin D ON D.ID = R.OriginID
	LEFT JOIN tblState S ON S.ID = R.StateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblOrderRejectReason REA ON REA.ID = R.ReasonID
	LEFT JOIN tblRateType RT ON RT.ID = R.RateTypeID
	LEFT JOIN tblUom U ON U.ID = R.UomID
	ORDER BY EffectiveDate

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper RateSheetRangeRate info for the specified criteria
/***********************************/
ALTER FUNCTION fnShipperRateSheetRangeRatesDisplay(@StartDate date, @EndDate date, @RouteMiles int, @ShipperID int, @ProductGroupID int, @OriginStateID int, @DestStateID int, @RegionID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.RateSheetID, R.ShipperID, R.ProductGroupID, R.OriginStateID, R.DestStateID, R.RegionID, R.MinRange, R.MaxRange, R.Rate, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate, R.NextEffectiveDate
		, Shipper = S.Name
		, ProductGroup = PG.Name
		, OriginState = OS.FullName
		, OriginStateAbbrev = OS.Abbreviation
		, DestState = OS.FullName
		, DestStateAbbrev = OS.Abbreviation
		, Region = REG.Name
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
		, Ranking
	FROM dbo.fnShipperRateSheetRangeRate(@StartDate, @EndDate, @RouteMiles, @ShipperID, @ProductGroupID, @OriginStateID, @DestStateID, @RegionID, 0) R
	LEFT JOIN tblCustomer S ON S.ID = R.ShipperID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblState OS ON OS.ID = R.OriginStateID
	LEFT JOIN tblState DS ON DS.ID = R.DestStateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	ORDER BY EffectiveDate

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper RouteRate rows for the specified criteria
/***********************************/
ALTER FUNCTION [dbo].[fnShipperRouteRatesDisplay](@StartDate date, @EndDate date, @OriginID int, @DestinationID int, @ShipperID int, @ProductGroupID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.RouteID, RO.ActualMiles, R.OriginID, R.DestinationID, R.ShipperID, R.ProductGroupID, R.Rate, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate, R.NextEffectiveDate
		, Origin = O.Name
		, OriginFull = O.FullName
		, Destination = D.Name
		, DestinationFull = D.FullName
		, Shipper = C.Name
		, ProductGroup = PG.Name
		, RateType = RT.Name
		, Uom = U.Name
		, UomShort = U.Abbrev
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch, Ranking
	FROM dbo.fnShipperRouteRate(@StartDate, @EndDate, @OriginID, @DestinationID, @ShipperID, @ProductGroupID, 0) R
	JOIN tblRoute RO ON RO.ID = R.RouteID
	JOIN viewOrigin O ON O.ID = R.OriginID
	JOIN viewDestination D ON D.ID = R.DestinationID
	LEFT JOIN tblCustomer C ON C.ID = R.ShipperID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblRateType RT ON RT.ID = R.RateTypeID
	LEFT JOIN tblUom U ON U.ID = R.UomID
	ORDER BY EffectiveDate

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*************************************************
- Date Created: 16 Feb 2015
- Author: Kevin Alons
- Purpose: return the data used by the Un-Audit MVC page
*************************************************/
CREATE VIEW viewOrderUnaudit AS
	SELECT O.ID, OrderNum, OrderDate=dbo.fnDateMdYY(O.OrderDate), Status = OrderStatus, Rejected, DispatchConfirmNum, Customer, Carrier, Driver
		, Origin, OriginGrossUnits, OriginNetUnits, Destination 
	FROM viewOrder O 
	LEFT JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = O.ID 
	WHERE O.StatusID=4 AND O.DeleteDateUTC IS NULL AND OSS.BatchID IS NULL 

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier WaitFeeParameter info for the specified order
/***********************************/
ALTER FUNCTION [dbo].[fnOrderCarrierWaitFeeParameter](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, SubUnitID, RoundingTypeID, OriginThresholdMinutes, DestThresholdMinutes
	FROM dbo.viewOrder O
	CROSS APPLY dbo.fnCarrierWaitFeeParameter(isnull(O.OrderDate, O.DueDate), null, O.CustomerID, O.CarrierID, O.ProductGroupID, O.OriginStateID, O.DestStateID, O.OriginRegionID, 1) R
	WHERE O.ID = @ID 

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper RouteRate info for the specified order
/***********************************/
ALTER FUNCTION [dbo].[fnOrderShipperRouteRate](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, RateTypeID, Rate, R.UomID
	FROM dbo.viewOrder O
	CROSS APPLY dbo.fnShipperRouteRate(O.OrderDate, null, O.OriginID, O.DestinationID, O.CustomerID, O.ProductGroupID, 1) R
	WHERE O.ID = @ID 

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details into a single view
/***********************************/
ALTER VIEW [dbo].[viewOrderExportFull_Reroute] AS 
	SELECT O.* 
		, dbo.fnRerouteDetails(O.ID, 'PreviousDestinations', '<br/>') AS PreviousDestinations
		, dbo.fnRerouteDetails(O.ID, 'RerouteUsers', '<br/>') AS RerouteUsers
		, dbo.fnRerouteDetails(O.ID, 'RerouteDates', '<br/>') AS RerouteDates
		, dbo.fnRerouteDetails(O.ID, 'RerouteNotes', '<br/>') AS RerouteNotes
	FROM dbo.viewOrderLocalDates O
	WHERE O.StatusID NOT IN (-10) -- don't include "Generated" orders (only Assigned+)

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details + FINANCIAL INFO into a single view
/***********************************/
ALTER VIEW [dbo].[viewOrder_Financial_Carrier] AS 
	SELECT O.* 
		, TicketNums = dbo.fnOrderTicketDetails(O.ID, 'TicketNums', '<br/>') 
		, TankNums = dbo.fnOrderTicketDetails(O.ID, 'TankNums', '<br/>') 
		, PreviousDestinations = dbo.fnRerouteDetails(O.ID, 'PreviousDestinations', '<br/>') 
		, RerouteUsers = dbo.fnRerouteDetails(O.ID, 'RerouteUsers', '<br/>') 
		, RerouteDates = dbo.fnRerouteDetails(O.ID, 'RerouteDates', '<br/>') 
		, RerouteNotes = dbo.fnRerouteDetails(O.ID, 'RerouteNotes', '<br/>') 
		, InvoiceRatesAppliedDate = dbo.fnUTC_To_Local(OS.CreateDateUTC, O.OriginTimeZoneID, O.OriginUseDST)
		, OS.BatchID
		, InvoiceBatchNum = OS.BatchNum 
		, InvoiceOriginWaitBillableMinutes = OS.OriginWaitBillableMinutes
		, InvoiceDestinationWaitBillableMinutes = OS.DestinationWaitBillableMinutes
		, InvoiceTotalWaitBillableMinutes = isnull(OS.OriginWaitBillableMinutes, 0) + ISNULL(OS.DestinationWaitBillableMinutes, 0) 
		, InvoiceWaitFeeParameterID = WaitFeeParameterID
		, InvoiceWaitFeeSubUnit = WaitFeeSubUnit
		, InvoiceWaitFeeRoundingType = WaitFeeRoundingType
		, InvoiceOriginWaitRate = OS.OriginWaitRate 
		, InvoiceOriginWaitAmount = OS.OriginWaitAmount 		
		, InvoiceDestinationWaitRate = OS.DestinationWaitRate 
		, InvoiceDestinationWaitAmount = OS.DestinationWaitAmount 
		, InvoiceTotalWaitAmount = OS.TotalWaitAmount
		, InvoiceOrderRejectRate = OS.OrderRejectRate	-- changed
		, InvoiceOrderRejectRateType = OS.OrderRejectRateType  -- changed
		, InvoiceOrderRejectAmount = OS.OrderRejectAmount  -- changed
		, InvoiceChainupRate = OS.ChainupRate
		, InvoiceChainupRateType = OS.ChainupRateType
		, InvoiceChainupAmount = OS.ChainupAmount 
		, InvoiceRerouteRate = OS.RerouteRate
		, InvoiceRerouteRateType = OS.RerouteRateType
		, InvoiceRerouteAmount = OS.RerouteAmount 
		, InvoiceH2SRate = OS.H2SRate
		, InvoiceH2SRateType = OS.H2SRateType
		, InvoiceH2SAmount = OS.H2SAmount
		, InvoiceSplitLoadRate = OS.SplitLoadRate
		, InvoiceSplitLoadRateType = OS.SplitLoadRateType
		, InvoiceSplitLoadAmount = OS.SplitLoadAmount
		, InvoiceOtherAmount = OS.OtherAmount
		, InvoiceTaxRate = OS.OriginTaxRate
		, InvoiceSettlementUom = OS.SettlementUom -- changed
		, InvoiceSettlementUomShort = OS.SettlementUomShort -- changed
		, InvoiceMinSettlementUnits = OS.MinSettlementUnits
		, InvoiceUnits = OS.SettlementUnits
		, InvoiceRouteRate = OS.RouteRate
		, InvoiceRouteRateType = OS.RouteRateType
		, InvoiceRateSheetRate = OS.RateSheetRate
		, InvoiceRateSheetRateType = OS.RateSheetRateType
		, InvoiceFuelSurchargeRate = OS.FuelSurchargeRate
		, InvoiceFuelSurchargeAmount = OS.FuelSurchargeAmount
		, InvoiceLoadAmount = OS.LoadAmount
		, InvoiceTotalAmount = OS.TotalAmount
	FROM dbo.viewOrderLocalDates O
	LEFT JOIN viewOrderSettlementCarrier OS ON OS.OrderID = O.ID
	WHERE O.StatusID NOT IN (-10) -- don't include "Generated" orders (only Assigned+)

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details + FINANCIAL INFO into a single view
/***********************************/
ALTER VIEW [dbo].[viewOrder_Financial_Shipper] AS 
	SELECT O.* 
		, Shipper = Customer
		, TicketNums = dbo.fnOrderTicketDetails(O.ID, 'TicketNums', '<br/>') 
		, TankNums = dbo.fnOrderTicketDetails(O.ID, 'TankNums', '<br/>') 
		, PreviousDestinations = dbo.fnRerouteDetails(O.ID, 'PreviousDestinations', '<br/>') 
		, RerouteUsers = dbo.fnRerouteDetails(O.ID, 'RerouteUsers', '<br/>') 
		, RerouteDates = dbo.fnRerouteDetails(O.ID, 'RerouteDates', '<br/>') 
		, RerouteNotes = dbo.fnRerouteDetails(O.ID, 'RerouteNotes', '<br/>') 
		, InvoiceRatesAppliedDate = dbo.fnUTC_To_Local(OS.CreateDateUTC, O.OriginTimeZoneID, O.OriginUseDST)
		, OS.BatchID
		, InvoiceBatchNum = OS.BatchNum 
		, InvoiceOriginWaitBillableMinutes = OS.OriginWaitBillableMinutes
		, InvoiceDestinationWaitBillableMinutes = OS.DestinationWaitBillableMinutes
		, InvoiceTotalWaitBillableMinutes = isnull(OS.OriginWaitBillableMinutes, 0) + ISNULL(OS.DestinationWaitBillableMinutes, 0) 
		, InvoiceWaitFeeParameterID = WaitFeeParameterID
		, InvoiceWaitFeeSubUnit = WaitFeeSubUnit
		, InvoiceWaitFeeRoundingType = WaitFeeRoundingType
		, InvoiceOriginWaitRate = OS.OriginWaitRate 
		, InvoiceOriginWaitAmount = OS.OriginWaitAmount 		
		, InvoiceDestinationWaitRate = OS.DestinationWaitRate 
		, InvoiceDestinationWaitAmount = OS.DestinationWaitAmount 
		, InvoiceTotalWaitAmount = OS.TotalWaitAmount
		, InvoiceOrderRejectRate = OS.OrderRejectRate  -- changed
		, InvoiceOrderRejectRateType = OS.OrderRejectRateType -- changed
		, InvoiceOrderRejectAmount = OS.OrderRejectAmount  -- changed
		, InvoiceChainupRate = OS.ChainupRate
		, InvoiceChainupRateType = OS.ChainupRateType
		, InvoiceChainupAmount = OS.ChainupAmount 
		, InvoiceRerouteRate = OS.RerouteRate
		, InvoiceRerouteRateType = OS.RerouteRateType
		, InvoiceRerouteAmount = OS.RerouteAmount 
		, InvoiceH2SRate = OS.H2SRate
		, InvoiceH2SRateType = OS.H2SRateType
		, InvoiceH2SAmount = OS.H2SAmount
		, InvoiceSplitLoadRate = OS.SplitLoadRate
		, InvoiceSplitLoadRateType = OS.SplitLoadRateType
		, InvoiceSplitLoadAmount = OS.SplitLoadAmount
		, InvoiceOtherAmount = OS.OtherAmount
		, InvoiceTaxRate = OS.OriginTaxRate
		, InvoiceSettlementUom = OS.SettlementUom -- changed
		, InvoiceSettlementUomShort = OS.SettlementUomShort -- changed
		, InvoiceMinSettlementUnits = OS.MinSettlementUnits
		, InvoiceUnits = OS.SettlementUnits
		, InvoiceRouteRate = OS.RouteRate
		, InvoiceRouteRateType = OS.RouteRateType
		, InvoiceRateSheetRate = OS.RateSheetRate
		, InvoiceRateSheetRateType = OS.RateSheetRateType
		, InvoiceFuelSurchargeRate = OS.FuelSurchargeRate
		, InvoiceFuelSurchargeAmount = OS.FuelSurchargeAmount
		, InvoiceLoadAmount = OS.LoadAmount
		, InvoiceTotalAmount = OS.TotalAmount
	FROM dbo.viewOrderLocalDates O
	LEFT JOIN viewOrderSettlementShipper OS ON OS.OrderID = O.ID
	WHERE O.StatusID NOT IN (-10) -- don't include "Generated" orders (only Assigned+)

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*********************************************
Date Create: 12 Feb 2015
Author: Kevin Alons
Purpose: return a "Mileage Log" from the Order table
*********************************************/
ALTER FUNCTION [dbo].[fnMileageLog](@StartDate date, @EndDate date, @CarrierID int = -1, @DriverID int = -1, @TruckID int = -1)
RETURNS @data TABLE
(
  ID int
  , OrderNum int
  , Carrier varchar(100)
  , Truck varchar(100)
  , Driver text
  , StartTime datetime
  , EndTime datetime
  , StartOdometer int
  , EndOdometer int
  , Mileage int
  , RouteMiles int
  , Type varchar(10)
  , Locked bit
)
AS BEGIN
	INSERT INTO @data
		SELECT ID, OrderNum, Carrier, Truck, Driver
			, StartTime = OriginArriveTime, EndTime = DestDepartTime
			, StartOdometer = OriginTruckMileage, EndOdometer = DestTruckMileage
			, Mileage = DestTruckMileage - OriginTruckMileage
			, RouteMiles = O.ActualMiles
			, Type = 'Loaded'
			, Locked = CASE WHEN O.StatusID = 4 THEN 1 ELSE 0 END
		FROM viewOrderLocalDates O
		WHERE O.OriginArriveTimeUTC IS NOT NULL AND O.DestDepartTimeUTC IS NOT NULL AND O.OriginTruckMileage IS NOT NULL AND O.DestTruckMileage IS NOT NULL
		  AND O.TruckID IS NOT NULL
		  AND (@CarrierID = -1 OR @CarrierID = CarrierID)
		  AND (@DriverID = -1 OR @DriverID = DriverID)
		  AND (@TruckID = -1 OR @TruckID = TruckID)
		  AND O.OrderDate BETWEEN @StartDate and @EndDate

	INSERT INTO @data
		SELECT ID = NULL, OrderNum = NULL, Carrier, Truck, Driver = NULL
			, StartTime = C.EndTime, EndTime = N.StartTime
			, StartOdometer = C.EndOdometer, EndOdometer = N.StartOdometer
			, Mileage = N.StartOdometer - C.EndOdometer
			, RouteMiles = NULL
			, Type = 'Unloaded'
			, Locked = 1
		FROM @data C
		CROSS APPLY (
			SELECT TOP 1 ID, StartTime, StartOdometer FROM @data N WHERE N.StartTime > C.StartTime AND N.Truck = C.Truck ORDER BY StartTime
		) N
	RETURN
END

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier AssessorialRate Amounts info for the specified order
/***********************************/
ALTER FUNCTION [dbo].[fnOrderCarrierAssessorialAmounts](@ID int, @Units decimal(18, 10), @UomID int) RETURNS TABLE AS RETURN
	SELECT AssessorialRateTypeID = TypeID, RateID, RateTypeID, Amount
	FROM (
		SELECT TypeID, RateID = ID, RateTypeID
			, Amount = dbo.fnRateToAmount(RateTypeID, @Units, @UomID, Rate, UomID, OSC.LoadAmount
			, CASE WHEN RateTypeID = 3 THEN (SELECT TOP 1 Amount FROM tblOrderSettlementShipperAssessorialCharge WHERE OrderID = @ID AND AssessorialRateTypeID = TypeID) 
				   ELSE NULL END)
		FROM dbo.fnOrderCarrierAssessorialRates(@ID)
		JOIN tblOrderSettlementCarrier OSC ON OSC.OrderID = @ID
	) X
	WHERE Amount IS NOT NULL

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper DestinationWait data info for the specified order
/***********************************/
ALTER FUNCTION [dbo].[fnOrderShipperDestinationWaitData](@ID int) RETURNS TABLE AS RETURN
	SELECT RateID, BillableMinutes, BillableHours, Amount = BillableHours * Rate
	FROM (
		SELECT RateID, BillableMinutes
			, BillableHours = dbo.fnComputeBillableWaitHours(BillableMinutes, SubUnitID, RoundingTypeID)
			, Rate
		FROM (
			SELECT RateID = OWR.ID
				, Rate
				, BillableMinutes = dbo.fnMaxInt(OWR.Minutes - WFP.DestThresholdMinutes, 0)
				, WFP.SubUnitID, WFP.RoundingTypeID
			FROM dbo.fnOrderShipperDestinationWaitRate(@ID) OWR 
			CROSS JOIN dbo.fnOrderShipperWaitFeeParameter(@ID) WFP 
		) X
	) X2
	WHERE BillableHours > 0.0

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper OriginWait data info for the specified order
/***********************************/
ALTER FUNCTION [dbo].[fnOrderShipperOriginWaitData](@ID int) RETURNS TABLE AS RETURN
	SELECT RateID, BillableMinutes, BillableHours, Amount = BillableHours * Rate, WaitFeeParameterID
	FROM (
		SELECT RateID, BillableMinutes
			, BillableHours = dbo.fnComputeBillableWaitHours(BillableMinutes, SubUnitID, RoundingTypeID)
			, Rate
			, WaitFeeParameterID
		FROM (
			SELECT RateID = OWR.ID
				, Rate
				, BillableMinutes = dbo.fnMaxInt(OWR.Minutes - WFP.OriginThresholdMinutes, 0)
				, WaitFeeParameterID = WFP.ID, WFP.SubUnitID, WFP.RoundingTypeID
			FROM dbo.fnOrderShipperOriginWaitRate(@ID) OWR 
			CROSS APPLY dbo.fnOrderShipperWaitFeeParameter(@ID) WFP
		) X
	) X2
	WHERE BillableHours > 0.0

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Carrier "Settlement" $$ values to an Delivered/Audited order
/***********************************/
ALTER PROCEDURE [dbo].[spApplyRatesCarrier]
(
  @ID int
, @UserName varchar(100)
, @OriginWaitAmount money = NULL
, @DestWaitAmount money = NULL
, @RejectionAmount money = NULL
, @FuelSurchargeAmount money = NULL
, @LoadAmount money = NULL
, @AssessorialRateTypeID_CSV varchar(max) = NULL
, @AssessorialAmount_CSV varchar(max) = null
, @ResetOverrides bit = 0
) AS BEGIN

	SET NOCOUNT ON
	
	-- ensure this order hasn't yet been fully settled
	IF EXISTS (SELECT * FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND BatchID IS NOT NULL)
	BEGIN
		RAISERROR('Invoice has already been fully settled', 16, 1)
		RETURN
	END

	-- ensure that Shipper Rates have been applied prior to applying Carrier rates (since they could be dependent on Shipper rates)
	IF NOT EXISTS (SELECT * FROM tblOrderSettlementShipper WHERE OrderID = @ID)
		EXEC spApplyRatesShipper @ID, @UserName

	/*
drop table #i
drop table #ia

declare @id int, @userName varchar(100)
, @OriginWaitAmount money 
, @DestWaitAmount money 
, @RejectionAmount money 
, @FuelSurchargeAmount money 
, @LoadAmount money 
, @AssessorialRateTypeID_CSV varchar(max) 
, @AssessorialAmount_CSV varchar(max) 
, @ResetOverrides bit 
select @ID=62210,@UserName='kalons'
--select @OriginWaitAmount=1.0000,@DestWaitAmount=2.0000,@RejectionAmount=3.0000,@FuelSurchargeAmount=4.0000,@LoadAmount=5.0000,@AssessorialRateTypeID_CSV='1',@AssessorialAmount_CSV='99'
select @ResetOverrides=0
--	*/
	-- get the provided manual Assessorial Rates (if any) into a usable able
	DECLARE @ManualAssessorialRates TABLE (ID int primary key, Amount money)
	INSERT INTO @ManualAssessorialRates (ID, Amount) 
		SELECT ID.ID, R.Amount
		FROM (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), ID FROM dbo.fnSplitCSVIDs(@AssessorialRateTypeID_CSV)) ID
		JOIN (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), amount = CAST(value as money) FROM dbo.fnSplitCSV(@AssessorialAmount_CSV)) R ON R.sortid = ID.sortid

	-- add in an existing manual Assessorial Rates from the existing Order Settlement (if any)
	INSERT INTO @ManualAssessorialRates (ID, Amount)
		SELECT AssessorialRateTypeID, Amount 
		FROM dbo.tblOrderSettlementCarrierAssessorialCharge 
		WHERE OrderID = @ID AND AssessorialRateID IS NULL 
			AND ID NOT IN (SELECT ID FROM @ManualAssessorialRates)
			AND @ResetOverrides = 0
--select * from @ManualAssessorialRates

	IF (@ResetOverrides = 0)
	BEGIN 
		SELECT @OriginWaitAmount = OriginWaitAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @OriginWaitAmount IS NULL AND OriginWaitRateID IS NULL
		SELECT @DestWaitAmount = DestinationWaitAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @DestWaitAmount IS NULL AND DestinationWaitRateID IS NULL
		SELECT @RejectionAmount = OrderRejectAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @RejectionAmount IS NULL AND OrderRejectRateID IS NULL
		SELECT @FuelSurchargeAmount = FuelSurchargeAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @FuelSurchargeAmount IS NULL AND FuelSurchargeRateID IS NULL
		SELECT @LoadAmount = LoadAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @LoadAmount IS NULL AND RouteRateID IS NULL AND RangeRateID IS NULL
	END
--select OverrideOriginWaitAmount = @OriginWaitAmount, OverrideDestWaitAmount = @DestWaitAmount, OverrideRejectionAmount = @RejectionAmount, OverrideFuelSurchargeAmount = @FuelSurchargeAmount, OverrideLoadAmount = @LoadAmount

	DECLARE @SettlementUnits TABLE
	(
		OrderID int
	  , SettlementUomID int
	  , SettlementFactorID int
	  , MinSettlementUnits decimal(18, 10)
	  , ActualUnits decimal(18, 10)
	  , SettlementUnits decimal(18, 10)
	)
	INSERT INTO @SettlementUnits (OrderID, SettlementUomID, SettlementFactorID, MinSettlementUnits, ActualUnits, SettlementUnits)
		SELECT OrderID, SettlementUomID, SettlementFactorID, MinSettlementUnits, ActualUnits, SettlementUnits
		FROM dbo.viewOrderSettlementUnitsCarrier 
		WHERE OrderID = @ID

	SELECT OrderID = @ID
		, OrderDate
		, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
		, RouteRateID, RangeRateID, LoadAmount
		, WaitFeeParameterID
		, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount 
		, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount
		, OrderRejectRateID, OrderRejectAmount
		, FuelSurchargeRateID, FuelSurchargeRate, FuelSurchargeAmount
		, OriginTaxRate
		, TotalAmount = ISNULL(LoadAmount, 0) 
			+ ISNULL(OriginWaitAmount, 0) 
			+ ISNULL(DestWaitAmount, 0) 
			+ ISNULL(OrderRejectAmount, 0) 
			+ ISNULL(FuelSurchargeAmount, 0)
		, CreatedByUser = @UserName
	INTO #I
	FROM (
		SELECT OrderDate
			, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
			, RouteRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RouteRateID ELSE NULL END
			, RangeRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RangeRateID ELSE NULL END
			, LoadAmount = round(ISNULL(@LoadAmount, RR.Amount), 2)
			, WaitFeeParameterID
			, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount = ROUND(OriginWaitAmount, 2)
			, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount = ROUND(DestWaitAmount, 2)
			, OrderRejectRateID = CASE WHEN @RejectionAmount IS NULL THEN RD.RateID ELSE NULL END
			, OrderRejectAmount = round(isnull(@RejectionAmount, RD.Amount), 2)
			, FuelSurchargeRateID = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.RateID ELSE NULL END
			, FuelSurchargeRate = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.Rate ELSE NULL END
			, FuelSurchargeAmount = round(isnull(@FuelSurchargeAmount, FSR.Amount), 2)
			, OriginTaxRate
		FROM (
			SELECT O.ID
				, O.OriginID
				, O.CarrierID
				, O.RouteID
				, SU.SettlementFactorID
				, SU.SettlementUomID
				, SU.ActualUnits
				, SU.MinSettlementUnits
				, SU.SettlementUnits
				, RouteMiles = O.ActualMiles
				, O.OrderDate
				, O.ChainUp
				, O.H2S
				, O.Rejected
				, O.RerouteCount
				, IsSplit = CASE WHEN O.Rejected = 0 AND O.TicketCount > 1 THEN 1 ELSE 0 END
				, WaitFeeParameterID = WFP.ID
				, OriginWaitRateID = CASE WHEN @OriginWaitAmount IS NULL THEN OWD.RateID ELSE NULL END
				, OriginWaitBillableMinutes = OWD.BillableMinutes
				, OriginWaitBillableHours = OWD.BillableHours
				, OriginWaitAmount = isnull(@OriginWaitAmount, OWD.Amount)
				, DestWaitRateID = CASE WHEN @DestWaitAmount IS NULL THEN DWD.RateID ELSE NULL END
				, DestWaitBillableMinutes = DWD.BillableMinutes
				, DestWaitBillableHours = DWD.BillableHours
				, DestWaitAmount = isnull(@DestWaitAmount, DWD.Amount)
				, OriginTaxRate = OO.TaxRate
			FROM dbo.viewOrder O
			JOIN @SettlementUnits SU ON SU.OrderID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			OUTER APPLY dbo.fnOrderCarrierOriginWaitData(@ID) OWD 
			OUTER APPLY dbo.fnOrderCarrierDestinationWaitData(@ID) DWD 
			OUTER APPLY dbo.fnOrderCarrierWaitFeeParameter(@ID) WFP
			WHERE O.ID = @ID
		) X
		OUTER APPLY dbo.fnOrderCarrierLoadAmount(@ID, SettlementUnits, SettlementUomID) RR 
		OUTER APPLY dbo.fnOrderCarrierOrderRejectData(@ID, SettlementUnits, SettlementUomID) RD
		OUTER APPLY dbo.fnOrderCarrierFuelSurchargeData(@ID) FSR
	) X2
--select * from #I
--select * from #i
	SELECT OrderID = @ID, AssessorialRateTypeID, AssessorialRateID = RateID, Amount = ROUND(Amount, 2), CreatedByUser = @UserName 
	INTO #IA
	FROM @SettlementUnits SU
	CROSS APPLY dbo.fnOrderCarrierAssessorialAmounts(@ID, SU.SettlementUnits, SU.SettlementUomID) CAA
	WHERE CAA.AssessorialRateTypeID NOT IN (SELECT ID FROM @ManualAssessorialRates)
/*
select OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser from #IA
union
select @ID, ID, NULL, Amount, @UserName from @ManualAssessorialRates
--*/
	DECLARE @CreatedTran bit; SET @CreatedTran = CASE WHEN @@TRANCOUNT = 0 THEN 1 ELSE 0 END
	BEGIN TRY
		IF (@CreatedTran = 1)
			BEGIN TRAN

		-- remove the existing settlment record (if any)
		DELETE FROM tblOrderSettlementCarrierAssessorialCharge WHERE OrderID = @ID
		DELETE FROM tblOrderSettlementCarrier WHERE OrderID = @ID
	
		-- all Units and Rates are first normalized to the Order.OriginUOM then consistent processing
		-- and persisted in the Invoice (Settlement) record in this Order.OriginUOM 
		INSERT INTO tblOrderSettlementCarrier (
			OrderID
			, OrderDate
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestinationWaitRateID 
			, DestinationWaitBillableMinutes 
			, DestinationWaitBillableHours
			, DestinationWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser)
		SELECT OrderID
			, OrderDate
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestWaitRateID 
			, DestWaitBillableMinutes 
			, DestWaitBillableHours
			, DestWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser
		FROM #I

		INSERT INTO tblOrderSettlementCarrierAssessorialCharge (OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser)
			SELECT OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser FROM #IA
			UNION
			SELECT @ID, ID, NULL, Amount, @UserName FROM @ManualAssessorialRates
	
		IF (@CreatedTran = 1)
			COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @msg varchar(max), @severity int
		SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY()
		ROLLBACK
		RAISERROR(@msg, @severity, 1)
		RETURN
	END CATCH	
	
END


GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Shipper "Settlement" $$ values to an Delivered/Audited order
/***********************************/
ALTER PROCEDURE [dbo].[spApplyRatesShipper]
(
  @ID int
, @UserName varchar(100)
, @OriginWaitAmount money = NULL
, @DestWaitAmount money = NULL
, @RejectionAmount money = NULL
, @FuelSurchargeAmount money = NULL
, @LoadAmount money = NULL
, @AssessorialRateTypeID_CSV varchar(max) = NULL
, @AssessorialAmount_CSV varchar(max) = null
, @ResetOverrides bit = 0
) AS BEGIN

	SET NOCOUNT ON
	
	-- ensure this order hasn't yet been fully settled
	IF EXISTS (SELECT * FROM tblOrderSettlementShipper WHERE OrderID = @ID AND BatchID IS NOT NULL)
	BEGIN
		RAISERROR('Invoice has already been fully settled', 16, 1)
		RETURN
	END

	-- get the provided manual Assessorial Rates (if any) into a usable able
	DECLARE @ManualAssessorialRates TABLE (ID int primary key, Amount money)
	INSERT INTO @ManualAssessorialRates (ID, Amount) 
		SELECT ID.ID, R.Amount
		FROM (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), ID FROM dbo.fnSplitCSVIDs(@AssessorialRateTypeID_CSV)) ID
		JOIN (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), amount = CAST(value as money) FROM dbo.fnSplitCSV(@AssessorialAmount_CSV)) R ON R.sortid = ID.sortid

	-- add in an existing manual Assessorial Rates from the existing Order Settlement (if any)
	INSERT INTO @ManualAssessorialRates (ID, Amount)
		SELECT AssessorialRateTypeID, Amount 
		FROM dbo.tblOrderSettlementShipperAssessorialCharge 
		WHERE OrderID = @ID AND AssessorialRateID IS NULL 
			AND ID NOT IN (SELECT ID FROM @ManualAssessorialRates)
			AND @ResetOverrides = 0

	IF (@ResetOverrides = 0)
	BEGIN 
		SELECT @OriginWaitAmount = OriginWaitAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @OriginWaitAmount IS NULL AND OriginWaitRateID IS NULL
		SELECT @DestWaitAmount = DestinationWaitAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @DestWaitAmount IS NULL AND DestinationWaitRateID IS NULL
		SELECT @RejectionAmount = OrderRejectAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @RejectionAmount IS NULL AND OrderRejectRateID IS NULL
		SELECT @FuelSurchargeAmount = FuelSurchargeAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @FuelSurchargeAmount IS NULL AND FuelSurchargeRateID IS NULL
		SELECT @LoadAmount = LoadAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @LoadAmount IS NULL AND RouteRateID IS NULL AND RangeRateID IS NULL
	END

	DECLARE @SettlementUnits TABLE
	(
		OrderID int
	  , SettlementUomID int
	  , SettlementFactorID int
	  , MinSettlementUnits decimal(18, 10)
	  , ActualUnits decimal(18, 10)
	  , SettlementUnits decimal(18, 10)
	)
	INSERT INTO @SettlementUnits (OrderID, SettlementUomID, SettlementFactorID, MinSettlementUnits, ActualUnits, SettlementUnits)
		SELECT OrderID, SettlementUomID, SettlementFactorID, MinSettlementUnits, ActualUnits, SettlementUnits
		FROM dbo.viewOrderSettlementUnitsShipper 
		WHERE OrderID = @ID

	SELECT OrderID = @ID
		, OrderDate
		, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
		, RouteRateID, RangeRateID, LoadAmount
		, WaitFeeParameterID
		, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount 
		, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount
		, OrderRejectRateID, OrderRejectAmount
		, FuelSurchargeRateID, FuelSurchargeRate, FuelSurchargeAmount
		, OriginTaxRate
		, TotalAmount = ISNULL(LoadAmount, 0) 
			+ ISNULL(OriginWaitAmount, 0) 
			+ ISNULL(DestWaitAmount, 0) 
			+ ISNULL(OrderRejectAmount, 0) 
			+ ISNULL(FuelSurchargeAmount, 0)
		, CreatedByUser = @UserName
	INTO #I
	FROM (
		SELECT OrderDate
			, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
			, RouteRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RouteRateID ELSE NULL END
			, RangeRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RangeRateID ELSE NULL END
			, LoadAmount = round(ISNULL(@LoadAmount, RR.Amount), 2)
			, WaitFeeParameterID
			, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount = ROUND(OriginWaitAmount, 2)
			, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount = ROUND(DestWaitAmount, 2)
			, OrderRejectRateID = CASE WHEN @RejectionAmount IS NULL THEN RD.RateID ELSE NULL END
			, OrderRejectAmount = round(isnull(@RejectionAmount, RD.Amount), 2)
			, FuelSurchargeRateID = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.RateID ELSE NULL END
			, FuelSurchargeRate = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.Rate ELSE NULL END
			, FuelSurchargeAmount = round(isnull(@FuelSurchargeAmount, FSR.Amount), 2)
			, OriginTaxRate
		FROM (
			SELECT O.ID
				, O.OriginID
				, O.CustomerID
				, O.RouteID
				, SU.SettlementFactorID
				, SU.SettlementUomID
				, SU.ActualUnits
				, SU.MinSettlementUnits
				, SU.SettlementUnits
				, RouteMiles = O.ActualMiles
				, O.OrderDate
				, O.ChainUp
				, O.H2S
				, O.Rejected
				, O.RerouteCount
				, IsSplit = CASE WHEN O.Rejected = 0 AND O.TicketCount > 1 THEN 1 ELSE 0 END
				, WaitFeeParameterID = WFP.ID
				, OriginWaitRateID = CASE WHEN @OriginWaitAmount IS NULL THEN OWD.RateID ELSE NULL END
				, OriginWaitBillableMinutes = OWD.BillableMinutes
				, OriginWaitBillableHours = OWD.BillableHours
				, OriginWaitAmount = isnull(@OriginWaitAmount, OWD.Amount)
				, DestWaitRateID = CASE WHEN @DestWaitAmount IS NULL THEN DWD.RateID ELSE NULL END
				, DestWaitBillableMinutes = DWD.BillableMinutes
				, DestWaitBillableHours = DWD.BillableHours
				, DestWaitAmount = isnull(@DestWaitAmount, DWD.Amount)
				, OriginTaxRate = OO.TaxRate
			FROM dbo.viewOrder O
			JOIN @SettlementUnits SU ON SU.OrderID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			OUTER APPLY dbo.fnOrderShipperOriginWaitData(@ID) OWD 
			OUTER APPLY dbo.fnOrderShipperDestinationWaitData(@ID) DWD 
			OUTER APPLY dbo.fnOrderShipperWaitFeeParameter(@ID) WFP
			WHERE O.ID = @ID
		) X
		OUTER APPLY dbo.fnOrderShipperLoadAmount(@ID, SettlementUnits, SettlementUomID) RR 
		OUTER APPLY dbo.fnOrderShipperOrderRejectData(@ID, SettlementUnits, SettlementUomID) RD
		OUTER APPLY dbo.fnOrderShipperFuelSurchargeData(@ID) FSR
	) X2

	SELECT OrderID = @ID, AssessorialRateTypeID, AssessorialRateID = RateID, Amount = ROUND(Amount, 2), CreatedByUser = @UserName 
	INTO #IA
	FROM @SettlementUnits SU
	CROSS APPLY dbo.fnOrderShipperAssessorialAmounts(@ID, SU.SettlementUnits, SU.SettlementUomID) CAA
	WHERE CAA.AssessorialRateTypeID NOT IN (SELECT ID FROM @ManualAssessorialRates)

	DECLARE @CreatedTran bit; SET @CreatedTran = CASE WHEN @@TRANCOUNT = 0 THEN 1 ELSE 0 END
	BEGIN TRY
		IF (@CreatedTran = 1)
			BEGIN TRAN

		-- remove the existing settlment record (if any)
		DELETE FROM tblOrderSettlementShipperAssessorialCharge WHERE OrderID = @ID
		DELETE FROM tblOrderSettlementShipper WHERE OrderID = @ID
	
		-- all Units and Rates are first normalized to the Order.OriginUOM then consistent processing
		-- and persisted in the Invoice (Settlement) record in this Order.OriginUOM 
		INSERT INTO tblOrderSettlementShipper (
			OrderID
			, OrderDate
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestinationWaitRateID 
			, DestinationWaitBillableMinutes 
			, DestinationWaitBillableHours
			, DestinationWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser)
		SELECT OrderID
			, OrderDate
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestWaitRateID 
			, DestWaitBillableMinutes 
			, DestWaitBillableHours
			, DestWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser
		FROM #I

		INSERT INTO tblOrderSettlementShipperAssessorialCharge (OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser)
			SELECT OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser FROM #IA
			UNION
			SELECT @ID, ID, NULL, Amount, @UserName FROM @ManualAssessorialRates
	
		IF (@CreatedTran = 1)
			COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @msg varchar(max), @severity int
		SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY()
		ROLLBACK
		RAISERROR(@msg, @severity, 1)
		RETURN
	END CATCH	
	
END


GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
/***********************************/
ALTER PROCEDURE [dbo].[spRetrieveOrdersFinancialCarrier]
(
  @StartDate datetime = NULL
, @EndDate datetime = NULL
, @ShipperID int = -1 -- all
, @CarrierID int = -1 -- all
, @ProductGroupID int = -1 -- all 
, @ProducerID int = -1 -- all
, @OriginStateID int = -1 -- all 
, @OriginRegionID int = -1 -- all
, @BatchID int = NULL  -- either show unbatched (NULL), or the specified batch orders
, @OnlyShipperSettled bit = 0 
) AS BEGIN

	SELECT @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	
	SELECT DISTINCT OE.* 
		, ShipperSettled = cast(CASE WHEN OSS.BatchID IS NOT NULL THEN 1 ELSE 0 END as bit)
	FROM dbo.viewOrder_Financial_Carrier OE
	LEFT JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = OE.ID AND OSS.BatchID IS NOT NULL
	WHERE ID IN (
		SELECT O.ID
		FROM viewOrderBase O
		JOIN tblProduct P ON P.ID = O.ProductID
		JOIN tblOrigin OO ON OO.ID = O.OriginID
		LEFT JOIN tblOrderSettlementCarrier OSC ON OSC.OrderID = O.ID
		WHERE O.StatusID IN (4)  
		  AND (@ShipperID=-1 OR O.CustomerID=@ShipperID) 
		  AND (@CarrierID=-1 OR O.CarrierID=@CarrierID) 
		  AND (@ProductGroupID=-1 OR P.ProductGroupID=@ProductGroupID) 
		  AND (@ProducerID=-1 OR O.ProducerID=@ProducerID) 
		  AND (@OriginStateID=-1 OR OO.StateID=@OriginStateID) 
		  AND (@OriginRegionID=-1 OR OO.RegionID=@OriginRegionID) 
		  AND (@StartDate IS NULL OR O.OrderDate >= @StartDate) 
		  AND (@EndDate IS NULL OR O.OrderDate <= @EndDate)
		  AND ((@BatchID IS NULL AND OSC.BatchID IS NULL) OR OSC.BatchID = @BatchID)
	)
	  AND (@OnlyShipperSettled = 0 OR OSS.BatchID IS NOT NULL)
	ORDER BY OE.OriginDepartTimeUTC
END

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
/***********************************/
ALTER PROCEDURE [dbo].[spRetrieveOrdersFinancialShipper]
(
  @StartDate datetime = NULL
, @EndDate datetime = NULL
, @ShipperID int = -1 -- all customers
, @OriginShipperRegion varchar(50) = NULL -- all origin.ShipperRegions
, @ProductGroupID int = -1 -- all product groups
, @ProducerID int = -1 -- all
, @OriginStateID int = -1 -- all 
, @OriginRegionID int = -1 -- all
, @BatchID int = NULL  -- either show unbatched (NULL), or the specified batch orders
) AS BEGIN

	SELECT @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	
	SELECT DISTINCT OE.* 
	FROM viewOrder_Financial_Shipper OE
	WHERE ID IN (
		SELECT O.ID
		FROM viewOrderBase O
		JOIN tblProduct P ON P.ID = O.ProductID
		JOIN tblOrigin OO ON OO.ID = O.OriginID
		LEFT JOIN tblOrderSettlementShipper ISC ON ISC.OrderID = O.ID
		WHERE O.StatusID IN (4)  
		  AND (@ShipperID=-1 OR O.CustomerID=@ShipperID) 
		  AND (@OriginShipperRegion IS NULL OR OriginShipperRegion = @OriginShipperRegion)
		  AND (@ProductGroupID=-1 OR P.ProductGroupID=@ProductGroupID) 
		  AND (@ProducerID=-1 OR O.ProducerID=@ProducerID) 
		  AND (@OriginStateID=-1 OR OO.StateID=@OriginStateID) 
		  AND (@OriginRegionID=-1 OR OO.RegionID=@OriginRegionID) 
		  AND (@StartDate IS NULL OR O.OrderDate >= @StartDate) 
		  AND (@EndDate IS NULL OR O.OrderDate <= @EndDate)
		  AND ((@BatchID IS NULL AND ISC.BatchID IS NULL) OR ISC.BatchID = @BatchID)
	)
	ORDER BY OE.OriginDepartTimeUTC
END 

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*************************************/
-- Date Created: 18 Jan 2015
-- Author: Kevin Alons
-- Purpose: ensure that a RangeRate record is not moved from one RateSheet to another
/*************************************/
ALTER TRIGGER [dbo].[trigCarrierRangeRate_IU] ON [dbo].[tblCarrierRangeRate] AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @error varchar(255)
	IF EXISTS (SELECT i.* FROM inserted i JOIN deleted d ON d.ID = i.ID WHERE i.RateSheetID <> d.RateSheetID)
		SET @error = 'RangeRate records cannot be moved to a different RateSheet'
	
	IF @error IS NOT NULL
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementCarrier SET RangeRateID = NULL, RouteRateID = NULL, LoadAmount = NULL
		WHERE BatchID IS NULL AND RangeRateID IN (SELECT ID FROM inserted)
END

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*************************************/
-- Date Created: 27 Dec 2014
-- Author: Kevin Alons
-- Purpose: ensure the TotalAmount always reflects the sum of the individual amounts (including associated Assessorial charges)
/*************************************/
ALTER TRIGGER [dbo].[trigOrderSettlementShipper_IU] ON [dbo].[tblOrderSettlementShipper] FOR INSERT, UPDATE  AS
BEGIN
	-- Note: the TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU') ) = 0 statement prevents recursion
	IF (TRIGGER_NESTLEVEL( OBJECT_ID('trigOrderSettlementShipper_IU')) = 1
		AND (
			UPDATE(LoadAmount)
			OR UPDATE(OrderRejectAmount)
			OR UPDATE(OriginWaitAmount)
			OR UPDATE(DestinationWaitAmount)
			OR UPDATE(FuelSurchargeAmount)
			OR UPDATE (TotalAmount)
			)
		)
	BEGIN
		UPDATE tblOrderSettlementShipper
			SET TotalAmount = isnull(SC.LoadAmount, 0) 
				+ isnull(SC.OrderRejectAmount, 0) 
				+ isnull(SC.OriginWaitAmount, 0) 
				+ isnull(SC.DestinationWaitAmount, 0)
				+ isnull(SC.FuelSurchargeAmount, 0)
				+ isnull((SELECT SUM(Amount) FROM tblOrderSettlementShipperAssessorialCharge WHERE OrderID = SC.OrderID), 0)
		FROM tblOrderSettlementShipper SC
		JOIN inserted i ON i.OrderID = SC.OrderID 
	END
END

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*************************************/
-- Date Created: 18 Jan 2015
-- Author: Kevin Alons
-- Purpose: ensure that a RangeRate record is not moved from one RateSheet to another
/*************************************/
ALTER TRIGGER [dbo].[trigShipperRangeRate_IU] ON [dbo].[tblShipperRangeRate] AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @error varchar(255)
	IF EXISTS (SELECT i.* FROM inserted i JOIN deleted d ON d.ID = i.ID WHERE i.RateSheetID <> d.RateSheetID)
		SET @error = 'RangeRate records cannot be moved to a different RateSheet'
	
	IF @error IS NOT NULL
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementShipper SET RangeRateID = NULL, RouteRateID = NULL, LoadAmount = NULL
		WHERE BatchID IS NULL AND RangeRateID IN (SELECT ID FROM inserted)
END

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			prevent changes to Locked (in use) records
			un-associate changed rates from rated orders
***********************************************/
ALTER TRIGGER [dbo].[trigCarrierDestinationWaitRate_IU] ON [dbo].[tblCarrierDestinationWaitRate] AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblCarrierDestinationWaitRate X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ReasonID, X.ReasonID) = 1
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(i.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Wait Rates are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewCarrierDestinationWaitRate X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ReasonID, X.ReasonID) = 0
		  OR dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
		  OR dbo.fnCompareNullableInts(d.CarrierID, X.CarrierID) = 0
		  OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
		  OR dbo.fnCompareNullableInts(d.DestinationID, X.DestinationID) = 0
		  OR dbo.fnCompareNullableInts(d.StateID, X.StateID) = 0
		  OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
		  OR d.EffectiveDate <> X.EffectiveDate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	IF @error IS NOT NULL 
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementCarrier SET DestinationWaitRateID = NULL, DestinationWaitAmount = NULL 
		WHERE BatchID IS NULL AND DestinationWaitRateID IN (SELECT ID FROM inserted)
END

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*************************************/
-- Date Created: 19 Feb 2015
-- Author: Kevin Alons
-- Purpose: prevent deletion of Locked records
/*************************************/
ALTER TRIGGER [dbo].[trigCarrierFuelSurchargeRate_IOD] ON [dbo].[tblCarrierFuelSurchargeRate] INSTEAD OF DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewCarrierFuelSurchargeRate X ON X.id = d.ID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('Locked (in use) records cannot be deleted', 16, 1)
		RETURN
	END
	ELSE
	BEGIN
		-- ensure all references to the deleted rates are fully removed
		UPDATE tblOrderSettlementCarrier
		  SET FuelSurchargeRateID = NULL, FuelSurchargeAmount = NULL
		WHERE BatchID IS NULL
		  AND FuelSurchargeRateID IN (SELECT ID FROM deleted)
		-- do the actual update here
		DELETE FROM tblCarrierFuelSurchargeRate WHERE ID IN (SELECT ID FROM deleted)
	END
END

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
-- Date Created: 20 Feb 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			prevent changes to Locked (in use) records
			un-associate changed rates from rated orders
***********************************************/
CREATE TRIGGER trigCarrierFuelSurchargeRate_IU ON tblCarrierFuelSurchargeRate AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblCarrierFuelSurchargeRate X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Fuel Surcharge Rates are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewCarrierFuelSurchargeRate X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
		  OR dbo.fnCompareNullableInts(d.CarrierID, X.CarrierID) = 0
		  OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
		  OR d.EffectiveDate <> X.EffectiveDate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	IF @error IS NOT NULL 
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementCarrier SET FuelSurchargeRateID = NULL, FuelSurchargeAmount = NULL 
		WHERE BatchID IS NULL AND FuelSurchargeRateID IN (SELECT ID FROM inserted)
END

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			prevent changes to Locked (in use) records
***********************************************/
ALTER TRIGGER [dbo].[trigCarrierOrderRejectRate_IU] ON [dbo].[tblCarrierOrderRejectRate] AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblCarrierOrderRejectRate X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ReasonID, X.ReasonID) = 1
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(i.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Reject Rates are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewCarrierOrderRejectRate X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ReasonID, X.ReasonID) = 0
		  OR dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
		  OR dbo.fnCompareNullableInts(d.CarrierID, X.CarrierID) = 0
		  OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
		  OR dbo.fnCompareNullableInts(d.OriginID, X.OriginID) = 0
		  OR dbo.fnCompareNullableInts(d.StateID, X.StateID) = 0
		  OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
		  OR d.EffectiveDate <> X.EffectiveDate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	IF @error IS NOT NULL 
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementCarrier SET OrderRejectRateID = NULL, OrderRejectAmount = NULL 
		WHERE BatchID IS NULL AND OrderRejectRateID IN (SELECT ID FROM inserted)
END

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			prevent changes to Locked (in use) records
***********************************************/
ALTER TRIGGER [dbo].[trigCarrierOriginWaitRate_IU] ON [dbo].[tblCarrierOriginWaitRate] AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblCarrierOriginWaitRate X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ReasonID, X.ReasonID) = 1
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(i.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Wait Rates are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewCarrierOriginWaitRate X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ReasonID, X.ReasonID) = 0
		  OR dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
		  OR dbo.fnCompareNullableInts(d.CarrierID, X.CarrierID) = 0
		  OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
		  OR dbo.fnCompareNullableInts(d.OriginID, X.OriginID) = 0
		  OR dbo.fnCompareNullableInts(d.StateID, X.StateID) = 0
		  OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
		  OR d.EffectiveDate <> X.EffectiveDate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	IF @error IS NOT NULL 
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementCarrier SET OriginWaitRateID = NULL, OriginWaitAmount = NULL 
		WHERE BatchID IS NULL AND OriginWaitRateID IN (SELECT ID FROM inserted)
END

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
/***********************************************/
ALTER TRIGGER [dbo].[trigCarrierRateSheet_IU] ON [dbo].[tblCarrierRateSheet] AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblCarrierRateSheet X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(i.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Rate Sheets are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewCarrierRateSheet X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
			OR dbo.fnCompareNullableInts(d.CarrierID, X.CarrierID) = 0
			OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
			OR dbo.fnCompareNullableInts(d.OriginStateID, X.OriginStateID) = 0
			OR dbo.fnCompareNullableInts(d.DestStateID, X.DestStateID) = 0
			OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
			OR d.EffectiveDate <> X.EffectiveDate 
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	
	IF (@error IS NOT NULL)
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
END

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
/***********************************************/
ALTER TRIGGER [dbo].[trigCarrierRouteRate_IU] ON [dbo].[tblCarrierRouteRate] AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblCarrierRouteRate X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND i.RouteID = X.RouteID
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Route Rates are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewCarrierRouteRate X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
			OR dbo.fnCompareNullableInts(d.CarrierID, X.CarrierID) = 0
			OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
			OR d.RouteID = X.RouteID
			OR d.EffectiveDate <> X.EffectiveDate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	IF (@error IS NOT NULL)
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementCarrier SET RouteRateID = NULL, RangeRateID = NULL, LoadAmount = NULL 
		WHERE BatchID IS NULL AND RouteRateID IN (SELECT ID FROM inserted)
END

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			prevent changes to Locked records (except for EndDate)
***********************************************/
ALTER TRIGGER [dbo].[trigCarrierWaitFeeParameter_IU] ON [dbo].[tblCarrierWaitFeeParameter] AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblCarrierWaitFeeParameter X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(i.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Parameters are not allowed'
	END
	ElSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewCarrierWaitFeeParameter X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
			OR dbo.fnCompareNullableInts(d.CarrierID, X.CarrierID) = 0
			OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
			OR dbo.fnCompareNullableInts(d.OriginStateID, X.OriginStateID) = 0
			OR dbo.fnCompareNullableInts(d.DestStateID, X.DestStateID) = 0
			OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
			OR d.EffectiveDate <> X.EffectiveDate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	
	IF (@error IS NOT NULL)
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementShipper SET WaitFeeParameterID = NULL
		WHERE BatchID IS NULL AND WaitFeeParameterID IN (SELECT ID FROM inserted)

END

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =============================================
-- Author:		Kevin Alons
-- Create date: 19 Dec 2012
-- Description:	trigger to 
				1) validate any changes, and fail the update if invalid changes are submitted
				2) add a unique, incrementing OrderNum to each new Order (manual Identity column)
				3) recompute wait times (origin|destination based on times provided)
				4) generate route table entry for any newly used origin-destination combination
				5) keep Order consistent with Origin.Producer|Operator|UOM & Dest.UOM then the Origin|Dest change
				6) roll the PrintStatus back to NOTFINALIZED (0) when status is rolled back
				7) when DriverID changes, mark those orders AS DELETED and clone the order with the new DriverID and associated records
				8) update any ticket quantities for open orders when the UOM is changed for the Origin
				9) update the Driver.Truck\Trailer\Trailer2 defaults & related DISPATCHED orders when driver updates them on an order
				10) update the Pickup/DeliverLastChangeDateUTC when Origin/Destination is changed (respectively)
				11) (re) apply Settlement Amounts to orders not yet fully settled when status is changed to DELIVERED
				12) if DBAudit is turned on, save an audit record for this Order change
-- =============================================*/
ALTER TRIGGER [dbo].[trigOrder_IU] ON [dbo].[tblOrder] AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @changesFound bit
	
		-- Note: the TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU') ) = 1 statement prevents recursion
	IF (TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU')) = 1 AND TRIGGER_NESTLEVEL( OBJECT_ID('trigOrderTicket_IU')) = 0) BEGIN

		/**********  START OF VALIDATION SECTION ************************/

		IF (UPDATE(OrderNum) 
			OR UPDATE(PriorityID) 
			OR UPDATE(DueDate) 
			OR UPDATE(RouteID) 
			OR UPDATE(OriginID) 
			OR UPDATE(OriginArriveTimeUTC) 
			OR UPDATE(OriginDepartTimeUTC) 
			OR UPDATE(OriginMinutes) 
			OR UPDATE(OriginWaitNotes) 
			OR UPDATE(OriginBOLNum) 
			OR UPDATE(OriginGrossUnits) 
			OR UPDATE(OriginNetUnits) 
			OR UPDATE(DestinationID) 
			OR UPDATE(DestArriveTimeUTC) 
			OR UPDATE(DestDepartTimeUTC) 
			OR UPDATE(DestMinutes) 
			OR UPDATE(DestWaitNotes) 
			OR UPDATE(DestBOLNum) 
			OR UPDATE(DestGrossUnits) 
			OR UPDATE(DestNetUnits) 
			OR UPDATE(CustomerID) 
			OR UPDATE(CarrierID) 
			OR UPDATE(DriverID) 
			OR UPDATE(TruckID)
			OR UPDATE(TrailerID) 
			OR UPDATE(Trailer2ID) 
			OR UPDATE(OperatorID) 
			OR UPDATE(PumperID) 
			OR UPDATE(TicketTypeID) 
			OR UPDATE(Rejected) 
			OR UPDATE(RejectNotes) 
			OR UPDATE(ChainUp) 
			-- allow this to be changed even in audit status
			--OR UPDATE(OriginTruckMileage) 
			OR UPDATE(OriginTankNum) 
			-- allow this to be changed even in audit status
			--OR UPDATE(DestTruckMileage) 
			OR UPDATE(CarrierTicketNum) 
			OR UPDATE(AuditNotes) 
			OR UPDATE(CreateDateUTC) 
			-- it is permissible to change the ActualMiles value on Audited orders up to the point the Order has been Shipper-Settled
			OR (UPDATE(ActualMiles) AND EXISTS (SELECT * FROM deleted i JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = i.ID WHERE OSS.BatchID IS NOT NULL))
			OR UPDATE(ProducerID) 
			OR UPDATE(CreatedByUser) 
			OR UPDATE(LastChangeDateUTC) 
			OR UPDATE(LastChangedByUser) 
			OR UPDATE(DeleteDateUTC) 
			OR UPDATE(DeletedByUser) 
			OR UPDATE(DestProductBSW)
			OR UPDATE(DestProductGravity) 
			OR UPDATE(DestProductTemp) 
			OR UPDATE(ProductID) 
			OR UPDATE(AcceptLastChangeDateUTC) 
			OR UPDATE(PickupLastChangeDateUTC) 
			OR UPDATE(DeliverLastChangeDateUTC) 
			OR UPDATE(OriginUomID) 
			OR UPDATE(DestUomID) 
			OR UPDATE(PickupPrintStatusID) 
			OR UPDATE(DeliverPrintStatusID)
			OR UPDATE(PickupPrintDateUTC) 
			OR UPDATE(DeliverPrintDateUTC) 
			OR UPDATE(OriginTankID) 
			OR UPDATE(OriginGrossStdUnits) 
			OR UPDATE(DispatchConfirmNum) 
			OR UPDATE(DispatchNotes)
			OR UPDATE(PickupDriverNotes)
			OR UPDATE(DeliverDriverNotes)
			OR UPDATE(OriginWaitReasonID)
			OR UPDATE(DestWaitReasonID)
			OR UPDATE(RejectReasonID) 
			OR UPDATE(DestOpenMeterUnits) 
			OR UPDATE(DestCloseMeterUnits))
			--OR UPDATE(ReassignKey)) we don't do any work if this value changes so just ignore it
		BEGIN
			-- only allow the StatusID value to be changed on an audited order
			IF EXISTS (SELECT * FROM deleted WHERE StatusID = 4)
			BEGIN
				-- if the "Allow Audited Order Update (temp only)" setting is TRUE then only warn when this occurs
				IF (dbo.fnToBool(dbo.fnSettingValue(35)) = 1)
					PRINT 'AUDITED orders is being modified - please investigate why!'
				ELSE BEGIN
					-- otherwise (normal behavior) - prevent AUDITED orders from being modified (except to UN-AUDIT them)
					RAISERROR('AUDITED orders cannot be modified!', 16, 1)
					IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
					RETURN
				END
			END
		END
		ELSE -- NO CHANGES DETECTED SO JUST QUIT
			RETURN

		PRINT 'trigOrder_IU FIRED'
					
		-- ensure the Origin and Destinations are both specified unless the Status is:
		--   (Generated, Assigned, Dispatched or Declined)
		IF  EXISTS(SELECT * FROM inserted O WHERE (O.OriginID IS NULL OR O.DestinationID IS NULL) AND O.StatusID NOT IN (-10,1,2,9))
		BEGIN
			RAISERROR('Invalid Order Status when the Origin and/or Or Destination is not specified', 16, 1)
			IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
			RETURN
		END
		
		/**********  END OF VALIDATION SECTION ************************/

		WHILE (SELECT count(*) FROM inserted i JOIN tblOrder O ON O.ID = i.ID WHERE O.OrderNum IS NULL) > 0
		BEGIN
			UPDATE tblOrder 
			  SET OrderNum = isnull((SELECT max(OrderNum) FROM tblOrder), 100000) + 1
				, CreateDateUTC = getutcdate()
			WHERE ID = (SELECT min(O.ID) FROM tblOrder O JOIN inserted i ON i.ID = O.ID WHERE O.OrderNum IS NULL)
		END
		
		-- re-compute the OriginMinutes (in case the website failed to compute it properly)
		IF UPDATE(OriginArriveTimeUTC) OR UPDATE(OriginDepartTimeUTC)
		BEGIN
			UPDATE tblOrder SET OriginMinutes = datediff(minute, i.OriginArriveTimeUTC, i.OriginDepartTimeUTC)
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
		END
		-- re-compute the DestMinutes (in case the website failed to compute it properly)
		IF UPDATE(DestArriveTimeUTC) OR UPDATE(DestDepartTimeUTC)
		BEGIN
			UPDATE tblOrder SET DestMinutes = datediff(minute, i.DestArriveTimeUTC, i.DestDepartTimeUTC)
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
		END
		
		-- ensure missing Routes are created and assigned for the specified Origin/Destination combination
		IF UPDATE(OriginID) OR UPDATE(DestinationID)
		BEGIN
			-- create any missing Route records
			INSERT INTO tblRoute (OriginID, DestinationID, CreateDateUTC, CreatedByUser)
				SELECT i.OriginID, i.DestinationID, GETUTCDATE(), ISNULL(i.LastChangedByUser, i.CreatedByUser)
				FROM inserted i
				LEFT JOIN tblRoute R ON R.OriginID = i.OriginID AND R.DestinationID = i.DestinationID
				WHERE i.OriginID IS NOT NULL AND i.DestinationID IS NOT NULL AND R.ID IS NULL
			-- ensure the Order records refer to the correct Route (ID)
			UPDATE tblOrder SET RouteID = R.ID
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
			LEFT JOIN tblRoute R ON R.OriginID = O.OriginID AND R.DestinationID = O.DestinationID

			-- update the ActualMiles from the related Route
			UPDATE tblOrder SET ActualMiles = R.ActualMiles
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
			JOIN tblRoute R ON R.ID = O.RouteID
		END
		
		IF (UPDATE(OriginID))
		BEGIN
			-- update Order.ProducerID to match what is assigned to the new Origin
			UPDATE tblOrder 
				SET ProducerID = OO.ProducerID
					-- update this stage specific date field so the mobile app knows the pickup has been modified
					, PickupLastChangeDateUTC = GETUTCDATE() -- mobile app never changes this so just use getutcdate()
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			WHERE d.OriginID <> O.OriginID

			-- update Order.OriginUomID to match what is assigned to the new Origin (quantities are updated below)
			UPDATE tblOrder SET OriginUomID = OO.UomID
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			WHERE d.OriginID <> O.OriginID AND d.OriginUomID <> OO.UomID
		END
		
		-- keep the DestUomID in sync with the Destination (units are updated below)
		IF (UPDATE(DestinationID))
		BEGIN
			-- update Order.DestUomID to match what is assigned to the new Destination
			UPDATE tblOrder 
			  SET DestUomID = DD.UomID
				-- updating this will ensure the mobile app refreshes itself with this revision (it doesn't use LastChangeDateUTC but rather the stage specific date value)
				, DeliverLastChangeDateUTC = GETUTCDATE() -- this will never be called by the mobile app so just use getutcdate()
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID
			JOIN tblDestination DD ON DD.ID = O.DestinationID
			WHERE d.DestinationID <> O.DestinationID AND d.DestUomID <> DD.UomID
		END
		
		-- ensure that any set PrintStatusID values are reset to 0 when backing up the general StatusID value
		IF (UPDATE(StatusID))
		BEGIN
			UPDATE tblOrder 
			  SET DeliverPrintStatusID = 0 
			  FROM tblOrder O
			  JOIN deleted d ON d.ID = O.ID
			WHERE O.StatusID <> d.StatusID AND O.StatusID IN (-10, 1, 2, 7, 8) -- Generated, Assigned, Dispatched, Accepted, Picked-Up

			UPDATE tblOrder 
			  SET PickupPrintStatusID = 0 
			  FROM tblOrder O
			  JOIN deleted d ON d.ID = O.ID
			WHERE O.StatusID <> d.StatusID AND O.StatusID IN (-10, 1, 2, 7) -- Generated, Assigned, Dispatched, Accepted
		END

		-- just updating the tickets here, trigOrderTicket_IU will in turn update this table
		-- ensure any existing OrderTickets are recomputed with the new OriginUOM
		UPDATE tblOrderTicket
		  SET GrossUnits = dbo.fnConvertUOM(GrossUnits, d.OriginUomID, O.OriginUomID)
			, NetUnits = dbo.fnConvertUOM(NetUnits, d.OriginUomID, O.OriginUomID)
		FROM tblOrderTicket OT
		JOIN tblOrder O ON O.ID = OT.OrderID
		JOIN deleted d ON d.ID = O.ID
		WHERE O.OriginUomID <> d.OriginUomID
		
		-- recalculate the Destination Unit values if only if the DestUomID changed but the values didn't
		UPDATE tblOrder
		  SET DestGrossUnits = dbo.fnConvertUOM(O.DestGrossUnits, d.DestUomID, O.DestUomID)
			, DestNetUnits = dbo.fnConvertUOM(O.DestNetUnits, d.DestUomID, O.DestUomID)
		FROM tblOrder O
		JOIN deleted d ON d.ID = O.ID
		WHERE d.DestUomID <> O.DestUomID 
		  AND d.DestGrossUnits = O.DestGrossUnits
		  AND d.DestNetUnits = O.DestNetUnits

		/*************************************************************************************************************/
		/* handle driver changes - "orphan" the order/ticket (with a new ORDERNUM) - and clone it for the new driver */
		IF (UPDATE(DriverID))
		BEGIN
			-- create cloned order/ticket records (these records will be cloned then the original reset to orphaned status)
			SELECT O.*, NewOrderNum = CAST(NULL as int)
			INTO #clone
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID AND d.DriverID IS NOT NULL AND d.DriverID <> isnull(O.DriverID, 0)

			/* generate the next OrderNum values for these new records */
			DECLARE @newOrderNum int; SET @newOrderNum = isnull((SELECT max(OrderNum) FROM tblOrder), 100000) + 1;
			DECLARE @newReassignKey int; SET @newReassignKey = isnull((SELECT max(ReassignKey) FROM tblOrder), 0) + 1;
			WHILE (SELECT count(*) FROM #clone WHERE NewOrderNum IS NULL) > 0
			BEGIN
				UPDATE #clone
				  SET NewOrderNum = @newOrderNum, ReassignKey = isnull(ReassignKey, @newReassignKey)
					, CreateDateUTC = getutcdate()
				WHERE ID = (SELECT min(ID) FROM #clone WHERE NewOrderNum IS NULL)
				-- if the @newReassignKey wasn't used above, then that # will be skipped, but this should not be an issue
				SELECT @newOrderNum = @newOrderNum + 1, @newReassignKey = @newReassignKey + 1
			END
			
			-- reset the existing records as orphaned
			UPDATE tblOrder
				SET DeleteDateUTC = isnull(O.DeleteDateUTC, GETUTCDATE()), DeletedByUser = isnull(O.DeletedByUser, O.LastChangedByUser)
					, CarrierID = d.CarrierID, DriverID = d.DriverID, TruckID = d.TruckID, TrailerID = d.TrailerID, Trailer2ID = d.Trailer2ID
					, DispatchConfirmNum = NULL
					, ReassignKey = C.ReassignKey
			FROM tblOrder O
			JOIN #clone C ON C.ID = O.ID
			JOIN deleted d on D.ID = O.id

			-- create the cloned records (which is essentially the original with a new ID and the Carrrier & default Truck|Trailers of the new driver)
			INSERT INTO tblOrder (OrderNum, [StatusID], [PriorityID], [DueDate], [RouteID], [OriginID], [OriginArriveTimeUTC], [OriginDepartTimeUTC], [OriginMinutes], [OriginWaitNotes], [OriginBOLNum], [OriginGrossUnits], [OriginNetUnits], [DestinationID], [DestArriveTimeUTC], [DestDepartTimeUTC], [DestMinutes], [DestWaitNotes], [DestBOLNum], [DestGrossUnits], [DestNetUnits], [CustomerID], [CarrierID], [DriverID], [TruckID], [TrailerID], [Trailer2ID], [OperatorID], [PumperID], [TicketTypeID], [Rejected], [RejectNotes], [ChainUp], [OriginTruckMileage], [OriginTankNum], [DestTruckMileage], [CarrierTicketNum], [AuditNotes], [CreateDateUTC], [ActualMiles], [ProducerID], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser], [DestProductBSW], [DestProductGravity], [DestProductTemp], [ProductID], [AcceptLastChangeDateUTC], [PickupLastChangeDateUTC], [DeliverLastChangeDateUTC], [OriginUomID], [DestUomID], [PickupPrintStatusID], [DeliverPrintStatusID], [PickupPrintDateUTC], [DeliverPrintDateUTC], [OriginTankID], [OriginGrossStdUnits], [DispatchNotes], [PickupDriverNotes], [DeliverDriverNotes], [DispatchConfirmNum], [OriginWaitReasonID], [DestWaitReasonID], [RejectReasonID], [DestOpenMeterUnits], [DestCloseMeterUnits], [ReassignKey])
				SELECT NewOrderNum, [StatusID], [PriorityID], [DueDate], [RouteID], [OriginID], [OriginArriveTimeUTC], [OriginDepartTimeUTC], [OriginMinutes], [OriginWaitNotes], [OriginBOLNum], [OriginGrossUnits], [OriginNetUnits], [DestinationID], [DestArriveTimeUTC], [DestDepartTimeUTC], [DestMinutes], [DestWaitNotes], [DestBOLNum], [DestGrossUnits], [DestNetUnits], [CustomerID], D.[CarrierID], [DriverID], D.TruckID, D.TrailerID, D.Trailer2ID, [OperatorID], [PumperID], [TicketTypeID], [Rejected], [RejectNotes], [ChainUp], [OriginTruckMileage], [OriginTankNum], [DestTruckMileage], [CarrierTicketNum], [AuditNotes], O.CreateDateUTC, [ActualMiles], [ProducerID], O.CreatedByUser, O.LastChangeDateUTC, O.LastChangedByUser, O.DeleteDateUTC, O.DeletedByUser, [DestProductBSW], [DestProductGravity], [DestProductTemp], [ProductID], [AcceptLastChangeDateUTC], [PickupLastChangeDateUTC], [DeliverLastChangeDateUTC], [OriginUomID], [DestUomID], [PickupPrintStatusID], [DeliverPrintStatusID], [PickupPrintDateUTC], [DeliverPrintDateUTC], [OriginTankID], [OriginGrossStdUnits], [DispatchNotes], [PickupDriverNotes], [DeliverDriverNotes], DispatchConfirmNum, [OriginWaitReasonID], [DestWaitReasonID], [RejectReasonID], [DestOpenMeterUnits], [DestCloseMeterUnits], ReassignKey
				FROM #clone O
				LEFT JOIN tblDriver D ON D.ID = O.DriverID
		
			-- cache the affected tickets
			SELECT OT.*, C.NewOrderNum, OrderDeletedByUser = C.LastChangedByUser
			INTO #cloneTicket
			FROM tblOrderTicket OT
			JOIN #clone C ON C.ID = OT.OrderID
			WHERE OT.DeleteDateUTC IS NULL
			
			-- create the cloned Order Ticket records (copy of existing ticket(s) pointed at new "original" Order)
			INSERT INTO tblOrderTicket (OrderID, CarrierTicketNum, TicketTypeID, TankNum, ProductObsGravity, ProductObsTemp, ProductBSW, OpeningGaugeFeet, OpeningGaugeInch, OpeningGaugeQ, ClosingGaugeFeet, ClosingGaugeInch, ClosingGaugeQ
					, GrossUnits, NetUnits, Rejected, RejectNotes, SealOff, SealOn, BOLNum, ProductHighTemp, ProductLowTemp, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser
					, UID, FromMobileApp, OriginTankID, BottomFeet, BottomInches, BottomQ, GrossStdUnits, RejectReasonID, MeterFactor, OpenMeterUnits, CloseMeterUnits)
				SELECT O.ID, CT.CarrierTicketNum, CT.TicketTypeID, CT.TankNum, CT.ProductObsGravity, CT.ProductObsTemp, CT.ProductBSW, CT.OpeningGaugeFeet, CT.OpeningGaugeInch, CT.OpeningGaugeQ, CT.ClosingGaugeFeet, CT.ClosingGaugeInch, CT.ClosingGaugeQ
					, CT.GrossUnits, CT.NetUnits, CT.Rejected, CT.RejectNotes, CT.SealOff, CT.SealOn, CT.BOLNum, CT.ProductHighTemp, CT.ProductLowTemp, CT.CreateDateUTC, CT.CreatedByUser, CT.LastChangeDateUTC, CT.LastChangedByUser, CT.DeleteDateUTC, CT.DeletedByUser
					, newid(), CT.FromMobileApp, CT.OriginTankID, CT.BottomFeet, CT.BottomInches, CT.BottomQ, CT.GrossStdUnits, CT.RejectReasonID, CT.MeterFactor, CT.OpenMeterUnits, CT.CloseMeterUnits
				FROM #cloneTicket CT
				JOIN tblOrder O ON O.OrderNum = CT.NewOrderNum
		END
		/*************************************************************************************************************/
		
		/* HANDLE TRUCK | TRAILER | TRAILER2 default changes when the driver updates his Truck | Trailer | Trailer2 on ACCEPTANCE */
		-- TRUCK
		IF (UPDATE(TruckID) AND EXISTS (
			SELECT i.* 
			FROM inserted i 
			JOIN deleted d ON d.ID = i.ID
			  AND i.DriverID = d.DriverID 
			  AND i.TruckID <> d.TruckID
			WHERE i.StatusID IN (7, 8, 3) -- ACCEPTED, PICKEDUP, DELIVERED
			  AND d.StatusID IN (2))) -- DISPATCHED
		BEGIN
			UPDATE tblDriver
			  SET TruckID = i.TruckID
				, LastChangeDateUTC = i.LastChangeDateUTC
				, LastChangedByUser = i.LastChangedByUser
			FROM tblDriver DR
			JOIN inserted i ON i.DriverID = DR.ID
			JOIN deleted d ON d.DriverID = DR.ID
			WHERE i.TruckID <> d.TruckID
			
			UPDATE tblOrder
			  SET TruckID = i.TruckID
				, LastChangeDateUTC = i.LastChangeDateUTC
				, LastChangedByUser = i.LastChangedByUser
			FROM tblOrder O
			JOIN inserted i ON i.DriverID = O.DriverID 
			  AND O.ID <> i.ID
			  AND O.StatusID IN (2) -- DISPATCHED
			  AND i.TruckID <> O.TruckID
			  AND O.DeleteDateUTC IS NULL
		END
		-- TRAILER
		IF (UPDATE(TrailerID) AND EXISTS (
			SELECT i.* 
			FROM inserted i 
			JOIN deleted d ON d.ID = i.ID
			  AND i.DriverID = d.DriverID 
			  AND i.TrailerID <> d.TrailerID
			WHERE i.StatusID IN (7, 8, 3) -- ACCEPTED, PICKEDUP, DELIVERED
			  AND d.StatusID IN (2))) -- DISPATCHED
		BEGIN
			UPDATE tblDriver
			  SET TrailerID = i.TrailerID
				, LastChangeDateUTC = i.LastChangeDateUTC
				, LastChangedByUser = i.LastChangedByUser
			FROM tblDriver DR
			JOIN inserted i ON i.DriverID = DR.ID
			JOIN deleted d ON d.DriverID = DR.ID
			WHERE i.TrailerID <> d.TrailerID
			
			UPDATE tblOrder
			  SET TrailerID = i.TrailerID
				, LastChangeDateUTC = i.LastChangeDateUTC
				, LastChangedByUser = i.LastChangedByUser
			FROM tblOrder O
			JOIN inserted i ON i.DriverID = O.DriverID 
			  AND O.ID <> i.ID
			  AND O.StatusID IN (2) -- DISPATCHED
			  AND i.TrailerID <> O.TrailerID
			  AND O.DeleteDateUTC IS NULL
		END
		-- TRAILER 2
		IF (UPDATE(Trailer2ID) AND EXISTS (
			SELECT i.* 
			FROM inserted i 
			JOIN deleted d ON d.ID = i.ID
			  AND i.DriverID = d.DriverID 
			  AND isnull(i.Trailer2ID, 0) <> isnull(d.Trailer2ID, 0)
			WHERE i.StatusID IN (7, 8, 3) -- ACCEPTED, PICKEDUP, DELIVERED
			  AND d.StatusID IN (2))) -- DISPATCHED
		BEGIN
			UPDATE tblDriver
			  SET TrailerID = i.TrailerID
				, LastChangeDateUTC = i.LastChangeDateUTC
				, LastChangedByUser = i.LastChangedByUser
			FROM tblDriver DR
			JOIN inserted i ON i.DriverID = DR.ID
			JOIN deleted d ON d.DriverID = DR.ID
			WHERE isnull(i.Trailer2ID, 0) <> isnull(d.Trailer2ID, 0)
			
			UPDATE tblOrder
			  SET Trailer2ID = i.Trailer2ID
				, LastChangeDateUTC = i.LastChangeDateUTC
				, LastChangedByUser = i.LastChangedByUser
			FROM tblOrder O
			JOIN inserted i ON i.DriverID = O.DriverID 
			  AND O.ID <> i.ID
			  AND O.StatusID IN (2) -- DISPATCHED
			  AND isnull(i.Trailer2ID, 0) <> isnull(O.Trailer2ID, 0)
			  AND O.DeleteDateUTC IS NULL
		END
		/*************************************************************************************************************/
	
		-- Apply Settlement Rates/Amounts to Order when STATUS is changed to DELIVERED (until it is FINAL settled)
		IF UPDATE(StatusID) OR UPDATE(DeliverPrintStatusID)
		BEGIN
			DECLARE @deliveredIDs TABLE (ID int)
			INSERT INTO @deliveredIDs (ID)
				SELECT i.ID 
				FROM inserted i
				JOIN tblPrintStatus iPS ON iPS.ID = i.DeliverPrintStatusID
				JOIN deleted d ON d.ID = i.ID
				JOIN tblPrintStatus dPS ON dPS.ID = d.DeliverPrintStatusID
				WHERE i.StatusID = 3 AND iPS.IsCompleted = 1 AND i.StatusID + iPS.IsCompleted <> d.StatusID + dPS.IsCompleted
			
			DECLARE @id int
			WHILE EXISTS (SELECT 1 FROM @deliveredIDs)
			BEGIN
				SELECT TOP 1 @id = ID FROM @deliveredIDs
				EXEC spApplyRatesBoth @id, 'System' 
				DELETE FROM @deliveredIDs WHERE ID = @id
			END
		END
		
		--NOTE: we do not update the DestOpenMeterUnits/DestCloseMeterUnits since they don't auto update
		--  (and they simply represent a reading that is assumed to in the DestUomID UOM at entry)
		--  (if they are updated in the future [manually] the will calculate again using the DestUomID at that time)

		IF EXISTS (SELECT * FROM inserted EXCEPT SELECT * FROM deleted)
		BEGIN
			-- optionally add tblOrderDBAudit records
			BEGIN TRY
				IF EXISTS(SELECT * FROM tblSetting WHERE ID = 32 AND (Value LIKE 'true' OR Value LIKE 'yes'))
					INSERT INTO tblOrderDbAudit (DBAuditDate, [ID], [OrderNum], [StatusID], [PriorityID], [DueDate], [RouteID], [OriginID], [OriginArriveTimeUTC], [OriginDepartTimeUTC], [OriginMinutes], [OriginWaitNotes], [OriginBOLNum], [OriginGrossUnits], [OriginNetUnits], [DestinationID], [DestArriveTimeUTC], [DestDepartTimeUTC], [DestMinutes], [DestWaitNotes], [DestBOLNum], [DestGrossUnits], [DestNetUnits], [CustomerID], [CarrierID], [DriverID], [TruckID], [TrailerID], [Trailer2ID], [OperatorID], [PumperID], [TicketTypeID], [Rejected], [RejectNotes], [ChainUp], [OriginTruckMileage], [OriginTankNum], [DestTruckMileage], [CarrierTicketNum], [AuditNotes], [CreateDateUTC], [ActualMiles], [ProducerID], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser], [DestProductBSW], [DestProductGravity], [DestProductTemp], [ProductID], [AcceptLastChangeDateUTC], [PickupLastChangeDateUTC], [DeliverLastChangeDateUTC], [OriginUomID], [DestUomID], [PickupPrintStatusID], [DeliverPrintStatusID], [PickupPrintDateUTC], [DeliverPrintDateUTC], [OriginTankID], [OriginGrossStdUnits], [DispatchNotes], [PickupDriverNotes], [DeliverDriverNotes], [DispatchConfirmNum], [OriginWaitReasonID], [DestWaitReasonID], [RejectReasonID], [DestOpenMeterUnits], [DestCloseMeterUnits], ReassignKey)
						SELECT GETUTCDATE(), [ID], [OrderNum], [StatusID], [PriorityID], [DueDate], [RouteID], [OriginID], [OriginArriveTimeUTC], [OriginDepartTimeUTC], [OriginMinutes], [OriginWaitNotes], [OriginBOLNum], [OriginGrossUnits], [OriginNetUnits], [DestinationID], [DestArriveTimeUTC], [DestDepartTimeUTC], [DestMinutes], [DestWaitNotes], [DestBOLNum], [DestGrossUnits], [DestNetUnits], [CustomerID], [CarrierID], [DriverID], [TruckID], [TrailerID], [Trailer2ID], [OperatorID], [PumperID], [TicketTypeID], [Rejected], [RejectNotes], [ChainUp], [OriginTruckMileage], [OriginTankNum], [DestTruckMileage], [CarrierTicketNum], [AuditNotes], [CreateDateUTC], [ActualMiles], [ProducerID], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser], [DestProductBSW], [DestProductGravity], [DestProductTemp], [ProductID], [AcceptLastChangeDateUTC], [PickupLastChangeDateUTC], [DeliverLastChangeDateUTC], [OriginUomID], [DestUomID], [PickupPrintStatusID], [DeliverPrintStatusID], [PickupPrintDateUTC], [DeliverPrintDateUTC], [OriginTankID], [OriginGrossStdUnits], [DispatchNotes], [PickupDriverNotes], [DeliverDriverNotes], [DispatchConfirmNum], [OriginWaitReasonID], [DestWaitReasonID], [RejectReasonID], [DestOpenMeterUnits], [DestCloseMeterUnits], ReassignKey
						FROM deleted d
			END TRY
			BEGIN CATCH
				PRINT 'trigOrder_IU.DBAUDIT FAILURE: ' + ERROR_MESSAGE()
			END CATCH
		END
		
	PRINT 'trigOrder_IU COMPLETE'

	END
	
END



GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			prevent changes to Locked (in use) records
***********************************************/
ALTER TRIGGER [dbo].[trigShipperDestinationWaitRate_IU] ON [dbo].[tblShipperDestinationWaitRate] AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblShipperDestinationWaitRate X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ReasonID, X.ReasonID) = 1
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(i.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Wait Rates are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewShipperDestinationWaitRate X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ReasonID, X.ReasonID) = 0
		  OR dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
		  OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
		  OR dbo.fnCompareNullableInts(d.DestinationID, X.DestinationID) = 0
		  OR dbo.fnCompareNullableInts(d.StateID, X.StateID) = 0
		  OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
		  OR d.EffectiveDate <> X.EffectiveDate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	IF @error IS NOT NULL 
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementCarrier SET DestinationWaitRateID = NULL, DestinationWaitAmount = NULL 
		WHERE BatchID IS NULL AND DestinationWaitRateID IN (SELECT ID FROM inserted)
END

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*************************************/
-- Date Created: 19 Feb 2015
-- Author: Kevin Alons
-- Purpose: prevent deletion of Locked records
/*************************************/
ALTER TRIGGER [dbo].[trigShipperFuelSurchargeRate_IOD] ON [dbo].[tblShipperFuelSurchargeRate] INSTEAD OF DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewShipperFuelSurchargeRate X ON X.id = d.ID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('Locked (in use) records cannot be deleted', 16, 1)
		RETURN
	END
	ELSE
	BEGIN
		-- ensure all references to the deleted rates are fully removed
		UPDATE tblOrderSettlementShipper
		  SET FuelSurchargeRateID = NULL, FuelSurchargeAmount = NULL
		WHERE BatchID IS NULL
		  AND FuelSurchargeRateID IN (SELECT ID FROM deleted)
		-- do the actual update here
		DELETE FROM tblShipperFuelSurchargeRate WHERE ID IN (SELECT ID FROM deleted)
	END
END

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
-- Date Created: 20 Feb 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			prevent changes to Locked (in use) records
			un-associate changed rates from rated orders
***********************************************/
CREATE TRIGGER trigShipperFuelSurchargeRate_IU ON tblShipperFuelSurchargeRate AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblShipperFuelSurchargeRate X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Fuel Surcharge Rates are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewShipperFuelSurchargeRate X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
		  OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
		  OR d.EffectiveDate <> X.EffectiveDate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	IF @error IS NOT NULL 
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementShipper SET FuelSurchargeRateID = NULL, FuelSurchargeAmount = NULL 
		WHERE BatchID IS NULL AND FuelSurchargeRateID IN (SELECT ID FROM inserted)
END

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			prevent changes to Locked (in use) records
***********************************************/
ALTER TRIGGER [dbo].[trigShipperOrderRejectRate_IU] ON [dbo].[tblShipperOrderRejectRate] AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblShipperOrderRejectRate X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ReasonID, X.ReasonID) = 1
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(i.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Reject Rates are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewShipperOrderRejectRate X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ReasonID, X.ReasonID) = 0
		  OR dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
		  OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
		  OR dbo.fnCompareNullableInts(d.OriginID, X.OriginID) = 0
		  OR dbo.fnCompareNullableInts(d.StateID, X.StateID) = 0
		  OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
		  OR d.EffectiveDate <> X.EffectiveDate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	IF @error IS NOT NULL 
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementShipper SET OrderRejectRateID = NULL, OrderRejectAmount = NULL 
		WHERE BatchID IS NULL AND OrderRejectRateID IN (SELECT ID FROM inserted)
END

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			prevent changes to Locked (in use) records
***********************************************/
ALTER TRIGGER [dbo].[trigShipperOriginWaitRate_IU] ON [dbo].[tblShipperOriginWaitRate] AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblShipperOriginWaitRate X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ReasonID, X.ReasonID) = 1
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(i.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Wait Rates are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewShipperOriginWaitRate X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ReasonID, X.ReasonID) = 0
		  OR dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
		  OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
		  OR dbo.fnCompareNullableInts(d.OriginID, X.OriginID) = 0
		  OR dbo.fnCompareNullableInts(d.StateID, X.StateID) = 0
		  OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
		  OR d.EffectiveDate <> X.EffectiveDate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	IF @error IS NOT NULL 
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementCarrier SET OriginWaitRateID = NULL, OriginWaitAmount = NULL 
		WHERE BatchID IS NULL AND OriginWaitRateID IN (SELECT ID FROM inserted)
END

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
/***********************************************/
ALTER TRIGGER [dbo].[trigShipperRouteRate_IU] ON [dbo].[tblShipperRouteRate] AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblShipperRouteRate X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND i.RouteID = X.RouteID
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Route Rates are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewShipperRouteRate X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
			OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
			OR d.RouteID = X.RouteID
			OR d.EffectiveDate <> X.EffectiveDate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
		IF (@error IS NOT NULL)
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementCarrier SET RouteRateID = NULL, RangeRateID = NULL, LoadAmount = NULL 
		WHERE BatchID IS NULL AND RouteRateID IN (SELECT ID FROM inserted)
END

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			prevent changes to Locked records (except for EndDate)
***********************************************/
ALTER TRIGGER [dbo].[trigShipperWaitFeeParameter_IU] ON [dbo].[tblShipperWaitFeeParameter] AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblShipperWaitFeeParameter X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(i.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Parameters are not allowed'
	END
	ElSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewShipperWaitFeeParameter X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
			OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
			OR dbo.fnCompareNullableInts(d.OriginStateID, X.OriginStateID) = 0
			OR dbo.fnCompareNullableInts(d.DestStateID, X.DestStateID) = 0
			OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
			OR d.EffectiveDate <> X.EffectiveDate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	
	IF (@error IS NOT NULL)
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementCarrier SET WaitFeeParameterID = NULL 
		WHERE BatchID IS NULL AND WaitFeeParameterID IN (SELECT ID FROM inserted)
END

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			ensure Locked records are not modified (except the EndDate)
			un-associate changed rates from rated orders
***********************************************/
ALTER TRIGGER [dbo].[trigCarrierAssessorialRate_IU] ON [dbo].[tblCarrierAssessorialRate] AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblCarrierAssessorialRate X 
			ON i.ID <> X.ID
			  AND i.TypeID = X.TypeID
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(i.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(i.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(i.ProducerID, X.ProducerID) = 1
			  AND dbo.fnCompareNullableInts(i.OperatorID, X.OperatorID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Assessorial Rates are not allowed';
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewCarrierAssessorialRate X ON d.ID = X.ID AND X.Locked = 1
		WHERE d.TypeID <> X.TypeID
			OR dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
			OR dbo.fnCompareNullableInts(d.CarrierID, X.CarrierID) = 0
			OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
			OR dbo.fnCompareNullableInts(d.OriginID, X.OriginID) = 0
			OR dbo.fnCompareNullableInts(d.DestinationID, X.DestinationID) = 0
			OR dbo.fnCompareNullableInts(d.OriginStateID, X.OriginStateID) = 0
			OR dbo.fnCompareNullableInts(d.DestStateID, X.DestStateID) = 0
			OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
			OR dbo.fnCompareNullableInts(d.ProducerID, X.ProducerID) = 0
			OR dbo.fnCompareNullableInts(d.OperatorID, X.OperatorID) = 0
			OR d.EffectiveDate <> X.EffectiveDate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed';
	END
	
	IF (@error IS NOT NULL)
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementCarrierAssessorialCharge
		  SET AssessorialRateID = NULL, Amount = NULL 
		FROM tblOrderSettlementCarrierAssessorialCharge AC
		JOIN tblOrderSettlementCarrier SC ON SC.OrderID = AC.OrderID
		JOIN inserted i ON i.ID = AC.AssessorialRateID
		WHERE SC.BatchID IS NULL
END

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			ensure Locked records are not modified (except the EndDate)
***********************************************/
ALTER TRIGGER [dbo].[trigShipperAssessorialRate_IU] ON [dbo].[tblShipperAssessorialRate] AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblShipperAssessorialRate X 
			ON i.ID <> X.ID
			  AND i.TypeID = X.TypeID
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(i.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(i.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(i.ProducerID, X.ProducerID) = 1
			  AND dbo.fnCompareNullableInts(i.OperatorID, X.OperatorID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Assessorial Rates are not allowed';
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewShipperAssessorialRate X ON d.ID = X.ID AND X.Locked = 1
		WHERE d.TypeID <> X.TypeID
			OR dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
			OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
			OR dbo.fnCompareNullableInts(d.OriginID, X.OriginID) = 0
			OR dbo.fnCompareNullableInts(d.DestinationID, X.DestinationID) = 0
			OR dbo.fnCompareNullableInts(d.OriginStateID, X.OriginStateID) = 0
			OR dbo.fnCompareNullableInts(d.DestStateID, X.DestStateID) = 0
			OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
			OR dbo.fnCompareNullableInts(d.ProducerID, X.ProducerID) = 0
			OR dbo.fnCompareNullableInts(d.OperatorID, X.OperatorID) = 0
			OR d.EffectiveDate <> X.EffectiveDate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed';
	END
	
	IF (@error IS NOT NULL)
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementShipperAssessorialCharge
		  SET AssessorialRateID = NULL, Amount = NULL 
		FROM tblOrderSettlementShipperAssessorialCharge AC
		JOIN tblOrderSettlementShipper SC ON SC.OrderID = AC.OrderID
		JOIN inserted i ON i.ID = AC.AssessorialRateID
		WHERE SC.BatchID IS NULL
END

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*************************************
-- Date Created: 18 Jan 2015
-- Author: Kevin Alons
-- Purpose: handle specialized logic related to editing Route Rates (due to combination of RateSheet + RangeRate tables)
*************************************/
ALTER TRIGGER [dbo].[trigViewCarrierRateSheetRangeRate_IU_Update] ON [dbo].[viewCarrierRateSheetRangeRate] INSTEAD OF INSERT, UPDATE  AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
		-- PRINT 'apply any RateSheet changes to the underlying RateSheet records'
		UPDATE tblCarrierRateSheet
			SET ShipperID = i.ShipperID
				, CarrierID = i.CarrierID
				, ProductGroupID = i.ProductGroupID
				, OriginStateID = i.OriginStateID
				, DestStateID = i.DestStateID
				, RegionID = i.RegionID
				, EffectiveDate = i.EffectiveDate
				, EndDate = i.EndDate
				, RateTypeID = i.RateTypeID
				, UomID = i.UomID
				, LastChangeDateUTC = isnull(i.LastChangeDateUTC, getutcdate())
				, LastChangedByUser = i.LastChangedByUser
		FROM tblCarrierRateSheet RS
		JOIN ( 
			SELECT DISTINCT i.RateSheetID, i.ShipperID, i.CarrierID, i.ProductGroupID, i.OriginStateID, i.DestStateID, i.RegionID, i.EffectiveDate, i.EndDate, i.RateTypeID, i.UomID, i.LastChangeDateUTC, i.LastChangedByUser
			FROM inserted i 
			JOIN deleted d ON d.ID = i.ID
			WHERE dbo.fnCompareNullableInts(i.ShipperID, d.ShipperID) = 0
				OR dbo.fnCompareNullableInts(i.CarrierID, d.CarrierID) = 0
				OR dbo.fnCompareNullableInts(i.ProductGroupID, d.ProductGroupID) = 0
				OR dbo.fnCompareNullableInts(i.OriginStateID, d.OriginStateID) = 0
				OR dbo.fnCompareNullableInts(i.DestStateID, d.DestStateID) = 0
				OR dbo.fnCompareNullableInts(i.RegionID, d.RegionID) = 0
		) i ON i.RateSheetID = RS.ID

		--PRINT 'ensure a RateSheet record exists for each new RangeRate records'
		INSERT INTO tblCarrierRateSheet (ShipperID, CarrierID, ProductGroupID, OriginStateID, DestStateID, RegionID, EffectiveDate, EndDate, RateTypeID, UomID, CreatedByUser)
			SELECT i.ShipperID, i.CarrierID, i.ProductGroupID, i.OriginStateID, i.DestStateID, i.RegionID, i.EffectiveDate, i.EndDate, i.RateTypeID, i.UomID, i.CreatedByUser 
			FROM inserted i
			LEFT JOIN tblCarrierRateSheet RS 
				ON i.EffectiveDate = RS.EffectiveDate
					AND dbo.fnCompareNullableInts(i.ShipperID, RS.ShipperID) = 1
					AND dbo.fnCompareNullableInts(i.CarrierID, RS.CarrierID) = 1
					AND dbo.fnCompareNullableInts(i.ProductGroupID, RS.ProductGroupID) = 1
					AND dbo.fnCompareNullableInts(i.OriginStateID, RS.OriginStateID) = 1
					AND dbo.fnCompareNullableInts(i.DestStateID, RS.DestStateID) = 1
					AND dbo.fnCompareNullableInts(i.RegionID, RS.RegionID) = 1
			WHERE isnull(RateSheetID, 0) = 0
			  AND RS.ID IS NULL
		
		-- optimization for typical usaage (where only 1 record is INSERTed)
		IF (SELECT count(1) FROM inserted i WHERE isnull(ID, 0) = 0) = 1
		BEGIN
			DECLARE @rsID int
			SELECT @rsID = isnull(RateSheetID, SCOPE_IDENTITY()) FROM inserted
			IF @rsID IS NULL
				SELECT @rsID = RS.ID
				FROM inserted i
				LEFT JOIN tblCarrierRateSheet RS 
					ON i.EffectiveDate = RS.EffectiveDate
						AND dbo.fnCompareNullableInts(i.ShipperID, RS.ShipperID) = 1
						AND dbo.fnCompareNullableInts(i.CarrierID, RS.CarrierID) = 1
						AND dbo.fnCompareNullableInts(i.ProductGroupID, RS.ProductGroupID) = 1
						AND dbo.fnCompareNullableInts(i.OriginStateID, RS.OriginStateID) = 1
						AND dbo.fnCompareNullableInts(i.DestStateID, RS.DestStateID) = 1
						AND dbo.fnCompareNullableInts(i.RegionID, RS.RegionID) = 1

			INSERT INTO tblCarrierRangeRate (RateSheetID, MinRange, MaxRange, Rate, CreateDateUTC, CreatedByUser)
				SELECT @rsID, i.MinRange, i.MaxRange, i.Rate, isnull(i.CreateDateUTC, getutcdate()), i.CreatedByUser
				FROM inserted i
				WHERE isnull(ID, 0) = 0
		END
		ELSE 	-- bulk insert or update
		BEGIN
			-- PRINT 'Updating any existing record editable data'
			UPDATE tblCarrierRangeRate
				SET MinRange = i.MinRange
					, MaxRange = i.MaxRange
					, Rate = i.Rate
					, LastChangeDateUTC = isnull(i.LastChangeDateUTC, getutcdate())
					, LastChangedByUser = i.LastChangedByUser
			FROM tblCarrierRangeRate X
			JOIN inserted i ON i.ID = X.ID

			-- PRINT 'insert any new records'
			INSERT INTO tblCarrierRangeRate (RateSheetID, MinRange, MaxRange, Rate, CreatedByUser)
				SELECT R.ID, i.MinRange, i.MaxRange, i.Rate, isnull(i.CreatedByUser, R.CreatedByUser)
				FROM inserted i
				JOIN tblCarrierRateSheet R ON i.EffectiveDate = R.EffectiveDate
					AND dbo.fnCompareNullableInts(i.ShipperID, R.ShipperID) = 1
					AND dbo.fnCompareNullableInts(i.CarrierID, R.CarrierID) = 1
					AND dbo.fnCompareNullableInts(i.ProductGroupID, R.ProductGroupID) = 1
					AND dbo.fnCompareNullableInts(i.OriginStateID, R.OriginStateID) = 1
					AND dbo.fnCompareNullableInts(i.DestStateID, R.DestStateID) = 1
					AND dbo.fnCompareNullableInts(i.RegionID, R.RegionID) = 1
				WHERE ISNULL(i.ID, 0) = 0
		END
	END TRY
	BEGIN CATCH
		DECLARE @error varchar(255)
		SET @error = substring(ERROR_MESSAGE(), 1, 255)
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END CATCH
END

GO

IF EXISTS(SELECT 1 FROM sys.database_principals WHERE [name] = N'dispatchcrude_iis_acct')
	GRANT SELECT ON OBJECT::[dbo].[fnCarrierRateSheetRangeRatesDisplay] TO [dispatchcrude_iis_acct]
GO

IF EXISTS(SELECT 1 FROM sys.database_principals WHERE [name] = N'dispatchcrude_iis_acct')
	GRANT SELECT ON OBJECT::[dbo].[fnShipperRateSheetRangeRatesDisplay] TO [dispatchcrude_iis_acct]
GO

IF EXISTS(SELECT 1 FROM sys.database_principals WHERE [name] = N'dispatchcrude_iis_acct')
	GRANT SELECT ON OBJECT::[dbo].[viewOrderUnaudit] TO [dispatchcrude_iis_acct]
GO

IF EXISTS(SELECT 1 FROM sys.database_principals WHERE [name] = N'dispatchcrude_iis_acct')
	GRANT SELECT ON OBJECT::[dbo].[viewCarrier_OrderInfo] TO [dispatchcrude_iis_acct]
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OriginWaitRate info for the specified order
/***********************************/
ALTER FUNCTION [dbo].[fnOrderCarrierOriginWaitRate](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, Minutes = O.DestMinutes, R.Rate
	FROM viewOrder O
	CROSS APPLY dbo.fnCarrierOriginWaitRate(O.OrderDate, null, O.OriginWaitReasonID, O.CustomerID, O.CarrierID, O.ProductGroupID, O.OriginID, O.OriginStateID, O.OriginRegionID, 1) R
	WHERE O.ID = @ID
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper OriginWaitRate info for the specified order
/***********************************/
ALTER FUNCTION [dbo].[fnOrderShipperOriginWaitRate](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, Minutes = O.OriginMinutes, R.Rate
	FROM viewOrder O
	CROSS APPLY dbo.fnShipperOriginWaitRate(O.OrderDate, null, O.OriginWaitReasonID, O.CustomerID, O.ProductGroupID, O.OriginID, O.OriginStateID, O.OriginRegionID, 1) R
	WHERE O.ID = @ID
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper DestinationWaitRate info for the specified order
/***********************************/
ALTER FUNCTION [dbo].[fnOrderShipperDestinationWaitRate](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, Minutes = O.DestMinutes, R.Rate
	FROM viewOrder O
	CROSS APPLY dbo.fnShipperDestinationWaitRate(O.OrderDate, null, O.DestWaitReasonID, O.CustomerID, O.ProductGroupID, O.DestinationID, O.DestStateID, O.DestRegionID, 1) R
	WHERE O.ID = @ID
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier DestinationWaitRate info for the specified order
/***********************************/
ALTER FUNCTION [dbo].[fnOrderCarrierDestinationWaitRate](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, Minutes = O.DestMinutes, R.Rate
	FROM viewOrder O
	CROSS APPLY dbo.fnCarrierDestinationWaitRate(O.OrderDate, null, O.DestWaitReasonID, O.CustomerID, O.CarrierID, O.ProductGroupID, O.DestinationID, O.DestStateID, O.DestRegionID, 1) R
	WHERE O.ID = @ID
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier FuelSurcharge info for the specified order
/***********************************/
ALTER FUNCTION [dbo].[fnOrderCarrierFuelSurchargeRate](@ID int) RETURNS TABLE AS RETURN
	SELECT RateID
		, Rate = IncrementAmount * round(Pricediff / nullif(IntervalAmount, 0.0) + .49, 0)
		, RouteMiles
	FROM (
		SELECT TOP 1 RateID = ID, FuelPriceFloor, IntervalAmount, IncrementAmount
			, PriceDiff = dbo.fnMaxDecimal(0, dbo.fnFuelPriceForOrigin(OriginID, OrderDate) - fuelPriceFloor)
			, RouteMiles
		FROM (
			SELECT R.ID, R.FuelPriceFloor, R.IntervalAmount, R.IncrementAmount, O.OrderDate, O.OriginID, RouteMiles = O.ActualMiles
			FROM dbo.viewOrder O
			CROSS APPLY dbo.fnCarrierFuelSurchargeRate(O.OrderDate, null, O.CustomerID, O.CarrierID, O.ProductGroupID, 1) R
			WHERE O.ID = @ID 
		) X
	) X2
GO
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper FuelSurcharge info for the specified order
/***********************************/
ALTER FUNCTION [dbo].[fnOrderShipperFuelSurchargeRate](@ID int) RETURNS TABLE AS RETURN
	SELECT RateID
		, Rate = IncrementAmount * round(Pricediff / nullif(IntervalAmount, 0.0) + .49, 0)
		, RouteMiles
	FROM (
		SELECT TOP 1 RateID = ID, FuelPriceFloor, IntervalAmount, IncrementAmount
			, PriceDiff = dbo.fnMaxDecimal(0, dbo.fnFuelPriceForOrigin(OriginID, OrderDate) - fuelPriceFloor)
			, RouteMiles
		FROM (
			SELECT R.ID, R.FuelPriceFloor, R.IntervalAmount, R.IncrementAmount, O.OrderDate, O.OriginID, RouteMiles = O.ActualMiles
			FROM dbo.viewOrder O
			CROSS APPLY dbo.fnShipperFuelSurchargeRate(O.OrderDate, null, O.CustomerID, O.ProductGroupID, 1) R
			WHERE O.ID = @ID 
		) X
	) X2
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OrderRejectRate info for the specified order
/***********************************/
ALTER FUNCTION [dbo].[fnOrderCarrierOrderRejectRate](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, R.Rate, R.RateTypeID, R.UomID
	FROM viewOrder O
	CROSS APPLY dbo.fnCarrierOrderRejectRate(O.OrderDate, null, O.RejectReasonID, O.CustomerID, O.CarrierID, O.ProductGroupID, O.OriginID, O.OriginStateID, O.OriginRegionID, 1) R
	WHERE O.ID = @ID
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper OrderRejectRate info for the specified order
/***********************************/
ALTER FUNCTION [dbo].[fnOrderShipperOrderRejectRate](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, R.Rate, R.RateTypeID, R.UomID
	FROM viewOrder O
	CROSS APPLY dbo.fnShipperOrderRejectRate(O.OrderDate, null, O.RejectReasonID, O.CustomerID, O.ProductGroupID, O.OriginID, O.OriginStateID, O.OriginRegionID, 1) R
	WHERE O.ID = @ID
GO

EXEC _spRebuildAllObjects
GO

COMMIT TRANSACTION
GO

--rollback
BEGIN TRAN
GO

CREATE PROCEDURE _spDropFunction(@name varchar(255)) AS
BEGIN
	SET NOCOUNT ON
	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(@name) AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
		EXEC ('DROP FUNCTION ' + @Name)
END
GO

EXEC _spDropFunction 'fnOrderCarrierAssessorialDetailsTSV'
GO
/*********************************************
-- Date Created: 22 Feb 2014
-- Author: Kevin Alons
-- Purpose: Concatenate the Assessorial Descriptions for a single order into a tab-separated string
*********************************************/
CREATE FUNCTION fnOrderCarrierAssessorialDetailsTSV(@OrderID int, @IncludeSystemRates bit = 0) RETURNS varchar(max) AS
BEGIN
	DECLARE @ret VARCHAR(max) 

	SELECT @ret = isnull(@ret + CHAR(13), '') 
		+ Name + isnull(CHAR(9) + RatePrefix + ltrim(Rate) + RateSuffix + CHAR(9) + ltrim(TypeID), '')
	FROM (
		SELECT ART.Name 
			, TypeID = AC.AssessorialRateTypeID
			, Rate = cast(AR.Rate as decimal(18, 4))
			, RatePrefix = CASE RT.ID WHEN 1 THEN '$' WHEN 2 THEN '$' WHEN 3 THEN '' WHEN 4 THEN '' END
			, RateSuffix = CASE RT.ID WHEN 1 THEN '/' + U.Abbrev WHEN 2 THEN '$' ELSE RT.Name END
			, AC.Amount
		FROM tblOrderSettlementCarrierAssessorialCharge AC
		JOIN tblAssessorialRateType ART ON ART.ID = AC.AssessorialRateTypeID
		LEFT JOIN tblCarrierAssessorialRate AR ON AR.ID = AC.AssessorialRateID
		LEFT JOIN tblRateType RT ON RT.ID = AR.RateTypeID 
		LEFT JOIN tblUom U ON U.ID = AR.UomID
		WHERE AC.OrderID = @OrderID
		  AND (@IncludeSystemRates = 1 OR AC.AssessorialRateTypeID > 4)
	) X
		
	RETURN (@ret)
END
GO
GRANT EXECUTE ON fnOrderCarrierAssessorialDetailsTSV TO dispatchcrude_iis_acct
GO

EXEC _spDropFunction 'fnOrderCarrierAssessorialAmountsTSV'
GO
/*********************************************
-- Date Created: 22 Feb 2014
-- Author: Kevin Alons
-- Purpose: Concatenate the Assessorial Descriptions for a single order into a tab-separated string
*********************************************/
CREATE FUNCTION fnOrderCarrierAssessorialAmountsTSV(@OrderID int, @IncludeSystemRates bit = 0) RETURNS varchar(max) AS
BEGIN
	DECLARE @ret VARCHAR(max) 

	SELECT @ret = isnull(@ret + CHAR(13), '') + ltrim(AC.Amount)
	FROM tblOrderSettlementCarrierAssessorialCharge AC
	WHERE AC.OrderID = @OrderID
		  AND (@IncludeSystemRates = 1 OR AC.AssessorialRateTypeID > 4)
	
	RETURN (@ret)
END
GO
GRANT EXECUTE ON fnOrderCarrierAssessorialAmountsTSV TO dispatchcrude_iis_acct
GO

EXEC _spDropFunction 'fnOrderShipperAssessorialDetailsTSV'
GO
/*********************************************
-- Date Created: 22 Feb 2014
-- Author: Kevin Alons
-- Purpose: Concatenate the Assessorial Descriptions for a single order into a tab-separated string
*********************************************/
CREATE FUNCTION fnOrderShipperAssessorialDetailsTSV(@OrderID int, @IncludeSystemRates bit = 0) RETURNS varchar(max) AS
BEGIN
	DECLARE @ret VARCHAR(max) 

	SELECT @ret = isnull(@ret + CHAR(13), '') 
		+ Name + isnull(CHAR(9) + RatePrefix + ltrim(Rate) + RateSuffix + CHAR(9) + ltrim(TypeID), '')
	FROM (
		SELECT ART.Name 
			, TypeID = AC.AssessorialRateTypeID
			, Rate = cast(AR.Rate as decimal(18, 4))
			, RatePrefix = CASE RT.ID WHEN 1 THEN '$' WHEN 2 THEN '$' WHEN 3 THEN '' WHEN 4 THEN '' END
			, RateSuffix = CASE RT.ID WHEN 1 THEN '/' + U.Abbrev WHEN 2 THEN '$' ELSE RT.Name END
			, AC.Amount
		FROM tblOrderSettlementShipperAssessorialCharge AC
		JOIN tblAssessorialRateType ART ON ART.ID = AC.AssessorialRateTypeID
		LEFT JOIN tblShipperAssessorialRate AR ON AR.ID = AC.AssessorialRateID
		LEFT JOIN tblRateType RT ON RT.ID = AR.RateTypeID 
		LEFT JOIN tblUom U ON U.ID = AR.UomID
		WHERE AC.OrderID = @OrderID
		  AND (@IncludeSystemRates = 1 OR AC.AssessorialRateTypeID > 4)
	) X
		
	RETURN (@ret)
END
GO
GRANT EXECUTE ON fnOrderShipperAssessorialDetailsTSV TO dispatchcrude_iis_acct
GO

EXEC _spDropFunction 'fnOrderShipperAssessorialAmountsTSV'
GO
/*********************************************
-- Date Created: 22 Feb 2014
-- Author: Kevin Alons
-- Purpose: Concatenate the Assessorial Descriptions for a single order into a tab-separated string
*********************************************/
CREATE FUNCTION fnOrderShipperAssessorialAmountsTSV(@OrderID int, @IncludeSystemRates bit = 0) RETURNS varchar(max) AS
BEGIN
	DECLARE @ret VARCHAR(max) 

	SELECT @ret = isnull(@ret + CHAR(13), '') + ltrim(AC.Amount)
	FROM tblOrderSettlementShipperAssessorialCharge AC
	WHERE AC.OrderID = @OrderID
	  AND (@IncludeSystemRates = 1 OR AC.AssessorialRateTypeID > 4)
	
	RETURN (@ret)
END
GO
GRANT EXECUTE ON fnOrderShipperAssessorialAmountsTSV TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
/***********************************/
ALTER PROCEDURE [dbo].[spRetrieveOrdersFinancialCarrier]
(
  @StartDate datetime = NULL
, @EndDate datetime = NULL
, @ShipperID int = -1 -- all
, @CarrierID int = -1 -- all
, @ProductGroupID int = -1 -- all 
, @ProducerID int = -1 -- all
, @OriginStateID int = -1 -- all 
, @OriginRegionID int = -1 -- all
, @BatchID int = NULL  -- either show unbatched (NULL), or the specified batch orders
, @OnlyShipperSettled bit = 0 
) AS BEGIN

	SELECT @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	
	SELECT DISTINCT OE.* 
		, ShipperSettled = cast(CASE WHEN OSS.BatchID IS NOT NULL THEN 1 ELSE 0 END as bit)
		, InvoiceOtherDetailsTSV = dbo.fnOrderCarrierAssessorialDetailsTSV(OE.ID, 0)
		, InvoiceOtherAmountsTSV = dbo.fnOrderCarrierAssessorialAmountsTSV(OE.ID, 0)
	FROM dbo.viewOrder_Financial_Carrier OE
	LEFT JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = OE.ID AND OSS.BatchID IS NOT NULL
	WHERE ID IN (
		SELECT O.ID
		FROM viewOrderBase O
		JOIN tblProduct P ON P.ID = O.ProductID
		JOIN tblOrigin OO ON OO.ID = O.OriginID
		LEFT JOIN tblOrderSettlementCarrier OSC ON OSC.OrderID = O.ID
		WHERE O.StatusID IN (4)  
		  AND (@ShipperID=-1 OR O.CustomerID=@ShipperID) 
		  AND (@CarrierID=-1 OR O.CarrierID=@CarrierID) 
		  AND (@ProductGroupID=-1 OR P.ProductGroupID=@ProductGroupID) 
		  AND (@ProducerID=-1 OR O.ProducerID=@ProducerID) 
		  AND (@OriginStateID=-1 OR OO.StateID=@OriginStateID) 
		  AND (@OriginRegionID=-1 OR OO.RegionID=@OriginRegionID) 
		  AND (@StartDate IS NULL OR O.OrderDate >= @StartDate) 
		  AND (@EndDate IS NULL OR O.OrderDate <= @EndDate)
		  AND ((@BatchID IS NULL AND OSC.BatchID IS NULL) OR OSC.BatchID = @BatchID)
	)
	  AND (@OnlyShipperSettled = 0 OR OSS.BatchID IS NOT NULL)
	ORDER BY OE.OriginDepartTimeUTC
END
GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
/***********************************/
ALTER PROCEDURE [dbo].[spRetrieveOrdersFinancialShipper]
(
  @StartDate datetime = NULL
, @EndDate datetime = NULL
, @ShipperID int = -1 -- all customers
, @OriginShipperRegion varchar(50) = NULL -- all origin.ShipperRegions
, @ProductGroupID int = -1 -- all product groups
, @ProducerID int = -1 -- all
, @OriginStateID int = -1 -- all 
, @OriginRegionID int = -1 -- all
, @BatchID int = NULL  -- either show unbatched (NULL), or the specified batch orders
) AS BEGIN

	SELECT @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	
	SELECT DISTINCT OE.* 
		, InvoiceOtherDetailsTSV = dbo.fnOrderShipperAssessorialDetailsTSV(OE.ID, 0)
		, InvoiceOtherAmountsTSV = dbo.fnOrderShipperAssessorialAmountsTSV(OE.ID, 0)
	FROM viewOrder_Financial_Shipper OE
	WHERE ID IN (
		SELECT O.ID
		FROM viewOrderBase O
		JOIN tblProduct P ON P.ID = O.ProductID
		JOIN tblOrigin OO ON OO.ID = O.OriginID
		LEFT JOIN tblOrderSettlementShipper ISC ON ISC.OrderID = O.ID
		WHERE O.StatusID IN (4)  
		  AND (@ShipperID=-1 OR O.CustomerID=@ShipperID) 
		  AND (@OriginShipperRegion IS NULL OR OriginShipperRegion = @OriginShipperRegion)
		  AND (@ProductGroupID=-1 OR P.ProductGroupID=@ProductGroupID) 
		  AND (@ProducerID=-1 OR O.ProducerID=@ProducerID) 
		  AND (@OriginStateID=-1 OR OO.StateID=@OriginStateID) 
		  AND (@OriginRegionID=-1 OR OO.RegionID=@OriginRegionID) 
		  AND (@StartDate IS NULL OR O.OrderDate >= @StartDate) 
		  AND (@EndDate IS NULL OR O.OrderDate <= @EndDate)
		  AND ((@BatchID IS NULL AND ISC.BatchID IS NULL) OR ISC.BatchID = @BatchID)
	)
	ORDER BY OE.OriginDepartTimeUTC
END 
GO

EXEC _spRebuildAllObjects
EXEC _spRebuildAllObjects
EXEC _spRebuildAllObjects
GO

COMMIT
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 20 Mar 2013
-- Description:	return the data for the TAX_ND_10A report (for the specified date range)
-- =============================================
ALTER PROCEDURE [dbo].[spReport_Tax_ND10A]
(
  @CustomerID int = -1
, @ProductID int = -1
, @StateID int = -1
, @StartDate datetime
, @EndDate datetime
, @IncludeUninvoiced bit = 0
)
AS BEGIN
	SELECT @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	
	SELECT O.FieldName
		, O.Operator
		, O.LeaseNum AS LeaseNumber
		, O.Name AS [Well Name and Number]
		, O.CTBNum AS [NDIC CTB No.]
		-- convert all units to Barrels for this report
		, cast(round(SUM(dbo.fnConvertUom(OD.OriginNetUnits, OD.OriginUomID, 1)), 0) as int) AS [Lease Total (Bbls)]
	FROM viewOrigin O
	JOIN viewOrder OD ON OD.OriginID = O.ID
	JOIN tblOrderSettlementShipper IC ON IC.OrderID = OD.ID AND (@IncludeUninvoiced = 1 OR IC.BatchID IS NOT NULL)
	WHERE O.OriginTypeID IN (2) -- WELLs ONLY
		AND OD.DeleteDateUTC IS NULL
		AND (@CustomerID = -1 OR OD.CustomerID = @CustomerID)
		AND (@ProductID = -1 OR OD.ProductID = @ProductID)
		AND (@StateID = -1 OR O.StateID = @StateID)
		AND OD.OrderDate BETWEEN @StartDate AND @EndDate
	GROUP BY O.FieldName, O.Operator, O.LeaseNum, O.Name, O.CTBNum
	ORDER BY O.FieldName, O.Operator, O.LeaseNum, O.Name, O.CTBNum
	END

GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 20 Mar 2013
-- Description:	return the data for the TAX_ND_10B report (for the specified date range)
-- =============================================
ALTER PROCEDURE [dbo].[spReport_Tax_ND10B]
(
  @CustomerID int = -1
, @ProductID int = -1
, @StateID int = -1
, @StartDate datetime
, @EndDate datetime
, @IncludeUninvoiced bit = 0
)
AS BEGIN
	SELECT @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	
	SELECT O.Name AS [Point Received]
		, OD.Customer AS [Purchaser]
		, OD.Destination
		-- convert all units to BARRELS (2) for this report
		, cast(round(SUM(dbo.fnConvertUom(OD.OriginNetUnits, OD.OriginUomID, 1)), 0) as int) AS [Lease Total (Bbls)]
	FROM viewOrder OD
	JOIN tblOrigin O ON O.ID = OD.OriginID
	JOIN tblOrderSettlementShipper IC ON IC.OrderID = OD.ID AND (@IncludeUninvoiced = 1 OR IC.BatchID IS NOT NULL)
	WHERE O.OriginTypeID IN (2) -- WELLs ONLY
		AND OD.DeleteDateUTC IS NULL
		AND (@CustomerID = -1 OR OD.CustomerID = @CustomerID)
		AND (@ProductID = -1 OR OD.ProductID = @ProductID)
		AND (@StateID = -1 OR O.StateID = @StateID)
		AND OD.OrderDate BETWEEN @StartDate AND @EndDate
	GROUP BY O.Name, OD.Customer, OD.Destination
	ORDER BY O.Name, OD.Customer, OD.Destination
	END

GO

/**********************************************************
-- Author: Kevin Alons
-- Date Created: 7 Oct 2014
-- Purpose: delete all unaudited, unpriced orders that have been "orphaned" and were deleted more than
		"Purge Deleted Orders Obsolete Days setting value" days
***********************************************************/		
ALTER PROCEDURE [dbo].[spMaint_PurgeObsoleteDeletedOrders] AS
BEGIN
	-- get the list of orders to PURGE
	SELECT O.ID 
	INTO #id
	FROM tblOrder O
	LEFT JOIN tblOrderSettlementShipper IOC ON IOC.OrderID = O.ID
	WHERE DeleteDateUTC IS NOT NULL AND DeleteDateUTC < DATEADD(day, -cast(dbo.fnSettingValue(34) as int), GETUTCDATE())
	  -- never purge AUDITed records
	  AND StatusID NOT IN (4)
	  -- never purge records that have been PRICED (for a SHIPPER)
	  AND IOC.OrderID IS NULL
	  	
	-- set the orders to a STATUS of DECLINED
	UPDATE tblOrder
		SET StatusID = 9
	WHERE ID IN (SELECT ID FROM #id)

	-- remvoe any child records for the soon to be deleted records
	DELETE FROM tblOrderSignature WHERE OrderID IN (SELECT ID FROM #id)
	DELETE FROM tblDriverLocation WHERE OrderID IN (SELECT ID FROM #id)
	DELETE FROM tblOrderTicket WHERE OrderID IN (SELECT ID FROM #id)
	
	-- PURGE (delete) the actual orders now
	DELETE FROM tblOrder WHERE ID IN (SELECT ID FROM #id)
END
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 19 Dec 2012
-- Description:	trigger to ensure the UomID for Carrier/Customer Route Rates matches that assigned to the Origin
-- =============================================
ALTER TRIGGER [dbo].[trigOrigin_IU] ON [dbo].[tblOrigin] AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	IF (UPDATE(UomID))
	BEGIN
		-- update matching CarrierRouteRates.UomID to match what is assigned to the new Origin
		UPDATE tblCarrierRouteRate 
		  SET UomID = i.UomID, LastChangeDateUTC = i.LastChangeDateUTC, LastChangedByUser = i.LastChangedByUser
		FROM tblCarrierRouteRate CRR
		JOIN tblRoute R ON R.ID = CRR.RouteID
		JOIN inserted i ON i.ID = R.OriginID
		JOIN deleted d ON d.ID = i.ID
		WHERE d.UomID <> i.UomID AND CRR.EffectiveDate <= dbo.fnDateOnly(GETDATE())

		-- update matching CustomerRouteRates.UomID to match what is assigned to the new Origin
		UPDATE tblShipperRouteRate 
		  SET UomID = i.UomID, LastChangeDateUTC = i.LastChangeDateUTC, LastChangedByUser = i.LastChangedByUser
		FROM tblShipperRouteRate CRR
		JOIN tblRoute R ON R.ID = CRR.RouteID
		JOIN inserted i ON i.ID = R.OriginID
		JOIN deleted d ON d.ID = i.ID
		WHERE d.UomID <> i.UomID AND CRR.EffectiveDate <= dbo.fnDateOnly(GETDATE())
	END
END
GO

CREATE PROCEDURE _spDropProcedure(@name varchar(255)) AS
BEGIN
	IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(@name) AND type in (N'P', N'PC'))
		EXEC ('DROP PROCEDURE ' + @name)
END
GO

EXEC _spDropFunction 'fnCustomerRouteRate'
EXEC _spDropProcedure 'spCarrierRouteRate_Add'
EXEC _spDropProcedure 'spCarrierDestinationWaitRate_Add'
EXEC _spDropProcedure 'spCarrierOrderRejectRate_Add'
EXEC _spDropProcedure 'spCarrierOriginWaitRate_Add'
EXEC _spDropProcedure 'spCustomerRouteRate_Add'
EXEC _spDropProcedure 'spCustomerDestinationWaitRate_Add'
EXEC _spDropProcedure 'spCustomerOrderRejectRate_Add'
EXEC _spDropProcedure 'spCustomerOriginWaitRate_Add'
GO

EXEC _spRebuildAllObjects
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RouteRate info for the specified criteria
/***********************************/
CREATE FUNCTION [dbo].[fnCarrierRouteRate](@StartDate date, @EndDate date, @OriginID int, @DestinationID int, @ShipperID int, @CarrierID int, @ProductGroupID int, @BestMatchOnly bit = 0) 
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT R.ID
			, Ranking =	dbo.fnRateRanking(@ShipperID, R.ShipperID, 4, 0)
				  + dbo.fnRateRanking(@CarrierID, R.CarrierID, 2, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 1, 0)
		FROM dbo.viewCarrierRouteRate R
		JOIN tblRoute RO ON RO.ID = R.RouteID
		WHERE (nullif(@OriginID, 0) IS NULL OR @OriginID = RO.OriginID)
		  AND (nullif(@DestinationID, 0) IS NULL OR @DestinationID = RO.DestinationID)
		  AND coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	)
	
	SELECT R.ID, RouteID, OriginID, DestinationID, ShipperID, CarrierID, ProductGroupID, Rate, RateTypeID, UomID, EffectiveDate, EndDate, NextEffectiveDate
	  , BestMatch, Ranking
	  , Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewCarrierRouteRate R
	JOIN (
		SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 3  -- ensure some type of match occurred on all 6 criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)

GO

/***********************************/
-- Date Created: 28 Jan 2014
-- Author: Kevin Alons
-- Purpose: return Order records with necessary JOINed fields for OrderList in MVC pages
/***********************************/
ALTER PROCEDURE [dbo].[spMVCOrders](@driverID int, @carrierID int) AS BEGIN
	SELECT O.* 
	FROM viewOrderDisplay O
	JOIN viewOrder vO ON vO.ID = O.ID
	WHERE O.DeleteDateUTC IS NULL 
	  AND (O.DriverID = @driverID
			OR O.CarrierID = @carrierID
			OR (O.CarrierID IS NOT NULL AND @carrierID = -1))
	  AND vO.PrintStatusID IN (2,7,8,3)

END
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier DestinationWait data info for the specified order
/***********************************/
ALTER FUNCTION fnOrderCarrierDestinationWaitData(@ID int) RETURNS TABLE AS RETURN
	SELECT RateID, BillableMinutes, BillableHours, Amount = BillableHours * Rate
	FROM (
		SELECT RateID, BillableMinutes
			, BillableHours = dbo.fnComputeBillableWaitHours(BillableMinutes, SubUnitID, RoundingTypeID)
			, Rate
		FROM (
			SELECT RateID = WR.ID
				, Rate
				, BillableMinutes = dbo.fnMaxInt(WR.Minutes - WFP.DestThresholdMinutes, 0)
				, WFP.SubUnitID, WFP.RoundingTypeID
			FROM dbo.fnOrderCarrierDestinationWaitRate(@ID) WR 
			CROSS JOIN dbo.fnOrderCarrierWaitFeeParameter(@ID) WFP 
		) X
	) X2
	WHERE BillableHours > 0.0
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OrderReject data info for the specified order
/***********************************/
ALTER FUNCTION fnOrderCarrierOrderRejectData(@ID int, @Units decimal(18, 10), @UomID int) RETURNS TABLE AS RETURN
	SELECT RateID = RR.ID
		, Amount = dbo.fnRateToAmount(RateTypeID, @Units, @UomID, Rate, UomID, NULL
			, CASE WHEN RateTypeID = 3 THEN (SELECT TOP 1 OrderRejectAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID) ELSE NULL END)
	FROM dbo.fnOrderCarrierOrderRejectRate(@ID) RR 
	JOIN tblOrder O ON O.ID = @ID
	WHERE O.Rejected = 1
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OrderRejectRate info for the specified order
/***********************************/
ALTER FUNCTION fnOrderCarrierOrderRejectRate(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, R.Rate, R.RateTypeID, R.UomID
	FROM viewOrder O
	CROSS APPLY dbo.fnCarrierOrderRejectRate(O.OrderDate, null, O.RejectReasonID, O.CustomerID, O.CarrierID, O.ProductGroupID, O.OriginID, O.OriginStateID, O.OriginRegionID, 1) R
	WHERE O.ID = @ID
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OriginWait data info for the specified order
/***********************************/
ALTER FUNCTION fnOrderCarrierOriginWaitData(@ID int) RETURNS TABLE AS RETURN
	SELECT RateID, BillableMinutes, BillableHours, Amount = BillableHours * Rate, WaitFeeParameterID
	FROM (
		SELECT RateID, BillableMinutes
			, BillableHours = dbo.fnComputeBillableWaitHours(BillableMinutes, SubUnitID, RoundingTypeID)
			, Rate
			, WaitFeeParameterID
		FROM (
			SELECT RateID = WR.ID
				, Rate
				, BillableMinutes = dbo.fnMaxInt(WR.Minutes - WFP.OriginThresholdMinutes, 0)
				, WaitFeeParameterID = WFP.ID, WFP.SubUnitID, WFP.RoundingTypeID
			FROM dbo.fnOrderCarrierOriginWaitRate(@ID) WR 
			CROSS APPLY dbo.fnOrderCarrierWaitFeeParameter(@ID) WFP
		) X
	) X2
	WHERE BillableHours > 0.0
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OriginWaitRate info for the specified order
/***********************************/
ALTER FUNCTION fnOrderCarrierOriginWaitRate(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, Minutes = O.DestMinutes, R.Rate
	FROM viewOrder O
	CROSS APPLY dbo.fnCarrierOriginWaitRate(O.OrderDate, null, O.OriginWaitReasonID, O.CustomerID, O.CarrierID, O.ProductGroupID, O.OriginID, O.OriginStateID, O.OriginRegionID, 1) R
	WHERE O.ID = @ID
GO

/**********************************************
Date Created: 13 Feb 2015
Author: Kevin Alons
Purpose: retrieve the combined thresholdminutes for a single order (used by the Driver Sync logic)
**********************************************/
ALTER FUNCTION fnOrderCombinedThresholdMinutes(@ID int)
RETURNS TABLE AS RETURN
	SELECT OriginThresholdMinutes = dbo.fnMaxInt(dbo.fnMaxInt(OCWFP.OriginThresholdMinutes, OSWFP.OriginThresholdMinutes), coalesce(OCWFP.OriginThresholdMinutes, OSWFP.OriginThresholdMinutes, STM.Value))
		, DestThresholdMinutes = dbo.fnMaxInt(dbo.fnMaxInt(OCWFP.DestThresholdMinutes, OSWFP.DestThresholdMinutes), coalesce(OCWFP.DestThresholdMinutes, OSWFP.DestThresholdMinutes, STM.Value))
	FROM dbo.fnOrderCarrierWaitFeeParameter(@ID) OCWFP
	CROSS JOIN dbo.fnOrderShipperWaitFeeParameter(@ID) OSWFP
	CROSS JOIN (SELECT Value FROM tblSetting WHERE ID = 6) STM
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper AssessorialRate Amounts info for the specified order
/***********************************/
ALTER FUNCTION fnOrderShipperAssessorialAmounts(@ID int, @Units decimal(18, 10), @UomID int) RETURNS TABLE AS RETURN
	SELECT AssessorialRateTypeID = TypeID, RateID, RateTypeID, Amount
	FROM (
		SELECT TypeID, RateID = ID, RateTypeID
			, Amount = dbo.fnRateToAmount(RateTypeID, @Units, @UomID, Rate, UomID, OSS.LoadAmount, NULL)
		FROM dbo.fnOrderShipperAssessorialRates(@ID)
		JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = @ID
	) X
	WHERE Amount IS NOT NULL
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper DestinationWait data info for the specified order
/***********************************/
ALTER FUNCTION fnOrderShipperDestinationWaitData(@ID int) RETURNS TABLE AS RETURN
	SELECT RateID, BillableMinutes, BillableHours, Amount = BillableHours * Rate
	FROM (
		SELECT RateID, BillableMinutes
			, BillableHours = dbo.fnComputeBillableWaitHours(BillableMinutes, SubUnitID, RoundingTypeID)
			, Rate
		FROM (
			SELECT RateID = WR.ID
				, Rate
				, BillableMinutes = dbo.fnMaxInt(WR.Minutes - WFP.DestThresholdMinutes, 0)
				, WFP.SubUnitID, WFP.RoundingTypeID
			FROM dbo.fnOrderShipperDestinationWaitRate(@ID) WR 
			CROSS JOIN dbo.fnOrderShipperWaitFeeParameter(@ID) WFP 
		) X
	) X2
	WHERE BillableHours > 0.0
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper RateSheetRangeRate info for the specified order
/***********************************/
ALTER FUNCTION fnOrderShipperLoadAmount(@ID int, @Units decimal(18, 10), @UomID int) RETURNS TABLE AS RETURN
	SELECT TOP 1 RouteRateID, RangeRateID
		, Amount = dbo.fnRateToAmount(RateTypeID, @Units, @UomID, Rate, UomID, NULL, NULL)
	FROM (
		SELECT SortID = 1, RouteRateID = ID, RangeRateID = NULL, RateTypeID, UomID, Rate FROM dbo.fnOrderShipperRouteRate(@ID)
		UNION SELECT 2, NULL, ID, RateTypeID, UomID, Rate FROM dbo.fnOrderShipperRateSheetRangeRate(@ID)
	) X
	ORDER BY SortID
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper OriginWaitRate info for the specified order
/***********************************/
ALTER FUNCTION [dbo].[fnOrderShipperOriginWaitRate](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, Minutes = O.OriginMinutes, R.Rate
	FROM viewOrder O
	CROSS APPLY dbo.fnShipperOriginWaitRate(O.OrderDate, null, O.OriginWaitReasonID, O.CustomerID, O.ProductGroupID, O.OriginID, O.OriginStateID, O.OriginRegionID, 1) R
	WHERE O.ID = @ID
GO
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper OrderRejectRate info for the specified order
/***********************************/
ALTER FUNCTION [dbo].[fnOrderShipperOrderRejectRate](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, R.Rate, R.RateTypeID, R.UomID
	FROM viewOrder O
	CROSS APPLY dbo.fnShipperOrderRejectRate(O.OrderDate, null, O.RejectReasonID, O.CustomerID, O.ProductGroupID, O.OriginID, O.OriginStateID, O.OriginRegionID, 1) R
	WHERE O.ID = @ID
GO
/***********************************/
-- Date Created: 21 Dec 2014/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper OriginWaitRate info for the specified order
/***********************************/
ALTER FUNCTION [dbo].[fnOrderShipperOriginWaitRate](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, Minutes = O.OriginMinutes, R.Rate
	FROM viewOrder O
	CROSS APPLY dbo.fnShipperOriginWaitRate(O.OrderDate, null, O.OriginWaitReasonID, O.CustomerID, O.ProductGroupID, O.OriginID, O.OriginStateID, O.OriginRegionID, 1) R
	WHERE O.ID = @ID
GO
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper OrderRejectRate info for the specified order
/***********************************/
ALTER FUNCTION [dbo].[fnOrderShipperOrderRejectRate](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, R.Rate, R.RateTypeID, R.UomID
	FROM viewOrder O
	CROSS APPLY dbo.fnShipperOrderRejectRate(O.OrderDate, null, O.RejectReasonID, O.CustomerID, O.ProductGroupID, O.OriginID, O.OriginStateID, O.OriginRegionID, 1) R
	WHERE O.ID = @ID
GO
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper DestinationWaitRate info for the specified order
/***********************************/
ALTER FUNCTION [dbo].[fnOrderShipperDestinationWaitRate](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, Minutes = O.DestMinutes, R.Rate
	FROM viewOrder O
	CROSS APPLY dbo.fnShipperDestinationWaitRate(O.OrderDate, null, O.DestWaitReasonID, O.CustomerID, O.ProductGroupID, O.DestinationID, O.DestStateID, O.DestRegionID, 1) R
	WHERE O.ID = @ID
GO
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OriginWaitRate info for the specified order
/***********************************/
ALTER FUNCTION [dbo].[fnOrderCarrierOriginWaitRate](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, Minutes = O.OriginMinutes, R.Rate
	FROM viewOrder O
	CROSS APPLY dbo.fnCarrierOriginWaitRate(O.OrderDate, null, O.OriginWaitReasonID, O.CustomerID, O.CarrierID, O.ProductGroupID, O.OriginID, O.OriginStateID, O.OriginRegionID, 1) R
	WHERE O.ID = @ID
GO
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier DestinationWaitRate info for the specified order
/***********************************/
ALTER FUNCTION [dbo].[fnOrderCarrierDestinationWaitRate](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, Minutes = O.DestMinutes, R.Rate
	FROM viewOrder O
	CROSS APPLY dbo.fnCarrierDestinationWaitRate(O.OrderDate, null, O.DestWaitReasonID, O.CustomerID, O.CarrierID, O.ProductGroupID, O.DestinationID, O.DestStateID, O.DestRegionID, 1) R
	WHERE O.ID = @ID
GO
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper DestinationWaitRate info for the specified order
/***********************************/
ALTER FUNCTION [dbo].[fnOrderShipperDestinationWaitRate](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, Minutes = O.DestMinutes, R.Rate
	FROM viewOrder O
	CROSS APPLY dbo.fnShipperDestinationWaitRate(O.OrderDate, null, O.DestWaitReasonID, O.CustomerID, O.ProductGroupID, O.DestinationID, O.DestStateID, O.DestRegionID, 1) R
	WHERE O.ID = @ID
GO
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OriginWaitRate info for the specified order
/***********************************/
ALTER FUNCTION [dbo].[fnOrderCarrierOriginWaitRate](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, Minutes = O.OriginMinutes, R.Rate
	FROM viewOrder O
	CROSS APPLY dbo.fnCarrierOriginWaitRate(O.OrderDate, null, O.OriginWaitReasonID, O.CustomerID, O.CarrierID, O.ProductGroupID, O.OriginID, O.OriginStateID, O.OriginRegionID, 1) R
	WHERE O.ID = @ID
GO
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier DestinationWaitRate info for the specified order
/***********************************/
ALTER FUNCTION [dbo].[fnOrderCarrierDestinationWaitRate](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, Minutes = O.DestMinutes, R.Rate
	FROM viewOrder O
	CROSS APPLY dbo.fnCarrierDestinationWaitRate(O.OrderDate, null, O.DestWaitReasonID, O.CustomerID, O.CarrierID, O.ProductGroupID, O.DestinationID, O.DestStateID, O.DestRegionID, 1) R
	WHERE O.ID = @ID
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper OriginWait data info for the specified order
/***********************************/
ALTER FUNCTION fnOrderShipperOriginWaitData(@ID int) RETURNS TABLE AS RETURN
	SELECT RateID, BillableMinutes, BillableHours, Amount = BillableHours * Rate, WaitFeeParameterID
	FROM (
		SELECT RateID, BillableMinutes
			, BillableHours = dbo.fnComputeBillableWaitHours(BillableMinutes, SubUnitID, RoundingTypeID)
			, Rate
			, WaitFeeParameterID
		FROM (
			SELECT RateID = WR.ID
				, Rate
				, BillableMinutes = dbo.fnMaxInt(WR.Minutes - WFP.OriginThresholdMinutes, 0)
				, WaitFeeParameterID = WFP.ID, WFP.SubUnitID, WFP.RoundingTypeID
			FROM dbo.fnOrderShipperOriginWaitRate(@ID) WR 
			CROSS APPLY dbo.fnOrderShipperWaitFeeParameter(@ID) WFP
		) X
	) X2
	WHERE BillableHours > 0.0
GO