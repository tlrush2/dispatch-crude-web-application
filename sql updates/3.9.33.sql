SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.9.32'
SELECT  @NewVersion = '3.9.33'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Fix for CODE export (Now removes illegal characters from tank numbers).  Removed two old/unused triggers (housekeeping).'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/*=============================================
	Author:			Ben Bloodworth
	Create date:	5 Jun 2015
	Description:	Displays the base information for the CODE export. Please note that some values could change per DispatchCrude customer and thus it may
					be better to retrieve these settings from the Data Exchange settings instead of this view.
					
	Updates:		8/21/15 - BB: Added CASE statments to account for Nulls in gravity readings and added additional filters for rejects and ticket types.
					8/26/15 - BB: Altered tank number field to use OriginTankText instead of TankNum.  Altered Property_Code per instructions from customer.
					9/24/15 - BB: Added Net Volume tickets to allowed ticket types list in where clause.
				   10/05/15 - BB: Changed export to use Gross Volume instead of Net Volume (Changed per Cash Rainer at CW2).
	 - 3.9.19.11 - 10/08/15 - KDA - fix several varchar -> numeric conversions that could fail with entered data 
							(CarrierTicketNum too long, LeaseNum not numeric, T.OriginTankText not numeric, use of fnIsNumeric() function)
				   10/14/15 - BB: removed KDA's changes in 3.9.19.11 because they broke the export and the customer noticed and called us on it!
									the lease numbers no longer printed and were being zero filled.  HUGE NO NO!!
				   10/28/15 - BB: Customer now says to ALWAYS send BSW value if it is collected.  Added all relevant ticket types to BSW field logic .
				   12/10/15 - KDA+BB: ensure any '-' characters in TankNum are eliminated prior to export processing (will cause a crash if not removed)
=============================================*/
ALTER VIEW [dbo].[viewOrderExport_CODE_Base] AS
SELECT 
	OrderID = O.ID
	,OrderTicketID = T.ID
	,Record_ID = '2'
	,Company_ID = C.CodeDXCode
	,Ticket_Type = TT.CodeDXCode
	,Transmit_Ind = '2'  -- Hard coded per Angie Toews (PDS Energy) but may need to be changed to dynamic if the customer says so
	,Run_Type = 1
	,Ticket_Number = CASE WHEN ISNUMERIC(RIGHT(T.CarrierTicketNum,1)) = 0 THEN dbo.fnFixedLenStr(T.CarrierTicketNum,7) 
						  ELSE dbo.fnFixedLenNum(T.CarrierTicketNum,7,0,0) 
					 END
	,Run_Ticket_Date = dbo.fnDateMMDDYY(O.OrderDate)
	
	--Property code is a non-standard CODE field for Plains (per Steve Carver).  It is a mixture of origin leasnum and destination station num
	,Property_Code = (CASE WHEN O.LeaseNum IS NULL THEN '000000' ELSE CAST(dbo.fnFixedLenNum(O.LeaseNum,6,0,0) AS VARCHAR(6)) END)
					+ '000'
					+ (CASE WHEN O.DestStation IS NULL THEN '00000' ELSE dbo.fnFixedLenStr(O.DestStation,5) END)
					
	,Gathering_Charge = '0'  -- Hard coded per Angie Toews (PDS Energy) but may need to be changed to dynamic if the customer says so	
	,Tank_Meter_Num = CAST(dbo.fnFixedLenNum(REPLACE(T.OriginTankText, '-', ''), 6, 0, 0) AS VARCHAR(6)) + '0'	
	,Adjustment_Ind = '0' -- Hard coded per Angie Toews (PDS Energy) but may need to be changed to dynamic if the customer says so
	,Opening_Reading = dbo.fnOpenCloseReadingCODEFormat(T.ID,TT.CodeDXCode,0)
	,Opening_Date = dbo.fnDateMMDD(O.OrderDate)
	,Closing_Reading = dbo.fnOpenCloseReadingCODEFormat(T.ID,TT.CodeDXCode,1)
	,Closing_Date = dbo.fnDateMMDD(O.DestDepartTime)
	,Meter_Factor = CASE WHEN T.MeterFactor IS NULL THEN '00000000' 
						 ELSE dbo.fnFixedLenNum(T.MeterFactor, 2, 6, 1) 
					END
	,Shrinkage_Incrustation = '1000000' -- Hard coded per Angie Toews (PDS Energy) but may need to be changed to dynamic if the customer says so
	,Opening_Temperature = CASE WHEN T.ProductHighTemp IS NULL THEN '0000' 
								ELSE dbo.fnFixedLenNum(T.ProductHighTemp, 3, 1, 1) 
						   END
	,Closing_Temperature = CASE WHEN T.ProductLowTemp IS NULL THEN '0000' 
								ELSE dbo.fnFixedLenNum(T.ProductLowTemp, 3, 1, 1) 
						   END
	,BSW = CASE WHEN TT.ID IN (1,2,3,6,7) THEN dbo.fnFixedLenNum(T.ProductBSW, 2, 2, 1) --Basic, CAN Basic, and Gross Volume tickets are exluded because they dont have this value					
				ELSE '0000'
		   END
	,Observed_Gravity =	CASE WHEN T.ProductObsGravity IS NULL THEN '000'
							 ELSE dbo.fnFixedLenNum(T.ProductObsGravity, 2, 1, 1)
						END
	,Observed_Temperature = dbo.fnFixedLenNum(T.ProductObsTemp, 3, 1, 1)
	,Pos_Neg_Code = '0' -- Hard coded per Angie Toews (PDS Energy) but may need to be changed to dynamic if the customer says so
	,Total_Net_Volume = dbo.fnFixedLenNum(T.GrossUnits, 7, 2, 1) -- BB 10/7/15 this sends GROSS on purpose. this change is per Plains Marketing
	,Shippers_Net_Volume = dbo.fnFixedLenNum(T.GrossUnits, 7, 2, 1) -- BB 10/7/15 this sends GROSS on purpose. this change is per Plains Marketing
	,Corrected_Gravity = CASE WHEN T.Gravity60F IS NULL THEN '000'
							  ELSE dbo.fnFixedLenNum(T.Gravity60F, 2, 1, 1)
						 END
	,Product_Code = dbo.fnFixedLenStr('',3)	--Leave 3 blank spaces unless product is not crude oil. For others lookup PETROEX code	
	,Transmission_Date = dbo.fnFixedLenStr('',3) --Leave 3 blank spaces. Filled in by export reciever.
FROM viewOrderLocalDates O
	JOIN dbo.viewOrderCustomerFinalExportPending EP ON EP.ID = O.ID
	LEFT JOIN viewOrderTicket T ON T.OrderID = O.ID AND T.DeleteDateUTC IS NULL
	LEFT JOIN tblTicketType TT ON TT.ID = O.TicketTypeID
	LEFT JOIN tblCustomer C ON C.ID = O.CustomerID
	LEFT JOIN tblProduct P ON P.ID = O.ProductID
WHERE O.DeleteDateUTC IS NULL
	AND O.Rejected = 0 -- No rejected orders
	AND T.Rejected = 0 -- No rejected tickets
	AND O.StatusID = 4 -- Only Audited orders
	AND TT.ID IN (1,2,7) -- Only Gauge Run, Gauge Net, and Net Volume tickets
GO

/*  Refresh dependent view */
sp_refreshview  viewOrderExport_CODE
GO


/* BB - 11/24/15 - Housekeeping - removing two old/unused triggers with approval from Kevin */
EXEC _spDropTrigger 'trigCarrier_D_Rates'
EXEC _spDropTrigger 'trigRegion_D_Rates'
GO


COMMIT
SET NOEXEC OFF