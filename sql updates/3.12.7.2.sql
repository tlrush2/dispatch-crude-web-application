-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.12.7.1'
SELECT  @NewVersion = '3.12.7.2'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1485 - Update dashboard to handle null dest volumens'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/********************************************
 Date Created: 2016/05/16
 Author: Joe Engler
 Purpose: Retrieve the DAILY Allocation + Production Units for the specified selection criteria + Date Range
 Changes:
	3.12.7.1	2016/06/14	JAE		- Add check to use origin volume if destination volume is null
	3.12.7.2	2016/06/14	JAE		- Added ProductGroupID filter		
********************************************/
ALTER PROCEDURE spAllocationDestinationShipper
( 
    @RegionID int = -1,
	@DestinationID int = -1,
	@ShipperID int = -1,
	@ProductGroupID int = -1,
	@StartDate date = NULL,
	@EndDate date = NULL,
	@UomID int = 1
) AS BEGIN

SET NOCOUNT ON 
	DECLARE @ret TABLE
	(
	  TicketCount INT,
	  TotalGross DECIMAL(18,10),
	  DestinationID INT,
	  Destination VARCHAR(50),
	  ShipperID INT,
	  Shipper VARCHAR(40),
	  ProductGroupID int,
	  ProductGroup VARCHAR(25),
	  DailyUnits DECIMAL(18,10),
	  AllocationStartDate DATE,
	  AllocationEndDate DATE,
	  AllocatedUnits DECIMAL(18,10),
	  DaysLeft INT
	) 
	
	IF @StartDate IS NULL SET @StartDate = dbo.fnFirstDOM(getdate())
	IF @EndDate IS NULL SET @EndDate = dbo.fnLastDOM(@StartDate)
	

 SELECT TicketCount = MAX(TicketCount), -- no need to sum in grouping since line items already aggregated totals 
		TotalGross = MAX(TotalGross), -- no need to sum in grouping since line items already aggregated totals 
 		DestinationID,
		Destination,
		CustomerID,
	    Customer,
	    ProductGroupID,
	    ProductGroup,
		DailyUnits = SUM(AllocatedUnits) / (DATEDIFF(DAY, MIN(qStart), MAX(qEnd)) + 1), -- Recalculate daily units since allocations may be weighted differently
		AllocationStartDate = MIN(qStart),
		AllocationEndDate = MAX(qEnd),
		AllocatedUnits = SUM(AllocatedUnits),
		DaysLeft = MAX(QualifiedDaysLeft)

   FROM (
		 SELECT *,
				AllocatedUnits = DailyUnits * (DATEDIFF(DAY, qStart, qEnd) + 1), -- broken down into daily average * number of days
 				QualifiedDaysLeft = CASE WHEN DATEDIFF(DAY, GETDATE(), qEnd) < 0 THEN 0 -- target date already past, set to zero
										 ELSE DATEDIFF(DAY, GETDATE(), qEnd) + 1 END -- include today

		   FROM (
		         -- Get orders for the given date range and merge with any allocations
				 -- Null allocations will be considered excess
				 SELECT TicketCount = COUNT(*),
						TotalGross = SUM(dbo.fnConvertUOM(COALESCE(DestGrossUnits, OriginGrossUnits, 0), DestUomID, @UomID)), 
						o.DestinationID,
						o.Destination,
						o.CustomerID,
						o.Customer,
						o.ProductGroupID,
						o.ProductGroup,
						Target = dbo.fnConvertUom(Units, ads.UomID, @UomID),
						DailyUnits = dbo.fnConvertUom(DailyUnits, ads.UomID, @UomID),
						qStart = CASE WHEN EffectiveDate > @StartDate THEN EffectiveDate -- get qualified start and end date
										 ELSE @StartDate END,
						qEnd = CASE WHEN EndDate < @EndDate THEN EndDate
										 ELSE @EndDate END
				   FROM viewOrder o 
 				   LEFT JOIN viewAllocationDestinationShipper ads ON o.DestinationID = ads.DestinationID
							AND o.CustomerID = ads.ShipperID
							AND o.ProductGroupID = ads.ProductGroupID
							AND (EffectiveDate BETWEEN @StartDate AND @EndDate OR EndDate BETWEEN @StartDate AND @EndDate)

 				  WHERE DeliverDate BETWEEN @StartDate AND @EndDate
					AND o.Rejected = 0 
					AND o.DeleteDateUTC IS NULL
					AND (@RegionID = -1 OR DestRegionID = @RegionID)
					AND (@DestinationID = -1 OR o.DestinationID = @DestinationID)
					AND (@ShipperID = -1 OR CustomerID = @ShipperID)
					AND (@ProductGroupID = -1 OR o.ProductGroupID = @ProductGroupID)
			   GROUP BY o.DestinationID, o.Destination, 
			            CustomerID, Customer, 
						o.ProductGroupID, o.ProductGroup,
						Units,
						ads.UomID, 
						DailyUnits,
						CASE WHEN EffectiveDate > @StartDate THEN EffectiveDate ELSE @StartDate END,
						CASE WHEN EndDate < @EndDate THEN EndDate ELSE @EndDate END
				 HAVING SUM(COALESCE(DestGrossUnits, OriginGrossUnits, 0)) > 0

				UNION

				-- Include any allocation with no delivered orders (or none yet)
				SELECT 0, 
						0, 
						DestinationID,
						Destination,
						ShipperID,
						Shipper,
						ProductGroupID,
						ProductGroup, 
						dbo.fnConvertUom(Units, ads.UomID, @UomID),
						dbo.fnConvertUom(ads.DailyUnits, ads.UomID, @UomID),
						qStart = CASE WHEN EffectiveDate > @StartDate THEN EffectiveDate -- get qualified start and end date
										ELSE @StartDate END,
						qEnd = CASE WHEN EndDate < @EndDate THEN EndDate
										ELSE @EndDate END

					FROM viewAllocationDestinationShipper ads
					WHERE (EffectiveDate BETWEEN @StartDate AND @EndDate OR EndDate BETWEEN @StartDate AND @EndDate)
					AND (@RegionID = -1 OR RegionID = @RegionID)
					AND (@ShipperID = -1 OR ShipperID = @ShipperID)
					AND (@ProductGroupID = -1 OR ProductGroupID = @ProductGroupID)
					AND (@DestinationID = -1 OR DestinationID = @DestinationID)
					AND NOT EXISTS (SELECT 1 FROM viewOrder o 
									WHERE DeliverDate BETWEEN @StartDate AND @EndDate
										AND o.DestinationID = ads.DestinationID
										AND o.CustomerID = ads.ShipperID
										AND o.ProductGroupID = ads.ProductGroupID)
				) q2
		) q

   GROUP BY DestinationID,
			Destination,
			CustomerID,
			Customer,
			ProductGroupID,
			ProductGroup

END

GO


COMMIT
SET NOEXEC OFF