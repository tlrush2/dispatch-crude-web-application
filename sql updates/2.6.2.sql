/* 
	fixes to viewCarrier|viewCustomer to properly label the Uom fields
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.6.1'
SELECT  @NewVersion = '2.6.2'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

/***********************************/
-- Date Created: 4 Apr 2013
-- Author: Kevin Alons
-- Purpose: return Carrier records with translated "friendly" values
/***********************************/
ALTER VIEW [dbo].[viewCarrier] AS
SELECT C.*
	, cast(CASE WHEN C.DeleteDateUTC IS NULL THEN 1 ELSE 0 END as bit) AS Active
	, CT.Name AS CarrierType
	, S.Abbreviation AS StateAbbrev
	, SF.Name AS SettlementFactor 
	, UOM.Name AS MinSettlementUOM
	, UOM.Abbrev AS MinSettlementUomShort
FROM tblCarrier C 
JOIN tblCarrierType CT ON CT.ID = C.CarrierTypeID
LEFT JOIN tblState S ON S.ID = C.StateID
LEFT JOIN tblSettlementFactor SF ON SF.ID = C.SettlementFactorID
LEFT JOIN dbo.tblUom UOM ON UOM.ID = C.MinSettlementUomID

GO

/***********************************/
-- Date Created: 27 Jan 2013
-- Author: Kevin Alons
-- Purpose: return Customer records with "translated friendly" values for FK relationships
/***********************************/
ALTER VIEW [dbo].[viewCustomer] AS
SELECT C.*
	, S.Abbreviation AS StateAbbrev
	, S.FullName AS State
	, SF.Name AS SettlementFactor
	, UOM.Name AS MinSettlementUOM
	, UOM.Abbrev AS MinSettlementUomShort
FROM dbo.tblCustomer C
LEFT JOIN dbo.tblState S ON S.ID = C.StateID
LEFT JOIN tblSettlementFactor SF ON SF.ID = C.SettlementFactorID
LEFT JOIN dbo.tblUom UOM ON UOM.ID = C.MinSettlementUomID

GO

COMMIT
SET NOEXEC OFF