/*
	-- more Report Center functionality - add viewUserReportDefinition
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.0.3'
SELECT  @NewVersion = '3.0.4'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

/*************************************************/
-- Date Created: 26 Jul 2014
-- Author: Kevin Alons
-- Purpose: return the tblUserReportDefinition along with any any relevant tblReportDefinition fields
/*************************************************/
CREATE VIEW [dbo].[viewUserReportDefinition] AS
	SELECT URD.*
		, RD.SourceTable
	FROM tblUserReportDefinition URD
	JOIN tblReportDefinition RD ON RD.ID = URD.ReportID

GO

GRANT SELECT ON viewUserReportDefinition TO dispatchcrude_iis_acct
GO

/***********************************************/
-- Date Created: 27 Jul 2014
-- Author: Kevin Alons
-- Purpose: add related JOINed fields to the tblUserReportColumnDefinition table results
/***********************************************/
CREATE VIEW [dbo].[viewUserReportColumnDefinition] AS
	SELECT URCD.ID
		, URCD.UserReportID
		, URCD.ReportColumnID
		, URCD.Caption
		, DataFormat = isnull(URCD.DataFormat, RCD.DataFormat)
		, URCD.SortNum
		, URCD.FilterOperatorID
		, URCD.FilterValue1
		, URCD.FilterValue2
		, URCD.CreateDateUTC, URCD.CreatedByUser
		, URCD.LastChangeDateUTC, URCD.LastChangedByUser
		, RCD.DataField
		, BaseCaption = RCD.Caption
		, OutputCaption = coalesce(URCD.Caption, RCD.Caption, RCD.DataField)
		, RCD.FilterTypeID
		, FilterDataField = isnull(RCD.FilterDataField, RCD.DataField)
		, RCD.FilterDropDownSql
		, RCD.FilterAllowCustomText
		, FilterOperator = RFO.Name
		, FilterValues = CASE 
				WHEN RFO.ID IN (1,4,5) OR RCD.FilterAllowCustomText = 1 THEN FilterValue1 + ISNULL(' - ' + FilterValue2, '')
				ELSE 
					CASE WHEN FilterValue1 IS NOT NULL THEN '&lt;filtered&gt;' 
						 ELSE NULL 
					END 
			END
	FROM tblUserReportColumnDefinition URCD
	JOIN tblReportColumnDefinition RCD ON RCD.ID = URCD.ReportColumnID
	JOIN tblReportFilterOperator RFO ON RFO.ID = URCD.FilterOperatorID

GO

GRANT SELECT ON viewUserReportColumnDefinition TO dispatchcrude_iis_acct
GO

UPDATE tblReportColumnDefinition
  SET DataField = 'PrintStatus', Caption = 'Status'
WHERE ID = 12
GO
UPDATE tblReportColumnDefinition
  SET DataFormat='m/d/yyyy'
WHERE ID = 4
GO
DELETE FROM tblUserReportColumnDefinition WHERE ReportColumnID = 39
GO
DELETE FROM tblReportColumnDefinition WHERE ID = 39
GO

/*******************************************/
-- Date Created: 23 Jul 2014
-- Author: Kevin Alons
-- Purpose: clone an existing UserReport (defaults to a full clone with columns, but can do an AddNew otherwise)
/*******************************************/
ALTER PROCEDURE [dbo].[spCloneUserReport]
(
  @userReportID int
, @reportName varchar(50)
, @userName varchar(100)
, @includeColumns bit = 1
, @newID int = 0 OUTPUT 
) AS
BEGIN
	SET NOCOUNT ON
	INSERT INTO tblUserReportDefinition (Name, ReportID, UserName, CreatedByUser)
		SELECT @reportName, ReportID, @userName, isnull(@userName, 'Administrator')
		FROM tblUserReportDefinition WHERE ID = @userReportID
	SET @newID = SCOPE_IDENTITY()
	IF (@includeColumns = 1)
	BEGIN
		INSERT INTO tblUserReportColumnDefinition (UserReportID, ReportColumnID, Caption
			, SortNum, DataFormat, FilterOperatorID, FilterValue1, FilterValue2, CreatedByUser)
			SELECT @newID, ReportColumnID, Caption
				, SortNum, DataFormat, FilterOperatorID, FilterValue1, FilterValue2, isnull(@userName, 'Administrator')
			FROM tblUserReportColumnDefinition WHERE UserReportID = @userReportID
	END
	
	SELECT NEWID=@newID
	RETURN (isnull(@newID, 0))
END

GO

COMMIT
SET NOEXEC OFF