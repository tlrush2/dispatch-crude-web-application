/* fix more bugs in DriverApp.Sync logic
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.1.5', @NewVersion = '2.1.6'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

/*******************************************/
-- Date Created: 25 Apr 2013
-- Author: Kevin Alons
-- Purpose: return OrderTicket data for Driver App sync
/*******************************************/
ALTER FUNCTION [dbo].[fnOrderTicket_DriverApp]( @DriverID int, @LastChangeDateUTC datetime ) RETURNS  TABLE AS
RETURN 
	SELECT OT.ID
		, OT.UID
		, OT.OrderID
		, OT.CarrierTicketNum
		, OT.TankNum
		, OT.TicketTypeID
		, OT.TankTypeID
		, OT.ProductObsGravity
		, OT.ProductObsTemp
		, OT.ProductHighTemp
		, OT.ProductLowTemp
		, OT.ProductBSW
		, cast(OT.OpeningGaugeFeet as tinyint) AS OpeningGaugeFeet
		, cast(OT.OpeningGaugeInch as tinyint) AS OpeningGaugeInch
		, cast(OT.OpeningGaugeQ as tinyint) AS OpeningGaugeQ
		, cast(OT.ClosingGaugeFeet as tinyint) AS ClosingGaugeFeet
		, cast(OT.ClosingGaugeInch as tinyint) AS ClosingGaugeInch
		, cast(OT.ClosingGaugeQ as tinyint) AS ClosingGaugeQ
		, OT.GrossBarrels
		, OT.NetBarrels
		, OT.Rejected
		, OT.RejectNotes
		, OT.SealOff
		, OT.SealOn
		, OT.BOLNum
		, OT.BarrelsPerInch
		, OT.CreateDateUTC
		, OT.CreatedByUser
		, OT.LastChangeDateUTC
		, OT.LastChangedByUser
		, OT.DeleteDateUTC
		, OT.DeletedByUser
		, cast(CASE WHEN OT.DeleteDateUTC IS NULL THEN 0 ELSE 1 END as bit) AS Deleted
	FROM dbo.tblOrderTicket OT
	JOIN dbo.tblOrder O ON O.ID = OT.OrderID
	WHERE (StatusID IN (2, 7, 8) -- Dispatched, Accepted, Picked Up
		-- or status = Delivered and the driver still has access to the record
		OR (StatusID = 3 AND O.DestDepartTimeUTC < DATEADD(hour, (SELECT cast(value as int) FROM tblSetting WHERE ID = 10), GETUTCDATE())))
	  AND O.DriverID = @DriverID
	  AND (@LastChangeDateUTC IS NULL 
		OR OT.CreateDateUTC >= @LastChangeDateUTC
		OR OT.LastChangeDateUTC >= @LastChangeDateUTC)

GO

/*******************************************/
-- Date Created: 31 Aug 2013
-- Author: Kevin Alons
-- Purpose: return driver editable Order data for Driver App sync
/*******************************************/
ALTER FUNCTION [dbo].[fnOrderEdit_DriverApp]( @DriverID int, @LastChangeDateUTC datetime ) RETURNS TABLE AS
RETURN 
	SELECT O.ID
		, O.StatusID
		, O.TruckID
		, O.TrailerID
		, O.Trailer2ID
		, O.OriginBOLNum
		, O.OriginArriveTimeUTC
		, O.OriginDepartTimeUTC
		, O.OriginMinutes
		, O.OriginWaitNotes
		, O.OriginTruckMileage
		, O.OriginGrossBarrels
		, O.OriginNetBarrels
		, O.ChainUp
		, O.Rejected
		, O.RejectNotes
		, O.OriginTankNum
		, O.DestArriveTimeUTC
		, O.DestDepartTimeUTC
		, O.DestMinutes
		, O.DestWaitNotes
		, O.DestTruckMileage
		, O.DestGrossBarrels
		, O.DestNetBarrels
		, O.DestProductTemp
		, O.DestProductBSW
		, O.DestProductGravity
		, O.DestOpenMeterBarrels
		, O.DestCloseMeterBarrels
		, O.CarrierTicketNum
		, O.AcceptLastChangeDateUTC
		, O.PickupLastChangeDateUTC
		, O.DeliverLastChangeDateUTC
	FROM dbo.tblOrder O
	LEFT JOIN tblOrderDriverAppVirtualDelete ODAVD ON ODAVD.OrderID = O.ID AND ODAVD.DriverID = @DriverID
	WHERE (O.StatusID IN (2, 7, 8) -- Dispatched, Accepted, Picked Up
		-- or status = Delivered and the driver still has access to the record
		OR (StatusID = 3 AND O.DestDepartTimeUTC < DATEADD(hour, (SELECT cast(value as int) FROM tblSetting WHERE ID = 10), GETUTCDATE())))
	  AND (O.DriverID = @DriverID OR ODAVD.DriverID = @DriverID)
	  AND (
		@LastChangeDateUTC IS NULL 
		OR O.CreateDateUTC > @LastChangeDateUTC
		OR O.LastChangeDateUTC >= @LastChangeDateUTC
		OR (ODAVD.ID IS NOT NULL AND ODAVD.VirtualDeleteDateUTC >= @LastChangeDateUTC))

GO

/*******************************************/
-- Date Created: 25 Apr 2013
-- Author: Kevin Alons
-- Purpose: return readonly Order data for Driver App sync
/*******************************************/
ALTER FUNCTION [dbo].[fnOrderReadOnly_DriverApp]( @DriverID int, @LastChangeDateUTC datetime ) RETURNS TABLE AS
RETURN 
	SELECT O.ID
		, O.OrderNum
		, O.StatusID
		, O.TicketTypeID
		, OO.TankTypeID
		, cast(P.PriorityNum as int) AS PriorityNum
		, O.Product
		, O.DueDate
		, O.Origin
		, O.OriginFull
		, OO.OriginType
		, OO.Station AS OriginStation
		, OO.County AS OriginCounty
		, O.OriginState
		, OO.WellAPI AS OriginAPI
		, O.Destination
		, O.DestinationFull
		, D.DestinationType AS DestType 
		, D.Station AS DestinationStation
		--, cast(CASE WHEN D.TicketTypeID = 2 THEN 1 ELSE 0 END as bit) AS DestBOLAvailable
		, OO.BarrelsPerInch AS OriginBarrelsPerInch
		, O.CreateDateUTC
		, O.CreatedByUser
		, O.LastChangeDateUTC
		, O.LastChangedByUser
		, isnull(ODAVD.VirtualDeleteDateUTC, O.DeleteDateUTC) AS DeleteDateUTC
		, isnull(ODAVD.VirtualDeletedByUser, O.DeletedByUser) AS DeletedByUser
		, cast(CASE WHEN isnull(ODAVD.VirtualDeleteDateUTC, O.DeleteDateUTC) IS NULL THEN 0 ELSE 1 END as bit) AS Deleted
		, O.OriginID
		, O.DestinationID
		, cast(O.PriorityID AS int) AS PriorityID
		, O.Operator
		, O.OperatorID
		, O.Pumper
		, O.PumperID
		, O.Producer
		, O.ProducerID
		, O.Customer
		, O.CustomerID
		, O.Carrier
		, O.CarrierID
		, O.ProductID
		, O.TicketType
		, C.ZPLHeaderBlob AS PrintHeaderBlob
		, isnull(C.EmergencyInfo, 'For an emergency (spill, leak, fire, or accident) contact CHEMTREC @ 800.424.9300 (toll free)') AS EmergencyInfo
		, O.DestTicketTypeID
		, O.DestTicketType
	FROM dbo.viewOrder O
	JOIN dbo.tblPriority P ON P.ID = O.PriorityID
	JOIN dbo.viewOrigin OO ON OO.ID = O.OriginID
	JOIN dbo.viewDestination D ON D.ID = O.DestinationID
	JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
	LEFT JOIN tblOrderDriverAppVirtualDelete ODAVD ON ODAVD.OrderID = O.ID AND ODAVD.DriverID = @DriverID
	WHERE (StatusID IN (2, 7, 8) -- Dispatched, Accepted, Picked Up
		-- or status = Delivered and the driver still has access to the record
		OR (StatusID = 3 AND O.DestDepartTimeUTC < DATEADD(hour, (SELECT cast(value as int) FROM tblSetting WHERE ID = 10), GETUTCDATE())))
	  AND (O.DriverID = @DriverID OR ODAVD.DriverID = @DriverID)
	  AND (
		@LastChangeDateUTC IS NULL 
		OR O.CreateDateUTC > @LastChangeDateUTC
		OR O.LastChangeDateUTC >= @LastChangeDateUTC
		OR (ODAVD.ID IS NOT NULL AND ODAVD.VirtualDeleteDateUTC >= @LastChangeDateUTC))

GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 6 Feb 2013
-- Description:	trigger to ensure the GrossBarrels and NetBarrels are computed for valid, entered data
-- =============================================
ALTER TRIGGER [dbo].[trigOrderTicket_IU] ON [dbo].[tblOrderTicket] AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	-- re-compute GaugeRun ticket Gross barrels
	IF UPDATE(OpeningGaugeFeet) OR UPDATE(OpeningGaugeInch) OR UPDATE(OpeningGaugeQ)
		OR UPDATE (ClosingGaugeFeet) OR UPDATE(ClosingGaugeInch) OR UPDATE(ClosingGaugeQ) OR UPDATE(BarrelsPerInch)
		OR UPDATE (GrossBarrels)
	BEGIN
		UPDATE tblOrderTicket
		  SET GrossBarrels = dbo.fnTankQtyBarrelsDelta(
				OpeningGaugeFeet
			  , OpeningGaugeInch
			  , OpeningGaugeQ
			  , ClosingGaugeFeet
			  , ClosingGaugeInch
			  , ClosingGaugeQ
			  , BarrelsPerInch)
		WHERE ID IN (SELECT ID FROM inserted) 
		  AND TicketTypeID = 1 -- Gauge Run Ticket
		  AND OpeningGaugeFeet IS NOT NULL
		  AND OpeningGaugeInch IS NOT NULL
		  AND OpeningGaugeQ IS NOT NULL
		  AND ClosingGaugeFeet IS NOT NULL
		  AND ClosingGaugeInch IS NOT NULL
		  AND ClosingGaugeQ IS NOT NULL
		  AND BarrelsPerInch IS NOT NULL
	END
	-- re-compute GaugeRun ticket Net Barrels
	IF UPDATE(OpeningGaugeFeet) OR UPDATE(OpeningGaugeInch) OR UPDATE(OpeningGaugeQ)
		OR UPDATE (ClosingGaugeFeet) OR UPDATE(ClosingGaugeInch) OR UPDATE(ClosingGaugeQ) 
		OR UPDATE(BarrelsPerInch) OR UPDATE(GrossBarrels) OR UPDATE(NetBarrels)
		OR UPDATE(ProductObsTemp) OR UPDATE(ProductObsGravity) OR UPDATE(ProductBSW)
	BEGIN
		UPDATE tblOrderTicket
		  SET NetBarrels = dbo.fnCrudeNetCalculator(GrossBarrels, ProductObsTemp, ProductObsGravity, ProductBSW)
		WHERE ID IN (SELECT ID FROM inserted)
		  AND TicketTypeID IN (1,2)
		  AND GrossBarrels IS NOT NULL
		  AND ProductObsTemp IS NOT NULL
		  AND ProductObsGravity IS NOT NULL
		  AND ProductBSW IS NOT NULL
	END
	-- ensure the Order record is in-sync with the Tickets
	UPDATE tblOrder 
	SET OriginGrossBarrels = (SELECT sum(GrossBarrels) FROM tblOrderTicket WHERE OrderID = O.ID AND Rejected = 0 AND DeleteDateUTC IS NULL)
	  , OriginNetBarrels = (SELECT sum(NetBarrels) FROM tblOrderTicket WHERE OrderID = O.ID AND Rejected = 0 AND DeleteDateUTC IS NULL)
		-- use the first MeterRun/BasicRun CarrierTicketNum num as the Order BOL Num
	  , OriginBOLNum = (SELECT min(BOLNum) FROM tblOrderTicket WHERE OrderID = O.ID AND TicketTypeID NOT IN (1,2) AND DeleteDateUTC IS NULL)
	  , CarrierTicketNum = (SELECT min(CarrierTicketNum) FROM tblOrderTicket WHERE ID IN 
		(SELECT min(ID) FROM tblOrderTicket WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL))
	  , LastChangeDateUTC = (SELECT MAX(isnull(LastChangeDateUTC, CreateDateUTC)) FROM inserted WHERE OrderID = O.ID)
	  , LastChangedByUser = (SELECT MIN(isnull(LastChangedByUser, CreatedByUser)) FROM inserted WHERE OrderID = O.ID)
	FROM tblOrder O
	WHERE ID IN (SELECT DISTINCT OrderID FROM inserted)
END

GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 3 Oct 2013
-- Description:	trigger to ensure the GrossBarrels and NetBarrels are computed for valid, entered data
-- =============================================
CREATE TRIGGER [dbo].[trigOrderTicket_D] ON [dbo].[tblOrderTicket] AFTER DELETE AS
BEGIN
	SET NOCOUNT ON;

	-- ensure the Order record is in-sync with the Tickets
	UPDATE tblOrder 
	SET OriginGrossBarrels = (SELECT sum(GrossBarrels) FROM tblOrderTicket WHERE OrderID = O.ID AND Rejected = 0 AND DeleteDateUTC IS NULL)
	  , OriginNetBarrels = (SELECT sum(NetBarrels) FROM tblOrderTicket WHERE OrderID = O.ID AND Rejected = 0 AND DeleteDateUTC IS NULL)
		-- use the first MeterRun/BasicRun CarrierTicketNum num as the Order BOL Num
	  , OriginBOLNum = (SELECT min(BOLNum) FROM tblOrderTicket WHERE OrderID = O.ID AND TicketTypeID NOT IN (1,2) AND DeleteDateUTC IS NULL)
	  , CarrierTicketNum = (SELECT min(CarrierTicketNum) FROM tblOrderTicket WHERE ID IN 
		(SELECT min(ID) FROM tblOrderTicket WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL))
	  , LastChangeDateUTC = (SELECT MAX(isnull(LastChangeDateUTC, CreateDateUTC)) FROM deleted WHERE OrderID = O.ID)
	  , LastChangedByUser = (SELECT MIN(isnull(LastChangedByUser, CreatedByUser)) FROM deleted WHERE OrderID = O.ID)
	FROM tblOrder O
	WHERE ID IN (SELECT DISTINCT OrderID FROM deleted)
END

GO

COMMIT
SET NOEXEC OFF