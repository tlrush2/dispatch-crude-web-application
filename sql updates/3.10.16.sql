SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.10.15'
SELECT  @NewVersion = '3.10.16'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1020: Adding missing Import Center tables and fields'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


-- 3.10.16 - BB - 2016/03/03 - Add missing table references to tblObject
INSERT tblObject (ID, Name, SqlSourceName, SqlTargetName, CreatedByUser)	
	SELECT 40, 'Time Zone' ,'tblTimeZone', 'tblTimeZone', 'System'
	UNION
	SELECT 41, 'Destination Type' ,'tblDestinationType', 'tblDestinationType', 'System'
	EXCEPT SELECT ID, Name, SqlSourceName, SqlTargetName, CreatedByUser
	FROM tblObject
GO


-- 3.10.16 - BB - 2016/03/03 - Add missing field references to tblObjectField
SET IDENTITY_INSERT tblObjectField ON 
INSERT tblObjectField (ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, ReadOnly, 
						AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName, CreatedByUser)
	SELECT 379, 19, 'TimeZoneID', 'Time Zone', 3, NULL, 0, 0, 0, 0, 40, 'ID', 'System'
	UNION
	SELECT 380, 20, 'TimeZoneID', 'Time Zone', 3, NULL, 0, 0, 0, 0, 40, 'ID', 'System'
	UNION
	SELECT 381, 20, 'DestinationTypeID', 'Destination Type', 3, NULL, 0, 0, 0, 0, 41, 'ID', 'System'
	UNION
	SELECT 382, 40, 'ID', 'ID', 3, NULL, 0, 0, 1, 0, NULL, NULL, 'System'
	UNION
	SELECT 383, 40, 'Name', 'Name', 1, NULL, 0, 0, 0, 0, NULL, NULL, 'System'
	UNION
	SELECT 384, 40, 'StandardAbbrev', 'Abbrev', 1, NULL, 0, 0, 0, 0, NULL, NULL, 'System'
	EXCEPT SELECT ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, ReadOnly, 
					AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName, CreatedByUser 
	FROM tblObjectField
SET IDENTITY_INSERT tblObjectField OFF
GO


-- Refresh associated views
EXEC sp_RefreshView viewImportCenterFieldDefinition
GO
EXEC sp_RefreshView viewObject
GO
EXEC sp_RefreshView viewObjectField
GO
EXEC sp_RefreshView viewSunocoSundex
GO


COMMIT
SET NOEXEC OFF