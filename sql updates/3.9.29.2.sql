-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.9.29.1.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.9.29.1.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0

SET NOEXEC OFF  -- since this is 
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.9.29.1'
SELECT  @NewVersion = '3.9.29.2'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'ReportCenter: Add "Ticket BS&W Percent x0.1" & "Dest BS&W Percent x0.1" & "Gauger Ticket BS&W Percent x0.1"'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


-- Insert the following new report center fields: "Ticket BS&W Percent x0.1", "Dest BS&W Percent x0.1", and "Gauger Ticket BS&W Percent x0.1"
SET IDENTITY_INSERT tblReportColumnDefinition ON
  INSERT tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport)
	SELECT 90038,1,'T_ProductBSW * 0.1','TICKET | PRODSPECS | ORIGIN | Ticket BS&W Percent x0.1',NULL,NULL,4,NULL,1,'*',0
	UNION
	SELECT 90039,1,'DestProductBSW * 0.1','TICKET | PRODSPECS | DESTINATION | Dest BS&W Percent x0.1',NULL,NULL,4,NULL,1,'*',0
	UNION
	SELECT 90040,1,'T_GaugerProductBSW * 0.1','GAUGER | TICKETS | Gauger Ticket BS&W Percent x0.1',NULL,NULL,4,NULL,1,'*',0
  EXCEPT SELECT ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport FROM tblReportColumnDefinition
SET IDENTITY_INSERT tblReportColumnDefinition OFF
GO


COMMIT
SET NOEXEC OFF