-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.7.7.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.7.7.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.7.7'
SELECT  @NewVersion = '3.7.8'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'add Mobile App (all): Sync Prior Offset setting to allow dynamically changing offset sync "overlap" offset period'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

INSERT INTO tblSetting (ID, Category, Name, SettingTypeID, Value, ReadOnly, CreateDateUTC, CreatedByUser)
	VALUES (52, 'Mobile App (all)', 'Sync Prior Offset (seconds)', 3, 60, 0, GETUTCDATE(), 'System')
GO

/*******************************************/
-- Date Created: 15 May 2015
-- Author: Kevin Alons
-- Purpose: return table to be used by Mobile App sync process to control the amount of "overlap" used to try to ensure all server changes are always sent to the mobile app device
/*******************************************/
CREATE FUNCTION fnSyncLCDOffset(@LastChangeDateUTC datetime) RETURNS TABLE AS
	RETURN
		SELECT LCD = DATEADD(second, -cast(dbo.fnSettingValue(52) as int), isnull(@LastChangeDateUTC, dateadd(day, -30, getutcdate())))
GO
GRANT SELECT ON fnSyncLCDOffset TO dispatchcrude_iis_acct
GO

/*******************************************/
-- Date Created: 31 Aug 2013
-- Author: Kevin Alons
-- Purpose: return driver editable Order data for Driver App sync
/*******************************************/
ALTER FUNCTION fnGaugerOrder_GaugerApp(@GaugerID int, @LastChangeDateUTC datetime) RETURNS TABLE AS
RETURN 
	SELECT GAO.OrderID
		, GAO.StatusID
		, GAO.GaugerID
		, GAO.ArriveTimeUTC
		, GAO.DepartTimeUTC
		, GAO.Rejected
		, GAO.RejectReasonID
		, GAO.RejectNotes
		, GAO.OriginTankID
		, GAO.OriginTankNum
		, GAO.GaugerNotes
		, GAO.DueDate
		, GAO.PriorityID
		, GAO.CreateDateUTC, GAO.CreatedByUser
		, GAO.LastChangeDateUTC, GAO.LastChangedByUser
		, DeleteDateUTC = isnull(GOVD.VirtualDeleteDateUTC, O.DeleteDateUTC), DeletedByUser = isnull(GOVD.VirtualDeletedByUser, O.DeletedByUser)
	FROM dbo.tblGaugerOrder GAO
	JOIN dbo.tblOrder O ON O.ID = GAO.OrderID
	LEFT JOIN tblGaugerOrderVirtualDelete GOVD ON GOVD.OrderID = GAO.OrderID AND GOVD.GaugerID = GAO.GaugerID
	CROSS JOIN fnSyncLCDOffset(@LastChangeDateUTC) LCD
	WHERE GAO.StatusID NOT IN (1) -- don't include REQUESTED Gauger Order records
	  AND GAO.OrderID IN (
		SELECT OrderID FROM tblGaugerOrder WHERE GaugerID = @GaugerID
		UNION 
		SELECT OrderID FROM tblGaugerOrderVirtualDelete WHERE GaugerID = @GaugerID
	)
	  AND (@LastChangeDateUTC IS NULL
		OR GAO.CreateDateUTC >= LCD.LCD
		OR GAO.LastChangeDateUTC >= LCD.LCD
		OR O.DeleteDateUTC >= LCD.LCD
		OR GOVD.VirtualDeleteDateUTC >= LCD.LCD)
GO

/*******************************************/
-- Date Created: 13 Apr 2015
-- Author: Kevin Alons
-- Purpose: return GaugerOrderTicket data for Gauger App sync
/*******************************************/
ALTER FUNCTION [dbo].[fnGaugerOrderTicket_GaugerApp](@GaugerID int, @LastChangeDateUTC datetime) RETURNS TABLE AS
RETURN SELECT OT.UID
		, OT.OrderID
		, OT.CarrierTicketNum
		, OT.OriginTankID
		, OT.DispatchConfirmNum
		, OT.TankNum
		, OT.TicketTypeID
		, OT.ProductObsGravity
		, OT.ProductObsTemp
		, OT.ProductBSW
		, cast(OT.OpeningGaugeFeet as tinyint) AS OpeningGaugeFeet
		, cast(OT.OpeningGaugeInch as tinyint) AS OpeningGaugeInch
		, cast(OT.OpeningGaugeQ as tinyint) AS OpeningGaugeQ
		, OT.BottomFeet
		, OT.BottomInches
		, OT.BottomQ
		, OT.Rejected
		, OT.RejectReasonID
		, OT.RejectNotes
		, OT.SealOff
		, OT.SealOn
		, OT.CreateDateUTC
		, OT.CreatedByUser
		, OT.LastChangeDateUTC
		, OT.LastChangedByUser
		, OT.DeleteDateUTC
		, OT.DeletedByUser
	FROM dbo.tblGaugerOrderTicket OT
	JOIN dbo.tblGaugerOrder GAO ON GAO.OrderID = OT.OrderID
	JOIN dbo.tblOrder O ON O.ID = GAO.OrderID
	LEFT JOIN tblGaugerOrderVirtualDelete GOVD ON GOVD.OrderID = GAO.OrderID AND GOVD.GaugerID = GAO.GaugerID
	CROSS JOIN fnSyncLCDOffset(@LastChangeDateUTC) LCD
	WHERE GAO.StatusID NOT IN (1) -- don't include REQUESTED Gauger Order records
	  AND GAO.OrderID IN (
		SELECT OrderID FROM tblGaugerOrder WHERE GaugerID = @GaugerID
		UNION 
		SELECT OrderID FROM tblGaugerOrderVirtualDelete WHERE GaugerID = @GaugerID
	)
	  AND (@LastChangeDateUTC IS NULL
		OR GAO.CreateDateUTC >= LCD.LCD
		OR GAO.LastChangeDateUTC >= LCD.LCD
		OR O.DeleteDateUTC >= LCD.LCD
		OR OT.CreateDateUTC >= LCD.LCD
		OR OT.LastChangeDateUTC >= LCD.LCD
		OR GOVD.VirtualDeleteDateUTC >= LCD.LCD)
GO

/*******************************************/
-- Date Created: 31 Aug 2013
-- Author: Kevin Alons
-- Purpose: return driver editable Order data for Driver App sync
-- Changes: 3.7.4 05/08/15 GSM Added Rack/Bay field
/*******************************************/
ALTER FUNCTION [dbo].[fnOrderEdit_DriverApp]( @DriverID int, @LastChangeDateUTC datetime ) RETURNS TABLE AS
RETURN 
	SELECT O.ID
		, O.StatusID
		, O.TruckID
		, O.TrailerID
		, O.Trailer2ID
		, O.OriginBOLNum
		, O.OriginArriveTimeUTC
		, O.OriginDepartTimeUTC
		, O.OriginMinutes
		, O.OriginWaitReasonID
		, O.OriginWaitNotes
		, O.OriginTruckMileage
		, O.OriginGrossUnits
		, O.OriginGrossStdUnits
		, O.OriginNetUnits
		, O.ChainUp
		, O.Rejected
		, O.RejectReasonID
		, O.RejectNotes
		, O.OriginTankNum
		, O.DestArriveTimeUTC
		, O.DestDepartTimeUTC
		, O.DestMinutes
		, O.DestWaitReasonID
		, O.DestWaitNotes
		, O.DestBOLNum
		, O.DestTruckMileage
		, O.DestGrossUnits
		, O.DestNetUnits
		, O.DestProductTemp
		, O.DestProductBSW
		, O.DestProductGravity
		, O.DestOpenMeterUnits
		, O.DestCloseMeterUnits
		, O.CarrierTicketNum
		, AcceptLastChangeDateUTC = dbo.fnMaxDateTime(O.AcceptLastChangeDateUTC, O.LastChangeDateUTC)
		, PickupLastChangeDateUTC = dbo.fnMaxDateTime(O.PickupLastChangeDateUTC, O.LastChangeDateUTC)
		, DeliverLastChangeDateUTC = dbo.fnMaxDateTime(O.DeliverLastChangeDateUTC, O.LastChangeDateUTC)
		, O.PickupPrintStatusID
		, O.DeliverPrintStatusID
		, O.PickupPrintDateUTC
		, O.DeliverPrintDateUTC
		, O.PickupDriverNotes
		, O.DeliverDriverNotes
		, O.RackBay
	FROM dbo.tblOrder O
	LEFT JOIN tblOrderDriverAppVirtualDelete ODAVD ON ODAVD.OrderID = O.ID AND ODAVD.DriverID = @DriverID
	CROSS JOIN fnSyncLCDOffset(@LastChangeDateUTC) LCD
	WHERE O.ID IN (
		SELECT id FROM tblOrder WHERE DriverID = @driverID AND StatusID IN (2,7,8,3) 
		UNION 
		SELECT OrderID FROM tblOrderDriverAppVirtualDelete WHERE DriverID = @driverID
	)
	  AND (@LastChangeDateUTC IS NULL
		OR O.CreateDateUTC >= LCD.LCD
		OR O.LastChangeDateUTC >= LCD.LCD
		OR O.DeleteDateUTC >= LCD.LCD
		OR ODAVD.VirtualDeleteDateUTC >= LCD.LCD)
GO

/*******************************************/
-- Date Created: 25 Apr 2013
-- Author: Kevin Alons
-- Purpose: return readonly Order data for Driver App sync
/*******************************************/
ALTER FUNCTION [dbo].[fnOrderReadOnly_DriverApp](@DriverID int, @LastChangeDateUTC datetime) RETURNS TABLE AS
RETURN 
	SELECT O.ID
		, O.OrderNum
		, O.StatusID
		, O.TicketTypeID
		, PriorityNum = cast(P.PriorityNum as int) 
		, O.Product
		, O.DueDate
		, O.Origin
		, O.OriginFull
		, OO.OriginType
		, O.OriginUomID
		, OriginStation = OO.Station 
		, OriginLeaseNum = OO.LeaseNum 
		, OriginCounty = OO.County 
		, OriginLegalDescription = OO.LegalDescription 
		, O.OriginNDIC
		, O.OriginNDM
		, O.OriginCA
		, O.OriginState
		, OO.WellAPI AS OriginAPI
		, OriginLat = OO.LAT 
		, OriginLon = OO.LON 
		, OriginGeoFenceRadiusMeters = OO.GeoFenceRadiusMeters 
		, O.Destination
		, O.DestinationFull
		, DestType = D.DestinationType 
		, O.DestUomID
		, DestLat = D.LAT 
		, DestLon = D.LON 
		, DestGeoFenceRadiusMeters = D.GeoFenceRadiusMeters 
		, D.Station AS DestinationStation
		, O.CreateDateUTC
		, O.CreatedByUser
		, O.LastChangeDateUTC
		, O.LastChangedByUser
		, DeleteDateUTC = isnull(ODAVD.VirtualDeleteDateUTC, O.DeleteDateUTC) 
		, DeletedByUser = isnull(ODAVD.VirtualDeletedByUser, O.DeletedByUser) 
		, O.OriginID
		, O.DestinationID
		, PriorityID = cast(O.PriorityID AS int) 
		, O.Operator
		, O.OperatorID
		, O.Pumper
		, O.PumperID
		, O.Producer
		, O.ProducerID
		, O.Customer
		, O.CustomerID
		, O.Carrier
		, O.CarrierID
		, O.ProductID
		, O.TicketType
		, PrintHeaderBlob = C.ZPLHeaderBlob 
		, EmergencyInfo = isnull(C.EmergencyInfo, 'For an emergency (spill, leak, fire, or accident) contact CHEMTREC @ 800.424.9300 (toll free)') 
		, O.DestTicketTypeID
		, O.DestTicketType
		, O.OriginTankNum
		, O.OriginTankID
		, O.DispatchNotes
		, O.DispatchConfirmNum
		, RouteActualMiles = isnull(R.ActualMiles, 0)
		, CarrierAuthority = CA.Authority 
		, OriginTimeZone = OO.TimeZone
		, DestTimeZone = D.TimeZone
		, OCTM.OriginThresholdMinutes
		, OCTM.DestThresholdMinutes
		, ShipperHelpDeskPhone = C.HelpDeskPhone
		, OriginDrivingDirections = OO.DrivingDirections
	FROM dbo.viewOrder O
	JOIN dbo.tblPriority P ON P.ID = O.PriorityID
	JOIN dbo.viewOrigin OO ON OO.ID = O.OriginID
	JOIN dbo.viewDestination D ON D.ID = O.DestinationID
	JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
	JOIN dbo.tblRoute R ON R.ID = O.RouteID
	JOIN dbo.tblCarrier CA ON CA.ID = O.CarrierID
	LEFT JOIN tblOrderDriverAppVirtualDelete ODAVD ON ODAVD.OrderID = O.ID AND ODAVD.DriverID = @DriverID
	CROSS JOIN fnSyncLCDOffset(@LastChangeDateUTC) LCD
	OUTER APPLY dbo.fnOrderCombinedThresholdMinutes(O.ID) OCTM
	WHERE O.ID IN (
		SELECT id FROM tblOrder WHERE DriverID = @driverID AND StatusID IN (2,7,8,3) 
		UNION 
		SELECT OrderID FROM tblOrderDriverAppVirtualDelete WHERE DriverID = @driverID
	)
	  AND (
		@LastChangeDateUTC IS NULL 
		OR O.CreateDateUTC >= LCD.LCD
		OR O.LastChangeDateUTC >= LCD.LCD
		OR O.DeleteDateUTC >= LCD.LCD
		OR C.LastChangeDateUTC >= LCD.LCD
		OR CA.LastChangeDateUTC >= LCD.LCD
		OR OO.LastChangeDateUTC >= LCD.LCD
		OR R.LastChangeDateUTC >= LCD.LCD
		OR ODAVD.VirtualDeleteDateUTC >= LCD.LCD)
GO

/*******************************************/
-- Date Created: 13 Apr 2015
-- Author: Kevin Alons
-- Purpose: return readonly Order data for Gauger App sync
/*******************************************/
ALTER FUNCTION [dbo].[fnOrderReadOnly_GaugerApp](@GaugerID int, @LastChangeDateUTC datetime) RETURNS TABLE AS
RETURN 
	SELECT O.ID
		, O.OrderNum
		, GAO.StatusID
		, GAO.TicketTypeID
		, GAO.GaugerID
		, PriorityNum = cast(P.PriorityNum as int) 
		, O.Product
		, GAO.DueDate
		, O.Origin
		, O.OriginFull
		, OO.OriginType
		, O.OriginUomID
		, OriginStation = OO.Station 
		, OriginLeaseNum = OO.LeaseNum 
		, OriginCounty = OO.County 
		, OriginLegalDescription = OO.LegalDescription 
		, O.OriginNDIC
		, O.OriginNDM
		, O.OriginCA
		, O.OriginState
		, OriginAPI = OO.WellAPI
		, OriginLat = OO.LAT 
		, OriginLon = OO.LON 
		, OriginGeoFenceRadiusMeters = OO.GeoFenceRadiusMeters 
		, O.CreateDateUTC
		, O.CreatedByUser
		, O.LastChangeDateUTC
		, O.LastChangedByUser
		, DeleteDateUTC = isnull(GOVD.VirtualDeleteDateUTC, O.DeleteDateUTC) 
		, DeletedByUser = isnull(GOVD.VirtualDeletedByUser, O.DeletedByUser) 
		, O.OriginID
		, PriorityID = cast(O.PriorityID AS int) 
		, O.Operator
		, O.OperatorID
		, O.Pumper
		, O.PumperID
		, O.Producer
		, O.ProducerID
		, O.Customer
		, O.CustomerID
		, O.ProductID
		, TicketType = GTT.Name
		, PrintHeaderBlob = S.ZPLHeaderBlob 
		, EmergencyInfo = isnull(S.EmergencyInfo, 'For an emergency (spill, leak, fire, or accident) contact CHEMTREC @ 800.424.9300 (toll free)') 
		, O.OriginTankID
		, O.OriginTankNum
		, GAO.DispatchNotes
		, O.CarrierTicketNum
		, O.DispatchConfirmNum
		, OriginTimeZone = OO.TimeZone
		, OCTM.OriginThresholdMinutes
		, ShipperHelpDeskPhone = S.HelpDeskPhone
		, OriginDrivingDirections = OO.DrivingDirections
	FROM dbo.viewOrder O
	JOIN dbo.tblGaugerOrder GAO ON GAO.OrderID = O.ID
	JOIN tblGaugerTicketType GTT ON GTT.ID = GAO.TicketTypeID
	JOIN dbo.tblPriority P ON P.ID = GAO.PriorityID
	JOIN dbo.viewOrigin OO ON OO.ID = O.OriginID
	JOIN dbo.viewDestination D ON D.ID = O.DestinationID
	JOIN dbo.tblCustomer S ON S.ID = O.CustomerID
	JOIN dbo.tblRoute R ON R.ID = O.RouteID
	LEFT JOIN tblGaugerOrderVirtualDelete GOVD ON GOVD.OrderID = O.ID AND GOVD.GaugerID = GAO.GaugerID
	CROSS JOIN fnSyncLCDOffset(@LastChangeDateUTC) LCD
	OUTER APPLY dbo.fnOrderCombinedThresholdMinutes(O.ID) OCTM
	WHERE GAO.StatusID NOT IN (1) -- don't include REQUESTED Gauger Order records
	  AND O.ID IN (
		SELECT OrderID FROM tblGaugerOrder WHERE GaugerID = @GaugerID
		UNION 
		SELECT OrderID FROM tblGaugerOrderVirtualDelete WHERE GaugerID = @GaugerID
	)
	  AND (@LastChangeDateUTC IS NULL
		OR O.CreateDateUTC >= LCD.LCD
		OR O.LastChangeDateUTC >= LCD.LCD
		OR OO.DeleteDateUTC >= LCD.LCD
		OR GOVD.VirtualDeleteDateUTC >= LCD.LCD)
GO

/*******************************************/
-- Date Created: 1 Mar 2015
-- Author: Kevin Alons
-- Purpose: return Order Rules for every changed Order
/*******************************************/
ALTER FUNCTION [dbo].[fnOrderRules_DriverApp]( @DriverID int, @LastChangeDateUTC datetime ) 
RETURNS 
	@ret TABLE (
		ID int
	  , OrderID int
	  , TypeID int
	  , Value varchar(255)
	)
AS BEGIN
	DECLARE @IDS TABLE (ID int)
	INSERT INTO @IDS
		SELECT O.ID
		FROM dbo.viewOrder O
		JOIN dbo.viewOrigin OO ON OO.ID = O.OriginID
		JOIN dbo.viewDestination D ON D.ID = O.DestinationID
		JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
		JOIN dbo.tblRoute R ON R.ID = O.RouteID
		JOIN dbo.tblCarrier CA ON CA.ID = O.CarrierID
		CROSS JOIN fnSyncLCDOffset(@LastChangeDateUTC) LCD
		WHERE O.ID IN (
			SELECT ID FROM tblOrder WHERE DriverID = @driverID AND StatusID IN (2,7,8,3) 
		)
		  AND (
			@LastChangeDateUTC IS NULL 
			OR O.CreateDateUTC >= LCD.LCD
			OR O.LastChangeDateUTC >= LCD.LCD
			OR O.DeleteDateUTC >= LCD.LCD
			OR C.LastChangeDateUTC >= LCD.LCD
			OR CA.LastChangeDateUTC >= LCD.LCD
			OR OO.LastChangeDateUTC >= LCD.LCD
			OR R.LastChangeDateUTC >= LCD.LCD)
	
	DECLARE @ID int
	WHILE EXISTS (SELECT * FROM @IDS)
	BEGIN
		SELECT TOP 1 @ID = ID FROM @IDS
		INSERT INTO @ret (ID, OrderID, TypeID, Value)
			SELECT ID, @ID, TypeID, Value FROM dbo.fnOrderOrderRules(@ID)
		DELETE FROM @IDS WHERE ID = @ID
		-- if no records exist, then return a NULL record to tell the Mobile App to delete any existing records for this order
		IF NOT EXISTS (SELECT * FROM @ret WHERE OrderID = @ID)
			INSERT INTO @ret (OrderID) VALUES (@ID)
	END
	RETURN
END
GO

/*******************************************/
-- Date Created: 1 Mar 2015
-- Author: Kevin Alons
-- Purpose: return Order Rules for every changed Order
/*******************************************/
ALTER FUNCTION [dbo].[fnOrderRules_GaugerApp](@GaugerID int, @LastChangeDateUTC datetime) 
RETURNS 
	@ret TABLE (
		ID int
	  , OrderID int
	  , TypeID int
	  , Value varchar(255)
	)
AS BEGIN
	DECLARE @IDS TABLE (ID int)
	INSERT INTO @IDS
		SELECT O.ID
		FROM dbo.viewOrder O
		JOIN tblGaugerOrder GAO ON GAO.OrderID = O.ID
		JOIN dbo.viewOrigin OO ON OO.ID = O.OriginID
		JOIN dbo.viewDestination D ON D.ID = O.DestinationID
		JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
		JOIN dbo.tblRoute R ON R.ID = O.RouteID
		JOIN dbo.tblCarrier CA ON CA.ID = O.CarrierID
		LEFT JOIN tblGaugerOrderVirtualDelete GOVD ON GOVD.OrderID = O.ID AND GOVD.GaugerID = GAO.GaugerID
		CROSS JOIN fnSyncLCDOffset(@LastChangeDateUTC) LCD
	WHERE GAO.StatusID NOT IN (1) -- don't include REQUESTED Gauger Order records
	  AND O.ID IN (
		SELECT OrderID FROM tblGaugerOrder WHERE GaugerID = @GaugerID
		UNION 
		SELECT OrderID FROM tblGaugerOrderVirtualDelete WHERE GaugerID = @GaugerID
	)
	  AND (@LastChangeDateUTC IS NULL
		OR O.CreateDateUTC >= LCD.LCD
		OR O.LastChangeDateUTC >= LCD.LCD
		OR OO.DeleteDateUTC >= LCD.LCD
		OR GOVD.VirtualDeleteDateUTC >= LCD.LCD)
	
	DECLARE @ID int
	WHILE EXISTS (SELECT * FROM @IDS)
	BEGIN
		SELECT TOP 1 @ID = ID FROM @IDS
		INSERT INTO @ret (ID, OrderID, TypeID, Value)
			SELECT ID, @ID, TypeID, Value FROM dbo.fnOrderOrderRules(@ID)
		DELETE FROM @IDS WHERE ID = @ID
		-- if no records exist, then return a NULL record to tell the Mobile App to delete any existing records for this order
		IF NOT EXISTS (SELECT * FROM @ret WHERE OrderID = @ID)
			INSERT INTO @ret (OrderID) VALUES (@ID)
	END
	RETURN
END
GO

/*******************************************/
-- Date Created: 25 Apr 2013
-- Author: Kevin Alons
-- Purpose: return OrderTicket data for Driver App sync
/*******************************************/
ALTER FUNCTION [dbo].[fnOrderTicket_DriverApp]( @DriverID int, @LastChangeDateUTC datetime ) RETURNS  TABLE AS
RETURN 
	SELECT OT.ID
		, OT.UID
		, OT.OrderID
		, OT.CarrierTicketNum
		, OT.OriginTankID
		, OT.TankNum
		, OT.TicketTypeID
		, OT.ProductObsGravity
		, OT.ProductObsTemp
		, OT.ProductHighTemp
		, OT.ProductLowTemp
		, OT.ProductBSW
		, cast(OT.OpeningGaugeFeet as tinyint) AS OpeningGaugeFeet
		, cast(OT.OpeningGaugeInch as tinyint) AS OpeningGaugeInch
		, cast(OT.OpeningGaugeQ as tinyint) AS OpeningGaugeQ
		, cast(OT.ClosingGaugeFeet as tinyint) AS ClosingGaugeFeet
		, cast(OT.ClosingGaugeInch as tinyint) AS ClosingGaugeInch
		, cast(OT.ClosingGaugeQ as tinyint) AS ClosingGaugeQ
		, OT.BottomFeet
		, OT.BottomInches
		, OT.BottomQ
		, OT.GrossUnits
		, OT.GrossStdUnits
		, OT.NetUnits
		, OT.Rejected
		, OT.RejectReasonID
		, OT.RejectNotes
		, OT.SealOff
		, OT.SealOn
		, OT.BOLNum
		, OT.CreateDateUTC
		, OT.CreatedByUser
		, OT.LastChangeDateUTC
		, OT.LastChangedByUser
		, OT.DeleteDateUTC
		, OT.DeletedByUser
		, OT.MeterFactor
		, OT.OpenMeterUnits
		, OT.CloseMeterUnits
		, OT.DispatchConfirmNum
	FROM dbo.tblOrderTicket OT
	JOIN dbo.tblOrder O ON O.ID = OT.OrderID
	LEFT JOIN tblOrderDriverAppVirtualDelete ODAVD ON ODAVD.OrderID = O.ID AND ODAVD.DriverID = @DriverID
	CROSS JOIN fnSyncLCDOffset(@LastChangeDateUTC) LCD
	WHERE (O.StatusID IN (2, 7, 8, 3) -- Dispatched, Accepted, Picked Up, Delivered
		-- or this potentially was a DriverApp record, that no longer is, so should be included (as deleted)
		OR (ODAVD.ID IS NOT NULL AND ODAVD.VirtualDeleteDateUTC >= LCD.LCD))
	  AND O.DriverID = @DriverID
	  AND (@LastChangeDateUTC IS NULL 
		OR OT.CreateDateUTC >= LCD.LCD
		OR OT.LastChangeDateUTC >= LCD.LCD
		OR ODAVD.VirtualDeleteDateUTC >= LCD.LCD)
GO

/*******************************************/
-- Date Created: 5 Apr 2014
-- Author: Kevin Alons
-- Purpose: return OriginTank data for Driver App sync
/*******************************************/
ALTER FUNCTION [dbo].[fnOriginTank_DriverApp](@DriverID int, @LastChangeDateUTC datetime) RETURNS TABLE AS
RETURN 
	SELECT DISTINCT OT.ID
		, OT.OriginID
		, OT.TankNum
		, OT.TankDescription
		, OT.CreateDateUTC
		, OT.CreatedByUser
		, OT.LastChangeDateUTC
		, OT.LastChangedByUser
		, OT.DeleteDateUTC
		, OT.DeletedByUser
	FROM dbo.tblOriginTank OT
	JOIN dbo.tblOrder O ON O.OriginID = OT.OriginID
	LEFT JOIN tblOrderDriverAppVirtualDelete ODAVD ON ODAVD.OrderID = O.ID AND ODAVD.DriverID = @DriverID
	CROSS JOIN fnSyncLCDOffset(@LastChangeDateUTC) LCD
	WHERE (StatusID IN (2, 7, 8, 3) -- Dispatched, Accepted, Picked Up, Delivered
		-- or this potentially was a DriverApp record, that no longer is, so should be included (as deleted)
		OR (ODAVD.ID IS NOT NULL AND ODAVD.VirtualDeleteDateUTC >= LCD.LCD))
	  AND O.DriverID = @DriverID
	  AND (@LastChangeDateUTC IS NULL 
		OR O.CreateDateUTC >= LCD.LCD
		-- required to ensure that when the Order.OriginID is changed, the OriginTanks/Strappings are always synced
		OR O.LastChangeDateUTC >= LCD.LCD
		OR OT.CreateDateUTC >= LCD.LCD
		OR OT.LastChangeDateUTC >= LCD.LCD
		OR OT.DeleteDateUTC >= LCD.LCD
		OR ODAVD.VirtualDeleteDateUTC >= LCD.LCD)
GO

/*******************************************/
-- Date Created: 5 Apr 2014
-- Author: Kevin Alons
-- Purpose: return OriginTank data for Gauger App sync
/*******************************************/
ALTER FUNCTION [dbo].[fnOriginTank_GaugerApp](@GaugerID int, @LastChangeDateUTC datetime) RETURNS TABLE AS
RETURN 
	SELECT DISTINCT OT.ID
		, OT.OriginID
		, OT.TankNum
		, OT.TankDescription
		, OT.CreateDateUTC
		, OT.CreatedByUser
		, OT.LastChangeDateUTC
		, OT.LastChangedByUser
		, OT.DeleteDateUTC
		, OT.DeletedByUser
	FROM dbo.tblOriginTank OT
	JOIN dbo.tblOrder O ON O.OriginID = OT.OriginID
	LEFT JOIN tblGaugerOrderVirtualDelete GOVD ON GOVD.OrderID = O.ID AND GOVD.GaugerID = @GaugerID
	CROSS JOIN fnSyncLCDOffset(@LastChangeDateUTC) LCD
	WHERE O.ID IN (
		SELECT OrderID FROM tblGaugerOrder WHERE GaugerID = @GaugerID
		UNION 
		SELECT OrderID FROM tblGaugerOrderVirtualDelete WHERE GaugerID = @GaugerID
	)
	  AND (@LastChangeDateUTC IS NULL
		OR O.CreateDateUTC >= LCD.LCD
		-- required to ensure that when the Order.OriginID is changed, the OriginTanks/Strappings are always synced
		OR O.LastChangeDateUTC >= LCD.LCD
		OR GOVD.VirtualDeleteDateUTC >= LCD.LCD
		OR OT.CreateDateUTC >= LCD.LCD
		OR OT.LastChangeDateUTC >= LCD.LCD
		)
GO

/*******************************************/
-- Date Created: 5 Apr 2014
-- Author: Kevin Alons
-- Purpose: return OriginTankStrapping data for Driver App sync
/*******************************************/
ALTER FUNCTION [dbo].[fnOriginTankStrapping_DriverApp](@DriverID int, @LastChangeDateUTC datetime) RETURNS TABLE AS
RETURN 
	SELECT DISTINCT OTS.*
/*		, OTS.OriginTankID
		, OTS.Feet
		, OTS.Inches
		, OTS.Q
		, OTS.BPQ
		, OTS.IsMinimum
		, OTS.IsMaximum
		, OTS.CreateDateUTC
		, OTS.CreatedByUser
		, OTS.LastChangeDateUTC
		, OTS.LastChangedByUser */
		, cast(CASE WHEN OTSD.ID IS NULL THEN 0 ELSE 1 END as bit) AS IsDeleted
	FROM dbo.tblOriginTankStrapping OTS
	JOIN dbo.tblOriginTank OT ON OT.ID = OTS.OriginTankID
	JOIN dbo.tblOrder O ON O.OriginID = OT.OriginID
	LEFT JOIN tblOrderDriverAppVirtualDelete ODAVD ON ODAVD.OrderID = O.ID AND ODAVD.DriverID = @DriverID
	LEFT JOIN dbo.tblOriginTankStrappingDeleted OTSD ON OTSD.ID = OTS.ID
	CROSS JOIN fnSyncLCDOffset(@LastChangeDateUTC) LCD
	WHERE (StatusID IN (2, 7, 8, 3) -- Dispatched, Accepted, Picked Up, Delivered
		-- or this potentially was a DriverApp record, that no longer is, so should be included (as deleted)
		OR (ODAVD.ID IS NOT NULL AND ODAVD.VirtualDeleteDateUTC >= LCD.LCD))
	  AND O.DriverID = @DriverID
	  AND (@LastChangeDateUTC IS NULL 
		OR O.CreateDateUTC >= LCD.LCD
		-- required to ensure that when the Order.OriginID is changed, the OriginTanks/Strappings are always synced
		OR O.LastChangeDateUTC >= LCD.LCD
		OR OT.CreateDateUTC >= LCD.LCD
		OR OT.LastChangeDateUTC >= LCD.LCD
		OR OTS.CreateDateUTC >= LCD.LCD
		OR OTS.LastChangeDateUTC >= LCD.LCD
		OR OTSD.DeleteDateUTC >= LCD.LCD)
GO

/*******************************************/
-- Date Created: 20 Apr 2015
-- Author: Kevin Alons
-- Purpose: return OriginTankStrapping data for Gauger App sync
/*******************************************/
ALTER FUNCTION [dbo].[fnOriginTankStrapping_GaugerApp](@GaugerID int, @LastChangeDateUTC datetime) RETURNS TABLE AS
RETURN 
	SELECT DISTINCT OTS.*
		, cast(CASE WHEN OTSD.ID IS NULL THEN 0 ELSE 1 END as bit) AS IsDeleted
	FROM dbo.tblOriginTankStrapping OTS
	JOIN dbo.tblOriginTank OT ON OT.ID = OTS.OriginTankID
	JOIN dbo.tblOrder O ON O.OriginID = OT.OriginID
	LEFT JOIN tblGaugerOrderVirtualDelete GOVD ON GOVD.OrderID = O.ID AND GOVD.GaugerID = @GaugerID
	LEFT JOIN dbo.tblOriginTankStrappingDeleted OTSD ON OTSD.ID = OTS.ID
	CROSS JOIN fnSyncLCDOffset(@LastChangeDateUTC) LCD
	WHERE O.ID IN (
		SELECT OrderID FROM tblGaugerOrder WHERE GaugerID = @GaugerID
		UNION 
		SELECT OrderID FROM tblGaugerOrderVirtualDelete WHERE GaugerID = @GaugerID
	)
	  AND (@LastChangeDateUTC IS NULL
		OR O.CreateDateUTC >= LCD.LCD
		-- required to ensure that when the Order.OriginID is changed, the OriginTanks/Strappings are always synced
		OR O.LastChangeDateUTC >= LCD.LCD
		OR GOVD.VirtualDeleteDateUTC >= LCD.LCD
		OR OT.CreateDateUTC >= LCD.LCD
		OR OT.LastChangeDateUTC >= LCD.LCD
		OR OTS.CreateDateUTC >= LCD.LCD
		OR OTS.LastChangeDateUTC >= LCD.LCD
		OR OTSD.DeleteDateUTC >= LCD.LCD)
GO

COMMIT
SET NOEXEC OFF