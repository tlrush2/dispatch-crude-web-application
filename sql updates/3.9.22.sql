-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.9.21.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.9.21.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0

SET NOEXEC OFF
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.9.21'
SELECT  @NewVersion = '3.9.22'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Prevent Transfer orders from auto-approving when approval module is turned on.'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO



/*************************************************
-- Date Created: 2015/08/13
-- Author: Kevin Alons
-- Purpose: put all Auto-Approve logic in one module (function)
-- Changes:
	- 3.9.22 - 2015/11/05 - KDA+JAE - add logic to not auto-approve Transferred orders
*************************************************/
ALTER FUNCTION fnIsValidAutoApprove
(
  @StatusID int
, @ShipperBatchID int
, @CarrierBatchID int
, @ShipperWaitBillableMin int
, @CarrierWaitBillableMin int
, @WaitFeeParameterID int
, @H2S bit
, @Chainup bit
, @Rerouted bit
, @Transferred bit
, @Approved bit
) RETURNS varchar(1000) AS
BEGIN
	DECLARE @ret varchar(1000)
	SELECT @ret = CASE WHEN ISNULL(@StatusID, 0) NOT IN (4) THEN '|Order not in Audited status' ELSE '' END
		+ CASE WHEN @ShipperBatchID IS NOT NULL THEN '|Order already Shipper Settled' ELSE '' END
		+ CASE WHEN @CarrierBatchID IS NOT NULL THEN '|Order already Carrier Settled' ELSE '' END
		+ CASE WHEN ISNULL(@ShipperWaitBillableMin, 0) > 0 THEN '|Order has Shipper Wait Billable Minutes' ELSE '' END
		+ CASE WHEN ISNULL(@CarrierWaitBillableMin, 0) > 0 THEN '|Order has Carrier Wait Billable Minutes' ELSE '' END
		+ CASE WHEN @WaitFeeParameterID IS NULL THEN '|Order does not have matching Wait Fee Parameter Rule' ELSE '' END
		+ CASE WHEN ISNULL(@H2S, 0) = 1 THEN '}Order has H2S = true' ELSE '' END
		+ CASE WHEN ISNULL(@Chainup, 0) = 1 THEN '|Order has Chainup = true' ELSE '' END
		+ CASE WHEN ISNULL(@Rerouted, 0) = 1 THEN '|Order has Rerouted = true' ELSE '' END
		+ CASE WHEN ISNULL(@Transferred, 0) = 1 THEN '|Order transferred' ELSE '' END
		+ CASE WHEN ISNULL(@Approved, 0) = 1 THEN '|Order is already Approved' ELSE '' END
	RETURN NULLIF(RTRIM(REPLACE(@ret, '|', char(13) + char(10))), '')
END
GO



/*************************************************
-- Date Created: 2015/08/13
-- Author: Kevin Alons
-- Purpose: return AutoApprove message for a single order 
-- Returns: NULL is valid, non-null is Invalid reason(s)
-- Changes:
	- 3.9.22 - 2015/11/05 - KDA+JAE - pass new @Transferred parameter to fnIsValidAutoApprove() function
*************************************************/
ALTER FUNCTION fnOrderIsValidAutoApprove
(
  @ID int
) RETURNS varchar(1000) AS
BEGIN
	DECLARE @ret varchar(1000)

	SELECT @ret = dbo.fnIsValidAutoApprove(ISH.StatusID, ISH.BatchID, OSC.BatchID
		, ISH.InvoiceTotalWaitBillableMinutes, ISNULL(OSC.OriginWaitBillableMinutes, 0) + ISNULL(OSC.DestinationWaitBillableMinutes, 0)
		, ISH.InvoiceWaitFeeParameterID
		, ISH.ChainUp, ISH.H2S, ISH.RerouteCount, ISNULL(OTR.TransferComplete, 0)
		, OA.Approved)
	FROM viewOrder_Financial_Shipper ISH
	LEFT JOIN tblOrderTransfer OTR ON OTR.OrderID = ISH.ID
	LEFT JOIN tblOrderSettlementCarrier OSC ON OSC.OrderID = ISH.ID
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = ISH.ID AND OA.Approved = 1
	WHERE ISH.ID = @ID

	RETURN @ret
END
GO

EXEC _spRebuildAllObjects
GO

EXEC _spRefreshAllViews
GO


COMMIT
SET NOEXEC OFF