-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.12.6.2'
SELECT  @NewVersion = '3.12.6.3'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'DCWEB-1471 Improve cascade deactivate script for carriers'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


ALTER TRIGGER trigCarrier_U_VirtualDelete ON tblCarrier AFTER UPDATE AS
BEGIN
	SET NOCOUNT ON;
	IF (UPDATE(DeleteDateUTC))
	BEGIN
		IF EXISTS(SELECT 1 FROM inserted i WHERE i.DeleteDateUTC IS NOT NULL)
		-- carrier is being deactivated
		BEGIN
			-- Deactivate active drivers associated to the carrier
			UPDATE tblDriver SET DeleteDateUTC = i.DeleteDateUTC, DeletedByUser = i.DeletedByUser
			FROM tblDriver D
			JOIN inserted i ON i.ID = D.CarrierID
			WHERE D.DeleteDateUTC IS NULL

			-- Deactivate active trucks associated to the carrier
			UPDATE tblTruck SET DeleteDateUTC = i.DeleteDateUTC, DeletedByUser = i.DeletedByUser
			FROM tblTruck T
			JOIN inserted i ON i.ID = T.CarrierID
			WHERE T.DeleteDateUTC IS NULL

			-- Deactivate active trailers associated to the carrier
			UPDATE tblTrailer SET DeleteDateUTC = i.DeleteDateUTC, DeletedByUser = i.DeletedByUser
			FROM tblTrailer T
			JOIN inserted i ON i.ID = T.CarrierID
			WHERE T.DeleteDateUTC IS NULL
		END

		IF EXISTS(SELECT 1 FROM inserted i WHERE i.DeleteDateUTC IS NULL)
		-- carrier is being reactivated
		BEGIN
			-- Reactivate previously active drivers associated to the carrier
			UPDATE tblDriver SET LastChangeDateUTC = GETUTCDATE(), LastChangedByUser = i.LastChangedByUser,
			                     DeleteDateUTC = NULL, DeletedByUser = NULL
			FROM tblDriver D
			JOIN inserted i ON i.ID = D.CarrierID
			JOIN deleted x ON x.ID = D.CarrierID
			WHERE D.DeleteDateUTC = x.DeleteDateUTC AND D.DeletedByUser = x.DeletedByUser

			-- Reactivate previously active trucks associated to the carrier
			UPDATE tblTruck SET LastChangeDateUTC = GETUTCDATE(), LastChangedByUser = i.LastChangedByUser,
			                    DeleteDateUTC = NULL, DeletedByUser = NULL
			FROM tblTruck T
			JOIN inserted i ON i.ID = T.CarrierID
			JOIN deleted d ON d.ID = T.CarrierID
			WHERE T.DeleteDateUTC = d.DeleteDateUTC AND T.DeletedByUser = d.DeletedByUser

			-- Reactivate previously active trailers associated to the carrier
			UPDATE tblTrailer SET LastChangeDateUTC = GETUTCDATE(), LastChangedByUser = i.LastChangedByUser,
			                      DeleteDateUTC = NULL, DeletedByUser = NULL
			FROM tblTrailer T
			JOIN inserted i ON i.ID = T.CarrierID
			JOIN deleted d ON d.ID = T.CarrierID
			WHERE T.DeleteDateUTC = d.DeleteDateUTC AND T.DeletedByUser = d.DeletedByUser
		END
	END
END
GO



COMMIT
SET NOEXEC OFF