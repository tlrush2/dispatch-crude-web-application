SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20) = '3.13.1.2'
	, @NewVersion varchar(20) = '3.13.1.3'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1538: Limit eligible Destination Lists by Region'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

-- 7/11/2016 - Add new system setting to control destinatino region restriction on the create page
INSERT INTO tblSetting (ID, Name, SettingTypeID, Value, Category, CreateDateUTC, CreatedByUser)
	SELECT 61, 'Limit Destination Selection to Origin Region', 2, 'FALSE', 'Order Create', GETUTCDATE(), 'System'
GO	

	
/********************************************
-- Author:		Kevin Alons
-- Create date: 13 May 2013
-- Description:	retrieve all currently eligible Destinations for the specified OriginID/ProductID values
-- Changes:
	- 3.8.11 - 2015/07/28 - KDA - revamp to use revamped viewCustomerOriginDestination
								- add new @shipper parameter
	- 3.13.1.3 - 2016/07/11 - BB - Add logic to check for "filtered by origin region setting"
*********************************************/
ALTER FUNCTION [dbo].[fnRetrieveEligibleDestinations](@originID int, @shipperID int, @productID int, @requireRoute bit = 0) RETURNS TABLE 
AS RETURN
	SELECT D.*
	FROM viewDestination D
	JOIN viewCustomerOriginDestination COD ON COD.DestinationID = D.ID
	LEFT JOIN tblOrigin O ON O.ID = COD.OriginID --Added to gain access to origin region 7/11/16
	LEFT JOIN tblOriginProducts OP ON OP.OriginID = COD.OriginID
	LEFT JOIN tblDestinationProducts DP ON DP.DestinationID = D.ID AND DP.ProductID = OP.ProductID
	WHERE D.DeleteDateUTC IS NULL
	  AND (isnull(@originID, 0) = 0 OR COD.OriginID = @originID)
	  AND (ISNULL(@shipperID, 0) = 0 OR COD.CustomerID = @shipperID)
	  AND (ISNULL(@productID, 0) = 0 OR OP.ProductID = @productID)
	  AND (@requireRoute = 0 OR D.ID IN (SELECT DestinationID FROM viewRoute WHERE OriginID = @OriginID)) 
	  AND (UPPER((SELECT Value FROM tblSetting WHERE ID = 61)) = 'FALSE' OR D.RegionID = O.RegionID)
GO



COMMIT
SET NOEXEC OFF