-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.8.16.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.8.16.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0

DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.8.16'
SELECT  @NewVersion = '3.8.17'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-729: Changes to C.O.D.E. export.'	
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/*=============================================
	Author:			Ben Bloodworth
	Create date:	5 Jun 2015
	Description:	Displays the base information for the CODE export. Please note that some values could change per DispatchCrude customer and thus it may
					be better to retrieve these settings from the Data Exchange settings instead of this view.
					
	Updates:		8/21/15 - BB: Added CASE statments to account for Nulls in gravity readings and added additional filters for rejects and ticket types
=============================================*/
ALTER VIEW viewOrderExport_CODE_Base AS
SELECT 
	OrderID = O.ID
	,OrderTicketID = T.ID
	,Record_ID = '2'
	,Company_ID = C.CodeDXCode
	,Ticket_Type = TT.CodeDXCode
	,Transmit_Ind = '2'  -- Hard coded per Angie Toews (PDS Energy) but may need to be changed to dynamic if the customer says so
	,Run_Type = 1
	,Ticket_Number = CASE WHEN ISNUMERIC(RIGHT(T.CarrierTicketNum,1)) = 0 THEN dbo.fnFixedLenStr(T.CarrierTicketNum,7) 
						  ELSE dbo.fnFixedLenNum(T.CarrierTicketNum,7,0,0) 
					 END
	,Run_Ticket_Date = dbo.fnDateMMDDYY(O.OrderDate)
	,Property_Code = CASE WHEN O.LeaseNum IS NULL THEN '00000000000000' 
						  ELSE
							  CASE WHEN ISNUMERIC(O.LeaseNum) = 1	THEN dbo.fnFixedLenNum(O.LeaseNum,14,0,0)								
								   ELSE dbo.fnFixedLenStr(O.LeaseNum,14) 
							  END
					 END
	,Gathering_Charge = '0'  -- Hard coded per Angie Toews (PDS Energy) but may need to be changed to dynamic if the customer says so
	,Tank_Meter_Num = CASE WHEN ISNUMERIC(T.TankNum) = 1 AND T.TankNum IS NOT NULL THEN dbo.fnFixedLenNum(T.TankNum, 7, 0, 1)
						   WHEN ISNUMERIC(T.TankNum) = 0 AND T.TankNum IS NOT NULL THEN '0000000'
						   ELSE '0000000' --This accounts for when there is a NULL for the tank/meter number
					  END	
	,Adjustment_Ind = '0' -- Hard coded per Angie Toews (PDS Energy) but may need to be changed to dynamic if the customer says so
	,Opening_Reading = dbo.fnOpenCloseReadingCODEFormat(T.ID,TT.CodeDXCode,0)
	,Opening_Date = dbo.fnDateMMDD(O.OrderDate)
	,Closing_Reading = dbo.fnOpenCloseReadingCODEFormat(T.ID,TT.CodeDXCode,1)
	,Closing_Date = dbo.fnDateMMDD(O.DestDepartTime)
	,Meter_Factor = CASE WHEN T.MeterFactor IS NULL THEN '00000000' 
						 ELSE dbo.fnFixedLenNum(T.MeterFactor, 2, 6, 1) 
					END
	,Shrinkage_Incrustation = '1000000' -- Hard coded per Angie Toews (PDS Energy) but may need to be changed to dynamic if the customer says so
	,Opening_Temperature = CASE WHEN T.ProductHighTemp IS NULL THEN '0000' 
								ELSE dbo.fnFixedLenNum(T.ProductHighTemp, 3, 1, 1) 
						   END
	,Closing_Temperature = CASE WHEN T.ProductLowTemp IS NULL THEN '0000' 
								ELSE dbo.fnFixedLenNum(T.ProductLowTemp, 3, 1, 1) 
						   END
	,BSW = CASE WHEN TT.ID IN (1,3) THEN dbo.fnFixedLenNum(T.ProductBSW, 2, 2, 1) --Non-Estmated ticket types (Gauge Run, Meter Run)					
				ELSE '0000'
		   END	
	,Observed_Gravity =	CASE WHEN T.ProductObsGravity IS NULL THEN '000'
							 ELSE dbo.fnFixedLenNum(T.ProductObsGravity, 2, 1, 1)
						END
	,Observed_Temperature = dbo.fnFixedLenNum(T.ProductObsTemp, 3, 1, 1)
	,Pos_Neg_Code = '0' -- Hard coded per Angie Toews (PDS Energy) but may need to be changed to dynamic if the customer says so
	,Total_Net_Volume = dbo.fnFixedLenNum(T.NetUnits, 7, 2, 1)
	,Shippers_Net_Volume = dbo.fnFixedLenNum(T.NetUnits, 7, 2, 1)
	,Corrected_Gravity = CASE WHEN T.Gravity60F IS NULL THEN '000'
							  ELSE dbo.fnFixedLenNum(T.Gravity60F, 2, 1, 1)
						 END
	,Product_Code = dbo.fnFixedLenStr('',3)	--Leave 3 blank spaces unless product is not crude oil. For others lookup PETROEX code	
	,Transmission_Date = dbo.fnFixedLenStr('',3) --Leave 3 blank spaces. Filled in by export reciever.
FROM viewOrderLocalDates O
	JOIN dbo.viewOrderCustomerFinalExportPending EP ON EP.ID = O.ID
	LEFT JOIN viewOrderTicket T ON T.OrderID = O.ID AND T.DeleteDateUTC IS NULL
	LEFT JOIN tblTicketType TT ON TT.ID = O.TicketTypeID
	LEFT JOIN tblCustomer C ON C.ID = O.CustomerID
	LEFT JOIN tblProduct P ON P.ID = O.ProductID
WHERE O.DeleteDateUTC IS NULL
	AND O.Rejected = 0 -- No rejected orders
	AND T.Rejected = 0 -- No rejected tickets
	AND O.StatusID = 4 -- Only Audited orders
	AND TT.ID IN (1,7) -- Only Gauge Run and Gauge Net tickets
GO



/*****************************************************************************************/
-- Author: Kevin Alons
-- Date Created: 27 Feb 2014
-- Purpose: with the provided parameters, export the data and mark done (if doing finalExport)
--		8/12/15 - Ben B. - Expanded procedure to include C.O.D.E. export options
--		8/21/15 - Ben B. - Added summary row to CODE export
/*****************************************************************************************/
ALTER PROCEDURE [dbo].[spOrderExportFinalCustomer]
( 
  @customerID int 
, @exportedByUser varchar(100) = NULL -- will default to SUSER_NAME() -- default value for now
, @exportFormat varchar(100) -- Options are: 'SUNOCO_SUNDEX' or 'CODE_STANDARD'
, @finalExport bit = 1 -- defualt to TRUE
) 
AS BEGIN
	SET NOCOUNT ON
	-- default to the current user if not supplied
	IF @exportedByUser IS NULL SET @exportedByUser = SUSER_NAME()
	
	-- Create variable for error catching with CODE export
	DECLARE @CODEErrorFlag BIT = 0 -- Default to false
	
	-- retrieve the order records to export
	DECLARE @orderIDs IDTABLE
	INSERT INTO @orderIDs (ID)
	SELECT ID
	FROM dbo.viewOrderCustomerFinalExportPending 
	WHERE CustomerID = @customerID 

	BEGIN TRAN exportCustomer
	
	-- export the orders in the specified format
	IF (@exportFormat = 'SUNOCO_SUNDEX') BEGIN
		SELECT E.* 
		INTO #output
		FROM dbo.viewSunocoSundex E
		JOIN @orderIDs IDs ON IDs.ID = E._ID
		WHERE E._CustomerID = @customerID
		ORDER BY _OrderNum
	END
	ELSE
	IF (@exportFormat = 'CODE_STANDARD') BEGIN
		DECLARE @CodeDXCode varchar(2) = (SELECT CodeDXCode FROM tblCustomer WHERE ID = @customerID)
		IF LEN(@CodeDXCode) = 2 BEGIN
			SELECT E.ExportText
			INTO #output2
			FROM dbo.viewOrderExport_CODE E
				JOIN @orderIDs IDs ON IDs.ID = E.OrderID
			WHERE E.CompanyID = @CodeDXCode
		END
		ELSE SET @CODEErrorFlag = 1	
	END

	-- mark the orders as exported FINAL (for a Customer)
	IF (@finalExport = 1) BEGIN
		EXEC spMarkOrderExportFinalCustomer @orderIDs, @exportedByUser
	END
	
	COMMIT TRAN exportCustomer

	-- return the data to 
	IF (@exportFormat = 'SUNOCO_SUNDEX') BEGIN
		SELECT * FROM #output
	END
	
	IF (@exportFormat = 'CODE_STANDARD') BEGIN
		IF @CODEErrorFlag = 0 BEGIN
			SELECT * FROM #output2
			--Include Summary Row
			UNION
			SELECT
				'4' -- Summary Record Type RecordID
				+ X.Company_ID 
				+ X.Record_Count 
				+ X.Pos_Neg_Code 
				+ X.Total_Net
				+ X.Shipper_Net
				+ dbo.fnFixedLenStr('',89)
				+ dbo.fnFixedLenStr('',3)
			FROM
				(SELECT
					ECB.Company_ID
					,CAST(dbo.fnFixedLenNum(COUNT(*), 10, 0, 1) AS VARCHAR)	AS Record_Count
					,ECB.Pos_Neg_Code
					,CAST(dbo.fnFixedLenNum(SUM(CAST(ECB.Total_Net_Volume AS DECIMAL)), 13, 0, 1) AS VARCHAR) Total_Net
					,CAST(dbo.fnFixedLenNum(SUM(CAST(ECB.Shippers_Net_Volume AS DECIMAL)), 13, 0, 1) AS VARCHAR) Shipper_Net
				FROM dbo.viewOrderExport_CODE_Base ECB			
					JOIN @orderIDs IDs ON IDs.ID = ECB.OrderID			
				WHERE ECB.Company_ID = @CodeDXCode
				GROUP BY ECB.Company_ID,ECB.Pos_Neg_Code) X
		END
		ELSE SELECT 'Invalid Shipper CODE ID specified.  Fix in Shipper maintenance table.'
	END		
END
GO


EXEC _spRebuildAllObjects
GO

COMMIT
SET NOEXEC OFF