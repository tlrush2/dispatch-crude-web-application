-- =============================================
-- Author:		Kevin Alons
-- Create date: 19 Dec 2012
-- Description:	trigger to add a unique, incrementing OrderNum to each new Order (manual Identity column)
--				and other supporting/coordinating logic
-- =============================================
ALTER TRIGGER [dbo].[trigOrder_IU] ON [dbo].[tblOrder] AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	WHILE (SELECT count(*) FROM inserted i JOIN tblOrder O ON O.ID = i.ID WHERE O.OrderNum IS NULL) > 0
	BEGIN
		UPDATE tblOrder 
		  SET OrderNum = isnull((SELECT max(OrderNum) FROM tblOrder), 100000) + 1
			, CreateDateUTC = getutcdate()
		WHERE ID = (SELECT min(O.ID) FROM tblOrder O JOIN inserted i ON i.ID = O.ID WHERE O.OrderNum IS NULL)
	END
	
	-- re-compute the OriginMinutes (in case the website failed to compute it properly)
	IF UPDATE(OriginArriveTimeUTC) OR UPDATE(OriginDepartTimeUTC)
	BEGIN
		UPDATE tblOrder SET OriginMinutes = datediff(minute, i.OriginArriveTimeUTC, i.OriginDepartTimeUTC)
		FROM tblOrder O
		JOIN inserted i ON i.ID = O.ID
	END
	-- re-compute the DestWaitMinutes (in case the website failed to compute it properly)
	IF UPDATE(DestArriveTimeUTC) OR UPDATE(DestDepartTimeUTC)
	BEGIN
		UPDATE tblOrder SET DestMinutes = datediff(minute, i.DestArriveTimeUTC, i.DestDepartTimeUTC)
		FROM tblOrder O
		JOIN inserted i ON i.ID = O.ID
	END
	
	-- ensure missing Routes are created and assigned for the specified Origin/Destination combination
	IF UPDATE(OriginID) OR UPDATE(DestinationID)
	BEGIN
		-- create any missing Route records
		INSERT INTO tblRoute (OriginID, DestinationID, CreateDateUTC, CreatedByUser)
			SELECT i.OriginID, i.DestinationID, GETUTCDATE(), ISNULL(i.LastChangedByUser, i.CreatedByUser)
			FROM inserted i
			LEFT JOIN tblRoute R ON R.OriginID = i.OriginID AND R.DestinationID = i.DestinationID
			WHERE i.OriginID IS NOT NULL AND i.DestinationID IS NOT NULL AND R.ID IS NULL
		-- ensure the Order records refer to the correct Route (ID)
		UPDATE tblOrder SET RouteID = R.ID
		FROM tblOrder O
		JOIN inserted i ON i.ID = O.ID
		LEFT JOIN tblRoute R ON R.OriginID = O.OriginID AND R.DestinationID = O.DestinationID

		-- update the ActualMiles from the related Route
		UPDATE tblOrder SET ActualMiles = R.ActualMiles
		FROM tblOrder O
		JOIN inserted i ON i.ID = O.ID
		JOIN tblRoute R ON R.ID = O.RouteID
	END
	
	IF (UPDATE(OriginID))
	BEGIN
		-- update Order.ProducerID to match what is assigned to the new Origin
		UPDATE tblOrder SET ProducerID = OO.ProducerID
		FROM tblOrder O
		JOIN deleted d ON d.ID = O.ID
		JOIN tblOrigin OO ON OO.ID = O.OriginID
		WHERE d.OriginID <> O.OriginID

		-- update Order.OriginUomID to match what is assigned to the new Origin (quantities are updated below)
		UPDATE tblOrder SET OriginUomID = OO.UomID
		FROM tblOrder O
		JOIN deleted d ON d.ID = O.ID
		JOIN tblOrigin OO ON OO.ID = O.OriginID
		WHERE d.OriginID <> O.OriginID AND d.OriginUomID <> OO.UomID
	END
	
	-- keep the DestUomID in sync with the Destination (units are updated below)
	IF (UPDATE(DestinationID))
	BEGIN
		-- update Order.DestUomID to match what is assigned to the new Destination
		UPDATE tblOrder 
		  SET DestUomID = DD.UomID
		FROM tblOrder O
		JOIN deleted d ON d.ID = O.ID
		JOIN tblDestination DD ON DD.ID = O.DestinationID
		WHERE d.DestinationID <> O.DestinationID AND d.DestUomID <> DD.UomID
	END
	
	-- ensure that any set PrintStatusID values are reset to 0 when backing up the general StatusID value
	IF (UPDATE(StatusID))
	BEGIN
		UPDATE tblOrder 
		  SET DeliverPrintStatusID = 0 
		  FROM tblOrder O
		  JOIN deleted d ON d.ID = O.ID
		WHERE O.StatusID <> d.StatusID AND O.StatusID IN (-10, 1, 2, 7, 8)

		UPDATE tblOrder 
		  SET PickupPrintStatusID = 0 
		  FROM tblOrder O
		  JOIN deleted d ON d.ID = O.ID
		WHERE O.StatusID <> d.StatusID AND O.StatusID IN (-10, 1, 2, 7)
	END

	-- ensure that any change to the DriverID will update the Truck/Trailer/Trailer2 values to this driver's defaults
	IF (UPDATE(DriverID))
	BEGIN
		UPDATE tblOrder 
		  SET TruckID = DR.TruckID
			, TrailerID = DR.TrailerID
			, Trailer2ID = DR.Trailer2ID
		  FROM tblOrder O
		  JOIN deleted d ON d.ID = O.ID
		  JOIN tblDriver DR ON DR.ID = O.DriverID
		WHERE O.DriverID <> d.DriverID 
		  AND O.StatusID IN (-10,1,2,7,9) -- Generated, Assigned, Dispatched, Accepted, Declined
	END
	
	-- just updating the tickets here, trigOrderTicket_IU will in turn update this table
	-- Note: the TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU') ) = 1 statement prevents recursion
	IF (TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU')) = 1)
	BEGIN
		-- ensure any existing OrderTickets are recomputed with the new OriginUOM
		UPDATE tblOrderTicket
		  SET GrossUnits = dbo.fnConvertUOM(GrossUnits, d.OriginUomID, i.OriginUomID)
			, NetUnits = dbo.fnConvertUOM(NetUnits, d.OriginUomID, i.OriginUomID)
		FROM tblOrderTicket OT
		JOIN tblOrder i ON i.ID = OT.OrderID
		JOIN deleted d ON d.ID = i.ID
		WHERE i.OriginUomID <> d.OriginUomID

	END
	
	-- recalculate the Destination Unit values if only if the DestUomID changed but the values didn't
	BEGIN
		UPDATE tblOrder
		  SET DestGrossUnits = dbo.fnConvertUOM(O.DestGrossUnits, d.DestUomID, O.DestUomID)
			, DestNetUnits = dbo.fnConvertUOM(O.DestNetUnits, d.DestUomID, O.DestUomID)
		FROM tblOrder O
		JOIN deleted d ON d.ID = O.ID
		WHERE d.DestUomID <> O.DestUomID 
		  AND d.DestGrossUnits = O.DestGrossUnits
		  AND d.DestNetUnits = O.DestNetUnits
	END
	--NOTE: we do not update the DestOpenMeterUnits/DestClosignMeterUnits since they don't auto update
	--  (and they simply represent a reading that is assumed to in the DestUomID UOM at entry)
	--  (if they are updated in the future [manually] the will calculate again using the DestUomID at that time)
	
	-- ensure that all changed orders have their LastChangeDateUTC set to UTCNOW
	UPDATE tblOrder
	  SET LastChangeDateUTC = GETUTCDATE()
		, LastChangedByUser = ISNULL(O.LastChangedByUser, 'System')
	FROM tblOrder O
	JOIN inserted i ON i.ID = O.ID
	WHERE O.LastChangeDateUTC IS NULL OR abs(datediff(second, O.LastChangeDateUTC, GETUTCDATE())) > 2
	
END

GO