-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.7.19.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.7.19.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0

DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.7.19'
SELECT  @NewVersion = '3.7.20'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Gauger App Sync: fix newly created bug blocking Gauger Ticket syncing'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

ALTER TRIGGER trigGaugerOrderTicket_IU ON tblGaugerOrderTicket AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	-- Note: the TRIGGER_NESTLEVEL( OBJECT_ID('trigGaugerOrderTicket_IU') ) = 1 statement prevents recursion
	IF (TRIGGER_NESTLEVEL( OBJECT_ID('trigGaugerOrderTicket_IU')) = 1) BEGIN
		
		-- only do anything if something actually changed
		IF EXISTS (SELECT * FROM inserted EXCEPT SELECT * FROM deleted)
		BEGIN
			PRINT 'trigGaugerOrderTicket_IU FIRED'
			
			/**********  START OF VALIDATION SECTION ************************/
			DECLARE @errorString varchar(1000)
			SET @errorString = '' -- default value (so we can APPEND values)

			IF EXISTS (
				SELECT * 
				FROM tblOrder 
				WHERE ID IN (SELECT OrderID FROM inserted) 
				  -- prevent changes to any tickets belonging to a Delivered or Audited Order
				  AND StatusID IN (4))
			BEGIN
				IF (dbo.fnToBool(dbo.fnSettingValue(35)) = 1)
					PRINT 'Gauger Ticket for an AUDITED orders is being modified - please investigate why!'
				ELSE 				
					SET @errorString = @errorString + '|Gauger Tickets of Audited Orders cannot be modified'
			END

			-- prevent reassigning a ticket from one order to another
			IF EXISTS (
				SELECT i.* 
				FROM inserted i
				JOIN deleted d ON i.UID = d.UID
				WHERE i.OrderID <> d.OrderID)
			BEGIN
				SET @errorString = @errorString + '|Tickets cannot be moved to a different Order'
			END

			IF EXISTS (
				SELECT OT.OrderID
				FROM tblGaugerOrderTicket OT
				JOIN inserted i ON i.OrderID = OT.OrderID AND i.CarrierTicketNum = OT.CarrierTicketNum AND i.UID <> OT.UID
				WHERE OT.DeleteDateUTC IS NULL and I.DeleteDateUTC IS NULL
				GROUP BY OT.OrderID, OT.CarrierTicketNum
				HAVING COUNT(*) > 1
			) 
			BEGIN
				SET @errorString = @errorString + '|Duplicate active Ticket Numbers are not allowed'
			END
			
			-- Tank is always required for Gauge Run Basic [1] (even for REJECTED tickets)
			IF EXISTS (SELECT * FROM inserted WHERE TicketTypeID IN (1) AND OriginTankID IS NULL AND TankNum IS NULL AND DeleteDateUTC IS NULL)
			BEGIN
				SET @errorString = @errorString + '|Tank is required for "Gauge Run Basic" tickets'
			END

			-- CarrierTicketNum (Ticket #) is always required for Gauge Run Basic [1] (even for REJECTED tickets)
			IF EXISTS (SELECT * FROM inserted WHERE TicketTypeID IN (1) AND CarrierTicketNum IS NULL AND DeleteDateUTC IS NULL)
			BEGIN
				SET @errorString = @errorString + '|Ticket # value is required for "Gauge Run Basic" tickets'
			END

			-- Product Measurement values are only required for NOT-REJECTED Gauge Run Basic [1]
			IF EXISTS (SELECT * FROM inserted WHERE Rejected = 0 AND DeleteDateUTC IS NULL
					AND (TicketTypeID IN (1) AND (ProductObsTemp IS NULL OR ProductObsGravity IS NULL OR ProductBSW IS NULL))
				)
			BEGIN
				SET @errorString = @errorString + '|All Product Measurement values are required for "Gauge Run Basic" tickets'
			END
			
			-- Opening Gauge values are only required for NOT-REJECTED Gauge Run Basic [1]
			IF EXISTS (SELECT * FROM inserted WHERE TicketTypeID IN (1) AND Rejected = 0 AND DeleteDateUTC IS NULL
				AND (OpeningGaugeFeet IS NULL OR OpeningGaugeInch IS NULL OR OpeningGaugeQ IS NULL))
			BEGIN
				SET @errorString = @errorString + '|All Opening Gauge values are required for "Gauge Run Basic" tickets'
			END

			-- if any errors were detected, cancel the entire operation (transaction) and return a LINEFEED-deliminated list of errors
			IF (len(@errorString) > 0)
			BEGIN
				SET @errorString = replace(substring(@errorString, 2, 1000), '|', char(13) + char(10))
				RAISERROR(@errorString, 16, 1)
				IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
			END
			
			/**********  END OF VALIDATION SECTION ************************/
			
			-- update any existing OrderTicket records that are still only populated from Gauger entry
			UPDATE tblOrderTicket
				SET CarrierTicketNum = i.CarrierTicketNum
				, DispatchConfirmNum = i.DispatchConfirmNum
				, OriginTankID = i.OriginTankID
				, TankNum = i.TankNum
				, ProductObsGravity = i.ProductObsGravity
				, ProductObsTemp = i.ProductObsTemp
				, ProductBSW = i.ProductBSW
				, OpeningGaugeFeet = i.OpeningGaugeFeet
				, OpeningGaugeInch = i.OpeningGaugeInch
				, OpeningGaugeQ = i.OpeningGaugeQ
				, BottomFeet = i.BottomFeet
				, BottomInches = i.BottomInches
				, BottomQ = i.BottomQ
				, Rejected = i.Rejected
				, RejectReasonID = i.RejectReasonID
				, RejectNotes = i.RejectNotes
				, SealOff = i.SealOff
				, SealOn = i.SealOn
				, FromMobileApp = i.FromMobileApp
				, LastChangeDateUTC = i.LastChangeDateUTC
				, LastChangedByUser = i.LastChangedByUser
				, DeleteDateUTC = i.DeleteDateUTC
				, DeletedByUser = i.DeletedByUser
			FROM tblOrderTicket OT
			JOIN tblOrder O ON O.ID = OT.OrderID AND O.StatusID IN (-9, -10) -- GAUGER, GENERATED StatusID			
			JOIN inserted i ON i.UID = OT.UID AND i.CreateDateUTC = OT.CreateDateUTC
			LEFT JOIN deleted d ON d.UID = OT.UID AND isnull(d.LastChangeDateUTC, getutcdate()) = isnull(OT.LastChangeDateUTC, getutcdate())

			-- create new OrderTicket records not not present
			INSERT INTO tblOrderTicket (UID, OrderID, TicketTypeID, CarrierTicketNum, DispatchConfirmNum, OriginTankID, TankNum
				, ProductObsGravity, ProductObsTemp, ProductBSW, OpeningGaugeFeet, OpeningGaugeInch, OpeningGaugeQ
				, BottomFeet, BottomInches, BottomQ, Rejected, RejectReasonID, RejectNotes, SealOff, SealOn, FromMobileApp
				, CreateDateUTC, CreatedByUser)
				SELECT i.UID, i.OrderID, GTT.TicketTypeID, i.CarrierTicketNum, i.DispatchConfirmNum, i.OriginTankID, i.TankNum
					, i.ProductObsGravity, i.ProductObsTemp, i.ProductBSW, i.OpeningGaugeFeet, i.OpeningGaugeInch, i.OpeningGaugeQ
					, i.BottomFeet, i.BottomInches, i.BottomQ, i.Rejected, i.RejectReasonID, i.RejectNotes, i.SealOff, i.SealOn, i.FromMobileApp
					, i.CreateDateUTC, i.CreatedByUser
				FROM inserted i
				JOIN tblGaugerTicketType GTT ON GTT.ID = i.TicketTypeID
				LEFT JOIN tblOrderTicket OT ON OT.UID = i.UID
				WHERE OT.UID IS NULL

			/* START DB AUDIT *********************************************************/
			BEGIN TRY
				IF EXISTS(SELECT * FROM tblSetting WHERE ID = 32 AND (Value LIKE 'true' OR Value LIKE 'yes'))
					INSERT INTO tblGaugerOrderTicketDbAudit (DBAuditDate, UID, OrderID, CarrierTicketNum, TicketTypeID, TankNum, ProductObsGravity, ProductObsTemp, OpeningGaugeFeet, OpeningGaugeInch, OpeningGaugeQ, Rejected, RejectNotes, SealOff, SealOn, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser, FromMobileApp, OriginTankID, BottomFeet, BottomInches, BottomQ, RejectReasonID)
						SELECT GETUTCDATE(), UID, OrderID, CarrierTicketNum, TicketTypeID, TankNum, ProductObsGravity, ProductObsTemp, OpeningGaugeFeet, OpeningGaugeInch, OpeningGaugeQ, Rejected, RejectNotes, SealOff, SealOn, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser, FromMobileApp, OriginTankID, BottomFeet, BottomInches, BottomQ, RejectReasonID
						FROM deleted d
			END TRY
			BEGIN CATCH
				PRINT 'trigGaugerOrderTicket_IU.DBAUDIT FAILURE: ' + ERROR_MESSAGE()
			END CATCH
			/* END DB AUDIT *********************************************************/

			PRINT 'trigGaugerOrderTicket_IU COMPLETE'
		END
	END
END


GO
EXEC sp_settriggerorder @triggername=N'[trigGaugerOrderTicket_IU]', @order=N'First', @stmttype=N'INSERT'
GO
EXEC sp_settriggerorder @triggername=N'[trigGaugerOrderTicket_IU]', @order=N'First', @stmttype=N'UPDATE'
GO

COMMIT
SET NOEXEC OFF