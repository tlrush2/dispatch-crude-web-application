-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.11.8'
SELECT  @NewVersion = '3.11.9'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Created viewUom to allow easier adding of Uom Type to UOM maintenance page.'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/***********************************/
-- Date Created: 1 Apr 2016
-- Author: Ben Bloodworth
-- Purpose: return Uom records with Uom Type values
-- Changes: 
/***********************************/
CREATE VIEW viewUom AS
SELECT U.*
	, UT.ID AS UomTypeTypeID
	, UT.Name AS UomTypeTypeName
FROM dbo.tblUom U
	LEFT JOIN dbo.tblUomType UT ON UT.ID = U.UomTypeID
GO


COMMIT 
SET NOEXEC OFF