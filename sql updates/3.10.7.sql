SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.10.6'
SELECT  @NewVersion = '3.10.7'

-- only ensure the curr version matches when we are not testing with an "x.x.x" DEV version
IF @NewVersion <> 'x.x.x' AND (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

-- only update the DB VERSION when not testing with an "x.x.x" DEV version
IF @NewVersion <> 'x.x.x' 
BEGIN
	UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

	INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
		SELECT @NewVersion, 0, 'DCWEB-967: Add "Driver Equipment" asset tracking for mobile equipment'
		EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
END
GO

SET QUOTED_IDENTIFIER ON
GO


/*  3.10.7 - 2015/12/22 - BB - Add equipment tracking columns to tblDriver_sync (DCWEB-967)  */
ALTER TABLE tblDriver_Sync
	ADD TabletID VARCHAR(20) NULL
	, PrinterSerial VARCHAR(100) NULL
GO



/*  3.10.7 - 2015/12/17 - BB - Create Driver Equipment Type Table (DCWEB-967)  */
CREATE TABLE tblDriverEquipmentType
(
	ID INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_DriverEquipmentType PRIMARY KEY
	, Name VARCHAR(50) NOT NULL
	, CreateDateUTC DATETIME NOT NULL  CONSTRAINT DF_DriverEquipmentType_CreateDate DEFAULT(GETUTCDATE())
	, CreatedByUser VARCHAR(100) NOT NULL  CONSTRAINT DF_DriverEquipmentType_CreatedByUser DEFAULT('System')
	, LastChangeDateUTC DATETIME NULL
	, LastChangedByUser VARCHAR(100) NULL
	, DeleteDateUTC DATETIME NULL
	, DeletedByUser VARCHAR(100) NULL
)
GO
GRANT SELECT, INSERT, UPDATE ON tblDriverEquipmentType TO role_iis_acct
GO


/*  3.10.7 - 2015/12/17 - BB - Insert Default records ("Tablet" & "Printer") into Driver Equipment Type table (DCWEB-967)  */
SET IDENTITY_INSERT tblDriverEquipmentType ON
INSERT tblDriverEquipmentType (ID, Name, CreateDateUTC, CreatedByUser)
	SELECT 1, 'Tablet', GETUTCDATE(), 'System'
	UNION
	SELECT 2, 'Printer', GETUTCDATE(), 'System'
	EXCEPT SELECT ID, Name, CreateDateUTC, CreatedByUser FROM tblDriverEquipmentType
SET IDENTITY_INSERT tblDriverEquipmentType OFF
GO


/*  3.10.7 - 2015/12/18 - BB - Create Driver Equipment Manufacturer Table (DCWEB-967)  */
CREATE TABLE tblDriverEquipmentManufacturer
(
	ID INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_DriverEquipmentManufacturer PRIMARY KEY	
	, Name VARCHAR(50) NOT NULL
	, CreateDateUTC DATETIME NOT NULL CONSTRAINT DF_DriverEquipmentManufacturer_CreateDate DEFAULT(GETUTCDATE())
	, CreatedByUser VARCHAR(100) NOT NULL CONSTRAINT DF_DriverEquipmentManufacturer_CreatedByUser DEFAULT('System')
	, LastChangeDateUTC DATETIME NULL
	, LastChangedByUser VARCHAR(100) NULL
	, DeleteDateUTC DATETIME NULL
	, DeletedByUser VARCHAR(100) NULL
)
GO
GRANT SELECT, INSERT, UPDATE ON tblDriverEquipmentManufacturer TO role_iis_acct
GO


/*  3.10.7 - 2015/12/18 - BB - Insert Default records ("Samsung" & "Zebra") into Driver Equipment Manufacturer table (DCWEB-967)  */
SET IDENTITY_INSERT tblDriverEquipmentManufacturer ON
INSERT tblDriverEquipmentManufacturer (ID, Name, CreateDateUTC, CreatedByUser)
	SELECT 1, 'Unknown', GETUTCDATE(), 'System'
	UNION
  SELECT 2, 'Samsung', GETUTCDATE(), 'System'
	UNION
	SELECT 3, 'Zebra', GETUTCDATE(), 'System'
	EXCEPT SELECT ID, Name, CreateDateUTC, CreatedByUser FROM tblDriverEquipmentManufacturer
SET IDENTITY_INSERT tblDriverEquipmentManufacturer OFF
GO


/*  3.10.7 - 2015/12/17 - BB - Create Driver Equipment Table (DCWEB-967)  */
-- Changes - 2016/02/01 - KDA - Removed square brackets around column names in Unique index creation statment below
CREATE TABLE tblDriverEquipment
(
	ID INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_DriverEquipment PRIMARY KEY
	, DriverEquipmentTypeID INT NOT NULL CONSTRAINT FK_DriverEquipment_DriverEquipmentType FOREIGN KEY REFERENCES tblDriverEquipmentType(ID)	
	, DriverEquipmentManufacturerID INT NOT NULL CONSTRAINT FK_DriverEquipment_DriverEquipmentManufacturer FOREIGN KEY REFERENCES tblDriverEquipmentManufacturer(ID)		
	, ModelNum VARCHAR(50) NULL
	, SerialNum VARCHAR(100) NULL
	, PhoneNum VARCHAR(20) NULL
	, CreateDateUTC DATETIME NOT NULL CONSTRAINT DF_DriverEquipment_CreateDate DEFAULT(GETUTCDATE())
	, CreatedByUser VARCHAR(100) NOT NULL CONSTRAINT DF_DriverEquipment_CreatedByUser DEFAULT('System')
	, LastChangeDateUTC DATETIME NULL
	, LastChangedByUser VARCHAR(100) NULL
	, DeleteDateUTC DATETIME NULL
	, DeletedByUser VARCHAR(100) NULL
)
GO
CREATE UNIQUE NONCLUSTERED INDEX udxDriverEquipment_SerialNum_ManufacturerID 
ON tblDriverEquipment (DriverEquipmentManufacturerID ASC, SerialNum ASC)
WHERE SerialNum IS NOT NULL
GO
GRANT SELECT, INSERT, UPDATE ON tblDriverEquipment TO role_iis_acct
GO


/***************************************
Date Created: 2016/02/01 - 3.10.7
Author: Kevin Alons
Purpose: Add or update the new TabletID and/or Printer Serial number to tblDriverEquipment
***************************************/
CREATE PROCEDURE spDriverSync_UpdateDevices
(
  @TabletID varchar(20)
, @PrinterSerial varchar(100)
, @UserName varchar(100)
) AS
BEGIN
	-- insert or update an equipment record for the specified tablet (if provided)
	IF (@TabletID IS NOT NULL AND @TabletID <> '')
	BEGIN
		INSERT INTO tblDriverEquipment (DriverEquipmentTypeID, DriverEquipmentManufacturerID, PhoneNum, CreatedByUser)
			SELECT 1, 1, @TabletID, @UserName 
			WHERE NOT EXISTS (SELECT ID FROM tblDriverEquipment DE WHERE DE.PhoneNum = @TabletID AND DriverEquipmentTypeID = 1)
		IF (@@ROWCOUNT = 0)	
			UPDATE tblDriverEquipment 
			SET LastChangeDateUTC = getutcdate(), LastChangedByUser = @UserName
				, DeleteDateUTC = NULL, DeletedByUser = NULL 
			WHERE PhoneNum = @TabletID AND DriverEquipmentTypeID = 1
	END
	IF (@PrinterSerial IS NOT NULL AND @PrinterSerial <> '')
	BEGIN
		INSERT INTO tblDriverEquipment (DriverEquipmentTypeID, DriverEquipmentManufacturerID, SerialNum, CreatedByUser)
			SELECT 2, 1, @PrinterSerial, @UserName
			WHERE NOT EXISTS (SELECT ID FROM tblDriverEquipment DE WHERE DE.SerialNum = @PrinterSerial AND DriverEquipmentTypeID = 2)
		IF (@@ROWCOUNT = 0)
			UPDATE tblDriverEquipment 
			SET LastChangeDateUTC = getutcdate(), LastChangedByUser = @UserName
				, DeleteDateUTC = NULL, DeletedByUser = NULL 
			WHERE SerialNum = @PrinterSerial AND DriverEquipmentTypeID = 2
	END
END
GO
GRANT EXECUTE ON spDriverSync_UpdateDevices TO role_iis_acct
GO



/***********************************/
-- Date Created: 26 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Drivers table records with "friendly" translated values included
-- Changes:  3.10.7 - 2016/01/27 - BB - Added TabletID and PrinterSerial from tblDriver_Sync
/***********************************/
ALTER VIEW viewDriverBase AS
SELECT X.*
	, CASE WHEN Active = 1 THEN '' ELSE 'Deleted: ' END + FullName AS FullNameD
FROM (
	SELECT D.*
	, Active = cast(CASE WHEN isnull(C.DeleteDateUTC, D.DeleteDateUTC) IS NULL THEN 1 ELSE 0 END as bit) 
	, FullName = D.FirstName + ' ' + D.LastName 
	, FullNameLF = D.LastName + ', ' + D.FirstName 
	, CarrierType= isnull(CT.Name, 'Unknown') 
	, Carrier = C.Name 
	, StateAbbrev = S.Abbreviation 
	, Truck = T.FullName
	, Trailer = T1.FullName 
	, Trailer2 = T2.FullName 
	, Region = R.Name 
	, DS.MobileAppVersion
	, DS.TabletID
	, DS.PrinterSerial
	FROM dbo.tblDriver D 
	JOIN dbo.tblCarrier C ON C.ID = D.CarrierID 
	LEFT JOIN dbo.tblDriver_Sync DS ON DS.DriverID = D.ID
	LEFT JOIN dbo.tblCarrierType CT ON CT.ID = C.CarrierTypeID
	LEFT JOIN tblState S ON S.ID = D.StateID
	LEFT JOIN viewTruck T ON T.ID = D.TruckID
	LEFT JOIN viewTrailer T1 ON T1.ID = D.TrailerID
	LEFT JOIN viewTrailer T2 ON T2.ID = D.Trailer2ID
	LEFT JOIN tblRegion R ON R.ID = D.RegionID
) X
GO


/*******************************************
-- Date Created: 22 Apr 2013
-- Author: Kevin Alons
-- Purpose: validate parameters, if valid insert/update the tblDriver_Sync table for the specified DriverID
-- Code References:
					DispatchCrude/Models/Sync/DriverApp/MasterData.cs
-- Changes: 3.10.7 - 2016/01/28 - BB - Added TabletID and PrinterSerial to the synced values
            3.10.7 - 2016/02/01 - KDA - Added EXEC spDriverSync_UpdateDevices @TabletID, @PrinterSerial, @UserName
*******************************************/
ALTER PROCEDURE spDriver_Sync
(
  @UserName varchar(100)
, @DriverID int
, @MobileAppVersion varchar(25) = NULL
, @SyncDateUTC datetime = NULL
, @PasswordHash varchar(25) = NULL
, @TabletID varchar(30) = NULL
, @PrinterSerial varchar(30) = NULL
, @Valid bit = NULL output
, @Message varchar(255) = NULL output
) AS
BEGIN
	-- if resetting, delete the entire record (it will be recreated below)
	IF (@SyncDateUTC IS NULL)
	BEGIN
		DELETE FROM tblDriver_Sync WHERE DriverID = @DriverID
		-- result of count(*) query will be 1 or 0 (true or false respectively)
		SELECT @Valid = (SELECT COUNT(*) FROM tblDriver WHERE ID = @DriverID)
		IF (@Valid = 0)
			SELECT @Message = 'DriverID was not valid'
	END
	ELSE
	BEGIN
		-- query the ValidatePasswordHash setting parameter
		DECLARE @ValidatePasswordHash bit
		SELECT @ValidatePasswordHash = CASE WHEN Value IN ('1', 'true', 'yes') THEN 1 ELSE 0 END
		FROM tblSetting
		WHERE ID = 13

		-- result of count(*) query will be 1 or 0 (true or false respectively)
		SELECT @Valid = (SELECT count(*) FROM tblDriver_Sync WHERE DriverID = @DriverID 
			AND (@ValidatePasswordHash = 0 OR PasswordHash = @PasswordHash))
		IF (@Valid = 0)
			SELECT @Message = 'PasswordHash was not valid'
	END
	
	EXEC spDriverSync_UpdateDevices @TabletID, @PrinterSerial, @UserName

	IF (@Valid = 1)
	BEGIN
		-- if a sync record already exists, just update the new LastSync value
		UPDATE tblDriver_Sync 
		SET LastSyncUTC = @SyncDateUTC
		, MobileAppVersion = @MobileAppVersion
		, TabletID = @TabletID
		, PrinterSerial = @PrinterSerial 
		WHERE DriverID = @DriverID
		-- otherwise insert a new record with a new passwordhash value
		INSERT INTO tblDriver_Sync (DriverID, MobileAppVersion, LastSyncUTC, PasswordHash, TabletID, PrinterSerial)
			SELECT @DriverID, @MobileAppVersion, NULL, dbo.fnGeneratePasswordHash(), @TabletID, @PrinterSerial
			FROM tblDriver D
			LEFT JOIN tblDriver_Sync DS ON DS.DriverID = D.ID
			WHERE D.ID = @DriverID AND DS.DriverID IS NULL

		-- return the current "Master" data
		SELECT *, TabletID = @TabletID, PrinterSerial = @PrinterSerial FROM dbo.fnDriverMasterData(@Valid, @DriverID, @UserName)
	END
	ELSE
	BEGIN
		SELECT @Valid AS Valid, @UserName AS UserName, 0 AS DriverID, NULL AS DriverName, 0 AS MobilePrint
			, NULL AS LastSyncUTC
			, (SELECT cast(Value as int) FROM tblSetting WHERE ID = 11) AS SyncMinutes
			, (SELECT Value FROM tblSetting WHERE ID = 0) AS SchemaVersion
			, @MobileAppVersion AS MobileAppVersion			
			, (SELECT Value FROM tblSetting WHERE ID = 12) AS LatestAppVersion
			, NULL AS PasswordHash
			, @TabletID AS TabletID
			, @PrinterSerial AS PrinterSerial
		FROM tblSetting S WHERE S.ID = 0
	END 
END
GO


/***********************************
-- Date Created: 28 Jan 2016  3.10.7
-- Author: Ben Bloodworth
-- Purpose: Return Driver Equipment records.  Also return the driver to last use each device listed if applicable.
***********************************/
CREATE VIEW viewDriverEquipment AS
SELECT DE.*
	, DEM.Name AS Manufacturer
	, DET.Name AS EquipmentType
	, LastDriver = (CASE WHEN DE.SerialNum = DSP.PrinterSerial THEN DP.FirstName + ' ' + DP.LastName
						 WHEN DE.PhoneNum = DST.TabletID THEN DT.FirstName + ' ' + DT.LastName END)
	, LastSync = (CASE WHEN DE.SerialNum = DSP.PrinterSerial THEN DSP.LastSyncUTC
					   WHEN DE.PhoneNum = DST.TabletID THEN DST.LastSyncUTC END)
FROM tblDriverEquipment DE
	LEFT JOIN tblDriverEquipmentManufacturer DEM ON DEM.ID = DE.DriverEquipmentManufacturerID
	LEFT JOIN tblDriverEquipmentType DET ON DET.ID = DE.DriverEquipmentTypeID
	LEFT JOIN tblDriver_Sync DSP ON DSP.PrinterSerial = DE.SerialNum
	LEFT JOIN tblDriver_Sync DST ON DST.TabletID = DE.PhoneNum
	LEFT JOIN tblDriver DP ON DP.ID = DSP.DriverID
	LEFT JOIN tblDriver DT ON DT.ID = DST.DriverID
GO


/*  Refresh views affected by tblDriver_Sync change to include new fields added above  */
EXEC sp_RefreshView viewDriverBase
GO
EXEC sp_RefreshView viewDriver
GO
EXEC sp_RefreshView viewDriverAvailability
GO
EXEC sp_RefreshView viewOrder
GO
EXEC sp_RefreshView viewSyncError


COMMIT
SET NOEXEC OFF