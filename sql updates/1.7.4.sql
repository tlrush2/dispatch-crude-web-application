/* fix bug in trigger causing an error when rejecting a Tank
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '1.7.3', @NewVersion = '1.7.4'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 6 Feb 2013
-- Description:	trigger to ensure the entered values for an OrderTicket are actually valid
-- =============================================
ALTER TRIGGER [dbo].[trigOrderTicket_IU_Validate] ON [dbo].[tblOrderTicket] AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	IF (SELECT COUNT(*) FROM (
		SELECT OT.OrderID
		FROM tblOrderTicket OT
		WHERE OrderID IN (SELECT DISTINCT OrderID FROM inserted)
		  AND OT.DeleteDate IS NULL
		GROUP BY OT.OrderID, OT.CarrierTicketNum
		HAVING COUNT(*) > 1) v) > 0
	BEGIN
		RAISERROR('Duplicate Ticket Numbers are not allowed', 16, 1)
		RETURN
	END
	
	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (3) AND BOLNum IS NULL) > 0
	BEGIN
		RAISERROR('BOL # value is required for Meter Run tickets', 16, 1)
		RETURN
	END
	
	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1, 2) AND TankNum IS NULL) > 0
	BEGIN
		RAISERROR('Tank # value is required for Gauge Run & Net Barrel tickets', 16, 1)
		RETURN
	END

	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1) AND CarrierTicketNum IS NULL) > 0
	BEGIN
		RAISERROR('Ticket # value is required for Gauge Run tickets', 16, 1)
		RETURN
	END

	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1) AND BarrelsPerInch IS NULL) > 0
	BEGIN
		RAISERROR('BarrelsPerInch value is required for Gauge Run tickets', 16, 1)
		RETURN
	END

	IF (SELECT COUNT(*) FROM inserted WHERE 
		(TicketTypeID IN (1, 2) AND (ProductOBsTemp IS NULL OR ProductObsGravity IS NULL OR ProductBSW IS NULL))
			OR (TicketTypeID IN (1) AND (ProductHighTemp IS NULL OR ProductLowTemp IS NULL))
		) > 0
	BEGIN
		RAISERROR('All Product Measurement values are required for Gauge Run & Net Barrel tickets', 16, 1)
		RETURN
	END
	
	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1) AND Rejected = 0 
		AND (OpeningGaugeFeet IS NULL OR OpeningGaugeInch IS NULL OR OpeningGaugeQ IS NULL)) > 0
	BEGIN
		RAISERROR('All Opening Gauge values are required for Gauge Run tickets', 16, 1)
		RETURN
	END
	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1) AND Rejected = 0 
		AND (ClosingGaugeFeet IS NULL OR ClosingGaugeInch IS NULL OR ClosingGaugeQ IS NULL)) > 0
	BEGIN
		RAISERROR('All Closing Gauge values are required for Gauge Run tickets', 16, 1)
		RETURN
	END

	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (2) AND Rejected = 0 
		AND (GrossBarrels IS NULL)) > 0
	BEGIN
		RAISERROR('Gross Barrel value is required for Net Barrel tickets', 16, 1)
		RETURN
	END

	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (3) AND Rejected = 0 
		AND (GrossBarrels IS NULL OR NetBarrels IS NULL)) > 0
	BEGIN
		RAISERROR('Gross & Net Barrel values are required for Meter Run tickets', 16, 1)
		RETURN
	END

	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1) AND Rejected = 0 
		AND (SealOff IS NULL OR SealOn IS NULL)) > 0
	BEGIN
		RAISERROR('All Seal Off & Seal On values are required for Gauge Run tickets', 16, 1)
		RETURN
	END

END

GO

COMMIT
SET NOEXEC OFF