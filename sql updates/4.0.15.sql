SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.0.14'
SELECT  @NewVersion = '4.0.15'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1702 Truck Mileage update'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO



/***********************************/
-- Date Created: 2016-05-10
-- Author: Joe Engler
-- Purpose: Handle null QRCode
-- Changes:
--		4.0.12		JAE		2016-09-01		Keep last odometer date in sync with last odometer
--		4.0.15		JAE		2016-09-06		Fix to put logic on the lastOdometerDate 
/***********************************/
ALTER TRIGGER trigTruck_IU ON tblTruck AFTER INSERT, UPDATE AS 
BEGIN
	-- ensure the QRCode value is always populated (really should be defined non-null instead)
    UPDATE tblTruck
	SET QRCode = NEWID()
	WHERE ID IN (SELECT i.ID FROM inserted i WHERE i.QRCode IS NULL)


	-- if the odometer was updated, make sure the lastOdometerDate is too
	IF NOT UPDATE(LastOdometerDateUTC) OR EXISTS (SELECT 1 FROM inserted WHERE LastOdometerDateUTC IS NULL)-- No LastOdometerDate or NULL was provided
	UPDATE tblTruck
	SET LastOdometerDateUTC = CASE WHEN i.LastOdometer <> d.LastOdometer OR d.LastOdometer IS NULL AND i.LastOdometer IS NOT NULL THEN GETUTCDATE() -- change, set to now
                                   WHEN i.LastOdometer IS NULL AND d.LastOdometer IS NOT NULL THEN NULL -- clear last odometer date
	                               ELSE d.LastOdometerDateUTC -- keep existing last odometer date
								END
	FROM tblTruck t
	JOIN inserted i on i.ID = t.ID
	JOIN deleted d on d.ID = t.ID
END 

GO



/**************************************************/
-- Created: 4.0.12 - 2016.09.01 - Joe Engler
-- Purpose: update the Truck.LastOdometer | LastOdometerDateUTC whenever an order change would advance these values
-- Changes:
--		4.0.15		JAE		2016/09/06		Added rejected orders to trigger
--											Remove UPDATE field clause since we want odometers to update any time
/**************************************************/
ALTER TRIGGER trigOrder_IU_TruckLastOdometer on tblOrder AFTER INSERT, UPDATE AS
BEGIN
	BEGIN
		/* Keep odometer up-to-date (4.0.12) */
		UPDATE tblTruck
			SET LastOdometer = O.Odometer
				, LastOdometerDateUTC = O.OdometerDateUTC
		FROM tblTruck T
		JOIN (
			SELECT TruckID, Odometer = max(Odometer), OdometerDateUTC = max(OdometerDateUTC)
			FROM (
				-- origin departures
				SELECT TruckID = ISNULL(OriginTruckID, TruckID), Odometer = OriginTruckMileage, OdometerDateUTC = OriginDepartTimeUTC
				FROM inserted i 
				LEFT JOIN tblOrderTransfer otr ON i.ID = otr.OrderID
				WHERE (i.StatusID = 8 AND i.PickupPrintStatusID IN (2,3)) -- Picked Up
				   OR (i.Rejected = 1) -- Rejecets
				
				UNION
				-- dest departures
				SELECT TruckID, DestTruckMileage, DestDepartTimeUTC
				FROM inserted i 
				WHERE i.StatusID = 3 AND i.DeliverPrintStatusID IN (2,3) AND Rejected = 0 -- Delivered
			) X
			GROUP BY TruckID
		) O ON O.TruckID = T.ID AND (O.Odometer >= T.LastOdometer OR T.LastOdometer IS NULL)
	END
END
GO


------------------------------------------------------------------------------------------------------------
-- THE REMAINING COMMANDS ARE UNCHANGED ENTRIES BUT x.x.x HAS BEEN REPLACED WITH APPROPRIATE VERSION
------------------------------------------------------------------------------------------------------------
GO


/***********************************/
-- Date Created: 26 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Truck records with FullName & translated "friendly" values
-- Changes: 
--		3.10.13		2016-02-29		JAE		Add Truck Type
--		4.0.12		2016-09-01		JAE		Move Garage State join here (removing odometer view)
/***********************************/
ALTER VIEW viewTruck AS
SELECT T.*
	, Active = cast(CASE WHEN isnull(C.DeleteDateUTC, T.DeleteDateUTC) IS NULL THEN 1 ELSE 0 END as bit) -- mark truck as active if the carrier and the truck are not deleted
	, FullName = T.IDNumber
	, TruckType = TT.Name
	, LoadOnTruck = TT.LoadOnTruck 
	, CarrierType = CT.Name
	, Carrier = C.Name
	, GarageStateAbbrev = S.Abbreviation  -- 3.11.12.2 (4.0.12)
FROM dbo.tblTruck T
LEFT JOIN dbo.tblTruckType TT ON TT.ID = T.TruckTypeID
JOIN dbo.tblCarrier C ON C.ID = T.CarrierID
LEFT JOIN dbo.tblCarrierType CT ON CT.ID = C.CarrierTypeID
LEFT JOIN tblState S ON S.ID = T.GarageStateID

GO



/************************************************
-- Author:		Kevin Alons
-- Create date: 2015/09/23
-- Description:	procedure to accomplish Order Transfer (driver) process
-- Changes:
--		JAE 11/4/2015 - "Touch" truck record to ensure the mobile app gets updated odometer reading. 
--                         Mobile app keys off last modify date for refreshing truck info
-- 		JAE 11/5/2015 - "Touch" order record to ensure mobile app gets the updated transfer order
-- 					  -  Mobile app keys off last modify date for refreshing order info
--		JAE	9/1/2016  - Update mileage for odometer
************************************************/
ALTER PROCEDURE spOrderTransfer
(
  @OrderID int
, @DestDriverID int
, @OriginTruckEndMileage int
, @PercentComplete int
, @Notes Text
, @UserName varchar(100)
, @DestTruckID int = NULL
) AS
BEGIN
	INSERT INTO tblOrderTransfer(OrderID, OriginDriverID, OriginTruckID, OriginTruckEndMileage, PercentComplete, Notes, CreateDateUTC, CreatedByUser, TransferComplete)
		SELECT ID, DriverID, TruckID, @OriginTruckEndMileage, @PercentComplete, @Notes, GETUTCDATE(), @UserName, 0
		FROM tblOrder 
		WHERE ID = @OrderID

	UPDATE tblOrder SET DriverID = @DestDriverID, TruckID = ISNULL(@DestTruckID, TruckID)
		, LastChangedByUser = @UserName, LastChangeDateUTC = GETUTCDATE() 
	WHERE ID = @OrderID
	
	/* 4.0.12 - update the LastOdometer value when a transfer occurs */
	UPDATE tblTruck SET LastOdometer = @OriginTruckEndMileage, LastOdometerDateUTC = GETUTCDATE()
	FROM tblTruck T
	JOIN tblOrderTransfer OT ON OT.OriginTruckID = T.ID
	WHERE OT.OrderID = @OrderID AND @OriginTruckEndMileage > T.LastOdometer
END

GO


sp_refreshView viewDriverBase
GO
sp_refreshView viewOrder
GO
sp_refreshView viewTruck
GO
sp_refreshView viewTruckInspection
GO
sp_refreshView viewTruckMaintenance
GO

EXEC _spRefreshAllViews
GO

EXEC _spRebuildAllObjects
GO


COMMIT
SET NOEXEC OFF