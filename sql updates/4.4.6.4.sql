SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.4.6.3'
SELECT  @NewVersion = '4.4.6.4'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'DCWEB-1994 Add system setting for formatting time durations'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

INSERT INTO tblSetting 
VALUES 
(69, 'Time Duration Format', 1, 'h:mm', 'System Wide', GETUTCDATE(), 'System', null, null, 0, 'Format how to display time intervals (Default is h:mm)')

GO

COMMIT
SET NOEXEC OFF
