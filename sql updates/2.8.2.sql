/*
	-- remove unused System_LocalPort settings record
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.8.1'
SELECT  @NewVersion = '2.8.2'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

BEGIN TRY
	ALTER TABLE dbo.tblOrigin
		DROP CONSTRAINT FK_Origin_TimeZone;
		
	ALTER TABLE dbo.tblTimeZone SET (LOCK_ESCALATION = TABLE)

	ALTER TABLE dbo.tblOrigin
		DROP CONSTRAINT FK_tblOrigin_tblProducer

	ALTER TABLE dbo.tblProducer SET (LOCK_ESCALATION = TABLE)

	ALTER TABLE dbo.tblOrigin
		DROP CONSTRAINT FK_tblOrigin_tblOriginTypes

	ALTER TABLE dbo.tblOriginType SET (LOCK_ESCALATION = TABLE)

	ALTER TABLE dbo.tblOrigin
		DROP CONSTRAINT FK_tblOrigin_tblTicketType

	ALTER TABLE dbo.tblTicketType SET (LOCK_ESCALATION = TABLE)

	ALTER TABLE dbo.tblOrigin
		DROP CONSTRAINT FK_tblOrigin_tblStates

	ALTER TABLE dbo.tblState SET (LOCK_ESCALATION = TABLE)

	ALTER TABLE dbo.tblOrigin
		DROP CONSTRAINT FK_tblOrigin_tblRegion

	ALTER TABLE dbo.tblRegion SET (LOCK_ESCALATION = TABLE)

	ALTER TABLE dbo.tblOrigin
		DROP CONSTRAINT FK_tblOrigin_tblOperator

	ALTER TABLE dbo.tblOperator SET (LOCK_ESCALATION = TABLE)

	ALTER TABLE dbo.tblOrigin
		DROP CONSTRAINT FK_tblOrigin_tblPumper

	ALTER TABLE dbo.tblPumper SET (LOCK_ESCALATION = TABLE)

	ALTER TABLE dbo.tblOrigin
		DROP CONSTRAINT FK_Origin_Uom

	ALTER TABLE dbo.tblUom SET (LOCK_ESCALATION = TABLE)

	ALTER TABLE dbo.tblOrigin
		DROP CONSTRAINT DF_Origin_CreateDateUTC

	ALTER TABLE dbo.tblOrigin
		DROP CONSTRAINT DF_tblOrigin_H2S

	ALTER TABLE dbo.tblOrigin
		DROP CONSTRAINT DF_Origin_TimeZone

	ALTER TABLE dbo.tblOrigin
		DROP CONSTRAINT DF_Origin_UseDST

	ALTER TABLE dbo.tblOrigin
		DROP CONSTRAINT DF_Origin_UomID

	CREATE TABLE dbo.Tmp_tblOrigin
		(
		ID int NOT NULL IDENTITY (1, 1),
		OriginTypeID int NOT NULL,
		Name varchar(50) NOT NULL,
		Address varchar(50) NULL,
		City varchar(30) NULL,
		StateID int NULL,
		Zip varchar(50) NULL,
		wellAPI varchar(20) NULL,
		LAT varchar(20) NULL,
		LON varchar(20) NULL,
		OperatorID int NULL,
		PumperID int NULL,
		County varchar(25) NULL,
		LeaseName varchar(35) NULL,
		LeaseNum varchar(30) NULL,
		TicketTypeID int NOT NULL,
		RegionID int NULL,
		TotalDepth int NULL,
		SpudDate smalldatetime NULL,
		FieldName varchar(25) NULL,
		NDICFileNum varchar(10) NULL,
		CTBNum nchar(10) NULL,
		CustomerID int NULL,
		ProducerID int NULL,
		CreateDateUTC smalldatetime NULL,
		CreatedByUser varchar(100) NULL,
		LastChangeDateUTC smalldatetime NULL,
		LastChangedByUser varchar(100) NULL,
		TaxRate smallmoney NULL,
		H2S bit NOT NULL,
		Station varchar(15) NULL,
		TimeZoneID tinyint NOT NULL,
		UseDST bit NOT NULL,
		UomID int NOT NULL,
		LegalDescription varchar(50) NULL,
		NDM varchar(25) NULL,
		CA varchar(25) NULL,
		DeleteDateUTC smalldatetime NULL,
		DeletedByUser varchar(100) NULL
		)  ON [PRIMARY]

	ALTER TABLE dbo.Tmp_tblOrigin SET (LOCK_ESCALATION = TABLE)

	ALTER TABLE dbo.Tmp_tblOrigin ADD CONSTRAINT
		DF_Origin_CreateDateUTC DEFAULT (getutcdate()) FOR CreateDateUTC

	ALTER TABLE dbo.Tmp_tblOrigin ADD CONSTRAINT
		DF_tblOrigin_H2S DEFAULT ((0)) FOR H2S

	ALTER TABLE dbo.Tmp_tblOrigin ADD CONSTRAINT
		DF_Origin_TimeZone DEFAULT ((1)) FOR TimeZoneID

	ALTER TABLE dbo.Tmp_tblOrigin ADD CONSTRAINT
		DF_Origin_UseDST DEFAULT ((1)) FOR UseDST

	ALTER TABLE dbo.Tmp_tblOrigin ADD CONSTRAINT
		DF_Origin_UomID DEFAULT ((1)) FOR UomID

	SET IDENTITY_INSERT dbo.Tmp_tblOrigin ON

	IF EXISTS(SELECT * FROM dbo.tblOrigin)
		 EXEC('INSERT INTO dbo.Tmp_tblOrigin (ID, OriginTypeID, Name, Address, City, StateID, Zip, wellAPI, LAT, LON, OperatorID, PumperID, County, LeaseName, LeaseNum, TicketTypeID, RegionID, TotalDepth, SpudDate, FieldName, NDICFileNum, CTBNum, CustomerID, ProducerID, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, TaxRate, H2S, Station, TimeZoneID, UseDST, UomID, LegalDescription, NDM, CA, DeleteDateUTC, DeletedByUser)
			SELECT ID, OriginTypeID, Name, Address, City, StateID, Zip, wellAPI, LAT, LON, OperatorID, PumperID, County, LeaseName, LeaseNum, TicketTypeID, RegionID, TotalDepth, SpudDate, FieldName, NDICFileNum, CTBNum, CustomerID, ProducerID, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, TaxRate, H2S, Station, TimeZoneID, UseDST, UomID, LegalDescription, NDM, CA, DeleteDateUTC, DeletedByUser FROM dbo.tblOrigin WITH (HOLDLOCK TABLOCKX)')

	SET IDENTITY_INSERT dbo.Tmp_tblOrigin OFF

	ALTER TABLE dbo.tblOriginProducts
		DROP CONSTRAINT FK_OriginProducts_Origin

	ALTER TABLE dbo.tblRoute
		DROP CONSTRAINT FK_tblRoute_tblOrigin

	ALTER TABLE dbo.tblOrderSignature
		DROP CONSTRAINT FK_OrderSignature_Origin

	ALTER TABLE dbo.tblOriginTank
		DROP CONSTRAINT FK_OriginTank_Origin

	ALTER TABLE dbo.tblOrigin
		DROP CONSTRAINT FK_tblOrigin_tblOrigin

	ALTER TABLE dbo.tblOrder
		DROP CONSTRAINT FK_Order_Origin

	DROP TABLE dbo.tblOrigin

	EXECUTE sp_rename N'dbo.Tmp_tblOrigin', N'tblOrigin', 'OBJECT' 

	ALTER TABLE dbo.tblOrigin ADD CONSTRAINT
		tblOrigin_PrimaryKey PRIMARY KEY CLUSTERED 
		(
		ID
		) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];


	CREATE NONCLUSTERED INDEX idxOrigin_County ON dbo.tblOrigin
		(
		County
		) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

	ALTER TABLE dbo.tblOrigin ADD CONSTRAINT
		uqOrigin_Name UNIQUE NONCLUSTERED 
		(
		Name
		) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]


	DECLARE @v sql_variant 
	SET @v = N'Ensure that Origin.Name is unique'
	EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'tblOrigin', N'CONSTRAINT', N'uqOrigin_Name'

	CREATE NONCLUSTERED INDEX idxOrigin_Region ON dbo.tblOrigin
		(
		RegionID
		) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

	CREATE NONCLUSTERED INDEX idxOrigin_State ON dbo.tblOrigin
		(
		StateID
		) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

	CREATE NONCLUSTERED INDEX idxOrigin_Customer ON dbo.tblOrigin
		(
		CustomerID
		) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

	CREATE NONCLUSTERED INDEX idxOrigin_OriginType ON dbo.tblOrigin
		(
		OriginTypeID
		) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

	ALTER TABLE dbo.tblOrigin ADD CONSTRAINT
		FK_Origin_Uom FOREIGN KEY
		(
		UomID
		) REFERENCES dbo.tblUom
		(
		ID
		) ON UPDATE  NO ACTION 
		 ON DELETE  NO ACTION 
		
	ALTER TABLE dbo.tblOrigin ADD CONSTRAINT
		FK_tblOrigin_tblOrigin FOREIGN KEY
		(
		ID
		) REFERENCES dbo.tblOrigin
		(
		ID
		) ON UPDATE  NO ACTION 
		 ON DELETE  NO ACTION 
		
	ALTER TABLE dbo.tblOrigin ADD CONSTRAINT
		FK_tblOrigin_tblPumper FOREIGN KEY
		(
		PumperID
		) REFERENCES dbo.tblPumper
		(
		ID
		) ON UPDATE  NO ACTION 
		 ON DELETE  NO ACTION 
		
	ALTER TABLE dbo.tblOrigin ADD CONSTRAINT
		FK_tblOrigin_tblOperator FOREIGN KEY
		(
		OperatorID
		) REFERENCES dbo.tblOperator
		(
		ID
		) ON UPDATE  NO ACTION 
		 ON DELETE  NO ACTION 
		
	ALTER TABLE dbo.tblOrigin ADD CONSTRAINT
		FK_tblOrigin_tblRegion FOREIGN KEY
		(
		RegionID
		) REFERENCES dbo.tblRegion
		(
		ID
		) ON UPDATE  NO ACTION 
		 ON DELETE  NO ACTION 
		
	ALTER TABLE dbo.tblOrigin ADD CONSTRAINT
		FK_tblOrigin_tblStates FOREIGN KEY
		(
		StateID
		) REFERENCES dbo.tblState
		(
		ID
		) ON UPDATE  NO ACTION 
		 ON DELETE  NO ACTION 
		
	ALTER TABLE dbo.tblOrigin ADD CONSTRAINT
		FK_tblOrigin_tblTicketType FOREIGN KEY
		(
		TicketTypeID
		) REFERENCES dbo.tblTicketType
		(
		ID
		) ON UPDATE  NO ACTION 
		 ON DELETE  NO ACTION 
		
	ALTER TABLE dbo.tblOrigin ADD CONSTRAINT
		FK_tblOrigin_tblOriginTypes FOREIGN KEY
		(
		OriginTypeID
		) REFERENCES dbo.tblOriginType
		(
		ID
		) ON UPDATE  NO ACTION 
		 ON DELETE  NO ACTION 
		
	ALTER TABLE dbo.tblOrigin ADD CONSTRAINT
		FK_tblOrigin_tblProducer FOREIGN KEY
		(
		ProducerID
		) REFERENCES dbo.tblProducer
		(
		ID
		) ON UPDATE  NO ACTION 
		 ON DELETE  NO ACTION; 
		
	ALTER TABLE dbo.tblOrigin ADD CONSTRAINT
		FK_Origin_TimeZone FOREIGN KEY
		(
		TimeZoneID
		) REFERENCES dbo.tblTimeZone
		(
		ID
		) ON UPDATE  NO ACTION 
		 ON DELETE  NO ACTION;

	ALTER TABLE dbo.tblOrder ADD CONSTRAINT
		FK_Order_Origin FOREIGN KEY
		(
		OriginID
		) REFERENCES dbo.tblOrigin
		(
		ID
		) ON UPDATE  NO ACTION 
		 ON DELETE  NO ACTION 
		
	ALTER TABLE dbo.tblOrder SET (LOCK_ESCALATION = TABLE)

	ALTER TABLE dbo.tblOriginTank ADD CONSTRAINT
		FK_OriginTank_Origin FOREIGN KEY
		(
		OriginID
		) REFERENCES dbo.tblOrigin
		(
		ID
		) ON UPDATE  NO ACTION 
		 ON DELETE  CASCADE 
		
	ALTER TABLE dbo.tblOriginTank SET (LOCK_ESCALATION = TABLE)

	ALTER TABLE dbo.tblOrderSignature ADD CONSTRAINT
		FK_OrderSignature_Origin FOREIGN KEY
		(
		OriginID
		) REFERENCES dbo.tblOrigin
		(
		ID
		) ON UPDATE  NO ACTION 
		 ON DELETE  NO ACTION 

	ALTER TABLE dbo.tblOrderSignature SET (LOCK_ESCALATION = TABLE)

	ALTER TABLE dbo.tblRoute ADD CONSTRAINT
		FK_tblRoute_tblOrigin FOREIGN KEY
		(
		OriginID
		) REFERENCES dbo.tblOrigin
		(
		ID
		) ON UPDATE  NO ACTION 
		 ON DELETE  NO ACTION 
		
	ALTER TABLE dbo.tblRoute SET (LOCK_ESCALATION = TABLE)

	ALTER TABLE dbo.tblOriginProducts ADD CONSTRAINT
		FK_OriginProducts_Origin FOREIGN KEY
		(
		OriginID
		) REFERENCES dbo.tblOrigin
		(
		ID
		) ON UPDATE  NO ACTION 
		 ON DELETE  CASCADE 
		
	ALTER TABLE dbo.tblOriginProducts SET (LOCK_ESCALATION = TABLE)
	
END TRY
BEGIN CATCH
ROLLBACK
END CATCH
GO
	
-- =============================================
-- Author:		Kevin Alons
-- Create date: 19 Dec 2012
-- Description:	trigger to ensure the UomID for Carrier/Customer Route Rates matches that assigned to the Origin
-- =============================================
CREATE TRIGGER [dbo].[trigOrigin_IU] ON dbo.tblOrigin AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	IF (UPDATE(UomID))
	BEGIN
		-- update matching CarrierRouteRates.UomID to match what is assigned to the new Origin
		UPDATE tblCarrierRouteRates 
		  SET UomID = i.UomID, LastChangeDateUTC = i.LastChangeDateUTC, LastChangedByUser = i.LastChangedByUser
		FROM tblCarrierRouteRates CRR
		JOIN tblRoute R ON R.ID = CRR.RouteID
		JOIN inserted i ON i.ID = R.OriginID
		JOIN deleted d ON d.ID = i.ID
		WHERE d.UomID <> i.UomID AND CRR.EffectiveDate <= dbo.fnDateOnly(GETDATE())

		-- update matching CustomerRouteRates.UomID to match what is assigned to the new Origin
		UPDATE tblCustomerRouteRates 
		  SET UomID = i.UomID, LastChangeDateUTC = i.LastChangeDateUTC, LastChangedByUser = i.LastChangedByUser
		FROM tblCustomerRouteRates CRR
		JOIN tblRoute R ON R.ID = CRR.RouteID
		JOIN inserted i ON i.ID = R.OriginID
		JOIN deleted d ON d.ID = i.ID
		WHERE d.UomID <> i.UomID AND CRR.EffectiveDate <= dbo.fnDateOnly(GETDATE())
	END
END
GO

COMMIT
SET NOEXEC OFF
