SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.4.6.5'
SELECT  @NewVersion = '4.4.7'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1876 - Convert origin tanks page to MVC (Also added new origin tank definitions page)'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/* Remove depreciated system setting related to origin tanks */
DELETE FROM tblSetting WHERE ID = 25
GO

/* Stick another setting that was marked for removal into the "to be removed" category & fix "DEPRECATED" */
UPDATE tblSetting SET Category = 'Legacy - To Be Removed', Name = '**Depreciated** - Limit to single active order' WHERE ID = 43
GO

/* Update existing origin types permissions to fit with new permissions being added below */
UPDATE aspnet_Roles SET Category = 'Assets - Locations - Origins', Description = 'Allow user to view the Origin Types maintenance page' WHERE RoleName = 'viewOriginTypes'
GO
UPDATE aspnet_Roles SET Category = 'Assets - Locations - Origins' WHERE RoleName = 'createOriginTypes'
GO
UPDATE aspnet_Roles SET Category = 'Assets - Locations - Origins' WHERE RoleName = 'editOriginTypes'
GO
UPDATE aspnet_Roles SET Category = 'Assets - Locations - Origins' WHERE RoleName = 'deactivateOriginTypes'
GO

/* Update existing origin page view permission to fit with new permissions being added below */
UPDATE aspnet_Roles
SET FriendlyName = 'Origins - View'
, Category = 'Assets - Locations - Origins'
, Description = 'Allow user to view the Origins maintenance page'
WHERE RoleName = 'viewOriginMaintenance'
GO

/* Update existing origin tank page view permission to fit with new permissions being added below */
UPDATE aspnet_Roles
SET FriendlyName = 'Tanks - View'
, RoleName = 'viewOriginTanks'
, LoweredRoleName = 'vieworigintanks'
, Category = 'Assets - Locations - Origins'
, Description = 'Allow user to view the Origin Tanks maintenance page'
WHERE RoleName = 'viewOriginTankMaintenance'
GO

/* Add new permissions for the Origin Tanks page */
EXEC spAddNewPermission 'createOriginTanks', 'Allow user to create Origin Tanks', 'Assets - Locations - Origins', 'Tanks - Create'
GO
EXEC spAddNewPermission 'editOriginTanks', 'Allow user to edit Origin Tanks', 'Assets - Locations - Origins', 'Tanks - Edit'
GO
EXEC spAddNewPermission 'deactivateOriginTanks', 'Allow user to deactivate Origin Tanks', 'Assets - Locations - Origins', 'Tanks - Deactivate'
GO

/* Add new permissions for the Origin Tank Definitions page */
EXEC spAddNewPermission 'viewOriginTankDefinitions', 'Allow user to view the Origin Tank Definitions maintenance page', 'Assets - Locations - Origins', 'Tank Definitions - View'
GO
EXEC spAddNewPermission 'createOriginTankDefinitions', 'Allow user to create Origin Tank Definitions', 'Assets - Locations - Origins', 'Tank Definitions - Create'
GO
EXEC spAddNewPermission 'editOriginTankDefinitions', 'Allow user to edit Origin Tank Definitions', 'Assets - Locations - Origins', 'Tank Definitions - Edit'
GO
EXEC spAddNewPermission 'deactivateOriginTankDefinitions', 'Allow user to deactivate Origin Tank Definitions', 'Assets - Locations - Origins', 'Tank Definitions - Deactivate'
GO


/* This fixes a problem we discovered where many databases have deactivated origins with tanks that were never deactivated */
UPDATE OT
SET OT.DeleteDateUTC = O.DeleteDateUTC
FROM tblOriginTank OT
	JOIN tblOrigin O ON O.ID = OT.OriginID
WHERE O.DeleteDateUTC IS NOT NULL
	AND OT.DeleteDateUTC IS NULL
GO


/*********************************************
Create date:	12/21/2016
Author:			Ben Bloodworth
Description:	trigger to cascade deactivate/reactivate origin tanks when origins are deactivated/reactivated
				(Basically this was just copied from [trigCarrier_U_VirtualDelete] to get the same functionality)
Changes:  
*********************************************/
CREATE TRIGGER trigOrigin_U_VirtualDelete ON tblOrigin AFTER UPDATE AS
BEGIN
	SET NOCOUNT ON;
	IF (UPDATE(DeleteDateUTC))
	BEGIN
		IF EXISTS(SELECT 1 FROM inserted i WHERE i.DeleteDateUTC IS NOT NULL)
		-- origin is being deactivated
		BEGIN
			-- Deactivate active tanks associated to the origin
			UPDATE tblOriginTank 
			SET DeleteDateUTC = i.DeleteDateUTC
				, DeletedByUser = i.DeletedByUser
			FROM tblOriginTank OT
				JOIN inserted i ON i.ID = OT.OriginID
			WHERE OT.DeleteDateUTC IS NULL
		END

		IF EXISTS(SELECT 1 FROM inserted i WHERE i.DeleteDateUTC IS NULL)
		-- origin is being reactivated
		BEGIN
			-- Reactivate previously active tanks associated to the origin
			UPDATE tblOriginTank 
			SET LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
				,DeleteDateUTC = NULL
				, DeletedByUser = NULL
			FROM tblOriginTank OT
				JOIN inserted i ON i.ID = OT.OriginID
				JOIN deleted x ON x.ID = OT.OriginID
			WHERE OT.DeleteDateUTC = x.DeleteDateUTC 
				AND OT.DeletedByUser = x.DeletedByUser
		END
	END
END
GO


-- Create Origin Tank Definition table
CREATE TABLE tblOriginTankDefinition
(  
	ID INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_OriginTankDefinition PRIMARY KEY CLUSTERED
	, Name VARCHAR(20) NOT NULL UNIQUE
	, BottomFeet TINYINT NULL DEFAULT 0
	, BottomInches TINYINT NULL DEFAULT 0
	, BottomQ TINYINT NULL DEFAULT 0
	, TopFeet TINYINT NOT NULL
	, TopInches TINYINT NOT NULL
	, TopQ TINYINT NOT NULL
	, TopBPQ DECIMAL(18, 10) NOT NULL
	, CreateDateUTC SMALLDATETIME NULL CONSTRAINT DF_OriginTankDefinition_CreateDateUTC  DEFAULT (GETUTCDATE())
	, CreatedByUser VARCHAR(100) NULL
	, LastChangeDateUTC SMALLDATETIME NULL
	, LastChangedByUser VARCHAR(100) NULL
	, DeleteDateUTC SMALLDATETIME NULL
	, DeletedByUser VARCHAR(100) NULL
) 
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblOriginTankDefinition TO role_iis_acct 
GO


-- Add foreign key column to tblOriginTank for new Definition field
ALTER TABLE tblOriginTank
ADD TankDefinitionID INT
GO

ALTER TABLE tblOriginTank  WITH CHECK ADD  CONSTRAINT FK_OriginTank_OriginTankDefinition FOREIGN KEY(TankDefinitionID)
REFERENCES tblOriginTankDefinition (ID)
ON DELETE CASCADE
GO

ALTER TABLE tblOriginTank CHECK CONSTRAINT FK_OriginTank_OriginTankDefinition
GO


-- Add default tank definitions
INSERT INTO tblOriginTankDefinition (Name, BottomFeet, BottomInches, BottomQ, TopFeet, TopInches, TopQ, TopBPQ, CreatedByUser)
SELECT '10FT|100BBLS',0,0,0,10,0,0,0.21,'System'
UNION
SELECT '10FT|200BBLS',0,0,0,10,0,0,0.4175,'System'
UNION 
SELECT '12FT|115BBLS',0,0,0,12,0,0,0.1875,'System'
UNION
SELECT '12FT|200BBLS',0,0,0,12,0,0,0.347225,'System'
UNION 
SELECT '12FT|400BBLS',0,0,0,12,0,0,0.6925,'System'
UNION 
SELECT '15FT|210BBLS',0,0,0,15,0,0,0.2925,'System'
UNION 
SELECT '15FT|300BBLS',0,0,0,15,0,0,0.4175,'System'
UNION 
SELECT '16FT|500BBLS',0,0,0,16,0,0,0.695,'System'
UNION 
SELECT '16FT|1000BBLS',0,0,0,16,0,0,1.302075,'System'
UNION 
SELECT '20FT|400BBLS',0,0,0,20,0,0,0.4175,'System'
UNION 
SELECT '20FT|500BBLS',0,0,0,20,0,0,0.520833,'System'
UNION 
SELECT '24FT|750BBLS',0,0,0,24,0,0,0.7,'System'
UNION 
SELECT '24FT|800BBLS',0,0,0,24,0,0,0.695,'System'
UNION 
SELECT '25FT|500BBLS',0,0,0,25,0,0,0.4175,'System'
UNION 
SELECT '25FT|550BBLS',0,0,0,25,0,0,0.4175,'System'
UNION 
SELECT '25FT|800BBLS',0,0,0,25,0,0,0.6998,'System'
UNION 
SELECT '30FT|1000BBLS',0,0,0,30,0,0,0.695,'System'
GO


COMMIT
SET NOEXEC OFF