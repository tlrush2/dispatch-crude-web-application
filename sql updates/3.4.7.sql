-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.4.6'
SELECT  @NewVersion = '3.4.7'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'increase tblUserReportColumnDefinition.DataFormat from varchar(50) to varchar(4000)'
GO

ALTER TABLE dbo.tblUserReportColumnDefinition
	DROP CONSTRAINT FK_UserReportColumnDefinition_UserReport
GO
ALTER TABLE dbo.tblUserReportDefinition SET (LOCK_ESCALATION = TABLE)
GO

ALTER TABLE dbo.tblUserReportColumnDefinition
	DROP CONSTRAINT FK_UserReportColumnDefinition_FilterOperator
GO
ALTER TABLE dbo.tblReportFilterOperator SET (LOCK_ESCALATION = TABLE)
GO

ALTER TABLE dbo.tblUserReportColumnDefinition
	DROP CONSTRAINT FK_UserReportColumnDefinition_ReportColumn
GO
ALTER TABLE dbo.tblReportColumnDefinition SET (LOCK_ESCALATION = TABLE)
GO

ALTER TABLE dbo.tblUserReportColumnDefinition
	DROP CONSTRAINT DF_UserReportColumnDefinition_FilterOperator
GO
ALTER TABLE dbo.tblUserReportColumnDefinition
	DROP CONSTRAINT DF_UserReportColumnDefinition_CreateDateUTC
GO
ALTER TABLE dbo.tblUserReportColumnDefinition
	DROP CONSTRAINT DF_UserReportColumnDefinition_CreatedByUser
GO
ALTER TABLE dbo.tblUserReportColumnDefinition
	DROP CONSTRAINT DF_UserReportColumnDefinition_Export
GO
CREATE TABLE dbo.Tmp_tblUserReportColumnDefinition
	(
	ID int NOT NULL IDENTITY (1, 1),
	UserReportID int NOT NULL,
	ReportColumnID int NOT NULL,
	Caption varchar(50) NULL,
	SortNum int NULL,
	DataFormat varchar(4000) NULL,
	FilterOperatorID int NOT NULL,
	FilterValue1 varchar(255) NULL,
	FilterValue2 varchar(255) NULL,
	CreateDateUTC datetime NOT NULL,
	CreatedByUser varchar(100) NOT NULL,
	LastChangeDateUTC datetime NULL,
	LastChangedByUser varchar(100) NULL,
	Export bit NOT NULL,
	DataSort smallint NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_tblUserReportColumnDefinition SET (LOCK_ESCALATION = TABLE)
GO
GRANT DELETE ON dbo.Tmp_tblUserReportColumnDefinition TO dispatchcrude_iis_acct  AS dbo
GO
GRANT INSERT ON dbo.Tmp_tblUserReportColumnDefinition TO dispatchcrude_iis_acct  AS dbo
GO
GRANT SELECT ON dbo.Tmp_tblUserReportColumnDefinition TO dispatchcrude_iis_acct  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_tblUserReportColumnDefinition TO dispatchcrude_iis_acct  AS dbo
GO
ALTER TABLE dbo.Tmp_tblUserReportColumnDefinition ADD CONSTRAINT
	DF_UserReportColumnDefinition_FilterOperator DEFAULT ((1)) FOR FilterOperatorID
GO
ALTER TABLE dbo.Tmp_tblUserReportColumnDefinition ADD CONSTRAINT
	DF_UserReportColumnDefinition_CreateDateUTC DEFAULT (getutcdate()) FOR CreateDateUTC
GO
ALTER TABLE dbo.Tmp_tblUserReportColumnDefinition ADD CONSTRAINT
	DF_UserReportColumnDefinition_CreatedByUser DEFAULT (suser_name()) FOR CreatedByUser
GO
ALTER TABLE dbo.Tmp_tblUserReportColumnDefinition ADD CONSTRAINT
	DF_UserReportColumnDefinition_Export DEFAULT ((1)) FOR Export
GO
SET IDENTITY_INSERT dbo.Tmp_tblUserReportColumnDefinition ON
GO
IF EXISTS(SELECT * FROM dbo.tblUserReportColumnDefinition)
	 EXEC('INSERT INTO dbo.Tmp_tblUserReportColumnDefinition (ID, UserReportID, ReportColumnID, Caption, SortNum, DataFormat, FilterOperatorID, FilterValue1, FilterValue2, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, Export, DataSort)
		SELECT ID, UserReportID, ReportColumnID, Caption, SortNum, DataFormat, FilterOperatorID, FilterValue1, FilterValue2, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, Export, DataSort FROM dbo.tblUserReportColumnDefinition WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_tblUserReportColumnDefinition OFF
GO
DROP TABLE dbo.tblUserReportColumnDefinition
GO
EXECUTE sp_rename N'dbo.Tmp_tblUserReportColumnDefinition', N'tblUserReportColumnDefinition', 'OBJECT' 
GO
ALTER TABLE dbo.tblUserReportColumnDefinition ADD CONSTRAINT
	PK_UserReportColumnDefinition PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.tblUserReportColumnDefinition ADD CONSTRAINT
	FK_UserReportColumnDefinition_ReportColumn FOREIGN KEY
	(
	ReportColumnID
	) REFERENCES dbo.tblReportColumnDefinition
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblUserReportColumnDefinition ADD CONSTRAINT
	FK_UserReportColumnDefinition_FilterOperator FOREIGN KEY
	(
	FilterOperatorID
	) REFERENCES dbo.tblReportFilterOperator
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblUserReportColumnDefinition ADD CONSTRAINT
	FK_UserReportColumnDefinition_UserReport FOREIGN KEY
	(
	UserReportID
	) REFERENCES dbo.tblUserReportDefinition
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
-- =============================================
-- Author:		Kevin Alons
-- Create date: 23 Jul 2014
-- Description:	trigger to advance any duplicated SortNums in a UserReport to make reordering easier
-- =============================================
CREATE TRIGGER [dbo].[trigUserReportColumnDefinition_IU_SortNum] ON dbo.tblUserReportColumnDefinition FOR INSERT, UPDATE AS
BEGIN
	IF (trigger_nestlevel() < 2)  -- logic below will recurse unless this is used to prevent it
	BEGIN
		IF (UPDATE(SortNum))
		BEGIN
			SELECT i.UserReportID, i.SortNum, ID = MAX(i2.ID)
			INTO #new
			FROM (
				SELECT UserReportID, SortNum = min(SortNum)
				FROM inserted
				GROUP BY UserReportID
			) i
			JOIN inserted i2 ON i2.UserReportID = i.UserReportID AND i2.SortNum = i.SortNum
			GROUP BY i.UserReportID, i.SortNum
			
			DECLARE @UserReportID int, @SortNum int, @ID int
			SELECT TOP 1 @UserReportID = UserReportID, @SortNum = SortNum, @ID = ID FROM #new 

			WHILE (@UserReportID IS NOT NULL)
			BEGIN
				UPDATE tblUserReportColumnDefinition 
					SET SortNum = N.NewSortNum
				FROM tblUserReportColumnDefinition URCD
				JOIN (
					SELECT ID, UserReportID, NewSortNum = @SortNum + ROW_NUMBER() OVER (ORDER BY SortNum) 
					FROM tblUserReportColumnDefinition
					WHERE UserReportID = @UserReportID AND SortNum >= @SortNum AND ID <> @ID
				) N ON N.ID = URCD.ID
				
				DELETE FROM #new WHERE ID = @ID
				SET @UserReportID = NULL
				SELECT TOP 1 @UserReportID = UserReportID, @SortNum = SortNum, @ID = ID FROM #new
			END	
		END
	END
END
GO

COMMIT
SET NOEXEC OFF