/*  
	-- update the new fnRetrieveElegibleDestinations function to only optionally require an existing Route
*/ 
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.7.4'
SELECT  @NewVersion = '2.7.4.1'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 13 May 2013
-- Description:	retrieve all currently elible Destinations for the specified OriginID/ProductID values
-- =============================================
ALTER FUNCTION [dbo].[fnRetrieveEligibleDestinations](@originID int, @productID int, @requireRoute bit = 0) RETURNS TABLE AS RETURN
	SELECT D.*
	FROM dbo.viewDestination D
	WHERE DeleteDateUTC IS NULL 
		AND ((@requireRoute = 0 OR ID IN (SELECT DestinationID FROM viewRoute WHERE OriginID = @OriginID)) 
			AND ID IN (SELECT DestinationID FROM tblDestinationProducts WHERE ProductID=@ProductID) 
			AND (@OriginID = 0 OR ID IN (SELECT DestinationID FROM viewCustomerOriginDestination DC WHERE OriginID=@OriginID)))

GO

COMMIT
SET NOEXEC OFF