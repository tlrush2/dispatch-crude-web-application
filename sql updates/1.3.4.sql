BEGIN TRAN

UPDATE tblSetting SET Value = '1.3.4' WHERE ID=0
GO

CREATE TABLE [dbo].[tblSettlementFactor](
	[ID] [int] NOT NULL,
	[Name] [varchar](25) NOT NULL,
 CONSTRAINT [PK_tblSettlementFactor] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE tblCarrier ADD MinSettlementBarrels int null
GO
ALTER TABLE tblCarrier ADD SettlementFactorID int null
GO
ALTER TABLE dbo.tblCarrier ADD CONSTRAINT
	FK_Carrier_SettlementFactor FOREIGN KEY
	(
	SettlementFactorID
	) REFERENCES dbo.tblSettlementFactor
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblCarrier SET (LOCK_ESCALATION = TABLE)
GO

ALTER TABLE tblOrderInvoiceCarrier ADD SettlementFactorID int null
GO
ALTER TABLE dbo.tblOrderInvoiceCarrier ADD CONSTRAINT
	FK_OrderInvoiceCarrier_SettlementFactor FOREIGN KEY
	(
	SettlementFactorID
	) REFERENCES dbo.tblSettlementFactor
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderInvoiceCarrier SET (LOCK_ESCALATION = TABLE)
GO

ALTER TABLE dbo.tblOrderInvoiceCarrier ADD RouteRate decimal(9,3) NULL
GO

ALTER TABLE tblCustomer ADD MinSettlementBarrels int null
GO
ALTER TABLE tblCustomer ADD SettlementFactorID int null
GO
ALTER TABLE dbo.tblCustomer ADD CONSTRAINT
	FK_Customer_SettlementFactor FOREIGN KEY
	(
	SettlementFactorID
	) REFERENCES dbo.tblSettlementFactor
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblCustomer SET (LOCK_ESCALATION = TABLE)
GO

ALTER TABLE tblOrderInvoiceCustomer ADD SettlementFactorID int null
GO
ALTER TABLE dbo.tblOrderInvoiceCustomer ADD CONSTRAINT
	FK_OrderInvoiceCustomer_SettlementFactor FOREIGN KEY
	(
	SettlementFactorID
	) REFERENCES dbo.tblSettlementFactor
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderInvoiceCustomer SET (LOCK_ESCALATION = TABLE)
GO

ALTER TABLE dbo.tblOrderInvoiceCustomer ADD RouteRate decimal(9,3) NULL
GO

CREATE FUNCTION [dbo].[fnMaxDecimal](@first decimal, @second decimal) RETURNS decimal AS BEGIN
	SELECT @first = isnull(@first, 0), @second = isnull(@second, 0)
	RETURN (CASE WHEN @first > @second THEN @first ELSE @second END)
END

GO
GRANT EXECUTE ON fnMaxDecimal TO dispatchcrude_iis_acct
GO

INSERT INTO tblSettlementFactor (ID, Name) VALUES (1, 'Gross')
INSERT INTO tblSettlementFactor (ID, Name) VALUES (2, 'Net')
GO

UPDATE tblCustomer SET SettlementFactorID = 1, MinSettlementBarrels = 180
UPDATE tblCarrier SET SettlementFactorID = 1, MinSettlementBarrels = 180
GO

/***********************************/
-- Date Created: 4 Apr 2013
-- Author: Kevin Alons
-- Purpose: return Carrier records with translated "friendly" values
/***********************************/
ALTER VIEW [dbo].[viewCarrier] AS
SELECT C.*, CT.Name AS CarrierType, S.Abbreviation AS StateAbbrev, SF.Name AS SettlementFactor 
FROM tblCarrier C 
JOIN tblCarrierType CT ON CT.ID = C.CarrierTypeID
LEFT JOIN tblState S ON S.ID = C.StateID
LEFT JOIN tblSettlementFactor SF ON SF.ID = C.SettlementFactorID

GO

/***********************************/
-- Date Created: 27 Jan 2013
-- Author: Kevin Alons
-- Purpose: return Customer records with "translated friendly" values for FK relationships
/***********************************/
ALTER VIEW [dbo].[viewCustomer] AS
SELECT C.*, S.Abbreviation AS StateAbbrev, S.FullName AS State, SF.Name AS SettlementFactor
FROM dbo.tblCustomer C
LEFT JOIN dbo.tblState S ON S.ID = C.StateID
LEFT JOIN tblSettlementFactor SF ON SF.ID = C.SettlementFactorID

GO

EXEC _spRefreshAllViews
GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Customer "Settlement" $$ values to an Audited order
/***********************************/
ALTER PROCEDURE [dbo].[spProcessCustomerInvoice] 
(
  @ID int
, @UserName varchar(100)
, @ChainupFee money = null
, @RerouteFee money = null
, @WaitFee money = null
, @RejectionFee money = null
, @LoadFee money = null
) AS BEGIN
	DELETE FROM tblOrderInvoiceCustomer WHERE OrderID = @ID
	INSERT INTO tblOrderInvoiceCustomer (OrderID, ChainupFee, RerouteFee, WaitFee, RejectionFee, RouteRate, LoadFee, TotalFee, CreateDate, CreatedByUser)
	SELECT D.ID, ChainupFee, RerouteFee, WaitFee, RejectionFee, RouteRate, LoadFee
		, ChainupFee + RerouteFee + WaitFee + LoadFee AS TotalFee
		, GETDATE(), @UserName
	FROM (
		SELECT S.ID
			, isnull(S.Chainup, 0) * coalesce(@ChainupFee, CR.ChainupFee, 0) AS ChainupFee
			, coalesce(@RerouteFee, CR.RerouteFee * S.RerouteCount, 0) AS RerouteFee
			, coalesce(@WaitFee, dbo.fnFullQtrHour(S.TotalWaitMinutes) * 1.0 / 60 * CR.WaitFee, 0) AS WaitFee
			, coalesce(@RejectionFee, S.Rejected * CR.RejectionFee, 0) AS RejectionFee
			, coalesce(@LoadFee, CRR.Rate) AS RouteRate
			, coalesce(@LoadFee, dbo.fnMaxDecimal(S.MinSettlementBarrels, S.ActualBarrels) * CRR.Rate, 0) AS LoadFee
		FROM (
			SELECT O.ID
				, O.CustomerID
				, O.RouteID
				, O.ChainUp
				, O.CarrierRateMiles
				, CASE WHEN C.SettlementFactorID = 1 THEN O.OriginGrossBarrels ELSE O.OriginNetBarrels END AS ActualBarrels
				, O.RerouteCount
				, O.TotalWaitMinutes
				, O.Rejected
				, coalesce(CRXX.ID, CRXA.ID, CRAX.ID, CRAA.ID) AS CRID
				, isnull(C.MinSettlementBarrels, 0) AS MinSettlementBarrels
			FROM dbo.viewOrderExportFull O
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			JOIN tblCustomer C ON C.ID = O.CustomerID
			LEFT JOIN tblCustomerRates CRXX ON CRXX.CustomerID = O.CustomerID AND CRXX.RegionID = OO.RegionID
			LEFT JOIN tblCustomerRates CRXA ON CRXA.CustomerID = O.CustomerID AND CRXA.RegionID = -1
			LEFT JOIN tblCustomerRates CRAX ON CRAX.CustomerID = -1 AND CRAX.RegionID = OO.RegionID
			LEFT JOIN tblCustomerRates CRAA ON CRAA.CustomerID = -1 AND CRAA.RegionID = -1
			WHERE O.ID = @ID
		) S
		LEFT JOIN tblCustomerRates CR ON CR.ID = S.CRID
		LEFT JOIN tblCustomerRouteRates CRR ON CRR.CustomerID = S.CustomerID AND CRR.RouteID = S.RouteID
	) D
END

GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Carrier "Settlement" $$ values to an Audited order
/***********************************/
ALTER PROCEDURE [dbo].[spProcessCarrierInvoice]
(
  @ID int
, @UserName varchar(100)
, @ChainupFee money = null
, @RerouteFee money = null
, @WaitFee money = null
, @RejectionFee money = null
, @LoadFee money = null
) AS BEGIN
	EXEC dbo.spSyncProducer @ID
	EXEC dbo.spSyncActualMiles @ID
	
	DELETE FROM tblOrderInvoiceCarrier WHERE OrderID = @ID
	INSERT INTO tblOrderInvoiceCarrier (OrderID, ChainupFee, RerouteFee, WaitFee, RejectionFee, RouteRate, LoadFee, TotalFee, CreateDate, CreatedByUser)
	SELECT D.ID, ChainupFee, RerouteFee, WaitFee, RejectionFee, RouteRate, LoadFee
		, ChainupFee + RerouteFee + WaitFee + LoadFee AS TotalFee
		, GETDATE(), @UserName
	FROM (
		SELECT S.ID
			, isnull(S.Chainup, 0) * coalesce(@ChainupFee, CR.ChainupFee, 0) AS ChainupFee
			, coalesce(@RerouteFee, CR.RerouteFee * S.RerouteCount, 0) AS RerouteFee
			, coalesce(@WaitFee, dbo.fnFullQtrHour(S.TotalWaitMinutes) * 1.0 / 60 * CR.WaitFee, 0) AS WaitFee
			, coalesce(@RejectionFee, S.Rejected * CR.RejectionFee, 0) AS RejectionFee
			, coalesce(@LoadFee, CRR.Rate) AS RouteRate
			, coalesce(@LoadFee, dbo.fnMaxDecimal(S.MinSettlementBarrels, S.ActualBarrels) * CRR.Rate, 0) AS LoadFee
		FROM (
			SELECT O.ID
				, O.CarrierID
				, O.RouteID
				, O.ChainUp
				, O.CarrierRateMiles
				, CASE WHEN C.SettlementFactorID = 1 THEN O.OriginGrossBarrels ELSE O.OriginNetBarrels END AS ActualBarrels
				, O.RerouteCount
				, O.TotalWaitMinutes
				, O.Rejected
				, coalesce(CRXX.ID, CRXA.ID, CRAX.ID, CRAA.ID) AS CRID
				, isnull(C.MinSettlementBarrels, 0) AS MinSettlementBarrels
			FROM dbo.viewOrderExportFull O
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			JOIN tblCarrier C ON C.ID = O.CarrierID
			LEFT JOIN tblCarrierRates CRXX ON CRXX.CarrierID = O.CarrierID AND CRXX.RegionID = OO.RegionID
			LEFT JOIN tblCarrierRates CRXA ON CRXA.CarrierID = O.CarrierID AND CRXA.RegionID = -1
			LEFT JOIN tblCarrierRates CRAX ON CRAX.CarrierID = -1 AND CRAX.RegionID = OO.RegionID
			LEFT JOIN tblCarrierRates CRAA ON CRAA.CarrierID = -1 AND CRAA.RegionID = -1
			WHERE O.ID = @ID
		) S
		LEFT JOIN tblCarrierRates CR ON CR.ID = S.CRID
		LEFT JOIN tblCarrierRouteRates CRR ON CRR.CarrierID = S.CarrierID AND CRR.RouteID = S.RouteID 
	) D
END

GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
/***********************************/
ALTER PROCEDURE [dbo].[spOrdersExportCarrier]
(
  @StartDate datetime
, @EndDate datetime
, @CarrierID int = -1 -- all carriers
, @IncludeInvoiced bit = 0  -- don't include already invoiced Orders
) AS BEGIN
	SELECT cast(CASE WHEN isnull(OIC.Invoiced, 0) = 1 THEN 0 ELSE 1 END as bit) as Selected
		, OE.* 
		, isnull(OIC.Invoiced, 0) AS Invoiced
		, OIC.ChainupFee
		, OIC.RerouteFee
		, OIC.WaitFee
		, OIC.RejectionFee
		, OIC.RouteRate
		, OIC.LoadFee
		, OIC.TotalFee
		, cast(CASE WHEN OE.TicketCount = 0 THEN OE.TicketType ELSE NULL END as varchar(max)) AS TicketTypes
		, cast(CASE WHEN OE.TicketCount = 0 THEN OE.CarrierTicketNum ELSE NULL END as varchar(max)) AS TicketNums
		, cast(NULL as varchar(max)) AS ProductObsTemps
		, cast(NULL as varchar(max)) AS ProductObsGravities
		, cast(NULL as varchar(max)) AS ProductBSWs
		, cast(NULL as varchar(max)) AS TicketsRejected
		, cast(NULL as varchar(max)) AS TicketsRejectNotes
		, cast(NULL as varchar(max)) AS TicketsGrossBBLS
		, cast(NULL as varchar(max)) AS TicketsNetBBLS
		, cast(NULL as varchar(max)) AS PreviousDestinations
		, cast(NULL as varchar(max)) AS RerouteUsers
		, cast(NULL as varchar(max)) AS RerouteDates
	INTO #Orders
	FROM dbo.viewOrderExportFull OE
	LEFT JOIN dbo.tblOrderInvoiceCarrier OIC ON OIC.OrderID = OE.ID
	WHERE OE.StatusID IN (4)  
	  AND (@CarrierID=-1 OR OE.CarrierID=@CarrierID) 
	  AND OriginDepartTime >= @StartDate AND OE.OriginDepartTime < dateadd(day, 1, @EndDate) 
	  AND (@IncludeInvoiced = 1 OR OIC.Invoiced IS NULL OR OIC.Invoiced = 0) 
	  AND OE.DeleteDate IS NULL
	ORDER BY OE.OriginDepartTime
	
	DECLARE @OrderID int, @TicketID int, @RerouteID int
	SELECT @OrderID = min(ID) FROM #Orders WHERE ID > 0
	PRINT isnull(@OrderID, -1)
	
	WHILE @OrderID IS NOT NULL
	BEGIN
		-- add the Tickets information to the order (done this way since there could be multiple)
		SELECT @TicketID = min(ID) FROM dbo.tblOrderTicket WHERE OrderID = @OrderID AND DeleteDate IS NULL AND ID > 0
		WHILE @TicketID IS NOT NULL
		BEGIN
			UPDATE #Orders 
				SET TicketTypes = isnull(O.TicketTypes + '<br/>', '') + OT.TicketType
					, TicketNums = isnull(O.TicketNums + '<br/>', '') + OT.CarrierTicketNum
					, ProductObsTemps = isnull(O.ProductObsTemps + '<br/>', '') + isnull(ltrim(OT.ProductObsTemp), '')
					, ProductObsGravities = isnull(O.ProductObsGravities + '<br/>', '') + isnull(ltrim(OT.ProductObsGravity), '')
					, ProductBSWs = isnull(O.ProductBSWs + '<br/>', '') + isnull(ltrim(OT.ProductBSW), '')
					, TicketsRejected = isnull(O.TicketsRejected + '<br/>', '') + (CASE WHEN OT.Rejected = 0 THEN 'Valid' ELSE 'Rejected' END)
					, TicketsRejectNotes = isnull(O.TicketsRejectNotes + '<br/>', '') + isnull(OT.RejectNotes, '')
					, TicketsGrossBBLS = isnull(O.TicketsGrossBBLS + '<br/>', '') + isnull(ltrim(GrossBarrels), '')
					, TicketsNetBBLS = isnull(O.TicketsNetBBLS + '<br/>', '') + isnull(ltrim(NetBarrels), '')
			FROM #Orders O, viewOrderTicket OT
			WHERE O.ID = @OrderID AND OT.ID = @TicketID
			SELECT @TicketID = min(ID) FROM tblOrderTicket WHERE OrderID = @OrderID AND ID > @TicketID AND DeleteDate IS NULL
		END	
		-- add the Reroutes information to the order (done this way since there could be multiple)
		SELECT @RerouteID = min(ID) FROM dbo.tblOrderReroute WHERE OrderID = @OrderID AND ID > 0
		WHILE @RerouteID IS NOT NULL
		BEGIN
			UPDATE #Orders
				SET PreviousDestinations = O.PreviousDestinations + '<br/>' + O_R.PreviousDestinationFull
					, RerouteUsers = O.RerouteUsers + '<br/>' + O_R.UserName
					, RerouteDates = O.RerouteDates + '<br/>' + dbo.fnDateMdYY(O_R.RerouteDate)
			FROM #Orders O, viewOrderReroute O_R
			WHERE O.ID = @OrderID AND O_R.ID = @RerouteID
			SELECT @RerouteID = min(ID) FROM tblOrderReroute WHERE OrderID = @OrderID AND ID > @RerouteID
		END
		SELECT @OrderID = min(ID) FROM tblOrder WHERE ID > @OrderID
	END
	SELECT * FROM #Orders
END

GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
/***********************************/
ALTER PROCEDURE [dbo].[spOrdersExportCustomer]
(
  @StartDate datetime
, @EndDate datetime
, @CustomerID int = -1 -- all carriers
, @IncludeInvoiced bit = 0  -- don't include already invoiced Orders
) AS BEGIN
	SELECT cast(CASE WHEN isnull(OIC.Invoiced, 0) = 1 THEN 0 ELSE 1 END as bit) as Selected
		, OE.* 
		, isnull(OIC.Invoiced, 0) AS Invoiced
		, OIC.ChainupFee
		, OIC.RerouteFee
		, OIC.WaitFee
		, OIC.RejectionFee
		, OIC.RouteRate
		, OIC.LoadFee
		, OIC.TotalFee
		, cast(CASE WHEN OE.TicketCount = 0 THEN OE.TicketType ELSE NULL END as varchar(max)) AS TicketTypes
		, cast(CASE WHEN OE.TicketCount = 0 THEN OE.CarrierTicketNum ELSE NULL END as varchar(max)) AS TicketNums
		, cast(NULL as varchar(max)) AS ProductObsTemps
		, cast(NULL as varchar(max)) AS ProductObsGravities
		, cast(NULL as varchar(max)) AS ProductBSWs
		, cast(NULL as varchar(max)) AS TicketsRejected
		, cast(NULL as varchar(max)) AS TicketsRejectNotes
		, cast(NULL as varchar(max)) AS TicketsGrossBBLS
		, cast(NULL as varchar(max)) AS TicketsNetBBLS
		, cast(NULL as varchar(max)) AS PreviousDestinations
		, cast(NULL as varchar(max)) AS RerouteUsers
		, cast(NULL as varchar(max)) AS RerouteDates
	INTO #Orders
	FROM dbo.viewOrderExportFull OE
	LEFT JOIN dbo.tblOrderInvoiceCustomer OIC ON OIC.OrderID = OE.ID
	WHERE OE.StatusID IN (4)  
	  AND (@CustomerID=-1 OR OE.CustomerID=@CustomerID) 
	  AND OriginDepartTime >= @StartDate AND OE.OriginDepartTime < dateadd(day, 1, @EndDate) 
	  AND (@IncludeInvoiced = 1 OR OIC.Invoiced IS NULL OR OIC.Invoiced = 0) 
	  AND OE.DeleteDate IS NULL
	ORDER BY OE.OriginDepartTime
	
	DECLARE @OrderID int, @TicketID int, @RerouteID int
	SELECT @OrderID = min(ID) FROM #Orders WHERE ID > 0
	PRINT isnull(@OrderID, -1)
	
	WHILE @OrderID IS NOT NULL
	BEGIN
		-- add the Tickets information to the order (done this way since there could be multiple)
		SELECT @TicketID = min(ID) FROM dbo.tblOrderTicket WHERE OrderID = @OrderID AND DeleteDate IS NULL AND ID > 0
		WHILE @TicketID IS NOT NULL
		BEGIN
			UPDATE #Orders 
				SET TicketTypes = isnull(O.TicketTypes + '<br/>', '') + OT.TicketType
					, TicketNums = isnull(O.TicketNums + '<br/>', '') + OT.CarrierTicketNum
					, ProductObsTemps = isnull(O.ProductObsTemps + '<br/>', '') + isnull(ltrim(OT.ProductObsTemp), '')
					, ProductObsGravities = isnull(O.ProductObsGravities + '<br/>', '') + isnull(ltrim(OT.ProductObsGravity), '')
					, ProductBSWs = isnull(O.ProductBSWs + '<br/>', '') + isnull(ltrim(OT.ProductBSW), '')
					, TicketsRejected = isnull(O.TicketsRejected + '<br/>', '') + (CASE WHEN OT.Rejected = 0 THEN 'Valid' ELSE 'Rejected' END)
					, TicketsRejectNotes = isnull(O.TicketsRejectNotes + '<br/>', '') + isnull(OT.RejectNotes, '')
					, TicketsGrossBBLS = isnull(O.TicketsGrossBBLS + '<br/>', '') + isnull(ltrim(GrossBarrels), '')
					, TicketsNetBBLS = isnull(O.TicketsNetBBLS + '<br/>', '') + isnull(ltrim(NetBarrels), '')
			FROM #Orders O, viewOrderTicket OT
			WHERE O.ID = @OrderID AND OT.ID = @TicketID
			SELECT @TicketID = min(ID) FROM tblOrderTicket WHERE OrderID = @OrderID AND ID > @TicketID AND DeleteDate IS NULL
		END	
		-- add the Reroutes information to the order (done this way since there could be multiple)
		SELECT @RerouteID = min(ID) FROM dbo.tblOrderReroute WHERE OrderID = @OrderID AND ID > 0
		WHILE @RerouteID IS NOT NULL
		BEGIN
			UPDATE #Orders
				SET PreviousDestinations = O.PreviousDestinations + '<br/>' + O_R.PreviousDestinationFull
					, RerouteUsers = O.RerouteUsers + '<br/>' + O_R.UserName
					, RerouteDates = O.RerouteDates + '<br/>' + dbo.fnDateMdYY(O_R.RerouteDate)
			FROM #Orders O, viewOrderReroute O_R
			WHERE O.ID = @OrderID AND O_R.ID = @RerouteID
			SELECT @RerouteID = min(ID) FROM tblOrderReroute WHERE OrderID = @OrderID AND ID > @RerouteID
		END
		SELECT @OrderID = min(ID) FROM tblOrder WHERE ID > @OrderID
	END
	SELECT * FROM #Orders
END

GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: return Carrier route rates with computed Earliest & End Date values)
/***********************************/
ALTER VIEW [dbo].[viewCarrierRouteRatesBase] AS
SELECT CRR.*
	, R.OriginID, R.DestinationID
	, (SELECT MIN(dateadd(day, -1, EffectiveDate)) FROM tblCarrierRouteRates CRRN WHERE CRRN.CarrierID = CRR.CarrierID AND CRRN.RouteID = CRR.RouteID AND CRRN.EffectiveDate > CRR.EffectiveDate) AS EndDate
	, (SELECT MAX(dateadd(day, 1, EffectiveDate)) FROM tblCarrierRouteRates CRRP WHERE CRRP.CarrierID = CRR.CarrierID AND CRRP.RouteID = CRR.RouteID AND CRRP.EffectiveDate < CRR.EffectiveDate) AS EarliestEffectiveDate
FROM tblCarrierRouteRates CRR
JOIN tblRoute R ON R.ID = CRR.RouteID

GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alon
-- Purpose: return Carrier route rates with translated "friendly" values + "missing route rates", computed Earliest & End Date values)
/***********************************/
ALTER VIEW [dbo].[viewCarrierRouteRates] AS
SELECT CRR.ID, CarrierID, RouteID, EffectiveDate, Rate, CRR.CreateDate, CRR.CreatedByUser, CRR.LastChangeDate, CRR.LastChangedByUser
	, R.OriginID, R.DestinationID, R.Origin, R.Destination, R.ActualMiles, C.Name AS Carrier, OpenOrderCount, EndDate, EarliestEffectiveDate
	, cast(CASE WHEN C.DeleteDate IS NOT NULL OR R.Active = 0 THEN 0 ELSE 1 END as bit) AS Active
	, CASE WHEN CRR.ID IS NULL THEN 'Missing' WHEN C.DeleteDate IS NOT NULL THEN 'Carrier Deleted' WHEN R.Active = 0 THEN 'Route Inactive' ELSE 'Active' END AS Status
FROM (
	SELECT ID, CarrierID, RouteID, Rate, EffectiveDate, 0 AS OpenOrderCount
		, CreateDate, CreatedByUser, LastChangeDate, LastChangedByUser, EndDate, EarliestEffectiveDate
	FROM viewCarrierRouteRatesBase
	UNION 
	SELECT NULL, O.CarrierID, O.RouteID, NULL, MIN(dbo.fnDateOnly(isnull(OriginDepartTime, DueDate))), COUNT(1) AS OpenOrderCount
		, NULL, NULL, NULL, NULL
		, (SELECT MIN(dateadd(day, -1, EffectiveDate)) FROM tblCarrierRouteRates WHERE EffectiveDate > MIN(dbo.fnDateOnly(isnull(OriginDepartTime, DueDate)))) AS EndDate
		, NULL
	FROM tblOrder O
	LEFT JOIN tblOrderInvoiceCarrier OIC ON OIC.OrderID = O.ID
	LEFT JOIN viewCarrierRouteRatesBase CRR ON CRR.CarrierID = O.CarrierID AND O.OriginDepartTime >= CRR.EffectiveDate AND (CRR.EndDate IS NULL OR O.OriginDepartTime < DATEADD(day, 1, CRR.EndDate))
	WHERE O.StatusID = 4 AND ISNULL(OIC.Invoiced, 0) = 0 AND CRR.ID IS NULL
	GROUP BY O.CarrierID, O.RouteID
) CRR
JOIN viewRoute R ON R.ID = CRR.RouteID
JOIN tblCarrier C ON C.ID = CRR.CarrierID
WHERE (Active = 1 OR OpenOrderCount > 0)

GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: return Customer route rates with computed Earliest & End Date values)
/***********************************/
CREATE VIEW [dbo].[viewCustomerRouteRatesBase] AS
SELECT CRR.*
	, R.OriginID, R.DestinationID
	, (SELECT MIN(dateadd(day, -1, EffectiveDate)) FROM tblCustomerRouteRates CRRN WHERE CRRN.CustomerID = CRR.CustomerID AND CRRN.RouteID = CRR.RouteID AND CRRN.EffectiveDate > CRR.EffectiveDate) AS EndDate
	, (SELECT MAX(dateadd(day, 1, EffectiveDate)) FROM tblCustomerRouteRates CRRP WHERE CRRP.CustomerID = CRR.CustomerID AND CRRP.RouteID = CRR.RouteID AND CRRP.EffectiveDate < CRR.EffectiveDate) AS EarliestEffectiveDate
FROM tblCustomerRouteRates CRR
JOIN tblRoute R ON R.ID = CRR.RouteID

GO

GRANT SELECT ON viewCustomerRouteRatesBase TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 28 May 2013
-- Author: Kevin Alons
-- Purpose: return Customer route rates with translated "friendly" values + "missing route rates", computed Earliest & End Date values)
/***********************************/
ALTER VIEW [dbo].[viewCustomerRouteRates] AS
SELECT CRR.ID, C.ID AS CustomerID, R.ID AS RouteID, R.Origin, R.Destination, CRR.Rate
	, isnull(CRR.EffectiveDate, OO.MinDate) AS EffectiveDate
	, isnull(R.Active, 0) AS Active
	, CASE WHEN CRR.ID IS NULL THEN 'Missing' WHEN R.Active = 0 THEN 'Route Inactive' ELSE 'Active' END AS Status
	, R.OriginID, R.DestinationID
	, R.ActualMiles
	, C.Name AS Customer
	, OO.MinDate AS FirstMissingDate
	, OO.OpenOrderCount
	, (SELECT MIN(dateadd(day, -1, EffectiveDate)) FROM tblCustomerRouteRates CRRN WHERE CRRN.CustomerID = CRR.CustomerID AND CRRN.RouteID = CRR.RouteID AND CRRN.EffectiveDate > CRR.EffectiveDate) AS EndDate
	, (SELECT MAX(dateadd(day, 1, EffectiveDate)) FROM tblCustomerRouteRates CRRP WHERE CRRP.CustomerID = CRR.CustomerID AND CRRP.RouteID = CRR.RouteID AND CRRP.EffectiveDate < CRR.EffectiveDate) AS EarliestEffectiveDate
	, CRR.CreateDate, CRR.CreatedByUser, CRR.LastChangeDate, CRR.LastChangedByUser
FROM tblOrigin O
JOIN tblCustomer C ON C.ID = O.CustomerID
JOIN viewRoute R ON R.OriginID = O.ID
LEFT JOIN (
	SELECT RouteID, MIN(dbo.fnDateOnly(isnull(OriginDepartTime, DueDate))) AS MinDate, COUNT(1) AS OpenOrderCount
	FROM tblOrder O
	LEFT JOIN tblOrderInvoiceCustomer OIC ON OIC.OrderID = O.ID
	WHERE O.StatusID = 4 AND ISNULL(OIC.Invoiced, 0) = 0
	GROUP BY RouteID) OO ON OO.RouteID = R.ID
LEFT JOIN tblCustomerRouteRates CRR ON CRR.CustomerID = C.ID and CRR.RouteID = R.ID
WHERE R.Active = 1 AND (CRR.ID IS NOT NULL OR OpenOrderCount > 0)

GO

COMMIT