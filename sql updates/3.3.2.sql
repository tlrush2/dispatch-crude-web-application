-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.3.1'
SELECT  @NewVersion = '3.3.2'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'Rewrite of Carrier|Shipper Route Rates module'
GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: return Carrier route rates with computed Earliest & End Date values)
/***********************************/
ALTER VIEW [dbo].[viewCarrierRouteRatesBase] AS
SELECT CRR.*
	, R.OriginID, R.DestinationID
	, EndDate = (SELECT MIN(dateadd(day, -1, EffectiveDate)) FROM tblCarrierRouteRates CRRN WHERE CRRN.CarrierID = CRR.CarrierID AND CRRN.RouteID = CRR.RouteID AND CRRN.EffectiveDate > CRR.EffectiveDate)
	, EarliestEffectiveDate = (SELECT MAX(dateadd(day, 1, EffectiveDate)) FROM tblCarrierRouteRates CRRP WHERE CRRP.CarrierID = CRR.CarrierID AND CRRP.RouteID = CRR.RouteID AND CRRP.EffectiveDate < CRR.EffectiveDate)
	, R.ActualMiles
	, R.Active
FROM tblCarrierRouteRates CRR
JOIN viewRoute R ON R.ID = CRR.RouteID

GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: return Customer route rates with computed Earliest & End Date values)
/***********************************/
ALTER VIEW [dbo].[viewCustomerRouteRatesBase] AS
SELECT CRR.*
	, R.OriginID, R.DestinationID
	, EndDate = (SELECT MIN(dateadd(day, -1, EffectiveDate)) FROM tblCustomerRouteRates CRRN WHERE CRRN.CustomerID = CRR.CustomerID AND CRRN.RouteID = CRR.RouteID AND CRRN.EffectiveDate > CRR.EffectiveDate) 
	, EarliestEffectiveDate = (SELECT MAX(dateadd(day, 1, EffectiveDate)) FROM tblCustomerRouteRates CRRP WHERE CRRP.CustomerID = CRR.CustomerID AND CRRP.RouteID = CRR.RouteID AND CRRP.EffectiveDate < CRR.EffectiveDate) 
	, R.ActualMiles
	, R.Active
FROM tblCustomerRouteRates CRR
JOIN viewRoute R ON R.ID = CRR.RouteID

GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[viewCarrier_RouteMetrics]'))
DROP VIEW [dbo].[viewCarrier_RouteMetrics]
GO
/***********************************/
-- Date Created: 20 Sep 2014
-- Author: Kevin Alons
-- Purpose: return Carrier metrics for all Order Statuses
/***********************************/
CREATE VIEW viewCarrier_RouteMetrics AS
	SELECT O.CarrierID
		, O.RouteID
		, O.OriginID, O.DestinationID
		, OrderCount = COUNT(1)
		, MinOrderDate = cast(MIN(O.OriginDepartTimeUTC) as date)
		, O.StatusID
		, IsSettled = CASE WHEN IOC.BatchID IS NULL THEN 0 ELSE 1 END
	FROM tblOrder O
	LEFT JOIN tblOrderInvoiceCarrier IOC ON IOC.OrderID = O.ID
	WHERE DeleteDateUTC IS NULL 
		AND RouteID IS NOT NULL 
		AND CarrierID IS NOT NULL
	GROUP BY CarrierID, RouteID
		, O.OriginID, O.DestinationID
		, StatusID, CASE WHEN IOC.BatchID IS NULL THEN 0 ELSE 1 END

GO
GRANT SELECT ON viewCarrier_RouteMetrics TO dispatchcrude_iis_acct
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[viewCustomer_RouteMetrics]'))
DROP VIEW [dbo].[viewCustomer_RouteMetrics]
GO
/***********************************/
-- Date Created: 20 Sep 2014
-- Author: Kevin Alons
-- Purpose: return Customer metrics for all Order Statuses
/***********************************/
CREATE VIEW viewCustomer_RouteMetrics AS
	SELECT O.CustomerID
		, O.RouteID
		, O.OriginID, O.DestinationID
		, OrderCount = COUNT(1)
		, MinOrderDate = cast(MIN(O.OriginDepartTimeUTC) as date)
		, O.StatusID
		, IsSettled = CASE WHEN IOC.BatchID IS NULL THEN 0 ELSE 1 END
	FROM tblOrder O
	LEFT JOIN tblOrderInvoiceCustomer IOC ON IOC.OrderID = O.ID
	WHERE DeleteDateUTC IS NULL 
		AND RouteID IS NOT NULL 
		AND CustomerID IS NOT NULL
	GROUP BY CustomerID, RouteID
		, O.OriginID, O.DestinationID
		, StatusID, CASE WHEN IOC.BatchID IS NULL THEN 0 ELSE 1 END

GO
GRANT SELECT ON viewCustomer_RouteMetrics TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 9 Jun 2013
-- Author: Kevin Alons
-- Purpose: return Carrier route rates with translated "friendly" values + "missing route rates", computed Earliest & End Date values)
/***********************************/
ALTER VIEW [dbo].[viewCarrierRouteRates] AS
SELECT RB.ID, RouteID = R.ID
	, O.UomID 
	, Rate = isnull(RB.Rate, 0) 
	, EffectiveDate = isnull(RB.EffectiveDate, RM.MinOrderDate) 
	, Active = cast(CASE WHEN RB.Active = 0 THEN 0 ELSE 1 END as bit) 
	, OpenOrderCount = isnull(RM.OrderCount, 0)
	, ActualMiles = isnull(R.ActualMiles, 0)
	, RB.CreateDateUTC, RB.CreatedByUser, RB.LastChangeDateUTC, RB.LastChangedByUser
	, RB.EndDate
	, RB.EarliestEffectiveDate
	, C.ID AS CarrierID, C.Name AS Carrier
	, R.OriginID, Origin = O.Name, OriginFull = O.FullName
	, R.DestinationID, Destination = D.Name, DestinationFull = D.FullName
	, Status = CASE WHEN RB.ID IS NULL THEN 'Missing' ELSE 'Active' END 
	, RouteInUse = cast(CASE WHEN OrderCount = 0 THEN 0 ELSE 1 END AS bit) 
	, O.UOM
	, O.UomShort
FROM viewCarrier C CROSS JOIN viewOrigin O 
JOIN tblRoute R ON R.OriginID = O.ID
JOIN viewDestination D ON D.ID = R.DestinationID
LEFT JOIN (
	SELECT CarrierID
		, RouteID
		, OrderCount = sum(OrderCount)
		, MinOrderDate = MIN(MinOrderDate)
		, IsSettled
	FROM viewCarrier_RouteMetrics 
	GROUP BY CarrierID, RouteID, IsSettled
) RM ON RM.CarrierID = C.ID AND RM.RouteID = R.ID
LEFT JOIN viewCarrierRouteRatesBase RB ON RB.CarrierID = C.ID AND RB.RouteID = R.ID
WHERE (RB.CarrierID IS NOT NULL OR (RM.IsSettled IS NOT NULL AND RM.IsSettled = 0))

GO

/***********************************/
-- Date Created: 9 Jun 2013
-- Author: Kevin Alons
-- Purpose: return Customer route rates with translated "friendly" values + "missing route rates", computed Earliest & End Date values)
/***********************************/
ALTER VIEW [dbo].[viewCustomerRouteRates] AS
SELECT RB.ID, RouteID = R.ID
	, O.UomID 
	, Rate = isnull(RB.Rate, 0) 
	, EffectiveDate = isnull(RB.EffectiveDate, RM.MinOrderDate) 
	, Active = cast(CASE WHEN RB.Active = 0 THEN 0 ELSE 1 END as bit) 
	, OpenOrderCount = isnull(RM.OrderCount, 0)
	, ActualMiles = isnull(R.ActualMiles, 0)
	, RB.CreateDateUTC, RB.CreatedByUser, RB.LastChangeDateUTC, RB.LastChangedByUser
	, RB.EndDate
	, RB.EarliestEffectiveDate
	, C.ID AS CustomerID, C.Name AS Customer
	, R.OriginID, Origin = O.Name, OriginFull = O.FullName
	, R.DestinationID, Destination = D.Name, DestinationFull = D.FullName
	, Status = CASE WHEN RB.ID IS NULL THEN 'Missing' ELSE 'Active' END 
	, RouteInUse = cast(CASE WHEN OrderCount = 0 THEN 0 ELSE 1 END AS bit) 
	, O.UOM
	, O.UomShort
FROM viewCustomer C 
JOIN viewOrigin O ON O.CustomerID = C.ID
JOIN tblRoute R ON R.OriginID = O.ID
JOIN viewDestination D ON D.ID = R.DestinationID
LEFT JOIN (
	SELECT CustomerID
		, RouteID
		, OrderCount = sum(OrderCount)
		, MinOrderDate = MIN(MinOrderDate)
		, IsSettled
	FROM viewCustomer_RouteMetrics 
	GROUP BY CustomerID, RouteID, IsSettled
) RM ON RM.CustomerID = C.ID AND RM.RouteID = R.ID
LEFT JOIN viewCustomerRouteRatesBase RB ON RB.CustomerID = C.ID AND RB.RouteID = R.ID
WHERE (RB.CustomerID IS NOT NULL OR (RM.IsSettled IS NOT NULL AND RM.IsSettled = 0))

GO

/***********************************/
-- Date Created: 18 Sep 2014
-- Author: Kevin Alons
-- Purpose: return Carrier route rates per the specified criteria (used by Carrier Route Rates page)
/***********************************/
ALTER PROCEDURE [dbo].[spGetCarrierRouteRates](
  @CarrierID int
, @originID int
, @destinationID int
, @startDate date
, @includeActive bit
, @includeMissing bit
) AS
BEGIN
	SELECT TOP 100 PERCENT *
		, NewRate = CASE WHEN Status LIKE '%Missing' THEN Rate ELSE NULL END 
		, NewEffectiveDate = CASE WHEN Status LIKE '%Missing' THEN EffectiveDate ELSE NULL END 
		, ImportOutcome = NULL
	FROM (
		-- get the currently defined or missing rates
		SELECT ID, RouteID, UomID, Rate, EffectiveDate, Active, OpenOrderCount, ActualMiles
			, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser
			, EndDate, EarliestEffectiveDate, CarrierID, Carrier, OriginID, Origin, OriginFull, DestinationID, Destination, DestinationFull
			, Status, RouteInUse, UOM, UomShort
		FROM dbo.viewCarrierRouteRates 
		WHERE (@CarrierID = -1 OR CarrierID = @CarrierID) 
			AND (@OriginID = -1 OR OriginID = @OriginID) 
			AND (@DestinationID = -1 OR DestinationID = @DestinationID) 
			AND (EndDate IS NULL OR EndDate >= @StartDate) 
			AND (@IncludeActive = 1 OR Status NOT LIKE 'Active') 
			AND (@IncludeMissing = 1 OR Status NOT LIKE 'Missing') 
	) X
	ORDER BY Carrier, Origin, Destination, EffectiveDate DESC
		
END

GO

/***********************************/
-- Date Created: 18 Sep 2014
-- Author: Kevin Alons
-- Purpose: return Customer route rates per the specified criteria (used by Customer Route Rates page)
/***********************************/
ALTER PROCEDURE [dbo].[spGetCustomerRouteRates](
  @CustomerID int
, @originID int
, @destinationID int
, @startDate date
, @includeActive bit
, @includeMissing bit
) AS
BEGIN
	SELECT TOP 100 PERCENT *
		, NewRate = CASE WHEN Status LIKE '%Missing' THEN Rate ELSE NULL END 
		, NewEffectiveDate = CASE WHEN Status LIKE '%Missing' THEN EffectiveDate ELSE NULL END 
		, ImportOutcome = NULL
	FROM (
		-- get the currently defined or missing rates
		SELECT ID, RouteID, UomID, Rate, EffectiveDate, Active, OpenOrderCount, ActualMiles
			, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser
			, EndDate, EarliestEffectiveDate, CustomerID, Customer, OriginID, Origin, OriginFull, DestinationID, Destination, DestinationFull
			, Status, RouteInUse, UOM, UomShort
		FROM dbo.viewCustomerRouteRates 
		WHERE (@CustomerID = -1 OR CustomerID = @CustomerID) 
			AND (@OriginID = -1 OR OriginID = @OriginID) 
			AND (@DestinationID = -1 OR DestinationID = @DestinationID) 
			AND (EndDate IS NULL OR EndDate >= @StartDate) 
			AND (@IncludeActive = 1 OR Status NOT LIKE 'Active') 
			AND (@IncludeMissing = 1 OR Status NOT LIKE 'Missing') 
	) X
	ORDER BY Customer, Origin, Destination, EffectiveDate DESC
		
END

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetRouteID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetRouteID]
GO
/***********************************/
-- Date Created: 15 Nov 2014
-- Author: Kevin Alons
-- Purpose: retrieve (or first generate) a RouteID
/***********************************/
CREATE PROCEDURE spGetRouteID(@originID int, @destinationID int, @UserName varchar(100), @routeID int = NULL OUTPUT) AS
BEGIN
	SELECT @routeID FROM tblRoute WHERE OriginID = @originID AND DestinationID = @destinationID
	IF (@routeID IS NULL)
		INSERT INTO tblRoute (OriginID, DestinationID, CreatedByUser, CreateDateUTC)
			VALUES (@originID, @destinationID, @UserName, GETUTCDATE())
	SELECT @routeID = SCOPE_IDENTITY()
END
GO
GRANT EXECUTE ON spGetRouteID to dispatchcrude_iis_acct
GO

EXEC _spRebuildAllObjects
GO

COMMIT
SET NOEXEC OFF