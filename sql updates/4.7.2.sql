SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.7.1'
SELECT  @NewVersion = '4.7.2'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'JT-3960 - Drivers scheulding fix for non utilized status'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/***********************************************************/
-- Date Created: 4.3.0 - 2016.10.25 - Kevin Alons
-- Purpose: set the DriverSchedule data for the specified parameters
-- Changes:
--		4.7.2		JAE			2017-05-25		Add default values when a non-utilized status omits start hour and duration
/***********************************************************/
ALTER PROCEDURE spUpdateDriverSchedule
(
  @DriverID int
, @ScheduleDate date
, @StatusID smallint
, @StartHours tinyint = null
, @DurationHours tinyint = null
, @ManualAssignment bit = 1
, @UserName varchar(100)
) AS 
BEGIN
	IF EXISTS(SELECT * FROM tblDriverSchedule WHERE DriverID = @DriverID AND ScheduleDate = @ScheduleDate)
		UPDATE tblDriverSchedule 
		SET StatusID = @StatusID, StartHours = @StartHours, DurationHours = @DurationHours, ManualAssignment = @ManualAssignment
			, LastChangeDateUTC = GETUTCDATE(), LastChangedByUser = @UserName
		WHERE DriverID = @DriverID AND ScheduleDate = @ScheduleDate
	ELSE
		INSERT INTO tblDriverSchedule (DriverID, ScheduleDate, StatusID, StartHours, DurationHours, ManualAssignment, CreatedByUser)
			VALUES (@DriverID, @ScheduleDate, @StatusID, @StartHours, @DurationHours, @ManualAssignment, @UserName)
END

GO

COMMIT
SET NOEXEC OFF
