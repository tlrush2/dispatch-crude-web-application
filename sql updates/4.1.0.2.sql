-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.1.0.1'
SELECT  @NewVersion = '4.1.0.2'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-945 - Add Independent Driver/Driver Group Settlement - table index additions/fixes'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

-- CARRIER indices
ALTER TABLE tblOrderSettlementCarrier DROP CONSTRAINT FK_OrderSettlementCarrier_DestinationWaitRate
GO
ALTER TABLE dbo.tblCarrierDestinationWaitRate DROP CONSTRAINT PK_CarrierDestinationWaitRate
GO
ALTER TABLE dbo.tblCarrierDestinationWaitRate ADD CONSTRAINT PK_CarrierDestinationWaitRate PRIMARY KEY NONCLUSTERED (ID)
GO
ALTER TABLE tblOrderSettlementCarrier ADD CONSTRAINT FK_OrderSettlementCarrier_DestinationWaitRate FOREIGN KEY (DestinationWaitRateID) REFERENCES tblCarrierDestinationWaitRate(ID)
GO

DROP INDEX udxCarrierDestinationWaitRate_Main ON dbo.tblCarrierDestinationWaitRate
GO
CREATE UNIQUE CLUSTERED INDEX udxCarrierDestinationWaitRate_Main ON dbo.tblCarrierDestinationWaitRate
(
	ReasonID,
	TruckTypeID,
	ShipperID,
	CarrierID,
	ProductGroupID,
	DriverGroupID,
	DestinationID,
	StateID,
	RegionID,
	ProducerID,
	EffectiveDate
)
GO

ALTER TABLE tblOrderSettlementCarrier DROP CONSTRAINT FK_OrderSettlementCarrier_OrderRejectRate
GO
ALTER TABLE dbo.tblCarrierOrderRejectRate DROP CONSTRAINT PK_CarrierOrderRejectRate
GO
ALTER TABLE dbo.tblCarrierOrderRejectRate ADD CONSTRAINT PK_CarrierOrderRejectRate PRIMARY KEY NONCLUSTERED (ID)
GO
ALTER TABLE tblOrderSettlementCarrier ADD CONSTRAINT FK_OrderSettlementCarrier_OrderRejectRate FOREIGN KEY (OrderRejectRateID) REFERENCES tblCarrierOrderRejectRate(ID)
GO
DROP INDEX udxCarrierOrderRejectRate_Main ON dbo.tblCarrierOrderRejectRate
GO
CREATE UNIQUE CLUSTERED INDEX udxCarrierOrderRejectRate_Main ON dbo.tblCarrierOrderRejectRate
(
	ReasonID,
	TruckTypeID,
	ShipperID,
	CarrierID,
	ProductGroupID,
	DriverGroupID,
	OriginID,
	StateID,
	RegionID,
	ProducerID,
	EffectiveDate
)
GO

ALTER TABLE tblOrderSettlementCarrier DROP CONSTRAINT FK_OrderSettlementCarrier_OriginWaitRate
GO
ALTER TABLE dbo.tblCarrierOriginWaitRate DROP CONSTRAINT PK_CarrierOriginWaitRate
GO
ALTER TABLE dbo.tblCarrierOriginWaitRate ADD  CONSTRAINT PK_CarrierOriginWaitRate PRIMARY KEY NONCLUSTERED (ID)
GO
ALTER TABLE tblOrderSettlementCarrier ADD CONSTRAINT FK_OrderSettlementCarrier_OriginWaitRate FOREIGN KEY (OriginWaitRateID) REFERENCES tblCarrierOriginWaitRate(ID)
GO
DROP INDEX udxCarrierOriginWaitRate_Main ON dbo.tblCarrierOriginWaitRate
GO
CREATE UNIQUE CLUSTERED INDEX udxCarrierOriginWaitRate_Main ON dbo.tblCarrierOriginWaitRate
(
	ReasonID,
	TruckTypeID,
	ShipperID,
	CarrierID,
	ProductGroupID,
	DriverGroupID,
	OriginID,
	StateID,
	RegionID,
	ProducerID,
	EffectiveDate
)
GO

--- SHIPPER indices
ALTER TABLE tblOrderSettlementShipper DROP CONSTRAINT FK_OrderSettlementShipper_DestinationWaitRate
GO
ALTER TABLE dbo.tblShipperDestinationWaitRate DROP CONSTRAINT PK_ShipperDestinationWaitRate
GO
ALTER TABLE dbo.tblShipperDestinationWaitRate ADD CONSTRAINT PK_ShipperDestinationWaitRate PRIMARY KEY NONCLUSTERED (ID)
GO
ALTER TABLE tblOrderSettlementShipper ADD CONSTRAINT FK_OrderSettlementShipper_DestinationWaitRate FOREIGN KEY (DestinationWaitRateID) REFERENCES tblShipperDestinationWaitRate(ID)
GO
DROP INDEX udxShipperDestinationWaitRate_Main ON dbo.tblShipperDestinationWaitRate
GO
CREATE UNIQUE CLUSTERED INDEX udxShipperDestinationWaitRate_Main ON dbo.tblShipperDestinationWaitRate
(
	ReasonID,
	TruckTypeID,
	ShipperID,
	ProductGroupID,
	DestinationID,
	StateID,
	RegionID,
	ProducerID,
	EffectiveDate
)
GO

ALTER TABLE tblOrderSettlementShipper DROP CONSTRAINT FK_OrderSettlementShipper_OrderRejectRate
GO
ALTER TABLE dbo.tblShipperOrderRejectRate DROP CONSTRAINT PK_ShipperOrderRejectRate
GO
ALTER TABLE dbo.tblShipperOrderRejectRate ADD CONSTRAINT PK_ShipperOrderRejectRate PRIMARY KEY NONCLUSTERED (ID)
GO
ALTER TABLE tblOrderSettlementShipper ADD CONSTRAINT FK_OrderSettlementShipper_OrderRejectRate FOREIGN KEY (OrderRejectRateID) REFERENCES tblShipperOrderRejectRate(ID)
GO
DROP INDEX udxShipperOrderRejectRate_Main ON dbo.tblShipperOrderRejectRate
GO
CREATE UNIQUE CLUSTERED INDEX udxShipperOrderRejectRate_Main ON dbo.tblShipperOrderRejectRate
(
	ReasonID,
	TruckTypeID,
	ShipperID,
	ProductGroupID,
	OriginID,
	StateID,
	RegionID,
	ProducerID,
	EffectiveDate
)
GO

ALTER TABLE tblOrderSettlementShipper DROP CONSTRAINT FK_OrderSettlementShipper_OriginWaitRate
GO
ALTER TABLE dbo.tblShipperOriginWaitRate DROP CONSTRAINT PK_ShipperOriginWaitRate
GO
ALTER TABLE dbo.tblShipperOriginWaitRate ADD  CONSTRAINT PK_ShipperOriginWaitRate PRIMARY KEY NONCLUSTERED (ID)
GO
ALTER TABLE tblOrderSettlementShipper ADD CONSTRAINT FK_OrderSettlementShipper_OriginWaitRate FOREIGN KEY (OriginWaitRateID) REFERENCES tblShipperOriginWaitRate(ID)
GO
DROP INDEX udxShipperOriginWaitRate_Main ON dbo.tblShipperOriginWaitRate
GO
CREATE UNIQUE CLUSTERED INDEX udxShipperOriginWaitRate_Main ON dbo.tblShipperOriginWaitRate
(
	ReasonID,
	TruckTypeID,
	ShipperID,
	ProductGroupID,
	OriginID,
	StateID,
	RegionID,
	ProducerID,
	EffectiveDate
)
GO

-- DRIVER indices
CREATE UNIQUE CLUSTERED INDEX udxDriverAssessorialRate_Main ON dbo.tblDriverAssessorialRate
(
	TypeID,
	TruckTypeID,
	ShipperID,
	CarrierID,
	ProductGroupID,
	DriverGroupID,
	DriverID,
	OriginID,
	DestinationID,
	OriginStateID,
	DestStateID,
	RegionID,
	ProducerID,
	EffectiveDate
)
GO

CREATE NONCLUSTERED INDEX idxDriverAssessorialRate_Driver ON dbo.tblDriverAssessorialRate
(
	DriverID
)
GO
CREATE NONCLUSTERED INDEX idxDriverAssessorialRate_Destination ON dbo.tblDriverAssessorialRate
(
	DestinationID
)
GO
CREATE NONCLUSTERED INDEX idxDriverAssessorialRate_DestState ON dbo.tblDriverAssessorialRate
(
	DestStateID
)
GO
CREATE NONCLUSTERED INDEX idxDriverAssessorialRate_DriverGroup ON dbo.tblDriverAssessorialRate
(
	DriverGroupID
)
GO
CREATE NONCLUSTERED INDEX idxDriverAssessorialRate_Origin ON dbo.tblDriverAssessorialRate
(
	OriginID
)
GO
CREATE NONCLUSTERED INDEX idxDriverAssessorialRate_OriginState ON dbo.tblDriverAssessorialRate
(
	OriginStateID
)
GO
CREATE NONCLUSTERED INDEX idxDriverAssessorialRate_Producer ON dbo.tblDriverAssessorialRate
(
	ProducerID
)
GO
CREATE NONCLUSTERED INDEX idxDriverAssessorialRate_ProductGroup ON dbo.tblDriverAssessorialRate
(
	ProductGroupID
)
GO
CREATE NONCLUSTERED INDEX idxDriverAssessorialRate_Region ON dbo.tblDriverAssessorialRate
(
	RegionID
)
GO
CREATE NONCLUSTERED INDEX idxDriverAssessorialRate_TruckType ON dbo.tblDriverAssessorialRate
(
	TruckTypeID
)
GO

CREATE NONCLUSTERED INDEX idxDriverDestinationWaitRate_Driver ON dbo.tblDriverDestinationWaitRate
(
	DriverID
)
GO
CREATE NONCLUSTERED INDEX idxDriverDestinationWaitRate_Destination ON dbo.tblDriverDestinationWaitRate
(
	DestinationID
)
GO
CREATE NONCLUSTERED INDEX idxDriverDestinationWaitRate_DriverGroup ON dbo.tblDriverDestinationWaitRate
(
	DriverGroupID
)
GO
CREATE NONCLUSTERED INDEX idxDriverDestinationWaitRate_Producer ON dbo.tblDriverDestinationWaitRate
(
	ProducerID
)
GO
CREATE NONCLUSTERED INDEX idxDriverDestinationWaitRate_ProductGroup ON dbo.tblDriverDestinationWaitRate
(
	ProductGroupID
)
GO
CREATE NONCLUSTERED INDEX idxDriverDestinationWaitRate_Reason ON dbo.tblDriverDestinationWaitRate
(
	ReasonID
)
GO
CREATE NONCLUSTERED INDEX idxDriverDestinationWaitRate_Region ON dbo.tblDriverDestinationWaitRate
(
	RegionID
)
GO
CREATE NONCLUSTERED INDEX idxDriverDestinationWaitRate_State ON dbo.tblDriverDestinationWaitRate
(
	StateID
)
GO
CREATE NONCLUSTERED INDEX idxDriverDestinationWaitRate_TruckType ON dbo.tblDriverDestinationWaitRate
(
	TruckTypeID
)
GO

ALTER TABLE tblOrderSettlementDriver DROP CONSTRAINT FK_OrderSettlementDriver_DestinationWaitRate 
GO
ALTER TABLE dbo.tblDriverDestinationWaitRate DROP CONSTRAINT PK_DriverDestinationWaitRate
GO
ALTER TABLE dbo.tblDriverDestinationWaitRate ADD CONSTRAINT PK_DriverDestinationWaitRate PRIMARY KEY NONCLUSTERED (ID)
GO
ALTER TABLE tblOrderSettlementDriver ADD CONSTRAINT FK_OrderSettlementDriver_DestinationWaitRate FOREIGN KEY (DestinationWaitRateID) REFERENCES tblDriverDestinationWaitRate(ID)
GO 
CREATE UNIQUE CLUSTERED INDEX udxDriverDestinationWaitRate_Main ON tblDriverDestinationWaitRate
(
	ReasonID,
	TruckTypeID,
	ShipperID,
	CarrierID,
	ProductGroupID,
	DriverGroupID,
	DriverID, 
	DestinationID,
	StateID,
	RegionID,
	ProducerID,
	EffectiveDate
)
GO

CREATE NONCLUSTERED INDEX idxDriverOrderRejectRate_Driver ON dbo.tblDriverOrderRejectRate
(
	DriverID
)
GO
CREATE NONCLUSTERED INDEX idxDriverOrderRejectRate_DriverGroup ON dbo.tblDriverOrderRejectRate
(
	DriverGroupID
)
GO
CREATE NONCLUSTERED INDEX idxDriverOrderRejectRate_Origin ON dbo.tblDriverOrderRejectRate
(
	OriginID
)
GO
CREATE NONCLUSTERED INDEX idxDriverOrderRejectRate_Producer ON dbo.tblDriverOrderRejectRate
(
	ProducerID
)
GO
CREATE NONCLUSTERED INDEX idxDriverOrderRejectRate_ProductGroup ON dbo.tblDriverOrderRejectRate
(
	ProductGroupID
)
GO
CREATE NONCLUSTERED INDEX idxDriverOrderRejectRate_Reason ON dbo.tblDriverOrderRejectRate
(
	ReasonID
)
GO 
CREATE NONCLUSTERED INDEX idxDriverOrderRejectRate_Region ON dbo.tblDriverOrderRejectRate
(
	RegionID
)
GO
CREATE NONCLUSTERED INDEX idxDriverOrderRejectRate_State ON dbo.tblDriverOrderRejectRate
(
	StateID
)
GO
CREATE NONCLUSTERED INDEX idxDriverOrderRejectRate_TruckType ON dbo.tblDriverOrderRejectRate
(
	TruckTypeID
)
GO

ALTER TABLE tblOrderSettlementDriver DROP CONSTRAINT FK_OrderSettlementDriver_OrderRejectRate
GO
ALTER TABLE dbo.tblDriverOrderRejectRate DROP CONSTRAINT PK_DriverOrderRejectRate
GO
ALTER TABLE dbo.tblDriverOrderRejectRate ADD CONSTRAINT PK_DriverOrderRejectRate PRIMARY KEY NONCLUSTERED (ID)
GO
ALTER TABLE tblOrderSettlementDriver ADD CONSTRAINT FK_OrderSettlementDriver_OrderRejectRate FOREIGN KEY (OrderRejectRateID) REFERENCES tblDriverOrderRejectRate(ID)
GO
CREATE UNIQUE CLUSTERED INDEX udxDriverOrderRejectRate_Main ON dbo.tblDriverOrderRejectRate
(
	ReasonID,
	TruckTypeID,
	ShipperID,
	CarrierID,
	ProductGroupID,
	DriverGroupID,
	DriverID,
	OriginID,
	StateID,
	RegionID,
	ProducerID,
	EffectiveDate
)
GO

CREATE NONCLUSTERED INDEX idxDriverOriginWaitRate_Driver ON dbo.tblDriverOriginWaitRate
(
	DriverID
)
GO
CREATE NONCLUSTERED INDEX idxDriverOriginWaitRate_DriverGroup ON dbo.tblDriverOriginWaitRate
(
	DriverGroupID
)
GO
CREATE NONCLUSTERED INDEX idxDriverOriginWaitRate_Origin ON dbo.tblDriverOriginWaitRate
(
	OriginID
)
GO
CREATE NONCLUSTERED INDEX idxDriverOriginWaitRate_Producer ON dbo.tblDriverOriginWaitRate
(
	ProducerID
)
GO
CREATE NONCLUSTERED INDEX idxDriverOriginWaitRate_ProductGroup ON dbo.tblDriverOriginWaitRate
(
	ProductGroupID
)
GO
CREATE NONCLUSTERED INDEX idxDriverOriginWaitRate_Reason ON dbo.tblDriverOriginWaitRate
(
	ReasonID
)
GO
CREATE NONCLUSTERED INDEX idxDriverOriginWaitRate_Region ON dbo.tblDriverOriginWaitRate
(
	RegionID
)
GO
CREATE NONCLUSTERED INDEX idxDriverOriginWaitRate_State ON dbo.tblDriverOriginWaitRate
(
	StateID
)
GO
CREATE NONCLUSTERED INDEX idxDriverOriginWaitRate_TruckType ON dbo.tblDriverOriginWaitRate
(
	TruckTypeID
)
GO

ALTER TABLE tblOrderSettlementDriver DROP CONSTRAINT FK_OrderSettlementDriver_OriginWaitRate
GO
ALTER TABLE dbo.tblDriverOriginWaitRate DROP CONSTRAINT PK_DriverOriginWaitRate
GO
ALTER TABLE dbo.tblDriverOriginWaitRate ADD  CONSTRAINT PK_DriverOriginWaitRate PRIMARY KEY NONCLUSTERED (ID)
GO
ALTER TABLE tblOrderSettlementDriver ADD CONSTRAINT FK_OrderSettlementDriver_OriginWaitRate FOREIGN KEY (OriginWaitRateID) REFERENCES tblDriverOriginWaitRate(ID)
GO
CREATE UNIQUE CLUSTERED INDEX udxDriverOriginWaitRate_Main ON dbo.tblDriverOriginWaitRate
(
	ReasonID,
	TruckTypeID,
	ShipperID,
	CarrierID,
	ProductGroupID,
	DriverGroupID,
	DriverID,
	OriginID,
	StateID,
	RegionID,
	ProducerID,
	EffectiveDate
)
GO

CREATE NONCLUSTERED INDEX idxDriverRateSheet_Driver ON dbo.tblDriverRateSheet
(
	DriverID
)
GO
CREATE NONCLUSTERED INDEX idxDriverRateSheet_DestState ON dbo.tblDriverRateSheet
(
	DestStateID
)
GO
CREATE NONCLUSTERED INDEX idxDriverRateSheet_DriverGroup ON dbo.tblDriverRateSheet
(
	DriverGroupID
)
GO
CREATE NONCLUSTERED INDEX idxDriverRateSheet_OriginState ON dbo.tblDriverRateSheet
(
	OriginStateID
)
GO
CREATE NONCLUSTERED INDEX idxDriverRateSheet_ProductGroup ON dbo.tblDriverRateSheet
(
	ProductGroupID
)
GO
CREATE NONCLUSTERED INDEX idxDriverRateSheet_Region ON dbo.tblDriverRateSheet
(
	RegionID
)
GO
CREATE NONCLUSTERED INDEX idxDriverRateSheet_TruckType ON dbo.tblDriverRateSheet
(
	TruckTypeID
)
GO
CREATE UNIQUE CLUSTERED INDEX udxDriverRangeRate_Main ON dbo.tblDriverRangeRate
(
	RateSheetID,
	MinRange,
	MaxRange
)
GO
CREATE UNIQUE CLUSTERED INDEX udxDriverRateSheet_Main ON dbo.tblDriverRateSheet
(
	TruckTypeID,
	ShipperID,
	CarrierID,
	ProductGroupID,
	DriverGroupID,
	DriverID, 
	RegionID,
	OriginStateID,
	DestStateID,
	ProducerID,
	EffectiveDate
)
GO

CREATE NONCLUSTERED INDEX idxDriverRouteRate_DriverGroup ON dbo.tblDriverRouteRate
(
	DriverGroupID
)
GO
CREATE NONCLUSTERED INDEX idxDriverRouteRate_Driver ON dbo.tblDriverRouteRate
(
	DriverID
)
GO
CREATE NONCLUSTERED INDEX idxDriverRouteRate_ProductGroup ON dbo.tblDriverRouteRate
(
	ProductGroupID
)
GO
CREATE NONCLUSTERED INDEX idxDriverRouteRate_Route ON dbo.tblDriverRouteRate
(
	RouteID
)
GO
CREATE NONCLUSTERED INDEX idxDriverRouteRate_TruckType ON dbo.tblDriverRouteRate
(
	TruckTypeID
)
GO
CREATE UNIQUE CLUSTERED INDEX udxDriverRouteRate_Main ON dbo.tblDriverRouteRate
(
	TruckTypeID,
	ShipperID,
	ProductGroupID,
	DriverGroupID,
	DriverID,
	RouteID,
	EffectiveDate
)
GO

ALTER TABLE tblOrderSettlementDriverAssessorialCharge DROP CONSTRAINT PK_OrderSettlementDriverOtherAssessorialCharge
GO
ALTER TABLE tblOrderSettlementDriverAssessorialCharge ADD CONSTRAINT PK_OrderSettlementDriverAssessorialCharge PRIMARY KEY NONCLUSTERED (ID)
GO
CREATE UNIQUE CLUSTERED INDEX [udxOrderSettlementDriverAssessorialCharge_Main] ON tblOrderSettlementDriverAssessorialCharge
(
	[OrderID] ASC,
	[AssessorialRateTypeID] ASC
)
GO

/**********************************/
-- Created: 2013/03/09 - ?.?.? - Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
-- Changes:
-- 4.1.0.2	- 2016.08.28 - KDA	- add some PRINT instrumentation statements for performance debugging purposes
/***********************************/
ALTER PROCEDURE spRetrieveOrdersFinancialDriver
(
  @StartDate date = NULL -- will default to first day of 3rd prior month for efficiency sake
, @EndDate date = NULL
, @ShipperID int = -1 -- all
, @CarrierID int = -1 -- all
, @ProductGroupID int = -1 -- all 
, @TruckTypeID int = -1 -- all
, @DriverGroupID int = -1 -- all
, @DriverID int = -1 -- all
, @OriginStateID int = -1 -- all 
, @DestStateID int = -1 -- all
, @ProducerID int = -1 -- all
, @BatchID int = NULL -- either show unbatched (NULL), or the specified batch orders
, @OnlyCarrierSettled bit = 0 
, @JobNumber varchar(20) = NULL -- 3.11.19
, @ContractNumber varchar(20) = NULL -- 3.11.19
, @StartSession bit = 0 -- when set to 1, assign the retrieved records to a new Session (ID), when 0 and a sessionID was assigned, then retrieve it
, @SessionID varchar(100) = NULL OUTPUT -- if @startSession = 1, then this return a new value, otherwise will retrieve the existing records in that session
) AS 
BEGIN
	PRINT 'Start=' + CONVERT(VARCHAR(24), GETDATE(), 121)

	IF (@StartSession IS NULL) SET @StartSession = CASE WHEN @SessionID IS NULL THEN 0 ELSE 1 END

	-- validate the incoming parameters
	IF (@BatchID IS NOT NULL
		AND (@ShipperID <> -1 
			OR @CarrierID <> -1 
			OR @ProductGroupID <> -1
			OR @TruckTypeID <> -1
			OR @DriverGroupID <> -1
			OR @DriverID <> -1
			OR @OriginStateID <> -1
			OR @DestStateID <> -1
			OR @ProducerID <> -1))
	BEGIN
		RAISERROR('@BatchID parameter cannot be specified with other parameters', 16, 1)
		RETURN
	END

	DECLARE @sql varchar(max) = '
		SELECT O.*
			, OS.RateApplySel, OS.BatchSel, OS.SessionID
		FROM viewOrder_Financial_Driver O
		LEFT JOIN tblOrderSettlementSelectionDriver OS ON OS.OrderID = O.ID'

	-- if a @BatchID value was specified then just use this criteria alone
    IF @BatchID IS NOT NULL
	BEGIN
		SET @sql = @sql + '
			WHERE O.BatchID = @BatchID'
		SET @sql = replace(@sql, '@BatchID', @BatchID)
	END
	ELSE IF (@StartSession = 0 AND @SessionID IS NOT NULL)
	BEGIN
		SET @sql = @sql + '
			WHERE O.ID IN (SELECT OrderID FROM tblOrderSettlementSelectionDriver WHERE SessionID = @SessionID)'
		SET @sql = replace(@sql, '@SessionID', @SessionID)
	END
	ELSE
	BEGIN
		SET @sql = @sql + '
			LEFT JOIN dbo.tblOrderTransfer OTR ON OTR.OrderID = O.ID
			LEFT JOIN dbo.viewDriverBase vODR ON vODR.ID = OTR.OriginDriverID
			LEFT JOIN dbo.viewDriverBase vDDR ON vDDR.ID = O.DriverID
			LEFT JOIN tblOrderSettlementCarrier OSP ON OSP.OrderID = O.ID AND OSP.BatchID IS NOT NULL
			WHERE O.StatusID IN (4)  
				AND O.DeleteDateUTC IS NULL
				AND O.BatchID IS NULL 
				AND (@ShipperID=-1 OR O.CustomerID=@ShipperID) 
				AND (@CarrierID=-1 OR O.CarrierID=@CarrierID) 
				AND (@ProductGroupID=-1 OR ProductGroupID=@ProductGroupID) 
				AND (@TruckTypeID=-1 OR TruckTypeID=@TruckTypeID)
				AND (@DriverGroupID=-1 OR ISNULL(vODR.DriverGroupID, vDDR.DriverGroupID) = @DriverGroupID) 
				AND (@DriverID=-1 OR ISNULL(vODR.ID, vDDR.ID) = @DriverID) 
				AND (@OriginStateID=-1 OR O.OriginStateID=@OriginStateID) 
				AND (@DestStateID=-1 OR O.DestStateID=@DestStateID) 
				AND (@ProducerID=-1 OR O.ProducerID=@ProducerID) 
				AND (@StartDate IS NULL OR O.OrderDate >= @StartDate) 
				AND (@EndDate IS NULL OR O.OrderDate <= @EndDate)
				AND (@OnlyCarrierSettled = 0 OR OSP.BatchID IS NOT NULL)
				AND (@JobNumber IS NULL OR O.JobNumber = isnull(@JobNumber, ''''))
				AND (@ContractNumber IS NULL OR O.ContractNumber = isnull(@ContractNumber, ''''))'
		SET @sql = replace(@sql, '@ShipperID', @ShipperID)
		SET @sql = replace(@sql, '@CarrierID', @CarrierID)
		SET @sql = replace(@sql, '@ProductGroupID', @ProductGroupID)
		SET @sql = replace(@sql, '@TruckTypeID', @TruckTypeID)
		SET @sql = replace(@sql, '@DriverGroupID', @DriverGroupID)
		SET @sql = replace(@sql, '@DriverID', @DriverID)
		SET @sql = replace(@sql, '@OriginStateID', @OriginStateID)
		SET @sql = replace(@sql, '@DestStateID', @DestStateID)
		SET @sql = replace(@sql, '@ProducerID', @ProducerID)
		SET @sql = replace(@sql, '@StartDate', dbo.fnQS(@StartDate))
		SET @sql = replace(@sql, '@EndDate', dbo.fnQS(@EndDate))
		SET @sql = replace(@sql, '@OnlyCarrierSettled', @OnlyCarrierSettled)
		SET @sql = replace(@sql, '@JobNumber', dbo.fnQS(nullif(rtrim(@JobNumber), '')))
		SET @sql = replace(@sql, '@ContractNumber', dbo.fnQS(nullif(rtrim(@ContractNumber), '')))
	END

	PRINT 'sql = ' + @sql
	
	BEGIN TRAN RetrieveOrdersFinancialDriver

	-- create the temp table to store the data
	SELECT *, RateApplySel = cast(1 as bit), BatchSel = cast(1 as bit), SessionID = cast(NULL AS varchar(100))
	INTO #ret 
	FROM viewOrder_Financial_Driver 
	WHERE 1 = 0
	
	-- get the data into the temp table
	PRINT 'Retrieve:Start=' + CONVERT(VARCHAR(24), GETDATE(), 121)
	INSERT INTO #ret EXEC (@sql)
	PRINT 'Retrieve:Finish=' + CONVERT(VARCHAR(24), GETDATE(), 121)

	-- do the SessionID logic
	IF (@StartSession = 1)
	BEGIN
		PRINT 'SessionCreate:Start=' + CONVERT(VARCHAR(24), GETDATE(), 121)
		DELETE FROM tblOrderSettlementSelectionDriver WHERE OrderID IN (SELECT ID FROM #ret) OR SessionID IN (SELECT SessionID FROM #ret)
		SELECT @sessionID = isnull(max(SessionID), 0) + 1 FROM tblOrderSettlementSelectionDriver
		UPDATE #ret SET SessionID = @sessionID, RateApplySel = 1, BatchSel = CASE WHEN HasError = 1 THEN 0 ELSE 1 END
		INSERT INTO tblOrderSettlementSelectionDriver (OrderID, SessionID, RateApplySel, BatchSel)
			SELECT ID, @sessionID, RateApplySel, BatchSel FROM #ret
		PRINT 'SessionCreate:Finish=' + CONVERT(VARCHAR(24), GETDATE(), 121)
	END
	
	PRINT 'Commit:Start=' + CONVERT(VARCHAR(24), GETDATE(), 121)
	COMMIT TRAN RetrieveOrdersFinancialDriver
	PRINT 'Commit:Finish=' + CONVERT(VARCHAR(24), GETDATE(), 121)

	-- return the data to the caller
	SELECT * FROM #ret

	PRINT 'Finish=' + CONVERT(VARCHAR(24), GETDATE(), 121)
END

GO

/***********************************/
-- Created: ?.?.? - 2014.12.23 - Kevin Alons
-- Purpose: compute and return the normalized "rate" + Amount for the specified order parameters
-- Changes:
-- 4.1.0.2 - 2016.08.28 - KDA	- update to work with "% of Carrier" rates
/***********************************/
ALTER FUNCTION fnRateToAmount
(
  @RateTypeID int
, @Units decimal(18, 10)
, @UomID int
, @Rate decimal(18, 10)
, @RateUomID int
, @LoadAmount money = NULL
, @ParentRate money = NULL
, @RouteMiles int = NULL
) RETURNS decimal(18, 10)
AS BEGIN
	DECLARE @ret money
	IF (@RateTypeID = 1) -- Per Unit rate type
		SET @ret = @Units * dbo.fnConvertRateUOM(@Rate, @RateUomID, @UomID)
	ELSE IF (@RateTypeID = 2) -- Flat
		SET @ret = @Rate
	ELSE IF (@RateTypeID IN (3, 6)) -- % of Customer (Shipper) or Carrier rate
		SET @ret = @Rate * @ParentRate / 100.0
	ELSE IF (@RateTypeID = 4) -- % of Load Amount
		SET @ret = @Rate * @LoadAmount / 100.0
	ELSE IF (@RateTypeID = 5) -- Per Mile rate
		SET @ret = @Rate * @RouteMiles
	RETURN (@ret)
END
GO

/*****************************************/
-- Created: ? - 2015.01.19 - Kevin Alons
-- Purpose: return the tblOrder table with a Local OrderDate field added
-- Changes:
-- ??		- 2015/05/17 - KDA - add local DeliverDate field
-- 3.8.11	- 2015/07/28 - KDA - remove OriginShipperRegion field
-- 3.9.2	- 2015/08/25 - KDA - remove computed OrderDate (it is now stored in the table directly)
-- 3.10.13	- 2015/02/29 - JAE - Add TruckType
/*****************************************/
ALTER VIEW viewOrderBase AS
	SELECT O.*
		, OriginStateID = OO.StateID
		, OriginRegionID = OO.RegionID
		, DestStateID = D.StateID
		, DestRegionID = D.RegionID
		, P.ProductGroupID
		, T.TruckTypeID
		, DR.DriverGroupID
		, DeliverDate = cast(dbo.fnUTC_to_Local(O.DestDepartTimeUTC, D.TimeZoneID, D.UseDST) as date) 
	FROM tblOrder O
	JOIN tblOrigin OO ON OO.ID = O.OriginID
	JOIN tblDestination D ON D.ID = O.DestinationID
	LEFT JOIN tblProduct P ON P.ID = O.ProductID
	LEFT JOIN tblTruck T ON T.ID = O.TruckID
	LEFT JOIN tblDriver DR ON DR.ID = O.DriverID

GO

/***************************************/
-- Date Created: ?.?.? - 2012.11.25 - Kevin Alons
-- Purpose: return Order records with "translated friendly" values for FK relationships
-- Changes:
-- 3.9.20  - 2015/10/22 - KDA	- add Origin|Dest DriverID fields (using new tblOrderTransfer table) 
--			2015/10/28  - JAE	- Added all Order Transfer fields for ease of use in reporting
--			2015/11/03  - BB		- added cast to make TransferComplet BIT type to avoid "Operand type clash" error when running this update script
-- 3.9.21  - 2015/11/03 - KDA	- add OriginDriverGroupID field (from OrderTransfer.OriginDriverID JOIN)
-- 3.9.25  - 2015/11/10 - JAE	- added origin driver and truck to view
-- 3.10.10 - 2016/02/15 - BB	- Add driver region ID to facilitate new user/region filtering feature
-- 3.10.11 - 2016/02/24 - JAE	- Add destination region (name) to view
-- 3.10.13 - 2016/02/29 - JAE	- Add TruckType
-- 3.13.8  - 2016/07/26	- BB	- Add Destination Station
-- 4.1.0.2 - 2016.08.28 - KDA	- rewrite RerouteCount, TicketCount to use a normal JOIN (instead of uncorrelated sub-query)
--								- use viewDriverBase instead of viewDriver
--								- eliminate viewGaugerBase (was unused), use tblOrderStatus instead of viewOrderPrintStatus
/***************************************/
ALTER VIEW viewOrder AS
SELECT O.*
	-- GENERATED orders that were GAUGED to COMPLETION or SKIPPED will show "Gauger [gauger status]"
	, PrintStatus = CASE WHEN O.StatusID = -10 AND GOS.IsComplete = 1 THEN 'Gauger ' + GOS.Name ELSE OPS.OrderStatus END
FROM (
	SELECT O.*
	, Origin = vO.Name 
	, OriginFull = vO.FullName 
	, OriginState = vO.State 
	, OriginStateAbbrev = vO.StateAbbrev 
	, OriginStation = vO.Station
	, OriginCounty = vO.County 
	, vO.LeaseName
	, vO.LeaseNum
	, OriginRegion = vO.Region
	, OriginCountryID = vO.CountryID
	, OriginCountry = vO.Country
	, OriginCountryShort = vO.CountryShort
	, OriginLegalDescription = vO.LegalDescription 
	, OriginNDIC = vO.NDICFileNum 
	, OriginNDM = vO.NDM 
	, OriginCA = vO.CA 
	, OriginTimeZoneID = vO.TimeZoneID 
	, OriginUseDST = vO.UseDST 
	, vO.H2S
	, Destination = vD.Name 
	, DestinationFull = vD.FullName 
	, DestinationState = vD.State 
	, DestinationStateAbbrev = vD.StateAbbrev 
	, DestinationTypeID = vD.ID
	, DestinationStation = vD.Station -- 3.13.8
	, vD.DestinationType
	, DestStation = vD.Station 
	, DestTimeZoneID = vD.TimeZoneID 
	, DestUseDST = vD.UseDST 
	, DestRegion = vD.Region -- 3.10.13
	, DestCountryID = vD.CountryID
	, DestCountry = vD.Country
	, DestCountryShort = vD.CountryShort
	, Customer = C.Name 
	, Carrier = CA.Name 
	, CarrierType = CT.Name 
	, OS.OrderStatus
	, OS.StatusNum
	, Driver = D.FullName 
	, DriverFirst = D.FirstName 
	, DriverLast = D.LastName 
	, DriverRegionID = D.RegionID  -- 3.10.10
	, Truck = TRU.FullName 
	, Trailer = TR1.FullName 
	, Trailer2 = TR2.FullName 
	, P.PriorityNum
	, TicketType = TT.Name 
	, DestTicketTypeID = vD.TicketTypeID 
	, DestTicketType = vD.TicketType 
	, Operator = OP.Name 
	, Producer = PR.Name 
	, Pumper = PU.FullName 
	, DriverNumber = D.IDNumber
	, CarrierNumber = CA.IDNumber
	, CarrierTypeID = CA.CarrierTypeID
	, TruckNumber = TRU.IDNumber
	, TruckType = TRU.TruckType 
	, TrailerNumber = TR1.IDNumber 
	, Trailer2Number = TR2.IDNumber 
	, Product = PRO.Name
	, ProductShort = PRO.ShortName 
	, PRO.ProductGroup
	, OriginUOM = OUom.Name 
	, OriginUomShort = OUom.Abbrev 
	, OriginTankID_Text = coalesce(ORT.TankNum, O.OriginTankNum, '?')
	, DestUOM = DUom.Name 
	, DestUomShort = DUom.Abbrev 
	, Active = CAST((CASE WHEN O.DeleteDateUTC IS NULL THEN 1 ELSE 0 END) AS BIT) 
	, IsDeleted = CAST((CASE WHEN O.DeleteDateUTC IS NOT NULL THEN 1 ELSE 0 END) AS BIT) 
	, PickupPrintStatus = PPS.Name 
	, PickupCompleted = PPS.IsCompleted 
	, DeliverPrintStatus = DPS.Name 
	, DeliverCompleted = DPS.IsCompleted 
	, PrintStatusID = CASE WHEN O.StatusID = -9 THEN ISNULL(-GAO.StatusID, O.StatusID)
						   WHEN O.StatusID = 8 AND PPS.IsCompleted = 0 THEN 7
						   WHEN O.StatusID = 3 AND DPS.IsCompleted = 0 THEN 8
						ELSE O.StatusID END 
	, OriginTankText = CASE WHEN O.OriginTankID IS NULL OR ORT.TankNum = '*' THEN O.OriginTankNum ELSE ORT.TankNum END
	, OriginWaitNum = OWR.Num
	, OriginWaitDesc = OWR.Description
	, OriginWaitNumDesc = OWR.NumDesc
	, DestWaitNum = DWR.Num
	, DestWaitDesc = DWR.Description
	, DestWaitNumDesc = DWR.NumDesc
	, RejectNum = ORR.Num
	, RejectDesc = ORR.Description
	, RejectNumDesc = ORR.NumDesc
	, ORE.RerouteCount
	, OT.TicketCount
	, TotalMinutes = ISNULL(OriginMinutes, 0) + ISNULL(DestMinutes, 0)
	-- TRANSFER FIELDS
	, OriginDriverID = ISNULL(OTR.OriginDriverID, O.DriverID)
	, OriginDriverGroupID = ISNULL(ODG.ID, DDG.ID) 
	, OriginDriverGroup = ISNULL(ODG.Name, DDG.Name)
	, OriginDriver = ISNULL(vODR.FullName, vDDR.FullName)
	, OriginDriverFirst = ISNULL(vODR.FirstName, vDDR.FirstName)
	, OriginDriverLast = ISNULL(vODR.LastName, vDDR.LastName)
	, OriginTruckID = ISNULL(OTR.OriginTruckID, O.TruckID)
	, OriginTruck = ISNULL(vOTRU.FullName, vDTRU.FullName)
	, DestDriverID = O.DriverID
	, DestDriver = D.FullName
	, DestDriverFirst = D.FirstName 
	, DestDriverLast = D.LastName 
	, DestTruckID = O.TruckID
	, DestTruck = TRU.FullName 
	, OriginTruckEndMileage = OTR.OriginTruckEndMileage
	, DestTruckStartMileage = OTR.DestTruckStartMileage
	, TransferPercent = OTR.PercentComplete
	, TransferNotes = OTR.Notes
	, TransferDateUTC = OTR.CreateDateUTC
	, TransferComplete = CAST(OTR.TransferComplete AS BIT)
	, IsTransfer = CAST((CASE WHEN OTR.OrderID IS NOT NULL THEN 1 ELSE 0 END) AS BIT)	
	-- 
	, GaugerStatusID = GAO.StatusID
	, GaugerID = GAO.GaugerID	
/* 
*/
	FROM dbo.viewOrderBase O
	JOIN dbo.tblOrderStatus OS ON OS.ID = O.StatusID
	LEFT JOIN (SELECT OrderID, TicketCount = COUNT(1) FROM tblOrderTicket OT WHERE OT.DeleteDateUTC IS NULL GROUP BY OrderID) OT ON OT.OrderID = O.ID
	LEFT JOIN (SELECT OrderID, RerouteCount = COUNT(1) FROM tblOrderReroute GROUP BY OrderID) ORE ON ORE.OrderID = O.ID
	LEFT JOIN dbo.tblOrderTransfer OTR ON OTR.OrderID = O.ID
	LEFT JOIN dbo.viewOrigin vO ON vO.ID = O.OriginID
	LEFT JOIN dbo.viewDestination vD ON vD.ID = O.DestinationID
	LEFT JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
	LEFT JOIN dbo.tblCarrier CA ON CA.ID = O.CarrierID
	LEFT JOIN dbo.tblCarrierType CT ON CT.ID = CA.CarrierTypeID
	LEFT JOIN dbo.viewDriverBase D ON D.ID = O.DriverID
	LEFT JOIN dbo.viewDriverBase vODR ON vODR.ID = OTR.OriginDriverID
	LEFT JOIN dbo.viewDriver vDDR ON vDDR.ID = O.DriverID
	LEFT JOIN dbo.tblDriverGroup ODG ON ODG.ID = vODR.DriverGroupID
	LEFT JOIN dbo.tblDriverGroup DDG ON DDG.ID = O.DriverGroupID
	LEFT JOIN dbo.viewTruck TRU ON TRU.ID = O.TruckID
	LEFT JOIN dbo.viewTruck vOTRU ON vOTRU.ID = OTR.OriginTruckID
	LEFT JOIN dbo.viewTruck vDTRU ON vDTRU.ID = O.TruckID
	LEFT JOIN dbo.viewTrailer TR1 ON TR1.ID = O.TrailerID
	LEFT JOIN dbo.viewTrailer TR2 ON TR2.ID = O.Trailer2ID
	LEFT JOIN dbo.tblPriority P ON P.ID = O.PriorityID
	LEFT JOIN dbo.tblTicketType TT ON TT.ID = O.TicketTypeID
	LEFT JOIN dbo.tblOriginTank ORT ON ORT.ID = O.OriginTankID
	LEFT JOIN dbo.tblOperator OP ON OP.ID = O.OperatorID
	LEFT JOIN dbo.viewPumper PU ON PU.ID = O.PumperID
	LEFT JOIN dbo.tblProducer PR ON PR.ID = O.ProducerID
	LEFT JOIN dbo.viewProduct PRO ON PRO.ID = O.ProductID
	LEFT JOIN dbo.tblUom OUom ON OUom.ID = O.OriginUomID
	LEFT JOIN dbo.tblUom DUom ON DUom.ID = O.DestUomID
	LEFT JOIN dbo.tblPrintStatus PPS ON PPS.ID = O.PickupPrintStatusID
	LEFT JOIN dbo.tblPrintStatus DPS ON DPS.ID = O.DeliverPrintStatusID
	LEFT JOIN dbo.viewOriginWaitReason OWR ON OWR.ID = O.OriginWaitReasonID
	LEFT JOIN dbo.viewDestinationWaitReason DWR ON DWR.ID = O.DestWaitReasonID
	LEFT JOIN dbo.viewOrderRejectReason ORR ON ORR.ID = O.RejectReasonID	
	LEFT JOIN dbo.tblGaugerOrder GAO ON GAO.OrderID = O.ID
) O
LEFT JOIN dbo.tblOrderStatus OPS ON OPS.ID = O.PrintStatusID
LEFT JOIN dbo.tblGaugerOrderStatus GOS ON GOS.ID = O.GaugerStatusID

GO

exec _spRefreshAllViews
exec _spRefreshAllViews
go

/***********************************/
-- Created: 4.1.0 - 2016/08/15 - Kevin Alons
-- Purpose: return BASE Order details + Carrier FINANCIAL INFO into a single view
-- Changes:
--	4.1.0.2 - 2016.08.28 - KDA	- remove redundant calculation of ORR.RerouteCount
--								- remove redundant tblOrigin JOIN (for H2S which is already available in viewOrder)
/***********************************/
ALTER VIEW viewOrder_Financial_Base_Carrier AS 
	SELECT *
		, HasError = cast(CASE WHEN ErrorFieldCSV IS NOT NULL THEN 1 ELSE 0 END as bit)
	FROM (
		SELECT *
			, OrderID = ID
			, ErrorFieldCSV = nullif(substring(CASE WHEN Approved = 0 THEN ',Approved' ELSE '' END
				+ CASE WHEN InvoiceLoadAmount IS NULL THEN ',InvoiceLoadAmount,InvoiceRouteRate,InvoiceRateSheet' ELSE '' END
				+ CASE WHEN FinalChainup = 1 AND InvoiceChainupAmount IS NULL THEN ',InvoiceChainupAmount' ELSE '' END
				+ CASE WHEN FinalRerouteCount > 0 AND InvoiceRerouteAmount IS NULL THEN ',InvoiceRerouteAmount' ELSE '' END
				+ CASE WHEN FinalH2S = 1 AND InvoiceH2SAmount IS NULL THEN ',InvoiceH2SAmount' ELSE '' END
				+ CASE WHEN TicketCount > 1 AND InvoiceSplitLoadAmount IS NULL THEN ',InvoiceSplitLoadAmount' ELSE '' END
				+ CASE WHEN InvoiceOriginWaitBillableMinutes > 0 AND InvoiceOriginWaitAmount IS NULL THEN ',InvoiceOriginWaitAmount' ELSE '' END
				+ CASE WHEN InvoiceDestinationWaitBillableMinutes > 0 AND InvoiceDestinationWaitAmount IS NULL THEN ',InvoiceDestinationWaitAmount' ELSE '' END
				+ CASE WHEN Rejected = 1 AND InvoiceOrderRejectAmount IS NULL THEN ',InvoiceOrderRejectAmount' ELSE '' END
				+ CASE WHEN InvoiceMinSettlementUnitsID IS NULL THEN ',InvoiceMinSettlementUnits' ELSE '' END
				+ CASE WHEN InvoiceSettlementFactorID IS NULL THEN ',InvoiceSettlementFactor' ELSE '' END
				+ CASE WHEN Rejected = 0 AND InvoiceSettlementFactorID IS NULL THEN ',InvoiceSettlementFactor' ELSE '' END
				+ CASE WHEN Rejected = 0 AND InvoiceSettlementFactorID IS NOT NULL AND dbo.fnOrderSettlementUnits(InvoiceSettlementFactorID, OriginGrossUnits, OriginNetUnits, OriginGrossStdUnits, DestGrossUnits, DestNetUnits) IS NULL THEN ','+OrderUnitsField ELSE '' END
				+ CASE WHEN InvoiceWaitFeeParameterID IS NULL THEN ',InvoiceOriginWaitBillableMinutes,InvoiceDestinationWaitBillableMinutes' ELSE '' END
				+ CASE WHEN isnull(ActualMiles, 0) = 0 THEN ',ActualMiles' ELSE '' END, 2, 100000), '')
		FROM (
			SELECT O.* 
				, InvoiceRatesAppliedDateUTC = OS.CreateDateUTC
				, OS.BatchID
				, Approved = cast(ISNULL(OA.Approved, 0) as bit)
				, FinalActualMiles = ISNULL(OA.OverrideActualMiles, O.ActualMiles)
				, FinalOriginMinutes = ISNULL(OA.OverrideOriginMinutes, O.OriginMinutes)
				, FinalDestMinutes = ISNULL(OA.OverrideDestMinutes, O.DestMinutes)
				, FinalH2S = cast(CASE WHEN isnull(OA.OverrideH2S, 0) = 1 THEN 0 ELSE O.H2S END as bit)
				, FinalChainup = cast(CASE WHEN isnull(OA.OverrideChainup, 0) = 1 THEN 0 ELSE O.Chainup END as bit)
				, FinalRerouteCount = CASE WHEN isnull(OA.OverrideReroute, 0) = 1 THEN 0 ELSE isnull(O.RerouteCount, 0) END
				, FC.OrderUnitsField
				, InvoiceBatchNum = OS.BatchNum 
				, InvoiceOriginWaitBillableMinutes = OS.OriginWaitBillableMinutes
				, InvoiceDestinationWaitBillableMinutes = OS.DestinationWaitBillableMinutes
				, InvoiceTotalWaitBillableMinutes = isnull(OS.OriginWaitBillableMinutes, 0) + ISNULL(OS.DestinationWaitBillableMinutes, 0) 
				, InvoiceWaitFeeParameterID = WaitFeeParameterID
				, InvoiceWaitFeeSubUnit = WaitFeeSubUnit
				, InvoiceWaitFeeRoundingType = WaitFeeRoundingType
				, InvoiceOriginWaitRate = OS.OriginWaitRate 
				, InvoiceOriginWaitAmount = OS.OriginWaitAmount 		
				, InvoiceDestinationWaitRate = OS.DestinationWaitRate 
				, InvoiceDestinationWaitAmount = OS.DestinationWaitAmount 
				, InvoiceTotalWaitAmount = OS.TotalWaitAmount
				, InvoiceOrderRejectRate = OS.OrderRejectRate	
				, InvoiceOrderRejectRateType = OS.OrderRejectRateType  
				, InvoiceOrderRejectAmount = OS.OrderRejectAmount  
				, InvoiceChainupRate = OS.ChainupRate
				, InvoiceChainupRateType = OS.ChainupRateType
				, InvoiceChainupAmount = OS.ChainupAmount 
				, InvoiceRerouteRate = OS.RerouteRate
				, InvoiceRerouteRateType = OS.RerouteRateType
				, InvoiceRerouteAmount = OS.RerouteAmount 
				, InvoiceH2SRate = OS.H2SRate
				, InvoiceH2SRateType = OS.H2SRateType
				, InvoiceH2SAmount = OS.H2SAmount
				, InvoiceSplitLoadRate = OS.SplitLoadRate
				, InvoiceSplitLoadRateType = OS.SplitLoadRateType
				, InvoiceSplitLoadAmount = OS.SplitLoadAmount
				, InvoiceOtherAmount = OS.OtherAmount
				, InvoiceTaxRate = OS.OriginTaxRate
				, InvoiceSettlementUom = OS.SettlementUom 
				, InvoiceSettlementUomShort = OS.SettlementUomShort 
				, InvoiceCarrierSettlementFactorID = OS.CarrierSettlementFactorID 
				, InvoiceSettlementFactorID = OS.SettlementFactorID 
				, InvoiceSettlementFactor = OS.SettlementFactor 
				, InvoiceMinSettlementUnitsID = OS.MinSettlementUnitsID 
				, InvoiceMinSettlementUnits = OS.MinSettlementUnits
				, InvoiceUnits = OS.SettlementUnits
				, InvoiceRouteRate = OS.RouteRate
				, InvoiceRouteRateType = OS.RouteRateType
				, InvoiceRateSheetRate = OS.RateSheetRate
				, InvoiceRateSheetRateType = OS.RateSheetRateType
				, InvoiceFuelSurchargeRate = OS.FuelSurchargeRate
				, InvoiceFuelSurchargeAmount = OS.FuelSurchargeAmount
				, InvoiceLoadAmount = OS.LoadAmount
				, InvoiceTotalAmount = OS.TotalAmount
			FROM dbo.viewOrder O
			LEFT JOIN viewOrderSettlementCarrier OS ON OS.OrderID = O.ID
			LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
			LEFT JOIN tblSettlementFactor FC ON FC.ID = OS.SettlementFactorID
			WHERE O.StatusID NOT IN (-10) -- don't include "Generated" orders (only Assigned+)
		) X
	) X1

GO

/***********************************/
-- Created: 4.1.0 - 2016/08/15 - Kevin Alons
-- Purpose: return BASE Order details + Driver FINANCIAL INFO into a single view
-- Changes:
--	4.1.0.2 - 2016.08.28 - KDA	- remove redundant calculation of ORR.RerouteCount
--								- remove redundant tblOrigin JOIN (for H2S which is already available in viewOrder)
/***********************************/
ALTER VIEW viewOrder_Financial_Base_Driver AS 
	SELECT *
		, HasError = cast(CASE WHEN ErrorFieldCSV IS NOT NULL THEN 1 ELSE 0 END as bit)
	FROM (
		SELECT *
			, OrderID = ID
			, ErrorFieldCSV = nullif(substring(CASE WHEN Approved = 0 THEN ',Approved' ELSE '' END
				+ CASE WHEN InvoiceLoadAmount IS NULL THEN ',InvoiceLoadAmount,InvoiceRouteRate,InvoiceRateSheet' ELSE '' END
				+ CASE WHEN FinalChainup = 1 AND InvoiceChainupAmount IS NULL THEN ',InvoiceChainupAmount' ELSE '' END
				+ CASE WHEN FinalRerouteCount > 0 AND InvoiceRerouteAmount IS NULL THEN ',InvoiceRerouteAmount' ELSE '' END
				+ CASE WHEN FinalH2S = 1 AND InvoiceH2SAmount IS NULL THEN ',InvoiceH2SAmount' ELSE '' END
				+ CASE WHEN TicketCount > 1 AND InvoiceSplitLoadAmount IS NULL THEN ',InvoiceSplitLoadAmount' ELSE '' END
				+ CASE WHEN InvoiceOriginWaitBillableMinutes > 0 AND InvoiceOriginWaitAmount IS NULL THEN ',InvoiceOriginWaitAmount' ELSE '' END
				+ CASE WHEN InvoiceDestinationWaitBillableMinutes > 0 AND InvoiceDestinationWaitAmount IS NULL THEN ',InvoiceDestinationWaitAmount' ELSE '' END
				+ CASE WHEN Rejected = 1 AND InvoiceOrderRejectAmount IS NULL THEN ',InvoiceOrderRejectAmount' ELSE '' END
				+ CASE WHEN InvoiceMinSettlementUnitsID IS NULL THEN ',InvoiceMinSettlementUnits' ELSE '' END
				+ CASE WHEN InvoiceSettlementFactorID IS NULL THEN ',InvoiceSettlementFactor' ELSE '' END
				+ CASE WHEN Rejected = 0 AND InvoiceSettlementFactorID IS NULL THEN ',InvoiceSettlementFactor' ELSE '' END
				+ CASE WHEN Rejected = 0 AND InvoiceSettlementFactorID IS NOT NULL AND dbo.fnOrderSettlementUnits(InvoiceSettlementFactorID, OriginGrossUnits, OriginNetUnits, OriginGrossStdUnits, DestGrossUnits, DestNetUnits) IS NULL THEN ','+OrderUnitsField ELSE '' END
				+ CASE WHEN InvoiceWaitFeeParameterID IS NULL THEN ',InvoiceOriginWaitBillableMinutes,InvoiceDestinationWaitBillableMinutes' ELSE '' END
				+ CASE WHEN isnull(ActualMiles, 0) = 0 THEN ',ActualMiles' ELSE '' END, 2, 100000), '')
		FROM (
			SELECT O.* 
				, DriverGroup = DG.Name
				, InvoiceRatesAppliedDateUTC = OS.CreateDateUTC
				, OS.BatchID
				, Approved = cast(ISNULL(OA.Approved, 0) as bit)
				, FinalActualMiles = ISNULL(OA.OverrideActualMiles, O.ActualMiles)
				, FinalOriginMinutes = ISNULL(OA.OverrideOriginMinutes, O.OriginMinutes)
				, FinalDestMinutes = ISNULL(OA.OverrideDestMinutes, O.DestMinutes)
				, FinalH2S = cast(CASE WHEN isnull(OA.OverrideH2S, 0) = 1 THEN 0 ELSE O.H2S END as bit)
				, FinalChainup = cast(CASE WHEN isnull(OA.OverrideChainup, 0) = 1 THEN 0 ELSE O.Chainup END as bit)
				, FinalRerouteCount = CASE WHEN isnull(OA.OverrideReroute, 0) = 1 THEN 0 ELSE isnull(O.RerouteCount, 0) END
				, FC.OrderUnitsField
				, InvoiceBatchNum = OS.BatchNum 
				, InvoiceOriginWaitBillableMinutes = OS.OriginWaitBillableMinutes
				, InvoiceDestinationWaitBillableMinutes = OS.DestinationWaitBillableMinutes
				, InvoiceTotalWaitBillableMinutes = isnull(OS.OriginWaitBillableMinutes, 0) + ISNULL(OS.DestinationWaitBillableMinutes, 0) 
				, InvoiceWaitFeeParameterID = WaitFeeParameterID
				, InvoiceWaitFeeSubUnit = WaitFeeSubUnit
				, InvoiceWaitFeeRoundingType = WaitFeeRoundingType
				, InvoiceOriginWaitRate = OS.OriginWaitRate 
				, InvoiceOriginWaitAmount = OS.OriginWaitAmount 		
				, InvoiceDestinationWaitRate = OS.DestinationWaitRate 
				, InvoiceDestinationWaitAmount = OS.DestinationWaitAmount 
				, InvoiceTotalWaitAmount = OS.TotalWaitAmount
				, InvoiceOrderRejectRate = OS.OrderRejectRate	
				, InvoiceOrderRejectRateType = OS.OrderRejectRateType  
				, InvoiceOrderRejectAmount = OS.OrderRejectAmount  
				, InvoiceChainupRate = OS.ChainupRate
				, InvoiceChainupRateType = OS.ChainupRateType
				, InvoiceChainupAmount = OS.ChainupAmount 
				, InvoiceRerouteRate = OS.RerouteRate
				, InvoiceRerouteRateType = OS.RerouteRateType
				, InvoiceRerouteAmount = OS.RerouteAmount 
				, InvoiceH2SRate = OS.H2SRate
				, InvoiceH2SRateType = OS.H2SRateType
				, InvoiceH2SAmount = OS.H2SAmount
				, InvoiceSplitLoadRate = OS.SplitLoadRate
				, InvoiceSplitLoadRateType = OS.SplitLoadRateType
				, InvoiceSplitLoadAmount = OS.SplitLoadAmount
				, InvoiceOtherAmount = OS.OtherAmount
				, InvoiceTaxRate = OS.OriginTaxRate
				, InvoiceSettlementUom = OS.SettlementUom 
				, InvoiceSettlementUomShort = OS.SettlementUomShort 
				, InvoiceCarrierSettlementFactorID = OS.CarrierSettlementFactorID 
				, InvoiceSettlementFactorID = OS.SettlementFactorID 
				, InvoiceSettlementFactor = OS.SettlementFactor 
				, InvoiceMinSettlementUnitsID = OS.MinSettlementUnitsID 
				, InvoiceMinSettlementUnits = OS.MinSettlementUnits
				, InvoiceUnits = OS.SettlementUnits
				, InvoiceRouteRate = OS.RouteRate
				, InvoiceRouteRateType = OS.RouteRateType
				, InvoiceRateSheetRate = OS.RateSheetRate
				, InvoiceRateSheetRateType = OS.RateSheetRateType
				, InvoiceFuelSurchargeRate = OS.FuelSurchargeRate
				, InvoiceFuelSurchargeAmount = OS.FuelSurchargeAmount
				, InvoiceLoadAmount = OS.LoadAmount
				, InvoiceTotalAmount = OS.TotalAmount
			FROM dbo.viewOrder O
			LEFT JOIN tblDriverGroup DG ON DG.ID = O.DriverGroupID
			LEFT JOIN viewOrderSettlementDriver OS ON OS.OrderID = O.ID
			LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
			LEFT JOIN tblSettlementFactor FC ON FC.ID = OS.SettlementFactorID
			WHERE O.StatusID NOT IN (-10) -- don't include "Generated" orders (only Assigned+)
		) X
	) X1

GO

/***********************************/
-- Created: 4.1.0 - 2016/08/15 - Kevin Alons
-- Purpose: return BASE Order details + Shipper FINANCIAL INFO into a single view
-- Changes:
--	4.1.0.2 - 2016.08.28 - KDA	- remove redundant calculation of ORR.RerouteCount
--								- remove redundant tblOrigin JOIN (for H2S which is already available in viewOrder)
/***********************************/
ALTER VIEW [dbo].[viewOrder_Financial_Base_Shipper] AS 
	SELECT *
		, HasError = cast(CASE WHEN ErrorFieldCSV IS NOT NULL THEN 1 ELSE 0 END as bit)
	FROM (
		SELECT *
			, OrderID = ID
			, ErrorFieldCSV = nullif(substring(CASE WHEN Approved = 0 THEN ',Approved' ELSE '' END
				+ CASE WHEN InvoiceLoadAmount IS NULL THEN ',InvoiceLoadAmount,InvoiceRouteRate,InvoiceRateSheet' ELSE '' END
				+ CASE WHEN FinalChainup = 1 AND InvoiceChainupAmount IS NULL THEN ',InvoiceChainupAmount' ELSE '' END
				+ CASE WHEN FinalRerouteCount > 0 AND InvoiceRerouteAmount IS NULL THEN ',InvoiceRerouteAmount' ELSE '' END
				+ CASE WHEN FinalH2S = 1 AND InvoiceH2SAmount IS NULL THEN ',InvoiceH2SAmount' ELSE '' END
				+ CASE WHEN TicketCount > 1 AND InvoiceSplitLoadAmount IS NULL THEN ',InvoiceSplitLoadAmount' ELSE '' END
				+ CASE WHEN InvoiceOriginWaitBillableMinutes > 0 AND InvoiceOriginWaitAmount IS NULL THEN ',InvoiceOriginWaitAmount' ELSE '' END
				+ CASE WHEN InvoiceDestinationWaitBillableMinutes > 0 AND InvoiceDestinationWaitAmount IS NULL THEN ',InvoiceDestinationWaitAmount' ELSE '' END
				+ CASE WHEN Rejected = 1 AND InvoiceOrderRejectAmount IS NULL THEN ',InvoiceOrderRejectAmount' ELSE '' END
				+ CASE WHEN InvoiceMinSettlementUnitsID IS NULL THEN ',InvoiceMinSettlementUnits' ELSE '' END
				+ CASE WHEN InvoiceSettlementFactorID IS NULL THEN ',InvoiceSettlementFactor' ELSE '' END
				+ CASE WHEN Rejected = 0 AND InvoiceSettlementFactorID IS NULL THEN ',InvoiceSettlementFactor' ELSE '' END
				+ CASE WHEN Rejected = 0 AND InvoiceSettlementFactorID IS NOT NULL AND dbo.fnOrderSettlementUnits(InvoiceSettlementFactorID, OriginGrossUnits, OriginNetUnits, OriginGrossStdUnits, DestGrossUnits, DestNetUnits) IS NULL THEN ','+OrderUnitsField ELSE '' END
				+ CASE WHEN InvoiceWaitFeeParameterID IS NULL THEN ',InvoiceOriginWaitBillableMinutes,InvoiceDestinationWaitBillableMinutes' ELSE '' END
				+ CASE WHEN isnull(ActualMiles, 0) = 0 THEN ',ActualMiles' ELSE '' END, 2, 100000), '')
		FROM (
			SELECT O.* 
				, InvoiceRatesAppliedDateUTC = OS.CreateDateUTC
				, OS.BatchID
				, Approved = cast(ISNULL(OA.Approved, 0) as bit)
				, FinalActualMiles = ISNULL(OA.OverrideActualMiles, O.ActualMiles)
				, FinalOriginMinutes = ISNULL(OA.OverrideOriginMinutes, O.OriginMinutes)
				, FinalDestMinutes = ISNULL(OA.OverrideDestMinutes, O.DestMinutes)
				, FinalH2S = cast(CASE WHEN isnull(OA.OverrideH2S, 0) = 1 THEN 0 ELSE O.H2S END as bit)
				, FinalChainup = cast(CASE WHEN isnull(OA.OverrideChainup, 0) = 1 THEN 0 ELSE O.Chainup END as bit)
				, FinalRerouteCount = CASE WHEN isnull(OA.OverrideReroute, 0) = 1 THEN 0 ELSE isnull(O.RerouteCount, 0) END
				, FC.OrderUnitsField
				, InvoiceBatchNum = OS.BatchNum 
				, InvoiceOriginWaitBillableMinutes = OS.OriginWaitBillableMinutes
				, InvoiceDestinationWaitBillableMinutes = OS.DestinationWaitBillableMinutes
				, InvoiceTotalWaitBillableMinutes = isnull(OS.OriginWaitBillableMinutes, 0) + ISNULL(OS.DestinationWaitBillableMinutes, 0) 
				, InvoiceWaitFeeParameterID = WaitFeeParameterID
				, InvoiceWaitFeeSubUnit = WaitFeeSubUnit
				, InvoiceWaitFeeRoundingType = WaitFeeRoundingType
				, InvoiceOriginWaitRate = OS.OriginWaitRate 
				, InvoiceOriginWaitAmount = OS.OriginWaitAmount 		
				, InvoiceDestinationWaitRate = OS.DestinationWaitRate 
				, InvoiceDestinationWaitAmount = OS.DestinationWaitAmount 
				, InvoiceTotalWaitAmount = OS.TotalWaitAmount
				, InvoiceOrderRejectRate = OS.OrderRejectRate	
				, InvoiceOrderRejectRateType = OS.OrderRejectRateType  
				, InvoiceOrderRejectAmount = OS.OrderRejectAmount  
				, InvoiceChainupRate = OS.ChainupRate
				, InvoiceChainupRateType = OS.ChainupRateType
				, InvoiceChainupAmount = OS.ChainupAmount 
				, InvoiceRerouteRate = OS.RerouteRate
				, InvoiceRerouteRateType = OS.RerouteRateType
				, InvoiceRerouteAmount = OS.RerouteAmount 
				, InvoiceH2SRate = OS.H2SRate
				, InvoiceH2SRateType = OS.H2SRateType
				, InvoiceH2SAmount = OS.H2SAmount
				, InvoiceSplitLoadRate = OS.SplitLoadRate
				, InvoiceSplitLoadRateType = OS.SplitLoadRateType
				, InvoiceSplitLoadAmount = OS.SplitLoadAmount
				, InvoiceOtherAmount = OS.OtherAmount
				, InvoiceTaxRate = OS.OriginTaxRate
				, InvoiceSettlementUom = OS.SettlementUom 
				, InvoiceSettlementUomShort = OS.SettlementUomShort 
				, InvoiceShipperSettlementFactorID = OS.ShipperSettlementFactorID 
				, InvoiceSettlementFactorID = OS.SettlementFactorID 
				, InvoiceSettlementFactor = OS.SettlementFactor 
				, InvoiceMinSettlementUnitsID = OS.MinSettlementUnitsID 
				, InvoiceMinSettlementUnits = OS.MinSettlementUnits
				, InvoiceUnits = OS.SettlementUnits
				, InvoiceRouteRate = OS.RouteRate
				, InvoiceRouteRateType = OS.RouteRateType
				, InvoiceRateSheetRate = OS.RateSheetRate
				, InvoiceRateSheetRateType = OS.RateSheetRateType
				, InvoiceFuelSurchargeRate = OS.FuelSurchargeRate
				, InvoiceFuelSurchargeAmount = OS.FuelSurchargeAmount
				, InvoiceLoadAmount = OS.LoadAmount
				, InvoiceTotalAmount = OS.TotalAmount
			FROM dbo.viewOrder O
			LEFT JOIN viewOrderSettlementShipper OS ON OS.OrderID = O.ID
			LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
			LEFT JOIN tblSettlementFactor FC ON FC.ID = OS.SettlementFactorID
			WHERE O.StatusID NOT IN (-10) -- don't include "Generated" orders (only Assigned+)
		) X
	) X1

GO

exec _spRefreshAllViews
go

COMMIT
SET NOEXEC OFF