-- rollback
set noexec off
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.2.5'
SELECT  @NewVersion = '4.3.0'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'DCWEB-1665 - Driver Scheduling rewrite for flexibility and performance improvements'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

create table tblDriverScheduleStatus 
(
  ID smallint not null identity(1000, 1) constraint PK_DriverScheduleStatus primary key
, Name varchar(25) not null constraint UQ_DriverScheduleStatus_Name unique
, Abbrev varchar(3) not null constraint UQ_DriverScheduleStatus_Abbrev unique
, OnDuty bit not null constraint DF_DriverScheduleStatus_OnDuty default (0)
, Utilized bit not null constraint DF_DriverScheduleStatus_Utilized default (1)
, CreateDateUTC datetime not null constraint DF_DriverScheduleStatus_CreateDateUTC default (getdate())
, CreatedByUser varchar(100) not null constraint DF_DriverScheduleStatus_CreatedByUser default ('System')
, LastChangeDateUTC datetime null 
, LastChangedByUser varchar(100)
, DeleteDateUTC datetime null
, DeletedByUser varchar(100) null
)
go

/*********************************/
-- Created: 4.3.0 - 2016.10.24 - Kevin Alons
-- Purpose: prevent deletion of "System" Driver Schedule Status records
-- Changes:
/*********************************/
CREATE TRIGGER trigDriverScheduleStatus_D ON tblDriverScheduleStatus FOR DELETE AS
BEGIN
	IF EXISTS (SELECT * FROM deleted WHERE id < 1000)
	BEGIN
		RAISERROR('System Driver Schedule Statuses cannot be deleted', 16, 1)
	END
END
go

set identity_insert tblDriverScheduleStatus ON
insert into tblDriverScheduleStatus (ID, Name, Abbrev, OnDuty, Utilized)
	select 1, 'Undefined', '??', 0, 0
	union
	select 1000, 'On Duty', 'OD', 1, 1
	union
	select 1001, 'Not Available', 'NA', 0, 0
	union 
	select 1002, 'Open', 'O', 0, 0
go
set identity_insert tblDriverScheduleStatus OFF

create table tblDriverShiftType
(
  ID smallint identity(1, 1) not null constraint PK_DriverShiftType primary key
, Name varchar(25) not null constraint UQ_DriverShiftType_Name unique
, CreateDateUTC datetime not null constraint DF_DriverShiftType_CreateDateUTC default (getdate())
, CreatedByUser varchar(100) not null constraint DF_DriverShiftType_CreatedByUser default ('System')
, LastChangeDateUTC datetime null 
, LastChangedByUser varchar(100)
, DeleteDateUTC datetime null
, DeletedByUser varchar(100) null
)
go

create table tblDriverShiftTypeDay
(
  ID int identity(1, 1) not null constraint PK_DriverShiftTypeDay primary key
, DriverShiftTypeID smallint not null constraint FK_DriverShiftTypeDay_DriverShiftType foreign key references tblDriverShiftType(ID) on delete cascade
, Position tinyint not null
, StatusID smallint not null constraint FK_DriverShiftTypeDay_Status foreign key references tblDriverScheduleStatus(ID)
, StartHours tinyint null constraint CK_DriverShiftTypeDay_StartHours_Validate CHECK (StartHours IS NULL OR StartHours BETWEEN 0 AND 23)
, DurationHours tinyint constraint CK_DriverShiftTypeDay_DurationHours_Validate CHECK (DurationHours IS NULL OR DurationHours BETWEEN 0 AND 23)
, CreateDateUTC datetime not null constraint DF_DriverShiftTypeDay_CreateDateUTC default (getdate())
, CreatedByUser varchar(100) not null constraint DF_DriverShiftTypeDay_CreatedByUser default ('System')
)
go 
insert into tblDriverShiftType (name)
	select 'Weekly 8-5'
go
insert into tblDriverShiftTypeDay (DriverShiftTypeID, Position, StatusID, StartHours, DurationHours)
	select 1, 1, 1, null, null
	union
	select 1, 2, 1000, 8, 8
	union
	select 1, 3, 1000, 8, 8
	union
	select 1, 4, 1000, 8, 8
	union
	select 1, 5, 1000, 8, 8
	union
	select 1, 6, 1000, 8, 8
	union
	select 1, 7, 1, null, null
go

create table tblDriverScheduleAssignment
(
  ID int identity(1, 1) not null constraint PK_DriverScheduleAssignment primary key
, DriverID int not null constraint FK_DriverScheduleAssignment_Driver foreign key references tblDriver(ID)
	constraint UC_DriverScheduleAssignment_DriverID unique
, DriverShiftTypeID smallint null constraint FK_DriverScheduleAssignment_ShiftType foreign key references tblDriverShiftType(ID)
, DriverShiftStartDate date null
, CreateDateUTC datetime not null constraint DF_DriverScheduleAssignment_CreateDateUTC default (getdate())
, CreatedByUser varchar(100) not null constraint DF_DriverScheduleAssignment_CreatedByUser default ('System')
, LastChangedDateUTC datetime null
, LastChangedByUser varchar(100) null
)
go
 
create table tblDriverSchedule
(
  ID int identity(1, 1) not null constraint PK_DriverSchedule primary key
, DriverID int not null constraint FK_DriverSchedule_Driver foreign key references tblDriver(ID)
, ScheduleDate date not null
, StatusID smallint not null constraint FK_DriverSchedule_Status foreign key references tblDriverScheduleStatus(ID)
, StartHours tinyint null constraint CK_DriverSchedule_StartHours_Validate CHECK (StartHours IS NULL OR StartHours BETWEEN 0 AND 23)
, DurationHours tinyint constraint CK_DriverSchedule_DurationHours_Validate CHECK (DurationHours IS NULL OR DurationHours BETWEEN 0 AND 23)
, ManualAssignment bit not null constraint DF_DriverSchedule_ManualAssignment default (1)
, CreateDateUTC datetime not null constraint DF_DriverSchedule_CreateDateUTC default (getdate())
, CreatedByUser varchar(100) not null constraint DF_DriverSchedule_CreatedByUser default ('System')
, LastChangeDateUTC datetime null
, LastChangedByUser varchar(100) null
, constraint UC_DriverSchedule_Driver_ScheduleDate unique (DriverID, ScheduleDate)
)
go

alter table tblDriver add DriverShiftTypeID smallint null constraint FK_Driver_DriverShiftType foreign key references tblDriverShiftType(ID)
go
alter table tblDriver add DriverShiftStartDate date null 
go

/*******************************************/
-- Created: 4.3.0 - 2016.09.13 - Kevin Alons
-- Purpose: return DriverShiftTypeDay + JOIN fields
-- Changes:
/*******************************************/
CREATE VIEW viewDriverShiftTypeDay AS
	SELECT STD.*, ShiftDayDesc = SS.Abbrev + isnull('-' + format(cast(ltrim(STD.StartHours) + ':00' as datetime), 'h:mm tt') + '+' + ltrim(STD.DurationHours), '')
		, ShiftType = ST.Name, Status = SS.Name, StatusAbbrev = SS.Abbrev, SS.OnDuty, SS.Utilized
	FROM tblDriverShiftTypeDay STD
	JOIN tblDriverShiftType ST ON ST.ID = STD.DriverShiftTypeID
	JOIN tblDriverScheduleStatus SS ON SS.ID = STD.StatusID
GO

/*******************************************/
-- Created: 4.3.0 - 2016.09.13 - Kevin Alons
-- Purpose: return concatenated DriverShiftTypeDay records (for user-presentation)
-- Changes:
/*******************************************/
CREATE FUNCTION fnDriverShiftTypeDays(@ID smallint) RETURNS varchar(max) AS
BEGIN
	DECLARE @ret varchar(max)
	SELECT @ret = isnull(@ret + ' | ', '') + ShiftDayDesc
	FROM viewDriverShiftTypeDay STD
	ORDER BY Position

	RETURN @ret
END
GO

/*******************************************/
-- Created: 4.3.0 - 2016.09.13 - Kevin Alons
-- Purpose: return DriverShiftType + related JOIN data + user-viewable presentation of Day schedule
-- Changes:
/*******************************************/
CREATE VIEW viewDriverShiftType AS
	SELECT DST.*, ShiftDays = dbo.fnDriverShiftTypeDays(DST.ID)
	FROM tblDriverShiftType DST
GO

/***********************************************************/
-- Date Created: 4.3.0 - 2016.10.20 - Kevin Alons
-- Purpose: return all "potential" Driver Scheduling records for a date range and input criteria
-- Changes:
/***********************************************************/
CREATE FUNCTION fnDriverSchedule
(
  @StartDate date
, @EndDate date
, @RegionID int = -1
, @CarrierID int = -1
, @DriverGrouPID int = -1
) RETURNS TABLE AS RETURN
	-- generates a full list of records for the specified criteria (with NULL or not assigned as a default)
	WITH cteBASE AS (
		SELECT D.RegionID, D.Region, D.CarrierID, D.Carrier, D.DriverGroupID, DriverGroup = D.DriverGroupName
			, DriverID = D.ID, Driver = D.FullName, D.MobilePhone, DT.ScheduleDate
			, StatusID = 1 /* undefined */, StartHours = cast(null as tinyint), DurationHours = cast(null as tinyint)
			, Pos = ROW_NUMBER() OVER (PARTITION BY D.ID ORDER By DT.ScheduleDate)
		FROM viewDriverBase D
		CROSS JOIN (
			-- get a table with the entire specified date range (date only values)
			SELECT ScheduleDate = dateadd(day, n-1, @StartDate)
			FROM viewNumbers1000 
			WHERE n <= datediff(day, @StartDate, @EndDate) + 1
		) DT
		WHERE (@CarrierID = -1 OR D.CarrierID = @CarrierID)
		 AND (@RegionID = -1 OR D.RegionID = @RegionID)
		 AND D.Active = 1
	)

	SELECT B.RegionID, B.Region, B.CarrierID, B.Carrier, B.DriverGroupID, B.DriverGroup, B.DriverID, B.Driver, B.MobilePhone
		, DSA.DriverShiftTypeID, DSA.DriverShiftStartDate
		, StatusID = isnull(DS.StatusID, B.StatusID), Status = SS.Abbrev, Pos
		, StartHours = isnull(DS.StartHours, B.StartHours), DurationHours = isnull(DS.DurationHours, B.DurationHours)
		, B.ScheduleDate, DS.ManualAssignment
		, DS.CreateDateUTC, DS.CreatedByUser
	FROM cteBASE B
	LEFT JOIN tblDriverSchedule DS ON DS.DriverID = B.DriverID AND DS.ScheduleDate = B.ScheduleDate
	LEFT JOIN tblDriverScheduleAssignment DSA ON DSA.DriverID = B.DriverID
	LEFT JOIN tblDriverScheduleStatus SS ON SS.ID = isnull(DS.StatusID, B.StatusID)

GO
GRANT SELECT ON dbo.fnDriverSchedule to role_iis_acct
GO

exec _spDropProcedure 'spDriverSchedulePivotSource'
go
/***********************************************************/
-- Date Created: 4.3.0 - 2016.10.20 - Kevin Alons
-- Purpose: return all "potential" Driver Scheduling records for a date range and input criteria, in format for code-based PIVOTing
-- Changes:
/***********************************************************/
CREATE PROCEDURE spDriverSchedulePivotSource(@Start date, @End date, @RegionID int = -1, @CarrierID int = -1, @DriverGroupID int = -1) AS
BEGIN
	SELECT DriverID, Region, Carrier, DriverGroup, Driver, DriverShiftTypeID, DriverShiftStartDate
		, Heading = FORMAT(ScheduleDate, 'SDM_d_yy')
		, PivotValue = ltrim(Pos) + '|' + ltrim(StatusID) + '|' + Status + '|' + isnull(ltrim(StartHours), '') + '|' + isnull(ltrim(DurationHours), '') + '|' + isnull(ltrim(ManualAssignment), '0')
	FROM dbo.fnDriverSchedule(@Start, @End, @RegionID, @CarrierID, @DriverGroupID)
	ORDER BY Carrier, Driver, ScheduleDate;
END
GO
GRANT EXECUTE ON spDriverSchedulePivotSource TO role_iis_acct
GO

exec _spDropProcedure 'spUpdateDriverShiftStartDate'
go
/***********************************************************/
-- Date Created: 4.3.0 - 2016.10.25 - Kevin Alons
-- Purpose: set the DriverShiftStartDate assignment for the specified parameters
-- Changes:
/***********************************************************/
CREATE PROCEDURE spUpdateDriverShiftStartDate(@DriverID int, @ShiftStartDate date, @UserName varchar(100)) AS
BEGIN
	IF EXISTS (SELECT * FROM tblDriverScheduleAssignment WHERE DriverID = @DriverID)
		UPDATE tblDriverScheduleAssignment 
		SET DriverShiftStartDate = @ShiftStartDate, LastChangedDateUTC = getutcdate(), LastChangedByUser = @UserName
		WHERE DriverID = @DriverID
	ELSE
		INSERT INTO tblDriverScheduleAssignment(DriverID, DriverShiftTypeID, DriverShiftStartDate, CreatedByUser)
			VALUES (@DriverID, NULL, @ShiftStartDate, @UserName)
END
GO
GRANT EXECUTE ON spUpdateDriverShiftStartDate TO role_iis_acct
GO

exec _spDropProcedure 'spUpdateDriverShiftType'
go
/***********************************************************/
-- Date Created: 4.3.0 - 2016.10.25 - Kevin Alons
-- Purpose: set the DriverShiftTypeID assignment for the specified parameters
-- Changes:
/***********************************************************/
CREATE PROCEDURE spUpdateDriverShiftType(@DriverID int, @ShiftTypeID int, @UserName varchar(100)) AS
BEGIN
	IF EXISTS (SELECT * FROM tblDriverScheduleAssignment WHERE DriverID = @DriverID)
		UPDATE tblDriverScheduleAssignment 
		SET DriverShiftTypeID = @ShiftTypeID, LastChangedDateUTC = getutcdate(), LastChangedByUser = @UserName 
		WHERE DriverID = @DriverID
	ELSE
		INSERT INTO tblDriverScheduleAssignment(DriverID, DriverShiftTypeID, DriverShiftStartDate, CreatedByUser)
			VALUES (@DriverID, @ShiftTypeID, NULL, @UserName)
END
GO
GRANT EXECUTE ON spUpdateDriverShiftType TO role_iis_acct
GO

exec _spDropProcedure 'spUpdateDriverSchedule'
go
/***********************************************************/
-- Date Created: 4.3.0 - 2016.10.25 - Kevin Alons
-- Purpose: set the DriverSchedule data for the specified parameters
-- Changes:
/***********************************************************/
CREATE PROCEDURE spUpdateDriverSchedule
(
  @DriverID int
, @ScheduleDate date
, @StatusID smallint
, @StartHours tinyint
, @DurationHours tinyint
, @ManualAssignment bit = 1
, @UserName varchar(100)
) AS 
BEGIN
	IF EXISTS(SELECT * FROM tblDriverSchedule WHERE DriverID = @DriverID AND ScheduleDate = @ScheduleDate)
		UPDATE tblDriverSchedule 
		SET StatusID = @StatusID, StartHours = @StartHours, DurationHours = @DurationHours, ManualAssignment = @ManualAssignment
			, LastChangeDateUTC = GETUTCDATE(), LastChangedByUser = @UserName
		WHERE DriverID = @DriverID AND ScheduleDate = @ScheduleDate
	ELSE
		INSERT INTO tblDriverSchedule (DriverID, ScheduleDate, StatusID, StartHours, DurationHours, ManualAssignment, CreatedByUser)
			VALUES (@DriverID, @ScheduleDate, @StatusID, @StartHours, @DurationHours, @ManualAssignment, @UserName)
END
GO
GRANT EXECUTE ON spUpdateDriverSchedule TO role_iis_acct
GO

exec _spDropProcedure 'spApplyShiftToDriver'
go
/***************************************/
-- Created: 2016.10.27 - 4.3.0 - Kevin Alons
-- Purpose: apply the DriverShiftType to the specified driver for the specified date range
-- Changes:
/***************************************/
CREATE PROCEDURE spApplyShiftToDriver
(
  @start date
, @end date
, @driverID int
, @replaceManualAssignments bit
, @userName varchar(100)
) AS
BEGIN
	DECLARE @driverShiftTypeID int
		, @driverShiftStartDate date
	
	SELECT TOP 1 @driverShiftTypeID = DriverShiftTypeID, @driverShiftStartDate = isnull(DriverShiftStartDate, @start)
	FROM tblDriverScheduleAssignment
	WHERE DriverID = @driverID

	SELECT Position, StatusID, StartHours, DurationHours 
	INTO #days
	FROM tblDriverShiftTypeDay 
	WHERE DriverShiftTypeID = @DriverShiftTypeID

	DECLARE @workDate date = @driverShiftStartDate, @pos tinyint = 1, @count tinyint = (SELECT count(*) FROM #days)
	-- advance to the @start (in the Shift days sequence)
	WHILE (@workDate < @start)
	BEGIN
		SELECT @workDate = dateadd(day, 1, @workDate), @pos = (@pos % @count) + 1
	END

	-- do the assignment
	DECLARE @lastStartDate date = @driverShiftStartDate
	WHILE (@workDate <= @end)
	BEGIN
		IF (@replaceManualAssignments = 1) OR NOT EXISTS (SELECT * FROM tblDriverSchedule WHERE DriverID = @driverID AND ScheduleDate = @workDate AND ManualAssignment = 1)
		BEGIN
			DELETE FROM tblDriverSchedule WHERE DriverID = @driverID AND ScheduleDate = @workDate
			INSERT INTO tblDriverSchedule (DriverID, ScheduleDate, StatusID, StartHours, DurationHours, ManualAssignment, CreatedByUser)
				SELECT @driverID, @workDate, StatusID, StartHours, DurationHours, 0, @userName
				FROM #days
				WHERE Position = @Pos
		END
		SELECT @workDate = dateadd(day, 1, @workdate), @pos = (@pos % @count) + 1
		IF (@pos = 1) SET @lastStartDate = @workDate
	END
	UPDATE tblDriverScheduleAssignment SET DriverShiftStartDate = @lastStartDate WHERE DriverID = @driverID
END
GO
GRANT EXECUTE ON spApplyShiftToDriver TO role_iis_acct
GO

/***************************************/
-- Created: 2016/07/30 - 3.13.10 - Kevin Alons
-- Purpose: apply the DriverShiftType to the specified drivers for the specified date range
-- Changes:
-- 4.3.0 - 2016.10.27 - KDA	- convert to use new tblDriverSchedule & related tables
/***************************************/
ALTER PROCEDURE spApplyShiftToDrivers
(
  @start date
, @end date
, @driverID_CSV varchar(max)
, @replaceManualAssignments bit
, @userName varchar(100)
) AS
BEGIN
	IF (datediff(day, @start, @end) < 0)
	BEGIN
		RAISERROR('Start date cannot be after End date', 16, 1)
		RETURN
	END
	ELSE
	BEGIN
		DECLARE @driverIDs IDTABLE
		INSERT INTO @driverIDs SELECT ID FROM dbo.fnSplitCSVIDs(@driverID_CSV)
		WHILE EXISTS (SELECT * FROM @driverIDs)
		BEGIN
			DECLARE @driverID int = (SELECT min(ID) FROM @driverIDs)
			EXEC spApplyShiftToDriver @start, @end, @driverID, @replaceManualAssignments, @userName
			DELETE FROM @driverIDs WHERE ID = @driverID
		END
	END

END

GO

exec _spRefreshAllViews
go
exec _spRebuildAllObjects
go

commit
set noexec off