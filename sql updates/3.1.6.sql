/*
	-- performance optimizations to Route Rate query logic
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.1.5'
SELECT  @NewVersion = '3.1.6'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

/***********************************/
-- Date Created: 9 Jun 2013
-- Author: Kevin Alons
-- Purpose: return Carrier route rates with translated "friendly" values + "missing route rates", computed Earliest & End Date values)
/***********************************/
ALTER VIEW [dbo].[viewCarrierRouteRates] AS
SELECT RB.ID, R.ID AS RouteID
	, isnull(RB.UomID, U.ID) AS UomID
	, isnull(RB.Rate, 0) AS Rate
	, isnull(RB.EffectiveDate, dbo.fnDateOnly(ORD.MinOpenOrderDate)) AS EffectiveDate
	, cast(CASE WHEN C.DeleteDateUTC IS NOT NULL OR RB.Active = 0 THEN 0 ELSE 1 END as bit) AS Active
	, isnull(ORD.OpenOrderCount, 0) as OpenOrderCount
	, isnull(R.ActualMiles, 0) AS ActualMiles
	, RB.CreateDateUTC, RB.CreatedByUser, RB.LastChangeDateUTC, RB.LastChangedByUser
	, RB.EndDate
	, RB.EarliestEffectiveDate
	, C.ID AS CarrierID, C.Name AS Carrier
	, R.OriginID, R.Origin, R.OriginFull
	, R.DestinationID , R.Destination, R.DestinationFull
	, CASE WHEN RB.ID IS NULL THEN 'Missing' WHEN C.Active = 0 THEN 'Carrier Deleted' WHEN RB.Active = 0 THEN 'Route Inactive' ELSE 'Active' END AS Status
	, cast(CASE WHEN R.ID IS NULL OR isnull(ORD.OpenOrderCount, 0) = 0 THEN 0 ELSE 1 END as bit) AS RouteInUse
	, U.Name AS UOM
	, U.Abbrev AS UomShort
FROM (viewCarrier C CROSS JOIN viewRoute R)
LEFT JOIN viewCarrierRouteRatesBase RB ON RB.CarrierID = C.ID AND RB.OriginID = R.OriginID AND RB.DestinationID = R.DestinationID
LEFT JOIN (SELECT CarrierID, RouteID, count(1) AS OpenOrderCount, min(OrderDate) AS MinOpenOrderDate FROM viewOrder WHERE StatusID IN (4) GROUP BY CarrierID, RouteID) ORD ON ORD.CarrierID = C.ID AND ORD.RouteID = R.ID
LEFT JOIN tblOrigin O ON O.ID = R.OriginID
LEFT JOIN tblUom U ON U.ID = isnull(RB.UomID, O.UomID)

GO

/***********************************/
-- Date Created: 9 Jun 2013
-- Author: Kevin Alons
-- Purpose: return Customer route rates with translated "friendly" values + "missing route rates", computed Earliest & End Date values)
/***********************************/
ALTER VIEW [dbo].[viewCustomerRouteRates] AS
SELECT RB.ID, R.ID AS RouteID
	, isnull(RB.UomID, U.ID) AS UomID
	, isnull(RB.Rate, 0) AS Rate
	, isnull(RB.EffectiveDate, dbo.fnDateOnly(ORD.MinOpenOrderDate)) AS EffectiveDate
	, cast(CASE WHEN RB.Active = 0 THEN 0 ELSE 1 END as bit) AS Active
	, isnull(ORD.OpenOrderCount, 0) as OpenOrderCount
	, isnull(R.ActualMiles, 0) AS ActualMiles
	, RB.CreateDateUTC, RB.CreatedByUser, RB.LastChangeDateUTC, RB.LastChangedByUser
	, RB.EndDate
	, RB.EarliestEffectiveDate
	, C.ID AS CustomerID, C.Name AS Customer
	, R.OriginID, R.Origin, R.OriginFull
	, R.DestinationID , R.Destination, R.DestinationFull
	, CASE WHEN RB.ID IS NULL THEN 'Missing' WHEN RB.Active = 0 THEN 'Route Inactive' ELSE 'Active' END AS Status
	, cast(CASE WHEN R.ID IS NULL OR isnull(ORD.OpenOrderCount, 0) = 0 THEN 0 ELSE 1 END as bit) AS RouteInUse
	, U.Name AS UOM
	, U.Abbrev AS UomShort
FROM (viewCustomer C CROSS JOIN viewRoute R)
LEFT JOIN viewCustomerRouteRatesBase RB ON RB.CustomerID = C.ID AND RB.OriginID = R.OriginID AND RB.DestinationID = R.DestinationID
LEFT JOIN (SELECT CustomerID, RouteID, count(1) AS OpenOrderCount, min(OrderDate) AS MinOpenOrderDate FROM viewOrder WHERE StatusID IN (4) GROUP BY CustomerID, RouteID) ORD ON ORD.CustomerID = C.ID AND ORD.RouteID = R.ID
LEFT JOIN tblOrigin O ON O.ID = R.OriginID
LEFT JOIN tblUom U ON U.ID = isnull(RB.UomID, O.UomID)

GO

/***********************************/
-- Date Created: 18 Sep 2014
-- Author: Kevin Alons
-- Purpose: return Carrier route rates per the specified criteria (used by Carrier Route Rates page)
/***********************************/
CREATE PROCEDURE spGetCarrierRouteRates(
  @Carrierid int
, @originID int
, @destinationID int
, @startDate date
, @includeActive bit
, @includeMissing bit
, @includeRouteInactive bit
, @IncludeCarrierDeleted bit
, @includeMissingRoutes bit
) AS
BEGIN
	SELECT TOP 100 PERCENT *
		, CASE WHEN Status LIKE '%Missing' THEN Rate ELSE NULL END AS NewRate
		, CASE WHEN Status LIKE '%Missing' THEN EffectiveDate ELSE NULL END AS NewEffectiveDate
		, NULL AS ImportOutcome 
	FROM (
		-- get the currently defined or missing rates
		SELECT ID, RouteID, UomID, Rate, EffectiveDate, Active, OpenOrderCount, ActualMiles
			, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser
			, EndDate, EarliestEffectiveDate, CarrierID, Carrier, OriginID, Origin, OriginFull, DestinationID, Destination, DestinationFull
			, Status, RouteInUse, UOM, UomShort
		FROM dbo.viewCarrierRouteRates 
		WHERE (@CarrierID = -1 OR CarrierID = @CarrierID) 
			AND (@OriginID = -1 OR OriginID = @OriginID) 
			AND (@DestinationID = -1 OR DestinationID = @DestinationID) 
			AND (EndDate IS NULL OR EndDate >= @StartDate) 
			AND (@IncludeActive = 1 OR Status NOT LIKE 'Active') 
			AND (@IncludeMissing = 1 OR Status NOT LIKE 'Missing') 
			AND (@IncludeRouteInactive = 1 OR Status NOT LIKE 'Route Inactive') 
			AND (@IncludeCarrierDeleted = 1 OR Status NOT LIKE 'Carrier Deleted')	
		UNION 

		-- generate any rates that could exist for missing/undefined routes
		SELECT ID = NULL, RouteID = NULL, UomID = O.UomID, Rate = NULL, EffectiveDate = isnull(@StartDate, getdate()), Active = cast(0 as bit), OpenOrderCount = 0, ActualMiles = 0
		, null, null, null, null
		, null, null, C.ID, C.Name, O.ID, O.Name, O.FullName, D.ID, D.Name, D.FullName
		, 'Route Missing', cast(0 as bit), U.Name, U.Abbrev 
		FROM viewOrigin O
		JOIN tblUom U ON U.ID = O.UomID
		CROSS JOIN viewDestination D
		JOIN tblCarrier C ON C.ID = @CarrierID
		LEFT JOIN viewCarrierRouteRates R ON R.OriginID = O.ID AND R.DestinationID = D.ID AND (R.EndDate IS NULL OR EndDate >= @StartDate)
		WHERE (@includeMissingRoutes = 1)
		  AND R.ID IS NULL
		  AND (@CarrierID = -1 OR C.ID = @CarrierID)
		  AND (@originID = -1 OR O.ID = @originID)
		  AND (@destinationID = -1 OR O.ID = @destinationID)
	) X
	ORDER BY Carrier, Origin, Destination, EffectiveDate DESC
		
END

GO
GRANT EXECUTE ON spGetCarrierRouteRates TO dispatchcrude_iis_acct 
GO

/***********************************/
-- Date Created: 18 Sep 2014
-- Author: Kevin Alons
-- Purpose: return Carrier route rates per the specified criteria (used by Carrier Route Rates page)
/***********************************/
CREATE PROCEDURE spGetCustomerRouteRates(
  @customerid int
, @originID int
, @destinationID int
, @startDate date
, @includeActive bit
, @includeMissing bit
, @includeRouteInactive bit
, @includeMissingRoutes bit
) AS
BEGIN
	SELECT TOP 100 PERCENT *
		, CASE WHEN Status LIKE '%Missing' THEN Rate ELSE NULL END AS NewRate
		, CASE WHEN Status LIKE '%Missing' THEN EffectiveDate ELSE NULL END AS NewEffectiveDate
		, NULL AS ImportOutcome 
	FROM (
		-- get the currently defined or missing rates
		SELECT ID, RouteID, UomID, Rate, EffectiveDate, Active, OpenOrderCount, ActualMiles
			, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser
			, EndDate, EarliestEffectiveDate, CustomerID, Customer, OriginID, Origin, OriginFull, DestinationID, Destination, DestinationFull
			, Status, RouteInUse, UOM, UomShort
		FROM dbo.viewCustomerRouteRates 
		WHERE (@CustomerID = -1 OR CustomerID = @CustomerID) 
			AND (@OriginID = -1 OR OriginID = @OriginID) 
			AND (@DestinationID = -1 OR DestinationID = @DestinationID) 
			AND (EndDate IS NULL OR EndDate >= @StartDate) 
			AND (@IncludeActive = 1 OR Status NOT LIKE 'Active') 
			AND (@IncludeMissing = 1 OR Status NOT LIKE 'Missing') 
			AND (@IncludeRouteInactive = 1 OR Status NOT LIKE 'Route Inactive') 
		
		UNION 

		-- generate any rates that could exist for missing/undefined routes
		SELECT ID = NULL, RouteID = NULL, UomID = O.UomID, Rate = NULL, EffectiveDate = isnull(@StartDate, getdate()), Active = cast(0 as bit), OpenOrderCount = 0, ActualMiles = 0
		, null, null, null, null
		, null, null, C.ID, C.Name, O.ID, O.Name, O.FullName, D.ID, D.Name, D.FullName
		, 'Route Missing', cast(0 as bit), U.Name, U.Abbrev 
		FROM viewOrigin O
		JOIN tblUom U ON U.ID = O.UomID
		CROSS JOIN viewDestination D
		JOIN tblCustomer C ON C.ID = @CustomerID
		LEFT JOIN viewCustomerRouteRates R ON R.OriginID = O.ID AND R.DestinationID = D.ID AND (R.EndDate IS NULL OR EndDate >= @StartDate)
		WHERE (@includeMissingRoutes = 1)
		  AND R.ID IS NULL
		  AND (@customerid = -1 OR C.ID = @customerid)
		  AND (@originID = -1 OR O.ID = @originID)
		  AND (@destinationID = -1 OR O.ID = @destinationID)
	) X
	ORDER BY Customer, Origin, Destination, EffectiveDate DESC
		
END

GO
GRANT EXECUTE ON spGetCustomerRouteRates TO dispatchcrude_iis_acct 
GO

--spGetCarrierRouteRates 0, 0, 0, '6/1/2014', 1, 1, 1, 1, 1
--spGetCustomerRouteRates 12, -1, -1, '6/1/2014', 0, 1, 0, 0
/*
declare @includeActive bit, @includeMissing bit; select @includeActive = 0, @includeMissing = 1;
select * 
from viewCustomerRouteRates 
where CustomerID = 12 and (EndDate is null or EndDate >= '9/18/2014')
			AND (@IncludeActive = 1 OR Status NOT LIKE 'Active') 
			AND (@IncludeMissing = 1 OR Status NOT LIKE 'Missing') 
*/

EXEC _spRebuildAllObjects
GO

COMMIT
SET NOEXEC OFF