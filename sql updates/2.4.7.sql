/* 
	allow setting default OriginBOLNum when creating orders
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.4.6'
SELECT  @NewVersion = '2.4.7'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

/***********************************/
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: create new Order (loads) for the specified criteria
/***********************************/
ALTER PROCEDURE [dbo].[spCreateLoads]
(
  @OriginID int
, @DestinationID int
, @TicketTypeID int
, @DueDate datetime
, @CustomerID int
, @CarrierID int = NULL
, @DriverID int = NULL
, @Qty int
, @UserName varchar(100)
, @TankNum varchar(20) = NULL
, @BOLNum varchar(15) = NULL
, @PriorityID int = 3 -- LOW priority
, @StatusID smallint = -10 -- GENERATED
, @ProductID int = 1 -- basic Crude Oil
) AS
BEGIN
	DECLARE @PumperID int, @OperatorID int, @ProducerID int, @OriginUomID int, @DestUomID int
	SELECT @PumperID = PumperID, @OperatorID = OperatorID, @ProducerID = ProducerID, @OriginUomID = UomID
	FROM tblOrigin 
	WHERE ID = @OriginID
	SELECT @DestUomID = UomID FROM tblDestination WHERE ID = @DestinationID

	DECLARE @i int
	SELECT @i = 0
	
	WHILE @i < @Qty BEGIN
		
		INSERT INTO dbo.tblOrder (OriginID, OriginUomID, DestinationID, DestUomID, TicketTypeID, DueDate, CustomerID
				, CarrierID, DriverID, StatusID, PriorityID, OriginTankNum, OriginBOLNum
				, OrderNum, ProductID, PumperID, OperatorID, ProducerID, CreateDateUTC, CreatedByUser)
			VALUES (@OriginID, @OriginUomID, @DestinationID, @DestUomID, @TicketTypeID, @DueDate, @CustomerID
				, @CarrierID, @DriverID, @StatusID, @PriorityID, @TankNum, @BOLNum
				, (SELECT isnull((SELECT max(OrderNum) FROM tblOrder), 100000) + 1), @ProductID
				, @PumperID, @OperatorID, @ProducerID, GETUTCDATE(), @UserName)
		
		IF (@DriverID IS NOT NULL)
		BEGIN
			UPDATE tblOrder SET TruckID = D.TruckID, TrailerID = D.TrailerID, Trailer2ID = D.Trailer2ID
			FROM tblOrder O
			JOIN tblDriver D ON D.ID = O.DriverID
			WHERE O.ID = SCOPE_IDENTITY()
		END
		
		SET @i = @i + 1
	END
END

GO

COMMIT
SET NOEXEC OFF