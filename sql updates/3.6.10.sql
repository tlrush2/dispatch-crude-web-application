-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.6.9'
SELECT  @NewVersion = '3.6.10'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'Report Center: add Pickup Driver Notes | Delivery Driver Notes'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/* Update three field names */
UPDATE tblReportColumnDefinition
SET DataField = 'PickupDriverNotes', Caption = 'GENERAL | Pickup Driver Notes'
WHERE ID = 51
GO

SET IDENTITY_INSERT tblReportColumnDefinition ON

IF NOT EXISTS (SELECT * FROM tblReportColumnDefinition WHERE ID = 245)
	INSERT INTO tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport)
	SELECT 245, ReportID, 'DeliverDriverNotes', 'GENERAL | Deliver Driver Notes', DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport
	FROM tblReportColumnDefinition
	WHERE ID = 51

SET IDENTITY_INSERT tblReportColumnDefinition OFF

COMMIT
SET NOEXEC OFF