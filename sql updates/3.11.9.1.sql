-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.11.9'
SELECT  @NewVersion = '3.11.9.1'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1207: Add "Alt Tank ID" field to origin tanks.'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/* Add AltTankNum field to tblOriginTank (this accomodates an issue Regal Fuels has with SUNDEX) */
ALTER TABLE tblOriginTank
ADD AltTankNum VARCHAR(20) NULL
GO


/***********************************/
-- Date Created: 12 Mar 2014
-- Author: Kevin Alons
-- Purpose: return Origin Tank records with translated values
-- Changes:
	-- 3.7.22 - 6/5/2015 - GSM - make Delete Date fields reflect Origin.DeleteX values when NULL
	-- 3.11.9.1 - 04/04/2016 - BB - Add AltTankNum
/***********************************/
ALTER VIEW [dbo].[viewOriginTank] AS
	SELECT OT.ID				 				
		, OT.OriginID					
		, OT.TankNum 			
		, OT.AltTankNum		-- 3.11.9.1
		, OT.TankDescription 	
		, OT.CreateDateUTC 
		, OT.CreatedByUser 		
		, OT.LastChangeDateUTC
		, OT.LastChangedByUser
		, DeleteDateUTC = ISNULL(OT.DeleteDateUTC, O.DeleteDateUTC)	
		, DeletedByUser = ISNULL(OT.DeletedByUser, O.DeletedByUser)	
		, Origin = O.Name
		, OriginLeaseNum = O.LeaseNum
		, TankNum_Unstrapped = CASE WHEN OT.TankNum = '*' THEN '[Enter Details]' ELSE OT.TankNum END 
		, Unstrapped = CASE WHEN OT.TankNum = '*' THEN 1 ELSE 0 END
		, ForGauger = CASE WHEN O.GaugerTicketTypeID IS NULL THEN 0 ELSE 1 END
	FROM dbo.tblOriginTank OT
	JOIN dbo.viewOrigin O ON O.ID = OT.OriginID
GO


/***********************************/
-- Date Created: 22 Jan 2013
-- Author: Kevin Alons
-- Purpose: query the tblOrderTicket table and include "friendly" translated values
-- Changes: 3.11.9.1 - 04/04/2016 - BB - Add AltTankNum field
/***********************************/
ALTER VIEW [dbo].[viewOrderTicket] AS
SELECT OT.*
	, TicketType = TT.Name 
	, OriginTankText = CASE WHEN OT.OriginTankID IS NULL OR ORT.TankNum = '*' THEN OT.TankNum ELSE ORT.TankNum END 
	, IsStrappedTank = cast(CASE WHEN OT.OriginTankID IS NULL OR ORT.TankNum = '*' THEN 0 ELSE 1 END as bit) 
	, Active = cast((CASE WHEN OT.DeleteDateUTC IS NULL THEN 1 ELSE 0 END) as bit) 
	, IsDeleted = cast((CASE WHEN OT.DeleteDateUTC IS NOT NULL THEN 1 ELSE 0 END) as bit) 
	, Gravity60F = dbo.fnCorrectedAPIGravity(ProductObsGravity, ProductObsTemp)
	, RejectNum = OTRR.Num
	, RejectDesc = OTRR.Description
	, RejectNumDesc = OTRR.NumDesc
	, ORT.AltTankNum		-- 3.11.9.1
FROM tblOrderTicket OT
JOIN tblTicketType TT ON TT.ID = OT.TicketTypeID
LEFT JOIN tblOriginTank ORT ON ORT.ID = OT.OriginTankID
LEFT JOIN dbo.viewOrderTicketRejectReason OTRR ON OTRR.ID = OT.RejectReasonID
GO


/***********************************
Date Created: 9 Mar 2013
Author: Kevin Alons
Purpose: return Order JOIN OrderTicket + computed Reroute details into a single view
Special: 
	1) all TICKET fields should be preceded with T_ to ensure they are unique and to explicitly identify them as TICKET (not ORDER) level values
Changes:
	- 3.7.16 - 2015/05/22 - KDA - added T_UID field
	- 3.8.15 - 2015/08/15 -  BB - added T_TankID field
	- 3.9.25 - 2015/11/06 -  BB - DCWEB-921 - Added T_TicketTypeID field for filtering on ticket ticket type
	- 3.9.38 - 2015/12/21 - BB - Add WeightNetUnits, WeightGrossUnits, and WeightTareUnits (DCWEB-972)
  - 3.11.9.1 - 2016/04/04 - BB - Added AltTankNum field
***********************************/
ALTER VIEW [dbo].[viewOrder_OrderTicket_Full] AS 
	SELECT OE.* 
        , T_UID =  OT.UID
		, T_ID = OT.ID
		, T_TicketType = CASE WHEN OE.TicketCount = 0 THEN OE.TicketType ELSE OT.TicketType END 
		, T_TicketTypeID = OT.TicketTypeID
		, T_CarrierTicketNum = CASE WHEN OE.TicketCount = 0 THEN ltrim(OE.OrderNum) + CASE WHEN OE.Rejected = 1 THEN 'X' ELSE '' END ELSE OT.CarrierTicketNum END 
		, T_BOLNum = CASE WHEN OE.TicketCount = 0 THEN OE.OriginBOLNum ELSE OT.BOLNum END 
		, T_TankNum = isnull(OT.OriginTankText, OE.OriginTankText)
		, T_AltTankNum = OT.AltTankNum		-- 3.11.9.1
		, T_TankID = OT.OriginTankID
		, T_IsStrappedTank = OT.IsStrappedTank 
		, T_BottomFeet = OT.BottomFeet
		, T_BottomInches = OT.BottomInches
		, T_BottomQ = OT.BottomQ 
		, T_OpeningGaugeFeet = OT.OpeningGaugeFeet 
		, T_OpeningGaugeInch = OT.OpeningGaugeInch 
		, T_OpeningGaugeQ = OT.OpeningGaugeQ 
		, T_ClosingGaugeFeet = OT.ClosingGaugeFeet 
		, T_ClosingGaugeInch = OT.ClosingGaugeInch 
		, T_ClosingGaugeQ = OT.ClosingGaugeQ 
		, T_OpenTotalQ = dbo.fnGaugeQtrInches(OT.OpeningGaugeFeet, OT.OpeningGaugeInch, OT.OpeningGaugeQ) 
		, T_CloseTotalQ = dbo.fnGaugeQtrInches(OT.ClosingGaugeFeet, OT.ClosingGaugeInch, ot.ClosingGaugeQ) 
		, T_OpenReading = ltrim(OT.OpeningGaugeFeet) + 'ft ' + ltrim(OT.OpeningGaugeInch) + 'in ' + ltrim(OT.OpeningGaugeQ) + 'q' 
		, T_CloseReading = ltrim(OT.ClosingGaugeFeet) + 'ft ' + ltrim(OT.ClosingGaugeInch) + 'in ' + ltrim(OT.ClosingGaugeQ) + 'q' 
		, T_CorrectedAPIGravity = round(cast(OT.Gravity60F as decimal(9,4)), 9, 4) 
		, T_GrossStdUnits = OT.GrossStdUnits
		, T_SealOff = ltrim(OT.SealOff) 
		, T_SealOn = ltrim(OT.SealOn) 
		, T_HighTemp = OT.ProductHighTemp
		, T_LowTemp = OT.ProductLowTemp
		, T_ProductObsTemp = OT.ProductObsTemp 
		, T_ProductObsGravity = OT.ProductObsGravity 
		, T_ProductBSW = OT.ProductBSW 
		, T_Rejected = isnull(OT.Rejected, OE.Rejected)
		, T_RejectReasonID = OT.RejectReasonID
		, T_RejectNum = OT.RejectNum
		, T_RejectDesc = OT.RejectDesc
		, T_RejectNumDesc = OT.RejectNumDesc
		, T_RejectNotes = OT.RejectNotes 
		, T_GrossUnits = OT.GrossUnits 
		, T_NetUnits = OT.NetUnits 
		, T_MeterFactor = OT.MeterFactor
		, T_OpenMeterUnits = OT.OpenMeterUnits
		, T_CloseMeterUnits = OT.CloseMeterUnits
		, T_DispatchConfirmNum = CASE WHEN OE.TicketCount = 0 THEN OE.DispatchConfirmNum ELSE isnull(OT.DispatchConfirmNum, OE.DispatchConFirmNum) END
		, T_WeightTareUnits = OT.WeightTareUnits  -- 3.9.38
		, T_WeightGrossUnits = OT.WeightGrossUnits  -- 3.9.38
		, T_WeightNetUnits = OT.WeightNetUnits  -- 3.9.38
		, PreviousDestinations = dbo.fnRerouteDetails(OE.ID, 'PreviousDestinations', '<br/>') 
		, RerouteUsers = dbo.fnRerouteDetails(OE.ID, 'RerouteUsers', '<br/>') 
		, RerouteDates = dbo.fnRerouteDetails(OE.ID, 'RerouteDates', '<br/>') 
		, RerouteNotes = dbo.fnRerouteDetails(OE.ID, 'RerouteNotes', '<br/>')
	FROM dbo.viewOrderLocalDates OE
	LEFT JOIN viewOrderTicket OT ON OT.OrderID = OE.ID AND OT.DeleteDateUTC IS NULL
	WHERE OE.DeleteDateUTC IS NULL
GO


/* Add "AltTankNum" to report center fields */
SET IDENTITY_INSERT tblReportColumnDefinition ON
INSERT tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport)
	SELECT 284,1,'T_AltTankNum','TICKET | GENERAL | Ticket Alt Tank #',NULL, NULL, 1, NULL, 1, '*', 0
	EXCEPT SELECT ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport FROM tblReportColumnDefinition
SET IDENTITY_INSERT tblReportColumnDefinition OFF
GO


/*****************************************************************************************/
-- Author: Kevin Alons
-- Date Created: 27 Feb 2014
-- Purpose: return export final orders in the SunocoSundex
-- Changes:
	-- 3.7.21 - 6/3/2015 - BBloodworth - Ticket_Source_Code is now a dynamic system setting value
	-- 3.10.14 - 02/25/2016 - KDA & BB - Added custom data field functionality to the view.
	-- 3.11.9.1 - 04/04/2016 - BB - Change tank number to always use newly added "AltTankNum" field
/*****************************************************************************************/
ALTER VIEW [dbo].[viewSunocoSundex] AS
SELECT
	_ID = O.ID
	, _CustomerID = O.CustomerID
	, _OrderNum = O.OrderNum
	, Request_Code = CASE WHEN EP.IsNew = 1 THEN 'A' ELSE 'C' END
	, Company_Code = isnull((SELECT Value FROM tblObjectCustomData WHERE ObjectFieldID = 90003 AND RecordID = O.DestinationID), (SELECT Value FROM tblSetting WHERE ID = 59))
	, Ticket_Type = UPPER(left(O.TicketType, 1))
	/* Changed ticket source code 3.10.14 */
	, Ticket_Source_Code = isnull((SELECT Value FROM tblObjectCustomData WHERE ObjectFieldID = 90002 AND RecordID = O.DestinationID), (SELECT Value FROM tblSetting WHERE ID = 53))
	, Ticket_Number = isnull(T.CarrierTicketNum, ltrim(O.OrderNum) + 'X')
	, Ticket_Date = dbo.fnDateMMddYYYY(O.OrderDate)
	, SXL_Property_Code = isnull(O.LeaseNum, '')
	, TP_Property_Code = ''
	, Lease_Company_Name = O.Origin
	, Destination = isnull(OCD.Value, '')
	, Tank_Meter_Number = coalesce((SELECT min(AltTankNum) FROM tblOriginTank WHERE DeleteDateUTC IS NULL AND ID = O.OriginTankID), T.OriginTankText, O.OriginTankNum)  -- 3.11.9.1
	, Open_Date = dbo.fnDateMMddYYYY(O.OriginArriveTime)
	, Open_Time = dbo.fnTimeOnly(O.OriginArriveTime)
	, Close_Date = dbo.fnDateMMddYYYY(O.OriginDepartTime)
	, Close_Time = dbo.fnTimeOnly(O.OriginDepartTime)
	, Estimated_Volume = cast(ROUND(T.GrossUnits, 2) as decimal(18, 2))
	, Gross_Volume = cast(ROUND(T.GrossUnits, 2) as decimal(18, 2))
	, Net_Volume = cast(ROUND(T.NetUnits, 2) as decimal(18, 2))
	, Observed_Gravity = isnull(ltrim(T.ProductObsGravity), '')
	, Observed_Temperature = isnull(ltrim(T.ProductObsTemp), '')
	, Observed_BSW = isnull(ltrim(T.ProductBSW), '')
	, Corrected_Gravity_API = 0
	, Purchaser = 'Sonoco Logistics'
	, First_Reading_Gauge_Ft = isnull(ltrim(T.OpeningGaugeFeet), '')
	, First_Reading_Gauge_In = isnull(ltrim(T.OpeningGaugeInch), '')
	, First_Reading_Gauge_Nu = isnull(ltrim(T.OpeningGaugeQ), '')
	, First_Reading_Gauge_De = 4
	, First_Temperature = isnull(ltrim(T.ProductHighTemp), '')
	, First_Bottom_Ft = isnull(ltrim(T.BottomFeet), '')
	, First_Bottom_In = isnull(ltrim(T.BottomInches), '')
	, First_Bottom_Nu = isnull(ltrim(T.BottomQ), '')
	, First_Bottom_De = 4
	, Second_Reading_Gauge_Ft = isnull(ltrim(T.ClosingGaugeFeet), '')
	, Second_Reading_Gauge_In = isnull(ltrim(T.ClosingGaugeInch), '')
	, Second_Reading_Gauge_Nu = isnull(ltrim(T.ClosingGaugeQ), '')
	, Second_Reading_Gauge_De = 4
	, Second_Temperature = isnull(ltrim(T.ProductLowTemp), '')
	, Second_Bottom_Ft = 0
	, Second_Bottom_In = 0
	, Second_Bottom_Nu = 0
	, Second_Bottom_De = 4
	, Shrinkage_Incrustation_Factor = 1
	, First_Reading_Meter = T.OpenMeterUnits
	, Second_Reading_Meter = T.CloseMeterUnits
	, Meter_Factor = T.MeterFactor
	, Temp_Comp_Meter = ''
	, Avg_Line_Temp = T.ProductObsTemp
	, Truck_ID = ''
	, Trailer_ID = ''
	, Driver_ID =''
	, Miles = ISNULL(O.ActualMiles, 0)
	, County = ''
	, State = ''
	, Invoice_Number = ''
	, Invoice_Date = ''
	, Remarks= ''
	, API_Compliant_Chapter = ''
	, Use_SXL_Calculation = 'Y'
	, Seal_On = dbo.fnTrimSealValue(T.SealOn, '0')
	, Seal_Off = dbo.fnTrimSealValue(T.SealOff, '0')
	, Ticket_Exclusion_Cd = CASE WHEN isnull(T.Rejected, O.Rejected) = 1 THEN 'RF' ELSE '' END
	, Confirmation_Number = isnull(T.DispatchConfirmNum, O.DispatchConfirmNum)
	, Split_Flag = CASE WHEN (SELECT COUNT(*) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND DeleteDateUTC IS NULL) > 1 THEN 'Y' ELSE 'N' END
	, Paired_Ticket_Number = isnull((SELECT min(CarrierTicketNum) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND DeleteDateUTC IS NULL AND OT.CarrierTicketNum <> T.CarrierTicketNum), '')
	, Bobtail_Flag = 'N'
	, Ticket_Extra_Info_Flag = CASE WHEN T.Rejected = 1 THEN T.RejectNum ELSE '' END
FROM viewOrderLocalDates O
JOIN dbo.viewOrderCustomerFinalExportPending EP ON EP.ID = O.ID
LEFT JOIN viewOrderTicket T ON T.OrderID = O.ID AND T.DeleteDateUTC IS NULL
/* Changed join to get destination code from tblCustomerDestinationCode to tblObjectCustomData 3.10.14 */
LEFT JOIN dbo.tblObjectCustomData OCD ON OCD.ObjectFieldID = 90001 AND OCD.RecordID = O.DestinationID
GO


/* Refresh all views 4 times to ensure all views in "the report center view stack" are updated. This should also cover the SUNDEX export view */
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO


COMMIT 
SET NOEXEC OFF