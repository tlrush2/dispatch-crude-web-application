SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.6.3'
SELECT  @NewVersion = '4.6.4'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'JT-2956 - Add Per Mile feature to Accessorial Rates'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


UPDATE tblRateType SET ForAssessorialFee = 1 WHERE ID = 5 /* PER MILE */

GO


/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier AssessorialRate Amounts info for the specified order
-- Changes:
	- 3.7.13 - 2015/09/05 - KDA - fix to "% of Shipper" rates
	- 3.9.16 - 2015/09/21 - KDA - fix issue where wrong ShipperAssessorialCharge was used for "% of Shipper" rates when multiple Shipper Assessorial charges are present
	- 4.6.4 - 2017/04/28 - JAE - Added Miles to pass into the RateToAmount function
***********************************/
ALTER FUNCTION fnOrderCarrierAssessorialAmounts(@ID int, @Units decimal(18, 10), @UomID int, @Miles int) RETURNS TABLE AS RETURN
	SELECT AssessorialRateTypeID = TypeID, RateID, RateTypeID, Amount
	FROM (
		SELECT TypeID, RateID = ID, RateTypeID
			, Amount = dbo.fnRateToAmount(RateTypeID, @Units, @UomID, Rate, UomID, OSC.LoadAmount
			, CASE WHEN RateTypeID = 3 THEN (SELECT TOP 1 Amount FROM tblOrderSettlementShipperAssessorialCharge SSAC WHERE OrderID = @ID AND SSAC.AssessorialRateTypeID = CAR.TypeID) ELSE NULL END
			, @Miles)
		FROM dbo.fnOrderCarrierAssessorialRates(@ID) CAR
		JOIN tblOrderSettlementCarrier OSC ON OSC.OrderID = @ID
	) X
	WHERE Amount IS NOT NULL

GO

	
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper AssessorialRate Amounts info for the specified order
--		4.6.4			2017-04-28		JAE				Added Miles to pass into the RateToAmount function
/***********************************/
ALTER FUNCTION fnOrderShipperAssessorialAmounts(@ID int, @Units decimal(18, 10), @UomID int, @Miles int) RETURNS TABLE AS RETURN
	SELECT AssessorialRateTypeID = TypeID, RateID, RateTypeID, Amount
	FROM (
		SELECT TypeID, RateID = ID, RateTypeID
			, Amount = dbo.fnRateToAmount(RateTypeID, @Units, @UomID, Rate, UomID, OSS.LoadAmount, NULL, @Miles)
		FROM dbo.fnOrderShipperAssessorialRates(@ID)
		JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = @ID
	) X
	WHERE Amount IS NOT NULL


GO


/***********************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: retrieve and return the Driver AssessorialRate Amounts info for the specified order
-- Changes:
--		4.6.4			2017-04-28		JAE				Added Miles to pass into the RateToAmount function
/***********************************/
ALTER FUNCTION fnOrderDriverAssessorialAmounts(@ID int, @Units decimal(18, 10), @UomID int, @Miles int) RETURNS TABLE AS RETURN
	SELECT AssessorialRateTypeID = TypeID, RateID, RateTypeID, Amount
	FROM (
		SELECT TypeID, RateID = ID, RateTypeID
			, Amount = dbo.fnRateToAmount(RateTypeID, @Units, @UomID, Rate, UomID, OS.LoadAmount
			, CASE WHEN RateTypeID = 6 THEN (SELECT TOP 1 Amount FROM tblOrderSettlementCarrierAssessorialCharge SAC WHERE OrderID = @ID AND SAC.AssessorialRateTypeID = CAR.TypeID) ELSE NULL END
			, @Miles)
		FROM dbo.fnOrderDriverAssessorialRates(@ID) CAR
		JOIN tblOrderSettlementDriver OS ON OS.OrderID = @ID
	) X
	WHERE Amount IS NOT NULL

GO



/*************************************************/
-- Created: 2013.06.02 - Kevin Alons
-- Purpose: compute and add the various Carrier "Settlement" $$ values to an Delivered/Audited order
-- Changes:
-- 3.7.26	- 2015/06/13	- KDA	- add support for: Driver|DriverGroup, Producer to some rates, separate Carrier|Shipper MinSettlementUnits|SettlementFactor best-match values, 
-- 3.9.0	- 2015/08/13	- KDA	- honor Approve overrides in rating process
-- 3.9.9	- 2015/08/31	- KDA	- add new @AllowReApplyPostBatch parameter to allow re-applying after batch is applied
-- 3.9.19.2	- 2015/09/24	- KDA	- fix erroneous logic that computed Asssessorial Settlement results before the Order Level results were posted
-- 3.9.37	- 2015/12/22	- JAE	- Use tblOrderApproval to get override for Carrier Min Settlement Units
-- 3.9.39	- 2016/01/15	- JAE	- Also need to recalculate settlement units since the calculated/max values was already passed
-- 3.13.1	- 2016/07/04	- KDA	- add START/DONE PRINT statements (for debugging purposes)
-- 4.1.8.6	- 2016.09.21	- KDA	- move populate #IA table before transaction starts
--									- @debugStatements parameter + add additional START/DONE PRINT statements (for debugging purposes)
-- 4.1.9.1	- 2016.09.26	- KDA	- restore regressed 3.9.19.2 changes that accidentally got reverted with 4.1.8.6 (added a NOTE to hopefully prevent this again)
-- 4.4.1	- 2016/11/04	- JAE	- Add Destination Chainup
-- 4.6.2	- 2017.04.20	- KDA	- add @Notes parameter (and logic to persist to DB)
-- 4.6.4	- 2017-04-28	- JAE	- Added Miles to the Assessorial Amounts function call
/*************************************************/
ALTER PROCEDURE spApplyRatesCarrier
(
  @ID int
, @UserName varchar(100)
, @OriginWaitAmount money = NULL
, @DestWaitAmount money = NULL
, @RejectionAmount money = NULL
, @FuelSurchargeAmount money = NULL
, @LoadAmount money = NULL
, @Notes varchar(255) = NULL
, @AssessorialRateTypeID_CSV varchar(max) = NULL
, @AssessorialAmount_CSV varchar(max) = null
, @ResetOverrides bit = 0
, @AllowReApplyPostBatch bit = 0
, @debugStatements bit = 0 -- turn on to get debugging statements during execution
) AS BEGIN

	SET NOCOUNT ON

	IF (@debugStatements = 1) PRINT 'spApplyRatesCarrier (' + ltrim(@ID) + ') START ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	
	IF (@AllowReApplyPostBatch = 0) AND EXISTS (SELECT * FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND BatchID IS NOT NULL)
	BEGIN
		RAISERROR('Invoice has already been Carrier Settled', 16, 1)
		RETURN
	END
	
	/* ensure that Shipper Rates have been applied prior to applying Carrier rates (since they could be dependent on Shipper rates) */
	IF NOT EXISTS (SELECT * FROM tblOrderSettlementShipper WHERE OrderID = @ID)
		EXEC spApplyRatesShipper @ID, @UserName

	/* transform the provided manual Assessorial Rates (if any) into a queryable table variable */
	DECLARE @ManualAssessorialRates TABLE (ID int primary key, Amount money)
	INSERT INTO @ManualAssessorialRates (ID, Amount) 
		SELECT ID.ID, R.Amount
		FROM (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), ID FROM dbo.fnSplitCSVIDs(@AssessorialRateTypeID_CSV)) ID
		JOIN (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), amount = CAST(value as money) FROM dbo.fnSplitCSV(@AssessorialAmount_CSV)) R ON R.sortid = ID.sortid

	/* persist all existing MANUAL Assessorial Rates from the existing Order Settlement (if any present and not resetting them) */
	INSERT INTO @ManualAssessorialRates (ID, Amount)
		SELECT AssessorialRateTypeID, Amount 
		FROM dbo.tblOrderSettlementCarrierAssessorialCharge 
		WHERE OrderID = @ID AND AssessorialRateID IS NULL 
			AND ID NOT IN (SELECT ID FROM @ManualAssessorialRates)
			AND @ResetOverrides = 0

	/* persist all existing settlement amount overrides from the existing Order Settlement - if not resetting them */
	IF (@ResetOverrides = 0)
	BEGIN 
		SELECT @OriginWaitAmount = OriginWaitAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @OriginWaitAmount IS NULL AND OriginWaitRateID IS NULL
		SELECT @DestWaitAmount = DestinationWaitAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @DestWaitAmount IS NULL AND DestinationWaitRateID IS NULL
		SELECT @RejectionAmount = OrderRejectAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @RejectionAmount IS NULL AND OrderRejectRateID IS NULL
		SELECT @FuelSurchargeAmount = FuelSurchargeAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @FuelSurchargeAmount IS NULL AND FuelSurchargeRateID IS NULL
		SELECT @LoadAmount = LoadAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @LoadAmount IS NULL AND RouteRateID IS NULL AND RangeRateID IS NULL
	END
	/* persist any existing notes by preserving them if no new note is defined */
	SELECT @Notes = Notes FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @Notes IS NULL AND Notes IS NOT NULL

	/* compute the basic settlement units from the Order being invoices */
	DECLARE @SettlementUnits TABLE
	(
		OrderID int
	  , SettlementUomID int
	  , SettlementFactorID int
	  , CarrierSettlementFactorID int
	  , MinSettlementUnits decimal(18, 10)
	  , MinSettlementUnitsID int
	  , ActualUnits decimal(18, 10)
	  , SettlementUnits decimal(18, 10) -- calculate value basically max(actual, minimum)
	)
	INSERT INTO @SettlementUnits (OrderID, SettlementUomID, SettlementFactorID, CarrierSettlementFactorID, MinSettlementUnits, MinSettlementUnitsID, ActualUnits, SettlementUnits)
		SELECT OSU.OrderID, OSU.SettlementUomID, OSU.SettlementFactorID, OSU.CarrierSettlementFactorID, ISNULL(OA.OverrideCarrierMinSettlementUnits, OSU.MinSettlementUnits), OSU.MinSettlementUnitsID, OSU.ActualUnits, 
			-- use passed settlement (already calculated) or pick the max between the actual and overrride min - JAE 1/15/16 - 3.9.39
			CASE WHEN OA.OverrideCarrierMinSettlementUnits IS NULL THEN OSU.SettlementUnits
						ELSE dbo.fnMaxDecimal(OSU.ActualUnits, OA.OverrideCarrierMinSettlementUnits) END
		FROM dbo.viewOrderSettlementUnitsCarrier OSU
		LEFT JOIN tblOrderApproval OA ON OA.OrderID = OSU.OrderID
		WHERE OSU.OrderID = @ID

	/* compute the order level settlement rateIDs & settlement amounts - into temp table #I */
	SELECT OrderID = @ID
		, OrderDate
		, CarrierSettlementFactorID, MinSettlementUnitsID
		, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
		, RouteRateID, RangeRateID, LoadAmount
		, WaitFeeParameterID
		, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount 
		, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount
		, OrderRejectRateID, OrderRejectAmount
		, FuelSurchargeRateID, FuelSurchargeRate, FuelSurchargeAmount
		, OriginTaxRate
		, TotalAmount = ISNULL(LoadAmount, 0) 
			+ ISNULL(OriginWaitAmount, 0) 
			+ ISNULL(DestWaitAmount, 0) 
			+ ISNULL(OrderRejectAmount, 0) 
			+ ISNULL(FuelSurchargeAmount, 0)
		, CreatedByUser = @UserName
		, OriginChainUp, DestChainUp, H2S, Rerouted
	INTO #I
	FROM (
		SELECT OrderDate
			, CarrierSettlementFactorID, MinSettlementUnitsID
			, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
			, RouteRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RouteRateID ELSE NULL END
			, RangeRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RangeRateID ELSE NULL END
			, LoadAmount = round(ISNULL(@LoadAmount, RR.Amount), 2)
			, WaitFeeParameterID
			, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount = ROUND(OriginWaitAmount, 2)
			, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount = ROUND(DestWaitAmount, 2)
			, OrderRejectRateID = CASE WHEN @RejectionAmount IS NULL THEN RD.RateID ELSE NULL END
			, OrderRejectAmount = round(isnull(@RejectionAmount, RD.Amount), 2)
			, FuelSurchargeRateID = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.RateID ELSE NULL END
			, FuelSurchargeRate = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.Rate ELSE NULL END
			, FuelSurchargeAmount = round(isnull(@FuelSurchargeAmount, FSR.Amount), 2)
			, OriginTaxRate
			, OriginChainUp, DestChainup, H2S, Rerouted = cast(CASE WHEN RerouteCount = 0 THEN 0 ELSE 1 END as bit) /* used for the Auto-Approve logic below */
		FROM (
			SELECT O.ID
				, O.OriginID
				, O.CarrierID
				, O.RouteID
				, SU.CarrierSettlementFactorID
				, SU.SettlementFactorID
				, SU.SettlementUomID
				, SU.ActualUnits
				, SU.MinSettlementUnitsID
				, SU.MinSettlementUnits
				, SettlementUnits = CASE WHEN O.Rejected = 1 THEN 0 ELSE SU.SettlementUnits END
				, RouteMiles = isnull(OA.OverrideActualMiles, O.ActualMiles)
				, OrderDate = isnull(O.OrderDate, O.DueDate)
				, OriginChainUp = cast(CASE WHEN OA.OverrideOriginChainup = 1 THEN 0 ELSE O.OriginChainup END as bit)
				, DestChainUp = cast(CASE WHEN OA.OverrideDestChainup = 1 THEN 0 ELSE O.DestChainup END as bit)
				, H2S = cast(CASE WHEN OA.OverrideH2S = 1 THEN 0 ELSE O.H2S END as bit)
				, O.Rejected
				, RerouteCount = CASE WHEN OA.OverrideReroute = 1 THEN 0 ELSE O.RerouteCount END
				, IsSplit = CASE WHEN O.Rejected = 0 AND O.TicketCount > 1 THEN 1 ELSE 0 END
				, WaitFeeParameterID = WFP.ID
				, OriginWaitRateID = CASE WHEN @OriginWaitAmount IS NULL THEN OWD.RateID ELSE NULL END
				, OriginWaitBillableMinutes = OWD.BillableMinutes
				, OriginWaitBillableHours = OWD.BillableHours
				, OriginWaitAmount = isnull(@OriginWaitAmount, OWD.Amount)
				, DestWaitRateID = CASE WHEN @DestWaitAmount IS NULL THEN DWD.RateID ELSE NULL END
				, DestWaitBillableMinutes = DWD.BillableMinutes
				, DestWaitBillableHours = DWD.BillableHours
				, DestWaitAmount = isnull(@DestWaitAmount, DWD.Amount)
				, OriginTaxRate = OO.TaxRate
			FROM dbo.viewOrder O
			JOIN @SettlementUnits SU ON SU.OrderID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
			OUTER APPLY dbo.fnOrderCarrierOriginWaitData(@ID) OWD 
			OUTER APPLY dbo.fnOrderCarrierDestinationWaitData(@ID) DWD 
			OUTER APPLY dbo.fnOrderCarrierWaitFeeParameter(@ID) WFP
			WHERE O.ID = @ID
		) X
		OUTER APPLY dbo.fnOrderCarrierLoadAmount(@ID, SettlementUnits, SettlementUomID) RR 
		OUTER APPLY dbo.fnOrderCarrierOrderRejectData(@ID, SettlementUnits, SettlementUomID) RD
		OUTER APPLY dbo.fnOrderCarrierFuelSurchargeData(@ID) FSR
	) X2

	DECLARE @CreatedTran bit; SET @CreatedTran = CASE WHEN @@TRANCOUNT = 0 THEN 1 ELSE 0 END
	BEGIN TRY
		IF (@CreatedTran = 1)
			BEGIN TRAN

		-- remove the existing settlment record (if any)
		DELETE FROM tblOrderSettlementCarrierAssessorialCharge WHERE OrderID = @ID
		DELETE FROM tblOrderSettlementCarrier WHERE OrderID = @ID
	
		-- all Units and Rates are first normalized to the Order.OriginUOM then consistent processing
		-- and persisted in the Invoice (Settlement) record in this Order.OriginUOM 
		INSERT INTO tblOrderSettlementCarrier 
			(OrderID
			, OrderDate
			, CarrierSettlementFactorID
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnitsID
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestinationWaitRateID 
			, DestinationWaitBillableMinutes 
			, DestinationWaitBillableHours
			, DestinationWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser
			, Notes)
		SELECT OrderID
			, OrderDate
			, CarrierSettlementFactorID
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnitsID
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestWaitRateID 
			, DestWaitBillableMinutes 
			, DestWaitBillableHours
			, DestWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser
			, left(@Notes, 255)
		FROM #I

		/* compute the new Assessorial Rates for this order - into temp table #IA */
		/* NOTE: while it would be preferred for this to be out of the TX, it can't because it relies on the final Settlement changes being present */
		SELECT OrderID = @ID, AssessorialRateTypeID, AssessorialRateID = RateID, Amount = ROUND(Amount, 2), CreatedByUser = @UserName 
		INTO #IA
		FROM @SettlementUnits SU
		CROSS APPLY dbo.fnOrderCarrierAssessorialAmounts(@ID, SU.SettlementUnits, SU.SettlementUomID, (SELECT FinalActualMiles FROM viewOrder_Financial_Base_Shipper where ID = @ID)) CAA
		WHERE CAA.AssessorialRateTypeID NOT IN (SELECT ID FROM @ManualAssessorialRates)

		/* apply the computed settlement assessorial rate/amount records to the DB */
		INSERT INTO tblOrderSettlementCarrierAssessorialCharge (OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser)
			SELECT OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser FROM #IA
			UNION
			SELECT @ID, ID, NULL, Amount, @UserName FROM @ManualAssessorialRates

		IF (@CreatedTran = 1)
			COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @msg varchar(max), @severity int
		SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY()
		ROLLBACK
		RAISERROR(@msg, @severity, 1)
		RETURN
	END CATCH	

	IF (@debugStatements = 1) PRINT 'spApplyRatesCarrier (' + ltrim(@ID) + ') DONE ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	
END

GO


/*************************************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: compute and add the various Driver "Settlement" $$ values to an Delivered/Audited order
-- Changes:
-- 4.1.8.6	- 2016.09.21	- KDA	- move populate #IA table before transaction starts
--									- @debugStatements parameter + add additional START/DONE PRINT statements (for debugging purposes)
-- 4.1.9.1	- 2016.09.26	- KDA	- restore regressed 3.9.19.2 changes that accidentally got reverted with 4.1.8.6 (added a NOTE to hopefully prevent this again)
-- 4.4.1	- 2016/11/04	- JAE	- Add Destination Chainup
-- 4.6.2	- 2017.04.20	- KDA	- add @Notes parameter (and logic to persist to DB)
-- 4.6.4	- 2017-04-28	- JAE	- Added Miles to the Assessorial Amounts function call
/*************************************************/
ALTER PROCEDURE spApplyRatesDriver
(
  @ID int
, @UserName varchar(100)
, @OriginWaitAmount money = NULL
, @DestWaitAmount money = NULL
, @RejectionAmount money = NULL
, @FuelSurchargeAmount money = NULL
, @LoadAmount money = NULL
, @Notes varchar(255) = NULL
, @AssessorialRateTypeID_CSV varchar(max) = NULL
, @AssessorialAmount_CSV varchar(max) = null
, @ResetOverrides bit = 0
, @AllowReApplyPostBatch bit = 0
, @debugStatements bit = 0 -- turn on to get debugging statements during execution
) AS BEGIN

	SET NOCOUNT ON

	IF (@debugStatements = 1) PRINT 'spApplyRatesDriver (' + ltrim(@ID) + ') START ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	
	IF (@AllowReApplyPostBatch = 0) AND EXISTS (SELECT * FROM tblOrderSettlementDriver WHERE OrderID = @ID AND BatchID IS NOT NULL)
	BEGIN
		RAISERROR('Invoice has already been Driver Settled', 16, 1)
		RETURN
	END
	
	/* ensure that Carrier Rates have been applied prior to applying Driver rates (since they could be dependent on Carrier rates) */
	IF NOT EXISTS (SELECT * FROM tblOrderSettlementCarrier WHERE OrderID = @ID)
		EXEC spApplyRatesCarrier @ID, @UserName

	/* transform the provided manual Assessorial Rates (if any) into a queryable table variable */
	DECLARE @ManualAssessorialRates TABLE (ID int primary key, Amount money)
	INSERT INTO @ManualAssessorialRates (ID, Amount) 
		SELECT ID.ID, R.Amount
		FROM (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), ID FROM dbo.fnSplitCSVIDs(@AssessorialRateTypeID_CSV)) ID
		JOIN (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), amount = CAST(value as money) FROM dbo.fnSplitCSV(@AssessorialAmount_CSV)) R ON R.sortid = ID.sortid

	/* persist all existing MANUAL Assessorial Rates from the existing Order Settlement (if any present and not resetting them) */
	INSERT INTO @ManualAssessorialRates (ID, Amount)
		SELECT AssessorialRateTypeID, Amount 
		FROM dbo.tblOrderSettlementDriverAssessorialCharge 
		WHERE OrderID = @ID AND AssessorialRateID IS NULL 
			AND ID NOT IN (SELECT ID FROM @ManualAssessorialRates)
			AND @ResetOverrides = 0

	/* persist all existing settlement amount overrides from the existing Order Settlement - if not resetting them */
	IF (@ResetOverrides = 0)
	BEGIN 
		SELECT @OriginWaitAmount = OriginWaitAmount FROM tblOrderSettlementDriver WHERE OrderID = @ID AND @OriginWaitAmount IS NULL AND OriginWaitRateID IS NULL
		SELECT @DestWaitAmount = DestinationWaitAmount FROM tblOrderSettlementDriver WHERE OrderID = @ID AND @DestWaitAmount IS NULL AND DestinationWaitRateID IS NULL
		SELECT @RejectionAmount = OrderRejectAmount FROM tblOrderSettlementDriver WHERE OrderID = @ID AND @RejectionAmount IS NULL AND OrderRejectRateID IS NULL
		SELECT @FuelSurchargeAmount = FuelSurchargeAmount FROM tblOrderSettlementDriver WHERE OrderID = @ID AND @FuelSurchargeAmount IS NULL AND FuelSurchargeRateID IS NULL
		SELECT @LoadAmount = LoadAmount FROM tblOrderSettlementDriver WHERE OrderID = @ID AND @LoadAmount IS NULL AND RouteRateID IS NULL AND RangeRateID IS NULL
	END
	/* persist any existing notes by preserving them if no new note is defined */
	SELECT @Notes = Notes FROM tblOrderSettlementDriver WHERE OrderID = @ID AND @Notes IS NULL AND Notes IS NOT NULL

	/* compute the basic settlement units from the Order being invoices */
	DECLARE @SettlementUnits TABLE
	(
		OrderID int
	  , SettlementUomID int
	  , SettlementFactorID int
	  , CarrierSettlementFactorID int
	  , MinSettlementUnits decimal(18, 10)
	  , MinSettlementUnitsID int
	  , ActualUnits decimal(18, 10)
	  , SettlementUnits decimal(18, 10) -- calculate value basically max(actual, minimum)
	)
	INSERT INTO @SettlementUnits (OrderID, SettlementUomID, SettlementFactorID, CarrierSettlementFactorID, MinSettlementUnits, MinSettlementUnitsID, ActualUnits, SettlementUnits)
		SELECT OSU.OrderID, OSU.SettlementUomID, OSU.SettlementFactorID, OSU.CarrierSettlementFactorID, ISNULL(OA.OverrideCarrierMinSettlementUnits, OSU.MinSettlementUnits), OSU.MinSettlementUnitsID, OSU.ActualUnits
			-- use passed settlement (already calculated) or pick the max between the actual and overrride min - JAE 1/15/16 - 3.9.39
			, CASE WHEN OA.OverrideCarrierMinSettlementUnits IS NULL THEN OSU.SettlementUnits
					ELSE dbo.fnMaxDecimal(OSU.ActualUnits, OA.OverrideCarrierMinSettlementUnits) END
		FROM dbo.viewOrderSettlementUnitsCarrier OSU
		LEFT JOIN tblOrderApproval OA ON OA.OrderID = OSU.OrderID
		WHERE OSU.OrderID = @ID

	/* compute the order level settlement rateIDs & settlement amounts - into temp table #I */
	SELECT OrderID = @ID
		, OrderDate
		, CarrierSettlementFactorID, MinSettlementUnitsID
		, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
		, RouteRateID, RangeRateID, LoadAmount
		, WaitFeeParameterID
		, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount 
		, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount
		, OrderRejectRateID, OrderRejectAmount
		, FuelSurchargeRateID, FuelSurchargeRate, FuelSurchargeAmount
		, OriginTaxRate
		, TotalAmount = ISNULL(LoadAmount, 0) 
			+ ISNULL(OriginWaitAmount, 0) 
			+ ISNULL(DestWaitAmount, 0) 
			+ ISNULL(OrderRejectAmount, 0) 
			+ ISNULL(FuelSurchargeAmount, 0)
		, CreatedByUser = @UserName
		, OriginChainUp, DestChainUp, H2S, Rerouted
	INTO #I
	FROM (
		SELECT OrderDate
			, CarrierSettlementFactorID, MinSettlementUnitsID
			, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
			, RouteRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RouteRateID ELSE NULL END
			, RangeRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RangeRateID ELSE NULL END
			, LoadAmount = round(ISNULL(@LoadAmount, RR.Amount), 2)
			, WaitFeeParameterID
			, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount = ROUND(OriginWaitAmount, 2)
			, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount = ROUND(DestWaitAmount, 2)
			, OrderRejectRateID = CASE WHEN @RejectionAmount IS NULL THEN RD.RateID ELSE NULL END
			, OrderRejectAmount = round(isnull(@RejectionAmount, RD.Amount), 2)
			, FuelSurchargeRateID = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.RateID ELSE NULL END
			, FuelSurchargeRate = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.Rate ELSE NULL END
			, FuelSurchargeAmount = round(isnull(@FuelSurchargeAmount, FSR.Amount), 2)
			, OriginTaxRate
			, OriginChainUp, DestChainup, H2S, Rerouted = cast(CASE WHEN RerouteCount = 0 THEN 0 ELSE 1 END as bit) /* used for the Auto-Approve logic below */
		FROM (
			SELECT O.ID
				, O.OriginID
				, O.CarrierID
				, O.RouteID
				, SU.CarrierSettlementFactorID
				, SU.SettlementFactorID
				, SU.SettlementUomID
				, SU.ActualUnits
				, SU.MinSettlementUnitsID
				, SU.MinSettlementUnits
				, SettlementUnits = CASE WHEN O.Rejected = 1 THEN 0 ELSE SU.SettlementUnits END
				, RouteMiles = isnull(OA.OverrideActualMiles, O.ActualMiles)
				, OrderDate = isnull(O.OrderDate, O.DueDate)
				, OriginChainUp = cast(CASE WHEN OA.OverrideOriginChainup = 1 THEN 0 ELSE O.OriginChainup END as bit)
				, DestChainUp = cast(CASE WHEN OA.OverrideDestChainup = 1 THEN 0 ELSE O.DestChainup END as bit)
				, H2S = cast(CASE WHEN OA.OverrideH2S = 1 THEN 0 ELSE O.H2S END as bit)
				, O.Rejected
				, RerouteCount = CASE WHEN OA.OverrideReroute = 1 THEN 0 ELSE O.RerouteCount END
				, IsSplit = CASE WHEN O.Rejected = 0 AND O.TicketCount > 1 THEN 1 ELSE 0 END
				, WaitFeeParameterID = WFP.ID
				, OriginWaitRateID = CASE WHEN @OriginWaitAmount IS NULL THEN OWD.RateID ELSE NULL END
				, OriginWaitBillableMinutes = OWD.BillableMinutes
				, OriginWaitBillableHours = OWD.BillableHours
				, OriginWaitAmount = isnull(@OriginWaitAmount, OWD.Amount)
				, DestWaitRateID = CASE WHEN @DestWaitAmount IS NULL THEN DWD.RateID ELSE NULL END
				, DestWaitBillableMinutes = DWD.BillableMinutes
				, DestWaitBillableHours = DWD.BillableHours
				, DestWaitAmount = isnull(@DestWaitAmount, DWD.Amount)
				, OriginTaxRate = OO.TaxRate
			FROM dbo.viewOrder O
			JOIN @SettlementUnits SU ON SU.OrderID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
			OUTER APPLY dbo.fnOrderDriverOriginWaitData(@ID) OWD 
			OUTER APPLY dbo.fnOrderDriverDestinationWaitData(@ID) DWD 
			OUTER APPLY dbo.fnOrderCarrierWaitFeeParameter(@ID) WFP
			WHERE O.ID = @ID
		) X
		OUTER APPLY dbo.fnOrderDriverLoadAmount(@ID, SettlementUnits, SettlementUomID) RR 
		OUTER APPLY dbo.fnOrderDriverOrderRejectData(@ID, SettlementUnits, SettlementUomID) RD
		OUTER APPLY dbo.fnOrderDriverFuelSurchargeData(@ID) FSR
	) X2

	DECLARE @CreatedTran bit; SET @CreatedTran = CASE WHEN @@TRANCOUNT = 0 THEN 1 ELSE 0 END
	BEGIN TRY
		IF (@CreatedTran = 1)
			BEGIN TRAN

		-- remove the existing settlment record (if any)
		DELETE FROM tblOrderSettlementDriverAssessorialCharge WHERE OrderID = @ID
		DELETE FROM tblOrderSettlementDriver WHERE OrderID = @ID
	
		-- all Units and Rates are first normalized to the Order.OriginUOM then consistent processing
		-- and persisted in the Invoice (Settlement) record in this Order.OriginUOM 
		INSERT INTO tblOrderSettlementDriver 
			(OrderID
			, OrderDate
			, CarrierSettlementFactorID
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnitsID
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestinationWaitRateID 
			, DestinationWaitBillableMinutes 
			, DestinationWaitBillableHours
			, DestinationWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser
			, Notes)
		SELECT OrderID
			, OrderDate
			, CarrierSettlementFactorID
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnitsID
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestWaitRateID 
			, DestWaitBillableMinutes 
			, DestWaitBillableHours
			, DestWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser
			, left(@Notes, 255)
		FROM #I

		/* compute the new Assessorial Rates for this order - into temp table #IA */
		/* NOTE: while it would be preferred for this to be out of the TX, it can't because it relies on the final Settlement changes being present */
		SELECT OrderID = @ID, AssessorialRateTypeID, AssessorialRateID = RateID, Amount = ROUND(Amount, 2), CreatedByUser = @UserName 
		INTO #IA
		FROM @SettlementUnits SU
		CROSS APPLY dbo.fnOrderDriverAssessorialAmounts(@ID, SU.SettlementUnits, SU.SettlementUomID,  (SELECT FinalActualMiles FROM viewOrder_Financial_Base_Driver where ID = @ID)) AA
		WHERE AA.AssessorialRateTypeID NOT IN (SELECT ID FROM @ManualAssessorialRates)

		/* apply the computed settlement assessorial rate/amount records to the DB */
		INSERT
		 INTO tblOrderSettlementDriverAssessorialCharge (OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser)
			SELECT OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser FROM #IA
			UNION
			SELECT @ID, ID, NULL, Amount, @UserName FROM @ManualAssessorialRates

		IF (@CreatedTran = 1)
			COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @msg varchar(max), @severity int
		SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY()
		ROLLBACK
		RAISERROR(@msg, @severity, 1)
		RETURN
	END CATCH	

	IF (@debugStatements = 1) PRINT 'spApplyRatesDriver (' + ltrim(@ID) + ') DONE ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	
END

GO




/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Shipper "Settlement" $$ values to an Delivered/Audited order
-- Changes:
-- 3.9.0	- 2015/08/13	- KDA	- add Auto-Approve logic
--										- honor Approve overrides in rating process
-- 3.9.9	- 2015/08/31	- KDA	- add new @AllowReApplyPostBatch parameter to allow re-applying after batch is applied
-- 3.9.19.2	- 2015/09/24	- KDA	- fix erroneous logic that computed Asssessorial Settlement results before the Order Level results were posted
-- 3.9.37	- 2015/12/22	- JAE	- Use tblOrderApproval to get override for Shipper Min Settlement Units
-- 3.9.39	- 2016/01/15	- JAE	- Also need to recalculate settlement units since the calculated/max values was already passed
-- 3.13.1	- 2016/07/04	- KDA	- add START/DONE PRINT statements (for debugging purposes)
-- 4.1.8.6	- 2016.09.21	- KDA	- move populate #IA table before transaction starts
--									- fix erroneous OR ActualMiles criteria (needed to be wrapped in () to avoid unnecessary processing)
--									- @debugStatements parameter + add additional START/DONE PRINT statements (for debugging purposes)
-- 4.1.9.1	- 2016.09.26	- KDA	- restore regressed 3.9.19.2 changes that accidentally got reverted with 4.1.8.6 (added a NOTE to hopefully prevent this again)
-- 4.4.1	- 2016/11/04	- JAE	- Add Destination Chainup
-- 4.6.2	- 2017.04.20	- KDA	- add @Notes parameter (and logic to persist to DB)
-- 4.6.4	- 2017-04-28	- JAE	- Added Miles to the Assessorial Amounts function call
/**********************************/
ALTER PROCEDURE spApplyRatesShipper
(
  @ID int
, @UserName varchar(100)
, @OriginWaitAmount money = NULL
, @DestWaitAmount money = NULL
, @RejectionAmount money = NULL
, @FuelSurchargeAmount money = NULL
, @LoadAmount money = NULL
, @Notes varchar(255) = NULL
, @AssessorialRateTypeID_CSV varchar(max) = NULL
, @AssessorialAmount_CSV varchar(max) = null
, @ResetOverrides bit = 0
, @AllowReApplyPostBatch bit = 0
, @debugStatements bit = 0 -- turn on to get debugging statements during execution
) AS BEGIN

	SET NOCOUNT ON
	
	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ') START ' + (CONVERT(VARCHAR(24), GETDATE(), 121))

	-- ensure this order hasn't yet been fully settled
	IF (@AllowReApplyPostBatch = 0) AND EXISTS (SELECT * FROM tblOrderSettlementShipper WHERE OrderID = @ID AND BatchID IS NOT NULL)
	BEGIN
		RAISERROR('Invoice has already been Shipper Settled', 16, 1)
		RETURN
	END

	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ').UpdateActualMiles START ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	-- ensure the current Route.ActualMiles is assigned to the order
	UPDATE tblOrder SET ActualMiles = R.ActualMiles
	FROM tblOrder O
	JOIN tblRoute R ON R.ID = O.RouteID
	LEFT JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = O.ID
	WHERE O.ID = @ID
	  AND (O.ActualMiles IS NULL OR O.ActualMiles <> R.ActualMiles)
	  AND (@AllowReApplyPostBatch = 1 OR OSS.BatchID IS NULL)
	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ').UpdateActualMiles DONE ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	
	/* transform the provided manual Assessorial Rates (if any) into a queryable table variable */
	DECLARE @ManualAssessorialRates TABLE (ID int primary key, Amount money)
	INSERT INTO @ManualAssessorialRates (ID, Amount) 
		SELECT ID.ID, R.Amount
		FROM (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), ID FROM dbo.fnSplitCSVIDs(@AssessorialRateTypeID_CSV)) ID
		JOIN (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), amount = CAST(value as money) FROM dbo.fnSplitCSV(@AssessorialAmount_CSV)) R ON R.sortid = ID.sortid

	/* persist all existing MANUAL Assessorial Rates from the existing Order Settlement (if any present and not resetting them) */
	INSERT INTO @ManualAssessorialRates (ID, Amount)
		SELECT AssessorialRateTypeID, Amount 
		FROM dbo.tblOrderSettlementShipperAssessorialCharge 
		WHERE OrderID = @ID AND AssessorialRateID IS NULL 
			AND ID NOT IN (SELECT ID FROM @ManualAssessorialRates)
			AND @ResetOverrides = 0

	/* persist all existing settlement amount overrides from the existing Order Settlement - if not resetting them */
	IF (@ResetOverrides = 0)
	BEGIN 
		SELECT @OriginWaitAmount = OriginWaitAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @OriginWaitAmount IS NULL AND OriginWaitRateID IS NULL
		SELECT @DestWaitAmount = DestinationWaitAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @DestWaitAmount IS NULL AND DestinationWaitRateID IS NULL
		SELECT @RejectionAmount = OrderRejectAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @RejectionAmount IS NULL AND OrderRejectRateID IS NULL
		SELECT @FuelSurchargeAmount = FuelSurchargeAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @FuelSurchargeAmount IS NULL AND FuelSurchargeRateID IS NULL
		SELECT @LoadAmount = LoadAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @LoadAmount IS NULL AND RouteRateID IS NULL AND RangeRateID IS NULL
	END
	/* persist any existing notes by preserving them if no new note is defined */
	SELECT @Notes = Notes FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @Notes IS NULL AND Notes IS NOT NULL

	/* compute the basic settlement units from the Order being invoices */
	DECLARE @SettlementUnits TABLE
	(
		OrderID int
	  , SettlementUomID int
	  , SettlementFactorID int
	  , ShipperSettlementFactorID int
	  , MinSettlementUnits decimal(18, 10)
	  , MinSettlementUnitsID int
	  , ActualUnits decimal(18, 10)
	  , SettlementUnits decimal(18, 10)
	)
	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ').RetrieveSettlementUnits START ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	INSERT INTO @SettlementUnits (OrderID, SettlementUomID, SettlementFactorID, ShipperSettlementFactorID, MinSettlementUnits, MinSettlementUnitsID, ActualUnits, SettlementUnits)
		SELECT OSU.OrderID, OSU.SettlementUomID, OSU.SettlementFactorID, OSU.ShipperSettlementFactorID, ISNULL(OA.OverrideShipperMinSettlementUnits, OSU.MinSettlementUnits), OSU.MinSettlementUnitsID, OSU.ActualUnits, 
			-- use passed settlement (already calculated) or pick the max between the actual and overrride min - JAE 1/15/16 - 3.9.39
			CASE WHEN OA.OverrideShipperMinSettlementUnits IS NULL THEN OSU.SettlementUnits
						ELSE dbo.fnMaxDecimal(OSU.ActualUnits, OA.OverrideShipperMinSettlementUnits) END
		FROM dbo.viewOrderSettlementUnitsShipper OSU
		LEFT JOIN tblOrderApproval OA ON OA.OrderID = OSU.OrderID
		WHERE OSU.OrderID = @ID
	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ').RetrieveSettlementUnits DONE ' + (CONVERT(VARCHAR(24), GETDATE(), 121))

	/* compute the order level settlement rateIDs & settlement amounts - into temp table #I */
	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ').RetrieveData START ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	SELECT OrderID = @ID
		, OrderDate
		, ShipperSettlementFactorID, MinSettlementUnitsID
		, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
		, RouteRateID, RangeRateID, LoadAmount
		, WaitFeeParameterID
		, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount 
		, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount
		, OrderRejectRateID, OrderRejectAmount
		, FuelSurchargeRateID, FuelSurchargeRate, FuelSurchargeAmount
		, OriginTaxRate
		, TotalAmount = ISNULL(LoadAmount, 0) 
			+ ISNULL(OriginWaitAmount, 0) 
			+ ISNULL(DestWaitAmount, 0) 
			+ ISNULL(OrderRejectAmount, 0) 
			+ ISNULL(FuelSurchargeAmount, 0)
		, CreatedByUser = @UserName
		, OriginChainup, DestChainup, H2S, Rerouted /* used for the Auto-Approve logic below */
	INTO #I
	FROM (
		SELECT OrderDate
			, ShipperSettlementFactorID, MinSettlementUnitsID
			, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
			, RouteRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RouteRateID ELSE NULL END
			, RangeRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RangeRateID ELSE NULL END
			, LoadAmount = round(ISNULL(@LoadAmount, RR.Amount), 2)
			, WaitFeeParameterID
			, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount = ROUND(OriginWaitAmount, 2)
			, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount = ROUND(DestWaitAmount, 2)
			, OrderRejectRateID = CASE WHEN @RejectionAmount IS NULL THEN RD.RateID ELSE NULL END
			, OrderRejectAmount = round(isnull(@RejectionAmount, RD.Amount), 2)
			, FuelSurchargeRateID = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.RateID ELSE NULL END
			, FuelSurchargeRate = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.Rate ELSE NULL END
			, FuelSurchargeAmount = round(isnull(@FuelSurchargeAmount, FSR.Amount), 2)
			, OriginTaxRate
			, OriginChainup, DestChainup, H2S, Rerouted = cast(CASE WHEN RerouteCount = 0 THEN 0 ELSE 1 END as bit) /* used for the Auto-Approve logic below */
		FROM (
			SELECT O.ID
				, O.OriginID
				, O.CustomerID
				, O.RouteID
				, SU.ShipperSettlementFactorID
				, SU.SettlementFactorID
				, SU.SettlementUomID
				, SU.ActualUnits
				, SU.MinSettlementUnitsID
				, SU.MinSettlementUnits
				, SettlementUnits = CASE WHEN O.Rejected = 1 THEN 0 ELSE SU.SettlementUnits END
				, RouteMiles = isnull(OA.OverrideActualMiles, O.ActualMiles)
				, OrderDate = isnull(O.OrderDate, O.DueDate)
				, OriginChainUp = cast(CASE WHEN OA.OverrideOriginChainup = 1 THEN 0 ELSE O.OriginChainup END as bit)
				, DestChainUp = cast(CASE WHEN OA.OverrideDestChainup = 1 THEN 0 ELSE O.DestChainup END as bit)
				, H2S = cast(CASE WHEN OA.OverrideH2S = 1 THEN 0 ELSE O.H2S END as bit)
				, O.Rejected
				, RerouteCount = CASE WHEN OA.OverrideReroute = 1 THEN 0 ELSE O.RerouteCount END
				, IsSplit = CASE WHEN O.Rejected = 0 AND O.TicketCount > 1 THEN 1 ELSE 0 END
				, WaitFeeParameterID = WFP.ID
				, OriginWaitRateID = CASE WHEN @OriginWaitAmount IS NULL THEN OWD.RateID ELSE NULL END
				, OriginWaitBillableMinutes = OWD.BillableMinutes
				, OriginWaitBillableHours = OWD.BillableHours
				, OriginWaitAmount = isnull(@OriginWaitAmount, OWD.Amount)
				, DestWaitRateID = CASE WHEN @DestWaitAmount IS NULL THEN DWD.RateID ELSE NULL END
				, DestWaitBillableMinutes = DWD.BillableMinutes
				, DestWaitBillableHours = DWD.BillableHours
				, DestWaitAmount = isnull(@DestWaitAmount, DWD.Amount)
				, OriginTaxRate = OO.TaxRate
			FROM dbo.viewOrder O
			JOIN @SettlementUnits SU ON SU.OrderID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
			OUTER APPLY dbo.fnOrderShipperOriginWaitData(@ID) OWD 
			OUTER APPLY dbo.fnOrderShipperDestinationWaitData(@ID) DWD 
			OUTER APPLY dbo.fnOrderShipperWaitFeeParameter(@ID) WFP
			WHERE O.ID = @ID
		) X
		OUTER APPLY dbo.fnOrderShipperLoadAmount(@ID, SettlementUnits, SettlementUomID) RR 
		OUTER APPLY dbo.fnOrderShipperOrderRejectData(@ID, SettlementUnits, SettlementUomID) RD
		OUTER APPLY dbo.fnOrderShipperFuelSurchargeData(@ID) FSR
	) X2
	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ').RetrieveData DONE ' + (CONVERT(VARCHAR(24), GETDATE(), 121))

	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ').StoreData START ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	DECLARE @CreatedTran bit; SET @CreatedTran = CASE WHEN @@TRANCOUNT = 0 THEN 1 ELSE 0 END
	BEGIN TRY
		IF (@CreatedTran = 1)
			BEGIN TRAN

		-- remove the existing settlment record (if any)
		DELETE FROM tblOrderSettlementShipperAssessorialCharge WHERE OrderID = @ID
		DELETE FROM tblOrderSettlementShipper WHERE OrderID = @ID

		-- all Units and Rates are first normalized to the Order.OriginUOM then consistent processing
		-- and persisted in the Invoice (Settlement) record in this Order.OriginUOM 
		INSERT INTO tblOrderSettlementShipper (
/*1*/		OrderID
/*2*/		, OrderDate
/*3*/		, ShipperSettlementFactorID
/*4*/		, SettlementFactorID 
/*5*/		, SettlementUomID 
/*6*/		, MinSettlementUnitsID
/*7*/		, MinSettlementUnits 
/*8*/		, SettlementUnits
/*9*/		, RouteRateID
/*10*/		, RangeRateID 
/*11*/		, LoadAmount
/*12*/		, WaitFeeParameterID 
/*13*/		, OriginWaitRateID 
/*14*/		, OriginWaitBillableMinutes 
/*15*/		, OriginWaitBillableHours
/*16*/		, OriginWaitAmount 
/*17*/		, DestinationWaitRateID 
/*18*/		, DestinationWaitBillableMinutes 
/*19*/		, DestinationWaitBillableHours
/*20*/		, DestinationWaitAmount 
/*21*/		, OrderRejectRateID 
/*22*/		, OrderRejectAmount 
/*23*/		, FuelSurchargeRateID 
/*24*/		, FuelSurchargeRate 
/*25*/		, FuelSurchargeAmount 
/*26*/		, OriginTaxRate 
/*27*/		, TotalAmount 
/*28*/		, CreatedByUser
/*29*/		, Notes)
SELECT 
/*01*/		OrderID
/*02*/		, OrderDate
/*03*/		, ShipperSettlementFactorID
/*04*/		, SettlementFactorID 
/*05*/		, SettlementUomID 
/*06*/		, MinSettlementUnitsID
/*07*/		, MinSettlementUnits 
/*08*/		, SettlementUnits
/*09*/		, RouteRateID
/*10*/		, RangeRateID 
/*11*/		, LoadAmount
/*12*/		, WaitFeeParameterID 
/*13*/		, OriginWaitRateID 
/*14*/		, OriginWaitBillableMinutes 
/*15*/		, OriginWaitBillableHours
/*16*/		, OriginWaitAmount 
/*17*/		, DestWaitRateID 
/*18*/		, DestWaitBillableMinutes 
/*19*/		, DestWaitBillableHours
/*20*/		, DestWaitAmount 
/*21*/		, OrderRejectRateID 
/*22*/		, OrderRejectAmount 
/*23*/		, FuelSurchargeRateID 
/*24*/		, FuelSurchargeRate 
/*25*/		, FuelSurchargeAmount 
/*26*/		, OriginTaxRate 
/*27*/		, TotalAmount 
/*28*/		, CreatedByUser
/*29*/		, left(@Notes, 255)
		FROM #I

		IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ').RetrieveAssessorialData START ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
		/* compute the new Assessorial Rates for this order - into temp table #IA */
		/* NOTE: while it would be preferred for this to be out of the TX, it can't because it relies on the final Settlement changes being present */
		SELECT OrderID = @ID, AssessorialRateTypeID, AssessorialRateID = RateID, Amount = ROUND(Amount, 2), CreatedByUser = @UserName 
		INTO #IA
		FROM @SettlementUnits SU
		CROSS APPLY dbo.fnOrderShipperAssessorialAmounts(@ID, SU.SettlementUnits, SU.SettlementUomID, (SELECT FinalActualMiles FROM viewOrder_Financial_Base_Shipper where ID = @ID)) CAA
		WHERE CAA.AssessorialRateTypeID NOT IN (SELECT ID FROM @ManualAssessorialRates)
		IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ').RetrieveAssessorialData DONE ' + (CONVERT(VARCHAR(24), GETDATE(), 121))

		/* apply the computed settlement assessorial rate/amount records to the DB */
		INSERT INTO tblOrderSettlementShipperAssessorialCharge (OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser)
			SELECT OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser FROM #IA
			UNION
			SELECT @ID, ID, NULL, Amount, @UserName FROM @ManualAssessorialRates

		IF (@CreatedTran = 1)
			COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @msg varchar(max), @severity int
		SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY()
		ROLLBACK
		RAISERROR(@msg, @severity, 1)
		RETURN
	END CATCH	
	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ').StoreData DONE ' + (CONVERT(VARCHAR(24), GETDATE(), 121))

	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ') DONE ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	
END

GO


/*********************************************
-- Date Created: 22 Feb 2014
-- Author: Kevin Alons
-- Purpose: Concatenate the Assessorial Descriptions for a single order into a tab-separated string
-- Changes:
--		4.4.1		11/16/2016		JAE			Add check for system (not assume only 4 system settings)
--		4.6.4		5/1/2017		JAE			Clean up rate prefix and suffix to also include per mile option
*********************************************/
ALTER FUNCTION fnOrderShipperAssessorialDetailsTSV(@OrderID int, @IncludeSystemRates bit = 0) RETURNS varchar(max) AS
BEGIN
	DECLARE @ret VARCHAR(max) 

	SELECT @ret = isnull(@ret + CHAR(13), '') 
		+ Name + isnull(CHAR(9) + RatePrefix + ltrim(Rate) + RateSuffix + CHAR(9) + ltrim(TypeID), '')
	FROM (
		SELECT ART.Name 
			, TypeID = AC.AssessorialRateTypeID
			, Rate = cast(AR.Rate as decimal(18, 4))
			, RatePrefix = CASE RT.ID WHEN 1 THEN '$' WHEN 2 THEN '$' WHEN 5 THEN '$' ELSE '' END
			, RateSuffix = CASE RT.ID WHEN 1 THEN '/' + U.Abbrev WHEN 2 THEN '$' WHEN 5 THEN '/Mile' ELSE RT.Name END
			, AC.Amount
		FROM tblOrderSettlementShipperAssessorialCharge AC
		JOIN tblAssessorialRateType ART ON ART.ID = AC.AssessorialRateTypeID
		LEFT JOIN tblShipperAssessorialRate AR ON AR.ID = AC.AssessorialRateID
		LEFT JOIN tblRateType RT ON RT.ID = AR.RateTypeID 
		LEFT JOIN tblUom U ON U.ID = AR.UomID
		WHERE AC.OrderID = @OrderID
		  AND (@IncludeSystemRates = 1 OR ART.IsSystem = 0)
	) X
		
	RETURN (@ret)
END

GO


/*********************************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: Concatenate the Assessorial Descriptions for a single order into a tab-separated string
-- Changes:
--		4.4.1		11/16/2016		JAE			Add check for system (not assume only 4 system settings)
--		4.6.4		5/1/2017		JAE			Clean up rate prefix and suffix to also include per mile option
/*********************************************/
ALTER FUNCTION fnOrderDriverAssessorialDetailsTSV(@OrderID int, @IncludeSystemRates bit = 0) RETURNS varchar(max) AS
BEGIN
	DECLARE @ret VARCHAR(max) 

	SELECT @ret = isnull(@ret + CHAR(13), '') 
		+ Name + isnull(CHAR(9) + RatePrefix + ltrim(Rate) + RateSuffix + CHAR(9) + ltrim(TypeID), '')
	FROM (
		SELECT ART.Name 
			, TypeID = AC.AssessorialRateTypeID
			, Rate = cast(AR.Rate as decimal(18, 4))
			, RatePrefix = CASE RT.ID WHEN 1 THEN '$' WHEN 2 THEN '$' WHEN 5 THEN '$' ELSE '' END
			, RateSuffix = CASE RT.ID WHEN 1 THEN '/' + U.Abbrev WHEN 2 THEN '$' WHEN 5 THEN '/Mile' ELSE RT.Name END
			, AC.Amount
		FROM tblOrderSettlementDriverAssessorialCharge AC
		JOIN tblAssessorialRateType ART ON ART.ID = AC.AssessorialRateTypeID
		LEFT JOIN tblDriverAssessorialRate AR ON AR.ID = AC.AssessorialRateID
		LEFT JOIN tblRateType RT ON RT.ID = AR.RateTypeID 
		LEFT JOIN tblUom U ON U.ID = AR.UomID
		WHERE AC.OrderID = @OrderID
		  AND (@IncludeSystemRates = 1 OR ART.IsSystem = 0)
	) X
		
	RETURN (@ret)
END

GO



/*********************************************
-- Date Created: 22 Feb 2014
-- Author: Kevin Alons
-- Purpose: Concatenate the Assessorial Descriptions for a single order into a tab-separated string
--		4.4.1		11/16/2016		JAE			Add check for system (not assume only 4 system settings)
--		4.6.4		5/1/2017		JAE			Clean up rate prefix and suffix to also include per mile option
*********************************************/
ALTER FUNCTION fnOrderCarrierAssessorialDetailsTSV(@OrderID int, @IncludeSystemRates bit = 0) RETURNS varchar(max) AS
BEGIN
	DECLARE @ret VARCHAR(max) 

	SELECT @ret = isnull(@ret + CHAR(13), '') 
		+ Name + isnull(CHAR(9) + RatePrefix + ltrim(Rate) + RateSuffix + CHAR(9) + ltrim(TypeID), '')
	FROM (
		SELECT ART.Name 
			, TypeID = AC.AssessorialRateTypeID
			, Rate = cast(AR.Rate as decimal(18, 4))
			, RatePrefix = CASE RT.ID WHEN 1 THEN '$' WHEN 2 THEN '$' WHEN 5 THEN '$' ELSE '' END
			, RateSuffix = CASE RT.ID WHEN 1 THEN '/' + U.Abbrev WHEN 2 THEN '$' WHEN 5 THEN '/Mile' ELSE RT.Name END
			, AC.Amount
		FROM tblOrderSettlementCarrierAssessorialCharge AC
		JOIN tblAssessorialRateType ART ON ART.ID = AC.AssessorialRateTypeID
		LEFT JOIN tblCarrierAssessorialRate AR ON AR.ID = AC.AssessorialRateID
		LEFT JOIN tblRateType RT ON RT.ID = AR.RateTypeID 
		LEFT JOIN tblUom U ON U.ID = AR.UomID
		WHERE AC.OrderID = @OrderID
		  AND (@IncludeSystemRates = 1 OR ART.IsSystem = 0)
	) X
		
	RETURN (@ret)
END

GO


COMMIT
SET NOEXEC OFF