DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '1.6.0', @NewVersion = '1.6.1'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

ALTER TABLE dbo.tblTruckInspectionType
	DROP CONSTRAINT DF_TruckInspectionType_Required
GO
CREATE TABLE dbo.Tmp_tblTruckInspectionType
	(
	ID int NOT NULL IDENTITY (1, 1),
	Name varchar(50) NOT NULL,
	ExpirationMonths int NOT NULL,
	Required bit NOT NULL,
	CreateDate smalldatetime NULL,
	CreatedByUser varchar(100) NULL,
	LastChangeDate smalldatetime NULL,
	LastChangedByUser varchar(100) NULL,
	DeleteDate smalldatetime NULL,
	DeletedByUser varchar(100) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_tblTruckInspectionType SET (LOCK_ESCALATION = TABLE)
GO
GRANT DELETE ON dbo.Tmp_tblTruckInspectionType TO dispatchcrude_iis_acct  AS dbo
GO
GRANT INSERT ON dbo.Tmp_tblTruckInspectionType TO dispatchcrude_iis_acct  AS dbo
GO
GRANT SELECT ON dbo.Tmp_tblTruckInspectionType TO dispatchcrude_iis_acct  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_tblTruckInspectionType TO dispatchcrude_iis_acct  AS dbo
GO
ALTER TABLE dbo.Tmp_tblTruckInspectionType ADD CONSTRAINT
	DF_TruckInspectionType_Required DEFAULT ((1)) FOR Required
GO
SET IDENTITY_INSERT dbo.Tmp_tblTruckInspectionType ON
GO
IF EXISTS(SELECT * FROM dbo.tblTruckInspectionType)
	 EXEC('INSERT INTO dbo.Tmp_tblTruckInspectionType (ID, Name, ExpirationMonths, Required, CreateDate, CreatedByUser, LastChangeDate, LastChangedByUser, DeleteDate, DeletedByUser)
		SELECT ID, Name, ExpirationMonths, Required, CreateDate, CreatedByUser, LastChangeDate, LastChangedByUser, DeleteDate, DeletedByUser FROM dbo.tblTruckInspectionType WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_tblTruckInspectionType OFF
GO
ALTER TABLE dbo.tblTruckInspection
	DROP CONSTRAINT FK_TruckInspection_Type
GO
DROP TABLE dbo.tblTruckInspectionType
GO
EXECUTE sp_rename N'dbo.Tmp_tblTruckInspectionType', N'tblTruckInspectionType', 'OBJECT' 
GO
ALTER TABLE dbo.tblTruckInspectionType ADD CONSTRAINT
	PK_TruckInspectionType PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.tblTruckInspection ADD CONSTRAINT
	FK_TruckInspection_Type FOREIGN KEY
	(
	TruckInspectionTypeID
	) REFERENCES dbo.tblTruckInspectionType
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblTruckInspection SET (LOCK_ESCALATION = TABLE)
GO

EXEC _spRefreshAllViews

COMMIT
SET NOEXEC OFF