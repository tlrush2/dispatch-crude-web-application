SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.4.22'
SELECT  @NewVersion = '4.4.22.1'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'JT-181 Allow HOS filtering of hours'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/****************************************************/
-- Created: 2016.09.14 - 4.1.7
-- Author: Joe Engler
-- Purpose: Retrieve the drivers (without last known GPS coordinates) for the specified filter criteria
-- Changes:
--	4.1.8.3		JAE		2016-09-21		Fixed reference to current time to start time (passed in)
--	4.1.10.1	JAE		2016-09-27		Added qualifier to DeleteDateUTC column (since same column was added to route table)
--	4.2.0		JAE		2016-09-30		Add hos fields to view
--	4.2.5		JAE		2016-10-26		Use operating state (instead of state) to carrier rule call
--											Added Weekly OnDuty fields
--	4.3.2		KDA		2016.11.07		replace tblDriverAvailability references with viewDriverSchedule (tblDriverSchedule)
--	4.4.14		JAE		2016-12-22		Update compliance check to use new compliance tables			
--  4.4.22.1	JAE		2017-01-31		Join HOS policy to get default times when no history occurs, update hours left based on date
/****************************************************/
ALTER FUNCTION fnRetrieveEligibleDrivers_NoGPS(@CarrierID INT, @RegionID INT, @StateID INT, @DriverGroupID INT, @StartDate DATETIME) 
RETURNS @ret TABLE 
(
	ID INT, 
	LastName VARCHAR(20), FirstName VARCHAR(20), FullName VARCHAR(41), FullNameLF VARCHAR(42),
	CarrierID INT, Carrier VARCHAR(40),
	RegionID INT,
	TruckID INT, Truck VARCHAR(10),
	TrailerID INT, Trailer VARCHAR(10),
	AvailabilityFactor DECIMAL(4,2),
	ComplianceFactor DECIMAL(4,2),
	TruckAvailabilityFactor DECIMAL(4,2),
	CurrentWorkload INT,
	CurrentECOT INT,
	HOSFactor DECIMAL(4,2),
	OnDutyHoursSinceWeeklyReset DECIMAL(6,2),
	WeeklyOnDutyHoursLeft DECIMAL(6,2),
	DrivingHoursSinceWeeklyReset DECIMAL(6,2),
	WeeklyDrivingHoursLeft DECIMAL(6,2),
	OnDutyHoursSinceDailyReset DECIMAL(6,2),
	OnDutyHoursLeft DECIMAL(6,2),
	DrivingHoursSinceDailyReset DECIMAL(6,2),
	DrivingHoursLeft DECIMAL(6,2),
	HOSHrsOnShift DECIMAL(6,2),
	DriverScore INT 
)
AS BEGIN
	DECLARE @__HOS_POLICY__ INT = 1
	DECLARE @__ENFORCE_DRIVER_SCHEDULING__ INT = 13
	DECLARE @__ENFORCE_DRIVER_COMPLIANCE__ INT = 14
	DECLARE @__ENFORCE_HOS__ INT = 15
	
	IF @StartDate IS NULL 
	   SELECT @StartDate = dbo.fnDateOnly(GETDATE())

	INSERT INTO @ret
	SELECT X.*,
		DriverScore = 100 * AvailabilityFactor * ComplianceFactor * TruckAvailabilityFactor * HOSFactor
	FROM (
		SELECT d.ID, 
			d.FirstName, d.LastName, FullName, d.FullNameLF,
			d.CarrierID, d.Carrier,
			d.RegionID,
			d.TruckID, d.Truck,
			d.TrailerID, d.Trailer,

			--Availability
			AvailabilityFactor = CASE WHEN cr_a.Value IS NULL OR dbo.fnToBool(cr_a.Value) = 0 THEN 1 -- scheduling not enforced
										ELSE isnull(DS.OnDuty, 0) END,

			-- Compliance
			ComplianceFactor = CASE WHEN cr_c.Value IS NULL OR dbo.fnToBool(cr_c.Value) = 0 THEN 1 -- compliance not enforced
									WHEN (select count(*) from fnDriverComplianceSummary(0, 0) WHERE DriverID = d.ID) > 0 THEN 0
									ELSE 1 END, -- all ok

			--Truck/Trailer Availability
			TruckAvailabilityFactor = 1,
			
			--current workload, time on shift
			o.CurrentWorkload,
			CurrentECOT = ISNULL(o.CurrentECOT, 0),

			-- HOS
			HOSFactor = CASE WHEN cr_h.Value IS NULL OR dbo.fnToBool(cr_h.Value) = 0 THEN 1 -- HOS not enforced
							WHEN @StartDate > DATEADD(HOUR, policy.WeeklyResetHR, GETUTCDATE()) THEN 1 -- Equivalent of next "week"
							WHEN @StartDate > GETUTCDATE() AND hos.WeeklyOnDutyHoursLeft <= 0 THEN 0 -- check tomorrow (out of weekly hours)
							WHEN @StartDate > GETUTCDATE() AND hos.WeeklyOnDutyHoursLeft > 0 THEN 1 -- check tomorrow (ignore daily numbers)
							WHEN hos.WeeklyOnDutyHoursLeft <= 0 OR hos.OnDutyHoursLeft <= 0 OR hos.DrivingHoursLeft <= 0 THEN 0 -- check today
							-- compare ECOT to hours and see
							ELSE 1 END,
			hos.OnDutyHoursSinceWeeklyReset,
			WeeklyOnDutyHoursLeft = CASE WHEN @StartDate > DATEADD(HOUR, policy.WeeklyResetHR, GETUTCDATE()) THEN policy.OnDutyWeeklyLimitHR ELSE ISNULL(hos.WeeklyOnDutyHoursLeft, policy.OnDutyWeeklyLimitHR) END,
			hos.DrivingHoursSinceWeeklyReset,
			WeeklyDrivingHoursLeft = CASE WHEN @StartDate > GETUTCDATE() THEN policy.DrivingWeeklyLimitHR ELSE ISNULL(hos.WeeklyDrivingHoursLeft, policy.DrivingWeeklyLimitHR) END,
			hos.OnDutyHoursSinceDailyReset,
			OnDutyHoursLeft = CASE WHEN @StartDate > GETUTCDATE() THEN policy.OnDutyDailyLimitHR ELSE hos.OnDutyHoursLeft END,
			hos.DrivingHoursSinceDailyReset,
			DrivingHoursLeft = CASE WHEN @StartDate > GETUTCDATE() THEN policy.DrivingDailyLimitHR ELSE hos.DrivingHoursLeft END,
			HrsOnShift = CAST(DATEDIFF(MINUTE, ds.StartDateTime, getdate())/60.0*isnull(ds.OnDuty, 0) AS DECIMAL(5,1)) -- from DriverScheduling
			/* return the number of hours the Driver Scheduling module defines the driver is Scheduled on the specified day */
			--,ShiftHours = dbo.fnMinInt(DS.DurationHours, 24 - DS.StartHours % 24)-- from DriverScheduling

		FROM viewDriver d
		LEFT JOIN (SELECT * FROM viewDriverSchedule WHERE OnDuty = 1) ds ON ds.DriverID = d.ID AND ScheduleDate = CAST(@StartDate AS DATE)
		OUTER APPLY dbo.fnCarrierRules(@StartDate, null, @__ENFORCE_DRIVER_SCHEDULING__, d.CarrierID, d.ID, d.OperatingStateID, d.RegionID, 1) cr_a -- Enforce Driver Scheduling Carrier Rule
		OUTER APPLY dbo.fnCarrierRules(@StartDate, null, @__ENFORCE_DRIVER_COMPLIANCE__, d.CarrierID, d.ID, d.OperatingStateID, d.RegionID, 1) cr_c
		OUTER APPLY dbo.fnCarrierRules(@StartDate, null, @__ENFORCE_HOS__, d.CarrierID, d.ID, d.OperatingStateID, d.RegionID, 1) cr_h
		OUTER APPLY (SELECT TOP 1 * FROM dbo.fnHosViolationDetail(d.ID, null, @StartDate) ORDER BY LogDateUTC DESC) hos
		OUTER APPLY (SELECT TOP 1 * FROM viewHosPolicy WHERE ID = ISNULL((SELECT TOP 1 p.ID FROM tblHosPolicy p WHERE Name = 
												(SELECT Value FROM dbo.fnCarrierRules(@StartDate, null, 1 /* HOS Policy Carrier Rule */, d.CarrierID, d.ID, d.OperatingStateID, d.RegionID, 1)))
									, 1)) policy
		OUTER APPLY (
			SELECT CurrentWorkload = COUNT(*), 
					CurrentECOT = SUM(CASE WHEN o.StatusID = 8 THEN ISNULL(r.ECOT, 60+ISNULL(r.ActualMiles,60)+60)/2
										   ELSE COALESCE(r.ECOT, 60+ISNULL(r.ActualMiles,60)+60) END)
			FROM tblOrder o LEFT JOIN tblRoute r ON o.RouteID = r.ID
			WHERE o.DriverID = d.ID
			  AND o.StatusID IN (2,7,8) -- Dispatched, Accepted, Picked Up
			  AND o.DeleteDateUTC IS NULL) o

		WHERE d.DeleteDateUTC IS NULL
			AND d.TruckID IS NOT NULL AND d.TrailerID IS NOT NULL
			AND (ISNULL(@CarrierID, 0) <= 0 OR @CarrierID = d.CarrierID)
			AND (ISNULL(@DriverGroupID, 0) <= 0 OR @DriverGroupID = d.ID)
			AND (ISNULL(@RegionID, -1) <= 0 OR @RegionID = d.RegionID)
			AND (ISNULL(@StateID, 0) <= 0 OR @StateID = d.StateID)
	) AS X
	ORDER BY DriverScore DESC, LastName, FirstName

	RETURN
END

GO


COMMIT
SET NOEXEC OFF