SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.6.6'
SELECT  @NewVersion = '4.6.7'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'JT-3642 - Add new edit permission to dispatch board'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


-- Create new edit permission for dispatch board
EXEC spAddNewPermission 'editDispatchBoard', 'Allow user to use the Dispatch Board page to process orders (requires Dispatch - View permission)', 'Dispatch', 'Dispatch Board - Edit'
GO

-- Grant edit to all users that currently have view access
EXEC spAddNewPermissionToGroupsWithExistingRole 'viewDispatchBoard', 'editDispatchBoard'
GO


-- Update view permission to reflect read-only-ness
UPDATE aspnet_Roles 
SET FriendlyName = 'Dispatch Board - View', 
	Description =  'Allow user to see the Dispatch Board page - Read Only (requires Dispatch - View permission)' 
WHERE RoleName='viewDispatchBoard'

GO


-- Add system setting for hiding unavailable drivers from dispatch board
INSERT INTO tblSetting
SELECT 72, 'Hide Unavailable Drivers - Dispatch Planner', 2, 'False', 'Order Entry Rules', GETUTCDATE(), 'System', null, null, 0, 'When set to TRUE, the dispatch planner will omit unavailable drivers from the page', 0
WHERE NOT EXISTS (SELECT 1 FROM tblSetting WHERE ID = 72)

GO


COMMIT
SET NOEXEC OFF