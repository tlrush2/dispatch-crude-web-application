/*  
	-- add the tblOrderSignature table
*/ 
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.7.4.1'
SELECT  @NewVersion = '2.7.5'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

CREATE TABLE tblOrderSignature
(
  ID int identity(1, 1) NOT NULL CONSTRAINT PK_OrderSignature PRIMARY KEY
, UID uniqueidentifier NOT NULL CONSTRAINT DF_OrderSignature_UID DEFAULT (newid())
, OrderID int NOT NULL CONSTRAINT FK_OrderSignature_Order FOREIGN KEY REFERENCES tblOrder(ID)
, OriginID int NULL CONSTRAINT FK_OrderSignature_Origin FOREIGN KEY REFERENCES tblOrigin(ID)
, DestinationID int NULL CONSTRAINT FK_OrderSignature_Destination FOREIGN KEY REFERENCES tblDestination(ID)
, DriverID int NULL CONSTRAINT FK_OrderSignature_Driver FOREIGN KEY REFERENCES tblDriver(ID)
, SignatureBlob varbinary(max) NULL
, CreateDateUTC smalldatetime NOT NULL CONSTRAINT DF_OrderSignature_CreateDateUTC DEFAULT (getutcdate())
, CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_OrderSignature_CreatedByUser DEFAULT ('System')
)
GO
GRANT SELECT, INSERT, DELETE ON tblOrderSignature TO dispatchcrude_iis_acct
GO

COMMIT
SET NOEXEC OFF