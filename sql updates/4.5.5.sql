SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.5.4'
SELECT  @NewVersion = '4.5.5'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'JT-219 - Compliance Report'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO



-- Add new Compliance summary report permission
EXEC spAddNewPermission 'viewComplianceReport', 'Allow user to view compliance summary report', 'Assets - Compliance', 'Compliance Summary Report'
GO

-- Add new Report view to anyone with viewXXXCompliance 
EXEC spAddNewPermissionToGroupsWithExistingRole 'viewCarrierCompliance', 'viewComplianceReport'
GO
EXEC spAddNewPermissionToGroupsWithExistingRole 'viewDriverCompliance', 'viewComplianceReport'
GO
EXEC spAddNewPermissionToGroupsWithExistingRole 'viewTruckCompliance', 'viewComplianceReport'
GO
EXEC spAddNewPermissionToGroupsWithExistingRole 'viewTrailerCompliance', 'viewComplianceReport'
GO


COMMIT
SET NOEXEC OFF