-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.9.18.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.9.18.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0

SET NOEXEC OFF
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.9.18'
SELECT  @NewVersion = '3.9.19'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'ZPL Printing: remove BestMatch criteria from tblDriverAppPrintTicketTemplate + drop related bestmatch functions & *Sync table'
	UNION SELECT @NewVersion, 0, 'DriverApp.Sync: return tblDriverAppPrintTicketTemplate as TicketTemplates in Sync data'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/*******************************************
-- Date Created: 2013/04/25
-- Author: Kevin Alons
-- Purpose: return readonly Order data for Driver App sync
-- Changes:
	- 3.8.1 - 2015/07/02 - KDA - add new split DriverApp printing capability (HeaderImageLeft|Top|Width|Height & Pickup|Ticket|Deliver TemplateText)
	- 3.9.19 - 2015/07/23 - KDA - remove TicketTemplate column (it will be provided as a separate TicketTemplates sync table)
*******************************************/
ALTER FUNCTION fnOrderReadOnly_DriverApp(@DriverID int, @LastChangeDateUTC datetime) RETURNS TABLE AS
RETURN 
	WITH cteBase AS
	(
		SELECT O.ID
			, O.OrderNum
			, O.StatusID
			, O.TicketTypeID
			, PriorityNum = cast(P.PriorityNum as int) 
			, Product = PRO.Name
			, O.DueDate
			, Origin = OO.Name
			, OriginFull = OO.FullName
			, OO.OriginType
			, O.OriginUomID
			, OriginStation = OO.Station 
			, OriginLeaseNum = OO.LeaseNum 
			, OriginCounty = OO.County 
			, OriginLegalDescription = OO.LegalDescription 
			, OriginNDIC = OO.NDICFileNum
			, OriginNDM = OO.NDM
			, OriginCA = OO.CA
			, OriginState = OO.State
			, OriginAPI = OO.WellAPI 
			, OriginLat = OO.LAT 
			, OriginLon = OO.LON 
			, OriginGeoFenceRadiusMeters = OO.GeoFenceRadiusMeters 
			, Destination = D.Name
			, DestinationFull = D.FullName
			, DestType = D.DestinationType 
			, O.DestUomID
			, DestLat = D.LAT 
			, DestLon = D.LON 
			, DestGeoFenceRadiusMeters = D.GeoFenceRadiusMeters 
			, DestinationStation = D.Station 
			, O.CreateDateUTC
			, O.CreatedByUser
			, O.LastChangeDateUTC
			, O.LastChangedByUser
			, DeleteDateUTC = isnull(ODAVD.VirtualDeleteDateUTC, O.DeleteDateUTC) 
			, DeletedByUser = isnull(ODAVD.VirtualDeletedByUser, O.DeletedByUser) 
			, O.OriginID
			, O.DestinationID
			, PriorityID = cast(O.PriorityID AS int) 
			, Operator = OO.Operator
			, O.OperatorID
			, Pumper = OO.Pumper
			, O.PumperID
			, Producer = OO.Producer
			, O.ProducerID
			, Customer = C.Name
			, O.CustomerID
			, Carrier = CA.Name
			, O.CarrierID
			, O.ProductID
			, TicketType = OO.TicketType
			, EmergencyInfo = isnull(C.EmergencyInfo, 'For an emergency (spill, leak, fire, or accident) contact CHEMTREC @ 800.424.9300 (toll free)') 
			, DestTicketTypeID = D.TicketTypeID
			, DestTicketType = D.TicketType
			, O.OriginTankNum
			, O.OriginTankID
			, O.DispatchNotes
			, O.DispatchConfirmNum
			, RouteActualMiles = isnull(R.ActualMiles, 0)
			, CarrierAuthority = CA.Authority 
			, OriginTimeZone = OO.TimeZone
			, DestTimeZone = D.TimeZone
			, OCTM.OriginThresholdMinutes
			, OCTM.DestThresholdMinutes
			, ShipperHelpDeskPhone = C.HelpDeskPhone
			, OriginDrivingDirections = OO.DrivingDirections
			, LCD.LCD
			, CustomerLastChangeDateUTC = C.LastChangeDateUTC 
			, CarrierLastChangeDateUTC = CA.LastChangeDateUTC 
			, OriginLastChangeDateUTC = OO.LastChangeDateUTC 
			, DestLastChangeDateUTC = D.LastChangeDateUTC
			, RouteLastChangeDateUTC = R.LastChangeDateUTC
			, DriverID = @DriverID
		FROM dbo.tblOrder O
		JOIN dbo.tblPriority P ON P.ID = O.PriorityID
		JOIN dbo.viewOrigin OO ON OO.ID = O.OriginID
		JOIN dbo.viewDestination D ON D.ID = O.DestinationID
		JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
		JOIN dbo.tblRoute R ON R.ID = O.RouteID
		JOIN dbo.tblCarrier CA ON CA.ID = O.CarrierID
		JOIN dbo.tblProduct PRO ON PRO.ID = O.ProductID
		OUTER APPLY dbo.fnOrderBestMatchDriverAppPrintHeaderImage(O.ID) DAHI
		LEFT JOIN tblOrderDriverAppVirtualDelete ODAVD ON ODAVD.OrderID = O.ID AND ODAVD.DriverID = @DriverID
		CROSS JOIN (SELECT DATEADD(second, -5, @LastChangeDateUTC) AS LCD) LCD
		OUTER APPLY dbo.fnOrderCombinedThresholdMinutes(O.ID) OCTM
		WHERE O.ID IN (
			SELECT id FROM tblOrder WHERE DriverID = @driverID AND StatusID IN (2,7,8,3) 
			UNION 
			SELECT OrderID FROM tblOrderDriverAppVirtualDelete WHERE DriverID = @driverID
		)
	)

	SELECT O.*
		, HeaderImageID = DAHI.ID
		, PrintHeaderBlob = DAHI.ImageBlob
		, HeaderImageLeft = DAHI.ImageLeft
		, HeaderImageTop = DAHI.ImageTop
		, HeaderImageWidth = DAHI.ImageWidth
		, HeaderImageHeight = DAHI.ImageHeight
		, PickupTemplateID = DAPT.ID
		, PickupTemplateText = DAPT.TemplateText
		, DeliverTemplateID = DADT.ID
		, DeliverTemplateText = DADT.TemplateText
	FROM cteBase O
	OUTER APPLY dbo.fnOrderBestMatchDriverAppPrintHeaderImage(O.ID) DAHI
	LEFT JOIN tblDriverAppPrintHeaderImageSync DAHIS ON DAHIS.OrderID = O.ID AND DAHIS.DriverID = O.DriverID AND DAHIS.RecordID <> DAHI.ID
	OUTER APPLY dbo.fnOrderBestMatchDriverAppPrintPickupTemplate(O.ID) DAPT
	LEFT JOIN tblDriverAppPrintPickupTemplateSync DAPTS ON DAPTS.OrderID = O.ID AND DAPTS.DriverID = O.DriverID AND DAPTS.RecordID <> DAPT.ID
	OUTER APPLY dbo.fnOrderBestMatchDriverAppPrintDeliverTemplate(O.ID) DADT
	LEFT JOIN tblDriverAppPrintDeliverTemplateSync DADTS ON DADTS.OrderID = O.ID AND DADTS.DriverID = O.DriverID AND DADTS.RecordID <> DADT.ID
	WHERE O.ID IN (
		SELECT ID FROM tblOrder X WHERE DriverID = @DriverID AND StatusID IN (2,7,8,3) 
		UNION 
		SELECT OrderID FROM tblOrderDriverAppVirtualDelete WHERE DriverID = @driverID
	)
	  AND (
		@LastChangeDateUTC IS NULL 
		OR CreateDateUTC >= LCD
		OR O.LastChangeDateUTC >= LCD
		OR O.DeleteDateUTC >= LCD
		OR CustomerLastChangeDateUTC >= LCD
		OR CarrierLastChangeDateUTC >= LCD
		OR OriginLastChangeDateUTC >= LCD
		OR DestLastChangeDateUTC >= LCD
		OR RouteLastChangeDateUTC >= LCD
		-- if any print related record was changed or a different template/image is now valid
		OR DAHI.LastChangeDateUTC >= LCD
		OR DAHIS.RecordID IS NOT NULL
		OR DAPT.LastChangeDateUTC >= LCD
		OR DAPTS.RecordID IS NOT NULL
		OR DADT.LastChangeDateUTC >= LCD
		OR DADTS.RecordID IS NOT NULL
	)
GO

EXEC _spDropFunction 'fnBestMatchDriverAppPrintTicketTemplate'
GO
EXEC _spDropFunction 'fnBestMatchDriverAppPrintTicketTemplateDisplay'
GO
EXEC _spDropFunction 'fnOrderBestMatchDriverAppPrintTicketTemplate'
GO

/*******************************************
-- Date Created: 2013/04/25
-- Author: Kevin Alons
-- Purpose: return readonly Order data for Driver App sync (and log the Print XXX records so we can faithfully re-sync them when a change occurrs)
-- Changes:
	- 3.8.1 - 2015/07/02 - KDA - ADDED
	- 3.9.19 - 2015/09/23 - KDA - remove references to tblDriverAppPrintTicketTemplateSync
*******************************************/
ALTER PROCEDURE spOrderReadOnly_DriverApp(@DriverID int, @LastChangeDateUTC datetime = NULL) AS
BEGIN
	BEGIN TRY	
		BEGIN TRAN

		SELECT * 
		INTO #d 
		FROM dbo.fnOrderReadOnly_DriverApp(@DriverID, @LastChangeDateUTC)
		
		-- forget the last sync records for this driver
		DELETE FROM tblDriverAppPrintHeaderImageSync
		WHERE DriverID = @DriverID 
		  AND OrderID IN (SELECT DISTINCT ID FROM #d)
		DELETE FROM tblDriverAppPrintPickupTemplateSync
		WHERE DriverID = @DriverID 
		  AND OrderID IN (SELECT DISTINCT ID FROM #d)
		DELETE FROM tblDriverAppPrintDeliverTemplateSync
		WHERE DriverID = @DriverID 
		  AND OrderID IN (SELECT DISTINCT ID FROM #d)
		
		-- re-cache the last sync records for the sync orders
		INSERT INTO tblDriverAppPrintHeaderImageSync (OrderID, DriverID, RecordID)
			SELECT DISTINCT ID, @DriverID, HeaderImageID FROM #d WHERE HeaderImageID IS NOT NULL
		INSERT INTO tblDriverAppPrintPickupTemplateSync (OrderID, DriverID, RecordID)
			SELECT DISTINCT ID, @DriverID, PickupTemplateID FROM #d WHERE PickupTemplateID IS NOT NULL
		INSERT INTO tblDriverAppPrintDeliverTemplateSync (OrderID, DriverID, RecordID)
			SELECT DISTINCT ID, @DriverID, DeliverTemplateID FROM #d WHERE DeliverTemplateID IS NOT NULL
		
		COMMIT TRAN
		SELECT * FROM #d
	END TRY
	BEGIN CATCH
		DECLARE @msg varchar(255), @severity int
		SELECT @msg = left(ERROR_MESSAGE(), 255), @severity = ERROR_SEVERITY()
		IF @@TRANCOUNT > 0
			ROLLBACK TRAN 
		RAISERROR(@msg, @severity, 1)
	END CATCH
END

GO

/************************************************
-- Author:		Kevin Alons
-- Create date: 19 Dec 2012
-- Description:	trigger to 
				1) validate any changes, and fail the update if invalid changes are submitted
				2) add a unique, incrementing OrderNum to each new Order (manual Identity column)
				3) recompute wait times (origin|destination based on times provided)
				4) generate route table entry for any newly used origin-destination combination
				5) keep Order consistent with Origin.Producer|Operator|UOM & Dest.UOM then the Origin|Dest change
				6) roll the PrintStatus back to NOTFINALIZED (0) when status is rolled back
				7) when DriverID changes, mark those orders AS DELETED and clone the order with the new DriverID and associated records
				8) update any ticket quantities for open orders when the UOM is changed for the Origin
				9) update the Driver.Truck\Trailer\Trailer2 defaults & related DISPATCHED orders when driver updates them on an order
-REMOVED		10) update the Pickup/DeliverLastChangeDateUTC when Origin/Destination is changed (respectively)
				11) (re) apply Settlement Amounts to orders not yet fully settled when status is changed to DELIVERED
				12) if DBAudit is turned on, save an audit record for this Order change
-- Changes: 
	- 3.7.4 05/08/15 GSM Added Rack/Bay field to DBAudit logic
	- 3.7.7 - 15 May 2015 - KDA - REMOVE #10 above, instead update the LastChangeDateUTC whenever an Origin or Destination is changed
	- 3.7.11 - 18 May 2015 - KDA - generally use GETUTCDATE for all LastChangeDateUTC revisions (instead of related Order.LastChangeDateUTC, etc)
	- 3.7.12 - 5/19/2015 - KDA - update any existing OrderTicket records when the Order is DISPATCHED to a driver (so the Driver App will ALWAYS receive the existing TICKETS from the GAUGER)
	- 3.7.23 - 06/05/2015 - GSM - DCDRV-154: Ticket Type following Origin Change
	- 3.7.23 - 06/05/2015 - GSM - DCWEB-530 - ensure ASSIGNED orders with a driver defined are updated to DISPATCHED
	- 3.8.1  - 2015/07/04 - KDA - purge any Driver|Gauger App ZPL related SYNC change records when order is AUDITED
	- 3.9.0  - 2015/08/20 - KDA - add invocation of spAutoAuditOrder for DELIVERED orders (that qualify) when the Auto-Audit global setting value is TRUE
								- add invocation of spAutoApproveOrder for AUDITED orders (that qualify)
	- 3.9.2  - 2015/08/25 - KDA - appropriately use new tblOrder.OrderDate date column
	- 3.9.4  - 2015/08/30 - KDA - performanc optimization to prevent Orderdate computation if no timestamp fields are yet populated
	- 3.9.5  - 2015/08/31 - KDA - more extensive performance optimizations to minimize performance ramifications or computing OrderDate dynamically from OrderRule
	- 3.9.13 - 2015/09/01 - KDA - add (but commented out) some code to validate Arrive|Depart time being present when required
								- allow orders to be rolled back from AUDITED to DELIVERED when in AUTO-AUDIT mode
								- always AUTO UNAPPROVE orders when the status is reverted from AUDITED to DELIVERED 
	- 3.9.19 - 2015/09/23 - KDA - remove obsolete reference to tblDriverAppPrintTicketTemplate
************************************************/
ALTER TRIGGER [trigOrder_IU] ON [tblOrder] AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @changesFound bit
	
		-- Note: the TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU') ) = 1 statement prevents recursion
	IF (TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU')) = 1 AND TRIGGER_NESTLEVEL( OBJECT_ID('trigOrderTicket_IU')) = 0) BEGIN

		/**********  START OF VALIDATION SECTION ************************/

		IF (UPDATE(OrderNum) 
			OR UPDATE(PriorityID) 
			OR UPDATE(DueDate) 
			OR UPDATE(RouteID) 
			OR UPDATE(OriginID) 
			OR UPDATE(OriginArriveTimeUTC) 
			OR UPDATE(OriginDepartTimeUTC) 
			OR UPDATE(OriginMinutes) 
			OR UPDATE(OriginWaitNotes) 
			OR UPDATE(OriginBOLNum) 
			OR UPDATE(OriginGrossUnits) 
			OR UPDATE(OriginNetUnits) 
			OR UPDATE(DestinationID) 
			OR UPDATE(DestArriveTimeUTC) 
			OR UPDATE(DestDepartTimeUTC) 
			OR UPDATE(DestMinutes) 
			OR UPDATE(DestWaitNotes) 
			OR UPDATE(DestBOLNum) 
			OR UPDATE(DestGrossUnits) 
			OR UPDATE(DestNetUnits) 
			OR UPDATE(CustomerID) 
			OR UPDATE(CarrierID) 
			OR UPDATE(DriverID) 
			OR UPDATE(TruckID)
			OR UPDATE(TrailerID) 
			OR UPDATE(Trailer2ID) 
			OR UPDATE(OperatorID) 
			OR UPDATE(PumperID) 
			OR UPDATE(TicketTypeID) 
			OR UPDATE(Rejected) 
			OR UPDATE(RejectNotes) 
			OR UPDATE(ChainUp) 
			-- allow this to be changed even in audit status
			--OR UPDATE(OriginTruckMileage) 
			OR UPDATE(OriginTankNum) 
			-- allow this to be changed even in audit status
			--OR UPDATE(DestTruckMileage) 
			OR UPDATE(CarrierTicketNum) 
			OR UPDATE(AuditNotes) 
			OR UPDATE(CreateDateUTC) 
			-- it is permissible to change the ActualMiles value on Audited orders up to the point the Order has been Shipper-Settled
			OR (UPDATE(ActualMiles) AND EXISTS (SELECT * FROM deleted i JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = i.ID WHERE OSS.BatchID IS NOT NULL))
			OR UPDATE(ProducerID) 
			OR UPDATE(CreatedByUser) 
			OR UPDATE(LastChangeDateUTC) 
			OR UPDATE(LastChangedByUser) 
			OR UPDATE(DeleteDateUTC) 
			OR UPDATE(DeletedByUser) 
			OR UPDATE(DestProductBSW)
			OR UPDATE(DestProductGravity) 
			OR UPDATE(DestProductTemp) 
			OR UPDATE(ProductID) 
			OR UPDATE(AcceptLastChangeDateUTC) 
			OR UPDATE(PickupLastChangeDateUTC) 
			OR UPDATE(DeliverLastChangeDateUTC) 
			OR UPDATE(OriginUomID) 
			OR UPDATE(DestUomID) 
			OR UPDATE(PickupPrintStatusID) 
			OR UPDATE(DeliverPrintStatusID)
			OR UPDATE(PickupPrintDateUTC) 
			OR UPDATE(DeliverPrintDateUTC) 
			OR UPDATE(OriginTankID) 
			OR UPDATE(OriginGrossStdUnits) 
			OR UPDATE(DispatchConfirmNum) 
			OR UPDATE(DispatchNotes)
			OR UPDATE(PickupDriverNotes)
			OR UPDATE(DeliverDriverNotes)
			OR UPDATE(OriginWaitReasonID)
			OR UPDATE(DestWaitReasonID)
			OR UPDATE(RejectReasonID) 
			OR UPDATE(DestOpenMeterUnits) 
			OR UPDATE(DestCloseMeterUnits)
			OR UPDATE(DestRackBay)
			OR UPDATE(OrderDate)
		)
			--OR UPDATE(ReassignKey)) we don't do any work if this value changes so just ignore it
		BEGIN
			-- only allow the StatusID value to be changed on an audited order
			IF EXISTS (SELECT * FROM deleted WHERE StatusID = 4)
			BEGIN
				-- if the "Allow Audited Order Update (temp only)" setting is TRUE then only warn when this occurs
				IF (dbo.fnToBool(dbo.fnSettingValue(35)) = 1)
					PRINT 'AUDITED orders is being modified - please investigate why!'
				ELSE BEGIN
					-- otherwise (normal behavior) - prevent AUDITED orders from being modified (except to UN-AUDIT them)
					RAISERROR('AUDITED orders cannot be modified!', 16, 1)
					IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
					RETURN
				END
			END
		END
		ELSE IF NOT UPDATE(StatusID) -- NO CHANGES DETECTED SO JUST QUIT
			RETURN

		PRINT 'trigOrder_IU FIRED'
					
		-- ensure the Origin and Destinations are both specified unless the Status is:
		--   (Generated, Assigned, Dispatched or Declined)
		IF  EXISTS(SELECT * FROM inserted O WHERE (O.OriginID IS NULL OR O.DestinationID IS NULL) AND O.StatusID NOT IN (-10,1,2,9))
		BEGIN
			RAISERROR('Invalid Order Status when the Origin and/or Or Destination is not specified', 16, 1)
			IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
			RETURN
		END
		/* this is commented out until we get the Android issues closer to resolved 
		IF EXISTS (SELECT * FROM inserted WHERE StatusID IN (8, 3, 4) AND OriginArriveTimeUTC IS NULL OR OriginDepartTimeUTC IS NULL)
		BEGIN
			RAISERROR('OriginArriveTimeUTC and/or OriginDepartTimeUTC missing when required', 16, 1)
			IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
			RETURN
		END
		IF EXISTS (SELECT * FROM inserted WHERE StatusID IN (3, 4) AND DestArriveTimeUTC IS NULL OR DestDepartTimeUTC IS NULL)
		BEGIN
			RAISERROR('DestArriveTimeUTC and/or DestDepartTimeUTC missing when required', 16, 1)
			IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
			RETURN
		END
		******************************************************************************/
		
		/**********  END OF VALIDATION SECTION ************************/

		WHILE (SELECT count(*) FROM inserted i JOIN tblOrder O ON O.ID = i.ID WHERE O.OrderNum IS NULL) > 0
		BEGIN
			UPDATE tblOrder 
			  SET OrderNum = isnull((SELECT max(OrderNum) FROM tblOrder), 100000) + 1
				, CreateDateUTC = getutcdate()
			WHERE ID = (SELECT min(O.ID) FROM tblOrder O JOIN inserted i ON i.ID = O.ID WHERE O.OrderNum IS NULL)
		END
		
		-- re-compute the OriginMinutes (in case the website failed to compute it properly)
		IF UPDATE(OriginArriveTimeUTC) OR UPDATE(OriginDepartTimeUTC)
		BEGIN
			UPDATE tblOrder SET OriginMinutes = datediff(minute, i.OriginArriveTimeUTC, i.OriginDepartTimeUTC)
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
		END
		-- re-compute the DestMinutes (in case the website failed to compute it properly)
		IF UPDATE(DestArriveTimeUTC) OR UPDATE(DestDepartTimeUTC)
		BEGIN
			UPDATE tblOrder SET DestMinutes = datediff(minute, i.DestArriveTimeUTC, i.DestDepartTimeUTC)
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
		END
		
		/* ensure any changes to the order always update the Order.OrderDate field */
		IF (UPDATE(CustomerID) 
			OR UPDATE(CarrierID) 
			OR UPDATE(ProductID) 
			OR UPDATE(OriginID) 
			OR UPDATE(DestinationID) 
			OR UPDATE(ProducerID) 
			OR UPDATE(OriginArriveTimeUTC) OR UPDATE(OriginDepartTimeUTC)
			OR UPDATE(DestArriveTimeUTC) OR UPDATE(DestDepartTimeUTC))
		BEGIN
			UPDATE tblOrder 
			  SET OrderDate = dbo.fnOrderDate(O.ID)
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
			JOIN deleted d ON d.ID = i.ID
			LEFT JOIN tblOrderSettlementCarrier OSC ON OSC.OrderID = i.ID
			LEFT JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = i.ID
			WHERE i.StatusID <> 4 
			  -- the order has at least 1 valid timestamp
			  AND (i.OriginArriveTimeUTC IS NOT NULL OR i.OriginDepartTimeUTC IS NOT NULL OR i.DestArriveTimeUTC IS NOT NULL OR i.DestDepartTimeUTC IS NOT NULL)
			  -- the order is not yet settled
			  AND OSC.BatchID IS NULL
			  AND OSS.BatchID IS NULL
		END
		
		-- ensure missing Routes are created and assigned for the specified Origin/Destination combination
		IF UPDATE(OriginID) OR UPDATE(DestinationID)
		BEGIN
			-- create any missing Route records
			INSERT INTO tblRoute (OriginID, DestinationID, CreateDateUTC, CreatedByUser)
				SELECT i.OriginID, i.DestinationID, GETUTCDATE(), ISNULL(i.LastChangedByUser, i.CreatedByUser)
				FROM inserted i
				LEFT JOIN tblRoute R ON R.OriginID = i.OriginID AND R.DestinationID = i.DestinationID
				WHERE i.OriginID IS NOT NULL AND i.DestinationID IS NOT NULL AND R.ID IS NULL
			
			-- ensure the Order records refer to the correct Route (ID)
			UPDATE tblOrder SET RouteID = R.ID
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
			LEFT JOIN tblRoute R ON R.OriginID = O.OriginID AND R.DestinationID = O.DestinationID
			WHERE O.RouteID IS NULL OR O.RouteID <> R.ID
			
			-- update the ActualMiles from the related Route
			UPDATE tblOrder SET ActualMiles = R.ActualMiles
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
			JOIN tblRoute R ON R.ID = O.RouteID
			WHERE O.ActualMiles <> R.ActualMiles
		END
		
		IF (UPDATE(OriginID))
		BEGIN
			-- update Order.ProducerID/OperatorID/PumperID to match what is assigned to the new Origin
			UPDATE tblOrder 
				SET TicketTypeID = OO.TicketTypeID
					, ProducerID = OO.ProducerID
					, OperatorID = OO.OperatorID
					, PumperID = OO.PumperID
					, LastChangeDateUTC = GETUTCDATE() 
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			WHERE d.OriginID <> O.OriginID

			-- update Order.OriginUomID to match what is assigned to the new Origin (quantities are updated below)
			UPDATE tblOrder SET OriginUomID = OO.UomID
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			WHERE d.OriginID <> O.OriginID AND d.OriginUomID <> OO.UomID
		END
		
		-- keep the DestUomID in sync with the Destination (units are updated below)
		IF (UPDATE(DestinationID))
		BEGIN
			-- update Order.DestUomID to match what is assigned to the new Destination
			UPDATE tblOrder 
			  SET DestUomID = DD.UomID
				, LastChangeDateUTC = GETUTCDATE()
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID
			JOIN tblDestination DD ON DD.ID = O.DestinationID
			WHERE d.DestinationID <> O.DestinationID AND d.DestUomID <> DD.UomID
		END
		
		-- ensure that any set PrintStatusID values are reset to 0 when backing up the general StatusID value
		IF (UPDATE(StatusID))
		BEGIN
			UPDATE tblOrder 
			  SET DeliverPrintStatusID = 0 
				, LastChangeDateUTC = GETUTCDATE()
			  FROM tblOrder O
			  JOIN deleted d ON d.ID = O.ID
			WHERE O.StatusID <> d.StatusID AND O.StatusID IN (-10, 1, 2, 7, 8) -- Generated, Assigned, Dispatched, Accepted, Picked-Up

			UPDATE tblOrder 
			  SET PickupPrintStatusID = 0 
				, LastChangeDateUTC = GETUTCDATE()
			  FROM tblOrder O
			  JOIN deleted d ON d.ID = O.ID
			WHERE O.StatusID <> d.StatusID AND O.StatusID IN (-10, 1, 2, 7) -- Generated, Assigned, Dispatched, Accepted
			
			/* ensure that any orders that are DISPATCHED - any existing orders are TOUCHED so they are syncable to the Driver App */
			UPDATE tblOrderTicket
				SET LastChangeDateUTC = GETUTCDATE()
			WHERE OrderID IN (
				SELECT i.ID 
				FROM inserted i
				JOIN deleted d ON d.ID = i.ID
				WHERE i.StatusID <> d.StatusID AND i.StatusID IN (2) -- DISPATCHED
			)
	END

		-- just updating the tickets here, trigOrderTicket_IU will in turn update this table
		-- ensure any existing OrderTickets are recomputed with the new OriginUOM
		UPDATE tblOrderTicket
		  SET GrossUnits = dbo.fnConvertUOM(GrossUnits, d.OriginUomID, O.OriginUomID)
			, NetUnits = dbo.fnConvertUOM(NetUnits, d.OriginUomID, O.OriginUomID)
			, LastChangeDateUTC = GETUTCDATE()
		FROM tblOrderTicket OT
		JOIN tblOrder O ON O.ID = OT.OrderID
		JOIN deleted d ON d.ID = O.ID
		WHERE O.OriginUomID <> d.OriginUomID
		
		-- recalculate the Destination Unit values if only if the DestUomID changed but the values didn't
		UPDATE tblOrder
		  SET DestGrossUnits = dbo.fnConvertUOM(O.DestGrossUnits, d.DestUomID, O.DestUomID)
			, DestNetUnits = dbo.fnConvertUOM(O.DestNetUnits, d.DestUomID, O.DestUomID)
			, LastChangeDateUTC = GETUTCDATE()
		FROM tblOrder O
		JOIN deleted d ON d.ID = O.ID
		WHERE d.DestUomID <> O.DestUomID 
		  AND d.DestGrossUnits = O.DestGrossUnits
		  AND d.DestNetUnits = O.DestNetUnits

		/* DCWEB-530 - ensure any ASSIGNED orders with a DRIVER assigned is set to DISPATCHED */
		UPDATE tblOrder SET StatusID = 2 /*DISPATCHED*/
		FROM tblOrder O
		JOIN inserted i ON I.ID = O.ID
		WHERE i.StatusID = 1 /*ASSIGNED*/ AND i.CarrierID IS NOT NULL AND i.DriverID IS NOT NULL AND i.DeleteDateUTC IS NULL

		/*************************************************************************************************************/
		/* handle driver changes - "orphan" the order/ticket (with a new ORDERNUM) - and clone it for the new driver */
		IF (UPDATE(DriverID))
		BEGIN
			-- create cloned order/ticket records (these records will be cloned then the original reset to orphaned status)
			SELECT O.*, NewOrderNum = CAST(NULL as int)
			INTO #clone
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID AND d.DriverID IS NOT NULL AND d.DriverID <> isnull(O.DriverID, 0)

			/* generate the next OrderNum values for these new records */
			DECLARE @newOrderNum int; SET @newOrderNum = isnull((SELECT max(OrderNum) FROM tblOrder), 100000) + 1;
			DECLARE @newReassignKey int; SET @newReassignKey = isnull((SELECT max(ReassignKey) FROM tblOrder), 0) + 1;
			WHILE (SELECT count(*) FROM #clone WHERE NewOrderNum IS NULL) > 0
			BEGIN
				UPDATE #clone
				  SET NewOrderNum = @newOrderNum, ReassignKey = isnull(ReassignKey, @newReassignKey)
					, CreateDateUTC = getutcdate()
				WHERE ID = (SELECT min(ID) FROM #clone WHERE NewOrderNum IS NULL)
				-- if the @newReassignKey wasn't used above, then that # will be skipped, but this should not be an issue
				SELECT @newOrderNum = @newOrderNum + 1, @newReassignKey = @newReassignKey + 1
			END
			
			-- reset the existing records as orphaned
			UPDATE tblOrder
				SET DeleteDateUTC = isnull(O.DeleteDateUTC, GETUTCDATE()), DeletedByUser = isnull(O.DeletedByUser, O.LastChangedByUser)
					, CarrierID = d.CarrierID, DriverID = d.DriverID, TruckID = d.TruckID, TrailerID = d.TrailerID, Trailer2ID = d.Trailer2ID
					, DispatchConfirmNum = NULL
					, ReassignKey = C.ReassignKey
			FROM tblOrder O
			JOIN #clone C ON C.ID = O.ID
			JOIN deleted d on D.ID = O.id

			-- create the cloned records (which is essentially the original with a new ID and the Carrrier & default Truck|Trailers of the new driver)
			INSERT INTO tblOrder (OrderNum, StatusID, PriorityID, DueDate, RouteID, OriginID, OriginArriveTimeUTC, OriginDepartTimeUTC, OriginMinutes, OriginWaitNotes, OriginBOLNum, OriginGrossUnits, OriginNetUnits, DestinationID, DestArriveTimeUTC, DestDepartTimeUTC, DestMinutes, DestWaitNotes, DestBOLNum, DestGrossUnits, DestNetUnits, CustomerID, CarrierID, DriverID, TruckID, TrailerID, Trailer2ID, OperatorID, PumperID, TicketTypeID, Rejected, RejectNotes, ChainUp, OriginTruckMileage, OriginTankNum, DestTruckMileage, CarrierTicketNum, AuditNotes, CreateDateUTC, ActualMiles, ProducerID, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser, DestProductBSW, DestProductGravity, DestProductTemp, ProductID, AcceptLastChangeDateUTC, PickupLastChangeDateUTC, DeliverLastChangeDateUTC, OriginUomID, DestUomID, PickupPrintStatusID, DeliverPrintStatusID, PickupPrintDateUTC, DeliverPrintDateUTC, OriginTankID, OriginGrossStdUnits, DispatchNotes, PickupDriverNotes, DeliverDriverNotes, DispatchConfirmNum, OriginWaitReasonID, DestWaitReasonID, RejectReasonID, DestOpenMeterUnits, DestCloseMeterUnits, ReassignKey)
				SELECT NewOrderNum, StatusID, PriorityID, DueDate, RouteID, OriginID, OriginArriveTimeUTC, OriginDepartTimeUTC, OriginMinutes, OriginWaitNotes, OriginBOLNum, OriginGrossUnits, OriginNetUnits, DestinationID, DestArriveTimeUTC, DestDepartTimeUTC, DestMinutes, DestWaitNotes, DestBOLNum, DestGrossUnits, DestNetUnits, CustomerID, D.CarrierID, DriverID, D.TruckID, D.TrailerID, D.Trailer2ID, OperatorID, PumperID, TicketTypeID, Rejected, RejectNotes, ChainUp, OriginTruckMileage, OriginTankNum, DestTruckMileage, CarrierTicketNum, AuditNotes, O.CreateDateUTC, ActualMiles, ProducerID, O.CreatedByUser, GETUTCDATE(), O.LastChangedByUser, O.DeleteDateUTC, O.DeletedByUser, DestProductBSW, DestProductGravity, DestProductTemp, ProductID, AcceptLastChangeDateUTC, PickupLastChangeDateUTC, DeliverLastChangeDateUTC, OriginUomID, DestUomID, PickupPrintStatusID, DeliverPrintStatusID, PickupPrintDateUTC, DeliverPrintDateUTC, OriginTankID, OriginGrossStdUnits, DispatchNotes, PickupDriverNotes, DeliverDriverNotes, DispatchConfirmNum, OriginWaitReasonID, DestWaitReasonID, RejectReasonID, DestOpenMeterUnits, DestCloseMeterUnits, ReassignKey
				FROM #clone O
				LEFT JOIN tblDriver D ON D.ID = O.DriverID
		
			-- cache the affected tickets
			SELECT OT.*, C.NewOrderNum, OrderDeletedByUser = C.LastChangedByUser
			INTO #cloneTicket
			FROM tblOrderTicket OT
			JOIN #clone C ON C.ID = OT.OrderID
			WHERE OT.DeleteDateUTC IS NULL
			
			-- create the cloned Order Ticket records (copy of existing ticket(s) pointed at new "original" Order)
			INSERT INTO tblOrderTicket (OrderID, CarrierTicketNum, TicketTypeID, TankNum, ProductObsGravity, ProductObsTemp, ProductBSW, OpeningGaugeFeet, OpeningGaugeInch, OpeningGaugeQ, ClosingGaugeFeet, ClosingGaugeInch, ClosingGaugeQ
					, GrossUnits, NetUnits, Rejected, RejectNotes, SealOff, SealOn, BOLNum, ProductHighTemp, ProductLowTemp, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser
					, UID, FromMobileApp, OriginTankID, BottomFeet, BottomInches, BottomQ, GrossStdUnits, RejectReasonID, MeterFactor, OpenMeterUnits, CloseMeterUnits)
				SELECT O.ID, CT.CarrierTicketNum, CT.TicketTypeID, CT.TankNum, CT.ProductObsGravity, CT.ProductObsTemp, CT.ProductBSW, CT.OpeningGaugeFeet, CT.OpeningGaugeInch, CT.OpeningGaugeQ, CT.ClosingGaugeFeet, CT.ClosingGaugeInch, CT.ClosingGaugeQ
					, CT.GrossUnits, CT.NetUnits, CT.Rejected, CT.RejectNotes, CT.SealOff, CT.SealOn, CT.BOLNum, CT.ProductHighTemp, CT.ProductLowTemp, CT.CreateDateUTC, CT.CreatedByUser, GETUTCDATE(), CT.LastChangedByUser, CT.DeleteDateUTC, CT.DeletedByUser
					, newid(), CT.FromMobileApp, CT.OriginTankID, CT.BottomFeet, CT.BottomInches, CT.BottomQ, CT.GrossStdUnits, CT.RejectReasonID, CT.MeterFactor, CT.OpenMeterUnits, CT.CloseMeterUnits
				FROM #cloneTicket CT
				JOIN tblOrder O ON O.OrderNum = CT.NewOrderNum
		END
		/*************************************************************************************************************/
		
		/* HANDLE TRUCK | TRAILER | TRAILER2 default changes when the driver updates his Truck | Trailer | Trailer2 on ACCEPTANCE */
		-- TRUCK
		IF (UPDATE(TruckID) AND EXISTS (
			SELECT i.* 
			FROM inserted i 
			JOIN deleted d ON d.ID = i.ID
			  AND i.DriverID = d.DriverID 
			  AND i.TruckID <> d.TruckID
			WHERE i.StatusID IN (7, 8, 3) -- ACCEPTED, PICKEDUP, DELIVERED
			  AND d.StatusID IN (2))) -- DISPATCHED
		BEGIN
			UPDATE tblDriver
			  SET TruckID = i.TruckID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblDriver DR
			JOIN inserted i ON i.DriverID = DR.ID
			JOIN deleted d ON d.DriverID = DR.ID
			WHERE i.TruckID <> d.TruckID
			
			UPDATE tblOrder
			  SET TruckID = i.TruckID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblOrder O
			JOIN inserted i ON i.DriverID = O.DriverID 
			  AND O.ID <> i.ID
			  AND O.StatusID IN (2) -- DISPATCHED
			  AND i.TruckID <> O.TruckID
			  AND O.DeleteDateUTC IS NULL
		END
		-- TRAILER
		IF (UPDATE(TrailerID) AND EXISTS (
			SELECT i.* 
			FROM inserted i 
			JOIN deleted d ON d.ID = i.ID
			  AND i.DriverID = d.DriverID 
			  AND i.TrailerID <> d.TrailerID
			WHERE i.StatusID IN (7, 8, 3) -- ACCEPTED, PICKEDUP, DELIVERED
			  AND d.StatusID IN (2))) -- DISPATCHED
		BEGIN
			UPDATE tblDriver
			  SET TrailerID = i.TrailerID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblDriver DR
			JOIN inserted i ON i.DriverID = DR.ID
			JOIN deleted d ON d.DriverID = DR.ID
			WHERE i.TrailerID <> d.TrailerID
			
			UPDATE tblOrder
			  SET TrailerID = i.TrailerID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblOrder O
			JOIN inserted i ON i.DriverID = O.DriverID 
			  AND O.ID <> i.ID
			  AND O.StatusID IN (2) -- DISPATCHED
			  AND i.TrailerID <> O.TrailerID
			  AND O.DeleteDateUTC IS NULL
		END
		-- TRAILER 2
		IF (UPDATE(Trailer2ID) AND EXISTS (
			SELECT i.* 
			FROM inserted i 
			JOIN deleted d ON d.ID = i.ID
			  AND i.DriverID = d.DriverID 
			  AND isnull(i.Trailer2ID, 0) <> isnull(d.Trailer2ID, 0)
			WHERE i.StatusID IN (7, 8, 3) -- ACCEPTED, PICKEDUP, DELIVERED
			  AND d.StatusID IN (2))) -- DISPATCHED
		BEGIN
			UPDATE tblDriver
			  SET TrailerID = i.TrailerID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblDriver DR
			JOIN inserted i ON i.DriverID = DR.ID
			JOIN deleted d ON d.DriverID = DR.ID
			WHERE isnull(i.Trailer2ID, 0) <> isnull(d.Trailer2ID, 0)
			
			UPDATE tblOrder
			  SET Trailer2ID = i.Trailer2ID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblOrder O
			JOIN inserted i ON i.DriverID = O.DriverID 
			  AND O.ID <> i.ID
			  AND O.StatusID IN (2) -- DISPATCHED
			  AND isnull(i.Trailer2ID, 0) <> isnull(O.Trailer2ID, 0)
			  AND O.DeleteDateUTC IS NULL
		END
		/*************************************************************************************************************/
	
		DECLARE @deliveredIDs TABLE (ID int)
		DECLARE @auditedIDs TABLE (ID int)
		DECLARE @id int
		DECLARE @autoAudit bit; SET @autoAudit = dbo.fnToBool(dbo.fnSettingValue(54))
		DECLARE @toAudit bit
		-- Auto-Audit/Apply Settlement Rates/Amounts/Auto-Approve to Order when STATUS is changed to DELIVERED (until it is FINAL settled)
		IF (UPDATE(StatusID) OR UPDATE(DeliverPrintStatusID))
		BEGIN
			INSERT INTO @deliveredIDs (ID)
				SELECT i.ID 
				FROM inserted i
				JOIN tblPrintStatus iPS ON iPS.ID = i.DeliverPrintStatusID
				JOIN deleted d ON d.ID = i.ID
				JOIN tblPrintStatus dPS ON dPS.ID = d.DeliverPrintStatusID
				WHERE i.StatusID = 3 AND d.StatusID <> 4 AND iPS.IsCompleted = 1 AND i.StatusID + iPS.IsCompleted <> d.StatusID + dPS.IsCompleted
			
			SELECT @id = MIN(ID) FROM @deliveredIDs
			WHILE @id IS NOT NULL
			BEGIN
				-- attempt to AUTO-AUDIT the order if this feature is enabled (global setting)
				IF (@autoAudit = 1)
				BEGIN
					EXEC spAutoAuditOrder @ID, 'System', @toAudit OUTPUT
					/* if the order was updated to AUDITED status, then we need to attempt also to auto-approve [below] */
					IF (@toAudit = 1)
						INSERT INTO @auditedIDs VALUES (@id)
				END
				-- attempt to apply rates to all newly DELIVERED orders
				EXEC spApplyRatesBoth @id, 'System' 
				SET @id = (SELECT MIN(id) FROM @deliveredIDs WHERE ID > @id)
			END
		END

		-- Auto-Approve any un-approved orders when STATUS changed to AUDIT
		IF (UPDATE(StatusID) OR EXISTS (SELECT * FROM @auditedIDs))
		BEGIN
			INSERT INTO @auditedIDs (ID)
				SELECT i.ID 
				FROM inserted i
				JOIN deleted d ON d.ID = i.ID
				LEFT JOIN tblOrderApproval OA ON OA.OrderID = i.ID
				LEFT JOIN @auditedIDs AID ON AID.ID = i.ID
				WHERE i.StatusID = 4 AND d.StatusID <> 4 AND OA.OrderID IS NULL AND AID.ID IS NULL

			SELECT @id = MIN(ID) FROM @auditedIDs
			WHILE @id IS NOT NULL
			BEGIN
				EXEC spAutoApproveOrder @id, 'System'
				SET @id = (SELECT MIN(id) FROM @auditedIDs WHERE ID > @id)
			END
			
		END
		
		/* auto UNAPPROVE any orders that have their status reverted from AUDITED */
		IF (UPDATE(StatusID))
		BEGIN
			DELETE FROM tblOrderApproval
			WHERE OrderID IN (
				SELECT i.ID
				FROM inserted i
				JOIN deleted d ON d.ID = i.ID
				JOIN tblOrderApproval OA ON OA.OrderID = i.ID
				WHERE d.StatusID = 4 AND i.StatusID <> 4
			)
		END
		
		-- purge any DriverApp/Gauger App sync records for any orders that are not in AUDITED status
		IF UPDATE(StatusID) AND EXISTS (SELECT ID FROM inserted WHERE StatusID IN (4))
		BEGIN
			-- DRIVER APP records
			DELETE FROM tblDriverAppPrintHeaderImageSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID IN (4))
			DELETE FROM tblDriverAppPrintPickupTemplateSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID IN (4))
			DELETE FROM tblDriverAppPrintDeliverTemplateSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID IN (4))
			-- GAUGER APP records
			DELETE FROM tblGaugerAppPrintHeaderImageSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID IN (4))
			DELETE FROM tblGaugerAppPrintPickupTemplateSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID IN (4))
			DELETE FROM tblGaugerAppPrintTicketTemplateSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID IN (4))
		END
				
		--NOTE: we do not update the DestOpenMeterUnits/DestCloseMeterUnits since they don't auto update
		--  (and they simply represent a reading that is assumed to in the DestUomID UOM at entry)
		--  (if they are updated in the future [manually] the will calculate again using the DestUomID at that time)

		IF EXISTS (SELECT * FROM inserted EXCEPT SELECT * FROM deleted)
		BEGIN
			-- optionally add tblOrderDBAudit records
			BEGIN TRY
				IF EXISTS(SELECT * FROM tblSetting WHERE ID = 32 AND (Value LIKE 'true' OR Value LIKE 'yes'))
					INSERT INTO tblOrderDbAudit (DBAuditDate, ID, OrderNum, StatusID, PriorityID, DueDate, RouteID, OriginID, OriginArriveTimeUTC, OriginDepartTimeUTC, OriginMinutes, OriginWaitNotes, OriginBOLNum, OriginGrossUnits, OriginNetUnits, DestinationID, DestArriveTimeUTC, DestDepartTimeUTC, DestMinutes, DestWaitNotes, DestBOLNum, DestGrossUnits, DestNetUnits, CustomerID, CarrierID, DriverID, TruckID, TrailerID, Trailer2ID, OperatorID, PumperID, TicketTypeID, Rejected, RejectNotes, ChainUp, OriginTruckMileage, OriginTankNum, DestTruckMileage, CarrierTicketNum, AuditNotes, CreateDateUTC, ActualMiles, ProducerID, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser, DestProductBSW, DestProductGravity, DestProductTemp, ProductID, AcceptLastChangeDateUTC, PickupLastChangeDateUTC, DeliverLastChangeDateUTC, OriginUomID, DestUomID, PickupPrintStatusID, DeliverPrintStatusID, PickupPrintDateUTC, DeliverPrintDateUTC, OriginTankID, OriginGrossStdUnits, DispatchNotes, PickupDriverNotes, DeliverDriverNotes, DispatchConfirmNum, OriginWaitReasonID, DestWaitReasonID, RejectReasonID, DestOpenMeterUnits, DestCloseMeterUnits, ReassignKey, DestRackBay, OrderDate)
						SELECT GETUTCDATE(), ID, OrderNum, StatusID, PriorityID, DueDate, RouteID, OriginID, OriginArriveTimeUTC, OriginDepartTimeUTC, OriginMinutes, OriginWaitNotes, OriginBOLNum, OriginGrossUnits, OriginNetUnits, DestinationID, DestArriveTimeUTC, DestDepartTimeUTC, DestMinutes, DestWaitNotes, DestBOLNum, DestGrossUnits, DestNetUnits, CustomerID, CarrierID, DriverID, TruckID, TrailerID, Trailer2ID, OperatorID, PumperID, TicketTypeID, Rejected, RejectNotes, ChainUp, OriginTruckMileage, OriginTankNum, DestTruckMileage, CarrierTicketNum, AuditNotes, CreateDateUTC, ActualMiles, ProducerID, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser, DestProductBSW, DestProductGravity, DestProductTemp, ProductID, AcceptLastChangeDateUTC, PickupLastChangeDateUTC, DeliverLastChangeDateUTC, OriginUomID, DestUomID, PickupPrintStatusID, DeliverPrintStatusID, PickupPrintDateUTC, DeliverPrintDateUTC, OriginTankID, OriginGrossStdUnits, DispatchNotes, PickupDriverNotes, DeliverDriverNotes, DispatchConfirmNum, OriginWaitReasonID, DestWaitReasonID, RejectReasonID, DestOpenMeterUnits, DestCloseMeterUnits, ReassignKey, DestRackBay, OrderDate
						FROM deleted d
			END TRY
			BEGIN CATCH
				PRINT 'trigOrder_IU.DBAUDIT FAILURE: ' + ERROR_MESSAGE()
			END CATCH
		END
		
	PRINT 'trigOrder_IU COMPLETE'

	END
	
END

GO
EXEC sp_settriggerorder @triggername=N'[trigOrder_IU]', @order=N'First', @stmttype=N'INSERT'
GO
EXEC sp_settriggerorder @triggername=N'[trigOrder_IU]', @order=N'First', @stmttype=N'UPDATE'
GO

/* remove BestMatch criteria from tblDriverAppPrintTicketTemplate + DROP TABLE tblDriverAppPrintTicketTemplateSync tables */
ALTER TABLE dbo.tblDriverAppPrintTicketTemplate
	DROP CONSTRAINT FK_DriverAppPrintTicketTemplate_TicketType
GO
ALTER TABLE dbo.tblDriverAppPrintTicketTemplate
	DROP CONSTRAINT DF_DriverAppPrintTicketTemplate_CreateDateUTC
GO
ALTER TABLE dbo.tblDriverAppPrintTicketTemplate
	DROP CONSTRAINT DF_DriverAppPrintTicketTemplate_CreatedByUser
GO
CREATE TABLE dbo.Tmp_tblDriverAppPrintTicketTemplate
	(
	ID int NOT NULL IDENTITY (1, 1),
	TicketTypeID int NOT NULL,
	TemplateText text NOT NULL,
	CreateDateUTC smalldatetime NULL,
	CreatedByUser varchar(100) NOT NULL,
	LastChangeDateUTC smalldatetime NULL,
	LastChangedByUser varchar(100) NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON dbo.Tmp_tblDriverAppPrintTicketTemplate TO role_iis_acct  AS dbo
GO
ALTER TABLE dbo.Tmp_tblDriverAppPrintTicketTemplate ADD CONSTRAINT
	DF_DriverAppPrintTicketTemplate_CreateDateUTC DEFAULT (getutcdate()) FOR CreateDateUTC
GO
ALTER TABLE dbo.Tmp_tblDriverAppPrintTicketTemplate ADD CONSTRAINT
	DF_DriverAppPrintTicketTemplate_CreatedByUser DEFAULT ('N/A') FOR CreatedByUser
GO
SET IDENTITY_INSERT dbo.Tmp_tblDriverAppPrintTicketTemplate ON
GO
INSERT INTO dbo.Tmp_tblDriverAppPrintTicketTemplate (ID, TicketTypeID, TemplateText, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser)
	SELECT ID, TicketTypeID, TemplateText, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM dbo.tblDriverAppPrintTicketTemplate
	WHERE TicketTypeID IS NOT NULL
GO
SET IDENTITY_INSERT dbo.Tmp_tblDriverAppPrintTicketTemplate OFF
GO
ALTER TABLE dbo.tblDriverAppPrintTicketTemplateSync
	DROP CONSTRAINT FK_DriverAppPrintTicketTemplateSync_Record
GO
DROP TABLE tblDriverAppPrintTicketTemplateSync
GO 
DROP TABLE dbo.tblDriverAppPrintTicketTemplate
GO
EXECUTE sp_rename N'dbo.Tmp_tblDriverAppPrintTicketTemplate', N'tblDriverAppPrintTicketTemplate', 'OBJECT' 
GO
ALTER TABLE dbo.tblDriverAppPrintTicketTemplate ADD CONSTRAINT
	PK_DriverAppPrintTicketTemplate PRIMARY KEY NONCLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE UNIQUE CLUSTERED INDEX udxDriverAppPrintTicketTemplate_TicketType ON dbo.tblDriverAppPrintTicketTemplate
	(
	TicketTypeID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.tblDriverAppPrintTicketTemplate ADD CONSTRAINT
	FK_DriverAppPrintTicketTemplate_TicketType FOREIGN KEY
	(
	TicketTypeID
	) REFERENCES dbo.tblTicketType
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE
	
GO

EXEC _spRebuildAllObjects
GO

COMMIT
SET NOEXEC OFF