SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.2.3'
SELECT  @NewVersion = '4.2.4'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1889 - Fix duplicate rows in reports when ticket level data not present'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/* Add IsTicketField column to tblReportColumnDefinition for specifically identifiying 
ticket level fields that will cause a split load to show up as two rows in reports*/
ALTER TABLE tblReportColumnDefinition
	ADD IsTicketField BIT NOT NULL DEFAULT 0
GO


/* update actual ticket fields to show they are indeed ticket level fields */
UPDATE tblReportColumnDefinition SET IsTicketField = 1 WHERE DataField LIKE '%T[_]%'
GO

/* Change ticket type field to an order level field because it is used in both ways */
UPDATE tblReportColumnDefinition SET IsTicketField = 0 WHERE ID IN (84)
GO


/* Refresh views before adding the new field to the below views.  This is to prevent the update from failing */
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO



/***********************************************/
-- Date Created: (Undefined)
-- Author: Kevin Alons
-- Purpose: (Undefined)
-- Changes: BB 10/26/16 x.x.x - Added IsTicketField for ticket level field identification DCWEB-1889
/***********************************************/
ALTER VIEW [dbo].[viewReportColumnDefinition] AS
	SELECT RCD.ID
		, RCD.ReportID
		, RCD.DataField
		, Caption = coalesce(CE.Caption, RCD.Caption, RCD.DataField)
		, RCD.DataFormat
		, RCD.FilterDataField
		, RCD.FilterTypeID
		, RCD.FilterDropDownSql
		, RCD.FilterAllowCustomText
		, RCD.AllowedRoles
		, RCD.OrderSingleExport
		, RFT.FilterOperatorID_CSV
		, BaseFilterCount = (SELECT COUNT(*) FROM tblReportColumnDefinitionBaseFilter WHERE ReportColumnID = RCD.ID)
		, FilterOperatorID_CSV_Delim = ',' + RFT.FilterOperatorID_CSV + ','
		, RCD.IsTicketField
	FROM tblReportColumnDefinition RCD
	LEFT JOIN tblReportColumnDefinitionCountryException CE ON CE.CountryID = dbo.fnSettingValue(36) AND CE.ReportColumnID = RCD.ID
	JOIN tblReportFilterType RFT ON RFT.ID = RCD.FilterTypeID;
GO



/***********************************************/
-- Date Created: 27 Jul 2014
-- Author: Kevin Alons
-- Purpose: add related JOINed fields to the tblUserReportColumnDefinition table results
-- Changes: BB 10/26/16 x.x.x - Added IsTicketField for ticket level field identification DCWEB-1889
/***********************************************/
ALTER VIEW [dbo].[viewUserReportColumnDefinition] AS
	SELECT URCD.ID
		, URCD.UserReportID
		, URCD.ReportColumnID
		, URCD.Caption
		, DataFormat = isnull(URCD.DataFormat, RCD.DataFormat)
		, URCD.SortNum
		, URCD.FilterOperatorID
		, URCD.FilterValue1
		, URCD.FilterValue2
		, URCD.Export
		, URCD.DataSort
		, URCD.GroupingFunctionID
		, DataSortABS = abs(URCD.DataSort)
		, URCD.CreateDateUTC, URCD.CreatedByUser
		, URCD.LastChangeDateUTC, URCD.LastChangedByUser
		, RCD.DataField
		, BaseCaption = dbo.fnCaptionRoot(RCD.Caption)
		, OutputCaption = coalesce(URCD.Caption, dbo.fnCaptionRoot(RCD.Caption), RCD.DataField)
		, RCD.FilterTypeID
		, FilterDataField = isnull(RCD.FilterDataField, RCD.DataField)
		, RCD.FilterDropDownSql
		, RCD.FilterAllowCustomText
		, RCD.AllowedRoles
		, RCD.OrderSingleExport
		, FilterOperator = RFO.Name
		, FilterValues = CASE 
				WHEN RFO.ID IN (1,4,5) OR RCD.FilterAllowCustomText = 1 THEN FilterValue1 + ISNULL(' - ' + FilterValue2, '')
				ELSE 
					CASE WHEN FilterValue1 IS NOT NULL THEN '&lt;filtered&gt;' 
						 ELSE NULL 
					END 
			END
		, RCD.IsTicketField
	FROM tblUserReportColumnDefinition URCD
	JOIN viewReportColumnDefinition RCD ON RCD.ID = URCD.ReportColumnID
	LEFT JOIN tblReportFilterOperator RFO ON RFO.ID = URCD.FilterOperatorID;
GO




/* Refresh/Rebuild/Recompile anythign that may use tblReportColumnDefinition */
EXEC _spRecompileAllStoredProcedures
GO
EXEC _spRebuildAllObjects
GO
EXEC _spRebuildAllObjects
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO


COMMIT
SET NOEXEC OFF