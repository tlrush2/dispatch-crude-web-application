-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.4.0'
SELECT  @NewVersion = '3.4.1'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Add TABLE tblSyncError"'
	UNION SELECT @NewVersion, 0, 'Add PROCEDURE spLogSyncError'
	UNION SELECT @NewVersion, 0, 'Add VIEW viewSyncError'
	UNION SELECT @NewVersion, 0, 'Add FUNCTION spLocal_to_UTC_User'
	UNION SELECT @NewVersion, 0, 'Add FUNCTION spUTC_to_Local_User'
GO

CREATE TABLE tblSyncError
(
  ID int identity(1, 1) not null constraint PK_SyncError PRIMARY KEY
, DriverID int NOT NULL CONSTRAINT FK_SyncError FOREIGN KEY REFERENCES tblDriver(ID)
, CreateDateUTC datetime CONSTRAINT DF_SyncError_ErrorDate DEFAULT (getutcdate())
, ErrorMsg varchar(max) NULL
)
GO
GRANT SELECT, INSERT, DELETE ON tblSyncError TO dispatchcrude_iis_acct
GO

CREATE PROCEDURE spLogSyncError(@driverID int, @errorMsg varchar(max)) AS
BEGIN
	INSERT INTO tblSyncError (DriverID, ErrorMsg) VALUES (@driverID, @errorMsg)	
END

GO
GRANT EXECUTE ON spLogSyncError TO dispatchcrude_iis_acct
GO

/****************************************************
-- Date Created: 12/7/2014 
-- Author: Kevin Alons
-- Purpose: return the SyncErrors with translated "friendly" values
****************************************************/
CREATE VIEW viewSyncError AS
	SELECT SE.*
		, Driver = D.FullName
	FROM tblSyncError SE
	LEFT JOIN viewDriver D ON D.ID = SE.DriverID
GO
GRANT SELECT ON viewSyncError TO dispatchcrude_iis_acct
GO

/************************************************/
-- Create Date: 26 Aug 2013
-- Author: Kevin Alons
-- Purpose: convert a local DateTime to UTC
/************************************************/
CREATE FUNCTION fnLocal_to_UTC_User(@local datetime, @username varchar(100)) RETURNS datetime AS
BEGIN
	DECLARE @ret datetime, @timeZoneID tinyint, @useDST bit; 
	SELECT @timeZoneID = ProfileValue FROM dbo.fnUserProfileValues('TimeZoneID') WHERE UserName = @username
	IF @timeZoneID IS NULL SET @timeZoneID = (SELECT ID FROM tblTimeZone WHERE Name LIKE '%Central%')
	SELECT @useDST = ProfileValue FROM dbo.fnUserProfileValues('UseDST') WHERE UserName = @username
	IF @useDST IS NULL SET @useDST = 1
	SELECT @ret = dbo.fnLocal_to_UTC(@local, @timeZoneID, @useDST)
	RETURN @ret
END

GO
GRANT EXECUTE ON fnLocal_to_UTC_User TO dispatchcrude_iis_acct
GO

/************************************************/
-- Create Date: 26 Aug 2013
-- Author: Kevin Alons
-- Purpose: convert a local DateTime to UTC
/************************************************/
CREATE FUNCTION fnUTC_to_Local_User(@local datetime, @username varchar(100)) RETURNS datetime AS
BEGIN
	DECLARE @ret datetime, @timeZoneID tinyint, @useDST bit; 
	SELECT @timeZoneID = ProfileValue FROM dbo.fnUserProfileValues('TimeZoneID') WHERE UserName = @username
	IF @timeZoneID IS NULL SET @timeZoneID = (SELECT ID FROM tblTimeZone WHERE Name LIKE '%Central%')
	SELECT @useDST = ProfileValue FROM dbo.fnUserProfileValues('UseDST') WHERE UserName = @username
	IF @useDST IS NULL SET @useDST = 1
	SELECT @ret = dbo.fnUTC_to_Local(@local, @timeZoneID, @useDST)
	RETURN @ret
END

GO
GRANT EXECUTE ON fnUTC_to_Local_User TO dispatchcrude_iis_acct
GO

COMMIT
SET NOEXEC OFF