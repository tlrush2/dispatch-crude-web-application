/* 
	-- fix existing Tax reports to always use BBLS (instead of erroneous Gallons!)
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.6.15'
SELECT  @NewVersion = '2.6.16'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 20 Mar 2013
-- Description:	return the data for the TAX_ND_10A report (for the specified date range)
-- =============================================
ALTER PROCEDURE [dbo].[spReport_Tax_ND10A]
(
  @CustomerID int = -1
, @ProductID int = -1
, @StateID int = -1
, @StartDate datetime
, @EndDate datetime
, @IncludeUninvoiced bit = 0
)
AS BEGIN
	SELECT @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	
	SELECT O.FieldName
		, O.Operator
		, O.LeaseNum AS LeaseNumber
		, O.Name AS [Well Name and Number]
		, O.CTBNum AS [NDIC CTB No.]
		-- convert all units to Barrels for this report
		, cast(round(SUM(dbo.fnConvertUom(OD.OriginNetUnits, OD.OriginUomID, 1)), 0) as int) AS [Lease Total (Bbls)]
	FROM viewOrigin O
	JOIN viewOrder OD ON OD.OriginID = O.ID
	JOIN tblOrderInvoiceCustomer IC ON IC.OrderID = OD.ID AND (@IncludeUninvoiced = 1 OR IC.BatchID IS NOT NULL)
	WHERE O.OriginTypeID IN (2) -- WELLs ONLY
		AND OD.DeleteDateUTC IS NULL
		AND (@CustomerID = -1 OR OD.CustomerID = @CustomerID)
		AND (@ProductID = -1 OR OD.ProductID = @ProductID)
		AND (@StateID = -1 OR O.StateID = @StateID)
		AND OD.OrderDate BETWEEN @StartDate AND @EndDate
	GROUP BY O.FieldName, O.Operator, O.LeaseNum, O.Name, O.CTBNum
	ORDER BY O.FieldName, O.Operator, O.LeaseNum, O.Name, O.CTBNum
	END

GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 20 Mar 2013
-- Description:	return the data for the TAX_ND_10B report (for the specified date range)
-- =============================================
ALTER PROCEDURE [dbo].[spReport_Tax_ND10B]
(
  @CustomerID int = -1
, @ProductID int = -1
, @StateID int = -1
, @StartDate datetime
, @EndDate datetime
, @IncludeUninvoiced bit = 0
)
AS BEGIN
	SELECT @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	
	SELECT O.Name AS [Point Received]
		, OD.Customer AS [Purchaser]
		, OD.Destination
		-- convert all units to BARRELS (2) for this report
		, cast(round(SUM(dbo.fnConvertUom(OD.OriginNetUnits, OD.OriginUomID, 1)), 0) as int) AS [Lease Total (Bbls)]
	FROM viewOrder OD
	JOIN tblOrigin O ON O.ID = OD.OriginID
	JOIN tblOrderInvoiceCustomer IC ON IC.OrderID = OD.ID AND (@IncludeUninvoiced = 1 OR IC.BatchID IS NOT NULL)
	WHERE O.OriginTypeID IN (2) -- WELLs ONLY
		AND OD.DeleteDateUTC IS NULL
		AND (@CustomerID = -1 OR OD.CustomerID = @CustomerID)
		AND (@ProductID = -1 OR OD.ProductID = @ProductID)
		AND (@StateID = -1 OR O.StateID = @StateID)
		AND OD.OrderDate BETWEEN @StartDate AND @EndDate
	GROUP BY O.Name, OD.Customer, OD.Destination
	ORDER BY O.Name, OD.Customer, OD.Destination
	END


GO

COMMIT
SET NOEXEC OFF