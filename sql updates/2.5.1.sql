/* 
	performance optimization for Converting Uom (no-op when to/from units are the same
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.5.0'
SELECT  @NewVersion = '2.5.1'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

-- update the app version to '0.9.5'
UPDATE tblSetting SET Value = '0.9.5' WHERE ID=12
GO

/*************************************************************
** Date Created: 26 Nov 2013
** Author: Kevin Alons
** Purpose: convert any Units Qty into a Gallons Qty
*************************************************************/
ALTER FUNCTION [dbo].[fnConvertUOM](@units decimal(9,4), @fromUomID int, @toUomID int) RETURNS decimal(18,6) AS BEGIN
	DECLARE @ret decimal(18,6)
	IF (@fromUomID = @toUomID)
		SELECT @ret = @units
	ELSE
		SELECT @ret = (
			SELECT @units * GallonEquivalent FROM tblUom WHERE ID=@fromUomID
		) / GallonEquivalent FROM tblUom WHERE ID=@toUomID
	RETURN (@ret)
END

GO

COMMIT
SET NOEXEC OFF