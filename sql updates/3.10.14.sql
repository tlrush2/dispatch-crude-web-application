SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.10.13.7'
SELECT  @NewVersion = '3.10.14'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1088: Changed SUNDEX Export Process to accomodate the custom data fields necessary to allow DC customers to have more than one SUNDEX ticket source code'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/* 3.10.14 - BB - 2016/02/25 - Add destination related custom data fields to tblObjectField for the Sunoco SUNDEX export */
SET IDENTITY_INSERT tblObjectField ON
INSERT INTO tblObjectField (ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, ReadOnly, AllowNullID, IsKey, IsCustom)
	SELECT 90001, 20, 'SundexDestCode', 'Sundex Destination Code', 1, NULL, 0, 1, 0, 1
	UNION
	SELECT 90002, 20, 'SundexTicketSourceCode', 'Sundex Ticket Source Code', 1, NULL, 0, 1, 0, 1	
	UNION
	SELECT 90003, 20, 'SundexCompanyCode', 'Sundex Company Code', 1, NULL, 0, 1, 0, 1	
	EXCEPT SELECT ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, ReadOnly, AllowNullID, IsKey, IsCustom FROM tblObjectField
SET IDENTITY_INSERT tblObjectField OFF
GO


/* 3.10.14 - BB - 2016/02/25 - Add Sunoco Sundex Company Code setting */
INSERT INTO tblSetting (ID, Name, SettingTypeID, Value, Category, CreateDateUTC, CreatedByUser, ReadOnly)
	SELECT 59, 'SUNDEX Company Code (10 Digits)', 1, NULL, 'Data Exchange', GETUTCDATE(), 'System', 0
GO

	
/*****************************************************************************************/
-- Author: Kevin Alons
-- Date Created: 27 Feb 2014
-- Purpose: return export final orders in the SunocoSundex
-- Changes:
	-- 3.7.21 - 6/3/2015 - BBloodworth - Ticket_Source_Code is now a dynamic system setting value
	-- 3.10.14 - 02/25/2016 - KDA & BB - Added custom data field functionality to the view.
/*****************************************************************************************/
ALTER VIEW [dbo].[viewSunocoSundex] AS
SELECT
	_ID = O.ID
	, _CustomerID = O.CustomerID
	, _OrderNum = O.OrderNum
	, Request_Code = CASE WHEN EP.IsNew = 1 THEN 'A' ELSE 'C' END
	, Company_Code = isnull((SELECT Value FROM tblObjectCustomData WHERE ObjectFieldID = 90003 AND RecordID = O.DestinationID), (SELECT Value FROM tblSetting WHERE ID = 59))
	, Ticket_Type = UPPER(left(O.TicketType, 1))
	/* Changed ticket source code 3.10.14 */
	, Ticket_Source_Code = isnull((SELECT Value FROM tblObjectCustomData WHERE ObjectFieldID = 90002 AND RecordID = O.DestinationID), (SELECT Value FROM tblSetting WHERE ID = 53))
	, Ticket_Number = isnull(T.CarrierTicketNum, ltrim(O.OrderNum) + 'X')
	, Ticket_Date = dbo.fnDateMMddYYYY(O.OrderDate)
	, SXL_Property_Code = isnull(O.LeaseNum, '')
	, TP_Property_Code = ''
	, Lease_Company_Name = O.Origin
	, Destination = isnull(OCD.Value, '')
	, Tank_Meter_Number = coalesce(T.OriginTankText, (SELECT min(TankNum) FROM tblOriginTank WHERE DeleteDateUTC IS NULL AND ID = O.OriginTankID), O.OriginTankNum)
	, Open_Date = dbo.fnDateMMddYYYY(O.OriginArriveTime)
	, Open_Time = dbo.fnTimeOnly(O.OriginArriveTime)
	, Close_Date = dbo.fnDateMMddYYYY(O.OriginDepartTime)
	, Close_Time = dbo.fnTimeOnly(O.OriginDepartTime)
	, Estimated_Volume = cast(ROUND(T.GrossUnits, 2) as decimal(18, 2))
	, Gross_Volume = cast(ROUND(T.GrossUnits, 2) as decimal(18, 2))
	, Net_Volume = cast(ROUND(T.NetUnits, 2) as decimal(18, 2))
	, Observed_Gravity = isnull(ltrim(T.ProductObsGravity), '')
	, Observed_Temperature = isnull(ltrim(T.ProductObsTemp), '')
	, Observed_BSW = isnull(ltrim(T.ProductBSW), '')
	, Corrected_Gravity_API = 0
	, Purchaser = 'Sonoco Logistics'
	, First_Reading_Gauge_Ft = isnull(ltrim(T.OpeningGaugeFeet), '')
	, First_Reading_Gauge_In = isnull(ltrim(T.OpeningGaugeInch), '')
	, First_Reading_Gauge_Nu = isnull(ltrim(T.OpeningGaugeQ), '')
	, First_Reading_Gauge_De = 4
	, First_Temperature = isnull(ltrim(T.ProductHighTemp), '')
	, First_Bottom_Ft = isnull(ltrim(T.BottomFeet), '')
	, First_Bottom_In = isnull(ltrim(T.BottomInches), '')
	, First_Bottom_Nu = isnull(ltrim(T.BottomQ), '')
	, First_Bottom_De = 4
	, Second_Reading_Gauge_Ft = isnull(ltrim(T.ClosingGaugeFeet), '')
	, Second_Reading_Gauge_In = isnull(ltrim(T.ClosingGaugeInch), '')
	, Second_Reading_Gauge_Nu = isnull(ltrim(T.ClosingGaugeQ), '')
	, Second_Reading_Gauge_De = 4
	, Second_Temperature = isnull(ltrim(T.ProductLowTemp), '')
	, Second_Bottom_Ft = 0
	, Second_Bottom_In = 0
	, Second_Bottom_Nu = 0
	, Second_Bottom_De = 4
	, Shrinkage_Incrustation_Factor = 1
	, First_Reading_Meter = T.OpenMeterUnits
	, Second_Reading_Meter = T.CloseMeterUnits
	, Meter_Factor = T.MeterFactor
	, Temp_Comp_Meter = ''
	, Avg_Line_Temp = T.ProductObsTemp
	, Truck_ID = ''
	, Trailer_ID = ''
	, Driver_ID =''
	, Miles = ISNULL(O.ActualMiles, 0)
	, County = ''
	, State = ''
	, Invoice_Number = ''
	, Invoice_Date = ''
	, Remarks= ''
	, API_Compliant_Chapter = ''
	, Use_SXL_Calculation = 'Y'
	, Seal_On = dbo.fnTrimSealValue(T.SealOn, '0')
	, Seal_Off = dbo.fnTrimSealValue(T.SealOff, '0')
	, Ticket_Exclusion_Cd = CASE WHEN isnull(T.Rejected, O.Rejected) = 1 THEN 'RF' ELSE '' END
	, Confirmation_Number = isnull(T.DispatchConfirmNum, O.DispatchConfirmNum)
	, Split_Flag = CASE WHEN (SELECT COUNT(*) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND DeleteDateUTC IS NULL) > 1 THEN 'Y' ELSE 'N' END
	, Paired_Ticket_Number = isnull((SELECT min(CarrierTicketNum) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND DeleteDateUTC IS NULL AND OT.CarrierTicketNum <> T.CarrierTicketNum), '')
	, Bobtail_Flag = 'N'
	, Ticket_Extra_Info_Flag = CASE WHEN T.Rejected = 1 THEN T.RejectNum ELSE '' END
FROM viewOrderLocalDates O
JOIN dbo.viewOrderCustomerFinalExportPending EP ON EP.ID = O.ID
LEFT JOIN viewOrderTicket T ON T.OrderID = O.ID AND T.DeleteDateUTC IS NULL
/* Changed join to get destination code from tblCustomerDestinationCode to tblObjectCustomData 3.10.14 */
LEFT JOIN dbo.tblObjectCustomData OCD ON OCD.ObjectFieldID = 90001 AND OCD.RecordID = O.DestinationID
GO


COMMIT
SET NOEXEC OFF