/*
	-- add a Max Open/Close Temp Deviation (Degrees) system setting
	-- add ShowBottomDetails system setting
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.8.9'
SELECT  @NewVersion = '2.8.10'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

INSERT INTO tblSetting (ID, Name, SettingTypeID, Value, Category, CreateDateUTC, CreatedByUser, ReadOnly)
	SELECT 23, 'Max Open/Close Temp Deviation (Degrees)', 4, 5, 'Product Acceptance Criteria', GETUTCDATE(), 'System', 0
	
INSERT INTO tblSetting (ID, Name, SettingTypeID, Value, Category, CreateDateUTC, CreatedByUser, ReadOnly)
	SELECT 24, 'Show Bottom Detail Entry Fields (", Q)', 2, 'False', 'Product Acceptance Criteria', GETUTCDATE(), 'System', 0

COMMIT
SET NOEXEC OFF