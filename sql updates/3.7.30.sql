-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.7.29.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.7.29.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0
SET NOEXEC OFF
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.7.29'
SELECT  @NewVersion = '3.7.30'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'Settlement Center - remove DriverID criteria from Best-Match criteria'
	UNION
	SELECT @NewVersion, 0, 'Settlement Center - bug fixes to Carrier | Shipper RateSheet updates'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

DROP INDEX idxCarrierAssessorialRate_Driver ON dbo.tblCarrierAssessorialRate
DROP INDEX udxCarrierAssessorialRate_Main ON dbo.tblCarrierAssessorialRate
ALTER TABLE dbo.tblCarrierAssessorialRate DROP COLUMN DriverID
GO
CREATE UNIQUE CLUSTERED INDEX udxCarrierAssessorialRate_Main ON tblCarrierAssessorialRate
(
	TypeID ASC,
	ShipperID ASC,
	CarrierID ASC,
	ProductGroupID ASC,
	DriverGroupID ASC,
	OriginID ASC,
	DestinationID ASC,
	OriginStateID ASC,
	DestStateID ASC,
	RegionID ASC,
	ProducerID ASC,
	EffectiveDate ASC
)
GO

DROP INDEX idxCarrierDestinationWaitRate_Driver ON dbo.tblCarrierDestinationWaitRate
DROP INDEX udxCarrierDestinationWaitRate_Main ON dbo.tblCarrierDestinationWaitRate
ALTER TABLE dbo.tblCarrierDestinationWaitRate DROP COLUMN DriverID
GO
CREATE UNIQUE NONCLUSTERED INDEX udxCarrierDestinationWaitRate_Main ON tblCarrierDestinationWaitRate
(
	ReasonID ASC,
	ShipperID ASC,
	CarrierID ASC,
	ProductGroupID ASC,
	DriverGroupID ASC,
	DestinationID ASC,
	StateID ASC,
	RegionID ASC,
	ProducerID ASC,
	EffectiveDate ASC
)
GO

DROP INDEX idxCarrierOrderRejectRate_Driver ON dbo.tblCarrierOrderRejectRate
DROP INDEX udxCarrierOrderRejectRate_Main ON dbo.tblCarrierOrderRejectRate
ALTER TABLE dbo.tblCarrierOrderRejectRate DROP COLUMN DriverID
GO
CREATE UNIQUE NONCLUSTERED INDEX udxCarrierOrderRejectRate_Main ON tblCarrierOrderRejectRate
(
	ReasonID ASC,
	ShipperID ASC,
	CarrierID ASC,
	ProductGroupID ASC,
	DriverGroupID ASC,
	OriginID ASC,
	StateID ASC,
	RegionID ASC,
	ProducerID ASC,
	EffectiveDate ASC
)
GO

DROP INDEX idxCarrierOriginWaitRate_Driver ON dbo.tblCarrierOriginWaitRate
DROP INDEX udxCarrierOriginWaitRate_Main ON dbo.tblCarrierOriginWaitRate
ALTER TABLE dbo.tblCarrierOriginWaitRate DROP COLUMN DriverID
GO
CREATE UNIQUE NONCLUSTERED INDEX udxCarrierOriginWaitRate_Main ON tblCarrierOriginWaitRate
(
	ReasonID ASC,
	ShipperID ASC,
	CarrierID ASC,
	ProductGroupID ASC,
	DriverGroupID ASC,
	OriginID ASC,
	StateID ASC,
	RegionID ASC,
	ProducerID ASC,
	EffectiveDate ASC
)
GO

DROP INDEX idxCarrierRateSheet_Driver ON dbo.tblCarrierRateSheet
DROP INDEX udxCarrierRateSheet_Main ON dbo.tblCarrierRateSheet
ALTER TABLE dbo.tblCarrierRateSheet DROP COLUMN DriverID
GO
CREATE UNIQUE CLUSTERED INDEX udxCarrierRateSheet_Main ON tblCarrierRateSheet
(
	ShipperID ASC,
	CarrierID ASC,
	ProductGroupID ASC,
	DriverGroupID ASC,
	RegionID ASC,
	OriginStateID ASC,
	DestStateID ASC,
	ProducerID ASC,
	EffectiveDate ASC
)
GO

DROP INDEX idxCarrierRouteRate_Driver ON dbo.tblCarrierRouteRate
DROP INDEX udxCarrierRouteRate_Main ON dbo.tblCarrierRouteRate
ALTER TABLE dbo.tblCarrierRouteRate DROP COLUMN DriverID
GO
CREATE UNIQUE CLUSTERED INDEX udxCarrierRouteRate_Main ON tblCarrierRouteRate
(
	ShipperID ASC,
	CarrierID ASC,
	ProductGroupID ASC,
	DriverGroupID ASC,
	RouteID ASC,
	EffectiveDate ASC
)
GO

/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all CarrierAssessorialRate records
-- Changes:
	- 3.7.28 - 2015/06/18 - KDA - add new columns: move ProducerID column to end (so consistent with all other Best-Match rates/parameters
								- add DriverID/DriverGroup columns
	- 3.7.30 - 2015/06/19 - KDA - remove DriverID
******************************************************/
ALTER VIEW viewCarrierAssessorialRate AS
	SELECT X.*
		, MaxEffectiveDate = (SELECT min(OrderDate) FROM viewOrderSettlementCarrierAssessorialCharge WHERE AssessorialRateID = X.ID AND BatchID IS NOT NULL)
		, MinEndDate = (SELECT max(OrderDate) FROM viewOrderSettlementCarrierAssessorialCharge WHERE AssessorialRateID = X.ID AND BatchID IS NOT NULL)
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM viewOrderSettlementCarrierAssessorialCharge WHERE AssessorialRateID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblCarrierAssessorialRate XN 
			WHERE XN.TypeID = X.TypeID
			  AND dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT max(XN.EndDate) 
			FROM tblCarrierAssessorialRate XN 
			WHERE XN.TypeID = X.TypeID
			  AND dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EndDate < X.EffectiveDate)
	FROM tblCarrierAssessorialRate X

GO

/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all CarrierDestinationWaitRate records
-- Changes:
	- 3.7.28 - 2015/06/18 - KDA - Add DriverID | DriverGroupID | ProducerID
	- 3.7.30 - 2015/06/19 - KDA - remove DriverID
******************************************************/
ALTER VIEW viewCarrierDestinationWaitRate AS
	SELECT X.*
		, MaxEffectiveDate = (SELECT Min(OrderDate) FROM tblOrderSettlementCarrier WHERE DestinationWaitRateID = X.ID AND BatchID IS NOT NULL)
		, MinEndDate = (SELECT Max(OrderDate) FROM tblOrderSettlementCarrier WHERE DestinationWaitRateID = X.ID AND BatchID IS NOT NULL)
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementCarrier WHERE DestinationWaitRateID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate)
			FROM tblCarrierDestinationWaitRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(XN.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT max(XN.EndDate)
			FROM tblCarrierDestinationWaitRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(XN.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EndDate < X.EffectiveDate)
	FROM tblCarrierDestinationWaitRate X

GO

/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all CarrierFuelSurcharge records
	- 3.7.28 - 2015/06/18 - KDA - add ProducerID|DriverID|DriverGroupID
	- 3.7.30 - 2015/06/19 - KDA - remove DriverID
******************************************************/
ALTER VIEW viewCarrierFuelSurchargeRate AS
	SELECT X.*
		, MaxEffectiveDate = (SELECT min(OrderDate) FROM tblOrderSettlementCarrier WHERE FuelSurchargeRateID = X.ID AND BatchID IS NOT NULL)
		, MinEndDate = (SELECT max(OrderDate) FROM tblOrderSettlementCarrier WHERE FuelSurchargeRateID = X.ID AND BatchID IS NOT NULL)
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementCarrier WHERE FuelSurchargeRateID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate)
			FROM tblCarrierDestinationWaitRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverGroupID, X.DriverGroupID) = 1
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT max(XN.EndDate)
			FROM tblCarrierDestinationWaitRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverGroupID, X.DriverGroupID) = 1
			  AND XN.EndDate < X.EffectiveDate)
	FROM tblCarrierFuelSurchargeRate X

GO

/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all CarrierOrderRejectRate records
-- Changes:
	- 3.7.28 - 2015/06/18 - KDA - Add DriverID | DriverGroupID | ProducerID
	- 3.7.30 - 2015/06/19 - KDA - remove DriverID
******************************************************/
ALTER VIEW viewCarrierOrderRejectRate AS
	SELECT X.*
		, MaxEffectiveDate = (SELECT MIN(OrderDate) FROM tblOrderSettlementCarrier WHERE OrderRejectRateID = X.ID AND BatchID IS NOT NULL)
		, MinEndDate = (SELECT MAX(OrderDate) FROM tblOrderSettlementCarrier WHERE OrderRejectRateID = X.ID AND BatchID IS NOT NULL)
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementCarrier WHERE OrderRejectRateID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblCarrierOrderRejectRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(XN.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT max(XN.EndDate) 
			FROM tblCarrierOrderRejectRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(XN.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EndDate < X.EffectiveDate)
	FROM tblCarrierOrderRejectRate X

GO

/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all CarrierOriginWaitRate records
-- Changes:
	- 3.7.28 - 2015/06/18 - KDA - Add DriverID | DriverGroupID | ProducerID
	- 3.7.30 - 2015/06/19 - KDA - remove DriverID
******************************************************/
ALTER VIEW viewCarrierOriginWaitRate AS
	SELECT X.*
		, MaxEffectiveDate = (SELECT MIN(OrderDate) FROM tblOrderSettlementCarrier WHERE OriginWaitRateID = X.ID AND BatchID IS NOT NULL)
		, MinEndDate = (SELECT Max(OrderDate) FROM tblOrderSettlementCarrier WHERE OriginWaitRateID = X.ID AND BatchID IS NOT NULL)
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementCarrier WHERE OriginWaitRateID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate)
			FROM tblCarrierOriginWaitRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(XN.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT max(XN.EndDate)
			FROM tblCarrierOriginWaitRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(XN.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EndDate < X.EffectiveDate)
	FROM tblCarrierOriginWaitRate X

GO

/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all CarrierRateSheet records
-- Changes:
	- 3.7.28 - 2015/06/18 - KDA - Add DriverID | DriverGroupID | ProducerID
	- 3.7.30 - 2015/06/19 - KDA - remove DriverID
******************************************************/
ALTER VIEW viewCarrierRateSheet AS
	SELECT X.*
		, MaxEffectiveDate = (SELECT min(OrderDate) FROM tblOrderSettlementCarrier SC JOIN tblCarrierRangeRate RR ON RR.ID = SC.RangeRateID WHERE RR.RateSheetID = X.ID AND SC.BatchID IS NOT NULL)
		, MinEndDate = (SELECT max(OrderDate) FROM tblOrderSettlementCarrier SC JOIN tblCarrierRangeRate RR ON RR.ID = SC.RangeRateID WHERE RR.RateSheetID = X.ID AND SC.BatchID IS NOT NULL)
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementCarrier SC JOIN tblCarrierRangeRate RR ON RR.ID = SC.RangeRateID WHERE RR.RateSheetID = X.ID AND SC.BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblCarrierRateSheet XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT max(XN.EndDate) 
			FROM tblCarrierRateSheet XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EndDate < X.EffectiveDate)
	FROM tblCarrierRateSheet X

GO

/************************************************
-- Date Created: 18 Jan 2015
-- Author: Kevin Alons
-- Purpose: return combined RateSheet + RangeRate data + friendly translated values
			allow updating of both RateSheet & RangeRate data together
-- Changes:
	- 3.7.28 - 2015/06/18 - KDA - Add DriverID | DriverGroupID | ProducerID
	- 3.7.30 - 2015/06/19 - KDA - remove DriverID
************************************************/
ALTER VIEW viewCarrierRateSheetRangeRate AS
	SELECT RR.ID, RateSheetID = R.ID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.DriverGroupID, R.OriginStateID, R.DestStateID, R.RegionID, R.ProducerID, RR.Rate, RR.MinRange, RR.MaxRange, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate, R.NextEffectiveDate
		, R.Locked
		, RR.CreateDateUTC, RR.CreatedByUser
		, RR.LastChangeDateUTC, RR.LastChangedByUser
	FROM dbo.viewCarrierRateSheet R
	JOIN dbo.tblCarrierRangeRate RR ON RR.RateSheetID = R.ID
GO

/***************************************************
-- Date Created: 6 May 2015
-- Author: Kevin Alons
-- Purpose: return the RateSheet JOIN RangeRate data with translated "friendly" values
-- Changes:
	- 3.7.28 - 2015/06/18 - KDA - Add DriverID | DriverGroupID | ProducerID
	- 3.7.30 - 2015/06/19 - KDA - remove DriverID
***************************************************/
ALTER VIEW viewCarrierRateSheetRangeRatesDisplay AS
	SELECT R.*
		, Shipper = SH.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, DriverGroup = DG.Name
		, OriginState = OS.FullName
		, OriginStateAbbrev = OS.Abbreviation
		, DestinationState = DS.FullName
		, DestinationStateAbbrev = DS.Abbreviation
		, Region = REG.Name
		, Producer = P.Name
		, RateType = RT.Name
		, Uom = U.Name
		, UomShort = U.Abbrev
	FROM viewCarrierRateSheetRangeRate R
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblDriverGroup DG ON DG.ID = R.DriverGroupID
	LEFT JOIN tblState OS ON OS.ID = R.OriginStateID
	LEFT JOIN tblState DS ON DS.ID = R.DestStateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblProducer P ON P.ID = R.ProducerID
	LEFT JOIN tblRateType RT ON RT.ID = R.RateTypeID
	LEFT JOIN tblUom U ON U.ID = R.UomID
GO

/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all CarrierRouteRate records
-- Changes:
	- 3.7.28 - 2015/06/18 - KDA - ADDED
	- 3.7.30 - 2015/06/19 - KDA - remvoe DriverID
******************************************************/
ALTER VIEW viewCarrierRouteRate AS
	SELECT X.*
		, MaxEffectiveDate = (SELECT MIN(OrderDate) FROM tblOrderSettlementCarrier WHERE RouteRateID = X.ID AND BatchID IS NOT NULL)
		, MinEndDate = (SELECT MAX(OrderDate) FROM tblOrderSettlementCarrier WHERE RouteRateID = X.ID AND BatchID IS NOT NULL)
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementCarrier WHERE RouteRateID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, R.OriginID
		, R.DestinationID
		, R.ActualMiles
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblCarrierRouteRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverGroupID, X.DriverGroupID) = 1
			  AND XN.RouteID = X.RouteID
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT min(XN.EndDate) 
			FROM tblCarrierRouteRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverGroupID, X.DriverGroupID) = 1
			  AND XN.RouteID = X.RouteID
			  AND XN.EffectiveDate < X.EffectiveDate)
	FROM tblCarrierRouteRate X
	JOIN tblRoute R ON R.ID = X.RouteID
GO

/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all CarrierWaitFeeParameter records
-- Changes:
	- 3.7.28 - 2015/06/18 - KDA - ADDED
	- 3.7.30 - 2015/06/19 - KDA - remvoe DriverID
******************************************************/
ALTER VIEW viewCarrierWaitFeeParameter AS
	SELECT X.*
		, MaxEffectiveDate = (SELECT MIN(OrderDate) FROM tblOrderSettlementCarrier WHERE WaitFeeParameterID = X.ID AND BatchID IS NOT NULL)
		, MinEndDate = (SELECT MAX(OrderDate) FROM tblOrderSettlementCarrier WHERE WaitFeeParameterID = X.ID AND BatchID IS NOT NULL)
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementCarrier WHERE WaitFeeParameterID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblCarrierWaitFeeParameter XN
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT max(XN.EndDate) 
			FROM tblCarrierWaitFeeParameter XN
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EndDate < X.EffectiveDate)
	FROM tblCarrierWaitFeeParameter X

GO

/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all ShipperDestinationWaitRate records
-- Changes:
	- 3.7.28 - 2015/06/19 - KDA - add ProducerID criteria
	- 3.7.30 - 2015/06/19 - KDA - convert to use of fnCompareNullableInts()
******************************************************/
ALTER VIEW viewShipperDestinationWaitRate AS
	SELECT X.*
		, MaxEffectiveDate = (SELECT MIN(OrderDate) FROM tblOrderSettlementShipper WHERE DestinationWaitRateID = X.ID AND BatchID IS NOT NULL)
		, MinEndDate = (SELECT MAX(OrderDate) FROM tblOrderSettlementShipper WHERE DestinationWaitRateID = X.ID AND BatchID IS NOT NULL)
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementShipper WHERE DestinationWaitRateID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate)
			FROM tblShipperDestinationWaitRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(XN.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT max(XN.EndDate)
			FROM tblShipperDestinationWaitRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(XN.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EndDate < X.EffectiveDate)
	FROM tblShipperDestinationWaitRate X
GO

/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all ShipperFuelSurcharge records
-- Changes:
	- 3.7.28 - 2015/06/19 - KDA - add ProducerID criteria
	- 3.7.30 - 2015/06/19 - KDA - convert to use of fnCompareNullableInts()
******************************************************/
ALTER VIEW viewShipperFuelSurchargeRate AS
	SELECT X.*
		, MaxEffectiveDate = (SELECT MIN(OrderDate) FROM tblOrderSettlementShipper WHERE FuelSurchargeRateID = X.ID AND BatchID IS NOT NULL)
		, MinEndDate = (SELECT MAX(OrderDate) FROM tblOrderSettlementShipper WHERE FuelSurchargeRateID = X.ID AND BatchID IS NOT NULL)
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementShipper WHERE FuelSurchargeRateID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate)
			FROM tblShipperDestinationWaitRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT max(XN.EndDate)
			FROM tblShipperDestinationWaitRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND XN.EndDate < X.EffectiveDate)
	FROM tblShipperFuelSurchargeRate X

GO

/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all ShipperOrderRejectRate records
-- Changes:
	- 3.7.28 - 2015/06/19 - KDA - add ProducerID criteria
	- 3.7.30 - 2015/06/19 - KDA - convert to use of fnCompareNullableInts()
******************************************************/
ALTER VIEW viewShipperOrderRejectRate AS
	SELECT X.*
		, MaxEffectiveDate = (SELECT MIN(OrderDate) FROM tblOrderSettlementShipper WHERE OrderRejectRateID = X.ID AND BatchID IS NOT NULL)
		, MinEndDate = (SELECT MAX(OrderDate) FROM tblOrderSettlementShipper WHERE OrderRejectRateID = X.ID AND BatchID IS NOT NULL)
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementShipper WHERE OrderRejectRateID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblShipperOrderRejectRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(XN.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT max(XN.EndDate) 
			FROM tblShipperOrderRejectRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(XN.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EndDate < X.EffectiveDate)
	FROM tblShipperOrderRejectRate X

GO

/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all ShipperOriginWaitRate records
-- Changes:
	- 3.7.28 - 2015/06/19 - KDA - add ProducerID criteria
	- 3.7.30 - 2015/06/19 - KDA - convert to use of fnCompareNullableInts()
******************************************************/
ALTER VIEW viewShipperOriginWaitRate AS
	SELECT X.*
		, MaxEffectiveDate = (SELECT MIN(OrderDate) FROM tblOrderSettlementShipper WHERE OriginWaitRateID = X.ID AND BatchID IS NOT NULL)
		, MinEndDate = (SELECT MAX(OrderDate) FROM tblOrderSettlementShipper WHERE OriginWaitRateID = X.ID AND BatchID IS NOT NULL)
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementShipper WHERE OriginWaitRateID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblShipperOriginWaitRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(XN.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT max(XN.EndDate) 
			FROM tblShipperOriginWaitRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(XN.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EndDate < X.EffectiveDate)
	FROM tblShipperOriginWaitRate X

GO

/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all ShipperRateSheet records
-- Changes:
	- 3.7.28 - 2015/06/19 - KDA - add ProducerID criteria
	- 3.7.30 - 2015/06/19 - KDA - convert to use of fnCompareNullableInts()
******************************************************/
ALTER VIEW viewShipperRateSheet AS
	SELECT X.*
		, MaxEffectiveDate = (SELECT MIN(OrderDate) FROM tblOrderSettlementShipper SC JOIN tblShipperRangeRate RR ON RR.ID = SC.RangeRateID WHERE RR.RateSheetID = X.ID AND SC.BatchID IS NOT NULL)
		, MinEndDate = (SELECT MAX(OrderDate) FROM tblOrderSettlementShipper SC JOIN tblShipperRangeRate RR ON RR.ID = SC.RangeRateID WHERE RR.RateSheetID = X.ID AND SC.BatchID IS NOT NULL)
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementShipper SC JOIN tblShipperRangeRate RR ON RR.ID = SC.RangeRateID WHERE RR.RateSheetID = X.ID AND SC.BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblShipperRateSheet XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT max(XN.EndDate) 
			FROM tblShipperRateSheet XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EndDate < X.EffectiveDate)
	FROM tblShipperRateSheet X

GO

/************************************************
-- Date Created: 18 Jan 2015
-- Author: Kevin Alons
-- Purpose: return combined RateSheet + RangeRate data + friendly translated values
			allow updating of both RateSheet & RangeRate data together
-- Changes:
	- 3.7.28 - 2015/06/19 - KDA - add ProducerID criteria
	- 3.7.30 - 2015/06/19 - KDA - remove "Display" fields
************************************************/
ALTER VIEW viewShipperRateSheetRangeRate AS
	SELECT RR.ID, RateSheetID = R.ID, R.ShipperID, R.ProductGroupID, R.OriginStateID, R.DestStateID, R.RegionID, R.ProducerID, RR.Rate, RR.MinRange, RR.MaxRange, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate, R.NextEffectiveDate
		, R.Locked
		, RR.CreateDateUTC, RR.CreatedByUser
		, RR.LastChangeDateUTC, RR.LastChangedByUser
	FROM dbo.viewShipperRateSheet R
	JOIN dbo.tblShipperRangeRate RR ON RR.RateSheetID = R.ID
GO

/***************************************************
-- Date Created: 6 May 2015
-- Author: Kevin Alons
-- Purpose: return the RateSheet JOIN RangeRate data with translated "friendly" values
-- Changes:
	- 3.7.28 - 2015/06/19 - KDA - add ProducerID criteria
	- 3.7.30 - 2015/06/19 - KDA - cleanup
***************************************************/
ALTER VIEW viewShipperRateSheetRangeRatesDisplay AS
	SELECT R.*
		, Shipper = SH.Name
		, ProductGroup = PG.Name
		, OriginState = OS.FullName
		, OriginStateAbbrev = OS.Abbreviation
		, DestinationState = DS.FullName
		, DestinationStateAbbrev = DS.Abbreviation
		, Region = REG.Name
		, Producer = P.Name
		, RateType = RT.Name
		, Uom = U.Name
		, UomShort = U.Abbrev
	FROM viewShipperRateSheetRangeRate R
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblState OS ON OS.ID = R.OriginStateID
	LEFT JOIN tblState DS ON DS.ID = R.DestStateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblProducer P ON P.ID = R.ProducerID
	LEFT JOIN tblRateType RT ON RT.ID = R.RateTypeID
	LEFT JOIN tblUom U ON U.ID = R.UomID
GO

/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all ShipperWaitFeeParameter records
-- Changes:
	- 3.7.30 - 2015/06/19 - KDA - add ProducerID criteria
								- convert to use of fnCompareNullableInts()
******************************************************/
ALTER VIEW viewShipperWaitFeeParameter AS
	SELECT X.*
		, MaxEffectiveDate = (SELECT MIN(OrderDate) FROM tblOrderSettlementShipper WHERE WaitFeeParameterID = X.ID AND BatchID IS NOT NULL)
		, MinEndDate = (SELECT MAX(OrderDate) FROM tblOrderSettlementShipper WHERE WaitFeeParameterID = X.ID AND BatchID IS NOT NULL)
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementShipper WHERE WaitFeeParameterID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblShipperWaitFeeParameter XN
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT max(XN.EndDate) 
			FROM tblShipperWaitFeeParameter XN
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EndDate < X.EffectiveDate)
	FROM tblShipperWaitFeeParameter X
GO

/*************************************
-- Date Created: 18 Jan 2015
-- Author: Kevin Alons
-- Purpose: handle specialized logic related to editing Route Rates (due to combination of RateSheet + RangeRate tables)
-- Changes:
	- 3.7.29 - 2015/06/19 - KDA - add support for DriverID/DriverGroupID/ProducerID
	- 3.7.30 - 2015/06/19 - KDA - remove DriverID
*************************************/
ALTER TRIGGER trigViewCarrierRateSheetRangeRate_IU_Update ON viewCarrierRateSheetRangeRate INSTEAD OF INSERT, UPDATE  AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
		-- PRINT 'apply any RateSheet changes to the underlying RateSheet records'
		UPDATE tblCarrierRateSheet
			SET ShipperID = i.ShipperID
				, CarrierID = i.CarrierID
				, ProductGroupID = i.ProductGroupID
				, DriverGroupID = i.DriverGroupID
				, OriginStateID = i.OriginStateID
				, DestStateID = i.DestStateID
				, RegionID = i.RegionID
				, ProducerID = i.ProducerID
				, EffectiveDate = i.EffectiveDate
				, EndDate = i.EndDate
				, RateTypeID = i.RateTypeID
				, UomID = i.UomID
				, LastChangeDateUTC = isnull(i.LastChangeDateUTC, getutcdate())
				, LastChangedByUser = i.LastChangedByUser
		FROM tblCarrierRateSheet RS
		JOIN ( 
			SELECT DISTINCT i.RateSheetID, i.ShipperID, i.CarrierID, i.ProductGroupID, i.DriverGroupID, i.OriginStateID, i.DestStateID, i.RegionID, i.ProducerID, i.EffectiveDate, i.EndDate, i.RateTypeID, i.UomID, i.LastChangeDateUTC, i.LastChangedByUser
			FROM inserted i 
			JOIN deleted d ON d.ID = i.ID
			WHERE dbo.fnCompareNullableInts(i.ShipperID, d.ShipperID) = 0
				OR dbo.fnCompareNullableInts(i.CarrierID, d.CarrierID) = 0
				OR dbo.fnCompareNullableInts(i.ProductGroupID, d.ProductGroupID) = 0
				OR dbo.fnCompareNullableInts(i.DriverGroupID, d.DriverGroupID) = 0
				OR dbo.fnCompareNullableInts(i.OriginStateID, d.OriginStateID) = 0
				OR dbo.fnCompareNullableInts(i.DestStateID, d.DestStateID) = 0
				OR dbo.fnCompareNullableInts(i.RegionID, d.RegionID) = 0
				OR dbo.fnCompareNullableInts(i.ProducerID, d.ProducerID) = 0
				OR i.EndDate <> d.EndDate
		) i ON i.RateSheetID = RS.ID

		--PRINT 'ensure a RateSheet record exists for each new RangeRate records'
		INSERT INTO tblCarrierRateSheet (ShipperID, CarrierID, ProductGroupID, DriverGroupID, OriginStateID, DestStateID, RegionID, ProducerID, EffectiveDate, EndDate, RateTypeID, UomID, CreatedByUser)
			SELECT i.ShipperID, i.CarrierID, i.ProductGroupID, i.DriverGroupID, i.OriginStateID, i.DestStateID, i.RegionID, i.ProducerID, i.EffectiveDate, i.EndDate, i.RateTypeID, i.UomID, i.CreatedByUser 
			FROM inserted i
			LEFT JOIN tblCarrierRateSheet RS 
				ON i.EffectiveDate = RS.EffectiveDate
					AND dbo.fnCompareNullableInts(i.ShipperID, RS.ShipperID) = 1
					AND dbo.fnCompareNullableInts(i.CarrierID, RS.CarrierID) = 1
					AND dbo.fnCompareNullableInts(i.ProductGroupID, RS.ProductGroupID) = 1
					AND dbo.fnCompareNullableInts(i.DriverGroupID, RS.DriverGroupID) = 1
					AND dbo.fnCompareNullableInts(i.OriginStateID, RS.OriginStateID) = 1
					AND dbo.fnCompareNullableInts(i.DestStateID, RS.DestStateID) = 1
					AND dbo.fnCompareNullableInts(i.RegionID, RS.RegionID) = 1
					AND dbo.fnCompareNullableInts(i.ProducerID, RS.ProducerID) = 1
			WHERE isnull(RateSheetID, 0) = 0
			  AND RS.ID IS NULL
		
		-- optimization for typical usaage (where only 1 record is INSERTed)
		IF (SELECT count(1) FROM inserted i WHERE isnull(ID, 0) = 0) = 1
		BEGIN
			DECLARE @rsID int
			SELECT @rsID = isnull(RateSheetID, SCOPE_IDENTITY()) FROM inserted
			IF @rsID IS NULL
				SELECT @rsID = RS.ID
				FROM inserted i
				LEFT JOIN tblCarrierRateSheet RS 
					ON i.EffectiveDate = RS.EffectiveDate
						AND dbo.fnCompareNullableInts(i.ShipperID, RS.ShipperID) = 1
						AND dbo.fnCompareNullableInts(i.CarrierID, RS.CarrierID) = 1
						AND dbo.fnCompareNullableInts(i.ProductGroupID, RS.ProductGroupID) = 1
						AND dbo.fnCompareNullableInts(i.DriverGroupID, RS.DriverGroupID) = 1
						AND dbo.fnCompareNullableInts(i.OriginStateID, RS.OriginStateID) = 1
						AND dbo.fnCompareNullableInts(i.DestStateID, RS.DestStateID) = 1
						AND dbo.fnCompareNullableInts(i.RegionID, RS.RegionID) = 1
						AND dbo.fnCompareNullableInts(i.ProducerID, RS.ProducerID) = 1

			INSERT INTO tblCarrierRangeRate (RateSheetID, MinRange, MaxRange, Rate, CreateDateUTC, CreatedByUser)
				SELECT @rsID, i.MinRange, i.MaxRange, i.Rate, isnull(i.CreateDateUTC, getutcdate()), i.CreatedByUser
				FROM inserted i
				WHERE isnull(ID, 0) = 0
		END
		ELSE 	-- bulk insert or update
		BEGIN
			-- PRINT 'Updating any existing record editable data'
			UPDATE tblCarrierRangeRate
				SET MinRange = i.MinRange
					, MaxRange = i.MaxRange
					, Rate = i.Rate
					, LastChangeDateUTC = isnull(i.LastChangeDateUTC, getutcdate())
					, LastChangedByUser = i.LastChangedByUser
			FROM tblCarrierRangeRate X
			JOIN inserted i ON i.ID = X.ID

			-- PRINT 'insert any new records'
			INSERT INTO tblCarrierRangeRate (RateSheetID, MinRange, MaxRange, Rate, CreatedByUser)
				SELECT R.ID, i.MinRange, i.MaxRange, i.Rate, isnull(i.CreatedByUser, R.CreatedByUser)
				FROM inserted i
				JOIN tblCarrierRateSheet R ON i.EffectiveDate = R.EffectiveDate
					AND dbo.fnCompareNullableInts(i.ShipperID, R.ShipperID) = 1
					AND dbo.fnCompareNullableInts(i.CarrierID, R.CarrierID) = 1
					AND dbo.fnCompareNullableInts(i.ProductGroupID, R.ProductGroupID) = 1
					AND dbo.fnCompareNullableInts(i.DriverGroupID, R.DriverGroupID) = 1
					AND dbo.fnCompareNullableInts(i.OriginStateID, R.OriginStateID) = 1
					AND dbo.fnCompareNullableInts(i.DestStateID, R.DestStateID) = 1
					AND dbo.fnCompareNullableInts(i.RegionID, R.RegionID) = 1
					AND dbo.fnCompareNullableInts(i.ProducerID, R.ProducerID) = 1
				WHERE ISNULL(i.ID, 0) = 0
		END
	END TRY
	BEGIN CATCH
		DECLARE @error varchar(255)
		SET @error = substring(ERROR_MESSAGE(), 1, 255)
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END CATCH
END
GO

/*************************************
-- Date Created: 18 Jan 2015
-- Author: Kevin Alons
-- Purpose: handle specialized logic related to editing Route Rates (due to combination of RateSheet + RangeRate tables)
-- Changes:
	- 3.7.30 - 2015/06/19 - KDA - fix missing criteria comparisons
*************************************/
ALTER TRIGGER trigViewShipperRateSheetRangeRate_IU_Update ON viewShipperRateSheetRangeRate INSTEAD OF INSERT, UPDATE  AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
		-- PRINT 'apply any RateSheet changes to the underlying RateSheet records'
		UPDATE tblShipperRateSheet
			SET ShipperID = i.ShipperID
				, ProductGroupID = i.ProductGroupID
				, OriginStateID = i.OriginStateID
				, DestStateID = i.DestStateID
				, RegionID = i.RegionID
				, ProducerID = i.ProducerID
				, EffectiveDate = i.EffectiveDate
				, EndDate = i.EndDate
				, RateTypeID = i.RateTypeID
				, UomID = i.UomID
				, LastChangeDateUTC = isnull(i.LastChangeDateUTC, getutcdate())
				, LastChangedByUser = i.LastChangedByUser
		FROM tblShipperRateSheet RS
		JOIN ( 
			SELECT DISTINCT i.RateSheetID, i.ShipperID, i.ProductGroupID, i.OriginStateID, i.DestStateID, i.RegionID, i.ProducerID, i.EffectiveDate, i.EndDate, i.RateTypeID, i.UomID, i.LastChangeDateUTC, i.LastChangedByUser
			FROM inserted i 
			JOIN deleted d ON d.ID = i.ID
			WHERE dbo.fnCompareNullableInts(i.ShipperID, d.ShipperID) = 0
				OR dbo.fnCompareNullableInts(i.ProductGroupID, d.ProductGroupID) = 0
				OR dbo.fnCompareNullableInts(i.OriginStateID, d.OriginStateID) = 0
				OR dbo.fnCompareNullableInts(i.DestStateID, d.DestStateID) = 0
				OR dbo.fnCompareNullableInts(i.RegionID, d.RegionID) = 0
				OR dbo.fnCompareNullableInts(i.ProducerID, d.ProducerID) = 0
				OR i.EndDate <> d.EndDate
		) i ON i.RateSheetID = RS.ID

		--PRINT 'ensure a RateSheet record exists for each new RangeRate records'
		INSERT INTO tblShipperRateSheet (ShipperID, ProductGroupID, OriginStateID, DestStateID, RegionID, ProducerID, EffectiveDate, EndDate, RateTypeID, UomID, CreatedByUser)
			SELECT i.ShipperID, i.ProductGroupID, i.OriginStateID, i.DestStateID, i.RegionID, i.ProducerID, i.EffectiveDate, i.EndDate, i.RateTypeID, i.UomID, i.CreatedByUser 
			FROM inserted i
			LEFT JOIN tblShipperRateSheet RS 
				ON i.EffectiveDate = RS.EffectiveDate
					AND dbo.fnCompareNullableInts(i.ShipperID, RS.ShipperID) = 1
					AND dbo.fnCompareNullableInts(i.ProductGroupID, RS.ProductGroupID) = 1
					AND dbo.fnCompareNullableInts(i.OriginStateID, RS.OriginStateID) = 1
					AND dbo.fnCompareNullableInts(i.DestStateID, RS.DestStateID) = 1
					AND dbo.fnCompareNullableInts(i.RegionID, RS.RegionID) = 1
					AND dbo.fnCompareNullableInts(i.ProducerID, RS.ProducerID) = 1
			WHERE isnull(RateSheetID, 0) = 0
			  AND RS.ID IS NULL
		
		-- optimization for typical usaage (where only 1 record is INSERTed)
		IF (SELECT count(1) FROM inserted i WHERE isnull(ID, 0) = 0) = 1
		BEGIN
			DECLARE @rsID int
			SELECT @rsID = isnull(RateSheetID, SCOPE_IDENTITY()) FROM inserted
			IF @rsID IS NULL
				SELECT @rsID = RS.ID
				FROM inserted i
				LEFT JOIN tblShipperRateSheet RS 
					ON i.EffectiveDate = RS.EffectiveDate
						AND dbo.fnCompareNullableInts(i.ShipperID, RS.ShipperID) = 1
						AND dbo.fnCompareNullableInts(i.ProductGroupID, RS.ProductGroupID) = 1
						AND dbo.fnCompareNullableInts(i.OriginStateID, RS.OriginStateID) = 1
						AND dbo.fnCompareNullableInts(i.DestStateID, RS.DestStateID) = 1
						AND dbo.fnCompareNullableInts(i.RegionID, RS.RegionID) = 1
						AND dbo.fnCompareNullableInts(i.ProducerID, RS.ProducerID) = 1

			INSERT INTO tblShipperRangeRate (RateSheetID, MinRange, MaxRange, Rate, CreateDateUTC, CreatedByUser)
				SELECT @rsID, i.MinRange, i.MaxRange, i.Rate, isnull(i.CreateDateUTC, getutcdate()), i.CreatedByUser
				FROM inserted i
				WHERE isnull(ID, 0) = 0
		END
		ELSE 	-- bulk insert or update
		BEGIN
			-- PRINT 'Updating any existing record editable data'
			UPDATE tblShipperRangeRate
				SET MinRange = i.MinRange
					, MaxRange = i.MaxRange
					, Rate = i.Rate
					, LastChangeDateUTC = isnull(i.LastChangeDateUTC, getutcdate())
					, LastChangedByUser = i.LastChangedByUser
			FROM tblShipperRangeRate X
			JOIN inserted i ON i.ID = X.ID

			-- PRINT 'insert any new records'
			INSERT INTO tblShipperRangeRate (RateSheetID, MinRange, MaxRange, Rate, CreatedByUser)
				SELECT R.ID, i.MinRange, i.MaxRange, i.Rate, isnull(i.CreatedByUser, R.CreatedByUser)
				FROM inserted i
				JOIN tblShipperRateSheet R ON i.EffectiveDate = R.EffectiveDate
					AND dbo.fnCompareNullableInts(i.ShipperID, R.ShipperID) = 1
					AND dbo.fnCompareNullableInts(i.ProductGroupID, R.ProductGroupID) = 1
					AND dbo.fnCompareNullableInts(i.OriginStateID, R.OriginStateID) = 1
					AND dbo.fnCompareNullableInts(i.DestStateID, R.DestStateID) = 1
					AND dbo.fnCompareNullableInts(i.ProducerID, R.ProducerID) = 1
					AND dbo.fnCompareNullableInts(i.RegionID, R.RegionID) = 1
				WHERE ISNULL(i.ID, 0) = 0
		END
	END TRY
	BEGIN CATCH
		DECLARE @error varchar(255)
		SET @error = substring(ERROR_MESSAGE(), 1, 255)
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END CATCH
END
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier AssessorialRate info for the specified order
-- Changes:
	- 3.7.28 - 2015/06/18 - KDA - add new columns: move ProducerID parameter to end (so consistent with all other Best-Match rates/parameters
								 - add DriverID/DriverGroup parameters
	- 3.7.30 - 2015/06/19 - KDA - remove DriverID							 
***********************************/
ALTER FUNCTION fnCarrierAssessorialRates
(
  @StartDate date
, @EndDate date
, @TypeID int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @DriverGroupID int
, @OriginID int
, @DestinationID int
, @OriginStateID int
, @DestStateID int
, @RegionID int
, @ProducerID int
, @BestMatchOnly bit = 0
)
RETURNS 
	@ret TABLE (
		ID int
	  , TypeID int
	  , ShipperID int
	  , CarrierID int
	  , ProductGroupID int
	  , DriverGroupID int
	  , OriginID int
	  , DestinationID int
	  , OriginStateID int
	  , DestStateID int
	  , RegionID int
	  , ProducerID int
	  , Rate decimal(18, 10)
	  , RateTypeID int
	  , UomID int
	  , EffectiveDate date
	  , EndDate date
	  , MaxEffectiveDate date
	  , MinEndDate date
	  , NextEffectiveDate date
	  , PriorEndDate date	  
	  , BestMatch bit
	  , Ranking smallmoney
	  , Locked bit
	  , CreateDateUTC datetime
	  , CreatedByUser varchar(100)
	  , LastChangeDateUTC datetime
	  , LastChangedByUser varchar(100)
	)
AS BEGIN
	-- get the raw data (all matched rows)
	DECLARE @src TABLE (ID int, TypeID int, Ranking smallmoney)
	INSERT INTO @src (ID, TypeID, Ranking)
		SELECT ID, TypeID
			, Ranking =	dbo.fnRateRanking(@ShipperID, R.ShipperID, 512, 0)
					  + dbo.fnRateRanking(@CarrierID, R.CarrierID, 256, 0)
					  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 128, 0)
					  + dbo.fnRateRanking(@DriverGroupID, R.DriverGroupID, 64, 1)
					  + dbo.fnRateRanking(@OriginID, R.OriginID, 32, 0)
					  + dbo.fnRateRanking(@DestinationID, R.DestinationID, 16, 0)
					  + dbo.fnRateRanking(@OriginStateID, R.OriginStateID, 8, 0)
					  + dbo.fnRateRanking(@DestStateID, R.DestStateID, 4, 0)
					  + dbo.fnRateRanking(@RegionID, R.RegionID, 2, 0)
					  + dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM dbo.viewCarrierAssessorialRate R
		WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(R.ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@DriverGroupID, 0), R.DriverGroupID, 0) = coalesce(DriverGroupID, nullif(@DriverGroupID, 0), 0)
		  AND coalesce(nullif(@OriginID, 0), R.OriginID, 0) = coalesce(R.OriginID, nullif(@OriginID, 0), 0)
		  AND coalesce(nullif(@DestinationID, 0), R.DestinationID, 0) = coalesce(R.DestinationID, nullif(@DestinationID, 0), 0)
		  AND coalesce(nullif(@OriginStateID, 0), R.OriginStateID, 0) = coalesce(R.OriginStateID, nullif(@OriginStateID, 0), 0)
		  AND coalesce(nullif(@DestStateID, 0), R.DestStateID, 0) = coalesce(R.DestStateID, nullif(@DestStateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(R.RegionID, nullif(@RegionID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(R.ProducerID, nullif(@ProducerID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
		  AND (nullif(@TypeID, 0) IS NULL OR TypeID = @TypeID)

	-- return the rate data for the best-match rates 
	INSERT INTO @ret (ID, TypeID, ShipperID, CarrierID, ProductGroupID, DriverGroupID, OriginID, DestinationID, OriginStateID, DestStateID, RegionID, ProducerID, Rate, RateTypeID, UomID, EffectiveDate, EndDate
			, MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate, BestMatch, Ranking, Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser)
		SELECT CAR.ID, TypeID, ShipperID, CarrierID, ProductGroupID, DriverGroupID, OriginID, DestinationID, OriginStateID, DestStateID, RegionID, ProducerID, Rate, RateTypeID, UomID, EffectiveDate, EndDate
			, MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate, BestMatch, Ranking, Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser
		FROM viewCarrierAssessorialRate CAR
		JOIN (
			SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.TypeID IS NULL THEN 0 ELSE 1 END as bit)
			FROM @src S
			LEFT JOIN (
				SELECT TypeID, Ranking = MAX(Ranking)
				FROM @src
				WHERE @StartDate = ISNULL(@EndDate, @StartDate)
				  AND Ranking % 1 = 0.01 * 10  -- ensure some type of match occurred on all criteria choices
				GROUP BY TypeID
			) X ON X.TypeID = S.TypeID AND X.Ranking = S.Ranking
		) X ON X.ID = CAR.ID
		WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
		
	RETURN
END
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier AssessorialRate rows for the specified criteria
-- Changes:
	- 3.7.28 - 2015/06/18 - KDA - add new columns: move ProducerID parameter to end (so consistent with all other Best-Match rates/parameters
								 - add DriverID/DriverGroup parameters
	- 3.7.30 - 2015/06/19 - KDA - remove DriverID							 
**********************************/
ALTER FUNCTION fnCarrierAssessorialRatesDisplay
(  
  @StartDate date
, @EndDate date
, @TypeID int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @DriverGroupID int
, @OriginID int
, @DestinationID int
, @OriginStateID int
, @DestStateID int
, @RegionID int
, @ProducerID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.DriverGroupID, R.OriginID, R.DestinationID, R.OriginStateID, R.DestStateID, R.RegionID, R.ProducerID, R.TypeID, R.Rate, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Type = RT.Name
		, Shipper = S.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, DriverGroup = DG.Name
		, Origin = O.Name
		, OriginFull = O.FullName
		, Destination = D.Name
		, DestinationFull = D.FullName
		, OriginState = OS.FullName
		, OriginStateAbbrev = OS.Abbreviation
		, DestState = DS.FullName
		, DestStateAbbrev = DS.Abbreviation
		, Region = RE.Name
		, Producer = PR.Name
		, RateType = RT.Name
		, Uom = U.Name
		, UomShort = U.Abbrev
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch, Ranking
	FROM dbo.fnCarrierAssessorialRates(@StartDate, @EndDate, @TypeID, @ShipperID, @CarrierID, @ProductGroupID, @DriverGroupID, @OriginID, @DestinationID, @OriginStateID, @DestStateID, @RegionID, @ProducerID, 0) R
	JOIN tblAssessorialRateType RT ON RT.ID = R.TypeID
	LEFT JOIN tblCustomer S ON S.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblDriverGroup DG ON DG.ID = R.DriverGroupID
	LEFT JOIN viewOrigin O ON O.ID = R.OriginID
	LEFT JOIN viewDestination D ON D.ID = R.DestinationID
	LEFT JOIN tblState OS ON OS.ID = R.OriginStateID
	LEFT JOIN tblState DS ON DS.ID = R.DestStateID
	LEFT JOIN tblRegion RE ON RE.ID = R.RegionID
	LEFT JOIN tblProducer PR ON PR.ID = R.ProducerID
	LEFT JOIN tblUom U ON U.ID = R.UomID
	ORDER BY EffectiveDate
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier DestinationWaitRate info for the specified criteria
-- Changes:
	- 3.7.28 - 2015/06/13 - KDA - Add DriverID | DriverGroupID | ProducerID
	- 3.7.30 - 2015/06/19 - KDA - remove DriverID
***********************************/
ALTER FUNCTION fnCarrierDestinationWaitRate
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @DriverGroupID int
, @DestinationID int
, @StateID int
, @RegionID int
, @ProducerID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID, ReasonID
			, Ranking =	dbo.fnRateRanking(@ReasonID, R.ReasonID, 256, 1)
				  + dbo.fnRateRanking(@ShipperID, R.ShipperID, 128, 0)
				  + dbo.fnRateRanking(@CarrierID, R.CarrierID, 64, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 32, 0)
				  + dbo.fnRateRanking(@DriverGroupID, R.DriverGroupID, 16, 1)
				  + dbo.fnRateRanking(@DestinationID, R.DestinationID, 8, 0)
				  + dbo.fnRateRanking(@StateID, R.StateID, 4, 0)
				  + dbo.fnRateRanking(@RegionID, R.RegionID, 2, 0)
				  + dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 0)
		FROM  dbo.viewCarrierDestinationWaitRate R
		WHERE coalesce(nullif(@ReasonID, 0), R.ReasonID, 0) = coalesce(ReasonID, nullif(@ReasonID, 0), 0)
		  AND coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@DriverGroupID, 0), R.DriverGroupID, 0) = coalesce(DriverGroupID, nullif(@DriverGroupID, 0), 0)
		  AND coalesce(nullif(@DestinationID, 0), R.DestinationID, 0) = coalesce(DestinationID, nullif(@DestinationID, 0), 0)
		  AND coalesce(nullif(@StateID, 0), R.StateID, 0) = coalesce(StateID, nullif(@StateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(ProducerID, nullif(@ProducerID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	),
	cte2 AS
	(
		SELECT ID, S.ReasonID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT ReasonID = ISNULL(ReasonID, 0), Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 9  -- ensure some type of match occurred on all criteria choices
			GROUP BY ReasonID
		) X ON X.ReasonID = isnull(S.ReasonID, 0) AND X.Ranking = S.Ranking
	), 
	cte3 AS
	(	-- when both ReasonID = NULL AND ReasonID = @ReasonID records are BestMatch = 1, favor the ReasonID = @ReasonID record
		SELECT ID, X.ReasonID, X.Ranking, BestMatch = CASE WHEN BEST.ReasonID IS NULL THEN X.BestMatch ELSE 0 END
		FROM cte2 X
		LEFT JOIN (
			SELECT TOP 1 ReasonID FROM cte2 WHERE ReasonID = @ReasonID AND BestMatch = 1 ORDER BY Ranking DESC
		) BEST ON X.ReasonID IS NULL AND X.BestMatch = 1
	)
	
	SELECT TOP (CASE WHEN @BestMatchOnly = 1 THEN 1 ELSE 10000 END) R.ID, R.ReasonID, ShipperID, CarrierID, ProductGroupID, DriverGroupID, DestinationID, StateID, RegionID, ProducerID
		, Rate, EffectiveDate, EndDate
	  , MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate
	  , BestMatch, Ranking
	  , Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewCarrierDestinationWaitRate R
	JOIN cte3 X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
	ORDER BY Ranking DESC
)
GO

/***********************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier DestinationWaitRate rows for the specified criteria
-- Changes:
	- 3.7.28 - 2015/06/13 - KDA - Add DriverID | DriverGroupID | ProducerID
	- 3.7.30 - 2015/06/19 - KDA - remove DriverID
***********************************/
ALTER FUNCTION fnCarrierDestinationWaitRatesDisplay
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @DriverGroupID int
, @DestinationID int
, @StateID int
, @RegionID int
, @ProducerID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ReasonID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.DriverGroupID, R.DestinationID, R.StateID, R.RegionID, R.ProducerID, R.Rate, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Shipper = SH.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, DriverGroup = DG.Name
		, Destination = D.Name
		, DestinationFull = D.FullName
		, State = S.FullName
		, StateAbbrev = S.Abbreviation
		, Region = REG.Name
		, Producer = P.Name
		, Reason = REA.Description
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
		, Ranking
	FROM dbo.fnCarrierDestinationWaitRate(@StartDate, @EndDate, @ReasonID, @ShipperID, @CarrierID, @ProductGroupID, @DriverGroupID, @DestinationID, @StateID, @RegionID, @ProducerID, 0) R
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblDriverGroup DG ON DG.ID = R.DriverGroupID
	LEFT JOIN viewDestination D ON D.ID = R.DestinationID
	LEFT JOIN tblState S ON S.ID = R.StateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblProducer P ON P.ID = R.ProducerID
	LEFT JOIN tblDestinationWaitReason REA ON REA.ID = R.ReasonID
	ORDER BY EffectiveDate
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier DestinationWaitRate info for the specified criteria
-- Changes:
	- 3.7.28 - 2015/06/18 - KDA - add DriverID/DriverGroupID parameters
	- 3.7.30 - 2015/06/19 - KDA - remove DriverID
***********************************/
ALTER FUNCTION fnCarrierFuelSurchargeRate
(
  @StartDate date
, @EndDate date
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @DriverGroupID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID
			, Ranking =	dbo.fnRateRanking(@ShipperID, R.ShipperID, 8, 0)
				  + dbo.fnRateRanking(@CarrierID, R.CarrierID, 4, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 2, 0)
				  + dbo.fnRateRanking(@DriverGroupID, R.DriverGroupID, 1, 1)
		FROM  dbo.viewCarrierFuelSurchargeRate R
		WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@DriverGroupID, 0), R.DriverGroupID, 0) = coalesce(DriverGroupID, nullif(@DriverGroupID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	)
	SELECT R.ID, ShipperID, CarrierID, ProductGroupID, DriverGroupID, FuelPriceFloor, IntervalAmount, IncrementAmount, EffectiveDate, EndDate
	  , MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate
	  , BestMatch, Ranking
	  , Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewCarrierFuelSurchargeRate R
	JOIN (
		SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 4  -- ensure some type of match occurred on all criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)
GO

/***********************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RouteRate rows for the specified criteria
-- Changes:
	- 3.7.28 - 2015/06/18 - KDA - add DriverID/DriverGroupID parameters
	- 3.7.30 - 2015/06/19 - KDA - remove DriverID
***********************************/
ALTER FUNCTION fnCarrierFuelSurchargeRateDisplay
(
  @StartDate date
, @EndDate date
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @DriverGroupID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.DriverGroupID, R.FuelPriceFloor, R.IntervalAmount, R.IncrementAmount, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Shipper = S.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, DriverGroup = DG.Name
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch, Ranking
	FROM dbo.fnCarrierFuelSurchargeRate(@StartDate, @EndDate, @ShipperID, @CarrierID, @ProductGroupID, @DriverGroupID, 0) R
	LEFT JOIN tblCustomer S ON S.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblDriverGroup DG ON DG.ID = R.DriverGroupID
	ORDER BY EffectiveDate
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OrderRejectRate info for the specified criteria
-- Changes:
	- 3.7.28 - 2015/06/18 - KDA - add DriverID/DriverGroupID/Producer parameters
	- 3.7.30 - 2015/06/18 - KDA - remove DriverID
***********************************/
ALTER FUNCTION fnCarrierOrderRejectRate
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @DriverGroupID int
, @OriginID int
, @StateID int
, @RegionID int
, @ProducerID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID, ReasonID
			, Ranking =	dbo.fnRateRanking(@ReasonID, R.ReasonID, 256, 1)
				  + dbo.fnRateRanking(@ShipperID, R.ShipperID, 128, 0)
				  + dbo.fnRateRanking(@CarrierID, R.CarrierID, 64, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 32, 0)
				  + dbo.fnRateRanking(@DriverGroupID, R.DriverGroupID, 16, 1)
				  + dbo.fnRateRanking(@OriginID, R.OriginID, 8, 0)
				  + dbo.fnRateRanking(@StateID, R.StateID, 4, 0)
				  + dbo.fnRateRanking(@RegionID, R.RegionID, 2, 0)
				  + dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM  dbo.viewCarrierOrderRejectRate R
		WHERE coalesce(nullif(@ReasonID, 0), R.ReasonID, 0) = coalesce(ReasonID, nullif(@ReasonID, 0), 0)
		  AND coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@DriverGroupID, 0), R.DriverGroupID, 0) = coalesce(DriverGroupID, nullif(@DriverGroupID, 0), 0)
		  AND coalesce(nullif(@OriginID, 0), R.OriginID, 0) = coalesce(OriginID, nullif(@OriginID, 0), 0)
		  AND coalesce(nullif(@StateID, 0), R.StateID, 0) = coalesce(StateID, nullif(@StateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(ProducerID, nullif(@ProducerID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	),
	cte2 AS
	(
		SELECT ID, S.ReasonID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT ReasonID = ISNULL(ReasonID, 0), Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 9  -- ensure some type of match occurred on all criteria choices
			GROUP BY ReasonID
		) X ON X.ReasonID = isnull(S.ReasonID, 0) AND X.Ranking = S.Ranking
	), 
	cte3 AS
	(	-- when both ReasonID = NULL AND ReasonID = @ReasonID records are BestMatch = 1, favor the ReasonID = @ReasonID record
		SELECT ID, X.ReasonID, X.Ranking, BestMatch = CASE WHEN BEST.ReasonID IS NULL THEN X.BestMatch ELSE 0 END
		FROM cte2 X
		LEFT JOIN (
			SELECT TOP 1 ReasonID FROM cte2 WHERE ReasonID = @ReasonID AND BestMatch = 1 ORDER BY Ranking DESC
		) BEST ON X.ReasonID IS NULL AND X.BestMatch = 1
	)
	
	SELECT R.ID, R.ReasonID, ShipperID, CarrierID, ProductGroupID, DriverGroupID, OriginID, StateID, RegionID, ProducerID
	  , Rate, RateTypeID, UomID, EffectiveDate, EndDate
	  , MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate
	  , BestMatch, Ranking
	  , Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewCarrierOrderRejectRate R
	JOIN cte3 X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)
GO

/***********************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OrderRejectRate rows for the specified criteria
-- Changes:
	-- 3.7.28 - 2015/06/18 - KDA - add DriverID/DriverGroupID/ProducerID parameters
***********************************/
ALTER FUNCTION fnCarrierOrderRejectRatesDisplay
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @DriverID int
, @DriverGroupID int
, @OriginID int
, @StateID int
, @RegionID int
, @ProducerID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ReasonID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.DriverGroupID, R.OriginID, R.StateID, R.RegionID, R.ProducerID
		, R.Rate, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Shipper = SH.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, DriverGroup = DG.Name
		, Origin = D.Name
		, OriginFull = D.FullName
		, State = S.FullName
		, StateAbbrev = S.Abbreviation
		, Region = REG.Name
		, Producer = PR.Name
		, Reason = REA.Description
		, RateType = RT.Name
		, Uom = U.Name
		, UomShort = U.Abbrev
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
	FROM dbo.fnCarrierOrderRejectRate(@StartDate, @EndDate, @ReasonID, @ShipperID, @CarrierID, @ProductGroupID, @DriverGroupID, @OriginID, @StateID, @RegionID, @ProducerID, 0) R
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblDriverGroup DG ON DG.ID = R.DriverGroupID
	LEFT JOIN viewOrigin D ON D.ID = R.OriginID
	LEFT JOIN tblState S ON S.ID = R.StateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblProducer PR ON PR.ID = R.ProducerID
	LEFT JOIN tblOrderRejectReason REA ON REA.ID = R.ReasonID
	LEFT JOIN tblRateType RT ON RT.ID = R.RateTypeID
	LEFT JOIN tblUom U ON U.ID = R.UomID
	ORDER BY EffectiveDate
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OriginWaitRate info for the specified criteria
-- Changes:
	- 3.7.28 - 2015/06/18 - KDA - add DriverID/DriverGroupID/ProducerID parameters
	- 3.7.28 - 2015/06/18 - KDA - remove DriveID
***********************************/
ALTER FUNCTION fnCarrierOriginWaitRate
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @DriverGroupID int
, @OriginID int
, @StateID int
, @RegionID int
, @ProducerID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID, ReasonID
			, Ranking =	dbo.fnRateRanking(@ReasonID, R.ReasonID, 256, 1)
				  + dbo.fnRateRanking(@ShipperID, R.ShipperID, 128, 0)
				  + dbo.fnRateRanking(@CarrierID, R.CarrierID, 64, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 32, 0)
				  + dbo.fnRateRanking(@DriverGroupID, R.DriverGroupID, 16, 1)
				  + dbo.fnRateRanking(@OriginID, R.OriginID, 8, 0)
				  + dbo.fnRateRanking(@StateID, R.StateID, 4, 0)
				  + dbo.fnRateRanking(@RegionID, R.RegionID, 2, 0)
				  + dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM  dbo.viewCarrierOriginWaitRate R
		WHERE coalesce(nullif(@ReasonID, 0), R.ReasonID, 0) = coalesce(ReasonID, nullif(@ReasonID, 0), 0)
		  AND coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@DriverGroupID, 0), R.DriverGroupID, 0) = coalesce(DriverGroupID, nullif(@DriverGroupID, 0), 0)
		  AND coalesce(nullif(@OriginID, 0), R.OriginID, 0) = coalesce(OriginID, nullif(@OriginID, 0), 0)
		  AND coalesce(nullif(@StateID, 0), R.StateID, 0) = coalesce(StateID, nullif(@StateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(ProducerID, nullif(@ProducerID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	),
	cte2 AS
	(
		SELECT ID, S.ReasonID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT ReasonID = ISNULL(ReasonID, 0), Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 9  -- ensure some type of match occurred on all criteria choices
			GROUP BY ReasonID
		) X ON X.ReasonID = isnull(S.ReasonID, 0) AND X.Ranking = S.Ranking
	), 
	cte3 AS
	(	-- when both ReasonID = NULL AND ReasonID = @ReasonID records are BestMatch = 1, favor the ReasonID = @ReasonID record
		SELECT ID, X.ReasonID, X.Ranking, BestMatch = CASE WHEN BEST.ReasonID IS NULL THEN X.BestMatch ELSE 0 END
		FROM cte2 X
		LEFT JOIN (
			SELECT TOP 1 ReasonID FROM cte2 WHERE ReasonID = @ReasonID AND BestMatch = 1 ORDER BY Ranking DESC
		) BEST ON X.ReasonID IS NULL AND X.BestMatch = 1
	)
	
	SELECT TOP (CASE WHEN @BestMatchOnly = 1 THEN 1 ELSE 10000 END) R.ID, R.ReasonID, ShipperID, CarrierID, ProductGroupID, DriverGroupID, OriginID, StateID, RegionID, ProducerID
	  , Rate, EffectiveDate, EndDate
	  , R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
	  , BestMatch, Ranking
	  , Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewCarrierOriginWaitRate R
	JOIN cte3 X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
	ORDER BY Ranking DESC
)
GO

/***********************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OriginWaitRate rows for the specified criteria
-- Changes:
	- 3.7.28 - 2015/06/18 - KDA - add DriverID/DriverGroupID/ProducerID parameters
	- 3.7.30 - 2015/06/18 - KDA - remove DriverID
***********************************/
ALTER FUNCTION fnCarrierOriginWaitRatesDisplay
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @OriginID int
, @DriverGroupID int
, @StateID int
, @RegionID int
, @ProducerID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ReasonID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.DriverGroupID, R.OriginID, R.StateID, R.RegionID, R.ProducerID
		, R.Rate, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Shipper = SH.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, DriverGroup = DG.Name
		, Origin = D.Name
		, OriginFull = D.FullName
		, State = S.FullName
		, StateAbbrev = S.Abbreviation
		, Region = REG.Name
		, Producer = PR.Name
		, Reason = REA.Description
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
		, Ranking
	FROM dbo.fnCarrierOriginWaitRate(@StartDate, @EndDate, @ReasonID, @ShipperID, @CarrierID, @ProductGroupID, @DriverGroupID, @OriginID, @StateID, @RegionID, @ProducerID, 0) R
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblDriverGroup DG ON DG.ID = R.DriverGroupID
	LEFT JOIN viewOrigin D ON D.ID = R.OriginID
	LEFT JOIN tblState S ON S.ID = R.StateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblProducer PR ON PR.ID = R.ProducerID
	LEFT JOIN tblOriginWaitReason REA ON REA.ID = R.ReasonID
	ORDER BY EffectiveDate
GO

/***********************************
-- Date Created: 5 May 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RateSheet info for the specified criteria
-- Changes:
	- 3.7.28 - 2015/06/18 - KDA - add DriverID/DriverGroupID/ProducerID parameters
	- 3.7.30 - 2015/06/19 - KDA - remove DriverID
***********************************/
ALTER FUNCTION fnCarrierRateSheet
(
  @StartDate date
, @EndDate date
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @DriverGroupID int
, @OriginStateID int
, @DestStateID int
, @RegionID int
, @ProducerID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT R.ID
			, Ranking = dbo.fnRateRanking(@ShipperID, R.ShipperID, 128, 0)
				+ dbo.fnRateRanking(@CarrierID, R.CarrierID, 64, 0)
				+ dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 32, 0)
				+ dbo.fnRateRanking(@DriverGroupID, R.DriverGroupID, 16, 1)
				+ dbo.fnRateRanking(@OriginStateID, R.OriginStateID, 8, 0)
				+ dbo.fnRateRanking(@DestStateID, R.DestStateID, 4, 0)
				+ dbo.fnRateRanking(@RegionID, R.RegionID, 2, 0)
				+ dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM dbo.viewCarrierRateSheet R
		WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@DriverGroupID, 0), R.DriverGroupID, 0) = coalesce(DriverGroupID, nullif(@DriverGroupID, 0), 0)
		  AND coalesce(nullif(@OriginStateID, 0), R.OriginStateID, 0) = coalesce(OriginStateID, nullif(@OriginStateID, 0), 0)
		  AND coalesce(nullif(@DestStateID, 0), R.DestStateID, 0) = coalesce(DestStateID, nullif(@DestStateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(ProducerID, nullif(@ProducerID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	)
	
	SELECT R.ID, ShipperID, CarrierID, ProductGroupID, DriverGroupID, OriginStateID, DestStateID, RegionID, ProducerID, RateTypeID, UomID, EffectiveDate, EndDate
	  , MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate
	  , BestMatch, Ranking
	  , Locked, R.CreateDateUTC, R.CreatedByUser, R.LastChangeDateUTC, R.LastChangedByUser 
	FROM dbo.viewCarrierRateSheet R
	JOIN (
		SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 8  -- ensure some type of match occurred on all criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.ID = R.ID 
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)
GO

/***********************************
-- Date Created: 5 May 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RateSheetRangeRate info for the specified criteria
-- Changes:
	- 3.7.28 - 2015/06/13 - KDA - Add DriverID | DriverGroupID | ProducerID
	- 3.7.30 - 2015/06/19 - KDA - remove DriverID
***********************************/
ALTER FUNCTION fnCarrierRateSheetDisplay
(
  @StartDate date
, @EndDate date
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @DriverGroupID int
, @OriginStateID int
, @DestStateID int
, @RegionID int
, @ProducerID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.DriverGroupID, R.OriginStateID, R.DestStateID, R.RegionID, R.ProducerID, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Shipper = SH.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, DriverGroup = DG.Name
		, OriginState = OS.FullName
		, OriginStateAbbrev = OS.Abbreviation
		, DestState = OS.FullName
		, DestStateAbbrev = OS.Abbreviation
		, Region = REG.Name
		, Producer = P.Name
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
		, Ranking
	FROM dbo.fnCarrierRateSheet(@StartDate, @EndDate, @ShipperID, @CarrierID, @ProductGroupID, @DriverGroupID, @OriginStateID, @DestStateID, @RegionID, @ProducerID, 0) R
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblDriverGroup DG ON DG.ID = R.DriverGroupID
	LEFT JOIN tblState OS ON OS.ID = R.OriginStateID
	LEFT JOIN tblState DS ON DS.ID = R.DestStateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblProducer P ON P.ID = R.ProducerID
	ORDER BY EffectiveDate
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RateSheetRangeRate info for the specified criteria
-- Changes:
	- 3.7.28 - 2015/06/13 - KDA - Add DriverID | DriverGroupID | ProducerID
	- 3.7.30 - 2015/06/19 - KDA - remove DriverID
***********************************/
ALTER FUNCTION fnCarrierRateSheetRangeRate
(
  @StartDate date
, @EndDate date
, @RouteMiles int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @DriverGroupID int
, @OriginStateID int
, @DestStateID int
, @RegionID int
, @ProducerID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT RateSheetID = R.ID, RangeRateID = RR.ID
			-- the manually added .01 is normally added via the fnRateRanking routine
			, Ranking = CASE WHEN @RouteMiles BETWEEN RR.MinRange AND RR.MaxRange THEN 128.01 ELSE 0 END
				+ dbo.fnRateRanking(@ShipperID, R.ShipperID, 128, 0)
				+ dbo.fnRateRanking(@CarrierID, R.CarrierID, 64, 0)
				+ dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 32, 0)
				+ dbo.fnRateRanking(@DriverGroupID, R.DriverGroupID, 16, 1)
				+ dbo.fnRateRanking(@OriginStateID, R.OriginStateID, 8, 0)
				+ dbo.fnRateRanking(@DestStateID, R.DestStateID, 4, 0)
				+ dbo.fnRateRanking(@RegionID, R.RegionID, 2, 0)
				+ dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM dbo.viewCarrierRateSheet R
		JOIN dbo.tblCarrierRangeRate RR ON RR.RateSheetID = R.ID 
		  AND isnull(@RouteMiles, RR.MinRange) BETWEEN RR.MinRange AND RR.MaxRange
		WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@DriverGroupID, 0), R.DriverGroupID, 0) = coalesce(DriverGroupID, nullif(@DriverGroupID, 0), 0)
		  AND coalesce(nullif(@OriginStateID, 0), R.OriginStateID, 0) = coalesce(OriginStateID, nullif(@OriginStateID, 0), 0)
		  AND coalesce(nullif(@DestStateID, 0), R.DestStateID, 0) = coalesce(DestStateID, nullif(@DestStateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(ProducerID, nullif(@ProducerID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	)
	
	SELECT RR.ID, RateSheetID = R.ID, ShipperID, CarrierID, ProductGroupID, DriverGroupID, OriginStateID, DestStateID, RegionID, ProducerID
	  , Rate, MinRange, MaxRange, RateTypeID, UomID, EffectiveDate, EndDate
	  , MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate
	  , BestMatch, Ranking
	  , Locked, R.CreateDateUTC, R.CreatedByUser, R.LastChangeDateUTC, R.LastChangedByUser 
	FROM dbo.viewCarrierRateSheet R
	JOIN dbo.tblCarrierRangeRate RR ON RR.RateSheetID = R.ID
	JOIN (
		SELECT RateSheetID, RangeRateID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 9  -- ensure some type of match occurred on all criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.RateSheetID = R.ID AND X.RangeRateID = RR.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RateSheetRangeRate info for the specified criteria
-- Changes:
	- 3.7.28 - 2015/06/13 - KDA - Add DriverID | DriverGroupID | ProducerID
	- 3.7.30 - 2015/06/19 - KDA - remove DriverID
***********************************/
ALTER FUNCTION fnCarrierRateSheetRangeRatesDisplay
(
  @StartDate date
, @EndDate date
, @RouteMiles int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @DriverGroupID int
, @OriginStateID int
, @DestStateID int
, @RegionID int
, @ProducerID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.RateSheetID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.DriverGroupID, R.OriginStateID, R.DestStateID, R.RegionID, R.ProducerID, R.MinRange, R.MaxRange, R.Rate, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Shipper = SH.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, DriverGroup = DG.Name
		, OriginState = OS.FullName
		, OriginStateAbbrev = OS.Abbreviation
		, DestState = OS.FullName
		, DestStateAbbrev = OS.Abbreviation
		, Region = REG.Name
		, Producer = P.Name
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
		, Ranking
	FROM dbo.fnCarrierRateSheetRangeRate(@StartDate, @EndDate, @RouteMiles, @ShipperID, @CarrierID, @ProductGroupID, @DriverGroupID, @OriginStateID, @DestStateID, @RegionID, @ProducerID, 0) R
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblDriverGroup DG ON DG.ID = R.DriverGroupID
	LEFT JOIN tblState OS ON OS.ID = R.OriginStateID
	LEFT JOIN tblState DS ON DS.ID = R.DestStateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblProducer P ON P.ID = R.ProducerID
	ORDER BY EffectiveDate
GO

/****************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RouteRate info for the specified criteria
-- Changes:
	- 3.7.28 - 2015/06/13 - KDA - Add DriverID | DriverGroupID | ProducerID
	- 3.7.30 - 2015/06/19 - KDA - remove DriverID
*****************************************************/
ALTER FUNCTION fnCarrierRouteRate
(
  @StartDate date
, @EndDate date
, @OriginID int
, @DestinationID int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @DriverGroupID int
, @BestMatchOnly bit = 0
) RETURNS @ret TABLE 
(
  ID int
, RouteID int
, OriginID int
, DestinationID int
, ShipperID int
, CarrierID int
, ProductGroupID int
, DriverGroupID int
, Rate decimal(18, 10)
, RateTypeID int
, UomID int
, EffectiveDate date
, EndDate date
, MaxEffectiveDate date
, MinEndDate date
, NextEffectiveDate date
, PriorEndDate date
, BestMatch bit
, Ranking decimal(10, 2)
, Locked bit
, CreateDateUTC datetime
, CreatedByUser varchar(100)
, LastChangeDateUTC datetime
, LastChangedByUser varchar(100)
) AS BEGIN

	DECLARE @data TABLE (ID int, RouteID int, Ranking decimal(10, 2))
	INSERT INTO @data
		SELECT R.ID
			, R.RouteID
			, Ranking =	dbo.fnRateRanking(@ShipperID, R.ShipperID, 8, 0)
				  + dbo.fnRateRanking(@CarrierID, R.CarrierID, 4, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 2, 0)
				  + dbo.fnRateRanking(@DriverGroupID, R.DriverGroupID, 1, 1)
		FROM dbo.viewCarrierRouteRate R
		JOIN tblRoute RO ON RO.ID = R.RouteID
		WHERE (nullif(@OriginID, 0) IS NULL OR @OriginID = RO.OriginID)
		  AND (nullif(@DestinationID, 0) IS NULL OR @DestinationID = RO.DestinationID)
		  AND coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@DriverGroupID, 0), R.DriverGroupID, 0) = coalesce(DriverGroupID, nullif(@DriverGroupID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)

	INSERT INTO @ret
	SELECT R.ID, RouteID, OriginID, DestinationID, ShipperID, CarrierID, ProductGroupID, DriverGroupID, Rate, RateTypeID, UomID, EffectiveDate, EndDate
		, MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate
		, BestMatch, Ranking
		, Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewCarrierRouteRate R
	JOIN (
		SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM @data S
		LEFT JOIN (
			SELECT RouteID, Ranking = MAX(Ranking)
			FROM @data
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 4  -- ensure some type of match occurred on all criteria choices
			GROUP BY RouteID			  
		) X ON X.RouteID = S.RouteID AND X.Ranking = S.Ranking
	) X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
	
	RETURN
END
GO

/****************************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RouteRate rows for the specified criteria
-- Changes:
	- 3.7.28 - 2015/06/13 - KDA - Add DriverID | DriverGroupID | ProducerID
	- 3.7.30 - 2015/06/19 - KDA - remove DriverID
*****************************************************/
ALTER FUNCTION fnCarrierRouteRatesDisplay
(
  @StartDate date
, @EndDate date
, @OriginID int
, @DestinationID int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @DriverGroupID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.RouteID, RO.ActualMiles, R.OriginID, R.DestinationID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.DriverGroupID, R.Rate, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Origin = O.Name
		, OriginFull = O.FullName
		, Destination = D.Name
		, DestinationFull = D.FullName
		, Shipper = SH.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, DriverGroup = DG.Name
		, RateType = RT.Name
		, Uom = U.Name
		, UomShort = U.Abbrev
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch, Ranking
	FROM dbo.fnCarrierRouteRate(@StartDate, @EndDate, @OriginID, @DestinationID, @ShipperID, @CarrierID, @ProductGroupID, @DriverGroupID, 0) R
	JOIN tblRoute RO ON RO.ID = R.RouteID
	JOIN viewOrigin O ON O.ID = R.OriginID
	JOIN viewDestination D ON D.ID = R.DestinationID
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblDriverGroup DG ON DG.ID = R.DriverGroupID
	LEFT JOIN tblRateType RT ON RT.ID = R.RateTypeID
	LEFT JOIN tblUom U ON U.ID = R.UomID
	ORDER BY EffectiveDate
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier AssessorialRate info for the specified order
-- Changes:
	- 3.7.28 - 2015/06/18 - KDA - add new columns: add DriverID/DriverGroupID parameters, move ProducerID parameter to end (so consistent with all other Best-Match rates/parameters
	- 3.7.30 - 2015/06/19 - KDA - remove DriverID
***********************************/
ALTER FUNCTION fnOrderCarrierAssessorialRates(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, TypeID, Rate, RateTypeID, UomID
	FROM dbo.viewOrder O
	CROSS APPLY dbo.fnCarrierAssessorialRates(O.OrderDate, null, null, O.CustomerID, O.CarrierID, O.ProductGroupID, O.DriverGroupID, O.OriginID, O.DestinationID, O.OriginStateID, O.DestStateID, O.OriginRegionID, O.ProducerID, 1) R
	WHERE O.ID = @ID
	  AND ((R.TypeID = 1 AND O.ChainUp = 1)
		OR (R.TypeID = 2 AND O.RerouteCount > 0)
		OR (R.TypeID = 3 AND O.TicketCount > 1)
		OR (R.TypeID = 4 AND O.H2S = 1)
		OR R.TypeID > 4)
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier DestinationWaitRate info for the specified order
-- Changes
	- 3.7.13 - 5/21/15 - KDA - fix bug which prevented missing rate warning when billable minutes existed but no rate matched
	- 3.7.28 - 2015/05/18 - KDA - add use of @ProducerID, @DriverID, @DriverGroupID parameter
	- 3.7.30 - 2015/05/18 - KDA - remove DriverID
***********************************/
ALTER FUNCTION fnOrderCarrierDestinationWaitRate(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID
		, Minutes = DestMinutes
		, R.Rate
	FROM viewOrderBase O
	OUTER APPLY dbo.fnCarrierDestinationWaitRate(O.OrderDate, null, O.DestWaitReasonID, O.CustomerID, O.CarrierID, O.ProductGroupID, O.DriverGroupID, O.DestinationID, O.DestStateID, O.DestRegionID, O.ProducerID, 1) R
	WHERE O.ID = @ID
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OrderRejectRate info for the specified order
-- Changes:
	- 3.7.28 - 2015/06/18 - KDA - add DriverID/DriverGroupID/ProducerID parameters
	- 3.7.30 - 2015/06/19 - KDA - remove DriverID
***********************************/
ALTER FUNCTION fnOrderCarrierOrderRejectRate(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, R.Rate, R.RateTypeID, R.UomID
	FROM viewOrderBase O
	CROSS APPLY dbo.fnCarrierOrderRejectRate(O.OrderDate, null, O.RejectReasonID, O.CustomerID, O.CarrierID, O.ProductGroupID, O.DriverGroupID, O.OriginID, O.OriginStateID, O.OriginRegionID, O.ProducerID, 1) R
	WHERE O.ID = @ID
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OriginWaitRate info for the specified order
-- Changes
	- 3.7.13 - 5/21/15 - KDA - fix bug which prevented missing rate warning when billable minutes existed but no rate matched
	- 3.7.28 - 2015/06/18 - KDA - add usage of DriverID/DriverGroupID/ProducerID parameters
	- 3.7.30 - 2015/06/18 - KDA - remove DriverID
***********************************/
ALTER FUNCTION fnOrderCarrierOriginWaitRate(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, Minutes = O.OriginMinutes, R.Rate
	FROM viewOrderBase O
	CROSS APPLY dbo.fnCarrierOriginWaitRate(O.OrderDate, null, O.OriginWaitReasonID, O.CustomerID, O.CarrierID, O.ProductGroupID, O.DriverGroupID, O.OriginID, O.OriginStateID, O.OriginRegionID, O.ProducerID, 1) R
	WHERE O.ID = @ID
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RateSheetRangeRate info for the specified order
-- Changes:
	- 3.7.28 - 2015/06/18 - KDA - add usage of DriverID/DriverGroupID/ProducerID parameters
	- 3.7.30 - 2015/06/10 - KDA - remove DriverID
***********************************/
ALTER FUNCTION fnOrderCarrierRateSheetRangeRate(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, RateTypeID, Rate, UomID
	FROM dbo.viewOrderBase O
	CROSS APPLY dbo.fnCarrierRateSheetRangeRate(O.OrderDate, null, O.ActualMiles, O.CustomerID, O.CarrierID, O.ProductGroupID, O.DriverGroupID, O.OriginStateID, O.DestStateID, O.OriginRegionID, O.ProducerID, 1) R
	WHERE O.ID = @ID
GO

/****************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RouteRate info for the specified order
-- Changes:
	- 3.7.28 - 2015/06/18 - KDA - add use of DriverID/DriverGroupID parameters
	- 3.7.30 - 2015/06/18 - KDA - remove DriverID
*****************************************************/
ALTER FUNCTION fnOrderCarrierRouteRate(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, RateTypeID, Rate, R.UomID
	FROM dbo.viewOrderBase O
	CROSS APPLY dbo.fnCarrierRouteRate(O.OrderDate, null, O.OriginID, O.DestinationID, O.CustomerID, O.CarrierID, O.ProductGroupID, O.DriverGroupID, 1) R
	WHERE O.ID = @ID 
GO

COMMIT
SET NOEXEC OFF