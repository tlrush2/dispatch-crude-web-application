-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.11.20'
SELECT  @NewVersion = '3.11.20.1'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'HOTFIX - Reapply 90-day window for settlement orders, orders still missing on old batches'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/***********************************
Date Created: 9 Mar 2013
Author: Kevin Alons
Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
Changes:
- 3.7.31	- 2015/06/19 - KDA	- reorder input parameters (move ProviderID down) and add @DriverGroupID parameter
- 3.9.0		- 2/15/08/14 - KDA	- return Approved column
- 3.9.19.6	- 2015/09/30 - KDA	- add OverrideXXX fields from OrderApproval table
- 3.9.21	- 2015/10/03 - KDA	- use [viewOrder] O.OriginDriverGroupID instead of O.DriverGroupID (which could be DestDriver.DriverGroupID)
- 3.9.34	- 2015/12/16 - BB	- Remove deleted orders from the results (DCWEB-851)
- 3.10.13.6 - 2015/03/01 - JAE	- add TruckTypeID
- 3.11.3.1	- 2016/03/22 - KDA	- fix to no-op when @BatchID = 0
- 3.11.19 - 2016/05/02 - BB - Add Job number and Contract number
- 3.11.20 - 2016/05/04 - JAE - 3 month limit was always being applied, skip if batch is provided
- 3.11.20.1 - 2016/05/04 - JAE - Undoing change as timeouts reoccurring, need to investigate
***********************************/
ALTER PROCEDURE [dbo].[spRetrieveOrdersFinancialCarrier]
(
  @StartDate datetime = NULL -- will default to first day of 3rd prior month for efficiency sake
, @EndDate datetime = NULL
, @ShipperID int = -1 -- all
, @CarrierID int = -1 -- all
, @ProductGroupID int = -1 -- all 
, @TruckTypeID int = -1 -- all
, @DriverGroupID int = -1 -- all
, @OriginStateID int = -1 -- all 
, @DestStateID int = -1 -- all
, @ProducerID int = -1 -- all
, @BatchID int = NULL  -- either show unbatched (NULL), or the specified batch orders
, @OnlyShipperSettled bit = 0 
, @JobNumber varchar(20) = NULL -- 3.11.19
, @ContractNumber varchar(20) = NULL -- 3.11.19
) AS BEGIN

    -- if batch is null, only show last three months of data
    --IF @BatchID IS NULL
	--BEGIN
		SELECT @StartDate = dbo.fnDateOnly(isnull(@StartDate, dateadd(month, -3, dbo.fnFirstDOM(getdate())))), @EndDate = dbo.fnDateOnly(isnull(@EndDate, dateadd(day, 1, getdate())))
	--END
	--ELSE
	--BEGIN
		--SELECT @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	--END

	SELECT DISTINCT OE.* 
		, ShipperSettled = cast(CASE WHEN OSC.BatchID IS NOT NULL THEN 1 ELSE 0 END as bit)
		, InvoiceOtherDetailsTSV = dbo.fnOrderCarrierAssessorialDetailsTSV(OE.ID, 0)
		, InvoiceOtherAmountsTSV = dbo.fnOrderCarrierAssessorialAmountsTSV(OE.ID, 0)
		, Approved = cast(ISNULL(OA.Approved, 0) as bit)
		, FinalActualMiles = ISNULL(OA.OverrideActualMiles, OE.ActualMiles)
		, FinalOriginMinutes = ISNULL(OA.OverrideOriginMinutes, OE.OriginMinutes)
		, FinalDestMinutes = ISNULL(OA.OverrideDestMinutes, OE.DestMinutes)
		, FinalH2S = cast(CASE WHEN isnull(OA.OverrideH2S, 0) = 1 THEN 0 ELSE OE.H2S END as bit)
		, FinalChainup = cast(CASE WHEN isnull(OA.OverrideChainup, 0) = 1 THEN 0 ELSE OE.Chainup END as bit)
		, FinalRerouteCount = CASE WHEN isnull(OA.OverrideReroute, 0) = 1 THEN 0 ELSE OE.RerouteCount END
	FROM dbo.viewOrder_Financial_Carrier OE
	LEFT JOIN tblOrderSettlementShipper OSC ON OSC.OrderID = OE.ID AND OSC.BatchID IS NOT NULL
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = OE.ID
	WHERE ID IN (
		SELECT O.ID
		FROM viewOrder O
		JOIN tblProduct P ON P.ID = O.ProductID
		JOIN tblTruck T ON T.ID = O.OriginTruckID
		JOIN tblOrigin OO ON OO.ID = O.OriginID
		JOIN tblDestination D ON D.ID = O.DestinationID
		LEFT JOIN tblOrderSettlementCarrier OSC ON OSC.OrderID = O.ID
		WHERE O.StatusID IN (4)  
		  AND O.DeleteDateUTC IS NULL  -- 3.9.34
		  AND (@ShipperID=-1 OR O.CustomerID=@ShipperID) 
		  AND (@CarrierID=-1 OR O.CarrierID=@CarrierID) 
		  AND (@ProductGroupID=-1 OR P.ProductGroupID=@ProductGroupID) 
		  AND (@TruckTypeID=-1 OR T.TruckTypeID=@TruckTypeID)
		  AND (@DriverGroupID=-1 OR O.OriginDriverGroupID=@DriverGroupID) 
		  AND (@OriginStateID=-1 OR OO.StateID=@OriginStateID) 
		  AND (@DestStateID=-1 OR D.StateID=@DestStateID) 
		  AND (@ProducerID=-1 OR O.ProducerID=@ProducerID) 
		  AND (@StartDate IS NULL OR O.OrderDate >= @StartDate) 
		  AND (@EndDate IS NULL OR O.OrderDate <= @EndDate)
		  AND (ISNULL(@JobNumber,'') = '' OR O.JobNumber = @JobNumber)  -- 3.11.19
		  AND (ISNULL(@ContractNumber,'') = '' OR O.ContractNumber = @ContractNumber)  -- 3.11.19
		  -- if @BatchID = 0 - retrieve no records , if @BatchID IS NULL - retrieve all to orders eligible for settlement, otherwise retrieve the orders for the specified @BatchID value
		  AND (isnull(@BatchID, -999) <> 0 AND ((@BatchID IS NULL AND OSC.BatchID IS NULL) OR OSC.BatchID = @BatchID))
	)
	  AND (@OnlyShipperSettled = 0 OR OSC.BatchID IS NOT NULL)
	ORDER BY OE.OriginDepartTimeUTC
END

GO

COMMIT 
SET NOEXEC OFF