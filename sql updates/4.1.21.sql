SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.1.20.1'
SELECT  @NewVersion = '4.1.21'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1571: Add new page to recalculate order volumes'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/* Add new permissions for the recalculate page */
EXEC spAddNewPermission 'viewRecalculateVolumes', 'Allow user to recalculate order volumes <span class="label label-danger" title="This setting can adversely affect settlement!" data-toggle="tooltip"><strong>WARNING &nbsp;<i class="fa fa-info-circle"></i></strong></span>', 'Orders', 'Recalculate Volumes'
GO



/***********************************/
-- Date Created: 10 Oct 2016
-- Author: Ben Bloodworth
-- Purpose: Take a list of order numbers and find the active tickets for those orders then trigger trigOrderTicket_IU for
--          those tickets with a false update so that they can be recalculated whenever requested by a customer.
--          This will still require the setting for changing audited orders to be set to true.
/***********************************/
CREATE PROCEDURE spRecalculateOrderTicketVolumes
(
  @ORDERS VARCHAR(MAX)
, @USERNAME VARCHAR(100)
) AS
BEGIN

	-- Update the list of tickets derived above so that the recaculation will take place where applicable (based on the trigger's criteria)
	UPDATE tblOrderTicket 
		SET LastChangeDateUTC = GETUTCDATE()   --This causes TrigOrderTicket_IU to fire
			, LastChangedByUser = @USERNAME    --This causes TrigOrderTicket_IU to fire

			--The rest of these are to "trip" the trigger's field change checks for ticket types that only recalculate if these fields are updated.
			, GrossUnits = GrossUnits
			, OriginTankID = OriginTankID
			, OpeningGaugeFeet = OpeningGaugeFeet
			, OpeningGaugeInch = OpeningGaugeInch
			, OpeningGaugeQ = OpeningGaugeQ
			, ClosingGaugeFeet = ClosingGaugeFeet
			, ClosingGaugeInch = ClosingGaugeInch
			, ClosingGaugeQ = ClosingGaugeQ
			, OpenMeterUnits = OpenMeterUnits
			, CloseMeterUnits = CloseMeterUnits
			, GrossStdUnits = GrossStdUnits
			, NetUnits = NetUnits
			, ProductObsTemp = ProductObsTemp
			, ProductObsGravity = ProductObsGravity
			, ProductBSW = ProductBSW
			, ProductHighTemp = ProductHighTemp
			, ProductLowTemp = ProductLowTemp
			, WeightGrossUnits = WeightGrossUnits
			, WeightTareUnits = WeightTareUnits
		WHERE ID IN (
						SELECT OT.ID
						FROM tblOrderTicket OT
							LEFT JOIN tblOrder O ON O.ID = OT.OrderID
						WHERE OT.DeleteDateUTC IS NULL
							AND O.ID IN (SELECT * FROM fnStringList2Table(@ORDERS))
					)
END
GO


/****************************************
Date Created: 10/14/16
Author: Ben Bloodworth (Actual Author: Nick N. http://stackoverflow.com/a/16664442/5152586)
Purpose: Convert a comma separated string of values into a table
Changes:
****************************************/
CREATE FUNCTION fnStringList2Table
(
    @List varchar(MAX)
)
RETURNS 
@ParsedList table
(
    item int
)
AS
BEGIN
    DECLARE @item varchar(800), @Pos int

    SET @List = LTRIM(RTRIM(@List))+ ','
    SET @Pos = CHARINDEX(',', @List, 1)

    WHILE @Pos > 0
    BEGIN
        SET @item = LTRIM(RTRIM(LEFT(@List, @Pos - 1)))
        IF @item <> ''
        BEGIN
            INSERT INTO @ParsedList (item) 
            VALUES (CAST(@item AS int))
        END
        SET @List = RIGHT(@List, LEN(@List) - @Pos)
        SET @Pos = CHARINDEX(',', @List, 1)
    END

    RETURN
END
GO


COMMIT
SET NOEXEC OFF