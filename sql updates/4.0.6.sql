SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.0.5.3'
SELECT  @NewVersion = '4.0.6'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1660 and DCWEB-1661: HOS Cleanup'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


ALTER TABLE tblHos DROP CONSTRAINT DF_Hos_CreateDateUTC
ALTER TABLE tblHos ALTER COLUMN CreateDateUTC DATETIME NOT NULL 
ALTER TABLE tblHos ADD CONSTRAINT DF_Hos_CreateDateUTC DEFAULT GETUTCDATE() FOR CreateDateUTC
GO
ALTER TABLE tblHos ALTER COLUMN LastChangeDateUTC DATETIME NULL 
GO
ALTER TABLE tblHos ALTER COLUMN DeleteDateUTC DATETIME NULL 
GO

ALTER TABLE tblHos ADD LogTimeZoneID TINYINT NULL CONSTRAINT FK_Hos_LogTimeZone REFERENCES tblTimeZone(ID)
ALTER TABLE tblHos DROP COLUMN LogTZOffset
ALTER TABLE tblHos ADD Notes VARCHAR(255) NULL
ALTER TABLE tblHos ADD ApprovedByUser VARCHAR(100) NULL
ALTER TABLE tblHos ADD ApproveDateUTC DATETIME NULL
GO

ALTER TABLE tblHosDBAudit ALTER COLUMN CreateDateUTC DATETIME NULL 
GO
ALTER TABLE tblHosDBAudit ALTER COLUMN LastChangeDateUTC DATETIME NULL 
GO
ALTER TABLE tblHosDBAudit ALTER COLUMN DeleteDateUTC DATETIME NULL 
GO

ALTER TABLE tblHosDBAudit ADD LogTimeZoneID TINYINT NULL
ALTER TABLE tblHosDBAudit DROP COLUMN LogTZOffset
ALTER TABLE tblHosDBAudit ADD Notes VARCHAR(255) NULL
ALTER TABLE tblHosDBAudit ADD ApprovedByUser VARCHAR(100) NULL
ALTER TABLE tblHosDBAudit ADD ApproveDateUTC DATETIME NULL
GO


/*****************************************
-- Date Created: 2016/06/16
-- Author: Joe Engler
-- Purpose: Audit history for HOS entries
-- Changes:
		4.0.6	JAE		2016-08-26		Add approved and notes to trigger
*****************************************/
ALTER TRIGGER trigHos_IU ON tblHos AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @__SYSTEM_SETTING_DBAUDIT_ENABLE__ INT = 32
	-- only do anything if something actually changed
	IF EXISTS (SELECT * FROM inserted EXCEPT SELECT * FROM deleted)
	BEGIN
		/* START DB AUDIT *********************************************************/
		BEGIN TRY
			IF EXISTS(SELECT * FROM tblSetting WHERE ID = @__SYSTEM_SETTING_DBAUDIT_ENABLE__ AND (Value LIKE 'true' OR Value LIKE 'yes'))
				INSERT INTO tblHosDbAudit (DBAuditDate, ID, UID, DriverID, HosDriverStatusID, HosStatusID, Lat, Lon, NearestCity, PersonalUse, YardMove
												, LogDateUTC, LogTimeZoneID
												, Notes, ApprovedByUser, ApproveDateUTC
												, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser)
					SELECT GETUTCDATE(), ID, UID, DriverID, HosDriverStatusID, HosStatusID, Lat, Lon, NearestCity, PersonalUse, YardMove
												, LogDateUTC, LogTimeZoneID
												, Notes, ApprovedByUser, ApproveDateUTC
												, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser
					FROM deleted d
		END TRY
		BEGIN CATCH
			PRINT 'trigHos_IU.DBAUDIT FAILURE: ' + ERROR_MESSAGE()
		END CATCH
		/* END DB AUDIT *********************************************************/
	END
END

GO

/******************************************************
-- Date Created: 2016-08-26
-- Author: Joe Engler
-- Purpose: Add view for easily selecting 
******************************************************/
CREATE VIEW viewHOSwithHistory AS
with x as (
	select *, ROW_NUMBER() OVER (PARTITION BY ID ORDER BY DBAuditDate) AS seq
	from (
		select getutcdate() as dbauditdate, *, IsCurrent = 1 from tblhos
		UNION 
		select *, IsCurrent = 0 from tblHosDbAudit
	) a
) 
select *, IsOriginal = case when seq=1 then 1 else 0 end
from x 

GO


COMMIT
SET NOEXEC OFF