-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.12.4'
SELECT  @NewVersion = '3.12.5'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'DCWEB-1407 Conform to significant digits for specific fields'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


-- Add significant digits to the Unit of Measure table

ALTER TABLE tblUom ADD SignificantDigits INT NOT NULL DEFAULT 0
GO
UPDATE tblUom SET SignificantDigits = 2 WHERE Name IN ('Barrel', 'Gallon')
GO
UPDATE tblUom SET SignificantDigits = 3 WHERE Name IN ('Cubic Meter', 'US Ton')
GO
UPDATE tblUom SET SignificantDigits = 0 WHERE Name IN ('Pound')
GO

exec sp_RefreshView viewUom
GO

/***********************************/
-- Date Created: 04 August 2015
-- Author: Geoff Mochau
-- Purpose: REVISED to use 3 temperatures: compute the Net Qty from the raw (Gross) values
--   3.12.5 - Use appropriate significant digits in calculation
/***********************************/
ALTER FUNCTION fnCrudeNetCalculator3T
(
  @Gross float
, @ObsTemp float
, @OpenTemp float
, @CloseTemp float
, @ObsGravity float
, @BSW float
, @SigFig int = 2
) RETURNS float AS
BEGIN
	-- Round significant digits
	SET @Gross = ROUND(@Gross, @SigFig);
	SET @ObsTemp = ROUND(@ObsTemp, 1);
	SET @OpenTemp = ROUND(@OpenTemp, 1);
	SET @CloseTemp = ROUND(@CloseTemp, 1);
	SET @ObsGravity = ROUND(@ObsGravity, 1);
	SET @BSW = ROUND(@BSW, 3);


	DECLARE @const1 float, @const2 float, @const3 float, @const4 float
	DECLARE @const5 float, @const6 float, @const7 float
	SELECT @const1 = 341.0957, @const2 = 0.8, @const3 = 0.00001278, @const4 = 141360.198
		, @const5 = 131.5, @const6 = 0.0000000062, @const7 = 100
		
	DECLARE @factor1 numeric(38,30), @factor2 float, @factor3 float, @factor4 decimal(38, 10)
	DECLARE @factor5 float, @factor6 float, @factor7 float, @factor8 decimal(38, 10)
	DECLARE @factor9 float, @factor10 float, @factor11 float
	
	/* For backward compatibility with the DriverApp (v1.2.9), assume that zero open- & close- temps are just missing and repeat the observed temp */
	IF ((@OpenTemp = 0) AND (@CloseTemp = 0))
	BEGIN
		SELECT @OpenTemp = @ObsTemp;
		SELECT @CloseTemp = @ObsTemp;
	END

	DECLARE @ambientTemp float
	SELECT @ambientTemp = ( (@OpenTemp + @CloseTemp) / 2 ); /* Calculate the mean ambient temperature (primary temp operand) */

	SELECT @factor1 = (1-(@const3*(@ambientTemp-60))-((@const6)*((@ambientTemp-60)*(@ambientTemp-60))))*(@const4/(@const5+@ObsGravity))
	SELECT @factor3 = EXP(-((@const1/@factor1)/@factor1*(@ambientTemp-60))-(@const2*((@const1/@factor1)/@factor1*(@const1/@factor1)/@factor1)*((@ambientTemp-60)*(@ambientTemp-60))))
	SELECT @factor4 = @factor1/@factor3
	SELECT @factor5 = @factor1/EXP(-((@const1/(@factor4*@factor4))*(@ambientTemp-60))-(@const2*((@const1/(@factor4*@factor4))*(@const1/(@factor4*@factor4)))*((@ambientTemp-60)*(@ambientTemp-60))))
	SELECT @factor6 = @factor1/EXP(-((@const1/(@factor5*@factor5))*(@ambientTemp-60))-(@const2*((@const1/(@factor5*@factor5))*(@const1/(@factor5*@factor5)))*((@ambientTemp-60)*(@ambientTemp-60))))
	SELECT @factor7 = @factor1/EXP(-((@const1/(@factor6*@factor6))*(@ambientTemp-60))-(@const2*((@const1/(@factor6*@factor6))*(@const1/(@factor6*@factor6)))*((@ambientTemp-60)*(@ambientTemp-60))))
	SELECT @factor8 = @factor1/EXP(-((@const1/(@factor7*@factor7))*(@ambientTemp-60))-(@const2*((@const1/(@factor7*@factor7))*(@const1/(@factor7*@factor7)))*((@ambientTemp-60)*(@ambientTemp-60))))

	SELECT @factor9 = @factor1/EXP(-((@const1/(@factor8*@factor8))*(@ObsTemp-60))-(@const2*((@const1/(@factor8*@factor8))*(@const1/(@factor8*@factor8)))*((@ObsTemp-60)*(@ObsTemp-60))))

	DECLARE @CorrGravity float
	SELECT @CorrGravity = (@const4/@factor9)-@const5

	SELECT @factor10 = (@const1/((1-(@const3*(@ambientTemp-60))-((@const6)*((@ambientTemp-60)*(@ambientTemp-60))))*(@const4/(@const5+@CorrGravity))))/((1-(@const3*(@ambientTemp-60))-((@const6)*((@ambientTemp-60)*(@ambientTemp-60))))*(@const4/(@const5+@CorrGravity)))
	SELECT @factor11 = EXP(-(@factor10*(@ambientTemp-60))-(@const2*(@factor10*@factor10)*((@ambientTemp-60)*(@ambientTemp-60))))
	RETURN ROUND((@Gross*(1-(@BSW/@const7))*@factor11), @SigFig)
END

GO


COMMIT
SET NOEXEC OFF