-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.9.16.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.9.16.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0

SET NOEXEC OFF
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.9.16'
SELECT  @NewVersion = '3.9.17'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-874: Cloning reports does not keep Export checkbox state (fixed)'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/*******************************************/
-- Date Created: 23 Jul 2014
-- Author: Kevin Alons
-- Purpose: clone an existing UserReport (defaults to a full clone with columns, but can do an AddNew otherwise)
--			9/21/15 - BB - Added export column to preserve column export preference
/*******************************************/
ALTER PROCEDURE [dbo].[spCloneUserReport]
(
  @userReportID int
, @reportName varchar(50)
, @userNames varchar(max)
, @includeColumns bit = 1
, @newID int = 0 OUTPUT 
) AS
BEGIN
	SET NOCOUNT ON
	SET @userNames = REPLACE(@userNames, ' ', '')
	INSERT INTO tblUserReportDefinition (Name, ReportID, UserNames, CreatedByUser)
		SELECT @reportName, ReportID, @userNames, CASE WHEN @userNames IS NULL OR @userNames LIKE '%,%' THEN 'Administrator' ELSE @userNames END
		FROM tblUserReportDefinition WHERE ID = @userReportID
	SET @newID = SCOPE_IDENTITY()
	IF (@includeColumns = 1)
	BEGIN
		INSERT INTO tblUserReportColumnDefinition (UserReportID, ReportColumnID, Caption
			, SortNum, DataSort, GroupingFunctionID, DataFormat, FilterOperatorID, FilterValue1, FilterValue2, Export
			, CreatedByUser)
			SELECT @newID, ReportColumnID, Caption
				, SortNum, DataSort, GroupingFunctionID, DataFormat, FilterOperatorID, FilterValue1, FilterValue2, Export
				, (SELECT CreatedByUser FROM tblUserReportDefinition WHERE ID = @newID)
			FROM tblUserReportColumnDefinition WHERE UserReportID = @userReportID
	END
	
	SELECT [NewID]=@newID
	RETURN (isnull(@newID, 0))
END
GO

COMMIT
SET NOEXEC OFF

