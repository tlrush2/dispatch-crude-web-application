/* add EntrySortNum to tblOrderStatus
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.0.14', @NewVersion = '2.0.15'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

ALTER TABLE tblOrderStatus ADD EntrySortNum int null
GO

UPDATE tblOrderStatus SET EntrySortNum = 1 WHERE ID = 8
UPDATE tblOrderStatus SET EntrySortNum = 2 WHERE ID = 7
UPDATE tblOrderStatus SET EntrySortNum = 3 WHERE ID = 2
UPDATE tblOrderStatus SET EntrySortNum = 4 WHERE ID = 3
GO

EXEC _spRefreshAllViews
GO

COMMIT
SET NOEXEC OFF