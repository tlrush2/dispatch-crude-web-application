/* 
  add UoM table and related linkages to Order/Origins/Destinations/Trailers
  add Trailer.CapacityUnits
  add scalar fnCapacityGallons function to get baseline Gallon value for any Uom CapacityUnits
  rename all Qty field from Barrels to Units

*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.3.2'
SELECT  @NewVersion = '2.4.0'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

CREATE TABLE tblUom (
  ID int identity(1, 1) not null constraint PK_Uom primary key
, Name varchar(25) not null
, Abbrev varchar(10) not null
, GallonEquivalent decimal(18,6) not null
, CreateDateUTC smalldatetime NULL CONSTRAINT DF_UOM_CreateDateUTC DEFAULt (getutcdate())
, CreatedByUser varchar(100) NULL
, LastChangeDateUTC smalldatetime NULL
, LastChangedByUser varchar(100) NULL
, DeleteDateUTC smalldatetime NULL
, DeletedByUser varchar(100) NULL
)
GO

CREATE UNIQUE INDEX udxUom_Name ON tblUoM(Name)
GO
CREATE UNIQUE INDEX udxUom_Abbrev ON tblUoM(Abbrev)
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblUom TO dispatchcrude_iis_acct
GO

SET IDENTITY_INSERT tbluOM ON

INSERT INTO tblUom (ID, Name, Abbrev, GallonEquivalent) 
  SELECT 1, 'Barrel', 'BBL', 42.0
  UNION
  SELECT 2, 'Gallon', 'Gal', 1.0
  UNION
  SELECT 3, 'Cubic Meter', 'CM', 264.172
GO

SET IDENTITY_INSERT tbluOM OFF
GO

/*************************************************************
** Date Created: 26 Nov 2013
** Author: Kevin Alons
** Purpose: convert any Units Qty into a Gallons Qty
*************************************************************/
CREATE FUNCTION fnGalEquiv(@uomID int, @units decimal(9,4)) RETURNS decimal(18,6) AS BEGIN
	DECLARE @ret decimal(18,6)
	SELECT @ret = @units * GallonEquivalent FROM tblUom WHERE ID = @uomID
	RETURN (@ret)
END
GO
GRANT EXECUTE ON fnGalEquiv TO dispatchcrude_iis_acct
GO 

/*************************************************************
** Date Created: 26 Nov 2013
** Author: Kevin Alons
** Purpose: convert any Units Qty into a Gallons Qty
*************************************************************/
CREATE FUNCTION fnConvertUOM(@units decimal(9,4), @fromUomID int, @toUomID int) RETURNS decimal(18,6) AS BEGIN
	DECLARE @ret decimal(18,6)
	SELECT @ret = (
		SELECT @units * GallonEquivalent FROM tblUom WHERE ID=@fromUomID
	) / GallonEquivalent FROM tblUom WHERE ID=@toUomID
	RETURN (@ret)
END
GO
GRANT EXECUTE ON fnConvertUOM TO dispatchcrude_iis_acct
GO

ALTER TABLE tblOrigin ADD UomID int not null 
	CONSTRAINT DF_Origin_UomID DEFAULT (1) 
	CONSTRAINT FK_Origin_Uom FOREIGN KEY REFERENCES tblUom(ID)
GO

/***********************************/
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Origin records with translated value and FullName field (which includes Origin Type)
/***********************************/
ALTER VIEW [dbo].[viewOrigin] AS
SELECT O.*
	, CASE WHEN O.H2S = 1 THEN 'H2S-' ELSE '' END + OT.OriginType + ' - ' + O.Name AS FullName
	, OT.OriginType
	, S.FullName AS State
	, S.Abbreviation AS StateAbbrev
	, OP.Name AS Operator
	, P.FullName AS Pumper
	, TT.Name AS TicketType
	, R.Name AS Region
	, C.Name AS Customer
	, PR.Name AS Producer
	, TAT.FullName AS TankType
	, TZ.Name AS TimeZone
	, UOM.Name AS UOM
	, UOM.Abbrev AS UomShort
FROM dbo.tblOrigin O
JOIN dbo.tblOriginType OT ON OT.ID = O.OriginTypeID
LEFT JOIN tblState S ON S.ID = O.StateID
LEFT JOIN dbo.tblOperator OP ON OP.ID = O.OperatorID
LEFT JOIN dbo.viewPumper P ON P.ID = O.PumperID
LEFT JOIN dbo.tblTicketType TT ON TT.ID = O.TicketTypeID
LEFT JOIN dbo.tblRegion R ON R.ID = O.RegionID
LEFT JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
LEFT JOIN dbo.tblProducer PR ON PR.ID = O.ProducerID
LEFT JOIN dbo.viewTankType TAT ON TAT.ID = O.TankTypeID
LEFT JOIN dbo.tblTimeZone TZ ON TZ.ID = O.TimeZoneID
LEFT JOIN dbo.tblUom UOM ON UOM.ID = O.UomID

GO

ALTER TABLE tblDestination ADD UomID int not null 
	CONSTRAINT DF_Destination_UomID DEFAULT (1) 
	CONSTRAINT FK_Destination_Uom FOREIGN KEY REFERENCES tblUom(ID)
GO

/***********************************/
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Destination records with translated value and FullName field (which includes Destination Type)
/***********************************/
ALTER VIEW [dbo].[viewDestination] AS
SELECT D.*
	, cast(CASE WHEN D.DeleteDateUTC IS NULL THEN 1 ELSE 0 END as bit) AS Active
	, DT.DestinationType
	, DT.DestinationType + ' - ' + D.Name AS FullName
	, S.FullName AS State, S.Abbreviation AS StateAbbrev
	, R.Name AS Region
	, DTT.Name AS TicketType
	, TZ.Name AS TimeZone
	, UOM.Name AS UOM
	, UOM.Abbrev AS UomShort
FROM dbo.tblDestination D
JOIN dbo.tblDestinationType DT ON DT.ID = D.DestinationTypeID
JOIN dbo.tblDestTicketType DTT ON DTT.ID = D.TicketTypeID
LEFT JOIN tblState S ON S.ID = D.StateID
LEFT JOIN dbo.tblRegion R ON R.ID = D.RegionID
LEFT JOIN dbo.tblTimeZone TZ ON TZ.ID = D.TimeZoneID
LEFT JOIN dbo.tblUom UOM ON UOM.ID = D.UomID

GO

ALTER TABLE tblOrder ADD OriginUomID int not null 
	CONSTRAINT DF_Order_OriginUomID DEFAULT (1) 
	CONSTRAINT FK_Order_OriginUom FOREIGN KEY REFERENCES tblUom(ID)
GO
ALTER TABLE tblOrder ADD DestUomID int not null 
	CONSTRAINT DF_Order_DestUomID DEFAULT (1) 
	CONSTRAINT FK_Order_DestUom FOREIGN KEY REFERENCES tblUom(ID)
GO

sp_Rename 'tblOrder.OriginGrossBarrels', 'OriginGrossUnits', 'COLUMN'
GO
sp_Rename 'tblOrder.OriginNetBarrels', 'OriginNetUnits', 'COLUMN'
GO
sp_Rename 'tblOrder.DestGrossBarrels', 'DestGrossUnits', 'COLUMN'
GO
sp_Rename 'tblOrder.DestNetBarrels', 'DestNetUnits', 'COLUMN'
GO
sp_Rename 'tblOrder.DestOpenMeterBarrels', 'DestOpenMeterUnits', 'COLUMN'
GO
sp_Rename 'tblOrder.DestCloseMeterBarrels', 'DestCloseMeterUnits', 'COLUMN'
GO

sp_Rename 'tblOrderTicket.GrossBarrels', 'GrossUnits', 'COLUMN'
GO
sp_Rename 'tblOrderTicket.NetBarrels', 'NetUnits', 'COLUMN'
GO

/***********************************/
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Order records with "translated friendly" values for FK relationships
/***********************************/
ALTER VIEW [dbo].[viewOrder] AS
SELECT O.*
, dbo.fnDateOnly(dbo.fnUTC_to_Local(O.OriginDepartTimeUTC, vO.TimeZoneID, vO.UseDST)) AS OrderDate
, vO.Name AS Origin
, vO.FullName AS OriginFull
, vO.State AS OriginState
, vO.StateAbbrev AS OriginStateAbbrev
, vO.Station AS OriginStation
, vO.LeaseName
, vO.LeaseNum
, vO.TimeZoneID AS OriginTimeZoneID
, vO.UseDST AS OriginUseDST
, vO.H2S
, vD.Name AS Destination
, vD.FullName AS DestinationFull
, vD.State AS DestinationState
, vD.StateAbbrev AS DestinationStateAbbrev
, vD.DestinationType
, vD.Station AS DestStation
, vD.TimeZoneID AS DestTimeZoneID
, vD.UseDST AS DestUseDST
, C.Name AS Customer
, CA.Name AS Carrier
, CT.Name AS CarrierType
, OT.OrderStatus
, OT.StatusNum
, D.FullName AS Driver
, D.FirstName AS DriverFirst
, D.LastName AS DriverLast
, TRU.FullName AS Truck
, TR1.FullName AS Trailer
, TR2.FullName AS Trailer2
, P.PriorityNum
, TT.Name AS TicketType
, vD.TicketTypeID AS DestTicketTypeID
, vD.TicketType AS DestTicketType
, OP.Name AS Operator
, PR.Name AS Producer
, PU.FullName AS Pumper
, D.IDNumber AS DriverNumber
, CA.IDNumber AS CarrierNumber
, TRU.IDNumber AS TruckNumber
, TR1.IDNumber AS TrailerNumber
, TR2.IDNumber AS Trailer2Number
, PRO.Name as Product
, PRO.ShortName AS ProductShort
, OUom.Name AS OriginUOM
, OUom.Abbrev AS OriginUomShort
, DUom.Name AS DestUOM
, DUom.Abbrev AS DestUomShort
, cast((CASE WHEN O.DeleteDateUTC IS NULL THEN 1 ELSE 0 END) as bit) AS Active
, cast((CASE WHEN O.DeleteDateUTC IS NOT NULL THEN 1 ELSE 0 END) as bit) AS IsDeleted
FROM dbo.tblOrder O
LEFT JOIN dbo.viewOrigin vO ON vO.ID = O.OriginID
LEFT JOIN dbo.viewDestination vD ON vD.ID = O.DestinationID
LEFT JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
JOIN dbo.tblOrderStatus AS OT ON OT.ID = O.StatusID
LEFT JOIN dbo.tblCarrier CA ON CA.ID = O.CarrierID
LEFT JOIN dbo.tblCarrierType CT ON CT.ID = CA.CarrierTypeID
LEFT JOIN dbo.viewDriver D ON D.ID = O.DriverID
LEFT JOIN dbo.viewTruck TRU ON TRU.ID = O.TruckID
LEFT JOIN dbo.viewTrailer TR1 ON TR1.ID = O.TrailerID
LEFT JOIN dbo.viewTrailer TR2 ON TR2.ID = O.Trailer2ID
LEFT JOIN dbo.tblPriority P ON P.ID = O.PriorityID
LEFT JOIN dbo.tblTicketType TT ON TT.ID = O.TicketTypeID
LEFT JOIN dbo.tblOperator OP ON OP.ID = O.OperatorID
LEFT JOIN dbo.viewPumper PU ON PU.ID = O.PumperID
LEFT JOIN dbo.tblProducer PR ON PR.ID = O.ProducerID
LEFT JOIN dbo.tblProduct PRO ON PRO.ID = O.ProductID
LEFT JOIN dbo.tblUom OUom ON OUom.ID = O.OriginUomID
LEFT JOIN dbo.tblUom DUom ON DUom.ID = O.DestUomID

GO

/***********************************/
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return the Local Arrive|Depart Times + their respective TimeZone abbreviations
/***********************************/
ALTER VIEW [dbo].[viewOrderLocalDates] AS
SELECT O.*
, dbo.fnUTC_to_Local(O.OriginArriveTimeUTC, O.OriginTimeZoneID, O.OriginUseDST) AS OriginArriveTime
, dbo.fnUTC_to_Local(O.OriginDepartTimeUTC, O.OriginTimeZoneID, O.OriginUseDST) AS OriginDepartTime
, dbo.fnUTC_to_Local(O.DestArriveTimeUTC, O.DestTimeZoneID, O.DestUseDST) AS DestArriveTime
, dbo.fnUTC_to_Local(O.DestDepartTimeUTC, O.DestTimeZoneID, O.DestUseDST) AS DestDepartTime
, dbo.fnTimeZoneAbbrev(O.OriginArriveTimeUTC, O.OriginTimeZoneID, O.OriginUseDST) AS OriginTimeZone
, dbo.fnTimeZoneAbbrev(O.DestArriveTimeUTC, O.DestTimeZoneID, O.DestUseDST) AS DestTimeZone
, DATEDIFF(minute, O.OriginDepartTimeUTC, O.DestArriveTimeUTC) AS TransitMinutes
FROM viewOrder O

GO

/***********************************/
-- Date Created: 28 Feb 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
/***********************************/
ALTER VIEW [dbo].[viewOrderExportFull] AS
SELECT *
  , OriginMinutes + DestMinutes AS TotalMinutes
  , isnull(OriginWaitMinutes, 0) + isnull(DestWaitMinutes, 0) AS TotalWaitMinutes
FROM (
	SELECT O.*
	  , dbo.fnMaxInt(0, isnull(OriginMinutes, 0) - cast(S.Value as int)) AS OriginWaitMinutes
	  , dbo.fnMaxInt(0, isnull(DestMinutes, 0) - cast(S.Value as int)) AS DestWaitMinutes
	  , (SELECT count(*) FROM tblOrderTicket WHERE OrderID = O.ID AND DeleteDateUTC IS NULL) AS TicketCount
	  , (SELECT count(*) FROM tblOrderReroute WHERE OrderID = O.ID) AS RerouteCount
	FROM dbo.viewOrderLocalDates O
	JOIN dbo.tblSetting S ON S.ID = 7 -- the Unbillable Wait Time threshold minutes 
WHERE O.StatusID NOT IN (-10) -- don't include "Generated" orders (only Assigned+)
  AND O.DeleteDateUTC IS NULL
) v

GO

ALTER TABLE tblOrderPrintLog ADD OriginUomID int not null 
	CONSTRAINT DF_OrderPrintLog_OriginUomID DEFAULT (1) 
	CONSTRAINT FK_OrderPrintLog_OriginUom FOREIGN KEY REFERENCES tblUom(ID)
GO
ALTER TABLE tblOrderPrintLog ADD DestUomID int not null 
	CONSTRAINT DF_OrderPrintLog_DestUomID DEFAULT (1) 
	CONSTRAINT FK_OrderPrintLog_DestUom FOREIGN KEY REFERENCES tblUom(ID)
GO

ALTER TABLE tblTrailer ADD UomID int not null 
	CONSTRAINT DF_Trailer_UomID DEFAULT (1) 
	CONSTRAINT FK_Trailer_Uom FOREIGN KEY REFERENCES tblUom(ID)
GO

sp_Rename 'tblTrailer.Compartment1Barrels', 'Compartment1Units', 'COLUMN'
GO
sp_Rename 'tblTrailer.Compartment2Barrels', 'Compartment2Units', 'COLUMN'
GO
sp_Rename 'tblTrailer.Compartment3Barrels', 'Compartment3Units', 'COLUMN'
GO
sp_Rename 'tblTrailer.Compartment4Barrels', 'Compartment4Units', 'COLUMN'
GO
sp_Rename 'tblTrailer.Compartment5Barrels', 'Compartment5Units', 'COLUMN'
GO

/***********************************/
-- Date Created: 26 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Trailer records with FullName & translated "friendly" values
/***********************************/
ALTER VIEW [dbo].[viewTrailer] AS
SELECT T.*
	, cast(CASE WHEN isnull(C.DeleteDateUTC, T.DeleteDateUTC) IS NULL THEN 1 ELSE 0 END as bit) AS Active
	, ISNULL(Compartment1Units, 0) 
		+ ISNULL(Compartment2Units, 0) 
		+ ISNULL(Compartment3Units, 0) 
		+ ISNULL(Compartment4Units, 0) 
		+ ISNULL(Compartment5Units, 0) AS TotalCapacityUnits
	, CASE WHEN isnull(Compartment1Units, 0) > 0 THEN 1 ELSE 0 END 
		+ CASE WHEN isnull(Compartment2Units, 0) > 0 THEN 1 ELSE 0 END 
		+ CASE WHEN isnull(Compartment3Units, 0) > 0 THEN 1 ELSE 0 END
		+ CASE WHEN isnull(Compartment4Units, 0) > 0 THEN 1 ELSE 0 END 
		+ CASE WHEN isnull(Compartment5Units, 0) > 0 THEN 1 ELSE 0 END AS CompartmentCount
	, T.IDNumber AS FullName
	, TT.Name AS TrailerType
	, isnull(CT.Name, 'Unknown') AS CarrierType
	, C.Name AS Carrier
	, UOM.Name AS UOM
	, UOM.Abbrev AS UomShort
FROM dbo.tblTrailer T
LEFT JOIN dbo.tblTrailerType TT ON TT.ID = T.TrailerTypeID
JOIN dbo.tblCarrier C ON C.ID = T.CarrierID
LEFT JOIN dbo.tblCarrierType CT ON CT.ID = C.CarrierTypeID
LEFT JOIN dbo.tblUom UOM ON UOM.ID = T.UomID

GO

ALTER TABLE tblCarrier Add MinSettlementUomID int NOT NULL 
	CONSTRAINT DF_Carrier_MinSettlementUomID DEFAULT (1) 
	CONSTRAINT FK_Carrier_MinSettlementUom FOREIGN KEY REFERENCES tblUom(ID)
GO

sp_Rename 'tblCarrier.MinSettlementBarrels', 'MinSettlementUnits', 'COLUMN'
GO

/***********************************/
-- Date Created: 4 Apr 2013
-- Author: Kevin Alons
-- Purpose: return Carrier records with translated "friendly" values
/***********************************/
ALTER VIEW [dbo].[viewCarrier] AS
SELECT C.*
	, cast(CASE WHEN C.DeleteDateUTC IS NULL THEN 1 ELSE 0 END as bit) AS Active
	, CT.Name AS CarrierType
	, S.Abbreviation AS StateAbbrev
	, SF.Name AS SettlementFactor 
	, UOM.Name AS UOM
	, UOM.Abbrev AS UomShort
FROM tblCarrier C 
JOIN tblCarrierType CT ON CT.ID = C.CarrierTypeID
LEFT JOIN tblState S ON S.ID = C.StateID
LEFT JOIN tblSettlementFactor SF ON SF.ID = C.SettlementFactorID
LEFT JOIN dbo.tblUom UOM ON UOM.ID = C.MinSettlementUomID

GO

ALTER TABLE tblCustomer Add MinSettlementUomID int NOT NULL 
	CONSTRAINT DF_Customer_MinSettlementUomID DEFAULT (1) 
	CONSTRAINT FK_Customer_MinSettlementUom FOREIGN KEY REFERENCES tblUom(ID)
GO

sp_Rename 'tblCustomer.MinSettlementBarrels', 'MinSettlementUnits', 'COLUMN'
GO

/***********************************/
-- Date Created: 27 Jan 2013
-- Author: Kevin Alons
-- Purpose: return Customer records with "translated friendly" values for FK relationships
/***********************************/
ALTER VIEW [dbo].[viewCustomer] AS
SELECT C.*
	, S.Abbreviation AS StateAbbrev
	, S.FullName AS State
	, SF.Name AS SettlementFactor
	, UOM.Name AS UOM
	, UOM.Abbrev AS UomShort
FROM dbo.tblCustomer C
LEFT JOIN dbo.tblState S ON S.ID = C.StateID
LEFT JOIN tblSettlementFactor SF ON SF.ID = C.SettlementFactorID
LEFT JOIN dbo.tblUom UOM ON UOM.ID = C.MinSettlementUomID

GO

ALTER TABLE tblOrderPrintLogTicket ADD OriginUomID int not null 
	CONSTRAINT DF_OrderPrintLogTicket_OriginUomID DEFAULT (1) 
	CONSTRAINT FK_OrderPrintLogTicket_OriginUom FOREIGN KEY REFERENCES tblUom(ID)
GO

sp_Rename 'tblOrderPrintLogTicket.GrossBarrels', 'GrossUnits', 'COLUMN'
GO
sp_Rename 'tblOrderPrintLogTicket.NetBarrels', 'NetUnits', 'COLUMN'
GO

ALTER TABLE tblCarrierRates ADD UomID int not null 
	CONSTRAINT DF_CarrierRates_UomID DEFAULT (1) 
	CONSTRAINT FK_CarrierRates_Uom FOREIGN KEY REFERENCES tblUom(ID)
GO

ALTER TABLE tblCustomerRates ADD UomID int not null 
	CONSTRAINT DF_CustomerRates_UomID DEFAULT (1) 
	CONSTRAINT FK_CustomerRates_Uom FOREIGN KEY REFERENCES tblUom(ID)
GO

/***********************************/
-- Date Created: 9 Jun 2013
-- Author: Kevin Alons
-- Purpose: return Carrier route rates with translated "friendly" values + "missing route rates", computed Earliest & End Date values)
/***********************************/
ALTER VIEW [dbo].[viewCarrierRouteRates] AS
SELECT RB.ID, R.ID AS RouteID
	, O.UOM
	, O.UomShort
	, isnull(RB.Rate, 0) AS Rate
	, isnull(RB.EffectiveDate, dbo.fnDateOnly(ORD.MinOpenOrderDate)) AS EffectiveDate
	, cast(CASE WHEN C.DeleteDateUTC IS NOT NULL OR RB.Active = 0 THEN 0 ELSE 1 END as bit) AS Active
	, isnull(ORD.OpenOrderCount, 0) as OpenOrderCount
	, isnull(R.ActualMiles, 0) AS ActualMiles
	, RB.CreateDateUTC, RB.CreatedByUser, RB.LastChangeDateUTC, RB.LastChangedByUser
	, RB.EndDate
	, RB.EarliestEffectiveDate
	, C.ID AS CarrierID, C.Name AS Carrier
	, R.OriginID, R.Origin, R.OriginFull
	, R.DestinationID , R.Destination, R.DestinationFull
	, CASE WHEN R.ID IS NULL THEN 'Route Missing' WHEN RB.ID IS NULL THEN 'Missing' WHEN C.Active = 0 THEN 'Carrier Deleted' WHEN RB.Active = 0 THEN 'Route Inactive' ELSE 'Active' END AS Status
	, cast(CASE WHEN R.ID IS NULL OR isnull(ORD.OpenOrderCount, 0) = 0 THEN 0 ELSE 1 END as bit) AS RouteInUse
FROM (viewCarrier C CROSS JOIN viewRouteWithMissing R)
LEFT JOIN viewCarrierRouteRatesBase RB ON RB.CarrierID = C.ID AND RB.OriginID = R.OriginID AND RB.DestinationID = R.DestinationID
LEFT JOIN (SELECT CarrierID, RouteID, count(1) AS OpenOrderCount, min(OrderDate) AS MinOpenOrderDate FROM viewOrder WHERE StatusID IN (4) GROUP BY CarrierID, RouteID) ORD ON ORD.CarrierID = C.ID AND ORD.RouteID = R.ID
LEFT JOIN viewOrigin O ON O.ID = R.OriginID

GO

/***********************************/
-- Date Created: 9 Jun 2013
-- Author: Kevin Alons
-- Purpose: return Customer route rates with translated "friendly" values + "missing route rates", computed Earliest & End Date values)
/***********************************/
ALTER VIEW [dbo].[viewCustomerRouteRates] AS
SELECT RB.ID, R.ID AS RouteID
	, O.UOM
	, O.UomShort
	, isnull(RB.Rate, 0) AS Rate
	, isnull(RB.EffectiveDate, dbo.fnDateOnly(ORD.MinOpenOrderDate)) AS EffectiveDate
	, cast(CASE WHEN RB.Active = 0 THEN 0 ELSE 1 END as bit) AS Active
	, isnull(ORD.OpenOrderCount, 0) as OpenOrderCount
	, isnull(R.ActualMiles, 0) AS ActualMiles
	, RB.CreateDateUTC, RB.CreatedByUser, RB.LastChangeDateUTC, RB.LastChangedByUser
	, RB.EndDate
	, RB.EarliestEffectiveDate
	, C.ID AS CustomerID, C.Name AS Customer
	, R.OriginID, R.Origin, R.OriginFull
	, R.DestinationID , R.Destination, R.DestinationFull
	, CASE WHEN R.ID IS NULL THEN 'Route Missing' WHEN RB.ID IS NULL THEN 'Missing' WHEN RB.Active = 0 THEN 'Route Inactive' ELSE 'Active' END AS Status
	, cast(CASE WHEN R.ID IS NULL OR isnull(ORD.OpenOrderCount, 0) = 0 THEN 0 ELSE 1 END as bit) AS RouteInUse
FROM (viewCustomer C CROSS JOIN viewRouteWithMissing R)
LEFT JOIN viewCustomerRouteRatesBase RB ON RB.CustomerID = C.ID AND RB.OriginID = R.OriginID AND RB.DestinationID = R.DestinationID
LEFT JOIN (SELECT CustomerID, RouteID, count(1) AS OpenOrderCount, min(OrderDate) AS MinOpenOrderDate FROM viewOrder WHERE StatusID IN (4) GROUP BY CustomerID, RouteID) ORD ON ORD.CustomerID = C.ID AND ORD.RouteID = R.ID
LEFT JOIN viewOrigin O ON O.ID = R.OriginID

GO

/***********************************/
-- Date Created: 15 Aug 2013
-- Author: Kevin Alons
-- Purpose: return the CarrierSettlementBatch table contents with the CreateDate formatted DateOnly (for the BatchDate)
/***********************************/
ALTER VIEW [dbo].[viewCarrierSettlementBatch] 
AS
SELECT x.*
	, P.Name AS Product, P.ShortName AS ShortProduct
	, '#' + ltrim(BatchNum) + ' [' + dbo.fnDateMdYY(BatchDate) 
		+ '] ' + ISNULL(P.Name, '') + ' / ' + ltrim(OrderCount) AS FullName
FROM (
	SELECT SB.*
		, CASE WHEN C.Active = 1 THEN '' ELSE 'Deleted: ' END + C.Name AS Carrier
		, (SELECT COUNT(*) FROM tblOrderInvoiceCarrier WHERE BatchID = SB.ID) AS OrderCount
	FROM dbo.tblCarrierSettlementBatch SB
	JOIN dbo.viewCarrier C ON C.ID = SB.CarrierID
) x
LEFT JOIN dbo.tblProduct P ON P.ID = x.ProductID

GO

/***********************************/
-- Date Created: 15 Aug 2013
-- Author: Kevin Alons
-- Purpose: return the CustomerSettlementBatch table contents with the CreateDate formatted DateOnly (for the BatchDate)
/***********************************/
ALTER VIEW [dbo].[viewCustomerSettlementBatch] 
AS
SELECT x.*
	, P.Name AS Product, P.ShortName AS ShortProduct
	, '#' + ltrim(BatchNum) + ' [' + dbo.fnDateMdYY(BatchDate) 
		+ '] ' + ISNULL(P.Name, '') + ' / ' + ltrim(OrderCount) AS FullName
FROM (
	SELECT SB.*
		, C.Name AS Customer
		, (SELECT COUNT(*) FROM tblOrderInvoiceCustomer WHERE BatchID = SB.ID) AS OrderCount
	FROM dbo.tblCustomerSettlementBatch SB
	JOIN dbo.viewCustomer C ON C.ID = SB.CustomerID
) x
LEFT JOIN dbo.tblProduct P ON P.ID = x.ProductID

GO

/***********************************/
-- Date Created: 22 Jan 2013
-- Author: Kevin Alons
-- Purpose: query the tblOrderTicket table and include "friendly" translated values
/***********************************/
ALTER VIEW [dbo].[viewOrderTicket] AS
SELECT OT.*
	, TT.Name AS TicketType
	, VTT.FullName AS TankType
	, cast((CASE WHEN OT.DeleteDateUTC IS NULL THEN 1 ELSE 0 END) as bit) AS Active
	, cast((CASE WHEN OT.DeleteDateUTC IS NOT NULL THEN 1 ELSE 0 END) as bit) AS IsDeleted
FROM tblOrderTicket OT
JOIN tblTicketType TT ON TT.ID = OT.TicketTypeID
LEFT JOIN viewTankType VTT ON VTT.ID = OT.TankTypeID

GO

/***********************************/
-- Date Created: 21 Jan 2013
-- Author: Kevin Alons
-- Purpose: compute the Net Qty from the raw (Gross) values
/***********************************/
ALTER FUNCTION [dbo].[fnCrudeNetCalculator]
(
  @Gross float
, @ObsTemp float
, @ObsGravity float
, @BSW float
) RETURNS float AS
BEGIN
	DECLARE @const1 float, @const2 float, @const3 float, @const4 float
	DECLARE @const5 float, @const6 float, @const7 float
	SELECT @const1 = 341.0957, @const2 = 0.8, @const3 = 0.00001278, @const4 = 141360.198
		, @const5 = 131.5, @const6 = 0.0000000062, @const7 = 100

	DECLARE @factor1 numeric(38,30), @factor2 float, @factor3 float, @factor4 decimal(38, 10)
	DECLARE @factor5 float, @factor6 float, @factor7 float, @factor8 decimal(38, 10)
	DECLARE @factor9 float, @factor10 float, @factor11 float

	SELECT @factor1 = (1-(@const3*(@ObsTemp-60))-((@const6)*((@ObsTemp-60)*(@ObsTemp-60))))*(@const4/(@const5+@ObsGravity))
	SELECT @factor3 = EXP(-((@const1/@factor1)/@factor1*(@ObsTemp-60))-(@const2*((@const1/@factor1)/@factor1*(@const1/@factor1)/@factor1)*((@ObsTemp-60)*(@ObsTemp-60))))
	SELECT @factor4 = @factor1/@factor3
	SELECT @factor5 = @factor1/EXP(-((@const1/(@factor4*@factor4))*(@ObsTemp-60))-(@const2*((@const1/(@factor4*@factor4))*(@const1/(@factor4*@factor4)))*((@ObsTemp-60)*(@ObsTemp-60))))
	SELECT @factor6 = @factor1/EXP(-((@const1/(@factor5*@factor5))*(@ObsTemp-60))-(@const2*((@const1/(@factor5*@factor5))*(@const1/(@factor5*@factor5)))*((@ObsTemp-60)*(@ObsTemp-60))))
	SELECT @factor7 = @factor1/EXP(-((@const1/(@factor6*@factor6))*(@ObsTemp-60))-(@const2*((@const1/(@factor6*@factor6))*(@const1/(@factor6*@factor6)))*((@ObsTemp-60)*(@ObsTemp-60))))
	SELECT @factor8 = @factor1/EXP(-((@const1/(@factor7*@factor7))*(@ObsTemp-60))-(@const2*((@const1/(@factor7*@factor7))*(@const1/(@factor7*@factor7)))*((@ObsTemp-60)*(@ObsTemp-60))))
	SELECT @factor9 = @factor1/EXP(-((@const1/(@factor8*@factor8))*(@ObsTemp-60))-(@const2*((@const1/(@factor8*@factor8))*(@const1/(@factor8*@factor8)))*((@ObsTemp-60)*(@ObsTemp-60))))

	DECLARE @CorrGravity float
	SELECT @CorrGravity = (@const4/@factor9)-@const5

	SELECT @factor10 = (@const1/((1-(@const3*(@ObsTemp-60))-((@const6)*((@ObsTemp-60)*(@ObsTemp-60))))*(@const4/(@const5+@CorrGravity))))/((1-(@const3*(@ObsTemp-60))-((@const6)*((@ObsTemp-60)*(@ObsTemp-60))))*(@const4/(@const5+@CorrGravity)))
	SELECT @factor11 = EXP(-(@factor10*(@ObsTemp-60))-(@const2*(@factor10*@factor10)*((@ObsTemp-60)*(@ObsTemp-60))))
	RETURN (@Gross*(1-(@BSW/@const7))*@factor11)
END
GO

/***********************************/
-- Date Created: 21 Jan 2013
-- Author: Kevin Alons
-- Purpose: compute the Net Qty from the raw (Gross) values
/***********************************/
ALTER PROCEDURE [dbo].[spCrudeNetCalculator]
(
  @GrossQty float
, @ObsTemp float
, @ObsGravity float
, @BSW float
, @NetQty float = NULL output 
) AS
BEGIN
	SELECT @NetQty = dbo.fnCrudeNetCalculator(@GrossQty, @ObsTemp, @ObsGravity, @BSW)
END

GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 20 Mar 2013
-- Description:	return the data for the TAX_ND_10A report (for the specified date range)
-- =============================================
ALTER PROCEDURE [dbo].[spReport_Tax_ND10A]
(
  @CustomerID int = -1
, @ProductID int = -1
, @StateID int = -1
, @StartDate datetime
, @EndDate datetime
, @IncludeUninvoiced bit = 0
)
AS BEGIN
	SELECT @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	
	SELECT O.FieldName
		, O.Operator
		, O.LeaseNum AS LeaseNumber
		, O.Name AS [Well Name and Number]
		, O.CTBNum AS [NDIC CTB No.]
		-- convert all units to Barrels for this report
		, cast(round(SUM(dbo.fnConvertUom(OD.OriginNetUnits, OD.OriginUomID, 2)), 0) as int) AS [Lease Total (Bbls)]
	FROM viewOrigin O
	JOIN viewOrder OD ON OD.OriginID = O.ID
	JOIN tblOrderInvoiceCustomer IC ON IC.OrderID = OD.ID AND (@IncludeUninvoiced = 1 OR IC.BatchID IS NOT NULL)
	WHERE O.OriginTypeID IN (2) -- WELLs ONLY
		AND OD.DeleteDateUTC IS NULL
		AND (@CustomerID = -1 OR OD.CustomerID = @CustomerID)
		AND (@ProductID = -1 OR OD.ProductID = @ProductID)
		AND (@StateID = -1 OR O.StateID = @StateID)
		AND OD.OrderDate BETWEEN @StartDate AND @EndDate
	GROUP BY O.FieldName, O.Operator, O.LeaseNum, O.Name, O.CTBNum
	ORDER BY O.FieldName, O.Operator, O.LeaseNum, O.Name, O.CTBNum
	END

GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 20 Mar 2013
-- Description:	return the data for the TAX_ND_10B report (for the specified date range)
-- =============================================
ALTER PROCEDURE [dbo].[spReport_Tax_ND10B]
(
  @CustomerID int = -1
, @ProductID int = -1
, @StateID int = -1
, @StartDate datetime
, @EndDate datetime
, @IncludeUninvoiced bit = 0
)
AS BEGIN
	SELECT @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	
	SELECT O.Name AS [Point Received]
		, OD.Customer AS [Purchaser]
		, OD.Destination
		-- convert all units to BARRELS (2) for this report
		, cast(round(SUM(dbo.fnConvertUom(OD.OriginNetUnits, OD.OriginUomID, 2)), 0) as int) AS [Lease Total (Bbls)]
	FROM viewOrder OD
	JOIN tblOrigin O ON O.ID = OD.OriginID
	JOIN tblOrderInvoiceCustomer IC ON IC.OrderID = OD.ID AND (@IncludeUninvoiced = 1 OR IC.BatchID IS NOT NULL)
	WHERE O.OriginTypeID IN (2) -- WELLs ONLY
		AND OD.DeleteDateUTC IS NULL
		AND (@CustomerID = -1 OR OD.CustomerID = @CustomerID)
		AND (@ProductID = -1 OR OD.ProductID = @ProductID)
		AND (@StateID = -1 OR O.StateID = @StateID)
		AND OD.OrderDate BETWEEN @StartDate AND @EndDate
	GROUP BY O.Name, OD.Customer, OD.Destination
	ORDER BY O.Name, OD.Customer, OD.Destination
	END

GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Carrier "Settlement" $$ values to an Audited order
/***********************************/
ALTER PROCEDURE [dbo].[spProcessCarrierInvoice]
(
  @ID int
, @UserName varchar(100)
, @ChainupFee smallmoney = NULL
, @WaitFee smallmoney = NULL
, @RerouteFee smallmoney = NULL
, @RejectionFee smallmoney = NULL
, @H2SFee smallmoney = NULL
, @LoadFee smallmoney = NULL
) AS BEGIN
	DELETE FROM tblOrderInvoiceCarrier WHERE OrderID = @ID
	
	-- all Units and Rates are first normalized to GALLONS UOM then processed consistently in Gallons
	INSERT INTO tblOrderInvoiceCarrier (OrderID, ChainupFee, RerouteFee, WaitFeeSubUnitID, WaitFeeRoundingTypeID, BillableWaitMinutes, WaitRate, WaitFee
		, RejectionFee, RouteRate, LoadFee, H2SRate, H2SFee, FuelSurcharge, TaxRate
		, TotalFee, CreateDateUTC, CreatedByUser)
	SELECT D.ID, ChainupFee, RerouteFee, WaitFeeSubUnitID, WaitFeeRoundingTypeID, BillableWaitMinutes, WaitRate, WaitFee
		, RejectionFee, RouteRate, LoadFee, H2SRate, H2SFee, FuelSurcharge, TaxRate
		, RejectionFee + ChainupFee + RerouteFee + WaitFee + H2SFee + LoadFee AS TotalFee
		, GETUTCDATE(), @UserName
	FROM (
		-- compute the actual fees or use the manual overide fee values (if provided)
		SELECT ID
			, coalesce(@ChainupFee, Chainup * ChainupFee, 0) AS ChainupFee
			, coalesce(@RerouteFee, RerouteFee * RerouteCount, 0) AS RerouteFee
			, WaitFeeSubUnitID
			, WaitFeeRoundingTypeID
			, cast(round(BillableWaitHours * 60, 0) as int) AS BillableWaitMinutes
			, WaitFee AS WaitRate
			, coalesce(@WaitFee, BillableWaitHours * WaitFee, 0) AS WaitFee
			, coalesce(@RejectionFee, Rejected * RejectionFee, 0) AS RejectionFee
			, H2SRate
			, coalesce(@H2SFee, H2S * dbo.fnMaxDecimal(MinSettlementGallons, ActualGallons) * H2SRate, 0) AS H2SFee
			, isnull(TaxRate, 0) AS TaxRate
			, isnull(RouteRate, 0) AS RouteRate
			-- if rejected, use 0 AS the Rate
			, round(coalesce(@LoadFee, (1 - Rejected) * dbo.fnMaxDecimal(MinSettlementGallons, ActualGallons) * RouteRate, 0), 4) AS LoadFee
			, isnull(FuelSurcharge, 0) AS FuelSurcharge
		FROM (
			-- normalize the Accessorial Rates to GALLONS + add normalized Route Rate and other time normalization
			SELECT S.ID
				, S.ChainUp
				, CR.ChainupFee
				, CR.RerouteFee
				, S.RerouteCount
				, S.TotalWaitMinutes
				, CR.WaitFeeSubUnitID
				, CR.WaitFeeRoundingTypeID
				, dbo.fnComputeBillableWaitHours(S.TotalWaitMinutes, CR.WaitFeeSubUnitID, CR.WaitFeeRoundingTypeID) AS BillableWaitHours
				, CR.WaitFee
				, S.Rejected
				, CR.RejectionFee
				, S.H2S
				-- normalize the Origin.UOM H2SRate for "Gallons" UOM
				, dbo.fnConvertUOM(isnull(S.H2S * CR.H2SRate, 0), OriginUomID, 2) AS H2SRate
				, S.TaxRate
				-- normalize the Origin.UOM Route Rate for "Gallons" UOM
				, dbo.fnConvertUom(dbo.fnCarrierRouteRate(S.CarrierID, S.RouteID, S.OrderDate), OriginUomID, 2) AS RouteRate
				, S.MinSettlementGallons
				, isnull(S.ActualGallons, 0) AS ActualGallons
				, CR.FuelSurcharge
			FROM (
				-- get the Order raw data (with Units Normalized to GALLONS) and matching Carrier Accessorial Rate ID
				SELECT O.ID
					, O.CarrierID
					, O.RouteID
					, O.ChainUp
					-- normalize the Order.OriginUOM ActualUnits for "Gallons" UOM
					, dbo.fnConvertUOM(CASE WHEN C.SettlementFactorID = 1 THEN O.OriginGrossUnits ELSE O.OriginNetUnits END, O.OriginUomID, 2) AS ActualGallons
					, O.OriginUomID
					, O.RerouteCount
					, O.TotalWaitMinutes
					, O.Rejected
					, OO.H2S
					, O.OrderDate
					, coalesce(CRXX.ID, CRXA.ID, CRAX.ID, CRAA.ID) AS CRID
					-- normalize the Origin.UOM MinSettlementUnits for "Gallons" UOM
					, isnull(dbo.fnConvertUOM(C.MinSettlementUnits, C.MinSettlementUomID, 2), 0) AS MinSettlementGallons
					, OO.TaxRate
				FROM dbo.viewOrderExportFull O
				JOIN tblOrigin OO ON OO.ID = O.OriginID
				JOIN tblCarrier C ON C.ID = O.CarrierID
				LEFT JOIN tblCarrierRates CRXX ON CRXX.CarrierID = O.CarrierID AND CRXX.RegionID = OO.RegionID
				LEFT JOIN tblCarrierRates CRXA ON CRXA.CarrierID = O.CarrierID AND CRXA.RegionID = -1
				LEFT JOIN tblCarrierRates CRAX ON CRAX.CarrierID = -1 AND CRAX.RegionID = OO.RegionID
				LEFT JOIN tblCarrierRates CRAA ON CRAA.CarrierID = -1 AND CRAA.RegionID = -1
				WHERE O.ID = @ID
			) S
			LEFT JOIN tblCarrierRates CR ON CR.ID = S.CRID
		) SS
	) D
END

GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Customer "Settlement" $$ values to an Audited order
/***********************************/
ALTER PROCEDURE [dbo].[spProcessCustomerInvoice]
(
  @ID int
, @UserName varchar(100)
, @ChainupFee smallmoney = NULL
, @WaitFee smallmoney = NULL
, @RerouteFee smallmoney = NULL
, @RejectionFee smallmoney = NULL
, @H2SFee smallmoney = NULL
, @LoadFee smallmoney = NULL
) AS BEGIN
	DELETE FROM tblOrderInvoiceCustomer WHERE OrderID = @ID
	
	-- all Units and Rates are first normalized to GALLONS UOM then processed consistently in Gallons
	INSERT INTO tblOrderInvoiceCustomer (OrderID, ChainupFee, RerouteFee, WaitFeeSubUnitID, WaitFeeRoundingTypeID, BillableWaitMinutes, WaitRate, WaitFee
		, RejectionFee, RouteRate, LoadFee, H2SRate, H2SFee, FuelSurcharge, TaxRate
		, TotalFee, CreateDateUTC, CreatedByUser)
	SELECT D.ID, ChainupFee, RerouteFee, WaitFeeSubUnitID, WaitFeeRoundingTypeID, BillableWaitMinutes, WaitRate, WaitFee
		, RejectionFee, RouteRate, LoadFee, H2SRate, H2SFee, FuelSurcharge, TaxRate
		, RejectionFee + ChainupFee + RerouteFee + WaitFee + H2SFee + LoadFee AS TotalFee
		, GETUTCDATE(), @UserName
	FROM (
		-- compute the actual fees or use the manual overide fee values (if provided)
		SELECT ID
			, coalesce(@ChainupFee, Chainup * ChainupFee, 0) AS ChainupFee
			, coalesce(@RerouteFee, RerouteFee * RerouteCount, 0) AS RerouteFee
			, WaitFeeSubUnitID
			, WaitFeeRoundingTypeID
			, cast(round(BillableWaitHours * 60, 0) as int) AS BillableWaitMinutes
			, WaitFee AS WaitRate
			, coalesce(@WaitFee, BillableWaitHours * WaitFee, 0) AS WaitFee
			, coalesce(@RejectionFee, Rejected * RejectionFee, 0) AS RejectionFee
			, H2SRate
			, coalesce(@H2SFee, H2S * dbo.fnMaxDecimal(MinSettlementGallons, ActualGallons) * H2SRate, 0) AS H2SFee
			, isnull(TaxRate, 0) AS TaxRate
			, isnull(RouteRate, 0) AS RouteRate
			-- if rejected, use 0 AS the Rate
			, round(coalesce(@LoadFee, (1 - Rejected) * dbo.fnMaxDecimal(MinSettlementGallons, ActualGallons) * RouteRate, 0), 4) AS LoadFee
			, isnull(FuelSurcharge, 0) AS FuelSurcharge
		FROM (
			-- normalize the Accessorial Rates to GALLONS + add normalized Route Rate and other time normalization
			SELECT S.ID
				, S.ChainUp
				, CR.ChainupFee
				, CR.RerouteFee
				, S.RerouteCount
				, S.TotalWaitMinutes
				, CR.WaitFeeSubUnitID
				, CR.WaitFeeRoundingTypeID
				, dbo.fnComputeBillableWaitHours(S.TotalWaitMinutes, CR.WaitFeeSubUnitID, CR.WaitFeeRoundingTypeID) AS BillableWaitHours
				, CR.WaitFee
				, S.Rejected
				, CR.RejectionFee
				, S.H2S
				-- normalize the Origin.UOM H2SRate for "Gallons" UOM
				, dbo.fnConvertUOM(isnull(S.H2S * CR.H2SRate, 0), OriginUomID, 2) AS H2SRate
				, S.TaxRate
				-- normalize the Origin.UOM Route Rate for "Gallons" UOM
				, dbo.fnConvertUom(dbo.fnCustomerRouteRate(S.CustomerID, S.RouteID, S.OrderDate), OriginUomID, 2) AS RouteRate
				, S.MinSettlementGallons
				, isnull(S.ActualGallons, 0) AS ActualGallons
				, CR.FuelSurcharge
			FROM (
				-- get the Order raw data (with Units Normalized to GALLONS) and matching Customer Accessorial Rate ID
				SELECT O.ID
					, O.CustomerID
					, O.RouteID
					, O.ChainUp
					-- normalize the Order.OriginUOM ActualUnits for "Gallons" UOM
					, dbo.fnConvertUOM(CASE WHEN C.SettlementFactorID = 1 THEN O.OriginGrossUnits ELSE O.OriginNetUnits END, O.OriginUomID, 2) AS ActualGallons
					, O.OriginUomID
					, O.RerouteCount
					, O.TotalWaitMinutes
					, O.Rejected
					, OO.H2S
					, O.OrderDate
					, coalesce(CRXX.ID, CRXA.ID, CRAX.ID, CRAA.ID) AS CRID
					-- normalize the Origin.UOM MinSettlementUnits for "Gallons" UOM
					, isnull(dbo.fnConvertUOM(C.MinSettlementUnits, C.MinSettlementUomID, 2), 0) AS MinSettlementGallons
					, OO.TaxRate
				FROM dbo.viewOrderExportFull O
				JOIN tblOrigin OO ON OO.ID = O.OriginID
				JOIN tblCustomer C ON C.ID = O.CustomerID
				LEFT JOIN tblCustomerRates CRXX ON CRXX.CustomerID = O.CustomerID AND CRXX.RegionID = OO.RegionID
				LEFT JOIN tblCustomerRates CRXA ON CRXA.CustomerID = O.CustomerID AND CRXA.RegionID = -1
				LEFT JOIN tblCustomerRates CRAX ON CRAX.CustomerID = -1 AND CRAX.RegionID = OO.RegionID
				LEFT JOIN tblCustomerRates CRAA ON CRAA.CustomerID = -1 AND CRAA.RegionID = -1
				WHERE O.ID = @ID
			) S
			LEFT JOIN tblCustomerRates CR ON CR.ID = S.CRID
		) SS
	) D
END

GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 6 Feb 2013
-- Description:	trigger to ensure the GrossBarrels and NetBarrels are computed for valid, entered data
-- =============================================
ALTER TRIGGER [dbo].[trigOrderTicket_IU] ON [dbo].[tblOrderTicket] AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	-- ensure the Ticket BPI value stays in sync with the related Origin
	UPDATE tblOrderTicket
		SET BarrelsPerInch = ORIG.BarrelsPerInch
	FROM tblOrderTicket OT
	JOIN inserted i ON I.ID = OT.ID
	JOIN tblOrder O ON O.ID = i.OrderID
	JOIN tblOrigin ORIG ON ORIG.ID = O.OriginID

	-- re-compute GaugeRun ticket Gross barrels
	IF UPDATE(OpeningGaugeFeet) OR UPDATE(OpeningGaugeInch) OR UPDATE(OpeningGaugeQ)
		OR UPDATE (ClosingGaugeFeet) OR UPDATE(ClosingGaugeInch) OR UPDATE(ClosingGaugeQ) OR UPDATE(BarrelsPerInch)
		OR UPDATE (GrossUnits)
	BEGIN
		UPDATE tblOrderTicket
		  SET GrossUnits = dbo.fnConvertUom(dbo.fnTankQtyBarrelsDelta(
				OpeningGaugeFeet
			  , OpeningGaugeInch
			  , OpeningGaugeQ
			  , ClosingGaugeFeet
			  , ClosingGaugeInch
			  , ClosingGaugeQ
			  , BarrelsPerInch), 1, O.OriginUomID) -- ensure this value is always normalized from BARRELS
		FROM tblOrderTicket OT
		JOIN tblOrder O ON O.ID = OT.OrderID
		WHERE OT.ID IN (SELECT ID FROM inserted) 
		  AND OT.TicketTypeID = 1 -- Gauge Run Ticket
		  AND OT.OpeningGaugeFeet IS NOT NULL
		  AND OT.OpeningGaugeInch IS NOT NULL
		  AND OT.OpeningGaugeQ IS NOT NULL
		  AND OT.ClosingGaugeFeet IS NOT NULL
		  AND OT.ClosingGaugeInch IS NOT NULL
		  AND OT.ClosingGaugeQ IS NOT NULL
		  AND OT.BarrelsPerInch IS NOT NULL
	END
	-- re-compute GaugeRun | Net Barrel tickets NetUnits
	IF UPDATE(OpeningGaugeFeet) OR UPDATE(OpeningGaugeInch) OR UPDATE(OpeningGaugeQ)
		OR UPDATE (ClosingGaugeFeet) OR UPDATE(ClosingGaugeInch) OR UPDATE(ClosingGaugeQ) 
		OR UPDATE(BarrelsPerInch) OR UPDATE(GrossUnits) OR UPDATE(NetUnits)
		OR UPDATE(ProductObsTemp) OR UPDATE(ProductObsGravity) OR UPDATE(ProductBSW)
	BEGIN
		UPDATE tblOrderTicket
		  SET NetUnits = dbo.fnCrudeNetCalculator(GrossUnits, ProductObsTemp, ProductObsGravity, ProductBSW)
		WHERE ID IN (SELECT ID FROM inserted)
		  AND TicketTypeID IN (1,2)
		  AND GrossUnits IS NOT NULL
		  AND ProductObsTemp IS NOT NULL
		  AND ProductObsGravity IS NOT NULL
		  AND ProductBSW IS NOT NULL
	END
	
	-- ensure the Order record is in-sync with the Tickets
	UPDATE tblOrder 
	SET OriginGrossUnits = (SELECT sum(GrossUnits) FROM tblOrderTicket WHERE OrderID = O.ID AND Rejected = 0 AND DeleteDateUTC IS NULL)
	  , OriginNetUnits = (SELECT sum(NetUnits) FROM tblOrderTicket WHERE OrderID = O.ID AND Rejected = 0 AND DeleteDateUTC IS NULL)
		-- use the first MeterRun/BasicRun CarrierTicketNum num as the Order BOL Num
	  , OriginBOLNum = (SELECT min(BOLNum) FROM tblOrderTicket WHERE OrderID = O.ID AND TicketTypeID NOT IN (1,2) AND DeleteDateUTC IS NULL)
	  , CarrierTicketNum = (SELECT min(CarrierTicketNum) FROM tblOrderTicket WHERE ID IN 
		(SELECT min(ID) FROM tblOrderTicket WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL))
	  , LastChangeDateUTC = (SELECT MAX(isnull(LastChangeDateUTC, CreateDateUTC)) FROM inserted WHERE OrderID = O.ID)
	  , LastChangedByUser = (SELECT MIN(isnull(LastChangedByUser, CreatedByUser)) FROM inserted WHERE OrderID = O.ID)
	FROM tblOrder O
	WHERE ID IN (SELECT DISTINCT OrderID FROM inserted)
END

GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 3 Oct 2013
-- Description:	trigger to ensure the GrossUnits and NetUnits are computed for valid, entered data
-- =============================================
ALTER TRIGGER [dbo].[trigOrderTicket_D] ON [dbo].[tblOrderTicket] AFTER DELETE AS
BEGIN
	SET NOCOUNT ON;

	-- ensure the Order record is in-sync with the Tickets
	UPDATE tblOrder 
	SET OriginGrossUnits = (SELECT sum(GrossUnits) FROM tblOrderTicket WHERE OrderID = O.ID AND Rejected = 0 AND DeleteDateUTC IS NULL)
	  , OriginNetUnits = (SELECT sum(NetUnits) FROM tblOrderTicket WHERE OrderID = O.ID AND Rejected = 0 AND DeleteDateUTC IS NULL)
		-- use the first MeterRun/BasicRun CarrierTicketNum num as the Order BOL Num
	  , OriginBOLNum = (SELECT min(BOLNum) FROM tblOrderTicket WHERE OrderID = O.ID AND TicketTypeID NOT IN (1,2) AND DeleteDateUTC IS NULL)
	  , CarrierTicketNum = (SELECT min(CarrierTicketNum) FROM tblOrderTicket WHERE ID IN 
		(SELECT min(ID) FROM tblOrderTicket WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL))
	  , LastChangeDateUTC = (SELECT MAX(isnull(LastChangeDateUTC, CreateDateUTC)) FROM deleted WHERE OrderID = O.ID)
	  , LastChangedByUser = (SELECT MIN(isnull(LastChangedByUser, CreatedByUser)) FROM deleted WHERE OrderID = O.ID)
	FROM tblOrder O
	WHERE ID IN (SELECT DISTINCT OrderID FROM deleted)
END

GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 6 Feb 2013
-- Description:	trigger to ensure the entered values for an OrderTicket are actually valid
-- =============================================
ALTER TRIGGER [dbo].[trigOrderTicket_IU_Validate] ON [dbo].[tblOrderTicket] AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	IF (SELECT COUNT(*) FROM (
		SELECT OT.OrderID
		FROM tblOrderTicket OT
		WHERE OrderID IN (SELECT DISTINCT OrderID FROM inserted)
		  AND OT.DeleteDateUTC IS NULL
		GROUP BY OT.OrderID, OT.CarrierTicketNum
		HAVING COUNT(*) > 1) v) > 0
	BEGIN
		RAISERROR('Duplicate Ticket Numbers are not allowed', 16, 1)
		RETURN
	END
	
	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (3) AND BOLNum IS NULL) > 0
	BEGIN
		RAISERROR('BOL # value is required for Meter Run tickets', 16, 1)
		RETURN
	END
	
	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1, 2) AND TankNum IS NULL) > 0
	BEGIN
		RAISERROR('Tank # value is required for Gauge Run & Net Barrel tickets', 16, 1)
		RETURN
	END

	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1) AND CarrierTicketNum IS NULL) > 0
	BEGIN
		RAISERROR('Ticket # value is required for Gauge Run tickets', 16, 1)
		RETURN
	END

	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1) AND BarrelsPerInch IS NULL) > 0
	BEGIN
		RAISERROR('BarrelsPerInch value is required for Gauge Run tickets', 16, 1)
		RETURN
	END

	IF (SELECT COUNT(*) FROM inserted WHERE 
		(TicketTypeID IN (1, 2) AND (ProductOBsTemp IS NULL OR ProductObsGravity IS NULL OR ProductBSW IS NULL))
			OR (TicketTypeID IN (1) AND (ProductHighTemp IS NULL OR ProductLowTemp IS NULL))
		) > 0
	BEGIN
		RAISERROR('All Product Measurement values are required for Gauge Run & Net Barrel tickets', 16, 1)
		RETURN
	END
	
	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1) AND Rejected = 0 
		AND (OpeningGaugeFeet IS NULL OR OpeningGaugeInch IS NULL OR OpeningGaugeQ IS NULL)) > 0
	BEGIN
		RAISERROR('All Opening Gauge values are required for Gauge Run tickets', 16, 1)
		RETURN
	END
	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1) AND Rejected = 0 
		AND (ClosingGaugeFeet IS NULL OR ClosingGaugeInch IS NULL OR ClosingGaugeQ IS NULL)) > 0
	BEGIN
		RAISERROR('All Closing Gauge values are required for Gauge Run tickets', 16, 1)
		RETURN
	END

	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (2) AND Rejected = 0 
		AND (GrossUnits IS NULL)) > 0
	BEGIN
		RAISERROR('Gross Volume value is required for Net Unit tickets', 16, 1)
		RETURN
	END

	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (3) AND Rejected = 0 
		AND (GrossUnits IS NULL OR NetUnits IS NULL)) > 0
	BEGIN
		RAISERROR('Gross & Net Volume values are required for Meter Run tickets', 16, 1)
		RETURN
	END

	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1) AND Rejected = 0 
		AND (SealOff IS NULL OR SealOn IS NULL)) > 0
	BEGIN
		RAISERROR('All Seal Off & Seal On values are required for Gauge Run tickets', 16, 1)
		RETURN
	END

END

GO

/*******************************************/
-- Date Created: 31 Aug 2013
-- Author: Kevin Alons
-- Purpose: return driver editable Order data for Driver App sync
/*******************************************/
ALTER FUNCTION [dbo].[fnOrderEdit_DriverApp]( @DriverID int, @LastChangeDateUTC datetime ) RETURNS TABLE AS
RETURN 
	SELECT O.ID
		, O.StatusID
		, O.TruckID
		, O.TrailerID
		, O.Trailer2ID
		, O.OriginBOLNum
		, O.OriginArriveTimeUTC
		, O.OriginDepartTimeUTC
		, O.OriginMinutes
		, O.OriginWaitNotes
		, O.OriginTruckMileage
		, O.OriginGrossUnits
		, O.OriginNetUnits
		, O.ChainUp
		, O.Rejected
		, O.RejectNotes
		, O.OriginTankNum
		, O.DestArriveTimeUTC
		, O.DestDepartTimeUTC
		, O.DestMinutes
		, O.DestWaitNotes
		, O.DestBOLNum
		, O.DestTruckMileage
		, O.DestGrossUnits
		, O.DestNetUnits
		, O.DestProductTemp
		, O.DestProductBSW
		, O.DestProductGravity
		, O.DestOpenMeterUnits
		, O.DestCloseMeterUnits
		, O.CarrierTicketNum
		, O.AcceptLastChangeDateUTC
		, O.PickupLastChangeDateUTC
		, O.DeliverLastChangeDateUTC
	FROM dbo.tblOrder O
	LEFT JOIN tblOrderDriverAppVirtualDelete ODAVD ON ODAVD.OrderID = O.ID AND ODAVD.DriverID = @DriverID
	WHERE (O.StatusID IN (2, 7, 8) -- Dispatched, Accepted, Picked Up
		-- or status = Delivered and the driver still has access to the record
		OR (O.StatusID = 3 AND O.DestDepartTimeUTC < DATEADD(hour, (SELECT cast(value as int) FROM tblSetting WHERE ID = 10), GETUTCDATE()))
		-- or this potentially was a DriverApp record, that no longer is, so should be included (as deleted)
		OR (ODAVD.ID IS NOT NULL AND ODAVD.VirtualDeleteDateUTC >= @LastChangeDateUTC))
	  AND (O.DriverID = @DriverID OR ODAVD.DriverID = @DriverID)
	  AND (
		@LastChangeDateUTC IS NULL 
		OR O.CreateDateUTC > @LastChangeDateUTC
		OR O.LastChangeDateUTC >= @LastChangeDateUTC
		OR O.DeleteDateUTC >= @LastChangeDateUTC
		OR ODAVD.VirtualDeleteDateUTC >= @LastChangeDateUTC)

GO

/*******************************************/
-- Date Created: 25 Apr 2013
-- Author: Kevin Alons
-- Purpose: return readonly Order data for Driver App sync
/*******************************************/
ALTER FUNCTION [dbo].[fnOrderReadOnly_DriverApp]( @DriverID int, @LastChangeDateUTC datetime ) RETURNS TABLE AS
RETURN 
	SELECT O.ID
		, O.OrderNum
		, O.StatusID
		, O.TicketTypeID
		, OO.TankTypeID
		, cast(P.PriorityNum as int) AS PriorityNum
		, O.Product
		, O.DueDate
		, O.Origin
		, O.OriginFull
		, OO.OriginType
		, O.OriginUomID
		, OO.Station AS OriginStation
		, OO.County AS OriginCounty
		, O.OriginState
		, OO.WellAPI AS OriginAPI
		, O.Destination
		, O.DestinationFull
		, D.DestinationType AS DestType 
		, O.DestUomID
		, D.Station AS DestinationStation
		--, cast(CASE WHEN D.TicketTypeID = 2 THEN 1 ELSE 0 END as bit) AS DestBOLAvailable
		, OO.BarrelsPerInch AS OriginBarrelsPerInch
		, O.CreateDateUTC
		, O.CreatedByUser
		, O.LastChangeDateUTC
		, O.LastChangedByUser
		, isnull(ODAVD.VirtualDeleteDateUTC, O.DeleteDateUTC) AS DeleteDateUTC
		, isnull(ODAVD.VirtualDeletedByUser, O.DeletedByUser) AS DeletedByUser
		, cast(CASE WHEN isnull(ODAVD.VirtualDeleteDateUTC, O.DeleteDateUTC) IS NULL THEN 0 ELSE 1 END as bit) AS Deleted
		, O.OriginID
		, O.DestinationID
		, cast(O.PriorityID AS int) AS PriorityID
		, O.Operator
		, O.OperatorID
		, O.Pumper
		, O.PumperID
		, O.Producer
		, O.ProducerID
		, O.Customer
		, O.CustomerID
		, O.Carrier
		, O.CarrierID
		, O.ProductID
		, O.TicketType
		, C.ZPLHeaderBlob AS PrintHeaderBlob
		, isnull(C.EmergencyInfo, 'For an emergency (spill, leak, fire, or accident) contact CHEMTREC @ 800.424.9300 (toll free)') AS EmergencyInfo
		, O.DestTicketTypeID
		, O.DestTicketType
		, O.OriginTankNum
	FROM dbo.viewOrder O
	JOIN dbo.tblPriority P ON P.ID = O.PriorityID
	JOIN dbo.viewOrigin OO ON OO.ID = O.OriginID
	JOIN dbo.viewDestination D ON D.ID = O.DestinationID
	JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
	LEFT JOIN tblOrderDriverAppVirtualDelete ODAVD ON ODAVD.OrderID = O.ID AND ODAVD.DriverID = @DriverID
	WHERE (O.StatusID IN (2, 7, 8) -- Dispatched, Accepted, Picked Up
		-- or status = Delivered and the driver still has access to the record
		OR (StatusID = 3 AND O.DestDepartTimeUTC < DATEADD(hour, (SELECT cast(value as int) FROM tblSetting WHERE ID = 10), GETUTCDATE()))
		-- or this potentially was a DriverApp record, that no longer is, so should be included (as deleted)
		OR (ODAVD.ID IS NOT NULL AND ODAVD.VirtualDeleteDateUTC >= @LastChangeDateUTC))
	  AND (O.DriverID = @DriverID OR ODAVD.DriverID = @DriverID)
	  AND (
		@LastChangeDateUTC IS NULL 
		OR O.CreateDateUTC > @LastChangeDateUTC
		OR O.LastChangeDateUTC >= @LastChangeDateUTC
		OR O.DeleteDateUTC >= @LastChangeDateUTC
		OR C.LastChangeDateUTC >= @LastChangeDateUTC
		OR ODAVD.VirtualDeleteDateUTC >= @LastChangeDateUTC)

GO

/*******************************************/
-- Date Created: 25 Apr 2013
-- Author: Kevin Alons
-- Purpose: return OrderTicket data for Driver App sync
/*******************************************/
ALTER FUNCTION [dbo].[fnOrderTicket_DriverApp]( @DriverID int, @LastChangeDateUTC datetime ) RETURNS  TABLE AS
RETURN 
	SELECT OT.ID
		, OT.UID
		, OT.OrderID
		, OT.CarrierTicketNum
		, OT.TankNum
		, OT.TicketTypeID
		, OT.TankTypeID
		, OT.ProductObsGravity
		, OT.ProductObsTemp
		, OT.ProductHighTemp
		, OT.ProductLowTemp
		, OT.ProductBSW
		, cast(OT.OpeningGaugeFeet as tinyint) AS OpeningGaugeFeet
		, cast(OT.OpeningGaugeInch as tinyint) AS OpeningGaugeInch
		, cast(OT.OpeningGaugeQ as tinyint) AS OpeningGaugeQ
		, cast(OT.ClosingGaugeFeet as tinyint) AS ClosingGaugeFeet
		, cast(OT.ClosingGaugeInch as tinyint) AS ClosingGaugeInch
		, cast(OT.ClosingGaugeQ as tinyint) AS ClosingGaugeQ
		, OT.GrossUnits
		, OT.NetUnits
		, OT.Rejected
		, OT.RejectNotes
		, OT.SealOff
		, OT.SealOn
		, OT.BOLNum
		, OT.BarrelsPerInch
		, OT.CreateDateUTC
		, OT.CreatedByUser
		, OT.LastChangeDateUTC
		, OT.LastChangedByUser
		, OT.DeleteDateUTC
		, OT.DeletedByUser
		, cast(CASE WHEN OT.DeleteDateUTC IS NULL THEN 0 ELSE 1 END as bit) AS Deleted
	FROM dbo.tblOrderTicket OT
	JOIN dbo.tblOrder O ON O.ID = OT.OrderID
	WHERE (StatusID IN (2, 7, 8) -- Dispatched, Accepted, Picked Up
		-- or status = Delivered and the driver still has access to the record
		OR (StatusID = 3 AND O.DestDepartTimeUTC < DATEADD(hour, (SELECT cast(value as int) FROM tblSetting WHERE ID = 10), GETUTCDATE())))
	  AND O.DriverID = @DriverID
	  AND (@LastChangeDateUTC IS NULL 
		OR OT.CreateDateUTC >= @LastChangeDateUTC
		OR OT.LastChangeDateUTC >= @LastChangeDateUTC)

GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details into a single view
/***********************************/
ALTER VIEW [dbo].[viewOrder_OrderTicket_Full] AS 
	SELECT OE.* 
		, CASE WHEN OE.TicketCount = 0 THEN OE.TicketType ELSE OT.TicketType END AS T_TicketType
		, CASE WHEN OE.TicketCount = 0 THEN OE.CarrierTicketNum ELSE OT.CarrierTicketNum END AS T_CarrierTicketNum
		, CASE WHEN OE.TicketCount = 0 THEN OE.OriginBOLNum ELSE OT.BOLNum END AS T_BOLNum
		, OT.TankNum AS T_TankNum
		, OT.OpeningGaugeFeet AS T_OpeningGaugeFeet, OT.OpeningGaugeInch AS T_OpeningGaugeInch, OT.OpeningGaugeQ AS T_OpeningGaugeQ
		, OT.ClosingGaugeFeet AS T_ClosingGaugeFeet, OT.ClosingGaugeInch AS T_ClosingGaugeInch, OT.ClosingGaugeQ AS T_ClosingGaugeQ
		, dbo.fnGaugeQtrInches(OT.OpeningGaugeFeet, OT.OpeningGaugeInch, OT.OpeningGaugeQ) AS T_OpenTotalQ
		, dbo.fnGaugeQtrInches(OT.ClosingGaugeFeet, OT.ClosingGaugeInch, ot.ClosingGaugeQ) AS T_CloseTotalQ
		, ltrim(OT.OpeningGaugeFeet) + 'ft ' + ltrim(OT.OpeningGaugeInch) + 'in ' + ltrim(OT.OpeningGaugeQ) + 'q' AS T_OpenReading
		, ltrim(OT.ClosingGaugeFeet) + 'ft ' + ltrim(OT.ClosingGaugeInch) + 'in ' + ltrim(OT.ClosingGaugeQ) + 'q' AS T_CloseReading
		-- using cast(xx as decimal(9,4)) to properly round to 3 decimal digits (with no trailing 0's)
		, ltrim(round(cast(dbo.fnCorrectedAPIGravity(OT.ProductObsGravity, OT.ProductObsTemp) as decimal(9,4)), 9, 4)) AS T_CorrectedAPIGravity
		, ltrim(OT.SealOff) AS T_SealOff
		, ltrim(OT.SealOn) AS T_SealOn
		, OT.ProductObsTemp AS T_ProductObsTemp
		, OT.ProductObsGravity AS T_ProductObsGravity
		, OT.ProductBSW AS T_ProductBSW
		, OT.Rejected AS T_Rejected
		, OT.RejectNotes AS T_RejectNotes
		, OT.GrossUnits AS T_GrossUnits
		, OT.NetUnits AS T_NetUnits
		, dbo.fnRerouteDetails(OE.ID, 'PreviousDestinations', '<br/>') AS PreviousDestinations
		, dbo.fnRerouteDetails(OE.ID, 'RerouteUsers', '<br/>') AS RerouteUsers
		, dbo.fnRerouteDetails(OE.ID, 'RerouteDates', '<br/>') AS RerouteDates
		, dbo.fnRerouteDetails(OE.ID, 'RerouteNotes', '<br/>') AS RerouteNotes
	FROM dbo.viewOrderExportFull OE
	LEFT JOIN viewOrderTicket OT ON OT.OrderID = OE.ID AND OT.DeleteDateUTC IS NULL
	WHERE OE.DeleteDateUTC IS NULL
	
GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details + FINANCIAL INFO into a single view
/***********************************/
ALTER VIEW [dbo].[viewOrder_Financial_Carrier] AS 
	SELECT OE.* 
		, dbo.fnLocal_to_UTC(ISNULL(OIC.LastChangeDateUTC, OIC.CreateDateUTC), OE.OriginTimeZoneID, OE.OriginUseDST) AS RatesAppliedDate
		, OIC.BatchID
		, SB.BatchNum
		, OIC.ChainupFee
		, OIC.RerouteFee
		, OIC.BillableWaitMinutes
		, OIC.WaitFeeSubUnitID
		, OIC.WaitFeeRoundingTypeID
		, OIC.WaitRate
		, OIC.WaitFee
		, OIC.RejectionFee
		, OIC.H2SRate
		, OIC.H2SFee
		, OIC.TaxRate
		, OIC.RouteRate
		, OIC.LoadFee
		, OIC.TotalFee
		, OIC.FuelSurcharge
		, C.MinSettlementUnits
		, C.MinSettlementUomID
		, dbo.fnOrderTicketDetails(OE.ID, 'TicketNums', '<br/') AS TicketNums
		, dbo.fnOrderTicketDetails(OE.ID, 'TankNums', '<br/') AS TankNums
	FROM dbo.viewOrderExportFull OE
	JOIN dbo.tblCarrier C ON C.ID = OE.CarrierID
	LEFT JOIN dbo.tblOrderInvoiceCarrier OIC ON OIC.OrderID = OE.ID
	LEFT JOIN dbo.tblCarrierSettlementBatch SB ON SB.ID = OIC.BatchID

GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details + FINANCIAL INFO into a single view
/***********************************/
ALTER VIEW [dbo].[viewOrder_Financial_Customer] AS 
	SELECT OE.* 
		, dbo.fnLocal_to_UTC(ISNULL(OIC.LastChangeDateUTC, OIC.CreateDateUTC), OE.OriginTimeZoneID, OE.OriginUseDST) AS RatesAppliedDate
		, OIC.BatchID
		, SB.BatchNum
		, OIC.ChainupFee
		, OIC.RerouteFee
		, OIC.BillableWaitMinutes
		, OIC.WaitFeeSubUnitID
		, OIC.WaitFeeRoundingTypeID
		, OIC.WaitRate
		, OIC.WaitFee
		, OIC.RejectionFee
		, OIC.H2SRate
		, OIC.H2SFee
		, OIC.TaxRate
		, OIC.RouteRate
		, OIC.LoadFee
		, OIC.TotalFee
		, OIC.FuelSurcharge
		, C.MinSettlementUnits
		, C.MinSettlementUomID
		, dbo.fnOrderTicketDetails(OE.ID, 'TicketNums', '<br/') AS TicketNums
		, dbo.fnOrderTicketDetails(OE.ID, 'TankNums', '<br/') AS TankNums
	FROM dbo.viewOrderExportFull OE
	JOIN dbo.tblCustomer C ON C.ID = OE.CustomerID
	LEFT JOIN dbo.tblOrderInvoiceCustomer OIC ON OIC.OrderID = OE.ID
	LEFT JOIN dbo.tblCustomerSettlementBatch SB ON SB.ID = OIC.BatchID

GO

COMMIT
SET NOEXEC OFF

EXEC _spRefreshAllViews
GO
EXEC _spRecompileAllStoredProcedures
GO