-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.7.35.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.7.35.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0
SET NOEXEC OFF
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.7.35'
SELECT  @NewVersion = '3.7.36'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'Settlement Center - fix Shipper Wait Fee Parameter page errors'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper WaitFeeParameter rows for the specified criteria
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - add ProducerID parameter/field as new Best-Match criteria
	- 3.7.35 - 2015/06/23 - KDA - add missing EffectiveDate, EndDate returned columns
***********************************/
ALTER FUNCTION [fnShipperWaitFeeParametersDisplay](@StartDate date, @EndDate date, @ShipperID int, @ProductGroupID int, @OriginStateID int, @DestStateID int, @RegionID int, @ProducerID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ShipperID, R.ProductGroupID, R.OriginStateID, R.DestStateID, R.RegionID, R.ProducerID
		, R.SubUnitID, R.RoundingTypeID, R.OriginThresholdMinutes, R.DestThresholdMinutes
		, R.OriginMinBillableMinutes, R.OriginMaxBillableMinutes, R.DestMinBillableMinutes, R.DestMaxBillableMinutes
		, R.EffectiveDate, R.EndDate, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Shipper = C.Name
		, ProductGroup = PG.Name
		, OriginState = OS.FullName
		, OriginStateAbbrev = OS.Abbreviation
		, DestinationState = DS.FullName
		, DestinationStateAbbrev = DS.Abbreviation
		, Region = REG.Name
		, Producer = PR.Name
		, RoundingType = WFRT.Name
		, SubUnit = WFSU.Name
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch, Ranking
	FROM dbo.fnShipperWaitFeeParameter(@StartDate, @EndDate, @ShipperID, @ProductGroupID, @OriginStateID, @DestStateID, @RegionID, @ProducerID, 0) R 
	JOIN tblWaitFeeRoundingType WFRT ON WFRT.ID = R.RoundingTypeID
	JOIN tblWaitFeeSubUnit WFSU ON WFSU.ID = R.SubUnitID
	LEFT JOIN tblCustomer C ON C.ID = R.ShipperID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblState OS ON OS.ID = R.OriginStateID
	LEFT JOIN tblState DS ON DS.ID = R.DestStateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblProducer PR ON PR.ID = R.ProducerID
	ORDER BY EffectiveDate
GO

COMMIT 
SET NOEXEC OFF