/* updates to DriverApp.Sync logic (Virtual Delete mostly)
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.2.0'
SELECT  @NewVersion = '2.2.1'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

/*******************************************/
-- Date Created: 31 Aug 2013
-- Author: Kevin Alons
-- Purpose: return driver editable Order data for Driver App sync
/*******************************************/
ALTER FUNCTION [dbo].[fnOrderEdit_DriverApp]( @DriverID int, @LastChangeDateUTC datetime ) RETURNS TABLE AS
RETURN 
	SELECT O.ID
		, O.StatusID
		, O.TruckID
		, O.TrailerID
		, O.Trailer2ID
		, O.OriginBOLNum
		, O.OriginArriveTimeUTC
		, O.OriginDepartTimeUTC
		, O.OriginMinutes
		, O.OriginWaitNotes
		, O.OriginTruckMileage
		, O.OriginGrossBarrels
		, O.OriginNetBarrels
		, O.ChainUp
		, O.Rejected
		, O.RejectNotes
		, O.OriginTankNum
		, O.DestArriveTimeUTC
		, O.DestDepartTimeUTC
		, O.DestMinutes
		, O.DestWaitNotes
		, O.DestBOLNum
		, O.DestTruckMileage
		, O.DestGrossBarrels
		, O.DestNetBarrels
		, O.DestProductTemp
		, O.DestProductBSW
		, O.DestProductGravity
		, O.DestOpenMeterBarrels
		, O.DestCloseMeterBarrels
		, O.CarrierTicketNum
		, O.AcceptLastChangeDateUTC
		, O.PickupLastChangeDateUTC
		, O.DeliverLastChangeDateUTC
	FROM dbo.tblOrder O
	LEFT JOIN tblOrderDriverAppVirtualDelete ODAVD ON ODAVD.OrderID = O.ID AND ODAVD.DriverID = @DriverID
	WHERE (O.StatusID IN (2, 7, 8) -- Dispatched, Accepted, Picked Up
		-- or status = Delivered and the driver still has access to the record
		OR (O.StatusID = 3 AND O.DestDepartTimeUTC < DATEADD(hour, (SELECT cast(value as int) FROM tblSetting WHERE ID = 10), GETUTCDATE()))
		-- or this potentially was a DriverApp record, that no longer is, so should be included (as deleted)
		OR (ODAVD.ID IS NOT NULL AND ODAVD.VirtualDeleteDateUTC >= @LastChangeDateUTC))
	  AND (O.DriverID = @DriverID OR ODAVD.DriverID = @DriverID)
	  AND (
		@LastChangeDateUTC IS NULL 
		OR O.CreateDateUTC > @LastChangeDateUTC
		OR O.LastChangeDateUTC >= @LastChangeDateUTC
		OR O.DeleteDateUTC >= @LastChangeDateUTC
		OR ODAVD.VirtualDeleteDateUTC >= @LastChangeDateUTC)

GO

COMMIT
SET NOEXEC OFF