-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.11.20.1'
SELECT  @NewVersion = '3.11.20.2'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Hotfix: Replacing original viewDriverLocation_OriginFirstArrive and viewDriverLocation_DestinationFirstArrive due to timeout/sync issue...again.'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/**********************************************************
-- Author: Kevin Alons
-- Date Created: 8 Oct 2014
-- Purpose: JOIN the DriverLocation table with the Order Table to find the most suitable Origin Arrival GPS record
-- Changes:
3.11.14 - 2016/04/18 - JAE		- Added top 1 to ensure only one record is selected (sync can sometimes send duplicates)
3.11.15.1 - 2016/04/20 - JAE	- Undo top 1 --NEED TO INVESTIGATE WHY THIS WOULD BE AN ISSUE
3.11.18.1 - 2016/05/03 - KDA	- stop using UID but instead use ID for performance reasons
3.11.20.1 - 2016/05/04 - JAE & BB - undo last change of UID > ID due to timeout/syncing errors
***********************************************************/
ALTER VIEW viewDriverLocation_OriginFirstArrive AS
SELECT DL3.*
FROM tblDriverLocation DL3
JOIN (
	-- get the first matching record (by lowest UID)
	SELECT DL2.OrderID, DL2.OriginID, UID = min(cast(DL2.UID as varchar(100)))
	FROM tblDriverLocation DL2
	JOIN (
		-- get the best (lowest) SourceAccuracyMeters
		SELECT DL.OrderID, DL.OriginID, X.SourceDateUTC, SourceAccuracyMeters = min(DL.SourceAccuracyMeters)
		FROM tblDriverLocation DL
		JOIN (
			-- get the first SourceDateUTC
			SELECT OrderID, OriginID, SourceDateUTC = MIN(SourceDateUTC)
			FROM tblDriverLocation 
			WHERE OriginID IS NOT NULL
			GROUP BY OrderID, OriginID
		) X ON X.OrderID = DL.OrderID
			AND X.OriginID = DL.OriginID
			AND X.SourceDateUTC = DL.SourceDateUTC
		GROUP BY DL.OrderID, DL.OriginID, X.SourceDateUTC
	) X2 ON X2.OrderID = DL2.OrderID AND X2.OriginID = DL2.OriginID 
		AND X2.SourceDateUTC = DL2.SourceDateUTC AND X2.SourceAccuracyMeters = DL2.SourceAccuracyMeters
	GROUP BY DL2.OrderID, DL2.OriginID
) X3 ON X3.OrderID = DL3.OrderID AND X3.OriginID = DL3.OriginID  AND X3.UID = DL3.UID
GO


/**********************************************************
-- Author: Kevin Alons
-- Date Created: 8 Oct 2014
-- Purpose: JOIN the DriverLocation table with the Order Table to find the most suitable Destination Arrival GPS record
-- Changes:
3.11.14 - 2016/04/18 - JAE		- Added top 1 to ensure only one record is selected (sync can sometimes send duplicates)
3.11.15.1 - 2016/04/20 - JAE	- Undo top 1
3.11.18.1 - 2016/05/03 - KDA	- stop using UID but instead use ID for performance reasons
3.11.20.1 - 2016/05/04 - JAE & BB - undo last change of UID > ID due to timeout/syncing errors
***********************************************************/
ALTER VIEW [dbo].[viewDriverLocation_DestinationFirstArrive] AS
SELECT DL3.*
FROM tblDriverLocation DL3
JOIN (
	-- get the first matching record (by lowest UID)
	SELECT DL2.OrderID, DL2.DestinationID, UID = min(cast(DL2.UID as varchar(100)))
	FROM tblDriverLocation DL2
	JOIN (
		-- get the best (lowest) SourceAccuracyMeters
		SELECT DL.OrderID, DL.DestinationID, X.SourceDateUTC, SourceAccuracyMeters = min(DL.SourceAccuracyMeters)
		FROM tblDriverLocation DL
		JOIN (
			-- get the first SourceDateUTC
			SELECT OrderID, DestinationID, SourceDateUTC = MIN(SourceDateUTC)
			FROM tblDriverLocation 
			WHERE DestinationID IS NOT NULL
			GROUP BY OrderID, DestinationID
		) X ON X.OrderID = DL.OrderID
			AND X.DestinationID = DL.DestinationID
			AND X.SourceDateUTC = DL.SourceDateUTC
		GROUP BY DL.OrderID, DL.DestinationID, X.SourceDateUTC
	) X2 ON X2.OrderID = DL2.OrderID AND X2.DestinationID = DL2.DestinationID 
		AND X2.SourceDateUTC = DL2.SourceDateUTC AND X2.SourceAccuracyMeters = DL2.SourceAccuracyMeters
	GROUP BY DL2.OrderID, DL2.DestinationID
) X3 ON X3.OrderID = DL3.OrderID AND X3.DestinationID = DL3.DestinationID  AND X3.UID = DL3.UID
GO

EXEC sp_refreshview viewDriverLocation_OriginFirstArrive
GO
EXEC sp_refreshview viewDriverLocation_DestinationFirstArrive
GO
EXEC sp_refreshview viewReportCenter_Orders
GO
EXEC _spRebuildAllObjects
GO



COMMIT 
SET NOEXEC OFF