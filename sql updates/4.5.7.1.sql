SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.5.7'
SELECT  @NewVersion = '4.5.7.1'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Update to mobile app sync - send tank strappings always'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/*******************************************
 Date Created: 5 Apr 2014
 Author: Kevin Alons
 Purpose: return OriginTankStrapping data for Driver App sync
 Changes:
 - 3.10.5		- 2016/01/29	- KDA	- use new OAC table to only include when the Order.OriginID changes
										- show IsDeleted = 1 when the record or any relevant parent records are deleted
 - 3.10.10.2	- 2016/02/24	- KDA	- remove VirtualDelete logic and ensure when an order.LastChangeDateUTC changes (not just CreateDateUTC)
 - 3.11.2		- 2016/02/20	- KDA	- eliminate use of Virtual Deletes - we can't delete them from here for deleted orders (these could be needed for another order)
										- rename @LastChangeDateUTC to @LastSyncDateUTC
 - 4.5.7.1		- 2017/02/16	- JAE	- send always (remove date filter) since it only sent changes before and now mobile expects everytime !!! NEED TO ADDRESS with OTSD table and new MVC Tank page
*******************************************/
ALTER FUNCTION fnOriginTankStrapping_DriverApp(@DriverID int, @LastSyncDateUTC datetime) RETURNS TABLE AS
RETURN 
	SELECT DISTINCT OTS.*
		, cast(CASE WHEN isnull(ltrim(OTSD.ID), ltrim(OT.DeleteDateUTC)) IS NULL THEN 0 ELSE 1 END as bit) AS IsDeleted
	FROM dbo.tblOriginTankStrapping OTS
	JOIN dbo.tblOriginTank OT ON OT.ID = OTS.OriginTankID
	JOIN dbo.tblOrder O ON O.OriginID = OT.OriginID
	LEFT JOIN tblOrderAppChanges OAC ON OAC.OrderID = O.ID
	LEFT JOIN dbo.tblOriginTankStrappingDeleted OTSD ON OTSD.ID = OTS.ID
	CROSS JOIN fnSyncLCDOffset(@LastSyncDateUTC) LCD
	WHERE O.StatusID IN (2, 7, 8, 3) -- Dispatched, Accepted, Picked Up, Delivered
	  AND O.DeleteDateUTC IS NULL -- don't send OriginTank records for deleted orders
	  AND O.DriverID = @DriverID
/*
	  AND (@LastSyncDateUTC IS NULL 
		OR O.CreateDateUTC >= LCD
		OR O.LastChangeDateUTC >= LCD
		-- required to ensure that when the Order.OriginID is changed, the OriginTanks/Strappings are always synced
		OR OAC.OriginChangeDateUTC >= LCD
		OR OT.CreateDateUTC >= LCD
		OR OT.LastChangeDateUTC >= LCD
		OR OTS.CreateDateUTC >= LCD
		OR OTS.LastChangeDateUTC >= LCD
		OR OTSD.DeleteDateUTC >= LCD)
*/

GO

COMMIT
SET NOEXEC OFF