-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.9.1.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.9.1.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0

SET NOEXEC OFF
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.9.2'
SELECT  @NewVersion = '3.9.3'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'OrderApproval: add new FinalXXX fields for display purposes'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/*************************************************************
-- Date Created: 15 Jun 2015
-- Author: Kevin Alons
-- Purpose: return the data for the Order Approval page
-- Changes:
	- 3.9.2 - 2015/08/26 - KDA - remove obsolete performance optimization using OrderDateUTC to force use of index (now OrderDate is indexed)
	- 3.9.3 - 2015/08/29 - KDA - add new FinalXXX fields
*************************************************************/
ALTER PROCEDURE spOrderApprovalSource
(
  @userName varchar(100)
, @ShipperID int = NULL
, @CarrierID int = NULL
, @ProductGroupID int = NULL
, @OriginStateID int = NULL
, @DestStateID int = NULL
, @Start date = null
, @End date = null
, @IncludeApproved bit = 0
) AS BEGIN
	SET NOCOUNT ON;
	
	SELECT *
		, FinalActualMiles = ISNULL(OverrideActualMiles, ActualMiles)
		, FinalOriginMinutes = ISNULL(OverrideOriginMinutes, OriginMinutes)
		, FinalDestMinutes = ISNULL(OverrideDestMinutes, DestMinutes)
		, FinalChainup = cast(ISNULL(1-nullif(OverrideChainup, 0), Chainup) as bit)
		, FinalH2S = cast(ISNULL(1-nullif(OverrideH2S, 0), H2S) as bit)
		, FinalRerouted = cast(ISNULL(1-nullif(OverrideReroute, 0), Rerouted) as bit)
	INTO #X
	FROM viewOrderApprovalSource 
	WHERE (NULLIF(@ShipperID, -1) IS NULL OR ShipperID = @ShipperID)
	  AND (NULLIF(@CarrierID, -1) IS NULL OR CarrierID = @CarrierID)
	  AND (NULLIF(@ProductGroupID, -1) IS NULL OR ProductGroupID = @ProductGroupID)
	  AND (NULLIF(@OriginStateID, -1) IS NULL OR OriginStateID = @OriginStateID)
	  AND (NULLIF(@DestStateID, -1) IS NULL OR DestStateID = @DestStateID)
	  AND OrderDate BETWEEN isnull(@Start, '1/1/1900') AND isnull(@End, '1/1/2200')
	  AND (@IncludeApproved = 1 OR isnull(Approved, 0) = 0) 
	  
	DECLARE @id int
	WHILE EXISTS (SELECT * FROM #x WHERE OrderID IS NULL)
	BEGIN
		SELECT @id = min(ID) FROM #x WHERE OrderID IS NULL
		EXEC spApplyRatesBoth @id, @userName
		UPDATE #x SET OrderID = @id, Approved = (SELECT Approved FROM tblOrderApproval WHERE OrderID = @id) WHERE ID = @id
	END
	ALTER TABLE #X DROP COLUMN OrderID
	
	SELECT * FROM #x WHERE (@IncludeApproved = 1 OR isnull(Approved, 0) = 0) 

END
GO

SET NOEXEC OFF
COMMIT