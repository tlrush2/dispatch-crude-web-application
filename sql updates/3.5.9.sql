-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.5.8'
SELECT  @NewVersion = '3.5.9'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'Rewrite Fuel Surcharge logic to allow ALL Carrier|Shipper & ProductGroup records (and BestMatch lookup logic)'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

ALTER TABLE tblRateType ADD ForAssessorialFee bit NOT NULL CONSTRAINT DF_RateType_ForAssessorialFee DEFAULT (0)
GO
ALTER TABLE tblRateType ADD ForLoadFee bit NOT NULL CONSTRAINT DF_RateType_ForLoadFee DEFAULT (0)
GO
UPDATE tblRateType SET ForAssessorialFee = 1, ForLoadFee = 1
GO
INSERT INTO tblRateType (ID, Name, ForCarrier, ForOrderReject, ForShipper, ForAssessorialFee, ForLoadFee)
	VALUES (4, '% of Load $$', 1, 0, 1, 1, 0)
GO

/***********************************/
-- Date Created: 23 Dec 2014
-- Author: Kevin Alons
-- Purpose: compute and return the normalized "rate" + Amount for the specified order parameters
/***********************************/
ALTER FUNCTION fnRateToAmount(@RateTypeID int, @Units decimal(18, 10), @UomID int, @Rate decimal(18, 10), @RateUomID int, @LoadAmount money = NULL, @CustomerAmount money = NULL) RETURNS decimal(18, 10)
AS BEGIN
	DECLARE @ret money
	IF (@RateTypeID = 1) -- Per Unit rate type
		SET @ret = @Units * dbo.fnConvertRateUOM(@Rate, @RateUomID, @UomID)
	ELSE IF (@RateTypeID = 2) -- Flat
		SET @ret = @Rate
	ELSE IF (@RateTypeID = 3) -- % of Customer (Shipper) rate
		SET @ret = @Rate * @CustomerAmount / 100.0
	ELSE IF (@RateTypeID = 4) -- % of Load Amount
		SET @ret = @Rate * @LoadAmount / 100.0
	RETURN (@ret)
END
GO

-----------------------------------------------
-- CARRIER changes
-----------------------------------------------

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'tblCarrierWaitFeeParameter') AND name = N'udxCarrierWaitFeeParameter_Main')
	DROP INDEX udxCarrierWaitFeeParameter_Main ON tblCarrierWaitFeeParameter WITH ( ONLINE = OFF )
GO
CREATE UNIQUE CLUSTERED INDEX udxCarrierWaitFeeParameter_Main ON tblCarrierWaitFeeParameter
(
	ShipperID
	, CarrierID
	, ProductGroupID
	, OriginStateID
	, DestStateID
	, RegionID
	, EffectiveDate
)
GO

CREATE TABLE tblCarrierFuelSurchargeRate
(
	ID int IDENTITY(1,1) NOT NULL CONSTRAINT PK_CarrierFuelSurchargeRate PRIMARY KEY NONCLUSTERED
	, ShipperID int NULL CONSTRAINT FK_CarrierFuelSurcharge_Shipper FOREIGN KEY REFERENCES tblCustomer(ID)
	, CarrierID int NULL CONSTRAINT FK_CarrierFuelSurcharge_Carrier FOREIGN KEY REFERENCES tblCarrier(ID)
	, ProductGroupID int NULL CONSTRAINT FK_CarrierFuelSurcharge_ProductGroup FOREIGN KEY REFERENCES tblProductGroup(ID)
	, FuelPriceFloor money NOT NULL
	, IntervalAmount money NOT NULL
	, IncrementAmount money NOT NULL
	, EffectiveDate date NOT NULL
	, EndDate date NOT NULL
	, CreateDateUTC smalldatetime NOT NULL CONSTRAINT DF_CarrierFuelSurcharge_CreateDateUTC DEFAULT (getutcdate())
	, CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_CarrierFuelSurcharge_CreatedBy DEFAULT (SUSER_NAME())
	, LastChangeDateUTC smalldatetime NULL
	, LastChangedByUser varchar(100) NULL
	, CONSTRAINT CK_CarrierFuelSurchargeRate_EndDate_Greater CHECK  (([EndDate]>=[EffectiveDate]))
)
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblCarrierFuelSurchargeRate TO dispatchcrude_iis_acct
GO
CREATE UNIQUE CLUSTERED INDEX udxCarrierFuelSurchargeRate_Main ON tblCarrierFuelSurchargeRate
(
	ShipperID
	, CarrierID
	, ProductGroupID
	, EffectiveDate
)
GO
SELECT TOP 1 CarrierID = NULL, FuelPriceFloor, IntervalAmount = min(IntervalAmount), IncrementAmount = min(IncrementAmount) INTO #temp FROM tblCarrierFuelSurchargeRates GROUP BY FuelPriceFloor ORDER BY COUNT(*) DESC
INSERT INTO tblCarrierFuelSurchargeRate (CarrierID, FuelPriceFloor, IntervalAmount, IncrementAmount, EffectiveDate, EndDate)
	SELECT CarrierID, FuelPriceFloor, IntervalAmount, IncrementAmount, '1/1/2014', '12/31/2014' FROM #temp
	UNION 
	SELECT R.CarrierID, R.FuelPriceFloor, R.IntervalAmount, R.IncrementAmount, '1/1/2014', '12/31/2014' 
	FROM tblCarrierFuelSurchargeRates R 
	LEFT JOIN #temp t ON t.FuelPriceFloor = R.FuelPriceFloor 
	WHERE t.FuelPriceFloor IS NULL
	UNION
	SELECT CarrierID, FuelPriceFloor, IntervalAmount, IncrementAmount, '1/1/2015', '12/31/2015' FROM #temp
	UNION 
	SELECT R.CarrierID, R.FuelPriceFloor, R.IntervalAmount, R.IncrementAmount, '1/1/2015', '12/31/2015' 
	FROM tblCarrierFuelSurchargeRates R 
	LEFT JOIN #temp t ON t.FuelPriceFloor = R.FuelPriceFloor 
	WHERE t.FuelPriceFloor IS NULL
GO
DROP TABLE #temp
GO

IF EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'FK_CarrierFuelSurchargeRates_Customer') AND parent_object_id = OBJECT_ID(N'tblCarrierFuelSurchargeRates'))
	ALTER TABLE tblCarrierFuelSurchargeRates DROP CONSTRAINT FK_CarrierFuelSurchargeRates_Customer
GO
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'DF_CarrierFuelSurchargeRates_CreateDateUTC') AND type = 'D')
BEGIN
	ALTER TABLE tblCarrierFuelSurchargeRates DROP CONSTRAINT DF_CarrierFuelSurchargeRates_CreateDateUTC
END
GO
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'DF_CarrierFuelSurchargeRates_CreatedByUser') AND type = 'D')
BEGIN
	ALTER TABLE tblCarrierFuelSurchargeRates DROP CONSTRAINT DF_CarrierFuelSurchargeRates_CreatedByUser
END
GO
IF EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'FK_OrderSettlementCarrier_FuelSurchargeRate') AND parent_object_id = OBJECT_ID(N'tblOrderSettlementCarrier'))
	ALTER TABLE tblOrderSettlementCarrier DROP CONSTRAINT FK_OrderSettlementCarrier_FuelSurchargeRate
GO 
UPDATE tblOrderSettlementCarrier
	SET FuelSurchargeRateID = R.ID
FROM tblOrderSettlementCarrier S
JOIN tblCarrierFuelSurchargeRates O ON O.ID = S.FuelSurchargeRateID
LEFT JOIN tblCarrierFuelSurchargeRate R ON (R.CarrierID IS NULL AND R.FuelPriceFloor = O.FuelPriceFloor) OR (R.CarrierID = O.CarrierID AND R.FuelPriceFloor = O.FuelPriceFloor)
GO

ALTER TABLE tblOrderSettlementCarrier ADD CONSTRAINT FK_OrderSettlementCarrier_FuelSurchargeRate FOREIGN KEY (FuelSurchargeRateID) REFERENCES tblCarrierFuelSurchargeRate(ID)
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'tblCarrierFuelSurchargeRates') AND type in (N'U'))
	DROP TABLE tblCarrierFuelSurchargeRates
GO

/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all CarrierFuelSurcharge records
******************************************************/
CREATE VIEW viewCarrierFuelSurchargeRate AS
	SELECT X.*
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementCarrier WHERE FuelSurchargeRateID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate)
			FROM tblCarrierDestinationWaitRate XN 
			WHERE isnull(XN.CarrierID, 0) = isnull(X.CarrierID, 0) 
			  AND isnull(XN.ProductGroupID, 0) = isnull(X.ProductGroupID, 0) 
			  AND XN.EffectiveDate > X.EffectiveDate)
	FROM tblCarrierFuelSurchargeRate X
GO
GRANT SELECT ON viewCarrierFuelSurchargeRate TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'fnCarrierFuelSurchargeRate') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION fnCarrierFuelSurchargeRate
GO
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier DestinationWaitRate info for the specified criteria
/***********************************/
CREATE FUNCTION fnCarrierFuelSurchargeRate
(
  @StartDate date
, @EndDate date
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID
			, Ranking =	dbo.fnRateRanking(@ShipperID, R.ShipperID, 4, 0)
				  + dbo.fnRateRanking(@CarrierID, R.CarrierID, 2, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 1, 0)
		FROM  dbo.viewCarrierFuelSurchargeRate R
		WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	)
	SELECT R.ID, ShipperID, CarrierID, ProductGroupID, FuelPriceFloor, IntervalAmount, IncrementAmount, EffectiveDate, EndDate, NextEffectiveDate
	  , BestMatch, Ranking
	  , Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewCarrierFuelSurchargeRate R
	JOIN (
		SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 3  -- ensure some type of match occurred on all 3 criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)
GO
GRANT SELECT ON fnCarrierFuelSurchargeRate TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RouteRate rows for the specified criteria
/***********************************/
CREATE FUNCTION fnCarrierFuelSurchargeRateDisplay(@StartDate date, @EndDate date, @ShipperID int, @CarrierID int, @ProductGroupID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.FuelPriceFloor, R.IntervalAmount, R.IncrementAmount, R.EffectiveDate, R.EndDate, R.NextEffectiveDate
		, Shipper = S.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch, Ranking
	FROM dbo.fnCarrierFuelSurchargeRate(@StartDate, @EndDate, @ShipperID, @CarrierID, @ProductGroupID, 0) R
	LEFT JOIN tblCustomer S ON S.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	ORDER BY EffectiveDate
GO
GRANT SELECT ON fnCarrierFuelSurchargeRate TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier FuelSurcharge info for the specified order
/***********************************/
CREATE FUNCTION fnOrderCarrierFuelSurchargeRate(@ID int) RETURNS TABLE AS RETURN
	SELECT RateID
		, Rate = IncrementAmount * (round(Pricediff / nullif(IntervalAmount, 0), 0) + CASE WHEN PriceDiff % IntervalAmount > 0 THEN 1 ELSE 0 END)
		, RouteMiles
	FROM (
		SELECT TOP 1 RateID = ID, FuelPriceFloor, IntervalAmount, IncrementAmount
			, PriceDiff = dbo.fnMaxDecimal(0, dbo.fnFuelPriceForOrigin(OriginID, OrderDate) - fuelPriceFloor)
			, RouteMiles
		FROM (
			SELECT R.ID, R.FuelPriceFloor, R.IntervalAmount, R.IncrementAmount, O.OrderDate, O.OriginID, RouteMiles = O.ActualMiles
			FROM dbo.viewOrder O
			CROSS APPLY dbo.fnCarrierFuelSurchargeRate(O.OrderDate, null, O.CustomerID, O.CarrierID, O.ProductGroupID, 1) R
			WHERE O.ID = @ID 
		) X
	) X2
GO
GRANT SELECT ON fnOrderCarrierFuelSurchargeRate TO dispatchcrude_iis_acct
GO 

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier FuelSurcharge data info for the specified order
/***********************************/
CREATE FUNCTION fnOrderCarrierFuelSurchargeData(@ID int) RETURNS TABLE AS RETURN
	SELECT RateID, Rate, Amount = Rate * R.RouteMiles
	FROM dbo.fnOrderCarrierFuelSurchargeRate(@ID) R
	WHERE Rate > 0
GO
GRANT SELECT ON fnOrderCarrierFuelSurchargeData TO dispatchcrude_iis_acct
GO

ALTER TABLE tblCarrierAssessorialRate ADD ShipperID int NULL CONSTRAINT FK_CarrierAssessorialRate_Shipper FOREIGN KEY REFERENCES tblCustomer(ID)
GO
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tblCarrierAssessorialRate]') AND name = N'udxCarrierAssessorialRate_Main')
	DROP INDEX [udxCarrierAssessorialRate_Main] ON [dbo].[tblCarrierAssessorialRate] WITH ( ONLINE = OFF )
GO
CREATE UNIQUE CLUSTERED INDEX [udxCarrierAssessorialRate_Main] ON [dbo].[tblCarrierAssessorialRate] 
(
	[TypeID] ASC,
	ShipperID ASC,
	[CarrierID] ASC,
	[ProductGroupID] ASC,
	[OriginID] ASC,
	[DestinationID] ASC,
	[OriginStateID] ASC,
	[DestStateID] ASC,
	[RegionID] ASC,
	[ProducerID] ASC,
	[OperatorID] ASC,
	[EffectiveDate] ASC
)
GO

/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all CarrierAssessorialRate records
******************************************************/
ALTER VIEW [dbo].[viewCarrierAssessorialRate] AS
	SELECT X.*
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM viewOrderSettlementCarrierAssessorialCharge WHERE AssessorialRateID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblCarrierAssessorialRate XN 
			WHERE XN.TypeID = X.TypeID
			  AND dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND dbo.fnCompareNullableInts(XN.OperatorID, X.OperatorID) = 1
			  AND XN.EffectiveDate > X.EffectiveDate)
	FROM tblCarrierAssessorialRate X

GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier AssessorialRate info for the specified order
/***********************************/
ALTER FUNCTION [dbo].[fnCarrierAssessorialRates](@StartDate date, @EndDate date, @TypeID int, @ShipperID int, @CarrierID int, @ProductGroupID int, @OriginID int, @DestinationID int, @OriginStateID int, @DestStateID int, @RegionID int, @ProducerID int, @OperatorID int, @BestMatchOnly bit = 0)
RETURNS 
	@ret TABLE (
		ID int
	  , TypeID int
	  , ShipperID int
	  , CarrierID int
	  , ProductGroupID int
	  , OriginID int
	  , DestinationID int
	  , OriginStateID int
	  , DestStateID int
	  , RegionID int
	  , ProducerID int
	  , OperatorID int
	  , Rate decimal(18, 10)
	  , RateTypeID int
	  , UomID int
	  , EffectiveDate date
	  , EndDate date
	  , NextEffectiveDate date
	  , BestMatch bit
	  , Ranking smallmoney
	  , Locked bit
	  , CreateDateUTC datetime
	  , CreatedByUser varchar(100)
	  , LastChangeDateUTC datetime
	  , LastChangedByUser varchar(100)
	)
AS BEGIN
	-- get the raw data (all matched rows)
	DECLARE @src TABLE (ID int, TypeID int, Ranking smallmoney)
	INSERT INTO @src (ID, TypeID, Ranking)
		SELECT ID, TypeID
			, Ranking =	dbo.fnRateRanking(@ShipperID, R.ShipperID, 512, 0)
					  + dbo.fnRateRanking(@CarrierID, R.CarrierID, 256, 0)
					  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 128, 0)
					  + dbo.fnRateRanking(@OriginID, R.OriginID, 64, 0)
					  + dbo.fnRateRanking(@DestinationID, R.DestinationID, 32, 0)
					  + dbo.fnRateRanking(@OriginStateID, R.OriginStateID, 16, 0)
					  + dbo.fnRateRanking(@DestStateID, R.DestStateID, 8, 0)
					  + dbo.fnRateRanking(@RegionID, R.RegionID, 4, 0)
					  + dbo.fnRateRanking(@ProducerID, R.ProducerID, 2, 1)
					  + dbo.fnRateRanking(@OperatorID, R.OperatorID, 1, 1)
		FROM dbo.viewCarrierAssessorialRate R
		WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(R.ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@OriginID, 0), R.OriginID, 0) = coalesce(R.OriginID, nullif(@OriginID, 0), 0)
		  AND coalesce(nullif(@DestinationID, 0), R.DestinationID, 0) = coalesce(R.DestinationID, nullif(@DestinationID, 0), 0)
		  AND coalesce(nullif(@OriginStateID, 0), R.OriginStateID, 0) = coalesce(R.OriginStateID, nullif(@OriginStateID, 0), 0)
		  AND coalesce(nullif(@DestStateID, 0), R.DestStateID, 0) = coalesce(R.DestStateID, nullif(@DestStateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(R.RegionID, nullif(@RegionID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(R.ProducerID, nullif(@ProducerID, 0), 0)
		  AND coalesce(nullif(@OperatorID, 0), R.OperatorID, 0) = coalesce(R.OperatorID, nullif(@OperatorID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
		  AND (nullif(@TypeID, 0) IS NULL OR TypeID = @TypeID)

	-- return the rate data for the best-match rates 
	INSERT INTO @ret (ID, TypeID, ShipperID, CarrierID, ProductGroupID, OriginID, DestinationID, OriginStateID, DestStateID, RegionID, ProducerID, OperatorID, Rate, RateTypeID, UomID, EffectiveDate, EndDate, NextEffectiveDate, BestMatch, Ranking, Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser)
		SELECT CAR.ID, TypeID, ShipperID, CarrierID, ProductGroupID, OriginID, DestinationID, OriginStateID, DestStateID, RegionID, ProducerID, OperatorID, Rate, RateTypeID, UomID, EffectiveDate, EndDate, NextEffectiveDate, BestMatch, Ranking, Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser
		FROM viewCarrierAssessorialRate CAR
		JOIN (
			SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.TypeID IS NULL THEN 0 ELSE 1 END as bit)
			FROM @src S
			LEFT JOIN (
				SELECT TypeID, Ranking = MAX(Ranking)
				FROM @src
				WHERE @StartDate = ISNULL(@EndDate, @StartDate)
				  AND Ranking % 1 = 0.01 * 10  -- ensure some type of match occurred on all 10 criteria choices
				GROUP BY TypeID
			) X ON X.TypeID = S.TypeID AND X.Ranking = S.Ranking
		) X ON X.ID = CAR.ID
		WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
		
	RETURN
END
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier AssessorialRate rows for the specified criteria
/***********************************/
ALTER FUNCTION fnCarrierAssessorialRatesDisplay(@StartDate date, @EndDate date, @TypeID int, @ShipperID int, @CarrierID int, @ProductGroupID int, @OriginID int, @DestinationID int, @OriginStateID int, @DestStateID int, @RegionID int, @ProducerID int, @OperatorID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.OriginID, R.DestinationID, R.OriginStateID, R.DestStateID, R.RegionID, R.ProducerID, R.OperatorID, R.TypeID, R.Rate, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate, R.NextEffectiveDate
		, Type = RT.Name
		, Shipper = S.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, Origin = O.Name
		, OriginFull = O.FullName
		, Destination = D.Name
		, DestinationFull = D.FullName
		, OriginState = OS.FullName
		, OriginStateAbbrev = OS.Abbreviation
		, DestState = DS.FullName
		, DestStateAbbrev = DS.Abbreviation
		, Region = RE.Name
		, RateType = RT.Name
		, Uom = U.Name
		, UomShort = U.Abbrev
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch, Ranking
	FROM dbo.fnCarrierAssessorialRates(@StartDate, @EndDate, @TypeID, @ShipperID, @CarrierID, @ProductGroupID, @OriginID, @DestinationID, @OriginStateID, @DestStateID, @RegionID, @ProducerID, @OperatorID, 0) R
	JOIN tblAssessorialRateType RT ON RT.ID = R.TypeID
	LEFT JOIN tblCustomer S ON S.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN viewOrigin O ON O.ID = R.OriginID
	LEFT JOIN viewDestination D ON D.ID = R.DestinationID
	LEFT JOIN tblState OS ON OS.ID = R.OriginStateID
	LEFT JOIN tblState DS ON DS.ID = R.DestStateID
	LEFT JOIN tblRegion RE ON RE.ID = R.RegionID
	LEFT JOIN tblUom U ON U.ID = R.UomID
	ORDER BY EffectiveDate
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier AssessorialRate info for the specified order
/***********************************/
ALTER FUNCTION fnOrderCarrierAssessorialRates(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, TypeID, Rate, RateTypeID, UomID
	FROM dbo.viewOrder O
	CROSS APPLY dbo.fnCarrierAssessorialRates(O.OrderDate, null, null, O.CustomerID, O.CarrierID, O.ProductGroupID, O.OriginID, O.DestinationID, O.OriginStateID, O.DestStateID, O.OriginRegionID, O.ProducerID, O.OperatorID, 1) R
	WHERE O.ID = @ID
	  AND ((R.TypeID = 1 AND O.ChainUp = 1)
		OR (R.TypeID = 2 AND O.RerouteCount > 0)
		OR (R.TypeID = 3 AND O.TicketCount > 1)
		OR (R.TypeID = 4 AND O.H2S = 1)
		OR R.TypeID > 4)
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier AssessorialRate Amounts info for the specified order
/***********************************/
ALTER FUNCTION fnOrderCarrierAssessorialAmounts(@ID int, @Units decimal(18, 10), @UomID int) RETURNS TABLE AS RETURN
	SELECT AssessorialRateTypeID = TypeID, RateID, RateTypeID, Amount
	FROM (
		SELECT TypeID, RateID = ID, RateTypeID
			, Amount = dbo.fnRateToAmount(RateTypeID, @Units, @UomID, Rate, UomID, OSC.LoadAmount
			, CASE WHEN RateTypeID = 3 THEN (SELECT TOP 1 Amount FROM tblOrderSettlementCarrierAssessorialCharge WHERE OrderID = @ID AND AssessorialRateTypeID = TypeID) 
				   ELSE NULL END)
		FROM dbo.fnOrderCarrierAssessorialRates(@ID)
		JOIN tblOrderSettlementCarrier OSC ON OSC.OrderID = @ID
	) X
	WHERE Amount IS NOT NULL
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RateSheetRangeRate info for the specified order
/***********************************/
ALTER FUNCTION fnOrderCarrierLoadAmount(@ID int, @Units decimal(18, 10), @UomID int) RETURNS TABLE AS RETURN
	SELECT TOP 1 RouteRateID, RangeRateID
		, Amount = dbo.fnRateToAmount(RateTypeID, @Units, @UomID, Rate, UomID, NULL
			, CASE WHEN RateTypeID = 3 THEN (SELECT TOP 1 LoadAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID) ELSE NULL END)
	FROM (
		SELECT SortID = 1, RouteRateID = ID, RangeRateID = NULL, RateTypeID, UomID, Rate FROM dbo.fnOrderCarrierRouteRate(@ID)
		UNION SELECT 2, NULL, ID, RateTypeID, UomID, Rate FROM dbo.fnOrderCarrierRateSheetRangeRate(@ID)
	) X
	ORDER BY SortID
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OrderReject data info for the specified order
/***********************************/
ALTER FUNCTION fnOrderCarrierOrderRejectData(@ID int, @Units decimal(18, 10), @UomID int) RETURNS TABLE AS RETURN
	SELECT RateID = OWR.ID
		, Amount = dbo.fnRateToAmount(RateTypeID, @Units, @UomID, Rate, UomID, NULL
			, CASE WHEN RateTypeID = 3 THEN (SELECT TOP 1 OrderRejectAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID) ELSE NULL END)
	FROM dbo.fnOrderCarrierOrderRejectRate(@ID) OWR 
	JOIN tblOrder O ON O.ID = @ID
	WHERE O.Rejected = 1
GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Carrier "Settlement" $$ values to an Delivered/Audited order
/***********************************/
ALTER PROCEDURE spApplyRatesCarrier
(
  @ID int
, @UserName varchar(100)
, @OriginWaitAmount money = NULL
, @DestWaitAmount money = NULL
, @RejectionAmount money = NULL
, @FuelSurchargeAmount money = NULL
, @LoadAmount money = NULL
, @AssessorialRateTypeID_CSV varchar(max) = NULL
, @AssessorialAmount_CSV varchar(max) = null
, @ResetOverrides bit = 0
) AS BEGIN

	SET NOCOUNT ON
	
	-- ensure this order hasn't yet been fully settled
	IF EXISTS (SELECT * FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND BatchID IS NOT NULL)
	BEGIN
		RAISERROR('Invoice has already been fully settled', 16, 1)
		RETURN
	END

	-- ensure that Shipper Rates have been applied prior to applying Carrier rates (since they could be dependent on Shipper rates)
	IF NOT EXISTS (SELECT * FROM tblOrderSettlementShipper WHERE OrderID = @ID)
		EXEC spApplyRatesShipper @ID, @UserName

	/*
drop table #i
drop table #ia

declare @id int, @userName varchar(100)
, @OriginWaitAmount money 
, @DestWaitAmount money 
, @RejectionAmount money 
, @FuelSurchargeAmount money 
, @LoadAmount money 
, @AssessorialRateTypeID_CSV varchar(max) 
, @AssessorialAmount_CSV varchar(max) 
, @ResetOverrides bit 
select @ID=66969,@UserName='kalons'
--select @OriginWaitAmount=1.0000,@DestWaitAmount=2.0000,@RejectionAmount=3.0000,@FuelSurchargeAmount=4.0000,@LoadAmount=5.0000,@AssessorialRateTypeID_CSV='1',@AssessorialAmount_CSV='99'
select @ResetOverrides=0
--	*/
	-- get the provided manual Assessorial Rates (if any) into a usable able
	DECLARE @ManualAssessorialRates TABLE (ID int primary key, Amount money)
	INSERT INTO @ManualAssessorialRates (ID, Amount) 
		SELECT ID.ID, R.Amount
		FROM (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), ID FROM dbo.fnSplitCSVIDs(@AssessorialRateTypeID_CSV)) ID
		JOIN (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), amount = CAST(value as money) FROM dbo.fnSplitCSV(@AssessorialAmount_CSV)) R ON R.sortid = ID.sortid

	-- add in an existing manual Assessorial Rates from the existing Order Settlement (if any)
	INSERT INTO @ManualAssessorialRates (ID, Amount)
		SELECT AssessorialRateTypeID, Amount 
		FROM dbo.tblOrderSettlementCarrierAssessorialCharge 
		WHERE OrderID = @ID AND AssessorialRateID IS NULL 
			AND ID NOT IN (SELECT ID FROM @ManualAssessorialRates)
			AND @ResetOverrides = 0
--select * from @ManualAssessorialRates

	IF (@ResetOverrides = 0)
	BEGIN 
		SELECT @OriginWaitAmount = OriginWaitAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @OriginWaitAmount IS NULL AND OriginWaitRateID IS NULL
		SELECT @DestWaitAmount = DestinationWaitAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @DestWaitAmount IS NULL AND DestinationWaitRateID IS NULL
		SELECT @RejectionAmount = OrderRejectAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @RejectionAmount IS NULL AND OrderRejectRateID IS NULL
		SELECT @FuelSurchargeAmount = FuelSurchargeAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @FuelSurchargeAmount IS NULL AND FuelSurchargeRateID IS NULL
		SELECT @LoadAmount = LoadAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @LoadAmount IS NULL AND RouteRateID IS NULL AND RangeRateID IS NULL
	END
--select OverrideOriginWaitAmount = @OriginWaitAmount, OverrideDestWaitAmount = @DestWaitAmount, OverrideRejectionAmount = @RejectionAmount, OverrideFuelSurchargeAmount = @FuelSurchargeAmount, OverrideLoadAmount = @LoadAmount

	DECLARE @SettlementUnits TABLE
	(
		OrderID int
	  , SettlementUomID int
	  , SettlementFactorID int
	  , MinSettlementUnits decimal(18, 10)
	  , ActualUnits decimal(18, 10)
	  , SettlementUnits decimal(18, 10)
	)
	INSERT INTO @SettlementUnits (OrderID, SettlementUomID, SettlementFactorID, MinSettlementUnits, ActualUnits, SettlementUnits)
		SELECT OrderID, SettlementUomID, SettlementFactorID, MinSettlementUnits, ActualUnits, SettlementUnits
		FROM dbo.viewOrderSettlementUnitsCarrier 
		WHERE OrderID = @ID

	SELECT OrderID = @ID
		, OrderDate
		, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
		, RouteRateID, RangeRateID, LoadAmount
		, WaitFeeParameterID
		, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount 
		, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount
		, OrderRejectRateID, OrderRejectAmount
		, FuelSurchargeRateID, FuelSurchargeRate, FuelSurchargeAmount
		, OriginTaxRate
		, TotalAmount = ISNULL(LoadAmount, 0) 
			+ ISNULL(OriginWaitAmount, 0) 
			+ ISNULL(DestWaitAmount, 0) 
			+ ISNULL(OrderRejectAmount, 0) 
			+ ISNULL(FuelSurchargeAmount, 0)
		, CreatedByUser = @UserName
	INTO #I
	FROM (
		SELECT OrderDate
			, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
			, RouteRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RouteRateID ELSE NULL END
			, RangeRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RangeRateID ELSE NULL END
			, LoadAmount = ISNULL(@LoadAmount, RR.Amount)
			, WaitFeeParameterID
			, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount
			, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount
			, OrderRejectRateID = CASE WHEN @RejectionAmount IS NULL THEN RD.RateID ELSE NULL END
			, OrderRejectAmount = isnull(@RejectionAmount, RD.Amount)
			, FuelSurchargeRateID = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.RateID ELSE NULL END
			, FuelSurchargeRate = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.Rate ELSE NULL END
			, FuelSurchargeAmount = isnull(@FuelSurchargeAmount, FSR.Amount)
			, OriginTaxRate
		FROM (
			SELECT O.ID
				, O.OriginID
				, O.CarrierID
				, O.RouteID
				, SU.SettlementFactorID
				, SU.SettlementUomID
				, SU.ActualUnits
				, SU.MinSettlementUnits
				, SU.SettlementUnits
				, RouteMiles = O.ActualMiles
				, O.OrderDate
				, O.ChainUp
				, O.H2S
				, O.Rejected
				, O.RerouteCount
				, IsSplit = CASE WHEN O.Rejected = 0 AND O.TicketCount > 1 THEN 1 ELSE 0 END
				, OWD.WaitFeeParameterID
				, OriginWaitRateID = CASE WHEN @OriginWaitAmount IS NULL THEN OWD.RateID ELSE NULL END
				, OriginWaitBillableMinutes = OWD.BillableMinutes
				, OriginWaitBillableHours = OWD.BillableHours
				, OriginWaitAmount = isnull(@OriginWaitAmount, OWD.Amount)
				, DestWaitRateID = CASE WHEN @DestWaitAmount IS NULL THEN DWD.RateID ELSE NULL END
				, DestWaitBillableMinutes = DWD.BillableMinutes
				, DestWaitBillableHours = DWD.BillableHours
				, DestWaitAmount = isnull(@DestWaitAmount, DWD.Amount)
				, OriginTaxRate = OO.TaxRate
			FROM dbo.viewOrder O
			JOIN @SettlementUnits SU ON SU.OrderID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			OUTER APPLY dbo.fnOrderCarrierOriginWaitData(@ID) OWD 
			OUTER APPLY dbo.fnOrderCarrierDestinationWaitData(@ID) DWD 
			WHERE O.ID = @ID
		) X
		OUTER APPLY dbo.fnOrderCarrierLoadAmount(@ID, SettlementUnits, SettlementUomID) RR 
		OUTER APPLY dbo.fnOrderCarrierOrderRejectData(@ID, SettlementUnits, SettlementUomID) RD
		OUTER APPLY dbo.fnOrderCarrierFuelSurchargeData(@ID) FSR
	) X2
--select * from #I

	SELECT OrderID = @ID, AssessorialRateTypeID, AssessorialRateID = RateID, Amount, CreatedByUser = @UserName 
	INTO #IA
	FROM @SettlementUnits SU
	CROSS APPLY dbo.fnOrderCarrierAssessorialAmounts(@ID, SU.SettlementUnits, SU.SettlementUomID) CAA
	WHERE CAA.AssessorialRateTypeID NOT IN (SELECT ID FROM @ManualAssessorialRates)
/*
select OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser from #IA
union
select @ID, ID, NULL, Amount, @UserName from @ManualAssessorialRates
--*/
	DECLARE @CreatedTran bit; SET @CreatedTran = CASE WHEN @@TRANCOUNT = 0 THEN 1 ELSE 0 END
	BEGIN TRY
		IF (@CreatedTran = 1)
			BEGIN TRAN

		-- remove the existing settlment record (if any)
		DELETE FROM tblOrderSettlementCarrierAssessorialCharge WHERE OrderID = @ID
		DELETE FROM tblOrderSettlementCarrier WHERE OrderID = @ID
	
		-- all Units and Rates are first normalized to the Order.OriginUOM then consistent processing
		-- and persisted in the Invoice (Settlement) record in this Order.OriginUOM 
		INSERT INTO tblOrderSettlementCarrier (
			OrderID
			, OrderDate
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestinationWaitRateID 
			, DestinationWaitBillableMinutes 
			, DestinationWaitBillableHours
			, DestinationWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser)
		SELECT OrderID
			, OrderDate
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestWaitRateID 
			, DestWaitBillableMinutes 
			, DestWaitBillableHours
			, DestWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser
		FROM #I

		INSERT INTO tblOrderSettlementCarrierAssessorialCharge (OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser)
			SELECT OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser FROM #IA
			UNION
			SELECT @ID, ID, NULL, Amount, @UserName FROM @ManualAssessorialRates
	
		IF (@CreatedTran = 1)
			COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @msg varchar(max), @severity int
		SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY()
		ROLLBACK
		RAISERROR(@msg, @severity, 1)
		RETURN
	END CATCH	
	
END
GO

-----------------------------------------------
-- SHIPPER CHANGS
-----------------------------------------------
GO 
CREATE TABLE tblShipperFuelSurchargeRate
(
	ID int IDENTITY(1,1) NOT NULL CONSTRAINT PK_ShipperFuelSurchargeRate PRIMARY KEY NONCLUSTERED
	, ShipperID int NULL CONSTRAINT FK_ShipperFuelSurcharge_Shipper FOREIGN KEY REFERENCES tblCustomer(ID)
	, ProductGroupID int NULL CONSTRAINT FK_ShipperFuelSurcharge_ProductGroup FOREIGN KEY REFERENCES tblProductGroup(ID)
	, FuelPriceFloor money NOT NULL
	, IntervalAmount money NOT NULL
	, IncrementAmount money NOT NULL
	, EffectiveDate date NOT NULL
	, EndDate date NOT NULL
	, CreateDateUTC smalldatetime NOT NULL CONSTRAINT DF_ShipperFuelSurcharge_CreateDateUTC DEFAULT (getutcdate())
	, CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_ShipperFuelSurcharge_CreatedBy DEFAULT (SUSER_NAME())
	, LastChangeDateUTC smalldatetime NULL
	, LastChangedByUser varchar(100) NULL
	, CONSTRAINT [CK_ShipperFuelSurchargeRate_EndDate_Greater] CHECK  (([EndDate]>=[EffectiveDate]))
)
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblShipperFuelSurchargeRate TO dispatchcrude_iis_acct
GO
CREATE UNIQUE CLUSTERED INDEX udxShipperFuelSurchargeRate_Main ON tblShipperFuelSurchargeRate
(
	ShipperID
	, ProductGroupID
	, EffectiveDate
)
GO
SELECT TOP 1 ShipperID = NULL, FuelPriceFloor, IntervalAmount = min(IntervalAmount), IncrementAmount = min(IncrementAmount) INTO #temp FROM tblShipperFuelSurchargeRates GROUP BY FuelPriceFloor ORDER BY COUNT(*) DESC
INSERT INTO tblShipperFuelSurchargeRate (ShipperID, FuelPriceFloor, IntervalAmount, IncrementAmount, EffectiveDate, EndDate)
	SELECT ShipperID, FuelPriceFloor, IntervalAmount, IncrementAmount, '1/1/2014', '12/31/2014' FROM #temp
	UNION 
	SELECT R.ShipperID, R.FuelPriceFloor, R.IntervalAmount, R.IncrementAmount, '1/1/2014', '12/31/2014' 
	FROM tblShipperFuelSurchargeRates R 
	LEFT JOIN #temp t ON t.FuelPriceFloor = R.FuelPriceFloor 
	WHERE t.FuelPriceFloor IS NULL
	UNION
	SELECT ShipperID, FuelPriceFloor, IntervalAmount, IncrementAmount, '1/1/2015', '12/31/2015' FROM #temp
	UNION 
	SELECT R.ShipperID, R.FuelPriceFloor, R.IntervalAmount, R.IncrementAmount, '1/1/2015', '12/31/2015' 
	FROM tblShipperFuelSurchargeRates R 
	LEFT JOIN #temp t ON t.FuelPriceFloor = R.FuelPriceFloor 
	WHERE t.FuelPriceFloor IS NULL
GO
DROP TABLE #temp
GO

IF EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'FK_ShipperFuelSurchargeRates_Customer') AND parent_object_id = OBJECT_ID(N'tblShipperFuelSurchargeRates'))
	ALTER TABLE tblShipperFuelSurchargeRates DROP CONSTRAINT FK_ShipperFuelSurchargeRates_Customer
GO
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'DF_ShipperFuelSurchargeRates_CreateDateUTC') AND type = 'D')
BEGIN
	ALTER TABLE tblShipperFuelSurchargeRates DROP CONSTRAINT DF_ShipperFuelSurchargeRates_CreateDateUTC
END
GO
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'DF_ShipperFuelSurchargeRates_CreatedByUser') AND type = 'D')
BEGIN
	ALTER TABLE tblShipperFuelSurchargeRates DROP CONSTRAINT DF_ShipperFuelSurchargeRates_CreatedByUser
END
GO
IF EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'FK_OrderSettlementShipper_FuelSurchargeRate') AND parent_object_id = OBJECT_ID(N'tblOrderSettlementShipper'))
	ALTER TABLE tblOrderSettlementShipper DROP CONSTRAINT FK_OrderSettlementShipper_FuelSurchargeRate
GO 
UPDATE tblOrderSettlementShipper
	SET FuelSurchargeRateID = R.ID
FROM tblOrderSettlementShipper S
JOIN tblShipperFuelSurchargeRates O ON O.ID = S.FuelSurchargeRateID
LEFT JOIN tblShipperFuelSurchargeRate R ON (R.ShipperID IS NULL AND R.FuelPriceFloor = O.FuelPriceFloor) OR (R.ShipperID = O.ShipperID AND R.FuelPriceFloor = O.FuelPriceFloor)
GO

ALTER TABLE tblOrderSettlementShipper ADD CONSTRAINT FK_OrderSettlementShipper_FuelSurchargeRate FOREIGN KEY (FuelSurchargeRateID) REFERENCES tblShipperFuelSurchargeRate(ID)
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'tblShipperFuelSurchargeRates') AND type in (N'U'))
	DROP TABLE tblShipperFuelSurchargeRates
GO

/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all ShipperFuelSurcharge records
******************************************************/
CREATE VIEW viewShipperFuelSurchargeRate AS
	SELECT X.*
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementShipper WHERE FuelSurchargeRateID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate)
			FROM tblShipperDestinationWaitRate XN 
			WHERE isnull(XN.ShipperID, 0) = isnull(X.ShipperID, 0) 
			  AND isnull(XN.ProductGroupID, 0) = isnull(X.ProductGroupID, 0) 
			  AND XN.EffectiveDate > X.EffectiveDate)
	FROM tblShipperFuelSurchargeRate X
GO
GRANT SELECT ON viewShipperFuelSurchargeRate TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'fnShipperFuelSurchargeRate') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION fnShipperFuelSurchargeRate
GO
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper DestinationWaitRate info for the specified criteria
/***********************************/
CREATE FUNCTION fnShipperFuelSurchargeRate
(
  @StartDate date
, @EndDate date
, @ShipperID int
, @ProductGroupID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID
			, Ranking =	dbo.fnRateRanking(@ShipperID, R.ShipperID, 2, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 1, 0)
		FROM  dbo.viewShipperFuelSurchargeRate R
		WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	)
	SELECT R.ID, ShipperID, ProductGroupID, FuelPriceFloor, IntervalAmount, IncrementAmount, EffectiveDate, EndDate, NextEffectiveDate
	  , BestMatch, Ranking
	  , Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewShipperFuelSurchargeRate R
	JOIN (
		SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 2  -- ensure some type of match occurred on all 6 criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)
GO
GRANT SELECT ON fnShipperFuelSurchargeRate TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper RouteRate rows for the specified criteria
/***********************************/
CREATE FUNCTION fnShipperFuelSurchargeRateDisplay(@StartDate date, @EndDate date, @ShipperID int, @ProductGroupID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ShipperID, R.ProductGroupID, R.FuelPriceFloor, R.IntervalAmount, R.IncrementAmount, R.EffectiveDate, R.EndDate, R.NextEffectiveDate
		, Shipper = C.Name
		, ProductGroup = PG.Name
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch, Ranking
	FROM dbo.fnShipperFuelSurchargeRate(@StartDate, @EndDate, @ShipperID, @ProductGroupID, 0) R
	LEFT JOIN tblCustomer C ON C.ID = R.ShipperID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	ORDER BY EffectiveDate
GO
GRANT SELECT ON fnShipperFuelSurchargeRate TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper FuelSurcharge info for the specified order
/***********************************/
CREATE FUNCTION fnOrderShipperFuelSurchargeRate(@ID int) RETURNS TABLE AS RETURN
	SELECT RateID
		, Rate = IncrementAmount * (round(Pricediff / nullif(IntervalAmount, 0), 0) + CASE WHEN PriceDiff % IntervalAmount > 0 THEN 1 ELSE 0 END)
		, RouteMiles
	FROM (
		SELECT TOP 1 RateID = ID, FuelPriceFloor, IntervalAmount, IncrementAmount
			, PriceDiff = dbo.fnMaxDecimal(0, dbo.fnFuelPriceForOrigin(OriginID, OrderDate) - fuelPriceFloor)
			, RouteMiles
		FROM (
			SELECT R.ID, R.FuelPriceFloor, R.IntervalAmount, R.IncrementAmount, O.OrderDate, O.OriginID, RouteMiles = O.ActualMiles
			FROM dbo.viewOrder O
			CROSS APPLY dbo.fnShipperFuelSurchargeRate(O.OrderDate, null, O.CustomerID, O.ProductGroupID, 1) R
			WHERE O.ID = @ID 
		) X
	) X2
GO
GRANT SELECT ON fnOrderShipperFuelSurchargeRate TO dispatchcrude_iis_acct
GO 

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper FuelSurcharge data info for the specified order
/***********************************/
CREATE FUNCTION fnOrderShipperFuelSurchargeData(@ID int) RETURNS TABLE AS RETURN
	SELECT RateID, Rate, Amount = Rate * R.RouteMiles
	FROM dbo.fnOrderShipperFuelSurchargeRate(@ID) R
	WHERE Rate > 0
GO
GRANT SELECT ON fnOrderShipperFuelSurchargeData TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper AssessorialRate Amounts info for the specified order
/***********************************/
ALTER FUNCTION [dbo].[fnOrderShipperAssessorialAmounts](@ID int, @Units decimal(18, 10), @UomID int) RETURNS TABLE AS RETURN
	SELECT AssessorialRateTypeID = TypeID, RateID, RateTypeID, Amount
	FROM (
		SELECT TypeID, RateID = ID, RateTypeID
			, Amount = dbo.fnRateToAmount(RateTypeID, @Units, @UomID, Rate, UomID, OSC.LoadAmount
			, CASE WHEN RateTypeID = 3 THEN (SELECT TOP 1 Amount FROM tblOrderSettlementShipperAssessorialCharge WHERE OrderID = @ID AND AssessorialRateTypeID = TypeID) 
				   ELSE NULL END)
		FROM dbo.fnOrderShipperAssessorialRates(@ID)
		JOIN tblOrderSettlementShipper OSC ON OSC.OrderID = @ID
	) X
	WHERE Amount IS NOT NULL
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper RateSheetRangeRate info for the specified order
/***********************************/
ALTER FUNCTION fnOrderShipperLoadAmount(@ID int, @Units decimal(18, 10), @UomID int) RETURNS TABLE AS RETURN
	SELECT TOP 1 RouteRateID, RangeRateID
		, Amount = dbo.fnRateToAmount(RateTypeID, @Units, @UomID, Rate, UomID, NULL
			, CASE WHEN RateTypeID = 3 THEN (SELECT TOP 1 LoadAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID) ELSE NULL END)
	FROM (
		SELECT SortID = 1, RouteRateID = ID, RangeRateID = NULL, RateTypeID, UomID, Rate FROM dbo.fnOrderShipperRouteRate(@ID)
		UNION SELECT 2, NULL, ID, RateTypeID, UomID, Rate FROM dbo.fnOrderShipperRateSheetRangeRate(@ID)
	) X
	ORDER BY SortID
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper OrderReject data info for the specified order
/***********************************/
ALTER FUNCTION fnOrderShipperOrderRejectData(@ID int, @Units decimal(18, 10), @UomID int) RETURNS TABLE AS RETURN
	SELECT RateID = OWR.ID
		, Amount = dbo.fnRateToAmount(RateTypeID, @Units, @UomID, Rate, UomID, NULL
			, CASE WHEN RateTypeID = 3 THEN (SELECT TOP 1 OrderRejectAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID) ELSE NULL END)
	FROM dbo.fnOrderShipperOrderRejectRate(@ID) OWR 
	JOIN tblOrder O ON O.ID = @ID
	WHERE O.Rejected = 1
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper AssessorialRate Amounts info for the specified order
/***********************************/
ALTER FUNCTION [dbo].[fnOrderShipperAssessorialAmounts](@ID int, @Units decimal(18, 10), @UomID int) RETURNS TABLE AS RETURN
	SELECT AssessorialRateTypeID = TypeID, RateID, RateTypeID, Amount
	FROM (
		SELECT TypeID, RateID = ID, RateTypeID
			, Amount = dbo.fnRateToAmount(RateTypeID, @Units, @UomID, Rate, UomID, OSS.LoadAmount
			, CASE WHEN RateTypeID = 3 THEN (SELECT TOP 1 Amount FROM tblOrderSettlementShipperAssessorialCharge WHERE OrderID = @ID AND AssessorialRateTypeID = TypeID) 
				   ELSE NULL END)
		FROM dbo.fnOrderShipperAssessorialRates(@ID)
		JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = @ID
	) X
	WHERE Amount IS NOT NULL
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper RateSheetRangeRate info for the specified order
/***********************************/
ALTER FUNCTION [dbo].[fnOrderShipperLoadAmount](@ID int, @Units decimal(18, 10), @UomID int) RETURNS TABLE AS RETURN
	SELECT TOP 1 RouteRateID, RangeRateID
		, Amount = dbo.fnRateToAmount(RateTypeID, @Units, @UomID, Rate, UomID, NULL
			, CASE WHEN RateTypeID = 3 THEN (SELECT TOP 1 LoadAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID) ELSE NULL END)
	FROM (
		SELECT SortID = 1, RouteRateID = ID, RangeRateID = NULL, RateTypeID, UomID, Rate FROM dbo.fnOrderShipperRouteRate(@ID)
		UNION SELECT 2, NULL, ID, RateTypeID, UomID, Rate FROM dbo.fnOrderShipperRateSheetRangeRate(@ID)
	) X
	ORDER BY SortID
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper OrderReject data info for the specified order
/***********************************/
ALTER FUNCTION [dbo].[fnOrderShipperOrderRejectData](@ID int, @Units decimal(18, 10), @UomID int) RETURNS TABLE AS RETURN
	SELECT RateID = OWR.ID
		, Amount = dbo.fnRateToAmount(RateTypeID, @Units, @UomID, Rate, UomID, NULL, NULL)
	FROM dbo.fnOrderShipperOrderRejectRate(@ID) OWR 
	JOIN tblOrder O ON O.ID = @ID
	WHERE O.Rejected = 1
GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Shipper "Settlement" $$ values to an Delivered/Audited order
/***********************************/
ALTER PROCEDURE [dbo].[spApplyRatesShipper]
(
  @ID int
, @UserName varchar(100)
, @OriginWaitAmount money = NULL
, @DestWaitAmount money = NULL
, @RejectionAmount money = NULL
, @FuelSurchargeAmount money = NULL
, @LoadAmount money = NULL
, @AssessorialRateTypeID_CSV varchar(max) = NULL
, @AssessorialAmount_CSV varchar(max) = null
, @ResetOverrides bit = 0
) AS BEGIN

	SET NOCOUNT ON
	
	-- ensure this order hasn't yet been fully settled
	IF EXISTS (SELECT * FROM tblOrderSettlementShipper WHERE OrderID = @ID AND BatchID IS NOT NULL)
	BEGIN
		RAISERROR('Invoice has already been fully settled', 16, 1)
		RETURN
	END

	-- get the provided manual Assessorial Rates (if any) into a usable able
	DECLARE @ManualAssessorialRates TABLE (ID int primary key, Amount money)
	INSERT INTO @ManualAssessorialRates (ID, Amount) 
		SELECT ID.ID, R.Amount
		FROM (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), ID FROM dbo.fnSplitCSVIDs(@AssessorialRateTypeID_CSV)) ID
		JOIN (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), amount = CAST(value as money) FROM dbo.fnSplitCSV(@AssessorialAmount_CSV)) R ON R.sortid = ID.sortid

	-- add in an existing manual Assessorial Rates from the existing Order Settlement (if any)
	INSERT INTO @ManualAssessorialRates (ID, Amount)
		SELECT AssessorialRateTypeID, Amount 
		FROM dbo.tblOrderSettlementShipperAssessorialCharge 
		WHERE OrderID = @ID AND AssessorialRateID IS NULL 
			AND ID NOT IN (SELECT ID FROM @ManualAssessorialRates)
			AND @ResetOverrides = 0

	IF (@ResetOverrides = 0)
	BEGIN 
		SELECT @OriginWaitAmount = OriginWaitAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @OriginWaitAmount IS NULL AND OriginWaitRateID IS NULL
		SELECT @DestWaitAmount = DestinationWaitAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @DestWaitAmount IS NULL AND DestinationWaitRateID IS NULL
		SELECT @RejectionAmount = OrderRejectAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @RejectionAmount IS NULL AND OrderRejectRateID IS NULL
		SELECT @FuelSurchargeAmount = FuelSurchargeAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @FuelSurchargeAmount IS NULL AND FuelSurchargeRateID IS NULL
		SELECT @LoadAmount = LoadAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @LoadAmount IS NULL AND RouteRateID IS NULL AND RangeRateID IS NULL
	END

	DECLARE @SettlementUnits TABLE
	(
		OrderID int
	  , SettlementUomID int
	  , SettlementFactorID int
	  , MinSettlementUnits decimal(18, 10)
	  , ActualUnits decimal(18, 10)
	  , SettlementUnits decimal(18, 10)
	)
	INSERT INTO @SettlementUnits (OrderID, SettlementUomID, SettlementFactorID, MinSettlementUnits, ActualUnits, SettlementUnits)
		SELECT OrderID, SettlementUomID, SettlementFactorID, MinSettlementUnits, ActualUnits, SettlementUnits
		FROM dbo.viewOrderSettlementUnitsShipper 
		WHERE OrderID = @ID

	SELECT OrderID = @ID
		, OrderDate
		, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
		, RouteRateID, RangeRateID, LoadAmount
		, WaitFeeParameterID
		, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount 
		, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount
		, OrderRejectRateID, OrderRejectAmount
		, FuelSurchargeRateID, FuelSurchargeRate, FuelSurchargeAmount
		, OriginTaxRate
		, TotalAmount = ISNULL(LoadAmount, 0) 
			+ ISNULL(OriginWaitAmount, 0) 
			+ ISNULL(DestWaitAmount, 0) 
			+ ISNULL(OrderRejectAmount, 0) 
			+ ISNULL(FuelSurchargeAmount, 0)
		, CreatedByUser = @UserName
	INTO #I
	FROM (
		SELECT OrderDate
			, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
			, RouteRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RouteRateID ELSE NULL END
			, RangeRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RangeRateID ELSE NULL END
			, LoadAmount = ISNULL(@LoadAmount, RR.Amount)
			, WaitFeeParameterID
			, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount
			, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount
			, OrderRejectRateID = CASE WHEN @RejectionAmount IS NULL THEN RD.RateID ELSE NULL END
			, OrderRejectAmount = isnull(@RejectionAmount, RD.Amount)
			, FuelSurchargeRateID = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.RateID ELSE NULL END
			, FuelSurchargeRate = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.Rate ELSE NULL END
			, FuelSurchargeAmount = isnull(@FuelSurchargeAmount, FSR.Amount)
			, OriginTaxRate
		FROM (
			SELECT O.ID
				, O.OriginID
				, O.CustomerID
				, O.RouteID
				, SU.SettlementFactorID
				, SU.SettlementUomID
				, SU.ActualUnits
				, SU.MinSettlementUnits
				, SU.SettlementUnits
				, RouteMiles = O.ActualMiles
				, O.OrderDate
				, O.ChainUp
				, O.H2S
				, O.Rejected
				, O.RerouteCount
				, IsSplit = CASE WHEN O.Rejected = 0 AND O.TicketCount > 1 THEN 1 ELSE 0 END
				, OWD.WaitFeeParameterID
				, OriginWaitRateID = CASE WHEN @OriginWaitAmount IS NULL THEN OWD.RateID ELSE NULL END
				, OriginWaitBillableMinutes = OWD.BillableMinutes
				, OriginWaitBillableHours = OWD.BillableHours
				, OriginWaitAmount = isnull(@OriginWaitAmount, OWD.Amount)
				, DestWaitRateID = CASE WHEN @DestWaitAmount IS NULL THEN DWD.RateID ELSE NULL END
				, DestWaitBillableMinutes = DWD.BillableMinutes
				, DestWaitBillableHours = DWD.BillableHours
				, DestWaitAmount = isnull(@DestWaitAmount, DWD.Amount)
				, OriginTaxRate = OO.TaxRate
			FROM dbo.viewOrder O
			JOIN @SettlementUnits SU ON SU.OrderID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			OUTER APPLY dbo.fnOrderShipperOriginWaitData(@ID) OWD 
			OUTER APPLY dbo.fnOrderShipperDestinationWaitData(@ID) DWD 
			WHERE O.ID = @ID
		) X
		OUTER APPLY dbo.fnOrderShipperLoadAmount(@ID, SettlementUnits, SettlementUomID) RR 
		OUTER APPLY dbo.fnOrderShipperOrderRejectData(@ID, SettlementUnits, SettlementUomID) RD
		OUTER APPLY dbo.fnOrderShipperFuelSurchargeData(@ID) FSR
	) X2

	SELECT OrderID = @ID, AssessorialRateTypeID, AssessorialRateID = RateID, Amount, CreatedByUser = @UserName 
	INTO #IA
	FROM @SettlementUnits SU
	CROSS APPLY dbo.fnOrderShipperAssessorialAmounts(@ID, SU.SettlementUnits, SU.SettlementUomID) CAA
	WHERE CAA.AssessorialRateTypeID NOT IN (SELECT ID FROM @ManualAssessorialRates)

	DECLARE @CreatedTran bit; SET @CreatedTran = CASE WHEN @@TRANCOUNT = 0 THEN 1 ELSE 0 END
	BEGIN TRY
		IF (@CreatedTran = 1)
			BEGIN TRAN

		-- remove the existing settlment record (if any)
		DELETE FROM tblOrderSettlementShipperAssessorialCharge WHERE OrderID = @ID
		DELETE FROM tblOrderSettlementShipper WHERE OrderID = @ID
	
		-- all Units and Rates are first normalized to the Order.OriginUOM then consistent processing
		-- and persisted in the Invoice (Settlement) record in this Order.OriginUOM 
		INSERT INTO tblOrderSettlementShipper (
			OrderID
			, OrderDate
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestinationWaitRateID 
			, DestinationWaitBillableMinutes 
			, DestinationWaitBillableHours
			, DestinationWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser)
		SELECT OrderID
			, OrderDate
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestWaitRateID 
			, DestWaitBillableMinutes 
			, DestWaitBillableHours
			, DestWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser
		FROM #I

		INSERT INTO tblOrderSettlementShipperAssessorialCharge (OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser)
			SELECT OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser FROM #IA
			UNION
			SELECT @ID, ID, NULL, Amount, @UserName FROM @ManualAssessorialRates
	
		IF (@CreatedTran = 1)
			COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @msg varchar(max), @severity int
		SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY()
		ROLLBACK
		RAISERROR(@msg, @severity, 1)
		RETURN
	END CATCH	
	
END
GO

EXEC _spRebuildAllObjects
GO

COMMIT
SET NOEXEC OFF