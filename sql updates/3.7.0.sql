--backup database [dispatchcrude.dev] to disk = 'C:\files\Consulting\cts\dc_backups\dispatchcrude.dev.3.6.25.bak'
--restore database [DispatchCrude.Dev] from disk = 'C:\files\Consulting\cts\dc_backups\dispatchcrude.dev.3.6.25.bak'
--go
-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.6.28'
SELECT  @NewVersion = '3.7.0'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'GaugerApp: "clone" Driver App DB tables/logic for Gauger Role'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[_spDropView]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[_spDropView]
GO

CREATE PROCEDURE [dbo].[_spDropView](@name varchar(255)) AS
BEGIN
	IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(@name))
		EXEC ('DROP VIEW ' + @name)
END
GO

-- create a new "Gauger" role
INSERT INTO aspnet_Roles (ApplicationId, RoleId, RoleName, LoweredRoleName, Description)
	SELECT ApplicationId, NEWID(), 'Gauger', 'gauger', 'Gauger Role' from aspnet_Applications
	WHERE NOT EXISTS (SELECT * FROM aspnet_Roles WHERE LoweredRoleName LIKE 'gauger')
GO

-- create new "GAUGER" order status
INSERT INTO tblOrderStatus (ID, OrderStatus, StatusNum, ActionText, CreateDateUTC, CreatedByUser, NextID, EntrySortNum)
VALUES (-9, 'Gauger', -9, 'Gauger', GETUTCDATE(), 'System', -10, NULL)
GO

CREATE TABLE tblGauger
(
	ID int IDENTITY(1,1) NOT NULL CONSTRAINT PK_Gauger PRIMARY KEY,
	FirstName varchar(20) NOT NULL,
	LastName varchar(20) NOT NULL,
	IDNumber varchar(20) NOT NULL,
	MobilePhone varchar(20) NULL,
	MobileProvider varchar(30) NULL,
	Email varchar(75) NULL,
	CreateDateUTC smalldatetime NULL,
	CreatedByUser varchar(100) NULL,
	LastChangeDateUTC smalldatetime NULL,
	LastChangedByUser varchar(100) NULL,
	DeleteDateUTC smalldatetime NULL,
	DeletedByUser varchar(100) NULL,
	RegionID int NULL CONSTRAINT FK_Gauger_Region FOREIGN KEY REFERENCES tblRegion(ID),
	HomeStateID int NULL CONSTRAINT FK_Gauger_HomeState FOREIGN KEY REFERENCES tblState(ID),
	MobilePrint bit NOT NULL,
	MobileApp bit NOT NULL,
)
GO
GRANT INSERT, SELECT, DELETE, UPDATE ON tblGauger TO dispatchcrude_iis_acct
GO

CREATE TABLE tblGaugerSync 
(
	GaugerID int NOT NULL CONSTRAINT FK_GaugerSync_Gauger FOREIGN KEY REFERENCES tblGauger(ID),
	LastSyncUTC datetime NULL,
	PasswordHash varchar(25) NULL,
	AppVersion varchar(25) NULL,
	CONSTRAINT PK_GaugerSync PRIMARY KEY CLUSTERED 
	(
		GaugerID ASC
	)
)
GO
GRANT INSERT, SELECT, DELETE, UPDATE ON tblGaugerSync TO dispatchcrude_iis_acct
GO

CREATE TABLE dbo.tblGaugerSyncLog
(
	ID int IDENTITY(1,1) NOT NULL CONSTRAINT PK_GaugerSyncLog PRIMARY KEY CLUSTERED,
	GaugerID int NULL,
	SyncDate datetime NULL CONSTRAINT DF_GaugerSyncLog_SyncDate  DEFAULT (getdate()),
	IncomingJson text NULL,
	OutgoingJson text NULL
) 
GO
GRANT INSERT, SELECT, DELETE, UPDATE ON tblGaugerSyncLog TO dispatchcrude_iis_acct
GO

CREATE TABLE tblGaugerOrderStatus
(
  ID int not null constraint PK_GaugerOrderStatus primary key
, Name varchar(25) not null
, IsComplete bit not null constraint DF_GaugerOrderStatus default (0)
)
GO
GRANT SELECT ON tblGaugerOrderStatus TO dispatchcrude_iis_acct
GO
create unique index udxGaugerOrderStatus_Name on tblGaugerOrderStatus(Name)
GO
INSERT INTO tblGaugerOrderStatus
	SELECT 1, 'Requested', 0 -- gauger equivalent of "GENERATED"
	UNION
	SELECT 2, 'Dispatched', 0 -- gauger equivalent of "DISPATCHED"
	UNION
	SELECT 3, 'Accepted', 0 -- gauger equivalent of "ACCEPTED"
	UNION
	SELECT 4, 'Entered', 0  -- gauger equivalent of "PICKED UP"
	UNION
	SELECT 5, 'Finalized', 0 -- gauger equivalent of "DELIVERED - FINALIZED"
	UNION
	SELECT 6, 'Completed', 1 -- gauger equivalent of "PRINTED and/or HANDWRITTEN"
	UNION
	SELECT 7, 'Skipped', 1 -- this jumps directly to Driver App side of application (Gauging is "complete")
GO

CREATE TABLE tblGaugerTicketType
(
  ID int not null constraint PK_GaugerTicketType primary key
, Name varchar(25) not null
, TicketTypeID int not null constraint FK_GaugerTicketType_TicketType foreign key references tblTicketType(ID)
)
GO
GRANT SELECT ON tblGaugerTicketType TO dispatchcrude_iis_acct
GO
create unique index udxGaugeTicketType_Name ON tblGaugerTicketType(Name)
GO
INSERT INTO tblGaugerTicketType
	SELECT 1, 'Gauge Run Basic', 1
GO 

CREATE TABLE tblGaugerOrder
(
  OrderID int not null 
	constraint PK_GaugerOrder_Order PRIMARY KEY
	constraint FK_GaugerOrder_Order FOREIGN KEY REFERENCES tblOrder(ID)
, TicketTypeID int not null constraint FK_GaugerOrder_TicketType FOREIGN KEY REFERENCES tblGaugerTicketType(ID)
, StatusID int not null constraint FK_GaugerOrder_Status FOREIGN KEY REFERENCES tblGaugerOrderStatus(ID)
, OriginTankID int NULL constraint FK_GaugerOrder_OriginTank FOREIGN KEY REFERENCES tblOriginTank(ID)
, OriginTankNum varchar(20) NULL
, ArriveTimeUTC datetime null
, DepartTimeUTC datetime null
, GaugerID int null constraint FK_GaugerOrder_Gauger FOREIGN KEY REFERENCES tblGauger(ID)
, Rejected bit not null constraint DF_GaugerOrder_Rejected DEFAULT (0)
, RejectReasonID int null constraint FK_GaugerOrder_RejectReason FOREIGN KEY REFERENCES tblOrderRejectReason(ID)
, RejectNotes varchar(255) null
, Handwritten bit not null constraint DF_GaugerOrder_HandWritten default (0)
, PrintDateUTC datetime null
, GaugerNotes varchar(255) null
, DueDate date not null
, PriorityID tinyint null constraint DF_GaugerOrder_Priority foreign key references tblPriority(ID)
, CreateDateUTC smalldatetime NULL
, CreatedByUser varchar(100) NULL
, LastChangeDateUTC smalldatetime NULL
, LastChangedByUser varchar(100) NULL
, DispatchNotes varchar(500) NULL
)
GO
GRANT INSERT, SELECT, DELETE, UPDATE ON tblGaugerOrder TO dispatchcrude_iis_acct
GO

CREATE TABLE tblGaugerOrderVirtualDelete
(
  ID int IDENTITY(1,1) NOT NULL CONSTRAINT PK_GaugerOrderVirtualDelete PRIMARY KEY
, OrderID int NOT NULL CONSTRAINT FK_GaugerOrderVirtualDelete_Order FOREIGN KEY REFERENCES tblOrder(ID) ON DELETE CASCADE
, GaugerID int NOT NULL CONSTRAINT FK_GaugerOrderVirtualDelete_Gauger FOREIGN KEY REFERENCES tblGauger(ID) ON DELETE CASCADE
, VirtualDeleteDateUTC smalldatetime NOT NULL CONSTRAINT DF_GaugerOrderVirtualDelete_DeleteDateUTC DEFAULT (getutcdate())
, VirtualDeletedByUser varchar(100) NULL
) 
GO
GRANT SELECT, INSERT, DELETE ON tblGaugerOrderVirtualDelete TO dispatchcrude_iis_acct
GO

CREATE TABLE tblGaugerOrderDbAudit
(
  DbAuditDate datetime CONSTRAINT DF_GaugerOrderDbAudit DEFAULT (getutcdate())
, OrderID int not null 
, TicketTypeID int null
, StatusID int null 
, OriginTankID int null
, OriginTankNum varchar(20) null
, ArriveTimeUTC datetime null
, DepartTimeUTC datetime null
, GaugerID int null 
, Rejected bit null 
, RejectReasonID int null 
, RejectNotes varchar(255) null
, Handwritten bit null
, PrintDateUTC datetime null
, GaugerNotes varchar(255) null
, DueDate date not null
, PriorityID tinyint null
, CreateDateUTC smalldatetime NULL
, CreatedByUser varchar(100) NULL
, LastChangeDateUTC smalldatetime NULL
, LastChangedByUser varchar(100) NULL
, DispatchNotes varchar(500) NULL
)
GO
GRANT INSERT, SELECT ON tblGaugerOrderDbAudit TO dispatchcrude_iis_acct
GO

/* =============================================
-- Author:		Kevin Alons
-- Create date: 19 Dec 2012
-- Description:	trigger to 
				1) validate any changes, and fail the update if invalid changes are submitted
				2) update the tblOrder table with revelant and appropriate changes
				3) if DBAudit is turned on, save an audit record for this Order change
-- =============================================*/
CREATE TRIGGER trigGaugerOrder_U ON tblGaugerOrder AFTER UPDATE AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @changesFound bit
		-- Note: the TRIGGER_NESTLEVEL( OBJECT_ID('trigGaugerOrder_U') ) = 1 statement prevents recursion
	IF (TRIGGER_NESTLEVEL( OBJECT_ID('trigGaugerOrder_U')) = 1) BEGIN

		/**********  START OF VALIDATION SECTION ************************/
		IF (
			   UPDATE(TicketTypeID)
			OR UPDATE(StatusID)
			OR UPDATE(OriginTankID)
			OR UPDATE(OriginTankNum)
			OR UPDATE(ArriveTimeUTC)
			OR UPDATE(DepartTimeUTC)
			OR UPDATE(GaugerID)
			OR UPDATE(Rejected)
			OR UPDATE(RejectReasonID)
			OR UPDATE(RejectNotes)
			OR UPDATE(PrintDateUTC)
			OR UPDATE(GaugerNotes)
			OR UPDATE(DueDate)
			OR UPDATE(PriorityID)
			OR UPDATE(CreateDateUTC)
			OR UPDATE(CreatedByUser)
			OR UPDATE(LastChangeDateUTC)
			OR UPDATE(LastChangedByUser)
			OR UPDATE(DispatchNotes) 
		) 
		BEGIN
			IF EXISTS ( -- if any affected orders are in AUDITED status
				SELECT * 
				FROM tblOrder O
				JOIN inserted i ON i.OrderID = O.ID
				WHERE O.StatusID IN (4) -- AUDITED
			)
			BEGIN				
				-- if the "Allow Audited Order Update (temp only)" setting is TRUE then only warn when this occurs
				IF (dbo.fnToBool(dbo.fnSettingValue(35)) = 1)
					PRINT 'Gauger data for AUDITED order(s) are being modified - please investigate why!'
				ELSE BEGIN
					-- otherwise (normal behavior) - prevent AUDITED orders from being modified (except to UN-AUDIT them)
					RAISERROR('Gauger data for AUDITED orders cannot be modified!', 16, 1)
					IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
					RETURN
				END
			END
		END
		ELSE -- NO CHANGES DETECTED SO JUST QUIT
			RETURN

		PRINT 'trigGaugerOrder_U FIRED'
					
		/**********  END OF VALIDATION SECTION ************************/

		-- update the associated, relevant fields on the underlying order when base order is still in GENERATED status
		UPDATE tblOrder
			SET OriginTankID = i.OriginTankID
				, OriginTankNum = i.OriginTankNum
				, Rejected = i.Rejected
				, RejectReasonID = i.RejectReasonID
				, RejectNotes = i.RejectNotes
				, LastChangeDateUTC = i.LastChangeDateUTC
				, LastChangedByUser = i.LastChangedByUser
				-- advance the order to GENERATED if the Gauger Status is COMPLETED
				, StatusID = CASE WHEN GOS.IsComplete = 1 THEN -10 ELSE O.StatusID END
		FROM tblOrder O
		JOIN inserted i ON i.OrderID = O.ID
		JOIN tblGaugerOrderStatus GOS ON GOS.ID = i.StatusID
		WHERE O.StatusID IN (-9) -- GENERATED
		
		-- record that the gauger order used to belong to the former Gauger (show it as deleted for this user)
		INSERT INTO tblGaugerOrderVirtualDelete (OrderID, GaugerID, VirtualDeleteDateUTC, VirtualDeletedByUser)
			SELECT d.OrderID, d.GaugerID, i.LastChangeDateUTC, i.LastChangedByUser
			FROM deleted d
			JOIN inserted i ON i.OrderID = d.OrderID
			WHERE d.GaugerID <> i.GaugerID
			
		IF EXISTS (SELECT * FROM inserted EXCEPT SELECT * FROM deleted)
		BEGIN
			-- optionally add tblGaugerOrderDBAudit records
			BEGIN TRY
				IF EXISTS(SELECT * FROM tblSetting WHERE ID = 32 AND (Value LIKE 'true' OR Value LIKE 'yes'))
					INSERT INTO tblGaugerOrderDbAudit (DBAuditDate, OrderID, StatusID, OriginTankID, OriginTankNum, ArriveTimeUTC, DepartTimeUTC, GaugerID, TicketTypeID, Rejected, RejectReasonID, RejectNotes, Handwritten, PrintDateUTC, GaugerNotes, DueDate, PriorityID, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DispatchNotes)
						SELECT GETUTCDATE(), OrderID, StatusID, OriginTankID, OriginTankNum, ArriveTimeUTC, DepartTimeUTC, GaugerID, TicketTypeID, Rejected, RejectReasonID, RejectNotes, Handwritten, PrintDateUTC, GaugerNotes, DueDate, PriorityID, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DispatchNotes
						FROM deleted d
			END TRY
			BEGIN CATCH
				PRINT 'trigGaugerOrder_U.DBAUDIT FAILURE: ' + ERROR_MESSAGE()
			END CATCH
		END
		
	PRINT 'trigGaugerOrder_U COMPLETE'

	END
END
GO 
EXEC sp_settriggerorder @triggername=N'[dbo].[trigGaugerOrder_U]', @order=N'First', @stmttype=N'UPDATE'
GO

CREATE TABLE tblGaugerOrderTicket
(
	UID uniqueidentifier NOT NULL 
		constraint PK_GaugerOrderTicket primary key
		constraint DF_GaugerOrderTicket default (newID()),
	OrderID int not null constraint FK_GaugerOrderTicket_Order foreign key references tblOrder(ID),
	CarrierTicketNum varchar(15) NOT NULL,
	DispatchConfirmNum varchar(25) NULL,
	TicketTypeID int NOT NULL constraint FK_GaugerOrderTicket_TicketType FOREIGN KEY REFERENCES tblGaugerTicketType(ID),
	OriginTankID int NULL constraint FK_GaugerOrderTicket_OriginTank FOREIGN KEY REFERENCES tblOriginTank(ID),
	TankNum varchar(20) NULL,
	ProductObsGravity decimal(9, 3) NULL,
	ProductObsTemp decimal(9, 3) NULL,
	ProductBSW decimal(9, 3) NULL,
	OpeningGaugeFeet tinyint NULL,
	OpeningGaugeInch tinyint NULL,
	OpeningGaugeQ tinyint NULL,
	BottomFeet tinyint NULL,
	BottomInches tinyint NULL,
	BottomQ tinyint NULL,
	Rejected bit NOT NULL,
	RejectReasonID int NULL constraint FK_GaugerOrderTicket_RejectReason FOREIGN KEY REFERENCES tblOrderTicketRejectReason(ID),
	RejectNotes varchar(255) NULL,
	SealOff varchar(10) NULL,
	SealOn varchar(10) NULL,
	FromMobileApp bit NOT NULL,
	CreateDateUTC smalldatetime NOT NULL constraint DF_GaugerOrderTicket_CreateDate default (getutcdate()),
	CreatedByUser varchar(100) NULL,
	LastChangeDateUTC smalldatetime NULL,
	LastChangedByUser varchar(100) NULL,
	DeleteDateUTC smalldatetime NULL,
	DeletedByUser varchar(100) NULL
)
GO
GRANT INSERT, SELECT, DELETE, UPDATE ON tblGaugerOrderTicket TO dispatchcrude_iis_acct
GO
CREATE NONCLUSTERED INDEX idxGaugerOrderTicket_OrderID_TicketNum ON tblGaugerOrderTicket 
(
	OrderID ASC,
	CarrierTicketNum ASC
)
GO

CREATE TABLE tblGaugerOrderTicketDbAudit
(
	DBAuditDate datetime constraint DF_GaugerOrderTicketDbAudit_DbAuditDate DEFAULT (getutcdate()),
	UID uniqueidentifier NOT NULL,
	OrderID int not null,
	CarrierTicketNum varchar(15) NULL,
	DispatchConfirmNum varchar(25) NULL,
	TicketTypeID int NULL,
	OriginTankID int NULL ,
	TankNum varchar(20) NULL,
	ProductObsGravity decimal(9, 3) NULL,
	ProductObsTemp decimal(9, 3) NULL,
	ProductBSW decimal(9, 3) NULL,
	OpeningGaugeFeet tinyint NULL,
	OpeningGaugeInch tinyint NULL,
	OpeningGaugeQ tinyint NULL,
	BottomFeet tinyint NULL,
	BottomInches tinyint NULL,
	BottomQ tinyint NULL,
	Rejected bit NOT NULL,
	RejectReasonID int NULL,
	RejectNotes varchar(255) NULL,
	SealOff varchar(10) NULL,
	SealOn varchar(10) NULL,
	FromMobileApp bit NULL,
	CreateDateUTC smalldatetime NULL,
	CreatedByUser varchar(100) NULL,
	LastChangeDateUTC smalldatetime NULL,
	LastChangedByUser varchar(100) NULL,
	DeleteDateUTC smalldatetime NULL,
	DeletedByUser varchar(100) NULL
)
GO
GRANT INSERT, SELECT ON tblGaugerOrderTicketDbAudit TO dispatchcrude_iis_acct
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 6 Feb 2013
-- Description:	trigger to ensure the GrossBarrels and NetBarrels are computed for valid, entered data
-- =============================================
ALTER TRIGGER trigOrderTicket_IU ON tblOrderTicket AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	-- Note: the TRIGGER_NESTLEVEL( OBJECT_ID('trigOrderTicket_IU') ) = 1 statement prevents recursion
	IF (TRIGGER_NESTLEVEL( OBJECT_ID('trigOrderTicket_IU')) = 1) BEGIN
		
		-- only do anything if something actually changed
		IF EXISTS (SELECT * FROM inserted EXCEPT SELECT * FROM deleted)
		BEGIN
			PRINT 'trigOrderTicket_IU FIRED'
			
			/**********  START OF VALIDATION SECTION ************************/
			DECLARE @errorString varchar(1000); SET @errorString = ''

			IF EXISTS (
				SELECT * 
				FROM tblOrder 
				WHERE ID IN (SELECT OrderID FROM inserted) 
				  -- prevent changes to any tickets belonging to a Delivered or Audited Order
				  AND StatusID IN (4))
			BEGIN
				IF (dbo.fnToBool(dbo.fnSettingValue(35)) = 1)
					PRINT 'Ticket for an AUDITED orders is being modified - please investigate why!'
				ELSE 				
					SET @errorString = @errorString + '|Tickets of Audited Orders cannot be modified'
			END

			-- prevent reassigning a ticket from one order to another
			IF EXISTS (
				SELECT i.* 
				FROM inserted i
				JOIN deleted d ON i.UID = d.UID
				WHERE i.OrderID <> d.OrderID)
			BEGIN
				SET @errorString = @errorString + '|Tickets cannot be moved to a different Order'
			END
			
			IF EXISTS (
				SELECT OT.OrderID
				FROM tblOrderTicket OT
				JOIN tblOrder O ON O.ID = OT.OrderID
				JOIN inserted i ON i.OrderID = OT.OrderID AND i.CarrierTicketNum = OT.CarrierTicketNum AND i.ID <> OT.ID
				WHERE OT.DeleteDateUTC IS NULL and I.DeleteDateUTC IS NULL
				GROUP BY OT.OrderID, OT.CarrierTicketNum
				HAVING COUNT(*) > 1
			)
			BEGIN
				SET @errorString = @errorString + '|Duplicate Ticket Numbers are not allowed'
			END
			
			-- store all the tickets for orders that are not in Generated status and not deleted (only these need validation)
			SELECT i.*, O.StatusID 
			INTO #i
			FROM inserted i
			JOIN tblOrder O ON O.ID = i.OrderID
			WHERE i.DeleteDateUTC IS NULL
				
			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (3) AND BOLNum IS NULL)
			BEGIN
				SET @errorString = @errorString + '|BOL # value is required for Meter Run tickets'
			END
			
			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (1, 2) AND OriginTankID IS NULL AND TankNum IS NULL AND Rejected = 0)
			BEGIN
				SET @errorString = @errorString + '|Tank is required for Gauge Run & Net Volume tickets'
			END

			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (1) AND CarrierTicketNum IS NULL)
			BEGIN
				SET @errorString = @errorString + '|Ticket # value is required for Gauge Run tickets'
			END

			IF EXISTS (SELECT ID FROM #i WHERE (TicketTypeID IN (1, 2) AND (ProductObsTemp IS NULL OR ProductObsGravity IS NULL OR ProductBSW IS NULL))
						OR (TicketTypeID IN (1) AND StatusID NOT IN (-9) AND (ProductHighTemp IS NULL OR ProductLowTemp IS NULL))
			)
			BEGIN
				SET @errorString = @errorString + '|All Product Measurement values are required for Gauge Run tickets'
			END
			
			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (1) AND Rejected = 0 
				AND (OpeningGaugeFeet IS NULL OR OpeningGaugeInch IS NULL OR OpeningGaugeQ IS NULL))
			BEGIN
				SET @errorString = @errorString + '|All Opening Gauge values are required for Gauge Run tickets'
			END
			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (1) AND Rejected = 0 AND StatusID NOT IN (-9)
				AND (ClosingGaugeFeet IS NULL OR ClosingGaugeInch IS NULL OR ClosingGaugeQ IS NULL))
			BEGIN
				SET @errorString = @errorString + '|All Closing Gauge values are required for Gauge Run tickets'
			END

			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (2) AND Rejected = 0 AND StatusID NOT IN (-9) AND (GrossUnits IS NULL))
			BEGIN
				SET @errorString = @errorString + '|Gross Volume value is required for Net Volume tickets'
			END

			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (7) AND Rejected = 0 AND StatusID NOT IN (-9) AND (GrossUnits IS NULL))
			BEGIN
				SET @errorString = @errorString + '|Gross Volume value is required for Gauge Net tickets'
			END

			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (3) AND Rejected = 0 AND StatusID NOT IN (-9) AND (GrossUnits IS NULL OR NetUnits IS NULL))
			BEGIN
				SET @errorString = @errorString + '|Gross & Net Volume values are required for Meter Run tickets'
			END

			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (1) AND Rejected = 0 AND StatusID NOT IN (-9) AND (SealOff IS NULL OR SealOn IS NULL))
			BEGIN
				SET @errorString = @errorString + '|All Seal Off & Seal On values are required for Gauge Run tickets'
			END

			IF (len(@errorString) > 0)
			BEGIN
				SET @errorString = replace(substring(@errorString, 2, 1000), '|', char(13) + char(10))
				RAISERROR(@errorString, 16, 1)
				IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
				RETURN
			END
			/**********  END OF VALIDATION SECTION ************************/
			
			-- re-compute GaugeRun ticket Gross barrels
			IF UPDATE(OriginTankID) OR UPDATE(OpeningGaugeFeet) OR UPDATE(OpeningGaugeInch) OR UPDATE(OpeningGaugeQ)
				OR UPDATE (ClosingGaugeFeet) OR UPDATE(ClosingGaugeInch) OR UPDATE(ClosingGaugeQ) 
				OR UPDATE (GrossUnits)
			BEGIN
				UPDATE tblOrderTicket
				  SET GrossUnits = dbo.fnConvertUom(dbo.fnOriginTankStrappingLevelDeltaBarrels(
						OT.OriginTankID
					  , OpeningGaugeFeet
					  , OpeningGaugeInch
					  , OpeningGaugeQ
					  , ClosingGaugeFeet
					  , ClosingGaugeInch
					  , ClosingGaugeQ), 1, O.OriginUomID) -- ensure this value is always normalized from BARRELS
				FROM tblOrderTicket OT
				JOIN tblOrder O ON O.ID = OT.OrderID
				WHERE OT.ID IN (SELECT ID FROM inserted) 
				  AND OT.TicketTypeID = 1 -- Gauge Run Ticket
				  AND OT.OpeningGaugeFeet IS NOT NULL
				  AND OT.OpeningGaugeInch IS NOT NULL
				  AND OT.OpeningGaugeQ IS NOT NULL
				  AND OT.ClosingGaugeFeet IS NOT NULL
				  AND OT.ClosingGaugeInch IS NOT NULL
				  AND OT.ClosingGaugeQ IS NOT NULL
			END
			-- recompute the GrossUnits of any changed Meter Run tickets
			IF UPDATE(OpenMeterUnits) OR UPDATE(CloseMeterUnits) BEGIN
				--Print 'updating tblOrderTicket GOV from Open/Close Meter Units'
				UPDATE tblOrderTicket 
					SET GrossUnits = CloseMeterUnits - OpenMeterUnits
				WHERE TicketTypeID = 3  -- meter run tickets ONLY
				  AND OpenMeterUnits IS NOT NULL AND CloseMeterUnits IS NOT NULL
				
			END
			-- re-compute GaugeRun | Net Barrel | Meter Run tickets NetUnits
			IF UPDATE(OpeningGaugeFeet) OR UPDATE(OpeningGaugeInch) OR UPDATE(OpeningGaugeQ)
				OR UPDATE (ClosingGaugeFeet) OR UPDATE(ClosingGaugeInch) OR UPDATE(ClosingGaugeQ) 
				OR UPDATE(OriginTankID) OR UPDATE(GrossUnits) OR UPDATE(GrossStdUnits) OR UPDATE(NetUnits)
				OR UPDATE(ProductObsTemp) OR UPDATE(ProductObsGravity) OR UPDATE(ProductBSW)
				OR UPDATE(OpenMeterUnits) OR UPDATE(CloseMeterUnits)
			BEGIN
				--PRINT 'updating tblOrderTicket NSV/GSV'
				UPDATE tblOrderTicket
				  SET NetUnits = dbo.fnCrudeNetCalculator(GrossUnits, ProductObsTemp, ProductObsGravity, isnull(ProductBSW, 0))
					, GrossStdUnits = dbo.fnCrudeNetCalculator(GrossUnits, ProductObsTemp, ProductObsGravity, 0)
				WHERE ID IN (SELECT ID FROM inserted)
				  AND TicketTypeID IN (1, 2, 7) -- GAUGE RUN, NET VOLUME, GAUGE NET
				  AND GrossUnits IS NOT NULL
				  AND ProductObsTemp IS NOT NULL
				  AND ProductObsGravity IS NOT NULL
			END
			
			-- ensure the Order record is in-sync with the Tickets
			--declare @gov decimal(9,3); SELECT TOP 1 @gov = OT.GrossUnits FROM tblOrderTicket OT JOIN inserted i ON i.ID = OT.ID;
			--PRINT 'updating tblOrder GOV/GSV/NSV GOV = ' + ltrim(@gov)
			UPDATE tblOrder 
			SET OriginGrossUnits = (SELECT sum(GrossUnits) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND OT.Rejected = 0 AND OT.DeleteDateUTC IS NULL)
			  , OriginNetUnits = (SELECT sum(NetUnits) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND OT.Rejected = 0 AND OT.DeleteDateUTC IS NULL)
			  , OriginGrossStdUnits = (SELECT sum(GrossStdUnits) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND OT.Rejected = 0 AND OT.DeleteDateUTC IS NULL)

				-- use the first MeterRun/BasicRun BOLNum as the Order BOL Num (favor the first un-Rejected)
			  , OriginBOLNum = (
					SELECT TOP 1 BOLNum FROM (
						SELECT TOP 1 BOLNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID NOT IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
						UNION SELECT TOP 1 BOLNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID NOT IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X)
				-- use the first GaugeRun|NetVolume CarrierTicketNum as the Order.CarrierTicketNum (favor first un-Rejected)
			  , CarrierTicketNum = (
					SELECT TOP 1 CarrierTicketNum FROM (
						SELECT TOP 1 CarrierTicketNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
						UNION SELECT TOP 1 CarrierTicketNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X)
				-- use the first GaugeRun|NetVolume OriginTankID for the Order (favor first un-Rejected ticket)
			  , OriginTankID = (
					SELECT TOP 1 OriginTankID FROM (
						SELECT TOP 1 OriginTankID FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
						UNION SELECT TOP 1 OriginTankID FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X)
				-- use the first GaugeRun|NetVolume OriginTankNum for the Order (favor first un-Rejected ticket)
			  , OriginTankNum = (
					SELECT TOP 1 OriginTankNum FROM (
						SELECT TOP 1 OriginTankNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
						UNION SELECT TOP 1 OriginTankNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X)
			  , LastChangeDateUTC = (SELECT MAX(isnull(LastChangeDateUTC, CreateDateUTC)) FROM inserted WHERE OrderID = O.ID)
			  , LastChangedByUser = (SELECT MIN(isnull(LastChangedByUser, CreatedByUser)) FROM inserted WHERE OrderID = O.ID)
			FROM tblOrder O
			WHERE ID IN (SELECT DISTINCT OrderID FROM inserted)

			/* START DB AUDIT *********************************************************/
			BEGIN TRY
				IF EXISTS(SELECT * FROM tblSetting WHERE ID = 32 AND (Value LIKE 'true' OR Value LIKE 'yes'))
					INSERT INTO tblOrderTicketDbAudit (DBAuditDate, ID, OrderID, CarrierTicketNum, TicketTypeID, TankNum, ProductObsGravity, ProductObsTemp, OpeningGaugeFeet, OpeningGaugeInch, OpeningGaugeQ, ClosingGaugeFeet, ClosingGaugeInch, ClosingGaugeQ, GrossUnits, NetUnits, Rejected, RejectNotes, SealOff, SealOn, BOLNum, ProductHighTemp, ProductLowTemp, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser, UID, FromMobileApp, OriginTankID, GrossStdUnits, BottomFeet, BottomInches, BottomQ, RejectReasonID, MeterFactor, OpenMeterUnits, CloseMeterUnits)
						SELECT GETUTCDATE(), ID, OrderID, CarrierTicketNum, TicketTypeID, TankNum, ProductObsGravity, ProductObsTemp, OpeningGaugeFeet, OpeningGaugeInch, OpeningGaugeQ, ClosingGaugeFeet, ClosingGaugeInch, ClosingGaugeQ, GrossUnits, NetUnits, Rejected, RejectNotes, SealOff, SealOn, BOLNum, ProductHighTemp, ProductLowTemp, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser, UID, FromMobileApp, OriginTankID, GrossStdUnits, BottomFeet, BottomInches, BottomQ, RejectReasonID, MeterFactor, OpenMeterUnits, CloseMeterUnits
						FROM deleted d
			END TRY
			BEGIN CATCH
				PRINT 'trigOrderTicket_IU.DBAUDIT FAILURE: ' + ERROR_MESSAGE()
			END CATCH
			/* END DB AUDIT *********************************************************/

			PRINT 'trigOrderTicket_IU COMPLETE'
		END
	END	
END
GO
EXEC sp_settriggerorder @triggername=N'[dbo].[trigOrderTicket_IU]', @order=N'First', @stmttype=N'INSERT'
GO
EXEC sp_settriggerorder @triggername=N'[dbo].[trigOrderTicket_IU]', @order=N'First', @stmttype=N'UPDATE'
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 18 Apr 2015
-- Description:	trigger for the following purposes:
--				1) validate data
--				2) push relevant changes to the tblOrderTicket table
--				3) DBAudit
-- =============================================
CREATE TRIGGER trigGaugerOrderTicket_IU ON tblGaugerOrderTicket AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	-- Note: the TRIGGER_NESTLEVEL( OBJECT_ID('trigGaugerOrderTicket_IU') ) = 1 statement prevents recursion
	IF (TRIGGER_NESTLEVEL( OBJECT_ID('trigGaugerOrderTicket_IU')) = 1) BEGIN
		
		-- only do anything if something actually changed
		IF EXISTS (SELECT * FROM inserted EXCEPT SELECT * FROM deleted)
		BEGIN
			PRINT 'trigGaugerOrderTicket_IU FIRED'
			
			/**********  START OF VALIDATION SECTION ************************/
			DECLARE @errorString varchar(255)

			IF EXISTS (
				SELECT * 
				FROM tblOrder 
				WHERE ID IN (SELECT OrderID FROM inserted) 
				  -- prevent changes to any tickets belonging to a Delivered or Audited Order
				  AND StatusID IN (4))
			BEGIN
				IF (dbo.fnToBool(dbo.fnSettingValue(35)) = 1)
					PRINT 'Gauger Ticket for an AUDITED orders is being modified - please investigate why!'
				ELSE 				
					SET @errorString = 'Gauger Tickets of Audited Orders cannot be modified'
			END

			-- prevent reassigning a ticket from one order to another
			IF EXISTS (
				SELECT i.* 
				FROM inserted i
				JOIN deleted d ON i.UID = d.UID
				WHERE i.OrderID <> d.OrderID)
			BEGIN
				SET @errorString = 'Tickets cannot be moved to a different Order'
			END

			IF EXISTS (
				SELECT OT.OrderID
				FROM tblGaugerOrderTicket OT
				JOIN inserted i ON i.OrderID = OT.OrderID AND i.CarrierTicketNum = OT.CarrierTicketNum AND i.UID <> OT.UID
				WHERE OT.DeleteDateUTC IS NULL and I.DeleteDateUTC IS NULL
				GROUP BY OT.OrderID, OT.CarrierTicketNum
				HAVING COUNT(*) > 1
			) 
			BEGIN
				SET @errorString = 'Duplicate active Ticket Numbers are not allowed'
			END
			
			ELSE IF EXISTS (SELECT * FROM inserted WHERE TicketTypeID IN (1) AND OriginTankID IS NULL AND TankNum IS NULL AND Rejected = 0 AND DeleteDateUTC IS NULL)
			BEGIN
				SET @errorString = 'Tank is required for "Gauge Run Basic" tickets'
			END

			ELSE IF EXISTS (SELECT * FROM inserted WHERE TicketTypeID IN (1) AND CarrierTicketNum IS NULL AND DeleteDateUTC IS NULL)
			BEGIN
				SET @errorString = 'Ticket # value is required for "Gauge Run Basic" tickets'
			END

			ELSE IF EXISTS (SELECT * FROM inserted WHERE DeleteDateUTC IS NULL
					AND (TicketTypeID IN (1) AND (ProductObsTemp IS NULL OR ProductObsGravity IS NULL OR ProductBSW IS NULL))
				)
			BEGIN
				SET @errorString = 'All Product Measurement values are required for "Gauge Run Basic" tickets'
			END
			
			ELSE IF EXISTS (SELECT * FROM inserted WHERE TicketTypeID IN (1) AND Rejected = 0 AND DeleteDateUTC IS NULL
				AND (OpeningGaugeFeet IS NULL OR OpeningGaugeInch IS NULL OR OpeningGaugeQ IS NULL))
			BEGIN
				SET @errorString = 'All Opening Gauge values are required for "Gauge Run Basic" tickets'
			END

			IF (@errorString IS NOT NULL)
			BEGIN
				RAISERROR(@errorString, 16, 1)
				IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
			END
			
			/**********  END OF VALIDATION SECTION ************************/
			
			-- update any existing OrderTicket records that are still only populated from Gauger entry
			UPDATE tblOrderTicket
				SET CarrierTicketNum = i.CarrierTicketNum
				, DispatchConfirmNum = i.DispatchConfirmNum
				, OriginTankID = i.OriginTankID
				, TankNum = i.TankNum
				, ProductObsGravity = i.ProductObsGravity
				, ProductObsTemp = i.ProductObsTemp
				, ProductBSW = i.ProductBSW
				, OpeningGaugeFeet = i.OpeningGaugeFeet
				, OpeningGaugeInch = i.OpeningGaugeInch
				, OpeningGaugeQ = i.OpeningGaugeQ
				, BottomFeet = i.BottomFeet
				, BottomInches = i.BottomInches
				, BottomQ = i.BottomQ
				, Rejected = i.Rejected
				, RejectReasonID = i.RejectReasonID
				, RejectNotes = i.RejectNotes
				, SealOff = i.SealOff
				, SealOn = i.SealOn
				, FromMobileApp = i.FromMobileApp
				, LastChangeDateUTC = i.LastChangeDateUTC
				, LastChangedByUser = i.LastChangedByUser
				, DeleteDateUTC = i.DeleteDateUTC
				, DeletedByUser = i.DeletedByUser
			FROM tblOrderTicket OT
			JOIN tblOrder O ON O.ID = OT.OrderID AND O.StatusID IN (-9) -- GAUGER StatusID			
			JOIN inserted i ON i.UID = OT.UID AND i.CreateDateUTC = OT.CreateDateUTC
			LEFT JOIN deleted d ON d.UID = OT.UID AND isnull(d.LastChangeDateUTC, getutcdate()) = isnull(OT.LastChangeDateUTC, getutcdate())

			-- create new OrderTicket records not not present
			INSERT INTO tblOrderTicket (UID, OrderID, TicketTypeID, CarrierTicketNum, DispatchConfirmNum, OriginTankID, TankNum
				, ProductObsGravity, ProductObsTemp, ProductBSW, OpeningGaugeFeet, OpeningGaugeInch, OpeningGaugeQ
				, BottomFeet, BottomInches, BottomQ, Rejected, RejectReasonID, RejectNotes, SealOff, SealOn, FromMobileApp
				, CreateDateUTC, CreatedByUser)
				SELECT i.UID, i.OrderID, GTT.TicketTypeID, i.CarrierTicketNum, i.DispatchConfirmNum, i.OriginTankID, i.TankNum
					, i.ProductObsGravity, i.ProductObsTemp, i.ProductBSW, i.OpeningGaugeFeet, i.OpeningGaugeInch, i.OpeningGaugeQ
					, i.BottomFeet, i.BottomInches, i.BottomQ, i.Rejected, i.RejectReasonID, i.RejectNotes, i.SealOff, i.SealOn, i.FromMobileApp
					, i.CreateDateUTC, i.CreatedByUser
				FROM inserted i
				JOIN tblGaugerTicketType GTT ON GTT.ID = i.TicketTypeID
				LEFT JOIN tblOrderTicket OT ON OT.UID = i.UID
				WHERE OT.UID IS NULL

			/* START DB AUDIT *********************************************************/
			BEGIN TRY
				IF EXISTS(SELECT * FROM tblSetting WHERE ID = 32 AND (Value LIKE 'true' OR Value LIKE 'yes'))
					INSERT INTO tblGaugerOrderTicketDbAudit (DBAuditDate, UID, OrderID, CarrierTicketNum, TicketTypeID, TankNum, ProductObsGravity, ProductObsTemp, OpeningGaugeFeet, OpeningGaugeInch, OpeningGaugeQ, Rejected, RejectNotes, SealOff, SealOn, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser, FromMobileApp, OriginTankID, BottomFeet, BottomInches, BottomQ, RejectReasonID)
						SELECT GETUTCDATE(), UID, OrderID, CarrierTicketNum, TicketTypeID, TankNum, ProductObsGravity, ProductObsTemp, OpeningGaugeFeet, OpeningGaugeInch, OpeningGaugeQ, Rejected, RejectNotes, SealOff, SealOn, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser, FromMobileApp, OriginTankID, BottomFeet, BottomInches, BottomQ, RejectReasonID
						FROM deleted d
			END TRY
			BEGIN CATCH
				PRINT 'trigGaugerOrderTicket_IU.DBAUDIT FAILURE: ' + ERROR_MESSAGE()
			END CATCH
			/* END DB AUDIT *********************************************************/

			PRINT 'trigGaugerOrderTicket_IU COMPLETE'
		END
	END
END
GO
EXEC sp_settriggerorder @triggername=N'[dbo].[trigGaugerOrderTicket_IU]', @order=N'First', @stmttype=N'INSERT'
GO
EXEC sp_settriggerorder @triggername=N'[dbo].[trigGaugerOrderTicket_IU]', @order=N'First', @stmttype=N'UPDATE'
GO

/***********************************/
-- Date Created: 10 Apr 2015
-- Author: Kevin Alons
-- Purpose: return Gaugers table records with "friendly" translated values included
/***********************************/
CREATE VIEW viewGaugerBase AS
SELECT X.*
	, CASE WHEN Active = 1 THEN '' ELSE 'Deleted: ' END + FullName AS FullNameD
FROM (
	SELECT G.*
	, Active = cast(CASE WHEN G.DeleteDateUTC IS NULL THEN 1 ELSE 0 END as bit) 
	, FullName = G.FirstName + ' ' + G.LastName 
	, FullNameLF = G.LastName + ', ' + G.FirstName 
	, HomeStateAbbrev = S.Abbreviation 
	, HomeState = S.FullName
	, Region = R.Name 
	, GS.AppVersion
	FROM dbo.tblGauger G
	LEFT JOIN dbo.tblGaugerSync GS ON GS.GaugerID = G.ID
	LEFT JOIN tblState S ON S.ID = G.HomeStateID
	LEFT JOIN tblRegion R ON R.ID = G.RegionID
) X
GO
GRANT SELECT ON viewGaugerBase TO dispatchcrude_iis_acct
GO

/*************************************************************/
-- Date Created: 10 Apr 2015
-- Author: Kevin Alons
-- Purpose: return the UserNames (CSV style) for each Gauger
/*************************************************************/
CREATE FUNCTION fnGaugerUserNames() RETURNS 
	@ret TABLE (
		GaugerID int
	  , UserNames varchar(1000)
	) AS
BEGIN
	DECLARE @PG TABLE (id int, UserName varchar(100), GaugerID int)
	INSERT INTO @PG SELECT ROW_NUMBER() OVER (ORDER BY ProfileValue, UserName), * FROM dbo.fnUserProfileValues('GaugerID')

	DECLARE @id int, @gaugerID int, @priorGaugerID int, @name varchar(100), @names varchar(1000);
	
	SELECT TOP 1 @id = id, @priorGaugerID = GaugerID, @gaugerID = GaugerID, @name = UserName FROM @PG ORDER BY id
	WHILE (@id IS NOT NULL)
	BEGIN
		DELETE FROM @PG WHERE id = @id
		IF (@gaugerID = @priorGaugerID)
			SELECT @names = isnull(@names + ',', '') + @name
		ELSE
		BEGIN
			INSERT INTO @ret (GaugerID, UserNames) VALUES (@priorGaugerID, @names)
			SELECT @priorGaugerID = @gaugerID, @names = @name
		END
		SET @id = NULL
		SELECT TOP 1 @id = id, @gaugerID = GaugerID, @name = UserName FROM @PG ORDER BY id
	END

	RETURN
END
GO
GRANT SELECT ON fnGaugerUserNames TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 26 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Gauger table records with LastGaugerOrderTime + computed assigned ASP.NET usernames
/***********************************/
CREATE VIEW viewGauger AS
SELECT GB.*
	, LastGaugerOrderTime = (SELECT MAX(DepartTimeUTC) FROM dbo.tblGaugerOrder WHERE GaugerID = GB.ID) 
	, GUN.UserNames
FROM viewGaugerBase GB
LEFT JOIN dbo.fnGaugerUserNames() GUN ON GUN.GaugerID = GB.ID
GO
GRANT SELECT ON viewGauger TO dispatchcrude_iis_acct
GO

/***********************************************************************/
-- Date Created: 18 Apr 2013
-- Author: Kevin Alons
-- Purpose: return the GaugerApp.MasterData for a single gauger/login
/***********************************************************************/
CREATE FUNCTION fnGaugerMasterData(@Valid bit, @GaugerID int, @UserName varchar(100)) RETURNS TABLE AS 
RETURN
	SELECT CASE WHEN GS.GaugerID IS NULL THEN 0 ELSE @Valid END AS Valid
		, UserName = @UserName 
		, GaugerID = @GaugerID 
		, GaugerName = D.FullName 
		, D.MobilePrint
		, GS.LastSyncUTC
		, SyncMinutes = cast(SSF.Value as int) 
		, SchemaVersion = SSV.Value  
		, LatestAppVersion = LAV.Value 
		, GS.AppVersion
		, GS.PasswordHash
	FROM viewGaugerBase D
	LEFT JOIN tblGaugerSync GS ON GS.GaugerID = D.ID
	JOIN tblSetting SSV ON SSV.ID = 0  -- Schema Version
	JOIN tblSetting SSF ON SSF.ID = 45 -- sync frequency (in minutes)
	JOIN tblSetting LAV ON LAV.ID = 46 -- LatestAppVersion
	WHERE D.ID = @GaugerID
GO
GRANT SELECT ON fnGaugerMasterData TO dispatchcrude_iis_acct
GO

-- clone the relevant Driver App settings for the Gauger App
INSERT INTO tblSetting (ID, Category, Name, SettingTypeID, Value, CreateDateUTC, CreatedByUser, ReadOnly)
	-- 45 = Sync Frequency, 46 = LatestAppVersion, 47 = ValidatePasswordHash, 48 = MinimumAppVersion, 49 = Debug_Sync_Gauger_Sync, 50 = Arrive_Depart_Tolerance_Min
	SELECT ID + 34, 'Gauger App Settings', replace(Name, 'Driver', 'Gauger'), SettingTypeID, Value, GETUTCDATE(), 'System', 0 FROM tblSetting WHERE ID IN (11, 12, 13)
	UNION
	SELECT 48, 'Gauger App Settings', replace(Name, 'Driver', 'Gauger'), SettingTypeID, Value, GETUTCDATE(), 'System', 0 FROM tblSetting WHERE ID IN (33)
	UNION
	SELECT 49, 'Gauger App Settings', replace(Name, 'Driver', 'Gauger'), SettingTypeID, Value, GETUTCDATE(), 'System', 0 FROM tblSetting WHERE ID IN (43)
	UNION
	SELECT 50, 'Gauger App Settings', replace(Name, 'Driver', 'Gauger'), SettingTypeID, Value, GETUTCDATE(), 'System', 0 FROM tblSetting WHERE ID IN (41)
	UNION
	SELECT 51, 'Gauger App Settings', 'Show Bottom Detail Entry Fields (FT, Q)', SettingTypeID, Value, GETUTCDATE(), 'System', 0 FROM tblSetting WHERE ID IN (24)
	
GO

/***********************************/
-- Date Created: 10 Apr 2015
-- Author: Kevin Alons
-- Purpose: return a full list of Order Statuses (including inverted Gauger sub-statuses)
/***********************************/
CREATE VIEW viewOrderPrintStatus AS
	SELECT ID, OrderStatus, StatusNum, ActionText, ForGauger = CAST(0 AS bit) FROM tblOrderStatus
	UNION SELECT ID = -ID, 'Gauger ' + Name, ID - 9, NULL, 1 FROM tblGaugerOrderStatus
GO 
GRANT SELECT ON viewOrderPrintStatus TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Order records with "translated friendly" values for FK relationships
/***********************************/
ALTER VIEW viewOrder AS
SELECT O.*
	, PrintStatus = OPS.OrderStatus
FROM (
	SELECT O.*
	, Origin = vO.Name 
	, OriginFull = vO.FullName 
	, OriginState = vO.State 
	, OriginStateAbbrev = vO.StateAbbrev 
	, OriginStation = vO.Station
	, OriginCounty = vO.County 
	, vO.LeaseName
	, vO.LeaseNum
	, OriginRegion = vO.Region
	, OriginRegionID = vO.RegionID
	, OriginCountryID = vO.CountryID
	, OriginCountry = vO.Country
	, OriginCountryShort = vO.CountryShort
	, OriginLegalDescription = vO.LegalDescription 
	, OriginNDIC = vO.NDICFileNum 
	, OriginNDM = vO.NDM 
	, OriginCA = vO.CA 
	, OriginTimeZoneID = vO.TimeZoneID 
	, OriginUseDST = vO.UseDST 
	, OriginStateID = vO.StateID
	, vO.H2S
	, Destination = vD.Name 
	, DestinationFull = vD.FullName 
	, DestinationState = vD.State 
	, DestinationStateAbbrev = vD.StateAbbrev 
	, DestinationTypeID = vD.ID
	, vD.DestinationType
	, DestStation = vD.Station 
	, DestTimeZoneID = vD.TimeZoneID 
	, DestUseDST = vD.UseDST 
	, DestStateID = vD.StateID
	, DestRegion = vO.Region
	, DestRegionID = vO.RegionID
	, DestCountryID = vD.CountryID
	, DestCountry = vD.Country
	, DestCountryShort = vD.CountryShort
	, Customer = C.Name 
	, Carrier = CA.Name 
	, CarrierType = CT.Name 
	, OS.OrderStatus
	, OS.StatusNum
	, Driver = D.FullName 
	, DriverFirst = D.FirstName 
	, DriverLast = D.LastName 
	, Truck = TRU.FullName 
	, Trailer = TR1.FullName 
	, Trailer2 = TR2.FullName 
	, P.PriorityNum
	, TicketType = TT.Name 
	, DestTicketTypeID = vD.TicketTypeID 
	, DestTicketType = vD.TicketType 
	, Operator = OP.Name 
	, Producer = PR.Name 
	, Pumper = PU.FullName 
	, DriverNumber = D.IDNumber
	, CarrierNumber = CA.IDNumber
	, CarrierTypeID = CA.CarrierTypeID
	, TruckNumber = TRU.IDNumber 
	, TrailerNumber = TR1.IDNumber 
	, Trailer2Number = TR2.IDNumber 
	, Product = PRO.Name
	, ProductShort = PRO.ShortName 
	, PRO.ProductGroupID
	, PRO.ProductGroup
	, OriginUOM = OUom.Name 
	, OriginUomShort = OUom.Abbrev 
	, OriginTankID_Text = CASE WHEN ORT.TankNum = '*' THEN '*' + isnull(O.OriginTankNum, '?') ELSE ORT.TankNum END 
	, DestUOM = DUom.Name 
	, DestUomShort = DUom.Abbrev 
	, Active = cast((CASE WHEN O.DeleteDateUTC IS NULL THEN 1 ELSE 0 END) as bit) 
	, IsDeleted = cast((CASE WHEN O.DeleteDateUTC IS NOT NULL THEN 1 ELSE 0 END) as bit) 
	, PickupPrintStatus = PPS.Name 
	, PickupCompleted = PPS.IsCompleted 
	, DeliverPrintStatus = DPS.Name 
	, DeliverCompleted = DPS.IsCompleted 
	, PrintStatusID = CASE WHEN O.StatusID = -9 THEN ISNULL(-GAO.StatusID, O.StatusID)
						   WHEN O.StatusID = 8 AND PPS.IsCompleted = 0 THEN 7
						   WHEN O.StatusID = 3 AND DPS.IsCompleted = 0 THEN 8
						ELSE O.StatusID END 
	, OriginTankText = CASE WHEN O.OriginTankID IS NULL OR ORT.TankNum = '*' THEN O.OriginTankNum ELSE ORT.TankNum END
	, OriginWaitNum = OWR.Num
	, OriginWaitDesc = OWR.Description
	, OriginWaitNumDesc = OWR.NumDesc
	, DestWaitNum = DWR.Num
	, DestWaitDesc = DWR.Description
	, DestWaitNumDesc = DWR.NumDesc
	, RejectNum = ORR.Num
	, RejectDesc = ORR.Description
	, RejectNumDesc = ORR.NumDesc
	, RerouteCount = (SELECT COUNT(1) FROM tblOrderReroute ORE WHERE ORE.OrderID = O.ID)
	, TicketCount = (SELECT COUNT(1) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND OT.DeleteDateUTC IS NULL)
	, TotalMinutes = isnull(OriginMinutes, 0) + ISNULL(DestMinutes, 0)
	, GaugerStatusID = GAO.StatusID
	, GaugerID = GAO.GaugerID
	FROM dbo.viewOrderBase O
	LEFT JOIN dbo.viewOrigin vO ON vO.ID = O.OriginID
	LEFT JOIN dbo.viewDestination vD ON vD.ID = O.DestinationID
	LEFT JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
	JOIN dbo.tblOrderStatus AS OS ON OS.ID = O.StatusID
	LEFT JOIN dbo.tblCarrier CA ON CA.ID = O.CarrierID
	LEFT JOIN dbo.tblCarrierType CT ON CT.ID = CA.CarrierTypeID
	LEFT JOIN dbo.viewDriverBase D ON D.ID = O.DriverID
	LEFT JOIN dbo.viewTruck TRU ON TRU.ID = O.TruckID
	LEFT JOIN dbo.viewTrailer TR1 ON TR1.ID = O.TrailerID
	LEFT JOIN dbo.viewTrailer TR2 ON TR2.ID = O.Trailer2ID
	LEFT JOIN dbo.tblPriority P ON P.ID = O.PriorityID
	LEFT JOIN dbo.tblTicketType TT ON TT.ID = O.TicketTypeID
	LEFT JOIN dbo.tblOriginTank ORT ON ORT.ID = O.OriginTankID
	LEFT JOIN dbo.tblOperator OP ON OP.ID = O.OperatorID
	LEFT JOIN dbo.viewPumper PU ON PU.ID = O.PumperID
	LEFT JOIN dbo.tblProducer PR ON PR.ID = O.ProducerID
	LEFT JOIN dbo.viewProduct PRO ON PRO.ID = O.ProductID
	LEFT JOIN dbo.tblUom OUom ON OUom.ID = O.OriginUomID
	LEFT JOIN dbo.tblUom DUom ON DUom.ID = O.DestUomID
	LEFT JOIN dbo.tblPrintStatus PPS ON PPS.ID = O.PickupPrintStatusID
	LEFT JOIN dbo.tblPrintStatus DPS ON DPS.ID = O.DeliverPrintStatusID
	LEFT JOIN dbo.viewOriginWaitReason OWR ON OWR.ID = O.OriginWaitReasonID
	LEFT JOIN dbo.viewDestinationWaitReason DWR ON DWR.ID = O.DestWaitReasonID
	LEFT JOIN dbo.viewOrderRejectReason ORR ON ORR.ID = O.RejectReasonID
	LEFT JOIN dbo.tblGaugerOrder GAO ON GAO.OrderID = O.ID
	LEFT JOIN dbo.viewGauger G ON G.ID = GAO.GaugerID
) O
LEFT JOIN dbo.viewOrderPrintStatus OPS ON OPS.ID = O.PrintStatusID
GO

EXEC _spDropView 'viewGaugerOrder'
GO
/**********************************/
-- Date Created: 18 Apr 2015
-- Author: Kevin Alons
-- Purpose: return GaugerOrder records with "translated friendly" values for FK relationships
/***********************************/
CREATE VIEW viewGaugerOrder AS
SELECT GAO.*
	, OrderNum = O.OrderNum
	, OrderDate = O.OrderDate
	, Status = GOS.Name
	, CarrierTicketNum = O.CarrierTicketNum
	, DispatchConfirmNum = O.DispatchConfirmNum
	, Gauger = G.FullName
	, OriginID = O.OriginID
	, Origin = O.Origin
	, OriginFull = O.OriginFull
	, OriginState = O.OriginState
	, OriginStateAbbrev = O.OriginStateAbbrev
	, CustomerID = O.CustomerID
	, ShipperID = O.CustomerID
	, Shipper = O.Customer
	, DestinationID = O.DestinationID
	, Destination = O.Destination
	, DestinationFull = O.DestinationFull
	, DestinationState = O.DestinationState
	, DestinationStateAbbrev = O.DestinationStateAbbrev
	, ProductID = O.ProductID
	, Product = O.Product
	, ProductShort = O.ProductShort
	, ProductGroupID = O.ProductGroupID
	, ProductGroup = O.ProductGroup
	, OriginTankText = ISNULL(OT.TankNum, GAO.OriginTankNum)
	, P.PriorityNum
	, TicketType = GTT.Name
	, PrintStatusID = O.PrintStatusID
	, OrderStatusID = O.StatusID
	, DeleteDateUTC = O.DeleteDateUTC
	, DeletedByUser = O.DeletedByUser
FROM dbo.tblGaugerOrder GAO 
JOIN viewOrder O ON O.ID = GAO.OrderID 
JOIN dbo.tblGaugerOrderStatus GOS ON GOS.ID = GAO.StatusID
LEFT JOIN dbo.tblOriginTank OT ON OT.ID = GAO.OriginTankID
LEFT JOIN dbo.viewGauger G ON G.ID = GAO.GaugerID
LEFT JOIN dbo.tblGaugerTicketType GTT ON GTT.ID = GAO.TicketTypeID
LEFT JOIN tblPriority P ON P.ID = GAO.PriorityID
GO
GRANT SELECT ON viewGaugerOrder TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 18 Apr 2015
-- Author: Kevin Alons
-- Purpose: return Order records with optional GAUGER data included
/***********************************/
CREATE VIEW viewOrderWithGauger AS
	SELECT O.*
	, GaugerStatus = GAO.Status
	, GaugerArriveTimeUTC = GAO.ArriveTimeUTC
	, GaugerDepartTimeUTC = GAO.DepartTimeUTC
	, GaugerNotes = GAO.GaugerNotes
	, Gauger = GAO.Gauger
	, GaugerCarrierTicketNum = GAO.CarrierTicketNum
	, GaugerHandwritten = GAO.Handwritten
	, GaugerPrintDateUTC = GAO.PrintDateUTC
	, GaugerRejected = GAO.Rejected
	, GaugerRejectReasonID = GAO.RejectReasonID
	, GaugerRejectNotes = GAO.RejectNotes
	, GaugerCreateDateUTC = GAO.CreateDateUTC
	, GaugerCreatedByUser = GAO.CreatedByUser
	, GaugerLastChangeDateUTC = GAO.LastChangeDateUTC
	, GaugerLastChangedByUser = GAO.LastChangedByUser
	, GaugerOriginTankID = GAO.OriginTankID
	, GaugerOriginTankText = GAO.OriginTankText
	, GaugerPriorityNum = GAO.PriorityNum
	, GaugerTicketType = GAO.TicketType
	FROM viewOrder O
	LEFT JOIN viewGaugerOrder GAO ON GAO.OrderID = O.ID
GO
GRANT SELECT ON viewOrderWithGauger TO dispatchcrude_iis_acct
GO

UPDATE tblReportColumnDefinition SET FilterDropDownSql = 'SELECT ID, Name=OrderStatus FROM viewOrderPrintStatus ORDER BY StatusNum' WHERE ID = 12
GO

EXEC _spDropFunction 'fnOrderReadOnly_GaugerApp'
GO
/*******************************************/
-- Date Created: 13 Apr 2015
-- Author: Kevin Alons
-- Purpose: return readonly Order data for Gauger App sync
/*******************************************/
CREATE FUNCTION fnOrderReadOnly_GaugerApp(@GaugerID int, @LastChangeDateUTC datetime) RETURNS TABLE AS
RETURN 
	SELECT O.ID
		, O.OrderNum
		, GAO.StatusID
		, GAO.TicketTypeID
		, GAO.GaugerID
		, PriorityNum = cast(P.PriorityNum as int) 
		, O.Product
		, GAO.DueDate
		, O.Origin
		, O.OriginFull
		, OO.OriginType
		, O.OriginUomID
		, OriginStation = OO.Station 
		, OriginLeaseNum = OO.LeaseNum 
		, OriginCounty = OO.County 
		, OriginLegalDescription = OO.LegalDescription 
		, O.OriginNDIC
		, O.OriginNDM
		, O.OriginCA
		, O.OriginState
		, OriginAPI = OO.WellAPI
		, OriginLat = OO.LAT 
		, OriginLon = OO.LON 
		, OriginGeoFenceRadiusMeters = OO.GeoFenceRadiusMeters 
		, O.CreateDateUTC
		, O.CreatedByUser
		, O.LastChangeDateUTC
		, O.LastChangedByUser
		, DeleteDateUTC = isnull(GOVD.VirtualDeleteDateUTC, O.DeleteDateUTC) 
		, DeletedByUser = isnull(GOVD.VirtualDeletedByUser, O.DeletedByUser) 
		, O.OriginID
		, PriorityID = cast(O.PriorityID AS int) 
		, O.Operator
		, O.OperatorID
		, O.Pumper
		, O.PumperID
		, O.Producer
		, O.ProducerID
		, O.Customer
		, O.CustomerID
		, O.ProductID
		, TicketType = GTT.Name
		, PrintHeaderBlob = S.ZPLHeaderBlob 
		, EmergencyInfo = isnull(S.EmergencyInfo, 'For an emergency (spill, leak, fire, or accident) contact CHEMTREC @ 800.424.9300 (toll free)') 
		, O.OriginTankID
		, O.OriginTankNum
		, GAO.DispatchNotes
		, O.CarrierTicketNum
		, O.DispatchConfirmNum
		, OriginTimeZone = OO.TimeZone
		, OCTM.OriginThresholdMinutes
		, ShipperHelpDeskPhone = S.HelpDeskPhone
	FROM dbo.viewOrder O
	JOIN dbo.tblGaugerOrder GAO ON GAO.OrderID = O.ID
	LEFT JOIN tblGaugerTicketType GTT ON GTT.ID = GAO.TicketTypeID
	JOIN dbo.tblPriority P ON P.ID = O.PriorityID
	JOIN dbo.viewOrigin OO ON OO.ID = O.OriginID
	JOIN dbo.viewDestination D ON D.ID = O.DestinationID
	JOIN dbo.tblCustomer S ON S.ID = O.CustomerID
	JOIN dbo.tblRoute R ON R.ID = O.RouteID
	LEFT JOIN tblGaugerOrderVirtualDelete GOVD ON GOVD.OrderID = O.ID AND GOVD.GaugerID = @GaugerID
	CROSS JOIN (SELECT DATEADD(second, -5, @LastChangeDateUTC) AS LCD) LCD
	OUTER APPLY dbo.fnOrderCombinedThresholdMinutes(O.ID) OCTM
	WHERE O.ID IN (
		SELECT OrderID FROM tblGaugerOrder WHERE GaugerID = @GaugerID
		UNION 
		SELECT OrderID FROM tblGaugerOrderVirtualDelete WHERE GaugerID = @GaugerID
	)
	  AND (@LastChangeDateUTC IS NULL
		OR O.CreateDateUTC >= LCD.LCD
		OR O.LastChangeDateUTC >= LCD.LCD
		OR OO.DeleteDateUTC >= LCD.LCD
		OR GOVD.VirtualDeleteDateUTC >= LCD.LCD)
GO
GRANT SELECT ON fnOrderReadOnly_GaugerApp TO dispatchcrude_iis_acct
GO

EXEC _spDropFunction 'fnGaugerOrder_GaugerApp'
GO
/*******************************************/
-- Date Created: 31 Aug 2013
-- Author: Kevin Alons
-- Purpose: return driver editable Order data for Driver App sync
/*******************************************/
CREATE FUNCTION fnGaugerOrder_GaugerApp(@GaugerID int, @LastChangeDateUTC datetime) RETURNS TABLE AS
RETURN 
	SELECT O.OrderID
		, O.StatusID
		, O.ArriveTimeUTC
		, O.DepartTimeUTC
		, O.Rejected
		, O.RejectReasonID
		, O.RejectNotes
		, O.OriginTankID
		, O.OriginTankNum
		, O.GaugerNotes
		, O.DueDate
		, O.PriorityID
		, O.CreateDateUTC, O.CreatedByUser
		, O.LastChangeDateUTC, O.LastChangedByUser
		, DeleteDateUTC = isnull(GOVD.VirtualDeleteDateUTC, OO.DeleteDateUTC), DeletedByUser = isnull(GOVD.VirtualDeletedByUser, OO.DeletedByUser)
	FROM dbo.tblGaugerOrder O
	JOIN dbo.tblOrder OO ON OO.ID = O.OrderID AND OO.StatusID IN (-9)
	LEFT JOIN tblGaugerOrderVirtualDelete GOVD ON GOVD.OrderID = O.OrderID AND GOVD.GaugerID = @GaugerID
	CROSS JOIN (SELECT DATEADD(second, -5, isnull(@LastChangeDateUTC, dateadd(day, -30, getutcdate()))) AS LCD) LCD
	WHERE O.OrderID IN (
		SELECT OrderID FROM tblGaugerOrder WHERE GaugerID = @GaugerID
		UNION 
		SELECT OrderID FROM tblGaugerOrderVirtualDelete WHERE GaugerID = @GaugerID
	)
	  AND (@LastChangeDateUTC IS NULL
		OR O.CreateDateUTC >= LCD.LCD
		OR O.LastChangeDateUTC >= LCD.LCD
		OR OO.DeleteDateUTC >= LCD.LCD
		OR GOVD.VirtualDeleteDateUTC >= LCD.LCD)
GO
GRANT SELECT ON fnGaugerOrder_GaugerApp TO dispatchcrude_iis_acct
GO

/*******************************************/
-- Date Created: 13 Apr 2015
-- Author: Kevin Alons
-- Purpose: return GaugerOrderTicket data for Gauger App sync
/*******************************************/
CREATE FUNCTION fnGaugerOrderTicket_GaugerApp(@GaugerID int, @LastChangeDateUTC datetime) RETURNS TABLE AS
RETURN SELECT OT.UID
		, OT.OrderID
		, OT.CarrierTicketNum
		, OT.OriginTankID
		, OT.DispatchConfirmNum
		, OT.TankNum
		, OT.TicketTypeID
		, OT.ProductObsGravity
		, OT.ProductObsTemp
		, OT.ProductBSW
		, cast(OT.OpeningGaugeFeet as tinyint) AS OpeningGaugeFeet
		, cast(OT.OpeningGaugeInch as tinyint) AS OpeningGaugeInch
		, cast(OT.OpeningGaugeQ as tinyint) AS OpeningGaugeQ
		, OT.BottomFeet
		, OT.BottomInches
		, OT.BottomQ
		, OT.Rejected
		, OT.RejectReasonID
		, OT.RejectNotes
		, OT.SealOff
		, OT.SealOn
		, OT.CreateDateUTC
		, OT.CreatedByUser
		, OT.LastChangeDateUTC
		, OT.LastChangedByUser
		, OT.DeleteDateUTC
		, OT.DeletedByUser
	FROM dbo.tblGaugerOrderTicket OT
	JOIN dbo.tblGaugerOrder O ON O.OrderID = OT.OrderID
	JOIN dbo.tblOrder OO ON OO.ID = O.OrderID AND OO.StatusID IN (-9)
	LEFT JOIN tblGaugerOrderVirtualDelete GOVD ON GOVD.OrderID = O.OrderID AND GOVD.GaugerID = @GaugerID
	CROSS JOIN (SELECT DATEADD(second, -5, @LastChangeDateUTC) AS LCD) LCD
	WHERE O.OrderID IN (
		SELECT OrderID FROM tblGaugerOrder WHERE GaugerID = @GaugerID
		UNION 
		SELECT OrderID FROM tblGaugerOrderVirtualDelete WHERE GaugerID = @GaugerID
	)
	  AND (@LastChangeDateUTC IS NULL
		OR O.CreateDateUTC >= LCD.LCD
		OR O.LastChangeDateUTC >= LCD.LCD
		OR OO.DeleteDateUTC >= LCD.LCD
		OR GOVD.VirtualDeleteDateUTC >= LCD.LCD)
GO
GRANT SELECT ON fnGaugerOrderTicket_GaugerApp TO dispatchcrude_iis_acct
GO

/*******************************************/
-- Date Created: 13 Apr 2015
-- Author: Kevin Alons
-- Purpose: validate parameters, if valid insert/update the tblGaugerSync table for the specified GaugerID
/*******************************************/
CREATE PROCEDURE spGaugerSync
(
  @UserName varchar(100)
, @GaugerID int
, @AppVersion varchar(25) = NULL
, @SyncDateUTC datetime = NULL
, @PasswordHash varchar(25) = NULL
, @Valid bit = NULL output
, @Message varchar(255) = NULL output
) AS
BEGIN
	-- if resetting, delete the entire record (it will be recreated below)
	IF (@SyncDateUTC IS NULL)
	BEGIN
		DELETE FROM tblGaugerSync WHERE GaugerID = @GaugerID
		-- result of count(*) query will be 1 or 0 (true or false respectively)
		SELECT @Valid = (SELECT COUNT(*) FROM tblGauger WHERE ID = @GaugerID)
		IF (@Valid = 0)
			SELECT @Message = 'GaugerID was not valid'
	END
	ELSE
	BEGIN
		-- query the ValidatePasswordHash setting parameter
		DECLARE @ValidatePasswordHash bit
		SELECT @ValidatePasswordHash = CASE WHEN Value IN ('1', 'true', 'yes') THEN 1 ELSE 0 END
		FROM tblSetting
		WHERE ID = 47

		-- result of count(*) query will be 1 or 0 (true or false respectively)
		SELECT @Valid = (SELECT count(*) FROM tblGaugerSync WHERE GaugerID = @GaugerID 
			AND (@ValidatePasswordHash = 0 OR PasswordHash = @PasswordHash))
		IF (@Valid = 0)
			SELECT @Message = 'PasswordHash was not valid'
	END
	
	IF (@Valid = 1)
	BEGIN
		-- if a sync record already exists, just update the new LastSync value
		UPDATE tblGaugerSync SET LastSyncUTC = @SyncDateUTC, AppVersion = @AppVersion WHERE GaugerID = @GaugerID
		-- otherwise insert a new record with a new passwordhash value
		INSERT INTO tblGaugerSync (GaugerID, AppVersion, LastSyncUTC, PasswordHash)
			SELECT @GaugerID, @AppVersion, NULL, dbo.fnGeneratePasswordHash()
			FROM tblGauger D
			LEFT JOIN tblGaugerSync DS ON DS.GaugerID = D.ID
			WHERE D.ID = @GaugerID AND DS.GaugerID IS NULL

		-- return the current "Master" data
		SELECT * FROM dbo.fnGaugerMasterData(@Valid, @GaugerID, @UserName)
	END
	ELSE
	BEGIN
		SELECT Valid = @Valid, UserName = @UserName, GaugerID = 0, GaugerName = NULL, MobilePrint = 0
			, LastSyncUTC = NULL 
			, SyncMinutes = (SELECT cast(Value as int) FROM tblSetting WHERE ID = 45) 
			, SchemaVersion = (SELECT Value FROM tblSetting WHERE ID = 0) 
			, AppVersion = @AppVersion 
			, LatestAppVersion = (SELECT Value FROM tblSetting WHERE ID = 46)
			, PasswordHash = NULL 
		FROM tblSetting S WHERE S.ID = 0
	END 
END
GO
GRANT EXECUTE ON spGaugerSync TO dispatchcrude_iis_acct
GO

/*******************************************/
-- Date Created: 13 Apr 2015
-- Author: Kevin Alons
-- Purpose: provide a way to reset the existing Password Hash value for a Gauger (when the acct password is changed)
/*******************************************/
CREATE PROCEDURE spGauger_ResetPasswordHash
(
  @UserName varchar(100)
, @GaugerID int
, @Valid bit = NULL output
, @Message varchar(255) = NULL output
) AS BEGIN
	SELECT @Valid = (SELECT COUNT(*) FROM tblGauger WHERE ID = @GaugerID)
	IF (@Valid = 0)
		SELECT @Message = 'GaugerID was not valid'
	ELSE BEGIN
		-- if a sync record already exists, just update the new LastSync value
		UPDATE tblGaugerSync SET PasswordHash = dbo.fnGeneratePasswordHash() WHERE GaugerID = @GaugerID
		-- return the current "Master" data
		SELECT * FROM dbo.fnGaugerMasterData(@Valid, @GaugerID, @UserName)
	END		
END
GO
GRANT EXECUTE ON spGauger_ResetPasswordHash TO dispatchcrude_iis_acct
GO

CREATE PROCEDURE spDriverAppLogSyncError(@driverID int, @errorMsg varchar(max)) AS
BEGIN
	INSERT INTO tblDriverAppSyncError (DriverID, ErrorMsg) VALUES (@driverID, @errorMsg)	
END
GO
GRANT EXECUTE ON spDriverAppLogSyncError TO dispatchcrude_iis_acct
GO

CREATE TABLE dbo.tblGaugerAppSyncError
(
	ID int IDENTITY(1,1) NOT NULL constraint PK_GaugerAppSyncError primary key ,
	GaugerID int NOT NULL constraint FK_GaugerAppSyncError_Gauger foreign key references tblGauger(ID),
	CreateDateUTC datetime NULL constraint DF_GaugerAppSyncError_CreateDate DEFAULT (getutcdate()),
	ErrorMsg varchar(max) NULL,
) 
GO
GRANT INSERT ON tblGaugerAppSyncError TO dispatchcrude_iis_acct
GO

CREATE PROCEDURE spGaugerAppLogSyncError(@driverID int, @errorMsg varchar(max)) AS
BEGIN
	INSERT INTO tblGaugerAppSyncError (GaugerID, ErrorMsg) VALUES (@driverID, @errorMsg)	
END
GO
GRANT EXECUTE ON spGaugerAppLogSyncError TO dispatchcrude_iis_acct
GO

ALTER TABLE tblOrigin ADD GaugerID int null constraint FK_Origin_Gauger FOREIGN KEY REFERENCES tblGauger(ID)
GO
ALTER TABLE tblOrigin ADD GaugerTicketTypeID int null constraint FK_Origin_GaugerTicketType FOREIGN KEY REFERENCES tblGaugerTicketType(ID)
GO
ALTER TABLE tblOrigin ADD GaugerRequired bit NOT NULL constraint DF_Origin_GaugerRequired DEFAULT (0)
GO

EXEC _spRebuildAllObjects
GO

/***********************************/
-- Date Created: 12 Mar 2014
-- Author: Kevin Alons
-- Purpose: return Origin Tank records with translated values
/***********************************/
ALTER VIEW viewOriginTank AS
	SELECT OT.*
		, Origin = O.Name
		, OriginLeaseNum = O.LeaseNum
		, TankNum_Unstrapped = CASE WHEN OT.TankNum = '*' THEN '[Enter Details]' ELSE OT.TankNum END 
		, Unstrapped = CASE WHEN OT.TankNum = '*' THEN 1 ELSE 0 END
		, ForGauger = CASE WHEN O.GaugerTicketTypeID IS NULL THEN 0 ELSE 1 END
	FROM dbo.tblOriginTank OT
	JOIN dbo.viewOrigin O ON O.ID = OT.OriginID
GO

/***********************************/
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: create new Order (loads) for the specified criteria
/***********************************/
ALTER PROCEDURE spCreateLoads
(
  @OriginID int
, @DestinationID int
, @TicketTypeID int
, @DueDate datetime
, @CustomerID int
, @CarrierID int = NULL
, @DriverID int = NULL
, @Qty int
, @UserName varchar(100)
, @OriginTankID int = NULL
, @OriginTankNum varchar(20) = NULL
, @OriginBOLNum varchar(15) = NULL
, @PriorityID int = 3 -- LOW priority
, @StatusID smallint = -10 -- GENERATED
, @ProductID int = 1 -- basic Crude Oil
, @DispatchConfirmNum varchar(30) = NULL
, @IDs_CSV varchar(max) = NULL OUTPUT
, @count int = 0 OUTPUT
) AS
BEGIN
	DECLARE @PumperID int, @OperatorID int, @ProducerID int, @OriginUomID int, @DestUomID int
	SELECT @PumperID = PumperID, @OperatorID = OperatorID, @ProducerID = ProducerID, @OriginUomID = UomID
	FROM tblOrigin 
	WHERE ID = @OriginID
	SELECT @DestUomID = UomID FROM tblDestination WHERE ID = @DestinationID

	DECLARE @incrementBOL bit
	SELECT @incrementBOL = CASE WHEN @OriginBOLNum LIKE '%+' THEN 1 ELSE 0 END
	IF (@incrementBOL = 1) SELECT @OriginBOLNum = left(@OriginBOLNum, len(@OriginBOLNum) - 1)
	
	DECLARE @i int, @id int
	SELECT @i = 0, @IDs_CSV = ''
	
	WHILE @i < @Qty BEGIN
		
		INSERT INTO dbo.tblOrder (OriginID, OriginUomID, DestinationID, DestUomID, TicketTypeID, DueDate, CustomerID
				, CarrierID, DriverID, StatusID, PriorityID, OriginTankID, OriginTankNum, OriginBOLNum
				, OrderNum, ProductID, PumperID, OperatorID, ProducerID, DispatchConfirmNum
				, CreateDateUTC, CreatedByUser)
			VALUES (@OriginID, @OriginUomID, @DestinationID, @DestUomID, @TicketTypeID, @DueDate, @CustomerID
				, @CarrierID, @DriverID, @StatusID, @PriorityID, @OriginTankID, @OriginTankNum, @OriginBOLNum
				, (SELECT isnull((SELECT max(OrderNum) FROM tblOrder), 100000) + 1), @ProductID
				, @PumperID, @OperatorID, @ProducerID, @DispatchConfirmNum
				, GETUTCDATE(), @UserName)
		
		SELECT @IDs_CSV = @IDs_CSV + LTRIM(SCOPE_IDENTITY()), @count = @count + 1
		
		IF (@incrementBOL = 1 AND isnumeric(@OriginBOLNum) = 1) SELECT @OriginBOLNum = rtrim(cast(@OriginBOLNum as int) + 1)

		SET @i = @i + 1
	END
	IF (@DriverID IS NOT NULL)
	BEGIN
		UPDATE tblOrder SET TruckID = D.TruckID, TrailerID = D.TrailerID, Trailer2ID = D.Trailer2ID
		FROM tblOrder O
		JOIN tblDriver D ON D.ID = O.DriverID
		WHERE O.ID IN (SELECT ID FROM dbo.fnSplitCSVIDs(@IDs_CSV))
	END
END
GO

EXEC _spDropProcedure 'spCreateGaugerLoads'
GO
/***********************************/
-- Date Created: 18 Apr 2015
-- Author: Kevin Alons
-- Purpose: create new Gauger Order (loads) for the specified criteria
/***********************************/
CREATE PROCEDURE spCreateGaugerLoads
(
  @OriginID int
, @DestinationID int
, @GaugerTicketTypeID int
, @DueDate datetime
, @OriginTankID int
, @ProductID int
, @UserName varchar(100)
, @GaugerID int = NULL
, @Qty int = 1
, @PriorityID int = 3 -- LOW priority
, @DispatchConfirmNum varchar(30) = NULL
, @IDs_CSV varchar(max) = NULL OUTPUT
, @count int = 0 OUTPUT
) AS
BEGIN
	DECLARE @TicketTypeID int, @ShipperID int
	SELECT @TicketTypeID = TicketTypeID FROM tblGaugerTicketType WHERE ID = @GaugerTicketTypeID
	SELECT @ShipperID = CustomerID FROM tblOrigin WHERE ID = @OriginID
	EXEC spCreateLoads @StatusID=-9, @OriginID=@OriginID, @OriginTankID=@OriginTankID, @ProductID=@ProductID, @DestinationID=@DestinationID
		, @CustomerID=@ShipperID, @PriorityID=@PriorityID, @TicketTypeID=@TicketTypeID, @DispatchConfirmNum=@DispatchConfirmNum
		, @DueDate=@DueDate, @Qty=@Qty, @UserName=@UserName
		, @IDs_CSV=@IDs_CSV OUTPUT
		, @count=@count OUTPUT
	
	INSERT INTO tblGaugerOrder (OrderID, TicketTypeID, StatusID, OriginTankID, OriginTankNum, GaugerID
		, Rejected, Handwritten, DueDate, PriorityID, CreateDateUTC, CreatedByUser)
		SELECT ID, @GaugerTicketTypeID, CASE WHEN @GaugerID IS NULL THEN 1 ELSE 2 END, @OriginTankID, NULL, @GaugerID
			, 0, 0, @DueDate, @PriorityID, getutcdate(), @UserName
		FROM tblOrder 
		WHERE ID IN (SELECT ID FROM dbo.fnSplitCSVIDs(@IDs_CSV))
END
GO
GRANT EXECUTE ON spCreateGaugerLoads TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 18 Apr 2015
-- Author: Kevin Alons
-- Purpose: return the gauger orders be displayed on the GuagerOrder Creation page
/***********************************/
CREATE PROCEDURE spGaugerOrderCreationSource
(
  @RegionID int = -1
) AS BEGIN
	SELECT Selected = cast(0 as bit)
		, OriginIDZ = isnull(OriginID, 0)
		, * 
	FROM dbo.viewGaugerOrder O
	WHERE (@RegionID = -1
		OR OriginID IN (SELECT ID FROM tblOrigin WHERE RegionID = @RegionID) 
		OR DestinationID IN (SELECT ID FROM tblDestination WHERE RegionID = @RegionID)) 
	AND OrderStatusID IN (-9) 
	AND DeleteDateUTC IS NULL 
	ORDER BY PriorityNum, StatusID DESC, Origin, Destination, DueDate
END
GO
GRANT EXECUTE ON spGaugerOrderCreationSource TO dispatchcrude_iis_acct
GO

ALTER TABLE tblOrderSignature ADD GaugerID int null constraint FK_OrderSignature_Gauger FOREIGN KEY REFERENCES tblGauger(ID)
GO

update tblorigin set GaugerTicketTypeID = 1 where origintypeid in (select distinct id from tblorigintype where hastanks = 1)
go

/*******************************************/
-- Date Created: 5 Apr 2014
-- Author: Kevin Alons
-- Purpose: return OriginTank data for Gauger App sync
/*******************************************/
CREATE FUNCTION fnOriginTank_GaugerApp(@GaugerID int, @LastChangeDateUTC datetime) RETURNS TABLE AS
RETURN 
	SELECT DISTINCT OT.ID
		, OT.OriginID
		, OT.TankNum
		, OT.TankDescription
		, OT.CreateDateUTC
		, OT.CreatedByUser
		, OT.LastChangeDateUTC
		, OT.LastChangedByUser
		, OT.DeleteDateUTC
		, OT.DeletedByUser
	FROM dbo.tblOriginTank OT
	JOIN dbo.tblOrder O ON O.OriginID = OT.OriginID
	LEFT JOIN tblGaugerOrderVirtualDelete GOVD ON GOVD.OrderID = O.ID AND GOVD.GaugerID = @GaugerID
	CROSS JOIN (SELECT DATEADD(second, -5, @LastChangeDateUTC) AS LCD) LCD
	WHERE O.ID IN (
		SELECT OrderID FROM tblGaugerOrder WHERE GaugerID = @GaugerID
		UNION 
		SELECT OrderID FROM tblGaugerOrderVirtualDelete WHERE GaugerID = @GaugerID
	)
	  AND (@LastChangeDateUTC IS NULL
		OR O.CreateDateUTC >= LCD.LCD
		OR O.LastChangeDateUTC >= LCD.LCD
		OR GOVD.VirtualDeleteDateUTC >= LCD.LCD
		OR OT.CreateDateUTC >= LCD.LCD
		OR OT.LastChangeDateUTC >= LCD.LCD
		)
GO
GRANT SELECT ON fnOriginTank_GaugerApp TO dispatchcrude_iis_acct
GO

/*******************************************/
-- Date Created: 20 Apr 2015
-- Author: Kevin Alons
-- Purpose: return OriginTankStrapping data for Gauger App sync
/*******************************************/
CREATE FUNCTION fnOriginTankStrapping_GaugerApp(@GaugerID int, @LastChangeDateUTC datetime) RETURNS TABLE AS
RETURN 
	SELECT DISTINCT OTS.*
		, cast(CASE WHEN OTSD.ID IS NULL THEN 0 ELSE 1 END as bit) AS IsDeleted
	FROM dbo.tblOriginTankStrapping OTS
	JOIN dbo.tblOriginTank OT ON OT.ID = OTS.OriginTankID
	JOIN dbo.tblOrder O ON O.OriginID = OT.OriginID
	LEFT JOIN tblGaugerOrderVirtualDelete GOVD ON GOVD.OrderID = O.ID AND GOVD.GaugerID = @GaugerID
	LEFT JOIN dbo.tblOriginTankStrappingDeleted OTSD ON OTSD.ID = OTS.ID
	CROSS JOIN (SELECT DATEADD(second, -5, @LastChangeDateUTC) AS LCD) LCD
	WHERE O.ID IN (
		SELECT OrderID FROM tblGaugerOrder WHERE GaugerID = @GaugerID
		UNION 
		SELECT OrderID FROM tblGaugerOrderVirtualDelete WHERE GaugerID = @GaugerID
	)
	  AND (@LastChangeDateUTC IS NULL
		OR O.CreateDateUTC >= LCD.LCD
		OR O.LastChangeDateUTC >= LCD.LCD
		OR GOVD.VirtualDeleteDateUTC >= LCD.LCD
		OR OT.CreateDateUTC >= LCD.LCD
		OR OT.LastChangeDateUTC >= LCD.LCD
		OR OTS.CreateDateUTC >= LCD.LCD
		OR OTS.LastChangeDateUTC >= LCD.LCD
		OR OTSD.DeleteDateUTC >= LCD.LCD)
GO
GRANT SELECT ON fnOriginTankStrapping_GaugerApp TO dispatchcrude_iis_acct
GO

COMMIT
SET NOEXEC OFF