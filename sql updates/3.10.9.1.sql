-- rollback
SET NOEXEC OFF
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.10.9'
SELECT  @NewVersion = '3.10.9.1'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'WORKAROUND: always PUSH all order rules to Driver App - to temporarily resolve issue where sometimes missing on app'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/*******************************************
 Date Created: 1 Mar 2015
 Author: Kevin Alons
 Purpose: return Order Rules for every changed Order
 - 3.10.5 - 2016/01/29 - KDA - use tblOrderAppChanges to only include records when Order.OriginID not any Order change
 - 3.10.9.1 - 2016/02/18 - KDA	- WORKAROUND: eliminate any filtering - temporarily ALWAYS PUSH all OrderRules to Driver App on every sync
*******************************************/
ALTER FUNCTION fnOrderRules_DriverApp(@DriverID int, @LastChangeDateUTC datetime) 
RETURNS 
	@ret TABLE (
		ID int
	  , OrderID int
	  , TypeID int
	  , Value varchar(255)
	)
AS BEGIN
	DECLARE @IDS TABLE (ID int)
	INSERT INTO @IDS
	/* WORKAROUND: commented out to always end all order rules (to eliminate the possibility of missing Rules on driver app)
		SELECT O.ID
		FROM dbo.tblOrder O
		JOIN dbo.tblOrigin OO ON OO.ID = O.OriginID
		JOIN dbo.tblDestination D ON D.ID = O.DestinationID
		JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
		JOIN dbo.tblRoute R ON R.ID = O.RouteID
		JOIN dbo.tblCarrier CA ON CA.ID = O.CarrierID
		LEFT JOIN tblOrderAppChanges OAC ON OAC.OrderID = O.ID
		CROSS JOIN fnSyncLCDOffset(@LastChangeDateUTC) LCD
		WHERE O.ID IN (
	*/ 
			SELECT ID FROM tblOrder WHERE DriverID = @driverID AND StatusID IN (2,7,8,3) 
/*		)		WORKAROUND: also commented out
		  AND (
			@LastChangeDateUTC IS NULL 
			OR O.CreateDateUTC >= LCD.LCD
			OR OAC.OrderRuleChangeDateUTC >= LCD.LCD
			OR O.DeleteDateUTC >= LCD.LCD
			OR C.LastChangeDateUTC >= LCD.LCD
			OR CA.LastChangeDateUTC >= LCD.LCD
			OR OO.LastChangeDateUTC >= LCD.LCD
			OR R.LastChangeDateUTC >= LCD.LCD)
*/
	DECLARE @ID int
	WHILE EXISTS (SELECT * FROM @IDS)
	BEGIN
		SELECT TOP 1 @ID = ID FROM @IDS
		INSERT INTO @ret (ID, OrderID, TypeID, Value)
			SELECT ID, @ID, TypeID, Value FROM dbo.fnOrderOrderRules(@ID)
		DELETE FROM @IDS WHERE ID = @ID
		-- if no records exist, then return a NULL record to tell the Mobile App to delete any existing records for this order
		IF NOT EXISTS (SELECT * FROM @ret WHERE OrderID = @ID)
			INSERT INTO @ret (OrderID) VALUES (@ID)
	END
	RETURN
END
GO

COMMIT
SET NOEXEC OFF