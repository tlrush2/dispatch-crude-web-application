/* 
  add additional fields to viewOrderExportFull/spOrdersFullExport to allow all Ticket History report be present on Order History report
  - add [fnSplitCSVIDs]
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.3.1'
SELECT  @NewVersion = '2.3.2'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details into a single view
/***********************************/
ALTER VIEW [dbo].[viewOrder_OrderTicket_Full] AS 
	SELECT OE.* 
		, CASE WHEN OE.TicketCount = 0 THEN OE.TicketType ELSE OT.TicketType END AS T_TicketType
		, CASE WHEN OE.TicketCount = 0 THEN OE.CarrierTicketNum ELSE OT.CarrierTicketNum END AS T_CarrierTicketNum
		, isnull(CASE WHEN OE.TicketCount = 0 THEN OE.OriginBOLNum ELSE OT.BOLNum END, '') AS T_BOLNum
		, OT.TankNum AS T_TankNum
		, OT.OpeningGaugeFeet AS T_OpeningGaugeFeet, OT.OpeningGaugeInch AS T_OpeningGaugeInch, OT.OpeningGaugeQ AS T_OpeningGaugeQ
		, OT.ClosingGaugeFeet AS T_ClosingGaugeFeet, OT.ClosingGaugeInch AS T_ClosingGaugeInch, OT.ClosingGaugeQ AS T_ClosingGaugeQ
		, dbo.fnGaugeQtrInches(OT.OpeningGaugeFeet, OT.OpeningGaugeInch, OT.OpeningGaugeQ) AS T_OpenTotalQ
		, dbo.fnGaugeQtrInches(OT.ClosingGaugeFeet, OT.ClosingGaugeInch, ot.ClosingGaugeQ) AS T_CloseTotalQ
		, ltrim(OT.OpeningGaugeFeet) + 'ft ' + ltrim(OT.OpeningGaugeInch) + 'in ' + ltrim(OT.OpeningGaugeQ) + 'q' AS T_OpenReading
		, ltrim(OT.ClosingGaugeFeet) + 'ft ' + ltrim(OT.ClosingGaugeInch) + 'in ' + ltrim(OT.ClosingGaugeQ) + 'q' AS T_CloseReading
		-- using cast(xx as decimal(9,4)) to properly round to 3 decimal digits (with no trailing 0's)
		, ltrim(round(cast(dbo.fnCorrectedAPIGravity(OT.ProductObsGravity, OT.ProductObsTemp) as decimal(9,4)), 9, 4)) AS T_CorrectedAPIGravity
		, ltrim(OT.SealOff) AS T_SealOff
		, ltrim(OT.SealOn) AS T_SealOn
		, OT.ProductObsTemp AS T_ProductObsTemp
		, OT.ProductObsGravity AS T_ProductObsGravity
		, OT.ProductBSW AS T_ProductBSW
		, OT.Rejected AS T_Rejected
		, OT.RejectNotes AS T_RejectNotes
		, OT.GrossBarrels AS T_GrossBarrels
		, OT.NetBarrels AS T_NetBarrels
		, dbo.fnRerouteDetails(OE.ID, 'PreviousDestinations', '<br/>') AS PreviousDestinations
		, dbo.fnRerouteDetails(OE.ID, 'RerouteUsers', '<br/>') AS RerouteUsers
		, dbo.fnRerouteDetails(OE.ID, 'RerouteDates', '<br/>') AS RerouteDates
		, dbo.fnRerouteDetails(OE.ID, 'RerouteNotes', '<br/>') AS RerouteNotes
	FROM dbo.viewOrderExportFull OE
	LEFT JOIN viewOrderTicket OT ON OT.OrderID = OE.ID AND OT.DeleteDateUTC IS NULL
	WHERE OE.DeleteDateUTC IS NULL
	
GO

--The following is a general purpose UDF to split comma separated lists into individual items.
--Consider an additional input parameter for the delimiter, so that you can use any delimiter you like.
CREATE FUNCTION [dbo].[fnSplitCSVIDs]
(
	@IDList varchar(max)
)
RETURNS 
@ParsedList table
(
	ID int
)
AS
BEGIN
	IF (@IDList LIKE '%;%' OR @IDList LIKE '%GO%')
	BEGIN
		RETURN
	END

	DECLARE @ID varchar(10), @Pos int

	SET @IDList = LTRIM(RTRIM(@IDList))+ ','
	SET @Pos = CHARINDEX(',', @IDList, 1)

	IF REPLACE(@IDList, ',', '') <> ''
	BEGIN
		WHILE @Pos > 0
		BEGIN
			SET @ID = LTRIM(RTRIM(LEFT(@IDList, @Pos - 1)))
			IF @ID <> ''
			BEGIN
				INSERT INTO @ParsedList (ID) 
				VALUES (CAST(@ID AS int)) --Use Appropriate conversion
			END
			SET @IDList = RIGHT(@IDList, LEN(@IDList) - @Pos)
			SET @Pos = CHARINDEX(',', @IDList, 1)

		END
	END	
	RETURN
END

GO
GRANT SELECT ON fnSplitCSVIDs TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 28 Feb 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc for 3rd parties (Customer/Carrier)
/***********************************/
ALTER PROCEDURE [dbo].[spOrdersFullExport]
(
  @StartDate datetime
, @EndDate datetime
, @CarrierID int = 0 -- -1 = all carriers
, @CustomerID int = 0 -- -1 = all customers
, @ProducerID int = 0
, @StatusID_CSV varchar(max) = '4'
) AS BEGIN
	SELECT @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	
	SELECT * 
	FROM dbo.viewOrder_OrderTicket_Full OE
	WHERE (@CarrierID=-1 OR @CustomerID=-1 OR CarrierID=@CarrierID OR CustomerID=@CustomerID OR ProducerID=@ProducerID) 
	  AND OrderDate BETWEEN @StartDate AND @EndDate
	  AND DeleteDateUTC IS NULL
	  AND (StatusID IN (SELECT ID FROM dbo.fnSplitCSVIDs(@StatusID_CSV)))
	ORDER BY OrderDate, OrderNum
END

GO

EXEC _spRefreshAllViews
GO
EXEC _spRecompileAllStoredProcedures
GO

COMMIT
SET NOEXEC OFF