-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.9.19.2.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.9.19.2.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0

SET NOEXEC OFF
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.9.19.2'
SELECT  @NewVersion = '3.9.19.3'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'Settlement/Approval: use Approval.OverrideRouteMiles for Settlement Miles during rate application'
	UNION SELECT @NewVersion, 0, 'tblOrderApproval - make all XXX|LastChangeDate + CreateDate + DeleteDate UTC fields be datetime (vs smalldatetime)'
	UNION SELECT @NewVersion, 1, 'Driver App: Sync logic no longer users "LastChangeDateUTC" if later as XXXLastChangeDateUTC value during sync output'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RateSheetRangeRate info for the specified order
-- Changes:
	- 3.7.28 - 2015/06/18 - KDA - add usage of DriverID/DriverGroupID/ProducerID parameters
	- 3.7.30 - 2015/06/10 - KDA - remove DriverID
	- 3.9.19.3 - 2015/09/20 - KDA - use OrderApproval.OverrideActualMiles when present
***********************************/
ALTER FUNCTION fnOrderCarrierRateSheetRangeRate(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, RateTypeID, Rate, UomID
	FROM dbo.viewOrderBase O
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
	CROSS APPLY dbo.fnCarrierRateSheetRangeRate(O.OrderDate, NULL, isnull(OA.OverrideActualMiles, O.ActualMiles), O.CustomerID, O.CarrierID, O.ProductGroupID, O.DriverGroupID, O.OriginStateID, O.DestStateID, O.OriginRegionID, O.ProducerID, 1) R
	WHERE O.ID = @ID
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RateSheetRangeRate info for the specified order
	- 3.7.30 - 2015/06/10 - KDA - remove DriverID
	- 3.9.19.3 - 2015/09/20 - KDA - use OrderApproval.OverrideActualMiles when present
***********************************/
ALTER FUNCTION fnOrderCarrierLoadAmount(@ID int, @Units decimal(18, 10), @UomID int) RETURNS TABLE AS RETURN
	SELECT RouteRateID, RangeRateID
		, Amount = dbo.fnRateToAmount(RateTypeID, @Units, @UomID, Rate, UomID, NULL
			, CASE WHEN RateTypeID = 3 THEN (SELECT TOP 1 LoadAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID) ELSE NULL END
			, CASE WHEN RateTypeID = 5 THEN (
					SELECT TOP 1 isnull(OA.OverrideActualMiles, O.ActualMiles) 
					FROM tblOrder O 
					LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID 
					WHERE O.ID = @ID
				) ELSE NULL END
			)
	FROM (
		SELECT TOP 1 * 
		FROM (
			SELECT SortID = 1, RouteRateID = ID, RangeRateID = NULL, RateTypeID, UomID, Rate FROM dbo.fnOrderCarrierRouteRate(@ID)
			UNION SELECT 2, NULL, ID, RateTypeID, UomID, Rate FROM dbo.fnOrderCarrierRateSheetRangeRate(@ID)
		) X
		ORDER BY SortID
	) X
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper RateSheetRangeRate info for the specified order
	- 3.9.19.3 - 2015/09/20 - KDA - use OrderApproval.OverrideActualMiles when present
***********************************/
ALTER FUNCTION fnOrderShipperRateSheetRangeRate(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, RateTypeID, Rate, UomID
	FROM dbo.viewOrder O
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
	CROSS APPLY dbo.fnShipperRateSheetRangeRate(O.OrderDate, null, isnull(OA.OverrideActualMiles, O.ActualMiles), O.CustomerID, O.ProductGroupID, O.ProducerID, O.OriginStateID, O.DestStateID, O.OriginRegionID, 1) R
	WHERE O.ID = @ID
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper RateSheetRangeRate info for the specified order
	- 3.9.19.3 - 2015/09/20 - KDA - use OrderApproval.OverrideActualMiles when present
***********************************/
ALTER FUNCTION fnOrderShipperLoadAmount(@ID int, @Units decimal(18, 10), @UomID int) RETURNS TABLE AS RETURN
	SELECT TOP 1 RouteRateID, RangeRateID
		, Amount = dbo.fnRateToAmount(RateTypeID, @Units, @UomID, Rate, UomID, NULL, NULL
		, CASE WHEN RateTypeID = 5 THEN (
				SELECT TOP 1 isnull(OA.OverrideActualMiles, O.ActualMiles) 
				FROM tblOrder O 
				LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID 
				WHERE O.ID = @ID
			) ELSE NULL END
		)
	FROM (
		SELECT SortID = 1, RouteRateID = ID, RangeRateID = NULL, RateTypeID, UomID, Rate FROM dbo.fnOrderShipperRouteRate(@ID)
		UNION SELECT 2, NULL, ID, RateTypeID, UomID, Rate FROM dbo.fnOrderShipperRateSheetRangeRate(@ID)
	) X
	ORDER BY SortID
GO

/*******************************************
-- Date Created: 31 Aug 2013
-- Author: Kevin Alons
-- Purpose: return driver editable Order data for Driver App sync
-- Changes: 
    -- 3.8.9 - 07/24/15 - GSM Added TickeTypeID field (was read-only before)
	-- 3.7.4 - 05/08/15 - GSM Added Rack/Bay field
	-- 3.7.11 - 5/18/2015 - KDA - renamed RackBay to DestRackBay
	-- 3.9.19.3 - 2015/09/29 - GSM - remove transaction on Order.Accept|Pickup|Deliver LastChangeDateUTC fields (no longer use max of O.LastChangeDateUTC)
*******************************************/
ALTER FUNCTION fnOrderEdit_DriverApp(@DriverID int, @LastChangeDateUTC datetime) RETURNS TABLE AS
RETURN 
	SELECT O.ID
		, O.StatusID
		, O.TruckID
		, O.TrailerID
		, O.Trailer2ID
		, O.OriginBOLNum
		, O.OriginArriveTimeUTC
		, O.OriginDepartTimeUTC
		, O.OriginMinutes
		, O.OriginWaitReasonID
		, O.OriginWaitNotes
		, O.OriginTruckMileage
		, O.OriginGrossUnits
		, O.OriginGrossStdUnits
		, O.OriginNetUnits
		, O.ChainUp
		, O.Rejected
		, O.RejectReasonID
		, O.RejectNotes
		, O.OriginTankNum
		, O.DestArriveTimeUTC
		, O.DestDepartTimeUTC
		, O.DestMinutes
		, O.DestWaitReasonID
		, O.DestWaitNotes
		, O.DestBOLNum
		, O.DestTruckMileage
		, O.DestGrossUnits
		, O.DestNetUnits
		, O.DestProductTemp
		, O.DestProductBSW
		, O.DestProductGravity
		, O.DestOpenMeterUnits
		, O.DestCloseMeterUnits
		, O.CarrierTicketNum
		, O.AcceptLastChangeDateUTC 
		, O.PickupLastChangeDateUTC 
		, O.DeliverLastChangeDateUTC
		, O.PickupPrintStatusID
		, O.DeliverPrintStatusID
		, O.PickupPrintDateUTC
		, O.DeliverPrintDateUTC
		, O.PickupDriverNotes
		, O.DeliverDriverNotes
		, O.DestRackBay
		, O.TicketTypeID
	FROM dbo.tblOrder O
	LEFT JOIN tblOrderDriverAppVirtualDelete ODAVD ON ODAVD.OrderID = O.ID AND ODAVD.DriverID = @DriverID
	CROSS JOIN fnSyncLCDOffset(@LastChangeDateUTC) LCD
	WHERE O.ID IN (
		SELECT id FROM tblOrder WHERE DriverID = @driverID AND StatusID IN (2,7,8,3) 
		UNION 
		SELECT OrderID FROM tblOrderDriverAppVirtualDelete WHERE DriverID = @driverID
	)
	  AND (@LastChangeDateUTC IS NULL
		OR O.CreateDateUTC >= LCD.LCD
		OR O.LastChangeDateUTC >= LCD.LCD
		OR O.AcceptLastChangeDateUTC >= LCD.LCD
		OR O.PickupLastChangeDateUTC >= LCD.LCD
		OR O.DeliverLastChangeDateUTC >= LCD.LCD
		OR O.DeleteDateUTC >= LCD.LCD
		OR ODAVD.VirtualDeleteDateUTC >= LCD.LCD)
GO

--------------------------------------------
-- change all Accept|Pickup|Deliver+LastChangeDateUTC from SMALLDATETIME -> DATETIME (for full second resolution)
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_Operator
GO
ALTER TABLE dbo.tblOperator SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_Pickup_PrintStatus
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_Deliver_PrintStatus
GO
ALTER TABLE dbo.tblPrintStatus SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_Customer
GO
ALTER TABLE dbo.tblCustomer SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_tblOrder_tblProducer
GO
ALTER TABLE dbo.tblProducer SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_Carrier
GO
ALTER TABLE dbo.tblCarrier SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_Destination
GO
ALTER TABLE dbo.tblDestination SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_TicketType
GO
ALTER TABLE dbo.tblTicketType SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_Route
GO
ALTER TABLE dbo.tblRoute SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_Driver
GO
ALTER TABLE dbo.tblDriver SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_Pumper
GO
ALTER TABLE dbo.tblPumper SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_Priority
GO
ALTER TABLE dbo.tblPriority SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_OrderStatus
GO
ALTER TABLE dbo.tblOrderStatus SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_Truck
GO
ALTER TABLE dbo.tblTruck SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_RejectReason
GO
ALTER TABLE dbo.tblOrderRejectReason SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_DestWaitReason
GO
ALTER TABLE dbo.tblDestinationWaitReason SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_Trailer
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_Trailer2
GO
ALTER TABLE dbo.tblTrailer SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_OriginWaitReason
GO
ALTER TABLE dbo.tblOriginWaitReason SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_Origin
GO
ALTER TABLE dbo.tblOrigin SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_OriginUom
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_DestUom
GO
ALTER TABLE dbo.tblUom SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT DF_tblOrders_StatusID
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT DF_tblOrder__TicketTicketType
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT DF_tblOrder_Rejected
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT DF_tblTransactions_ChainUp
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT DF_Order_CreateDateUTC
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT DF_Order_Product
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT DF_Order_OriginUomID
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT DF_Order_DestUomID
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT DF_Order_PickupPrintStatus
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT DF_Order_DeliverPrintStatus
GO
CREATE TABLE dbo.Tmp_tblOrder
	(
	ID int NOT NULL IDENTITY (1, 1),
	OrderNum int NULL,
	StatusID int NOT NULL,
	PriorityID tinyint NULL,
	DueDate date NOT NULL,
	RouteID int NULL,
	OriginID int NULL,
	OriginArriveTimeUTC datetime NULL,
	OriginDepartTimeUTC datetime NULL,
	OriginMinutes int NULL,
	OriginWaitNotes varchar(255) NULL,
	OriginBOLNum varchar(15) NULL,
	OriginGrossUnits decimal(9, 3) NULL,
	OriginNetUnits decimal(9, 3) NULL,
	DestinationID int NULL,
	DestArriveTimeUTC datetime NULL,
	DestDepartTimeUTC datetime NULL,
	DestMinutes int NULL,
	DestWaitNotes varchar(255) NULL,
	DestBOLNum varchar(30) NULL,
	DestGrossUnits decimal(9, 3) NULL,
	DestNetUnits decimal(9, 3) NULL,
	CustomerID int NULL,
	CarrierID int NULL,
	DriverID int NULL,
	TruckID int NULL,
	TrailerID int NULL,
	Trailer2ID int NULL,
	OperatorID int NULL,
	PumperID int NULL,
	TicketTypeID int NOT NULL,
	Rejected bit NOT NULL,
	RejectNotes varchar(255) NULL,
	ChainUp bit NOT NULL,
	OriginTruckMileage int NULL,
	OriginTankNum varchar(20) NULL,
	DestTruckMileage int NULL,
	CarrierTicketNum varchar(15) NULL,
	AuditNotes varchar(255) NULL,
	CreateDateUTC datetime NULL,
	ActualMiles int NULL,
	ProducerID int NULL,
	CreatedByUser varchar(100) NULL,
	LastChangeDateUTC datetime NULL,
	LastChangedByUser varchar(100) NULL,
	DeleteDateUTC datetime NULL,
	DeletedByUser varchar(100) NULL,
	DestProductBSW decimal(9, 3) NULL,
	DestProductGravity decimal(9, 3) NULL,
	DestProductTemp decimal(9, 3) NULL,
	ProductID int NOT NULL,
	AcceptLastChangeDateUTC datetime NULL,
	PickupLastChangeDateUTC datetime NULL,
	DeliverLastChangeDateUTC datetime NULL,
	OriginUomID int NOT NULL,
	DestUomID int NOT NULL,
	PickupPrintStatusID int NOT NULL,
	DeliverPrintStatusID int NOT NULL,
	PickupPrintDateUTC datetime NULL,
	DeliverPrintDateUTC datetime NULL,
	OriginTankID int NULL,
	OriginGrossStdUnits decimal(18, 6) NULL,
	DispatchConfirmNum varchar(25) NULL,
	DispatchNotes varchar(500) NULL,
	OriginWaitReasonID int NULL,
	DestWaitReasonID int NULL,
	RejectReasonID int NULL,
	DestOpenMeterUnits decimal(18, 3) NULL,
	DestCloseMeterUnits decimal(18, 3) NULL,
	ReassignKey int NULL,
	DestRailCarNum varchar(25) NULL,
	DestTrailerWaterCapacity int NULL,
	PickupDriverNotes varchar(255) NULL,
	DeliverDriverNotes varchar(255) NULL,
	DestRackBay varchar(50) NULL,
	OrderDate date NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_tblOrder SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_tblOrder ADD CONSTRAINT
	DF_tblOrders_StatusID DEFAULT ((1)) FOR StatusID
GO
ALTER TABLE dbo.Tmp_tblOrder ADD CONSTRAINT
	DF_tblOrder__TicketTicketType DEFAULT ((1)) FOR TicketTypeID
GO
ALTER TABLE dbo.Tmp_tblOrder ADD CONSTRAINT
	DF_tblOrder_Rejected DEFAULT ((0)) FOR Rejected
GO
ALTER TABLE dbo.Tmp_tblOrder ADD CONSTRAINT
	DF_tblTransactions_ChainUp DEFAULT ((0)) FOR ChainUp
GO
ALTER TABLE dbo.Tmp_tblOrder ADD CONSTRAINT
	DF_Order_CreateDateUTC DEFAULT (getutcdate()) FOR CreateDateUTC
GO
ALTER TABLE dbo.Tmp_tblOrder ADD CONSTRAINT
	DF_Order_Product DEFAULT ((1)) FOR ProductID
GO
ALTER TABLE dbo.Tmp_tblOrder ADD CONSTRAINT
	DF_Order_OriginUomID DEFAULT ((1)) FOR OriginUomID
GO
ALTER TABLE dbo.Tmp_tblOrder ADD CONSTRAINT
	DF_Order_DestUomID DEFAULT ((1)) FOR DestUomID
GO
ALTER TABLE dbo.Tmp_tblOrder ADD CONSTRAINT
	DF_Order_PickupPrintStatus DEFAULT ((0)) FOR PickupPrintStatusID
GO
ALTER TABLE dbo.Tmp_tblOrder ADD CONSTRAINT
	DF_Order_DeliverPrintStatus DEFAULT ((0)) FOR DeliverPrintStatusID
GO
SET IDENTITY_INSERT dbo.Tmp_tblOrder ON
GO
IF EXISTS(SELECT * FROM dbo.tblOrder)
	 EXEC('INSERT INTO dbo.Tmp_tblOrder (ID, OrderNum, StatusID, PriorityID, DueDate, RouteID, OriginID, OriginArriveTimeUTC, OriginDepartTimeUTC, OriginMinutes, OriginWaitNotes, OriginBOLNum, OriginGrossUnits, OriginNetUnits, DestinationID, DestArriveTimeUTC, DestDepartTimeUTC, DestMinutes, DestWaitNotes, DestBOLNum, DestGrossUnits, DestNetUnits, CustomerID, CarrierID, DriverID, TruckID, TrailerID, Trailer2ID, OperatorID, PumperID, TicketTypeID, Rejected, RejectNotes, ChainUp, OriginTruckMileage, OriginTankNum, DestTruckMileage, CarrierTicketNum, AuditNotes, CreateDateUTC, ActualMiles, ProducerID, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser, DestProductBSW, DestProductGravity, DestProductTemp, ProductID, AcceptLastChangeDateUTC, PickupLastChangeDateUTC, DeliverLastChangeDateUTC, OriginUomID, DestUomID, PickupPrintStatusID, DeliverPrintStatusID, PickupPrintDateUTC, DeliverPrintDateUTC, OriginTankID, OriginGrossStdUnits, DispatchConfirmNum, DispatchNotes, OriginWaitReasonID, DestWaitReasonID, RejectReasonID, DestOpenMeterUnits, DestCloseMeterUnits, ReassignKey, DestRailCarNum, DestTrailerWaterCapacity, PickupDriverNotes, DeliverDriverNotes, DestRackBay, OrderDate)
		SELECT ID, OrderNum, StatusID, PriorityID, DueDate, RouteID, OriginID, OriginArriveTimeUTC, OriginDepartTimeUTC, OriginMinutes, OriginWaitNotes, OriginBOLNum, OriginGrossUnits, OriginNetUnits, DestinationID, DestArriveTimeUTC, DestDepartTimeUTC, DestMinutes, DestWaitNotes, DestBOLNum, DestGrossUnits, DestNetUnits, CustomerID, CarrierID, DriverID, TruckID, TrailerID, Trailer2ID, OperatorID, PumperID, TicketTypeID, Rejected, RejectNotes, ChainUp, OriginTruckMileage, OriginTankNum, DestTruckMileage, CarrierTicketNum, AuditNotes, CONVERT(datetime, CreateDateUTC), ActualMiles, ProducerID, CreatedByUser, CONVERT(datetime, LastChangeDateUTC), LastChangedByUser, CONVERT(datetime, DeleteDateUTC), DeletedByUser, DestProductBSW, DestProductGravity, DestProductTemp, ProductID, CONVERT(datetime, AcceptLastChangeDateUTC), CONVERT(datetime, PickupLastChangeDateUTC), CONVERT(datetime, DeliverLastChangeDateUTC), OriginUomID, DestUomID, PickupPrintStatusID, DeliverPrintStatusID, PickupPrintDateUTC, DeliverPrintDateUTC, OriginTankID, OriginGrossStdUnits, DispatchConfirmNum, DispatchNotes, OriginWaitReasonID, DestWaitReasonID, RejectReasonID, DestOpenMeterUnits, DestCloseMeterUnits, ReassignKey, DestRailCarNum, DestTrailerWaterCapacity, PickupDriverNotes, DeliverDriverNotes, DestRackBay, OrderDate FROM dbo.tblOrder WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_tblOrder OFF
GO
ALTER TABLE dbo.tblDriverAppPrintHeaderImageSync
	DROP CONSTRAINT FK_DriverAppPrintHeaderImageSync_Order
GO
ALTER TABLE dbo.tblDriverAppPrintPickupTemplateSync
	DROP CONSTRAINT FK_DriverAppPrintPickupTemplateSync_Order
GO
ALTER TABLE dbo.tblDriverAppPrintDeliverTemplateSync
	DROP CONSTRAINT FK_DriverAppPrintDeliverTemplateSync_Order
GO
ALTER TABLE dbo.tblOrderSettlementCarrier
	DROP CONSTRAINT FK_OrderSettlementCarrier_Order
GO
ALTER TABLE dbo.tblGaugerOrder
	DROP CONSTRAINT FK_GaugerOrder_Order
GO
ALTER TABLE dbo.tblOrderSettlementShipper
	DROP CONSTRAINT FK_OrderSettlementShipper_Order
GO
ALTER TABLE dbo.tblOrderSignature
	DROP CONSTRAINT FK_OrderSignature_Order
GO
ALTER TABLE dbo.tblGaugerOrderVirtualDelete
	DROP CONSTRAINT FK_GaugerOrderVirtualDelete_Order
GO
ALTER TABLE dbo.tblOrderDriverAppVirtualDelete
	DROP CONSTRAINT FK_OrderDriverAppVirtualDelete_Order
GO
ALTER TABLE dbo.tblGaugerOrderTicket
	DROP CONSTRAINT FK_GaugerOrderTicket_Order
GO
ALTER TABLE dbo.tblOrderReroute
	DROP CONSTRAINT FK_OrderReroute_Order
GO
ALTER TABLE dbo.tblGaugerAppPrintHeaderImageSync
	DROP CONSTRAINT FK_GaugerAppPrintHeaderImageSync_Order
GO
ALTER TABLE dbo.tblOrderTicket
	DROP CONSTRAINT FK_OrderTicket_Order
GO
ALTER TABLE dbo.tblGaugerAppPrintPickupTemplateSync
	DROP CONSTRAINT FK_GaugerAppPrintPickupTemplateSync_Order
GO
ALTER TABLE dbo.tblGaugerAppPrintTicketTemplateSync
	DROP CONSTRAINT FK_GaugerAppPrintTicketTemplateSync_Order
GO
ALTER TABLE dbo.tblOrderApproval
	DROP CONSTRAINT FK_OrderApproval_Order
GO
ALTER TABLE dbo.tblDriverLocation
	DROP CONSTRAINT FK_DriverLocation_Order
GO
DROP TABLE dbo.tblOrder
GO
EXECUTE sp_rename N'dbo.Tmp_tblOrder', N'tblOrder', 'OBJECT' 
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	PK_Order PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE UNIQUE NONCLUSTERED INDEX udxOrder_OrderNum ON dbo.tblOrder
	(
	OrderNum
	) INCLUDE (ID) 
 WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxOrder_StatusID ON dbo.tblOrder
	(
	StatusID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxOrder_PriorityID ON dbo.tblOrder
	(
	PriorityID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxOrder_CarrierID ON dbo.tblOrder
	(
	CarrierID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxOrder_OriginID ON dbo.tblOrder
	(
	OriginID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxOrder_DestinationID ON dbo.tblOrder
	(
	DestinationID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxOrder_RouteID ON dbo.tblOrder
	(
	RouteID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxOrder_OriginDepartTime ON dbo.tblOrder
	(
	OriginDepartTimeUTC
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxOrder_DeleteDate_StatusID_Covering ON dbo.tblOrder
	(
	DeleteDateUTC,
	StatusID
	) INCLUDE (ID, OrderNum, PriorityID, DueDate, RouteID, OriginID, OriginArriveTimeUTC, OriginDepartTimeUTC, OriginMinutes, OriginWaitNotes, OriginBOLNum, OriginGrossUnits, OriginNetUnits, DestinationID, DestArriveTimeUTC, DestDepartTimeUTC, DestMinutes, DestWaitNotes, DestBOLNum, DestGrossUnits, DestNetUnits, CustomerID, CarrierID, DriverID, TruckID, TrailerID, Trailer2ID, OperatorID, PumperID, TicketTypeID, Rejected, RejectNotes, ChainUp, OriginTruckMileage, OriginTankNum, DestTruckMileage, CarrierTicketNum, AuditNotes, CreateDateUTC, ActualMiles, ProducerID, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeletedByUser, DestProductBSW, DestProductGravity, DestProductTemp) 
 WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxOrder_DriverID ON dbo.tblOrder
	(
	DriverID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxOrder_FindDuplicateDispatchConfirmNum ON dbo.tblOrder
	(
	DeleteDateUTC
	) INCLUDE (ID, OriginID, CustomerID, DispatchConfirmNum) 
 WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxOrder_Producer ON dbo.tblOrder
	(
	ProducerID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxOrder_DeleteDateUTC ON dbo.tblOrder
	(
	DeleteDateUTC
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxOrder_Status_DeleteDateUTC ON dbo.tblOrder
	(
	StatusID,
	DeleteDateUTC
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxOrder_OrderDate ON dbo.tblOrder
	(
	OrderDate
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_OriginUom FOREIGN KEY
	(
	OriginUomID
	) REFERENCES dbo.tblUom
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_DestUom FOREIGN KEY
	(
	DestUomID
	) REFERENCES dbo.tblUom
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_Origin FOREIGN KEY
	(
	OriginID
	) REFERENCES dbo.tblOrigin
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_OriginWaitReason FOREIGN KEY
	(
	OriginWaitReasonID
	) REFERENCES dbo.tblOriginWaitReason
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_Trailer FOREIGN KEY
	(
	TrailerID
	) REFERENCES dbo.tblTrailer
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  SET NULL 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_DestWaitReason FOREIGN KEY
	(
	DestWaitReasonID
	) REFERENCES dbo.tblDestinationWaitReason
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_Trailer2 FOREIGN KEY
	(
	Trailer2ID
	) REFERENCES dbo.tblTrailer
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_RejectReason FOREIGN KEY
	(
	RejectReasonID
	) REFERENCES dbo.tblOrderRejectReason
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_Truck FOREIGN KEY
	(
	TruckID
	) REFERENCES dbo.tblTruck
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  SET NULL 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_OrderStatus FOREIGN KEY
	(
	StatusID
	) REFERENCES dbo.tblOrderStatus
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_Priority FOREIGN KEY
	(
	PriorityID
	) REFERENCES dbo.tblPriority
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  SET NULL 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_Pumper FOREIGN KEY
	(
	PumperID
	) REFERENCES dbo.tblPumper
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  SET NULL 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_Driver FOREIGN KEY
	(
	DriverID
	) REFERENCES dbo.tblDriver
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_Route FOREIGN KEY
	(
	RouteID
	) REFERENCES dbo.tblRoute
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_TicketType FOREIGN KEY
	(
	TicketTypeID
	) REFERENCES dbo.tblTicketType
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_Destination FOREIGN KEY
	(
	DestinationID
	) REFERENCES dbo.tblDestination
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_Carrier FOREIGN KEY
	(
	CarrierID
	) REFERENCES dbo.tblCarrier
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_tblOrder_tblProducer FOREIGN KEY
	(
	ProducerID
	) REFERENCES dbo.tblProducer
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_Customer FOREIGN KEY
	(
	CustomerID
	) REFERENCES dbo.tblCustomer
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_Pickup_PrintStatus FOREIGN KEY
	(
	PickupPrintStatusID
	) REFERENCES dbo.tblPrintStatus
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_Deliver_PrintStatus FOREIGN KEY
	(
	DeliverPrintStatusID
	) REFERENCES dbo.tblPrintStatus
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_Operator FOREIGN KEY
	(
	OperatorID
	) REFERENCES dbo.tblOperator
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  SET NULL 
	
GO
/**********************************************************/
-- Author: Kevin Alons
-- Date Created: 26 Sep 2013
-- Purpose: track changes to Orders that cause it be "virtually" deleted for a Driver (for DriveApp.Sync purposes)
--			track changes to Orders that cause it be "virtually" deleted for a Gauger (for GaugerApp.Sync purposes)
/**********************************************************/
CREATE TRIGGER trigOrder_U_VirtualDelete ON dbo.tblOrder AFTER UPDATE AS
BEGIN
	DECLARE @NewRecords TABLE (
		  OrderID int NOT NULL
		, DriverID int NOT NULL
		, LastChangeDateUTC smalldatetime NOT NULL
		, LastChangedByUser varchar(100) NULL
	)

	SET NOCOUNT ON;
	
	IF (TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU')) = 0 AND TRIGGER_NESTLEVEL( OBJECT_ID('trigOrderTicket_IU')) = 0
		AND EXISTS (SELECT * FROM inserted EXCEPT SELECT * FROM deleted)) 
	BEGIN
		PRINT 'trigOrder_U_VirtualDelete FIRED'
		
		-- delete any records that no longer apply because now the record is valid for the new driver/status
		DELETE FROM tblOrderDriverAppVirtualDelete
		FROM tblOrderDriverAppVirtualDelete ODAVD
		JOIN inserted i ON i.ID = ODAVD.OrderID AND i.DriverID = ODAVD.DriverID
		WHERE i.DeleteDateUTC IS NULL 
			AND dbo.fnPrintStatusID(i.StatusID, i.PickupPrintStatusID, i.DeliverPrintStatusID) IN (2,7,8,3)

		INSERT INTO @NewRecords
			-- insert any applicable VIRTUALDELETE records where the status changed from 
			--   a valid status to an invalid [DriverApp] status
			SELECT d.ID, d.DriverID, GETUTCDATE(), ISNULL(i.LastChangedByUser, 'System')
			FROM deleted d
			JOIN inserted i ON i.ID = D.ID
			WHERE dbo.fnPrintStatusID(i.StatusID, i.PickupPrintStatusID, i.DeliverPrintStatusID) NOT IN (2,7,8,3)
			  AND dbo.fnPrintStatusID(d.StatusID, d.PickupPrintStatusID, d.DeliverPrintStatusID) IN (2,7,8,3)
			  AND d.DriverID IS NOT NULL
			  
			UNION

			-- insert any records where the DriverID changed to another DriverID (for a valid status)
			SELECT d.ID, d.DriverID, GETUTCDATE(), ISNULL(i.LastChangedByUser, 'System')
			FROM deleted d
			JOIN inserted i ON i.ID = D.ID
			WHERE d.DriverID <> i.DriverID 
			  AND dbo.fnPrintStatusID(d.StatusID, d.PickupPrintStatusID, d.DeliverPrintStatusID) IN (2,7,8,3)
			  AND d.DriverID IS NOT NULL
			  
		-- update the VirtualDeleteDateUTC value for any existing records
		UPDATE tblOrderDriverAppVirtualDelete
		  SET VirtualDeleteDateUTC = NR.LastChangeDateUTC, VirtualDeletedByUser = NR.LastChangedByUser
		FROM tblOrderDriverAppVirtualDelete ODAVD
		JOIN @NewRecords NR ON NR.OrderID = ODAVD.OrderID AND NR.DriverID = ODAVD.DriverID

		-- insert the truly new VirtualDelete records	
		INSERT INTO tblOrderDriverAppVirtualDelete (OrderID, DriverID, VirtualDeleteDateUTC, VirtualDeletedByUser)
			SELECT NR.OrderID, NR.DriverID, NR.LastChangeDateUTC, NR.LastChangedByUser
			FROM @NewRecords NR
			LEFT JOIN tblOrderDriverAppVirtualDelete ODAVD ON ODAVD.OrderID = NR.OrderID AND ODAVD.DriverID = NR.DriverID
			WHERE ODAVD.ID IS NULL

		-- delete any records that no longer apply because now the record is valid for the new gauger/status
		DELETE FROM tblGaugerOrderVirtualDelete
		FROM tblGaugerOrderVirtualDelete GOVD
		JOIN tblGaugerOrder GAO ON GAO.OrderID = GOVD.OrderID
		JOIN inserted i ON i.ID = GOVD.OrderID AND GAO.GaugerID = GOVD.GaugerID
		WHERE i.StatusID IN (-9, -10)
			
		-- record that the gauger order is no longer in a status that can be viewed/edited by a Gauger
		INSERT INTO tblGaugerOrderVirtualDelete (OrderID, GaugerID, VirtualDeleteDateUTC, VirtualDeletedByUser)
			SELECT i.ID, GAO.GaugerID, i.LastChangeDateUTC, i.LastChangedByUser
			FROM inserted i 
			JOIN deleted d ON d.ID = i.ID
			JOIN tblGaugerOrder GAO ON GAO.OrderID = i.ID
			LEFT JOIN tblGaugerOrderVirtualDelete GOVD ON GOVD.OrderID = i.ID AND GOVD.GaugerID = GAO.GaugerID
			WHERE i.StatusID <> d.StatusID
			  AND GOVD.ID IS NULL
			  AND i.StatusID NOT IN (-9, -10)

	END
END
GO
sp_settriggerorder N'trigOrder_U_VirtualDelete', N'last', N'update'
GO
-- =============================================
-- Author:		Kevin Alons
-- Create date: 13 Dec 2012
-- Description:	ensure that only "Assigned|Dispatched|Declined|Generated" orders can be deleted (marked deleted)
-- =============================================
CREATE TRIGGER trigOrder_IOD ON dbo.tblOrder INSTEAD OF DELETE AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	IF (SELECT count(*) FROM deleted d where StatusID NOT IN (-10, 1, 2, 9)) > 0
	BEGIN
		RAISERROR('Order could not be deleted - it has a status other than "Generated, Assigned, Dispatched or Declined"', 16, 1)
		RETURN
	END
	ELSE
		DELETE FROM tblOrderTicket WHERE OrderID IN (SELECT ID FROM deleted)
		DELETE FROM tblOrder WHERE ID IN (SELECT ID FROM deleted)
END
GO
/************************************************
-- Author:		Kevin Alons
-- Create date: 19 Dec 2012
-- Description:	trigger to 
				1) validate any changes, and fail the update if invalid changes are submitted
				2) add a unique, incrementing OrderNum to each new Order (manual Identity column)
				3) recompute wait times (origin|destination based on times provided)
				4) generate route table entry for any newly used origin-destination combination
				5) keep Order consistent with Origin.Producer|Operator|UOM & Dest.UOM then the Origin|Dest change
				6) roll the PrintStatus back to NOTFINALIZED (0) when status is rolled back
				7) when DriverID changes, mark those orders AS DELETED and clone the order with the new DriverID and associated records
				8) update any ticket quantities for open orders when the UOM is changed for the Origin
				9) update the Driver.Truck\Trailer\Trailer2 defaults & related DISPATCHED orders when driver updates them on an order
-REMOVED		10) update the Pickup/DeliverLastChangeDateUTC when Origin/Destination is changed (respectively)
				11) (re) apply Settlement Amounts to orders not yet fully settled when status is changed to DELIVERED
				12) if DBAudit is turned on, save an audit record for this Order change
-- Changes: 
	- 3.7.4 05/08/15 GSM Added Rack/Bay field to DBAudit logic
	- 3.7.7 - 15 May 2015 - KDA - REMOVE #10 above, instead update the LastChangeDateUTC whenever an Origin or Destination is changed
	- 3.7.11 - 18 May 2015 - KDA - generally use GETUTCDATE for all LastChangeDateUTC revisions (instead of related Order.LastChangeDateUTC, etc)
	- 3.7.12 - 5/19/2015 - KDA - update any existing OrderTicket records when the Order is DISPATCHED to a driver (so the Driver App will ALWAYS receive the existing TICKETS from the GAUGER)
	- 3.7.23 - 06/05/2015 - GSM - DCDRV-154: Ticket Type following Origin Change
	- 3.7.23 - 06/05/2015 - GSM - DCWEB-530 - ensure ASSIGNED orders with a driver defined are updated to DISPATCHED
	- 3.8.1  - 2015/07/04 - KDA - purge any Driver|Gauger App ZPL related SYNC change records when order is AUDITED
	- 3.9.0  - 2015/08/20 - KDA - add invocation of spAutoAuditOrder for DELIVERED orders (that qualify) when the Auto-Audit global setting value is TRUE
								- add invocation of spAutoApproveOrder for AUDITED orders (that qualify)
	- 3.9.2  - 2015/08/25 - KDA - appropriately use new tblOrder.OrderDate date column
	- 3.9.4  - 2015/08/30 - KDA - performanc optimization to prevent Orderdate computation if no timestamp fields are yet populated
	- 3.9.5  - 2015/08/31 - KDA - more extensive performance optimizations to minimize performance ramifications or computing OrderDate dynamically from OrderRule
	- 3.9.13 - 2015/09/01 - KDA - add (but commented out) some code to validate Arrive|Depart time being present when required
								- allow orders to be rolled back from AUDITED to DELIVERED when in AUTO-AUDIT mode
								- always AUTO UNAPPROVE orders when the status is reverted from AUDITED to DELIVERED 
	- 3.9.19 - 2015/09/23 - KDA - remove obsolete reference to tblDriverAppPrintTicketTemplate
************************************************/
CREATE TRIGGER trigOrder_IU ON dbo.tblOrder AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @changesFound bit
	
		-- Note: the TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU') ) = 1 statement prevents recursion
	IF (TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU')) = 1 AND TRIGGER_NESTLEVEL( OBJECT_ID('trigOrderTicket_IU')) = 0) BEGIN

		/**********  START OF VALIDATION SECTION ************************/

		IF (UPDATE(OrderNum) 
			OR UPDATE(PriorityID) 
			OR UPDATE(DueDate) 
			OR UPDATE(RouteID) 
			OR UPDATE(OriginID) 
			OR UPDATE(OriginArriveTimeUTC) 
			OR UPDATE(OriginDepartTimeUTC) 
			OR UPDATE(OriginMinutes) 
			OR UPDATE(OriginWaitNotes) 
			OR UPDATE(OriginBOLNum) 
			OR UPDATE(OriginGrossUnits) 
			OR UPDATE(OriginNetUnits) 
			OR UPDATE(DestinationID) 
			OR UPDATE(DestArriveTimeUTC) 
			OR UPDATE(DestDepartTimeUTC) 
			OR UPDATE(DestMinutes) 
			OR UPDATE(DestWaitNotes) 
			OR UPDATE(DestBOLNum) 
			OR UPDATE(DestGrossUnits) 
			OR UPDATE(DestNetUnits) 
			OR UPDATE(CustomerID) 
			OR UPDATE(CarrierID) 
			OR UPDATE(DriverID) 
			OR UPDATE(TruckID)
			OR UPDATE(TrailerID) 
			OR UPDATE(Trailer2ID) 
			OR UPDATE(OperatorID) 
			OR UPDATE(PumperID) 
			OR UPDATE(TicketTypeID) 
			OR UPDATE(Rejected) 
			OR UPDATE(RejectNotes) 
			OR UPDATE(ChainUp) 
			-- allow this to be changed even in audit status
			--OR UPDATE(OriginTruckMileage) 
			OR UPDATE(OriginTankNum) 
			-- allow this to be changed even in audit status
			--OR UPDATE(DestTruckMileage) 
			OR UPDATE(CarrierTicketNum) 
			OR UPDATE(AuditNotes) 
			OR UPDATE(CreateDateUTC) 
			-- it is permissible to change the ActualMiles value on Audited orders up to the point the Order has been Shipper-Settled
			OR (UPDATE(ActualMiles) AND EXISTS (SELECT * FROM deleted i JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = i.ID WHERE OSS.BatchID IS NOT NULL))
			OR UPDATE(ProducerID) 
			OR UPDATE(CreatedByUser) 
			OR UPDATE(LastChangeDateUTC) 
			OR UPDATE(LastChangedByUser) 
			OR UPDATE(DeleteDateUTC) 
			OR UPDATE(DeletedByUser) 
			OR UPDATE(DestProductBSW)
			OR UPDATE(DestProductGravity) 
			OR UPDATE(DestProductTemp) 
			OR UPDATE(ProductID) 
			OR UPDATE(AcceptLastChangeDateUTC) 
			OR UPDATE(PickupLastChangeDateUTC) 
			OR UPDATE(DeliverLastChangeDateUTC) 
			OR UPDATE(OriginUomID) 
			OR UPDATE(DestUomID) 
			OR UPDATE(PickupPrintStatusID) 
			OR UPDATE(DeliverPrintStatusID)
			OR UPDATE(PickupPrintDateUTC) 
			OR UPDATE(DeliverPrintDateUTC) 
			OR UPDATE(OriginTankID) 
			OR UPDATE(OriginGrossStdUnits) 
			OR UPDATE(DispatchConfirmNum) 
			OR UPDATE(DispatchNotes)
			OR UPDATE(PickupDriverNotes)
			OR UPDATE(DeliverDriverNotes)
			OR UPDATE(OriginWaitReasonID)
			OR UPDATE(DestWaitReasonID)
			OR UPDATE(RejectReasonID) 
			OR UPDATE(DestOpenMeterUnits) 
			OR UPDATE(DestCloseMeterUnits)
			OR UPDATE(DestRackBay)
			OR UPDATE(OrderDate)
		)
			--OR UPDATE(ReassignKey)) we don't do any work if this value changes so just ignore it
		BEGIN
			-- only allow the StatusID value to be changed on an audited order
			IF EXISTS (SELECT * FROM deleted WHERE StatusID = 4)
			BEGIN
				-- if the "Allow Audited Order Update (temp only)" setting is TRUE then only warn when this occurs
				IF (dbo.fnToBool(dbo.fnSettingValue(35)) = 1)
					PRINT 'AUDITED orders is being modified - please investigate why!'
				ELSE BEGIN
					-- otherwise (normal behavior) - prevent AUDITED orders from being modified (except to UN-AUDIT them)
					RAISERROR('AUDITED orders cannot be modified!', 16, 1)
					IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
					RETURN
				END
			END
		END
		ELSE IF NOT UPDATE(StatusID) -- NO CHANGES DETECTED SO JUST QUIT
			RETURN

		PRINT 'trigOrder_IU FIRED'
					
		-- ensure the Origin and Destinations are both specified unless the Status is:
		--   (Generated, Assigned, Dispatched or Declined)
		IF  EXISTS(SELECT * FROM inserted O WHERE (O.OriginID IS NULL OR O.DestinationID IS NULL) AND O.StatusID NOT IN (-10,1,2,9))
		BEGIN
			RAISERROR('Invalid Order Status when the Origin and/or Or Destination is not specified', 16, 1)
			IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
			RETURN
		END
		/* this is commented out until we get the Android issues closer to resolved 
		IF EXISTS (SELECT * FROM inserted WHERE StatusID IN (8, 3, 4) AND OriginArriveTimeUTC IS NULL OR OriginDepartTimeUTC IS NULL)
		BEGIN
			RAISERROR('OriginArriveTimeUTC and/or OriginDepartTimeUTC missing when required', 16, 1)
			IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
			RETURN
		END
		IF EXISTS (SELECT * FROM inserted WHERE StatusID IN (3, 4) AND DestArriveTimeUTC IS NULL OR DestDepartTimeUTC IS NULL)
		BEGIN
			RAISERROR('DestArriveTimeUTC and/or DestDepartTimeUTC missing when required', 16, 1)
			IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
			RETURN
		END
		******************************************************************************/
		
		/**********  END OF VALIDATION SECTION ************************/

		WHILE (SELECT count(*) FROM inserted i JOIN tblOrder O ON O.ID = i.ID WHERE O.OrderNum IS NULL) > 0
		BEGIN
			UPDATE tblOrder 
			  SET OrderNum = isnull((SELECT max(OrderNum) FROM tblOrder), 100000) + 1
				, CreateDateUTC = getutcdate()
			WHERE ID = (SELECT min(O.ID) FROM tblOrder O JOIN inserted i ON i.ID = O.ID WHERE O.OrderNum IS NULL)
		END
		
		-- re-compute the OriginMinutes (in case the website failed to compute it properly)
		IF UPDATE(OriginArriveTimeUTC) OR UPDATE(OriginDepartTimeUTC)
		BEGIN
			UPDATE tblOrder SET OriginMinutes = datediff(minute, i.OriginArriveTimeUTC, i.OriginDepartTimeUTC)
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
		END
		-- re-compute the DestMinutes (in case the website failed to compute it properly)
		IF UPDATE(DestArriveTimeUTC) OR UPDATE(DestDepartTimeUTC)
		BEGIN
			UPDATE tblOrder SET DestMinutes = datediff(minute, i.DestArriveTimeUTC, i.DestDepartTimeUTC)
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
		END
		
		/* ensure any changes to the order always update the Order.OrderDate field */
		IF (UPDATE(CustomerID) 
			OR UPDATE(CarrierID) 
			OR UPDATE(ProductID) 
			OR UPDATE(OriginID) 
			OR UPDATE(DestinationID) 
			OR UPDATE(ProducerID) 
			OR UPDATE(OriginArriveTimeUTC) OR UPDATE(OriginDepartTimeUTC)
			OR UPDATE(DestArriveTimeUTC) OR UPDATE(DestDepartTimeUTC))
		BEGIN
			UPDATE tblOrder 
			  SET OrderDate = dbo.fnOrderDate(O.ID)
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
			JOIN deleted d ON d.ID = i.ID
			LEFT JOIN tblOrderSettlementCarrier OSC ON OSC.OrderID = i.ID
			LEFT JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = i.ID
			WHERE i.StatusID <> 4 
			  -- the order has at least 1 valid timestamp
			  AND (i.OriginArriveTimeUTC IS NOT NULL OR i.OriginDepartTimeUTC IS NOT NULL OR i.DestArriveTimeUTC IS NOT NULL OR i.DestDepartTimeUTC IS NOT NULL)
			  -- the order is not yet settled
			  AND OSC.BatchID IS NULL
			  AND OSS.BatchID IS NULL
		END
		
		-- ensure missing Routes are created and assigned for the specified Origin/Destination combination
		IF UPDATE(OriginID) OR UPDATE(DestinationID)
		BEGIN
			-- create any missing Route records
			INSERT INTO tblRoute (OriginID, DestinationID, CreateDateUTC, CreatedByUser)
				SELECT i.OriginID, i.DestinationID, GETUTCDATE(), ISNULL(i.LastChangedByUser, i.CreatedByUser)
				FROM inserted i
				LEFT JOIN tblRoute R ON R.OriginID = i.OriginID AND R.DestinationID = i.DestinationID
				WHERE i.OriginID IS NOT NULL AND i.DestinationID IS NOT NULL AND R.ID IS NULL
			
			-- ensure the Order records refer to the correct Route (ID)
			UPDATE tblOrder SET RouteID = R.ID
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
			LEFT JOIN tblRoute R ON R.OriginID = O.OriginID AND R.DestinationID = O.DestinationID
			WHERE O.RouteID IS NULL OR O.RouteID <> R.ID
			
			-- update the ActualMiles from the related Route
			UPDATE tblOrder SET ActualMiles = R.ActualMiles
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
			JOIN tblRoute R ON R.ID = O.RouteID
			WHERE O.ActualMiles <> R.ActualMiles
		END
		
		IF (UPDATE(OriginID))
		BEGIN
			-- update Order.ProducerID/OperatorID/PumperID to match what is assigned to the new Origin
			UPDATE tblOrder 
				SET TicketTypeID = OO.TicketTypeID
					, ProducerID = OO.ProducerID
					, OperatorID = OO.OperatorID
					, PumperID = OO.PumperID
					, LastChangeDateUTC = GETUTCDATE() 
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			WHERE d.OriginID <> O.OriginID

			-- update Order.OriginUomID to match what is assigned to the new Origin (quantities are updated below)
			UPDATE tblOrder SET OriginUomID = OO.UomID
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			WHERE d.OriginID <> O.OriginID AND d.OriginUomID <> OO.UomID
		END
		
		-- keep the DestUomID in sync with the Destination (units are updated below)
		IF (UPDATE(DestinationID))
		BEGIN
			-- update Order.DestUomID to match what is assigned to the new Destination
			UPDATE tblOrder 
			  SET DestUomID = DD.UomID
				, LastChangeDateUTC = GETUTCDATE()
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID
			JOIN tblDestination DD ON DD.ID = O.DestinationID
			WHERE d.DestinationID <> O.DestinationID AND d.DestUomID <> DD.UomID
		END
		
		-- ensure that any set PrintStatusID values are reset to 0 when backing up the general StatusID value
		IF (UPDATE(StatusID))
		BEGIN
			UPDATE tblOrder 
			  SET DeliverPrintStatusID = 0 
				, LastChangeDateUTC = GETUTCDATE()
			  FROM tblOrder O
			  JOIN deleted d ON d.ID = O.ID
			WHERE O.StatusID <> d.StatusID AND O.StatusID IN (-10, 1, 2, 7, 8) -- Generated, Assigned, Dispatched, Accepted, Picked-Up

			UPDATE tblOrder 
			  SET PickupPrintStatusID = 0 
				, LastChangeDateUTC = GETUTCDATE()
			  FROM tblOrder O
			  JOIN deleted d ON d.ID = O.ID
			WHERE O.StatusID <> d.StatusID AND O.StatusID IN (-10, 1, 2, 7) -- Generated, Assigned, Dispatched, Accepted
			
			/* ensure that any orders that are DISPATCHED - any existing orders are TOUCHED so they are syncable to the Driver App */
			UPDATE tblOrderTicket
				SET LastChangeDateUTC = GETUTCDATE()
			WHERE OrderID IN (
				SELECT i.ID 
				FROM inserted i
				JOIN deleted d ON d.ID = i.ID
				WHERE i.StatusID <> d.StatusID AND i.StatusID IN (2) -- DISPATCHED
			)
	END

		-- just updating the tickets here, trigOrderTicket_IU will in turn update this table
		-- ensure any existing OrderTickets are recomputed with the new OriginUOM
		UPDATE tblOrderTicket
		  SET GrossUnits = dbo.fnConvertUOM(GrossUnits, d.OriginUomID, O.OriginUomID)
			, NetUnits = dbo.fnConvertUOM(NetUnits, d.OriginUomID, O.OriginUomID)
			, LastChangeDateUTC = GETUTCDATE()
		FROM tblOrderTicket OT
		JOIN tblOrder O ON O.ID = OT.OrderID
		JOIN deleted d ON d.ID = O.ID
		WHERE O.OriginUomID <> d.OriginUomID
		
		-- recalculate the Destination Unit values if only if the DestUomID changed but the values didn't
		UPDATE tblOrder
		  SET DestGrossUnits = dbo.fnConvertUOM(O.DestGrossUnits, d.DestUomID, O.DestUomID)
			, DestNetUnits = dbo.fnConvertUOM(O.DestNetUnits, d.DestUomID, O.DestUomID)
			, LastChangeDateUTC = GETUTCDATE()
		FROM tblOrder O
		JOIN deleted d ON d.ID = O.ID
		WHERE d.DestUomID <> O.DestUomID 
		  AND d.DestGrossUnits = O.DestGrossUnits
		  AND d.DestNetUnits = O.DestNetUnits

		/* DCWEB-530 - ensure any ASSIGNED orders with a DRIVER assigned is set to DISPATCHED */
		UPDATE tblOrder SET StatusID = 2 /*DISPATCHED*/
		FROM tblOrder O
		JOIN inserted i ON I.ID = O.ID
		WHERE i.StatusID = 1 /*ASSIGNED*/ AND i.CarrierID IS NOT NULL AND i.DriverID IS NOT NULL AND i.DeleteDateUTC IS NULL

		/*************************************************************************************************************/
		/* handle driver changes - "orphan" the order/ticket (with a new ORDERNUM) - and clone it for the new driver */
		IF (UPDATE(DriverID))
		BEGIN
			-- create cloned order/ticket records (these records will be cloned then the original reset to orphaned status)
			SELECT O.*, NewOrderNum = CAST(NULL as int)
			INTO #clone
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID AND d.DriverID IS NOT NULL AND d.DriverID <> isnull(O.DriverID, 0)

			/* generate the next OrderNum values for these new records */
			DECLARE @newOrderNum int; SET @newOrderNum = isnull((SELECT max(OrderNum) FROM tblOrder), 100000) + 1;
			DECLARE @newReassignKey int; SET @newReassignKey = isnull((SELECT max(ReassignKey) FROM tblOrder), 0) + 1;
			WHILE (SELECT count(*) FROM #clone WHERE NewOrderNum IS NULL) > 0
			BEGIN
				UPDATE #clone
				  SET NewOrderNum = @newOrderNum, ReassignKey = isnull(ReassignKey, @newReassignKey)
					, CreateDateUTC = getutcdate()
				WHERE ID = (SELECT min(ID) FROM #clone WHERE NewOrderNum IS NULL)
				-- if the @newReassignKey wasn't used above, then that # will be skipped, but this should not be an issue
				SELECT @newOrderNum = @newOrderNum + 1, @newReassignKey = @newReassignKey + 1
			END
			
			-- reset the existing records as orphaned
			UPDATE tblOrder
				SET DeleteDateUTC = isnull(O.DeleteDateUTC, GETUTCDATE()), DeletedByUser = isnull(O.DeletedByUser, O.LastChangedByUser)
					, CarrierID = d.CarrierID, DriverID = d.DriverID, TruckID = d.TruckID, TrailerID = d.TrailerID, Trailer2ID = d.Trailer2ID
					, DispatchConfirmNum = NULL
					, ReassignKey = C.ReassignKey
			FROM tblOrder O
			JOIN #clone C ON C.ID = O.ID
			JOIN deleted d on D.ID = O.id

			-- create the cloned records (which is essentially the original with a new ID and the Carrrier & default Truck|Trailers of the new driver)
			INSERT INTO tblOrder (OrderNum, StatusID, PriorityID, DueDate, RouteID, OriginID, OriginArriveTimeUTC, OriginDepartTimeUTC, OriginMinutes, OriginWaitNotes, OriginBOLNum, OriginGrossUnits, OriginNetUnits, DestinationID, DestArriveTimeUTC, DestDepartTimeUTC, DestMinutes, DestWaitNotes, DestBOLNum, DestGrossUnits, DestNetUnits, CustomerID, CarrierID, DriverID, TruckID, TrailerID, Trailer2ID, OperatorID, PumperID, TicketTypeID, Rejected, RejectNotes, ChainUp, OriginTruckMileage, OriginTankNum, DestTruckMileage, CarrierTicketNum, AuditNotes, CreateDateUTC, ActualMiles, ProducerID, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser, DestProductBSW, DestProductGravity, DestProductTemp, ProductID, AcceptLastChangeDateUTC, PickupLastChangeDateUTC, DeliverLastChangeDateUTC, OriginUomID, DestUomID, PickupPrintStatusID, DeliverPrintStatusID, PickupPrintDateUTC, DeliverPrintDateUTC, OriginTankID, OriginGrossStdUnits, DispatchNotes, PickupDriverNotes, DeliverDriverNotes, DispatchConfirmNum, OriginWaitReasonID, DestWaitReasonID, RejectReasonID, DestOpenMeterUnits, DestCloseMeterUnits, ReassignKey)
				SELECT NewOrderNum, StatusID, PriorityID, DueDate, RouteID, OriginID, OriginArriveTimeUTC, OriginDepartTimeUTC, OriginMinutes, OriginWaitNotes, OriginBOLNum, OriginGrossUnits, OriginNetUnits, DestinationID, DestArriveTimeUTC, DestDepartTimeUTC, DestMinutes, DestWaitNotes, DestBOLNum, DestGrossUnits, DestNetUnits, CustomerID, D.CarrierID, DriverID, D.TruckID, D.TrailerID, D.Trailer2ID, OperatorID, PumperID, TicketTypeID, Rejected, RejectNotes, ChainUp, OriginTruckMileage, OriginTankNum, DestTruckMileage, CarrierTicketNum, AuditNotes, O.CreateDateUTC, ActualMiles, ProducerID, O.CreatedByUser, GETUTCDATE(), O.LastChangedByUser, O.DeleteDateUTC, O.DeletedByUser, DestProductBSW, DestProductGravity, DestProductTemp, ProductID, AcceptLastChangeDateUTC, PickupLastChangeDateUTC, DeliverLastChangeDateUTC, OriginUomID, DestUomID, PickupPrintStatusID, DeliverPrintStatusID, PickupPrintDateUTC, DeliverPrintDateUTC, OriginTankID, OriginGrossStdUnits, DispatchNotes, PickupDriverNotes, DeliverDriverNotes, DispatchConfirmNum, OriginWaitReasonID, DestWaitReasonID, RejectReasonID, DestOpenMeterUnits, DestCloseMeterUnits, ReassignKey
				FROM #clone O
				LEFT JOIN tblDriver D ON D.ID = O.DriverID
		
			-- cache the affected tickets
			SELECT OT.*, C.NewOrderNum, OrderDeletedByUser = C.LastChangedByUser
			INTO #cloneTicket
			FROM tblOrderTicket OT
			JOIN #clone C ON C.ID = OT.OrderID
			WHERE OT.DeleteDateUTC IS NULL
			
			-- create the cloned Order Ticket records (copy of existing ticket(s) pointed at new "original" Order)
			INSERT INTO tblOrderTicket (OrderID, CarrierTicketNum, TicketTypeID, TankNum, ProductObsGravity, ProductObsTemp, ProductBSW, OpeningGaugeFeet, OpeningGaugeInch, OpeningGaugeQ, ClosingGaugeFeet, ClosingGaugeInch, ClosingGaugeQ
					, GrossUnits, NetUnits, Rejected, RejectNotes, SealOff, SealOn, BOLNum, ProductHighTemp, ProductLowTemp, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser
					, UID, FromMobileApp, OriginTankID, BottomFeet, BottomInches, BottomQ, GrossStdUnits, RejectReasonID, MeterFactor, OpenMeterUnits, CloseMeterUnits)
				SELECT O.ID, CT.CarrierTicketNum, CT.TicketTypeID, CT.TankNum, CT.ProductObsGravity, CT.ProductObsTemp, CT.ProductBSW, CT.OpeningGaugeFeet, CT.OpeningGaugeInch, CT.OpeningGaugeQ, CT.ClosingGaugeFeet, CT.ClosingGaugeInch, CT.ClosingGaugeQ
					, CT.GrossUnits, CT.NetUnits, CT.Rejected, CT.RejectNotes, CT.SealOff, CT.SealOn, CT.BOLNum, CT.ProductHighTemp, CT.ProductLowTemp, CT.CreateDateUTC, CT.CreatedByUser, GETUTCDATE(), CT.LastChangedByUser, CT.DeleteDateUTC, CT.DeletedByUser
					, newid(), CT.FromMobileApp, CT.OriginTankID, CT.BottomFeet, CT.BottomInches, CT.BottomQ, CT.GrossStdUnits, CT.RejectReasonID, CT.MeterFactor, CT.OpenMeterUnits, CT.CloseMeterUnits
				FROM #cloneTicket CT
				JOIN tblOrder O ON O.OrderNum = CT.NewOrderNum
		END
		/*************************************************************************************************************/
		
		/* HANDLE TRUCK | TRAILER | TRAILER2 default changes when the driver updates his Truck | Trailer | Trailer2 on ACCEPTANCE */
		-- TRUCK
		IF (UPDATE(TruckID) AND EXISTS (
			SELECT i.* 
			FROM inserted i 
			JOIN deleted d ON d.ID = i.ID
			  AND i.DriverID = d.DriverID 
			  AND i.TruckID <> d.TruckID
			WHERE i.StatusID IN (7, 8, 3) -- ACCEPTED, PICKEDUP, DELIVERED
			  AND d.StatusID IN (2))) -- DISPATCHED
		BEGIN
			UPDATE tblDriver
			  SET TruckID = i.TruckID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblDriver DR
			JOIN inserted i ON i.DriverID = DR.ID
			JOIN deleted d ON d.DriverID = DR.ID
			WHERE i.TruckID <> d.TruckID
			
			UPDATE tblOrder
			  SET TruckID = i.TruckID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblOrder O
			JOIN inserted i ON i.DriverID = O.DriverID 
			  AND O.ID <> i.ID
			  AND O.StatusID IN (2) -- DISPATCHED
			  AND i.TruckID <> O.TruckID
			  AND O.DeleteDateUTC IS NULL
		END
		-- TRAILER
		IF (UPDATE(TrailerID) AND EXISTS (
			SELECT i.* 
			FROM inserted i 
			JOIN deleted d ON d.ID = i.ID
			  AND i.DriverID = d.DriverID 
			  AND i.TrailerID <> d.TrailerID
			WHERE i.StatusID IN (7, 8, 3) -- ACCEPTED, PICKEDUP, DELIVERED
			  AND d.StatusID IN (2))) -- DISPATCHED
		BEGIN
			UPDATE tblDriver
			  SET TrailerID = i.TrailerID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblDriver DR
			JOIN inserted i ON i.DriverID = DR.ID
			JOIN deleted d ON d.DriverID = DR.ID
			WHERE i.TrailerID <> d.TrailerID
			
			UPDATE tblOrder
			  SET TrailerID = i.TrailerID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblOrder O
			JOIN inserted i ON i.DriverID = O.DriverID 
			  AND O.ID <> i.ID
			  AND O.StatusID IN (2) -- DISPATCHED
			  AND i.TrailerID <> O.TrailerID
			  AND O.DeleteDateUTC IS NULL
		END
		-- TRAILER 2
		IF (UPDATE(Trailer2ID) AND EXISTS (
			SELECT i.* 
			FROM inserted i 
			JOIN deleted d ON d.ID = i.ID
			  AND i.DriverID = d.DriverID 
			  AND isnull(i.Trailer2ID, 0) <> isnull(d.Trailer2ID, 0)
			WHERE i.StatusID IN (7, 8, 3) -- ACCEPTED, PICKEDUP, DELIVERED
			  AND d.StatusID IN (2))) -- DISPATCHED
		BEGIN
			UPDATE tblDriver
			  SET TrailerID = i.TrailerID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblDriver DR
			JOIN inserted i ON i.DriverID = DR.ID
			JOIN deleted d ON d.DriverID = DR.ID
			WHERE isnull(i.Trailer2ID, 0) <> isnull(d.Trailer2ID, 0)
			
			UPDATE tblOrder
			  SET Trailer2ID = i.Trailer2ID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblOrder O
			JOIN inserted i ON i.DriverID = O.DriverID 
			  AND O.ID <> i.ID
			  AND O.StatusID IN (2) -- DISPATCHED
			  AND isnull(i.Trailer2ID, 0) <> isnull(O.Trailer2ID, 0)
			  AND O.DeleteDateUTC IS NULL
		END
		/*************************************************************************************************************/
	
		DECLARE @deliveredIDs TABLE (ID int)
		DECLARE @auditedIDs TABLE (ID int)
		DECLARE @id int
		DECLARE @autoAudit bit; SET @autoAudit = dbo.fnToBool(dbo.fnSettingValue(54))
		DECLARE @toAudit bit
		-- Auto-Audit/Apply Settlement Rates/Amounts/Auto-Approve to Order when STATUS is changed to DELIVERED (until it is FINAL settled)
		IF (UPDATE(StatusID) OR UPDATE(DeliverPrintStatusID))
		BEGIN
			INSERT INTO @deliveredIDs (ID)
				SELECT i.ID 
				FROM inserted i
				JOIN tblPrintStatus iPS ON iPS.ID = i.DeliverPrintStatusID
				JOIN deleted d ON d.ID = i.ID
				JOIN tblPrintStatus dPS ON dPS.ID = d.DeliverPrintStatusID
				WHERE i.StatusID = 3 AND d.StatusID <> 4 AND iPS.IsCompleted = 1 AND i.StatusID + iPS.IsCompleted <> d.StatusID + dPS.IsCompleted
			
			SELECT @id = MIN(ID) FROM @deliveredIDs
			WHILE @id IS NOT NULL
			BEGIN
				-- attempt to AUTO-AUDIT the order if this feature is enabled (global setting)
				IF (@autoAudit = 1)
				BEGIN
					EXEC spAutoAuditOrder @ID, 'System', @toAudit OUTPUT
					/* if the order was updated to AUDITED status, then we need to attempt also to auto-approve [below] */
					IF (@toAudit = 1)
						INSERT INTO @auditedIDs VALUES (@id)
				END
				-- attempt to apply rates to all newly DELIVERED orders
				EXEC spApplyRatesBoth @id, 'System' 
				SET @id = (SELECT MIN(id) FROM @deliveredIDs WHERE ID > @id)
			END
		END

		-- Auto-Approve any un-approved orders when STATUS changed to AUDIT
		IF (UPDATE(StatusID) OR EXISTS (SELECT * FROM @auditedIDs))
		BEGIN
			INSERT INTO @auditedIDs (ID)
				SELECT i.ID 
				FROM inserted i
				JOIN deleted d ON d.ID = i.ID
				LEFT JOIN tblOrderApproval OA ON OA.OrderID = i.ID
				LEFT JOIN @auditedIDs AID ON AID.ID = i.ID
				WHERE i.StatusID = 4 AND d.StatusID <> 4 AND OA.OrderID IS NULL AND AID.ID IS NULL

			SELECT @id = MIN(ID) FROM @auditedIDs
			WHILE @id IS NOT NULL
			BEGIN
				EXEC spAutoApproveOrder @id, 'System'
				SET @id = (SELECT MIN(id) FROM @auditedIDs WHERE ID > @id)
			END
			
		END
		
		/* auto UNAPPROVE any orders that have their status reverted from AUDITED */
		IF (UPDATE(StatusID))
		BEGIN
			DELETE FROM tblOrderApproval
			WHERE OrderID IN (
				SELECT i.ID
				FROM inserted i
				JOIN deleted d ON d.ID = i.ID
				JOIN tblOrderApproval OA ON OA.OrderID = i.ID
				WHERE d.StatusID = 4 AND i.StatusID <> 4
			)
		END
		
		-- purge any DriverApp/Gauger App sync records for any orders that are not in AUDITED status
		IF UPDATE(StatusID) AND EXISTS (SELECT ID FROM inserted WHERE StatusID IN (4))
		BEGIN
			-- DRIVER APP records
			DELETE FROM tblDriverAppPrintHeaderImageSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID IN (4))
			DELETE FROM tblDriverAppPrintPickupTemplateSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID IN (4))
			DELETE FROM tblDriverAppPrintDeliverTemplateSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID IN (4))
			-- GAUGER APP records
			DELETE FROM tblGaugerAppPrintHeaderImageSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID IN (4))
			DELETE FROM tblGaugerAppPrintPickupTemplateSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID IN (4))
			DELETE FROM tblGaugerAppPrintTicketTemplateSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID IN (4))
		END
				
		--NOTE: we do not update the DestOpenMeterUnits/DestCloseMeterUnits since they don't auto update
		--  (and they simply represent a reading that is assumed to in the DestUomID UOM at entry)
		--  (if they are updated in the future [manually] the will calculate again using the DestUomID at that time)

		IF EXISTS (SELECT * FROM inserted EXCEPT SELECT * FROM deleted)
		BEGIN
			-- optionally add tblOrderDBAudit records
			BEGIN TRY
				IF EXISTS(SELECT * FROM tblSetting WHERE ID = 32 AND (Value LIKE 'true' OR Value LIKE 'yes'))
					INSERT INTO tblOrderDbAudit (DBAuditDate, ID, OrderNum, StatusID, PriorityID, DueDate, RouteID, OriginID, OriginArriveTimeUTC, OriginDepartTimeUTC, OriginMinutes, OriginWaitNotes, OriginBOLNum, OriginGrossUnits, OriginNetUnits, DestinationID, DestArriveTimeUTC, DestDepartTimeUTC, DestMinutes, DestWaitNotes, DestBOLNum, DestGrossUnits, DestNetUnits, CustomerID, CarrierID, DriverID, TruckID, TrailerID, Trailer2ID, OperatorID, PumperID, TicketTypeID, Rejected, RejectNotes, ChainUp, OriginTruckMileage, OriginTankNum, DestTruckMileage, CarrierTicketNum, AuditNotes, CreateDateUTC, ActualMiles, ProducerID, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser, DestProductBSW, DestProductGravity, DestProductTemp, ProductID, AcceptLastChangeDateUTC, PickupLastChangeDateUTC, DeliverLastChangeDateUTC, OriginUomID, DestUomID, PickupPrintStatusID, DeliverPrintStatusID, PickupPrintDateUTC, DeliverPrintDateUTC, OriginTankID, OriginGrossStdUnits, DispatchNotes, PickupDriverNotes, DeliverDriverNotes, DispatchConfirmNum, OriginWaitReasonID, DestWaitReasonID, RejectReasonID, DestOpenMeterUnits, DestCloseMeterUnits, ReassignKey, DestRackBay, OrderDate)
						SELECT GETUTCDATE(), ID, OrderNum, StatusID, PriorityID, DueDate, RouteID, OriginID, OriginArriveTimeUTC, OriginDepartTimeUTC, OriginMinutes, OriginWaitNotes, OriginBOLNum, OriginGrossUnits, OriginNetUnits, DestinationID, DestArriveTimeUTC, DestDepartTimeUTC, DestMinutes, DestWaitNotes, DestBOLNum, DestGrossUnits, DestNetUnits, CustomerID, CarrierID, DriverID, TruckID, TrailerID, Trailer2ID, OperatorID, PumperID, TicketTypeID, Rejected, RejectNotes, ChainUp, OriginTruckMileage, OriginTankNum, DestTruckMileage, CarrierTicketNum, AuditNotes, CreateDateUTC, ActualMiles, ProducerID, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser, DestProductBSW, DestProductGravity, DestProductTemp, ProductID, AcceptLastChangeDateUTC, PickupLastChangeDateUTC, DeliverLastChangeDateUTC, OriginUomID, DestUomID, PickupPrintStatusID, DeliverPrintStatusID, PickupPrintDateUTC, DeliverPrintDateUTC, OriginTankID, OriginGrossStdUnits, DispatchNotes, PickupDriverNotes, DeliverDriverNotes, DispatchConfirmNum, OriginWaitReasonID, DestWaitReasonID, RejectReasonID, DestOpenMeterUnits, DestCloseMeterUnits, ReassignKey, DestRackBay, OrderDate
						FROM deleted d
			END TRY
			BEGIN CATCH
				PRINT 'trigOrder_IU.DBAUDIT FAILURE: ' + ERROR_MESSAGE()
			END CATCH
		END
		
	PRINT 'trigOrder_IU COMPLETE'

	END
	
END
GO
sp_settriggerorder N'trigOrder_IU', N'first', N'insert'
GO
sp_settriggerorder N'trigOrder_IU', N'first', N'update'
GO
ALTER TABLE dbo.tblDriverLocation ADD CONSTRAINT
	FK_DriverLocation_Order FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.tblOrder
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblDriverLocation SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderApproval ADD CONSTRAINT
	FK_OrderApproval_Order FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.tblOrder
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
GO
ALTER TABLE dbo.tblOrderApproval SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblGaugerAppPrintTicketTemplateSync ADD CONSTRAINT
	FK_GaugerAppPrintTicketTemplateSync_Order FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.tblOrder
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
GO
ALTER TABLE dbo.tblGaugerAppPrintTicketTemplateSync SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblGaugerAppPrintPickupTemplateSync ADD CONSTRAINT
	FK_GaugerAppPrintPickupTemplateSync_Order FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.tblOrder
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
GO
ALTER TABLE dbo.tblGaugerAppPrintPickupTemplateSync SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderTicket ADD CONSTRAINT
	FK_OrderTicket_Order FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.tblOrder
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderTicket SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblGaugerAppPrintHeaderImageSync ADD CONSTRAINT
	FK_GaugerAppPrintHeaderImageSync_Order FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.tblOrder
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
GO
ALTER TABLE dbo.tblGaugerAppPrintHeaderImageSync SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderReroute ADD CONSTRAINT
	FK_OrderReroute_Order FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.tblOrder
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
GO
ALTER TABLE dbo.tblOrderReroute SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblGaugerOrderTicket ADD CONSTRAINT
	FK_GaugerOrderTicket_Order FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.tblOrder
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblGaugerOrderTicket SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderDriverAppVirtualDelete ADD CONSTRAINT
	FK_OrderDriverAppVirtualDelete_Order FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.tblOrder
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
GO
ALTER TABLE dbo.tblOrderDriverAppVirtualDelete SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblGaugerOrderVirtualDelete ADD CONSTRAINT
	FK_GaugerOrderVirtualDelete_Order FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.tblOrder
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
GO
ALTER TABLE dbo.tblGaugerOrderVirtualDelete SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderSignature ADD CONSTRAINT
	FK_OrderSignature_Order FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.tblOrder
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderSignature SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderSettlementShipper ADD CONSTRAINT
	FK_OrderSettlementShipper_Order FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.tblOrder
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderSettlementShipper SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblGaugerOrder ADD CONSTRAINT
	FK_GaugerOrder_Order FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.tblOrder
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblGaugerOrder SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderSettlementCarrier ADD CONSTRAINT
	FK_OrderSettlementCarrier_Order FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.tblOrder
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderSettlementCarrier SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblDriverAppPrintDeliverTemplateSync ADD CONSTRAINT
	FK_DriverAppPrintDeliverTemplateSync_Order FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.tblOrder
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
GO
ALTER TABLE dbo.tblDriverAppPrintDeliverTemplateSync SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblDriverAppPrintPickupTemplateSync ADD CONSTRAINT
	FK_DriverAppPrintPickupTemplateSync_Order FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.tblOrder
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
GO
ALTER TABLE dbo.tblDriverAppPrintPickupTemplateSync SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblDriverAppPrintHeaderImageSync ADD CONSTRAINT
	FK_DriverAppPrintHeaderImageSync_Order FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.tblOrder
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
GO
ALTER TABLE dbo.tblDriverAppPrintHeaderImageSync SET (LOCK_ESCALATION = TABLE)
GO

--------------------------------------------

EXEC _spRebuildAllObjects
GO

COMMIT
SET NOEXEC OFF