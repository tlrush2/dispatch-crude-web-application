SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.1.5'
SELECT  @NewVersion = '4.1.6'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1750 Order rule to require completed reject tickets'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


INSERT INTO tblOrderRuleType
  VALUES 
  (20, 'Require completed Reject Ticket', 2, 1)

GO


/***********************************************************************/
-- Date Created: 18 Apr 2013
-- Author: Kevin Alons
-- Purpose: given the parameters, convert UncorrectedAPIGravity to CorrectedAPIGravity
-- Source: http://ioilfield.com/units_correl/o_api_co.html
-- Changes:
--	3.13.3.1	07/20/2016	JAE		Corrected formula to match the 11.1 API documentation
--  3.13.8.1	07/29/2016	JAE		Altered to handle extreme temps (which caused large looping)
--	4.0.2		08/17/2016	JAE		Removed hydrometer correction and updated constant for water density at 60
--	4.1.6		09/16/2016	JAE		Added check for VCF = 0 (large alpha was causing divide by 0)
/***********************************************************************/
ALTER FUNCTION fnCorrectedAPIGravity(@ObsGravity decimal(18,8), @ObsTemp decimal(8,3))
RETURNS DECIMAL(18,1)
BEGIN
	DECLARE @WATER_DENSITY_60F FLOAT = 999.016
	DECLARE @K0 FLOAT = 341.0957
	DECLARE @K1 FLOAT = 0.0000
	DECLARE @i INT = 0

	--calculate difference in observed and base temps
	DECLARE @delta FLOAT
	SELECT @delta = @obsTemp - 60.0

	--compute hydrometer correction term
	DECLARE @hyc FLOAT = 1.0 - ROUND(0.00001278 * @delta,9) - ROUND(0.0000000062 * @delta * @delta,9)

	--convert API gravity to density
	DECLARE @rho FLOAT = ROUND((141.5 * @WATER_DENSITY_60F)/(131.5 + @obsGravity), 2)

    -- Apply hydrometer correction
	DECLARE @rhoT FLOAT = ROUND(@rho * @hyc, 2)

	-- Initialize density @ 60F
	DECLARE @rho60 FLOAT = @rhoT

	DECLARE @rho60prev FLOAT = @rho60 -1 -- something to trigger the first loop
	DECLARE @alpha FLOAT
	DECLARE @vcf FLOAT

	WHILE ABS(@rho60 - @rho60prev) >= .05 AND @i < 20
	BEGIN
		-- calculate coefficient of thermal expansion
		SELECT @alpha = ROUND(@K0 / @rho60 / @rho60 + @K1 / @rho60, 7)

		-- calculate volume correction factor
		SELECT @vcf = ROUND(EXP(ROUND(-@alpha * @delta - 0.8 * @alpha * @alpha * @delta * @delta, 8)),6)
		IF (@vcf = 0) 
			BREAK

		SELECT @rho60prev = @rho60
		SELECT @rho60 = ROUND(@rhoT/@vcf, 3, 1)

		SELECT @i = @i + 1
	END

	RETURN ROUND(141.5 * @WATER_DENSITY_60F / @rho60 - 131.5, 1)
END

GO

COMMIT
SET NOEXEC OFF