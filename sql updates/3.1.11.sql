-- select * from tblsetting where id = 0
/* 
	-- add ROLLBACK TRANSACTION to Validate triggers
*/ 
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.1.10'
SELECT  @NewVersion = '3.1.11'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 21 Jun 2013
-- Description:	trigger to ensure the entered values for an Order are actually valid
-- =============================================
ALTER TRIGGER [dbo].[trigOrder_IU_Validate] ON [dbo].[tblOrder] AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

		
	-- ensure the Origin and Destinations are both specified unless the Status is:
	--   (Generated, Assigned, Dispatched or Declined)
	IF  EXISTS(SELECT * FROM inserted O WHERE (O.OriginID IS NULL OR O.DestinationID IS NULL) AND O.StatusID NOT IN (-10,1,2,9))
	BEGIN
		RAISERROR('Invalid Order Status when the Origin and/or Or Destination is not specified', 16, 1)
		IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
		RETURN
	END
END

GO
EXEC sp_settriggerorder @triggername=N'[dbo].[trigOrder_IU_Validate]', @order=N'First', @stmttype=N'INSERT'
GO
EXEC sp_settriggerorder @triggername=N'[dbo].[trigOrder_IU_Validate]', @order=N'First', @stmttype=N'UPDATE'
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 6 Feb 2013
-- Description:	trigger to ensure the entered values for an OrderTicket are actually valid
-- =============================================
ALTER TRIGGER [dbo].[trigOrderTicket_IU_Validate] ON [dbo].[tblOrderTicket] AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	PRINT 'trigOrderTicket_IU_Validate FIRED'
	
	DECLARE @errorString varchar(255)
	
	IF (SELECT COUNT(*) FROM (
			SELECT OT.OrderID
			FROM tblOrderTicket OT
			JOIN inserted i ON i.OrderID = OT.OrderID AND i.CarrierTicketNum = OT.CarrierTicketNum
			WHERE OT.DeleteDateUTC IS NULL
			GROUP BY OT.OrderID, OT.CarrierTicketNum
			HAVING COUNT(*) > 1
		) v) > 0
	BEGIN
		SET @errorString = 'Duplicate Ticket Numbers are not allowed'
	END
	
	ELSE IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (3) AND BOLNum IS NULL AND DeleteDateUTC IS NULL) > 0
	BEGIN
		SET @errorString = 'BOL # value is required for Meter Run tickets'
	END
	
	ELSE IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1, 2) AND OriginTankID IS NULL AND TankNum IS NULL AND DeleteDateUTC IS NULL) > 0
	BEGIN
		SET @errorString = 'Tank ID value is required for Gauge Run & Net Volume tickets'
	END

	ELSE IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1) AND CarrierTicketNum IS NULL AND DeleteDateUTC IS NULL) > 0
	BEGIN
		SET @errorString = 'Ticket # value is required for Gauge Run tickets'
	END

	ELSE IF (SELECT COUNT(*) FROM inserted WHERE DeleteDateUTC IS NULL
			AND (TicketTypeID IN (1, 2) AND (ProductObsTemp IS NULL OR ProductObsGravity IS NULL OR ProductBSW IS NULL))
				OR (TicketTypeID IN (1) AND (ProductHighTemp IS NULL OR ProductLowTemp IS NULL))
		) > 0
	BEGIN
		SET @errorString = 'All Product Measurement values are required for Gauge Run & Net Volume tickets'
	END
	
	ELSE IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1) AND Rejected = 0 AND DeleteDateUTC IS NULL
		AND (OpeningGaugeFeet IS NULL OR OpeningGaugeInch IS NULL OR OpeningGaugeQ IS NULL)) > 0
	BEGIN
		SET @errorString = 'All Opening Gauge values are required for Gauge Run tickets'
	END
	ELSE IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1) AND Rejected = 0 AND DeleteDateUTC IS NULL
		AND (ClosingGaugeFeet IS NULL OR ClosingGaugeInch IS NULL OR ClosingGaugeQ IS NULL)) > 0
	BEGIN
		SET @errorString = 'All Closing Gauge values are required for Gauge Run tickets'
	END

	ELSE IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (2) AND Rejected = 0 AND DeleteDateUTC IS NULL
		AND (GrossUnits IS NULL)) > 0
	BEGIN
		SET @errorString = 'Gross Volume value is required for Net Volume tickets'
	END

	ELSE IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (3) AND Rejected = 0 AND DeleteDateUTC IS NULL
		AND (GrossUnits IS NULL OR NetUnits IS NULL)) > 0
	BEGIN
		SET @errorString = 'Gross & Net Volume values are required for Meter Run tickets'
		IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
		RETURN
	END

	ELSE IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1) AND Rejected = 0 AND DeleteDateUTC IS NULL
		AND (SealOff IS NULL OR SealOn IS NULL)) > 0
	BEGIN
		SET @errorString = 'All Seal Off & Seal On values are required for Gauge Run tickets'
	END

	IF (@errorString IS NOT NULL)
	BEGIN
		RAISERROR(@errorString, 16, 1)
		IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	END
	
	PRINT 'trigOrderTicket_IU_Validate COMPLETE'

END


GO
EXEC sp_settriggerorder @triggername=N'[dbo].[trigOrderTicket_IU_Validate]', @order=N'First', @stmttype=N'INSERT'
GO
EXEC sp_settriggerorder @triggername=N'[dbo].[trigOrderTicket_IU_Validate]', @order=N'First', @stmttype=N'UPDATE'
GO

COMMIT
SET NOEXEC OFF