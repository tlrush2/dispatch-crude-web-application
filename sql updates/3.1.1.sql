/*
	-- add OrderSingleExport bit field
	-- fix bug in viewSunocoSundex
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.1.0'
SELECT  @NewVersion = '3.1.1'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

IF COL_LENGTH('tblReportColumnDefinition','OrderSingleExport') IS NULL
BEGIN
	ALTER TABLE tblReportColumnDefinition ADD OrderSingleExport bit NOT NULL CONSTRAINT FK_ReportColumnDefinition_OrderSingleExport DEFAULT (0)
END

UPDATE tblReportColumnDefinition SET OrderSingleExport = 1 WHERE ID IN (7, 8, 9, 25, 26, 46, 47, 80, 81, 82, 83)
GO

ALTER VIEW [dbo].[viewReportColumnDefinition] AS
	SELECT RCD.ID
		, RCD.ReportID
		, RCD.DataField
		, Caption = ISNULL(RCD.Caption, RCD.DataField)
		, RCD.DataFormat
		, RCD.FilterDataField
		, RCD.FilterTypeID
		, RCD.FilterDropDownSql
		, RCD.FilterAllowCustomText
		, RCD.AllowedRoles
		, RCD.OrderSingleExport
		, RFT.FilterOperatorID_CSV
		, BaseFilterCount = (SELECT COUNT(*) FROM tblReportColumnDefinitionBaseFilter WHERE ReportColumnID = RCD.ID)
		, FilterOperatorID_CSV_Delim = ',' + RFT.FilterOperatorID_CSV + ','
	FROM tblReportColumnDefinition RCD
	JOIN tblReportFilterType RFT ON RFT.ID = RCD.FilterTypeID;
GO

/***********************************************/
-- Date Created: 27 Jul 2014
-- Author: Kevin Alons
-- Purpose: add related JOINed fields to the tblUserReportColumnDefinition table results
/***********************************************/
ALTER VIEW [dbo].[viewUserReportColumnDefinition] AS
	SELECT URCD.ID
		, URCD.UserReportID
		, URCD.ReportColumnID
		, URCD.Caption
		, DataFormat = isnull(URCD.DataFormat, RCD.DataFormat)
		, URCD.SortNum
		, URCD.FilterOperatorID
		, URCD.FilterValue1
		, URCD.FilterValue2
		, URCD.Export
		, URCD.CreateDateUTC, URCD.CreatedByUser
		, URCD.LastChangeDateUTC, URCD.LastChangedByUser
		, RCD.DataField
		, BaseCaption = RCD.Caption
		, OutputCaption = coalesce(URCD.Caption, RCD.Caption, RCD.DataField)
		, RCD.FilterTypeID
		, FilterDataField = isnull(RCD.FilterDataField, RCD.DataField)
		, RCD.FilterDropDownSql
		, RCD.FilterAllowCustomText
		, RCD.AllowedRoles
		, RCD.OrderSingleExport
		, FilterOperator = RFO.Name
		, FilterValues = CASE 
				WHEN RFO.ID IN (1,4,5) OR RCD.FilterAllowCustomText = 1 THEN FilterValue1 + ISNULL(' - ' + FilterValue2, '')
				ELSE 
					CASE WHEN FilterValue1 IS NOT NULL THEN '&lt;filtered&gt;' 
						 ELSE NULL 
					END 
			END
	FROM tblUserReportColumnDefinition URCD
	JOIN viewReportColumnDefinition RCD ON RCD.ID = URCD.ReportColumnID
	LEFT JOIN tblReportFilterOperator RFO ON RFO.ID = URCD.FilterOperatorID;

GO
	
/*****************************************************************************************/
-- Author: Kevin Alons
-- Date Created: 27 Feb 2014
-- Purpose: return export final orders in the SunocoSundex
-- TODO: this has some hardcoded values for the SUNOCO customer (shipper), need to make dynamic
/*****************************************************************************************/
ALTER VIEW [dbo].[viewSunocoSundex] AS
SELECT
	_ID = O.ID
	, _CustomerID = O.CustomerID
	, _OrderNum = O.OrderNum
	, Request_Code = CASE WHEN EP.IsNew = 1 THEN 'A' ELSE 'C' END
	, Company_Code = '0007020233'
	, Ticket_Type = UPPER(left(O.TicketType, 1))
	, Ticket_Source_Code = 'BATA'
	, Ticket_Number = isnull(T.CarrierTicketNum, ltrim(O.OrderNum) + 'X')
	, Ticket_Date = dbo.fnDateMMddYYYY(O.OrderDate)
	, SXL_Property_Code = isnull(O.LeaseNum, '')
	, TP_Property_Code = ''
	, Lease_Company_Name = O.Origin
	, Destination = isnull(CDC.Code, '')
	, Tank_Meter_Number = coalesce(T.OriginTankText, (SELECT min(TankNum) FROM tblOriginTank WHERE DeleteDateUTC IS NULL AND OriginID = O.OriginID), '1')
	, Open_Date = dbo.fnDateMMddYYYY(O.OriginArriveTime)
	, Open_Time = dbo.fnTimeOnly(O.OriginArriveTime)
	, Close_Date = dbo.fnDateMMddYYYY(O.OriginDepartTime)
	, Close_Time = dbo.fnTimeOnly(O.OriginDepartTime)
	, Estimated_Volume = cast(ROUND(O.OriginGrossStdUnits, 2) as decimal(18, 2))
	, Gross_Volume = cast(ROUND(O.OriginGrossUnits, 2) as decimal(18, 2))
	, Net_Volume = cast(ROUND(O.OriginNetUnits, 2) as decimal(18, 2))
	, Observed_Gravity = isnull(ltrim(T.ProductObsGravity), '')
	, Observed_Temperature = isnull(ltrim(T.ProductObsTemp), '')
	, Observed_BSW = isnull(ltrim(T.ProductBSW), '')
	, Corrected_Gravity_API = 0
	, Purchaser = 'Sonoco Logistics'
	, First_Reading_Gauge_Ft = isnull(ltrim(T.OpeningGaugeFeet), '')
	, First_Reading_Gauge_In = isnull(ltrim(T.OpeningGaugeInch), '')
	, First_Reading_Gauge_Nu = isnull(ltrim(T.OpeningGaugeQ), '')
	, First_Reading_Gauge_De = 4
	, First_Temperature = isnull(ltrim(T.ProductHighTemp), '')
	, First_Bottom_Ft = isnull(ltrim(T.BottomFeet), '')
	, First_Bottom_In = isnull(ltrim(T.BottomInches), '')
	, First_Bottom_Nu = isnull(ltrim(T.BottomQ), '')
	, First_Bottom_De = 4
	, Second_Reading_Gauge_Ft = isnull(ltrim(T.ClosingGaugeFeet), '')
	, Second_Reading_Gauge_In = isnull(ltrim(T.ClosingGaugeInch), '')
	, Second_Reading_Gauge_Nu = isnull(ltrim(T.ClosingGaugeQ), '')
	, Second_Reading_Gauge_De = 4
	, Second_Temperature = isnull(ltrim(T.ProductLowTemp), '')
	, Second_Bottom_Ft = 0
	, Second_Bottom_In = 0
	, Second_Bottom_Nu = 0
	, Second_Bottom_De = 4
	, Shrinkage_Incrustation_Factor = 1
	, First_Reading_Meter = 0
	, Second_Reading_Meter = 0
	, Meter_Factor = 0
	, Temp_Comp_Meter = ''
	, Avg_Line_Temp = ''
	, Truck_ID = ''
	, Trailer_ID = ''
	, Driver_ID =''
	, Miles = ''
	, County = ''
	, State = ''
	, Invoice_Number = ''
	, Invoice_Date = ''
	, Remarks= ''
	, API_Compliant_Chapter = ''
	, Use_SXL_Calculation = 'Y'
	, Seal_On = isnull(replace(T.SealOn, 'n/a', ''), '')
	, Seal_Off = isnull(replace(T.SealOff, 'n/a', ''), '')
	, Ticket_Exclusion_Cd = CASE WHEN isnull(T.Rejected, O.Rejected) = 1 THEN 'RF' ELSE '' END
	, Confirmation_Number = O.DispatchConfirmNum
	, Split_Flag = CASE WHEN (SELECT COUNT(*) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND DeleteDateUTC IS NULL) > 1 THEN 'Y' ELSE 'N' END
	, Paired_Ticket_Number = isnull((SELECT min(CarrierTicketNum) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND DeleteDateUTC IS NULL AND OT.CarrierTicketNum <> T.CarrierTicketNum), '')
	, Bobtail_Flag = 'N'
	, Ticket_Extra_Info_Flag = ''
FROM viewOrderLocalDates O
JOIN dbo.viewOrderCustomerFinalExportPending EP ON EP.ID = O.ID
LEFT JOIN viewOrderTicket T ON T.OrderID = O.ID AND T.DeleteDateUTC IS NULL
LEFT JOIN dbo.tblCustomerDestinationCode CDC ON CDC.DestinationID = O.DestinationID

GO

EXEC _spRebuildAllObjects
GO

COMMIT
SET NOEXEC OFF