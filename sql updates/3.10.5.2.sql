-- rollback
SET NOEXEC OFF
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.10.5.1'
SELECT  @NewVersion = '3.10.5.2'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Fix to save OrderDate on Driver Transfer'
	UNION
	SELECT @NewVersion, 0, 'Ensure Accept|Pickup|Deliver LastChangeDateUTC is updated on relevant Order update'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/************************************************
 Author:		Kevin Alons
 Create date: 19 Dec 2012
 Description:	trigger to 
			1) validate any changes, and fail the update if invalid changes are submitted
			2) add a unique, incrementing OrderNum to each new Order (manual Identity column)
			3) recompute wait times (origin|destination based on times provided)
			4) generate route table entry for any newly used origin-destination combination
			5) keep Order consistent with Origin.Producer|Operator|UOM & Dest.UOM then the Origin|Dest change
			6) roll the PrintStatus back to NOTFINALIZED (0) when status is rolled back
			7) when DriverID changes, mark those orders AS DELETED and clone the order with the new DriverID and associated records
			8) update any ticket quantities for open orders when the UOM is changed for the Origin
			9) update the Driver.Truck\Trailer\Trailer2 defaults & related DISPATCHED orders when driver updates them on an order
-REMOVED	10) update the Pickup/DeliverLastChangeDateUTC when Origin/Destination is changed (respectively)
			11) (re) apply Settlement Amounts to orders not yet fully settled when status is changed to DELIVERED
			12) if DBAudit is turned on, save an audit record for this Order change
 Changes: 
  - 3.7.4 05/08/15 GSM Added Rack/Bay field to DBAudit logic
  - 3.7.7 - 15 May 2015 - KDA - REMOVE #10 above, instead update the LastChangeDateUTC whenever an Origin or Destination is changed
  - 3.7.11 - 2015/05/18 - KDA - generally use GETUTCDATE for all LastChangeDateUTC revisions (instead of related Order.LastChangeDateUTC, etc)
  - 3.7.12 - 5/19/2015  - KDA - update any existing OrderTicket records when the Order is DISPATCHED to a driver (so the Driver App will ALWAYS receive the existing TICKETS from the GAUGER)
  - 3.7.23 - 06/05/2015 - GSM - DCDRV-154: Ticket Type following Origin Change
  - 3.7.23 - 06/05/2015 - GSM - DCWEB-530 - ensure ASSIGNED orders with a driver defined are updated to DISPATCHED
  - 3.8.1  - 2015/07/04 - KDA - purge any Driver|Gauger App ZPL related SYNC change records when order is AUDITED
  - 3.9.0  - 2015/08/20 - KDA - add invocation of spAutoAuditOrder for DELIVERED orders (that qualify) when the Auto-Audit global setting value is TRUE
							  - add invocation of spAutoApproveOrder for AUDITED orders (that qualify)
  - 3.9.2  - 2015/08/25 - KDA - appropriately use new tblOrder.OrderDate date column
  - 3.9.4  - 2015/08/30 - KDA - performanc optimization to prevent Orderdate computation if no timestamp fields are yet populated
  - 3.9.5  - 2015/08/31 - KDA - more extensive performance optimizations to minimize performance ramifications or computing OrderDate dynamically from OrderRule
  - 3.9.13 - 2015/09/01 - KDA - add (but commented out) some code to validate Arrive|Depart time being present when required
							  - allow orders to be rolled back from AUDITED to DELIVERED when in AUTO-AUDIT mode
							  - always AUTO UNAPPROVE orders when the status is reverted from AUDITED to DELIVERED 
  - 3.9.19 - 2015/09/23 - KDA - remove obsolete reference to tblDriverAppPrintTicketTemplate
  - 3.9.20 - 2015/09/23 - KDA - add support for tblOrderTransfer (do not accomplish #7 above - cloning/delete orders that are driver re-assigned)
  - 3.9.29.5 - 2015/12/03 - JAE - added DestTrailerWaterCapacity and DestRailCarNum to dbaudit trigger
  - 3.9.38 - 2015/12/21 - BB - Add WeightNetUnits (DCWEB-972)
  - 3.9.38 - 2016/01/11 - JAE - Recompute net for destination weights
  - 3.10.5 - 2016/01/29 - KDA - fix to DriverApp/Gauger App purge sync records for ineligible orders [was order.StatusID IN (4), now NOT IN (valid statuses)]
  - 3.10.5.2 - 2016/02/09 - KDA - preserve OrderDate on OrderTransfer (driver transfer)
								- ensure Accept | Pickup | Deliver LastChangeDateUTC is updated on relevant update
			- 2016/02/11 - JAE  - Added DispatchNotes since mobile screens display that data and any changes should trigger the view to refresh
************************************************/
ALTER TRIGGER trigOrder_IU ON tblOrder AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @changesFound bit
	
		-- Note: the TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU') ) = 1 statement prevents recursion
	IF (TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU')) = 1 AND TRIGGER_NESTLEVEL( OBJECT_ID('trigOrderTicket_IU')) = 0) BEGIN

		/**********  START OF VALIDATION SECTION ************************/

		IF (UPDATE(OrderNum) 
			OR UPDATE(PriorityID) 
			OR UPDATE(DueDate) 
			OR UPDATE(RouteID) 
			OR UPDATE(OriginID) 
			OR UPDATE(OriginArriveTimeUTC) 
			OR UPDATE(OriginDepartTimeUTC) 
			OR UPDATE(OriginMinutes) 
			OR UPDATE(OriginWaitNotes) 
			OR UPDATE(OriginBOLNum) 
			OR UPDATE(OriginGrossUnits) 
			OR UPDATE(OriginNetUnits) 
			--OR UPDATE(OriginWeightGrossUnits) -- 3.9.38  Per Maverick 12/21/15 we do not need gross on the order level.
			OR UPDATE(OriginWeightNetUnits) -- 3.9.38
			OR UPDATE(DestinationID) 
			OR UPDATE(DestArriveTimeUTC) 
			OR UPDATE(DestDepartTimeUTC) 
			OR UPDATE(DestMinutes) 
			OR UPDATE(DestWaitNotes) 
			OR UPDATE(DestBOLNum) 
			OR UPDATE(DestGrossUnits) 
			OR UPDATE(DestNetUnits) 
			OR UPDATE(CustomerID) 
			OR UPDATE(CarrierID) 
			OR UPDATE(DriverID) 
			OR UPDATE(TruckID)
			OR UPDATE(TrailerID) 
			OR UPDATE(Trailer2ID) 
			OR UPDATE(OperatorID) 
			OR UPDATE(PumperID) 
			OR UPDATE(TicketTypeID) 
			OR UPDATE(Rejected) 
			OR UPDATE(RejectNotes) 
			OR UPDATE(ChainUp) 
			-- allow this to be changed even in audit status
			--OR UPDATE(OriginTruckMileage) 
			OR UPDATE(OriginTankNum) 
			-- allow this to be changed even in audit status
			--OR UPDATE(DestTruckMileage) 
			OR UPDATE(CarrierTicketNum) 
			OR UPDATE(AuditNotes) 
			OR UPDATE(CreateDateUTC) 
			-- it is permissible to change the ActualMiles value on Audited orders up to the point the Order has been Shipper-Settled
			OR (UPDATE(ActualMiles) AND EXISTS (SELECT * FROM deleted i JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = i.ID WHERE OSS.BatchID IS NOT NULL))
			OR UPDATE(ProducerID) 
			OR UPDATE(CreatedByUser) 
			OR UPDATE(LastChangeDateUTC) 
			OR UPDATE(LastChangedByUser) 
			OR UPDATE(DeleteDateUTC) 
			OR UPDATE(DeletedByUser) 
			OR UPDATE(DestProductBSW)
			OR UPDATE(DestProductGravity) 
			OR UPDATE(DestProductTemp) 
			OR UPDATE(ProductID) 
			OR UPDATE(AcceptLastChangeDateUTC) 
			OR UPDATE(PickupLastChangeDateUTC) 
			OR UPDATE(DeliverLastChangeDateUTC) 
			OR UPDATE(OriginUomID) 
			OR UPDATE(DestUomID) 
			OR UPDATE(PickupPrintStatusID) 
			OR UPDATE(DeliverPrintStatusID)
			OR UPDATE(PickupPrintDateUTC) 
			OR UPDATE(DeliverPrintDateUTC) 
			OR UPDATE(OriginTankID) 
			OR UPDATE(OriginGrossStdUnits) 
			OR UPDATE(DispatchConfirmNum) 
			OR UPDATE(DispatchNotes)
			OR UPDATE(PickupDriverNotes)
			OR UPDATE(DeliverDriverNotes)
			OR UPDATE(OriginWaitReasonID)
			OR UPDATE(DestWaitReasonID)
			OR UPDATE(RejectReasonID) 
			OR UPDATE(DestOpenMeterUnits) 
			OR UPDATE(DestCloseMeterUnits)
			OR UPDATE(DestRackBay)
			OR UPDATE(OrderDate)
			OR UPDATE(DestWeightGrossUnits) 
			OR UPDATE(DestWeightTareUnits)
		)
			--OR UPDATE(ReassignKey)) we don't do any work if this value changes so just ignore it
		BEGIN
			-- only allow the StatusID value to be changed on an audited order
			IF EXISTS (SELECT * FROM deleted WHERE StatusID = 4)
			BEGIN
				-- if the "Allow Audited Order Update (temp only)" setting is TRUE then only warn when this occurs
				IF (dbo.fnToBool(dbo.fnSettingValue(35)) = 1)
					PRINT 'AUDITED orders is being modified - please investigate why!'
				ELSE BEGIN
					-- otherwise (normal behavior) - prevent AUDITED orders from being modified (except to UN-AUDIT them)
					RAISERROR('AUDITED orders cannot be modified!', 16, 1)
					IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
					RETURN
				END
			END
		END
		ELSE IF NOT UPDATE(StatusID) -- NO CHANGES DETECTED SO JUST QUIT
			RETURN

		PRINT 'trigOrder_IU FIRED'
					
		-- ensure the Origin and Destinations are both specified unless the Status is:
		--   (Generated, Assigned, Dispatched or Declined)
		IF  EXISTS(SELECT * FROM inserted O WHERE (O.OriginID IS NULL OR O.DestinationID IS NULL) AND O.StatusID NOT IN (-10,1,2,9))
		BEGIN
			RAISERROR('Invalid Order Status when the Origin and/or Or Destination is not specified', 16, 1)
			IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
			RETURN
		END
		/* this is commented out until we get the Android issues closer to resolved 
		IF EXISTS (SELECT * FROM inserted WHERE StatusID IN (8, 3, 4) AND OriginArriveTimeUTC IS NULL OR OriginDepartTimeUTC IS NULL)
		BEGIN
			RAISERROR('OriginArriveTimeUTC and/or OriginDepartTimeUTC missing when required', 16, 1)
			IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
			RETURN
		END
		IF EXISTS (SELECT * FROM inserted WHERE StatusID IN (3, 4) AND DestArriveTimeUTC IS NULL OR DestDepartTimeUTC IS NULL)
		BEGIN
			RAISERROR('DestArriveTimeUTC and/or DestDepartTimeUTC missing when required', 16, 1)
			IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
			RETURN
		END
		******************************************************************************/
		
		/**********  END OF VALIDATION SECTION ************************/

		WHILE (SELECT count(*) FROM inserted i JOIN tblOrder O ON O.ID = i.ID WHERE O.OrderNum IS NULL) > 0
		BEGIN
			UPDATE tblOrder 
			  SET OrderNum = isnull((SELECT max(OrderNum) FROM tblOrder), 100000) + 1
				, CreateDateUTC = getutcdate()
			WHERE ID = (SELECT min(O.ID) FROM tblOrder O JOIN inserted i ON i.ID = O.ID WHERE O.OrderNum IS NULL)
		END
		
		-- re-compute the OriginMinutes (in case the website failed to compute it properly)
		IF UPDATE(OriginArriveTimeUTC) OR UPDATE(OriginDepartTimeUTC)
		BEGIN
			UPDATE tblOrder SET OriginMinutes = datediff(minute, i.OriginArriveTimeUTC, i.OriginDepartTimeUTC)
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
		END
		-- re-compute the DestMinutes (in case the website failed to compute it properly)
		IF UPDATE(DestArriveTimeUTC) OR UPDATE(DestDepartTimeUTC)
		BEGIN
			UPDATE tblOrder SET DestMinutes = datediff(minute, i.DestArriveTimeUTC, i.DestDepartTimeUTC)
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
		END
		
		/* ensure any changes to the order always update the Order.OrderDate field */
		IF (UPDATE(CustomerID) 
			OR UPDATE(CarrierID) 
			OR UPDATE(ProductID) 
			OR UPDATE(OriginID) 
			OR UPDATE(DestinationID) 
			OR UPDATE(ProducerID) 
			OR UPDATE(OriginArriveTimeUTC) OR UPDATE(OriginDepartTimeUTC)
			OR UPDATE(DestArriveTimeUTC) OR UPDATE(DestDepartTimeUTC))
		BEGIN
			UPDATE tblOrder 
			  SET OrderDate = dbo.fnOrderDate(O.ID)
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
			JOIN deleted d ON d.ID = i.ID
			LEFT JOIN tblOrderSettlementCarrier OSC ON OSC.OrderID = i.ID
			LEFT JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = i.ID
			WHERE i.StatusID <> 4 
			  -- the order has at least 1 valid timestamp
			  AND (i.OriginArriveTimeUTC IS NOT NULL OR i.OriginDepartTimeUTC IS NOT NULL OR i.DestArriveTimeUTC IS NOT NULL OR i.DestDepartTimeUTC IS NOT NULL)
			  -- the order is not yet settled
			  AND OSC.BatchID IS NULL
			  AND OSS.BatchID IS NULL
		END
		
		-- ensure missing Routes are created and assigned for the specified Origin/Destination combination
		IF UPDATE(OriginID) OR UPDATE(DestinationID)
		BEGIN
			-- create any missing Route records
			INSERT INTO tblRoute (OriginID, DestinationID, CreateDateUTC, CreatedByUser)
				SELECT i.OriginID, i.DestinationID, GETUTCDATE(), ISNULL(i.LastChangedByUser, i.CreatedByUser)
				FROM inserted i
				LEFT JOIN tblRoute R ON R.OriginID = i.OriginID AND R.DestinationID = i.DestinationID
				WHERE i.OriginID IS NOT NULL AND i.DestinationID IS NOT NULL AND R.ID IS NULL
			
			-- ensure the Order records refer to the correct Route (ID)
			UPDATE tblOrder SET RouteID = R.ID
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
			LEFT JOIN tblRoute R ON R.OriginID = O.OriginID AND R.DestinationID = O.DestinationID
			WHERE O.RouteID IS NULL OR O.RouteID <> R.ID
			
			-- update the ActualMiles from the related Route
			UPDATE tblOrder SET ActualMiles = R.ActualMiles
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
			JOIN tblRoute R ON R.ID = O.RouteID
			WHERE O.ActualMiles <> R.ActualMiles
		END
		
		IF (UPDATE(OriginID))
		BEGIN
			-- update Order.ProducerID/OperatorID/PumperID to match what is assigned to the new Origin
			UPDATE tblOrder 
				SET TicketTypeID = OO.TicketTypeID
					, ProducerID = OO.ProducerID
					, OperatorID = OO.OperatorID
					, PumperID = OO.PumperID
					, LastChangeDateUTC = GETUTCDATE() 
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			WHERE d.OriginID <> O.OriginID

			-- update Order.OriginUomID to match what is assigned to the new Origin (quantities are updated below)
			UPDATE tblOrder SET OriginUomID = OO.UomID
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			WHERE d.OriginID <> O.OriginID AND d.OriginUomID <> OO.UomID
		END
		
		-- keep the DestUomID in sync with the Destination (units are updated below)
		IF (UPDATE(DestinationID))
		BEGIN
			-- update Order.DestUomID to match what is assigned to the new Destination
			UPDATE tblOrder 
			  SET DestUomID = DD.UomID
				, LastChangeDateUTC = GETUTCDATE()
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID
			JOIN tblDestination DD ON DD.ID = O.DestinationID
			WHERE d.DestinationID <> O.DestinationID AND d.DestUomID <> DD.UomID
		END
		
		-- ensure that any set PrintStatusID values are reset to 0 when backing up the general StatusID value
		IF (UPDATE(StatusID))
		BEGIN
			UPDATE tblOrder 
			  SET DeliverPrintStatusID = 0 
				, LastChangeDateUTC = GETUTCDATE()
			  FROM tblOrder O
			  JOIN deleted d ON d.ID = O.ID
			WHERE O.StatusID <> d.StatusID AND O.StatusID IN (-10, 1, 2, 7, 8) -- Generated, Assigned, Dispatched, Accepted, Picked-Up

			UPDATE tblOrder 
			  SET PickupPrintStatusID = 0 
				, LastChangeDateUTC = GETUTCDATE()
			  FROM tblOrder O
			  JOIN deleted d ON d.ID = O.ID
			WHERE O.StatusID <> d.StatusID AND O.StatusID IN (-10, 1, 2, 7) -- Generated, Assigned, Dispatched, Accepted
			
			/* ensure that any orders that are DISPATCHED - any existing orders are TOUCHED so they are syncable to the Driver App */
			UPDATE tblOrderTicket
				SET LastChangeDateUTC = GETUTCDATE()
			WHERE OrderID IN (
				SELECT i.ID 
				FROM inserted i
				JOIN deleted d ON d.ID = i.ID
				WHERE i.StatusID <> d.StatusID AND i.StatusID IN (2) -- DISPATCHED
			)
		END

		-- just updating the tickets here, trigOrderTicket_IU will in turn update this table
		-- ensure any existing OrderTickets are recomputed with the new OriginUOM
		UPDATE tblOrderTicket
		  SET GrossUnits = dbo.fnConvertUOM(GrossUnits, d.OriginUomID, O.OriginUomID)
			, NetUnits = dbo.fnConvertUOM(NetUnits, d.OriginUomID, O.OriginUomID)
			-- 3.9.38 - added to also force the WeightNetUnits be recomputed
			, WeightGrossUnits = dbo.fnConvertUOM(WeightGrossUnits, d.OriginUomID, O.OriginUomID)
			, LastChangeDateUTC = GETUTCDATE()
		FROM tblOrderTicket OT
		JOIN tblOrder O ON O.ID = OT.OrderID
		JOIN deleted d ON d.ID = O.ID
		WHERE O.OriginUomID <> d.OriginUomID
		
		-- recalculate the Destination Unit values if only if the DestUomID changed but the values didn't
		UPDATE tblOrder
		  SET DestGrossUnits = dbo.fnConvertUOM(O.DestGrossUnits, d.DestUomID, O.DestUomID)
			, DestNetUnits = dbo.fnConvertUOM(O.DestNetUnits, d.DestUomID, O.DestUomID)
			, LastChangeDateUTC = GETUTCDATE()
		FROM tblOrder O
		JOIN deleted d ON d.ID = O.ID
		WHERE d.DestUomID <> O.DestUomID 
		  AND d.DestGrossUnits = O.DestGrossUnits
		  AND d.DestNetUnits = O.DestNetUnits

		/* DCWEB-530 - ensure any ASSIGNED orders with a DRIVER assigned is set to DISPATCHED */
		UPDATE tblOrder SET StatusID = 2 /*DISPATCHED*/
		FROM tblOrder O
		JOIN inserted i ON I.ID = O.ID
		WHERE i.StatusID = 1 /*ASSIGNED*/ AND i.CarrierID IS NOT NULL AND i.DriverID IS NOT NULL AND i.DeleteDateUTC IS NULL

		/**************************************************************************************************************/
		/* 3.10.5.2 - ensure the Accept|Pickup|Deliver LastChangeDateUTC values are updated when any relevant data field changes */
		UPDATE tblOrder 
		  SET AcceptLastChangeDateUTC = LastChangeDateUTC
		WHERE ID IN (
			SELECT ID FROM (
				SELECT ID, TruckID, TrailerID, Trailer2ID, DispatchNotes FROM inserted 
				EXCEPT 
				SELECT ID, TruckID, TrailerID, Trailer2ID, DispatchNotes FROM deleted
			) X
		)
		UPDATE tblOrder 
		  SET PickupLastChangeDateUTC = LastChangeDateUTC
		WHERE ID IN (
			SELECT ID FROM (
				SELECT ID, OriginArriveTimeUTC, OriginDepartTimeUTC, OriginMinutes, OriginWaitReasonID, OriginWaitNotes
                    , CarrierTicketNum, OriginBOLNum, Rejected, RejectReasonID, RejectNotes, Chainup, OriginTruckMileage
                    , PickupPrintStatusID, PickupPrintDateUTC, OriginGrossUnits, OriginGrossStdUnits, OriginNetUnits, OriginWeightNetUnits
                    , PickupDriverNotes, DispatchNotes
                    , TicketTypeID 
				FROM inserted 
				EXCEPT 
				SELECT ID, OriginArriveTimeUTC, OriginDepartTimeUTC, OriginMinutes, OriginWaitReasonID, OriginWaitNotes
                    , CarrierTicketNum, OriginBOLNum, Rejected, RejectReasonID, RejectNotes, Chainup, OriginTruckMileage
                    , PickupPrintStatusID, PickupPrintDateUTC, OriginGrossUnits, OriginGrossStdUnits, OriginNetUnits, OriginWeightNetUnits
                    , PickupDriverNotes, DispatchNotes
                    , TicketTypeID 
				FROM deleted
			) X
		)
		UPDATE tblOrder 
		  SET DeliverLastChangeDateUTC = LastChangeDateUTC
		WHERE ID IN (
			SELECT ID FROM (
				SELECT ID, DeliverLastChangeDateUTC, DestArriveTimeUTC, DestDepartTimeUTC, DestMinutes, DestWaitReasonID, DestWaitNotes
                    , DestNetUnits, DestGrossUnits
                    , DestBOLNum, DestRailcarNum, DestTrailerWaterCapacity, DestOpenMeterUnits, DestCloseMeterUnits, DestProductBSW
                    , DestProductGravity, DestProductTemp, DestTruckMileage 
                    , DeliverPrintStatusID, DeliverPrintDateUTC, DeliverDriverNotes, DestRackBay
                    , DestWeightGrossUnits, DestWeightTareUnits, DestWeightNetUnits
                    , DispatchNotes 
				FROM inserted
				EXCEPT
				SELECT ID, DeliverLastChangeDateUTC, DestArriveTimeUTC, DestDepartTimeUTC, DestMinutes, DestWaitReasonID, DestWaitNotes
                    , DestNetUnits, DestGrossUnits
                    , DestBOLNum, DestRailcarNum, DestTrailerWaterCapacity, DestOpenMeterUnits, DestCloseMeterUnits, DestProductBSW
                    , DestProductGravity, DestProductTemp, DestTruckMileage 
                    , DeliverPrintStatusID, DeliverPrintDateUTC, DeliverDriverNotes, DestRackBay
                    , DestWeightGrossUnits, DestWeightTareUnits, DestWeightNetUnits 
                    , DispatchNotes
				FROM deleted
			) X
		)
		/**************************************************************************************************************/

		-- 3.9.38 - 2016/01/11 - JAE - recompute the Net Weight of any changed Mineral Run orders
		IF UPDATE(DestWeightGrossUnits) OR UPDATE(DestWeightTareUnits) OR UPDATE(DestUomID) BEGIN
			UPDATE tblOrder
				SET DestWeightNetUnits = O.DestWeightGrossUnits - O.DestWeightTareUnits
			FROM tblOrder O
			JOIN tblDestination D ON D.ID = O.DestinationID
			JOIN inserted i on i.ID = O.ID
			WHERE D.TicketTypeID = 9 -- Mineral Run tickets only
		END 			

		/*************************************************************************************************************/
		/* handle driver changes - "orphan" the order/ticket (with a new ORDERNUM) - and clone it for the new driver */
		IF (UPDATE(DriverID))
		BEGIN
			-- create cloned order/ticket records (these records will be cloned then the original reset to orphaned status)
			SELECT O.*, NewOrderNum = CAST(NULL as int)
			INTO #clone
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID AND d.DriverID IS NOT NULL AND d.DriverID <> isnull(O.DriverID, 0)
			/* JOIN to tblOrderTransfer so we can prevent treating an OrderTransfer "driver change" as a Orphaned Order */
			LEFT JOIN tblOrderTransfer OTR ON OTR.OrderID = O.ID  /* 3.9.20 - added */
			WHERE OTR.OrderID IS NULL

			/* generate the next OrderNum values for these new records */
			DECLARE @newOrderNum int; SET @newOrderNum = isnull((SELECT max(OrderNum) FROM tblOrder), 100000) + 1;
			DECLARE @newReassignKey int; SET @newReassignKey = isnull((SELECT max(ReassignKey) FROM tblOrder), 0) + 1;
			WHILE (SELECT count(*) FROM #clone WHERE NewOrderNum IS NULL) > 0
			BEGIN
				UPDATE #clone
				  SET NewOrderNum = @newOrderNum, ReassignKey = isnull(ReassignKey, @newReassignKey)
					, CreateDateUTC = getutcdate()
				WHERE ID = (SELECT min(ID) FROM #clone WHERE NewOrderNum IS NULL)
				-- if the @newReassignKey wasn't used above, then that # will be skipped, but this should not be an issue
				SELECT @newOrderNum = @newOrderNum + 1, @newReassignKey = @newReassignKey + 1
			END
			
			-- reset the existing records as orphaned
			UPDATE tblOrder
				SET DeleteDateUTC = isnull(O.DeleteDateUTC, GETUTCDATE()), DeletedByUser = isnull(O.DeletedByUser, O.LastChangedByUser)
					, CarrierID = d.CarrierID, DriverID = d.DriverID, TruckID = d.TruckID, TrailerID = d.TrailerID, Trailer2ID = d.Trailer2ID
					, DispatchConfirmNum = NULL
					, ReassignKey = C.ReassignKey
			FROM tblOrder O
			JOIN #clone C ON C.ID = O.ID
			JOIN deleted d on D.ID = O.id

			-- create the cloned records (which is essentially the original with a new ID and the Carrrier & default Truck|Trailers of the new driver)
			INSERT INTO tblOrder (OrderDate, OrderNum, StatusID, PriorityID, DueDate, RouteID, OriginID, OriginArriveTimeUTC, OriginDepartTimeUTC
				, OriginMinutes, OriginWaitNotes, OriginBOLNum, OriginGrossUnits, OriginNetUnits, DestinationID, DestArriveTimeUTC, DestDepartTimeUTC
				, DestMinutes, DestWaitNotes, DestBOLNum, DestGrossUnits, DestNetUnits, CustomerID, CarrierID, DriverID, TruckID, TrailerID
				, Trailer2ID, OperatorID, PumperID, TicketTypeID, Rejected, RejectNotes, ChainUp, OriginTruckMileage, OriginTankNum, DestTruckMileage
				, CarrierTicketNum, AuditNotes, CreateDateUTC, ActualMiles, ProducerID, CreatedByUser, LastChangeDateUTC, LastChangedByUser
				, DeleteDateUTC, DeletedByUser, DestProductBSW, DestProductGravity, DestProductTemp, ProductID, AcceptLastChangeDateUTC
				, PickupLastChangeDateUTC, DeliverLastChangeDateUTC, OriginUomID, DestUomID, PickupPrintStatusID, DeliverPrintStatusID
				, PickupPrintDateUTC, DeliverPrintDateUTC, OriginTankID, OriginGrossStdUnits, DispatchNotes, PickupDriverNotes, DeliverDriverNotes
				, DispatchConfirmNum, OriginWaitReasonID, DestWaitReasonID, RejectReasonID, DestOpenMeterUnits, DestCloseMeterUnits
				, OriginWeightNetUnits, ReassignKey)
				SELECT OrderDate, NewOrderNum, StatusID, PriorityID, DueDate, RouteID, OriginID, OriginArriveTimeUTC, OriginDepartTimeUTC, OriginMinutes, OriginWaitNotes
					, OriginBOLNum, OriginGrossUnits, OriginNetUnits, DestinationID, DestArriveTimeUTC, DestDepartTimeUTC, DestMinutes, DestWaitNotes
					, DestBOLNum, DestGrossUnits, DestNetUnits, CustomerID, D.CarrierID, DriverID, D.TruckID, D.TrailerID, D.Trailer2ID, OperatorID
					, PumperID, TicketTypeID, Rejected, RejectNotes, ChainUp, OriginTruckMileage, OriginTankNum, DestTruckMileage, CarrierTicketNum
					, AuditNotes, O.CreateDateUTC, ActualMiles, ProducerID, O.CreatedByUser, GETUTCDATE(), O.LastChangedByUser, O.DeleteDateUTC, O.DeletedByUser
					, DestProductBSW, DestProductGravity, DestProductTemp, ProductID, AcceptLastChangeDateUTC, PickupLastChangeDateUTC, DeliverLastChangeDateUTC
					, OriginUomID, DestUomID, PickupPrintStatusID, DeliverPrintStatusID, PickupPrintDateUTC, DeliverPrintDateUTC, OriginTankID
					, OriginGrossStdUnits, DispatchNotes, PickupDriverNotes, DeliverDriverNotes, DispatchConfirmNum, OriginWaitReasonID, DestWaitReasonID
					, RejectReasonID, DestOpenMeterUnits, DestCloseMeterUnits, OriginWeightNetUnits, ReassignKey
				FROM #clone O
				LEFT JOIN tblDriver D ON D.ID = O.DriverID
		
			-- cache the affected tickets
			SELECT OT.*, C.NewOrderNum, OrderDeletedByUser = C.LastChangedByUser
			INTO #cloneTicket
			FROM tblOrderTicket OT
			JOIN #clone C ON C.ID = OT.OrderID
			WHERE OT.DeleteDateUTC IS NULL
			
			-- create the cloned Order Ticket records (copy of existing ticket(s) pointed at new "original" Order)
			INSERT INTO tblOrderTicket (OrderID, CarrierTicketNum, TicketTypeID, TankNum, ProductObsGravity, ProductObsTemp, ProductBSW, OpeningGaugeFeet
				, OpeningGaugeInch, OpeningGaugeQ, ClosingGaugeFeet, ClosingGaugeInch, ClosingGaugeQ, GrossUnits, NetUnits, Rejected, RejectNotes
				, SealOff, SealOn, BOLNum, ProductHighTemp, ProductLowTemp, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser
				, DeleteDateUTC, DeletedByUser, UID, FromMobileApp, OriginTankID, BottomFeet, BottomInches, BottomQ, GrossStdUnits, RejectReasonID
				, MeterFactor, OpenMeterUnits, CloseMeterUnits, WeightTareUnits, WeightGrossUnits, WeightNetUnits)
				SELECT O.ID, CT.CarrierTicketNum, CT.TicketTypeID, CT.TankNum, CT.ProductObsGravity, CT.ProductObsTemp, CT.ProductBSW, CT.OpeningGaugeFeet
					, CT.OpeningGaugeInch, CT.OpeningGaugeQ, CT.ClosingGaugeFeet, CT.ClosingGaugeInch, CT.ClosingGaugeQ, CT.GrossUnits, CT.NetUnits
					, CT.Rejected, CT.RejectNotes, CT.SealOff, CT.SealOn, CT.BOLNum, CT.ProductHighTemp, CT.ProductLowTemp, CT.CreateDateUTC, CT.CreatedByUser
					, GETUTCDATE(), CT.LastChangedByUser, CT.DeleteDateUTC, CT.DeletedByUser, newid(), CT.FromMobileApp, CT.OriginTankID, CT.BottomFeet
					, CT.BottomInches, CT.BottomQ, CT.GrossStdUnits, CT.RejectReasonID, CT.MeterFactor, CT.OpenMeterUnits, CT.CloseMeterUnits, WeightTareUnits
					, WeightGrossUnits, WeightNetUnits
				FROM #cloneTicket CT
				JOIN tblOrder O ON O.OrderNum = CT.NewOrderNum
		END
		/*************************************************************************************************************/
		
		/* HANDLE TRUCK | TRAILER | TRAILER2 default changes when the driver updates his Truck | Trailer | Trailer2 on ACCEPTANCE */
		-- TRUCK
		IF (UPDATE(TruckID) AND EXISTS (
			SELECT i.* 
			FROM inserted i 
			JOIN deleted d ON d.ID = i.ID
			  AND i.DriverID = d.DriverID 
			  AND i.TruckID <> d.TruckID
			WHERE i.StatusID IN (7, 8, 3) -- ACCEPTED, PICKEDUP, DELIVERED
			  AND d.StatusID IN (2))) -- DISPATCHED
		BEGIN
			UPDATE tblDriver
			  SET TruckID = i.TruckID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblDriver DR
			JOIN inserted i ON i.DriverID = DR.ID
			JOIN deleted d ON d.DriverID = DR.ID
			WHERE i.TruckID <> d.TruckID
			
			UPDATE tblOrder
			  SET TruckID = i.TruckID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblOrder O
			JOIN inserted i ON i.DriverID = O.DriverID 
			  AND O.ID <> i.ID
			  AND O.StatusID IN (2) -- DISPATCHED
			  AND i.TruckID <> O.TruckID
			  AND O.DeleteDateUTC IS NULL
		END
		-- TRAILER
		IF (UPDATE(TrailerID) AND EXISTS (
			SELECT i.* 
			FROM inserted i 
			JOIN deleted d ON d.ID = i.ID
			  AND i.DriverID = d.DriverID 
			  AND i.TrailerID <> d.TrailerID
			WHERE i.StatusID IN (7, 8, 3) -- ACCEPTED, PICKEDUP, DELIVERED
			  AND d.StatusID IN (2))) -- DISPATCHED
		BEGIN
			UPDATE tblDriver
			  SET TrailerID = i.TrailerID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblDriver DR
			JOIN inserted i ON i.DriverID = DR.ID
			JOIN deleted d ON d.DriverID = DR.ID
			WHERE i.TrailerID <> d.TrailerID
			
			UPDATE tblOrder
			  SET TrailerID = i.TrailerID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblOrder O
			JOIN inserted i ON i.DriverID = O.DriverID 
			  AND O.ID <> i.ID
			  AND O.StatusID IN (2) -- DISPATCHED
			  AND i.TrailerID <> O.TrailerID
			  AND O.DeleteDateUTC IS NULL
		END
		-- TRAILER 2
		IF (UPDATE(Trailer2ID) AND EXISTS (
			SELECT i.* 
			FROM inserted i 
			JOIN deleted d ON d.ID = i.ID
			  AND i.DriverID = d.DriverID 
			  AND isnull(i.Trailer2ID, 0) <> isnull(d.Trailer2ID, 0)
			WHERE i.StatusID IN (7, 8, 3) -- ACCEPTED, PICKEDUP, DELIVERED
			  AND d.StatusID IN (2))) -- DISPATCHED
		BEGIN
			UPDATE tblDriver
			  SET TrailerID = i.TrailerID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblDriver DR
			JOIN inserted i ON i.DriverID = DR.ID
			JOIN deleted d ON d.DriverID = DR.ID
			WHERE isnull(i.Trailer2ID, 0) <> isnull(d.Trailer2ID, 0)
			
			UPDATE tblOrder
			  SET Trailer2ID = i.Trailer2ID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblOrder O
			JOIN inserted i ON i.DriverID = O.DriverID 
			  AND O.ID <> i.ID
			  AND O.StatusID IN (2) -- DISPATCHED
			  AND isnull(i.Trailer2ID, 0) <> isnull(O.Trailer2ID, 0)
			  AND O.DeleteDateUTC IS NULL
		END
		/*************************************************************************************************************/
	
		DECLARE @deliveredIDs TABLE (ID int)
		DECLARE @auditedIDs TABLE (ID int)
		DECLARE @id int
		DECLARE @autoAudit bit; SET @autoAudit = dbo.fnToBool(dbo.fnSettingValue(54))
		DECLARE @toAudit bit
		-- Auto-Audit/Apply Settlement Rates/Amounts/Auto-Approve to Order when STATUS is changed to DELIVERED (until it is FINAL settled)
		IF (UPDATE(StatusID) OR UPDATE(DeliverPrintStatusID))
		BEGIN
			INSERT INTO @deliveredIDs (ID)
				SELECT i.ID 
				FROM inserted i
				JOIN tblPrintStatus iPS ON iPS.ID = i.DeliverPrintStatusID
				JOIN deleted d ON d.ID = i.ID
				JOIN tblPrintStatus dPS ON dPS.ID = d.DeliverPrintStatusID
				WHERE i.StatusID = 3 AND d.StatusID <> 4 AND iPS.IsCompleted = 1 AND i.StatusID + iPS.IsCompleted <> d.StatusID + dPS.IsCompleted
			
			SELECT @id = MIN(ID) FROM @deliveredIDs
			WHILE @id IS NOT NULL
			BEGIN
				-- attempt to AUTO-AUDIT the order if this feature is enabled (global setting)
				IF (@autoAudit = 1)
				BEGIN
					EXEC spAutoAuditOrder @ID, 'System', @toAudit OUTPUT
					/* if the order was updated to AUDITED status, then we need to attempt also to auto-approve [below] */
					IF (@toAudit = 1)
						INSERT INTO @auditedIDs VALUES (@id)
				END
				-- attempt to apply rates to all newly DELIVERED orders
				EXEC spApplyRatesBoth @id, 'System' 
				SET @id = (SELECT MIN(id) FROM @deliveredIDs WHERE ID > @id)
			END
		END

		-- Auto-Approve any un-approved orders when STATUS changed to AUDIT
		IF (UPDATE(StatusID) OR EXISTS (SELECT * FROM @auditedIDs))
		BEGIN
			INSERT INTO @auditedIDs (ID)
				SELECT i.ID 
				FROM inserted i
				JOIN deleted d ON d.ID = i.ID
				LEFT JOIN tblOrderApproval OA ON OA.OrderID = i.ID
				LEFT JOIN @auditedIDs AID ON AID.ID = i.ID
				WHERE i.StatusID = 4 AND d.StatusID <> 4 AND OA.OrderID IS NULL AND AID.ID IS NULL

			SELECT @id = MIN(ID) FROM @auditedIDs
			WHILE @id IS NOT NULL
			BEGIN
				EXEC spAutoApproveOrder @id, 'System'
				SET @id = (SELECT MIN(id) FROM @auditedIDs WHERE ID > @id)
			END
			
		END
		
		/* auto UNAPPROVE any orders that have their status reverted from AUDITED */
		IF (UPDATE(StatusID))
		BEGIN
			DELETE FROM tblOrderApproval
			WHERE OrderID IN (
				SELECT i.ID
				FROM inserted i
				JOIN deleted d ON d.ID = i.ID
				JOIN tblOrderApproval OA ON OA.OrderID = i.ID
				WHERE d.StatusID = 4 AND i.StatusID <> 4
			)
		END

		-- purge any DriverApp/Gauger App sync records for any orders that are not in an DRIVER APP eligible status
		IF UPDATE(StatusID) AND EXISTS (SELECT ID FROM inserted WHERE StatusID NOT IN (2,7,8,3))
		BEGIN
			DELETE FROM tblDriverAppPrintHeaderImageSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID NOT IN (2,7,8,3))
			DELETE FROM tblDriverAppPrintPickupTemplateSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID NOT IN (2,7,8,3))
			DELETE FROM tblDriverAppPrintDeliverTemplateSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID NOT IN (2,7,8,3))
			DELETE FROM tblOrderAppChanges WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID NOT IN (2,7,8,3))
		END
		-- purge any DriverApp/Gauger App sync records for any orders that are not in a GAUGER APP eligible status
		IF UPDATE(StatusID) AND EXISTS (SELECT ID FROM inserted WHERE StatusID NOT IN (-9,-11))
		BEGIN
			DELETE FROM tblGaugerAppPrintHeaderImageSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID NOT IN (-9,-11))
			DELETE FROM tblGaugerAppPrintPickupTemplateSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID NOT IN (-9,-11))
			DELETE FROM tblGaugerAppPrintTicketTemplateSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID NOT IN (-9,-11))
		END
				
		--NOTE: we do not update the DestOpenMeterUnits/DestCloseMeterUnits since they don't auto update
		--  (and they simply represent a reading that is assumed to in the DestUomID UOM at entry)
		--  (if they are updated in the future [manually] the will calculate again using the DestUomID at that time)

		IF EXISTS (SELECT * FROM inserted EXCEPT SELECT * FROM deleted)
		BEGIN
			-- optionally add tblOrderDBAudit records
			BEGIN TRY
				IF EXISTS(SELECT * FROM tblSetting WHERE ID = 32 AND (Value LIKE 'true' OR Value LIKE 'yes'))
					INSERT INTO tblOrderDbAudit (DBAuditDate, ID, OrderNum, StatusID, PriorityID, DueDate, RouteID, OriginID
						, OriginArriveTimeUTC, OriginDepartTimeUTC, OriginMinutes, OriginWaitNotes, OriginBOLNum, OriginGrossUnits
						, OriginNetUnits, DestinationID, DestArriveTimeUTC, DestDepartTimeUTC, DestMinutes, DestWaitNotes, DestBOLNum
						, DestGrossUnits, DestNetUnits, CustomerID, CarrierID, DriverID, TruckID, TrailerID, Trailer2ID, OperatorID
						, PumperID, TicketTypeID, Rejected, RejectNotes, ChainUp, OriginTruckMileage, OriginTankNum, DestTruckMileage
						, CarrierTicketNum, AuditNotes, CreateDateUTC, ActualMiles, ProducerID, CreatedByUser, LastChangeDateUTC
						, LastChangedByUser, DeleteDateUTC, DeletedByUser, DestProductBSW, DestProductGravity, DestProductTemp
						, ProductID, AcceptLastChangeDateUTC, PickupLastChangeDateUTC, DeliverLastChangeDateUTC, OriginUomID, DestUomID
						, PickupPrintStatusID, DeliverPrintStatusID, PickupPrintDateUTC, DeliverPrintDateUTC, OriginTankID
						, OriginGrossStdUnits, DispatchNotes, PickupDriverNotes, DeliverDriverNotes, DispatchConfirmNum, OriginWaitReasonID
						, DestWaitReasonID, RejectReasonID, DestOpenMeterUnits, DestCloseMeterUnits, ReassignKey, DestRackBay, OrderDate
						, DestTrailerWaterCapacity, DestRailCarNum, /*OriginWeightGrossUnits,*/ OriginWeightNetUnits, DestWeightGrossUnits
						, DestWeightTareUnits, DestWeightNetUnits)
						SELECT GETUTCDATE(), ID, OrderNum, StatusID, PriorityID, DueDate, RouteID, OriginID, OriginArriveTimeUTC
							, OriginDepartTimeUTC, OriginMinutes, OriginWaitNotes, OriginBOLNum, OriginGrossUnits, OriginNetUnits
							, DestinationID, DestArriveTimeUTC, DestDepartTimeUTC, DestMinutes, DestWaitNotes, DestBOLNum, DestGrossUnits
							, DestNetUnits, CustomerID, CarrierID, DriverID, TruckID, TrailerID, Trailer2ID, OperatorID, PumperID
							, TicketTypeID, Rejected, RejectNotes, ChainUp, OriginTruckMileage, OriginTankNum, DestTruckMileage
							, CarrierTicketNum, AuditNotes, CreateDateUTC, ActualMiles, ProducerID, CreatedByUser, LastChangeDateUTC
							, LastChangedByUser, DeleteDateUTC, DeletedByUser, DestProductBSW, DestProductGravity, DestProductTemp
							, ProductID, AcceptLastChangeDateUTC, PickupLastChangeDateUTC, DeliverLastChangeDateUTC, OriginUomID
							, DestUomID, PickupPrintStatusID, DeliverPrintStatusID, PickupPrintDateUTC, DeliverPrintDateUTC, OriginTankID
							, OriginGrossStdUnits, DispatchNotes, PickupDriverNotes, DeliverDriverNotes, DispatchConfirmNum
							, OriginWaitReasonID, DestWaitReasonID, RejectReasonID, DestOpenMeterUnits, DestCloseMeterUnits, ReassignKey
							, DestRackBay, OrderDate, DestTrailerWaterCapacity, DestRailCarNum, /*OriginWeightGrossUnits,*/ OriginWeightNetUnits
							, DestWeightGrossUnits, DestWeightTareUnits, DestWeightNetUnits
						FROM deleted d
			END TRY
			BEGIN CATCH
				PRINT 'trigOrder_IU.DBAUDIT FAILURE: ' + ERROR_MESSAGE()
			END CATCH
		END
		
	PRINT 'trigOrder_IU COMPLETE'

	END
END

GO
EXEC sp_settriggerorder @triggername=N'[dbo].[trigOrder_IU]', @order=N'First', @stmttype=N'INSERT'
GO
EXEC sp_settriggerorder @triggername=N'[dbo].[trigOrder_IU]', @order=N'First', @stmttype=N'UPDATE'
GO


/*****************************************
-- Date Created: ??
-- Author: Kevin Alons, Modified by Geoff Mochau
-- Purpose: specialized, db-level logic enforcing business rules, audit changes, etc
-- Changes:
    - 3.8.12 - 2015/08/04 - GSM - use the revised (3-temperature) API function
	- 3.8.10 - 2015/07/26 - KDA - always update the associated tblOrder.LastChangeDateUTC to NOW when a Ticket is changed)
	- 3.9.29.5 - 2015/12/03 - JAE - add ProductBSW to dbaudit trigger
	- 3.9.38 - 2015/12/17 - BB - Add WeightTareUnits, WeightGrossUnits, and WeightNetUnits columns to db audit (DCWEB-972)
	- 3.9.38 - 2015/12/21 - BB - Add validation, calculations, etc. for WeightTareUnits, WeightGrossUnits, and WeightNetUnits (DCWEB-972)
	- 3.10.5.2 - 2016/02/11 - JAE - Update to order table to sync ticket changes also resets the Pickup Last Change Date
*****************************************/
ALTER TRIGGER trigOrderTicket_IU ON tblOrderTicket AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	-- Note: the TRIGGER_NESTLEVEL( OBJECT_ID('trigOrderTicket_IU') ) = 1 statement prevents recursion
	IF (TRIGGER_NESTLEVEL( OBJECT_ID('trigOrderTicket_IU')) = 1) BEGIN
		
		-- only do anything if something actually changed
		IF EXISTS (SELECT * FROM inserted EXCEPT SELECT * FROM deleted)
		BEGIN
			PRINT 'trigOrderTicket_IU FIRED'
			
			/**********  START OF VALIDATION SECTION ************************/
			DECLARE @errorString varchar(1000); SET @errorString = ''

			IF EXISTS (
				SELECT * 
				FROM tblOrder 
				WHERE ID IN (SELECT OrderID FROM inserted) 
				  -- prevent changes to any tickets belonging to a Delivered or Audited Order
				  AND StatusID IN (4))
			BEGIN
				IF (dbo.fnToBool(dbo.fnSettingValue(35)) = 1)
					PRINT 'Ticket for an AUDITED orders is being modified - please investigate why!'
				ELSE 				
					SET @errorString = @errorString + '|Tickets of Audited Orders cannot be modified'
			END

			-- prevent reassigning a ticket from one order to another
			IF EXISTS (
				SELECT i.* 
				FROM inserted i
				JOIN deleted d ON i.UID = d.UID
				WHERE i.OrderID <> d.OrderID)
			BEGIN
				SET @errorString = @errorString + '|Tickets cannot be moved to a different Order'
			END
			
			IF EXISTS (
				SELECT OT.OrderID
				FROM tblOrderTicket OT
				JOIN tblOrder O ON O.ID = OT.OrderID
				JOIN inserted i ON i.OrderID = OT.OrderID AND i.CarrierTicketNum = OT.CarrierTicketNum AND i.ID <> OT.ID
				WHERE OT.DeleteDateUTC IS NULL and I.DeleteDateUTC IS NULL
				GROUP BY OT.OrderID, OT.CarrierTicketNum
				HAVING COUNT(*) > 1
			)
			BEGIN
				SET @errorString = @errorString + '|Duplicate Ticket Numbers are not allowed'
			END
			
			-- store all the tickets for orders that are not in Generated status and not deleted (only these need validation)
			SELECT i.*, O.StatusID 
			INTO #i
			FROM inserted i
			JOIN tblOrder O ON O.ID = i.OrderID
			WHERE i.DeleteDateUTC IS NULL
			
			/* -- removed with version 3.7.5 
			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (3) AND BOLNum IS NULL)
			BEGIN
				SET @errorString = @errorString + '|BOL # value is required for Meter Run tickets'
			END
			*/
			
			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (1, 2) AND OriginTankID IS NULL AND TankNum IS NULL AND Rejected = 0)
			BEGIN
				SET @errorString = @errorString + '|Tank is required for Gauge Run & Net Volume tickets'
			END

			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (1) AND CarrierTicketNum IS NULL)
			BEGIN
				SET @errorString = @errorString + '|Ticket # value is required for Gauge Run tickets'
			END

			/* require all parameters for orders NOT IN (GAUGER, GENERATED, ASSIGNED, DISPATCHED) */
			IF  EXISTS (SELECT  ID FROM #i WHERE  Rejected =  0
					AND  ((TicketTypeID IN  (1, 2)  AND  (ProductObsTemp IS  NULL  OR  ProductObsGravity IS  NULL  OR  ProductBSW IS  NULL))
						OR  (TicketTypeID IN  (1)  AND  StatusID NOT  IN  (-9, -10, 1, 2)  AND  (ProductHighTemp IS  NULL  OR  ProductLowTemp IS  NULL))
					)
				)
			BEGIN
				SET @errorString = @errorString + '|All Product Measurement values are required for Gauge Run tickets'
			END
			
			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (1) AND Rejected = 0 
				AND (OpeningGaugeFeet IS NULL OR OpeningGaugeInch IS NULL OR OpeningGaugeQ IS NULL))
			BEGIN
				SET @errorString = @errorString + '|All Opening Gauge values are required for Gauge Run tickets'
			END
			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (1) AND Rejected = 0 AND StatusID NOT IN (-9, -10, 1, 2)
				AND (ClosingGaugeFeet IS NULL OR ClosingGaugeInch IS NULL OR ClosingGaugeQ IS NULL))
			BEGIN
				SET @errorString = @errorString + '|All Closing Gauge values are required for Gauge Run tickets'
			END

			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (2) AND Rejected = 0 AND StatusID NOT IN (-9, -10, 1, 2) AND (GrossUnits IS NULL))
			BEGIN
				SET @errorString = @errorString + '|Gross Volume value is required for Net Volume tickets'
			END

			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (7) AND Rejected = 0 AND StatusID NOT IN (-9, -10, 1, 2) AND (GrossUnits IS NULL))
			BEGIN
				SET @errorString = @errorString + '|Gross Volume value is required for Gauge Net tickets'
			END

			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (3) AND Rejected = 0 AND StatusID NOT IN (-9, -10, 1, 2) AND (GrossUnits IS NULL OR NetUnits IS NULL))
			BEGIN
				SET @errorString = @errorString + '|Gross & Net Volume values are required for Meter Run tickets'
			END

			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (1) AND Rejected = 0 AND StatusID NOT IN (-9, -10, 1, 2) AND (SealOff IS NULL OR SealOn IS NULL))
			BEGIN
				SET @errorString = @errorString + '|All Seal Off & Seal On values are required for Gauge Run tickets'
			END
			
			-- 3.9.38 - 2015/12/21 - BB - Add WeightGrossUnits check
			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (9) AND Rejected = 0 AND StatusID NOT IN (-9, -10, 1, 2) AND (WeightGrossUnits IS NULL OR WeightNetUnits IS NULL))
			BEGIN
				SET @errorString = @errorString + '|Gross & Net Weight values are required for Mineral Run tickets.'
			END
			
			-- 3.9.38 - 2015/12/21 - BB - Add WeightTareUnits check
			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (9) AND Rejected = 0 AND StatusID NOT IN (-9, -10, 1, 2) AND (WeightTareUnits IS NULL))
			BEGIN
				SET @errorString = @errorString + '|Tare Weight is required for Mineral Run tickets.'
			END

			-- if any errors were detected, cancel the entire operation (transaction) and return a LINEFEED-deliminated list of errors
			IF (len(@errorString) > 0)
			BEGIN
				SET @errorString = replace(substring(@errorString, 2, 1000), '|', char(13) + char(10))
				RAISERROR(@errorString, 16, 1)
				IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
				RETURN
			END
			/**********  END OF VALIDATION SECTION ************************/
			
			-- re-compute GaugeRun ticket Gross barrels
			IF UPDATE(OriginTankID) OR UPDATE(OpeningGaugeFeet) OR UPDATE(OpeningGaugeInch) OR UPDATE(OpeningGaugeQ)
				OR UPDATE (ClosingGaugeFeet) OR UPDATE(ClosingGaugeInch) OR UPDATE(ClosingGaugeQ) 
				OR UPDATE (GrossUnits)
			BEGIN
				UPDATE tblOrderTicket
				  SET GrossUnits = dbo.fnConvertUom(dbo.fnOriginTankStrappingLevelDeltaBarrels(
						OT.OriginTankID
					  , OpeningGaugeFeet
					  , OpeningGaugeInch
					  , OpeningGaugeQ
					  , ClosingGaugeFeet
					  , ClosingGaugeInch
					  , ClosingGaugeQ), 1, O.OriginUomID) -- ensure this value is always normalized from BARRELS
				FROM tblOrderTicket OT
				JOIN tblOrder O ON O.ID = OT.OrderID
				WHERE OT.ID IN (SELECT ID FROM inserted) 
				  AND OT.TicketTypeID = 1 -- Gauge Run Ticket
				  AND OT.OpeningGaugeFeet IS NOT NULL
				  AND OT.OpeningGaugeInch IS NOT NULL
				  AND OT.OpeningGaugeQ IS NOT NULL
				  AND OT.ClosingGaugeFeet IS NOT NULL
				  AND OT.ClosingGaugeInch IS NOT NULL
				  AND OT.ClosingGaugeQ IS NOT NULL
			END
			-- recompute the GrossUnits of any changed Meter Run tickets
			IF UPDATE(OpenMeterUnits) OR UPDATE(CloseMeterUnits) BEGIN
				--Print 'updating tblOrderTicket GOV from Open/Close Meter Units'
				UPDATE tblOrderTicket 
					SET GrossUnits = CloseMeterUnits - OpenMeterUnits
				WHERE TicketTypeID = 3  -- meter run tickets ONLY
				  AND OpenMeterUnits IS NOT NULL AND CloseMeterUnits IS NOT NULL
				
			END
			-- re-compute GaugeRun | Net Barrel | Meter Run tickets NetUnits
			IF UPDATE(OpeningGaugeFeet) OR UPDATE(OpeningGaugeInch) OR UPDATE(OpeningGaugeQ)
				OR UPDATE (ClosingGaugeFeet) OR UPDATE(ClosingGaugeInch) OR UPDATE(ClosingGaugeQ) 
				OR UPDATE(OriginTankID) OR UPDATE(GrossUnits) OR UPDATE(GrossStdUnits) OR UPDATE(NetUnits)
				OR UPDATE(ProductObsTemp) OR UPDATE(ProductObsGravity) OR UPDATE(ProductBSW)
				OR UPDATE(OpenMeterUnits) OR UPDATE(CloseMeterUnits)
				OR UPDATE(ProductHighTemp) OR UPDATE(ProductLowTemp)
			BEGIN
				--PRINT 'updating tblOrderTicket NSV/GSV'
				UPDATE tblOrderTicket
				  SET NetUnits = dbo.fnCrudeNetCalculator3T(GrossUnits, ProductObsTemp, ProductHighTemp, ProductLowTemp, ProductObsGravity, isnull(ProductBSW, 0))
					, GrossStdUnits = dbo.fnCrudeNetCalculator3T(GrossUnits, ProductObsTemp, ProductHighTemp, ProductLowTemp, ProductObsGravity, 0)
				WHERE ID IN (SELECT ID FROM inserted)
				  AND TicketTypeID IN (1, 2, 7) -- GAUGE RUN, NET VOLUME, GAUGE NET
				  AND GrossUnits IS NOT NULL
				  AND ProductHighTemp IS NOT NULL
				  AND ProductLowTemp IS NOT NULL
				  AND ProductObsTemp IS NOT NULL
				  AND ProductObsGravity IS NOT NULL
			END
			
			-- 3.9.38 - 2015/12/21 - BB - recompute the Net Weight of any changed Mineral Run tickets (DCWEB-972)
			IF UPDATE(WeightGrossUnits) OR UPDATE(WeightTareUnits) BEGIN
				--Print 'updating tblOrderTicket WeightNetUnits from WeightGrossUnits and WeightTareUnits
				UPDATE tblOrderTicket
					SET WeightNetUnits = WeightGrossUnits - WeightTareUnits
				WHERE TicketTypeID = 9 -- Mineral Run tickets only
					AND WeightGrossUnits IS NOT NULL AND  WeightTareUnits IS NOT NULL
			END 			
			
			-- ensure the Order record is in-sync with the Tickets
			/* test lines below
			declare @gov decimal(9,3); SELECT TOP 1 @gov = OT.GrossUnits FROM tblOrderTicket OT JOIN inserted i ON i.ID = OT.ID;
			PRINT 'updating tblOrder GOV/GSV/NSV GOV = ' + ltrim(@gov)
			--*/
			UPDATE tblOrder 
			SET OriginGrossUnits = (SELECT sum(GrossUnits) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND OT.Rejected = 0 AND OT.DeleteDateUTC IS NULL)
			  , OriginNetUnits = (SELECT sum(NetUnits) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND OT.Rejected = 0 AND OT.DeleteDateUTC IS NULL)
			  , OriginGrossStdUnits = (SELECT sum(GrossStdUnits) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND OT.Rejected = 0 AND OT.DeleteDateUTC IS NULL)
			    --3.9.38 - 2015/12/21 - BB - Update weight fields (DCWEB-972)  -- Per Maverick 12/21/15 we do not need gross on the order level.
			  --, OriginWeightGrossUnits = (SELECT sum(WeightGrossUnits) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND OT.Rejected = 0 AND OT.DeleteDateUTC IS NULL)
			  , OriginWeightNetUnits = 	(SELECT sum(WeightNetUnits) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND OT.Rejected = 0 AND OT.DeleteDateUTC IS NULL)
				-- use the first MeterRun/BasicRun BOLNum as the Order BOL Num (favor the first un-Rejected)
			  , OriginBOLNum = (
					SELECT TOP 1 BOLNum FROM (
						SELECT TOP 1 BOLNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID NOT IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
						UNION SELECT TOP 1 BOLNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID NOT IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X)
				-- use the first GaugeRun|NetVolume CarrierTicketNum as the Order.CarrierTicketNum (favor first un-Rejected)
			  , CarrierTicketNum = (
					SELECT TOP 1 CarrierTicketNum FROM (
						SELECT TOP 1 CarrierTicketNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
						UNION SELECT TOP 1 CarrierTicketNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X)
				-- use the first GaugeRun|NetVolume OriginTankID for the Order (favor first un-Rejected ticket)
				-- BB included ticket additional ticket type (Gauge Net-7)
			  , OriginTankID = (
					SELECT TOP 1 OriginTankID FROM (
						SELECT TOP 1 OriginTankID FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2,7) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
						UNION SELECT TOP 1 OriginTankID FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2,7) AND DeleteDateUTC IS NULL ORDER BY ID) X)
				-- use the first GaugeRun|NetVolume OriginTankNum for the Order (favor first un-Rejected ticket)
			  , OriginTankNum = (
					SELECT TOP 1 OriginTankNum FROM (
						SELECT TOP 1 OriginTankNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
						UNION SELECT TOP 1 OriginTankNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X)
			  , PickupLastChangeDateUTC = GETUTCDATE() -- 3.10.5.2 info tied to the pickup has been changed
			  , LastChangeDateUTC = GETUTCDATE() -- 3.8.10 update
			  , LastChangedByUser = (SELECT MIN(isnull(LastChangedByUser, CreatedByUser)) FROM inserted WHERE OrderID = O.ID)
			FROM tblOrder O
			WHERE ID IN (SELECT DISTINCT OrderID FROM inserted)

			/* START DB AUDIT *********************************************************/
			BEGIN TRY
				IF EXISTS(SELECT * FROM tblSetting WHERE ID = 32 AND (Value LIKE 'true' OR Value LIKE 'yes'))
					INSERT INTO tblOrderTicketDbAudit (DBAuditDate, ID, OrderID, CarrierTicketNum, TicketTypeID, TankNum, ProductObsGravity
													, ProductObsTemp, OpeningGaugeFeet, OpeningGaugeInch, OpeningGaugeQ, ClosingGaugeFeet, ClosingGaugeInch
													, ClosingGaugeQ, GrossUnits, NetUnits, Rejected, RejectNotes, SealOff, SealOn, BOLNum, ProductHighTemp
													, ProductLowTemp, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC
													, DeletedByUser, UID, FromMobileApp, OriginTankID, GrossStdUnits, BottomFeet, BottomInches
													, BottomQ, RejectReasonID, MeterFactor, OpenMeterUnits, CloseMeterUnits, ProductBSW, WeightTareUnits
													, WeightGrossUnits, WeightNetUnits)
						SELECT GETUTCDATE(), ID, OrderID, CarrierTicketNum, TicketTypeID, TankNum, ProductObsGravity, ProductObsTemp, OpeningGaugeFeet
							, OpeningGaugeInch, OpeningGaugeQ, ClosingGaugeFeet, ClosingGaugeInch, ClosingGaugeQ, GrossUnits, NetUnits, Rejected, RejectNotes
							, SealOff, SealOn, BOLNum, ProductHighTemp, ProductLowTemp, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser
							, DeleteDateUTC, DeletedByUser, UID, FromMobileApp, OriginTankID, GrossStdUnits, BottomFeet, BottomInches, BottomQ, RejectReasonID
							, MeterFactor, OpenMeterUnits, CloseMeterUnits, ProductBSW, WeightTareUnits, WeightGrossUnits, WeightNetUnits
						FROM deleted d
			END TRY
			BEGIN CATCH
				PRINT 'trigOrderTicket_IU.DBAUDIT FAILURE: ' + ERROR_MESSAGE()
			END CATCH
			/* END DB AUDIT *********************************************************/

			PRINT 'trigOrderTicket_IU COMPLETE'
		END
	END	
END

GO

EXEC sp_settriggerorder @triggername=N'[dbo].[trigOrderTicket_IU]', @order=N'First', @stmttype=N'INSERT'
GO

EXEC sp_settriggerorder @triggername=N'[dbo].[trigOrderTicket_IU]', @order=N'First', @stmttype=N'UPDATE'
GO

/*********************************************
Create date:	3 Oct 2013
Author:			Kevin Alons
Description:	trigger to ensure the Order totals for all related tickets are computed for remaining undeleted tickets
Changes:
  - 3.10.5.2 - 2016/02/10 - JAE+KDA - ensure the Order.PickupLastChangeDateUTC is updated for any ticket deletion
*********************************************/
ALTER TRIGGER trigOrderTicket_D ON tblOrderTicket AFTER DELETE AS
BEGIN
	SET NOCOUNT ON;

	-- ensure the Order record is in-sync with the Tickets
	UPDATE tblOrder 
	SET OriginGrossUnits = (SELECT sum(GrossUnits) FROM tblOrderTicket WHERE OrderID = O.ID AND Rejected = 0 AND DeleteDateUTC IS NULL)
	  , OriginNetUnits = (SELECT sum(NetUnits) FROM tblOrderTicket WHERE OrderID = O.ID AND Rejected = 0 AND DeleteDateUTC IS NULL)
		-- use the first MeterRun/BasicRun CarrierTicketNum num as the Order BOL Num
	  , OriginBOLNum = (SELECT min(BOLNum) FROM tblOrderTicket WHERE OrderID = O.ID AND TicketTypeID NOT IN (1,2) AND DeleteDateUTC IS NULL)
	  , CarrierTicketNum = (SELECT min(CarrierTicketNum) FROM tblOrderTicket WHERE ID IN 
		(SELECT min(ID) FROM tblOrderTicket WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL))
	  , PickupLastChangeDateUTC = (SELECT MAX(isnull(LastChangeDateUTC, CreateDateUTC)) FROM deleted WHERE OrderID = O.ID)
	  , LastChangeDateUTC = (SELECT MAX(isnull(LastChangeDateUTC, CreateDateUTC)) FROM deleted WHERE OrderID = O.ID)
	  , LastChangedByUser = (SELECT MIN(isnull(LastChangedByUser, CreatedByUser)) FROM deleted WHERE OrderID = O.ID)
	FROM tblOrder O
	WHERE ID IN (SELECT DISTINCT OrderID FROM deleted)
END
GO


COMMIT
SET NOEXEC OFF