-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.4.7'
SELECT  @NewVersion = '3.4.8'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'fix bug in Destination Products|Shippers in revisions for Popup Editing'
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fnDestinationProductIDCSV]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION fnDestinationProductIDCSV
GO
-- =============================================
-- Author:		Kevin Alons
-- Create date: 3 Oct 2014
-- Description:	return Products [CSV] field for the specified Destination
-- =============================================
CREATE FUNCTION [dbo].[fnDestinationProductIDCSV](@destinationID int) RETURNS varchar(1000) AS BEGIN
	DECLARE @ret varchar(max)
	SELECT @ret = 
	  STUFF(
		(
		  SELECT ',' + ltrim(ProductID)
		  FROM tblDestinationProducts
		  WHERE DestinationID = @destinationID
		  FOR XML PATH(''),TYPE
		  ).value('.','VARCHAR(MAX)'
		), 1, 1, '')

	RETURN (@ret)
END

GO
GRANT EXECUTE ON fnDestinationProductIDCSV TO dispatchcrude_iis_acct
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fnDestinationCustomerIDCSV]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION fnDestinationCustomerIDCSV
GO
-- =============================================
-- Author:		Kevin Alons
-- Create date: 3 Oct 2014
-- Description:	return Customers [CSV] field for the specified Destination
-- =============================================
CREATE FUNCTION [dbo].[fnDestinationCustomerIDCSV](@destinationID int) RETURNS varchar(1000) AS BEGIN
	DECLARE @ret varchar(max)
	SELECT @ret = 
	  STUFF(
		(
		  SELECT ',' + ltrim(CustomerID)
		  FROM tblDestinationCustomers
		  WHERE DestinationID = @destinationID
		  FOR XML PATH(''),TYPE
		  ).value('.','VARCHAR(MAX)'
		), 1, 1, '')

	RETURN (@ret)
END

GO
GRANT EXECUTE ON fnDestinationCustomerIDCSV TO dispatchcrude_iis_acct
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fnDestinationProductsCSV]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION fnDestinationProductsCSV
GO
-- =============================================
-- Author:		Kevin Alons
-- Create date: 3 Oct 2014
-- Description:	return Products [CSV] field for the specified Destination
-- =============================================
CREATE FUNCTION [dbo].[fnDestinationProductsCSV](@originID int) RETURNS varchar(1000) AS BEGIN
	DECLARE @ret varchar(max)
	SELECT @ret = 
	  STUFF(
		(
		  SELECT ', ' + ShortName
		  FROM tblProduct P
		  WHERE ID IN (SELECT ProductID FROM tblDestinationProducts OP WHERE OP.DestinationID = @originID)
		  ORDER BY ShortName
		  FOR XML PATH(''),TYPE
		  ).value('.','VARCHAR(MAX)'
		), 1, 1, '')

	RETURN (@ret)
END

GO
GRANT EXECUTE ON fnDestinationProductsCSV TO dispatchcrude_iis_acct
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fnDestinationCustomersCSV]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION fnDestinationCustomersCSV
GO
-- =============================================
-- Author:		Kevin Alons
-- Create date: 3 Oct 2014
-- Description:	return Customers [CSV] field for the specified Destination
-- =============================================
CREATE FUNCTION [dbo].[fnDestinationCustomersCSV](@originID int) RETURNS varchar(1000) AS BEGIN
	DECLARE @ret varchar(max)
	SELECT @ret = 
	  STUFF(
		(
		  SELECT ', ' + Name
		  FROM tblCustomer P
		  WHERE ID IN (SELECT CustomerID FROM tblDestinationCustomers OP WHERE OP.DestinationID = @originID)
		  ORDER BY Name
		  FOR XML PATH(''),TYPE
		  ).value('.','VARCHAR(MAX)'
		), 1, 1, '')

	RETURN (@ret)
END

GO
GRANT EXECUTE ON fnDestinationCustomersCSV TO dispatchcrude_iis_acct
GO

COMMIT
SET NOEXEC OFF