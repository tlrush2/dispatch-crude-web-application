-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.5.9'
SELECT  @NewVersion = '3.5.10'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'Add Shipper criteria to all remaining Carrire Rates'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

ALTER TABLE tblCarrierDestinationWaitRate ADD ShipperID int NULL CONSTRAINT FK_CarrierDestinationWaitRate_Shipper FOREIGN KEY REFERENCES tblCustomer(ID)
GO
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tblCarrierDestinationWaitRate]') AND name = N'udxCarrierDestinationWaitRate_Main')
	DROP INDEX [udxCarrierDestinationWaitRate_Main] ON [dbo].[tblCarrierDestinationWaitRate] WITH ( ONLINE = OFF )
GO
CREATE UNIQUE NONCLUSTERED INDEX [udxCarrierDestinationWaitRate_Main] ON [dbo].[tblCarrierDestinationWaitRate] 
(
	[ReasonID] ASC,
	ShipperID ASC,
	[CarrierID] ASC,
	[ProductGroupID] ASC,
	[DestinationID] ASC,
	[StateID] ASC,
	[RegionID] ASC,
	[EffectiveDate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all CarrierDestinationWaitRate records
******************************************************/
ALTER VIEW viewCarrierDestinationWaitRate AS
	SELECT X.*
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementCarrier WHERE DestinationWaitRateID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate)
			FROM tblCarrierDestinationWaitRate XN 
			WHERE isnull(XN.ShipperID, 0) = isnull(X.ShipperID, 0) 
			  AND isnull(XN.CarrierID, 0) = isnull(X.CarrierID, 0) 
			  AND isnull(XN.ProductGroupID, 0) = isnull(X.ProductGroupID, 0) 
			  AND isnull(XN.DestinationID, 0) = isnull(X.DestinationID, 0) 
			  AND isnull(XN.StateID, 0) = isnull(X.StateID, 0) 
			  AND isnull(XN.RegionID, 0) = isnull(X.RegionID, 0) 
			  AND XN.EffectiveDate > X.EffectiveDate)
	FROM tblCarrierDestinationWaitRate X
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier DestinationWaitRate info for the specified criteria
/***********************************/
ALTER FUNCTION [dbo].[fnCarrierDestinationWaitRate]
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @DestinationID int
, @StateID int
, @RegionID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID, ReasonID
			, Ranking =	dbo.fnRateRanking(@ReasonID, R.ReasonID, 64, 1)
				  + dbo.fnRateRanking(@ShipperID, R.ShipperID, 32, 0)
				  + dbo.fnRateRanking(@CarrierID, R.CarrierID, 16, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 8, 0)
				  + dbo.fnRateRanking(@DestinationID, R.DestinationID, 4, 0)
				  + dbo.fnRateRanking(@StateID, R.StateID, 2, 0)
				  + dbo.fnRateRanking(@RegionID, R.RegionID, 1, 0)
		FROM  dbo.viewCarrierDestinationWaitRate R
		WHERE coalesce(nullif(@ReasonID, 0), R.ReasonID, 0) = coalesce(ReasonID, nullif(@ReasonID, 0), 0)
		  AND coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@DestinationID, 0), R.DestinationID, 0) = coalesce(DestinationID, nullif(@DestinationID, 0), 0)
		  AND coalesce(nullif(@StateID, 0), R.StateID, 0) = coalesce(StateID, nullif(@StateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	),
	cte2 AS
	(
		SELECT ID, S.ReasonID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT ReasonID = ISNULL(ReasonID, 0), Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 7  -- ensure some type of match occurred on all 7 criteria choices
			GROUP BY ReasonID
		) X ON X.ReasonID = isnull(S.ReasonID, 0) AND X.Ranking = S.Ranking
	), 
	cte3 AS
	(	-- when both ReasonID = NULL AND ReasonID = @ReasonID records are BestMatch = 1, favor the ReasonID = @ReasonID record
		SELECT ID, X.ReasonID, X.Ranking, BestMatch = CASE WHEN BEST.ReasonID IS NULL THEN X.BestMatch ELSE 0 END
		FROM cte2 X
		LEFT JOIN (
			SELECT TOP 1 ReasonID FROM cte2 WHERE ReasonID = @ReasonID AND BestMatch = 1 ORDER BY Ranking DESC
		) BEST ON X.ReasonID IS NULL AND X.BestMatch = 1
	)
	
	SELECT TOP (CASE WHEN @BestMatchOnly = 1 THEN 1 ELSE 10000 END) R.ID, R.ReasonID, ShipperID, CarrierID, ProductGroupID, DestinationID, StateID, RegionID, Rate, EffectiveDate, EndDate, NextEffectiveDate
	  , BestMatch, Ranking
	  , Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewCarrierDestinationWaitRate R
	JOIN cte3 X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
	ORDER BY Ranking DESC
)
GO

/***********************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier DestinationWaitRate rows for the specified criteria
/***********************************/
ALTER FUNCTION [dbo].[fnCarrierDestinationWaitRatesDisplay]
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @DestinationID int
, @StateID int
, @RegionID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ReasonID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.DestinationID, R.StateID, R.RegionID, R.Rate, R.EffectiveDate, R.EndDate, R.NextEffectiveDate
		, Shipper = SH.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, Destination = D.Name
		, DestinationFull = D.FullName
		, State = S.FullName
		, StateAbbrev = S.Abbreviation
		, Region = REG.Name
		, Reason = REA.Description
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
		, Ranking
	FROM dbo.fnCarrierDestinationWaitRate(@StartDate, @EndDate, @ReasonID, @ShipperID, @CarrierID, @ProductGroupID, @DestinationID, @StateID, @RegionID, 0) R
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN viewDestination D ON D.ID = R.DestinationID
	LEFT JOIN tblState S ON S.ID = R.StateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblDestinationWaitReason REA ON REA.ID = R.ReasonID
	ORDER BY EffectiveDate
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier DestinationWaitRate info for the specified order
/***********************************/
ALTER FUNCTION [dbo].[fnOrderCarrierDestinationWaitRate](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, Minutes = O.DestMinutes, R.Rate
	FROM viewOrder O
	CROSS APPLY dbo.fnCarrierDestinationWaitRate(O.OrderDate, null, O.DestWaitReasonID, O.CustomerID, O.CarrierID, O.ProductGroupID, O.OriginID, O.DestStateID, O.DestRegionID, 1) R
	WHERE O.ID = @ID
GO

ALTER TABLE tblCarrierOriginWaitRate ADD ShipperID int NULL CONSTRAINT FK_CarrierOriginWaitRate_Shipper FOREIGN KEY REFERENCES tblCustomer(ID)
GO
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tblCarrierOriginWaitRate]') AND name = N'udxCarrierOriginWaitRate_Main')
	DROP INDEX [udxCarrierOriginWaitRate_Main] ON [dbo].[tblCarrierOriginWaitRate] WITH ( ONLINE = OFF )
GO
CREATE UNIQUE NONCLUSTERED INDEX [udxCarrierOriginWaitRate_Main] ON [dbo].[tblCarrierOriginWaitRate] 
(
	[ReasonID] ASC,
	ShipperID ASC,
	[CarrierID] ASC,
	[ProductGroupID] ASC,
	[OriginID] ASC,
	[StateID] ASC,
	[RegionID] ASC,
	[EffectiveDate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all CarrierOriginWaitRate records
******************************************************/
ALTER VIEW viewCarrierOriginWaitRate AS
	SELECT X.*
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementCarrier WHERE OriginWaitRateID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate)
			FROM tblCarrierOriginWaitRate XN 
			WHERE isnull(XN.ShipperID, 0) = isnull(X.ShipperID, 0) 
			  AND isnull(XN.CarrierID, 0) = isnull(X.CarrierID, 0) 
			  AND isnull(XN.ProductGroupID, 0) = isnull(X.ProductGroupID, 0) 
			  AND isnull(XN.OriginID, 0) = isnull(X.OriginID, 0) 
			  AND isnull(XN.StateID, 0) = isnull(X.StateID, 0) 
			  AND isnull(XN.RegionID, 0) = isnull(X.RegionID, 0) 
			  AND XN.EffectiveDate > X.EffectiveDate)
	FROM tblCarrierOriginWaitRate X
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OriginWaitRate info for the specified criteria
/***********************************/
ALTER FUNCTION [dbo].[fnCarrierOriginWaitRate]
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @OriginID int
, @StateID int
, @RegionID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID, ReasonID
			, Ranking =	dbo.fnRateRanking(@ReasonID, R.ReasonID, 64, 1)
				  + dbo.fnRateRanking(@ShipperID, R.ShipperID, 32, 0)
				  + dbo.fnRateRanking(@CarrierID, R.CarrierID, 16, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 8, 0)
				  + dbo.fnRateRanking(@OriginID, R.OriginID, 4, 0)
				  + dbo.fnRateRanking(@StateID, R.StateID, 2, 0)
				  + dbo.fnRateRanking(@RegionID, R.RegionID, 1, 0)
		FROM  dbo.viewCarrierOriginWaitRate R
		WHERE coalesce(nullif(@ReasonID, 0), R.ReasonID, 0) = coalesce(ReasonID, nullif(@ReasonID, 0), 0)
		  AND coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@OriginID, 0), R.OriginID, 0) = coalesce(OriginID, nullif(@OriginID, 0), 0)
		  AND coalesce(nullif(@StateID, 0), R.StateID, 0) = coalesce(StateID, nullif(@StateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	),
	cte2 AS
	(
		SELECT ID, S.ReasonID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT ReasonID = ISNULL(ReasonID, 0), Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 7  -- ensure some type of match occurred on all 7 criteria choices
			GROUP BY ReasonID
		) X ON X.ReasonID = isnull(S.ReasonID, 0) AND X.Ranking = S.Ranking
	), 
	cte3 AS
	(	-- when both ReasonID = NULL AND ReasonID = @ReasonID records are BestMatch = 1, favor the ReasonID = @ReasonID record
		SELECT ID, X.ReasonID, X.Ranking, BestMatch = CASE WHEN BEST.ReasonID IS NULL THEN X.BestMatch ELSE 0 END
		FROM cte2 X
		LEFT JOIN (
			SELECT TOP 1 ReasonID FROM cte2 WHERE ReasonID = @ReasonID AND BestMatch = 1 ORDER BY Ranking DESC
		) BEST ON X.ReasonID IS NULL AND X.BestMatch = 1
	)
	
	SELECT TOP (CASE WHEN @BestMatchOnly = 1 THEN 1 ELSE 10000 END) R.ID, R.ReasonID, ShipperID, CarrierID, ProductGroupID, OriginID, StateID, RegionID, Rate, EffectiveDate, EndDate, NextEffectiveDate
	  , BestMatch, Ranking
	  , Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewCarrierOriginWaitRate R
	JOIN cte3 X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
	ORDER BY Ranking DESC
)
GO

/***********************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OriginWaitRate rows for the specified criteria
/***********************************/
ALTER FUNCTION [dbo].[fnCarrierOriginWaitRatesDisplay]
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @OriginID int
, @StateID int
, @RegionID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ReasonID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.OriginID, R.StateID, R.RegionID, R.Rate, R.EffectiveDate, R.EndDate, R.NextEffectiveDate
		, Shipper = SH.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, Origin = D.Name
		, OriginFull = D.FullName
		, State = S.FullName
		, StateAbbrev = S.Abbreviation
		, Region = REG.Name
		, Reason = REA.Description
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
		, Ranking
	FROM dbo.fnCarrierOriginWaitRate(@StartDate, @EndDate, @ReasonID, @ShipperID, @CarrierID, @ProductGroupID, @OriginID, @StateID, @RegionID, 0) R
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN viewOrigin D ON D.ID = R.OriginID
	LEFT JOIN tblState S ON S.ID = R.StateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblOriginWaitReason REA ON REA.ID = R.ReasonID
	ORDER BY EffectiveDate
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OriginWaitRate info for the specified order
/***********************************/
ALTER FUNCTION [dbo].[fnOrderCarrierOriginWaitRate](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, Minutes = O.DestMinutes, R.Rate
	FROM viewOrder O
	CROSS APPLY dbo.fnCarrierOriginWaitRate(O.OrderDate, null, O.DestWaitReasonID, O.CustomerID, O.CarrierID, O.ProductGroupID, O.OriginID, O.DestStateID, O.DestRegionID, 1) R
	WHERE O.ID = @ID
GO


ALTER TABLE tblCarrierOrderRejectRate ADD ShipperID int NULL CONSTRAINT FK_CarrierOrderRejectRate_Shipper FOREIGN KEY REFERENCES tblCustomer(ID)
GO
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tblCarrierOrderRejectRate]') AND name = N'udxCarrierOrderRejectRate_Main')
	DROP INDEX [udxCarrierOrderRejectRate_Main] ON [dbo].[tblCarrierOrderRejectRate] WITH ( ONLINE = OFF )
GO
CREATE UNIQUE NONCLUSTERED INDEX [udxCarrierOrderRejectRate_Main] ON [dbo].[tblCarrierOrderRejectRate] 
(
	[ReasonID] ASC,
	ShipperID ASC,
	[CarrierID] ASC,
	[ProductGroupID] ASC,
	[OriginID] ASC,
	[StateID] ASC,
	[RegionID] ASC,
	[EffectiveDate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all CarrierOrderRejectRate records
******************************************************/
ALTER VIEW [dbo].[viewCarrierOrderRejectRate] AS
	SELECT X.*
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementCarrier WHERE OrderRejectRateID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblCarrierOrderRejectRate XN 
			WHERE isnull(XN.ShipperID, 0) = isnull(X.ShipperID, 0) 
			  AND isnull(XN.CarrierID, 0) = isnull(X.CarrierID, 0) 
			  AND isnull(XN.ProductGroupID, 0) = isnull(X.ProductGroupID, 0) 
			  AND isnull(XN.OriginID, 0) = isnull(X.OriginID, 0) 
			  AND isnull(XN.StateID, 0) = isnull(X.StateID, 0) 
			  AND isnull(XN.RegionID, 0) = isnull(X.RegionID, 0) 
			  AND XN.EffectiveDate > X.EffectiveDate)
	FROM tblCarrierOrderRejectRate X
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OrderRejectRate info for the specified criteria
/***********************************/
ALTER FUNCTION [dbo].[fnCarrierOrderRejectRate]
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @OriginID int
, @StateID int
, @RegionID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID, ReasonID
			, Ranking =	dbo.fnRateRanking(@ReasonID, R.ReasonID, 64, 1)
				  + dbo.fnRateRanking(@ShipperID, R.ShipperID, 32, 0)
				  + dbo.fnRateRanking(@CarrierID, R.CarrierID, 16, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 8, 0)
				  + dbo.fnRateRanking(@OriginID, R.OriginID, 4, 0)
				  + dbo.fnRateRanking(@StateID, R.StateID, 2, 0)
				  + dbo.fnRateRanking(@RegionID, R.RegionID, 1, 0)
		FROM  dbo.viewCarrierOrderRejectRate R
		WHERE coalesce(nullif(@ReasonID, 0), R.ReasonID, 0) = coalesce(ReasonID, nullif(@ReasonID, 0), 0)
		  AND coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@OriginID, 0), R.OriginID, 0) = coalesce(OriginID, nullif(@OriginID, 0), 0)
		  AND coalesce(nullif(@StateID, 0), R.StateID, 0) = coalesce(StateID, nullif(@StateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	),
	cte2 AS
	(
		SELECT ID, S.ReasonID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT ReasonID = ISNULL(ReasonID, 0), Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 7  -- ensure some type of match occurred on all 6 criteria choices
			GROUP BY ReasonID
		) X ON X.ReasonID = isnull(S.ReasonID, 0) AND X.Ranking = S.Ranking
	), 
	cte3 AS
	(	-- when both ReasonID = NULL AND ReasonID = @ReasonID records are BestMatch = 1, favor the ReasonID = @ReasonID record
		SELECT ID, X.ReasonID, X.Ranking, BestMatch = CASE WHEN BEST.ReasonID IS NULL THEN X.BestMatch ELSE 0 END
		FROM cte2 X
		LEFT JOIN (
			SELECT TOP 1 ReasonID FROM cte2 WHERE ReasonID = @ReasonID AND BestMatch = 1 ORDER BY Ranking DESC
		) BEST ON X.ReasonID IS NULL AND X.BestMatch = 1
	)
	
	SELECT R.ID, R.ReasonID, ShipperID, CarrierID, ProductGroupID, OriginID, StateID, RegionID, Rate, RateTypeID, UomID, EffectiveDate, EndDate, NextEffectiveDate
	  , BestMatch, Ranking
	  , Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewCarrierOrderRejectRate R
	JOIN cte3 X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)
GO

/***********************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OrderRejectRate rows for the specified criteria
/***********************************/
ALTER FUNCTION [dbo].[fnCarrierOrderRejectRatesDisplay]
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @OriginID int
, @StateID int
, @RegionID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ReasonID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.OriginID, R.StateID, R.RegionID, R.Rate, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate, R.NextEffectiveDate
		, Shipper = SH.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, Origin = D.Name
		, OriginFull = D.FullName
		, State = S.FullName
		, StateAbbrev = S.Abbreviation
		, Region = REG.Name
		, Reason = REA.Description
		, RateType = RT.Name
		, Uom = U.Name
		, UomShort = U.Abbrev
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
	FROM dbo.fnCarrierOrderRejectRate(@StartDate, @EndDate, @ReasonID, @ShipperID, @CarrierID, @ProductGroupID, @OriginID, @StateID, @RegionID, 0) R
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN viewOrigin D ON D.ID = R.OriginID
	LEFT JOIN tblState S ON S.ID = R.StateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblOrderRejectReason REA ON REA.ID = R.ReasonID
	LEFT JOIN tblRateType RT ON RT.ID = R.RateTypeID
	LEFT JOIN tblUom U ON U.ID = R.UomID
	ORDER BY EffectiveDate
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OrderRejectRate info for the specified order
/***********************************/
ALTER FUNCTION [dbo].[fnOrderCarrierOrderRejectRate](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, Minutes = O.DestMinutes, R.Rate, R.RateTypeID, R.UomID
	FROM viewOrder O
	CROSS APPLY dbo.fnCarrierOrderRejectRate(O.OrderDate, null, O.DestWaitReasonID, O.CustomerID, O.CarrierID, O.ProductGroupID, O.OriginID, O.DestStateID, O.DestRegionID, 1) R
	WHERE O.ID = @ID
GO


ALTER TABLE tblCarrierRateSheet ADD ShipperID int NULL CONSTRAINT FK_CarrierRateSheetRate_Shipper FOREIGN KEY REFERENCES tblCustomer(ID)
GO
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tblCarrierRateSheet]') AND name = N'udxCarrierRateSheet_Main')
	DROP INDEX [udxCarrierRateSheet_Main] ON [dbo].[tblCarrierRateSheet] WITH ( ONLINE = OFF )
GO
CREATE UNIQUE CLUSTERED INDEX [udxCarrierRateSheet_Main] ON [dbo].[tblCarrierRateSheet] 
(
	ShipperID ASC,
	[CarrierID] ASC,
	[ProductGroupID] ASC,
	[RegionID] ASC,
	[OriginStateID] ASC,
	[DestStateID] ASC,
	[EffectiveDate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all CarrierRateSheet records
******************************************************/
ALTER VIEW [dbo].[viewCarrierRateSheet] AS
	SELECT X.*
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementCarrier SC JOIN tblCarrierRangeRate RR ON RR.ID = SC.RangeRateID WHERE RR.RateSheetID = X.ID AND SC.BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblCarrierRateSheet XN 
			WHERE isnull(XN.ShipperID, 0) = isnull(X.ShipperID, 0) 
			  AND isnull(XN.CarrierID, 0) = isnull(X.CarrierID, 0) 
			  AND isnull(XN.ProductGroupID, 0) = isnull(X.ProductGroupID, 0) 
			  AND isnull(XN.OriginStateID, 0) = isnull(X.OriginStateID, 0) 
			  AND isnull(XN.DestStateID, 0) = isnull(X.DestStateID, 0) 
			  AND isnull(XN.RegionID, 0) = isnull(X.RegionID, 0) 
			  AND XN.EffectiveDate > X.EffectiveDate
		)
	FROM tblCarrierRateSheet X
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RateSheetRangeRate info for the specified criteria
/***********************************/
ALTER FUNCTION [dbo].[fnCarrierRateSheetRangeRate](@StartDate date, @EndDate date, @RouteMiles int, @ShipperID int, @CarrierID int, @ProductGroupID int, @OriginStateID int, @DestStateID int, @RegionID int, @BestMatchOnly bit = 0)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT RateSheetID = R.ID, RangeRateID = RR.ID
			, Ranking = CASE WHEN @RouteMiles BETWEEN RR.MinRange AND RR.MaxRange THEN 64 ELSE 0 END
				+ dbo.fnRateRanking(@ShipperID, R.ShipperID, 32, 0)
				+ dbo.fnRateRanking(@CarrierID, R.CarrierID, 16, 0)
				+ dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 8, 0)
				+ dbo.fnRateRanking(@OriginStateID, R.OriginStateID, 4, 0)
				+ dbo.fnRateRanking(@DestStateID, R.DestStateID, 2, 0)
				+ dbo.fnRateRanking(@RegionID, R.RegionID, 1, 0)
		FROM dbo.viewCarrierRateSheet R
		JOIN dbo.tblCarrierRangeRate RR ON RR.RateSheetID = R.ID AND isnull(@RouteMiles, RR.MinRange) BETWEEN RR.MinRange AND RR.MaxRange
		WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@OriginStateID, 0), R.OriginStateID, 0) = coalesce(OriginStateID, nullif(@OriginStateID, 0), 0)
		  AND coalesce(nullif(@DestStateID, 0), R.DestStateID, 0) = coalesce(DestStateID, nullif(@DestStateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	)
	
	SELECT RR.ID, RateSheetID = R.ID, ShipperID, CarrierID, ProductGroupID, OriginStateID, DestStateID, RegionID, Rate, MinRange, MaxRange, RateTypeID, UomID, EffectiveDate, EndDate, NextEffectiveDate
	  , BestMatch, Ranking
	  , Locked, R.CreateDateUTC, R.CreatedByUser, R.LastChangeDateUTC, R.LastChangedByUser 
	FROM dbo.viewCarrierRateSheet R
	JOIN dbo.tblCarrierRangeRate RR ON RR.RateSheetID = R.ID
	JOIN (
		SELECT RateSheetID, RangeRateID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 7  -- ensure some type of match occurred on all 6 criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.RateSheetID = R.ID AND X.RangeRateID = RR.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RateSheetRangeRate info for the specified criteria
/***********************************/
ALTER FUNCTION [dbo].[fnCarrierRateSheetRangeRatesDisplay](@StartDate date, @EndDate date, @RouteMiles int, @ShipperID int, @CarrierID int, @ProductGroupID int, @OriginStateID int, @DestStateID int, @RegionID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.RateSheetID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.OriginStateID, R.DestStateID, R.RegionID, R.MinRange, R.MaxRange, R.Rate, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate, R.NextEffectiveDate
		, Shipper = SH.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, OriginState = OS.FullName
		, OriginStateAbbrev = OS.Abbreviation
		, DestState = OS.FullName
		, DestStateAbbrev = OS.Abbreviation
		, Region = REG.Name
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
		, Ranking
	FROM dbo.fnCarrierRateSheetRangeRate(@StartDate, @EndDate, @RouteMiles, @ShipperID, @CarrierID, @ProductGroupID, @OriginStateID, @DestStateID, @RegionID, 0) R
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblState OS ON OS.ID = R.OriginStateID
	LEFT JOIN tblState DS ON DS.ID = R.DestStateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	ORDER BY EffectiveDate
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RateSheetRangeRate info for the specified order
/***********************************/
ALTER FUNCTION [dbo].[fnOrderCarrierRateSheetRangeRate](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, RateTypeID, Rate, UomID
	FROM dbo.viewOrder O
	CROSS APPLY dbo.fnCarrierRateSheetRangeRate(O.OrderDate, null, O.ActualMiles, O.CustomerID, O.CarrierID, O.ProductGroupID, O.OriginStateID, O.DestStateID, O.OriginRegionID, 1) R
	WHERE O.ID = @ID
GO


ALTER TABLE tblCarrierRouteRate ADD ShipperID int NULL CONSTRAINT FK_CarrierRouteRate_Shipper FOREIGN KEY REFERENCES tblCustomer(ID)
GO
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tblCarrierRouteRate]') AND name = N'udxCarrierRateSheet_Main')
	DROP INDEX udxCarrierRateSheet_Main ON [dbo].[tblCarrierRouteRate] WITH ( ONLINE = OFF )
GO 
CREATE UNIQUE CLUSTERED INDEX [udxCarrierRouteRate_Main] ON [dbo].[tblCarrierRouteRate] 
(
	ShipperID ASC,
	[CarrierID] ASC,
	[ProductGroupID] ASC,
	[RouteID] ASC,
	[EffectiveDate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all CarrierRouteRate records
******************************************************/
ALTER VIEW [dbo].[viewCarrierRouteRate] AS
	SELECT X.*
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementCarrier WHERE RouteRateID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, R.OriginID
		, R.DestinationID
		, R.ActualMiles
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblCarrierRouteRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND XN.RouteID = X.RouteID
			  AND XN.EffectiveDate > X.EffectiveDate)
	FROM tblCarrierRouteRate X
	JOIN tblRoute R ON R.ID = X.RouteID
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RouteRate info for the specified criteria
/***********************************/
ALTER FUNCTION [dbo].[fnCarrierRouteRate](@StartDate date, @EndDate date, @OriginID int, @DestinationID int, @ShipperID int, @CarrierID int, @ProductGroupID int, @BestMatchOnly bit = 0) 
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT R.ID
			, Ranking =	dbo.fnRateRanking(@ShipperID, R.ShipperID, 4, 0)
				  + dbo.fnRateRanking(@CarrierID, R.CarrierID, 2, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 1, 0)
		FROM dbo.viewCarrierRouteRate R
		JOIN tblRoute RO ON RO.ID = R.RouteID
		WHERE (nullif(@OriginID, 0) IS NULL OR @OriginID = RO.OriginID)
		  AND (nullif(@DestinationID, 0) IS NULL OR @DestinationID = RO.DestinationID)
		  AND coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	)
	
	SELECT R.ID, RouteID, OriginID, DestinationID, ShipperID, CarrierID, ProductGroupID, Rate, RateTypeID, UomID, EffectiveDate, EndDate, NextEffectiveDate
	  , BestMatch, Ranking
	  , Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewCarrierRouteRate R
	JOIN (
		SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 3  -- ensure some type of match occurred on all 6 criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)
GO

/***********************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RouteRate rows for the specified criteria
/***********************************/
ALTER FUNCTION [dbo].[fnCarrierRouteRatesDisplay](@StartDate date, @EndDate date, @OriginID int, @DestinationID int, @ShipperID int, @CarrierID int, @ProductGroupID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.RouteID, RO.ActualMiles, R.OriginID, R.DestinationID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.Rate, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate, R.NextEffectiveDate
		, Origin = O.Name
		, OriginFull = O.FullName
		, Destination = D.Name
		, DestinationFull = D.FullName
		, Shipper = SH.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, RateType = RT.Name
		, Uom = U.Name
		, UomShort = U.Abbrev
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch, Ranking
	FROM dbo.fnCarrierRouteRate(@StartDate, @EndDate, @OriginID, @DestinationID, @ShipperID, @CarrierID, @ProductGroupID, 0) R
	JOIN tblRoute RO ON RO.ID = R.RouteID
	JOIN viewOrigin O ON O.ID = R.OriginID
	JOIN viewDestination D ON D.ID = R.DestinationID
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblRateType RT ON RT.ID = R.RateTypeID
	LEFT JOIN tblUom U ON U.ID = R.UomID
	ORDER BY EffectiveDate
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RouteRate info for the specified order
/***********************************/
ALTER FUNCTION [dbo].[fnOrderCarrierRouteRate](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, RateTypeID, Rate, R.UomID
	FROM dbo.viewOrder O
	CROSS APPLY dbo.fnCarrierRouteRate(O.OrderDate, null, O.OriginID, O.DestinationID, O.CustomerID, O.CarrierID, O.ProductGroupID, 1) R
	WHERE O.ID = @ID 
GO


/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper RateSheetRangeRate info for the specified criteria
/***********************************/
ALTER FUNCTION [dbo].[fnShipperRateSheetRangeRate](@StartDate date, @EndDate date, @RouteMiles int, @ShipperID int, @ProductGroupID int, @OriginStateID int, @DestStateID int, @RegionID int, @BestMatchOnly bit = 0)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT RateSheetID = R.ID, RangeRateID = RR.ID
			, Ranking = CASE WHEN @RouteMiles BETWEEN RR.MinRange AND RR.MaxRange THEN 32 ELSE 0 END
				+ dbo.fnRateRanking(@ShipperID, R.ShipperID, 16, 0)
				+ dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 8, 0)
				+ dbo.fnRateRanking(@OriginStateID, R.OriginStateID, 4, 0)
				+ dbo.fnRateRanking(@DestStateID, R.DestStateID, 2, 0)
				+ dbo.fnRateRanking(@RegionID, R.RegionID, 1, 0)
		FROM dbo.viewShipperRateSheet R
		JOIN dbo.tblShipperRangeRate RR ON RR.RateSheetID = R.ID AND isnull(@RouteMiles, RR.MinRange) BETWEEN RR.MinRange AND RR.MaxRange
		WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@OriginStateID, 0), R.OriginStateID, 0) = coalesce(OriginStateID, nullif(@OriginStateID, 0), 0)
		  AND coalesce(nullif(@DestStateID, 0), R.DestStateID, 0) = coalesce(DestStateID, nullif(@DestStateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	)
	
	SELECT RR.ID, RateSheetID = R.ID, ShipperID, ProductGroupID, OriginStateID, DestStateID, RegionID, Rate, MinRange, MaxRange, RateTypeID, UomID, EffectiveDate, EndDate, NextEffectiveDate
	  , BestMatch, Ranking
	  , Locked, R.CreateDateUTC, R.CreatedByUser, R.LastChangeDateUTC, R.LastChangedByUser 
	FROM dbo.viewShipperRateSheet R
	JOIN dbo.tblShipperRangeRate RR ON RR.RateSheetID = R.ID
	JOIN (
		SELECT RateSheetID, RangeRateID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 6  -- ensure some type of match occurred on all 6 criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.RateSheetID = R.ID AND X.RangeRateID = RR.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)
GO

DROP INDEX udxShipperRateSheet_Main ON dbo.tblShipperRouteRate
GO
CREATE UNIQUE CLUSTERED INDEX udxShipperRouteRate_Main ON dbo.tblShipperRouteRate
	(
	ShipperID,
	ProductGroupID,
	RouteID,
	EffectiveDate
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.tblShipperRouteRate SET (LOCK_ESCALATION = TABLE)
GO

EXEC _spRebuildAllObjects
GO

COMMIT TRAN
SET NOEXEC OFF