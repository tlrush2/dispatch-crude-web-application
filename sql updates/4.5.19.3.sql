SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.5.19.2'
SELECT  @NewVersion = '4.5.19.3'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'JT-2993 - Add Custom Fields to import center'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


 -- reset the seed so any web created fields are in the "customer" range
DECLARE @seed INT
SELECT @seed = dbo.fnMaxInt((SELECT MAX(ID) FROM tblObjectField), 100000)
DBCC CHECKIDENT('tblObjectField', RESEED, @seed)
GO

ALTER TABLE tblObjectCustomData ADD CONSTRAINT DF_ObjectCustomData_CreatedByUser DEFAULT 'System' FOR CreatedByUser
GO

/*********************************************
 Creation Info: 2.12.8.2 - 2016/06/25
 Author: Kevin Alons
 Purpose: find and return the custom data value (or default if not available) for the specified parameters
 Changes:
 --		4.5.19.3		2017-04-06		JAE			Add referece to the appropriate custom field.  Failed if a record had multiple custom data
 *********************************************/
ALTER FUNCTION fnObjectCustomData(@ObjectFieldID int, @RecordID int, @DefaultValue varchar(max)) RETURNS varchar(max) AS
BEGIN
	DECLARE @ret varchar(max)
	SET @ret = isnull((SELECT Value FROM tblObjectCustomData WHERE RecordID = @RecordID AND ObjectFieldID = @ObjectFieldID), @DefaultValue)
	RETURN (@ret)
END

GO


/*********************************************
 Creation Info: 2.12.8.2 - 2016/06/25
 Author: Kevin Alons
 Purpose: ensure any OBJECT and TICKET Custom Data Fields are exposed as ReportCenter fields when created/modified and removed when deleted
 Changes:
 --		4.5.19.3		2017-04-06		JAE			Attempt to add ReportCenter field for non-order/non-ticket custom data.  For example adding
													a driver custom field will attempt to look up DriverID in the report center report and 
													will add an entry if the column exists in that view.  That column will appear under 
													CUSTOM | Driver | <custom field> from the report center designer
 *********************************************/
ALTER TRIGGER trigObjectField_IUD ON tblObjectField FOR INSERT, UPDATE, DELETE AS
BEGIN
	DECLARE @work TABLE (ID int, ObjectID int, ObjectFieldTypeID int, Name varchar(255), Object varchar(255), Action varchar(25), RCID int)

	INSERT INTO @work
		SELECT i.ID, IO.ObjectID, IO.ObjectFieldTypeID, IO.Name, IO.Object, CASE WHEN d.ID IS NULL OR CD.ObjectFieldID IS NULL THEN 'INSERT' ELSE 'UPDATE' END, CD.ReportColumnDefinitionID
		FROM inserted i
		JOIN viewObjectField IO ON IO.ID = i.ID
		LEFT JOIN deleted d ON d.ID = i.ID
		LEFT JOIN tblObjectCustomDataReportColumn CD ON CD.ObjectFieldID = i.ID
		WHERE IO.ID >= 100000 -- customer defined custom field
			
		UNION
		SELECT d.ID, NULL, NULL, NULL, NULL, 'DELETE', CD.ReportColumnDefinitionID
		FROM deleted d
		JOIN tblObjectCustomDataReportColumn CD ON CD.ObjectFieldID = d.id
		LEFT JOIN inserted i ON i.ID = d.ID
		WHERE d.ID >= 100000 -- customer defined custom field
		  AND i.ID IS NULL

	DECLARE @id int, @objectID int, @action varchar(25), @RCID int, @Name varchar(255), @OFTID int, @DataField varchar(max), @Caption varchar(255), @FilterTypeID int
	WHILE EXISTS (SELECT * FROM @work)
	BEGIN
		SELECT TOP 1 @id = ID, @objectID = ObjectID, @action = Action, @RCID = RCID, @Name = Name, @OFTID = ObjectFieldTypeID 
			, @DataField = 'dbo.fnObjectCustomData(' + ltrim(ID) + ', RS.' + CASE WHEN ObjectID = 1 THEN 'ID'  -- order
																				  WHEN ObjectID = 2 THEN 'T_ID' -- ticket
																				  ELSE (SELECT Name FROM tblObject WHERE ID = ObjectID)+'ID' END + ', '''')' -- other (xxxID)
			, @Caption = CASE WHEN ObjectID = 1 THEN 'CUSTOM | ' + Name -- order
						      WHEN ObjectID = 2 THEN 'TICKET | CUSTOM | ' + Name -- ticket 
						      ELSE 'CUSTOM | ' + (SELECT Name FROM tblObject WHERE ID = ObjectID) + ' | ' + Name END -- other

			, @FilterTypeID = CASE ObjectFieldTypeID WHEN 1 THEN 1 WHEN 2 THEN 5 WHEN 3 THEN 4 WHEN 4 THEN 4 WHEN 5 THEN 3 WHEN 6 THEN 3 WHEN 7 THEN 0 WHEN 8 THEN 1 END
		FROM @work
		WHERE ObjectID IN (1,2)
			OR EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS
						WHERE TABLE_NAME = 'viewReportCenter_Orders_NoGPS'
						AND COLUMN_NAME = (SELECT Name FROM tblObject WHERE ID = ObjectID)+'ID') -- will only insert if xxxID is a field
			OR ACTION = 'DELETE'

		IF (@action = 'INSERT') 
		BEGIN
			INSERT INTO tblReportColumnDefinition (ReportID, DataField, Caption, FilterDataField, FilterTypeID, FilterAllowCustomText, AllowedRoles, OrderSingleExport)
				VALUES (
					1
					, @DataField
					, @Caption
					, @DataField
					, @FilterTypeID
					, 1
					, '*'
					, CASE WHEN @ObjectID = 1 THEN 1 ELSE 0 END
				)
			INSERT INTO tblObjectCustomDataReportColumn (ObjectFieldID, ReportColumnDefinitionID) VALUES (@id, SCOPE_IDENTITY())
		END
		ELSE IF (@action = 'UPDATE')
			UPDATE tblReportColumnDefinition
				SET ReportID = 1
					, DataField = @DataField
					, Caption = @Caption
					, FilterDataField = @DataField
					, FilterTypeID = @FilterTypeID
					, FilterAllowCustomText = 1
					, AllowedRoles = '*'
					, OrderSingleExport = CASE WHEN @ObjectID = 1 THEN 1 ELSE 0 END
			WHERE ID = @RCID
		ELSE IF (@action = 'DELETE')
		BEGIN
			DELETE FROM tblObjectCustomDataReportColumn WHERE ObjectFieldID = @ID
			DELETE FROM tblUserReportColumnDefinition WHERE ReportColumnID = @RCID
			DELETE FROM tblReportColumnDefinition WHERE ID = @RCID
		END
		
		DELETE FROM @work WHERE id = @id
	END
END

GO


COMMIT
SET NOEXEC OFF