/*
	-- slight performance optimization
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.8.3'
SELECT  @NewVersion = '2.8.4'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details + FINANCIAL INFO into a single view
/***********************************/
ALTER VIEW [dbo].[viewOrder_Financial_Carrier] AS 
	SELECT OE.* 
		, dbo.fnOrderTicketDetails(OE.ID, 'TicketNums', '<br/>') AS TicketNums
		, dbo.fnOrderTicketDetails(OE.ID, 'TankNums', '<br/>') AS TankNums
		, dbo.fnRerouteDetails(OE.ID, 'PreviousDestinations', '<br/>') AS PreviousDestinations
		, dbo.fnRerouteDetails(OE.ID, 'RerouteUsers', '<br/>') AS RerouteUsers
		, dbo.fnRerouteDetails(OE.ID, 'RerouteDates', '<br/>') AS RerouteDates
		, dbo.fnRerouteDetails(OE.ID, 'RerouteNotes', '<br/>') AS RerouteNotes
		, InvoiceRatesAppliedDate = dbo.fnUTC_To_Local(ISNULL(OIC.LastChangeDateUTC, OIC.CreateDateUTC), OE.OriginTimeZoneID, OE.OriginUseDST)
		, OIC.BatchID
		, SB.BatchNum AS InvoiceBatchNum
		, OIC.ChainupFee AS InvoiceChainupFee
		, OIC.RerouteFee AS InvoiceRerouteFee
		, OIC.BillableOriginWaitMinutes AS InvoiceOriginBillableWaitMinutes
		, OIC.BillableDestWaitMinutes AS InvoiceDestBillableWaitMinutes
		, isnull(OIC.BillableOriginWaitMinutes, 0) + ISNULL(OIC.BillableDestWaitMinutes, 0) AS InvoiceTotalBillableWaitMinutes
		, isnull(WFSU.Name, 'None') AS InvoiceWaitFeeSubUnit
		, isnull(WFRT.Name, 'None') AS InvoiceWaitFeeRoundingType
		, OIC.WaitRate AS InvoiceWaitRate
		, OIC.OriginWaitFee AS InvoiceOriginWaitFee
		, OIC.DestWaitFee AS InvoiceDestWaitFee
		, ISNULL(OIC.OriginWaitFee, 0) + ISNULL(OIC.DestWaitFee, 0) AS InvoiceTotalWaitFee
		, OIC.RejectionFee AS InvoiceRejectionFee
		, OIC.H2SRate AS InvoiceH2SRate
		, OIC.H2SFee AS InvoiceH2SFee
		, OIC.TaxRate AS InvoiceTaxRate
		, U.Name AS InvoiceUom
		, U.Abbrev AS InvoiceUomShort
		, OIC.MinSettlementUnits AS InvoiceMinSettlementUnits
		, OIC.Units AS InvoiceUnits
		, OIC.RouteRate AS InvoiceRouteRate
		, OIC.LoadFee AS InvoiceLoadFee
		, OIC.TotalFee AS InvoiceTotalFee
		, OIC.FuelSurcharge AS InvoiceFuelSurcharge
	FROM dbo.viewOrderExportFull OE
	LEFT JOIN dbo.tblOrderInvoiceCarrier OIC ON OIC.OrderID = OE.ID
	LEFT JOIN dbo.tblCarrierSettlementBatch SB ON SB.ID = OIC.BatchID
	LEFT JOIN dbo.tblUom U ON U.ID = OIC.UomID	
	LEFT JOIN dbo.tblWaitFeeSubUnit WFSU ON WFSU.ID = OIC.WaitFeeSubUnitID
	LEFT JOIN dbo.tblWaitFeeRoundingType WFRT ON WFRT.ID = OIC.WaitFeeRoundingTypeID

GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details into a single view
/***********************************/
ALTER VIEW [dbo].[viewOrder_OrderTicket_Full] AS 
	SELECT OE.* 
		, CASE WHEN OE.TicketCount = 0 THEN OE.TicketType ELSE OT.TicketType END AS T_TicketType
		, CASE WHEN OE.TicketCount = 0 THEN OE.CarrierTicketNum ELSE OT.CarrierTicketNum END AS T_CarrierTicketNum
		, CASE WHEN OE.TicketCount = 0 THEN OE.OriginBOLNum ELSE OT.BOLNum END AS T_BOLNum
		, OT.OriginTankText AS T_TankNum
		, OT.IsStrappedTank AS T_IsStrappedTank
		, OT.BottomFeet AS T_BottomFeet, OT.BottomInches AS T_BottomInches, OT.BottomQ AS T_BottomQ
		, OT.OpeningGaugeFeet AS T_OpeningGaugeFeet, OT.OpeningGaugeInch AS T_OpeningGaugeInch, OT.OpeningGaugeQ AS T_OpeningGaugeQ
		, OT.ClosingGaugeFeet AS T_ClosingGaugeFeet, OT.ClosingGaugeInch AS T_ClosingGaugeInch, OT.ClosingGaugeQ AS T_ClosingGaugeQ
		, dbo.fnGaugeQtrInches(OT.OpeningGaugeFeet, OT.OpeningGaugeInch, OT.OpeningGaugeQ) AS T_OpenTotalQ
		, dbo.fnGaugeQtrInches(OT.ClosingGaugeFeet, OT.ClosingGaugeInch, ot.ClosingGaugeQ) AS T_CloseTotalQ
		, ltrim(OT.OpeningGaugeFeet) + 'ft ' + ltrim(OT.OpeningGaugeInch) + 'in ' + ltrim(OT.OpeningGaugeQ) + 'q' AS T_OpenReading
		, ltrim(OT.ClosingGaugeFeet) + 'ft ' + ltrim(OT.ClosingGaugeInch) + 'in ' + ltrim(OT.ClosingGaugeQ) + 'q' AS T_CloseReading
		-- using cast(xx as decimal(9,4)) to properly round to 3 decimal digits (with no trailing 0's)
		, ltrim(round(cast(OT.Gravity60F as decimal(9,4)), 9, 4)) AS T_CorrectedAPIGravity
		, T_GrossStdUnits = OT.GrossStdUnits
		, ltrim(OT.SealOff) AS T_SealOff
		, ltrim(OT.SealOn) AS T_SealOn
		, OT.ProductObsTemp AS T_ProductObsTemp
		, OT.ProductObsGravity AS T_ProductObsGravity
		, OT.ProductBSW AS T_ProductBSW
		, OT.Rejected AS T_Rejected
		, OT.RejectNotes AS T_RejectNotes
		, OT.GrossUnits AS T_GrossUnits
		, OT.NetUnits AS T_NetUnits
		, dbo.fnRerouteDetails(OE.ID, 'PreviousDestinations', '<br/>') AS PreviousDestinations
		, dbo.fnRerouteDetails(OE.ID, 'RerouteUsers', '<br/>') AS RerouteUsers
		, dbo.fnRerouteDetails(OE.ID, 'RerouteDates', '<br/>') AS RerouteDates
		, dbo.fnRerouteDetails(OE.ID, 'RerouteNotes', '<br/>') AS RerouteNotes
	FROM dbo.viewOrderExportFull OE
	LEFT JOIN viewOrderTicket OT ON OT.OrderID = OE.ID AND OT.DeleteDateUTC IS NULL
	WHERE OE.DeleteDateUTC IS NULL

GO

COMMIT
SET NOEXEC OFF