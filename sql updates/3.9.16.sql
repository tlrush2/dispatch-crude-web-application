-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.9.15.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.9.15.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0

SET NOEXEC OFF
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.9.15'
SELECT  @NewVersion = '3.9.16'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Carrier Settlement: fix bug where "% of Shipper" rates potentially applied against wrong Shipper rate if multiple are present'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier AssessorialRate Amounts info for the specified order
-- Changes:
	- 3.7.13 - 2015/09/05 - KDA - fix to "% of Shipper" rates
	- 3.9.16 - 2015/09/21 - KDA - fix issue where wrong ShipperAssessorialCharge was used for "% of Shipper" rates when multiple Shipper Assessorial charges are present
***********************************/
ALTER FUNCTION fnOrderCarrierAssessorialAmounts(@ID int, @Units decimal(18, 10), @UomID int) RETURNS TABLE AS RETURN
	SELECT AssessorialRateTypeID = TypeID, RateID, RateTypeID, Amount
	FROM (
		SELECT TypeID, RateID = ID, RateTypeID
			, Amount = dbo.fnRateToAmount(RateTypeID, @Units, @UomID, Rate, UomID, OSC.LoadAmount
			, CASE WHEN RateTypeID = 3 THEN (SELECT TOP 1 Amount FROM tblOrderSettlementShipperAssessorialCharge SSAC WHERE OrderID = @ID AND SSAC.AssessorialRateTypeID = CAR.TypeID) ELSE NULL END
			, NULL)
		FROM dbo.fnOrderCarrierAssessorialRates(@ID) CAR
		JOIN tblOrderSettlementCarrier OSC ON OSC.OrderID = @ID
	) X
	WHERE Amount IS NOT NULL

GO

COMMIT
SET NOEXEC OFF