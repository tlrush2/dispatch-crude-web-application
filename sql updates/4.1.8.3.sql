SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.1.8.2'
SELECT  @NewVersion = '4.1.8.3'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1552 Update to eligibledrivers script'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/****************************************************/
-- Created: 2016.09.14 - 4.1.7 - Joe Engler
-- Purpose: Retrieve the drivers (without last known GPS coordinates) for the specified filter criteria
-- Changes:
--		4.1.8.3		JAE		2016-09-21		Fixed refrence to current time to start time (passed in)
/****************************************************/
ALTER FUNCTION fnRetrieveEligibleDrivers_NoGPS(@CarrierID INT, @RegionID INT, @StateID INT, @DriverGroupID INT, @StartDate DATETIME) 
RETURNS @ret TABLE 
(
	ID INT, 
	LastName VARCHAR(20), FirstName VARCHAR(20), FullName VARCHAR(41), FullNameLF VARCHAR(42),
	CarrierID INT, Carrier VARCHAR(40),
	RegionID INT,
	TruckID INT, Truck VARCHAR(10),
	TrailerID INT, Trailer VARCHAR(10),
	AvailabilityFactor DECIMAL(4,2),
	ComplianceFactor DECIMAL(4,2),
	TruckAvailabilityFactor DECIMAL(4,2),
	CurrentWorkload INT,
	CurrentECOT INT,
	HrsOnShift FLOAT,
	HOSHrsOnShift FLOAT,
	HOSHrsLeftToday FLOAT,
	HOSHrsLeftWeek FLOAT,
	DriverScore INT 
)
AS BEGIN
	DECLARE @__ENFORCE_DRIVER_AVAILABILITY__ INT = 13
	DECLARE @__ENFORCE_DRIVER_COMPLIANCE__ INT = 14

	IF @StartDate IS NULL 
	   SELECT @StartDate = dbo.fnDateOnly(GETDATE())

	INSERT INTO @ret
	SELECT X.*,
		DriverScore = 100 * AvailabilityFactor * ComplianceFactor
	FROM (
		SELECT d.ID, 
			d.FirstName, d.LastName, FullName, d.FullNameLF,
			d.CarrierID, d.Carrier,
			d.RegionID,
			d.TruckID, d.Truck,
			d.TrailerID, d.Trailer,

			--Availability
			AvailabilityFactor = CASE WHEN cr_a.Value IS NULL OR dbo.fnToBool(cr_a.Value) = 0 THEN 1 -- availability not enfoced
										WHEN IsAvailable IS NULL THEN 0 -- Not explicitly available
										ELSE IsAvailable END,

			-- Compliance
			ComplianceFactor = CASE WHEN cr_c.Value IS NULL OR dbo.fnToBool(cr_c.Value) = 0 THEN 1 -- compliance not enforced
									WHEN d.CDLExpiration < @StartDate -- CDL expired
											OR d.H2SExpiration < @StartDate -- H2s expired
											OR d.DLExpiration < @StartDate -- DL expired
											--OR d.MedicalCardDate
											THEN 0
									ELSE 1 END, -- all ok

			--Truck/Trailer Availability
			TruckAvailabilityFactor = 1,
			
			--current workload, time on shift
			o.CurrentWorkload,
			CurrentECOT = ISNULL(o.CurrentECOT, 0),
				

			-- HOS
			HrsOnShift = CAST(DATEDIFF(MINUTE, AvailDateTime, GETDATE())/60.0*IsAvailable AS DECIMAL(5,1)), -- from availabilty
			HOSHrsOnShift = hos.TotalHours, -- from HOS
			HOSHrsLeftToday = 0,
			HOSHrsLeftWeek = 0

		FROM viewDriverBase d -- KDA NOTE: slightly more efficient
		LEFT JOIN tblDriverAvailability da ON da.DriverID = d.ID AND CAST(AvailDateTime AS DATE) = @StartDate
		OUTER APPLY dbo.fnCarrierRules(@StartDate, null, @__ENFORCE_DRIVER_AVAILABILITY__, d.CarrierID, d.ID, d.StateID, d.RegionID, 1) cr_a -- Enforce Driver Availability Carrier Rule
		OUTER APPLY dbo.fnCarrierRules(@StartDate, null, @__ENFORCE_DRIVER_COMPLIANCE__, d.CarrierID, d.ID, d.StateID, d.RegionID, 1) cr_c
		OUTER APPLY dbo.fnHosSummary(d.ID, @StartDate, DATEADD(DAY, 1, @StartDate)) hos
		OUTER APPLY (
			SELECT CurrentWorkload = COUNT(*), 
					CurrentECOT = SUM(CASE WHEN o.StatusID = 8 THEN ISNULL(r.ECOT, 60+ISNULL(r.ActualMiles,60)+60)/2
										   ELSE COALESCE(r.ECOT, 60+ISNULL(r.ActualMiles,60)+60) END)
			FROM tblOrder o LEFT JOIN tblRoute r ON o.RouteID = r.ID
			WHERE DriverID = d.ID
			  AND StatusID IN (2,7,8) -- Dispatched, Accepted, Picked Up
			  AND DeleteDateUTC IS NULL) o

		WHERE d.DeleteDateUTC IS NULL
			AND d.TruckID IS NOT NULL AND d.TrailerID IS NOT NULL
			AND (ISNULL(@CarrierID, 0) <= 0 OR @CarrierID = d.CarrierID)
			AND (ISNULL(@DriverGroupID, 0) <= 0 OR @DriverGroupID = d.ID)
			AND (ISNULL(@RegionID, -1) <= 0 OR @RegionID = d.RegionID)
			AND (ISNULL(@StateID, 0) <= 0 OR @StateID = d.StateID)
	) AS X
	ORDER BY DriverScore DESC, LastName, FirstName

	RETURN
END

GO


COMMIT
SET NOEXEC OFF