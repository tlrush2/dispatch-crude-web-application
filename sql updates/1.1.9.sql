--ROLLBACK
BEGIN TRANSACTION

DELETE FROM ELMAH_Error

ALTER TABLE tblTankType ADD BarrelsPerInch decimal(9,6) NULL
GO
UPDATE tblTankType SET BarrelsPerInch = 
	round(ISNULL(CapacityBarrelsActual, CapacityBarrels) / HeightFeet / 12, 6)
GO

ALTER TABLE dbo.tblTankType
	DROP CONSTRAINT DF_tblTankType_CreateDate
GO
CREATE TABLE dbo.Tmp_tblTankType
	(
	ID int NOT NULL IDENTITY (1, 1),
	CapacityBarrels smallint NOT NULL,
	HeightFeet smallint NOT NULL,
	CreateDate smalldatetime NULL,
	CreatedByUser varchar(100) NULL,
	LastChangeDate smalldatetime NULL,
	LastChangedByUser varchar(100) NULL,
	BarrelsPerInch decimal(9, 6) NOT NULL,
	BarrelsPerFoot decimal(9, 3) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_tblTankType SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_tblTankType ADD CONSTRAINT
	DF_tblTankType_CreateDate DEFAULT (getdate()) FOR CreateDate
GO
SET IDENTITY_INSERT dbo.Tmp_tblTankType ON
GO
IF EXISTS(SELECT * FROM dbo.tblTankType)
	 EXEC('INSERT INTO dbo.Tmp_tblTankType (ID, CapacityBarrels, HeightFeet, CreateDate, CreatedByUser, LastChangeDate, LastChangedByUser, BarrelsPerInch)
		SELECT ID, CapacityBarrels, HeightFeet, CreateDate, CreatedByUser, LastChangeDate, LastChangedByUser, BarrelsPerInch FROM dbo.tblTankType WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_tblTankType OFF
GO
ALTER TABLE dbo.tblOrderTicket
	DROP CONSTRAINT FK_OrderTicket_TankType
GO
DROP TABLE dbo.tblTankType
GO
EXECUTE sp_rename N'dbo.Tmp_tblTankType', N'tblTankType', 'OBJECT' 
GO
ALTER TABLE dbo.tblTankType ADD CONSTRAINT
	PK_TankType PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE UNIQUE NONCLUSTERED INDEX udxTankType_Capacity_Height ON dbo.tblTankType
	(
	CapacityBarrels,
	HeightFeet
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/***********************************/
-- Date Created: 9 Dec 2012
-- Author: Kevin Alons
-- Purpose: return TankType data + "Full Name" computed field
/***********************************/
ALTER VIEW [dbo].[viewTankType] AS
SELECT *
	, ltrim(CapacityBarrels) + 'BBL (' + ltrim(HeightFeet) + 'FT)' as FullName
FROM dbo.tblTankType

GO

ALTER TABLE dbo.tblOrderTicket ADD CONSTRAINT
	FK_OrderTicket_TankType FOREIGN KEY
	(
	TankTypeID
	) REFERENCES dbo.tblTankType
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderTicket SET (LOCK_ESCALATION = TABLE)
GO

ALTER TABLE dbo.tblOrderTicket ADD BarrelsPerInch decimal(9,6) null
GO

ALTER TABLE dbo.tblOrigin
	DROP CONSTRAINT FK_tblOrigin_tblProducer
GO
ALTER TABLE dbo.tblProducer SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrigin
	DROP CONSTRAINT FK_tblOrigin_tblOriginTypes
GO
ALTER TABLE dbo.tblOriginType SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrigin
	DROP CONSTRAINT FK_tblOrigin_tblTicketType
GO
ALTER TABLE dbo.tblTicketType SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrigin
	DROP CONSTRAINT FK_tblOrigin_tblStates
GO
ALTER TABLE dbo.tblState SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrigin
	DROP CONSTRAINT FK_tblOrigin_tblRegion
GO
ALTER TABLE dbo.tblRegion SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrigin
	DROP CONSTRAINT FK_tblOrigin_tblOperator
GO
ALTER TABLE dbo.tblOperator SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrigin
	DROP CONSTRAINT FK_tblOrigin_tblPumper
GO
ALTER TABLE dbo.tblPumper SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrigin
	DROP CONSTRAINT DF_tblOrigin_TicketTypeID
GO
ALTER TABLE dbo.tblOrigin
	DROP CONSTRAINT DF_tblOrigin_Active
GO
ALTER TABLE dbo.tblOrigin
	DROP CONSTRAINT DF_tblOrigin_TankTypeID
GO
ALTER TABLE dbo.tblOrigin
	DROP CONSTRAINT DF_tblOrigin_CreateDate
GO
CREATE TABLE dbo.Tmp_tblOrigin
	(
	ID int NOT NULL IDENTITY (1, 1),
	OriginTypeID int NOT NULL,
	Name varchar(30) NOT NULL,
	Address varchar(50) NULL,
	City varchar(30) NULL,
	StateID int NULL,
	Zip varchar(50) NULL,
	wellAPI varchar(20) NULL,
	LAT varchar(20) NULL,
	LON varchar(20) NULL,
	OperatorID int NULL,
	PumperID int NULL,
	County varchar(25) NULL,
	LeaseName varchar(35) NULL,
	LeaseNum varchar(30) NULL,
	TicketTypeID int NOT NULL,
	RegionID int NULL,
	TotalDepth int NULL,
	SpudDate smalldatetime NULL,
	FieldName varchar(25) NULL,
	NDICFileNum varchar(10) NULL,
	CTBNum nchar(10) NULL,
	Active bit NOT NULL,
	CustomerID int NULL,
	ProducerID int NULL,
	TankTypeID int NULL,
	CreateDate smalldatetime NULL,
	CreatedByUser varchar(100) NULL,
	LastChangeDate smalldatetime NULL,
	LastChangedByUser varchar(100) NULL,
	BarrelsPerInch decimal(9, 6) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_tblOrigin SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_tblOrigin ADD CONSTRAINT
	DF_tblOrigin_Active DEFAULT ((1)) FOR Active
GO
ALTER TABLE dbo.Tmp_tblOrigin ADD CONSTRAINT
	DF_tblOrigin_TankTypeID DEFAULT ((1)) FOR TankTypeID
GO
ALTER TABLE dbo.Tmp_tblOrigin ADD CONSTRAINT
	DF_tblOrigin_CreateDate DEFAULT (getdate()) FOR CreateDate
GO
SET IDENTITY_INSERT dbo.Tmp_tblOrigin ON
GO
IF EXISTS(SELECT * FROM dbo.tblOrigin)
	 EXEC('INSERT INTO dbo.Tmp_tblOrigin (ID, OriginTypeID, Name, Address, City, StateID, Zip, wellAPI, LAT, LON, OperatorID, PumperID, County, LeaseName, LeaseNum, TicketTypeID, RegionID, TotalDepth, SpudDate, FieldName, NDICFileNum, CTBNum, Active, CustomerID, ProducerID, TankTypeID, CreateDate, CreatedByUser, LastChangeDate, LastChangedByUser)
		SELECT ID, OriginTypeID, Name, Address, City, StateID, Zip, wellAPI, LAT, LON, OperatorID, PumperID, County, LeaseName, LeaseNum, TicketTypeID, RegionID, TotalDepth, SpudDate, FieldName, NDICFileNum, CTBNum, Active, CustomerID, ProducerID, TankTypeID, CreateDate, CreatedByUser, LastChangeDate, LastChangedByUser FROM dbo.tblOrigin WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_tblOrigin OFF
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_Origin
GO
ALTER TABLE dbo.tblOrigin
	DROP CONSTRAINT FK_tblOrigin_tblOrigin
GO
ALTER TABLE dbo.tblRoute
	DROP CONSTRAINT FK_tblRoute_tblOrigin
GO
DROP TABLE dbo.tblOrigin
GO
EXECUTE sp_rename N'dbo.Tmp_tblOrigin', N'tblOrigin', 'OBJECT' 
GO
ALTER TABLE dbo.tblOrigin ADD CONSTRAINT
	tblOrigin_PrimaryKey PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE NONCLUSTERED INDEX idxOrigin_County ON dbo.tblOrigin
	(
	County
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.tblOrigin ADD CONSTRAINT
	uqOrigin_Name UNIQUE NONCLUSTERED 
	(
	Name
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
DECLARE @v sql_variant 
SET @v = N'Ensure that Origin.Name is unique'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'tblOrigin', N'CONSTRAINT', N'uqOrigin_Name'
GO
CREATE NONCLUSTERED INDEX idxOrigin_Region ON dbo.tblOrigin
	(
	RegionID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxOrigin_State ON dbo.tblOrigin
	(
	StateID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxOrigin_Customer ON dbo.tblOrigin
	(
	CustomerID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxOrigin_OriginType ON dbo.tblOrigin
	(
	OriginTypeID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.tblOrigin ADD CONSTRAINT
	FK_tblOrigin_tblOrigin FOREIGN KEY
	(
	ID
	) REFERENCES dbo.tblOrigin
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrigin ADD CONSTRAINT
	FK_tblOrigin_tblPumper FOREIGN KEY
	(
	PumperID
	) REFERENCES dbo.tblPumper
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrigin ADD CONSTRAINT
	FK_tblOrigin_tblOperator FOREIGN KEY
	(
	OperatorID
	) REFERENCES dbo.tblOperator
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrigin ADD CONSTRAINT
	FK_tblOrigin_tblRegion FOREIGN KEY
	(
	RegionID
	) REFERENCES dbo.tblRegion
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrigin ADD CONSTRAINT
	FK_tblOrigin_tblStates FOREIGN KEY
	(
	StateID
	) REFERENCES dbo.tblState
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrigin ADD CONSTRAINT
	FK_tblOrigin_tblTicketType FOREIGN KEY
	(
	TicketTypeID
	) REFERENCES dbo.tblTicketType
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrigin ADD CONSTRAINT
	FK_tblOrigin_tblOriginTypes FOREIGN KEY
	(
	OriginTypeID
	) REFERENCES dbo.tblOriginType
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrigin ADD CONSTRAINT
	FK_tblOrigin_tblProducer FOREIGN KEY
	(
	ProducerID
	) REFERENCES dbo.tblProducer
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblRoute ADD CONSTRAINT
	FK_tblRoute_tblOrigin FOREIGN KEY
	(
	OriginID
	) REFERENCES dbo.tblOrigin
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblRoute SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_Origin FOREIGN KEY
	(
	OriginID
	) REFERENCES dbo.tblOrigin
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrder SET (LOCK_ESCALATION = TABLE)
GO

UPDATE tblOrigin SET TankTypeID = NULL, BarrelsPerInch = NULL WHERE TicketTypeID NOT IN (1)

UPDATE tblOrigin SET BarrelsPerInch = TT.BarrelsPerInch
FROM tblOrigin O
JOIN tblTankType TT ON TT.ID = O.TankTypeID
GO

/***********************************/
-- Date Created: 9 Dec 2012
-- Author: Kevin Alons
-- Purpose: return the capacity in a tank for the given feet/inches/q and BPI parameters
/***********************************/
ALTER FUNCTION [dbo].[fnTankQtyBarrels](@feet smallint, @inch tinyint, @q tinyint, @bpi decimal(9,6) = 1.67) RETURNS decimal(9,3) AS
BEGIN
	IF (@bpi IS NULL)
		SELECT @bpi = 1.67 -- default value
	RETURN (@bpi * ((@feet * 12) + @inch + (@q / 4)))
END
GO
/***********************************/
-- Date Created: 9 Dec 2012
-- Author: Kevin Alons
-- Purpose: return the change in capacity in a tank for the given open/close feet/inches/q values & BPI value
/***********************************/
ALTER FUNCTION [dbo].[fnTankQtyBarrelsDelta](
  @openFeet smallint
, @openInch tinyint
, @openQ tinyint
, @closeFeet smallint
, @closeInch tinyint
, @closeQ tinyint
, @bpi decimal(9,6)
) RETURNS decimal(9,3) AS
BEGIN
	RETURN (dbo.fnTankQtyBarrels(@openFeet, @openInch, @openQ, @bpi) - dbo.fnTankQtyBarrels(@closeFeet, @closeInch, @closeQ, @bpi))
END
GO

UPDATE tblOrderTicket 
	SET TankNum = isnull(TankNum, 'unknown')
		, ProductHighTemp = isnull(ProductHighTemp, ProductObsTemp)
		, ProductLowTemp = isnull(ProductLowTemp, ProductObsTemp)
WHERE TicketTypeID in (1,2) AND (TankNum IS NULL OR ProductHighTemp IS NULL or ProductLowTemp IS NULL)

UPDATE tblOrderTicket 
SET SealOff = isnull(SealOff, 'n/a'), SealOn = isnull(SealOn, 'n/a')
WHERE TicketTypeID IN (1,2) AND (Rejected = 1 AND (SealOff IS NULL OR SealOn IS NULL))

UPDATE tblOrderTicket SET RejectNotes = isnull(RejectNotes, 'N/A') WHERE Rejected = 1

UPDATE tblOrderTicket SET TankNum = 'N/A' where TicketTypeID IN (1,2) AND TankNum IS NULL
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 6 Feb 2013
-- Description:	trigger to ensure the GrossBarrels and NetBarrels are computed for valid, entered data
-- =============================================
ALTER TRIGGER [dbo].[trigOrderTicket_IU] ON [dbo].[tblOrderTicket] AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	IF (SELECT COUNT(*) FROM (
		SELECT OT.OrderID
		FROM tblOrderTicket OT
		WHERE OrderID IN (SELECT DISTINCT OrderID FROM inserted)
		  AND OT.DeleteDate IS NULL
		GROUP BY OT.OrderID, OT.CarrierTicketNum
		HAVING COUNT(*) > 1) v) > 0
	BEGIN
		RAISERROR('Duplicate Ticket Numbers are not allowed', 16, 1)
		RETURN
	END
	
	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (3) AND BOLNum IS NULL) > 0
	BEGIN
		RAISERROR('BOL # value is required for Meter Run tickets', 16, 1)
		RETURN
	END
	
	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1, 2) AND TankNum IS NULL) > 0
	BEGIN
		RAISERROR('Tank # value is required for Gauge Run & Net Barrel tickets', 16, 1)
		RETURN
	END

	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1) AND CarrierTicketNum IS NULL) > 0
	BEGIN
		RAISERROR('Ticket # value is required for Gauge Run tickets', 16, 1)
		RETURN
	END

	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1) AND BarrelsPerInch IS NULL) > 0
	BEGIN
		RAISERROR('BarrelsPerInch value is required for Gauge Run tickets', 16, 1)
		RETURN
	END

	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1, 2) 
		AND (ProductHighTemp IS NULL OR ProductObsTemp IS NULL OR ProductLowTemp IS NULL
			OR ProductObsGravity IS NULL OR ProductBSW IS NULL)) > 0
	BEGIN
		RAISERROR('All Product Measurement values are required for Gauge Run & Net Barrel tickets', 16, 1)
		RETURN
	END
	
	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1) AND Rejected = 0 
		AND (OpeningGaugeFeet IS NULL OR OpeningGaugeInch IS NULL OR OpeningGaugeQ IS NULL)) > 0
	BEGIN
		RAISERROR('All Opening Gauge values are required for Gauge Run tickets', 16, 1)
		RETURN
	END
	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1) AND Rejected = 0 
		AND (ClosingGaugeFeet IS NULL OR ClosingGaugeInch IS NULL OR ClosingGaugeQ IS NULL)) > 0
	BEGIN
		RAISERROR('All Closing Gauge values are required for Gauge Run tickets', 16, 1)
		RETURN
	END

	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (2, 3) AND Rejected = 0 
		AND (GrossBarrels IS NULL OR NetBarrels IS NULL)) > 0
	BEGIN
		RAISERROR('Gross & Net Barrel values are required for Net Barrel & Meter Run tickets', 16, 1)
		RETURN
	END

	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1) AND Rejected = 0 
		AND (SealOff IS NULL OR SealOn IS NULL)) > 0
	BEGIN
		RAISERROR('All Seal Off & Seal On values are required for Gauge Run tickets', 16, 1)
		RETURN
	END

	IF (SELECT COUNT(*) FROM inserted WHERE Rejected = 1 AND RejectNotes IS NULL) > 0
	BEGIN
		RAISERROR('Reject Notes are required for Rejected tickets', 16, 1)
		RETURN
	END

	-- re-compute GaugeRun ticket Gross barrels
	IF UPDATE(OpeningGaugeFeet) OR UPDATE(OpeningGaugeInch) OR UPDATE(OpeningGaugeQ)
		OR UPDATE (ClosingGaugeFeet) OR UPDATE(ClosingGaugeInch) OR UPDATE(ClosingGaugeQ) OR UPDATE(BarrelsPerInch)
		OR UPDATE (GrossBarrels)
	BEGIN
		UPDATE tblOrderTicket
		  SET GrossBarrels = dbo.fnTankQtyBarrelsDelta(
				OpeningGaugeFeet
			  , OpeningGaugeInch
			  , OpeningGaugeQ
			  , ClosingGaugeFeet
			  , ClosingGaugeInch
			  , ClosingGaugeQ
			  , BarrelsPerInch)
		WHERE ID IN (SELECT ID FROM inserted) 
		  AND TicketTypeID = 1 -- Gauge Run Ticket
		  AND OpeningGaugeFeet IS NOT NULL
		  AND OpeningGaugeInch IS NOT NULL
		  AND OpeningGaugeQ IS NOT NULL
		  AND ClosingGaugeFeet IS NOT NULL
		  AND ClosingGaugeInch IS NOT NULL
		  AND ClosingGaugeQ IS NOT NULL
		  AND BarrelsPerInch IS NOT NULL
	END
	-- re-compute GaugeRun ticket Net Barrels
	IF UPDATE(OpeningGaugeFeet) OR UPDATE(OpeningGaugeInch) OR UPDATE(OpeningGaugeQ)
		OR UPDATE (ClosingGaugeFeet) OR UPDATE(ClosingGaugeInch) OR UPDATE(ClosingGaugeQ) 
		OR UPDATE(BarrelsPerInch) OR UPDATE(GrossBarrels) OR UPDATE(NetBarrels)
		OR UPDATE(ProductObsTemp) OR UPDATE(ProductObsGravity) OR UPDATE(ProductBSW)
	BEGIN
		UPDATE tblOrderTicket
		  SET NetBarrels = dbo.fnCrudeNetCalculator(GrossBarrels, ProductObsTemp, ProductObsGravity, ProductBSW)
		WHERE ID IN (SELECT ID FROM inserted)
		  AND TicketTypeID IN (1,2)
		  AND GrossBarrels IS NOT NULL
		  AND ProductObsTemp IS NOT NULL
		  AND ProductObsGravity IS NOT NULL
		  AND ProductBSW IS NOT NULL
	END
	-- ensure the Order record is in-sync with the Tickets
	UPDATE tblOrder 
	SET OriginGrossBarrels = (SELECT sum(GrossBarrels) FROM tblOrderTicket WHERE OrderID = O.ID AND Rejected = 0 AND DeleteDate IS NULL)
	  , OriginNetBarrels = (SELECT sum(NetBarrels) FROM tblOrderTicket WHERE OrderID = O.ID AND Rejected = 0 AND DeleteDate IS NULL)
		-- use the first MeterRun CarrierTicketNum num as the Order BOL Num
	  , OriginBOLNum = (SELECT min(BOLNum) FROM tblOrderTicket WHERE OrderID = O.ID AND TicketTypeID = 3 AND DeleteDate IS NULL)
	  , CarrierTicketNum = (SELECT min(CarrierTicketNum) FROM tblOrderTicket WHERE ID IN 
		(SELECT min(ID) FROM tblOrderTicket WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND Rejected = 0 AND DeleteDate IS NULL))
	  , LastChangeDate = (SELECT MAX(LastChangeDate) FROM inserted WHERE OrderID = O.ID)
	  , LastChangedByUser = (SELECT MIN(LastChangedByUser) FROM inserted WHERE OrderID = O.ID)
	FROM tblOrder O
	WHERE ID IN (SELECT DISTINCT OrderID FROM inserted)
END

GO
EXEC _spRefreshAllViews
GO

/*******************************************/
-- Date Created: 25 Apr 2013
-- Author: Kevin Alons
-- Purpose: return Order data for Driver App sync
/*******************************************/
ALTER FUNCTION [dbo].[fnOrder_DriverApp]( @DriverID int, @LastChangeDate datetime ) RETURNS  TABLE AS
RETURN 
	SELECT O.ID
		, O.OrderNum
		, O.TicketTypeID
		, O.StatusID
		, P.PriorityNum
		, O.DueDate
		, O.TruckID
		, O.TrailerID
		, O.Trailer2ID
		, O.Origin
		, O.OriginFull
		, O.OriginArriveTime
		, O.OriginDepartTime
		, O.OriginWaitMinutes
		, O.OriginWaitNotes
		, O.OriginTruckMileage
		, O.OriginGrossBarrels
		, O.OriginNetBarrels
		, O.ChainUp
		, O.Rejected
		, O.RejectNotes
		, O.OriginTankNum
		, O.Destination
		, O.DestinationFull
		, O.DestArriveTime
		, O.DestDepartTime
		, O.DestWaitMinutes
		, O.DestWaitNotes
		, O.DestTruckMileage
		, O.DestGrossBarrels
		, O.DestNetBarrels
		, O.CarrierTicketNum
		, D.BOLAvailable AS DestBOLAvailable
		, OO.TankTypeID
		, OO.LAT AS OriginLAT
		, OO.LON AS OriginLON
		, OO.BarrelsPerInch AS OriginBarrelsPerInch
		, O.CreateDate
		, O.CreatedByUser
		, O.LastChangeDate
		, O.LastChangedByUser
		, O.DeleteDate
		, O.DeletedByUser
	FROM dbo.viewOrder O
	JOIN dbo.tblPriority P ON P.ID = O.PriorityID
	JOIN dbo.tblOrigin OO ON OO.ID = O.OriginID
	JOIN dbo.tblDestination D ON D.ID = O.DestinationID
	WHERE StatusID IN (2, 7, 8) -- Dispatched, Accepted, Picked Up
	  AND O.DriverID = @DriverID
	  AND (@LastChangeDate IS NULL OR O.LastChangeDate >= @LastChangeDate)

GO

/*******************************************/
-- Date Created: 25 Apr 2013
-- Author: Kevin Alons
-- Purpose: return OrderTicket data for Driver App sync
/*******************************************/
ALTER FUNCTION [dbo].[fnOrderTicket_DriverApp]( @DriverID int, @LastChangeDate datetime ) RETURNS  TABLE AS
RETURN 
	SELECT OT.ID
		, OT.UID
		, OT.OrderID
		, OT.CarrierTicketNum
		, OT.TankNum
		, OT.TicketTypeID
		, OT.TankTypeID
		, OT.ProductObsGravity
		, OT.ProductObsTemp
		, OT.ProductHighTemp
		, OT.ProductLowTemp
		, OT.ProductBSW
		, OT.OpeningGaugeFeet
		, OT.OpeningGaugeInch
		, OT.OpeningGaugeQ
		, OT.ClosingGaugeFeet
		, OT.ClosingGaugeInch
		, OT.ClosingGaugeQ
		, OT.GrossBarrels
		, OT.NetBarrels
		, OT.Rejected
		, OT.RejectNotes
		, OT.SealOff
		, OT.SealOn
		, OT.BOLNum
		, OT.BarrelsPerInch
		, OT.CreateDate
		, OT.CreatedByUser
		, OT.LastChangeDate
		, OT.LastChangedByUser
		, OT.DeleteDate
		, OT.DeletedByUser
	FROM dbo.tblOrderTicket OT
	JOIN dbo.tblOrder O ON O.ID = OT.OrderID
	WHERE StatusID IN (2, 7, 8) -- Dispatched, Accepted, Picked Up
	  AND O.DriverID = @DriverID
	  AND (@LastChangeDate IS NULL OR O.LastChangeDate >= @LastChangeDate)

GO

/***********************************/
-- Date Created: 24 Jan 2013
-- Author: Kevin Alons
-- Purpose: accomplish an Order Redirect
/***********************************/
ALTER PROCEDURE [dbo].[spRerouteOrder]
(
  @OrderID int
, @NewDestinationID int
, @UserName varchar(255)
, @Notes varchar(255)
)
AS BEGIN
	INSERT INTO tblOrderReroute (OrderID, PreviousDestinationID, UserName, Notes, RerouteDate)
		SELECT ID, DestinationID, @UserName, @Notes, getdate() FROM tblOrder WHERE ID = @OrderID
	
	UPDATE tblOrder 
		SET DestinationID = R.DestinationID, RouteID = R.ID, ActualMiles = R.ActualMiles
			, LastChangeDate=GETDATE(), LastChangedByUser=@UserName
	FROM tblOrder O
	JOIN tblRoute R ON R.OriginID = O.OriginID AND R.DestinationID = @NewDestinationID
	WHERE O.ID = @OrderID
END



GO

UPDATE tblOrderTicket SET BarrelsPerInch = TT.BarrelsPerInch
FROM tblOrderTicket OT
JOIN tblTankType TT ON TT.ID = OT.TankTypeID

UPDATE tblSetting SET Value = '1.1.9' WHERE ID=0

exec _spRefreshAllViews

COMMIT