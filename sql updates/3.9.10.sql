-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.9.9.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.9.9.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0

SET NOEXEC OFF
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.9.9'
SELECT  @NewVersion = '3.9.10'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Performance optimization to fnOrderXXXCSV functions'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/***********************************************
-- Author:		Kevin Alons
-- Create date: 2015/07/27
-- Description:	return Customers ID CSV field for the specified Origin
-- Changes:
	3.9.10 - 2015/08/31 - KDA - performance optimization (no longer using XML PATH trick)
***********************************************/
ALTER FUNCTION fnOriginCustomerIDCSV(@originID int) RETURNS varchar(1000) AS BEGIN
	DECLARE @ret varchar(max)
	SELECT @ret = COALESCE(@ret + ',', '') + LTRIM(CustomerID) FROM tblOriginCustomers WHERE OriginID = @originID
	RETURN (@ret)
END
GO

/***********************************************
-- Author:		Kevin Alons
-- Create date: 2015/07/27
-- Description:	return Customers CSV field for the specified Origin
-- Changes:
	3.9.10 - 2015/08/31 - KDA - performance optimization (no longer using XML PATH trick)
***********************************************/
ALTER FUNCTION fnOriginCustomersCSV(@originID int) RETURNS varchar(1000) AS BEGIN
	DECLARE @ret varchar(max)
	SELECT @ret = COALESCE(@ret + ',', '') + LTRIM(C.Name) FROM tblOriginCustomers OC JOIN tblCustomer C ON C.ID = OC.CustomerID WHERE OC.OriginID = @originID ORDER BY C.Name
	RETURN (@ret)
END
GO

/**********************************************
-- Author:		Kevin Alons
-- Create date: 3 Oct 2014
-- Description:	return Products [CSV] field for the specified Origin
	3.9.10 - 2015/08/31 - KDA - performance optimization (no longer using XML PATH trick)
**********************************************/
ALTER FUNCTION fnOriginProductIDCSV(@originID int) RETURNS varchar(1000) AS BEGIN
	DECLARE @ret varchar(max)
	SELECT @ret = COALESCE(@ret + ',', '') + LTRIM(ProductID) FROM tblOriginProducts WHERE OriginID = @originID
	RETURN (@ret)
END
GO

/**********************************************
-- Author:		Kevin Alons
-- Create date: 3 Oct 2014
-- Description:	return Products [CSV] field for the specified Origin
	3.9.10 - 2015/08/31 - KDA - performance optimization (no longer using XML PATH trick)
**********************************************/
ALTER FUNCTION fnOriginProductsCSV(@originID int) RETURNS varchar(1000) AS BEGIN
	DECLARE @ret varchar(max)
	SELECT @ret = COALESCE(@ret + ',', '') + LTRIM(P.ShortName) FROM tblOriginProducts OP JOIN tblProduct P ON P.ID = OP.ProductID WHERE OP.OriginID = @originID ORDER BY P.ShortName
	RETURN (@ret)
END
GO

COMMIT
SET NOEXEC OFF