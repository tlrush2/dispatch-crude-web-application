SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.12.8.2'
SELECT  @NewVersion = '3.12.9'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'DCWEB-1375 - Hours Of Service (HOS)'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


CREATE TABLE tblHosDriverStatus
(
	ID INT NOT NULL CONSTRAINT PK_HosDriverStatus PRIMARY KEY,
	Name VARCHAR(25) NOT NULL CONSTRAINT udxHosDriverStatus_Name UNIQUE,
	Driving BIT NOT NULL
)

INSERT INTO tblHosDriverStatus VALUES
(1, 'Off Duty', 0),
(2, 'Sleeper', 0),
(3, 'On Duty - Driving', 1),
(4, 'On Duty - Not Driving', 0)


CREATE TABLE tblHosStatus
(
	ID INT NOT NULL CONSTRAINT PK_HosStatus PRIMARY KEY,
	Name VARCHAR(25) NOT NULL CONSTRAINT udxHosStatus_Name UNIQUE,
)

INSERT INTO tblHosStatus VALUES
(1, 'Status Change'),
(2, 'Engine On'),
(3, 'Engine Off'),
(4, 'In Motion Regular Check')


CREATE TABLE tblHos
(
	ID INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_Hos PRIMARY KEY,
	UID UNIQUEIDENTIFIER NOT NULL CONSTRAINT DF_Hos_UID DEFAULT (NEWID())
								  CONSTRAINT udxHos_UID UNIQUE,
	DriverID INT NOT NULL CONSTRAINT FK_Hos_Driver REFERENCES dbo.tblDriver(ID),
	HosDriverStatusID INT NOT NULL CONSTRAINT FK_Hos_HosDriverStatus REFERENCES dbo.tblHosDriverStatus(ID),
	HosStatusID INT NULL CONSTRAINT FK_Hos_HosStatus REFERENCES dbo.tblHosStatus(ID),

	Lat DECIMAL(9, 6) NOT NULL,
	Lon DECIMAL(9, 6) NOT NULL,
	NearestCity VARCHAR(200) NULL,

	PersonalUse BIT NOT NULL,
	YardMove BIT NOT NULL,

	LogDateUTC SMALLDATETIME NULL,

	CreateDateUTC SMALLDATETIME NOT NULL CONSTRAINT DF_Hos_CreateDateUTC DEFAULT (GETUTCDATE()),
	CreatedByUser VARCHAR(100) NULL,
	LastChangeDateUTC SMALLDATETIME NULL,
	LastChangedByUser VARCHAR(100) NULL,
	DeleteDateUTC SMALLDATETIME NULL,
	DeletedByUser VARCHAR(100) NULL
)

GRANT SELECT, INSERT, UPDATE ON tblHos TO role_iis_acct
GO

/*
INSERT INTO tblHos(DriverID, HosDriverStatusID, HosStatusID, Lat, Lon, NearestCity, PersonalUse, YardMove, LogDateUTC) VALUES
((select top 1 id from tbldriver order by newid()),
 (select top 1 id from tblhosdriverstatus order by newid()),
 (select top 1 id from tblhosstatus order by newid()),
  35, -85, 'Evansville, IN', 0, 0, 
  dateadd(hour,-(rand()*(48-1)+1),GETUTCDATE()))
*/

CREATE TABLE tblHosDbAudit
(
	DBAuditDate DATETIME NOT NULL CONSTRAINT DF_tblHosDBAudit_AuditDate DEFAULT GETUTCDATE(),
	ID INT NOT NULL,
	UID UNIQUEIDENTIFIER NOT NULL,
	DriverID INT NOT NULL,
	HosDriverStatusID INT NOT NULL,
	HosStatusID INT NULL,

	Lat DECIMAL(9, 6) NOT NULL,
	Lon DECIMAL(9, 6) NOT NULL,
	NearestCity VARCHAR(200) NULL,

	PersonalUse BIT NOT NULL,
	YardMove BIT NOT NULL,

	LogDateUTC SMALLDATETIME NULL,

	CreateDateUTC SMALLDATETIME NULL,
	CreatedByUser VARCHAR(100) NULL,
	LastChangeDateUTC SMALLDATETIME NULL,
	LastChangedByUser VARCHAR(100) NULL,
	DeleteDateUTC SMALLDATETIME NULL,
	DeletedByUser VARCHAR(100) NULL
)

GO

/*****************************************
-- Date Created: 2016/06/16
-- Author: Joe Engler
-- Purpose: Audit history for HOS entries
*****************************************/
CREATE TRIGGER trigHos_IU ON tblHos AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	-- only do anything if something actually changed
	IF EXISTS (SELECT * FROM inserted EXCEPT SELECT * FROM deleted)
	BEGIN
		/* START DB AUDIT *********************************************************/
		BEGIN TRY
			IF EXISTS(SELECT * FROM tblSetting WHERE ID = 32 AND (Value LIKE 'true' OR Value LIKE 'yes'))
				INSERT INTO tblHosDbAudit (DBAuditDate, ID, UID, DriverID, HosDriverStatusID, HosStatusID, Lat, Lon, NearestCity, PersonalUse, YardMove, LogDateUTC
												, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser)
					SELECT GETUTCDATE(), ID, UID, DriverID, HosDriverStatusID, HosStatusID, Lat, Lon, NearestCity, PersonalUse, YardMove, LogDateUTC
												, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser
					FROM deleted d
		END TRY
		BEGIN CATCH
			PRINT 'trigHos_IU.DBAUDIT FAILURE: ' + ERROR_MESSAGE()
		END CATCH
		/* END DB AUDIT *********************************************************/
	END
END

GO


/********************************************
-- Date Created: 2016 Jun 23
-- Author: Joe Engler
-- Purpose: Get summary counts from HOS records
********************************************/
CREATE FUNCTION fnHosSummary(@DriverID INT, @StartDate DATETIME, @EndDate DATETIME) 
RETURNS TABLE AS RETURN
	WITH rows AS (
		-- Dummy start record, gets the previous status when the start date occurred
		SELECT 0 AS rownum,
			ISNULL((SELECT TOP 1 HosDriverStatusID FROM tblHos WHERE DriverID = @DriverID AND LogDateUTC < @StartDate ORDER BY LogDateUTC DESC), 1) AS HosDriverStatusID,
			@StartDate AS LogDateUTC

		UNION

		SELECT ROW_NUMBER() OVER (ORDER BY LogDateUTC) AS rownum,
			HosDriverStatusID, 
			LogDateUTC
		FROM tblHos
		WHERE DriverID = @DriverID
		AND LogDateUTC BETWEEN @StartDate AND @EndDate

		UNION

		-- Dummy end record, gets the final status when the end date occurred
		SELECT (select count(*)+1 FROM tblHos WHERE DriverID = @DriverID AND LogDateUTC BETWEEN @StartDate AND @EndDate) AS rownum,
			ISNULL((SELECT TOP 1 HosDriverStatusID FROM tblHos WHERE DriverID = @DriverID AND LogDateUTC <= @EndDate ORDER BY LogDateUTC DESC), 1), 
			@EndDate
	) 
	SELECT SUM(TotalHours) AS TotalHours, HosDriverStatusID FROM 
	(
		SELECT DATEDIFF(MINUTE, mc.LogDateUTC, mp.LogDateUTC)/60.0 AS TotalHours, mc.HosDriverStatusID
		FROM rows mc
		JOIN rows mp
		ON mc.rownum = mp.rownum-1
	) Q
	GROUP BY HosDriverStatusID
	
GO

INSERT INTO ASPNET_ROLES
select applicationid, NEWID(), 'eLogReports', LOWER('eLogReports'), 'Run Reports on HOS/eLogs' from aspnet_applications 
where not exists (select 1 from aspnet_roles where rolename = 'eLogReports')
GO

INSERT INTO ASPNET_ROLES
select applicationid, NEWID(), 'eLogManager', LOWER('eLogManager'), 'Manage HOS/eLogs' from aspnet_applications 
where not exists (select 1 from aspnet_roles where rolename = 'eLogManager')
GO

/* Add new permission to current Administrator users */
EXEC spAddUserPermissionsByRole 'Administrator', 'eLogReports'
GO
EXEC spAddUserPermissionsByRole 'Administrator', 'eLogReports'
GO


COMMIT
SET NOEXEC OFF