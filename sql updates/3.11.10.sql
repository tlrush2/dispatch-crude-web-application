-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.11.9.2'
SELECT  @NewVersion = '3.11.10'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1062 Added "Capacity" to Report Center'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

--------------------------------------------------------------
--------------------------------------------------------------

/***********************************/
-- Date Created: 22 Jan 2013
-- Author: Kevin Alons
-- Purpose: query the tblOrderTicket table and include "friendly" translated values
-- Changes: 3.11.9.1 - 04/04/2016 - BB - Add AltTankNum field
--		JS - 3.11.10 - 4/4/16 - Added Tank Description
/***********************************/
ALTER VIEW viewOrderTicket AS
SELECT OT.*
	, TicketType = TT.Name 
	, OriginTankText = CASE WHEN OT.OriginTankID IS NULL OR ORT.TankNum = '*' THEN OT.TankNum ELSE ORT.TankNum END 
	, IsStrappedTank = cast(CASE WHEN OT.OriginTankID IS NULL OR ORT.TankNum = '*' THEN 0 ELSE 1 END as bit) 
	, Active = cast((CASE WHEN OT.DeleteDateUTC IS NULL THEN 1 ELSE 0 END) as bit) 
	, IsDeleted = cast((CASE WHEN OT.DeleteDateUTC IS NOT NULL THEN 1 ELSE 0 END) as bit) 
	, Gravity60F = dbo.fnCorrectedAPIGravity(ProductObsGravity, ProductObsTemp)
	, RejectNum = OTRR.Num
	, RejectDesc = OTRR.Description
	, RejectNumDesc = OTRR.NumDesc
	, ORT.AltTankNum		-- 3.11.9.1
	, ORT.TankDescription
FROM tblOrderTicket OT
JOIN tblTicketType TT ON TT.ID = OT.TicketTypeID
LEFT JOIN tblOriginTank ORT ON ORT.ID = OT.OriginTankID
LEFT JOIN dbo.viewOrderTicketRejectReason OTRR ON OTRR.ID = OT.RejectReasonID

GO

--------------------------------------------------------------
--------------------------------------------------------------


/***********************************
Date Created: 9 Mar 2013
Author: Kevin Alons
Purpose: return Order JOIN OrderTicket + computed Reroute details into a single view
Special: 
	1) all TICKET fields should be preceded with T_ to ensure they are unique and to explicitly identify them as TICKET (not ORDER) level values
Changes:
	- 3.7.16 - 2015/05/22 - KDA - added T_UID field
	- 3.8.15 - 2015/08/15 -  BB - added T_TankID field
	- 3.9.25 - 2015/11/06 -  BB - DCWEB-921 - Added T_TicketTypeID field for filtering on ticket ticket type
	- 3.9.38 - 2015/12/21 - BB - Add WeightNetUnits, WeightGrossUnits, and WeightTareUnits (DCWEB-972)
	- 3.11.9.1 - 2016/04/04 - BB - Added AltTankNum field
	- 3.11.10 - 2016/04/04 - JS - Added tank description
***********************************/
ALTER VIEW [dbo].[viewOrder_OrderTicket_Full] AS 
	SELECT OE.* 
        , T_UID =  OT.UID
		, T_ID = OT.ID
		, T_TicketType = CASE WHEN OE.TicketCount = 0 THEN OE.TicketType ELSE OT.TicketType END 
		, T_TicketTypeID = OT.TicketTypeID
		, T_CarrierTicketNum = CASE WHEN OE.TicketCount = 0 THEN ltrim(OE.OrderNum) + CASE WHEN OE.Rejected = 1 THEN 'X' ELSE '' END ELSE OT.CarrierTicketNum END 
		, T_BOLNum = CASE WHEN OE.TicketCount = 0 THEN OE.OriginBOLNum ELSE OT.BOLNum END 
		, T_TankNum = isnull(OT.OriginTankText, OE.OriginTankText)
		, T_AltTankNum = OT.AltTankNum		-- 3.11.9.1
		, T_TankID = OT.OriginTankID
		, T_IsStrappedTank = OT.IsStrappedTank 
		, T_BottomFeet = OT.BottomFeet
		, T_BottomInches = OT.BottomInches
		, T_BottomQ = OT.BottomQ 
		, T_OpeningGaugeFeet = OT.OpeningGaugeFeet 
		, T_OpeningGaugeInch = OT.OpeningGaugeInch 
		, T_OpeningGaugeQ = OT.OpeningGaugeQ 
		, T_ClosingGaugeFeet = OT.ClosingGaugeFeet 
		, T_ClosingGaugeInch = OT.ClosingGaugeInch 
		, T_ClosingGaugeQ = OT.ClosingGaugeQ 
		, T_OpenTotalQ = dbo.fnGaugeQtrInches(OT.OpeningGaugeFeet, OT.OpeningGaugeInch, OT.OpeningGaugeQ) 
		, T_CloseTotalQ = dbo.fnGaugeQtrInches(OT.ClosingGaugeFeet, OT.ClosingGaugeInch, ot.ClosingGaugeQ) 
		, T_OpenReading = ltrim(OT.OpeningGaugeFeet) + 'ft ' + ltrim(OT.OpeningGaugeInch) + 'in ' + ltrim(OT.OpeningGaugeQ) + 'q' 
		, T_CloseReading = ltrim(OT.ClosingGaugeFeet) + 'ft ' + ltrim(OT.ClosingGaugeInch) + 'in ' + ltrim(OT.ClosingGaugeQ) + 'q' 
		, T_CorrectedAPIGravity = round(cast(OT.Gravity60F as decimal(9,4)), 9, 4) 
		, T_GrossStdUnits = OT.GrossStdUnits
		, T_SealOff = ltrim(OT.SealOff) 
		, T_SealOn = ltrim(OT.SealOn) 
		, T_HighTemp = OT.ProductHighTemp
		, T_LowTemp = OT.ProductLowTemp
		, T_ProductObsTemp = OT.ProductObsTemp 
		, T_ProductObsGravity = OT.ProductObsGravity 
		, T_ProductBSW = OT.ProductBSW 
		, T_Rejected = isnull(OT.Rejected, OE.Rejected)
		, T_RejectReasonID = OT.RejectReasonID
		, T_RejectNum = OT.RejectNum
		, T_RejectDesc = OT.RejectDesc
		, T_RejectNumDesc = OT.RejectNumDesc
		, T_RejectNotes = OT.RejectNotes 
		, T_GrossUnits = OT.GrossUnits 
		, T_NetUnits = OT.NetUnits 
		, T_MeterFactor = OT.MeterFactor
		, T_OpenMeterUnits = OT.OpenMeterUnits
		, T_CloseMeterUnits = OT.CloseMeterUnits
		, T_DispatchConfirmNum = CASE WHEN OE.TicketCount = 0 THEN OE.DispatchConfirmNum ELSE isnull(OT.DispatchConfirmNum, OE.DispatchConFirmNum) END
		, T_WeightTareUnits = OT.WeightTareUnits  -- 3.9.38
		, T_WeightGrossUnits = OT.WeightGrossUnits  -- 3.9.38
		, T_WeightNetUnits = OT.WeightNetUnits  -- 3.9.38
		, PreviousDestinations = dbo.fnRerouteDetails(OE.ID, 'PreviousDestinations', '<br/>') 
		, RerouteUsers = dbo.fnRerouteDetails(OE.ID, 'RerouteUsers', '<br/>') 
		, RerouteDates = dbo.fnRerouteDetails(OE.ID, 'RerouteDates', '<br/>') 
		, RerouteNotes = dbo.fnRerouteDetails(OE.ID, 'RerouteNotes', '<br/>')
        , T_TankDescription = OT.TankDescription
	FROM dbo.viewOrderLocalDates OE
	LEFT JOIN viewOrderTicket OT ON OT.OrderID = OE.ID AND OT.DeleteDateUTC IS NULL
	WHERE OE.DeleteDateUTC IS NULL

GO

--------------------------------------------------------------
--------------------------------------------------------------
SET IDENTITY_INSERT tblReportColumnDefinition ON
INSERT INTO tblReportColumnDefinition 
(
	   [ID]
	  ,[ReportID]
      ,[DataField]
      ,[Caption]
      ,[DataFormat]
      ,[FilterDataField]
      ,[FilterTypeID]
      ,[FilterDropDownSql]
      ,[FilterAllowCustomText]
      ,[AllowedRoles]
      ,[OrderSingleExport]
)
VALUES
(
	   283,
	   1,
	   'T_TankDescription',
	   'Ticket | GENERAL | Ticket Tank Capacity',
	   null,
	   null,
	   1,
	   null,
	   1,
	   '*',
	   0
)
SET IDENTITY_INSERT tblReportColumnDefinition OFF
--------------------------------------------------------------
--------------------------------------------------------------

EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO

COMMIT
SET NOEXEC OFF