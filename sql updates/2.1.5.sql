/* fix bug in ND10A report data source
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.1.4', @NewVersion = '2.1.5'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 20 Mar 2013
-- Description:	return the data for the TAX_ND_10A report (for the specified date range)
-- =============================================
ALTER PROCEDURE [dbo].[spReport_Tax_ND10A]
(
  @CustomerID int
, @StartDate datetime
, @EndDate datetime
, @IncludeUninvoiced bit = 0
)
AS BEGIN
	SELECT @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	
	SELECT O.FieldName
		, O.Operator
		, O.LeaseNum AS LeaseNumber
		, O.Name AS [Well Name and Number]
		, O.CTBNum AS [NDIC CTB No.]
		, cast(round(SUM(OD.OriginNetBarrels), 0) as int) AS [Lease Total (Bbls)]
	FROM viewOrigin O
	JOIN viewOrder OD ON OD.OriginID = O.ID
	JOIN tblOrderInvoiceCustomer IC ON IC.OrderID = OD.ID AND (@IncludeUninvoiced = 1 OR IC.BatchID IS NOT NULL)
	WHERE O.OriginTypeID IN (2) -- WELLs ONLY
		AND OD.DeleteDateUTC IS NULL
		AND (@CustomerID = -1 OR OD.CustomerID = @CustomerID)
		AND OD.OrderDate BETWEEN @StartDate AND @EndDate
	GROUP BY O.FieldName, O.Operator, O.LeaseNum, O.Name, O.CTBNum
	ORDER BY O.FieldName, O.Operator, O.LeaseNum, O.Name, O.CTBNum
	END

GO

COMMIT
SET NOEXEC OFF