/*  
	-- add viewOrder.OriginLegalDescription
*/ 
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.6.20'
SELECT  @NewVersion = '2.6.21'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

/***********************************/
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Order records with "translated friendly" values for FK relationships
/***********************************/
ALTER VIEW [dbo].[viewOrder] AS
SELECT O.*
	, OPS.OrderStatus AS PrintStatus
FROM (
	SELECT O.*
	, dbo.fnDateOnly(dbo.fnUTC_to_Local(O.OriginDepartTimeUTC, vO.TimeZoneID, vO.UseDST)) AS OrderDate
	, vO.Name AS Origin
	, vO.FullName AS OriginFull
	, vO.State AS OriginState
	, vO.StateAbbrev AS OriginStateAbbrev
	, vO.Station AS OriginStation
	, vO.LeaseName
	, vO.LeaseNum
	, vO.LegalDescription AS OriginLegalDescription
	, vO.NDICFileNum AS OriginNDIC
	, vO.NDM AS OriginNDM
	, vO.CA AS OriginCA
	, vO.TimeZoneID AS OriginTimeZoneID
	, vO.UseDST AS OriginUseDST
	, vO.H2S
	, vD.Name AS Destination
	, vD.FullName AS DestinationFull
	, vD.State AS DestinationState
	, vD.StateAbbrev AS DestinationStateAbbrev
	, vD.DestinationType
	, vD.Station AS DestStation
	, vD.TimeZoneID AS DestTimeZoneID
	, vD.UseDST AS DestUseDST
	, C.Name AS Customer
	, CA.Name AS Carrier
	, CT.Name AS CarrierType
	, OS.OrderStatus
	, OS.StatusNum
	, D.FullName AS Driver
	, D.FirstName AS DriverFirst
	, D.LastName AS DriverLast
	, TRU.FullName AS Truck
	, TR1.FullName AS Trailer
	, TR2.FullName AS Trailer2
	, P.PriorityNum
	, TT.Name AS TicketType
	, vD.TicketTypeID AS DestTicketTypeID
	, vD.TicketType AS DestTicketType
	, OP.Name AS Operator
	, PR.Name AS Producer
	, PU.FullName AS Pumper
	, D.IDNumber AS DriverNumber
	, CA.IDNumber AS CarrierNumber
	, TRU.IDNumber AS TruckNumber
	, TR1.IDNumber AS TrailerNumber
	, TR2.IDNumber AS Trailer2Number
	, PRO.Name as Product
	, PRO.ShortName AS ProductShort
	, OUom.Name AS OriginUOM
	, OUom.Abbrev AS OriginUomShort
	, CASE WHEN ORT.TankNum = '*' THEN '*' + isnull(O.OriginTankNum, '?') ELSE ORT.TankNum END AS OriginTankID_Text
	, DUom.Name AS DestUOM
	, DUom.Abbrev AS DestUomShort
	, cast((CASE WHEN O.DeleteDateUTC IS NULL THEN 1 ELSE 0 END) as bit) AS Active
	, cast((CASE WHEN O.DeleteDateUTC IS NOT NULL THEN 1 ELSE 0 END) as bit) AS IsDeleted
	, PPS.Name AS PickupPrintStatus
	, PPS.IsCompleted AS PickupCompleted
	, DPS.Name AS DeliverPrintStatus
	, DPS.IsCompleted AS DeliverCompleted
	, CASE WHEN StatusID = 8 AND PPS.IsCompleted = 0 THEN 7
		   WHEN StatusID = 3 AND DPS.IsCompleted = 0 THEN 8
		   ELSE StatusID END AS PrintStatusID
	FROM dbo.tblOrder O
	LEFT JOIN dbo.viewOrigin vO ON vO.ID = O.OriginID
	LEFT JOIN dbo.viewDestination vD ON vD.ID = O.DestinationID
	LEFT JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
	JOIN dbo.tblOrderStatus AS OS ON OS.ID = O.StatusID
	LEFT JOIN dbo.tblCarrier CA ON CA.ID = O.CarrierID
	LEFT JOIN dbo.tblCarrierType CT ON CT.ID = CA.CarrierTypeID
	LEFT JOIN dbo.viewDriver D ON D.ID = O.DriverID
	LEFT JOIN dbo.viewTruck TRU ON TRU.ID = O.TruckID
	LEFT JOIN dbo.viewTrailer TR1 ON TR1.ID = O.TrailerID
	LEFT JOIN dbo.viewTrailer TR2 ON TR2.ID = O.Trailer2ID
	LEFT JOIN dbo.tblPriority P ON P.ID = O.PriorityID
	LEFT JOIN dbo.tblTicketType TT ON TT.ID = O.TicketTypeID
	LEFT JOIN dbo.tblOriginTank ORT ON ORT.ID = O.OriginTankID
	LEFT JOIN dbo.tblOperator OP ON OP.ID = O.OperatorID
	LEFT JOIN dbo.viewPumper PU ON PU.ID = O.PumperID
	LEFT JOIN dbo.tblProducer PR ON PR.ID = O.ProducerID
	LEFT JOIN dbo.tblProduct PRO ON PRO.ID = O.ProductID
	LEFT JOIN dbo.tblUom OUom ON OUom.ID = O.OriginUomID
	LEFT JOIN dbo.tblUom DUom ON DUom.ID = O.DestUomID
	LEFT JOIN dbo.tblPrintStatus PPS ON PPS.ID = O.PickupPrintStatusID
	LEFT JOIN dbo.tblPrintStatus DPS ON DPS.ID = O.DeliverPrintStatusID
) O
LEFT JOIN dbo.tblOrderStatus OPS ON OPS.ID = O.PrintStatusID

GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 20 Mar 2013
-- Description:	return all CarrierTicketNums for an Order (separated by ",")
-- =============================================
ALTER VIEW [dbo].[viewOrder_AllTickets] AS 
WITH ActiveTickets AS
(
	SELECT ID, OrderID, CarrierTicketNum FROM tblOrderTicket WHERE DeleteDateUTC IS NULL
)
, Tickets AS 
( 
	--initialization 
	SELECT OT.OrderID, OT.ID, cast(CarrierTicketNum as varchar(255)) AS Tickets
	FROM ActiveTickets OT 
	JOIN (SELECT OrderID, MIN(ID) AS ID FROM tblOrderTicket GROUP BY OrderID) OTI 
		ON OTI.OrderID = OT.OrderID AND OTI.ID = OT.ID
	
	UNION ALL 
	--recursive execution 
	SELECT T.OrderID, OT.ID, cast(T.Tickets + ',' + OT.CarrierTicketNum as varchar(255)) AS Tickets
	FROM ActiveTickets OT JOIN Tickets T ON OT.ID > T.ID AND OT.OrderID = T.OrderID
)

SELECT O.*, T.Tickets
FROM viewOrderLocalDates O
LEFT JOIN (
	SELECT OrderID, max(Tickets) AS Tickets
	FROM Tickets T
	GROUP BY OrderID
) T ON T.OrderID = O.ID


GO

COMMIT
SET NOEXEC OFF

EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRecompileAllStoredProcedures
GO