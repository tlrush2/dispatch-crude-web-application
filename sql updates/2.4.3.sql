/* 
	add Carrier|Customer RouteRates.UomID (kept in sync Route.Origin.UomID value via triggers
	add associated triggers to keep all values in sync (and convert values when Uom changes)
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.4.2'
SELECT  @NewVersion = '2.4.3'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

REVOKE UPDATE ON tblRoute TO dispatchcrude_iis_acct
GO

ALTER TABLE tblCarrierRouteRates ADD UomID int not null 
	CONSTRAINT DF_CarrierRouteRates_UomID DEFAULT (1) 
	CONSTRAINT FK_CarrierRouteRates_Uom FOREIGN KEY REFERENCES tblUom(ID)
GO

ALTER TABLE tblCustomerRouteRates ADD UomID int not null 
	CONSTRAINT DF_CustomerRouteRates_UomID DEFAULT (1) 
	CONSTRAINT FK_CustomerRouteRates_Uom FOREIGN KEY REFERENCES tblUom(ID)
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 19 Dec 2012
-- Description:	trigger to ensure the UomID for Carrier/Customer Route Rates matches that assigned to the Origin
-- =============================================
CREATE TRIGGER [dbo].[trigOrigin_IU] ON [dbo].[tblOrigin] AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	IF (UPDATE(UomID))
	BEGIN
		-- update matching CarrierRouteRates.UomID to match what is assigned to the new Origin
		UPDATE tblCarrierRouteRates 
		  SET UomID = i.UomID, LastChangeDateUTC = i.LastChangeDateUTC, LastChangedByUser = i.LastChangedByUser
		FROM tblCarrierRouteRates CRR
		JOIN tblRoute R ON R.ID = CRR.RouteID
		JOIN inserted i ON i.ID = R.OriginID
		JOIN deleted d ON d.ID = i.ID
		WHERE d.UomID <> i.UomID AND CRR.EffectiveDate <= dbo.fnDateOnly(GETDATE())

		-- update matching CarrierRoutRates.UomID to match what is assigned to the new Origin
		UPDATE tblCustomerRouteRates 
		  SET UomID = i.UomID, LastChangeDateUTC = i.LastChangeDateUTC, LastChangedByUser = i.LastChangedByUser
		FROM tblCustomerRouteRates CRR
		JOIN tblRoute R ON R.ID = CRR.RouteID
		JOIN inserted i ON i.ID = R.OriginID
		JOIN deleted d ON d.ID = i.ID
		WHERE d.UomID <> i.UomID AND CRR.EffectiveDate <= dbo.fnDateOnly(GETDATE())
	END
END

GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 19 Dec 2012
-- Description:	trigger to ensure the UomID for Carrier/Customer Route Rates matches that assigned to the Origin
-- =============================================
CREATE TRIGGER [dbo].[trigCarrierRouteRates_IU] ON [dbo].[tblCarrierRouteRates] AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	IF (UPDATE(UomID))
	BEGIN
		UPDATE tblCarrierRouteRates
		  SET Rate = dbo.fnConvertRateUOM(CRR.Rate, d.UomID, CRR.UomID)
		FROM tblCarrierRouteRates CRR
		JOIN deleted d ON d.ID = CRR.ID
		WHERE d.UomID <> CRR.UomID
		  AND d.Rate = CRR.Rate
	END
END

GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 6 Dec 2013
-- Description:	trigger to ensure the UomID for Customer/Customer Route Rates matches that assigned to the Origin
-- =============================================
CREATE TRIGGER [dbo].[trigCustomerRouteRates_IU] ON [dbo].[tblCustomerRouteRates] AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	IF (UPDATE(UomID))
	BEGIN
		UPDATE tblCarrierRouteRates
		  SET Rate = dbo.fnConvertRateUOM(CRR.Rate, d.UomID, CRR.UomID)
		FROM tblCustomerRouteRates CRR
		JOIN deleted d ON d.ID = CRR.ID
		WHERE d.UomID <> CRR.UomID
		  AND d.Rate = CRR.Rate
	END
END

GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: return Carrier route rates with computed Earliest & End Date values)
/***********************************/
ALTER VIEW [dbo].[viewCarrierRouteRatesBase] AS
SELECT CRR.*
	, R.OriginID, R.DestinationID
	, (SELECT MIN(dateadd(day, -1, EffectiveDate)) FROM tblCarrierRouteRates CRRN WHERE CRRN.CarrierID = CRR.CarrierID AND CRRN.RouteID = CRR.RouteID AND CRRN.EffectiveDate > CRR.EffectiveDate) AS EndDate
	, (SELECT MAX(dateadd(day, 1, EffectiveDate)) FROM tblCarrierRouteRates CRRP WHERE CRRP.CarrierID = CRR.CarrierID AND CRRP.RouteID = CRR.RouteID AND CRRP.EffectiveDate < CRR.EffectiveDate) AS EarliestEffectiveDate
	, R.ActualMiles
	, R.Active
FROM tblCarrierRouteRates CRR
JOIN viewRoute R ON R.ID = CRR.RouteID

GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: return Customer route rates with computed Earliest & End Date values)
/***********************************/
ALTER VIEW [dbo].[viewCustomerRouteRatesBase] AS
SELECT CRR.*
	, R.OriginID, R.DestinationID
	, (SELECT MIN(dateadd(day, -1, EffectiveDate)) FROM tblCustomerRouteRates CRRN WHERE CRRN.CustomerID = CRR.CustomerID AND CRRN.RouteID = CRR.RouteID AND CRRN.EffectiveDate > CRR.EffectiveDate) AS EndDate
	, (SELECT MAX(dateadd(day, 1, EffectiveDate)) FROM tblCustomerRouteRates CRRP WHERE CRRP.CustomerID = CRR.CustomerID AND CRRP.RouteID = CRR.RouteID AND CRRP.EffectiveDate < CRR.EffectiveDate) AS EarliestEffectiveDate
	, R.ActualMiles
	, R.Active
FROM tblCustomerRouteRates CRR
JOIN viewRoute R ON R.ID = CRR.RouteID

GO

/***********************************/
-- Date Created: 9 Jun 2013
-- Author: Kevin Alons
-- Purpose: return Carrier route rates with translated "friendly" values + "missing route rates", computed Earliest & End Date values)
/***********************************/
ALTER VIEW [dbo].[viewCarrierRouteRates] AS
SELECT RB.ID, R.ID AS RouteID
	, RB.UomID
	, isnull(RB.Rate, 0) AS Rate
	, isnull(RB.EffectiveDate, dbo.fnDateOnly(ORD.MinOpenOrderDate)) AS EffectiveDate
	, cast(CASE WHEN C.DeleteDateUTC IS NOT NULL OR RB.Active = 0 THEN 0 ELSE 1 END as bit) AS Active
	, isnull(ORD.OpenOrderCount, 0) as OpenOrderCount
	, isnull(R.ActualMiles, 0) AS ActualMiles
	, RB.CreateDateUTC, RB.CreatedByUser, RB.LastChangeDateUTC, RB.LastChangedByUser
	, RB.EndDate
	, RB.EarliestEffectiveDate
	, C.ID AS CarrierID, C.Name AS Carrier
	, R.OriginID, R.Origin, R.OriginFull
	, R.DestinationID , R.Destination, R.DestinationFull
	, CASE WHEN R.ID IS NULL THEN 'Route Missing' WHEN RB.ID IS NULL THEN 'Missing' WHEN C.Active = 0 THEN 'Carrier Deleted' WHEN RB.Active = 0 THEN 'Route Inactive' ELSE 'Active' END AS Status
	, cast(CASE WHEN R.ID IS NULL OR isnull(ORD.OpenOrderCount, 0) = 0 THEN 0 ELSE 1 END as bit) AS RouteInUse
	, U.Name AS UOM
	, U.Abbrev AS UomShort
FROM (viewCarrier C CROSS JOIN viewRouteWithMissing R)
LEFT JOIN viewCarrierRouteRatesBase RB ON RB.CarrierID = C.ID AND RB.OriginID = R.OriginID AND RB.DestinationID = R.DestinationID
LEFT JOIN (SELECT CarrierID, RouteID, count(1) AS OpenOrderCount, min(OrderDate) AS MinOpenOrderDate FROM viewOrder WHERE StatusID IN (4) GROUP BY CarrierID, RouteID) ORD ON ORD.CarrierID = C.ID AND ORD.RouteID = R.ID
LEFT JOIN tblOrigin O ON O.ID = R.OriginID
LEFT JOIN tblUom U ON U.ID = isnull(RB.UomID, O.UomID)

GO

/***********************************/
-- Date Created: 9 Jun 2013
-- Author: Kevin Alons
-- Purpose: return Customer route rates with translated "friendly" values + "missing route rates", computed Earliest & End Date values)
/***********************************/
ALTER VIEW [dbo].[viewCustomerRouteRates] AS
SELECT RB.ID, R.ID AS RouteID
	, isnull(RB.Rate, 0) AS Rate
	, isnull(RB.EffectiveDate, dbo.fnDateOnly(ORD.MinOpenOrderDate)) AS EffectiveDate
	, cast(CASE WHEN RB.Active = 0 THEN 0 ELSE 1 END as bit) AS Active
	, isnull(ORD.OpenOrderCount, 0) as OpenOrderCount
	, isnull(R.ActualMiles, 0) AS ActualMiles
	, RB.CreateDateUTC, RB.CreatedByUser, RB.LastChangeDateUTC, RB.LastChangedByUser
	, RB.EndDate
	, RB.EarliestEffectiveDate
	, C.ID AS CustomerID, C.Name AS Customer
	, R.OriginID, R.Origin, R.OriginFull
	, R.DestinationID , R.Destination, R.DestinationFull
	, CASE WHEN R.ID IS NULL THEN 'Route Missing' WHEN RB.ID IS NULL THEN 'Missing' WHEN RB.Active = 0 THEN 'Route Inactive' ELSE 'Active' END AS Status
	, cast(CASE WHEN R.ID IS NULL OR isnull(ORD.OpenOrderCount, 0) = 0 THEN 0 ELSE 1 END as bit) AS RouteInUse
	, U.Name AS UOM
	, U.Abbrev AS UomShort
FROM (viewCustomer C CROSS JOIN viewRouteWithMissing R)
LEFT JOIN viewCustomerRouteRatesBase RB ON RB.CustomerID = C.ID AND RB.OriginID = R.OriginID AND RB.DestinationID = R.DestinationID
LEFT JOIN (SELECT CustomerID, RouteID, count(1) AS OpenOrderCount, min(OrderDate) AS MinOpenOrderDate FROM viewOrder WHERE StatusID IN (4) GROUP BY CustomerID, RouteID) ORD ON ORD.CustomerID = C.ID AND ORD.RouteID = R.ID
LEFT JOIN tblOrigin O ON O.ID = R.OriginID
LEFT JOIN tblUom U ON U.ID = isnull(RB.UomID, O.UomID)

GO

COMMIT
SET NOEXEC OFF
