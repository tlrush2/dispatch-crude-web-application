/* 
	make tblOrder.PickupPrintStatusID | DeliverPrintStatusID be NOT NULL
	better tblOrder trigger logic to ensure the above values get reset to 0 when reverting the StatusID value
		and also ensure the LastChangeDateUTC & LastChangedByUser values get updated when changes are made
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.6.3'
SELECT  @NewVersion = '2.6.4'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

UPDATE tblOrder SET PickupPrintStatusID = 0 WHERE PickupPrintStatusID IS NULL
UPDATE tblOrder SET DeliverPrintStatusID = 0 WHERE DeliverPrintStatusID IS NULL
GO

ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_Pickup_PrintStatus
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_Deliver_PrintStatus
GO
ALTER TABLE dbo.tblPrintStatus SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_Operator
GO
ALTER TABLE dbo.tblOperator SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_Customer
GO
ALTER TABLE dbo.tblCustomer SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_tblOrder_tblProducer
GO
ALTER TABLE dbo.tblProducer SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_Carrier
GO
ALTER TABLE dbo.tblCarrier SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_Destination
GO
ALTER TABLE dbo.tblDestination SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_TicketType
GO
ALTER TABLE dbo.tblTicketType SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_Route
GO
ALTER TABLE dbo.tblRoute SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_Driver
GO
ALTER TABLE dbo.tblDriver SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_Pumper
GO
ALTER TABLE dbo.tblPumper SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_Priority
GO
ALTER TABLE dbo.tblPriority SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_OrderStatus
GO
ALTER TABLE dbo.tblOrderStatus SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_OriginUom
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_DestUom
GO
ALTER TABLE dbo.tblUom SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_Truck
GO
ALTER TABLE dbo.tblTruck SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_Trailer
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_Trailer2
GO
ALTER TABLE dbo.tblTrailer SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_Origin
GO
ALTER TABLE dbo.tblOrigin SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT DF_tblOrders_StatusID
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT DF_tblOrder__TicketTicketType
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT DF_tblOrder_Rejected
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT DF_tblTransactions_ChainUp
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT DF_Order_CreateDateUTC
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT DF_Order_Product
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT DF_Order_OriginUomID
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT DF_Order_DestUomID
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT DF_Order_PickupPrintStatus
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT DF_Order_DeliverPrintStatus
GO
CREATE TABLE dbo.Tmp_tblOrder
	(
	ID int NOT NULL IDENTITY (1, 1),
	OrderNum int NULL,
	StatusID int NOT NULL,
	PriorityID tinyint NULL,
	DueDate date NOT NULL,
	RouteID int NULL,
	OriginID int NULL,
	OriginArriveTimeUTC datetime NULL,
	OriginDepartTimeUTC datetime NULL,
	OriginMinutes int NULL,
	OriginWaitNotes varchar(255) NULL,
	OriginBOLNum varchar(15) NULL,
	OriginGrossUnits decimal(9, 3) NULL,
	OriginNetUnits decimal(9, 3) NULL,
	DestinationID int NULL,
	DestArriveTimeUTC datetime NULL,
	DestDepartTimeUTC datetime NULL,
	DestMinutes int NULL,
	DestWaitNotes varchar(255) NULL,
	DestBOLNum varchar(30) NULL,
	DestGrossUnits decimal(9, 3) NULL,
	DestNetUnits decimal(9, 3) NULL,
	CustomerID int NULL,
	CarrierID int NULL,
	DriverID int NULL,
	TruckID int NULL,
	TrailerID int NULL,
	Trailer2ID int NULL,
	OperatorID int NULL,
	PumperID int NULL,
	TicketTypeID int NOT NULL,
	Rejected bit NOT NULL,
	RejectNotes varchar(255) NULL,
	ChainUp bit NOT NULL,
	OriginTruckMileage int NULL,
	OriginTankNum varchar(20) NULL,
	DestTruckMileage int NULL,
	CarrierTicketNum varchar(15) NULL,
	AuditNotes varchar(255) NULL,
	CreateDateUTC smalldatetime NULL,
	ActualMiles int NULL,
	ProducerID int NULL,
	CreatedByUser varchar(100) NULL,
	LastChangeDateUTC smalldatetime NULL,
	LastChangedByUser varchar(100) NULL,
	DeleteDateUTC smalldatetime NULL,
	DeletedByUser varchar(100) NULL,
	DestProductBSW decimal(9, 3) NULL,
	DestProductGravity decimal(9, 3) NULL,
	DestProductTemp decimal(9, 3) NULL,
	DestOpenMeterUnits decimal(9, 3) NULL,
	DestCloseMeterUnits decimal(9, 3) NULL,
	ProductID int NOT NULL,
	AcceptLastChangeDateUTC smalldatetime NULL,
	PickupLastChangeDateUTC smalldatetime NULL,
	DeliverLastChangeDateUTC smalldatetime NULL,
	OriginUomID int NOT NULL,
	DestUomID int NOT NULL,
	PickupPrintStatusID int NOT NULL,
	DeliverPrintStatusID int NOT NULL,
	PickupPrintDateUTC datetime NULL,
	DeliverPrintDateUTC datetime NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_tblOrder SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_tblOrder ADD CONSTRAINT
	DF_tblOrders_StatusID DEFAULT ((1)) FOR StatusID
GO
ALTER TABLE dbo.Tmp_tblOrder ADD CONSTRAINT
	DF_tblOrder__TicketTicketType DEFAULT ((1)) FOR TicketTypeID
GO
ALTER TABLE dbo.Tmp_tblOrder ADD CONSTRAINT
	DF_tblOrder_Rejected DEFAULT ((0)) FOR Rejected
GO
ALTER TABLE dbo.Tmp_tblOrder ADD CONSTRAINT
	DF_tblTransactions_ChainUp DEFAULT ((0)) FOR ChainUp
GO
ALTER TABLE dbo.Tmp_tblOrder ADD CONSTRAINT
	DF_Order_CreateDateUTC DEFAULT (getutcdate()) FOR CreateDateUTC
GO
ALTER TABLE dbo.Tmp_tblOrder ADD CONSTRAINT
	DF_Order_Product DEFAULT ((1)) FOR ProductID
GO
ALTER TABLE dbo.Tmp_tblOrder ADD CONSTRAINT
	DF_Order_OriginUomID DEFAULT ((1)) FOR OriginUomID
GO
ALTER TABLE dbo.Tmp_tblOrder ADD CONSTRAINT
	DF_Order_DestUomID DEFAULT ((1)) FOR DestUomID
GO
ALTER TABLE dbo.Tmp_tblOrder ADD CONSTRAINT
	DF_Order_PickupPrintStatus DEFAULT ((0)) FOR PickupPrintStatusID
GO
ALTER TABLE dbo.Tmp_tblOrder ADD CONSTRAINT
	DF_Order_DeliverPrintStatus DEFAULT ((0)) FOR DeliverPrintStatusID
GO
SET IDENTITY_INSERT dbo.Tmp_tblOrder ON
GO
IF EXISTS(SELECT * FROM dbo.tblOrder)
	 EXEC('INSERT INTO dbo.Tmp_tblOrder (ID, OrderNum, StatusID, PriorityID, DueDate, RouteID, OriginID, OriginArriveTimeUTC, OriginDepartTimeUTC, OriginMinutes, OriginWaitNotes, OriginBOLNum, OriginGrossUnits, OriginNetUnits, DestinationID, DestArriveTimeUTC, DestDepartTimeUTC, DestMinutes, DestWaitNotes, DestBOLNum, DestGrossUnits, DestNetUnits, CustomerID, CarrierID, DriverID, TruckID, TrailerID, Trailer2ID, OperatorID, PumperID, TicketTypeID, Rejected, RejectNotes, ChainUp, OriginTruckMileage, OriginTankNum, DestTruckMileage, CarrierTicketNum, AuditNotes, CreateDateUTC, ActualMiles, ProducerID, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser, DestProductBSW, DestProductGravity, DestProductTemp, DestOpenMeterUnits, DestCloseMeterUnits, ProductID, AcceptLastChangeDateUTC, PickupLastChangeDateUTC, DeliverLastChangeDateUTC, OriginUomID, DestUomID, PickupPrintStatusID, DeliverPrintStatusID, PickupPrintDateUTC, DeliverPrintDateUTC)
		SELECT ID, OrderNum, StatusID, PriorityID, DueDate, RouteID, OriginID, OriginArriveTimeUTC, OriginDepartTimeUTC, OriginMinutes, OriginWaitNotes, OriginBOLNum, OriginGrossUnits, OriginNetUnits, DestinationID, DestArriveTimeUTC, DestDepartTimeUTC, DestMinutes, DestWaitNotes, DestBOLNum, DestGrossUnits, DestNetUnits, CustomerID, CarrierID, DriverID, TruckID, TrailerID, Trailer2ID, OperatorID, PumperID, TicketTypeID, Rejected, RejectNotes, ChainUp, OriginTruckMileage, OriginTankNum, DestTruckMileage, CarrierTicketNum, AuditNotes, CreateDateUTC, ActualMiles, ProducerID, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser, DestProductBSW, DestProductGravity, DestProductTemp, DestOpenMeterUnits, DestCloseMeterUnits, ProductID, AcceptLastChangeDateUTC, PickupLastChangeDateUTC, DeliverLastChangeDateUTC, OriginUomID, DestUomID, PickupPrintStatusID, DeliverPrintStatusID, PickupPrintDateUTC, DeliverPrintDateUTC FROM dbo.tblOrder WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_tblOrder OFF
GO
ALTER TABLE dbo.tblOrderInvoiceCarrier
	DROP CONSTRAINT FK_tblOrderInvoiceCarrier_tblOrder
GO
ALTER TABLE dbo.tblOrderInvoiceCustomer
	DROP CONSTRAINT FK_tblOrderInvoiceCustomer_tblOrder
GO
ALTER TABLE dbo.tblOrderTicket
	DROP CONSTRAINT FK_OrderTicket_Order
GO
ALTER TABLE dbo.tblOrderReroute
	DROP CONSTRAINT FK_OrderReroute_Order
GO
ALTER TABLE dbo.tblOrderDriverAppVirtualDelete
	DROP CONSTRAINT FK_OrderDriverAppVirtualDelete_Order
GO
ALTER TABLE dbo.tblOrderPrintLog
	DROP CONSTRAINT FK_OrderPrintLog_Order
GO
DROP TABLE dbo.tblOrder
GO
EXECUTE sp_rename N'dbo.Tmp_tblOrder', N'tblOrder', 'OBJECT' 
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	PK_Order PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE UNIQUE NONCLUSTERED INDEX udxOrder_OrderNum ON dbo.tblOrder
	(
	OrderNum
	) INCLUDE (ID) 
 WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxOrder_StatusID ON dbo.tblOrder
	(
	StatusID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxOrder_PriorityID ON dbo.tblOrder
	(
	PriorityID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxOrder_CarrierID ON dbo.tblOrder
	(
	CarrierID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxOrder_OriginID ON dbo.tblOrder
	(
	OriginID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxOrder_DestinationID ON dbo.tblOrder
	(
	DestinationID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxOrder_RouteID ON dbo.tblOrder
	(
	RouteID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxOrder_OriginDepartTime ON dbo.tblOrder
	(
	OriginDepartTimeUTC
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxOrder_DeleteDate_StatusID_Covering ON dbo.tblOrder
	(
	DeleteDateUTC,
	StatusID
	) INCLUDE (ID, OrderNum, PriorityID, DueDate, RouteID, OriginID, OriginArriveTimeUTC, OriginDepartTimeUTC, OriginMinutes, OriginWaitNotes, OriginBOLNum, OriginGrossUnits, OriginNetUnits, DestinationID, DestArriveTimeUTC, DestDepartTimeUTC, DestMinutes, DestWaitNotes, DestBOLNum, DestGrossUnits, DestNetUnits, CustomerID, CarrierID, DriverID, TruckID, TrailerID, Trailer2ID, OperatorID, PumperID, TicketTypeID, Rejected, RejectNotes, ChainUp, OriginTruckMileage, OriginTankNum, DestTruckMileage, CarrierTicketNum, AuditNotes, CreateDateUTC, ActualMiles, ProducerID, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeletedByUser, DestProductBSW, DestProductGravity, DestProductTemp) 
 WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxOrder_DriverID ON dbo.tblOrder
	(
	DriverID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_Origin FOREIGN KEY
	(
	OriginID
	) REFERENCES dbo.tblOrigin
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_Trailer FOREIGN KEY
	(
	TrailerID
	) REFERENCES dbo.tblTrailer
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  SET NULL 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_Trailer2 FOREIGN KEY
	(
	Trailer2ID
	) REFERENCES dbo.tblTrailer
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_Truck FOREIGN KEY
	(
	TruckID
	) REFERENCES dbo.tblTruck
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  SET NULL 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_OriginUom FOREIGN KEY
	(
	OriginUomID
	) REFERENCES dbo.tblUom
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_DestUom FOREIGN KEY
	(
	DestUomID
	) REFERENCES dbo.tblUom
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_OrderStatus FOREIGN KEY
	(
	StatusID
	) REFERENCES dbo.tblOrderStatus
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_Priority FOREIGN KEY
	(
	PriorityID
	) REFERENCES dbo.tblPriority
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  SET NULL 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_Pumper FOREIGN KEY
	(
	PumperID
	) REFERENCES dbo.tblPumper
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  SET NULL 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_Driver FOREIGN KEY
	(
	DriverID
	) REFERENCES dbo.tblDriver
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_Route FOREIGN KEY
	(
	RouteID
	) REFERENCES dbo.tblRoute
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_TicketType FOREIGN KEY
	(
	TicketTypeID
	) REFERENCES dbo.tblTicketType
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_Destination FOREIGN KEY
	(
	DestinationID
	) REFERENCES dbo.tblDestination
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_Carrier FOREIGN KEY
	(
	CarrierID
	) REFERENCES dbo.tblCarrier
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_tblOrder_tblProducer FOREIGN KEY
	(
	ProducerID
	) REFERENCES dbo.tblProducer
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_Customer FOREIGN KEY
	(
	CustomerID
	) REFERENCES dbo.tblCustomer
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_Operator FOREIGN KEY
	(
	OperatorID
	) REFERENCES dbo.tblOperator
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  SET NULL 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_Pickup_PrintStatus FOREIGN KEY
	(
	PickupPrintStatusID
	) REFERENCES dbo.tblPrintStatus
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_Deliver_PrintStatus FOREIGN KEY
	(
	DeliverPrintStatusID
	) REFERENCES dbo.tblPrintStatus
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
/**********************************************************/
-- Author: Kevin Alons
-- Date Created: 26 Sep 2013
-- Purpose: track changes to Orders that cause it be "virtually" deleted for a Driver (for DriveApp.Sync purposes)
/**********************************************************/
CREATE TRIGGER [dbo].[trigOrder_U_VirtualDelete] ON dbo.tblOrder AFTER UPDATE AS
BEGIN
	DECLARE @NewRecords TABLE (
		  OrderID int NOT NULL
		, DriverID int NOT NULL
		, LastChangeDateUTC smalldatetime NOT NULL
		, LastChangedByUser varchar(100) NULL
	)

	-- delete any records that no longer apply because now the record is valid for the new driver/status
	DELETE FROM tblOrderDriverAppVirtualDelete
	FROM tblOrderDriverAppVirtualDelete ODAVD
	JOIN inserted i ON i.ID = ODAVD.OrderID AND i.DriverID = ODAVD.DriverID
	WHERE i.DeleteDateUTC IS NULL 
		AND dbo.fnPrintStatusID(i.StatusID, i.PickupPrintStatusID, i.DeliverPrintStatusID) IN (2,7,8)

	INSERT INTO @NewRecords
		-- insert any applicable VIRTUALDELETE records where the status changed from 
		--   a valid status to an invalid [DriverApp] status
		SELECT d.ID, d.DriverID, GETUTCDATE(), ISNULL(i.LastChangedByUser, 'System')
		FROM deleted d
		JOIN inserted i ON i.ID = D.ID
		WHERE dbo.fnPrintStatusID(i.StatusID, i.PickupPrintStatusID, i.DeliverPrintStatusID) NOT IN (2,7,8)
		  AND dbo.fnPrintStatusID(d.StatusID, d.PickupPrintStatusID, d.DeliverPrintStatusID) IN (2,7,8)

		UNION

		-- insert any records where the DriverID changed to another DriverID (for a valid status)
		SELECT d.ID, d.DriverID, GETUTCDATE(), ISNULL(i.LastChangedByUser, 'System')
		FROM deleted d
		JOIN inserted i ON i.ID = D.ID
		WHERE d.DriverID <> i.DriverID 
		  AND dbo.fnPrintStatusID(d.StatusID, d.PickupPrintStatusID, d.DeliverPrintStatusID) IN (2,7,8)

	-- update the VirtualDeleteDateUTC value for any existing records
	UPDATE tblOrderDriverAppVirtualDelete
	  SET VirtualDeleteDateUTC = NR.LastChangeDateUTC, VirtualDeletedByUser = NR.LastChangedByUser
	FROM tblOrderDriverAppVirtualDelete ODAVD
	JOIN @NewRecords NR ON NR.OrderID = ODAVD.OrderID AND NR.DriverID = ODAVD.DriverID

	-- insert the truly new VirtualDelete records	
	INSERT INTO tblOrderDriverAppVirtualDelete (OrderID, DriverID, VirtualDeleteDateUTC, VirtualDeletedByUser)
		SELECT NR.OrderID, NR.DriverID, NR.LastChangeDateUTC, NR.LastChangedByUser
		FROM @NewRecords NR
		LEFT JOIN tblOrderDriverAppVirtualDelete ODAVD ON ODAVD.OrderID = NR.OrderID AND ODAVD.DriverID = NR.DriverID
		WHERE ODAVD.ID IS NULL

END
GO
-- =============================================
-- Author:		Kevin Alons
-- Create date: 13 Dec 2012
-- Description:	ensure that only "Assigned|Dispatched|Declined|Generated" orders can be deleted (marked deleted)
-- =============================================
CREATE TRIGGER [dbo].[trigOrder_IOD] ON dbo.tblOrder INSTEAD OF DELETE AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	IF (SELECT count(*) FROM deleted d where StatusID NOT IN (-10, 1, 2, 9)) > 0
	BEGIN
		RAISERROR('Order could not be deleted - it has a status other than "Generated, Assigned, Dispatched or Declined"', 16, 1)
		RETURN
	END
	ELSE
		DELETE FROM tblOrderTicket WHERE OrderID IN (SELECT ID FROM deleted)
		DELETE FROM tblOrder WHERE ID IN (SELECT ID FROM deleted)
END
GO
-- =============================================
-- Author:		Kevin Alons
-- Create date: 19 Dec 2012
-- Description:	trigger to add a unique, incrementing OrderNum to each new Order (manual Identity column)
--				and other supporting/coordinating logic
-- =============================================
CREATE TRIGGER [dbo].[trigOrder_IU] ON dbo.tblOrder AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	WHILE (SELECT count(*) FROM inserted i JOIN tblOrder O ON O.ID = i.ID WHERE O.OrderNum IS NULL) > 0
	BEGIN
		UPDATE tblOrder 
		  SET OrderNum = isnull((SELECT max(OrderNum) FROM tblOrder), 100000) + 1
			, CreateDateUTC = getutcdate()
		WHERE ID = (SELECT min(O.ID) FROM tblOrder O JOIN inserted i ON i.ID = O.ID WHERE O.OrderNum IS NULL)
	END
	
	-- re-compute the OriginMinutes (in case the website failed to compute it properly)
	IF UPDATE(OriginArriveTimeUTC) OR UPDATE(OriginDepartTimeUTC)
	BEGIN
		UPDATE tblOrder SET OriginMinutes = datediff(minute, OriginArriveTimeUTC, OriginDepartTimeUTC)
	END
	-- re-compute the DestWaitMinutes (in case the website failed to compute it properly)
	IF UPDATE(DestArriveTimeUTC) OR UPDATE(DestDepartTimeUTC)
	BEGIN
		UPDATE tblOrder SET DestMinutes = datediff(minute, DestArriveTimeUTC, DestDepartTimeUTC)
	END
	
	-- ensure missing Routes are created and assigned for the specified Origin/Destination combination
	IF UPDATE(OriginID) OR UPDATE(DestinationID)
	BEGIN
		-- create any missing Route records
		INSERT INTO tblRoute (OriginID, DestinationID, CreateDateUTC, CreatedByUser)
			SELECT i.OriginID, i.DestinationID, GETUTCDATE(), ISNULL(i.LastChangedByUser, i.CreatedByUser)
			FROM inserted i
			LEFT JOIN tblRoute R ON R.OriginID = i.OriginID AND R.DestinationID = i.DestinationID
			WHERE i.OriginID IS NOT NULL AND i.DestinationID IS NOT NULL AND R.ID IS NULL
		-- ensure the Order records refer to the correct Route (ID)
		UPDATE tblOrder SET RouteID = R.ID
		FROM tblOrder O
		JOIN inserted i ON i.ID = O.ID
		LEFT JOIN tblRoute R ON R.OriginID = O.OriginID AND R.DestinationID = O.DestinationID

		-- update the ActualMiles from the related Route
		UPDATE tblOrder SET ActualMiles = R.ActualMiles
		FROM tblOrder O
		JOIN inserted i ON i.ID = O.ID
		JOIN tblRoute R ON R.ID = O.RouteID
	END
	
	IF (UPDATE(OriginID))
	BEGIN
		-- update Order.ProducerID to match what is assigned to the new Origin
		UPDATE tblOrder SET ProducerID = OO.ProducerID
		FROM tblOrder O
		JOIN deleted d ON d.ID = O.ID
		JOIN tblOrigin OO ON OO.ID = O.OriginID
		WHERE d.OriginID <> O.OriginID

		-- update Order.OriginUomID to match what is assigned to the new Origin (quantities are updated below)
		UPDATE tblOrder SET OriginUomID = OO.UomID
		FROM tblOrder O
		JOIN deleted d ON d.ID = O.ID
		JOIN tblOrigin OO ON OO.ID = O.OriginID
		WHERE d.OriginID <> O.OriginID AND d.OriginUomID <> OO.UomID
	END
	
	-- keep the DestUomID in sync with the Destination (units are updated below)
	IF (UPDATE(DestinationID))
	BEGIN
		-- update Order.DestUomID to match what is assigned to the new Destination
		UPDATE tblOrder 
		  SET DestUomID = DD.UomID
		FROM tblOrder O
		JOIN deleted d ON d.ID = O.ID
		JOIN tblDestination DD ON DD.ID = O.DestinationID
		WHERE d.DestinationID <> O.DestinationID AND d.DestUomID <> DD.UomID
	END
	
	-- just updating the tickets here, trigOrderTicket_IU will in turn update this table
	-- Note: the TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU') ) = 1 statement prevents recursion
	IF (TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU')) = 1)
	BEGIN
		-- ensure any existing OrderTickets are recomputed with the new OriginUOM
		UPDATE tblOrderTicket
		  SET GrossUnits = dbo.fnConvertUOM(GrossUnits, d.OriginUomID, i.OriginUomID)
			, NetUnits = dbo.fnConvertUOM(NetUnits, d.OriginUomID, i.OriginUomID)
		FROM tblOrderTicket OT
		JOIN tblOrder i ON i.ID = OT.OrderID
		JOIN deleted d ON d.ID = i.ID
		WHERE i.OriginUomID <> d.OriginUomID

	END
	
	-- recalculate the Destination Unit values if only if the DestUomID changed but the values didn't
	BEGIN
		UPDATE tblOrder
		  SET DestGrossUnits = dbo.fnConvertUOM(O.DestGrossUnits, d.DestUomID, O.DestUomID)
			, DestNetUnits = dbo.fnConvertUOM(O.DestNetUnits, d.DestUomID, O.DestUomID)
		FROM tblOrder O
		JOIN deleted d ON d.ID = O.ID
		WHERE d.DestUomID <> O.DestUomID 
		  AND d.DestGrossUnits = O.DestGrossUnits
		  AND d.DestNetUnits = O.DestNetUnits
	END
	--NOTE: we do not update the DestOpenMeterUnits/DestClosignMeterUnits since they don't auto update
	--  (and they simply represent a reading that is assumed to in the DestUomID UOM at entry)
	--  (if they are updated in the future [manually] the will calculate again using the DestUomID at that time)
END
GO
-- =============================================
-- Author:		Kevin Alons
-- Create date: 21 Jun 2013
-- Description:	trigger to ensure the entered values for an Order are actually valid
-- =============================================
CREATE TRIGGER [dbo].[trigOrder_IU_Validate] ON dbo.tblOrder AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	-- ensure the Origin and Destinations are both specified unless the Status is:
	--   (Generated, Assigned, Dispatched or Declined)
	IF (SELECT COUNT(1) 
		FROM inserted O
		WHERE (O.OriginID IS NULL OR O.DestinationID IS NULL) AND O.StatusID NOT IN (-10,1,2,9)) > 0
	BEGIN
		RAISERROR('Invalid Order Status when the Origin and/or Or Destination is not specified', 16, 1)
		RETURN
	END
	
END
GO
ALTER TABLE dbo.tblOrderPrintLog ADD CONSTRAINT
	FK_OrderPrintLog_Order FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.tblOrder
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
GO
ALTER TABLE dbo.tblOrderPrintLog SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderDriverAppVirtualDelete ADD CONSTRAINT
	FK_OrderDriverAppVirtualDelete_Order FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.tblOrder
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
GO
ALTER TABLE dbo.tblOrderDriverAppVirtualDelete SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderReroute ADD CONSTRAINT
	FK_OrderReroute_Order FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.tblOrder
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
GO
ALTER TABLE dbo.tblOrderReroute SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderTicket ADD CONSTRAINT
	FK_OrderTicket_Order FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.tblOrder
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderTicket SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderInvoiceCustomer ADD CONSTRAINT
	FK_tblOrderInvoiceCustomer_tblOrder FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.tblOrder
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
GO
ALTER TABLE dbo.tblOrderInvoiceCustomer SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderInvoiceCarrier ADD CONSTRAINT
	FK_tblOrderInvoiceCarrier_tblOrder FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.tblOrder
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
GO
ALTER TABLE dbo.tblOrderInvoiceCarrier SET (LOCK_ESCALATION = TABLE)
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 19 Dec 2012
-- Description:	trigger to add a unique, incrementing OrderNum to each new Order (manual Identity column)
--				and other supporting/coordinating logic
-- =============================================
ALTER TRIGGER [dbo].[trigOrder_IU] ON [dbo].[tblOrder] AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	WHILE (SELECT count(*) FROM inserted i JOIN tblOrder O ON O.ID = i.ID WHERE O.OrderNum IS NULL) > 0
	BEGIN
		UPDATE tblOrder 
		  SET OrderNum = isnull((SELECT max(OrderNum) FROM tblOrder), 100000) + 1
			, CreateDateUTC = getutcdate()
		WHERE ID = (SELECT min(O.ID) FROM tblOrder O JOIN inserted i ON i.ID = O.ID WHERE O.OrderNum IS NULL)
	END
	
	-- re-compute the OriginMinutes (in case the website failed to compute it properly)
	IF UPDATE(OriginArriveTimeUTC) OR UPDATE(OriginDepartTimeUTC)
	BEGIN
		UPDATE tblOrder SET OriginMinutes = datediff(minute, OriginArriveTimeUTC, OriginDepartTimeUTC)
	END
	-- re-compute the DestWaitMinutes (in case the website failed to compute it properly)
	IF UPDATE(DestArriveTimeUTC) OR UPDATE(DestDepartTimeUTC)
	BEGIN
		UPDATE tblOrder SET DestMinutes = datediff(minute, DestArriveTimeUTC, DestDepartTimeUTC)
	END
	
	-- ensure missing Routes are created and assigned for the specified Origin/Destination combination
	IF UPDATE(OriginID) OR UPDATE(DestinationID)
	BEGIN
		-- create any missing Route records
		INSERT INTO tblRoute (OriginID, DestinationID, CreateDateUTC, CreatedByUser)
			SELECT i.OriginID, i.DestinationID, GETUTCDATE(), ISNULL(i.LastChangedByUser, i.CreatedByUser)
			FROM inserted i
			LEFT JOIN tblRoute R ON R.OriginID = i.OriginID AND R.DestinationID = i.DestinationID
			WHERE i.OriginID IS NOT NULL AND i.DestinationID IS NOT NULL AND R.ID IS NULL
		-- ensure the Order records refer to the correct Route (ID)
		UPDATE tblOrder SET RouteID = R.ID
		FROM tblOrder O
		JOIN inserted i ON i.ID = O.ID
		LEFT JOIN tblRoute R ON R.OriginID = O.OriginID AND R.DestinationID = O.DestinationID

		-- update the ActualMiles from the related Route
		UPDATE tblOrder SET ActualMiles = R.ActualMiles
		FROM tblOrder O
		JOIN inserted i ON i.ID = O.ID
		JOIN tblRoute R ON R.ID = O.RouteID
	END
	
	IF (UPDATE(OriginID))
	BEGIN
		-- update Order.ProducerID to match what is assigned to the new Origin
		UPDATE tblOrder SET ProducerID = OO.ProducerID
		FROM tblOrder O
		JOIN deleted d ON d.ID = O.ID
		JOIN tblOrigin OO ON OO.ID = O.OriginID
		WHERE d.OriginID <> O.OriginID

		-- update Order.OriginUomID to match what is assigned to the new Origin (quantities are updated below)
		UPDATE tblOrder SET OriginUomID = OO.UomID
		FROM tblOrder O
		JOIN deleted d ON d.ID = O.ID
		JOIN tblOrigin OO ON OO.ID = O.OriginID
		WHERE d.OriginID <> O.OriginID AND d.OriginUomID <> OO.UomID
	END
	
	-- keep the DestUomID in sync with the Destination (units are updated below)
	IF (UPDATE(DestinationID))
	BEGIN
		-- update Order.DestUomID to match what is assigned to the new Destination
		UPDATE tblOrder 
		  SET DestUomID = DD.UomID
		FROM tblOrder O
		JOIN deleted d ON d.ID = O.ID
		JOIN tblDestination DD ON DD.ID = O.DestinationID
		WHERE d.DestinationID <> O.DestinationID AND d.DestUomID <> DD.UomID
	END
	
	-- ensure that any set PrintStatusID values are reset to 0 when backing up the general StatusID value
	IF (UPDATE(StatusID))
	BEGIN
		UPDATE tblOrder 
		  SET DeliverPrintStatusID = 0 
		  FROM tblOrder O
		  JOIN deleted d ON d.ID = O.ID
		WHERE O.StatusID <> d.StatusID AND O.StatusID IN (-10, 1, 2, 7, 8)

		UPDATE tblOrder 
		  SET PickupPrintStatusID = 0 
		  FROM tblOrder O
		  JOIN deleted d ON d.ID = O.ID
		WHERE O.StatusID <> d.StatusID AND O.StatusID IN (-10, 1, 2, 7)
	END
	
	-- just updating the tickets here, trigOrderTicket_IU will in turn update this table
	-- Note: the TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU') ) = 1 statement prevents recursion
	IF (TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU')) = 1)
	BEGIN
		-- ensure any existing OrderTickets are recomputed with the new OriginUOM
		UPDATE tblOrderTicket
		  SET GrossUnits = dbo.fnConvertUOM(GrossUnits, d.OriginUomID, i.OriginUomID)
			, NetUnits = dbo.fnConvertUOM(NetUnits, d.OriginUomID, i.OriginUomID)
		FROM tblOrderTicket OT
		JOIN tblOrder i ON i.ID = OT.OrderID
		JOIN deleted d ON d.ID = i.ID
		WHERE i.OriginUomID <> d.OriginUomID

	END
	
	-- recalculate the Destination Unit values if only if the DestUomID changed but the values didn't
	BEGIN
		UPDATE tblOrder
		  SET DestGrossUnits = dbo.fnConvertUOM(O.DestGrossUnits, d.DestUomID, O.DestUomID)
			, DestNetUnits = dbo.fnConvertUOM(O.DestNetUnits, d.DestUomID, O.DestUomID)
		FROM tblOrder O
		JOIN deleted d ON d.ID = O.ID
		WHERE d.DestUomID <> O.DestUomID 
		  AND d.DestGrossUnits = O.DestGrossUnits
		  AND d.DestNetUnits = O.DestNetUnits
	END
	--NOTE: we do not update the DestOpenMeterUnits/DestClosignMeterUnits since they don't auto update
	--  (and they simply represent a reading that is assumed to in the DestUomID UOM at entry)
	--  (if they are updated in the future [manually] the will calculate again using the DestUomID at that time)
	
	-- ensure that all changed orders have their LastChangeDateUTC set to UTCNOW
	UPDATE tblOrder
	  SET LastChangeDateUTC = GETUTCDATE()
		, LastChangedByUser = ISNULL(O.LastChangedByUser, 'System')
	FROM tblOrder O
	JOIN inserted i ON i.ID = O.ID
	WHERE O.LastChangeDateUTC IS NULL OR abs(datediff(second, O.LastChangeDateUTC, GETUTCDATE())) > 2
	
END

GO

COMMIT
SET NOEXEC OFF