SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.3.2'
SELECT  @NewVersion = '4.3.3'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Convert "Limit Single Order" system setting to a carrier rule'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

-- Add new carrier rule
INSERT INTO tblCarrierRuleType 
SELECT 22, 'Limit to single active order', 2, 1, 'Restrict mobile users to working on one order at a time.  If TRUE, all other orders will be reset to Dispatched status so the web users are aware of which order a driver is currently working on.'
WHERE NOT EXISTS (SELECT 1 FROM tblCarrierRuleType WHERE ID = 22 AND Name = 'Limit to single active order')

-- Add default carrier rule with old system setting value
INSERT INTO tblCarrierRule (TypeID, CarrierID, DriverID, StateID, RegionID, EffectiveDate, EndDate, Value, CreateDateUTC, CreatedByUser)
SELECT 22, null, null, null, null, CAST (GETDATE() AS DATE), CAST(DATEADD(YEAR, 1, GETDATE()) AS DATE), (SELECT Value FROM tblSetting WHERE ID = 43), GETUTCDATE(), 'System'
WHERE NOT EXISTS (SELECT 1 FROM tblCarrierRule WHERE TypeID = 22)

-- Add note to old system setting
UPDATE tblSetting 
SET Name = '**DEPRECATED** - ' + Name,
Description = 'PLEASE USE CARRIER RULE INSTEAD.  OLD MOBILE USERS MAY STILL SEE THIS SETTING.  Restrict mobile users to working on one order at a time.' 
WHERE ID = 43 AND Name = 'Limit to single active order'

GO

COMMIT
SET NOEXEC OFF
