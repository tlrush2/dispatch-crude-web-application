SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.0.5'
SELECT  @NewVersion = '4.0.5.1'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1681: Fix rounding error'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/***********************************************************/
--  Date Created: 22 June 2013
--  Author: Kevin Alons
--  Purpose: Round the Billable Wait minutes into a decimal hour value (based on rounding parameters)
--  Changes:
--		4.0.4.1		JAE		2016-08-25		Fix to formula that rounded wrong, initially divided by ID and not value so formula was using wrong interval
--		4.0.5.1		JAE		2016-08-25		Fix for when unit was none (round to nearest 1)
/***********************************************************/
ALTER FUNCTION [dbo].[fnComputeBillableWaitHours](@min int, @SubUnitID int, @RoundingTypeID int) RETURNS decimal (18, 6) AS
BEGIN
	DECLARE @ret decimal(18, 6)
	DECLARE @roundTo INT = 1

	SELECT @ret = @min
	
	IF (@SubUnitID <> 1 AND @RoundingTypeID <> 1)
	BEGIN
	    SELECT @roundTo = (SELECT TOP 1 cast(Name as int) FROM tblWaitFeeSubUnit WHERE ID = @SubUnitID)
		SELECT @ret = @ret / @roundTo
		IF (@RoundingTypeID = 2)
			SELECT @ret = floor(@ret)
		ELSE IF (@RoundingTypeID = 3)
			SELECT @ret = round(@ret - 0.01, 0)
		ELSE IF (@RoundingTypeID = 4)
			SELECT @ret = round(@ret, 0)
		ELSE IF (@RoundingTypeID = 5)
			SELECT @ret = ceiling(@ret)
		ELSE
			SELECT @ret = @min
	END
	
	RETURN @ret * @roundTo/60.0
END

GO


COMMIT
SET NOEXEC OFF