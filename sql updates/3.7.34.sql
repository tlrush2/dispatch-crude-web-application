-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.7.33.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.7.33.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0
SET NOEXEC OFF
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.7.33'
SELECT  @NewVersion = '3.7.34'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'Settlement Center - change XXX[Origin|Destination}MaxBillableMinutes to limit Billable minutes instead of Total Wait minutes'
	UNION SELECT @NewVersion, 1, 'Origin Tank Gross Units Calculator - revise logic to allow 0 ft, 0 in, 0 q entry to specify a non-zero starting quantity'
	UNION SELECT @NewVersion, 0, 'Setttlement Center (Rate Management): fix several bugs where changes to Carrier Rates un-rated Shipper rates instead'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier DestinationWait data info for the specified order
-- Changes:
	- 3.7.34 - 2015/06/20 - KDA - revise XXXMaxBillableMinutes to limit the actual Billable, not the total minutes
***********************************/
ALTER FUNCTION fnOrderCarrierDestinationWaitData(@ID int) RETURNS TABLE AS RETURN
	SELECT RateID, BillableMinutes, BillableHours, Amount = BillableHours * Rate
	FROM (
		SELECT RateID, BillableMinutes
			, BillableHours = dbo.fnComputeBillableWaitHours(BillableMinutes, SubUnitID, RoundingTypeID)
			, Rate
		FROM (
			SELECT RateID = WR.ID
				, Rate
				, BillableMinutes = dbo.fnMaxInt(
					-- Total Origin Minutes between range of DestMinBillableMinutes & DestMaxBillableMinutes (will be ignored if either Min|Max value is NULL)
					CASE WHEN WR.Minutes < WFP.DestMinBillableMinutes THEN 0
						 WHEN WR.Minutes - WFP.DestThresholdMinutes > WFP.DestMaxBillableMinutes THEN WFP.DestMaxBillableMinutes
						 ELSE WR.Minutes - WFP.DestThresholdMinutes END, 0)
				, WFP.SubUnitID, WFP.RoundingTypeID
			FROM dbo.fnOrderCarrierDestinationWaitRate(@ID) WR 
			CROSS JOIN dbo.fnOrderCarrierWaitFeeParameter(@ID) WFP 
		) X
	) X2
	WHERE BillableHours > 0.0
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OriginWait data info for the specified order
-- Changes:
	- 3.7.34 - 2015/06/20 - KDA - revise XXXMaxBillableMinutes to limit the actual Billable, not the total minutes
***********************************/
ALTER FUNCTION fnOrderCarrierOriginWaitData(@ID int) RETURNS TABLE AS RETURN
	SELECT RateID, BillableMinutes, BillableHours, Amount = BillableHours * Rate
	FROM (
		SELECT RateID, BillableMinutes
			, BillableHours = dbo.fnComputeBillableWaitHours(BillableMinutes, SubUnitID, RoundingTypeID)
			, Rate
		FROM (
			SELECT RateID = WR.ID
				, WR.Rate
				, BillableMinutes = dbo.fnMaxInt(
					-- Total Origin Minutes between range of DestMinBillableMinutes & DestMaxBillableMinutes (will be ignored if either Min|Max value is NULL)
					CASE WHEN WR.Minutes < WFP.OriginMinBillableMinutes THEN 0
						 WHEN WR.Minutes - WFP.OriginThresholdMinutes > WFP.OriginMaxBillableMinutes THEN WFP.OriginMaxBillableMinutes
						 ELSE WR.Minutes - WFP.OriginThresholdMinutes END, 0)
				, WFP.SubUnitID, WFP.RoundingTypeID
			FROM dbo.fnOrderCarrierOriginWaitRate(@ID) WR 
			CROSS JOIN dbo.fnOrderCarrierWaitFeeParameter(@ID) WFP 
		) X
	) X2
	WHERE BillableHours > 0.0
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper DestinationWait data info for the specified order
-- Changes:
	- 3.7.34 - 2015/06/20 - KDA - revise XXXMaxBillableMinutes to limit the actual Billable, not the total minutes
***********************************/
ALTER FUNCTION fnOrderShipperDestinationWaitData(@ID int) RETURNS TABLE AS RETURN
	SELECT RateID, BillableMinutes, BillableHours, Amount = BillableHours * Rate
	FROM (
		SELECT RateID, BillableMinutes
			, BillableHours = dbo.fnComputeBillableWaitHours(BillableMinutes, SubUnitID, RoundingTypeID)
			, Rate
		FROM (
			SELECT RateID = WR.ID
				, Rate
				, BillableMinutes = dbo.fnMaxInt(
					-- Total Origin Minutes between range of DestMinBillableMinutes & DestMaxBillableMinutes (will be ignored if either Min|Max value is NULL)
					CASE WHEN WR.Minutes < WFP.DestMinBillableMinutes THEN 0
						 WHEN WR.Minutes - WFP.DestThresholdMinutes > WFP.DestMaxBillableMinutes THEN WFP.DestMaxBillableMinutes
						 ELSE WR.Minutes - WFP.DestThresholdMinutes END, 0)
				, WFP.SubUnitID, WFP.RoundingTypeID
			FROM dbo.fnOrderShipperDestinationWaitRate(@ID) WR 
			CROSS JOIN dbo.fnOrderShipperWaitFeeParameter(@ID) WFP 
		) X
	) X2
	WHERE BillableHours > 0.0
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper OriginWait data info for the specified order
-- Changes:
	- 3.7.34 - 2015/06/20 - KDA - revise XXXMaxBillableMinutes to limit the actual Billable, not the total minutes
***********************************/
ALTER FUNCTION fnOrderShipperOriginWaitData(@ID int) RETURNS TABLE AS RETURN
	SELECT RateID, BillableMinutes, BillableHours, Amount = BillableHours * Rate
	FROM (
		SELECT RateID, BillableMinutes
			, BillableHours = dbo.fnComputeBillableWaitHours(BillableMinutes, SubUnitID, RoundingTypeID)
			, Rate
		FROM (
			SELECT RateID = WR.ID
				, WR.Rate
				, BillableMinutes = dbo.fnMaxInt(
					-- Total Origin Minutes between range of DestMinBillableMinutes & DestMaxBillableMinutes (will be ignored if either Min|Max value is NULL)
					CASE WHEN WR.Minutes < WFP.OriginMinBillableMinutes THEN 0
						 WHEN WR.Minutes - WFP.OriginThresholdMinutes > WFP.OriginMaxBillableMinutes THEN WFP.OriginMaxBillableMinutes
						 ELSE WR.Minutes - WFP.OriginThresholdMinutes END, 0)
				, WFP.SubUnitID, WFP.RoundingTypeID
			FROM dbo.fnOrderShipperOriginWaitRate(@ID) WR 
			CROSS JOIN dbo.fnOrderShipperWaitFeeParameter(@ID) WFP 
		) X
	) X2
	WHERE BillableHours > 0.0
GO

/***********************************
-- Date Created: 16 Mar 2014
-- Author: Kevin Alons
-- Purpose: return a virtual fully populated OriginTank Strapping Table (with running Barrels)
-- Changes:
	- 3.7.32 - 2015/06/20 - KDA - honor initial 0 ft, 0 in, 0 q BPQ setting as starting total quantity
***********************************/
ALTER FUNCTION [fnOriginTankStrapping](@originTankID int) 
RETURNS 
	@OriginTankStrapping TABLE (
		ID int
		, OriginTankID int
		, Feet tinyint
		, Inches tinyint
		, Q tinyint
		, TotalQ int
		, BPQ decimal(18, 10)
		, Barrels decimal(18, 10)
		, IsMinimum bit
		, IsMaximum bit
		, CreateDateUTC datetime
		, CreatedByUser varchar(100)
		, LastChangeDateUTC datetime
		, LastChangedByUser varchar(100)
		, Origin varchar(50)
		, TankNum varchar(20)
	)
AS BEGIN
	INSERT INTO @OriginTankStrapping
		SELECT ID
			, OriginTankID
			, Feet
			, Inches
			, Q
			, TotalQ
			, BPQ
			, 0
			, IsMinimum
			, IsMaximum 
			, CreateDateUTC
			, CreatedByUser 
			, LastChangeDateUTC
			, LastChangedByUser
			, Origin
			, TankNum
		FROM dbo.viewOriginTankStrapping
		WHERE OriginTankID = @originTankID
		ORDER BY TotalQ
		
	DECLARE @origin varchar(50), @tankNum varchar(20)
	SELECT @origin = MIN(Origin), @tankNum = MIN(TankNum) FROM @OriginTankStrapping
	DECLARE @workTotalQ int, @lastTotalQ int, @workBPQ decimal(18, 10), @Barrels decimal(18, 10)
	SELECT @workTotalQ = MIN(TotalQ), @lastTotalQ = MAX(TotalQ) FROM @OriginTankStrapping
	SELECT TOP 1 @workBPQ = BPQ, @Barrels = CASE WHEN TotalQ = 0 THEN BPQ ELSE TotalQ * BPQ END FROM @OriginTankStrapping ORDER BY TotalQ
	
	WHILE @workTotalQ <= @lastTotalQ
	BEGIN
		IF ((SELECT COUNT(*) FROM @OriginTankStrapping WHERE TotalQ = @workTotalQ) > 0)
			UPDATE @OriginTankStrapping SET Barrels = @Barrels WHERE TotalQ = @workTotalQ
		ELSE
			INSERT INTO @OriginTankStrapping (OriginTankID, Feet, Inches, Q, TotalQ
					, BPQ, Barrels, IsMinimum, IsMaximum, Origin, TankNum)
				VALUES (@originTankID, @workTotalQ / 48, (@workTotalQ % 48) / 4, @workTotalQ % 4, @workTotalQ
					, @workBPQ, @Barrels, 0, 0, @origin, @tankNum)
		-- advance to the next workTotalQ	
		SET @workTotalQ = @workTotalQ + 1				
		-- get the next appropriate BPQ value (which is the next higher)
		SELECT TOP 1 @workBPQ = BPQ FROM @OriginTankStrapping WHERE TotalQ > @workTotalQ ORDER BY TotalQ ASC
		-- compute the next total Barrels value
		SELECT @Barrels = @Barrels + @workBPQ
	END

	RETURN	
END

GO

/***********************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			prevent changes to Locked (in use) records
-- Changes:
	- 3.7.34 - 2015/06/20 - KDA - fix overlapping create logic to honor DriverGroupID
***********************************************/
ALTER TRIGGER [trigCarrierOriginWaitRate_IU] ON [tblCarrierOriginWaitRate] AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblCarrierOriginWaitRate X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ReasonID, X.ReasonID) = 1
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(i.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Wait Rates are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewCarrierOriginWaitRate X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ReasonID, X.ReasonID) = 0
		  OR dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
		  OR dbo.fnCompareNullableInts(d.CarrierID, X.CarrierID) = 0
		  OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
		  OR dbo.fnCompareNullableInts(d.DriverGroupID, X.DriverGroupID) = 0
		  OR dbo.fnCompareNullableInts(d.OriginID, X.OriginID) = 0
		  OR dbo.fnCompareNullableInts(d.StateID, X.StateID) = 0
		  OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
		  OR d.EffectiveDate <> X.EffectiveDate
		  OR d.Rate <> X.Rate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	IF @error IS NOT NULL 
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementCarrier SET OriginWaitRateID = NULL, OriginWaitAmount = NULL 
		WHERE BatchID IS NULL AND OriginWaitRateID IN (SELECT ID FROM inserted)
END
GO

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
ALTER TABLE dbo.tblOrderSettlementCarrier
	DROP CONSTRAINT FK_OrderSettlementCarrier_SettlementUom
GO
ALTER TABLE dbo.tblUom SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderSettlementCarrier
	DROP CONSTRAINT FK_OrderSettlementCarrier_SettlementFactor
GO
ALTER TABLE dbo.tblSettlementFactor SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderSettlementCarrier
	DROP CONSTRAINT FK_OrderSettlementCarrier_Batch
GO
ALTER TABLE dbo.tblCarrierSettlementBatch SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderSettlementCarrier
	DROP CONSTRAINT FK_OrderSettlementCarrier_Order
GO
ALTER TABLE dbo.tblOrder SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderSettlementCarrier
	DROP CONSTRAINT FK_OrderSettlementCarrier_OrderRejectRate
GO
ALTER TABLE dbo.tblCarrierOrderRejectRate SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderSettlementCarrier
	DROP CONSTRAINT FK_OrderSettlementCarrier_DestinationWaitRate
GO
ALTER TABLE dbo.tblCarrierDestinationWaitRate SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderSettlementCarrier
	DROP CONSTRAINT FK_OrderSettlementCarrier_OriginWaitRate
GO
ALTER TABLE dbo.tblCarrierOriginWaitRate SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderSettlementCarrier
	DROP CONSTRAINT FK_OrderSettlmentCarrier_RouteRate
GO
ALTER TABLE dbo.tblCarrierRouteRate SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderSettlementCarrier
	DROP CONSTRAINT FK_OrderSettlementCarrier_CarrierSettlementFactorID
GO
ALTER TABLE dbo.tblCarrierSettlementFactor SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderSettlementCarrier
	DROP CONSTRAINT FK_OrderSettlementCarrier_MinSettlementUnitsID
GO
ALTER TABLE dbo.tblCarrierMinSettlementUnits SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderSettlementCarrier
	DROP CONSTRAINT FK_OrderSettlementCarrier_RangeRate
GO
ALTER TABLE dbo.tblCarrierRangeRate SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderSettlementCarrier
	DROP CONSTRAINT FK_OrderSettlementCarrier_FuelSurchargeRate
GO
ALTER TABLE dbo.tblCarrierFuelSurchargeRate SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderSettlementCarrier
	DROP CONSTRAINT DF_OrderSettlementCarrier_CreateDateUTC
GO
ALTER TABLE dbo.tblOrderSettlementCarrier
	DROP CONSTRAINT DF_OrderSettlementCarrier_CreatedByUser
GO
CREATE TABLE dbo.Tmp_tblOrderSettlementCarrier
	(
	OrderID int NOT NULL,
	OrderDate date NOT NULL,
	BatchID int NULL,
	SettlementFactorID int NULL,
	SettlementUomID int NOT NULL,
	MinSettlementUnits int NULL,
	SettlementUnits decimal(18, 10) NOT NULL,
	RouteRateID int NULL,
	RangeRateID int NULL,
	LoadAmount money NULL,
	WaitFeeParameterID int NULL,
	OriginWaitRateID int NULL,
	OriginWaitBillableMinutes int NULL,
	OriginWaitBillableHours money NULL,
	OriginWaitAmount money NULL,
	DestinationWaitRateID int NULL,
	DestinationWaitBillableMinutes int NULL,
	DestinationWaitBillableHours money NULL,
	DestinationWaitAmount money NULL,
	OrderRejectRateID int NULL,
	OrderRejectAmount money NULL,
	FuelSurchargeRateID int NULL,
	FuelSurchargeRate money NULL,
	FuelSurchargeAmount money NULL,
	OriginTaxRate money NULL,
	TotalAmount money NOT NULL,
	CreateDateUTC datetime NOT NULL,
	CreatedByUser varchar(100) NOT NULL,
	MinSettlementUnitsID int NULL,
	CarrierSettlementFactorID int NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_tblOrderSettlementCarrier SET (LOCK_ESCALATION = TABLE)
GO
GRANT DELETE ON dbo.Tmp_tblOrderSettlementCarrier TO role_iis_acct  AS dbo
GO
GRANT INSERT ON dbo.Tmp_tblOrderSettlementCarrier TO role_iis_acct  AS dbo
GO
GRANT SELECT ON dbo.Tmp_tblOrderSettlementCarrier TO role_iis_acct  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_tblOrderSettlementCarrier TO role_iis_acct  AS dbo
GO
ALTER TABLE dbo.Tmp_tblOrderSettlementCarrier ADD CONSTRAINT
	DF_OrderSettlementCarrier_CreateDateUTC DEFAULT (getutcdate()) FOR CreateDateUTC
GO
ALTER TABLE dbo.Tmp_tblOrderSettlementCarrier ADD CONSTRAINT
	DF_OrderSettlementCarrier_CreatedByUser DEFAULT (suser_name()) FOR CreatedByUser
GO
IF EXISTS(SELECT * FROM dbo.tblOrderSettlementCarrier)
	 EXEC('INSERT INTO dbo.Tmp_tblOrderSettlementCarrier (OrderID, OrderDate, BatchID, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits, RouteRateID, RangeRateID, LoadAmount, WaitFeeParameterID, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount, DestinationWaitRateID, DestinationWaitBillableMinutes, DestinationWaitBillableHours, DestinationWaitAmount, OrderRejectRateID, OrderRejectAmount, FuelSurchargeRateID, FuelSurchargeRate, FuelSurchargeAmount, OriginTaxRate, TotalAmount, CreateDateUTC, CreatedByUser, MinSettlementUnitsID, CarrierSettlementFactorID)
		SELECT OrderID, OrderDate, BatchID, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits, RouteRateID, RangeRateID, LoadAmount, WaitFeeParameterID, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount, DestinationWaitRateID, DestinationWaitBillableMinutes, DestinationWaitBillableHours, DestinationWaitAmount, OrderRejectRateID, OrderRejectAmount, FuelSurchargeRateID, FuelSurchargeRate, FuelSurchargeAmount, OriginTaxRate, TotalAmount, CreateDateUTC, CreatedByUser, MinSettlementUnitsID, CarrierSettlementFactorID FROM dbo.tblOrderSettlementCarrier WITH (HOLDLOCK TABLOCKX)')
GO
ALTER TABLE dbo.tblOrderSettlementCarrierAssessorialCharge
	DROP CONSTRAINT FK_OrderSettlementCarrierOtherAssessorialCharge_Settlement
GO
DROP TABLE dbo.tblOrderSettlementCarrier
GO
EXECUTE sp_rename N'dbo.Tmp_tblOrderSettlementCarrier', N'tblOrderSettlementCarrier', 'OBJECT' 
GO
ALTER TABLE dbo.tblOrderSettlementCarrier ADD CONSTRAINT
	PK_OrderSettlementCarrier PRIMARY KEY CLUSTERED 
	(
	OrderID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE NONCLUSTERED INDEX idxOrderSettlementCarrier_Covering1 ON dbo.tblOrderSettlementCarrier
	(
	WaitFeeParameterID,
	BatchID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxOrderSettlementCarrier_Batch_Covering1 ON dbo.tblOrderSettlementCarrier
	(
	BatchID
	) INCLUDE (RangeRateID) 
 WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxOrderSettlementCarrier_RangeRate_Batch ON dbo.tblOrderSettlementCarrier
	(
	RangeRateID,
	BatchID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxOrderSettlementCarrier_Batch_Covering2 ON dbo.tblOrderSettlementCarrier
	(
	BatchID
	) INCLUDE (OrderID, OrderDate) 
 WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxOrderSettlementCarrier_Covering2 ON dbo.tblOrderSettlementCarrier
	(
	RouteRateID,
	BatchID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.tblOrderSettlementCarrier ADD CONSTRAINT
	CK_OrderSettlementCarrier_RouteOrRangeRateNull CHECK (([RouteRateID] IS NULL OR [RangeRateID] IS NULL))
GO
ALTER TABLE dbo.tblOrderSettlementCarrier ADD CONSTRAINT
	FK_OrderSettlementCarrier_FuelSurchargeRate FOREIGN KEY
	(
	FuelSurchargeRateID
	) REFERENCES dbo.tblCarrierFuelSurchargeRate
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderSettlementCarrier WITH NOCHECK ADD CONSTRAINT
	FK_OrderSettlementCarrier_RangeRate FOREIGN KEY
	(
	RangeRateID
	) REFERENCES dbo.tblCarrierRangeRate
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderSettlementCarrier ADD CONSTRAINT
	FK_OrderSettlementCarrier_MinSettlementUnitsID FOREIGN KEY
	(
	MinSettlementUnitsID
	) REFERENCES dbo.tblCarrierMinSettlementUnits
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderSettlementCarrier ADD CONSTRAINT
	FK_OrderSettlementCarrier_CarrierSettlementFactorID FOREIGN KEY
	(
	CarrierSettlementFactorID
	) REFERENCES dbo.tblCarrierSettlementFactor
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderSettlementCarrier ADD CONSTRAINT
	FK_OrderSettlmentCarrier_RouteRate FOREIGN KEY
	(
	RouteRateID
	) REFERENCES dbo.tblCarrierRouteRate
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderSettlementCarrier ADD CONSTRAINT
	FK_OrderSettlementCarrier_OriginWaitRate FOREIGN KEY
	(
	OriginWaitRateID
	) REFERENCES dbo.tblCarrierOriginWaitRate
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderSettlementCarrier ADD CONSTRAINT
	FK_OrderSettlementCarrier_DestinationWaitRate FOREIGN KEY
	(
	DestinationWaitRateID
	) REFERENCES dbo.tblCarrierDestinationWaitRate
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderSettlementCarrier ADD CONSTRAINT
	FK_OrderSettlementCarrier_OrderRejectRate FOREIGN KEY
	(
	OrderRejectRateID
	) REFERENCES dbo.tblCarrierOrderRejectRate
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderSettlementCarrier ADD CONSTRAINT
	FK_OrderSettlementCarrier_Order FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.tblOrder
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderSettlementCarrier ADD CONSTRAINT
	FK_OrderSettlementCarrier_Batch FOREIGN KEY
	(
	BatchID
	) REFERENCES dbo.tblCarrierSettlementBatch
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderSettlementCarrier ADD CONSTRAINT
	FK_OrderSettlementCarrier_SettlementFactor FOREIGN KEY
	(
	SettlementFactorID
	) REFERENCES dbo.tblSettlementFactor
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderSettlementCarrier ADD CONSTRAINT
	FK_OrderSettlementCarrier_SettlementUom FOREIGN KEY
	(
	SettlementUomID
	) REFERENCES dbo.tblUom
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO

ALTER TABLE dbo.tblOrderSettlementShipper
	DROP CONSTRAINT FK_OrderSettlementShipper_SettlementUom
GO
ALTER TABLE dbo.tblUom SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderSettlementShipper
	DROP CONSTRAINT FK_OrderSettlementShipper_SettlementFactor
GO
ALTER TABLE dbo.tblSettlementFactor SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderSettlementShipper
	DROP CONSTRAINT FK_OrderSettlementShipper_Batch
GO
ALTER TABLE dbo.tblShipperSettlementBatch SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderSettlementShipper
	DROP CONSTRAINT FK_OrderSettlementShipper_Order
GO
ALTER TABLE dbo.tblOrder SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderSettlementShipper
	DROP CONSTRAINT FK_OrderSettlementShipper_ShipperSettlementFactorID
GO
ALTER TABLE dbo.tblShipperSettlementFactor SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderSettlementShipper
	DROP CONSTRAINT FK_OrderSettlementShipper_MinSettlementUnitsID
GO
ALTER TABLE dbo.tblShipperMinSettlementUnits SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderSettlementShipper
	DROP CONSTRAINT FK_OrderSettlementShipper_OrderRejectRate
GO
ALTER TABLE dbo.tblShipperOrderRejectRate SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderSettlementShipper
	DROP CONSTRAINT FK_OrderSettlementShipper_DestinationWaitRate
GO
ALTER TABLE dbo.tblShipperDestinationWaitRate SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderSettlementShipper
	DROP CONSTRAINT FK_OrderSettlementShipper_OriginWaitRate
GO
ALTER TABLE dbo.tblShipperOriginWaitRate SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderSettlementShipper
	DROP CONSTRAINT FK_OrderSettlementShipper_RangeRate
GO
ALTER TABLE dbo.tblShipperRangeRate SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderSettlementShipper
	DROP CONSTRAINT FK_OrderSettlmentShipper_RouteRate
GO
ALTER TABLE dbo.tblShipperRouteRate SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderSettlementShipper
	DROP CONSTRAINT FK_OrderSettlementShipper_FuelSurchargeRate
GO
ALTER TABLE dbo.tblShipperFuelSurchargeRate SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderSettlementShipper
	DROP CONSTRAINT DF_OrderSettlementShipper_CreateDateUTC
GO
ALTER TABLE dbo.tblOrderSettlementShipper
	DROP CONSTRAINT DF_OrderSettlementShipper_CreatedByUser
GO
CREATE TABLE dbo.Tmp_tblOrderSettlementShipper
	(
	OrderID int NOT NULL,
	OrderDate date NOT NULL,
	BatchID int NULL,
	SettlementFactorID int NULL,
	SettlementUomID int NOT NULL,
	MinSettlementUnits int NULL,
	SettlementUnits decimal(18, 10) NOT NULL,
	RouteRateID int NULL,
	RangeRateID int NULL,
	LoadAmount money NULL,
	WaitFeeParameterID int NULL,
	OriginWaitRateID int NULL,
	OriginWaitBillableMinutes int NULL,
	OriginWaitBillableHours money NULL,
	OriginWaitAmount money NULL,
	DestinationWaitRateID int NULL,
	DestinationWaitBillableMinutes int NULL,
	DestinationWaitBillableHours money NULL,
	DestinationWaitAmount money NULL,
	OrderRejectRateID int NULL,
	OrderRejectAmount money NULL,
	FuelSurchargeRateID int NULL,
	FuelSurchargeRate money NULL,
	FuelSurchargeAmount money NULL,
	OriginTaxRate money NULL,
	TotalAmount money NOT NULL,
	CreateDateUTC datetime NOT NULL,
	CreatedByUser varchar(100) NOT NULL,
	MinSettlementUnitsID int NULL,
	ShipperSettlementFactorID int NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_tblOrderSettlementShipper SET (LOCK_ESCALATION = TABLE)
GO
GRANT DELETE ON dbo.Tmp_tblOrderSettlementShipper TO role_iis_acct  AS dbo
GO
GRANT INSERT ON dbo.Tmp_tblOrderSettlementShipper TO role_iis_acct  AS dbo
GO
GRANT SELECT ON dbo.Tmp_tblOrderSettlementShipper TO role_iis_acct  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_tblOrderSettlementShipper TO role_iis_acct  AS dbo
GO
ALTER TABLE dbo.Tmp_tblOrderSettlementShipper ADD CONSTRAINT
	DF_OrderSettlementShipper_CreateDateUTC DEFAULT (getutcdate()) FOR CreateDateUTC
GO
ALTER TABLE dbo.Tmp_tblOrderSettlementShipper ADD CONSTRAINT
	DF_OrderSettlementShipper_CreatedByUser DEFAULT (suser_name()) FOR CreatedByUser
GO
IF EXISTS(SELECT * FROM dbo.tblOrderSettlementShipper)
	 EXEC('INSERT INTO dbo.Tmp_tblOrderSettlementShipper (OrderID, OrderDate, BatchID, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits, RouteRateID, RangeRateID, LoadAmount, WaitFeeParameterID, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount, DestinationWaitRateID, DestinationWaitBillableMinutes, DestinationWaitBillableHours, DestinationWaitAmount, OrderRejectRateID, OrderRejectAmount, FuelSurchargeRateID, FuelSurchargeRate, FuelSurchargeAmount, OriginTaxRate, TotalAmount, CreateDateUTC, CreatedByUser, MinSettlementUnitsID, ShipperSettlementFactorID)
		SELECT OrderID, OrderDate, BatchID, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits, RouteRateID, RangeRateID, LoadAmount, WaitFeeParameterID, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount, DestinationWaitRateID, DestinationWaitBillableMinutes, DestinationWaitBillableHours, DestinationWaitAmount, OrderRejectRateID, OrderRejectAmount, FuelSurchargeRateID, FuelSurchargeRate, FuelSurchargeAmount, OriginTaxRate, TotalAmount, CreateDateUTC, CreatedByUser, MinSettlementUnitsID, ShipperSettlementFactorID FROM dbo.tblOrderSettlementShipper WITH (HOLDLOCK TABLOCKX)')
GO
ALTER TABLE dbo.tblOrderSettlementShipperAssessorialCharge
	DROP CONSTRAINT FK_OrderSettlementShipperOtherAssessorialCharge_Settlement
GO
DROP TABLE dbo.tblOrderSettlementShipper
GO
EXECUTE sp_rename N'dbo.Tmp_tblOrderSettlementShipper', N'tblOrderSettlementShipper', 'OBJECT' 
GO
ALTER TABLE dbo.tblOrderSettlementShipper ADD CONSTRAINT
	PK_OrderSettlementShipper PRIMARY KEY CLUSTERED 
	(
	OrderID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE NONCLUSTERED INDEX idxOrderSettlementShipper_Covering1 ON dbo.tblOrderSettlementShipper
	(
	WaitFeeParameterID,
	BatchID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxOrderSettlementShipper_Batch ON dbo.tblOrderSettlementShipper
	(
	BatchID
	) INCLUDE (OrderID) 
 WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.tblOrderSettlementShipper ADD CONSTRAINT
	CK_OrderSettlementShipper_RouteOrRangeRateNull CHECK (([RouteRateID] IS NULL OR [RangeRateID] IS NULL))
GO
ALTER TABLE dbo.tblOrderSettlementShipper ADD CONSTRAINT
	FK_OrderSettlementShipper_FuelSurchargeRate FOREIGN KEY
	(
	FuelSurchargeRateID
	) REFERENCES dbo.tblShipperFuelSurchargeRate
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderSettlementShipper ADD CONSTRAINT
	FK_OrderSettlmentShipper_RouteRate FOREIGN KEY
	(
	RouteRateID
	) REFERENCES dbo.tblShipperRouteRate
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderSettlementShipper ADD CONSTRAINT
	FK_OrderSettlementShipper_RangeRate FOREIGN KEY
	(
	RangeRateID
	) REFERENCES dbo.tblShipperRangeRate
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderSettlementShipper ADD CONSTRAINT
	FK_OrderSettlementShipper_OriginWaitRate FOREIGN KEY
	(
	OriginWaitRateID
	) REFERENCES dbo.tblShipperOriginWaitRate
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderSettlementShipper ADD CONSTRAINT
	FK_OrderSettlementShipper_DestinationWaitRate FOREIGN KEY
	(
	DestinationWaitRateID
	) REFERENCES dbo.tblShipperDestinationWaitRate
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderSettlementShipper ADD CONSTRAINT
	FK_OrderSettlementShipper_OrderRejectRate FOREIGN KEY
	(
	OrderRejectRateID
	) REFERENCES dbo.tblShipperOrderRejectRate
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderSettlementShipper ADD CONSTRAINT
	FK_OrderSettlementShipper_MinSettlementUnitsID FOREIGN KEY
	(
	MinSettlementUnitsID
	) REFERENCES dbo.tblShipperMinSettlementUnits
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderSettlementShipper ADD CONSTRAINT
	FK_OrderSettlementShipper_ShipperSettlementFactorID FOREIGN KEY
	(
	ShipperSettlementFactorID
	) REFERENCES dbo.tblShipperSettlementFactor
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderSettlementShipper ADD CONSTRAINT
	FK_OrderSettlementShipper_Order FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.tblOrder
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderSettlementShipper ADD CONSTRAINT
	FK_OrderSettlementShipper_Batch FOREIGN KEY
	(
	BatchID
	) REFERENCES dbo.tblShipperSettlementBatch
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderSettlementShipper ADD CONSTRAINT
	FK_OrderSettlementShipper_SettlementFactor FOREIGN KEY
	(
	SettlementFactorID
	) REFERENCES dbo.tblSettlementFactor
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderSettlementShipper ADD CONSTRAINT
	FK_OrderSettlementShipper_SettlementUom FOREIGN KEY
	(
	SettlementUomID
	) REFERENCES dbo.tblUom
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
/*************************************/
-- Date Created: 27 Dec 2014
-- Author: Kevin Alons
-- Purpose: ensure the TotalAmount always reflects the sum of the individual amounts (including associated Assessorial charges)
/*************************************/
CREATE TRIGGER [dbo].[trigOrderSettlementShipper_IU] ON dbo.tblOrderSettlementShipper FOR INSERT, UPDATE  AS
BEGIN
	-- Note: the TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU') ) = 0 statement prevents recursion
	IF (TRIGGER_NESTLEVEL( OBJECT_ID('trigOrderSettlementShipper_IU')) = 1
		AND (
			UPDATE(LoadAmount)
			OR UPDATE(OrderRejectAmount)
			OR UPDATE(OriginWaitAmount)
			OR UPDATE(DestinationWaitAmount)
			OR UPDATE(FuelSurchargeAmount)
			OR UPDATE (TotalAmount)
			)
		)
	BEGIN
		UPDATE tblOrderSettlementShipper
			SET TotalAmount = isnull(SC.LoadAmount, 0) 
				+ isnull(SC.OrderRejectAmount, 0) 
				+ isnull(SC.OriginWaitAmount, 0) 
				+ isnull(SC.DestinationWaitAmount, 0)
				+ isnull(SC.FuelSurchargeAmount, 0)
				+ isnull((SELECT SUM(Amount) FROM tblOrderSettlementShipperAssessorialCharge WHERE OrderID = SC.OrderID), 0)
		FROM tblOrderSettlementShipper SC
		JOIN inserted i ON i.OrderID = SC.OrderID 
	END
END
GO
ALTER TABLE dbo.tblOrderSettlementShipperAssessorialCharge ADD CONSTRAINT
	FK_OrderSettlementShipperOtherAssessorialCharge_Settlement FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.tblOrderSettlementShipper
	(
	OrderID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
GO
ALTER TABLE dbo.tblOrderSettlementShipperAssessorialCharge SET (LOCK_ESCALATION = TABLE)
GO

/*************************************
-- Date Created: 27 Dec 2014
-- Author: Kevin Alons
-- Purpose: ensure the TotalAmount always reflects the sum of the individual amounts (including associated Assessorial charges)
*************************************/
CREATE TRIGGER trigOrderSettlementCarrier_IU ON dbo.tblOrderSettlementCarrier FOR INSERT, UPDATE  AS
BEGIN
	-- Note: the TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU') ) = 0 statement prevents recursion
	IF (TRIGGER_NESTLEVEL( OBJECT_ID('trigOrderSettlementCarrier_IU')) = 1
		AND (
			UPDATE(LoadAmount)
			OR UPDATE(OrderRejectAmount)
			OR UPDATE(OriginWaitAmount)
			OR UPDATE(DestinationWaitAmount)
			OR UPDATE(FuelSurchargeAmount)
			OR UPDATE (TotalAmount)
			)
		)
	BEGIN
		UPDATE tblOrderSettlementCarrier
			SET TotalAmount = isnull(SC.LoadAmount, 0) 
				+ isnull(SC.OrderRejectAmount, 0) 
				+ isnull(SC.OriginWaitAmount, 0) 
				+ isnull(SC.DestinationWaitAmount, 0)
				+ isnull(SC.FuelSurchargeAmount, 0)
				+ isnull((SELECT SUM(Amount) FROM tblOrderSettlementCarrierAssessorialCharge WHERE OrderID = SC.OrderID), 0)
		FROM tblOrderSettlementCarrier SC
		JOIN inserted i ON i.OrderID = SC.OrderID 
	END
END
GO
ALTER TABLE dbo.tblOrderSettlementCarrierAssessorialCharge ADD CONSTRAINT
	FK_OrderSettlementCarrierOtherAssessorialCharge_Settlement FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.tblOrderSettlementCarrier
	(
	OrderID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
GO
ALTER TABLE dbo.tblOrderSettlementCarrierAssessorialCharge SET (LOCK_ESCALATION = TABLE)
GO

/*************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent deletion of Locked records
-- Changes:
	- 3.7.34 - 2015/06/20 - KDA - fix bug that referenced the tblOrderSettlementSHIPPER vs CARRIER
*************************************/
ALTER TRIGGER trigCarrierOrderRejectRate_IOD ON tblCarrierOrderRejectRate INSTEAD OF DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewCarrierOrderRejectRate X ON X.id = d.ID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('Locked (in use) records cannot be deleted', 16, 1)
		RETURN
	END
	ELSE
	BEGIN
		-- remove all references to the deleted rates
		UPDATE tblOrderSettlementCarrier
		  SET OrderRejectRateID = NULL, OrderRejectAmount = NULL
		WHERE BatchID IS NULL
		  AND OrderRejectRateID IN (SELECT ID FROM deleted)
		-- do the actual delete now
		DELETE FROM tblCarrierOrderRejectRate WHERE ID IN (SELECT ID FROM deleted)
	END
END
GO

/*************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent deletion of Locked records
-- Changes:
	- 3.7.34 - 2015/06/20 - KDA - fix bug that referenced the tblOrderSettlementSHIPPER vs CARRIER
*************************************/
ALTER TRIGGER trigCarrierOriginWaitRate_IOD ON tblCarrierOriginWaitRate INSTEAD OF DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewCarrierOriginWaitRate X ON X.id = d.ID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('Locked (in use) records cannot be deleted', 16, 1)
		RETURN
	END
	ELSE
	BEGIN
		-- remove any references to the deleted rates
		UPDATE tblOrderSettlementCarrier
		  SET OriginWaitRateID = NULL, OriginWaitAmount = NULL
		WHERE BatchID IS NULL
		  AND OriginWaitRateID IN (SELECT ID FROM deleted)
		DELETE FROM tblCarrierOriginWaitRate WHERE ID IN (SELECT ID FROM deleted)
	END
END
GO

/*************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent deletion of RangeRate records that belong to a Locked RateSheet
-- Changes:
	- 3.7.34 - 2015/06/20 - KDA - fix bug that referenced the tblOrderSettlementSHIPPER vs CARRIER
*************************************/
ALTER TRIGGER trigCarrierRangeRate_IOD ON tblCarrierRangeRate INSTEAD OF DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewCarrierRateSheet X ON X.ID = d.RateSheetID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('RangeRate records for a Locked Rate Sheet cannot be deleted', 16, 1)
		ROLLBACK
	END
	ELSE
	BEGIN
		-- remove any references to the deleted rates
		UPDATE tblOrderSettlementCarrier
		  SET RangeRateID = NULL, LoadAmount = NULL
		WHERE BatchID IS NULL
		  AND RangeRateID IN (SELECT ID FROM deleted)
		DELETE FROM tblCarrierRangeRate WHERE ID IN (SELECT ID FROM deleted)
	END
END
GO

/*************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: handle specialized logic related to Effective/End dates (all handled through the associated view)
-- Changes:
	- 3.7.34 - 2015/06/20 - KDA - fix bug that referenced the tblOrderSettlementSHIPPER vs CARRIER
*************************************/
ALTER TRIGGER trigCarrierRouteRate_IOD ON tblCarrierRouteRate INSTEAD OF DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewCarrierRouteRate X ON X.ID = d.ID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('Locked (in use) records cannot be deleted', 16, 1)
		RETURN
	END
	ELSE
	BEGIN
		-- remove any references to the deleted rates
		UPDATE tblOrderSettlementCarrier
		  SET RouteRateID = NULL, LoadAmount = NULL
		WHERE BatchID IS NULL
		  AND RouteRateID IN (SELECT ID FROM deleted)
		-- do the actual delete now
		DELETE FROM tblCarrierRouteRate WHERE ID IN (SELECT ID FROM deleted)
	END
END
GO

/*************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent deletion of Locked (in use) records
-- Changes:
	- 3.7.34 - 2015/06/20 - KDA - fix bug that referenced the tblOrderSettlementSHIPPER vs CARRIER
*************************************/
ALTER TRIGGER trigCarrierWaitFeeParameter_IOD ON tblCarrierWaitFeeParameter INSTEAD OF DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewCarrierWaitFeeParameter X ON X.ID = d.ID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('Locked (in use) records cannot be deleted', 16, 1)
		RETURN
	END
	ELSE
	BEGIN
		-- remove any references to the deleted rates
		UPDATE tblOrderSettlementCarrier
		  SET WaitFeeParameterID = NULL, DestinationWaitAmount = NULL
		WHERE BatchID IS NULL
		  AND WaitFeeParameterID IN (SELECT ID FROM deleted)
		-- do the actual delete action now
		DELETE FROM tblCarrierWaitFeeParameter WHERE ID IN (SELECT ID FROM deleted)
	END
END
GO

/*************************************
-- Date Created: 2015/06/20
-- Author: Kevin Alons
-- Purpose: prevent deletion of Locked records
-- Changes:
*************************************/
CREATE TRIGGER trigCarrierMinSettlementUnits_IOD ON tblCarrierMinSettlementUnits INSTEAD OF DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewCarrierMinSettlementUnits X ON X.id = d.ID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('Locked (in use) records cannot be deleted', 16, 1)
		RETURN
	END
	ELSE
	BEGIN
		-- remove all references to the deleted rates
		UPDATE tblOrderSettlementCarrier
		  SET MinSettlementUnitsID = NULL, MinSettlementUnits = NULL
		WHERE BatchID IS NULL
		  AND MinSettlementUnitsID IN (SELECT ID FROM deleted)
		-- do the actual delete now
		DELETE FROM tblCarrierMinSettlementUnits WHERE ID IN (SELECT ID FROM deleted)
	END
END
GO

/*************************************
-- Date Created: 2015/06/20
-- Author: Kevin Alons
-- Purpose: prevent deletion of Locked records
-- Changes:
*************************************/
CREATE TRIGGER trigCarrierSettlementFactor_IOD ON tblCarrierSettlementFactor INSTEAD OF DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewCarrierSettlementFactor X ON X.id = d.ID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('Locked (in use) records cannot be deleted', 16, 1)
		RETURN
	END
	ELSE
	BEGIN
		-- remove all references to the deleted rates
		UPDATE tblOrderSettlementCarrier
		  SET CarrierSettlementFactorID = NULL, SettlementFactorID = NULL
		WHERE BatchID IS NULL
		  AND CarrierSettlementFactorID IN (SELECT ID FROM deleted)
		-- do the actual delete now
		DELETE FROM tblCarrierSettlementFactor WHERE ID IN (SELECT ID FROM deleted)
	END
END
GO

/*************************************
-- Date Created: 2015/06/20
-- Author: Kevin Alons
-- Purpose: prevent deletion of Locked records
-- Changes:
*************************************/
CREATE TRIGGER trigShipperMinSettlementUnits_IOD ON tblShipperMinSettlementUnits INSTEAD OF DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewShipperMinSettlementUnits X ON X.id = d.ID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('Locked (in use) records cannot be deleted', 16, 1)
		RETURN
	END
	ELSE
	BEGIN
		-- remove all references to the deleted rates
		UPDATE tblOrderSettlementShipper
		  SET MinSettlementUnitsID = NULL, MinSettlementUnits = NULL
		WHERE BatchID IS NULL
		  AND MinSettlementUnitsID IN (SELECT ID FROM deleted)
		-- do the actual delete now
		DELETE FROM tblShipperMinSettlementUnits WHERE ID IN (SELECT ID FROM deleted)
	END
END
GO

/*************************************
-- Date Created: 2015/06/20
-- Author: Kevin Alons
-- Purpose: prevent deletion of Locked records
-- Changes:
*************************************/
CREATE TRIGGER trigShipperSettlementFactor_IOD ON tblShipperSettlementFactor INSTEAD OF DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewShipperSettlementFactor X ON X.id = d.ID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('Locked (in use) records cannot be deleted', 16, 1)
		RETURN
	END
	ELSE
	BEGIN
		-- remove all references to the deleted rates
		UPDATE tblOrderSettlementShipper
		  SET ShipperSettlementFactorID = NULL, SettlementFactorID = NULL
		WHERE BatchID IS NULL
		  AND ShipperSettlementFactorID IN (SELECT ID FROM deleted)
		-- do the actual delete now
		DELETE FROM tblShipperSettlementFactor WHERE ID IN (SELECT ID FROM deleted)
	END
END
GO

EXEC _spRebuildAllObjects
GO

COMMIT 
SET NOEXEC OFF