-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.7.25.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.7.25.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0

DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.7.26'
SELECT  @NewVersion = '3.7.27'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Report Center: Add 8 new custom formatted date and time columns to report center'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/*
	BB - 6/17/2015 -- Add 8 new custom formatted date and time fields to report center
	
	Origin Arrive (mm/dd/yyyy)
	Origin Arrive (h:mm AM/PM)
	Origin Depart (mm/dd/yyyy)
	Origin Depart (h:mm AM/PM)
	
	Dest Arrive (mm/dd/yyyy)	
	Dest Arrive (h:mm AM/PM)
	Dest Depart (mm/dd/yyyy)
	Dest Depart (h:mm AM/PM)
*/

SET IDENTITY_INSERT tblReportColumnDefinition ON
  INSERT tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport)
  SELECT 90010,1,'CONVERT(NVARCHAR(10),OriginArriveTime,101)','TRIP | TIMESTAMPS | Origin Arrive (mm/dd/yyyy)',NULL,NULL,0,NULL,1,'*',0
  UNION
  SELECT 90011,1,'LTRIM(RIGHT(CONVERT(NVARCHAR(100),OriginArriveTime,100),7))','TRIP | TIMESTAMPS | Origin Arrive (h:mm AM/PM)',NULL,NULL,0,NULL,1,'*',0
  UNION
  SELECT 90012,1,'CONVERT(NVARCHAR(10),OriginDepartTime,101)','TRIP | TIMESTAMPS | Origin Depart (mm/dd/yyyy)',NULL,NULL,0,NULL,1,'*',0
  UNION
  SELECT 90013,1,'LTRIM(RIGHT(CONVERT(NVARCHAR(100),OriginDepartTime,100),7))','TRIP | TIMESTAMPS | Origin Depart (h:mm AM/PM)',NULL,NULL,0,NULL,1,'*',0
  UNION
  SELECT 90014,1,'CONVERT(NVARCHAR(10),DestArriveTime,101)','TRIP | TIMESTAMPS | Dest Arrive (mm/dd/yyyy)',NULL,NULL,0,NULL,1,'*',0
  UNION
  SELECT 90015,1,'LTRIM(RIGHT(CONVERT(NVARCHAR(100),DestArriveTime,100),7))','TRIP | TIMESTAMPS | Dest Arrive (h:mm AM/PM)',NULL,NULL,0,NULL,1,'*',0
  UNION
  SELECT 90016,1,'CONVERT(NVARCHAR(10),DestDepartTime,101)','TRIP | TIMESTAMPS | Dest Depart (mm/dd/yyyy)',NULL,NULL,0,NULL,1,'*',0
  UNION
  SELECT 90017,1,'LTRIM(RIGHT(CONVERT(NVARCHAR(100),DestDepartTime,100),7))','TRIP | TIMESTAMPS | Dest Depart (h:mm AM/PM)',NULL,NULL,0,NULL,1,'*',0
  EXCEPT SELECT ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport FROM tblReportColumnDefinition
SET IDENTITY_INSERT tblReportColumnDefinition OFF

COMMIT
SET NOEXEC OFF