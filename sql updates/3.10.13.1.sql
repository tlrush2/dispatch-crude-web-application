SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.10.13'
SELECT  @NewVersion = '3.10.13.1'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Truck Type - Update settlement tables'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


-- PART 2
-- UPDATE THE CORE SETTLEMENT TABLES TO INCLUDE TruckType COLUMN
------------------------------------------------------------------------------------------

-- Add TruckType to CarrierAssessorialRate table
ALTER TABLE tblCarrierAssessorialRate 
	ADD TruckTypeID INT NULL 
	CONSTRAINT FK_CarrierAssessorialRate_TruckType FOREIGN KEY REFERENCES tblTruckType (ID)
GO


-- Add TruckType to CarrierDestinationWaitRate table
ALTER TABLE tblCarrierDestinationWaitRate 
	ADD TruckTypeID INT NULL 
	CONSTRAINT FK_CarrierDestinationWaitRate_TruckType FOREIGN KEY REFERENCES tblTruckType (ID)
GO

-- Add TruckType to CarrierFuelSurchargeRate table
ALTER TABLE tblCarrierFuelSurchargeRate 
	ADD TruckTypeID INT NULL
	CONSTRAINT FK_CarrierFuelSurchargeRate_TruckType FOREIGN KEY REFERENCES tblTruckType (ID)
GO


-- Add TruckType to CarrierMinSettlementUnits table
ALTER TABLE tblCarrierMinSettlementUnits 
	ADD TruckTypeID INT NULL
	CONSTRAINT FK_CarrierMinSettlementUnits_TruckType FOREIGN KEY REFERENCES tblTruckType (ID)
GO


-- Add TruckType to CarrierOrderRejectRate table
ALTER TABLE tblCarrierOrderRejectRate 
	ADD TruckTypeID INT NULL
	CONSTRAINT FK_CarrierOrderRejectRate_TruckType FOREIGN KEY REFERENCES tblTruckType (ID)
GO


-- Add TruckType to CarrierOriginWaitRate table
ALTER TABLE tblCarrierOriginWaitRate 
	ADD TruckTypeID INT NULL
	CONSTRAINT FK_CarrierOriginWaitRate_TruckType FOREIGN KEY REFERENCES tblTruckType (ID)
GO


-- Add TruckType to CarrierRateSheet table
ALTER TABLE tblCarrierRateSheet 
	ADD TruckTypeID INT NULL 
	CONSTRAINT FK_CarrierRateSheet_TruckType FOREIGN KEY REFERENCES tblTruckType (ID)
GO


-- Add TruckType to CarrierRouteRate table
ALTER TABLE tblCarrierRouteRate 
	ADD TruckTypeID INT NULL 
	CONSTRAINT FK_CarrierRouteRate_TruckType FOREIGN KEY REFERENCES tblTruckType (ID)
GO


-- Add TruckType to CarrierSettlementFactor table
ALTER TABLE tblCarrierSettlementFactor 
	ADD TruckTypeID INT NULL 
	CONSTRAINT FK_CarrierSettlementFactor_TruckType FOREIGN KEY REFERENCES tblTruckType (ID)
GO


-- Add TruckType to CarrierWaitFeeParameter table
ALTER TABLE tblCarrierWaitFeeParameter 
	ADD TruckTypeID INT NULL 
	CONSTRAINT FK_CarrierWaitFeeParameter_TruckType FOREIGN KEY REFERENCES tblTruckType (ID)
GO



-- Add TruckType to ShipperAssessorialRate table
ALTER TABLE tblShipperAssessorialRate 
	ADD TruckTypeID INT NULL
	CONSTRAINT FK_ShipperAssessorialRate_TruckType FOREIGN KEY REFERENCES tblTruckType (ID)
GO


-- Add TruckType to ShipperDestinationWaitRate table
ALTER TABLE tblShipperDestinationWaitRate 
	ADD TruckTypeID INT NULL
	CONSTRAINT FK_ShipperDestinationWaitRate_TruckType FOREIGN KEY REFERENCES tblTruckType (ID)
GO


-- Add TruckType to ShipperFuelSurchargeRate table
ALTER TABLE tblShipperFuelSurchargeRate 
	ADD TruckTypeID INT NULL
	CONSTRAINT FK_ShipperFuelSurchargeRate_TruckType FOREIGN KEY REFERENCES tblTruckType (ID)
GO


-- Add TruckType to ShipperMinSettlementUnits table
ALTER TABLE tblShipperMinSettlementUnits 
	ADD TruckTypeID INT NULL 
	CONSTRAINT FK_ShipperMinSettlementUnits_TruckType FOREIGN KEY REFERENCES tblTruckType (ID)
GO


-- Add TruckType to ShipperOrderRejectRate table
ALTER TABLE tblShipperOrderRejectRate 
	ADD TruckTypeID INT NULL 
	CONSTRAINT FK_ShipperOrderRejectRate_TruckType FOREIGN KEY REFERENCES tblTruckType (ID)
GO


-- Add TruckType to ShipperOriginWaitRate table
ALTER TABLE tblShipperOriginWaitRate 
	ADD TruckTypeID INT NULL 
	CONSTRAINT FK_ShipperOriginWaitRate_TruckType FOREIGN KEY REFERENCES tblTruckType (ID)
GO


-- Add TruckType to ShipperRateSheet table
ALTER TABLE tblShipperRateSheet 
	ADD TruckTypeID INT NULL 
	CONSTRAINT FK_ShipperRateSheet_TruckType FOREIGN KEY REFERENCES tblTruckType (ID)
GO


-- Add TruckType to ShipperRouteRate table
ALTER TABLE tblShipperRouteRate 
	ADD TruckTypeID INT NULL
	CONSTRAINT FK_ShipperRouteRate_TruckType FOREIGN KEY REFERENCES tblTruckType (ID)
GO


-- Add TruckType to ShipperSettlementFactor table
ALTER TABLE tblShipperSettlementFactor 
	ADD TruckTypeID INT NULL 
	CONSTRAINT FK_ShipperSettlementFactor_TruckType FOREIGN KEY REFERENCES tblTruckType (ID)
GO


-- Add TruckType to ShipperWaitFeeParameter table
ALTER TABLE tblShipperWaitFeeParameter 
	ADD TruckTypeID INT NULL
	CONSTRAINT FK_ShipperWaitFeeParameter_TruckType FOREIGN KEY REFERENCES tblTruckType (ID)
GO



COMMIT
SET NOEXEC OFF