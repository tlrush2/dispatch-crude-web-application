-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.9.29.3.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.9.29.3.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0

SET NOEXEC OFF  -- since this is 
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.9.29.3'
SELECT  @NewVersion = '3.9.29.4'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Rate Center: fix bug preventing Carrier Route Rate Driver Group specific rates (was always "ALL")'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/*************************************
 Date Created: 18 Jan 2015
 Author: Kevin Alons
 Purpose: handle specialized logic related to editing Route Rates (due to RouteID being "inputted" as Origin|Destination combination)
 Changes:
	3.9.29.4 - 2015/12/02 - KDA - add missing DriverGroupID fields to this trigger
*************************************/
ALTER TRIGGER trigViewCarrierRouteRate_IU_Update ON viewCarrierRouteRate INSTEAD OF INSERT, UPDATE  AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
		--PRINT 'ensure a Route record exists for the new Origin-Destination combo'
		INSERT INTO tblRoute (OriginID, DestinationID)
			SELECT DISTINCT OriginID, DestinationID FROM inserted
			EXCEPT SELECT OriginID, DestinationID FROM tblRoute
		
		-- PRINT 'Updating any existing record editable data'
		UPDATE tblCarrierRouteRate
			SET CarrierID = nullif(i.CarrierID, 0)
				, ProductGroupID = nullif(i.ProductGroupID, 0)
				, DriverGroupID = nullif(i.DriverGroupID, 0)
				, RouteID = R.ID
				, Rate = i.Rate
				, EffectiveDate = i.EffectiveDate
				, EndDate = i.EndDate
				, RateTypeID = i.RateTypeID
				, UomID = i.UomID
				, LastChangeDateUTC = i.LastChangeDateUTC
				, LastChangedByUser = i.LastChangedByUser
		FROM tblCarrierRouteRate X
		JOIN inserted i ON i.ID = X.ID
		JOIN tblRoute R ON R.OriginID = i.OriginID AND R.DestinationID = i.DestinationID 

		-- PRINT 'insert any new records'
		INSERT INTO tblCarrierRouteRate (CarrierID, ProductGroupID, DriverGroupID, RouteID, Rate, EffectiveDate, EndDate, RateTypeID, UomID, CreatedByUser)
			SELECT nullif(CarrierID, 0), nullif(ProductGroupID, 0), nullif(DriverGroupID, 0), R.ID, Rate, EffectiveDate, EndDate, RateTypeID, UomID, i.CreatedByUser
			FROM inserted i
			JOIN tblRoute R ON R.OriginID = i.OriginID AND R.DestinationID = i.DestinationID 
			WHERE ISNULL(i.ID, 0) = 0
	END TRY
	BEGIN CATCH
		DECLARE @error varchar(255)
		SET @error = ERROR_MESSAGE()
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END CATCH
END
GO

COMMIT
SET NOEXEC OFF