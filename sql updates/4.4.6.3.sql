SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.4.6.2'
SELECT  @NewVersion = '4.4.6.3'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'DCWEB-1978 HOS - Amend how Sleeper Berth Splits are determined'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


INSERT INTO tblCarrierRuleType
VALUES 
(23, 'HOS - Sleeper-Berth Split Total Hours', 3, 1, 'Total number of hours that resets a split sleeper-berth. (Default is 10)'),
(24, 'HOS - Sleeper-Berth Split Min Hours', 3, 1, 'Minimum number of hours that constitues a qualified break when determining the sleeper provision. (Default is 2)')

GO


/********************************************
-- Date Created: 2016 Sep 21
-- Author: Joe Engler
-- Purpose: Get summary counts from HOS records and determine if violations occur.  Use passed date minus a shift for a starting point
-- Changes:
--		4.2.5		2016-10-27		JAE			Added Weekly OnDuty fields
--		4.3.1		2016-11-04		JAE			Fixed on duty daily hours to only include on duty (breaks should not be included)
--		4.4.6.3		2016-12-02		JAE			Alter how Sleeper Berth is calculated
********************************************/
ALTER FUNCTION fnHosViolationDetail(@DriverID INT, @StartDate DATETIME, @EndDate DATETIME)
RETURNS @ret TABLE
(
	DriverID INT,
	LogDateUTC DATETIME, 
	EndDateUTC DATETIME, 
	TotalHours FLOAT, 
	TotalHoursSleeping FLOAT,
	TotalHoursDriving FLOAT,
	SleeperStatusID INT,
	WeeklyReset BIT,
	DailyReset BIT,
	SleepBreak BIT, 
	DriverBreak BIT,
	Status VARCHAR(20),
	SleeperReset BIT,
	LastWeeklyReset DATETIME,
	LastDailyReset DATETIME,
	LastBreak DATETIME,
	LastSleeperBerthReset DATETIME,
	HoursSinceWeeklyReset FLOAT,
	OnDutyHoursSinceWeeklyReset FLOAT,
	DrivingHoursSinceWeeklyReset FLOAT,
	OnDutyHoursSinceDailyReset FLOAT,
	DrivingHoursSinceDailyReset FLOAT,
	OnDutyDailyLimit FLOAT,
	HoursSinceBreak FLOAT,
	OnDutyHoursLeft FLOAT,
	OnDutyViolation BIT,
	DrivingDailyLimit FLOAT,
	DrivingHoursLeft FLOAT,
	DrivingViolation BIT, 
	BreakLimit FLOAT,
	HoursTilBreak FLOAT,
	BreakViolation BIT,
	WeeklyOnDutyLimit FLOAT,
	WeeklyOnDutyHoursLeft FLOAT,
	WeeklyOnDutyViolation BIT,
	WeeklyDrivingLimit FLOAT,
	WeeklyDrivingHoursLeft FLOAT,
	WeeklyDrivingViolation BIT,
	OnDutyViolationDateUTC DATETIME,
	DrivingViolationDateUTC DATETIME,
	BreakViolationDateUTC DATETIME,
	WeeklyOnDutyViolationDateUTC DATETIME,
	WeeklyDrivingViolationDateUTC DATETIME
)
AS BEGIN

	DECLARE @CarrierRules TABLE
	(
		ID INT,
		Value VARCHAR(255)
	)

	INSERT INTO @CarrierRules
	SELECT cr.TypeID, cr.Value
	FROM viewDriverBase d
	CROSS APPLY dbo.fnCarrierRules(@EndDate, null, null, d.CarrierID, d.ID, d.OperatingStateID, d.RegionID, 1) cr
	WHERE d.id = @DriverID

	DECLARE @WEEKLYRESET FLOAT = ISNULL((SELECT NULLIF(dbo.fnToFloat(Value),0) from @CarrierRules WHERE ID = 12), 34)
	DECLARE @DAILYRESET FLOAT = ISNULL((SELECT NULLIF(dbo.fnToFloat(Value),0) from @CarrierRules WHERE ID = 15), 10)
	DECLARE @SLEEPERBREAK FLOAT = ISNULL((SELECT NULLIF(dbo.fnToFloat(Value),0) from @CarrierRules WHERE ID = 17), 8)
	DECLARE @BREAK FLOAT = ISNULL((SELECT NULLIF(dbo.fnToFloat(Value),0) from @CarrierRules WHERE ID = 19), 30)/60.0
	DECLARE @SLEEPERRESET FLOAT = ISNULL((SELECT NULLIF(dbo.fnToFloat(Value),0) from @CarrierRules WHERE ID = 23), 10)
	DECLARE @QUALIFIEDREST FLOAT = ISNULL((SELECT NULLIF(dbo.fnToFloat(Value),0) from @CarrierRules WHERE ID = 24), 2)
	DECLARE @WEEK FLOAT = ISNULL((SELECT NULLIF(dbo.fnToFloat(Value),0) from @CarrierRules WHERE ID = 16), 7)
	DECLARE @ONDUTY_WEEKLY_LIMIT FLOAT = ISNULL((SELECT NULLIF(dbo.fnToFloat(Value),0) from @CarrierRules WHERE ID = 8), 60)
	DECLARE @DRIVING_WEEKLY_LIMIT FLOAT = ISNULL((SELECT NULLIF(dbo.fnToFloat(Value),0) from @CarrierRules WHERE ID = 6), 60)
	DECLARE @ONDUTY_DAILY_LIMIT FLOAT = ISNULL((SELECT NULLIF(dbo.fnToFloat(Value),0) from @CarrierRules WHERE ID = 7), 14)
	DECLARE @DRIVING_DAILY_LIMIT FLOAT = ISNULL((SELECT NULLIF(dbo.fnToFloat(Value),0) from @CarrierRules WHERE ID = 5), 11)
	DECLARE @DRIVING_BREAK_LIMIT FLOAT = ISNULL((SELECT NULLIF(dbo.fnToFloat(Value),0) from @CarrierRules WHERE ID = 18), 8)

	IF @StartDate IS NULL
		SELECT @StartDate = DATEADD(DAY, -1, @EndDate) -- one day

	-- Get records up to previous shift for calculating violations, these older records will get filtered out later
	DECLARE @StartDateX DATETIME = DATEADD(DAY, -@WEEK, @StartDate) -- one shift


	-- Core HOS data with the basic info filled in (i.e. is this a weekly reset, daily reset, etc)
	-- Sleeper and Off Duty are grouped together for summing off duty blocks
	DECLARE @tmpHOS TABLE 
	(
		LogDateUTC DATETIME, 
		EndDateUTC DATETIME, 
		TotalHours FLOAT, 
		TotalHoursSleeping FLOAT,
		TotalHoursDriving FLOAT,
		SleeperStatusID INT,
		WeeklyReset INT,
		DailyReset INT,
		SleepBreak INT, -- As INT for multiplying as factor 
		DriverBreak INT,
		Status VARCHAR(20)
	)

	INSERT INTO @tmpHOS
	SELECT qq.*,
		WeeklyReset = CASE WHEN SleeperStatusID = 1 AND TotalHours >= @WEEKLYRESET THEN 1 ELSE 0 END,
		DailyReset = CASE WHEN SleeperStatusID = 1 AND TotalHours >= @DAILYRESET THEN 1 ELSE 0 END,
		SleepBreak = CASE WHEN SleeperStatusID = 2 AND TotalHours >= @SLEEPERBREAK THEN 1 
						 WHEN SleeperStatusID = 1 AND TotalHours >= @DAILYRESET THEN 1 ELSE 0 END,
		DriverBreak = CASE WHEN SleeperStatusID IN (1,2) AND TotalHours >= @BREAK THEN 1 ELSE 0 END,
		Status = CASE WHEN SleeperStatusID = 1 AND TotalHours >= @WEEKLYRESET THEN 'WEEKLY RESET'
							WHEN SleeperStatusID = 1 AND TotalHours >= @DAILYRESET THEN 'DAILY RESET' 
							WHEN SleeperStatusID = 2 AND TotalHours < @DAILYRESET AND TotalHours >= @SLEEPERBREAK THEN 'SLEEP BREAK'
							WHEN SleeperStatusID = 2 AND TotalHours < @DAILYRESET AND TotalHours >= @BREAK THEN 'BREAK' ELSE '' END
	FROM (
		SELECT LogDateUTC = MIN(LogDateUTC), EndDateUTC = MAX(EndDateUTC), TotalHours = SUM(TotalHours), 
			TotalHoursSleeping = SUM(CASE WHEN HosDriverStatusID = 2 THEN TotalHours ELSE NULL END),
			TotalHoursDriving = SUM(CASE WHEN HosDriverStatusID = 3 THEN TotalHours ELSE NULL END),
			SleeperStatusID = CASE WHEN SleeperStatusID = 1 AND SUM(TotalHours) < @DAILYRESET THEN 2 --break
								ELSE SleeperStatusID END
		FROM
		(
			SELECT LogDateUTC, EndDateUTC, 
				TotalHours, 
				SleeperStatusID, HosDriverStatusID,
				Streak = (SELECT COUNT(*) FROM dbo.fnHosSummary(@driverid, @StartDateX, @EndDate) x
								WHERE x.SleeperStatusID <> y.SleeperStatusID AND x.LogDateUTC <= y.LogDateUTC) -- used just to group streaks
			FROM dbo.fnHosSummary(@DriverID, @StartDateX, @EndDate) y
		) Q
		GROUP BY Streak, SleeperStatusID
	) QQ
	WHERE TotalHours > 0 -- Ignore any "quick" changes
	ORDER BY LogDateUTC


	------------------------------------------

	-- Get core HOS data with "last" dates and sleeper reset information
	DECLARE @tmpHOSwithSleeperReset TABLE 
	(
		LogDateUTC DATETIME, 
		EndDateUTC DATETIME, 
		TotalHours FLOAT, 
		TotalHoursSleeping FLOAT,
		TotalHoursDriving FLOAT,
		SleeperStatusID INT,
		WeeklyReset INT,
		DailyReset INT,
		SleepBreak INT, -- As INT for multiplying as factor 
		DriverBreak INT,
		Status VARCHAR(20),
		SleeperReset INT,
		LastWeeklyReset DATETIME,
		LastDailyReset DATETIME,
		LastBreak DATETIME
	)

	INSERT INTO @tmpHOSwithSleeperReset 
	(
		LogDateUTC,
		EndDateUTC, 
		TotalHours, 
		TotalHoursSleeping,
		TotalHoursDriving,
		SleeperStatusID,
		WeeklyReset,
		DailyReset,
		SleepBreak, 
		DriverBreak,
		Status,
		SleeperReset,
		LastWeeklyReset,
		LastDailyReset,
		LastBreak
	)
	SELECT hos.*, 
		SleeperReset = CASE WHEN lsb.Hrs >= @SLEEPERRESET OR hos.DailyReset = 1 THEN 1 ELSE 0 END,

		LastWeeklyReset = ISNULL(lr.EndDateUTC, @StartDateX),
		LastDailyReset = COALESCE(ldr.EndDateUTC, lr.EndDateUTC, @StartDateX),
		LastBreak = COALESCE(lb.EndDateUTC, ldr.EndDateUTC, lr.EndDateUTC, @StartDateX)

	FROM @tmphos hos
	-- Last Weekly Reset
	OUTER APPLY (SELECT TOP 1 EndDateUTC 
					FROM @tmphos
					WHERE WeeklyReset = 1 AND LogDateUTC < hos.LogDateUTC 
					ORDER BY LogDateUTC DESC) lr
	-- Last Daily Reset
	OUTER APPLY (SELECT TOP 1 EndDateUTC 
					FROM @tmphos
					WHERE DailyReset = 1 AND LogDateUTC < hos.LogDateUTC 
					ORDER BY LogDateUTC DESC) ldr
	-- Last Break
	OUTER APPLY (SELECT TOP 1 EndDateUTC 
					FROM @tmphos
					WHERE DriverBreak = 1 AND LogDateUTC < hos.LogDateUTC 
					ORDER BY LogDateUTC DESC) lb
	-- Last Sleeper Berth Reset (used to see if a reset, date is grabbed later since you use the previous break)
	OUTER APPLY (SELECT EndDateUTC = MIN(EndDateUTC), 
						Hrs = SUM(ISNULL(TotalHoursSleeping,0)) --*MAX(ISNULL(SleepBreak,0)) -- Multiply by sleep break to ensure one of the entries is an 8 hr block
					FROM @tmphos
					WHERE DriverBreak = 1  AND logDateUTC < hos.EndDateUTC 
					AND TotalHoursSleeping >= @QUALIFIEDREST -- Both must be a qualified rest break to count
					AND EndDateUTC >= COALESCE(lb.EndDateUTC, ldr.EndDateUTC)) lsb -- Since Last Daily Reset


	------------------------------------------


	-- Get detail HOS information with total hours and violations
	INSERT INTO @ret (
		DriverID,
		LogDateUTC, 
		EndDateUTC, 
		TotalHours, 
		TotalHoursSleeping,
		TotalHoursDriving,
		SleeperStatusID,
		WeeklyReset,
		DailyReset,
		SleepBreak, 
		DriverBreak,
		Status,
		SleeperReset,
		LastWeeklyReset,
		LastDailyReset,
		LastBreak,
		LastSleeperBerthReset,
		HoursSinceWeeklyReset,
		OnDutyHoursSinceWeeklyReset,
		DrivingHoursSinceWeeklyReset,
		OnDutyHoursSinceDailyReset,
		DrivingHoursSinceDailyReset,
		HoursSinceBreak,
		OnDutyDailyLimit,
		OnDutyHoursLeft,
		OnDutyViolation,
		DrivingDailyLimit,
		DrivingHoursLeft,
		DrivingViolation,
		BreakLimit,
		HoursTilBreak,
		BreakViolation,
		WeeklyOnDutyLimit,
		WeeklyOnDutyHoursLeft,
		WeeklyOnDutyViolation,
		WeeklyDrivingLimit,
		WeeklyDrivingHoursLeft,
		WeeklyDrivingViolation,
		OnDutyViolationDateUTC,
		DrivingViolationDateUTC,
		BreakViolationDateUTC,
		WeeklyOnDutyViolationDateUTC,
		WeeklyDrivingViolationDateUTC
	)
	SELECT @DriverID,
		*,
		OnDutyViolationDateUTC = CASE WHEN OnDutyViolation = 0 THEN NULL
				ELSE DATEADD(MINUTE, 60*(TotalHours - (OnDutyHoursSinceDailyReset - OnDutyDailyLimit)), LogDateUTC) END,
		DrivingViolationDateUTC = CASE WHEN DrivingViolation = 0 THEN NULL
				ELSE dbo.fnMaxDateTime(DATEADD(MINUTE, 60*(TotalHours - (DrivingHoursSinceDailyReset - DrivingDailyLimit)), LogDateUTC), LogDateUTC) END, -- max of time or start of driving time (violation doesnt occur until you start driving)
		BreakViolationDateUTC = CASE WHEN BreakViolation = 0 THEN NULL
				ELSE dbo.fnMaxDateTime(DATEADD(MINUTE, 60*(TotalHours + HoursTilBreak), LogDateUTC), LogDateUTC) END, -- max of time or start of driving time (violation doesnt occur until you start driving)
		WeeklyOnDutyViolationDateUTC = CASE WHEN WeeklyOnDutyViolation = 0 THEN NULL
				ELSE DATEADD(MINUTE, 60*(TotalHours - (OnDutyHoursSinceWeeklyReset - WeeklyOnDutyLimit)), LogDateUTC) END,
		WeeklyDrivingViolationDateUTC = CASE WHEN WeeklyDrivingViolation = 0 THEN NULL
				ELSE DATEADD(MINUTE, 60*(TotalHours - (DrivingHoursSinceWeeklyReset - WeeklyDrivingLimit)), LogDateUTC) END
	FROM 
	(
		SELECT hos.*,
			OnDutyDailyLimit = @ONDUTY_DAILY_LIMIT,
			OnDutyHoursLeft = CASE WHEN @ONDUTY_DAILY_LIMIT < OnDutyHoursSinceDailyReset THEN 0 ELSE @ONDUTY_DAILY_LIMIT - OnDutyHoursSinceDailyReset END,
			OnDutyViolation = CASE WHEN @ONDUTY_DAILY_LIMIT < OnDutyHoursSinceDailyReset AND SleeperStatusID IN (3,4) THEN 1 ELSE 0 END,

			DrivingDailyLimit = @DRIVING_DAILY_LIMIT,
			DrivingHoursLeft = CASE WHEN @DRIVING_DAILY_LIMIT < DrivingHoursSinceDailyReset THEN 0 ELSE @DRIVING_DAILY_LIMIT - DrivingHoursSinceDailyReset END,
			DrivingViolation = CASE WHEN @DRIVING_DAILY_LIMIT < DrivingHoursSinceDailyReset AND SleeperStatusID = 3 THEN 1 ELSE 0 END,

			BreakLimit = @DRIVING_BREAK_LIMIT,
			HoursTilBreak = @DRIVING_BREAK_LIMIT - HoursSinceBreak,
			BreakViolation = CASE WHEN @DRIVING_BREAK_LIMIT < HoursSinceBreak AND SleeperStatusID = 3 THEN 1 ELSE 0 END,

			WeeklyOnDutyLimit = @ONDUTY_WEEKLY_LIMIT,
			WeeklyOnDutyHoursLeft = CASE WHEN @ONDUTY_WEEKLY_LIMIT < OnDutyHoursSinceWeeklyReset THEN 0 ELSE @ONDUTY_WEEKLY_LIMIT - OnDutyHoursSinceWeeklyReset END,
			WeeklyOnDutyViolation = CASE WHEN @ONDUTY_WEEKLY_LIMIT < OnDutyHoursSinceWeeklyReset AND SleeperStatusID IN (3,4) THEN 1 ELSE 0 END,

			WeeklyDrivingLimit = @DRIVING_WEEKLY_LIMIT,
			WeeklyDrivingHoursLeft = CASE WHEN @DRIVING_WEEKLY_LIMIT < DrivingHoursSinceWeeklyReset THEN 0 ELSE @DRIVING_WEEKLY_LIMIT - DrivingHoursSinceWeeklyReset END,
			WeeklyDrivingViolation = CASE WHEN @DRIVING_WEEKLY_LIMIT < DrivingHoursSinceWeeklyReset AND SleeperStatusID = 3 THEN 1 ELSE 0 END
		FROM
		(
			SELECT hos.*,
				HoursSinceWeeklyReset = 
							CASE WHEN WeeklyReset = 1 THEN 0
							ELSE ISNULL((SELECT SUM(TotalHours) FROM @tmphos WHERE LogDateUTC >= LastWeeklyReset AND LogDateUTC < hos.EndDateUTC), 0) END,

				OnDutyHoursSinceWeeklyReset = 
							CASE WHEN WeeklyReset = 1 THEN 0
							ELSE ISNULL((SELECT SUM(TotalHours) FROM @tmphos WHERE LogDateUTC >= LastWeeklyReset AND LogDateUTC < hos.EndDateUTC 
																			AND SleeperStatusID IN (3,4)), 0) END,
				DrivingHoursSinceWeeklyReset = 
							CASE WHEN WeeklyReset = 1 THEN 0
							ELSE ISNULL((SELECT SUM(TotalHours) FROM @tmphos WHERE LogDateUTC >= LastWeeklyReset AND LogDateUTC < hos.EndDateUTC 
																			AND SleeperStatusID = 3), 0) END,
				OnDutyHoursSinceDailyReset = 
							CASE WHEN DailyReset = 1 THEN 0
							ELSE ISNULL((SELECT SUM(TotalHours) FROM @tmphos WHERE LogDateUTC >= LastSleeperBerthReset AND LogDateUTC < hos.EndDateUTC 
																			--AND SleepBreak = 0 -- Skip sleeper block
																			AND SleeperStatusID IN (3,4)), 0) END, 
				DrivingHoursSinceDailyReset = 
							CASE WHEN DailyReset = 1 THEN 0
							ELSE ISNULL((SELECT SUM(TotalHours) FROM @tmphos WHERE LogDateUTC >= LastSleeperBerthReset AND LogDateUTC < hos.EndDateUTC 
																			AND SleeperStatusID = 3), 0) END,
				HoursSinceBreak = 
							CASE WHEN DriverBreak = 1 THEN 0
							ELSE ISNULL((SELECT SUM(TotalHours) FROM @tmphos WHERE SleepBreak = 0 AND LogDateUTC >= LastBreak AND LogDateUTC < hos.EndDateUTC),0) END
			FROM 
			(
				SELECT hos.*,
					LastSleeperBerthReset = dbo.fnMaxDateTime(lb.LastBreak, LastDailyReset) -- get more recent between sleeper reset or daily reset

				FROM @tmpHOSwithSleeperReset hos
				-- Last Sleeper Berth Reset
				OUTER APPLY (SELECT TOP 1 LastBreak
									FROM @tmpHOSwithSleeperReset
									WHERE SleeperReset = 1 AND LogDateUTC < hos.LogDateUTC
									ORDER BY LogDateUTC DESC) lb
			) hos
		) hos
	) Q
	WHERE EndDateUTC > @StartDate -- Filter older records used for calculating violations

	RETURN
END

GO


COMMIT
SET NOEXEC OFF
