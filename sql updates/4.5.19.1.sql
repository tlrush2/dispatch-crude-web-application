SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.5.19'
SELECT  @NewVersion = '4.5.19.1'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'JT-2710 - Assessorial to Accessorial'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


UPDATE aspnet_Roles SET Description = 'Allow user to view the Accessorial Rate Types page', FriendlyName = 'Accessorial Rate Types' WHERE RoleName = 'viewAssessorialRateTypes'
GO
UPDATE aspnet_Roles SET Description = 'Allow user to view the Carrier Accessorial Rates page', FriendlyName = 'Accessorial' WHERE RoleName = 'viewCarrierAssessorialRates'
GO
UPDATE aspnet_Roles SET Description = 'Allow user to view the Driver Accessorial Rates page', FriendlyName = 'Accessorial' WHERE RoleName = 'viewDriverAssessorialRates'
GO
UPDATE aspnet_Roles SET Description = 'Allow user to view the Shipper Accessorial Rates page', FriendlyName = 'Accessorial' WHERE RoleName = 'viewShipperAssessorialRates'
GO


COMMIT
SET NOEXEC OFF