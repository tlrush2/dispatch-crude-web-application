SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.6.2'
SELECT  @NewVersion = '4.6.3'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'JT-170 - Follow up to PDF Export'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/***********************************/
-- Created: 3.13.2 - 2016/07/08 - Kevin Alons
-- Purpose: return Report Center Order data without GPS data
-- Changes:
	-- 4.4.1	2016/11/04 - JAE - Add Destination Chainup
	-- 4.4.17	2017/01/10 - BSB - Add Settlement Batch Date and Invoice Num for Driver, Carrier, and Producer Settlement + Batch Date for Shipper
	-- 4.5.15	2017/03/06 - BSB - Add order approval notes
	-- 4.6.3	2017/04/25 - JAE - Add Settlement Notes and period End dates for Driver, Carrier and Shipper
/***********************************/
ALTER VIEW viewReportCenter_Orders_NoGPS AS
	SELECT X.*
	FROM (
		SELECT O.*
			--
			, ShipperBatchNum = SS.BatchNum
			, ShipperBatchDate = SSB.BatchDate		-- 4.4.17
			, ShipperBatchInvoiceNum = SSB.InvoiceNum
			, ShipperSettlementUomID = SS.SettlementUomID
			, ShipperSettlementUom = SS.SettlementUom
			, ShipperMinSettlementUnits = SS.MinSettlementUnits
			, ShipperSettlementUnits = SS.SettlementUnits
			, ShipperRateSheetRate = SS.RateSheetRate
			, ShipperRateSheetRateType = SS.RateSheetRateType
			, ShipperRouteRate = SS.RouteRate
			, ShipperRouteRateType = SS.RouteRateType
			, ShipperLoadRate = ISNULL(SS.RouteRate, SS.RateSheetRate)
			, ShipperLoadRateType = ISNULL(SS.RouteRateType, SS.RateSheetRateType)
			, ShipperLoadAmount = SS.LoadAmount
			, ShipperOrderRejectRate = SS.OrderRejectRate 
			, ShipperOrderRejectRateType = SS.OrderRejectRateType 
			, ShipperOrderRejectAmount = SS.OrderRejectAmount 
			, ShipperWaitFeeSubUnit = SS.WaitFeeSubUnit  
			, ShipperWaitFeeRoundingType = SS.WaitFeeRoundingType  
			, ShipperOriginWaitBillableHours = SS.OriginWaitBillableHours  
			, ShipperOriginWaitBillableMinutes = SS.OriginWaitBillableMinutes  
			, ShipperOriginWaitRate = SS.OriginWaitRate
			, ShipperOriginWaitAmount = SS.OriginWaitAmount
			, ShipperDestinationWaitBillableHours = SS.DestinationWaitBillableHours 
			, ShipperDestinationWaitBillableMinutes = SS.DestinationWaitBillableMinutes   
			, ShipperDestinationWaitRate = SS.DestinationWaitRate  
			, ShipperDestinationWaitAmount = SS.DestinationWaitAmount  
			, ShipperTotalWaitAmount = SS.TotalWaitAmount
			, ShipperTotalWaitBillableMinutes = NULLIF(ISNULL(SS.OriginWaitBillableMinutes, 0) + ISNULL(SS.DestinationWaitBillableMinutes, 0), 0)
			, ShipperTotalWaitBillableHours = NULLIF(ISNULL(SS.OriginWaitBillableHours, 0) + ISNULL(SS.DestinationWaitBillableHours, 0), 0)		
			, ShipperFuelSurchargeRate = SS.FuelSurchargeRate
			, ShipperFuelSurchargeAmount = SS.FuelSurchargeAmount
			, ShipperOriginChainupRate = SS.OriginChainupRate
			, ShipperOriginChainupRateType = SS.OriginChainupRateType  
			, ShipperOriginChainupAmount = SS.OriginChainupAmount
			, ShipperDestChainupRate = SS.DestChainupRate
			, ShipperDestChainupRateType = SS.DestChainupRateType  
			, ShipperDestChainupAmount = SS.DestChainupAmount
			, ShipperRerouteRate = SS.RerouteRate
			, ShipperRerouteRateType = SS.RerouteRateType  
			, ShipperRerouteAmount = SS.RerouteAmount
			, ShipperSplitLoadRate = SS.SplitLoadRate
			, ShipperSplitLoadRateType = SS.SplitLoadRateType  
			, ShipperSplitLoadAmount = SS.SplitLoadAmount
			, ShipperH2SRate = SS.H2SRate
			, ShipperH2SRateType = SS.H2SRateType  
			, ShipperH2SAmount = SS.H2SAmount
			, ShipperTaxRate = SS.OriginTaxRate
			, ShipperTotalAmount = SS.TotalAmount
			, ShipperDestCode = CDC.Code
			, ShipperTicketType = STT.TicketType
			, ShipperPeriodEndDate = SS.PeriodEndDate
			, ShipperNotes = SS.Notes
			--
			, CarrierBatchNum = SC.BatchNum			
			, CarrierBatchDate = CSB.BatchDate		-- 4.4.17
			, CarrierBatchInvoiceNum = CSB.InvoiceNum		-- 4.4.17
			, CarrierSettlementUomID = SC.SettlementUomID
			, CarrierSettlementUom = SC.SettlementUom
			, CarrierMinSettlementUnits = SC.MinSettlementUnits
			, CarrierSettlementUnits = SC.SettlementUnits
			, CarrierRateSheetRate = SC.RateSheetRate
			, CarrierRateSheetRateType = SC.RateSheetRateType
			, CarrierRouteRate = SC.RouteRate
			, CarrierRouteRateType = SC.RouteRateType
			, CarrierLoadRate = ISNULL(SC.RouteRate, SC.RateSheetRate)
			, CarrierLoadRateType = ISNULL(SC.RouteRateType, SC.RateSheetRateType)
			, CarrierLoadAmount = SC.LoadAmount
			, CarrierOrderRejectRate = SC.OrderRejectRate 
			, CarrierOrderRejectRateType = SC.OrderRejectRateType 
			, CarrierOrderRejectAmount = SC.OrderRejectAmount 
			, CarrierWaitFeeSubUnit = SC.WaitFeeSubUnit  
			, CarrierWaitFeeRoundingType = SC.WaitFeeRoundingType  
			, CarrierOriginWaitBillableHours = SC.OriginWaitBillableHours  
			, CarrierOriginWaitBillableMinutes = SC.OriginWaitBillableMinutes  
			, CarrierOriginWaitRate = SC.OriginWaitRate
			, CarrierOriginWaitAmount = SC.OriginWaitAmount
			, CarrierDestinationWaitBillableHours = SC.DestinationWaitBillableHours 
			, CarrierDestinationWaitBillableMinutes = SC.DestinationWaitBillableMinutes  
			, CarrierDestinationWaitRate = SC.DestinationWaitRate 
			, CarrierDestinationWaitAmount = SC.DestinationWaitAmount  
			, CarrierTotalWaitAmount = SC.TotalWaitAmount
			, CarrierTotalWaitBillableMinutes = NULLIF(ISNULL(SC.OriginWaitBillableMinutes, 0) + ISNULL(SC.DestinationWaitBillableMinutes, 0), 0)
			, CarrierTotalWaitBillableHours = NULLIF(ISNULL(SC.OriginWaitBillableHours, 0) + ISNULL(SC.DestinationWaitBillableHours, 0), 0)		
			, CarrierFuelSurchargeRate = SC.FuelSurchargeRate
			, CarrierFuelSurchargeAmount = SC.FuelSurchargeAmount
			, CarrierOriginChainupRate = SC.OriginChainupRate
			, CarrierOriginChainupRateType = SC.OriginChainupRateType  
			, CarrierOriginChainupAmount = SC.OriginChainupAmount
			, CarrierDestChainupRate = SC.DestChainupRate
			, CarrierDestChainupRateType = SC.DestChainupRateType  
			, CarrierDestChainupAmount = SC.DestChainupAmount
			, CarrierRerouteRate = SC.RerouteRate
			, CarrierRerouteRateType = SC.RerouteRateType  
			, CarrierRerouteAmount = SC.RerouteAmount
			, CarrierSplitLoadRate = SC.SplitLoadRate
			, CarrierSplitLoadRateType = SC.SplitLoadRateType  
			, CarrierSplitLoadAmount = SC.SplitLoadAmount
			, CarrierH2SRate = SC.H2SRate
			, CarrierH2SRateType = SC.H2SRateType  
			, CarrierH2SAmount = SC.H2SAmount
			, CarrierTaxRate = SC.OriginTaxRate
			, CarrierTotalAmount = SC.TotalAmount
			, CarrierPeriodEndDate = SC.PeriodEndDate
			, CarrierNotes = SC.Notes
			--
			, DriverBatchNum = SD.BatchNum
			, DriverBatchDate = DSB.BatchDate		-- 4.4.17
			, DriverBatchInvoiceNum = DSB.InvoiceNum		-- 4.4.17
			, DriverSettlementUomID = SD.SettlementUomID
			, DriverSettlementUom = SD.SettlementUom
			, DriverMinSettlementUnits = SD.MinSettlementUnits
			, DriverSettlementUnits = SD.SettlementUnits
			, DriverRateSheetRate = SD.RateSheetRate
			, DriverRateSheetRateType = SD.RateSheetRateType
			, DriverRouteRate = SD.RouteRate
			, DriverRouteRateType = SD.RouteRateType
			, DriverLoadRate = ISNULL(SD.RouteRate, SD.RateSheetRate)
			, DriverLoadRateType = ISNULL(SD.RouteRateType, SD.RateSheetRateType)
			, DriverLoadAmount = SD.LoadAmount
			, DriverOrderRejectRate = SD.OrderRejectRate 
			, DriverOrderRejectRateType = SD.OrderRejectRateType 
			, DriverOrderRejectAmount = SD.OrderRejectAmount 
			, DriverWaitFeeSubUnit = SD.WaitFeeSubUnit  
			, DriverWaitFeeRoundingType = SD.WaitFeeRoundingType  
			, DriverOriginWaitBillableHours = SD.OriginWaitBillableHours  
			, DriverOriginWaitBillableMinutes = SD.OriginWaitBillableMinutes  
			, DriverOriginWaitRate = SD.OriginWaitRate
			, DriverOriginWaitAmount = SD.OriginWaitAmount
			, DriverDestinationWaitBillableHours = SD.DestinationWaitBillableHours 
			, DriverDestinationWaitBillableMinutes = SD.DestinationWaitBillableMinutes  
			, DriverDestinationWaitRate = SD.DestinationWaitRate 
			, DriverDestinationWaitAmount = SD.DestinationWaitAmount  
			, DriverTotalWaitAmount = SD.TotalWaitAmount
			, DriverTotalWaitBillableMinutes = NULLIF(ISNULL(SD.OriginWaitBillableMinutes, 0) + ISNULL(SD.DestinationWaitBillableMinutes, 0), 0)
			, DriverTotalWaitBillableHours = NULLIF(ISNULL(SD.OriginWaitBillableHours, 0) + ISNULL(SD.DestinationWaitBillableHours, 0), 0)		
			, DriverFuelSurchargeRate = SD.FuelSurchargeRate
			, DriverFuelSurchargeAmount = SD.FuelSurchargeAmount
			, DriverOriginChainupRate = SD.OriginChainupRate
			, DriverOriginChainupRateType = SD.OriginChainupRateType  
			, DriverOriginChainupAmount = SD.OriginChainupAmount
			, DriverDestChainupRate = SD.DestChainupRate
			, DriverDestChainupRateType = SD.DestChainupRateType  
			, DriverDestChainupAmount = SD.DestChainupAmount
			, DriverRerouteRate = SD.RerouteRate
			, DriverRerouteRateType = SD.RerouteRateType  
			, DriverRerouteAmount = SD.RerouteAmount
			, DriverSplitLoadRate = SD.SplitLoadRate
			, DriverSplitLoadRateType = SD.SplitLoadRateType  
			, DriverSplitLoadAmount = SD.SplitLoadAmount
			, DriverH2SRate = SD.H2SRate
			, DriverH2SRateType = SD.H2SRateType  
			, DriverH2SAmount = SD.H2SAmount
			, DriverTaxRate = SD.OriginTaxRate
			, DriverTotalAmount = SD.TotalAmount
			, DriverPeriodEndDate = SD.PeriodEndDate
			, DriverNotes = SD.Notes
			--
			, ProducerBatchNum = SP.BatchNum
			, ProducerBatchDate = PSB.BatchDate		-- 4.4.17
			, ProducerBatchInvoiceNum = PSB.InvoiceNum		-- 4.4.17
			, ProducerSettlementUomID = SP.SettlementUomID
			, ProducerSettlementUom = SP.SettlementUom
			, ProducerSettlementUnits = SP.SettlementUnits
			, ProducerCommodityIndex = SP.CommodityIndex
			, ProducerCommodityIndexID = SP.CommodityIndexID
			, ProducerCommodityMethod = SP.CommodityMethod
			, ProducerCommodityMethodID = SP.CommodityMethodID
			, ProducerIndexPrice = SP.Price
			, ProducerIndexStartDate = SP.IndexStartDate
			, ProducerIndexEndDate = SP.IndexEndDate
			, ProducerPriceAmount = SP.CommodityPurchasePricebookPriceAmount
			, ProducerDeduct = SP.Deduct
			, ProducerDeductType = SP.Deduct
			, ProducerDeductAmount = SP.CommodityPurchasePricebookDeductAmount
			, ProducerPremium = SP.PremiumType
			, ProducerPremiumType = SP.PremiumType
			, ProducerPremiumAmount = SP.CommodityPurchasePricebookPremiumAmount
			, ProducerPremiumDesc = SP.PremiumDesc
			, ProducerTotalAmount = SP.CommodityPurchasePricebookTotalAmount
			--
			, OriginLatLon = LTRIM(OO.LAT) + ',' + LTRIM(OO.LON)
			, OriginGeoFenceRadiusMeters = OO.GeoFenceRadiusMeters
			, OriginCTBNum = OO.CTBNum
			, OriginFieldName = OO.FieldName
			, OriginCity = OO.City																				
			, OriginCityState = OO.City + ', ' + (SELECT Abbreviation FROM tblState WHERE ID = OO.StateID)		
			--
			, DestLatLon = LTRIM(D.LAT) + ',' + LTRIM(D.LON)
			, DestGeoFenceRadiusMeters = D.GeoFenceRadiusMeters
			, DestCity = D.City
			, DestCityState = D.City + ', ' + (SELECT Abbreviation FROM tblState WHERE ID = D.StateID) -- 4.1.0 - fix (was pointing to Origin.StateID)
			--
			, Gauger = GAO.Gauger						
			, GaugerIDNumber = GA.IDNumber
			, GaugerFirstName = GA.FirstName
			, GaugerLastName = GA.LastName
			, GaugerRejected = GAO.Rejected
			, GaugerRejectReasonID = GAO.RejectReasonID
			, GaugerRejectNotes = GAO.RejectNotes
			, GaugerRejectNumDesc = GORR.NumDesc
			, GaugerPrintDateUTC = GAO.PrintDateUTC -- 4.1.0 - added for use by GaugerPrintDate EXPRESSION column for effiency reasons (this is already queried, avoid doing the calculation unless needed)
			--, GaugerPrintDate = dbo.fnUTC_to_Local(GAO.PrintDateUTC, O.OriginTimeZoneID, O.OriginUseDST)
			--
			, T_GaugerCarrierTicketNum = CASE WHEN GAO.TicketCount = 0 THEN LTRIM(GAO.OrderNum) + CASE WHEN GAO.Rejected = 1 THEN 'X' ELSE '' END ELSE GOT.CarrierTicketNum END 
			, T_GaugerTankNum = ISNULL(GOT.OriginTankText, GAO.OriginTankText)
			, T_GaugerIsStrappedTank = GOT.IsStrappedTank 
			, T_GaugerProductObsTemp = GOT.ProductObsTemp
			, T_GaugerProductObsGravity = GOT.ProductObsGravity
			, T_GaugerProductBSW = GOT.ProductBSW		
			, T_GaugerOpeningGaugeFeet = GOT.OpeningGaugeFeet
			, T_GaugerOpeningGaugeInch = GOT.OpeningGaugeInch		
			, T_GaugerOpeningGaugeQ = GOT.OpeningGaugeQ			
			, T_GaugerBottomFeet = GOT.BottomFeet
			, T_GaugerBottomInches = GOT.BottomInches		
			, T_GaugerBottomQ = GOT.BottomQ		
			, T_GaugerRejected = GOT.Rejected
			, T_GaugerRejectReasonID = GOT.RejectReasonID
			, T_GaugerRejectNumDesc = GOT.RejectNumDesc
			, T_GaugerRejectNotes = GOT.RejectNotes	
			--
			--, OrderCreateDate = dbo.fnUTC_to_Local(O.CreateDateUTC, O.OriginTimeZoneID, O.OriginUseDST) -- replaced 4.1.0 with an expression type value (more efficient, only executed when needed)
			, OrderApproved = CAST(ISNULL(OA.Approved, 0) AS BIT)
			, ApprovalNotes = OA.Notes		-- 4.5.15
		FROM viewOrder_OrderTicket_Full O
		JOIN tblOrigin OO ON OO.ID = O.OriginID
		JOIN tblDestination D ON D.ID = O.DestinationID
		--
		LEFT JOIN viewGaugerOrder GAO ON GAO.OrderID = O.ID			            
		LEFT JOIN viewGaugerOrderTicket GOT ON GOT.UID = O.T_UID	            
		LEFT JOIN viewGauger GA ON GA.ID = GAO.GaugerID				            
		LEFT JOIN viewOrderRejectReason GORR ON GORR.ID = GAO.RejectReasonID 
		--
		LEFT JOIN tblCustomerDestinationCode CDC ON CDC.CustomerID = O.CustomerID AND CDC.DestinationID = O.DestinationID
		--
		LEFT JOIN viewOrderSettlementCarrier SC ON SC.OrderID = O.ID
		LEFT JOIN tblCarrierSettlementBatch CSB ON CSB.ID = SC.BatchID		-- 4.4.17
		--
		LEFT JOIN viewOrderSettlementShipper SS ON SS.OrderID = O.ID
		LEFT JOIN tblShipperSettlementBatch SSB ON SSB.ID = SS.BatchID
		--
		LEFT JOIN viewOrderSettlementDriver SD ON SD.OrderID = O.ID
		LEFT JOIN tblDriverSettlementBatch DSB ON DSB.ID = SD.BatchID		-- 4.4.17
		--
		LEFT JOIN viewOrderSettlementProducer SP ON SP.OrderID = O.ID
		LEFT JOIN tblProducerSettlementBatch PSB ON PSB.ID = SP.BatchID		-- 4.4.17
		--
		LEFT JOIN tblShipperTicketType STT ON STT.CustomerID = O.CustomerID AND STT.TicketTypeID = O.TicketTypeID
		--
		LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
	) X

GO


EXEC sp_refreshview viewReportCenter_Orders
GO


-- ---------------------------
-- Add fields to ReportCenter
-- --------------------------
SET IDENTITY_INSERT tblReportColumnDefinition ON

INSERT INTO tblReportColumnDefinition 
	(ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport, IsTicketField)
SELECT 376,	1, 'ShipperPeriodEndDate', 'SETTLEMENT | SHIPPER | Shipper Period End Date', 'm/d/yyyy', NULL, 6, NULL, 1, '*', 0, 0
UNION
SELECT 377,	1, 'ShipperNotes', 'SETTLEMENT | SHIPPER | Shipper Settlement Notes', NULL, NULL, 1, NULL, 1, '*', 0, 0
UNION
SELECT 378,	1, 'CarrierPeriodEndDate', 'SETTLEMENT | CARRIER | Carrier Period End Date', 'm/d/yyyy', NULL, 6, NULL, 1, '*', 0, 0
UNION
SELECT 379,	1, 'CarrierNotes', 'SETTLEMENT | CARRIER | Carrier Settlement Notes', NULL, NULL, 1, NULL, 1, '*', 0, 0
UNION
SELECT 380,	1, 'DriverPeriodEndDate', 'SETTLEMENT | DRIVER | Driver Period End Date', 'm/d/yyyy', NULL, 6, NULL, 1, '*', 0, 0
UNION
SELECT 381,	1, 'DriverNotes', 'SETTLEMENT | DRIVER | Driver Settlement Notes', NULL, NULL, 1, NULL, 1, '*', 0, 0
EXCEPT SELECT * FROM tblReportColumnDefinition

SET IDENTITY_INSERT tblReportColumnDefinition OFF
GO


/***********************************/
-- Created: 4.6.2 - 2017.04.20 - Kevin Alons
-- Purpose: return all OrderSettlementDriver "Amounts" as individual rows (only present if value valid), used for "Driver Settlement Statement" pdf exports
-- Changes:
--		4.6.3		2017-04-26		JAE				Suppress rates from this view since it is summing as though it were an amount
/***********************************/
ALTER VIEW viewOrderSettlementDriver_Amounts AS
	SELECT X.BatchID, X.OrderID, X.Name, Amount = isnull(X.Amount, 0.0), X.SortPos
		, OS.BatchOriginDriverID
	FROM (
		SELECT BatchID, OrderID, Name = 'Origin Demurrage', Amount = OriginWaitAmount, SortPos = 1 FROM tblOrderSettlementDriver WHERE OriginWaitAmount IS NOT NULL
		UNION ALL
		SELECT BatchID, OrderID, Name = 'Destination Demurrage', DestinationWaitAmount, SortPos = 2 FROM tblOrderSettlementDriver WHERE DestinationWaitAmount IS NOT NULL
		UNION ALL
		SELECT BatchID, OrderID, Name = 'Origin Chain-up', OriginChainupAmount, SortPos = 3 FROM viewOrderSettlementDriver WHERE OriginChainupAmount IS NOT NULL
		UNION ALL
		SELECT BatchID, OrderID, Name = 'Destination Chain-up', DestChainupAmount, SortPos = 4 FROM viewOrderSettlementDriver WHERE DestChainupAmount IS NOT NULL
		UNION ALL
		SELECT BatchID, OrderID, Name = 'Reject', OrderRejectAmount, SortPos = 5 FROM tblOrderSettlementDriver WHERE OrderRejectAmount IS NOT NULL
		UNION ALL
		SELECT BatchID, OrderID, Name = 'Reroute', RerouteAmount, SortPos = 6 FROM viewOrderSettlementDriver WHERE RerouteAmount IS NOT NULL
		UNION ALL
		SELECT BatchID, OrderID, Name = 'H2S', H2SAmount, SortPos = 7 FROM viewOrderSettlementDriver WHERE H2SAmount IS NOT NULL
		UNION ALL
		SELECT BatchID, OrderID, Name = 'Split Load', SplitLoadAmount, SortPos = 8 FROM viewOrderSettlementDriver WHERE SplitLoadAmount IS NOT NULL
		UNION ALL
		SELECT BatchID, OrderID, Name = 'Load Pay', LoadAmount, SortPos = 9 FROM viewOrderSettlementDriver
		UNION ALL
		/* add any non-system assessorial charges */
		SELECT BatchID, OrderID, RateType, Amount, SortPos = 100 FROM viewOrderSettlementDriverAssessorialCharge WHERE IsSystem = 0
	) X
	JOIN viewOrder_Financial_Base_Driver OS ON OS.BatchID = X.BatchID AND OS.OrderID = X.OrderID

GO


COMMIT
SET NOEXEC OFF