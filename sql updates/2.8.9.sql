/*
	-- yet another fix to Sunoco SUNDEX export (small date logic change in [viewOrderCustomerFinalExportPending] by Ben)
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.8.8'
SELECT  @NewVersion = '2.8.9'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

/**************************************************************/
-- Author: Kevin Alons
-- Date Created: 27 Feb 2014
-- Purpose: return all orderIDs not yet final exported for a Customer
/**************************************************************/
ALTER VIEW [dbo].[viewOrderCustomerFinalExportPending] AS
	SELECT O.ID
		, O.CustomerID
		, CASE WHEN OEFC.OrderID IS NULL THEN 1 ELSE 0 END AS IsNew
		, CASE WHEN OEFC.OrderID IS NOT NULL THEN 1 ELSE 0 END AS IsChanged
		, O.LastChangeDateUTC
		, O.LastChangedByUser
	FROM tblOrder O
	LEFT JOIN (
		SELECT OrderID, max(OrderLastChangeDateUTC) AS OrderLastChangeDateUTC
		FROM tblOrderExportFinalCustomer 
		GROUP BY OrderID
	) OEFC ON OEFC.OrderID = O.ID
	WHERE (OEFC.OrderID IS NULL OR OEFC.OrderLastChangeDateUTC > O.LastChangeDateUTC)
	  AND O.StatusID IN (4) -- AUDITED status only
	  AND O.DeleteDateUTC IS NULL

GO

COMMIT
SET NOEXEC OFF