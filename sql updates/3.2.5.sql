-- rollback
-- select value from tblsetting where id = 0
/*
	- add new Financial fields to Report Center - Order History 
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.2.4'
SELECT  @NewVersion = '3.2.5'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Report Center Order data
/***********************************/
ALTER VIEW [dbo].[viewReportCenter_Orders] AS
	SELECT O.*
		, ShipperH2SRate = IOC.H2SRate
		, ShipperH2SFee = IOC.H2SFee
		, ShipperChainupFee = IOC.ChainupFee
		, ShipperTaxRate = IOC.TaxRate
		, ShipperRerouteFee = IOC.RerouteFee
		, ShipperRejectFee = IOC.RejectionFee
		, ShipperOriginWaitRate = IOC.OriginWaitRate
		, ShipperOriginWaitFee = IOC.OriginWaitFee
		, ShipperDestWaitRate = IOC.DestinationWaitRate
		, ShipperDestWaitFee = IOC.DestWaitFee
		, ShipperTotalWaitFee = cast(IOC.OriginWaitFee as int) + cast(IOC.DestWaitFee as int)
		, ShipperTotalFee = IOC.TotalFee
		, ShipperSettlementUomID = IOC.UomID
		, ShipperSettlementUom = UOM.Abbrev
		, ShipperMinSettlementUnits = IOC.MinSettlementUnits
		, ShipperRouteRate = IOC.RouteRate
		, ShipperRouteFee = IOC.LoadFee
		, ShipperBatchNum = CSB.BatchNum
		, ShipperFuelCharge = IOC.FuelSurcharge
		, OriginGpsLatLon = ltrim(DLO.Lat) + ',' + ltrim(DLO.Lon)
		, OriginLatLon = ltrim(OO.LAT) + ',' + ltrim(OO.LON)
		, OriginAccuracyMeters = DLO.SourceAccuracyMeters
		, OriginDistance = isnull(cast(DLO.DistanceToPoint as int), 99999)
		, OriginGpsArrived = CASE WHEN DLO.DistanceToPoint IS NOT NULL AND DLO.DistanceToPoint <= OO.GeoFenceRadiusMeters THEN 1 ELSE 0 END
		, DestGpsLatLon = ltrim(DLD.Lat) + ',' + ltrim(DLD.Lon)
		, DestLatLon = ltrim(D.LAT) + ',' + ltrim(D.LON)
		, DestAccuracyMeters = DLD.SourceAccuracyMeters
		, DestDistance = isnull(cast(DLD.DistanceToPoint as int), 99999)
		, DestGpsArrived = CASE WHEN DLD.DistanceToPoint IS NOT NULL AND DLD.DistanceToPoint <= OO.GeoFenceRadiusMeters THEN 1 ELSE 0 END
	FROM viewOrder_OrderTicket_Full O
	JOIN tblOrigin OO ON OO.ID = O.OriginID
	JOIN tblDestination D ON D.ID = O.DestinationID
	LEFT JOIN tblOrderInvoiceCustomer IOC ON IOC.OrderID = O.ID
	LEFT JOIN tblCustomerSettlementBatch CSB ON CSB.ID = IOC.BatchID
	LEFT JOIN tblUom UOM ON UOM.ID = IOC.UomID
	LEFT JOIN viewDriverLocation_OriginFirstArrive DLO ON DLO.OrderID = O.ID AND DLO.OriginID = O.OriginID
	LEFT JOIN viewDriverLocation_DestinationFirstArrive DLD ON DLD.OrderID = O.ID AND DLD.DestinationID = O.DestinationID

GO

INSERT INTO tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport)
	SELECT 146, 1, 'ShipperH2SRate', 'Shipper H2S Rate', '#0.0000', null, 4, null, 1, 'Administrator,Management', 1
	UNION
	SELECT 147, 1, 'ShipperH2SFee', 'Shipper H2S $$', '#0.00', null, 4, null, 1, 'Administrator,Management', 1
	UNION
	SELECT 148, 1, 'ShipperChainupFee', 'Shipper Chainup $$', '#0.00', null, 4, null, 1, 'Administrator,Management', 1
	UNION
	SELECT 149, 1, 'ShipperTaxRate', 'Shipper Tax Rate', '#0.0000', null, 4, null, 1, 'Administrator,Management', 1
	UNION
	SELECT 150, 1, 'ShipperRerouteFee', 'Shipper Reroute $$', '#0.00', null, 4, null, 1, 'Administrator,Management', 1
	UNION
	SELECT 151, 1, 'ShipperRejectFee', 'Shipper Reroute $$', '#0.00', null, 4, null, 1, 'Administrator,Management', 1
	UNION
	SELECT 152, 1, 'ShipperOriginWaitRate', 'Shipper Origin Wait Rate', '#0.0000', null, 4, null, 1, 'Administrator,Management', 1
	UNION
	SELECT 153, 1, 'ShipperOriginWaitFee', 'Shipper Origin Wait Fee', '#0.00', null, 4, null, 1, 'Administrator,Management', 1
	UNION
	SELECT 154, 1, 'ShipperDestWaitRate', 'Shipper Dest Wait Rate', '#0.0000', null, 4, null, 1, 'Administrator,Management', 1
	UNION
	SELECT 155, 1, 'ShipperDestWaitFee', 'Shipper Dest Wait Fee', '#0.00', null, 4, null, 1, 'Administrator,Management', 1
	UNION
	SELECT 156, 1, 'ShipperTotalWaitFee', 'Shipper Total Wait Fee', '#0.00', null, 4, null, 1, 'Administrator,Management', 1
	UNION
	SELECT 157, 1, 'ShipperTotalFee', 'Shipper Total Fee', '#0.00', null, 4, null, 1, 'Administrator,Management', 1
	UNION
	SELECT 158, 1, 'ShipperSettlementUom', 'Shipper Settlement UOM', null, 'ShipperSettlementUomID', 2, 'SELECT ID, Name = Abbrev FROM tblUom ORDER BY Abbrev', 1, 'Administrator,Management', 1
	UNION
	SELECT 159, 1, 'ShipperMinSettlementUnits', 'Shipper Min Settle Units', '#0.00', null, 4, null, 1, 'Administrator,Management', 1
	UNION
	SELECT 160, 1, 'ShipperRouteRate', 'Shipper Route Rate', '#0.0000', null, 4, null, 1, 'Administrator,Management', 1
	UNION
	SELECT 161, 1, 'ShipperRouteFee', 'Shipper Route $$', '#0.00', null, 4, null, 1, 'Administrator,Management', 1
	UNION
	SELECT 162, 1, 'ShipperBatchNum', 'Shipper Batch #', null, null, 4, null, 1, 'Administrator,Management', 1
	UNION
	SELECT 163, 1, 'ShipperFuelCharge', 'Shipper Fuel Charge', '#0.00', null, 4, null, 1, 'Administrator,Management', 1

COMMIT
SET NOEXEC OFF