/* add logic to "virtual delete" any order info no longer available to a driver
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.0.20', @NewVersion = '2.1.0'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

CREATE TABLE tblOrderDriverAppVirtualDelete
(
  ID int not null identity(1, 1) CONSTRAINT PK_OrderDriverAppVirtualDelete PRIMARY KEY
, OrderID int not null CONSTRAINT FK_OrderDriverAppVirtualDelete_Order FOREIGN KEY REFERENCES tblOrder(ID) ON DELETE CASCADE
, DriverID int not null CONSTRAINT FK_OrderDriverAppVirtualDelete_Driver FOREIGN KEY REFERENCES tblDriver(ID) ON DELETE CASCADE
, VirtualDeleteDateUTC smalldatetime not null CONSTRAINT DF_OrderDriverAppVirtualDelete_DeleteDate DEFAULT getutcdate()
, VirtualDeletedByUser varchar(100) null
)

GO
CREATE UNIQUE INDEX udxOrderDriverAppVirtualDelete_OrderID_DriverID ON tblOrderDriverAppVirtualDelete(OrderID, DriverID)
GO

/**********************************************************/
-- Author: Kevin Alons
-- Date Created: 26 Sep 2013
-- Purpose: track changes to Orders that cause it be "virtually" deleted for a Driver (for DriveApp.Sync purposes)
/**********************************************************/
CREATE TRIGGER trigOrder_U_VirtualDelete ON tblOrder AFTER UPDATE AS
BEGIN
	DECLARE @NewRecords TABLE (
		  OrderID int not null
		, DriverID int not null
		, LastChangeDateUTC smalldatetime not null
		, LastChangedByUser varchar(100) null
	)
	
	INSERT INTO @NewRecords
		-- insert any applicable VIRTUALDELETE records where the status changed from 
		--   a valid status to an invalid [DriverApp] status
		SELECT d.ID, d.DriverID, isnull(i.LastChangeDateUTC, GETUTCDATE()), ISNULL(i.LastChangedByUser, 'System')
		FROM deleted d
		JOIN inserted i ON i.ID = D.ID
		WHERE d.StatusID IN (2,3,7,8) AND i.StatusID NOT IN (2,3,7,8)
		
		UNION
		-- insert any records where the driverID changed to another driverid (for a valid status)
		SELECT d.ID, d.DriverID, isnull(i.LastChangeDateUTC, GETUTCDATE())
		FROM deleted d
		JOIN inserted i ON i.ID = D.ID
		WHERE d.DriverID <> i.DriverID AND d.StatusID IN (2,3,7,8)

	-- delete any records that no longer apply because now the record is valid for the new driver/status
	DELETE FROM tblOrderDriverAppVirtualDelete
	FROM tblOrderDriverAppVirtualDelete ODAVD
	JOIN inserted i ON i.ID = ODAVD.OrderID AND i.DriverID = ODAVD.DriverID
	WHERE i.DeleteDateUTC IS NULL AND i.StatusID IN (2,3,7,8)
	
	-- update the VirtualDeleteDateUTC value for any existing records
	UPDATE tblOrderDriverAppVirtualDelete
	  SET VirtualDeleteDateUTC = NR.LastChangeDateUTC, VirtualDeletedByUser = NR.LastChangedByUser
	FROM tblOrderDriverAppVirtualDelete ODAVD
	JOIN @NewRecords NR ON NR.OrderID = ODAVD.OrderID AND NR.DriverID = ODAVD.DriverID

	-- insert the truly new VirtualDelete records	
	INSERT INTO tblOrderDriverAppVirtualDelete (OrderID, DriverID, VirtualDeleteDateUTC, VirtualDeletedByUser)
		SELECT OrderID, DriverID, LastChangeDateUTC, LastChangedByUser
		FROM @NewRecords
END
GO

/*******************************************/
-- Date Created: 31 Aug 2013
-- Author: Kevin Alons
-- Purpose: return driver editable Order data for Driver App sync
/*******************************************/
ALTER FUNCTION [dbo].[fnOrderEdit_DriverApp]( @DriverID int, @LastChangeDateUTC datetime ) RETURNS  TABLE AS
RETURN 
	SELECT O.ID
		, O.StatusID
		, O.TruckID
		, O.TrailerID
		, O.Trailer2ID
		, O.OriginBOLNum
		, O.OriginArriveTimeUTC
		, O.OriginDepartTimeUTC
		, O.OriginMinutes
		, O.OriginWaitNotes
		, O.OriginTruckMileage
		, O.OriginGrossBarrels
		, O.OriginNetBarrels
		, O.ChainUp
		, O.Rejected
		, O.RejectNotes
		, O.OriginTankNum
		, O.DestArriveTimeUTC
		, O.DestDepartTimeUTC
		, O.DestMinutes
		, O.DestWaitNotes
		, O.DestTruckMileage
		, O.DestGrossBarrels
		, O.DestNetBarrels
		, O.DestProductTemp
		, O.DestProductBSW
		, O.DestProductGravity
		, O.DestOpenMeterBarrels
		, O.DestCloseMeterBarrels
		, O.CarrierTicketNum
		, O.AcceptLastChangeDateUTC
		, O.PickupLastChangeDateUTC
		, O.DeliverLastChangeDateUTC
	FROM dbo.tblOrder O
	LEFT JOIN tblOrderDriverAppVirtualDelete ODAVD ON ODAVD.OrderID = O.ID
	WHERE (O.StatusID IN (2, 7, 8) -- Dispatched, Accepted, Picked Up
		-- or status = Delivered and the driver still has access to the record
		OR (StatusID = 3 AND O.DestDepartTimeUTC < DATEADD(hour, (SELECT cast(value as int) FROM tblSetting WHERE ID = 10), GETUTCDATE())))
	  AND O.DriverID = @DriverID
	  AND (
		@LastChangeDateUTC IS NULL 
		OR O.LastChangeDateUTC >= @LastChangeDateUTC
		OR (ODAVD.ID IS NOT NULL AND ODAVD.VirtualDeleteDateUTC >= @LastChangeDateUTC))

GO

/*******************************************/
-- Date Created: 25 Apr 2013
-- Author: Kevin Alons
-- Purpose: return readonly Order data for Driver App sync
/*******************************************/
ALTER FUNCTION [dbo].[fnOrderReadOnly_DriverApp]( @DriverID int, @LastChangeDateUTC datetime ) RETURNS  TABLE AS
RETURN 
	SELECT O.ID
		, O.OrderNum
		, O.StatusID
		, O.TicketTypeID
		, OO.TankTypeID
		, cast(P.PriorityNum as int) AS PriorityNum
		, O.Product
		, O.DueDate
		, O.Origin
		, O.OriginFull
		, OO.OriginType
		, OO.Station AS OriginStation
		, OO.County AS OriginCounty
		, O.OriginState
		, OO.WellAPI AS OriginAPI
		, O.Destination
		, O.DestinationFull
		, D.DestinationType AS DestType 
		, D.Station AS DestinationStation
		--, cast(CASE WHEN D.TicketTypeID = 2 THEN 1 ELSE 0 END as bit) AS DestBOLAvailable
		, OO.BarrelsPerInch AS OriginBarrelsPerInch
		, O.CreateDateUTC
		, O.CreatedByUser
		, O.LastChangeDateUTC
		, O.LastChangedByUser
		, isnull(ODAVD.VirtualDeleteDateUTC, O.DeleteDateUTC) AS DeleteDateUTC
		, isnull(ODAVD.VirtualDeletedByUser, O.DeletedByUser) AS DeletedByUser
		, O.OriginID
		, O.DestinationID
		, cast(O.PriorityID AS int) AS PriorityID
		, O.Operator
		, O.OperatorID
		, O.Pumper
		, O.PumperID
		, O.Producer
		, O.ProducerID
		, O.Customer
		, O.CustomerID
		, O.Carrier
		, O.CarrierID
		, O.ProductID
		, O.TicketType
		, C.ZPLHeaderBlob AS PrintHeaderBlob
		, isnull(C.EmergencyInfo, 'For an emergency (spill, leak, fire, or accident) contact CHEMTREC @ 800.424.9300 (toll free)') AS EmergencyInfo
		, O.DestTicketTypeID
		, O.DestTicketType
	FROM dbo.viewOrder O
	JOIN dbo.tblPriority P ON P.ID = O.PriorityID
	JOIN dbo.viewOrigin OO ON OO.ID = O.OriginID
	JOIN dbo.viewDestination D ON D.ID = O.DestinationID
	JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
	LEFT JOIN tblOrderDriverAppVirtualDelete ODAVD ON ODAVD.OrderID = O.ID
	WHERE (StatusID IN (2, 7, 8) -- Dispatched, Accepted, Picked Up
		-- or status = Delivered and the driver still has access to the record
		OR (StatusID = 3 AND O.DestDepartTimeUTC < DATEADD(hour, (SELECT cast(value as int) FROM tblSetting WHERE ID = 10), GETUTCDATE())))
	  AND O.DriverID = @DriverID
	  AND (
		@LastChangeDateUTC IS NULL 
		OR O.LastChangeDateUTC >= @LastChangeDateUTC
		OR (ODAVD.ID IS NOT NULL AND ODAVD.VirtualDeleteDateUTC >= @LastChangeDateUTC))

GO

COMMIT
SET NOEXEC OFF