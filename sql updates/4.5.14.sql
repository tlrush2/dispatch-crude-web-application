SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.5.13'
SELECT  @NewVersion = '4.5.14'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'JT-2255 - Added Android OS, Device Model Number, Device Serial Number, and Device Manufacturer to the information gathered about devices.  Also converted the equipment page to MVC.'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO



-- Update existing Driver Equipment view permission
UPDATE aspnet_Roles
SET Category = 'Assets - Resources - Equipment'
	, FriendlyName = 'Equipment - View'
WHERE RoleName = 'viewDriverEquipment'
GO

-- Add new Driver Equipment permissions
EXEC spAddNewPermission 'createDriverEquipment', 'Allow user to create Driver Equipment records', 'Assets - Resources - Equipment', 'Equipment - Create'
GO
EXEC spAddNewPermission 'editDriverEquipment', 'Allow user to edit Driver Equipment records', 'Assets - Resources - Equipment', 'Equipment - Edit'
GO
EXEC spAddNewPermission 'deactivateDriverEquipment', 'Allow user to deactivate Driver Equipment records', 'Assets - Resources - Equipment', 'Equipment - Deactivate'
GO

-- Add new Driver Equipment permissions to any groups that already had the "view" permission
EXEC spAddNewPermissionToGroupsWithExistingRole 'viewDriverEquipment', 'createDriverEquipment'
GO
EXEC spAddNewPermissionToGroupsWithExistingRole 'viewDriverEquipment', 'editDriverEquipment'
GO
EXEC spAddNewPermissionToGroupsWithExistingRole 'viewDriverEquipment', 'deactivateDriverEquipment'
GO

-- Update the Driver Equipment Manufacturers permissions' categories
UPDATE aspnet_Roles SET Category = 'Assets - Resources - Equipment'	WHERE RoleName = 'viewDriverEquipmentManufacturers'
GO
UPDATE aspnet_Roles SET Category = 'Assets - Resources - Equipment'	WHERE RoleName = 'createDriverEquipmentManufacturers'
GO
UPDATE aspnet_Roles SET Category = 'Assets - Resources - Equipment'	WHERE RoleName = 'editDriverEquipmentManufacturers'
GO
UPDATE aspnet_Roles SET Category = 'Assets - Resources - Equipment'	WHERE RoleName = 'deactivateDriverEquipmentManufacturers'
GO

-- Update the Driver App History view permission category
UPDATE aspnet_Roles SET Category = 'Assets - Resources - Equipment'	WHERE RoleName = 'viewDriverAppHistory'
GO


-- Create Android OS lookup table
CREATE TABLE tblAndroidOSVersion (
	ID INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_AndroidOSVersion PRIMARY KEY CLUSTERED
	, Name VARCHAR(50) NULL
	, VersionNumber VARCHAR(10) NOT NULL
	, ApiLevel VARCHAR(10) NOT NULL CONSTRAINT UQ_AndroidOSVersion_ApiLevel UNIQUE NONCLUSTERED
)
GO

-- Insert Android OS records (only back to 2.1 since DC will never see/support anything lower than that)
INSERT INTO tblAndroidOSVersion (Name, VersionNumber, ApiLevel)
	SELECT 'Eclair', '2.1', '7'
		UNION
	SELECT 'Froyo', '2.2', '8'
		UNION
	SELECT 'Gingerbread', '2.3', '9'
		UNION
	SELECT 'Gingerbread', '2.3.3', '10'
		UNION
	SELECT 'Honeycomb', '3.0', '11'
		UNION
	SELECT 'Honeycomb', '3.1', '12'
		UNION
	SELECT 'Honeycomb', '3.2', '13'
		UNION
	SELECT 'Ice Cream Sandwich', '4.0', '14'
		UNION
	SELECT 'Ice Cream Sandwich', '4.0.3', '15'
		UNION
	SELECT 'Jelly Bean', '4.1', '16'
		UNION
	SELECT 'Jelly Bean', '4.2', '17'
		UNION
	SELECT 'Jelly Bean', '4.3', '18'
		UNION
	SELECT 'KitKat', '4.4', '19'
		UNION
	SELECT 'KitKat Wear', '4.4W', '20'
		UNION
	SELECT 'Lollipop', '5.0', '21'
		UNION
	SELECT 'Lollipop', '5.1', '22'
		UNION
	SELECT 'Marshmallow', '6.0', '23'
		UNION
	SELECT 'Nougat', '7.0', '24'
		UNION
	SELECT 'Nougat', '7.1.1', '25'
GO


-- Add API Level to driver equipment table
ALTER TABLE tblDriverEquipment
	ADD AndroidApiLevel VARCHAR(10) NULL
GO


-- Add API Level, model number, serial number, and manufacturer (not foreign key) to driver update table
ALTER TABLE tblDriverUpdate
	ADD AndroidApiLevel VARCHAR(10) NULL
	, ModelNum VARCHAR(50) NULL
	, SerialNum VARCHAR(100) NULL
	, Manufacturer VARCHAR(50) NULL
GO


-- Add API Level, model number, serial number, and manufacturer (not foreign key) to driver_sync table
ALTER TABLE tblDriver_Sync
	ADD AndroidApiLevel VARCHAR(10) NULL
	, ModelNum VARCHAR(50) NULL
	, SerialNum VARCHAR(100) NULL
	, Manufacturer VARCHAR(50) NULL
GO


/***************************************
Date Created: 2016/02/01 - 3.10.7
Author: Kevin Alons
Purpose: Add or update the new TabletID and/or Printer Serial number to tblDriverEquipment
Changes:
	- 4.5.14		2017/02/23		BSB		Added Android API level, model number, serial number, and manufacturer to updateable information about tablets
***************************************/
ALTER PROCEDURE spDriverSync_UpdateDevices
(
  @TabletID VARCHAR(20)
, @PrinterSerial VARCHAR(100)
, @UserName VARCHAR(100)
, @AndroidApiLevel VARCHAR(10)
, @ModelNum VARCHAR(50)
, @SerialNum VARCHAR(100)
, @Manufacturer VARCHAR(50)
) AS
BEGIN
	-- 4.5.14 - prep work for the manufacturer name that is now being sent during sync
	DECLARE @MANUFACTURER_ID INT

	IF (@Manufacturer IS NULL OR @Manufacturer = '')  -- IF NULL
		BEGIN
			SET @MANUFACTURER_ID = 1 -- "Unknown"
		END
	ELSE  -- IF NOT NULL
		BEGIN			
			SET @MANUFACTURER_ID = (SELECT TOP 1 ID FROM tblDriverEquipmentManufacturer WHERE UPPER(Name) LIKE UPPER(@Manufacturer))

			IF (@MANUFACTURER_ID IS NULL)
				BEGIN
					-- INSERT RECORD
					INSERT INTO tblDriverEquipmentManufacturer (Name)
					SELECT @Manufacturer
					-- NOW GET ID OF NEWLY INSERTED RECORD
					SET @MANUFACTURER_ID = SCOPE_IDENTITY()
				END
		END	

	-- insert or update an equipment record for the specified tablet (if provided)
	IF (@TabletID IS NOT NULL AND @TabletID <> '')
	BEGIN
		INSERT INTO tblDriverEquipment (DriverEquipmentTypeID
										, DriverEquipmentManufacturerID
										, PhoneNum
										, AndroidApiLevel
										, ModelNum
										, SerialNum
										, CreatedByUser)
			SELECT 1, @MANUFACTURER_ID, @TabletID, @AndroidApiLevel, @ModelNum, @SerialNum, @UserName 
			WHERE NOT EXISTS (SELECT ID FROM tblDriverEquipment DE WHERE DE.PhoneNum = @TabletID AND DriverEquipmentTypeID = 1)
		IF (@@ROWCOUNT = 0)	
			UPDATE tblDriverEquipment 
			SET AndroidApiLevel = @AndroidApiLevel  -- Always update the current API level
				, ModelNum = @ModelNum  -- Always update the model number
				, SerialNum = @SerialNum  -- Always update the serial number
				, DriverEquipmentManufacturerID = @MANUFACTURER_ID  -- Always update the manufacturer
				, LastChangeDateUTC = getutcdate()
				, LastChangedByUser = @UserName
				, DeleteDateUTC = NULL
				, DeletedByUser = NULL 
			WHERE PhoneNum = @TabletID AND DriverEquipmentTypeID = 1
	END

	IF (@PrinterSerial IS NOT NULL AND @PrinterSerial <> '')
	BEGIN
		INSERT INTO tblDriverEquipment (DriverEquipmentTypeID
										, DriverEquipmentManufacturerID
										, SerialNum
										, CreatedByUser)
			SELECT 2, 1, @PrinterSerial, @UserName
			WHERE NOT EXISTS (SELECT ID FROM tblDriverEquipment DE WHERE DE.SerialNum = @PrinterSerial AND DriverEquipmentTypeID = 2)
		IF (@@ROWCOUNT = 0)
			UPDATE tblDriverEquipment 
			SET LastChangeDateUTC = getutcdate(), LastChangedByUser = @UserName
				, DeleteDateUTC = NULL, DeletedByUser = NULL 
			WHERE SerialNum = @PrinterSerial AND DriverEquipmentTypeID = 2
	END
END
GO


/*******************************************
-- Date Created: 16 Mar 2016
-- Author: Jeremiah Silliman
-- Purpose: Insert record with sync data listing driver, sync time and most recent mobile application version
-- Changes: 
	3.11.7		2016/03/23		JS		Updated spDriver_Update to cleanup SQL
	4.5.14		2017/02/23		BSB		Added Android API level, model number, serial number, and manufacturer
*******************************************/
ALTER PROCEDURE spDriver_Update
(
  @TabletID VARCHAR(20)
, @PrinterSerial VARCHAR(100)
, @MobileAppVersion VARCHAR(100)
, @AndroidApiLevel VARCHAR(10)
, @ModelNum VARCHAR(50)
, @SerialNum VARCHAR(100)
, @Manufacturer VARCHAR(50)
, @DriverID INT
) AS
BEGIN
IF (@MobileAppVersion IS NOT NULL AND @MobileAppVersion <> '')
	BEGIN
		INSERT INTO tblDriverUpdate (DriverID, MobileAppVersion, UpdateLastUTC, TabletID, PrinterSerial, AndroidApiLevel, ModelNum, SerialNum, Manufacturer)
			SELECT @DriverID, @MobileAppVersion, GETUTCDATE(), @TabletID, @PrinterSerial, @AndroidApiLevel, @ModelNum, @SerialNum, @Manufacturer
			WHERE NOT EXISTS (SELECT MobileAppVersion FROM tblDriverUpdate DUT WHERE DriverID = @DriverID AND MobileAppVersion = @MobileAppVersion)
	END
END
GO


/*******************************************
-- Date Created: 22 Apr 2013
-- Author: Kevin Alons
-- Purpose: validate parameters, if valid insert/update the tblDriver_Sync table for the specified DriverID
-- Code References:
					DispatchCrude/Models/Sync/DriverApp/MasterData.cs
-- Changes: 3.10.7 - 2016/01/28 - BB - Added TabletID and PrinterSerial to the synced values
            3.10.7 - 2016/02/01 - KDA - Added EXEC spDriverSync_UpdateDevices @TabletID, @PrinterSerial, @UserName
			3.11.7 - 2016/03/22 - JS - Added call to stored procedure to log mobile app version
			4.5.14 - 2017/02/23 - BSB - Added android api level, model number, serial number, and manufacturer
*******************************************/
ALTER PROCEDURE spDriver_Sync
(
  @UserName VARCHAR(100)
, @DriverID INT
, @MobileAppVersion VARCHAR(25) = NULL
, @SyncDateUTC DATETIME = NULL
, @PasswordHash VARCHAR(25) = NULL
, @TabletID VARCHAR(30) = NULL
, @PrinterSerial VARCHAR(30) = NULL
, @AndroidApiLevel VARCHAR(10) = NULL
, @ModelNum VARCHAR(50) = NULL
, @SerialNum VARCHAR(100) = NULL
, @Manufacturer VARCHAR(50) = NULL
, @Valid BIT = NULL OUTPUT
, @Message VARCHAR(255) = NULL OUTPUT
) AS
BEGIN
	-- if resetting, delete the entire record (it will be recreated below)
	IF (@SyncDateUTC IS NULL)
	BEGIN
		DELETE FROM tblDriver_Sync WHERE DriverID = @DriverID
		-- result of count(*) query will be 1 or 0 (true or false respectively)
		SELECT @Valid = (SELECT COUNT(*) FROM tblDriver WHERE ID = @DriverID)
		IF (@Valid = 0)
			SELECT @Message = 'DriverID was not valid'
	END
	ELSE
	BEGIN
		-- query the ValidatePasswordHash setting parameter
		DECLARE @ValidatePasswordHash bit
		SELECT @ValidatePasswordHash = CASE WHEN Value IN ('1', 'true', 'yes') THEN 1 ELSE 0 END
		FROM tblSetting
		WHERE ID = 13

		-- result of count(*) query will be 1 or 0 (true or false respectively)
		SELECT @Valid = (SELECT count(*) FROM tblDriver_Sync WHERE DriverID = @DriverID 
			AND (@ValidatePasswordHash = 0 OR PasswordHash = @PasswordHash))
		IF (@Valid = 0)
			SELECT @Message = 'PasswordHash was not valid'
	END
	
	EXEC spDriverSync_UpdateDevices @TabletID, @PrinterSerial, @UserName, @AndroidApiLevel, @ModelNum, @SerialNum, @Manufacturer
	-- Log the version history
	EXEC spDriver_Update @TabletID, @PrinterSerial, @MobileAppVersion, @AndroidApiLevel, @ModelNum, @SerialNum, @Manufacturer, @DriverID

	IF (@Valid = 1)
	BEGIN
		-- if a sync record already exists, just update the new LastSync value
		UPDATE tblDriver_Sync 
		SET LastSyncUTC = @SyncDateUTC
		, MobileAppVersion = @MobileAppVersion
		, TabletID = @TabletID
		, PrinterSerial = @PrinterSerial 
		, AndroidApiLevel = @AndroidApiLevel
		, ModelNum = @ModelNum
		, SerialNum = @SerialNum
		, Manufacturer = @Manufacturer
		WHERE DriverID = @DriverID

		-- otherwise insert a new record with a new passwordhash value
		INSERT INTO tblDriver_Sync (DriverID, MobileAppVersion, LastSyncUTC, PasswordHash, TabletID, PrinterSerial, AndroidApiLevel, ModelNum, SerialNum, Manufacturer)
			SELECT @DriverID, @MobileAppVersion, NULL, dbo.fnGeneratePasswordHash(), @TabletID, @PrinterSerial, @AndroidApiLevel, @ModelNum, @SerialNum, @Manufacturer
			FROM tblDriver D
			LEFT JOIN tblDriver_Sync DS ON DS.DriverID = D.ID
			WHERE D.ID = @DriverID AND DS.DriverID IS NULL

		-- return the current "Master" data
		SELECT *
			, TabletID = @TabletID
			, PrinterSerial = @PrinterSerial
			, AndroidApiLevel = @AndroidApiLevel 
			, ModelNum = @ModelNum
			, SerialNum = @SerialNum
			, Manufacturer = @Manufacturer
		FROM dbo.fnDriverMasterData(@Valid, @DriverID, @UserName)
	END
	ELSE
	BEGIN
		SELECT @Valid AS Valid, @UserName AS UserName, 0 AS DriverID, NULL AS DriverName, 0 AS MobilePrint
			, NULL AS LastSyncUTC
			, (SELECT cast(Value as int) FROM tblSetting WHERE ID = 11) AS SyncMinutes
			, (SELECT Value FROM tblSetting WHERE ID = 0) AS SchemaVersion
			, @MobileAppVersion AS MobileAppVersion			
			, (SELECT Value FROM tblSetting WHERE ID = 12) AS LatestAppVersion
			, NULL AS PasswordHash
			, @TabletID AS TabletID
			, @PrinterSerial AS PrinterSerial
			, @AndroidApiLevel AS AndroidApiLevel
		FROM tblSetting S WHERE S.ID = 0
	END 	
END
GO



/***********************************/
-- Date Created: 26 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Drivers table records with "friendly" translated values included
-- Changes: 
-- 3.10.7		- 2016/01/27 - BB	- Added TabletID and PrinterSerial from tblDriver_Sync
-- 3.11.15.2	- 2016/04/20 - BB	- Added DriverGroupName to fix filtering on web page
-- 4.4.14		- 2016/12/08 - BB	- Added Operating State, Added DriverShiftTypeName, Move UserNames from viewDriver to viewDriverBase
--							 - KDA	- use fnFnameWithDeleted() method so future changes can be done in a single place (leaving the views unchanged)
-- 4.4.20		- 2017/01/19 - BSB	- Moved LastDeliveredOrderTime from viewDriver to here so it can be displayed on the driver maintenance page
-- 4.5.0		- 2017/01/26 - BSB	- Moved Terminal here from viewDriver
-- 4.5.14		- 2017/02/23 - BSB	- Added AndroidApiLevel, model number, serial number, and manufacturer from tblDriver_Sync
/***********************************/
ALTER VIEW viewDriverBase AS
SELECT X.*
	, FullNameD = dbo.fnNameWithDeleted(FullName, DeleteDateUTC) -- 4.4.14 - Updated from "Deleted" 12/15/16
FROM (
	SELECT D.*
		, Active = cast(CASE WHEN isnull(C.DeleteDateUTC, D.DeleteDateUTC) IS NULL THEN 1 ELSE 0 END as bit) 
		, FullName = D.FirstName + ' ' + D.LastName 
		, FullNameLF = D.LastName + ', ' + D.FirstName 
		, CarrierType = isnull(CT.Name, 'Unknown') 
		, Carrier = C.Name 
		, StateAbbrev = S.Abbreviation 
		, OperatingStateAbbrev = S2.Abbreviation
		, Truck = T.FullName
		, Trailer = T1.FullName 
		, Trailer2 = T2.FullName 
		, Region = R.Name 
		, Terminal = TERM.Name
		, DS.MobileAppVersion
		, DS.TabletID
		, DS.PrinterSerial
		, DS.AndroidApiLevel
		, DS.ModelNum
		, DS.SerialNum
		, DS.Manufacturer
		, DriverGroupName = DG.Name
		, DriverShiftTypeName = DST.Name
		, DUN.UserNames
		, LastDeliveredOrderTime = (SELECT MAX(DestDepartTimeUTC) FROM tblOrder WHERE DriverID = D.ID)
	FROM tblDriver D 
	LEFT JOIN dbo.fnDriverUserNames() DUN ON DUN.DriverID = D.ID
	JOIN tblCarrier C ON C.ID = D.CarrierID 
	LEFT JOIN tblDriver_Sync DS ON DS.DriverID = D.ID
	LEFT JOIN tblCarrierType CT ON CT.ID = C.CarrierTypeID
	LEFT JOIN tblState S ON S.ID = D.StateID
	LEFT JOIN tblState S2 ON S2.ID = D.OperatingStateID
	LEFT JOIN viewTruck T ON T.ID = D.TruckID
	LEFT JOIN viewTrailer T1 ON T1.ID = D.TrailerID
	LEFT JOIN viewTrailer T2 ON T2.ID = D.Trailer2ID
	LEFT JOIN tblRegion R ON R.ID = D.RegionID
	LEFT JOIN tblTerminal TERM ON TERM.ID = D.TerminalID
	LEFT JOIN tblDriverGroup DG ON DG.ID = D.DriverGroupID
	LEFT JOIN tblDriverShiftType DST ON DST.ID = D.DriverShiftTypeID
) X
GO


EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRebuildAllObjects
GO



COMMIT
SET NOEXEC OFF