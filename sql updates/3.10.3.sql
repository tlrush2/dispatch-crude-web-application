SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.10.2.3'
SELECT  @NewVersion = '3.10.3'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Auto add new role for 3rd party dispatch'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

INSERT INTO ASPNET_ROLES
select applicationid, NEWID(), 'editOrders', LOWER('editOrders'), 'Edit access on new order edit pages' from aspnet_applications 
where not exists (select 1 from aspnet_roles where rolename = 'editOrders')

INSERT INTO ASPNET_ROLES
select applicationid, NEWID(), 'viewOrders', LOWER('viewOrders'), 'Read only access on new order edit pages' from aspnet_applications 
where not exists (select 1 from aspnet_roles where rolename = 'viewOrders')

INSERT INTO ASPNET_ROLES
select applicationid, NEWID(), 'Dispatch3P', LOWER('Dispatch3P'), '3rd Party Dispatch access (limited)' from aspnet_applications 
where not exists (select 1 from aspnet_roles where rolename = 'Dispatch3P')


COMMIT
SET NOEXEC OFF