--backup database [dispatchcrude.dev] to disk = 'C:\files\Consulting\cts\dc_backups\dispatchcrude.dev.3.6.25.bak'
--restore database [DispatchCrude.Dev] from disk = 'C:\files\Consulting\cts\dc_backups\dispatchcrude.dev.3.6.25.bak'
--go
-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.6.25'
SELECT  @NewVersion = '3.6.26'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'Add Origin.DrivingDirections field'
	UNION SELECT @NewVersion, 0, 'Driver App: Add OriginDrivingDirections to SYNC data (read only)'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

ALTER TABLE tblOrigin ADD DrivingDirections TEXT NULL
GO

EXEC _spRebuildAllObjects
GO

/*******************************************/
-- Date Created: 25 Apr 2013
-- Author: Kevin Alons
-- Purpose: return readonly Order data for Driver App sync
/*******************************************/
ALTER FUNCTION fnOrderReadOnly_DriverApp(@DriverID int, @LastChangeDateUTC datetime) RETURNS TABLE AS
RETURN 
	SELECT O.ID
		, O.OrderNum
		, O.StatusID
		, O.TicketTypeID
		, PriorityNum = cast(P.PriorityNum as int) 
		, O.Product
		, O.DueDate
		, O.Origin
		, O.OriginFull
		, OO.OriginType
		, O.OriginUomID
		, OriginStation = OO.Station 
		, OriginLeaseNum = OO.LeaseNum 
		, OriginCounty = OO.County 
		, OriginLegalDescription = OO.LegalDescription 
		, O.OriginNDIC
		, O.OriginNDM
		, O.OriginCA
		, O.OriginState
		, OO.WellAPI AS OriginAPI
		, OriginLat = OO.LAT 
		, OriginLon = OO.LON 
		, OriginGeoFenceRadiusMeters = OO.GeoFenceRadiusMeters 
		, O.Destination
		, O.DestinationFull
		, DestType = D.DestinationType 
		, O.DestUomID
		, DestLat = D.LAT 
		, DestLon = D.LON 
		, DestGeoFenceRadiusMeters = D.GeoFenceRadiusMeters 
		, D.Station AS DestinationStation
		, O.CreateDateUTC
		, O.CreatedByUser
		, O.LastChangeDateUTC
		, O.LastChangedByUser
		, DeleteDateUTC = isnull(ODAVD.VirtualDeleteDateUTC, O.DeleteDateUTC) 
		, DeletedByUser = isnull(ODAVD.VirtualDeletedByUser, O.DeletedByUser) 
		, O.OriginID
		, O.DestinationID
		, PriorityID = cast(O.PriorityID AS int) 
		, O.Operator
		, O.OperatorID
		, O.Pumper
		, O.PumperID
		, O.Producer
		, O.ProducerID
		, O.Customer
		, O.CustomerID
		, O.Carrier
		, O.CarrierID
		, O.ProductID
		, O.TicketType
		, PrintHeaderBlob = C.ZPLHeaderBlob 
		, EmergencyInfo = isnull(C.EmergencyInfo, 'For an emergency (spill, leak, fire, or accident) contact CHEMTREC @ 800.424.9300 (toll free)') 
		, O.DestTicketTypeID
		, O.DestTicketType
		, O.OriginTankNum
		, O.OriginTankID
		, O.DispatchNotes
		, O.DispatchConfirmNum
		, RouteActualMiles = isnull(R.ActualMiles, 0)
		, CarrierAuthority = CA.Authority 
		, OriginTimeZone = OO.TimeZone
		, DestTimeZone = D.TimeZone
		, OCTM.OriginThresholdMinutes
		, OCTM.DestThresholdMinutes
		, ShipperHelpDeskPhone = C.HelpDeskPhone
		, OriginDrivingDirections = OO.DrivingDirections
	FROM dbo.viewOrder O
	JOIN dbo.tblPriority P ON P.ID = O.PriorityID
	JOIN dbo.viewOrigin OO ON OO.ID = O.OriginID
	JOIN dbo.viewDestination D ON D.ID = O.DestinationID
	JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
	JOIN dbo.tblRoute R ON R.ID = O.RouteID
	JOIN dbo.tblCarrier CA ON CA.ID = O.CarrierID
	LEFT JOIN tblOrderDriverAppVirtualDelete ODAVD ON ODAVD.OrderID = O.ID AND ODAVD.DriverID = @DriverID
	CROSS JOIN (SELECT DATEADD(second, -5, @LastChangeDateUTC) AS LCD) LCD
	OUTER APPLY dbo.fnOrderCombinedThresholdMinutes(O.ID) OCTM
	WHERE O.ID IN (
		SELECT id FROM tblOrder WHERE DriverID = @driverID AND StatusID IN (2,7,8,3) 
		UNION 
		SELECT OrderID FROM tblOrderDriverAppVirtualDelete WHERE DriverID = @driverID
	)
	  AND (
		@LastChangeDateUTC IS NULL 
		OR O.CreateDateUTC >= LCD.LCD
		OR O.LastChangeDateUTC >= LCD.LCD
		OR O.DeleteDateUTC >= LCD.LCD
		OR C.LastChangeDateUTC >= LCD.LCD
		OR CA.LastChangeDateUTC >= LCD.LCD
		OR OO.LastChangeDateUTC >= LCD.LCD
		OR R.LastChangeDateUTC >= LCD.LCD
		OR ODAVD.VirtualDeleteDateUTC >= LCD.LCD)
GO

COMMIT
SET NOEXEC OFF