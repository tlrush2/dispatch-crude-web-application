/* add Truck.GPSUnit varchar(25) null column
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.0.8', @NewVersion = '2.0.9'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

ALTER TABLE tblTruck ADD GPSUnit varchar(25) null
GO

EXEC _spRefreshAllViews
GO

COMMIT
SET NOEXEC OFF