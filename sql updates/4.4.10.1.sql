SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.4.10'
SELECT  @NewVersion = '4.4.10.1'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'JT-427 - Fix to Daylight Savings lookup'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/************************************************/
-- Create Date: 26 Aug 2013
-- Author: Kevin Alons
-- Purpose: convert a UTC DateTime to local (which is specified)
-- Changes:
--		4.4.10.1		JAE			2017-01-02		Don't rely on manually-populated table for DST start and end
/************************************************/
ALTER FUNCTION [dbo].[fnUTC_to_Local](@utc datetime, @timeZoneID tinyint, @useDST bit) RETURNS datetime AS
BEGIN
	DECLARE @ret datetime

	declare @2ndSunMar DATETIME = DATEADD(hour, (7 + (6-(DATEDIFF(dd,0,DATEADD(mm,(YEAR(@utc)-1900) * 12 + 2,0))%7)))*24 + 2, DATEADD(mm,(YEAR(@utc)-1900) * 12 + 2,0))
	declare @1stSunNov DATETIME = DATEADD(hour, (6-(DATEDIFF(dd,0,DATEADD(mm,(YEAR(@utc)-1900) * 12 + 10,0))%7))*24 + 2, DATEADD(mm,(YEAR(@utc)-1900) * 12 + 10,0))

	SELECT @ret = dateadd(hour
		, CASE WHEN @utc BETWEEN @2ndSunMar AND @1stSunNov AND @useDST = 1 THEN TZ.DaylightOffsetHours ELSE TZ.StandardOffsetHours END
		, @utc)
	FROM tblTimeZone TZ
	WHERE TZ.ID = @timeZoneID
	RETURN @ret
END

GO


COMMIT
SET NOEXEC OFF
