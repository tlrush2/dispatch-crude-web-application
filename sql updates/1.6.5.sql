/* allow deletion of Order records (manually, the app will automatically mark Deleted instead of deleting)
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '1.6.4', @NewVersion = '1.6.5'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

DROP TRIGGER trigOrderTicket_IOD
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 13 Dec 2012
-- Description:	ensure that only "Assigned|Dispatched|Declined|Generated" orders can be deleted (marked deleted)
-- =============================================
ALTER TRIGGER [dbo].[trigOrder_IOD] ON [dbo].[tblOrder] INSTEAD OF DELETE AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	IF (SELECT count(*) FROM deleted d where StatusID NOT IN (-10, 1, 2, 9)) > 0
	BEGIN
		RAISERROR('Order could not be deleted - it has a status other than "Generated, Assigned, Dispatched or Declined"', 16, 1)
		RETURN
	END
	ELSE
		DELETE FROM tblOrderTicket WHERE OrderID IN (SELECT ID FROM deleted)
		DELETE FROM tblOrder WHERE ID IN (SELECT ID FROM deleted)
END

GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 1 Jun 2013
-- Description:	provide a way to "clone" an existing set of Accessorial rates
-- =============================================
ALTER PROCEDURE [dbo].[spCloneCarrierAccessorialRates](@ID int, @UserName varchar(100)) AS
BEGIN
	DELETE FROM tblCarrierRates WHERE CarrierID IS NULL AND RegionID IS NULL
	INSERT INTO tblCarrierRates (CarrierID, RegionID, ChainupFee, WaitFee, RejectionFee, RerouteFee, H2SRate, FuelSurcharge, CreateDate, CreatedByUser)
		SELECT 0, 0, ChainupFee, WaitFee, RejectionFee, RerouteFee, H2SRate, FuelSurcharge, GETDATE(), @UserName
		FROM tblCarrierRates WHERE ID = @ID
END

GO

DROP PROCEDURE spCloneCarrierRates
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 1 Jun 2013
-- Description:	provide a way to "clone" an existing set of Accessorial rates
-- =============================================
ALTER PROCEDURE [dbo].[spCloneCustomerAccessorialRates](@ID int, @UserName varchar(100)) AS
BEGIN
	DELETE FROM tblCustomerRates WHERE CustomerID IS NULL AND RegionID IS NULL
	INSERT INTO tblCustomerRates (CustomerID, RegionID, ChainupFee, WaitFee, RejectionFee, H2SRate, FuelSurcharge, RerouteFee, CreateDate, CreatedByUser)
		SELECT 0, 0, ChainupFee, WaitFee, RejectionFee, RerouteFee, H2SRate, FuelSurcharge, GETDATE(), @UserName
		FROM tblCustomerRates WHERE ID = @ID
END

GO

DROP PROCEDURE spCloneCustomerRates
GO

COMMIT
SET NOEXEC OFF