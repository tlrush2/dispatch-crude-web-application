-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.12.6.3'
SELECT  @NewVersion = '3.12.6.4'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'DCWEB-1464 Gauger App Setting to Disable Required Fields'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


--Add order rule
INSERT INTO tblOrderRuleType
(ID, Name, RuleTypeID, ForDriverApp)
VALUES (16, 'Disable Gauger Requirements', 2, NULL)

GO


--Update trigger to omit errors when gauger validation is turned off
ALTER TRIGGER trigGaugerOrderTicket_IU ON tblGaugerOrderTicket AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	
	-- Note: the TRIGGER_NESTLEVEL( OBJECT_ID('trigGaugerOrderTicket_IU') ) = 1 statement prevents recursion
	IF (TRIGGER_NESTLEVEL( OBJECT_ID('trigGaugerOrderTicket_IU')) = 1) BEGIN
		
		-- only do anything if something actually changed
		IF EXISTS (SELECT * FROM inserted EXCEPT SELECT * FROM deleted)
		BEGIN
			PRINT 'trigGaugerOrderTicket_IU FIRED'
			
			/**********  START OF VALIDATION SECTION ************************/
			DECLARE @errorString varchar(1000)
			SET @errorString = '' -- default value (so we can APPEND values)

			IF EXISTS (
				SELECT * 
				FROM tblOrder 
				WHERE ID IN (SELECT OrderID FROM inserted) 
				  -- prevent changes to any tickets belonging to a Delivered or Audited Order
				  AND StatusID IN (4))
			BEGIN
				IF (dbo.fnToBool(dbo.fnSettingValue(35)) = 1)
					PRINT 'Gauger Ticket for an AUDITED orders is being modified - please investigate why!'
				ELSE 				
					SET @errorString = @errorString + '|Gauger Tickets of Audited Orders cannot be modified'
			END

			-- prevent reassigning a ticket from one order to another
			IF EXISTS (
				SELECT i.* 
				FROM inserted i
				JOIN deleted d ON i.UID = d.UID
				WHERE i.OrderID <> d.OrderID)
			BEGIN
				SET @errorString = @errorString + '|Tickets cannot be moved to a different Order'
			END

			IF EXISTS (
				SELECT OT.OrderID
				FROM tblGaugerOrderTicket OT
				JOIN inserted i ON i.OrderID = OT.OrderID AND i.CarrierTicketNum = OT.CarrierTicketNum AND i.UID <> OT.UID
				WHERE OT.DeleteDateUTC IS NULL and I.DeleteDateUTC IS NULL
				GROUP BY OT.OrderID, OT.CarrierTicketNum
				HAVING COUNT(*) > 1
			) 
			BEGIN
				SET @errorString = @errorString + '|Duplicate active Ticket Numbers are not allowed'
			END
			
			-- Tank is always required for Gauge Run Basic [1] (even for REJECTED tickets)
			IF EXISTS (SELECT * FROM inserted WHERE TicketTypeID IN (1) AND OriginTankID IS NULL AND TankNum IS NULL AND DeleteDateUTC IS NULL)
			BEGIN
				SET @errorString = @errorString + '|Tank is required for "Gauge Run Basic" tickets'
			END

			-- CarrierTicketNum (Ticket #) is always required for Gauge Run Basic [1] (even for REJECTED tickets)
			IF EXISTS (SELECT * FROM inserted WHERE TicketTypeID IN (1) AND CarrierTicketNum IS NULL AND DeleteDateUTC IS NULL)
			BEGIN
				SET @errorString = @errorString + '|Ticket # value is required for "Gauge Run Basic" tickets'
			END

			-- Product Measurement values are only required for NOT-REJECTED Gauge Run Basic [1]
			IF EXISTS (SELECT * FROM inserted WHERE Rejected = 0 AND DeleteDateUTC IS NULL
					AND NOT EXISTS (SELECT 1 from dbo.fnOrderOrderRules(OrderID) where TypeID = 16 and dbo.fnToBool(value) = 1) -- Disable Gauger Validation
					AND (TicketTypeID IN (1) AND (ProductObsTemp IS NULL OR ProductObsGravity IS NULL OR ProductBSW IS NULL))
				)
			BEGIN
				SET @errorString = @errorString + '|All Product Measurement values are required for "Gauge Run Basic" tickets'
			END
			
			-- Opening Gauge values are only required for NOT-REJECTED Gauge Run Basic [1]
			IF EXISTS (SELECT * FROM inserted WHERE TicketTypeID IN (1) AND Rejected = 0 AND DeleteDateUTC IS NULL
				AND NOT EXISTS (SELECT 1 from dbo.fnOrderOrderRules(OrderID) where TypeID = 16 and dbo.fnToBool(value) = 1) -- Disable Gauger Validation
				AND (OpeningGaugeFeet IS NULL OR OpeningGaugeInch IS NULL OR OpeningGaugeQ IS NULL))
			BEGIN
				SET @errorString = @errorString + '|All Opening Gauge values are required for "Gauge Run Basic" tickets'
			END

			-- if any errors were detected, cancel the entire operation (transaction) and return a LINEFEED-deliminated list of errors
			IF (len(@errorString) > 0)
			BEGIN
				SET @errorString = replace(substring(@errorString, 2, 1000), '|', char(13) + char(10))
				RAISERROR(@errorString, 16, 1)
				IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
			END
			
			/**********  END OF VALIDATION SECTION ************************/
			
			-- update any existing OrderTicket records that are still only populated from Gauger entry
			UPDATE tblOrderTicket
				SET CarrierTicketNum = i.CarrierTicketNum
				, DispatchConfirmNum = i.DispatchConfirmNum
				, OriginTankID = i.OriginTankID
				, TankNum = i.TankNum
				, ProductObsGravity = i.ProductObsGravity
				, ProductObsTemp = i.ProductObsTemp
				, ProductBSW = i.ProductBSW
				, OpeningGaugeFeet = i.OpeningGaugeFeet
				, OpeningGaugeInch = i.OpeningGaugeInch
				, OpeningGaugeQ = i.OpeningGaugeQ
				, ProductHighTemp = i.ProductHighTemp
				, BottomFeet = i.BottomFeet
				, BottomInches = i.BottomInches
				, BottomQ = i.BottomQ
				, Rejected = i.Rejected
				, RejectReasonID = i.RejectReasonID
				, RejectNotes = i.RejectNotes
				, SealOff = i.SealOff
				, SealOn = i.SealOn
				, FromMobileApp = i.FromMobileApp
				, LastChangeDateUTC = i.LastChangeDateUTC
				, LastChangedByUser = i.LastChangedByUser
				, DeleteDateUTC = i.DeleteDateUTC
				, DeletedByUser = i.DeletedByUser
			FROM tblOrderTicket OT
			JOIN tblOrder O ON O.ID = OT.OrderID AND O.StatusID IN (-9, -10) -- GAUGER, GENERATED StatusID			
			JOIN inserted i ON i.UID = OT.UID AND i.CreateDateUTC = OT.CreateDateUTC
			LEFT JOIN deleted d ON d.UID = OT.UID AND isnull(d.LastChangeDateUTC, getutcdate()) = isnull(OT.LastChangeDateUTC, getutcdate())

			-- create new OrderTicket records not not present
			INSERT INTO tblOrderTicket (UID, OrderID, TicketTypeID, CarrierTicketNum, DispatchConfirmNum, OriginTankID, TankNum
				, ProductObsGravity, ProductObsTemp, ProductBSW, OpeningGaugeFeet, OpeningGaugeInch, OpeningGaugeQ, ProductHighTemp
				, BottomFeet, BottomInches, BottomQ, Rejected, RejectReasonID, RejectNotes, SealOff, SealOn, FromMobileApp
				, CreateDateUTC, CreatedByUser)
				SELECT i.UID, i.OrderID, GTT.TicketTypeID, i.CarrierTicketNum, i.DispatchConfirmNum, i.OriginTankID, i.TankNum
					, i.ProductObsGravity, i.ProductObsTemp, i.ProductBSW, i.OpeningGaugeFeet, i.OpeningGaugeInch, i.OpeningGaugeQ, i.ProductHighTemp
					, i.BottomFeet, i.BottomInches, i.BottomQ, i.Rejected, i.RejectReasonID, i.RejectNotes, i.SealOff, i.SealOn, i.FromMobileApp
					, i.CreateDateUTC, i.CreatedByUser
				FROM inserted i
				JOIN tblGaugerTicketType GTT ON GTT.ID = i.TicketTypeID
				LEFT JOIN tblOrderTicket OT ON OT.UID = i.UID
				WHERE OT.UID IS NULL

			/* START DB AUDIT *********************************************************/
			BEGIN TRY
				IF EXISTS(SELECT * FROM tblSetting WHERE ID = 32 AND (Value LIKE 'true' OR Value LIKE 'yes'))
					INSERT INTO tblGaugerOrderTicketDbAudit (DBAuditDate, UID, OrderID, CarrierTicketNum, TicketTypeID, TankNum, ProductObsGravity, ProductObsTemp, OpeningGaugeFeet, OpeningGaugeInch, OpeningGaugeQ, ProductHighTemp, Rejected, RejectNotes, SealOff, SealOn, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser, FromMobileApp, OriginTankID, BottomFeet, BottomInches, BottomQ, RejectReasonID)
						SELECT GETUTCDATE(), UID, OrderID, CarrierTicketNum, TicketTypeID, TankNum, ProductObsGravity, ProductObsTemp, OpeningGaugeFeet, OpeningGaugeInch, OpeningGaugeQ, ProductHighTemp, Rejected, RejectNotes, SealOff, SealOn, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser, FromMobileApp, OriginTankID, BottomFeet, BottomInches, BottomQ, RejectReasonID
						FROM deleted d
			END TRY
			BEGIN CATCH
				PRINT 'trigGaugerOrderTicket_IU.DBAUDIT FAILURE: ' + ERROR_MESSAGE()
			END CATCH
			/* END DB AUDIT *********************************************************/

			PRINT 'trigGaugerOrderTicket_IU COMPLETE'
		END
	END
END

GO


/*****************************************
-- Date Created: ??
-- Author: Kevin Alons, Modified by Geoff Mochau
-- Purpose: specialized, db-level logic enforcing business rules, audit changes, etc
-- Changes:
    - 3.8.12	- 2015/08/04 - GSM	- use the revised (3-temperature) API function
	- 3.8.10	- 2015/07/26 - KDA	- always update the associated tblOrder.LastChangeDateUTC to NOW when a Ticket is changed)
	- 3.9.29.5	- 2015/12/03 - JAE	- add ProductBSW to dbaudit trigger
	- 3.9.38	- 2015/12/17 - BB	- Add WeightTareUnits, WeightGrossUnits, and WeightNetUnits columns to db audit (DCWEB-972)
	- 3.9.38	- 2015/12/21 - BB	- Add validation, calculations, etc. for WeightTareUnits, WeightGrossUnits, and WeightNetUnits (DCWEB-972)
	- 3.10.5.2	- 2016/02/11 - JAE	- Update to order table to sync ticket changes also resets the Pickup Last Change Date
	- 3.11.8	- 2016/03/06 - KDA	- add NoDataCalc field logic (do not calculate quantities, ensure raw data is provided - validation)
	- 3.11.11   - 2016/04/08 - JMS  - add Scale Ticket Number to tblOrderTicketDbAudit
	- 3.12.4    - 2016/05/20 - GSM/JAE - add Water Run to Gross auto calculation
    - 3.12.5.1	- 2016/05/23 - JAE	- Added rounding per government standards
	- 3.12.6.3		- 2016/06/08 - JAE	- Suppress errors from gauger ticket when disable requirements are set
*****************************************/
ALTER TRIGGER [dbo].[trigOrderTicket_IU] ON [dbo].[tblOrderTicket] AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	-- Note: the TRIGGER_NESTLEVEL( OBJECT_ID('trigOrderTicket_IU') ) = 1 statement prevents recursion
	IF (TRIGGER_NESTLEVEL( OBJECT_ID('trigOrderTicket_IU')) = 1) BEGIN
		
		-- only do anything if something actually changed
		IF EXISTS (SELECT * FROM inserted EXCEPT SELECT * FROM deleted)
		BEGIN
			PRINT 'trigOrderTicket_IU FIRED'
			
			/**********  START OF VALIDATION SECTION ************************/
			DECLARE @errorString varchar(1000); SET @errorString = ''

			IF EXISTS (
				SELECT * 
				FROM tblOrder 
				WHERE ID IN (SELECT OrderID FROM inserted) 
				  -- prevent changes to any tickets belonging to a Delivered or Audited Order
				  AND StatusID IN (4))
			BEGIN
				IF (dbo.fnToBool(dbo.fnSettingValue(35)) = 1)
					PRINT 'Ticket for an AUDITED orders is being modified - please investigate why!'
				ELSE 				
					SET @errorString = @errorString + '|Tickets of Audited Orders cannot be modified'
			END

			-- prevent reassigning a ticket from one order to another
			IF EXISTS (
				SELECT i.* 
				FROM inserted i
				JOIN deleted d ON i.UID = d.UID
				WHERE i.OrderID <> d.OrderID)
			BEGIN
				SET @errorString = @errorString + '|Tickets cannot be moved to a different Order'
			END
			
			IF EXISTS (
				SELECT OT.OrderID
				FROM tblOrderTicket OT
				JOIN tblOrder O ON O.ID = OT.OrderID
				JOIN inserted i ON i.OrderID = OT.OrderID AND i.CarrierTicketNum = OT.CarrierTicketNum AND i.ID <> OT.ID
				WHERE OT.DeleteDateUTC IS NULL and I.DeleteDateUTC IS NULL
				GROUP BY OT.OrderID, OT.CarrierTicketNum
				HAVING COUNT(*) > 1
			)
			BEGIN
				SET @errorString = @errorString + '|Duplicate Ticket Numbers are not allowed'
			END
			
			-- store all the tickets for orders that are not in Generated status and not deleted (only these need validation)
			SELECT i.*, O.StatusID 
			INTO #i
			FROM inserted i
			JOIN tblOrder O ON O.ID = i.OrderID
			WHERE i.DeleteDateUTC IS NULL
			
			/* -- removed with version 3.7.5 
			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (3) AND BOLNum IS NULL)
			BEGIN
				SET @errorString = @errorString + '|BOL # value is required for Meter Run tickets'
			END
			*/
			
			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (1, 2) AND OriginTankID IS NULL AND TankNum IS NULL AND Rejected = 0)
			BEGIN
				SET @errorString = @errorString + '|Tank is required for Gauge Run & Net Volume tickets'
			END

			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (1) AND CarrierTicketNum IS NULL)
			BEGIN
				SET @errorString = @errorString + '|Ticket # value is required for Gauge Run tickets'
			END

			/* require all parameters for orders NOT IN (GAUGER, GENERATED, ASSIGNED, DISPATCHED) */
			IF  EXISTS (SELECT ID FROM #i WHERE Rejected =  0
					AND (   (TicketTypeID IN (2) AND (ProductObsTemp IS NULL OR ProductObsGravity IS NULL OR ProductBSW IS NULL))
						 OR (TicketTypeID IN (1) AND (ProductObsTemp IS NULL OR ProductObsGravity IS NULL OR ProductBSW IS NULL)
						      AND (   (NOT EXISTS (SELECT 1 from dbo.fnOrderOrderRules(OrderID) where TypeID = 16 and dbo.fnToBool(value) = 1)) -- Disable Gauger Validation
				                   OR (StatusID NOT IN (-9))))
						 OR (TicketTypeID IN (1) AND StatusID NOT IN (-9, -10, 1, 2) AND (ProductHighTemp IS NULL OR ProductLowTemp IS NULL))
					)
				)
			BEGIN
				SET @errorString = @errorString + '|All Product Measurement values are required for Gauge Run tickets'
			END
			
			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (1) AND Rejected = 0 
				AND (   (NOT EXISTS (SELECT 1 from dbo.fnOrderOrderRules(OrderID) where TypeID = 16 and dbo.fnToBool(value) = 1)) -- Disable Gauger Validation
				     OR (StatusID NOT IN (-9)))
				AND (OpeningGaugeFeet IS NULL OR OpeningGaugeInch IS NULL OR OpeningGaugeQ IS NULL))
			BEGIN
				SET @errorString = @errorString + '|All Opening Gauge values are required for Gauge Run tickets'
			END
			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (1) AND Rejected = 0 AND StatusID NOT IN (-9, -10, 1, 2)
				AND (ClosingGaugeFeet IS NULL OR ClosingGaugeInch IS NULL OR ClosingGaugeQ IS NULL))
			BEGIN
				SET @errorString = @errorString + '|All Closing Gauge values are required for Gauge Run tickets'
			END

			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (2) AND Rejected = 0 AND StatusID NOT IN (-9, -10, 1, 2) AND (GrossUnits IS NULL))
			BEGIN
				SET @errorString = @errorString + '|Gross Volume value is required for Net Volume tickets'
			END

			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (7) AND Rejected = 0 AND StatusID NOT IN (-9, -10, 1, 2) AND (GrossUnits IS NULL))
			BEGIN
				SET @errorString = @errorString + '|Gross Volume value is required for Gauge Net tickets'
			END

			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (3) AND Rejected = 0 AND StatusID NOT IN (-9, -10, 1, 2) AND (GrossUnits IS NULL OR NetUnits IS NULL))
			BEGIN
				SET @errorString = @errorString + '|Gross & Net Volume values are required for Meter Run tickets'
			END

			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (1) AND Rejected = 0 AND StatusID NOT IN (-9, -10, 1, 2) AND (SealOff IS NULL OR SealOn IS NULL))
			BEGIN
				SET @errorString = @errorString + '|All Seal Off & Seal On values are required for Gauge Run tickets'
			END
			
			-- 3.9.38 - 2015/12/21 - BB - Add WeightGrossUnits check
			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (9) AND Rejected = 0 AND StatusID NOT IN (-9, -10, 1, 2) AND (WeightGrossUnits IS NULL OR WeightNetUnits IS NULL))
			BEGIN
				SET @errorString = @errorString + '|Gross & Net Weight values are required for Mineral Run tickets.'
			END
			
			-- 3.9.38 - 2015/12/21 - BB - Add WeightTareUnits check
			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (9) AND Rejected = 0 AND StatusID NOT IN (-9, -10, 1, 2) AND (WeightTareUnits IS NULL))
			BEGIN
				SET @errorString = @errorString + '|Tare Weight is required for Mineral Run tickets.'
			END

			-- 3.11.8 - 2016/03/06 - KDA - ensure raw data is provided if NoDataCalc = 1 was set
			IF EXISTS (SELECT ID FROM #i WHERE NoDataCalc = 1 AND Rejected = 0 AND StatusID NOT IN (-9, -10, 1, 2) AND TicketTypeID NOT IN (9) AND (GrossUnits IS NULL OR NetUnits IS NULL))
			BEGIN
				SET @errorString = @errorString + '|Gross & Net Units must be provided for all Tickets with No Data Calc turned ON.'
			END

			-- if any errors were detected, cancel the entire operation (transaction) and return a LINEFEED-deliminated list of errors
			IF (len(@errorString) > 0)
			BEGIN
				SET @errorString = replace(substring(@errorString, 2, 1000), '|', char(13) + char(10))
				RAISERROR(@errorString, 16, 1)
				IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
				RETURN
			END
			/**********  END OF VALIDATION SECTION ************************/
			
			-- re-compute GaugeRun ticket Gross barrels
			IF UPDATE(OriginTankID) OR UPDATE(OpeningGaugeFeet) OR UPDATE(OpeningGaugeInch) OR UPDATE(OpeningGaugeQ)
				OR UPDATE (ClosingGaugeFeet) OR UPDATE(ClosingGaugeInch) OR UPDATE(ClosingGaugeQ) 
				OR UPDATE (GrossUnits)
			BEGIN
				UPDATE tblOrderTicket
				  SET GrossUnits = dbo.fnConvertUom(dbo.fnOriginTankStrappingLevelDeltaBarrels(
						OT.OriginTankID
					  , OpeningGaugeFeet
					  , OpeningGaugeInch
					  , OpeningGaugeQ
					  , ClosingGaugeFeet
					  , ClosingGaugeInch
					  , ClosingGaugeQ), 1, O.OriginUomID) -- ensure this value is always normalized from BARRELS
				FROM tblOrderTicket OT
				JOIN tblOrder O ON O.ID = OT.OrderID
				WHERE (OT.NoDataCalc = 0 OR OT.GrossUnits IS NULL) -- 3.11.8 - do not calculate these values when NoDataCalc is turned ON (1) and not provided
				  AND OT.ID IN (SELECT ID FROM inserted) 
				  AND OT.TicketTypeID IN (1, 11) -- Gauge Run Ticket & Production Water Run
				  AND OT.OpeningGaugeFeet IS NOT NULL
				  AND OT.OpeningGaugeInch IS NOT NULL
				  AND OT.OpeningGaugeQ IS NOT NULL
				  AND OT.ClosingGaugeFeet IS NOT NULL
				  AND OT.ClosingGaugeInch IS NOT NULL
				  AND OT.ClosingGaugeQ IS NOT NULL
			END
			-- recompute the GrossUnits of any changed Meter Run tickets
			IF UPDATE(OpenMeterUnits) OR UPDATE(CloseMeterUnits) BEGIN
				--Print 'updating tblOrderTicket GOV from Open/Close Meter Units'
				UPDATE tblOrderTicket 
					SET GrossUnits = CloseMeterUnits - OpenMeterUnits
				WHERE TicketTypeID = 3  -- meter run tickets ONLY
				  AND (NoDataCalc = 0 OR GrossUnits IS NULL)  -- 3.11.8 - do not calculate these values when NoDataCalc is turned ON (1) and not provided
				  AND OpenMeterUnits IS NOT NULL AND CloseMeterUnits IS NOT NULL
				
			END
			-- re-compute GaugeRun | Net Barrel | Meter Run tickets NetUnits
			IF UPDATE(OpeningGaugeFeet) OR UPDATE(OpeningGaugeInch) OR UPDATE(OpeningGaugeQ)
				OR UPDATE (ClosingGaugeFeet) OR UPDATE(ClosingGaugeInch) OR UPDATE(ClosingGaugeQ) 
				OR UPDATE(OriginTankID) OR UPDATE(GrossUnits) OR UPDATE(GrossStdUnits) OR UPDATE(NetUnits)
				OR UPDATE(ProductObsTemp) OR UPDATE(ProductObsGravity) OR UPDATE(ProductBSW)
				OR UPDATE(OpenMeterUnits) OR UPDATE(CloseMeterUnits)
				OR UPDATE(ProductHighTemp) OR UPDATE(ProductLowTemp)
			BEGIN
				--PRINT 'updating tblOrderTicket NSV/GSV'
				UPDATE tblOrderTicket
				  SET NetUnits = dbo.fnCrudeNetCalculator3T(GrossUnits, ProductObsTemp, ProductHighTemp, ProductLowTemp, ProductObsGravity, isnull(ProductBSW, 0), (SELECT TOP 1 u.SignificantDigits FROM tblUom u, tblOrder o WHERE o.OriginUomID = u.ID AND o.ID IN (SELECT OrderID FROM inserted)))
					, GrossStdUnits = dbo.fnCrudeNetCalculator3T(GrossUnits, ProductObsTemp, ProductHighTemp, ProductLowTemp, ProductObsGravity, 0, (SELECT TOP 1 u.SignificantDigits FROM tblUom u, tblOrder o WHERE o.OriginUomID = u.ID AND o.ID IN (SELECT OrderID FROM inserted)))
				WHERE ID IN (SELECT ID FROM inserted)
				  AND NoDataCalc = 0  -- 3.11.8 - do not calculate these values when NoDataCalc is turned ON (1)
				  AND TicketTypeID IN (1, 2, 7) -- GAUGE RUN, NET VOLUME, GAUGE NET
				  AND GrossUnits IS NOT NULL
				  AND ProductHighTemp IS NOT NULL
				  AND ProductLowTemp IS NOT NULL
				  AND ProductObsTemp IS NOT NULL
				  AND ProductObsGravity IS NOT NULL
			END
			
			-- 3.9.38 - 2015/12/21 - BB - recompute the Net Weight of any changed Mineral Run tickets (DCWEB-972)
			IF UPDATE(WeightGrossUnits) OR UPDATE(WeightTareUnits) BEGIN
				--Print 'updating tblOrderTicket WeightNetUnits from WeightGrossUnits and WeightTareUnits
				UPDATE tblOrderTicket
					SET WeightNetUnits = WeightGrossUnits - WeightTareUnits
				WHERE TicketTypeID = 9 -- Mineral Run tickets only
				  AND (NoDataCalc = 0 OR WeightNetUnits IS NULL) -- 3.11.8 - do not calculate these values when NoDataCalc is turned ON (1) and not provided
				  AND WeightGrossUnits IS NOT NULL AND  WeightTareUnits IS NOT NULL
			END 			
			
			-- ensure the Order record is in-sync with the Tickets
			/* test lines below
			declare @gov decimal(9,3); SELECT TOP 1 @gov = OT.GrossUnits FROM tblOrderTicket OT JOIN inserted i ON i.ID = OT.ID;
			PRINT 'updating tblOrder GOV/GSV/NSV GOV = ' + ltrim(@gov)
			--*/
			UPDATE tblOrder 
			SET OriginGrossUnits = (SELECT sum(GrossUnits) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND OT.Rejected = 0 AND OT.DeleteDateUTC IS NULL)
			  , OriginNetUnits = (SELECT sum(NetUnits) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND OT.Rejected = 0 AND OT.DeleteDateUTC IS NULL)
			  , OriginGrossStdUnits = (SELECT sum(GrossStdUnits) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND OT.Rejected = 0 AND OT.DeleteDateUTC IS NULL)
			    --3.9.38 - 2015/12/21 - BB - Update weight fields (DCWEB-972)  -- Per Maverick 12/21/15 we do not need gross on the order level.
			  --, OriginWeightGrossUnits = (SELECT sum(WeightGrossUnits) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND OT.Rejected = 0 AND OT.DeleteDateUTC IS NULL)
			  , OriginWeightNetUnits = 	(SELECT sum(WeightNetUnits) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND OT.Rejected = 0 AND OT.DeleteDateUTC IS NULL)
				-- use the first MeterRun/BasicRun BOLNum as the Order BOL Num (favor the first un-Rejected)
			  , OriginBOLNum = (
					SELECT TOP 1 BOLNum FROM (
						SELECT TOP 1 BOLNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID NOT IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
						UNION SELECT TOP 1 BOLNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID NOT IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X)
				-- use the first GaugeRun|NetVolume CarrierTicketNum as the Order.CarrierTicketNum (favor first un-Rejected)
			  , CarrierTicketNum = (
					SELECT TOP 1 CarrierTicketNum FROM (
						SELECT TOP 1 CarrierTicketNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
						UNION SELECT TOP 1 CarrierTicketNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X)
				-- use the first GaugeRun|NetVolume OriginTankID for the Order (favor first un-Rejected ticket)
				-- BB included ticket additional ticket type (Gauge Net-7)
			  , OriginTankID = (
					SELECT TOP 1 OriginTankID FROM (
						SELECT TOP 1 OriginTankID FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2,7) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
						UNION SELECT TOP 1 OriginTankID FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2,7) AND DeleteDateUTC IS NULL ORDER BY ID) X)
				-- use the first GaugeRun|NetVolume OriginTankNum for the Order (favor first un-Rejected ticket)
			  , OriginTankNum = (
					SELECT TOP 1 OriginTankNum FROM (
						SELECT TOP 1 OriginTankNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
						UNION SELECT TOP 1 OriginTankNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X)
			  , PickupLastChangeDateUTC = GETUTCDATE() -- 3.10.5.2 info tied to the pickup has been changed
			  , LastChangeDateUTC = GETUTCDATE() -- 3.8.10 update
			  , LastChangedByUser = (SELECT MIN(isnull(LastChangedByUser, CreatedByUser)) FROM inserted WHERE OrderID = O.ID)
			FROM tblOrder O
			WHERE ID IN (SELECT DISTINCT OrderID FROM inserted)

			/* START DB AUDIT *********************************************************/
			BEGIN TRY
				IF EXISTS(SELECT * FROM tblSetting WHERE ID = 32 AND (Value LIKE 'true' OR Value LIKE 'yes'))
					INSERT INTO tblOrderTicketDbAudit (DBAuditDate, ID, OrderID, CarrierTicketNum, TicketTypeID, TankNum, ProductObsGravity
													, ProductObsTemp, OpeningGaugeFeet, OpeningGaugeInch, OpeningGaugeQ, ClosingGaugeFeet, ClosingGaugeInch
													, ClosingGaugeQ, GrossUnits, NetUnits, Rejected, RejectNotes, SealOff, SealOn, BOLNum, ProductHighTemp
													, ProductLowTemp, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC
													, DeletedByUser, UID, FromMobileApp, OriginTankID, GrossStdUnits, BottomFeet, BottomInches
													, BottomQ, RejectReasonID, MeterFactor, OpenMeterUnits, CloseMeterUnits, ProductBSW, WeightTareUnits
													, WeightGrossUnits, WeightNetUnits, NoDataCalc, ScaleTicketNum)
						SELECT GETUTCDATE(), ID, OrderID, CarrierTicketNum, TicketTypeID, TankNum, ProductObsGravity, ProductObsTemp, OpeningGaugeFeet
							, OpeningGaugeInch, OpeningGaugeQ, ClosingGaugeFeet, ClosingGaugeInch, ClosingGaugeQ, GrossUnits, NetUnits, Rejected, RejectNotes
							, SealOff, SealOn, BOLNum, ProductHighTemp, ProductLowTemp, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser
							, DeleteDateUTC, DeletedByUser, UID, FromMobileApp, OriginTankID, GrossStdUnits, BottomFeet, BottomInches, BottomQ, RejectReasonID
							, MeterFactor, OpenMeterUnits, CloseMeterUnits, ProductBSW, WeightTareUnits, WeightGrossUnits, WeightNetUnits, NoDataCalc, ScaleTicketNum
						FROM deleted d
			END TRY
			BEGIN CATCH
				PRINT 'trigOrderTicket_IU.DBAUDIT FAILURE: ' + ERROR_MESSAGE()
			END CATCH
			/* END DB AUDIT *********************************************************/

			PRINT 'trigOrderTicket_IU COMPLETE'
		END
	END	
END

GO


/*******************************************
-- Date Created: 1 Mar 2015
-- Author: Kevin Alons
-- Purpose: return Order Rules for every changed Order
-- Changes:
	- 3.12.6.3		2016/06/08	JAE		Omitted unrelated joins and removed clause so all orders pass each time
*******************************************/
ALTER FUNCTION fnOrderRules_GaugerApp(@GaugerID int) 
RETURNS 
	@ret TABLE (
		ID int
	  , OrderID int
	  , TypeID int
	  , Value varchar(255)
	)
AS BEGIN
	DECLARE @IDS TABLE (ID int)
	INSERT INTO @IDS
		SELECT O.ID
		FROM dbo.viewOrder O
		JOIN tblGaugerOrder GAO ON GAO.OrderID = O.ID
		LEFT JOIN tblGaugerOrderVirtualDelete GOVD ON GOVD.OrderID = O.ID AND GOVD.GaugerID = GAO.GaugerID
	WHERE GAO.StatusID NOT IN (1) -- don't include REQUESTED Gauger Order records
	  AND O.ID IN (
		SELECT OrderID FROM tblGaugerOrder WHERE GaugerID = @GaugerID
		UNION 
		SELECT OrderID FROM tblGaugerOrderVirtualDelete WHERE GaugerID = @GaugerID
	)
	
	DECLARE @ID int
	WHILE EXISTS (SELECT * FROM @IDS)
	BEGIN
		SELECT TOP 1 @ID = ID FROM @IDS
		INSERT INTO @ret (ID, OrderID, TypeID, Value)
			SELECT ID, @ID, TypeID, Value FROM dbo.fnOrderOrderRules(@ID)
		DELETE FROM @IDS WHERE ID = @ID
		-- if no records exist, then return a NULL record to tell the Mobile App to delete any existing records for this order
		IF NOT EXISTS (SELECT * FROM @ret WHERE OrderID = @ID)
			INSERT INTO @ret (OrderID) VALUES (@ID)
	END
	RETURN
END

GO

COMMIT
SET NOEXEC OFF