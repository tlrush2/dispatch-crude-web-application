SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.10.9.2'
SELECT  @NewVersion = '3.10.10'

-- only ensure the curr version matches when we are not testing with an "x.x.x" DEV version
IF @NewVersion <> 'x.x.x' AND (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

-- only update the DB VERSION when not testing with an "x.x.x" DEV version
IF @NewVersion <> 'x.x.x' 
BEGIN
	UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

	INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
		SELECT @NewVersion, 1, 'DCWEB-1027 - Add & honor new "Restrict User to Profile Region" System-Wide Setting (Applies to Order Create and Dispatch pages)'
		EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
END
GO

INSERT INTO tblSetting (ID, Name, SettingTypeID, Value, Category, ReadOnly, CreatedByUser)
	SELECT 58, 'Restrict Users to Profile Region', 2, 'False', 'System Wide', 0, 'System'
GO

/****************************************************
-- Date Created: 25 Jan 2015
-- Author: Kevin Alons
-- Purpose: calculate DailyUnits value
** Changes:
- 3.8.4 - 2015/07/10 - KDA - fix DailyUnits being shorted by 1 day (was causing a DIVIDE BY ZERO error for 1 day allocation entry
- 3.10.10 - 2016/02/14 - KDA - add RegionID column to returned data
****************************************************/
ALTER VIEW viewAllocationDestinationShipper AS
	SELECT ADS.*
		, D.RegionID
		, DailyUnits = Units / cast(DATEDIFF(day, EffectiveDate, EndDate) + 1 as decimal(18, 10))
		, Destination = D.FullName
		, Shipper = C.Name
		, ProductGroup = PG.Name
		, Uom = U.Name
	FROM tblAllocationDestinationShipper ADS
	JOIN viewDestination D ON D.ID = ADS.DestinationID
	JOIN tblCustomer C ON C.ID = ADS.ShipperID
	JOIN tblProductGroup PG ON PG.ID = ADS.ProductGroupID
	JOIN tblUom U ON U.ID = ADS.UomID
GO

/********************************************
 Date Created: 2015/07/07
 Author: Kevin Alons
 Purpose: retrieve the DAILY Allocation + Production Units for the specified selection criteria + Date Range
 Changes:
 - 3.10.10	2016/02/14	KDA		add @RegionID parameter
********************************************/
ALTER PROCEDURE spAllocationDestinationShipper_Daily_ByMonth
( 
  @RegionID int = -1
, @DestinationID int = -1
, @ShipperID int = -1
, @ProductGroupID int = -1
, @StartDate date = NULL
, @EndDate date = NULL
, @UomID int = 1
) AS BEGIN

SET NOCOUNT ON 
	DECLARE @ret TABLE
	(
	  DestinationID int
	, ShipperID int
	, ProductGroupID int
	, UomID int
	, DayDate date
	, AllocatedUnits money
	, ProductionUnits money
	, LoadCount int
	, _ID int
	, _Processed bit
	, UNIQUE(DestinationID, ShipperID, ProductGroupID, DayDate)
	) 
	
	IF @StartDate IS NULL SET @StartDate = dbo.fnFirstDOM(getdate())
	IF @EndDate IS NULL SET @EndDate = dbo.fnLastDOM(@StartDate)
	
	DECLARE @QStart date, @QEnd date
	SELECT @QStart = dbo.fnFirstDOM(@StartDate), @QEnd = dbo.fnLastDOM(@EndDate)

	-- retrieve the relevant defined Allocation records (by DayDate), normalized to BBLS UOM
	INSERT INTO @ret (DestinationID, ShipperID, ProductGroupID, UomID, DayDate, AllocatedUnits, ProductionUnits, _Processed, _ID)
		SELECT ret.DestinationID, ret.ShipperID, ret.ProductGroupID, @UomID, DD.DayDate, Units = sum(dbo.fnConvertUOM(DailyUnits, UomID, @UomID)), 0, 0
			, ROW_NUMBER() OVER (ORDER BY DayDate, DestinationID, ShipperID, ProductGroupID)
		FROM viewAllocationDestinationShipper ret
		CROSS APPLY fnDaysInDateRange(@StartDate, @EndDate) DD 
		WHERE (@QStart BETWEEN ret.EffectiveDate AND ret.EndDate OR @QEnd BETWEEN ret.EffectiveDate AND ret.EndDate OR ret.EffectiveDate BETWEEN @QStart AND @QEnd)
		  AND (@RegionID = -1 OR ret.RegionID = @RegionID)
		  AND (@DestinationID = -1 OR ret.DestinationID = @DestinationID)
		  AND (@ShipperID = -1 OR ret.ShipperID = @ShipperID)
		  AND (@ProductGroupID = -1 OR ret.ProductGroupID = @ProductGroupID)
		GROUP BY ret.DestinationID, ret.ShipperID, ret.ProductGroupID, DD.DayDate

	-- retrieve the relevant, eligible DestGrossUnits (or OriginGrossUnits if not available) totals (normalized to BBLS UOM)
	DECLARE @PDS TABLE (DID int, SID int, PGID int, FDOM date, ProdUnits decimal(18, 4), LC int, _ID int, UNIQUE(DID, SID, PGID, FDOM))
	INSERT INTO @PDS
		SELECT O.DestinationID, O.CustomerID, P.ProductGroupID, dbo.fnFirstDOM(O.DeliverDate)
			, Units = sum(isnull(dbo.fnConvertUOM(nullif(O.DestGrossUnits, 0), DestUomID, @UomID), dbo.fnConvertUOM(nullif(O.OriginGrossUnits, 0), OriginUomID, @UomID)))
			, LC = COUNT(*)
			, ROW_NUMBER() OVER (ORDER BY O.DestinationID)
		FROM viewOrderBase O
		JOIN tblProduct P ON P.ID = O.ProductID
		WHERE DeliverDate BETWEEN @QStart AND @QEnd
		  AND DeleteDateUTC IS NULL
		  AND Rejected = 0
		  AND StatusID IN (3, 4)
		  AND (@DestinationID = -1 OR O.DestinationID = @DestinationID)
		  AND (@ShipperID = -1 OR O.CustomerID = @ShipperID)
		  AND (@ProductGroupID = -1 OR P.ProductGroupID = @ProductGroupID)
		GROUP BY O.DestinationID, O.CustomerID, P.ProductGroupID, dbo.fnFirstDOM(O.DeliverDate)

--select * from @ret
--select * from @PDS
	DECLARE @_ID int, @DID int, @SID int, @PGID int, @FDOM date, @Units decimal(18, 4), @LC int, @DD date, @done bit
	SELECT TOP 1 @_ID = _ID, @done = 0 FROM @PDS ORDER BY _ID
	WHILE EXISTS (SELECT * FROM @PDS)
	BEGIN
		SELECT TOP 1 @DID = DID, @SID = SID, @PGID = PGID, @FDOM = FDOM, @Units = ProdUnits, @LC = LC, @DD = FDOM, @done = 0 FROM @PDS WHERE _ID = @_ID
		WHILE MONTH(@DD) = MONTH(@FDOM)
		BEGIN
			-- set @done = FALSE and ensure @ROWCOUNT is reset to 0
			UPDATE @ret SET _Processed = 0 -- ensure they are all reset to FALSE
			SELECT @done = 0 WHERE @@ROWCOUNT >= 0
			-- sequentially "assign" the Production units against the daily allocation records (oldest to newest)
			WHILE (@done = 0)
			BEGIN
				UPDATE @ret
				  SET ProductionUnits = dbo.fnMinDecimal(AllocatedUnits, @Units), @Units = @Units - dbo.fnMinInt(AllocatedUnits, @Units), LoadCount = @LC, @LC = 0, _Processed = 1
				WHERE _ID IN (SELECT TOP 1 _ID FROM @ret WHERE DestinationID = @DID AND ShipperID = @SID AND ProductGroupID = @PGID AND DayDate = @DD AND _Processed = 0 ORDER BY _ID)
				SELECT @done = CASE WHEN @@ROWCOUNT = 0 THEN 1 ELSE 0 END
			END
			SET @DD = DATEADD(day, 1, @DD)
		END
		-- if remaining production units exist, then create a new record for these EXCESS units
		IF (@Units >= 1)
		BEGIN
			UPDATE @ret SET ProductionUnits = ProductionUnits + @Units, LoadCount = LoadCount + @LC WHERE DestinationID = @DID AND ShipperID = @SID AND ProductGroupID = @PGID AND DayDate = @FDOM
			IF @@ROWCOUNT = 0
				INSERT INTO @ret (DestinationID, ShipperID, ProductGroupID, UomID, DayDate, ProductionUnits, LoadCount)
					SELECT @DID, @SID, @PGID, @UomID, @FDOM, @Units, @LC
		END

		DELETE FROM @PDS WHERE _ID = @_ID
		SELECT TOP 1 @_ID = _ID FROM @PDS ORDER BY _ID
	END;
	
	WITH cteBase AS 
	(
		SELECT DestinationID, Destination = D.Name, ShipperID, Shipper = C.Name, ProductGroupID, ProductGroup = PG.Name, X.UomID, Uom = U.Name
			, DayDate, AllocatedUnits = ISNULL(AllocatedUnits, 0), ProductionUnits, LoadCount  
		FROM @ret X
		JOIN tblDestination D ON D.ID = X.DestinationID
		JOIN tblCustomer C ON C.ID = X.ShipperID
		JOIN tblProductGroup PG ON PG.ID = X.ProductGroupID
		JOIN tblUom U ON U.ID = X.UomID
	)

	SELECT Destination, Shipper, ProductGroup, Uom, DayDate, UnitType
		, Units = round(SUM(Units), 0)
		, RemainingDays = (SELECT RemainingDays = CASE WHEN MONTH(getdate()) = MONTH(@StartDate) THEN DATEDIFF(day, getdate(), dbo.fnLastDOM(getdate())) + 1 ELSE 0 END)
	FROM (
		SELECT DestinationID, Destination, ShipperID, Shipper, ProductGroup, ProductGroupID, UomID, Uom, DayDate
			, Units, UnitType
		FROM (
			SELECT DestinationID, Destination, ShipperID, Shipper, ProductGroup, ProductGroupID, UomID, Uom, DayDate
				, Units = dbo.fnMinDecimal(ProductionUnits, AllocatedUnits), UnitType = 'Production' FROM cteBase
		) X
		WHERE Units <> 0
		UNION
		SELECT DestinationID, Destination, ShipperID, Shipper, ProductGroup, ProductGroupID, UomID, Uom, DayDate
		, Units = abs(Units), UnitType = CASE WHEN Units < 0 THEN ' Excess' ELSE ' Remaining' END
		FROM (
			SELECT DestinationID, Destination, ShipperID, Shipper, ProductGroup, ProductGroupID, UomID, Uom, DayDate
				, Units = isnull(AllocatedUnits, 0) - isnull(ProductionUnits, 0) 
			FROM cteBase
		) X
		WHERE Units <> 0
	) X
	GROUP BY Destination, Shipper, ProductGroup, Uom, DayDate, UnitType
END
GO



/***********************************
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Order records with "translated friendly" values for FK relationships
-- Changes:
	- 3.9.20 - 2015/10/22 - KDA - add Origin|Dest DriverID fields (using new tblOrderTransfer table) 
			 - 2015/10/28 - JAE - Added all Order Transfer fields for ease of use in reporting
			 - 2015/11/03 - BB  - added cast to make TransferComplet BIT type to avoid "Operand type clash" error when running this update script
	- 3.9.21 - 2015/11/03 - KDA - add OriginDriverGroupID field (from OrderTransfer.OriginDriverID JOIN)
	- 3.9.25 - 2015/11/10 - JAE - added origin driver and truck to view
	- 3.10.10 - 2016/02/15 - BB - Add driver region ID to facilitate new user/region filtering feature
***********************************/
ALTER VIEW [dbo].[viewOrder] AS
SELECT O.*
	-- GENERATED orders that were GAUGED to COMPLETION or SKIPPED will show "Gauger [gauger status]"
	, PrintStatus = CASE WHEN O.StatusID = -10 AND GOS.IsComplete = 1 THEN 'Gauger ' + GOS.Name ELSE OPS.OrderStatus END
FROM (
	SELECT O.*
	, Origin = vO.Name 
	, OriginFull = vO.FullName 
	, OriginState = vO.State 
	, OriginStateAbbrev = vO.StateAbbrev 
	, OriginStation = vO.Station
	, OriginCounty = vO.County 
	, vO.LeaseName
	, vO.LeaseNum
	, OriginRegion = vO.Region
	, OriginCountryID = vO.CountryID
	, OriginCountry = vO.Country
	, OriginCountryShort = vO.CountryShort
	, OriginLegalDescription = vO.LegalDescription 
	, OriginNDIC = vO.NDICFileNum 
	, OriginNDM = vO.NDM 
	, OriginCA = vO.CA 
	, OriginTimeZoneID = vO.TimeZoneID 
	, OriginUseDST = vO.UseDST 
	, vO.H2S
	, Destination = vD.Name 
	, DestinationFull = vD.FullName 
	, DestinationState = vD.State 
	, DestinationStateAbbrev = vD.StateAbbrev 
	, DestinationTypeID = vD.ID
	, vD.DestinationType
	, DestStation = vD.Station 
	, DestTimeZoneID = vD.TimeZoneID 
	, DestUseDST = vD.UseDST 
	, DestCountryID = vD.CountryID
	, DestCountry = vD.Country
	, DestCountryShort = vD.CountryShort
	, Customer = C.Name 
	, Carrier = CA.Name 
	, CarrierType = CT.Name 
	, OS.OrderStatus
	, OS.StatusNum
	, Driver = D.FullName 
	, DriverFirst = D.FirstName 
	, DriverLast = D.LastName 
	, DriverRegionID = D.RegionID  -- 3.10.10
	, Truck = TRU.FullName 
	, Trailer = TR1.FullName 
	, Trailer2 = TR2.FullName 
	, P.PriorityNum
	, TicketType = TT.Name 
	, DestTicketTypeID = vD.TicketTypeID 
	, DestTicketType = vD.TicketType 
	, Operator = OP.Name 
	, Producer = PR.Name 
	, Pumper = PU.FullName 
	, DriverNumber = D.IDNumber
	, CarrierNumber = CA.IDNumber
	, CarrierTypeID = CA.CarrierTypeID
	, TruckNumber = TRU.IDNumber 
	, TrailerNumber = TR1.IDNumber 
	, Trailer2Number = TR2.IDNumber 
	, Product = PRO.Name
	, ProductShort = PRO.ShortName 
	, PRO.ProductGroup
	, OriginUOM = OUom.Name 
	, OriginUomShort = OUom.Abbrev 
	, OriginTankID_Text = CASE WHEN ORT.TankNum = '*' THEN '*' + isnull(O.OriginTankNum, '?') ELSE ORT.TankNum END 
	, DestUOM = DUom.Name 
	, DestUomShort = DUom.Abbrev 
	, Active = CAST((CASE WHEN O.DeleteDateUTC IS NULL THEN 1 ELSE 0 END) AS BIT) 
	, IsDeleted = CAST((CASE WHEN O.DeleteDateUTC IS NOT NULL THEN 1 ELSE 0 END) AS BIT) 
	, PickupPrintStatus = PPS.Name 
	, PickupCompleted = PPS.IsCompleted 
	, DeliverPrintStatus = DPS.Name 
	, DeliverCompleted = DPS.IsCompleted 
	, PrintStatusID = CASE WHEN O.StatusID = -9 THEN ISNULL(-GAO.StatusID, O.StatusID)
						   WHEN O.StatusID = 8 AND PPS.IsCompleted = 0 THEN 7
						   WHEN O.StatusID = 3 AND DPS.IsCompleted = 0 THEN 8
						ELSE O.StatusID END 
	, OriginTankText = CASE WHEN O.OriginTankID IS NULL OR ORT.TankNum = '*' THEN O.OriginTankNum ELSE ORT.TankNum END
	, OriginWaitNum = OWR.Num
	, OriginWaitDesc = OWR.Description
	, OriginWaitNumDesc = OWR.NumDesc
	, DestWaitNum = DWR.Num
	, DestWaitDesc = DWR.Description
	, DestWaitNumDesc = DWR.NumDesc
	, RejectNum = ORR.Num
	, RejectDesc = ORR.Description
	, RejectNumDesc = ORR.NumDesc
	, RerouteCount = (SELECT COUNT(1) FROM tblOrderReroute ORE WHERE ORE.OrderID = O.ID)
	, TicketCount = (SELECT COUNT(1) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND OT.DeleteDateUTC IS NULL)
	, TotalMinutes = ISNULL(OriginMinutes, 0) + ISNULL(DestMinutes, 0)
	-- TRANSFER FIELDS
	, OriginDriverID = ISNULL(OTR.OriginDriverID, O.DriverID)
	, OriginDriverGroupID = ISNULL(ODG.ID, DDG.ID) 
	, OriginDriverGroup = ISNULL(ODG.Name, DDG.Name)
	, OriginDriver = ISNULL(vODR.FullName, vDDR.FullName)
	, OriginDriverFirst = ISNULL(vODR.FirstName, vDDR.FirstName)
	, OriginDriverLast = ISNULL(vODR.LastName, vDDR.LastName)
	, OriginTruckID = ISNULL(OTR.OriginTruckID, O.TruckID)
	, OriginTruck = ISNULL(vOTRU.FullName, vDTRU.FullName)
	, DestDriverID = O.DriverID
	, DestDriver = D.FullName
	, DestDriverFirst = D.FirstName 
	, DestDriverLast = D.LastName 
	, DestTruckID = O.TruckID
	, DestTruck = TRU.FullName 
	, OriginTruckEndMileage = OTR.OriginTruckEndMileage
	, DestTruckStartMileage = OTR.DestTruckStartMileage
	, TransferPercent = OTR.PercentComplete
	, TransferNotes = OTR.Notes
	, TransferDateUTC = OTR.CreateDateUTC
	, TransferComplete = CAST(OTR.TransferComplete AS BIT)
	, IsTransfer = CAST((CASE WHEN OTR.OrderID IS NOT NULL THEN 1 ELSE 0 END) AS BIT)	
	--
	, GaugerStatusID = GAO.StatusID
	, GaugerID = GAO.GaugerID	
	FROM dbo.viewOrderBase O
	JOIN dbo.tblOrderStatus AS OS ON OS.ID = O.StatusID
	LEFT JOIN dbo.tblOrderTransfer OTR ON OTR.OrderID = O.ID
	LEFT JOIN dbo.viewOrigin vO ON vO.ID = O.OriginID
	LEFT JOIN dbo.viewDestination vD ON vD.ID = O.DestinationID
	LEFT JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
	LEFT JOIN dbo.tblCarrier CA ON CA.ID = O.CarrierID
	LEFT JOIN dbo.tblCarrierType CT ON CT.ID = CA.CarrierTypeID
	LEFT JOIN dbo.viewDriverBase D ON D.ID = O.DriverID
	LEFT JOIN dbo.viewDriver vODR ON vODR.ID = OTR.OriginDriverID
	LEFT JOIN dbo.viewDriver vDDR ON vDDR.ID = O.DriverID
	LEFT JOIN dbo.tblDriverGroup ODG ON ODG.ID = vODR.DriverGroupID
	LEFT JOIN dbo.tblDriverGroup DDG ON DDG.ID = O.DriverGroupID
	LEFT JOIN dbo.viewTruck TRU ON TRU.ID = O.TruckID
	LEFT JOIN dbo.viewTruck vOTRU ON vOTRU.ID = vODR.TruckID
	LEFT JOIN dbo.viewTruck vDTRU ON vDTRU.ID = O.TruckID
	LEFT JOIN dbo.viewTrailer TR1 ON TR1.ID = O.TrailerID
	LEFT JOIN dbo.viewTrailer TR2 ON TR2.ID = O.Trailer2ID
	LEFT JOIN dbo.tblPriority P ON P.ID = O.PriorityID
	LEFT JOIN dbo.tblTicketType TT ON TT.ID = O.TicketTypeID
	LEFT JOIN dbo.tblOriginTank ORT ON ORT.ID = O.OriginTankID
	LEFT JOIN dbo.tblOperator OP ON OP.ID = O.OperatorID
	LEFT JOIN dbo.viewPumper PU ON PU.ID = O.PumperID
	LEFT JOIN dbo.tblProducer PR ON PR.ID = O.ProducerID
	LEFT JOIN dbo.viewProduct PRO ON PRO.ID = O.ProductID
	LEFT JOIN dbo.tblUom OUom ON OUom.ID = O.OriginUomID
	LEFT JOIN dbo.tblUom DUom ON DUom.ID = O.DestUomID
	LEFT JOIN dbo.tblPrintStatus PPS ON PPS.ID = O.PickupPrintStatusID
	LEFT JOIN dbo.tblPrintStatus DPS ON DPS.ID = O.DeliverPrintStatusID
	LEFT JOIN dbo.viewOriginWaitReason OWR ON OWR.ID = O.OriginWaitReasonID
	LEFT JOIN dbo.viewDestinationWaitReason DWR ON DWR.ID = O.DestWaitReasonID
	LEFT JOIN dbo.viewOrderRejectReason ORR ON ORR.ID = O.RejectReasonID	
	LEFT JOIN dbo.tblGaugerOrder GAO ON GAO.OrderID = O.ID
	LEFT JOIN dbo.viewGaugerBase G ON G.ID = GAO.GaugerID
) O
LEFT JOIN dbo.viewOrderPrintStatus OPS ON OPS.ID = O.PrintStatusID
LEFT JOIN dbo.tblGaugerOrderStatus GOS ON GOS.ID = O.GaugerStatusID
GO

EXEC _spRefreshAllViews
GO

COMMIT
SET NOEXEC OFF