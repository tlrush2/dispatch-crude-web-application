SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.1.8.3'
SELECT  @NewVersion = '4.1.8.4'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1786: Driver settlement page permission issue fixed.'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO



-- Remove permission for non existant page (Driver wait fee parameters does not exist but a permission had been created for it)
DELETE FROM aspnet_RolesInGroups
WHERE RoleId = (SELECT RoleId FROM aspnet_Roles WHERE RoleName = 'viewDriverWaitFeeParameters')
GO

DELETE FROM aspnet_UsersInRoles
WHERE RoleId = (SELECT RoleId FROM aspnet_Roles WHERE RoleName = 'viewDriverWaitFeeParameters')
GO

DELETE FROM aspnet_Roles
WHERE RoleId = (SELECT RoleId FROM aspnet_Roles WHERE RoleName = 'viewDriverWaitFeeParameters')
GO



-- Update existing producer settlement role to match the rest of the roles
UPDATE aspnet_Roles
SET RoleName = 'viewProducerSettlement'
	, LoweredRoleName = 'viewproducersettlement'
	, Description = 'Allow user to view Producer Order Settlement page (Requires Commodity Pricing)'
WHERE RoleName = 'settleProducer'
GO



-- Add new permission for producer settlement batches to conform with the other settlement permissions
EXEC spAddNewPermission 'viewProducerSettlementBatches','Allow user to view the Producer Settlement Batches page','Settlement', 'Producer Batches'
GO


-- Update Carrier/Shipper role names (fixing a typo that was just noticed)
UPDATE aspnet_Roles
SET RoleName = 'viewCarrierAssessorialRates'
	, LoweredRoleName = 'viewcarrierassessorialrates'	
WHERE RoleName = 'viewCarrierAssorialRates'
GO

UPDATE aspnet_Roles
SET RoleName = 'viewShipperAssessorialRates'
	, LoweredRoleName = 'viewshipperassessorialrates'	
WHERE RoleName = 'viewShipperAssorialRates'
GO


COMMIT
SET NOEXEC OFF