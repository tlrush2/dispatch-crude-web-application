-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.1.0.2'
SELECT  @NewVersion = '4.1.0.3'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-945 - Add Independent Driver/Driver Group Settlement - table index additions/fixes'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

if ((select count(*) from tblReportColumnDefinition where id between 305 and 341) >= 36)
	delete from tblreportcolumndefinition where id between 305 and 341

SET IDENTITY_INSERT tblReportColumnDefinition ON 

INSERT INTO tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport)
	SELECT '306', '1', 'DriverH2SRate', 'SETTLEMENT | DRIVER | Driver H2S Rate', '#0.0000', NULL, '4', NULL, '1', 'viewDriverSettlement', '1'
	UNION SELECT '307', '1', 'DriverH2SAmount', 'SETTLEMENT | DRIVER | Driver H2S Fee', '#0.00', NULL, '4', NULL, '1', 'viewDriverSettlement', '1'
	UNION SELECT '308', '1', 'DriverChainupAmount', 'SETTLEMENT | DRIVER | Driver Chainup Fee', '#0.00', NULL, '4', NULL, '1', 'viewDriverSettlement', '1'
	UNION SELECT '309', '1', 'DriverTaxRate', 'SETTLEMENT | DRIVER | Driver Origin Tax Rate %', '#0.0000', NULL, '4', NULL, '1', 'viewDriverSettlement', '1'
	UNION SELECT '310', '1', 'DriverRerouteAmount', 'SETTLEMENT | DRIVER | Driver Reroute Fee', '#0.00', NULL, '4', NULL, '1', 'viewDriverSettlement', '1'
	UNION SELECT '311', '1', 'DriverRerouteRate', 'SETTLEMENT | DRIVER | Driver Reroute Rate', '#0.00', NULL, '4', NULL, '1', 'viewDriverSettlement', '1'
	UNION SELECT '312', '1', 'DriverOriginWaitRate', 'SETTLEMENT | DRIVER | Driver Origin Wait Rate', '#0.0000', NULL, '4', NULL, '1', 'viewDriverSettlement', '1'
	UNION SELECT '313', '1', 'DriverOriginWaitAmount', 'SETTLEMENT | DRIVER | Driver Origin Wait Fee', '#0.00', NULL, '4', NULL, '1', 'viewDriverSettlement', '1'
	UNION SELECT '314', '1', 'DriverDestinationWaitRate', 'SETTLEMENT | DRIVER | Driver Dest Wait Rate', '#0.0000', NULL, '4', NULL, '1', 'viewDriverSettlement', '1'
	UNION SELECT '315', '1', 'DriverDestinationWaitAmount', 'SETTLEMENT | DRIVER | Driver Dest Wait Fee', '#0.00', NULL, '4', NULL, '1', 'viewDriverSettlement', '1'
	UNION SELECT '316', '1', 'DriverTotalWaitAmount', 'SETTLEMENT | DRIVER | Driver Total Wait Fee', '#0.00', NULL, '4', NULL, '1', 'viewDriverSettlement', '1'
	UNION SELECT '317', '1', 'DriverTotalAmount', 'SETTLEMENT | DRIVER | Driver Total Fee', '#0.00', NULL, '4', NULL, '1', 'viewDriverSettlement', '1'
	UNION SELECT '318', '1', 'DriverSettlementUom', 'SETTLEMENT | DRIVER | Driver Settlement UOM', NULL, 'CarrierSettlementUomID', '2', 'SELECT ID, Name = Abbrev FROM tblUom ORDER BY Abbrev', '1', 'viewDriverSettlement', '1'
	UNION SELECT '319', '1', 'DriverRouteRate', 'SETTLEMENT | DRIVER | Driver Override Route Rate', '#0.0000', NULL, '4', NULL, '1', 'viewDriverSettlement', '1'
	UNION SELECT '320', '1', 'DriverLoadAmount', 'SETTLEMENT | DRIVER | Driver Route Fee', '#0.00', NULL, '4', NULL, '1', 'viewDriverSettlement', '1'
	UNION SELECT '321', '1', 'DriverBatchNum', 'SETTLEMENT | DRIVER | Driver Batch #', NULL, NULL, '4', NULL, '1', 'viewDriverSettlement', '0'
	UNION SELECT '322', '1', 'DriverFuelSurchargeAmount', 'SETTLEMENT | DRIVER | Driver Fuel Surcharge Fee', '#0.00', NULL, '4', NULL, '1', 'viewDriverSettlement', '1'
	UNION SELECT '323', '1', 'DriverFuelSurchargeRate', 'SETTLEMENT | DRIVER | Driver Fuel Surcharge Rate', '#0.0000', NULL, '4', NULL, '1', 'viewDriverSettlement', '1'
	UNION SELECT '324', '1', 'DriverSplitLoadAmount', 'SETTLEMENT | DRIVER | Driver Split Load Fee', '#0.0000', NULL, '4', NULL, '1', 'viewDriverSettlement', '1'
	UNION SELECT '325', '1', 'DriverOrderRejectRate', 'SETTLEMENT | DRIVER | Driver Reject Rate', '#0.00', NULL, '4', NULL, '1', 'viewDriverSettlement', '1'
	UNION SELECT '326', '1', 'DriverOrderRejectRateType', 'SETTLEMENT | DRIVER | Driver Reject Rate Type', NULL, NULL, '1', NULL, '1', 'viewDriverSettlement', '1'
	UNION SELECT '327', '1', 'DriverOrderRejectAmount', 'SETTLEMENT | DRIVER | Driver Reject $$', '#0.00', NULL, '4', NULL, '1', 'viewDriverSettlement', '1'
	UNION SELECT '328', '1', 'DriverOriginWaitBillableMinutes', 'SETTLEMENT | DRIVER | Driver Origin Wait Billable Minutes', NULL, NULL, '2', NULL, '1', 'viewDriverSettlement', '1'
	UNION SELECT '329', '1', 'DriverDestinationWaitBillableMinutes', 'SETTLEMENT | DRIVER | Driver Dest Wait Billable Minutes', NULL, NULL, '2', NULL, '1', 'viewDriverSettlement', '1'
	UNION SELECT '330', '1', 'DriverChainupRateType', 'SETTLEMENT | DRIVER | Driver Chainup Rate Type', NULL, NULL, '1', NULL, '1', 'viewDriverSettlement', '1'
	UNION SELECT '331', '1', 'DriverRerouteRateType', 'SETTLEMENT | DRIVER | Driver Reroute Rate Type', NULL, NULL, '1', NULL, '1', 'viewDriverSettlement', '1'
	UNION SELECT '332', '1', 'DriverSplitLoadRateType', 'SETTLEMENT | DRIVER | Driver Split Load Rate Type', NULL, NULL, '1', NULL, '1', 'viewDriverSettlement', '1'
	UNION SELECT '333', '1', 'DriverH2SRateType', 'SETTLEMENT | DRIVER | Driver H2S Rate Type', NULL, NULL, '1', NULL, '1', 'viewDriverSettlement', '1'
	UNION SELECT '334', '1', 'DriverChainupRate', 'SETTLEMENT | DRIVER | Driver Chainup Rate', '#0.00', NULL, '4', NULL, '1', 'viewDriverSettlement', '1'
	UNION SELECT '335', '1', 'DriverRouteRate', 'SETTLEMENT | DRIVER | Driver Rate Sheet Rate', '#0.0000', NULL, '4', NULL, '1', 'viewDriverSettlement', '1'
	UNION SELECT '336', '1', 'DriverSplitLoadRate', 'SETTLEMENT | DRIVER | Driver Split Load Rate', '#0.0000', NULL, '4', NULL, '1', 'viewDriverSettlement', '1'
	UNION SELECT '337', '1', 'DriverOriginWaitBillableHours', 'SETTLEMENT | DRIVER | Driver Origin Wait Billable Hours', NULL, NULL, '4', NULL, '1', 'viewDriverSettlement', '1'
	UNION SELECT '338', '1', 'DriverDestinationWaitBillableHours', 'SETTLEMENT | DRIVER | Driver Destination Wait Billable Hours', NULL, NULL, '4', NULL, '0', 'viewDriverSettlement', '0'
	UNION SELECT '339', '1', 'DriverTotalWaitBillableMinutes', 'SETTLEMENT | DRIVER | Driver Total Wait Billable Minutes', NULL, NULL, '4', NULL, '0', 'viewDriverSettlement', '0'
	UNION SELECT '340', '1', 'DriverLoadRate', 'SETTLEMENT | DRIVER | Driver Load Rate', NULL, NULL, '1', NULL, '1', 'viewDriverSettlement', '1'
	UNION SELECT '341', '1', 'OriginDriverGroup', 'SETTLEMENT | DRIVER | Driver Origin Driver Group', NULL, 'OriginDriverGroupID', '2', 'SELECT ID, Name FROM tblDriverGroup ORDER BY Name', '0', '*', '0'

SET IDENTITY_INSERT tblReportColumnDefinition OFF

update tblReportColumnDefinition set AllowedRoles = 'viewShipperSettlement' where caption like 'SETTLEMENT | SHIPPER%'

update tblReportColumnDefinition set AllowedRoles = 'viewCarrierSettlement' where caption like 'SETTLEMENT | CARRIER%'

COMMIT
SET NOEXEC OFF