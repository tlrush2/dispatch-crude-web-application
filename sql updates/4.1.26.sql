SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.1.25.3'
SELECT  @NewVersion = '4.1.26'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1866 - Convert Origin/Destination wait reasons & Order/Ticket Reject reasons pages to MVC.'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO



/* Add new permissions for the Producer maintenance page */
EXEC spAddNewPermission 'createReasonCodes', 'Allow user to create Reason Codes', 'Reason Codes', 'Create'
GO
EXEC spAddNewPermission 'editReasonCodes', 'Allow user to edit Reason Codes', 'Reason Codes', 'Edit'
GO
EXEC spAddNewPermission 'deactivateReasonCodes', 'Allow user to deactivate Reason Codes', 'Reason Codes', 'Deactivate'
GO



COMMIT
SET NOEXEC OFF