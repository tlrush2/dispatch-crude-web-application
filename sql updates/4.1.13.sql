SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.1.12'
SELECT  @NewVersion = '4.1.13'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1774 - Made driver and gauger groups permanent (undeleteable)'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/* update group names and descriptions */
UPDATE aspnet_Groups
SET GroupName = '_Driver'
, LoweredGroupName = '_driver'
, Description = 'Permanent group for drivers.  All drivers should be assigned to this group.'
WHERE GroupName = 'Driver'
GO

UPDATE aspnet_Groups
SET GroupName = '_Gauger'
, LoweredGroupName = '_gauger'
, Description = 'Permanent group for gaugers.  All gaugers should be assigned to this group.'
WHERE GroupName = 'Gauger'
GO


/* update corresponding permission descriptions */
UPDATE aspnet_Roles
SET Description = 'Legacy Driver permission.  The _Driver group REQUIRES this permission.'
WHERE RoleName = 'Driver'
GO

UPDATE aspnet_Roles
SET Description = 'Legacy Gauger permission.  The _Gauger group REQUIRES this permission.'
WHERE RoleName = 'Gauger'
GO


COMMIT
SET NOEXEC OFF