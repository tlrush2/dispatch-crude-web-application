-- backup database [dispatchcrude.dev] to disk = 'C:\files\Consulting\cts\dc_backups\dispatchcrude.dev.3.8.2.bak'
-- restore database [DispatchCrude.Dev] from disk = 'C:\files\Consulting\cts\dc_backups\dispatchcrude.dev.3.8.2.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.8.2'
SELECT  @NewVersion = '3.8.3'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Dashboard Module: add table and logic to grant access to N dashboards per user ROLE'
	UNION SELECT @NewVersion, 1, 'Dashboard Module: make HOME PAGE be dashboard tab list (if user has access to any dashboards)'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

CREATE TABLE tblDashboard
(
  ID int identity(1, 1) NOT NULL CONSTRAINT PK_Dashboard PRIMARY KEY
, Name varchar(50) NOT NULL
, DisplayName varchar(50) NOT NULL
, Url varchar(255) NOT NULL
, AllowedRoles_CSV varchar(max) NOT NULL
)
GO
GRANT SELECT ON tblDashboard TO role_iis_acct
GO
CREATE UNIQUE INDEX udxDashboard_Name ON tblDashboard(Name)
GO
CREATE UNIQUE INDEX udxDashboard_DisplayName ON tblDashboard(DisplayName)
GO

INSERT INTO tblDashboard (Name, DisplayName, Url, AllowedRoles_CSV)
	SELECT 'DestinationAllocation', 'Allocations', '~/Dashboards/DestinationAllocation.xml', 'Administrator,Management,Operations,Customer'
GO

/******************************************
** Date Created:	2015/07/07
** Author:			Kevin Alons
** Purpose:			return the tblDashboard table records available to the role(s) of the specified user
** Changes:
******************************************/
CREATE PROCEDURE spUserDashboards(@userName varchar(100)) AS
BEGIN
	SELECT DISTINCT DUR.ID, DUR.DisplayName, DUR.Url
	FROM (
		SELECT UR.UserName, Role = R.Value
		FROM viewUserRoles UR
		CROSS APPLY dbo.fnSplitCSV(UR.Roles) R
	) UR
	JOIN (
		SELECT D.ID, D.DisplayName, D.Url, Role = AR.Value
		FROM tblDashboard D
		CROSS APPLY dbo.fnSplitCSV(D.AllowedRoles_CSV) AR
	) DUR ON DUR.Role = UR.Role
	WHERE UR.UserName LIKE @userName
END
GO
GRANT EXECUTE ON spUserDashboards TO role_iis_acct
GO

COMMIT
SET NOEXEC OFF