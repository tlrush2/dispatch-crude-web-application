-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.12.7.5'
SELECT  @NewVersion = '3.12.7.6'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1324 & 1366 - Commodity Pricing Pages updates'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO



/* Rename table to eliminate discrepancy between DB and site pages */
EXEC sp_rename 'tblTradeHolidays', 'tblNonTradeDays'
GO


/*********************************************
-- Date Created: 21 Apr 2016
-- Author: Joe Engler
-- Purpose: Calculate the commodity price for a given date range
-- Changes: 
			x.x.x - 2016/05/11 - BB - Changed table name when table was renamed to tblNonTradeDays
*********************************************/
ALTER FUNCTION [dbo].[fnCalculateCommodityPriceFromMethod](@StartDate DATE, @EndDate DATE, @CommodityIndexID INT, @TradeDaysOnly BIT) RETURNS MONEY AS
BEGIN
	DECLARE @ret MONEY

	SELECT @ret = ISNULL(AVG(IndexPrice), 0)
	FROM tblCommodityPrice CP
	LEFT JOIN tblNonTradeDays H ON h.HolidayDate = CP.PriceDate
	WHERE CommodityIndexID = @CommodityIndexID
	  AND PriceDate BETWEEN @StartDate AND @EndDate
	  AND (   (@TradeDaysOnly = 0) -- Include non trade days (per method)
		   OR (DATEPART(WEEKDAY, CP.PriceDate) not in (1,7) -- skip weekends
		      AND H.HolidayDate IS NULL)) -- skip holidays

	RETURN (@ret)
END
GO


/* Add Create/Update/Delete columns to tblNonTradeDays */
ALTER TABLE tblNonTradeDays
	ADD CreateDateUTC DATETIME NOT NULL CONSTRAINT DF_NonTradeDays_CreateDateUTC DEFAULT(getutcdate())
	, CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_NonTradeDays_CreatedByUser DEFAULT('System')
	, LastChangeDateUTC DATETIME NULL
	, LastChangedByUser VARCHAR(100) NULL
	, DeleteDateUTC DATETIME NULL
	, DeletedByUser VARCHAR(100) NULL
GO


/* Adding more explicit permissions to roles table for the commodity pricing pages (this is necessary for permission based grid button control) */
INSERT INTO aspnet_Roles (ApplicationId, RoleId, RoleName, LoweredRoleName, Description)
SELECT '446467D6-39CD-45E3-B3E0-CAC7945AF3E8', NEWID(), 'createCommodityPricing', 'createCommodityPricing', 'Allow user to see the shippers maintenance page'
UNION
SELECT '446467D6-39CD-45E3-B3E0-CAC7945AF3E8', NEWID(), 'editCommodityPricing', 'editCommodityPricing', 'Allow user to see the shippers maintenance page'
UNION
SELECT '446467D6-39CD-45E3-B3E0-CAC7945AF3E8', NEWID(), 'deactivateCommodityPricing', 'deactivateCommodityPricing', 'Allow user to see the shippers maintenance page'
EXCEPT SELECT ApplicationId, RoleId, RoleName, LoweredRoleName, Description
FROM aspnet_Roles
GO


/* Add new permission to current Administrator users */
EXEC spAddUserPermissionsByRole 'Administrator', 'createCommodityPricing'
GO
EXEC spAddUserPermissionsByRole 'Administrator', 'editCommodityPricing'
GO
EXEC spAddUserPermissionsByRole 'Administrator', 'deactivateCommodityPricing'
GO


/* Add new roles to any non-admin users who may have been given the manageCommodityPricing previously */
EXEC spAddUserPermissionsByRole 'manageCommodityPricing', 'createCommodityPricing'
GO
EXEC spAddUserPermissionsByRole 'manageCommodityPricing', 'editCommodityPricing'
GO
EXEC spAddUserPermissionsByRole 'manageCommodityPricing', 'deactivateCommodityPricing'
GO


/* Change current commodity role to fit with more specific permissions added above */
UPDATE aspnet_Roles
SET RoleName = 'viewCommodityPricing', LoweredRoleName = 'viewcommoditypricing', Description = 'Allow user to view the commodity pricing pages'
WHERE RoleName = 'manageCommodityPricing'
GO



COMMIT
SET NOEXEC OFF