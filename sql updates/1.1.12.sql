BEGIN TRAN

UPDATE tblSetting SET Value = '1.1.12' WHERE ID=0

EXEC sp_rename 
	@objname = 'tblCarrier.CarrierNum',
    @newname = 'IDNumber',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblTruck.TruckNumber',
    @newname = 'IDNumber',
    @objtype = 'COLUMN'
GO
EXEC sp_rename 
	@objname = 'tblTrailer.TrailerNumber',
    @newname = 'IDNumber',
    @objtype = 'COLUMN'
GO

-- remove extraneous fields + make IDNumber NOT NULL
ALTER TABLE dbo.tblCarrier
	DROP CONSTRAINT FK_tblCarrier_tblStates
GO
ALTER TABLE dbo.tblState SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblCarrier
	DROP CONSTRAINT DF_tblCarrier_CarrierTypeID
GO
ALTER TABLE dbo.tblCarrier
	DROP CONSTRAINT DF_tblCarrier_CreateDate
GO
CREATE TABLE dbo.Tmp_tblCarrier
	(
	ID int NOT NULL IDENTITY (1000, 1),
	Name varchar(40) NOT NULL,
	Address varchar(40) NULL,
	City varchar(30) NULL,
	StateID int NULL,
	Zip varchar(10) NULL,
	ContactName varchar(40) NULL,
	ContactEmail varchar(50) NULL,
	ContactPhone varchar(15) NULL,
	Notes varchar(MAX) NULL,
	IDNumber varchar(20) NOT NULL,
	CarrierTypeID int NOT NULL,
	CreateDate smalldatetime NULL,
	CreatedByUser varchar(100) NULL,
	LastChangeDate smalldatetime NULL,
	LastChangedByUser varchar(100) NULL,
	DeleteDate smalldatetime NULL,
	DeletedByUser varchar(100) NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_tblCarrier SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_tblCarrier ADD CONSTRAINT
	DF_tblCarrier_CarrierTypeID DEFAULT ((1)) FOR CarrierTypeID
GO
ALTER TABLE dbo.Tmp_tblCarrier ADD CONSTRAINT
	DF_tblCarrier_CreateDate DEFAULT (getdate()) FOR CreateDate
GO
SET IDENTITY_INSERT dbo.Tmp_tblCarrier ON
GO
IF EXISTS(SELECT * FROM dbo.tblCarrier)
	 EXEC('INSERT INTO dbo.Tmp_tblCarrier (ID, Name, Address, City, StateID, Zip, ContactName, ContactEmail, ContactPhone, Notes, IDNumber, CarrierTypeID, CreateDate, CreatedByUser, LastChangeDate, LastChangedByUser)
		SELECT ID, Name, Address, City, StateID, Zip, ContactName, ContactEmail, ContactPhone, Notes, IDNumber, CarrierTypeID, CreateDate, CreatedByUser, LastChangeDate, LastChangedByUser FROM dbo.tblCarrier WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_tblCarrier OFF
GO
ALTER TABLE dbo.tblTruck
	DROP CONSTRAINT FK_tblTruck_tblCarrier
GO
ALTER TABLE dbo.tblDriver
	DROP CONSTRAINT FK_tblDriver_tblCarrier
GO
ALTER TABLE dbo.tblTrailer
	DROP CONSTRAINT FK_tblTrailer_tblCarrier
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_Carrier
GO
DROP TABLE dbo.tblCarrier
GO
EXECUTE sp_rename N'dbo.Tmp_tblCarrier', N'tblCarrier', 'OBJECT' 
GO
ALTER TABLE dbo.tblCarrier ADD CONSTRAINT
	Carrier_PrimaryKey PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.tblCarrier ADD CONSTRAINT
	uqCarrier_Name UNIQUE NONCLUSTERED 
	(
	Name
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE UNIQUE NONCLUSTERED INDEX uidxCarrier_IDNumber ON dbo.tblCarrier
	(
	IDNumber
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.tblCarrier ADD CONSTRAINT
	FK_tblCarrier_tblStates FOREIGN KEY
	(
	StateID
	) REFERENCES dbo.tblState
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
-- =============================================
-- Author:		Kevin Alons
-- Create date: 8 Mar 2013
-- Description:	trigger to do FK relationship with tblCarrierRates (since default records would not be allowed with traditional FK)
-- =============================================
CREATE TRIGGER trigCarrier_D_Rates ON dbo.tblCarrier AFTER DELETE AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DELETE FROM tblCarrierRates WHERE CarrierID IN (SELECT ID FROM deleted)
END
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_Carrier FOREIGN KEY
	(
	CarrierID
	) REFERENCES dbo.tblCarrier
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrder SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblTrailer ADD CONSTRAINT
	FK_tblTrailer_tblCarrier FOREIGN KEY
	(
	CarrierID
	) REFERENCES dbo.tblCarrier
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblTrailer SET (LOCK_ESCALATION = TABLE)
GO

-- make the Driver.IDNumber be required and add DeleteDate/DeletedByUser fields

ALTER TABLE dbo.tblDriver ADD CONSTRAINT
	FK_tblDriver_tblCarrier FOREIGN KEY
	(
	CarrierID
	) REFERENCES dbo.tblCarrier
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblDriver SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblTruck ADD CONSTRAINT
	FK_tblTruck_tblCarrier FOREIGN KEY
	(
	CarrierID
	) REFERENCES dbo.tblCarrier
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblTruck SET (LOCK_ESCALATION = TABLE)
GO

UPDATE tblDriver SET IdNumber = ID where IdNumber is null or IdNumber <= ''

ALTER TABLE dbo.tblDriver
	DROP CONSTRAINT FK_tblDriver_tblState
GO
ALTER TABLE dbo.tblState SET (LOCK_ESCALATION = TABLE)
GO

ALTER TABLE dbo.tblDriver
	DROP CONSTRAINT FK_tblDriver_tblCarrier
GO
ALTER TABLE dbo.tblCarrier SET (LOCK_ESCALATION = TABLE)
GO

ALTER TABLE dbo.tblDriver
	DROP CONSTRAINT FK_Driver_Truck
GO
ALTER TABLE dbo.tblTruck SET (LOCK_ESCALATION = TABLE)
GO

ALTER TABLE dbo.tblDriver
	DROP CONSTRAINT FK_Driver_Trailer
GO
ALTER TABLE dbo.tblDriver
	DROP CONSTRAINT FK_Driver_Trailer2
GO
ALTER TABLE dbo.tblTrailer SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblDriver
	DROP CONSTRAINT DF_tblDriver_DLonFile
GO
ALTER TABLE dbo.tblDriver
	DROP CONSTRAINT DF_tblDriver_CreateDate
GO
CREATE TABLE dbo.Tmp_tblDriver
	(
	ID int NOT NULL IDENTITY (1000, 1),
	CarrierID int NOT NULL,
	FirstName varchar(20) NOT NULL,
	LastName varchar(20) NOT NULL,
	IDNumber varchar(20) NOT NULL,
	PIN varchar(10) NULL,
	DLonFile bit NOT NULL,
	DLNumber varchar(20) NULL,
	DLExpiration date NULL,
	DOB date NULL,
	Address varchar(40) NULL,
	City varchar(30) NULL,
	StateID int NULL,
	Zip varchar(10) NULL,
	MobilePhone varchar(20) NULL,
	MobileProvider varchar(30) NULL,
	Email varchar(75) NULL,
	TruckID int NULL,
	TrailerID int NULL,
	Trailer2ID int NULL,
	CreateDate smalldatetime NULL,
	CreatedByUser varchar(100) NULL,
	LastChangeDate smalldatetime NULL,
	LastChangedByUser varchar(100) NULL,
	DeleteDate smalldatetime NULL,
	DeletedByUser varchar(100) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_tblDriver SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_tblDriver ADD CONSTRAINT
	DF_tblDriver_DLonFile DEFAULT ((0)) FOR DLonFile
GO
ALTER TABLE dbo.Tmp_tblDriver ADD CONSTRAINT
	DF_tblDriver_CreateDate DEFAULT (getdate()) FOR CreateDate
GO
SET IDENTITY_INSERT dbo.Tmp_tblDriver ON
GO
IF EXISTS(SELECT * FROM dbo.tblDriver)
	 EXEC('INSERT INTO dbo.Tmp_tblDriver (ID, CarrierID, FirstName, LastName, IDNumber, PIN, DLonFile, DLNumber, DLExpiration, DOB, Address, City, StateID, Zip, MobilePhone, MobileProvider, Email, TruckID, TrailerID, Trailer2ID, CreateDate, CreatedByUser, LastChangeDate, LastChangedByUser)
		SELECT ID, CarrierID, FirstName, LastName, IdNumber, PIN, DLonFile, DLNumber, DLExpiration, DOB, Address, City, StateID, Zip, MobilePhone, MobileProvider, Email, TruckID, TrailerID, Trailer2ID, CreateDate, CreatedByUser, LastChangeDate, LastChangedByUser FROM dbo.tblDriver WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_tblDriver OFF
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_Driver
GO
DROP TABLE dbo.tblDriver
GO
EXECUTE sp_rename N'dbo.Tmp_tblDriver', N'tblDriver', 'OBJECT' 
GO
ALTER TABLE dbo.tblDriver ADD CONSTRAINT
	tblDriver_PrimaryKey PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE UNIQUE NONCLUSTERED INDEX udxDriver_Carrier_First_Last ON dbo.tblDriver
	(
	CarrierID,
	FirstName,
	LastName
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.tblDriver ADD CONSTRAINT
	FK_Driver_Trailer FOREIGN KEY
	(
	TrailerID
	) REFERENCES dbo.tblTrailer
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  SET NULL 
	
GO
ALTER TABLE dbo.tblDriver ADD CONSTRAINT
	FK_Driver_Trailer2 FOREIGN KEY
	(
	Trailer2ID
	) REFERENCES dbo.tblTrailer
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblDriver ADD CONSTRAINT
	FK_Driver_Truck FOREIGN KEY
	(
	TruckID
	) REFERENCES dbo.tblTruck
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  SET NULL 
	
GO
ALTER TABLE dbo.tblDriver ADD CONSTRAINT
	FK_tblDriver_tblCarrier FOREIGN KEY
	(
	CarrierID
	) REFERENCES dbo.tblCarrier
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblDriver ADD CONSTRAINT
	FK_tblDriver_tblState FOREIGN KEY
	(
	StateID
	) REFERENCES dbo.tblState
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO

ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_Driver FOREIGN KEY
	(
	DriverID
	) REFERENCES dbo.tblDriver
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrder SET (LOCK_ESCALATION = TABLE)
GO

/***********************************/
-- Date Created: 26 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Trailer records with FullName & translated "friendly" values
/***********************************/
ALTER VIEW [dbo].[viewTrailer] AS
SELECT T.*
, T.IDNumber AS FullName
, C.Name AS Carrier
FROM dbo.tblTrailer T
JOIN dbo.tblCarrier C ON C.ID = T.CarrierID

GO

/***********************************/
-- Date Created: 26 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Truck records with FullName & translated "friendly" values
/***********************************/
ALTER VIEW [dbo].[viewTruck] AS
SELECT T.*
, T.IDNumber AS FullName
, C.Name AS Carrier
FROM dbo.tblTruck T
JOIN dbo.tblCarrier C ON C.ID = T.CarrierID

GO

DROP INDEX udxTrailer_Carrier_TrailerNumber ON tblTrailer
GO
CREATE UNIQUE INDEX udxTrailer_Carrier_IDNumber ON tblTrailer(CarrierID, IDNumber)
GO
DROP INDEX udxTruck_Carrier_TruckNumber ON tblTruck
GO
CREATE UNIQUE INDEX udxTruck_Carrier_IDNumber ON tblTruck(CarrierID, IDNumber)
GO

EXEC _spRefreshAllViews

/****** Object:  StoredProcedure [dbo].[spOrderTicketFullExport]    Script Date: 05/09/2013 15:41:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/***********************************/
-- Date Created: 28 Feb 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc for 3rd parties (Customer/Carrier)
/***********************************/
ALTER PROCEDURE [dbo].[spOrderTicketFullExport]
(
  @StartDate datetime
, @EndDate datetime
, @CarrierID int = 0 -- -1 = all carriers
, @CustomerID int = 0 -- -1 = all customers
) AS BEGIN
	SELECT OT.ID
		, O.OrderNum
		, O.OrderStatus
		, OT.TicketType 
		, OO.WellAPI
		, O.OriginDepartTime
		, OT.CarrierTicketNum AS TicketNum
		, OO.Name AS Origin
		, OO.LeaseName
		, O.Operator
		, O.Destination
		, OT.NetBarrels
		, TT.HeightFeet AS TankHeight
		, TT.CapacityBarrels AS TankBarrels
		, OT.TankNum
		, OT.ProductObsGravity
		, OT.ProductObsTemp
		, OT.ProductBSW
		, isnull(OT.ProductHighTemp, OT.ProductObsTemp) AS ProductHighTemp
		, OT.OpeningGaugeFeet
		, OT.OpeningGaugeInch
		, OT.OpeningGaugeQ
		, OT.ClosingGaugeFeet
		, OT.ClosingGaugeInch
		, OT.ClosingGaugeQ
		, isnull(OT.ProductLowTemp, OT.ProductObsTemp) AS ProductLowTemp
		, dbo.fnGaugeQtrInches(OT.OpeningGaugeFeet, OT.OpeningGaugeInch, OT.OpeningGaugeQ) AS OpenTotalQ
		, dbo.fnGaugeQtrInches(OT.ClosingGaugeFeet, OT.ClosingGaugeInch, ot.ClosingGaugeQ) AS CloseTotalQ
	FROM dbo.viewOrderTicket OT
	JOIN dbo.viewOrder O ON O.ID = OT.OrderID
	JOIN dbo.tblOrigin OO ON OO.ID = O.OriginID
	LEFT JOIN dbo.tblTankType TT ON TT.ID = OT.TankTypeID
	WHERE O.StatusID IN (3, 4) -- Delivered & Audited
	  AND (@CarrierID=-1 OR @CustomerID=-1 OR O.CarrierID=@CarrierID OR O.CustomerID=@CustomerID) 
	  AND O.OriginDepartTime >= @StartDate AND O.OriginDepartTime < dateadd(day, 1, @EndDate) 
	  AND O.DeleteDate IS NULL AND OT.DeleteDate IS NULL
	ORDER BY O.OriginDepartTime
	
END

GO
--rollback
COMMIT