SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.4.16'
SELECT  @NewVersion = '4.4.17'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'JT-210 - Add Batch Settled Date and Invoice Number to report center For Driver, Carrier, Shipper, and Producer'	
	UNION
	SELECT @NewVersion, 0, 'JT-532 - Add ECOT to Report Center, Truck Order Grid, and Dispatch Grid'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


-- Add ECOT Minutes to Report Center data field options
-- Also add Settlement Batch Date and Invoice Num for Driver, Carrier, Shipper, and Producer Settlement
SET IDENTITY_INSERT tblReportColumnDefinition ON
INSERT INTO tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport, IsTicketField)
  SELECT 353, 1, 'ECOT_Minutes', 'TRIP | ECOT Minutes', NULL, NULL, 4, NULL, 1, '*', 1, 0
  UNION
  SELECT 354, 1, 'ECOT_HoursMinutes', 'TRIP | ECOT (H:MM)', NULL, NULL, 4, NULL, 1, '*', 1, 0
  UNION
  SELECT 355, 1, 'ShipperBatchDate', 'SETTLEMENT | SHIPPER | Shipper Settled Date', 'm/d/yyyy', NULL, 6, NULL, 1, '*', 0, 0
  UNION
  SELECT 356, 1, 'CarrierBatchDate', 'SETTLEMENT | CARRIER | Carrier Settled Date', 'm/d/yyyy', NULL, 6, NULL, 1, '*', 0, 0
  UNION
  SELECT 357, 1, 'DriverBatchDate', 'SETTLEMENT | DRIVER | Driver Settled Date', 'm/d/yyyy', NULL, 6, NULL, 1, '*', 0, 0
  UNION
  SELECT 358, 1, 'ProducerBatchDate', 'SETTLEMENT | PRODUCER | Producer Settled Date', 'm/d/yyyy', NULL, 6, NULL, 1, '*', 0, 0
  UNION
  SELECT 359, 1, 'ShipperBatchInvoiceNum', 'SETTLEMENT | SHIPPER | Shipper Batch Invoice #', NULL, NULL, 4, NULL, 1, '*', 0, 0
  UNION
  SELECT 360, 1, 'CarrierBatchInvoiceNum', 'SETTLEMENT | CARRIER | Carrier Batch Invoice #', NULL, NULL, 4, NULL, 1, '*', 0, 0
  UNION
  SELECT 361, 1, 'DriverBatchInvoiceNum', 'SETTLEMENT | DRIVER | Driver Batch Invoice #', NULL, NULL, 4, NULL, 1, '*', 0, 0
  UNION
  SELECT 362, 1, 'ProducerBatchInvoiceNum', 'SETTLEMENT | PRODUCER | Producer Batch Invoice #', NULL, NULL, 4, NULL, 1, '*', 0, 0
SET IDENTITY_INSERT tblReportColumnDefinition OFF
GO



/*****************************************/
-- Created: 2015.01.19 - Kevin Alons
-- Purpose: return the tblOrder table with a Local OrderDate field added
-- Changes:
-- ??		- 2015/05/17 - KDA	- add local DeliverDate field
-- 3.8.11	- 2015/07/28 - KDA	- remove OriginShipperRegion field
-- 3.9.2	- 2015/08/25 - KDA	- remove computed OrderDate (it is now stored in the table directly)
-- 3.10.13	- 2015/02/29 - JAE	- Add TruckType
-- 4.1.8.6	- 2016.09.24 - KDA	- move H2S | TicketCount | RerouteCount fields from viewOrder down to viewOrderBase
-- 4.4.17	- 2017/01/09 - BSB	- Add ECOT Minutes and ECOT (H:MM) fields
/*****************************************/
ALTER VIEW viewOrderBase AS
	SELECT O.*
		, OriginStateID = OO.StateID
		, OriginRegionID = OO.RegionID
		, DestStateID = D.StateID
		, DestRegionID = D.RegionID
		, P.ProductGroupID
		, T.TruckTypeID
		, DR.DriverGroupID
		, DeliverDate = cast(dbo.fnUTC_to_Local(O.DestDepartTimeUTC, D.TimeZoneID, D.UseDST) as date) 
		, OO.H2S
		, OTC.TicketCount
		, ORC.RerouteCount
		, ECOT_Minutes = R.ECOT		-- 4.4.17
		, ECOT_HoursMinutes = (SELECT CAST(R.ECOT / 60 AS VARCHAR) + ':' + CAST(R.ECOT % 60 AS VARCHAR))	-- 4.4.17
	FROM tblOrder O
	JOIN tblOrigin OO ON OO.ID = O.OriginID
	JOIN tblDestination D ON D.ID = O.DestinationID
	JOIN tblRoute R ON R.ID = O.RouteID		-- 4.4.17
	LEFT JOIN tblProduct P ON P.ID = O.ProductID
	LEFT JOIN tblTruck T ON T.ID = O.TruckID
	LEFT JOIN tblDriver DR ON DR.ID = O.DriverID
	LEFT JOIN (SELECT OrderID, TicketCount = COUNT(1) FROM tblOrderTicket OT WHERE OT.DeleteDateUTC IS NULL GROUP BY OrderID) OTC ON OTC.OrderID = O.ID
	LEFT JOIN (SELECT OrderID, RerouteCount = COUNT(1) FROM tblOrderReroute GROUP BY OrderID) ORC ON ORC.OrderID = O.ID
GO



/***********************************/
-- Created: ?.?.? - 2016/07/08 - Kevin Alons
-- Purpose: return Report Center Order data without GPS data
-- Changes:
	-- 4.4.1	2016/11/04 - JAE - Add Destination Chainup
	-- 4.4.17	2017/01/10 - BSB - Add Settlement Batch Date and Invoice Num for Driver, Carrier, and Producer Settlement + Batch Date for Shipper
/***********************************/
ALTER VIEW viewReportCenter_Orders_NoGPS AS
	SELECT X.*
	FROM (
		SELECT O.*
			--
			, ShipperBatchNum = SS.BatchNum
			, ShipperBatchDate = SSB.BatchDate		-- 4.4.17
			, ShipperBatchInvoiceNum = SSB.InvoiceNum
			, ShipperSettlementUomID = SS.SettlementUomID
			, ShipperSettlementUom = SS.SettlementUom
			, ShipperMinSettlementUnits = SS.MinSettlementUnits
			, ShipperSettlementUnits = SS.SettlementUnits
			, ShipperRateSheetRate = SS.RateSheetRate
			, ShipperRateSheetRateType = SS.RateSheetRateType
			, ShipperRouteRate = SS.RouteRate
			, ShipperRouteRateType = SS.RouteRateType
			, ShipperLoadRate = ISNULL(SS.RouteRate, SS.RateSheetRate)
			, ShipperLoadRateType = ISNULL(SS.RouteRateType, SS.RateSheetRateType)
			, ShipperLoadAmount = SS.LoadAmount
			, ShipperOrderRejectRate = SS.OrderRejectRate 
			, ShipperOrderRejectRateType = SS.OrderRejectRateType 
			, ShipperOrderRejectAmount = SS.OrderRejectAmount 
			, ShipperWaitFeeSubUnit = SS.WaitFeeSubUnit  
			, ShipperWaitFeeRoundingType = SS.WaitFeeRoundingType  
			, ShipperOriginWaitBillableHours = SS.OriginWaitBillableHours  
			, ShipperOriginWaitBillableMinutes = SS.OriginWaitBillableMinutes  
			, ShipperOriginWaitRate = SS.OriginWaitRate
			, ShipperOriginWaitAmount = SS.OriginWaitAmount
			, ShipperDestinationWaitBillableHours = SS.DestinationWaitBillableHours 
			, ShipperDestinationWaitBillableMinutes = SS.DestinationWaitBillableMinutes   
			, ShipperDestinationWaitRate = SS.DestinationWaitRate  
			, ShipperDestinationWaitAmount = SS.DestinationWaitAmount  
			, ShipperTotalWaitAmount = SS.TotalWaitAmount
			, ShipperTotalWaitBillableMinutes = NULLIF(ISNULL(SS.OriginWaitBillableMinutes, 0) + ISNULL(SS.DestinationWaitBillableMinutes, 0), 0)
			, ShipperTotalWaitBillableHours = NULLIF(ISNULL(SS.OriginWaitBillableHours, 0) + ISNULL(SS.DestinationWaitBillableHours, 0), 0)		
			, ShipperFuelSurchargeRate = SS.FuelSurchargeRate
			, ShipperFuelSurchargeAmount = SS.FuelSurchargeAmount
			, ShipperOriginChainupRate = SS.OriginChainupRate
			, ShipperOriginChainupRateType = SS.OriginChainupRateType  
			, ShipperOriginChainupAmount = SS.OriginChainupAmount
			, ShipperDestChainupRate = SS.DestChainupRate
			, ShipperDestChainupRateType = SS.DestChainupRateType  
			, ShipperDestChainupAmount = SS.DestChainupAmount
			, ShipperRerouteRate = SS.RerouteRate
			, ShipperRerouteRateType = SS.RerouteRateType  
			, ShipperRerouteAmount = SS.RerouteAmount
			, ShipperSplitLoadRate = SS.SplitLoadRate
			, ShipperSplitLoadRateType = SS.SplitLoadRateType  
			, ShipperSplitLoadAmount = SS.SplitLoadAmount
			, ShipperH2SRate = SS.H2SRate
			, ShipperH2SRateType = SS.H2SRateType  
			, ShipperH2SAmount = SS.H2SAmount
			, ShipperTaxRate = SS.OriginTaxRate
			, ShipperTotalAmount = SS.TotalAmount
			, ShipperDestCode = CDC.Code
			, ShipperTicketType = STT.TicketType
			--
			, CarrierBatchNum = SC.BatchNum			
			, CarrierBatchDate = CSB.BatchDate		-- 4.4.17
			, CarrierBatchInvoiceNum = CSB.InvoiceNum		-- 4.4.17
			, CarrierSettlementUomID = SC.SettlementUomID
			, CarrierSettlementUom = SC.SettlementUom
			, CarrierMinSettlementUnits = SC.MinSettlementUnits
			, CarrierSettlementUnits = SC.SettlementUnits
			, CarrierRateSheetRate = SC.RateSheetRate
			, CarrierRateSheetRateType = SC.RateSheetRateType
			, CarrierRouteRate = SC.RouteRate
			, CarrierRouteRateType = SC.RouteRateType
			, CarrierLoadRate = ISNULL(SC.RouteRate, SC.RateSheetRate)
			, CarrierLoadRateType = ISNULL(SC.RouteRateType, SC.RateSheetRateType)
			, CarrierLoadAmount = SC.LoadAmount
			, CarrierOrderRejectRate = SC.OrderRejectRate 
			, CarrierOrderRejectRateType = SC.OrderRejectRateType 
			, CarrierOrderRejectAmount = SC.OrderRejectAmount 
			, CarrierWaitFeeSubUnit = SC.WaitFeeSubUnit  
			, CarrierWaitFeeRoundingType = SC.WaitFeeRoundingType  
			, CarrierOriginWaitBillableHours = SC.OriginWaitBillableHours  
			, CarrierOriginWaitBillableMinutes = SC.OriginWaitBillableMinutes  
			, CarrierOriginWaitRate = SC.OriginWaitRate
			, CarrierOriginWaitAmount = SC.OriginWaitAmount
			, CarrierDestinationWaitBillableHours = SC.DestinationWaitBillableHours 
			, CarrierDestinationWaitBillableMinutes = SC.DestinationWaitBillableMinutes  
			, CarrierDestinationWaitRate = SC.DestinationWaitRate 
			, CarrierDestinationWaitAmount = SC.DestinationWaitAmount  
			, CarrierTotalWaitAmount = SC.TotalWaitAmount
			, CarrierTotalWaitBillableMinutes = NULLIF(ISNULL(SC.OriginWaitBillableMinutes, 0) + ISNULL(SC.DestinationWaitBillableMinutes, 0), 0)
			, CarrierTotalWaitBillableHours = NULLIF(ISNULL(SC.OriginWaitBillableHours, 0) + ISNULL(SC.DestinationWaitBillableHours, 0), 0)		
			, CarrierFuelSurchargeRate = SC.FuelSurchargeRate
			, CarrierFuelSurchargeAmount = SC.FuelSurchargeAmount
			, CarrierOriginChainupRate = SC.OriginChainupRate
			, CarrierOriginChainupRateType = SC.OriginChainupRateType  
			, CarrierOriginChainupAmount = SC.OriginChainupAmount
			, CarrierDestChainupRate = SC.DestChainupRate
			, CarrierDestChainupRateType = SC.DestChainupRateType  
			, CarrierDestChainupAmount = SC.DestChainupAmount
			, CarrierRerouteRate = SC.RerouteRate
			, CarrierRerouteRateType = SC.RerouteRateType  
			, CarrierRerouteAmount = SC.RerouteAmount
			, CarrierSplitLoadRate = SC.SplitLoadRate
			, CarrierSplitLoadRateType = SC.SplitLoadRateType  
			, CarrierSplitLoadAmount = SC.SplitLoadAmount
			, CarrierH2SRate = SC.H2SRate
			, CarrierH2SRateType = SC.H2SRateType  
			, CarrierH2SAmount = SC.H2SAmount
			, CarrierTaxRate = SC.OriginTaxRate
			, CarrierTotalAmount = SC.TotalAmount
			--
			, DriverBatchNum = SD.BatchNum
			, DriverBatchDate = DSB.BatchDate		-- 4.4.17
			, DriverBatchInvoiceNum = DSB.InvoiceNum		-- 4.4.17
			, DriverSettlementUomID = SD.SettlementUomID
			, DriverSettlementUom = SD.SettlementUom
			, DriverMinSettlementUnits = SD.MinSettlementUnits
			, DriverSettlementUnits = SD.SettlementUnits
			, DriverRateSheetRate = SD.RateSheetRate
			, DriverRateSheetRateType = SD.RateSheetRateType
			, DriverRouteRate = SD.RouteRate
			, DriverRouteRateType = SD.RouteRateType
			, DriverLoadRate = ISNULL(SD.RouteRate, SD.RateSheetRate)
			, DriverLoadRateType = ISNULL(SD.RouteRateType, SD.RateSheetRateType)
			, DriverLoadAmount = SD.LoadAmount
			, DriverOrderRejectRate = SD.OrderRejectRate 
			, DriverOrderRejectRateType = SD.OrderRejectRateType 
			, DriverOrderRejectAmount = SD.OrderRejectAmount 
			, DriverWaitFeeSubUnit = SD.WaitFeeSubUnit  
			, DriverWaitFeeRoundingType = SD.WaitFeeRoundingType  
			, DriverOriginWaitBillableHours = SD.OriginWaitBillableHours  
			, DriverOriginWaitBillableMinutes = SD.OriginWaitBillableMinutes  
			, DriverOriginWaitRate = SD.OriginWaitRate
			, DriverOriginWaitAmount = SD.OriginWaitAmount
			, DriverDestinationWaitBillableHours = SD.DestinationWaitBillableHours 
			, DriverDestinationWaitBillableMinutes = SD.DestinationWaitBillableMinutes  
			, DriverDestinationWaitRate = SD.DestinationWaitRate 
			, DriverDestinationWaitAmount = SD.DestinationWaitAmount  
			, DriverTotalWaitAmount = SD.TotalWaitAmount
			, DriverTotalWaitBillableMinutes = NULLIF(ISNULL(SD.OriginWaitBillableMinutes, 0) + ISNULL(SD.DestinationWaitBillableMinutes, 0), 0)
			, DriverTotalWaitBillableHours = NULLIF(ISNULL(SD.OriginWaitBillableHours, 0) + ISNULL(SD.DestinationWaitBillableHours, 0), 0)		
			, DriverFuelSurchargeRate = SD.FuelSurchargeRate
			, DriverFuelSurchargeAmount = SD.FuelSurchargeAmount
			, DriverOriginChainupRate = SD.OriginChainupRate
			, DriverOriginChainupRateType = SD.OriginChainupRateType  
			, DriverOriginChainupAmount = SD.OriginChainupAmount
			, DriverDestChainupRate = SD.DestChainupRate
			, DriverDestChainupRateType = SD.DestChainupRateType  
			, DriverDestChainupAmount = SD.DestChainupAmount
			, DriverRerouteRate = SD.RerouteRate
			, DriverRerouteRateType = SD.RerouteRateType  
			, DriverRerouteAmount = SD.RerouteAmount
			, DriverSplitLoadRate = SD.SplitLoadRate
			, DriverSplitLoadRateType = SD.SplitLoadRateType  
			, DriverSplitLoadAmount = SD.SplitLoadAmount
			, DriverH2SRate = SD.H2SRate
			, DriverH2SRateType = SD.H2SRateType  
			, DriverH2SAmount = SD.H2SAmount
			, DriverTaxRate = SD.OriginTaxRate
			, DriverTotalAmount = SD.TotalAmount
			--
			, ProducerBatchNum = SP.BatchNum
			, ProducerBatchDate = PSB.BatchDate		-- 4.4.17
			, ProducerBatchInvoiceNum = PSB.InvoiceNum		-- 4.4.17
			, ProducerSettlementUomID = SP.SettlementUomID
			, ProducerSettlementUom = SP.SettlementUom
			, ProducerSettlementUnits = SP.SettlementUnits
			, ProducerCommodityIndex = SP.CommodityIndex
			, ProducerCommodityIndexID = SP.CommodityIndexID
			, ProducerCommodityMethod = SP.CommodityMethod
			, ProducerCommodityMethodID = SP.CommodityMethodID
			, ProducerIndexPrice = SP.Price
			, ProducerIndexStartDate = SP.IndexStartDate
			, ProducerIndexEndDate = SP.IndexEndDate
			, ProducerPriceAmount = SP.CommodityPurchasePricebookPriceAmount
			, ProducerDeduct = SP.Deduct
			, ProducerDeductType = SP.Deduct
			, ProducerDeductAmount = SP.CommodityPurchasePricebookDeductAmount
			, ProducerPremium = SP.PremiumType
			, ProducerPremiumType = SP.PremiumType
			, ProducerPremiumAmount = SP.CommodityPurchasePricebookPremiumAmount
			, ProducerPremiumDesc = SP.PremiumDesc
			, ProducerTotalAmount = SP.CommodityPurchasePricebookTotalAmount
			--
			, OriginLatLon = LTRIM(OO.LAT) + ',' + LTRIM(OO.LON)
			, OriginGeoFenceRadiusMeters = OO.GeoFenceRadiusMeters
			, OriginCTBNum = OO.CTBNum
			, OriginFieldName = OO.FieldName
			, OriginCity = OO.City																				
			, OriginCityState = OO.City + ', ' + (SELECT Abbreviation FROM tblState WHERE ID = OO.StateID)		
			--
			, DestLatLon = LTRIM(D.LAT) + ',' + LTRIM(D.LON)
			, DestGeoFenceRadiusMeters = D.GeoFenceRadiusMeters
			, DestCity = D.City
			, DestCityState = D.City + ', ' + (SELECT Abbreviation FROM tblState WHERE ID = D.StateID) -- 4.1.0 - fix (was pointing to Origin.StateID)
			--
			, Gauger = GAO.Gauger						
			, GaugerIDNumber = GA.IDNumber
			, GaugerFirstName = GA.FirstName
			, GaugerLastName = GA.LastName
			, GaugerRejected = GAO.Rejected
			, GaugerRejectReasonID = GAO.RejectReasonID
			, GaugerRejectNotes = GAO.RejectNotes
			, GaugerRejectNumDesc = GORR.NumDesc
			, GaugerPrintDateUTC = GAO.PrintDateUTC -- 4.1.0 - added for use by GaugerPrintDate EXPRESSION column for effiency reasons (this is already queried, avoid doing the calculation unless needed)
			--, GaugerPrintDate = dbo.fnUTC_to_Local(GAO.PrintDateUTC, O.OriginTimeZoneID, O.OriginUseDST)
			--
			, T_GaugerCarrierTicketNum = CASE WHEN GAO.TicketCount = 0 THEN LTRIM(GAO.OrderNum) + CASE WHEN GAO.Rejected = 1 THEN 'X' ELSE '' END ELSE GOT.CarrierTicketNum END 
			, T_GaugerTankNum = ISNULL(GOT.OriginTankText, GAO.OriginTankText)
			, T_GaugerIsStrappedTank = GOT.IsStrappedTank 
			, T_GaugerProductObsTemp = GOT.ProductObsTemp
			, T_GaugerProductObsGravity = GOT.ProductObsGravity
			, T_GaugerProductBSW = GOT.ProductBSW		
			, T_GaugerOpeningGaugeFeet = GOT.OpeningGaugeFeet
			, T_GaugerOpeningGaugeInch = GOT.OpeningGaugeInch		
			, T_GaugerOpeningGaugeQ = GOT.OpeningGaugeQ			
			, T_GaugerBottomFeet = GOT.BottomFeet
			, T_GaugerBottomInches = GOT.BottomInches		
			, T_GaugerBottomQ = GOT.BottomQ		
			, T_GaugerRejected = GOT.Rejected
			, T_GaugerRejectReasonID = GOT.RejectReasonID
			, T_GaugerRejectNumDesc = GOT.RejectNumDesc
			, T_GaugerRejectNotes = GOT.RejectNotes	
			--
			--, OrderCreateDate = dbo.fnUTC_to_Local(O.CreateDateUTC, O.OriginTimeZoneID, O.OriginUseDST) -- replaced 4.1.0 with an expression type value (more efficient, only executed when needed)
			, OrderApproved = CAST(ISNULL(OA.Approved, 0) AS BIT)
		FROM viewOrder_OrderTicket_Full O
		JOIN tblOrigin OO ON OO.ID = O.OriginID
		JOIN tblDestination D ON D.ID = O.DestinationID
		--
		LEFT JOIN viewGaugerOrder GAO ON GAO.OrderID = O.ID			            
		LEFT JOIN viewGaugerOrderTicket GOT ON GOT.UID = O.T_UID	            
		LEFT JOIN viewGauger GA ON GA.ID = GAO.GaugerID				            
		LEFT JOIN viewOrderRejectReason GORR ON GORR.ID = GAO.RejectReasonID 
		--
		LEFT JOIN tblCustomerDestinationCode CDC ON CDC.CustomerID = O.CustomerID AND CDC.DestinationID = O.DestinationID
		--
		LEFT JOIN viewOrderSettlementCarrier SC ON SC.OrderID = O.ID
		LEFT JOIN tblCarrierSettlementBatch CSB ON CSB.ID = SC.BatchID		-- 4.4.17
		--
		LEFT JOIN viewOrderSettlementShipper SS ON SS.OrderID = O.ID
		LEFT JOIN tblShipperSettlementBatch SSB ON SSB.ID = SS.BatchID
		--
		LEFT JOIN viewOrderSettlementDriver SD ON SD.OrderID = O.ID
		LEFT JOIN tblDriverSettlementBatch DSB ON DSB.ID = SD.BatchID		-- 4.4.17
		--
		LEFT JOIN viewOrderSettlementProducer SP ON SP.OrderID = O.ID
		LEFT JOIN tblProducerSettlementBatch PSB ON PSB.ID = SP.BatchID		-- 4.4.17
		--
		LEFT JOIN tblShipperTicketType STT ON STT.CustomerID = O.CustomerID AND STT.TicketTypeID = O.TicketTypeID
		--
		LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
	) X
GO


--Refresh EVERYTHING!
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRecompileAllStoredProcedures
GO
EXEC _spRebuildAllObjects
GO
EXEC _spRebuildAllObjects
GO



COMMIT
SET NOEXEC OFF