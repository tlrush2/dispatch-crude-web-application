SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.1.3.3'
SELECT  @NewVersion = '4.1.3.4'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1763 Driver Settlement import issues'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/*************************************/
-- Created: 4.1.0 - 2016.08.24 - Kevin Alons
-- Purpose: handle specialized logic related to editing Route Rates (due to RouteID being "inputted" as Origin|Destination combination)
-- Changes:
--		4.1.3.4		JAE		2016/09/15		Fixed reference to incorrect field (DriverID vs DriverGroupID) in insert statement
/*************************************/
ALTER TRIGGER trigViewDriverRouteRate_IU_Update ON viewDriverRouteRate INSTEAD OF INSERT, UPDATE  AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
		--PRINT 'ensure a Route record exists for the new Origin-Destination combo'
		INSERT INTO tblRoute (OriginID, DestinationID)
			SELECT DISTINCT OriginID, DestinationID FROM inserted
			EXCEPT SELECT OriginID, DestinationID FROM tblRoute
		
		-- PRINT 'Updating any existing record editable data'
		UPDATE tblDriverRouteRate
			SET ShipperID = nullif(i.ShipperID, 0)
				, DriverID = nullif(i.DriverID, 0)
				, ProductGroupID = nullif(i.ProductGroupID, 0)
				, TruckTypeID = nullif(i.TruckTypeID, 0)
				, DriverGroupID = nullif(i.DriverGroupID, 0)
				, RouteID = R.ID
				, Rate = i.Rate
				, EffectiveDate = i.EffectiveDate
				, EndDate = i.EndDate
				, RateTypeID = i.RateTypeID
				, UomID = i.UomID
				, LastChangeDateUTC = i.LastChangeDateUTC
				, LastChangedByUser = i.LastChangedByUser
		FROM tblDriverRouteRate X
		JOIN inserted i ON i.ID = X.ID
		JOIN tblRoute R ON R.OriginID = i.OriginID AND R.DestinationID = i.DestinationID 

		-- PRINT 'insert any new records'
		INSERT INTO tblDriverRouteRate (ShipperID, CarrierID, ProductGroupID, TruckTypeID, DriverGroupID, DriverID, RouteID, Rate, EffectiveDate, EndDate, RateTypeID, UomID, CreatedByUser)
			SELECT nullif(ShipperID, 0), nullif(CarrierID, 0), nullif(ProductGroupID, 0), nullif(TruckTypeID, 0), nullif(DriverGroupID, 0), nullif(DriverID, 0), R.ID, Rate, EffectiveDate, EndDate, RateTypeID, UomID, i.CreatedByUser
			FROM inserted i
			JOIN tblRoute R ON R.OriginID = i.OriginID AND R.DestinationID = i.DestinationID 
			WHERE ISNULL(i.ID, 0) = 0
	END TRY
	BEGIN CATCH
		DECLARE @error varchar(255)
		SET @error = ERROR_MESSAGE()
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END CATCH
END

GO


COMMIT
SET NOEXEC OFF