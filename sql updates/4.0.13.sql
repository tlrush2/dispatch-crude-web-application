SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.0.12'
SELECT  @NewVersion = '4.0.13'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1715 Fix missing truck/trailer types from Import Center'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


-- Added excepts to all queries since it was already run on Lessley account
INSERT INTO tblObject (ID, Name, SqlSourceName, SqlTargetName)
	SELECT 46, 'Truck Type', 'tblTruckType', 'tblTruckType'
	EXCEPT 	SELECT ID, Name, SqlSourceName, SqlTargetName FROM tblObject

INSERT INTO tblObject (ID, Name, SqlSourceName, SqlTargetName)
	SELECT 47, 'Trailer Type', 'tblTrailerType', 'tblTrailerType'
	EXCEPT 	SELECT ID, Name, SqlSourceName, SqlTargetName FROM tblObject
GO


SET IDENTITY_INSERT tblObjectField ON
INSERT INTO tblObjectField (ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName)
SELECT 429,'46','ID','ID','3',NULL,'1','1','0',NULL,NULL
EXCEPT SELECT ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName from tblObjectField
INSERT INTO tblObjectField (ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName)
SELECT 430,'46','LoadOnTruck','Load On Truck','2','((0))','1','0','0',NULL,NULL
EXCEPT SELECT ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName from tblObjectField
INSERT INTO tblObjectField (ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName)
SELECT 431,'46','Name','Name','1',NULL,'0','0','0',NULL,NULL
EXCEPT SELECT ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName from tblObjectField
INSERT INTO tblObjectField (ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName)
SELECT 432,'47','CertRequired','Cert Required','2',NULL,'0','0','0',NULL,NULL
EXCEPT SELECT ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName from tblObjectField
INSERT INTO tblObjectField (ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName)
SELECT 433,'47','ID','ID','3',NULL,'1','1','0',NULL,NULL
EXCEPT SELECT ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName from tblObjectField
INSERT INTO tblObjectField (ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName)
SELECT 434,'47','Name','Name','1',NULL,'0','0','0',NULL,NULL
EXCEPT SELECT ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName from tblObjectField
SET IDENTITY_INSERT tblObjectField OFF

SET IDENTITY_INSERT tblObjectField ON
INSERT INTO tblObjectField (ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName)
SELECT 435,'14','TruckTypeID','Truck Type','3',NULL,'1','0','0','46','ID'
EXCEPT SELECT ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName from tblObjectField
INSERT INTO tblObjectField (ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName)
SELECT 436,'15','TrailerTypeID','Trailer Type','3',NULL,'1','0','0','47','ID'
EXCEPT SELECT ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName from tblObjectField
SET IDENTITY_INSERT tblObjectField OFF

GO


COMMIT
SET NOEXEC OFF