/*
	-- revisions to DBAudit process (efficiency and way to calculate/view change history)
	-- revise tblOrder | tblOrderTicket trigger structure for efficiency & accuracy 
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.0.11'
SELECT  @NewVersion = '3.0.12'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblOrderDbAudit]') AND type in (N'U'))
CREATE TABLE [dbo].[tblOrderDbAudit](
	DBAuditDate datetime NOT NULL CONSTRAINT DF_OrderDbAudit DEFAULT (getutcdate()),
	[ID] [int] NOT NULL,
	[OrderNum] [int] NULL,
	[StatusID] [int] NOT NULL,
	[PriorityID] [tinyint] NULL,
	[DueDate] [date] NOT NULL,
	[RouteID] [int] NULL,
	[OriginID] [int] NULL,
	[OriginArriveTimeUTC] [datetime] NULL,
	[OriginDepartTimeUTC] [datetime] NULL,
	[OriginMinutes] [int] NULL,
	[OriginWaitNotes] [varchar](255) NULL,
	[OriginBOLNum] [varchar](15) NULL,
	[OriginGrossUnits] [decimal](9, 3) NULL,
	[OriginNetUnits] [decimal](9, 3) NULL,
	[DestinationID] [int] NULL,
	[DestArriveTimeUTC] [datetime] NULL,
	[DestDepartTimeUTC] [datetime] NULL,
	[DestMinutes] [int] NULL,
	[DestWaitNotes] [varchar](255) NULL,
	[DestBOLNum] [varchar](30) NULL,
	[DestGrossUnits] [decimal](9, 3) NULL,
	[DestNetUnits] [decimal](9, 3) NULL,
	[CustomerID] [int] NULL,
	[CarrierID] [int] NULL,
	[DriverID] [int] NULL,
	[TruckID] [int] NULL,
	[TrailerID] [int] NULL,
	[Trailer2ID] [int] NULL,
	[OperatorID] [int] NULL,
	[PumperID] [int] NULL,
	[TicketTypeID] [int] NOT NULL,
	[Rejected] [bit] NOT NULL,
	[RejectNotes] [varchar](255) NULL,
	[ChainUp] [bit] NOT NULL,
	[OriginTruckMileage] [int] NULL,
	[OriginTankNum] [varchar](20) NULL,
	[DestTruckMileage] [int] NULL,
	[CarrierTicketNum] [varchar](15) NULL,
	[AuditNotes] [varchar](255) NULL,
	[CreateDateUTC] [smalldatetime] NULL,
	[ActualMiles] [int] NULL,
	[ProducerID] [int] NULL,
	[CreatedByUser] [varchar](100) NULL,
	[LastChangeDateUTC] [smalldatetime] NULL,
	[LastChangedByUser] [varchar](100) NULL,
	[DeleteDateUTC] [smalldatetime] NULL,
	[DeletedByUser] [varchar](100) NULL,
	[DestProductBSW] [decimal](9, 3) NULL,
	[DestProductGravity] [decimal](9, 3) NULL,
	[DestProductTemp] [decimal](9, 3) NULL,
	[DestOpenMeterUnits] [decimal](9, 3) NULL,
	[DestCloseMeterUnits] [decimal](9, 3) NULL,
	[ProductID] [int] NOT NULL,
	[AcceptLastChangeDateUTC] [smalldatetime] NULL,
	[PickupLastChangeDateUTC] [smalldatetime] NULL,
	[DeliverLastChangeDateUTC] [smalldatetime] NULL,
	[OriginUomID] [int] NOT NULL,
	[DestUomID] [int] NOT NULL,
	[PickupPrintStatusID] [int] NOT NULL,
	[DeliverPrintStatusID] [int] NOT NULL,
	[PickupPrintDateUTC] [datetime] NULL,
	[DeliverPrintDateUTC] [datetime] NULL,
	[OriginTankID] [int] NULL,
	[OriginGrossStdUnits] [decimal](18, 6) NULL,
	[DispatchNotes] [varchar](500) NULL,
	[DriverNotes] [varchar](255) NULL,
	[DispatchConfirmNum] [varchar](25) NULL,
/*	[OriginWaitReasonID] [int] NULL,
	[DestWaitReasonID] [int] NULL,
	[RejectReasonID] [int] NULL, */
) ON [PRIMARY]
GO
GRANT INSERT ON tblOrderDbAudit TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trigOrder_U_dbaudit]') AND type in (N'TR'))
	DROP TRIGGER [trigOrder_U_dbaudit]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetOrderChanges]') AND type in (N'P'))
	DROP PROCEDURE spGetOrderChanges
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 11 Sep 2011
-- Description:	compute and return the order changes for a single order (specified by an Order.ID value)
-- =============================================
CREATE PROCEDURE spGetOrderChanges(@id int) 
AS BEGIN
	DECLARE @ret TABLE 
	(
		FieldName varchar(255)
	  , NewValue varchar(max)
	  , OldValue varchar(max)
	  , ChangeDateUTC datetime
	  , ChangedByUser varchar(100)
	  , IsCurrent bit
	)

	DECLARE @columns TABLE (id int, name varchar(100))
	INSERT INTO @columns 		
		SELECT ROW_NUMBER() OVER (ORDER BY COLUMN_NAME), COLUMN_NAME 
		FROM INFORMATION_SCHEMA.COLUMNS 
		WHERE TABLE_NAME = 'tblOrder' AND COLUMN_NAME NOT IN ('ID','LastChangeDateUTC')
	
	SELECT rowID = ROW_NUMBER() OVER (ORDER BY sortnum, dbauditdate desc, RouteID desc), * 
	INTO #history
	FROM (
		SELECT dbauditdate = NULL, *, sortnum = 0 from tblOrder where ID = @id
		UNION ALL SELECT *, sortnum = 1 from tblOrderDbAudit where ID = @id
	) X

	DECLARE @rowID int, @colID int, @colName varchar(100), @newValue varchar(max), @oldValue varchar(max), @sql nvarchar(max), @newColID int
	DECLARE @changeDateUTC datetime, @changeUser varchar(100), @isCurrent bit

	SELECT @rowID = MIN(rowID) FROM #history
	WHILE EXISTS(SELECT * FROM #history WHERE rowID = @rowID + 1) BEGIN
		SELECT TOP 1 @colName = name, @colID = id FROM @columns WHERE id = 1
		WHILE (@colName IS NOT NULL) BEGIN

			-- get the "now" value
			SET @sql = N'SELECT TOP 1 @value=' + @colName + N', @changeDateUTC=LastChangeDateUTC, @changeUser=LastChangedByUser FROM #history WHERE rowID = @rowID'
			EXEC sp_executesql @sql
				, N'@rowID int, @changeDateUTC datetime OUTPUT, @changeUser varchar(100) OUTPUT, @value varchar(max) OUTPUT'
				, @rowID = @rowID, @changeDateUTC = @changeDateUTC OUTPUT, @changeUser = @changeUser OUTPUT, @value = @newValue OUTPUT

			-- get the "from" value
			SET @sql = N'SELECT TOP 1 @value=' + @colName + N' FROM #history WHERE rowID = @rowID + 1'
			EXEC sp_executesql @sql, N'@rowID int, @value varchar(max) OUTPUT', @rowID = @rowID, @value = @oldValue OUTPUT
			
			IF (isnull(@newValue, '\r') <> isnull(@oldValue, '\r')) BEGIN
				INSERT INTO @ret 
					SELECT @colName, @newValue, @oldValue, @changeDateUTC, @changeUser, CASE WHEN @rowID = 1 THEN 1 ELSE 0 END
			END
			
			SET @colName = NULL
			SELECT TOP 1 @colName = name, @colID = id FROM @columns WHERE id > @colID ORDER BY ID
			
		END

		SET @rowID = @rowID + 1
	END
	
	SELECT * FROM @ret

END
GO
GRANT EXECUTE ON spGetOrderChanges TO dispatchcrude_iis_acct
GO

INSERT INTO tblSetting (ID, Name, SettingTypeID, Value, Category, CreatedByUser, ReadOnly)
	VALUES (32, 'DBAudit enabled', 2, 'false', 'System Wide', 'System', 0)
GO
INSERT INTO tblSetting (ID, Name, SettingTypeID, Value, Category, CreatedByUser, ReadOnly)
	VALUES (33, 'Minimum App Version', 1, '1.1.0', 'Driver App Settings', 'System', 0)
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trigOrder_U_dbaudit]') AND type in (N'TR'))
	DROP TRIGGER [trigOrder_U_dbaudit]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trigOrder_IU_LastChangeUpdate]') AND type in (N'TR'))
	DROP TRIGGER [trigOrder_IU_LastChangeUpdate]
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 21 Jun 2013
-- Description:	trigger to ensure the entered values for an Order are actually valid
-- =============================================
ALTER TRIGGER [dbo].[trigOrder_IU_Validate] ON [dbo].[tblOrder] AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	PRINT 'trigOrder_IU_Validate FIRED'
	
	-- ensure the Origin and Destinations are both specified unless the Status is:
	--   (Generated, Assigned, Dispatched or Declined)
	IF  EXISTS(SELECT * FROM inserted O
		WHERE (O.OriginID IS NULL OR O.DestinationID IS NULL) AND O.StatusID NOT IN (-10,1,2,9))
	BEGIN
		RAISERROR('Invalid Order Status when the Origin and/or Or Destination is not specified', 16, 1)
		RETURN
	END
	
END

GO

sp_settriggerorder @triggername= 'trigOrder_IU_Validate', @order='FIRST', @stmttype = 'INSERT'
GO
sp_settriggerorder @triggername= 'trigOrder_IU_Validate', @order='FIRST', @stmttype = 'UPDATE'
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 19 Dec 2012
-- Description:	trigger to add a unique, incrementing OrderNum to each new Order (manual Identity column)
--				and other supporting/coordinating logic
-- =============================================
ALTER TRIGGER [dbo].[trigOrder_IU] ON [dbo].[tblOrder] AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

		-- Note: the TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU') ) = 1 statement prevents recursion
	IF (TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU')) = 1) BEGIN

		PRINT 'trigOrder_IU FIRED'

		WHILE (SELECT count(*) FROM inserted i JOIN tblOrder O ON O.ID = i.ID WHERE O.OrderNum IS NULL) > 0
		BEGIN
			UPDATE tblOrder 
			  SET OrderNum = isnull((SELECT max(OrderNum) FROM tblOrder), 100000) + 1
				, CreateDateUTC = getutcdate()
			WHERE ID = (SELECT min(O.ID) FROM tblOrder O JOIN inserted i ON i.ID = O.ID WHERE O.OrderNum IS NULL)
		END
		
		-- re-compute the OriginMinutes (in case the website failed to compute it properly)
		IF UPDATE(OriginArriveTimeUTC) OR UPDATE(OriginDepartTimeUTC)
		BEGIN
			UPDATE tblOrder SET OriginMinutes = datediff(minute, i.OriginArriveTimeUTC, i.OriginDepartTimeUTC)
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
		END
		-- re-compute the DestWaitMinutes (in case the website failed to compute it properly)
		IF UPDATE(DestArriveTimeUTC) OR UPDATE(DestDepartTimeUTC)
		BEGIN
			UPDATE tblOrder SET DestMinutes = datediff(minute, i.DestArriveTimeUTC, i.DestDepartTimeUTC)
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
		END
		
		-- ensure missing Routes are created and assigned for the specified Origin/Destination combination
		IF UPDATE(OriginID) OR UPDATE(DestinationID)
		BEGIN
			-- create any missing Route records
			INSERT INTO tblRoute (OriginID, DestinationID, CreateDateUTC, CreatedByUser)
				SELECT i.OriginID, i.DestinationID, GETUTCDATE(), ISNULL(i.LastChangedByUser, i.CreatedByUser)
				FROM inserted i
				LEFT JOIN tblRoute R ON R.OriginID = i.OriginID AND R.DestinationID = i.DestinationID
				WHERE i.OriginID IS NOT NULL AND i.DestinationID IS NOT NULL AND R.ID IS NULL
			-- ensure the Order records refer to the correct Route (ID)
			UPDATE tblOrder SET RouteID = R.ID
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
			LEFT JOIN tblRoute R ON R.OriginID = O.OriginID AND R.DestinationID = O.DestinationID

			-- update the ActualMiles from the related Route
			UPDATE tblOrder SET ActualMiles = R.ActualMiles
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
			JOIN tblRoute R ON R.ID = O.RouteID
		END
		
		IF (UPDATE(OriginID))
		BEGIN
			-- update Order.ProducerID to match what is assigned to the new Origin
			UPDATE tblOrder SET ProducerID = OO.ProducerID
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			WHERE d.OriginID <> O.OriginID

			-- update Order.OriginUomID to match what is assigned to the new Origin (quantities are updated below)
			UPDATE tblOrder SET OriginUomID = OO.UomID
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			WHERE d.OriginID <> O.OriginID AND d.OriginUomID <> OO.UomID
		END
		
		-- keep the DestUomID in sync with the Destination (units are updated below)
		IF (UPDATE(DestinationID))
		BEGIN
			-- update Order.DestUomID to match what is assigned to the new Destination
			UPDATE tblOrder 
			  SET DestUomID = DD.UomID
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID
			JOIN tblDestination DD ON DD.ID = O.DestinationID
			WHERE d.DestinationID <> O.DestinationID AND d.DestUomID <> DD.UomID
		END
		
		-- ensure that any set PrintStatusID values are reset to 0 when backing up the general StatusID value
		IF (UPDATE(StatusID))
		BEGIN
			UPDATE tblOrder 
			  SET DeliverPrintStatusID = 0 
			  FROM tblOrder O
			  JOIN deleted d ON d.ID = O.ID
			WHERE O.StatusID <> d.StatusID AND O.StatusID IN (-10, 1, 2, 7, 8)

			UPDATE tblOrder 
			  SET PickupPrintStatusID = 0 
			  FROM tblOrder O
			  JOIN deleted d ON d.ID = O.ID
			WHERE O.StatusID <> d.StatusID AND O.StatusID IN (-10, 1, 2, 7)
		END

		-- ensure that any change to the DriverID will update the Truck/Trailer/Trailer2 values to this driver's defaults
		IF (UPDATE(DriverID))
		BEGIN
			UPDATE tblOrder 
			  SET TruckID = DR.TruckID
				, TrailerID = DR.TrailerID
				, Trailer2ID = DR.Trailer2ID
			  FROM tblOrder O
			  JOIN deleted d ON d.ID = O.ID
			  JOIN tblDriver DR ON DR.ID = O.DriverID
			WHERE O.DriverID <> d.DriverID 
			  AND O.StatusID IN (-10,1,2,7,9) -- Generated, Assigned, Dispatched, Accepted, Declined
		END
		
		-- just updating the tickets here, trigOrderTicket_IU will in turn update this table
		-- ensure any existing OrderTickets are recomputed with the new OriginUOM
		UPDATE tblOrderTicket
		  SET GrossUnits = dbo.fnConvertUOM(GrossUnits, d.OriginUomID, i.OriginUomID)
			, NetUnits = dbo.fnConvertUOM(NetUnits, d.OriginUomID, i.OriginUomID)
		FROM tblOrderTicket OT
		JOIN tblOrder i ON i.ID = OT.OrderID
		JOIN deleted d ON d.ID = i.ID
		WHERE i.OriginUomID <> d.OriginUomID
		
		-- recalculate the Destination Unit values if only if the DestUomID changed but the values didn't
		BEGIN
			UPDATE tblOrder
			  SET DestGrossUnits = dbo.fnConvertUOM(O.DestGrossUnits, d.DestUomID, O.DestUomID)
				, DestNetUnits = dbo.fnConvertUOM(O.DestNetUnits, d.DestUomID, O.DestUomID)
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID
			WHERE d.DestUomID <> O.DestUomID 
			  AND d.DestGrossUnits = O.DestGrossUnits
			  AND d.DestNetUnits = O.DestNetUnits
		END
		--NOTE: we do not update the DestOpenMeterUnits/DestClosignMeterUnits since they don't auto update
		--  (and they simply represent a reading that is assumed to in the DestUomID UOM at entry)
		--  (if they are updated in the future [manually] the will calculate again using the DestUomID at that time)

		-- ensure that all changed orders have their LastChangeDateUTC set to UTCNOW
		UPDATE tblOrder
		  SET LastChangeDateUTC = GETUTCDATE()
			, LastChangedByUser = ISNULL(O.LastChangedByUser, 'System')
		FROM tblOrder O
		JOIN inserted i ON i.ID = O.ID
		JOIN deleted d ON d.ID = O.id
		-- ensure a lastchangedateutc value is set (and updated if it isn't close to NOW and was explicitly changed by the callee)
		WHERE O.LastChangeDateUTC IS NULL 
			--
			OR (i.LastChangeDateUTC = d.LastChangeDateUTC AND abs(datediff(second, O.LastChangeDateUTC, GETUTCDATE())) > 2)

		-- optionally add tblOrderDBAudit records
		BEGIN TRY
			IF EXISTS(SELECT * FROM tblSetting WHERE ID = 32 AND (Value LIKE 'true' OR Value LIKE 'yes'))
				INSERT INTO tblOrderDbAudit
					SELECT GETUTCDATE(), d.* 
					FROM deleted d
		END TRY
		BEGIN CATCH
			PRINT 'trigOrder_IU.DBAUDIT FAILURE: ' + ERROR_MESSAGE()
		END CATCH

	PRINT 'trigOrder_IU COMPLETE'

	END
	
END

GO

/**********************************************************/
-- Author: Kevin Alons
-- Date Created: 26 Sep 2013
-- Purpose: track changes to Orders that cause it be "virtually" deleted for a Driver (for DriveApp.Sync purposes)
/**********************************************************/
ALTER TRIGGER [dbo].[trigOrder_U_VirtualDelete] ON [dbo].[tblOrder] AFTER UPDATE AS
BEGIN
	DECLARE @NewRecords TABLE (
		  OrderID int NOT NULL
		, DriverID int NOT NULL
		, LastChangeDateUTC smalldatetime NOT NULL
		, LastChangedByUser varchar(100) NULL
	)

	SET NOCOUNT ON;
	
	PRINT 'trigOrder_U_VirtualDelete FIRED'
	
	-- delete any records that no longer apply because now the record is valid for the new driver/status
	DELETE FROM tblOrderDriverAppVirtualDelete
	FROM tblOrderDriverAppVirtualDelete ODAVD
	JOIN inserted i ON i.ID = ODAVD.OrderID AND i.DriverID = ODAVD.DriverID
	WHERE i.DeleteDateUTC IS NULL 
		AND dbo.fnPrintStatusID(i.StatusID, i.PickupPrintStatusID, i.DeliverPrintStatusID) IN (2,7,8,3)

	INSERT INTO @NewRecords
		-- insert any applicable VIRTUALDELETE records where the status changed from 
		--   a valid status to an invalid [DriverApp] status
		SELECT d.ID, d.DriverID, GETUTCDATE(), ISNULL(i.LastChangedByUser, 'System')
		FROM deleted d
		JOIN inserted i ON i.ID = D.ID
		WHERE dbo.fnPrintStatusID(i.StatusID, i.PickupPrintStatusID, i.DeliverPrintStatusID) NOT IN (2,7,8,3)
		  AND dbo.fnPrintStatusID(d.StatusID, d.PickupPrintStatusID, d.DeliverPrintStatusID) IN (2,7,8,3)
		  AND d.DriverID IS NOT NULL
		  
		UNION

		-- insert any records where the DriverID changed to another DriverID (for a valid status)
		SELECT d.ID, d.DriverID, GETUTCDATE(), ISNULL(i.LastChangedByUser, 'System')
		FROM deleted d
		JOIN inserted i ON i.ID = D.ID
		WHERE d.DriverID <> i.DriverID 
		  AND dbo.fnPrintStatusID(d.StatusID, d.PickupPrintStatusID, d.DeliverPrintStatusID) IN (2,7,8,3)
		  AND d.DriverID IS NOT NULL
		  
	-- update the VirtualDeleteDateUTC value for any existing records
	UPDATE tblOrderDriverAppVirtualDelete
	  SET VirtualDeleteDateUTC = NR.LastChangeDateUTC, VirtualDeletedByUser = NR.LastChangedByUser
	FROM tblOrderDriverAppVirtualDelete ODAVD
	JOIN @NewRecords NR ON NR.OrderID = ODAVD.OrderID AND NR.DriverID = ODAVD.DriverID

	-- insert the truly new VirtualDelete records	
	INSERT INTO tblOrderDriverAppVirtualDelete (OrderID, DriverID, VirtualDeleteDateUTC, VirtualDeletedByUser)
		SELECT NR.OrderID, NR.DriverID, NR.LastChangeDateUTC, NR.LastChangedByUser
		FROM @NewRecords NR
		LEFT JOIN tblOrderDriverAppVirtualDelete ODAVD ON ODAVD.OrderID = NR.OrderID AND ODAVD.DriverID = NR.DriverID
		WHERE ODAVD.ID IS NULL

END

GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 6 Feb 2013
-- Description:	trigger to ensure the entered values for an OrderTicket are actually valid
-- =============================================
ALTER TRIGGER [dbo].[trigOrderTicket_IU_Validate] ON [dbo].[tblOrderTicket] AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	PRINT 'trigOrderTicket_IU_Validate FIRED'
	
	IF (SELECT COUNT(*) FROM (
			SELECT OT.OrderID
			FROM tblOrderTicket OT
			JOIN inserted i ON i.OrderID = OT.OrderID AND i.CarrierTicketNum = OT.CarrierTicketNum
			WHERE OT.DeleteDateUTC IS NULL
			GROUP BY OT.OrderID, OT.CarrierTicketNum
			HAVING COUNT(*) > 1
		) v) > 0
	BEGIN
		RAISERROR('Duplicate Ticket Numbers are not allowed', 16, 1)
		RETURN
	END
	
	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (3) AND BOLNum IS NULL AND DeleteDateUTC IS NULL) > 0
	BEGIN
		RAISERROR('BOL # value is required for Meter Run tickets', 16, 1)
		RETURN
	END
	
	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1, 2) AND OriginTankID IS NULL AND TankNum IS NULL AND DeleteDateUTC IS NULL) > 0
	BEGIN
		RAISERROR('Tank ID value is required for Gauge Run & Net Volume tickets', 16, 1)
		RETURN
	END

	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1) AND CarrierTicketNum IS NULL AND DeleteDateUTC IS NULL) > 0
	BEGIN
		RAISERROR('Ticket # value is required for Gauge Run tickets', 16, 1)
		RETURN
	END

	IF (SELECT COUNT(*) FROM inserted WHERE DeleteDateUTC IS NULL
			AND (TicketTypeID IN (1, 2) AND (ProductObsTemp IS NULL OR ProductObsGravity IS NULL OR ProductBSW IS NULL))
				OR (TicketTypeID IN (1) AND (ProductHighTemp IS NULL OR ProductLowTemp IS NULL))
		) > 0
	BEGIN
		RAISERROR('All Product Measurement values are required for Gauge Run & Net Volume tickets', 16, 1)
		RETURN
	END
	
	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1) AND Rejected = 0 AND DeleteDateUTC IS NULL
		AND (OpeningGaugeFeet IS NULL OR OpeningGaugeInch IS NULL OR OpeningGaugeQ IS NULL)) > 0
	BEGIN
		RAISERROR('All Opening Gauge values are required for Gauge Run tickets', 16, 1)
		RETURN
	END
	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1) AND Rejected = 0 AND DeleteDateUTC IS NULL
		AND (ClosingGaugeFeet IS NULL OR ClosingGaugeInch IS NULL OR ClosingGaugeQ IS NULL)) > 0
	BEGIN
		RAISERROR('All Closing Gauge values are required for Gauge Run tickets', 16, 1)
		RETURN
	END

	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (2) AND Rejected = 0 AND DeleteDateUTC IS NULL
		AND (GrossUnits IS NULL)) > 0
	BEGIN
		RAISERROR('Gross Volume value is required for Net Unit tickets', 16, 1)
		RETURN
	END

	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (3) AND Rejected = 0 AND DeleteDateUTC IS NULL
		AND (GrossUnits IS NULL OR NetUnits IS NULL)) > 0
	BEGIN
		RAISERROR('Gross & Net Volume values are required for Meter Run tickets', 16, 1)
		RETURN
	END

	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1) AND Rejected = 0 AND DeleteDateUTC IS NULL
		AND (SealOff IS NULL OR SealOn IS NULL)) > 0
	BEGIN
		RAISERROR('All Seal Off & Seal On values are required for Gauge Run tickets', 16, 1)
		RETURN
	END

	PRINT 'trigOrderTicket_IU_Validate COMPLETE'

END


GO

EXEC sp_settriggerorder @triggername=N'[dbo].[trigOrderTicket_IU_Validate]', @order=N'First', @stmttype=N'INSERT'
GO

EXEC sp_settriggerorder @triggername=N'[dbo].[trigOrderTicket_IU_Validate]', @order=N'First', @stmttype=N'UPDATE'
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 6 Feb 2013
-- Description:	trigger to ensure the GrossBarrels and NetBarrels are computed for valid, entered data
-- =============================================
ALTER TRIGGER [dbo].[trigOrderTicket_IU] ON [dbo].[tblOrderTicket] AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	-- Note: the TRIGGER_NESTLEVEL( OBJECT_ID('trigOrderTicket_IU') ) = 1 statement prevents recursion
	IF (TRIGGER_NESTLEVEL( OBJECT_ID('trigOrderTicket_IU')) = 1) BEGIN

		PRINT 'trigOrderTicket_IU FIRED'
		
		-- re-compute GaugeRun ticket Gross barrels
		IF UPDATE(OriginTankID) OR UPDATE(OpeningGaugeFeet) OR UPDATE(OpeningGaugeInch) OR UPDATE(OpeningGaugeQ)
			OR UPDATE (ClosingGaugeFeet) OR UPDATE(ClosingGaugeInch) OR UPDATE(ClosingGaugeQ) 
			OR UPDATE (GrossUnits)
		BEGIN
			UPDATE tblOrderTicket
			  SET GrossUnits = dbo.fnConvertUom(dbo.fnOriginTankStrappingLevelDeltaBarrels(
					OT.OriginTankID
				  , OpeningGaugeFeet
				  , OpeningGaugeInch
				  , OpeningGaugeQ
				  , ClosingGaugeFeet
				  , ClosingGaugeInch
				  , ClosingGaugeQ), 1, O.OriginUomID) -- ensure this value is always normalized from BARRELS
			FROM tblOrderTicket OT
			JOIN tblOrder O ON O.ID = OT.OrderID
			WHERE OT.ID IN (SELECT ID FROM inserted) 
			  AND OT.TicketTypeID = 1 -- Gauge Run Ticket
			  AND OT.OpeningGaugeFeet IS NOT NULL
			  AND OT.OpeningGaugeInch IS NOT NULL
			  AND OT.OpeningGaugeQ IS NOT NULL
			  AND OT.ClosingGaugeFeet IS NOT NULL
			  AND OT.ClosingGaugeInch IS NOT NULL
			  AND OT.ClosingGaugeQ IS NOT NULL
		END
		-- re-compute GaugeRun | Net Barrel tickets NetUnits
		IF UPDATE(OpeningGaugeFeet) OR UPDATE(OpeningGaugeInch) OR UPDATE(OpeningGaugeQ)
			OR UPDATE (ClosingGaugeFeet) OR UPDATE(ClosingGaugeInch) OR UPDATE(ClosingGaugeQ) 
			OR UPDATE(OriginTankID) OR UPDATE(GrossUnits) OR UPDATE(GrossStdUnits) OR UPDATE(NetUnits)
			OR UPDATE(ProductObsTemp) OR UPDATE(ProductObsGravity) OR UPDATE(ProductBSW)
		BEGIN
			UPDATE tblOrderTicket
			  SET NetUnits = dbo.fnCrudeNetCalculator(GrossUnits, ProductObsTemp, ProductObsGravity, ProductBSW)
				, GrossStdUnits = dbo.fnCrudeNetCalculator(GrossUnits, ProductObsTemp, ProductObsGravity, 0)
			WHERE ID IN (SELECT ID FROM inserted)
			  AND TicketTypeID IN (1,2)
			  AND GrossUnits IS NOT NULL
			  AND ProductObsTemp IS NOT NULL
			  AND ProductObsGravity IS NOT NULL
			  AND ProductBSW IS NOT NULL
		END
		
		-- ensure the Order record is in-sync with the Tickets
		UPDATE tblOrder 
		SET OriginGrossUnits = (SELECT sum(GrossUnits) FROM tblOrderTicket WHERE OrderID = O.ID AND Rejected = 0 AND DeleteDateUTC IS NULL)
		  , OriginNetUnits = (SELECT sum(NetUnits) FROM tblOrderTicket WHERE OrderID = O.ID AND Rejected = 0 AND DeleteDateUTC IS NULL)
		  , OriginGrossStdUnits = (SELECT sum(GrossStdUnits) FROM tblOrderTicket WHERE OrderID = O.ID AND Rejected = 0 AND DeleteDateUTC IS NULL)

			-- use the first MeterRun/BasicRun BOLNum as the Order BOL Num (favor the first un-Rejected)
		  , OriginBOLNum = (
				SELECT TOP 1 BOLNum FROM (
					SELECT TOP 1 BOLNum FROM tblOrderTicket 
					WHERE OrderID = O.ID AND TicketTypeID NOT IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
					UNION SELECT TOP 1 BOLNum FROM tblOrderTicket 
					WHERE OrderID = O.ID AND TicketTypeID NOT IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X)
			-- use the first GaugeRun|NetVolume CarrierTicketNum as the Order.CarrierTicketNum (favor first un-Rejected)
		  , CarrierTicketNum = (
				SELECT TOP 1 CarrierTicketNum FROM (
					SELECT TOP 1 CarrierTicketNum FROM tblOrderTicket 
					WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
					UNION SELECT TOP 1 CarrierTicketNum FROM tblOrderTicket 
					WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X)
			-- use the first GaugeRun|NetVolume OriginTankID for the Order (favor first un-Rejected ticket)
		  , OriginTankID = (
				SELECT TOP 1 OriginTankID FROM (
					SELECT TOP 1 OriginTankID FROM tblOrderTicket 
					WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
					UNION SELECT TOP 1 OriginTankID FROM tblOrderTicket 
					WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X)
			-- use the first GaugeRun|NetVolume OriginTankNum for the Order (favor first un-Rejected ticket)
		  , OriginTankNum = (
				SELECT TOP 1 OriginTankNum FROM (
					SELECT TOP 1 OriginTankNum FROM tblOrderTicket 
					WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
					UNION SELECT TOP 1 OriginTankNum FROM tblOrderTicket 
					WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X)
		  , LastChangeDateUTC = (SELECT MAX(isnull(LastChangeDateUTC, CreateDateUTC)) FROM inserted WHERE OrderID = O.ID)
		  , LastChangedByUser = (SELECT MIN(isnull(LastChangedByUser, CreatedByUser)) FROM inserted WHERE OrderID = O.ID)
		FROM tblOrder O
		WHERE ID IN (SELECT DISTINCT OrderID FROM inserted)

		PRINT 'trigOrderTicket_IU COMPLETE'
		
	END
	
END

GO

EXEC _spRebuildAllObjects
GO

COMMIT
SET NOEXEC OFF
