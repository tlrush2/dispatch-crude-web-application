/*
	-- revise Report Center to allow column filtering (whether they are available or not) by Roles
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.0.8'
SELECT  @NewVersion = '3.0.9'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

ALTER TABLE tblReportColumnDefinition ADD AllowedRoles varchar(1000) NOT NULL
	CONSTRAINT DF_ReportColumnDefinition_AllowedRoles DEFAULT ('*')
GO

UPDATE tblReportColumnDefinition SET AllowedRoles = 'Administrator,Management,Logistics' WHERE ID IN (1, 2, 31, 42, 70, 75);  -- Carrier, Shipper, Operator, Producer, CarrierType, CarrierNumber
GO

--The following is a general purpose UDF to split comma separated lists into individual items.
--Consider an additional input parameter for the delimiter, so that you can use any delimiter you like.
CREATE FUNCTION [dbo].[fnSplitCSV]
(
	@ValueList varchar(max)
)
RETURNS 
@ParsedList table
(
	Value varchar(255)
)
AS
BEGIN
	IF (@ValueList LIKE '%;%' OR @ValueList LIKE '%GO%')
	BEGIN
		RETURN
	END

	DECLARE @Value varchar(255), @Pos int

	SET @ValueList = LTRIM(RTRIM(@ValueList))+ ','
	SET @Pos = CHARINDEX(',', @ValueList, 1)

	IF REPLACE(@ValueList, ',', '') <> ''
	BEGIN
		WHILE @Pos > 0
		BEGIN
			SET @Value = LTRIM(RTRIM(LEFT(@ValueList, @Pos - 1)))
			IF @Value <> ''
			BEGIN
				INSERT INTO @ParsedList (Value) 
				VALUES (@Value) --Use Appropriate conversion
			END
			SET @ValueList = RIGHT(@ValueList, LEN(@ValueList) - @Pos)
			SET @Pos = CHARINDEX(',', @ValueList, 1)

		END
	END	
	RETURN
END

GO

GRANT SELECT ON dbo.fnSplitCSV TO dispatchcrude_iis_acct
GO

/*********************************************************************/
-- Author: Kevin Alons
-- Date Created: 24 Aug 2014
-- Purpose: check if the specified user belongs to at least one of the specified roles (will always match '*')
/*********************************************************************/
CREATE FUNCTION fnUserInRoles(@userName varchar(100), @roles_CSV varchar(1000)) RETURNS bit AS BEGIN
	DECLARE @ret bit
	
	IF (@roles_CSV = '*')
		SET @ret = 1
	ELSE 
		SELECT @ret = COUNT(*) 
		FROM aspnet_Users U
		JOIN aspnet_UsersInRoles UIR ON UIR.UserId = U.UserId
		JOIN aspnet_Roles R ON R.RoleId = UIR.RoleId
		JOIN aspnet_Applications A ON A.ApplicationId = U.ApplicationId
		WHERE A.ApplicationName = 'DispatchCrude'
			AND U.UserName = @userName
			AND R.LoweredRoleName IN (SELECT Value FROM dbo.fnSplitCSV(@roles_CSV))
	
	RETURN (@ret)
END

GO
GRANT EXECUTE ON dbo.fnUserInRoles TO dispatchcrude_iis_acct
GO

ALTER VIEW [dbo].[viewReportColumnDefinition] AS
	SELECT RCD.ID
		, RCD.ReportID
		, RCD.DataField
		, Caption = ISNULL(RCD.Caption, RCD.DataField)
		, RCD.DataFormat
		, RCD.FilterDataField
		, RCD.FilterTypeID
		, RCD.FilterDropDownSql
		, RCD.FilterAllowCustomText
		, RCD.AllowedRoles
		, RFT.FilterOperatorID_CSV
		, BaseFilterCount = (SELECT COUNT(*) FROM tblReportColumnDefinitionBaseFilter WHERE ReportColumnID = RCD.ID)
		, FilterOperatorID_CSV_Delim = ',' + RFT.FilterOperatorID_CSV + ','
	FROM tblReportColumnDefinition RCD
	JOIN tblReportFilterType RFT ON RFT.ID = RCD.FilterTypeID

GO

/***********************************************/
-- Date Created: 27 Jul 2014
-- Author: Kevin Alons
-- Purpose: add related JOINed fields to the tblUserReportColumnDefinition table results
/***********************************************/
ALTER VIEW [dbo].[viewUserReportColumnDefinition] AS
	SELECT URCD.ID
		, URCD.UserReportID
		, URCD.ReportColumnID
		, URCD.Caption
		, DataFormat = isnull(URCD.DataFormat, RCD.DataFormat)
		, URCD.SortNum
		, URCD.FilterOperatorID
		, URCD.FilterValue1
		, URCD.FilterValue2
		, URCD.CreateDateUTC, URCD.CreatedByUser
		, URCD.LastChangeDateUTC, URCD.LastChangedByUser
		, RCD.DataField
		, BaseCaption = RCD.Caption
		, OutputCaption = coalesce(URCD.Caption, RCD.Caption, RCD.DataField)
		, RCD.FilterTypeID
		, FilterDataField = isnull(RCD.FilterDataField, RCD.DataField)
		, RCD.FilterDropDownSql
		, RCD.FilterAllowCustomText
		, RCD.AllowedRoles
		, FilterOperator = RFO.Name
		, FilterValues = CASE 
				WHEN RFO.ID IN (1,4,5) OR RCD.FilterAllowCustomText = 1 THEN FilterValue1 + ISNULL(' - ' + FilterValue2, '')
				ELSE 
					CASE WHEN FilterValue1 IS NOT NULL THEN '&lt;filtered&gt;' 
						 ELSE NULL 
					END 
			END
	FROM tblUserReportColumnDefinition URCD
	JOIN viewReportColumnDefinition RCD ON RCD.ID = URCD.ReportColumnID
	JOIN tblReportFilterOperator RFO ON RFO.ID = URCD.FilterOperatorID

GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 13 May 2013
-- Description:	retrieve all currently elible Destinations for the specified OriginID/ProductID values
-- =============================================
ALTER FUNCTION [dbo].[fnRetrieveEligibleDestinations](@originID int, @productID int, @requireRoute bit = 0) RETURNS TABLE AS RETURN
	SELECT D.*
	FROM dbo.viewDestination D
	WHERE DeleteDateUTC IS NULL 
		AND ((@requireRoute = 0 OR ID IN (SELECT DestinationID FROM viewRoute WHERE OriginID = @OriginID)) 
			AND ID IN (SELECT DestinationID FROM tblDestinationProducts WHERE ProductID=@ProductID) 
			AND (@OriginID = 0 OR ID IN (SELECT DestinationID FROM viewCustomerOriginDestination DC WHERE OriginID=@OriginID)))

GO

UPDATE tblOrigin SET GeoFenceRadiusMeters = 100
UPDATE tblDestination SET GeoFenceRadiusMeters = 100
GO

COMMIT
SET NOEXEC OFF
GO

EXEC _spRefreshAllViews
