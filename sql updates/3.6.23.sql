-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.6.22'
SELECT  @NewVersion = '3.6.23'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'Settlement: Rejected orders should use 0 as "Min Settlement Units" volume for rating'
	UNION SELECT @NewVersion, 1, 'Settlement: replace Origin Region filter with Destination State filter'
	UNION SELECT @NewVersion, 1, 'Settlement: Fuel Surcharge should always be $0.00 for Rejected Orders (Origins)'
	UNION SELECT @NewVersion, 1, 'Order Rules: add "Destination GOV tolerance % (from Origin GOV)" order rule (for Driver App entry)'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Carrier "Settlement" $$ values to an Delivered/Audited order
/***********************************/
ALTER PROCEDURE [dbo].[spApplyRatesCarrier]
(
  @ID int
, @UserName varchar(100)
, @OriginWaitAmount money = NULL
, @DestWaitAmount money = NULL
, @RejectionAmount money = NULL
, @FuelSurchargeAmount money = NULL
, @LoadAmount money = NULL
, @AssessorialRateTypeID_CSV varchar(max) = NULL
, @AssessorialAmount_CSV varchar(max) = null
, @ResetOverrides bit = 0
) AS BEGIN

	SET NOCOUNT ON
	
	-- ensure this order hasn't yet been fully settled
	IF EXISTS (SELECT * FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND BatchID IS NOT NULL)
	BEGIN
		RAISERROR('Invoice has already been fully settled', 16, 1)
		RETURN
	END

	-- ensure that Shipper Rates have been applied prior to applying Carrier rates (since they could be dependent on Shipper rates)
	IF NOT EXISTS (SELECT * FROM tblOrderSettlementShipper WHERE OrderID = @ID)
		EXEC spApplyRatesShipper @ID, @UserName

	/*
drop table #i
drop table #ia

declare @id int, @userName varchar(100)
, @OriginWaitAmount money 
, @DestWaitAmount money 
, @RejectionAmount money 
, @FuelSurchargeAmount money 
, @LoadAmount money 
, @AssessorialRateTypeID_CSV varchar(max) 
, @AssessorialAmount_CSV varchar(max) 
, @ResetOverrides bit 
select @ID=62210,@UserName='kalons'
--select @OriginWaitAmount=1.0000,@DestWaitAmount=2.0000,@RejectionAmount=3.0000,@FuelSurchargeAmount=4.0000,@LoadAmount=5.0000,@AssessorialRateTypeID_CSV='1',@AssessorialAmount_CSV='99'
select @ResetOverrides=0
--	*/
	-- get the provided manual Assessorial Rates (if any) into a usable able
	DECLARE @ManualAssessorialRates TABLE (ID int primary key, Amount money)
	INSERT INTO @ManualAssessorialRates (ID, Amount) 
		SELECT ID.ID, R.Amount
		FROM (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), ID FROM dbo.fnSplitCSVIDs(@AssessorialRateTypeID_CSV)) ID
		JOIN (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), amount = CAST(value as money) FROM dbo.fnSplitCSV(@AssessorialAmount_CSV)) R ON R.sortid = ID.sortid

	-- add in an existing manual Assessorial Rates from the existing Order Settlement (if any)
	INSERT INTO @ManualAssessorialRates (ID, Amount)
		SELECT AssessorialRateTypeID, Amount 
		FROM dbo.tblOrderSettlementCarrierAssessorialCharge 
		WHERE OrderID = @ID AND AssessorialRateID IS NULL 
			AND ID NOT IN (SELECT ID FROM @ManualAssessorialRates)
			AND @ResetOverrides = 0
--select * from @ManualAssessorialRates

	IF (@ResetOverrides = 0)
	BEGIN 
		SELECT @OriginWaitAmount = OriginWaitAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @OriginWaitAmount IS NULL AND OriginWaitRateID IS NULL
		SELECT @DestWaitAmount = DestinationWaitAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @DestWaitAmount IS NULL AND DestinationWaitRateID IS NULL
		SELECT @RejectionAmount = OrderRejectAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @RejectionAmount IS NULL AND OrderRejectRateID IS NULL
		SELECT @FuelSurchargeAmount = FuelSurchargeAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @FuelSurchargeAmount IS NULL AND FuelSurchargeRateID IS NULL
		SELECT @LoadAmount = LoadAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @LoadAmount IS NULL AND RouteRateID IS NULL AND RangeRateID IS NULL
	END
--select OverrideOriginWaitAmount = @OriginWaitAmount, OverrideDestWaitAmount = @DestWaitAmount, OverrideRejectionAmount = @RejectionAmount, OverrideFuelSurchargeAmount = @FuelSurchargeAmount, OverrideLoadAmount = @LoadAmount

	DECLARE @SettlementUnits TABLE
	(
		OrderID int
	  , SettlementUomID int
	  , SettlementFactorID int
	  , MinSettlementUnits decimal(18, 10)
	  , ActualUnits decimal(18, 10)
	  , SettlementUnits decimal(18, 10)
	)
	INSERT INTO @SettlementUnits (OrderID, SettlementUomID, SettlementFactorID, MinSettlementUnits, ActualUnits, SettlementUnits)
		SELECT OrderID, SettlementUomID, SettlementFactorID, MinSettlementUnits, ActualUnits, SettlementUnits
		FROM dbo.viewOrderSettlementUnitsCarrier 
		WHERE OrderID = @ID

	SELECT OrderID = @ID
		, OrderDate
		, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
		, RouteRateID, RangeRateID, LoadAmount
		, WaitFeeParameterID
		, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount 
		, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount
		, OrderRejectRateID, OrderRejectAmount
		, FuelSurchargeRateID, FuelSurchargeRate, FuelSurchargeAmount
		, OriginTaxRate
		, TotalAmount = ISNULL(LoadAmount, 0) 
			+ ISNULL(OriginWaitAmount, 0) 
			+ ISNULL(DestWaitAmount, 0) 
			+ ISNULL(OrderRejectAmount, 0) 
			+ ISNULL(FuelSurchargeAmount, 0)
		, CreatedByUser = @UserName
	INTO #I
	FROM (
		SELECT OrderDate
			, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
			, RouteRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RouteRateID ELSE NULL END
			, RangeRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RangeRateID ELSE NULL END
			, LoadAmount = round(ISNULL(@LoadAmount, RR.Amount), 2)
			, WaitFeeParameterID
			, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount = ROUND(OriginWaitAmount, 2)
			, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount = ROUND(DestWaitAmount, 2)
			, OrderRejectRateID = CASE WHEN @RejectionAmount IS NULL THEN RD.RateID ELSE NULL END
			, OrderRejectAmount = round(isnull(@RejectionAmount, RD.Amount), 2)
			, FuelSurchargeRateID = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.RateID ELSE NULL END
			, FuelSurchargeRate = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.Rate ELSE NULL END
			, FuelSurchargeAmount = round(isnull(@FuelSurchargeAmount, FSR.Amount), 2)
			, OriginTaxRate
		FROM (
			SELECT O.ID
				, O.OriginID
				, O.CarrierID
				, O.RouteID
				, SU.SettlementFactorID
				, SU.SettlementUomID
				, SU.ActualUnits
				, SU.MinSettlementUnits
				, SettlementUnits = CASE WHEN O.Rejected = 1 THEN 0 ELSE SU.SettlementUnits END
				, RouteMiles = O.ActualMiles
				, OrderDate = isnull(O.OrderDate, O.DueDate)
				, O.ChainUp
				, O.H2S
				, O.Rejected
				, O.RerouteCount
				, IsSplit = CASE WHEN O.Rejected = 0 AND O.TicketCount > 1 THEN 1 ELSE 0 END
				, WaitFeeParameterID = WFP.ID
				, OriginWaitRateID = CASE WHEN @OriginWaitAmount IS NULL THEN OWD.RateID ELSE NULL END
				, OriginWaitBillableMinutes = OWD.BillableMinutes
				, OriginWaitBillableHours = OWD.BillableHours
				, OriginWaitAmount = isnull(@OriginWaitAmount, OWD.Amount)
				, DestWaitRateID = CASE WHEN @DestWaitAmount IS NULL THEN DWD.RateID ELSE NULL END
				, DestWaitBillableMinutes = DWD.BillableMinutes
				, DestWaitBillableHours = DWD.BillableHours
				, DestWaitAmount = isnull(@DestWaitAmount, DWD.Amount)
				, OriginTaxRate = OO.TaxRate
			FROM dbo.viewOrder O
			JOIN @SettlementUnits SU ON SU.OrderID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			OUTER APPLY dbo.fnOrderCarrierOriginWaitData(@ID) OWD 
			OUTER APPLY dbo.fnOrderCarrierDestinationWaitData(@ID) DWD 
			OUTER APPLY dbo.fnOrderCarrierWaitFeeParameter(@ID) WFP
			WHERE O.ID = @ID
		) X
		OUTER APPLY dbo.fnOrderCarrierLoadAmount(@ID, SettlementUnits, SettlementUomID) RR 
		OUTER APPLY dbo.fnOrderCarrierOrderRejectData(@ID, SettlementUnits, SettlementUomID) RD
		OUTER APPLY dbo.fnOrderCarrierFuelSurchargeData(@ID) FSR
	) X2
--select * from #I
--select * from #i
	SELECT OrderID = @ID, AssessorialRateTypeID, AssessorialRateID = RateID, Amount = ROUND(Amount, 2), CreatedByUser = @UserName 
	INTO #IA
	FROM @SettlementUnits SU
	CROSS APPLY dbo.fnOrderCarrierAssessorialAmounts(@ID, SU.SettlementUnits, SU.SettlementUomID) CAA
	WHERE CAA.AssessorialRateTypeID NOT IN (SELECT ID FROM @ManualAssessorialRates)
/*
select OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser from #IA
union
select @ID, ID, NULL, Amount, @UserName from @ManualAssessorialRates
--*/
	DECLARE @CreatedTran bit; SET @CreatedTran = CASE WHEN @@TRANCOUNT = 0 THEN 1 ELSE 0 END
	BEGIN TRY
		IF (@CreatedTran = 1)
			BEGIN TRAN

		-- remove the existing settlment record (if any)
		DELETE FROM tblOrderSettlementCarrierAssessorialCharge WHERE OrderID = @ID
		DELETE FROM tblOrderSettlementCarrier WHERE OrderID = @ID
	
		-- all Units and Rates are first normalized to the Order.OriginUOM then consistent processing
		-- and persisted in the Invoice (Settlement) record in this Order.OriginUOM 
		INSERT INTO tblOrderSettlementCarrier (
			OrderID
			, OrderDate
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestinationWaitRateID 
			, DestinationWaitBillableMinutes 
			, DestinationWaitBillableHours
			, DestinationWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser)
		SELECT OrderID
			, OrderDate
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestWaitRateID 
			, DestWaitBillableMinutes 
			, DestWaitBillableHours
			, DestWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser
		FROM #I

		INSERT INTO tblOrderSettlementCarrierAssessorialCharge (OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser)
			SELECT OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser FROM #IA
			UNION
			SELECT @ID, ID, NULL, Amount, @UserName FROM @ManualAssessorialRates
	
		IF (@CreatedTran = 1)
			COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @msg varchar(max), @severity int
		SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY()
		ROLLBACK
		RAISERROR(@msg, @severity, 1)
		RETURN
	END CATCH	
	
END

GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Shipper "Settlement" $$ values to an Delivered/Audited order
/***********************************/
ALTER PROCEDURE [dbo].[spApplyRatesShipper]
(
  @ID int
, @UserName varchar(100)
, @OriginWaitAmount money = NULL
, @DestWaitAmount money = NULL
, @RejectionAmount money = NULL
, @FuelSurchargeAmount money = NULL
, @LoadAmount money = NULL
, @AssessorialRateTypeID_CSV varchar(max) = NULL
, @AssessorialAmount_CSV varchar(max) = null
, @ResetOverrides bit = 0
) AS BEGIN

	SET NOCOUNT ON
	
	-- ensure this order hasn't yet been fully settled
	IF EXISTS (SELECT * FROM tblOrderSettlementShipper WHERE OrderID = @ID AND BatchID IS NOT NULL)
	BEGIN
		RAISERROR('Invoice has already been fully settled', 16, 1)
		RETURN
	END

	-- get the provided manual Assessorial Rates (if any) into a usable able
	DECLARE @ManualAssessorialRates TABLE (ID int primary key, Amount money)
	INSERT INTO @ManualAssessorialRates (ID, Amount) 
		SELECT ID.ID, R.Amount
		FROM (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), ID FROM dbo.fnSplitCSVIDs(@AssessorialRateTypeID_CSV)) ID
		JOIN (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), amount = CAST(value as money) FROM dbo.fnSplitCSV(@AssessorialAmount_CSV)) R ON R.sortid = ID.sortid

	-- add in an existing manual Assessorial Rates from the existing Order Settlement (if any)
	INSERT INTO @ManualAssessorialRates (ID, Amount)
		SELECT AssessorialRateTypeID, Amount 
		FROM dbo.tblOrderSettlementShipperAssessorialCharge 
		WHERE OrderID = @ID AND AssessorialRateID IS NULL 
			AND ID NOT IN (SELECT ID FROM @ManualAssessorialRates)
			AND @ResetOverrides = 0

	IF (@ResetOverrides = 0)
	BEGIN 
		SELECT @OriginWaitAmount = OriginWaitAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @OriginWaitAmount IS NULL AND OriginWaitRateID IS NULL
		SELECT @DestWaitAmount = DestinationWaitAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @DestWaitAmount IS NULL AND DestinationWaitRateID IS NULL
		SELECT @RejectionAmount = OrderRejectAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @RejectionAmount IS NULL AND OrderRejectRateID IS NULL
		SELECT @FuelSurchargeAmount = FuelSurchargeAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @FuelSurchargeAmount IS NULL AND FuelSurchargeRateID IS NULL
		SELECT @LoadAmount = LoadAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @LoadAmount IS NULL AND RouteRateID IS NULL AND RangeRateID IS NULL
	END

	DECLARE @SettlementUnits TABLE
	(
		OrderID int
	  , SettlementUomID int
	  , SettlementFactorID int
	  , MinSettlementUnits decimal(18, 10)
	  , ActualUnits decimal(18, 10)
	  , SettlementUnits decimal(18, 10)
	)
	INSERT INTO @SettlementUnits (OrderID, SettlementUomID, SettlementFactorID, MinSettlementUnits, ActualUnits, SettlementUnits)
		SELECT OrderID, SettlementUomID, SettlementFactorID, MinSettlementUnits, ActualUnits, SettlementUnits
		FROM dbo.viewOrderSettlementUnitsShipper 
		WHERE OrderID = @ID

	SELECT OrderID = @ID
		, OrderDate
		, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
		, RouteRateID, RangeRateID, LoadAmount
		, WaitFeeParameterID
		, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount 
		, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount
		, OrderRejectRateID, OrderRejectAmount
		, FuelSurchargeRateID, FuelSurchargeRate, FuelSurchargeAmount
		, OriginTaxRate
		, TotalAmount = ISNULL(LoadAmount, 0) 
			+ ISNULL(OriginWaitAmount, 0) 
			+ ISNULL(DestWaitAmount, 0) 
			+ ISNULL(OrderRejectAmount, 0) 
			+ ISNULL(FuelSurchargeAmount, 0)
		, CreatedByUser = @UserName
	INTO #I
	FROM (
		SELECT OrderDate
			, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
			, RouteRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RouteRateID ELSE NULL END
			, RangeRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RangeRateID ELSE NULL END
			, LoadAmount = round(ISNULL(@LoadAmount, RR.Amount), 2)
			, WaitFeeParameterID
			, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount = ROUND(OriginWaitAmount, 2)
			, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount = ROUND(DestWaitAmount, 2)
			, OrderRejectRateID = CASE WHEN @RejectionAmount IS NULL THEN RD.RateID ELSE NULL END
			, OrderRejectAmount = round(isnull(@RejectionAmount, RD.Amount), 2)
			, FuelSurchargeRateID = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.RateID ELSE NULL END
			, FuelSurchargeRate = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.Rate ELSE NULL END
			, FuelSurchargeAmount = round(isnull(@FuelSurchargeAmount, FSR.Amount), 2)
			, OriginTaxRate
		FROM (
			SELECT O.ID
				, O.OriginID
				, O.CustomerID
				, O.RouteID
				, SU.SettlementFactorID
				, SU.SettlementUomID
				, SU.ActualUnits
				, SU.MinSettlementUnits
				, SettlementUnits = CASE WHEN O.Rejected = 1 THEN 0 ELSE SU.SettlementUnits END
				, RouteMiles = O.ActualMiles
				, OrderDate = isnull(O.OrderDate, O.DueDate)
				, O.ChainUp
				, O.H2S
				, O.Rejected
				, O.RerouteCount
				, IsSplit = CASE WHEN O.Rejected = 0 AND O.TicketCount > 1 THEN 1 ELSE 0 END
				, WaitFeeParameterID = WFP.ID
				, OriginWaitRateID = CASE WHEN @OriginWaitAmount IS NULL THEN OWD.RateID ELSE NULL END
				, OriginWaitBillableMinutes = OWD.BillableMinutes
				, OriginWaitBillableHours = OWD.BillableHours
				, OriginWaitAmount = isnull(@OriginWaitAmount, OWD.Amount)
				, DestWaitRateID = CASE WHEN @DestWaitAmount IS NULL THEN DWD.RateID ELSE NULL END
				, DestWaitBillableMinutes = DWD.BillableMinutes
				, DestWaitBillableHours = DWD.BillableHours
				, DestWaitAmount = isnull(@DestWaitAmount, DWD.Amount)
				, OriginTaxRate = OO.TaxRate
			FROM dbo.viewOrder O
			JOIN @SettlementUnits SU ON SU.OrderID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			OUTER APPLY dbo.fnOrderShipperOriginWaitData(@ID) OWD 
			OUTER APPLY dbo.fnOrderShipperDestinationWaitData(@ID) DWD 
			OUTER APPLY dbo.fnOrderShipperWaitFeeParameter(@ID) WFP
			WHERE O.ID = @ID
		) X
		OUTER APPLY dbo.fnOrderShipperLoadAmount(@ID, SettlementUnits, SettlementUomID) RR 
		OUTER APPLY dbo.fnOrderShipperOrderRejectData(@ID, SettlementUnits, SettlementUomID) RD
		OUTER APPLY dbo.fnOrderShipperFuelSurchargeData(@ID) FSR
	) X2

	SELECT OrderID = @ID, AssessorialRateTypeID, AssessorialRateID = RateID, Amount = ROUND(Amount, 2), CreatedByUser = @UserName 
	INTO #IA
	FROM @SettlementUnits SU
	CROSS APPLY dbo.fnOrderShipperAssessorialAmounts(@ID, SU.SettlementUnits, SU.SettlementUomID) CAA
	WHERE CAA.AssessorialRateTypeID NOT IN (SELECT ID FROM @ManualAssessorialRates)

	DECLARE @CreatedTran bit; SET @CreatedTran = CASE WHEN @@TRANCOUNT = 0 THEN 1 ELSE 0 END
	BEGIN TRY
		IF (@CreatedTran = 1)
			BEGIN TRAN

		-- remove the existing settlment record (if any)
		DELETE FROM tblOrderSettlementShipperAssessorialCharge WHERE OrderID = @ID
		DELETE FROM tblOrderSettlementShipper WHERE OrderID = @ID
	
		-- all Units and Rates are first normalized to the Order.OriginUOM then consistent processing
		-- and persisted in the Invoice (Settlement) record in this Order.OriginUOM 
		INSERT INTO tblOrderSettlementShipper (
			OrderID
			, OrderDate
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestinationWaitRateID 
			, DestinationWaitBillableMinutes 
			, DestinationWaitBillableHours
			, DestinationWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser)
		SELECT OrderID
			, OrderDate
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestWaitRateID 
			, DestWaitBillableMinutes 
			, DestWaitBillableHours
			, DestWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser
		FROM #I

		INSERT INTO tblOrderSettlementShipperAssessorialCharge (OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser)
			SELECT OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser FROM #IA
			UNION
			SELECT @ID, ID, NULL, Amount, @UserName FROM @ManualAssessorialRates
	
		IF (@CreatedTran = 1)
			COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @msg varchar(max), @severity int
		SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY()
		ROLLBACK
		RAISERROR(@msg, @severity, 1)
		RETURN
	END CATCH	
	
END

GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
/***********************************/
ALTER PROCEDURE [dbo].[spRetrieveOrdersFinancialCarrier]
(
  @StartDate datetime = NULL
, @EndDate datetime = NULL
, @ShipperID int = -1 -- all
, @CarrierID int = -1 -- all
, @ProductGroupID int = -1 -- all 
, @ProducerID int = -1 -- all
, @OriginStateID int = -1 -- all 
, @DestStateID int = -1 -- all
, @BatchID int = NULL  -- either show unbatched (NULL), or the specified batch orders
, @OnlyShipperSettled bit = 0 
) AS BEGIN

	SELECT @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	
	SELECT DISTINCT OE.* 
		, ShipperSettled = cast(CASE WHEN OSS.BatchID IS NOT NULL THEN 1 ELSE 0 END as bit)
		, InvoiceOtherDetailsTSV = dbo.fnOrderCarrierAssessorialDetailsTSV(OE.ID, 0)
		, InvoiceOtherAmountsTSV = dbo.fnOrderCarrierAssessorialAmountsTSV(OE.ID, 0)
	FROM dbo.viewOrder_Financial_Carrier OE
	LEFT JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = OE.ID AND OSS.BatchID IS NOT NULL
	WHERE ID IN (
		SELECT O.ID
		FROM viewOrderBase O
		JOIN tblProduct P ON P.ID = O.ProductID
		JOIN tblOrigin OO ON OO.ID = O.OriginID
		JOIN tblDestination D ON D.ID = O.DestinationID
		LEFT JOIN tblOrderSettlementCarrier OSC ON OSC.OrderID = O.ID
		WHERE O.StatusID IN (4)  
		  AND (@ShipperID=-1 OR O.CustomerID=@ShipperID) 
		  AND (@CarrierID=-1 OR O.CarrierID=@CarrierID) 
		  AND (@ProductGroupID=-1 OR P.ProductGroupID=@ProductGroupID) 
		  AND (@ProducerID=-1 OR O.ProducerID=@ProducerID) 
		  AND (@OriginStateID=-1 OR OO.StateID=@OriginStateID) 
		  AND (@DestStateID=-1 OR D.StateID=@DestStateID) 
		  AND (@StartDate IS NULL OR O.OrderDate >= @StartDate) 
		  AND (@EndDate IS NULL OR O.OrderDate <= @EndDate)
		  AND ((@BatchID IS NULL AND OSC.BatchID IS NULL) OR OSC.BatchID = @BatchID)
	)
	  AND (@OnlyShipperSettled = 0 OR OSS.BatchID IS NOT NULL)
	ORDER BY OE.OriginDepartTimeUTC
END
GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
/***********************************/
ALTER PROCEDURE [dbo].[spRetrieveOrdersFinancialShipper]
(
  @StartDate datetime = NULL
, @EndDate datetime = NULL
, @ShipperID int = -1 -- all customers
, @OriginShipperRegion varchar(50) = NULL -- all origin.ShipperRegions
, @ProductGroupID int = -1 -- all product groups
, @ProducerID int = -1 -- all
, @OriginStateID int = -1 -- all 
, @DestStateID int = -1 -- all
, @BatchID int = NULL  -- either show unbatched (NULL), or the specified batch orders
) AS BEGIN

	SELECT @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	
	SELECT DISTINCT OE.* 
		, InvoiceOtherDetailsTSV = dbo.fnOrderShipperAssessorialDetailsTSV(OE.ID, 0)
		, InvoiceOtherAmountsTSV = dbo.fnOrderShipperAssessorialAmountsTSV(OE.ID, 0)
	FROM viewOrder_Financial_Shipper OE
	WHERE ID IN (
		SELECT O.ID
		FROM viewOrderBase O
		JOIN tblProduct P ON P.ID = O.ProductID
		JOIN tblOrigin OO ON OO.ID = O.OriginID
		JOIN tblDestination D ON D.ID = O.DestinationID
		LEFT JOIN tblOrderSettlementShipper ISC ON ISC.OrderID = O.ID
		WHERE O.StatusID IN (4)  
		  AND (@ShipperID=-1 OR O.CustomerID=@ShipperID) 
		  AND (@OriginShipperRegion IS NULL OR OriginShipperRegion = @OriginShipperRegion)
		  AND (@ProductGroupID=-1 OR P.ProductGroupID=@ProductGroupID) 
		  AND (@ProducerID=-1 OR O.ProducerID=@ProducerID) 
		  AND (@OriginStateID=-1 OR OO.StateID=@OriginStateID) 
		  AND (@DestStateID=-1 OR D.StateID=@DestStateID) 
		  AND (@StartDate IS NULL OR O.OrderDate >= @StartDate) 
		  AND (@EndDate IS NULL OR O.OrderDate <= @EndDate)
		  AND ((@BatchID IS NULL AND ISC.BatchID IS NULL) OR ISC.BatchID = @BatchID)
	)
	ORDER BY OE.OriginDepartTimeUTC
END 
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier FuelSurcharge info for the specified order
/***********************************/
ALTER FUNCTION [dbo].[fnOrderCarrierFuelSurchargeRate](@ID int) RETURNS TABLE AS RETURN
	SELECT RateID
		, Rate = IncrementAmount * round(Pricediff / nullif(IntervalAmount, 0.0) + .49, 0)
		, RouteMiles
	FROM (
		SELECT TOP 1 RateID = ID, FuelPriceFloor, IntervalAmount, IncrementAmount
			, PriceDiff = dbo.fnMaxDecimal(0, dbo.fnFuelPriceForOrigin(OriginID, OrderDate) - FuelPriceFloor)
			, RouteMiles
		FROM (
			SELECT R.ID, R.FuelPriceFloor, R.IntervalAmount, R.IncrementAmount, O.OrderDate, O.OriginID, RouteMiles = CASE WHEN O.Rejected = 1 THEN 0 ELSE O.ActualMiles END
			FROM dbo.viewOrder O
			CROSS APPLY dbo.fnCarrierFuelSurchargeRate(O.OrderDate, null, O.CustomerID, O.CarrierID, O.ProductGroupID, 1) R
			WHERE O.ID = @ID 
		) X
	) X2
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper FuelSurcharge info for the specified order
/***********************************/
ALTER FUNCTION [dbo].[fnOrderShipperFuelSurchargeRate](@ID int) RETURNS TABLE AS RETURN
	SELECT RateID
		, Rate = IncrementAmount * round(Pricediff / nullif(IntervalAmount, 0.0) + .49, 0)
		, RouteMiles
	FROM (
		SELECT TOP 1 RateID = ID, FuelPriceFloor, IntervalAmount, IncrementAmount
			, PriceDiff = dbo.fnMaxDecimal(0, dbo.fnFuelPriceForOrigin(OriginID, OrderDate) - fuelPriceFloor)
			, RouteMiles
		FROM (
			SELECT R.ID, R.FuelPriceFloor, R.IntervalAmount, R.IncrementAmount, O.OrderDate, O.OriginID, RouteMiles = CASE WHEN O.Rejected = 1 THEN 0 ELSE O.ActualMiles END
			FROM dbo.viewOrder O
			CROSS APPLY dbo.fnShipperFuelSurchargeRate(O.OrderDate, null, O.CustomerID, O.ProductGroupID, 1) R
			WHERE O.ID = @ID 
		) X
	) X2
GO

EXEC _spRebuildAllObjects
GO

INSERT INTO tblOrderRuleType (ID, Name, RuleTypeID) VALUES (3, 'Destination GOV tolerance % (from Origin GOV)', 3)

COMMIT
SET NOEXEC OFF