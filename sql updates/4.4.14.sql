SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.4.13.1'
SELECT  @NewVersion = '4.4.14'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'JT-246 - Split tblDriver into separate tables for driver compliance documents and actual driver related information'	
	UNION
	SELECT @NewVersion, 0, 'JT-246 - Convert Driver maintenance page to MVC'
	UNION
	SELECT @NewVersion, 0, 'JT-246 - Convert Driver compliance pages to MVC'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


-- ----------------------------------------------------------------
-- Create/Update permissions
-- ----------------------------------------------------------------

/* Update existing driver maintenance page view permission to fit with new permissions being added below */
UPDATE aspnet_Roles
SET FriendlyName = 'Drivers - View'
, Category = 'Assets - Resources - Drivers'
, Description = 'Allow user to view the Drivers maintenance page'
WHERE RoleName = 'viewDriverMaintenance'
GO

-- Add new driver permissions
EXEC spAddNewPermission 'createDriverMaintenance', 'Allow user to create Drivers', 'Assets - Resources - Drivers', 'Drivers - Create'
GO
EXEC spAddNewPermission 'editDriverMaintenance', 'Allow user to edit Drivers', 'Assets - Resources - Drivers', 'Drivers - Edit'
GO
EXEC spAddNewPermission 'deactivateDriverMaintenance', 'Allow user to deactivate Assets - Resources - Drivers', 'Drivers', 'Drivers - Deactivate'
GO
-- Add new driver permissions to any groups that already had the "view" permission 
EXEC spAddNewPermissionToGroupsWithExistingRole 'viewDriverMaintenance', 'createDriverMaintenance'
GO
EXEC spAddNewPermissionToGroupsWithExistingRole 'viewDriverMaintenance', 'editDriverMaintenance'
GO
EXEC spAddNewPermissionToGroupsWithExistingRole 'viewDriverMaintenance', 'deactivateDriverMaintenance'
GO



-- Update existing driver compliance page view permission to fit with new permissions being added below 
UPDATE aspnet_Roles
SET FriendlyName = 'Compliance Docs - View'
, RoleName = 'viewDriverCompliance'
, LoweredRoleName = 'viewdrivercompliance'
, Category = 'Assets - Compliance - Drivers'
, Description = 'Allow user to view the Driver Compliance pages'
WHERE RoleName = 'viewComplianceDriverDocuments'
GO

-- Add new driver compliance permissions
EXEC spAddNewPermission 'createDriverCompliance', 'Allow user to create Driver Compliance Documents', 'Assets - Compliance - Drivers', 'Compliance Docs - Create'
GO
EXEC spAddNewPermission 'editDriverCompliance', 'Allow user to edit Driver Compliance Documents', 'Assets - Compliance - Drivers', 'Compliance Docs - Edit'
GO
EXEC spAddNewPermission 'deactivateDriverCompliance', 'Allow user to deactivate Driver Compliance Documents', 'Assets - Compliance - Drivers', 'Compliance Docs - Deactivate'
GO
-- Add new driver compliance permissions to any groups that already had the "view" permission 
EXEC spAddNewPermissionToGroupsWithExistingRole 'viewDriverCompliance', 'createDriverCompliance'
GO
EXEC spAddNewPermissionToGroupsWithExistingRole 'viewDriverCompliance', 'editDriverCompliance'
GO
EXEC spAddNewPermissionToGroupsWithExistingRole 'viewDriverCompliance', 'deactivateDriverCompliance'
GO

-- Add new Driver compliance type permissions ("Types" permissions do not get added to users by default when they are new permissions.
-- They will be added on request when needed by a user.)
EXEC spAddNewPermission 'viewDriverComplianceTypes', 'Allow user to view Driver Compliance Types', 'Assets - Compliance - Drivers', 'Compliance Types - View'
GO
EXEC spAddNewPermission 'createDriverComplianceTypes', 'Allow user to create Driver Compliance Types', 'Assets - Compliance - Drivers', 'Compliance Types - Create'
GO
EXEC spAddNewPermission 'editDriverComplianceTypes', 'Allow user to edit Driver Compliance Types', 'Assets - Compliance - Drivers', 'Compliance Types - Edit'
GO
EXEC spAddNewPermission 'deactivateDriverComplianceTypes', 'Allow user to deactivate Driver Compliance Types', 'Assets - Compliance - Drivers', 'Compliance Types - Deactivate'
GO


-- ----------------------------------------------------------------
-- Create new Driver Compliance tables
-- ----------------------------------------------------------------
CREATE TABLE tblDriverComplianceCategory
(
	ID INT IDENTITY(1, 1) NOT NULL CONSTRAINT PK_DriverComplianceCategory PRIMARY KEY CLUSTERED,
	Name VARCHAR(50) NOT NULL
)

SET IDENTITY_INSERT tblDriverComplianceCategory ON
INSERT INTO tblDriverComplianceCategory (ID, Name)
VALUES
(1, 'Initial'),
(2, 'Licenses'),
(3, 'Medical')
SET IDENTITY_INSERT tblDriverComplianceCategory OFF

GO


CREATE TABLE tblDriverComplianceType
(
	ID INT IDENTITY(1000,1) NOT NULL CONSTRAINT PK_DriverComplianceType PRIMARY KEY CLUSTERED,
	Name VARCHAR(100) NOT NULL,
	CategoryID INT NOT NULL CONSTRAINT FK_DriverComplianceType_Category REFERENCES tblDriverComplianceCategory(ID),
	IsSystem BIT NOT NULL CONSTRAINT FK_DriverComplianceType_IsSystem DEFAULT 0,
	IsRequired BIT NOT NULL CONSTRAINT DF_DriverComplianceType_IsRequired DEFAULT 0,
	RequiresDocument BIT NOT NULL CONSTRAINT DF_DriverComplianceType_RequiresDocument DEFAULT 0,
	ExpirationLength INT NULL,
	CreateDateUTC datetime NOT NULL CONSTRAINT DF_DriverComplianceType_CreateDateUTC DEFAULT GETUTCDATE(),
	CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_DriverComplianceType_CreatedByUser DEFAULT SUSER_SNAME(),
	LastChangeDateUTC datetime NULL,
	LastChangedByUser varchar(100) NULL,
	DeleteDateUTC datetime NULL,
	DeletedByUser varchar(100) NULL
)

go

SET IDENTITY_INSERT tblDriverComplianceType ON
INSERT INTO tblDriverComplianceType (ID, Name, CategoryID, IsSystem, IsRequired, RequiresDocument, ExpirationLength, CreatedByUser)
VALUES
(1, 'Driver''s License', 2, 0, 1, 1, 365, 'System'),
(2, 'Commercial Driver''s License (CDL)', 2, 0, 1, 1, 365, 'System'),
(3, 'Drug Screen Background Check', 1, 0, 1, 1, 365, 'System'),
(4, 'Motor Vehicle Report', 1, 0, 1, 0, 365, 'System'),
(5, 'Medical Card', 3, 0, 1, 0, 1, 'System'),
(6, 'Long Form Physical', 3, 0, 1, 0, 365, 'System'),
(7, 'Employment Application', 1, 0, 0, 1, null, 'System'),
(8, 'Policy Manual Receipt', 1, 0, 0, 0, null, 'System'),
(9, 'Training Receipt', 1, 0, 0, 0, null, 'System'),
(10, 'Orentation Test', 1, 0, 1, 0, null, 'System'),
(11, 'Drug Screen Background Release', 1, 0, 0, 1, null, 'System'),
(12, 'H2S', 2, 1, 0, 0, 365, 'System')

--H2S, 1, 1
--Endorsements/Restrictions?
SET IDENTITY_INSERT tblDriverComplianceType OFF

GO


CREATE TABLE tblDriverCompliance 
(
	ID int IDENTITY(1000,1) NOT NULL,
	DriverID INT NOT NULL CONSTRAINT FK_DriverCompliance_Driver REFERENCES tblDriver(ID),
	ComplianceTypeID INT NOT NULL CONSTRAINT FK_DriverCompliance_DocumentType REFERENCES tblDriverComplianceType(ID),
	Document VARBINARY(MAX) NULL,
	DocumentName VARCHAR(255) NULL,
	DocumentDate SMALLDATETIME NULL CONSTRAINT DF_DriverCompliance_DocumentDate DEFAULT GETDATE(),
	ExpirationDate SMALLDATETIME NULL,
	Notes NVARCHAR(MAX) NULL,
	CreateDateUTC datetime NOT NULL CONSTRAINT DF_DriverCompliance_CreateDateUTC DEFAULT GETUTCDATE(),
	CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_DriverCompliance_CreatedByUser DEFAULT SUSER_SNAME(),
	LastChangeDateUTC datetime NULL,
	LastChangedByUser varchar(100) NULL,
	DeleteDateUTC datetime NULL,
	DeletedByUser varchar(100) NULL
)

GO

/*************************************/
-- Date Created: 20 Dec 2016 - 4.4.14
-- Author: Joe Engler
-- Purpose: ensure only one active compliance record exists
/*************************************/
CREATE TRIGGER trigDriverCompliance_IU ON tblDriverCompliance AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	UPDATE tblDriverCompliance
	SET DeleteDateUTC = ISNULL(i.LastChangeDateUTC, i.CreateDateUTC),
	    DeletedByUser = ISNULL(i.LastChangedByUser, i.CreatedByUser)
	FROM tblDriverCompliance c
	LEFT JOIN inserted i ON i.DriverID = c.DriverID AND i.ComplianceTypeID = c.ComplianceTypeID
	WHERE i.ID IS NOT NULL AND i.ID <> c.ID
	AND c.DeleteDateUTC IS NULL
END

GO

/*************************************
 Date Created: 20 Dec 2016 - 4.4.14
 Author: Joe Engler
 Purpose: return driver compliance with some helper logic (missing, expired, etc)
 Changes:
*************************************/
CREATE VIEW viewDriverCompliance AS
SELECT c.ID, 
	c.DriverID, 
	d.FullName,
	d.CarrierID,
	d.Carrier,
	c.ComplianceTypeID, 
	ComplianceType = ct.Name,
	c.DocumentName,
	c.DocumentDate,
	c.ExpirationDate,
	c.Notes,
	c.CreateDateUTC,
	c.CreatedByuser,
	c.LastChangeDateUTC,
	c.LastChangedByUser,
	c.DeleteDateUTC,
	c.DeletedByUser,
	MissingDocument = CAST(CASE WHEN Document IS NULL AND ct.RequiresDocument = 1 THEN 1 ELSE 0 END AS BIT),
	ExpiredNow = CAST(CASE WHEN ExpirationDate < CAST(GETDATE() AS DATE) THEN 1 ELSE 0 END AS BIT),
	ExpiredNext30 = CAST(CASE WHEN ExpirationDate < CAST(DATEADD(MONTH, 1, GETDATE()) AS DATE) AND ExpirationDate >= CAST(GETDATE() AS DATE) THEN 1 ELSE 0 END AS BIT),
	ExpiredNext60 = CAST(CASE WHEN ExpirationDate < CAST(DATEADD(MONTH, 2, GETDATE()) AS DATE) AND ExpirationDate >= CAST(DATEADD(MONTH, 1, GETDATE()) AS DATE) THEN 1 ELSE 0 END AS BIT),
	ExpiredNext90 = CAST(CASE WHEN ExpirationDate < CAST(DATEADD(MONTH, 3, GETDATE()) AS DATE) AND ExpirationDate >= CAST(DATEADD(MONTH, 2, GETDATE()) AS DATE) THEN 1 ELSE 0 END AS BIT)
 FROM tblDriverCompliance c
 JOIN tblDriverComplianceType ct ON c.ComplianceTypeID = ct.ID
 JOIN viewDriverBase d ON c.DriverID = d.ID
GO


-- ----------------------------------------------------------------
-- Copy existing data into new compliance table
-- ----------------------------------------------------------------
INSERT INTO tblDriverCompliance (DriverID, ComplianceTypeID, Document, DocumentName, DocumentDate)
SELECT ID, 1, DLDocument, DLDocName, DLExpiration FROM tblDriver 
WHERE DLDocName IS NOT NULL

--	DLNumber varchar(20) NULL,?
GO

INSERT INTO tblDriverCompliance (DriverID, ComplianceTypeID, Document, DocumentName, DocumentDate)
SELECT ID, 2, CDLDocument, CDLDocName, CDLExpiration FROM tblDriver 
WHERE CDLDocName IS NOT NULL

--	CDLNumber varchar(20) NULL,
GO

INSERT INTO tblDriverCompliance (DriverID, ComplianceTypeID, Document, DocumentName, DocumentDate)
SELECT ID, 3, DrugScreenBackgroundCheckDocument, DrugScreenBackgroundCheckDocName, NULL FROM tblDriver 
WHERE DrugScreenBackgroundCheckDocName IS NOT NULL

GO
	
INSERT INTO tblDriverCompliance (DriverID, ComplianceTypeID, Document, DocumentName, DocumentDate)
SELECT ID, 4, MotorVehicleReportDocument, MotorVehicleReportDocName, NULL FROM tblDriver 
WHERE MotorVehicleReportDocName IS NOT NULL

GO

INSERT INTO tblDriverCompliance (DriverID, ComplianceTypeID, Document, DocumentName, DocumentDate)
SELECT ID, 5, MedicalCardDocument, MedicalCardDocName, MedicalCardDate FROM tblDriver 
WHERE MedicalCardDocName IS NOT NULL

GO

INSERT INTO tblDriverCompliance (DriverID, ComplianceTypeID, Document, DocumentName, DocumentDate)
SELECT ID, 6, LongFormPhysicalDocument, LongFormPhysicalDocName, LongFormPhysicalDate FROM tblDriver 
WHERE LongFormPhysicalDocName IS NOT NULL

GO

INSERT INTO tblDriverCompliance (DriverID, ComplianceTypeID, Document, DocumentName, DocumentDate)
SELECT ID, 7, EmploymentAppDocument, EmploymentAppDocName, NULL FROM tblDriver 
WHERE EmploymentAppDocName IS NOT NULL

GO

INSERT INTO tblDriverCompliance (DriverID, ComplianceTypeID, Document, DocumentName, DocumentDate)
SELECT ID, 8, PolicyManualReceiptDocument, PolicyManualReceiptDocName, NULL FROM tblDriver
WHERE PolicyManualReceiptDocName IS NOT NULL

GO

INSERT INTO tblDriverCompliance (DriverID, ComplianceTypeID, Document, DocumentName, DocumentDate)
SELECT ID, 9, TrainingReceiptDocument, TrainingReceiptDocName, NULL FROM tblDriver
WHERE TrainingReceiptDocName IS NOT NULL

GO

INSERT INTO tblDriverCompliance (DriverID, ComplianceTypeID, Document, DocumentName, DocumentDate)
SELECT ID, 10, OrientationTestDocument, OrientationTestDocName, NULL FROM tblDriver
WHERE OrientationTestDocName IS NOT NULL

GO

INSERT INTO tblDriverCompliance (DriverID, ComplianceTypeID, Document, DocumentName, DocumentDate)
SELECT ID, 11, DrugScreenBackgroundReleaseDocument, DrugScreenBackgroundReleaseDocName, NULL FROM tblDriver 
WHERE DrugScreenBackgroundReleaseDocName IS NOT NULL

GO


-- ----------------------------------------------------------------
-- Purge old columns from Driver table
-- ----------------------------------------------------------------

ALTER TABLE tblDriver DROP COLUMN DLDocument
ALTER TABLE tblDriver DROP COLUMN DLDocName
ALTER TABLE tblDriver DROP COLUMN DLExpiration
--DLNumber

ALTER TABLE tblDriver DROP COLUMN CDLDocument
ALTER TABLE tblDriver DROP COLUMN CDLDocName
ALTER TABLE tblDriver DROP COLUMN CDLExpiration
--	CDLNumber varchar(20) NULL,

ALTER TABLE tblDriver DROP COLUMN DrugScreenBackgroundCheckDocument
ALTER TABLE tblDriver DROP COLUMN DrugScreenBackgroundCheckDocName

ALTER TABLE tblDriver DROP COLUMN MotorVehicleReportDocument
ALTER TABLE tblDriver DROP COLUMN MotorVehicleReportDocName

ALTER TABLE tblDriver DROP COLUMN MedicalCardDocument
ALTER TABLE tblDriver DROP COLUMN MedicalCardDocName
ALTER TABLE tblDriver DROP COLUMN MedicalCardDate

ALTER TABLE tblDriver DROP COLUMN LongFormPhysicalDocument
ALTER TABLE tblDriver DROP COLUMN LongFormPhysicalDocName
ALTER TABLE tblDriver DROP COLUMN LongFormPhysicalDate

ALTER TABLE tblDriver DROP COLUMN EmploymentAppDocument
ALTER TABLE tblDriver DROP COLUMN EmploymentAppDocName

ALTER TABLE tblDriver DROP COLUMN PolicyManualReceiptDocument
ALTER TABLE tblDriver DROP COLUMN PolicyManualReceiptDocName

ALTER TABLE tblDriver DROP COLUMN TrainingReceiptDocument
ALTER TABLE tblDriver DROP COLUMN TrainingReceiptDocName

ALTER TABLE tblDriver DROP COLUMN OrientationTestDocument
ALTER TABLE tblDriver DROP COLUMN OrientationTestDocName

ALTER TABLE tblDriver DROP COLUMN DrugScreenBackgroundReleaseDocument
ALTER TABLE tblDriver DROP COLUMN DrugScreenBackgroundReleaseDocName

--Add Notes and Termination date columns
ALTER TABLE tblDriver ADD Notes VARCHAR(MAX) NULL
ALTER TABLE tblDriver ADD TerminationDate SMALLDATETIME NULL

GO

-- ----------------------------------------------------------------
-- Update other Driver view/scripts
-- ----------------------------------------------------------------
go

/*****************************************************/
-- Date Created: 2015.01.14 - Kevin Alons
-- Author: Kevin Alons
-- Purpose: helper function to add 'Deleted: ' to "name" string based on DeleteDateUTC value
-- Changes: 
-- 4.0.5	- 8/24/16 - BB - DCWEB-1655: Changed "Deleted" to "Deactivated"
-- 4.4.14	- 2016.12.22 - KDA	- replace 'Deactivated' with [Deactivated]'
/*****************************************************/
ALTER FUNCTION fnNameWithDeleted(@name varchar(255), @DeleteDateUTC date) RETURNS varchar(255) AS
BEGIN
	SELECT @name = CASE WHEN @DeleteDateUTC IS NULL THEN '' ELSE '[DEACTIVATED] ' END + @name
	RETURN (@name) 
END
GO

/***********************************/
-- Date Created: 26 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Drivers table records with "friendly" translated values included
-- Changes: 
-- 3.10.7		- 2016/01/27 - BB	- Added TabletID and PrinterSerial from tblDriver_Sync
-- 3.11.15.2	- 2016/04/20 - BB	- Added DriverGroupName to fix filtering on web page
-- 4.4.14		- 2016/12/08 - BB	- Added Operating State, Added DriverShiftTypeName, Move UserNames from viewDriver to viewDriverBase
--							 - KDA	- use fnFnameWithDeleted() method so future changes can be done in a single place (leaving the views unchanged)
/***********************************/
ALTER VIEW viewDriverBase AS
SELECT X.*
	, FullNameD = dbo.fnNameWithDeleted(FullName, DeleteDateUTC) -- 4.4.14 - Updated from "Deleted" 12/15/16
FROM (
	SELECT D.*
		, Active = cast(CASE WHEN isnull(C.DeleteDateUTC, D.DeleteDateUTC) IS NULL THEN 1 ELSE 0 END as bit) 
		, FullName = D.FirstName + ' ' + D.LastName 
		, FullNameLF = D.LastName + ', ' + D.FirstName 
		, CarrierType = isnull(CT.Name, 'Unknown') 
		, Carrier = C.Name 
		, StateAbbrev = S.Abbreviation 
		, OperatingStateAbbrev = S2.Abbreviation
		, Truck = T.FullName
		, Trailer = T1.FullName 
		, Trailer2 = T2.FullName 
		, Region = R.Name 
		, DS.MobileAppVersion
		, DS.TabletID
		, DS.PrinterSerial
		, DriverGroupName = DG.Name
		, DriverShiftTypeName = DST.Name
		, DUN.UserNames
	FROM dbo.tblDriver D 
	LEFT JOIN dbo.fnDriverUserNames() DUN ON DUN.DriverID = D.ID
	JOIN dbo.tblCarrier C ON C.ID = D.CarrierID 
	LEFT JOIN dbo.tblDriver_Sync DS ON DS.DriverID = D.ID
	LEFT JOIN dbo.tblCarrierType CT ON CT.ID = C.CarrierTypeID
	LEFT JOIN tblState S ON S.ID = D.StateID
	LEFT JOIN tblState S2 ON S2.ID = D.OperatingStateID
	LEFT JOIN viewTruck T ON T.ID = D.TruckID
	LEFT JOIN viewTrailer T1 ON T1.ID = D.TrailerID
	LEFT JOIN viewTrailer T2 ON T2.ID = D.Trailer2ID
	LEFT JOIN tblRegion R ON R.ID = D.RegionID
	LEFT JOIN tblDriverGroup DG ON DG.ID = D.DriverGroupID
	LEFT JOIN tblDriverShiftType DST ON DST.ID = D.DriverShiftTypeID
) X
GO


/***********************************/
-- Date Created: 26 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Drivers table records with LastDelivereredOrderTime + computed assigned ASP.NET usernames
-- Changes
--	4.4.14 - JAE & BB - 2016-11-29 - Add previously included fields, Moved UserNames to viewDriverBase
/***********************************/
ALTER VIEW viewDriver AS
SELECT DB.*
	, LastDeliveredOrderTime = (SELECT MAX(DestDepartTimeUTC) FROM dbo.tblOrder WHERE DriverID = DB.ID) 
	, DLDocument = dl.Document
	, DLDocName = dl.DocumentName
	, DLExpiration = dl.DocumentDate
	, CDLDocument = cdl.Document
	, CDLDocName = cdl.DocumentName
	, CDLExpiration = cdl.DocumentDate
	, DrugScreenBackgroundCheckDocument = dsbc.Document
	, DrugScreenBackgroundCheckDocName = dsbc.DocumentName
	, MotorVehicleReportDocument = mvr.Document
	, MotorVehicleReportDocName = mvr.DocumentName
	, MedicalCardDocument = mc.Document
	, MedicalCardDocName = mc.DocumentName
	, MedicalCardDate = mc.DocumentDate
	, LongFormPhysicalDocument = lfp.Document
	, LongFormPhysicalDocName = lfp.DocumentName
	, LongFormPhysicalDate = lfp.DocumentDate
	, EmploymentAppDocument = ea.Document
	, EmploymentAppDocName = ea.DocumentName
	, PolicyManualReceiptDocument = pmr.Document
	, PolicyManualReceiptDocName = pmr.DocumentName
	, TrainingReceiptDocument = tr.Document
	, TrainingReceiptDocName = tr.DocumentName
	, OrientationTestDocument = ot.Document
	, OrientationTestDocName = ot.DocumentName
	, DrugScreenBackgroundReleaseDocument = dsbr.Document
	, DrugScreenBackgroundReleaseDocName = dsbr.DocumentName
FROM viewDriverBase DB
OUTER APPLY (select TOP 1 * FROM tblDriverCompliance WHERE ComplianceTypeID=1 AND DriverID = DB.ID ORDER BY DocumentDate DESC) dl
OUTER APPLY (select TOP 1 * FROM tblDriverCompliance WHERE ComplianceTypeID=2 AND DriverID = DB.ID ORDER BY DocumentDate DESC) cdl
OUTER APPLY (select TOP 1 * FROM tblDriverCompliance WHERE ComplianceTypeID=3 AND DriverID = DB.ID ORDER BY DocumentDate DESC) dsbc
OUTER APPLY (select TOP 1 * FROM tblDriverCompliance WHERE ComplianceTypeID=4 AND DriverID = DB.ID ORDER BY DocumentDate DESC) mvr
OUTER APPLY (select TOP 1 * FROM tblDriverCompliance WHERE ComplianceTypeID=5 AND DriverID = DB.ID ORDER BY DocumentDate DESC) mc
OUTER APPLY (select TOP 1 * FROM tblDriverCompliance WHERE ComplianceTypeID=6 AND DriverID = DB.ID ORDER BY DocumentDate DESC) lfp
OUTER APPLY (select TOP 1 * FROM tblDriverCompliance WHERE ComplianceTypeID=7 AND DriverID = DB.ID ORDER BY DocumentDate DESC) ea
OUTER APPLY (select TOP 1 * FROM tblDriverCompliance WHERE ComplianceTypeID=8 AND DriverID = DB.ID ORDER BY DocumentDate DESC) pmr
OUTER APPLY (select TOP 1 * FROM tblDriverCompliance WHERE ComplianceTypeID=9 AND DriverID = DB.ID ORDER BY DocumentDate DESC) tr
OUTER APPLY (select TOP 1 * FROM tblDriverCompliance WHERE ComplianceTypeID=10 AND DriverID = DB.ID ORDER BY DocumentDate DESC) ot
OUTER APPLY (select TOP 1 * FROM tblDriverCompliance WHERE ComplianceTypeID=11 AND DriverID = DB.ID ORDER BY DocumentDate DESC) dsbr
GO


/****************************************************/
-- Created: 2016.09.14 - 4.1.7
-- Author: Joe Engler
-- Purpose: Retrieve the drivers (without last known GPS coordinates) for the specified filter criteria
-- Changes:
--	4.1.8.3		JAE		2016-09-21		Fixed reference to current time to start time (passed in)
--	4.1.10.1	JAE		2016-09-27		Added qualifier to DeleteDateUTC column (since same column was added to route table)
--	4.2.0		JAE		2016-09-30		Add hos fields to view
--	4.2.5		JAE		2016-10-26		Use operating state (instead of state) to carrier rule call
--											Added Weekly OnDuty fields
--	4.3.2		KDA		2016.11.07		replace tblDriverAvailability references with viewDriverSchedule (tblDriverSchedule)
--	4.4.14		JAE		2016-12-22		Update compliance check to use new compliance tables			
/****************************************************/
ALTER FUNCTION fnRetrieveEligibleDrivers_NoGPS(@CarrierID INT, @RegionID INT, @StateID INT, @DriverGroupID INT, @StartDate DATETIME) 
RETURNS @ret TABLE 
(
	ID INT, 
	LastName VARCHAR(20), FirstName VARCHAR(20), FullName VARCHAR(41), FullNameLF VARCHAR(42),
	CarrierID INT, Carrier VARCHAR(40),
	RegionID INT,
	TruckID INT, Truck VARCHAR(10),
	TrailerID INT, Trailer VARCHAR(10),
	AvailabilityFactor DECIMAL(4,2),
	ComplianceFactor DECIMAL(4,2),
	TruckAvailabilityFactor DECIMAL(4,2),
	CurrentWorkload INT,
	CurrentECOT INT,
	HOSFactor DECIMAL(4,2),
	OnDutyHoursSinceWeeklyReset DECIMAL(6,2),
	WeeklyOnDutyHoursLeft DECIMAL(6,2),
	DrivingHoursSinceWeeklyReset DECIMAL(6,2),
	WeeklyDrivingHoursLeft DECIMAL(6,2),
	OnDutyHoursSinceDailyReset DECIMAL(6,2),
	OnDutyHoursLeft DECIMAL(6,2),
	DrivingHoursSinceDailyReset DECIMAL(6,2),
	DrivingHoursLeft DECIMAL(6,2),
	HOSHrsOnShift DECIMAL(6,2),
	DriverScore INT 
)
AS BEGIN
	DECLARE @__ENFORCE_DRIVER_SCHEDULING__ INT = 13
	DECLARE @__ENFORCE_DRIVER_COMPLIANCE__ INT = 14
	DECLARE @__ENFORCE_HOS__ INT = 15
	
	IF @StartDate IS NULL 
	   SELECT @StartDate = dbo.fnDateOnly(GETDATE())

	INSERT INTO @ret
	SELECT X.*,
		DriverScore = 100 * AvailabilityFactor * ComplianceFactor * TruckAvailabilityFactor * HOSFactor
	FROM (
		SELECT d.ID, 
			d.FirstName, d.LastName, FullName, d.FullNameLF,
			d.CarrierID, d.Carrier,
			d.RegionID,
			d.TruckID, d.Truck,
			d.TrailerID, d.Trailer,

			--Availability
			AvailabilityFactor = CASE WHEN cr_a.Value IS NULL OR dbo.fnToBool(cr_a.Value) = 0 THEN 1 -- scheduling not enforced
										ELSE isnull(DS.OnDuty, 0) END,

			-- Compliance
			ComplianceFactor = CASE WHEN cr_c.Value IS NULL OR dbo.fnToBool(cr_c.Value) = 0 THEN 1 -- compliance not enforced
									WHEN (select count(*) from fnDriverComplianceSummary(0, 0) WHERE DriverID = d.ID) > 0 THEN 0
									ELSE 1 END, -- all ok

			--Truck/Trailer Availability
			TruckAvailabilityFactor = 1,
			
			--current workload, time on shift
			o.CurrentWorkload,
			CurrentECOT = ISNULL(o.CurrentECOT, 0),

			-- HOS
			HOSFactor = CASE WHEN cr_h.Value IS NULL OR dbo.fnToBool(cr_h.Value) = 0 THEN 1 -- HOS not enforced
							WHEN hos.WeeklyOnDutyHoursLeft <= 0 OR hos.OnDutyHoursLeft <= 0 OR hos.DrivingHoursLeft <= 0 THEN 0 
							-- compare ECOT to hours and see
							ELSE 1 END,
			hos.OnDutyHoursSinceWeeklyReset,
			hos.WeeklyOnDutyHoursLeft,
			hos.DrivingHoursSinceWeeklyReset,
			hos.WeeklyDrivingHoursLeft,
			hos.OnDutyHoursSinceDailyReset,
			hos.OnDutyHoursLeft,
			hos.DrivingHoursSinceDailyReset,
			hos.DrivingHoursLeft,
			HrsOnShift = CAST(DATEDIFF(MINUTE, ds.StartDateTime, getdate())/60.0*isnull(ds.OnDuty, 0) AS DECIMAL(5,1)) -- from DriverScheduling
			/* return the number of hours the Driver Scheduling module defines the driver is Scheduled on the specified day */
			--,ShiftHours = dbo.fnMinInt(DS.DurationHours, 24 - DS.StartHours % 24)-- from DriverScheduling

		FROM viewDriver d
		LEFT JOIN (SELECT * FROM viewDriverSchedule WHERE OnDuty = 1) ds ON ds.DriverID = d.ID AND ScheduleDate = @StartDate
		OUTER APPLY dbo.fnCarrierRules(@StartDate, null, @__ENFORCE_DRIVER_SCHEDULING__, d.CarrierID, d.ID, d.OperatingStateID, d.RegionID, 1) cr_a -- Enforce Driver Scheduling Carrier Rule
		OUTER APPLY dbo.fnCarrierRules(@StartDate, null, @__ENFORCE_DRIVER_COMPLIANCE__, d.CarrierID, d.ID, d.OperatingStateID, d.RegionID, 1) cr_c
		OUTER APPLY dbo.fnCarrierRules(@StartDate, null, @__ENFORCE_HOS__, d.CarrierID, d.ID, d.OperatingStateID, d.RegionID, 1) cr_h
		OUTER APPLY (SELECT TOP 1 * FROM dbo.fnHosViolationDetail(d.ID, null, @StartDate) ORDER BY LogDateUTC DESC) hos
		OUTER APPLY (
			SELECT CurrentWorkload = COUNT(*), 
					CurrentECOT = SUM(CASE WHEN o.StatusID = 8 THEN ISNULL(r.ECOT, 60+ISNULL(r.ActualMiles,60)+60)/2
										   ELSE COALESCE(r.ECOT, 60+ISNULL(r.ActualMiles,60)+60) END)
			FROM tblOrder o LEFT JOIN tblRoute r ON o.RouteID = r.ID
			WHERE o.DriverID = d.ID
			  AND o.StatusID IN (2,7,8) -- Dispatched, Accepted, Picked Up
			  AND o.DeleteDateUTC IS NULL) o

		WHERE d.DeleteDateUTC IS NULL
			AND d.TruckID IS NOT NULL AND d.TrailerID IS NOT NULL
			AND (ISNULL(@CarrierID, 0) <= 0 OR @CarrierID = d.CarrierID)
			AND (ISNULL(@DriverGroupID, 0) <= 0 OR @DriverGroupID = d.ID)
			AND (ISNULL(@RegionID, -1) <= 0 OR @RegionID = d.RegionID)
			AND (ISNULL(@StateID, 0) <= 0 OR @StateID = d.StateID)
	) AS X
	ORDER BY DriverScore DESC, LastName, FirstName

	RETURN
END

GO

/***********************************
-- Date Created: 2016-05-03
-- Author: Joe Engler
-- Purpose: retrieve and return the Carrier Rule info for the specified order
-- Changes:
--		4.4.14		2016/12/26		JAE		Require a carrier at a minimum
***********************************/
ALTER FUNCTION fnCarrierRules(@StartDate date, @EndDate date, @TypeID int, @CarrierID int, @DriverID int, @StateID int, @RegionID int, @BestMatchOnly bit = 0)
RETURNS 
	@ret TABLE (
		ID int
	  , TypeID int
	  , CarrierID int
	  , DriverID int
	  , StateID int
	  , RegionID int
	  , Value varchar(255)
	  , RuleTypeID int
	  , EffectiveDate date
	  , EndDate date
	  , MaxEffectiveDate date
	  , MinEndDate date
	  , NextEffectiveDate date
	  , PriorEndDate date
	  , BestMatch bit
	  , Ranking smallmoney
	  , CreateDateUTC datetime
	  , CreatedByUser varchar(100)
	  , LastChangeDateUTC datetime
	  , LastChangedByUser varchar(100)
	)
AS BEGIN
	-- get the raw data (all matched rows)
	DECLARE @src TABLE (ID int, TypeID int, Ranking smallmoney)
	INSERT INTO @src (ID, TypeID, Ranking)
		SELECT ID, TypeID
			, Ranking =	dbo.fnRateRanking(@CarrierID, R.CarrierID, 8, 0)
					  + dbo.fnRateRanking(@DriverID, R.DriverID, 4, 1)
					  + dbo.fnRateRanking(@StateID, R.StateID, 2, 1)
					  + dbo.fnRateRanking(@RegionID, R.RegionID, 1, 1)
		FROM dbo.viewCarrierRule R
		WHERE coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@DriverID, 0), R.DriverID, 0) = coalesce(R.DriverID, nullif(@DriverID, 0), 0)
		  AND coalesce(nullif(@StateID, 0), R.StateID, 0) = coalesce(R.StateID, nullif(@StateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(R.RegionID, nullif(@RegionID, 0), 0)
		  AND (nullif(@TypeID, 0) IS NULL OR TypeID = @TypeID)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)

	-- return the rate data for the best-match rates 
	INSERT INTO @ret (ID, TypeID, CarrierID, DriverID, StateID, RegionID, Value, RuleTypeID, EffectiveDate, EndDate
			, NextEffectiveDate, PriorEndDate, BestMatch, Ranking, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser)
		SELECT R.ID, TypeID, CarrierID, DriverID, StateID, RegionID, R.Value, RuleTypeID, EffectiveDate, EndDate
			, NextEffectiveDate, PriorEndDate, BestMatch, Ranking, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser
		FROM viewCarrierRule R
		JOIN (
			SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.TypeID IS NULL THEN 0 ELSE 1 END as bit)
			FROM @src S
			LEFT JOIN (
				SELECT TypeID, Ranking = MAX(Ranking)
				FROM @src
				WHERE @StartDate = ISNULL(@EndDate, @StartDate)
				  AND Ranking % 1 = 0.01 * 4 -- ensure a complete match is found for BESTMATCH
				GROUP BY TypeID
			) X ON X.TypeID = S.TypeID AND X.Ranking = S.Ranking
		) X ON X.ID = R.ID
		WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
		
	RETURN
END

GO

/***********************************/
-- Date Created: 2016-05-03
-- Author: Joe Engler
-- Purpose: retrieve and return the Carrier Rule rows for the specified criteria
-- Changes:
--		4.4.14		JAE			2016-11-30		Switched to viewDriverBase
/***********************************/
ALTER FUNCTION fnCarrierRulesDisplay(@StartDate date, @EndDate date, @TypeID int, @CarrierID int, @DriverID int, @StateID int, @RegionID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT 
		R.ID
		, R.CarrierID
		, R.DriverID
		, R.StateID
		, R.RegionID
		, R.TypeID
		, R.Value
		, R.RuleTypeID
		, R.EffectiveDate
		, R.EndDate
		, R.NextEffectiveDate
		, R.PriorEndDate
		, Type = CRT.Name
		, Carrier = C.Name
		, Driver = D.FullName
		, Region = D.Region
		, State = D.StateAbbrev
		, RuleType = RT.Name
		, R.CreateDateUTC
		, R.CreatedByUser
		, R.LastChangeDateUTC
		, R.LastChangedByUser
		, BestMatch
		, Ranking
	FROM dbo.fnCarrierRules(@StartDate, @EndDate, @TypeID, @CarrierID, @DriverID, @StateID, @RegionID, 0) R
	JOIN tblCarrierRuleType CRT ON CRT.ID = R.TypeID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN viewDriverBase D ON D.ID = R.DriverID
	LEFT JOIN tblRuleType RT ON RT.ID = R.RuleTypeID
	ORDER BY EffectiveDate

GO


/***********************************
Date Created: 3/31/2016
Author: Joe Engler
Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
			fields passed in are web filters, not best match criteria
Changes:
- 3.11.20.3 - 2016/05/11 - KDA	- optimize @BatchID filtering to query directly from tblOrderSettlementCarrier table
								- use tblOrder and minimal JOINs intead of expensive viewOrder
- 4.4.14		- 2016/11/30 - JAE	- Switched to viewDriverBase
***********************************/
ALTER PROCEDURE spRetrieveOrdersFinancialProducer
(
  @StartDate date = NULL -- will default to first day of 6th prior month for efficiency sake
, @EndDate date = NULL
, @ShipperID int = -1 -- all
, @CarrierID int = -1 -- all
, @ProductGroupID int = -1 -- all 
, @TruckTypeID int = -1 -- all
, @DriverGroupID int = -1 -- all
, @OriginStateID int = -1 -- all 
, @DestStateID int = -1 -- all
, @ProducerID int = -1 -- all
, @BatchID int = NULL  -- either show unbatched (NULL), or the specified batch orders
) AS 
BEGIN
	-- validate the incoming parameters
	IF (@BatchID IS NOT NULL
		AND (@ShipperID <> -1 
			OR @CarrierID <> -1 
			OR @ProductGroupID <> -1
			OR @TruckTypeID <> -1
			OR @DriverGroupID <> -1
			OR @OriginStateID <> -1
			OR @DestStateID <> -1
			OR @ProducerID <> -1))
	BEGIN
		RAISERROR('@BatchID parameter cannot be specified with other parameters', 16, 1)
		RETURN
	END

	-- table variable to store the Order.ID values to return
	DECLARE @IDs IDTABLE

	-- if a @BatchID value was specified then just use this criteria alone
    IF @BatchID IS NOT NULL
	BEGIN
		INSERT INTO @IDs (ID)
			SELECT OrderID FROM tblOrderSettlementProducer WHERE BatchID = @BatchID
	END
	ELSE
	BEGIN
		-- if no Start was specified, never return data earlier than 6 months ago
		IF (@StartDate IS NULL) SET @StartDate = dateadd(month, -6, dbo.fnFirstDOM(getdate()))
		-- retrieve the Order.ID values for the specified parameters
		INSERT INTO @IDs (ID)
			SELECT O.ID
			FROM tblOrder O
			LEFT JOIN dbo.tblOrderTransfer OTR ON OTR.OrderID = O.ID
			LEFT JOIN dbo.viewDriverBase vODR ON vODR.ID = OTR.OriginDriverID
			LEFT JOIN dbo.viewDriverBase vDDR ON vDDR.ID = O.DriverID
			LEFT JOIN tblOrderSettlementProducer OSP ON OSP.OrderID = O.ID
			JOIN tblProduct P ON P.ID = O.ProductID
			JOIN tblTruck T ON T.ID = ISNULL(OTR.OriginTruckID, O.TruckID)
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			JOIN tblDestination D ON D.ID = O.DestinationID
			WHERE O.StatusID IN (4)  
			  AND O.DeleteDateUTC IS NULL  -- 3.9.34
			  AND OSP.BatchID IS NULL -- only get order if it's not part of a batch
			  AND (@ShipperID=-1 OR O.CustomerID=@ShipperID) 
			  AND (@CarrierID=-1 OR O.CarrierID=@CarrierID) 
			  AND (@ProductGroupID=-1 OR P.ProductGroupID=@ProductGroupID) 
			  AND (@TruckTypeID=-1 OR T.TruckTypeID=@TruckTypeID)
			  AND (@DriverGroupID=-1 OR ISNULL(vODR.DriverGroupID, vDDR.DriverGroupID) = @DriverGroupID) 
			  AND (@OriginStateID=-1 OR OO.StateID=@OriginStateID) 
			  AND (@DestStateID=-1 OR D.StateID=@DestStateID) 
			  AND (@ProducerID=-1 OR O.ProducerID=@ProducerID) 
			  AND (@StartDate IS NULL OR O.OrderDate >= @StartDate) 
			  AND (@EndDate IS NULL OR O.OrderDate <= @EndDate)
	END

	SELECT DISTINCT OE.* 
		, Approved = cast(ISNULL(OA.Approved, 0) as bit)
	FROM dbo.viewOrder_Financial_Producer OE
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = OE.ID
	WHERE ID IN (SELECT ID FROM @IDs)
	ORDER BY OE.OriginDepartTimeUTC
END

GO


/****************************************************
 Date Created: 2016/02/24
 Author: Kevin Alons
 Purpose: return all currently eligible DriverApp OrderReadOnly records (for all Drivers) 
 Changes:
	-	3.13.13		2016/08/04		JAE		Add Sequence # for sorting orders
	-	4.4.14		2016/11/30		JAE		Switch to viewDriverBase
****************************************************/
ALTER VIEW dbo.viewOrderReadOnlyAllEligible_DriverApp AS
SELECT O.ID
	, O.OrderNum
	, O.StatusID
	, O.TicketTypeID
	, PriorityNum = CAST(P.PriorityNum AS INT) 
	, Product = PRO.Name
	, O.DueDate
	, Origin = OO.Name
	, OriginFull = OO.FullName
	, OO.OriginType
	, O.OriginUomID
	, OriginStation = OO.Station 
	, OriginLeaseNum = OO.LeaseNum 
	, OriginCounty = OO.County 
	, OriginLegalDescription = OO.LegalDescription 
	, OriginNDIC = OO.NDICFileNum
	, OriginNDM = OO.NDM
	, OriginCA = OO.CA
	, OriginState = OO.State
	, OriginAPI = OO.WellAPI 
	, OriginLat = OO.LAT 
	, OriginLon = OO.LON 
	, OriginGeoFenceRadiusMeters = OO.GeoFenceRadiusMeters 
	, Destination = D.Name
	, DestinationFull = D.FullName
	, DestType = D.DestinationType 
	, O.DestUomID
	, DestLat = D.LAT 
	, DestLon = D.LON 
	, DestGeoFenceRadiusMeters = D.GeoFenceRadiusMeters 
	, DestinationStation = D.Station 
	, O.CreateDateUTC
	, O.CreatedByUser
	, O.DeleteDateUTC 
	, O.DeletedByUser 
	, O.OriginID
	, O.DestinationID
	, PriorityID = CAST(O.PriorityID AS INT) 
	, Operator = OO.Operator
	, O.OperatorID
	, Pumper = OO.Pumper
	, O.PumperID
	, Producer = OO.Producer
	, O.ProducerID
	, Customer = C.Name
	, O.CustomerID
	, Carrier = CA.Name
	, O.CarrierID
	, O.ProductID
	, TicketType = OO.TicketType
	, EmergencyInfo = ISNULL(C.EmergencyInfo, 'For an emergency (spill, leak, fire, or accident) contact CHEMTREC @ 800.424.9300 (toll free)') 
	, DestTicketTypeID = D.TicketTypeID
	, DestTicketType = D.TicketType
	, O.OriginTankNum
	, O.OriginTankID
	, O.DispatchNotes
	, O.DispatchConfirmNum
	, RouteActualMiles = ISNULL(R.ActualMiles, 0)
	, CarrierAuthority = CA.Authority 
	, OriginTimeZone = OO.TimeZone
	, DestTimeZone = D.TimeZone
	, OCTM.OriginThresholdMinutes
	, OCTM.DestThresholdMinutes
	, ShipperHelpDeskPhone = C.HelpDeskPhone
	, OriginDrivingDirections = OO.DrivingDirections
	, DestDrivingDirections = D.DrivingDirections
	, O.AcceptLastChangeDateUTC
	, O.PickupLastChangeDateUTC
	, O.DeliverLastChangeDateUTC
    , CustomerLastChangeDateUTC = C.LastChangeDateUTC 
    , CarrierLastChangeDateUTC = CA.LastChangeDateUTC 
    , OriginLastChangeDateUTC = OO.LastChangeDateUTC 
    , DestLastChangeDateUTC = D.LastChangeDateUTC
    , RouteLastChangeDateUTC = R.LastChangeDateUTC
    , DriverID = O.DriverID
    , OriginDriver = OD.FullName
    , OriginTruckID = ISNULL(OTR.OriginTruckID, O.TruckID)
    , DestDriver = DD.FullName
	, O.SequenceNum
FROM dbo.tblOrder O
JOIN dbo.tblPriority P ON P.ID = O.PriorityID
JOIN dbo.viewOrigin OO ON OO.ID = O.OriginID
JOIN dbo.viewDestination D ON D.ID = O.DestinationID
JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
JOIN dbo.tblRoute R ON R.ID = O.RouteID
JOIN dbo.tblCarrier CA ON CA.ID = O.CarrierID
JOIN dbo.tblProduct PRO ON PRO.ID = O.ProductID		
LEFT JOIN dbo.tblOrderTransfer OTR ON OTR.OrderID = O.ID
LEFT JOIN dbo.viewDriverBase OD ON OD.ID = ISNULL(OTR.OriginDriverID, O.DriverID)
LEFT JOIN dbo.viewDriverBase DD ON DD.ID = O.DriverID
OUTER APPLY dbo.fnOrderBestMatchDriverAppPrintHeaderImage(O.ID) DAHI
OUTER APPLY dbo.fnOrderCombinedThresholdMinutes(O.ID) OCTM
WHERE StatusID IN (2,7,8,3) AND O.DeleteDateUTC IS NULL

GO

/****************************************************
 Date Created: 2016/12/22 - 4.4.14
 Author: Joe Engler
 Purpose: return all driver compliance with non compliant and missing records
 Changes:
****************************************************/
CREATE FUNCTION fnDriverComplianceSummary(@showCompliant INT = 0, @showUnenforced INT = 0)
RETURNS @ret TABLE
(
	ID INT,
	DriverID INT,
	FullName VARCHAR(41),
	CarrierID INT,
	Carrier VARCHAR(40),
	ComplianceTypeID INT,
	ComplianceType VARCHAR(100),
	MissingDocument BIT,
	ExpiredNow BIT,
	ExpiredNext30 BIT,
	ExpiredNext60 BIT,
	ExpiredNext90 BIT,
	Missing BIT
)
AS BEGIN
 	DECLARE @__ENFORCE_DRIVER_COMPLIANCE__ INT = 14

	INSERT INTO @ret
	SELECT  ID, DriverID, FullName, CarrierID, Carrier, ComplianceTypeID, ComplianceType, MissingDocument, ExpiredNow, ExpiredNext30, ExpiredNext60, ExpiredNext90, Missing = CAST(0 AS BIT)
	  FROM viewDriverCompliance
	 WHERE DeleteDateUTC IS NULL
	   AND (@showCompliant = 1 OR MissingDocument = 1 OR ExpiredNow = 1)

	UNION
 
	SELECT ID = NULL,
		DriverID = q.ID,
		q.FullName,
		q.CarrierID,
		q.Carrier,
		ComplianceTypeID = ct.id,
		ComplianceType = ct.Name,
		MissingDocument = CAST(0 AS BIT),
		ExpiredNow = CAST(0 AS BIT),
		ExpiredNext30 = CAST(0 AS BIT),
		ExpiredNext60 = CAST(0 AS BIT),
		ExpiredNext90 = CAST(0 AS BIT),
		Missing = ct.IsRequired
	FROM ( -- Get active drivers with compliance enforced
			SELECT d.* 
				FROM viewDriverBase d
				OUTER APPLY dbo.fnCarrierRules(GETUTCDATE(), null, @__ENFORCE_DRIVER_COMPLIANCE__, d.CarrierID, d.ID, d.OperatingStateID, d.RegionID, 1) cr_c
				WHERE d.DeleteDateUTC is null --active drivers
				AND (@showUnenforced = 1 OR dbo.fnToBool(cr_c.Value) = 1) -- compliance enforced
		) q,
		tbldrivercompliancetype ct
	WHERE (@showCompliant = 1 OR ct.IsRequired = 1)
	 AND NOT EXISTS -- no active record in the driver compliance table
		(SELECT  1
			FROM viewDriverCompliance 
			WHERE DeleteDateUTC IS NULL AND DriverID = q.ID and ComplianceTypeID = ct.id
		)

	RETURN
END

GO


/****************************************************
 Date Created: 2016/12/22 - 4.4.14
 Author: Joe Engler
 Purpose: return all carrier compliance with non compliant and missing records
 =========== NEED TO REVISIT =========== 
 Changes:
****************************************************/
CREATE FUNCTION fnCarrierComplianceSummary(@showCompliant INT = 0, @showUnenforced INT = 0)
RETURNS @ret TABLE
(
	ID INT,
	CarrierID INT,
	Carrier VARCHAR(40),
	ComplianceTypeID INT,
	ComplianceType VARCHAR(100),
	MissingDocument BIT,
	ExpiredNow BIT,
	ExpiredNext30 BIT,
	ExpiredNext60 BIT,
	ExpiredNext90 BIT,
	Missing BIT
)
AS BEGIN
 	DECLARE @__ENFORCE_DRIVER_COMPLIANCE__ INT = 14

	INSERT INTO @ret
	SELECT  ID, CarrierID, Carrier, DocumentTypeID, DocumentType, MissingDocument = CAST(0 AS BIT), ExpiredNow = CAST(0 AS BIT), ExpiredNext30 = CAST(0 AS BIT), ExpiredNext60 = CAST(0 AS BIT), ExpiredNext90 = CAST(0 AS BIT), Missing = CAST(0 AS BIT)
	  FROM viewCarrierDocument
	 WHERE DeleteDateUTC IS NULL
--	   AND (@showCompliant = 1 OR MissingDocument = 1 OR ExpiredNow = 1 OR ExpiredNext30 = 1 OR ExpiredNext60 = 1 OR ExpiredNext90 = 0)

	UNION
 
	SELECT ID = NULL,
		CarrierID = q.ID,
		CarrierName = q.Name,
		ComplianceTypeID = dt.ID,
		ComplianceType = dt.Name,
		MissingDocument = CAST(0 AS BIT),
		ExpiredNow = CAST(0 AS BIT),
		ExpiredNext30 = CAST(0 AS BIT),
		ExpiredNext60 = CAST(0 AS BIT),
		ExpiredNext90 = CAST(0 AS BIT),
		Missing = CAST(1 AS BIT)--ct.IsRequired
	FROM ( -- Get active drivers with compliance enforced
			SELECT c.* 
				FROM viewCarrier c
--				OUTER APPLY dbo.fnCarrierRules(GETUTCDATE(), null, @__ENFORCE_DRIVER_COMPLIANCE__, d.CarrierID, d.ID, d.OperatingStateID, d.RegionID, 1) cr_c
				WHERE c.DeleteDateUTC is null --active carriers
--				AND (@showUnenforced = 1 OR dbo.fnToBool(cr_c.Value) = 1) -- compliance enforced
		) q,
		tblCarrierDocumentType dt
	WHERE --(@showCompliant = 1 OR ct.IsRequired = 1)
	 /*AND*/ NOT EXISTS -- no active record in the carrier document table
		(SELECT  1
			FROM viewCarrierDocument
			WHERE DeleteDateUTC IS NULL AND CarrierID = q.ID and DocumentTypeID = dt.ID
		)

	RETURN
END

GO


/****************************************************
 Date Created: 2016/12/22 - 4.4.14
 Author: Joe Engler
 Purpose: return carrier, driver, truck and trailer compliance
 Changes:
****************************************************/
CREATE FUNCTION fnComplianceReport(@showCompliant INT = 0, @showUnenforced INT = 0)
RETURNS TABLE AS
RETURN
	SELECT Category = 'Carrier',
			ItemID = CarrierID, ItemName = Carrier, 
			CarrierID, Carrier,
			ComplianceTypeID, ComplianceType,
			MissingDocument, ExpiredNow, ExpiredNext30, ExpiredNext60, ExpiredNext90, Missing
		FROM fnCarrierComplianceSummary(@showCompliant, @showUnenforced)
	UNION
	SELECT Category = 'Driver',
			ItemID = DriverID, ItemName = FullName,
			CarrierID, Carrier,
			ComplianceTypeID, ComplianceType,
			MissingDocument, ExpiredNow, ExpiredNext30, ExpiredNext60, ExpiredNext90, Missing
	 FROM fnDriverComplianceSummary(@showCompliant, @showUnenforced)
	--union 
	--truck (to be written)
	--union
	--trailer (to be written)
GO

/***********************/
/* cleanup mobile number and ssn fields (leave only numbers) so that the display can properly format them */
/***********************/
UPDATE tblDriver SET MobilePhone = REPLACE(MobilePhone,' ','')
GO
UPDATE tblDriver SET MobilePhone = REPLACE(MobilePhone,'(','')
GO
UPDATE tblDriver SET MobilePhone = REPLACE(MobilePhone,')','')
GO
UPDATE tblDriver SET MobilePhone = REPLACE(MobilePhone,'-','')
GO
UPDATE tblDriver SET SSN = REPLACE(SSN,'-','')
GO



exec _spRefreshAllViews
exec _spRefreshAllViews
exec _spRefreshAllViews
exec _spRefreshAllViews
exec _spRefreshAllViews
GO

exec _spRebuildAllObjects
exec _spRebuildAllObjects
GO




COMMIT
SET NOEXEC OFF