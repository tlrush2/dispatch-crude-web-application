-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.8.21.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.8.21.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0

DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.8.21'
SELECT  @NewVersion = '3.8.22'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'More C.O.D.E. Export changes per Steve Carver with Plains.'	
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


-- Update CodeDXCode (CODE ticket type) per Steve Carver
UPDATE tblTicketType SET CodeDXCode = 4 WHERE ID IN (2, 7)
GO

-- =============================================
-- Author:		Ben Bloodworth
-- Create date: 5 Jun 2015
-- Description:	Convert open/close gauge readings to CODE format spec. Also converts meter readings.
--				1=Meter, 2=Gauge, anything else returns zeros
--				0=Open Reading, 1=Close Reading
-- Updates:		8/28/15 - BB - Fixed gauge reading format per Steve Carver's instructions
-- =============================================
ALTER FUNCTION [dbo].[fnOpenCloseReadingCODEFormat]
(
  @TicketID int
  ,@ticketType int
  ,@closingReading bit
)
RETURNS varchar(10)
AS
BEGIN
	DECLARE @ret varchar(10)
		
	IF @closingReading = 0 
		BEGIN
			SELECT @ret = 
				CASE @ticketType
					WHEN 1 
						THEN dbo.fnFixedLenNum(OT.OpenMeterUnits,8,2,1)
					WHEN 2 
						THEN dbo.fnFixedLenNum(OT.OpeningGaugeFeet,4,0,1) 
							+ dbo.fnFixedLenNum(OT.OpeningGaugeInch,2,0,1) 
							+ dbo.fnFixedLenNum(OT.OpeningGaugeQ,2,0,1) 
							+ '04'
					ELSE '0000000000'		
				END	
			FROM tblOrderTicket OT
			WHERE ID = @TicketID
		END
	ELSE
		BEGIN
			SELECT @ret = 
				CASE @ticketType
					WHEN 1 
						THEN dbo.fnFixedLenNum(OT.CloseMeterUnits,8,2,1)
					WHEN 2 
						THEN dbo.fnFixedLenNum(OT.ClosingGaugeFeet,4,0,1) 
							+ dbo.fnFixedLenNum(OT.ClosingGaugeInch,2,0,1) 
							+ dbo.fnFixedLenNum(OT.ClosingGaugeQ,2,0,1) 
							+ '04'
					ELSE '0000000000'		
				END	
			FROM tblOrderTicket OT
			WHERE ID = @TicketID			
		END
		
	RETURN @ret
END
GO
GRANT EXECUTE ON fnOpenCloseReadingCODEFormat TO role_iis_acct
GO


EXEC _spRebuildAllObjects
GO

COMMIT
SET NOEXEC OFF