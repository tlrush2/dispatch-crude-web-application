-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.12.6.6'
SELECT  @NewVersion = '3.12.6.7'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1492 - Filter which gauger app orders get synced'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/*******************************************
-- Date Created: 13 Apr 2015
-- Author: Kevin Alons
-- Purpose: return readonly Order data for Gauger App sync
-- Changes:
	- 3.8.11 - 2015/07/28 - KDA - remove use of tblOrigin.CustomerID field (no longer 1 to 1 relationship)
	- 3.12.6.7  - 2016/06/15 - JAE - Don't send gauger orders when order is Assigned or later
*******************************************/
ALTER FUNCTION fnOrderReadOnly_GaugerApp(@GaugerID int, @LastChangeDateUTC datetime) RETURNS TABLE AS
RETURN 
	WITH cteBase AS
	(
		SELECT O.ID
			, O.OrderNum
			, GAO.StatusID
			, GAO.TicketTypeID
			, GAO.GaugerID
			, PriorityNum = cast(P.PriorityNum as int) 
			, Product = PRO.Name
			, GAO.DueDate
			, Origin = OO.Name
			, OriginFull = OO.FullName
			, OO.OriginType
			, O.OriginUomID
			, OriginStation = OO.Station 
			, OriginLeaseNum = OO.LeaseNum 
			, OriginCounty = OO.County 
			, OriginLegalDescription = OO.LegalDescription 
			, OriginNDIC = OO.NDICFileNum
			, OriginNDM = OO.NDM
			, OriginCA = OO.CA
			, OriginState = OO.State
			, OriginAPI = OO.WellAPI
			, OriginLat = OO.LAT 
			, OriginLon = OO.LON 
			, OriginGeoFenceRadiusMeters = OO.GeoFenceRadiusMeters 
			, O.CreateDateUTC
			, O.CreatedByUser
			, O.LastChangeDateUTC
			, O.LastChangedByUser
			, DeleteDateUTC = isnull(GOVD.VirtualDeleteDateUTC, O.DeleteDateUTC) 
			, DeletedByUser = isnull(GOVD.VirtualDeletedByUser, O.DeletedByUser) 
			, O.OriginID
			, PriorityID = cast(O.PriorityID AS int) 
			, OO.Operator
			, O.OperatorID
			, OO.Pumper
			, O.PumperID
			, OO.Producer
			, O.ProducerID
			, Customer = S.Name
			, O.CustomerID
			, O.ProductID
			, TicketType = GTT.Name
			, EmergencyInfo = isnull(S.EmergencyInfo, 'For an emergency (spill, leak, fire, or accident) contact CHEMTREC @ 800.424.9300 (toll free)') 
			, O.OriginTankID
			, O.OriginTankNum
			, GAO.DispatchNotes
			, O.CarrierTicketNum
			, O.DispatchConfirmNum
			, OriginTimeZone = OO.TimeZone
			, OCTM.OriginThresholdMinutes
			, ShipperHelpDeskPhone = S.HelpDeskPhone
			, OriginDrivingDirections = OO.DrivingDirections
			, LCD.LCD
			, CustomerLastChangeDateUTC = S.LastChangeDateUTC 
			, OriginLastChangeDateUTC = OO.LastChangeDateUTC 
			, DestLastChangeDateUTC = D.LastChangeDateUTC
			, RouteLastChangeDateUTC = R.LastChangeDateUTC
		FROM dbo.tblOrder O
		JOIN dbo.tblGaugerOrder GAO ON GAO.OrderID = O.ID
		JOIN tblGaugerTicketType GTT ON GTT.ID = GAO.TicketTypeID
		JOIN dbo.tblPriority P ON P.ID = GAO.PriorityID
		JOIN dbo.viewOrigin OO ON OO.ID = O.OriginID
		JOIN dbo.viewDestination D ON D.ID = O.DestinationID
		JOIN dbo.tblCustomer S ON S.ID = O.CustomerID
		JOIN dbo.tblRoute R ON R.ID = O.RouteID
		JOIN tblProduct PRO ON PRO.ID = O.ProductID
		LEFT JOIN tblGaugerOrderVirtualDelete GOVD ON GOVD.OrderID = O.ID AND GOVD.GaugerID = GAO.GaugerID
		CROSS JOIN fnSyncLCDOffset(@LastChangeDateUTC) LCD
		OUTER APPLY dbo.fnOrderCombinedThresholdMinutes(O.ID) OCTM
		WHERE GAO.StatusID NOT IN (1) -- don't include REQUESTED Gauger Order records
		  AND O.ID IN (
			SELECT OrderID FROM tblGaugerOrder WHERE GaugerID = @GaugerID
			UNION 
			SELECT OrderID FROM tblGaugerOrderVirtualDelete WHERE GaugerID = @GaugerID
		  AND O.StatusID IN (-9, -10) -- Only send GAUGER or GENERATED tickets, other statuses are outside of the gauger's scope and are no longer kept in sync
		)
	)
	SELECT O.* 
		, HeaderImageID = GAHI.ID
		, PrintHeaderBlob = GAHI.ImageBlob
		, HeaderImageLeft = GAHI.ImageLeft
		, HeaderImageTop = GAHI.ImageTop
		, HeaderImageWidth = GAHI.ImageWidth
		, HeaderImageHeight = GAHI.ImageHeight
		, PickupTemplateID = GAPT.ID
		, PickupTemplateText = GAPT.TemplateText
		, TicketTemplateID = GATT.ID
		, TicketTemplateText = GATT.TemplateText
	FROM cteBase O
	OUTER APPLY dbo.fnOrderBestMatchGaugerAppPrintHeaderImage(O.ID) GAHI
	LEFT JOIN tblGaugerAppPrintHeaderImageSync GAHIS ON GAHIS.OrderID = O.ID AND GAHIS.GaugerID = @GaugerID AND GAHIS.RecordID <> GAHI.ID
	OUTER APPLY dbo.fnOrderBestMatchGaugerAppPrintPickupTemplate(O.ID) GAPT
	LEFT JOIN tblGaugerAppPrintPickupTemplateSync GAPTS ON GAPTS.OrderID = O.ID AND GAPTS.GaugerID = @GaugerID AND GAPTS.RecordID <> GAPT.ID
	OUTER APPLY dbo.fnOrderBestMatchGaugerAppPrintTicketTemplate(O.ID) GATT
	LEFT JOIN tblGaugerAppPrintTicketTemplateSync GATTS ON GATTS.OrderID = O.ID AND GATTS.GaugerID = @GaugerID AND GATTS.RecordID <> GATT.ID
	WHERE (@LastChangeDateUTC IS NULL
		OR CreateDateUTC >= LCD
		OR O.LastChangeDateUTC >= LCD
		OR O.LastChangeDateUTC >= LCD
		-- if any print related record was changed or a different template/image is now valid
		OR GAHI.LastChangeDateUTC >= LCD
		OR GAHIS.RecordID IS NOT NULL
		OR GAPT.LastChangeDateUTC >= LCD
		OR GAPTS.RecordID IS NOT NULL
		OR GATT.LastChangeDateUTC >= LCD
		OR GATTS.RecordID IS NOT NULL
	)

GO


/*******************************************
-- Date Created: 31 Aug 2013
-- Author: Kevin Alons
-- Purpose: return driver editable Order data for Driver App sync
-- Changes:
	- 3.12.6.7  - 2016/06/15 - JAE - Don't send gauger orders when order is Assigned or later
*******************************************/
ALTER FUNCTION fnGaugerOrder_GaugerApp(@GaugerID int, @LastChangeDateUTC datetime) RETURNS TABLE AS
RETURN 
	SELECT GAO.OrderID
		, GAO.StatusID
		, GAO.GaugerID
		, GAO.ArriveTimeUTC
		, GAO.DepartTimeUTC
		, GAO.Rejected
		, GAO.RejectReasonID
		, GAO.RejectNotes
		, GAO.OriginTankID
		, GAO.OriginTankNum
		, GAO.GaugerNotes
		, GAO.DueDate
		, GAO.PriorityID
		, GAO.CreateDateUTC, GAO.CreatedByUser
		, GAO.LastChangeDateUTC, GAO.LastChangedByUser
		, DeleteDateUTC = isnull(GOVD.VirtualDeleteDateUTC, O.DeleteDateUTC), DeletedByUser = isnull(GOVD.VirtualDeletedByUser, O.DeletedByUser)
	FROM dbo.tblGaugerOrder GAO
	JOIN dbo.tblOrder O ON O.ID = GAO.OrderID
	LEFT JOIN tblGaugerOrderVirtualDelete GOVD ON GOVD.OrderID = GAO.OrderID AND GOVD.GaugerID = GAO.GaugerID
	CROSS JOIN fnSyncLCDOffset(@LastChangeDateUTC) LCD
	WHERE GAO.StatusID NOT IN (1) -- don't include REQUESTED Gauger Order records
	  AND GAO.OrderID IN (
		SELECT OrderID FROM tblGaugerOrder WHERE GaugerID = @GaugerID
		UNION 
		SELECT OrderID FROM tblGaugerOrderVirtualDelete WHERE GaugerID = @GaugerID
	)
	  AND O.StatusID IN (-9, -10) -- Only send GAUGER or GENERATED tickets, other statuses are outside of the gauger's scope and are no longer kept in sync
	  AND (@LastChangeDateUTC IS NULL
		OR GAO.CreateDateUTC >= LCD.LCD
		OR GAO.LastChangeDateUTC >= LCD.LCD
		OR O.DeleteDateUTC >= LCD.LCD
		OR GOVD.VirtualDeleteDateUTC >= LCD.LCD)

GO



COMMIT
SET NOEXEC OFF