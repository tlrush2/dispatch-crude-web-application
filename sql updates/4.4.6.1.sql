SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.4.6'
SELECT  @NewVersion = '4.4.6.1'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1972 Add missing triggers to driver settlement tables'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/***********************************************
-- Date Created: 1 Dec 2016
-- Author: Joe Engler
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			ensure Locked records are not modified (except the EndDate)
			un-associate changed rates from rated orders
-- Changes
***********************************************/
CREATE TRIGGER [dbo].[trigDriverAssessorialRate_IU] ON [dbo].[tblDriverAssessorialRate] AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblDriverAssessorialRate X 
			ON i.ID <> X.ID
			  AND i.TypeID = X.TypeID
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(i.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.DriverID, X.DriverID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(i.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(i.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(i.ProducerID, X.ProducerID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Assessorial Rates are not allowed';
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewDriverAssessorialRate X ON d.ID = X.ID AND X.Locked = 1
		WHERE d.TypeID <> X.TypeID
			OR dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
			OR dbo.fnCompareNullableInts(d.CarrierID, X.CarrierID) = 0
			OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
			OR dbo.fnCompareNullableInts(d.TruckTypeID, X.TruckTypeID) = 0
			OR dbo.fnCompareNullableInts(d.DriverGroupID, X.DriverGroupID) = 0
			OR dbo.fnCompareNullableInts(d.DriverID, X.DriverID) = 0
			OR dbo.fnCompareNullableInts(d.OriginID, X.OriginID) = 0
			OR dbo.fnCompareNullableInts(d.DestinationID, X.DestinationID) = 0
			OR dbo.fnCompareNullableInts(d.OriginStateID, X.OriginStateID) = 0
			OR dbo.fnCompareNullableInts(d.DestStateID, X.DestStateID) = 0
			OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
			OR dbo.fnCompareNullableInts(d.ProducerID, X.ProducerID) = 0
			OR d.EffectiveDate <> X.EffectiveDate
			OR d.Rate <> X.Rate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed';
	END
	
	IF (@error IS NOT NULL)
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		-- un-rate any Order Assessorial Rates associated with this assessorial rate record
		DELETE AC
		FROM tblOrderSettlementDriverAssessorialCharge AC
		JOIN tblOrderSettlementDriver SD ON SD. OrderID = AC.OrderID
		JOIN inserted i ON i.ID = AC.AssessorialRateID
		WHERE SD.BatchID IS NULL
END


GO




/*************************************/
-- Date Created: 1 Dec 2016
-- Author: Joe Engler
-- Purpose: handle specialized logic related to Effective/End dates (all handled through the associated view)
-- Changes:
/*************************************/
CREATE TRIGGER [dbo].[trigDriverAssessorialRate_IOD] ON [dbo].[tblDriverAssessorialRate] INSTEAD OF DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewDriverAssessorialRate X ON X.ID = d.ID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('Locked (in use) records cannot be deleted', 16, 1)
		ROLLBACK
	END
	ELSE
	BEGIN
		-- remove all Un-Settled references to this deleted rate
		DELETE FROM tblOrderSettlementDriverAssessorialCharge 
		FROM tblOrderSettlementDriverAssessorialCharge SDAC
		JOIN tblOrderSettlementDriver SD ON SD.OrderID = SDAC.OrderID
		JOIN deleted d ON d.ID = SDAC.AssessorialRateID
		WHERE SD.BatchID IS NULL
		-- do the actual delete action
		DELETE FROM tblDriverAssessorialRate WHERE ID IN (SELECT ID FROM deleted)
	END
END

GO


/***********************************************
-- Date Created: 1 Dec 2016
-- Author: Joe Engler
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			prevent changes to Locked (in use) records
			un-associate changed rates from rated orders
-- Changes
***********************************************/
CREATE TRIGGER [dbo].[trigDriverDestinationWaitRate_IU] ON [dbo].[tblDriverDestinationWaitRate] AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblDriverDestinationWaitRate X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ReasonID, X.ReasonID) = 1
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(i.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.DriverID, X.DriverID) = 1
			  AND dbo.fnCompareNullableInts(i.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(i.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(i.ProducerID, X.ProducerID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Wait Rates are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewDriverDestinationWaitRate X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ReasonID, X.ReasonID) = 0
		  OR dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
		  OR dbo.fnCompareNullableInts(d.CarrierID, X.CarrierID) = 0
		  OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
		  OR dbo.fnCompareNullableInts(d.TruckTypeID, X.TruckTypeID) = 0
		  OR dbo.fnCompareNullableInts(d.DriverGroupID, X.DriverGroupID) = 0
		  OR dbo.fnCompareNullableInts(d.DriverID, X.DriverID) = 0
		  OR dbo.fnCompareNullableInts(d.DestinationID, X.DestinationID) = 0
		  OR dbo.fnCompareNullableInts(d.StateID, X.StateID) = 0
		  OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
		  OR dbo.fnCompareNullableInts(d.ProducerID, X.ProducerID) = 0
		  OR d.EffectiveDate <> X.EffectiveDate
		  OR d.Rate <> X.Rate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	IF @error IS NOT NULL 
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementDriver SET DestinationWaitRateID = NULL, DestinationWaitAmount = NULL 
		WHERE BatchID IS NULL AND DestinationWaitRateID IN (SELECT ID FROM inserted)
END


GO


/*************************************/
-- Date Created: 1 Dec 2016
-- Author: Joe Engler
-- Purpose: prevent deletion of Locked records
/*************************************/
CREATE TRIGGER [dbo].[trigDriverDestinationWaitRate_IOD] ON [dbo].[tblDriverDestinationWaitRate] INSTEAD OF DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewDriverDestinationWaitRate X ON X.id = d.ID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('Locked (in use) records cannot be deleted', 16, 1)
		RETURN
	END
	ELSE
	BEGIN
		-- remove all references to the deleted rates
		UPDATE tblOrderSettlementDriver
		  SET DestinationWaitRateID = NULL, DestinationWaitAmount = NULL
		WHERE BatchID IS NULL
		  AND DestinationWaitRateID IN (SELECT ID FROM deleted)
		-- do the actual delete now
		DELETE FROM tblDriverDestinationWaitRate WHERE ID IN (SELECT ID FROM deleted)
	END
END

GO


/***********************************************
-- Date Created: 1 Dec 2016
-- Author: Joe Engler
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			prevent changes to Locked (in use) records
			un-associate changed rates from rated orders
-- Changes
***********************************************/
CREATE TRIGGER [dbo].[trigDriverFuelSurchargeRate_IU] ON [dbo].[tblDriverFuelSurchargeRate] AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblDriverFuelSurchargeRate X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(i.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.DriverID, X.DriverID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Fuel Surcharge Rates are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewDriverFuelSurchargeRate X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
		  OR dbo.fnCompareNullableInts(d.CarrierID, X.CarrierID) = 0
		  OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
		  OR dbo.fnCompareNullableInts(d.TruckTypeID, X.TruckTypeID) = 0
		  OR dbo.fnCompareNullableInts(d.DriverGroupID, X.DriverGroupID) = 0
		  OR dbo.fnCompareNullableInts(d.DriverID, X.DriverID) = 0
		  OR d.EffectiveDate <> X.EffectiveDate
		  OR d.FuelPriceFloor <> X.FuelPriceFloor
		  OR d.IncrementAmount <> X.IncrementAmount
		  OR d.IntervalAmount <> X.IntervalAmount
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	IF @error IS NOT NULL 
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementDriver SET FuelSurchargeRateID = NULL, FuelSurchargeAmount = NULL 
		WHERE BatchID IS NULL AND FuelSurchargeRateID IN (SELECT ID FROM inserted)
END


GO


/*************************************/
-- Date Created: 1 Dec 2016
-- Author: Joe Engler
-- Purpose: prevent deletion of Locked records
/*************************************/
CREATE TRIGGER [dbo].[trigDriverFuelSurchargeRate_IOD] ON [dbo].[tblDriverFuelSurchargeRate] INSTEAD OF DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewDriverFuelSurchargeRate X ON X.id = d.ID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('Locked (in use) records cannot be deleted', 16, 1)
		RETURN
	END
	ELSE
	BEGIN
		-- ensure all references to the deleted rates are fully removed
		UPDATE tblOrderSettlementDriver
		  SET FuelSurchargeRateID = NULL, FuelSurchargeAmount = NULL
		WHERE BatchID IS NULL
		  AND FuelSurchargeRateID IN (SELECT ID FROM deleted)
		-- do the actual update here
		DELETE FROM tblDriverFuelSurchargeRate WHERE ID IN (SELECT ID FROM deleted)
	END
END


GO

/***********************************************
-- Date Created: 1 Dec 2016
-- Author: Joe Engler
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			prevent changes to Locked (in use) records
-- Changes:
***********************************************/
CREATE TRIGGER [dbo].[trigDriverOrderRejectRate_IU] ON [dbo].[tblDriverOrderRejectRate] AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblDriverOrderRejectRate X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ReasonID, X.ReasonID) = 1
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(i.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.DriverID, X.DriverID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(i.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(i.ProducerID, X.ProducerID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Reject Rates are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewDriverOrderRejectRate X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ReasonID, X.ReasonID) = 0
		  OR dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
		  OR dbo.fnCompareNullableInts(d.CarrierID, X.CarrierID) = 0
		  OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
		  OR dbo.fnCompareNullableInts(d.TruckTypeID, X.TruckTypeID) = 0
		  OR dbo.fnCompareNullableInts(d.DriverGroupID, X.DriverGroupID) = 0
		  OR dbo.fnCompareNullableInts(d.DriverID, X.DriverID) = 0
		  OR dbo.fnCompareNullableInts(d.OriginID, X.OriginID) = 0
		  OR dbo.fnCompareNullableInts(d.StateID, X.StateID) = 0
		  OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
		  OR dbo.fnCompareNullableInts(d.ProducerID, X.ProducerID) = 0
		  OR d.EffectiveDate <> X.EffectiveDate
		  OR d.Rate <> X.Rate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	IF @error IS NOT NULL 
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementDriver SET OrderRejectRateID = NULL, OrderRejectAmount = NULL 
		WHERE BatchID IS NULL AND OrderRejectRateID IN (SELECT ID FROM inserted)
END


GO


/*************************************
-- Date Created: 1 Dec 2016
-- Author: Joe Engler
-- Purpose: prevent deletion of Locked records
-- Changes:
*************************************/
CREATE TRIGGER [dbo].[trigDriverOrderRejectRate_IOD] ON [dbo].[tblDriverOrderRejectRate] INSTEAD OF DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewDriverOrderRejectRate X ON X.id = d.ID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('Locked (in use) records cannot be deleted', 16, 1)
		RETURN
	END
	ELSE
	BEGIN
		-- remove all references to the deleted rates
		UPDATE tblOrderSettlementDriver
		  SET OrderRejectRateID = NULL, OrderRejectAmount = NULL
		WHERE BatchID IS NULL
		  AND OrderRejectRateID IN (SELECT ID FROM deleted)
		-- do the actual delete now
		DELETE FROM tblDriverOrderRejectRate WHERE ID IN (SELECT ID FROM deleted)
	END
END

GO


/***********************************************
-- Date Created: 1 Dec 2016
-- Author: Joe Engler
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			prevent changes to Locked (in use) records
-- Changes:
***********************************************/
CREATE TRIGGER [dbo].[trigDriverOriginWaitRate_IU] ON [dbo].[tblDriverOriginWaitRate] AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblDriverOriginWaitRate X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ReasonID, X.ReasonID) = 1
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(i.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.DriverID, X.DriverID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(i.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Wait Rates are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewDriverOriginWaitRate X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ReasonID, X.ReasonID) = 0
		  OR dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
		  OR dbo.fnCompareNullableInts(d.CarrierID, X.CarrierID) = 0
		  OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
		  OR dbo.fnCompareNullableInts(d.TruckTypeID, X.TruckTypeID) = 0
		  OR dbo.fnCompareNullableInts(d.DriverGroupID, X.DriverGroupID) = 0
		  OR dbo.fnCompareNullableInts(d.DriverID, X.DriverID) = 0
		  OR dbo.fnCompareNullableInts(d.OriginID, X.OriginID) = 0
		  OR dbo.fnCompareNullableInts(d.StateID, X.StateID) = 0
		  OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
		  OR d.EffectiveDate <> X.EffectiveDate
		  OR d.Rate <> X.Rate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	IF @error IS NOT NULL 
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementDriver SET OriginWaitRateID = NULL, OriginWaitAmount = NULL 
		WHERE BatchID IS NULL AND OriginWaitRateID IN (SELECT ID FROM inserted)
END


GO


/*************************************
-- Date Created: 1 Dec 2016
-- Author: Joe Engler
-- Purpose: prevent deletion of Locked records
-- Changes:
*************************************/
CREATE TRIGGER [dbo].[trigDriverOriginWaitRate_IOD] ON [dbo].[tblDriverOriginWaitRate] INSTEAD OF DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewDriverOriginWaitRate X ON X.id = d.ID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('Locked (in use) records cannot be deleted', 16, 1)
		RETURN
	END
	ELSE
	BEGIN
		-- remove any references to the deleted rates
		UPDATE tblOrderSettlementDriver
		  SET OriginWaitRateID = NULL, OriginWaitAmount = NULL
		WHERE BatchID IS NULL
		  AND OriginWaitRateID IN (SELECT ID FROM deleted)
		DELETE FROM tblDriverOriginWaitRate WHERE ID IN (SELECT ID FROM deleted)
	END
END

GO

/*************************************/
-- Date Created: 1 Dec 2016
-- Author: Joe Engler
-- Purpose: ensure that a RangeRate record is not moved from one RateSheet to another
/*************************************/
CREATE TRIGGER [dbo].[trigDriverRangeRate_IU] ON [dbo].[tblDriverRangeRate] AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @error varchar(255)
	IF EXISTS (SELECT i.* FROM inserted i JOIN deleted d ON d.ID = i.ID WHERE i.RateSheetID <> d.RateSheetID)
		SET @error = 'RangeRate records cannot be moved to a different RateSheet'
	ELSE IF EXISTS (SELECT * FROM tblDriverRangeRate SRR JOIN inserted i ON i.RateSheetID = SRR.RateSheetID AND i.ID <> SRR.ID
		AND (i.MinRange BETWEEN SRR.MinRange AND SRR.MaxRange 
			OR i.MaxRange BETWEEN SRR.MinRange AND SRR.MaxRange 
			OR SRR.MinRange BETWEEN i.MinRange AND i.MaxRange))
	BEGIN
		SET @error = 'Overlapping Range Rate records are not allowed'
	END
	
	IF @error IS NOT NULL
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementDriver SET RangeRateID = NULL, RouteRateID = NULL, LoadAmount = NULL
		WHERE BatchID IS NULL AND RangeRateID IN (SELECT ID FROM inserted)
END

GO


/*************************************
-- Date Created: 1 Dec 2016
-- Author: Joe Engler
-- Purpose: prevent deletion of RangeRate records that belong to a Locked RateSheet
-- Changes:
*************************************/
CREATE TRIGGER [dbo].[trigDriverRangeRate_IOD] ON [dbo].[tblDriverRangeRate] INSTEAD OF DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewDriverRateSheet X ON X.ID = d.RateSheetID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('RangeRate records for a Locked Rate Sheet cannot be deleted', 16, 1)
		ROLLBACK
	END
	ELSE
	BEGIN
		-- remove any references to the deleted rates
		UPDATE tblOrderSettlementDriver
		  SET RangeRateID = NULL, LoadAmount = NULL
		WHERE BatchID IS NULL
		  AND RangeRateID IN (SELECT ID FROM deleted)
		DELETE FROM tblDriverRangeRate WHERE ID IN (SELECT ID FROM deleted)
	END
END

GO


/***********************************************
-- Date Created: 1 Dec 2016
-- Author: Joe Engler
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
-- Changes
***********************************************/
CREATE TRIGGER [dbo].[trigDriverRateSheet_IU] ON [dbo].[tblDriverRateSheet] AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblDriverRateSheet X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.DriverID, X.DriverID) = 1
			  AND dbo.fnCompareNullableInts(i.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(i.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(i.ProducerID, X.ProducerID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Rate Sheets are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewDriverRateSheet X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
			OR dbo.fnCompareNullableInts(d.CarrierID, X.CarrierID) = 0
			OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
			OR dbo.fnCompareNullableInts(d.DriverGroupID, X.DriverGroupID) = 0
			OR dbo.fnCompareNullableInts(d.DriverID, X.DriverID) = 0
		    OR dbo.fnCompareNullableInts(d.TruckTypeID, X.TruckTypeID) = 0
			OR dbo.fnCompareNullableInts(d.OriginStateID, X.OriginStateID) = 0
			OR dbo.fnCompareNullableInts(d.DestStateID, X.DestStateID) = 0
			OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
			OR dbo.fnCompareNullableInts(d.ProducerID, X.ProducerID) = 0
			OR d.EffectiveDate <> X.EffectiveDate 
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	
	IF (@error IS NOT NULL)
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
END


GO


/*************************************/
-- Date Created: 1 Dec 2016
-- Author: Joe Engler
-- Purpose: manually do CASCADING Deletion of RangeRates (since using an INSTEAD OF Trigger there)
/*************************************/
CREATE TRIGGER [dbo].[trigDriverRateSheet_IOD] ON [dbo].[tblDriverRateSheet] INSTEAD OF DELETE AS
BEGIN
	SET NOCOUNT ON;
	-- manually do cascading delete
	DELETE FROM tblDriverRangeRate WHERE RateSheetID IN (SELECT ID FROM deleted)
	-- do the actual delete now
	DELETE FROM tblDriverRateSheet WHERE ID IN (SELECT ID FROM deleted)
END

GO


/*************************************/
-- Date Created: 1 Dec 2016
-- Author: Joe Engler
-- Purpose: handle specialized logic related to Effective/End dates (all handled through the associated view)
/*************************************/
CREATE TRIGGER [dbo].[trigDriverRateSheet_D] ON [dbo].[tblDriverRateSheet] AFTER DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewDriverRateSheet X ON X.ID = d.ID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('Locked (in use) records cannot be deleted', 16, 1)
		ROLLBACK
	END
END


GO


/***********************************************
-- Date Created: 1 Dec 2016
-- Author: Joe Engler
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
-- Changes
***********************************************/
CREATE TRIGGER [dbo].[trigDriverRouteRate_IU] ON [dbo].[tblDriverRouteRate] AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblDriverRouteRate X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(i.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.DriverID, X.DriverID) = 1
			  AND i.RouteID = X.RouteID
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Route Rates are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewDriverRouteRate X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
			OR dbo.fnCompareNullableInts(d.CarrierID, X.CarrierID) = 0
			OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
		    OR dbo.fnCompareNullableInts(d.TruckTypeID, X.TruckTypeID) = 0
			OR dbo.fnCompareNullableInts(d.DriverGroupID, X.DriverGroupID) = 0
			OR dbo.fnCompareNullableInts(d.DriverID, X.DriverID) = 0
			OR d.RouteID <> X.RouteID
			OR d.EffectiveDate <> X.EffectiveDate
			OR d.Rate <> X.Rate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	IF (@error IS NOT NULL)
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementDriver SET RouteRateID = NULL, RangeRateID = NULL, LoadAmount = NULL 
		WHERE BatchID IS NULL AND RouteRateID IN (SELECT ID FROM inserted)
END


GO


/*************************************
-- Date Created: 1 Dec 2016
-- Author: Joe Engler
-- Purpose: handle specialized logic related to Effective/End dates (all handled through the associated view)
-- Changes:
*************************************/
CREATE TRIGGER [dbo].[trigDriverRouteRate_IOD] ON [dbo].[tblDriverRouteRate] INSTEAD OF DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewDriverRouteRate X ON X.ID = d.ID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('Locked (in use) records cannot be deleted', 16, 1)
		RETURN
	END
	ELSE
	BEGIN
		-- remove any references to the deleted rates
		UPDATE tblOrderSettlementDriver
		  SET RouteRateID = NULL, LoadAmount = NULL
		WHERE BatchID IS NULL
		  AND RouteRateID IN (SELECT ID FROM deleted)
		-- do the actual delete now
		DELETE FROM tblDriverRouteRate WHERE ID IN (SELECT ID FROM deleted)
	END
END

GO



COMMIT
SET NOEXEC OFF
