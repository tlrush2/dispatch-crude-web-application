-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.1.20'
SELECT  @NewVersion = '4.1.20.1'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1822 - Auto-Audit/Approve/Unapprove functionality intermittently not working + timeout issues on Audit page'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/* remove any records with duplicate uid values */
delete from tblDriverLocation 
from tblDriverLocation DL
join (select uid, id = max(id) from tblDriverLocation group by uid having count(*) > 1) DUPE ON DUPE.uid = DL.uid AND DL.id <> DUPE.id
go

/* replace the non-unique UID index with a unique one*/
exec _spDropIndex 'idxDriverLocation_UID'
go
create unique index udxDriverLocation_UID ON tblDriverLocation(UID)
GO

/**********************************************************/
-- Author: Kevin Alons
-- Date Created: 8 Oct 2014
-- Purpose: JOIN the DriverLocation table with the Order Table to find the most suitable Origin Arrival GPS record
-- Changes:
-- 3.11.14 - 2016/04/18 - JAE		- Added top 1 to ensure only one record is selected (sync can sometimes send duplicates)
-- 3.11.15.1 - 2016/04/20 - JAE	- Undo top 1 --NEED TO INVESTIGATE WHY THIS WOULD BE AN ISSUE
-- 3.11.18.1 - 2016/05/03 - KDA	- stop using UID but instead use ID for performance reasons
-- 3.11.20.1 - 2016/05/04 - JAE & BB - undo last change of UID > ID due to timeout/syncing errors
-- 4.1.20.1	- 2016.10.15 - KDA	- add NOLOCK query hint to avoid db contention (this is a WRITE ONLY table anyway)
/***********************************************************/
ALTER VIEW viewDriverLocation_OriginFirstArrive AS
SELECT DL3.*
FROM tblDriverLocation DL3
JOIN (
	-- get the first matching record (by lowest UID)
	SELECT DL2.OrderID, DL2.OriginID, UID = min(cast(DL2.UID as varchar(100)))
	FROM tblDriverLocation DL2
	JOIN (
		-- get the best (lowest) SourceAccuracyMeters
		SELECT DL.OrderID, DL.OriginID, X.SourceDateUTC, SourceAccuracyMeters = min(DL.SourceAccuracyMeters)
		FROM tblDriverLocation DL
		JOIN (
			-- get the first SourceDateUTC
			SELECT OrderID, OriginID, SourceDateUTC = MIN(SourceDateUTC)
			FROM tblDriverLocation WITH (NOLOCK)
			WHERE OriginID IS NOT NULL
			GROUP BY OrderID, OriginID
		) X ON X.OrderID = DL.OrderID
			AND X.OriginID = DL.OriginID
			AND X.SourceDateUTC = DL.SourceDateUTC
		GROUP BY DL.OrderID, DL.OriginID, X.SourceDateUTC
	) X2 ON X2.OrderID = DL2.OrderID AND X2.OriginID = DL2.OriginID 
		AND X2.SourceDateUTC = DL2.SourceDateUTC AND X2.SourceAccuracyMeters = DL2.SourceAccuracyMeters
	GROUP BY DL2.OrderID, DL2.OriginID
) X3 ON X3.OrderID = DL3.OrderID AND X3.OriginID = DL3.OriginID AND X3.UID = DL3.UID

GO

/**********************************************************/
-- Author: Kevin Alons
-- Date Created: 8 Oct 2014
-- Purpose: JOIN the DriverLocation table with the Order Table to find the most suitable Destination Arrival GPS record
-- Changes:
-- 3.11.14 - 2016/04/18 - JAE		- Added top 1 to ensure only one record is selected (sync can sometimes send duplicates)
-- 3.11.15.1 - 2016/04/20 - JAE	- Undo top 1
-- 3.11.18.1 - 2016/05/03 - KDA	- stop using UID but instead use ID for performance reasons
-- 3.11.20.1 - 2016/05/04 - JAE & BB - undo last change of UID > ID due to timeout/syncing errors
-- 4.1.20.1	- 2016.10.15 - KDA	- add NOLOCK query hint to avoid db contention (this is a WRITE ONLY table anyway)
/***********************************************************/
ALTER VIEW viewDriverLocation_DestinationFirstArrive AS
SELECT DL3.*
FROM tblDriverLocation DL3
JOIN (
	-- get the first matching record (by lowest UID)
	SELECT DL2.OrderID, DL2.DestinationID, UID = min(cast(DL2.UID as varchar(100)))
	FROM tblDriverLocation DL2
	JOIN (
		-- get the best (lowest) SourceAccuracyMeters
		SELECT DL.OrderID, DL.DestinationID, X.SourceDateUTC, SourceAccuracyMeters = min(DL.SourceAccuracyMeters)
		FROM tblDriverLocation DL
		JOIN (
			-- get the first SourceDateUTC
			SELECT OrderID, DestinationID, SourceDateUTC = MIN(SourceDateUTC)
			FROM tblDriverLocation WITH (NOLOCK)
			WHERE DestinationID IS NOT NULL
			GROUP BY OrderID, DestinationID
		) X ON X.OrderID = DL.OrderID
			AND X.DestinationID = DL.DestinationID
			AND X.SourceDateUTC = DL.SourceDateUTC
		GROUP BY DL.OrderID, DL.DestinationID, X.SourceDateUTC
	) X2 ON X2.OrderID = DL2.OrderID AND X2.DestinationID = DL2.DestinationID 
		AND X2.SourceDateUTC = DL2.SourceDateUTC AND X2.SourceAccuracyMeters = DL2.SourceAccuracyMeters
	GROUP BY DL2.OrderID, DL2.DestinationID
) X3 ON X3.OrderID = DL3.OrderID AND X3.DestinationID = DL3.DestinationID AND X3.UID = DL3.UID

GO

CREATE TABLE tblOrderProcessStatusChangeAccomplishExceptions
(
  ID int identity(1, 1) not null constraint PK_OrderProcessStatusChangeAccomplishExceptions primary key
, OrderID int not null constraint FK_OrderProcessStatusChangeAccomplishExceptions foreign key references tblOrder(ID) ON DELETE CASCADE
, Action varchar(25) not null
, Exception varchar(100) null
, CreateDateUTC datetime not null constraint DF_OrderProcessStatusChangeAccomplishExceptions_CreateDateUTC default (getutcdate())
, CreatedByUser varchar(100) not null constraint DF_OrderProcessStatusChangeAccomplishExceptions_CreatedByUser default ('System')
)
GO

/*************************************************/
-- Created: 3.13.1 - 2016.07.04 - Kevin Alons
-- Purpose: accomplish any required OrderProcessStatusChanges operations
-- Changes:
-- 4.0.14 - 2016.09.05 - KDA	- fix Auto-Approve failing following an Auto-Approve operation 
--								-- (the new record tblOrderProcessStatusChange record was invalid) + it was added before the DELETE statement removed it)
-- 4.1.20.1  - 2016.10.15 - KDA	- use new tblOrderProcessStatusChangeAccomplishExceptions table (with TRY-CATCH blocks) to trap/report execution exceptions
--								- fix auto-UnAPPROVE logic (it was referring to incorrect tblOrderAppChanges table)
/*************************************************/
ALTER PROCEDURE spOrderProcessStatusChangesAccomplish AS
BEGIN
	SET NOCOUNT ON
	PRINT 'spOrderProcessStatusChangesAccomplish START: ' + (CONVERT(VARCHAR(24), GETDATE(), 121))

	DECLARE @toAudit bit, @id int
	DECLARE @action varchar(25), @errorIDS IDTABLE;
	DECLARE @retCount int = 0

	WHILE EXISTS (SELECT * FROM tblOrderProcessStatusChange WHERE OrderID NOT IN (SELECT ID FROM @errorIDS) AND NewStatusID = 3 /*DELIVERED*/ AND OldStatusID <> 4 /*AUDITED*/)
	BEGIN
		SELECT TOP 1 @ID = OrderID FROM tblOrderProcessStatusChange WHERE OrderID NOT IN (SELECT ID FROM @errorIDS) AND NewStatusID = 3 /*DELIVERED*/ AND OldStatusID <> 4 /*AUDITED*/

		BEGIN TRY
			-- attempt to apply rates to all newly DELIVERED orders
			SET @action = 'spApplyRatesBoth'
			EXEC spApplyRatesBoth @id, 'System' 

			-- attempt to Auto-Audit this order
			SET @action = 'spAutoAuditOrder'
			EXEC spAutoAuditOrder @ID, 'System', @toAudit OUTPUT

			-- remove this record to COMPLETE the Auto-Audit process
			DELETE FROM tblOrderProcessStatusChange WHERE OrderID = @ID

			/* if the order was updated to AUDITED status, then we need to attempt also to auto-approve [below] */
			IF (@toAudit = 1)
				-- add a status change record 
				INSERT INTO tblOrderProcessStatusChange (OrderID, OldStatusID, NewStatusID, StatusChangeDateUTC) 
					VALUES( @ID, 3 /*DELIVERED*/, 4 /*AUDITED*/, getutcdate() )

			SET @retCount = @retCount + 1
		END TRY
		BEGIN CATCH
			INSERT INTO tblOrderProcessStatusChangeAccomplishExceptions (OrderID, Action, Exception)
				SELECT @id, @action, ERROR_MESSAGE()
			INSERT INTO @errorIDS VALUES (@id)
		END CATCH
	END

	-- attempt to Auto_Approve any orders not yet approved
	WHILE EXISTS (SELECT OrderID FROM tblOrderProcessStatusChange WHERE OrderID NOT IN (SELECT ID FROM @errorIDS) AND NewStatusID = 4 /*AUDITED*/)
	BEGIN
		BEGIN TRY
			SELECT TOP 1 @id = OrderID FROM tblOrderProcessStatusChange WHERE OrderID NOT IN (SELECT ID FROM @errorIDS) AND NewStatusID = 4 /*AUDITED*/

			SET @action = 'spAutoApproveOrder'
			EXEC spAutoApproveOrder @ID, 'System'

			DELETE FROM tblOrderProcessStatusChange WHERE OrderID = @ID

			SET @retCount = @retCount + 1
		END TRY
		BEGIN CATCH
			INSERT INTO tblOrderProcessStatusChangeAccomplishExceptions (OrderID, Action, Exception)
				SELECT @id, @action, ERROR_MESSAGE()
			INSERT INTO @errorIDS VALUES (@id)
		END CATCH
	END

	-- ensure any orders Downgraded from AUDITED are automatically Un-APPROVED
	WHILE EXISTS (SELECT OrderID FROM tblOrderProcessStatusChange WHERE OrderID NOT IN (SELECT ID FROM @errorIDS) AND OldStatusID = 4 /*AUDITED*/)
	BEGIN
		BEGIN TRY
			SELECT TOP 1 @id = OrderID FROM tblOrderProcessStatusChange WHERE OrderID NOT IN (SELECT ID FROM @errorIDS) AND OldStatusID = 4 /*AUDITED*/

			SET @action = 'Un-Approve'
			UPDATE tblOrderApproval SET Approved = 0 WHERE OrderID = @id

			DELETE FROM tblOrderProcessStatusChange WHERE OrderID = @ID

			SET @retCount = @retCount + 1
		END TRY
		BEGIN CATCH
			INSERT INTO tblOrderProcessStatusChangeAccomplishExceptions (OrderID, Action, Exception)
				SELECT @id, @action, ERROR_MESSAGE()
			INSERT INTO @errorIDS VALUES (@id)
		END CATCH
	END

	-- calculate the number of error records
	DECLARE @errorCount int = (SELECT count(DISTINCT ID) FROM @errorIDS)

	PRINT 'spOrderProcessStatusChangesAccomplish(count=' + ltrim(@retCount) + ', errorCount=' + ltrim(@errorCount) + ') DONE: ' + (CONVERT(VARCHAR(24), GETDATE(), 121))

	-- return 0 for SUCCESS, and an error count as FAILURE
	RETURN @errorCount
END

GO

COMMIT
SET NOEXEC OFF