-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.7.20.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.7.20.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0

DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.7.21'
SELECT  @NewVersion = '3.7.22'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Omit Tanks for Deleted Origins on Order/Gauger Create pages'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/***********************************/
-- Date Created: 12 Mar 2014
-- Author: Kevin Alons
-- Purpose: return Origin Tank records with translated values
-- Changes:
	-- 3.7.22 - 6/5/2015 - GSM - make Delete Date fields reflect Origin.DeleteX values when NULL
/***********************************/
ALTER VIEW [dbo].[viewOriginTank] AS
	SELECT OT.ID				 				
		, OT.OriginID					
		, OT.TankNum 					
		, OT.TankDescription 	
		, OT.CreateDateUTC 
		, OT.CreatedByUser 		
		, OT.LastChangeDateUTC
		, OT.LastChangedByUser
		, DeleteDateUTC = ISNULL(OT.DeleteDateUTC, O.DeleteDateUTC)	
		, DeletedByUser = ISNULL(OT.DeletedByUser, O.DeletedByUser)	
		, Origin = O.Name
		, OriginLeaseNum = O.LeaseNum
		, TankNum_Unstrapped = CASE WHEN OT.TankNum = '*' THEN '[Enter Details]' ELSE OT.TankNum END 
		, Unstrapped = CASE WHEN OT.TankNum = '*' THEN 1 ELSE 0 END
		, ForGauger = CASE WHEN O.GaugerTicketTypeID IS NULL THEN 0 ELSE 1 END
	FROM dbo.tblOriginTank OT
	JOIN dbo.viewOrigin O ON O.ID = OT.OriginID

GO


COMMIT
SET NOEXEC OFF