-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.11.18.1'
SELECT  @NewVersion = '3.11.19'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1276: Add Job number and Contract number fields to orders'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/* ---------------TABLES-----------------*/

/*  DCWEB-1276 -- Add Job Number and Contract Number fields to tblOrder */
ALTER TABLE tblOrder
ADD JobNumber VARCHAR(20) NULL
  , ContractNumber VARCHAR(20) NULL
GO


/* Add new fields to audit table */
ALTER TABLE tblOrderDbAudit
ADD JobNumber VARCHAR(20) NULL
  , ContractNumber VARCHAR(20) NULL
GO


/*  DCWEB-1276 -- Add Job Number and Contract Number fields to report center */
SET IDENTITY_INSERT tblReportColumnDefinition ON
	INSERT INTO tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport)
	SELECT 302, 1, 'JobNumber', 'GENERAL | Job #', NULL, NULL, 1, NULL, 1, '*', 1
	UNION
	SELECT 303, 1, 'ContractNumber', 'GENERAL | Contract #', NULL, NULL, 1, NULL, 1, '*', 1
EXCEPT SELECT ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport FROM tblReportColumnDefinition
SET IDENTITY_INSERT tblReportColumnDefinition OFF
GO


EXEC sp_refreshview viewOrderBase
GO
EXEC sp_refreshview viewOrder
GO
EXEC sp_refreshview viewGauger
GO


/* ---------------VIEWS-----------------*/

/**********************************
-- Date Created: 18 Apr 2015
-- Author: Kevin Alons
-- Purpose: return GaugerOrder records with "translated friendly" values for FK relationships
-- Changes:
	-- 22 May 2015 - KDA - add "TicketCount" field
	- 3.11.19 - 2016/04/29 - BB - Add Job number and contract number fields
***********************************/
ALTER VIEW [dbo].[viewGaugerOrder] AS
SELECT GAO.*
	, OrderNum = O.OrderNum
	, OrderDate = O.OrderDate
	, Status = GOS.Name
	, CarrierTicketNum = O.CarrierTicketNum
	, DispatchConfirmNum = O.DispatchConfirmNum
	, JobNumber = O.JobNumber
	, ContractNumber = O.ContractNumber
	, Gauger = G.FullName
	, OriginID = O.OriginID
	, Origin = O.Origin
	, OriginFull = O.OriginFull
	, OriginState = O.OriginState
	, OriginStateAbbrev = O.OriginStateAbbrev
	, CustomerID = O.CustomerID
	, ShipperID = O.CustomerID
	, Shipper = O.Customer
	, DestinationID = O.DestinationID
	, Destination = O.Destination
	, DestinationFull = O.DestinationFull
	, DestinationState = O.DestinationState
	, DestinationStateAbbrev = O.DestinationStateAbbrev
	, ProductID = O.ProductID
	, Product = O.Product
	, ProductShort = O.ProductShort
	, ProductGroupID = O.ProductGroupID
	, ProductGroup = O.ProductGroup
	, OriginTankText = ISNULL(OT.TankNum, GAO.OriginTankNum)
	, P.PriorityNum
	, TicketType = GTT.Name
	, PrintStatusID = O.PrintStatusID
	, OrderStatusID = O.StatusID
	, DeleteDateUTC = O.DeleteDateUTC
	, DeletedByUser = O.DeletedByUser
	, TicketCount = (SELECT COUNT(*) FROM tblGaugerOrderTicket GOT WHERE GOT.OrderID = GAO.OrderID AND GOT.DeleteDateUTC IS NULL)
FROM dbo.tblGaugerOrder GAO 
JOIN viewOrder O ON O.ID = GAO.OrderID 
JOIN dbo.tblGaugerOrderStatus GOS ON GOS.ID = GAO.StatusID
LEFT JOIN dbo.tblOriginTank OT ON OT.ID = GAO.OriginTankID
LEFT JOIN dbo.viewGauger G ON G.ID = GAO.GaugerID
LEFT JOIN dbo.tblGaugerTicketType GTT ON GTT.ID = GAO.TicketTypeID
LEFT JOIN tblPriority P ON P.ID = GAO.PriorityID
GO


EXEC sp_refreshview viewOrderLocalDates
GO


/*************************************************************
-- Date Created: 15 Jun 2015
-- Author: Kevin Alons
-- Purpose: return the base data for the Order Approval page
-- Changes:
	- 3.9.2  - 2015/08/25 - KDA - remove obsolete OrderDateUTC & DeliverDateUTC fields (which were used to optimize date searches when OrderDate was virtual)
	- 3.9.20 - 2015/10/26 - JAE - Add columns for transfer percentage and override percentage
	- 3.9.25 - 2015/11/10 - JAE - Use origin driver for approval page
	- 3.9.36 - 2015/12/21 - JAE - Add columns for min settlement units (carrier and shipper)
	- 3.10.13  - 2016/02/29 - JAE - Add Truck Type
	- 3.11.17.2 - 2016/04/18 - JAE - Filter producer settled orders
	- 3.11.19 - 2016/05/02 - BB - Add Job number and Contract number
*************************************************************/
ALTER VIEW [dbo].[viewOrderApprovalSource]
AS
	SELECT O.ID
		, O.StatusID
		, Status = O.OrderStatus
		, O.OrderNum
		, O.JobNumber
		, O.ContractNumber
		, O.OrderDate
		, DeliverDate = ISNULL(O.DeliverDate, O.OrderDate)
		, ShipperID = O.CustomerID
		, O.CarrierID
		, DriverID = O.OriginDriverID
		, TruckID = O.OriginTruckID
		, O.TrailerID
		, O.Trailer2ID
		, O.ProducerID
		, O.OperatorID
		, O.PumperID
		, Shipper = O.Customer
		, O.Carrier
		, Driver = O.OriginDriver
		, O.Truck
		, O.Trailer
		, O.Trailer2
		, O.Producer
		, O.Operator
		, O.Pumper
		, O.TicketTypeID
		, O.TicketType
		, O.ProductID
		, O.Product
		, O.ProductGroupID
		, O.ProductGroup
		, O.TruckTypeID
		, O.TruckType
    
		, O.Rejected
		, RejectReason = O.RejectNumDesc
		, O.RejectNotes
		, O.ChainUp
		, OA.OverrideChainup
		, O.H2S
		, OA.OverrideH2S
		, O.DispatchConfirmNum
		, O.DispatchNotes
		, O.PickupDriverNotes
		, O.DeliverDriverNotes
		, Approved = CAST(ISNULL(OA.Approved, 0) AS BIT)
		, OSS.WaitFeeParameterID, O.AuditNotes
		
		, O.OriginID
		, O.Origin
		, O.OriginStateID
		, O.OriginArriveTime
		, O.OriginDepartTime
		, O.OriginMinutes
		, O.OriginUomID
		, O.OriginUOM
		, O.OriginGrossUnits
		, O.OriginGrossStdUnits
		, O.OriginNetUnits
		, OriginWaitReason = OWR.NumDesc
		, O.OriginWaitNotes
		, ShipperOriginWaitBillableMinutes = OSS.OriginWaitBillableMinutes
		, CarrierOriginWaitBillableMinutes = OSC.OriginWaitBillableMinutes
		, OA.OverrideOriginMinutes 
		
		, O.DestinationID
		, O.Destination
		, O.DestStateID
		, O.DestArriveTime
		, O.DestDepartTime
		, O.DestMinutes
		, O.DestUomID
		, O.DestUOM
		, O.DestGrossUnits
		, O.DestNetUnits
		, O.DestOpenMeterUnits
		, O.DestCloseMeterUnits
		, DestWaitReason = OWR.NumDesc
		, O.DestWaitNotes
		, ShipperDestinationWaitBillableMinutes = OSS.DestinationWaitBillableMinutes
		, CarrierDestinationWaitBillableMinutes = OSC.DestinationWaitBillableMinutes
		, OA.OverrideDestMinutes
	
		, Rerouted = CAST(CASE WHEN ORD.OrderID IS NULL THEN 0 ELSE 1 END AS BIT)
		, OA.OverrideReroute
		, O.ActualMiles
		, RerouteMiles = ROUND(ORD.RerouteMiles, 0)
		, OA.OverrideActualMiles
		, IsTransfer = CAST(CASE WHEN OT.OrderID IS NULL THEN 0 ELSE 1 END AS BIT)
		, TransferPercentComplete = ISNULL(OT.PercentComplete,100)

		, OA.OverrideTransferPercentComplete
		, CarrierMinSettlementUnits = OSC.MinSettlementUnits
		, CarrierMinSettlementUOM = CUOM.Abbrev
		, OA.OverrideCarrierMinSettlementUnits
		, ShipperMinSettlementUnits = OSS.MinSettlementUnits
		, ShipperMinSettlementUOM = CUOM.Abbrev
		, OA.OverrideShipperMinSettlementUnits
		
		, OA.CreateDateUTC
		, OA.CreatedByUser
		
		, OrderID = OSC.OrderID | OSS.OrderID -- NULL if either are NULL - if NULL indicates that the order is not yet rates (used below to rate these)
	FROM viewOrderLocalDates O
	OUTER APPLY dbo.fnOrderRerouteData(O.ID) ORD
	LEFT JOIN tblOrderReroute ORE ON ORE.OrderID = O.ID
	LEFT JOIN tblOrderTransfer OT ON OT.OrderID = O.ID
	LEFT JOIN tblOrderSettlementCarrier OSC ON OSC.OrderID = O.ID
	LEFT JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = O.ID
	LEFT JOIN tblOrderSettlementProducer OSP ON OSP.OrderID = O.ID
	LEFT JOIN viewOriginWaitReason OWR ON OWR.ID = O.OriginWaitReasonID
	LEFT JOIN viewDestinationWaitReason DWR ON DWR.ID = O.DestWaitReasonID
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
	LEFT JOIN viewOrderSettlementUnitsCarrier OSUC ON OSUC.OrderID = O.ID
	LEFT JOIN viewOrderSettlementUnitsShipper OSUS ON OSUS.OrderID = O.ID
	LEFT JOIN tblUom CUOM ON CUOM.ID = OSUC.MinSettlementUomID
	LEFT JOIN tblUom SUOM ON SUOM.ID = OSUS.MinSettlementUomID
	WHERE O.StatusID IN (4) /* only include orders that are marked AUDITED */
	  AND OSC.BatchID IS NULL AND OSS.BatchID IS NULL AND OSP.BatchID IS NULL /* don't show orders that have been settled */
GO


/*************************************************
- Date Created: 16 Feb 2015
- Author: Kevin Alons
- Purpose: return the data used by the Un-Audit MVC page
- Changes:
-		JAE - 11/10/2015 - Used origin driver instead of (destination) driver (3.9.25)
-       3.10.4 - JAE+KDA - 02/29/2016 - Filtered carrier settled orders as well
-       3.11.17.2 - JAE - 04/18/2016 - Filtered producer settled orders
-       3.11.19 - BB - 05/03/2016 - Add Job number and contract number
*************************************************/
ALTER VIEW [dbo].[viewOrderUnaudit] AS
	SELECT O.ID
		, OrderNum
		, OrderDate = dbo.fnDateMdYY(O.OrderDate)
		, Status = OrderStatus
		, Rejected
		, DispatchConfirmNum
		, JobNumber
		, ContractNumber
		, Customer
		, Carrier
		, OriginDriver
		, Origin
		, OriginGrossUnits
		, OriginNetUnits
		, Destination 
	FROM viewOrder O 
	LEFT JOIN tblOrderSettlementCarrier OSC ON OSC.OrderID = O.ID 
	LEFT JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = O.ID 
	LEFT JOIN tblOrderSettlementProducer OSP ON OSS.OrderID = O.ID 
	WHERE O.StatusID=4 
		AND O.DeleteDateUTC IS NULL 
		AND OSC.BatchID IS NULL
		AND OSS.BatchID IS NULL 
		AND OSP.BatchID IS NULL
GO


EXEC sp_refreshview viewOrderUnaudit
GO


/* ---------------SP'S-----------------*/

/***********************************
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: create new Order (loads) for the specified criteria
-- Changes:
	3.9.12 - 2015/09/01 - KDA - adding in @DispatchNotes parameter (this must have been placed here previously by Joe?)
	3.11.19 - 2016/04/29 - BB - Adding Job number and contract number fields
***********************************/
ALTER PROCEDURE [dbo].[spCreateLoads]
(
  @OriginID int
, @DestinationID int
, @TicketTypeID int
, @DueDate datetime
, @CustomerID int
, @CarrierID int = NULL
, @DriverID int = NULL
, @Qty int
, @UserName varchar(100)
, @OriginTankID int = NULL
, @OriginTankNum varchar(20) = NULL
, @OriginBOLNum varchar(15) = NULL
, @PriorityID int = 3 -- LOW priority
, @StatusID smallint = -10 -- GENERATED
, @ProductID int = 1 -- basic Crude Oil
, @DispatchConfirmNum varchar(30) = NULL
, @JobNumber varchar(20) = NULL
, @ContractNumber varchar(20) = NULL
, @DispatchNotes varchar(500) = NULL
, @IDs_CSV varchar(max) = NULL OUTPUT
, @count int = 0 OUTPUT
) AS
BEGIN

	DECLARE @PumperID int, @OperatorID int, @ProducerID int, @OriginUomID int, @DestUomID int
	SELECT @PumperID = PumperID, @OperatorID = OperatorID, @ProducerID = ProducerID, @OriginUomID = UomID, @count = 0
	FROM tblOrigin 
	WHERE ID = @OriginID
	SELECT @DestUomID = UomID FROM tblDestination WHERE ID = @DestinationID

	-- ensure that a driver is never assigned without a CarrierID
	IF (@DriverID IS NOT NULL AND @CarrierID IS NULL)
		SELECT @CarrierID = CarrierID FROM tblDriver WHERE ID = @DriverID
		
	DECLARE @incrementBOL bit
	SELECT @incrementBOL = CASE WHEN @OriginBOLNum LIKE '%+' THEN 1 ELSE 0 END
	IF (@incrementBOL = 1) SELECT @OriginBOLNum = left(@OriginBOLNum, len(@OriginBOLNum) - 1)
	
	DECLARE @i int, @id int
	SELECT @i = 0, @IDs_CSV = ''
	
	WHILE @i < @Qty BEGIN
		
		INSERT INTO dbo.tblOrder (OriginID, OriginUomID, DestinationID, DestUomID, TicketTypeID, DueDate, CustomerID
				, CarrierID, DriverID, StatusID, PriorityID, OriginTankID, OriginTankNum, OriginBOLNum
				, OrderNum, ProductID, PumperID, OperatorID, ProducerID, DispatchConfirmNum, JobNumber, ContractNumber, DispatchNotes
				, CreateDateUTC, CreatedByUser)
			VALUES (@OriginID, @OriginUomID, @DestinationID, @DestUomID, @TicketTypeID, @DueDate, @CustomerID
				, @CarrierID, @DriverID, @StatusID, @PriorityID, @OriginTankID, @OriginTankNum, @OriginBOLNum
				, (SELECT isnull((SELECT max(OrderNum) FROM tblOrder), 100000) + 1), @ProductID
				, @PumperID, @OperatorID, @ProducerID, @DispatchConfirmNum, @JobNumber, @ContractNumber, @DispatchNotes
				, GETUTCDATE(), @UserName)
		
		SELECT @IDs_CSV = @IDs_CSV + ',' + LTRIM(SCOPE_IDENTITY()), @count = @count + 1
		
		IF (@incrementBOL = 1)
		BEGIN
			IF (isnumeric(@OriginBOLNum) = 1) SELECT @OriginBOLNum = rtrim(cast(@OriginBOLNum as bigint) + 1)
		END

		SET @i = @i + 1
	END
	IF (@DriverID IS NOT NULL)
	BEGIN
		UPDATE tblOrder SET TruckID = D.TruckID, TrailerID = D.TrailerID, Trailer2ID = D.Trailer2ID
		FROM tblOrder O
		JOIN tblDriver D ON D.ID = O.DriverID
		WHERE O.ID IN (SELECT ID FROM dbo.fnSplitCSVIDs(@IDs_CSV))
	END
	SET @IDs_CSV = substring(@IDs_CSV, 2, 8000)
END
GO



/***********************************
-- Date Created: 18 Apr 2015
-- Author: Kevin Alons
-- Purpose: create new Gauger Order (loads) for the specified criteria
-- Changes:
	- 3.8.11 - 2015/07/28 - KDA - add explicit @ShipperID parameter (Customer no longer an field of tblOrigin
	- 3.11.19 - 2016/04/29 - BB - Adding Job number and contract number fields
***********************************/
ALTER PROCEDURE [dbo].[spCreateGaugerLoads]
(
  @OriginID int
, @ShipperID int
, @DestinationID int
, @GaugerTicketTypeID int
, @DueDate datetime
, @OriginTankID int
, @ProductID int
, @UserName varchar(100)
, @GaugerID int = NULL
, @Qty int = 1
, @PriorityID int = 3 -- LOW priority
, @DispatchConfirmNum varchar(30) = NULL
, @JobNumber varchar(20) = NULL
, @ContractNumber varchar(20) = NULL
, @IDs_CSV varchar(max) = NULL OUTPUT
, @count int = 0 OUTPUT
) AS
BEGIN
	DECLARE @TicketTypeID int
	SELECT @TicketTypeID = TicketTypeID FROM tblGaugerTicketType WHERE ID = @GaugerTicketTypeID
	EXEC spCreateLoads @StatusID=-9, @OriginID=@OriginID, @OriginTankID=@OriginTankID, @ProductID=@ProductID, @DestinationID=@DestinationID
		, @CustomerID=@ShipperID, @PriorityID=@PriorityID, @TicketTypeID=@TicketTypeID, @DispatchConfirmNum=@DispatchConfirmNum
		, @JobNumber=@JobNumber, @ContractNumber=@ContractNumber
		, @DueDate=@DueDate, @Qty=@Qty, @UserName=@UserName
		, @IDs_CSV=@IDs_CSV OUTPUT
		, @count=@count OUTPUT
	
	INSERT INTO tblGaugerOrder (OrderID, TicketTypeID, StatusID, OriginTankID, OriginTankNum, GaugerID
		, Rejected, Handwritten, DueDate, PriorityID, CreateDateUTC, CreatedByUser)
		SELECT ID, @GaugerTicketTypeID, CASE WHEN @GaugerID IS NULL THEN 1 ELSE 2 END, @OriginTankID, NULL, @GaugerID
			, 0, 0, @DueDate, @PriorityID, getutcdate(), @UserName
		FROM tblOrder 
		WHERE ID IN (SELECT ID FROM dbo.fnSplitCSVIDs(@IDs_CSV))
END
GO



/***********************************
Date Created: 9 Mar 2013
Author: Kevin Alons
Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
Changes:
- 3.7.31	- 2015/06/19 - KDA	- reorder input parameters (move ProviderID down) and add @DriverGroupID parameter
- 3.9.0		- 2/15/08/14 - KDA	- return Approved column
- 3.9.19.6	- 2015/09/30 - KDA	- add OverrideXXX fields from OrderApproval table
- 3.9.21	- 2015/10/03 - KDA	- use [viewOrder] O.OriginDriverGroupID instead of O.DriverGroupID (which could be DestDriver.DriverGroupID)
- 3.9.34	- 2015/12/16 - BB	- Remove deleted orders from the results (DCWEB-851)
- 3.10.13.6 - 2015/03/01 - JAE	- add TruckTypeID
- 3.11.3.1	- 2016/03/22 - KDA	- fix to no-op when @BatchID = 0
- 3.11.19 - 2016/05/02 - BB - Add Job number and Contract number
***********************************/
ALTER PROCEDURE [dbo].[spRetrieveOrdersFinancialCarrier]
(
  @StartDate datetime = NULL -- will default to first day of 3rd prior month for efficiency sake
, @EndDate datetime = NULL
, @ShipperID int = -1 -- all
, @CarrierID int = -1 -- all
, @ProductGroupID int = -1 -- all 
, @TruckTypeID int = -1 -- all
, @DriverGroupID int = -1 -- all
, @OriginStateID int = -1 -- all 
, @DestStateID int = -1 -- all
, @ProducerID int = -1 -- all
, @BatchID int = NULL  -- either show unbatched (NULL), or the specified batch orders
, @OnlyShipperSettled bit = 0 
, @JobNumber varchar(20) = NULL -- 3.11.19
, @ContractNumber varchar(20) = NULL -- 3.11.19
) AS BEGIN

	SELECT @StartDate = dbo.fnDateOnly(isnull(@StartDate, dateadd(month, -3, dbo.fnFirstDOM(getdate())))), @EndDate = dbo.fnDateOnly(isnull(@EndDate, dateadd(day, 1, getdate())))
	
	SELECT DISTINCT OE.* 
		, ShipperSettled = cast(CASE WHEN OSC.BatchID IS NOT NULL THEN 1 ELSE 0 END as bit)
		, InvoiceOtherDetailsTSV = dbo.fnOrderCarrierAssessorialDetailsTSV(OE.ID, 0)
		, InvoiceOtherAmountsTSV = dbo.fnOrderCarrierAssessorialAmountsTSV(OE.ID, 0)
		, Approved = cast(ISNULL(OA.Approved, 0) as bit)
		, FinalActualMiles = ISNULL(OA.OverrideActualMiles, OE.ActualMiles)
		, FinalOriginMinutes = ISNULL(OA.OverrideOriginMinutes, OE.OriginMinutes)
		, FinalDestMinutes = ISNULL(OA.OverrideDestMinutes, OE.DestMinutes)
		, FinalH2S = cast(CASE WHEN isnull(OA.OverrideH2S, 0) = 1 THEN 0 ELSE OE.H2S END as bit)
		, FinalChainup = cast(CASE WHEN isnull(OA.OverrideChainup, 0) = 1 THEN 0 ELSE OE.Chainup END as bit)
		, FinalRerouteCount = CASE WHEN isnull(OA.OverrideReroute, 0) = 1 THEN 0 ELSE OE.RerouteCount END
	FROM dbo.viewOrder_Financial_Carrier OE
	LEFT JOIN tblOrderSettlementShipper OSC ON OSC.OrderID = OE.ID AND OSC.BatchID IS NOT NULL
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = OE.ID
	WHERE ID IN (
		SELECT O.ID
		FROM viewOrder O
		JOIN tblProduct P ON P.ID = O.ProductID
		JOIN tblTruck T ON T.ID = O.OriginTruckID
		JOIN tblOrigin OO ON OO.ID = O.OriginID
		JOIN tblDestination D ON D.ID = O.DestinationID
		LEFT JOIN tblOrderSettlementCarrier OSC ON OSC.OrderID = O.ID
		WHERE O.StatusID IN (4)  
		  AND O.DeleteDateUTC IS NULL  -- 3.9.34
		  AND (@ShipperID=-1 OR O.CustomerID=@ShipperID) 
		  AND (@CarrierID=-1 OR O.CarrierID=@CarrierID) 
		  AND (@ProductGroupID=-1 OR P.ProductGroupID=@ProductGroupID) 
		  AND (@TruckTypeID=-1 OR T.TruckTypeID=@TruckTypeID)
		  AND (@DriverGroupID=-1 OR O.OriginDriverGroupID=@DriverGroupID) 
		  AND (@OriginStateID=-1 OR OO.StateID=@OriginStateID) 
		  AND (@DestStateID=-1 OR D.StateID=@DestStateID) 
		  AND (@ProducerID=-1 OR O.ProducerID=@ProducerID) 
		  AND (@StartDate IS NULL OR O.OrderDate >= @StartDate) 
		  AND (@EndDate IS NULL OR O.OrderDate <= @EndDate)
		  AND (ISNULL(@JobNumber,'') = '' OR O.JobNumber = @JobNumber)  -- 3.11.19
		  AND (ISNULL(@ContractNumber,'') = '' OR O.ContractNumber = @ContractNumber)  -- 3.11.19
		  -- if @BatchID = 0 - retrieve no records , if @BatchID IS NULL - retrieve all to orders eligible for settlement, otherwise retrieve the orders for the specified @BatchID value
		  AND (isnull(@BatchID, -999) <> 0 AND ((@BatchID IS NULL AND OSC.BatchID IS NULL) OR OSC.BatchID = @BatchID))
	)
	  AND (@OnlyShipperSettled = 0 OR OSC.BatchID IS NOT NULL)
	ORDER BY OE.OriginDepartTimeUTC
END
GO


/***********************************
Date Created: 9 Mar 2013
Author: Kevin Alons
Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
Changes
- 3.8.11	- 2015/07/28 - KDA	- remove OriginShipperRegion field
- 3.9.0		- 2/15/08/14 - KDA	- return Approved column
- 3.9.19.6	- 2015/09/30 - KDA	- add OverrideXXX fields from OrderApproval table
- 3.9.34	- 2015/12/16 - BB	- Remove deleted orders from the results (DCWEB-851)
- 3.10.13.6 - 2015/03/01 - JAE	- add TruckTypeID
- 3.11.3.1	- 2016/03/22 - KDA	- fix to no-op when @BatchID = 0
- 3.11.19 - 2016/05/02 - BB - Add Job number and Contract number
***********************************/
ALTER PROCEDURE [dbo].[spRetrieveOrdersFinancialShipper]
(
  @StartDate datetime = NULL
, @EndDate datetime = NULL
, @ShipperID int = -1 -- all customers
, @ProductGroupID int = -1 -- all product groups
, @TruckTypeID int = -1 -- all truck types
, @ProducerID int = -1 -- all
, @OriginStateID int = -1 -- all 
, @DestStateID int = -1 -- all
, @BatchID int = NULL  -- either show unbatched (NULL), or the specified batch orders
, @JobNumber varchar(20) = NULL -- 3.11.19
, @ContractNumber varchar(20) = NULL -- 3.11.19
) AS BEGIN

	SELECT @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	
	SELECT DISTINCT OE.* 
		, InvoiceOtherDetailsTSV = dbo.fnOrderShipperAssessorialDetailsTSV(OE.ID, 0)
		, InvoiceOtherAmountsTSV = dbo.fnOrderShipperAssessorialAmountsTSV(OE.ID, 0)
		, Approved = cast(ISNULL(OA.Approved, 0) as bit)
		, FinalActualMiles = ISNULL(OA.OverrideActualMiles, OE.ActualMiles)
		, FinalOriginMinutes = ISNULL(OA.OverrideOriginMinutes, OE.OriginMinutes)
		, FinalDestMinutes = ISNULL(OA.OverrideDestMinutes, OE.DestMinutes)
		, FinalH2S = cast(CASE WHEN isnull(OA.OverrideH2S, 0) = 1 THEN 0 ELSE OE.H2S END as bit)
		, FinalChainup = cast(CASE WHEN isnull(OA.OverrideChainup, 0) = 1 THEN 0 ELSE OE.Chainup END as bit)
		, FinalRerouteCount = CASE WHEN isnull(OA.OverrideReroute, 0) = 1 THEN 0 ELSE OE.RerouteCount END
	FROM viewOrder_Financial_Shipper OE
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = OE.ID
	WHERE ID IN (
		SELECT O.ID
		FROM viewOrder O
		JOIN tblProduct P ON P.ID = O.ProductID
		JOIN tblTruck T ON T.ID = O.OriginTruckID
		JOIN tblOrigin OO ON OO.ID = O.OriginID
		JOIN tblDestination D ON D.ID = O.DestinationID
		LEFT JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = O.ID
		WHERE O.StatusID IN (4)  
		  AND O.DeleteDateUTC IS NULL  -- 3.9.34
		  AND (@ShipperID=-1 OR O.CustomerID=@ShipperID) 
		  AND (@ProductGroupID=-1 OR P.ProductGroupID=@ProductGroupID) 
		  AND (@TruckTypeID=-1 OR T.TruckTypeID=@TruckTypeID)
		  AND (@ProducerID=-1 OR O.ProducerID=@ProducerID) 
		  AND (@OriginStateID=-1 OR OO.StateID=@OriginStateID) 
		  AND (@DestStateID=-1 OR D.StateID=@DestStateID) 
		  AND (@StartDate IS NULL OR O.OrderDate >= @StartDate) 
		  AND (@EndDate IS NULL OR O.OrderDate <= @EndDate)
		  AND (ISNULL(@JobNumber,'') = '' OR O.JobNumber = @JobNumber)  -- 3.11.19
		  AND (ISNULL(@ContractNumber,'') = '' OR O.ContractNumber = @ContractNumber)  -- 3.11.19
		  -- if @BatchID = 0 - retrieve no records , if @BatchID IS NULL - retrieve all to orders eligible for settlement, otherwise retrieve the orders for the specified @BatchID value
		  AND (isnull(@BatchID, -999) <> 0 AND ((@BatchID IS NULL AND OSS.BatchID IS NULL) OR OSS.BatchID = @BatchID))
	)
	ORDER BY OE.OriginDepartTimeUTC
END 
GO

/* Refresh all views */
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO

/* Rebuild all objects */
EXEC _spRebuildAllObjects
GO

/* Recompile all stored procedures */
EXEC _spRecompileAllStoredProcedures
GO


COMMIT 
SET NOEXEC OFF