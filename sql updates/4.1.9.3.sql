SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.1.9.2'
SELECT  @NewVersion = '4.1.9.3'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Settlement - resolve issue where batched record could get included in a future batch'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/**********************************/
-- Created: 2013/03/09 - ?.?.? - Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
-- Changes:
-- 3.7.31	- 2015/06/19 - KDA	- reorder input parameters (move ProviderID down) and add @DriverGroupID parameter
-- 3.9.0	- 2/15/08/14 - KDA	- return Approved column
-- 3.9.19.6	- 2015/09/30 - KDA	- add OverrideXXX fields from OrderApproval table
-- 3.9.21	- 2015/10/03 - KDA	- use [viewOrder] O.OriginDriverGroupID instead of O.DriverGroupID (which could be DestDriver.DriverGroupID)
-- 3.9.34	- 2015/12/16 - BB	- Remove deleted orders from the results (DCWEB-851)
-- 3.10.13.6 - 2015/03/01 - JAE	- add TruckTypeID
-- 3.11.3.1	- 2016/03/22 - KDA	- fix to no-op when @BatchID = 0
-- 3.11.19	- 2016/05/02 - BB	- Add Job number and Contract number
-- 3.11.20	- 2016/05/04 - JAE	- 3 month limit was always being applied, skip if batch is provided
-- 3.11.20.1 - 2016/05/04 - JAE	- Undoing change as timeouts reoccurring, need to investigate
-- 3.11.20.3 - 2016/05/11 - KDA	- optimize @BatchID filtering to query directly from tblOrderSettlementCarrier table
--								- use tblOrder and minimal JOINs intead of expensive viewOrder
-- 4.1.0	- 2016/08/08 - KDA	- use new @StartSession & @SessionID parameters to persist the retrieved records as a "Session"
--								- stop defaulting @StartDate to 3 months prior if not supplied (null parameter value provided)
--								- use simplified viewOrder_Financial_Carrier for return results - which is now again optimized for "reasonable" performance
-- 4.1.3.5	- 2016/09/17 - BB	- DCWEB-1770: Add reject filter
-- 4.1.8.6	- 2016.09.21 - KDA	- remove dynamic sql and retrieve data in 2 stages (like before) getting the orderID first, then the final data (expensive query)
--								- use new fnTrimToNull() to ensure optional string parameters are trimmed to NULL
-- 4.1.9.3	- 2026.09.28 - KDA	- ensure @StartSession = 0 when @BatchID is not null
/***********************************/
ALTER PROCEDURE spRetrieveOrdersFinancialCarrier
(
  @StartDate date = NULL -- will default to first day of 3rd prior month for efficiency sake
, @EndDate date = NULL
, @ShipperID int = -1 -- all
, @CarrierID int = -1 -- all
, @ProductGroupID int = -1 -- all 
, @TruckTypeID int = -1 -- all
, @DriverGroupID int = -1 -- all
, @OriginStateID int = -1 -- all 
, @DestStateID int = -1 -- all
, @ProducerID int = -1 -- all
, @BatchID int = NULL -- either show unbatched (NULL), or the specified batch orders
, @OnlyShipperSettled bit = 0 
, @JobNumber varchar(20) = NULL -- 3.11.19
, @ContractNumber varchar(20) = NULL -- 3.11.19
, @Rejected int = -1 -- all (rejected + not rejected) -- 4.1.3.5
, @StartSession bit = 1 -- when set to 1, assign the retrieved records to a new Session (ID), when 0 and a sessionID was assigned, then retrieve it
, @SessionID varchar(100) = NULL OUTPUT -- if @startSession = 1, then this return a new value, otherwise will retrieve the existing records in that session
) AS 
BEGIN
	IF (@BatchID IS NOT NULL) SET @StartSession = 0  -- if selecting orders by BatchID, do NOT start a session
	IF (@StartSession IS NULL) SET @StartSession = CASE WHEN @SessionID IS NULL THEN 0 ELSE 1 END

	-- if an empty/blank string parameter is provided, trim it to NULL
	SELECT @JobNumber = dbo.fnTrimToNull(@JobNumber)
		, @ContractNumber = dbo.fnTrimToNull(@ContractNumber)

	-- validate the incoming parameters
	IF (@BatchID IS NOT NULL
		AND (@ShipperID <> -1 
			OR @CarrierID <> -1 
			OR @ProductGroupID <> -1
			OR @TruckTypeID <> -1
			OR @DriverGroupID <> -1
			OR @OriginStateID <> -1
			OR @DestStateID <> -1
			OR @ProducerID <> -1))
	BEGIN
		RAISERROR('@BatchID parameter cannot be specified with other parameters', 16, 1)
		RETURN
	END

	DECLARE @IDs IDTABLE

	-- if a @BatchID value was specified then just use this criteria alone
    IF @BatchID IS NOT NULL
	BEGIN
		INSERT INTO @IDs SELECT OrderID FROM tblOrderSettlementCarrier WHERE BatchID = @BatchID
	END
	ELSE IF (@StartSession = 0 AND @SessionID IS NOT NULL)
	BEGIN
		INSERT INTO @IDs SELECT OrderID FROM tblOrderSettlementSelectionCarrier WHERE SessionID = @SessionID
	END
	ELSE
	BEGIN
		INSERT INTO @IDs 
			SELECT O.ID
			FROM tblOrder O
			JOIN tblProduct P ON P.ID = O.ProductID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			JOIN tblDestination D ON D.ID = O.DestinationID
			JOIN tblTruck T1 ON T1.ID = O.TruckID
			LEFT JOIN dbo.tblOrderTransfer OTR ON OTR.OrderID = O.ID
			LEFT JOIN tblTruck T2 ON T2.ID = OTR.OriginTruckID
			LEFT JOIN dbo.viewDriverBase vODR ON vODR.ID = OTR.OriginDriverID
			LEFT JOIN dbo.viewDriverBase vDDR ON vDDR.ID = O.DriverID
			LEFT JOIN tblOrderSettlementCarrier OS ON OS.OrderID = O.ID
			LEFT JOIN tblOrderSettlementShipper OSP ON OSP.OrderID = O.ID
			WHERE O.StatusID IN (4)  
			  AND O.DeleteDateUTC IS NULL  -- 3.9.34
			  AND OS.BatchID IS NULL -- only get order if it's not part of a batch
			  AND (@ShipperID=-1 OR O.CustomerID=@ShipperID) 
			  AND (@CarrierID=-1 OR O.CarrierID=@CarrierID) 
			  AND (@ProductGroupID=-1 OR P.ProductGroupID=@ProductGroupID) 
			  AND (@TruckTypeID=-1 OR T1.TruckTypeID=@TruckTypeID OR T2.TruckTypeID=@TruckTypeID)
			  AND (@DriverGroupID=-1 OR vODR.DriverGroupID = @DriverGroupID OR vDDR.DriverGroupID = @DriverGroupID) 
			  AND (@ProducerID=-1 OR O.ProducerID=@ProducerID) 
			  AND (@OriginStateID=-1 OR OO.StateID=@OriginStateID) 
			  AND (@DestStateID=-1 OR D.StateID=@DestStateID) 
			  AND (@StartDate IS NULL OR O.OrderDate >= @StartDate) 
			  AND (@EndDate IS NULL OR O.OrderDate <= @EndDate)
			  AND (@OnlyShipperSettled = 0 OR OSP.BatchID IS NOT NULL)
			  AND (@JobNumber IS NULL OR O.JobNumber = @JobNumber)
			  AND (@ContractNumber IS NULL OR O.ContractNumber = @ContractNumber)
			  AND (@Rejected=-1 OR O.Rejected=@Rejected) -- 4.1.3.5
	END

	-- create the temp table to store the data
	SELECT *, RateApplySel = cast(1 as bit), BatchSel = cast(1 as bit), SessionID = cast(NULL AS varchar(100))
	INTO #ret 
	FROM viewOrder_Financial_Carrier
	WHERE 1 = 0
	
	-- get the data into the temp table
	INSERT INTO #ret
		SELECT O.*
			, OS.RateApplySel, OS.BatchSel, OS.SessionID
		FROM viewOrder_Financial_Carrier O
		LEFT JOIN tblOrderSettlementSelectionCarrier OS ON OS.OrderID = O.ID
		WHERE O.OrderID IN (SELECT ID FROM @IDs)
	
	BEGIN TRAN RetrieveOrdersFinancialCarrier

	-- do the SessionID logic
	IF (@StartSession = 1)
	BEGIN
		DELETE FROM tblOrderSettlementSelectionCarrier WHERE OrderID IN (SELECT ID FROM #ret) OR SessionID IN (SELECT SessionID FROM #ret)
		SELECT @sessionID = isnull(max(SessionID), 0) + 1 FROM tblOrderSettlementSelectionCarrier
		UPDATE #ret SET SessionID = @sessionID, RateApplySel = 1, BatchSel = CASE WHEN HasError = 1 THEN 0 ELSE 1 END
		INSERT INTO tblOrderSettlementSelectionCarrier (OrderID, SessionID, RateApplySel, BatchSel)
			SELECT ID, @sessionID, RateApplySel, BatchSel FROM #ret
	END
	
	COMMIT TRAN RetrieveOrdersFinancialCarrier

	-- return the data to the caller
	SELECT * FROM #ret
END

GO

/**********************************/
-- Created: 2013/03/09 - ?.?.? - Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
-- Changes:
-- 4.1.0.2	- 2016.08.28 - KDA	- add some PRINT instrumentation statements for performance debugging purposes
-- 4.1.3.5	- 2016/09/17 - BB	- DCWEB-1770: Add reject filter
-- 4.1.8.6	- 2016.09.21 - KDA	- remove dynamic sql and retrieve data in 2 stages (like before) getting the orderID first, then the final data (expensive query)
--								- use new fnTrimToNull() to ensure optional string parameters are trimmed to NULL
-- 4.1.9.3	- 2026.09.28 - KDA	- ensure @StartSession = 0 when @BatchID is not null
/***********************************/
ALTER PROCEDURE spRetrieveOrdersFinancialDriver
(
  @StartDate date = NULL -- will default to first day of 3rd prior month for efficiency sake
, @EndDate date = NULL
, @ShipperID int = -1 -- all
, @CarrierID int = -1 -- all
, @ProductGroupID int = -1 -- all 
, @TruckTypeID int = -1 -- all
, @DriverGroupID int = -1 -- all
, @DriverID int = -1 -- all
, @OriginStateID int = -1 -- all 
, @DestStateID int = -1 -- all
, @ProducerID int = -1 -- all
, @BatchID int = NULL -- either show unbatched (NULL), or the specified batch orders
, @OnlyCarrierSettled bit = 0 
, @JobNumber varchar(20) = NULL -- 3.11.19
, @ContractNumber varchar(20) = NULL -- 3.11.19
, @Rejected int = -1 -- all (rejected + not rejected) -- 4.1.3.5
, @StartSession bit = 1 -- when set to 1, assign the retrieved records to a new Session (ID), when 0 and a sessionID was assigned, then retrieve it
, @SessionID varchar(100) = NULL OUTPUT -- if @startSession = 1, then this return a new value, otherwise will retrieve the existing records in that session
) AS 
BEGIN
	PRINT 'Start=' + CONVERT(VARCHAR(24), GETDATE(), 121)

	IF (@BatchID IS NOT NULL) SET @StartSession = 0  -- if selecting orders by BatchID, do NOT start a session
	IF (@StartSession IS NULL) SET @StartSession = CASE WHEN @SessionID IS NULL THEN 0 ELSE 1 END

	-- validate the incoming parameters
	IF (@BatchID IS NOT NULL
		AND (@ShipperID <> -1 
			OR @CarrierID <> -1 
			OR @ProductGroupID <> -1
			OR @TruckTypeID <> -1
			OR @DriverGroupID <> -1
			OR @DriverID <> -1
			OR @OriginStateID <> -1
			OR @DestStateID <> -1
			OR @ProducerID <> -1))
	BEGIN
		RAISERROR('@BatchID parameter cannot be specified with other parameters', 16, 1)
		RETURN
	END

	DECLARE @IDs IDTABLE

	-- if a @BatchID value was specified then just use this criteria alone
    IF @BatchID IS NOT NULL
	BEGIN
		INSERT INTO @IDs SELECT OrderID FROM tblOrderSettlementDriver WHERE BatchID = @BatchID
	END
	ELSE IF (@StartSession = 0 AND @SessionID IS NOT NULL)
	BEGIN
		INSERT INTO @IDs SELECT OrderID FROM tblOrderSettlementSelectionDriver WHERE SessionID = @SessionID
	END
	ELSE
	BEGIN
		INSERT INTO @IDs 
			SELECT O.ID
			FROM tblOrder O
			JOIN tblProduct P ON P.ID = O.ProductID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			JOIN tblDestination D ON D.ID = O.DestinationID
			JOIN tblTruck T1 ON T1.ID = O.TruckID
			LEFT JOIN dbo.tblOrderTransfer OTR ON OTR.OrderID = O.ID
			LEFT JOIN tblTruck T2 ON T2.ID = OTR.OriginTruckID
			LEFT JOIN dbo.viewDriverBase vODR ON vODR.ID = OTR.OriginDriverID
			LEFT JOIN dbo.viewDriverBase vDDR ON vDDR.ID = O.DriverID
			LEFT JOIN tblOrderSettlementDriver OS ON OS.OrderID = O.ID
			LEFT JOIN tblOrderSettlementCarrier OSP ON OSP.OrderID = O.ID
			WHERE O.StatusID IN (4)  
			  AND O.DeleteDateUTC IS NULL  -- 3.9.34
			  AND OS.BatchID IS NULL -- only get order if it's not part of a batch
			  AND (@ShipperID=-1 OR O.CustomerID=@ShipperID) 
			  AND (@CarrierID=-1 OR O.CarrierID=@CarrierID) 
			  AND (@ProductGroupID=-1 OR P.ProductGroupID=@ProductGroupID) 
			  AND (@TruckTypeID=-1 OR T1.TruckTypeID=@TruckTypeID OR T2.TruckTypeID=@TruckTypeID)
			  AND (@DriverGroupID=-1 OR vODR.DriverGroupID = @DriverGroupID OR vDDR.DriverGroupID = @DriverGroupID) 
			  AND (@DriverID=-1 OR vODR.ID = @DriverID OR vDDR.ID = @DriverID) 
			  AND (@OriginStateID=-1 OR OO.StateID=@OriginStateID) 
			  AND (@DestStateID=-1 OR D.StateID=@DestStateID) 
			  AND (@ProducerID=-1 OR O.ProducerID=@ProducerID) 
			  AND (@StartDate IS NULL OR O.OrderDate >= @StartDate) 
			  AND (@EndDate IS NULL OR O.OrderDate <= @EndDate)
			  AND (@OnlyCarrierSettled = 0 OR OSP.BatchID IS NOT NULL)
			  AND (@JobNumber IS NULL OR O.JobNumber = @JobNumber)
			  AND (@ContractNumber IS NULL OR O.ContractNumber = @ContractNumber)
			  AND (@Rejected=-1 OR O.Rejected=@Rejected)  -- 4.1.3.5
	END

	-- create the temp table to store the data
	SELECT *, RateApplySel = cast(1 as bit), BatchSel = cast(1 as bit), SessionID = cast(NULL AS varchar(100))
	INTO #ret 
	FROM viewOrder_Financial_Driver
	WHERE 1 = 0
	
	-- get the data into the temp table
	INSERT INTO #ret
		SELECT O.*
			, OS.RateApplySel, OS.BatchSel, OS.SessionID
		FROM viewOrder_Financial_Driver O
		LEFT JOIN tblOrderSettlementSelectionDriver OS ON OS.OrderID = O.ID
		WHERE O.OrderID IN (SELECT ID FROM @IDs)

	-- do the SessionID logic
	IF (@StartSession = 1)
	BEGIN
		PRINT 'SessionCreate:Start=' + CONVERT(VARCHAR(24), GETDATE(), 121)
		BEGIN TRAN RetrieveOrdersFinancialDriver

		DELETE FROM tblOrderSettlementSelectionDriver WHERE OrderID IN (SELECT ID FROM #ret) OR SessionID IN (SELECT SessionID FROM #ret)
		SELECT @sessionID = isnull(max(SessionID), 0) + 1 FROM tblOrderSettlementSelectionDriver
		UPDATE #ret SET SessionID = @sessionID, RateApplySel = 1, BatchSel = CASE WHEN HasError = 1 THEN 0 ELSE 1 END
		INSERT INTO tblOrderSettlementSelectionDriver (OrderID, SessionID, RateApplySel, BatchSel)
			SELECT ID, @sessionID, RateApplySel, BatchSel FROM #ret
	
		COMMIT TRAN RetrieveOrdersFinancialDriver
		PRINT 'SessionCreate:Finish=' + CONVERT(VARCHAR(24), GETDATE(), 121)
	END
	
	-- return the data to the caller
	SELECT * FROM #ret

	PRINT 'Finish=' + CONVERT(VARCHAR(24), GETDATE(), 121)
END

GO

/***********************************/
-- Created: ?.?.? - 2013/03/09 - Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
-- Changes:
-- 3.8.11	- 2015/07/28 - KDA	- remove OriginShipperRegion field
-- 3.9.0	- 2/15/08/14 - KDA	- return Approved column
-- 3.9.19.6	- 2015/09/30 - KDA	- add OverrideXXX fields from OrderApproval table
-- 3.9.34	- 2015/12/16 - BB	- Remove deleted orders from the results (DCWEB-851)
-- 3.10.13.6 - 2015/03/01 - JAE	- add TruckTypeID
-- 3.11.3.1	- 2016/03/22 - KDA	- fix to no-op when @BatchID = 0
-- 3.11.19	- 2016/05/02 - BB	- Add Job number and Contract number
-- 3.11.20.3 - 2016/05/11 - KDA	- optimize @BatchID filtering to query directly from tblOrderSettlementShipper table
--								- use tblOrder and minimal JOINs intead of expensive viewOrder
-- 4.1.0	- 2016/08/08 - KDA	- use new @StartSession & @SessionID parameters to persist the retrieved records as a "Session"
--								- stop defaulting @StartDate to 3 months prior if not supplied (null parameter value provided)
--								- use simplified viewOrder_Financial_Shipper for return results - which is now again optimized for "reasonable" performance
-- 4.1.3.5	- 2016/09/17 - BB	- DCWEB-1770: Add reject filter
-- 4.1.8.6	- 2016.09.21 - KDA	- remove dynamic sql and retrieve data in 2 stages (like before) getting the orderID first, then the final data (expensive query)
--								- use new fnTrimToNull() to ensure optional string parameters are trimmed to NULL
-- 4.1.9.3	- 2026.09.28 - KDA	- ensure @StartSession = 0 when @BatchID is not null
/***********************************/
ALTER PROCEDURE spRetrieveOrdersFinancialShipper
(
  @StartDate date = NULL -- will default to first day of 3rd prior month for efficiency sake
, @EndDate date = NULL
, @ShipperID int = -1 -- all
, @ProductGroupID int = -1 -- all 
, @TruckTypeID int = -1 -- all
, @OriginStateID int = -1 -- all 
, @DestStateID int = -1 -- all
, @ProducerID int = -1 -- all
, @BatchID int = NULL -- either show unbatched (NULL), or the specified batch orders
, @JobNumber varchar(20) = NULL -- 3.11.19
, @ContractNumber varchar(20) = NULL -- 3.11.19
, @Rejected int = -1 -- all (rejected + not rejected) -- 4.1.3.5
, @StartSession bit = 1 -- when set to 1, assign the retrieved records to a new Session (ID), when 0 and a sessionID was assigned, then retrieve it
, @SessionID varchar(100) = NULL OUTPUT -- if @startSession = 1, then this return a new value, otherwise will retrieve the existing records in that session
) AS 
BEGIN
	IF (@BatchID IS NOT NULL) SET @StartSession = 0  -- if selecting orders by BatchID, do NOT start a session
	IF (@StartSession IS NULL) SET @StartSession = CASE WHEN @SessionID IS NULL THEN 0 ELSE 1 END

	-- if an empty/blank string parameter is provided, trim it to NULL
	SELECT @JobNumber = dbo.fnTrimToNull(@JobNumber)
		, @ContractNumber = dbo.fnTrimToNull(@ContractNumber)

	-- validate the incoming parameters
	IF (@BatchID IS NOT NULL
		AND (@ShipperID <> -1 
			OR @ProductGroupID <> -1
			OR @TruckTypeID <> -1
			OR @OriginStateID <> -1
			OR @DestStateID <> -1
			OR @ProducerID <> -1))
	BEGIN
		RAISERROR('@BatchID parameter cannot be specified with other parameters', 16, 1)
		RETURN
	END

	DECLARE @IDs IDTABLE

	-- if a @BatchID value was specified then just use this criteria alone
    IF @BatchID IS NOT NULL
	BEGIN
		INSERT INTO @IDs SELECT OrderID FROM tblOrderSettlementShipper WHERE BatchID = @BatchID
	END
	ELSE IF (@StartSession = 0 AND @SessionID IS NOT NULL)
	BEGIN
		INSERT INTO @IDs SELECT OrderID FROM tblOrderSettlementSelectionShipper WHERE SessionID = @SessionID
	END
	ELSE
	BEGIN
		INSERT INTO @IDs 
			SELECT O.ID
			FROM tblOrder O
			JOIN tblProduct P ON P.ID = O.ProductID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			JOIN tblDestination D ON D.ID = O.DestinationID
			JOIN tblTruck T1 ON T1.ID = O.TruckID
			LEFT JOIN dbo.tblOrderTransfer OTR ON OTR.OrderID = O.ID
			LEFT JOIN tblTruck T2 ON T2.ID = OTR.OriginTruckID
			LEFT JOIN tblOrderSettlementShipper OS ON OS.OrderID = O.ID
			WHERE O.StatusID IN (4)  
			  AND O.DeleteDateUTC IS NULL  -- 3.9.34
			  AND OS.BatchID IS NULL -- only get order if it's not part of a batch
			  AND (@ShipperID=-1 OR O.CustomerID=@ShipperID) 
			  AND (@ProductGroupID=-1 OR P.ProductGroupID=@ProductGroupID) 
			  AND (@TruckTypeID=-1 OR T1.TruckTypeID=@TruckTypeID OR T2.TruckTypeID=@TruckTypeID)
			  AND (@ProducerID=-1 OR O.ProducerID=@ProducerID) 
			  AND (@OriginStateID=-1 OR OO.StateID=@OriginStateID) 
			  AND (@DestStateID=-1 OR D.StateID=@DestStateID) 
			  AND (@StartDate IS NULL OR O.OrderDate >= @StartDate) 
			  AND (@EndDate IS NULL OR O.OrderDate <= @EndDate)
			  AND (@JobNumber IS NULL OR O.JobNumber = @JobNumber)
			  AND (@ContractNumber IS NULL OR O.ContractNumber = @ContractNumber)
			  AND (@Rejected=-1 OR O.Rejected=@Rejected) -- 4.1.3.5
	END
	
	-- create the temp table to store the data
	SELECT *, RateApplySel = cast(1 as bit), BatchSel = cast(1 as bit), SessionID = cast(NULL AS varchar(100))
	INTO #ret 
	FROM viewOrder_Financial_Shipper 
	WHERE 1 = 0
	
	-- get the data into the temp table
	INSERT INTO #ret
		SELECT O.*
			, OS.RateApplySel, OS.BatchSel, OS.SessionID
		FROM viewOrder_Financial_Shipper O
		LEFT JOIN tblOrderSettlementSelectionShipper OS ON OS.OrderID = O.ID
		WHERE O.OrderID IN (SELECT ID FROM @IDs)

	-- do the SessionID logic
	IF (@StartSession = 1)
	BEGIN
		BEGIN TRAN RetrieveOrdersFinancialShipper
	
		DELETE FROM tblOrderSettlementSelectionShipper WHERE OrderID IN (SELECT ID FROM #ret) OR SessionID IN (SELECT SessionID FROM #ret)
		SELECT @sessionID = isnull(max(SessionID), 0) + 1 FROM tblOrderSettlementSelectionShipper
		UPDATE #ret SET SessionID = @sessionID, RateApplySel = 1, BatchSel = CASE WHEN HasError = 1 THEN 0 ELSE 1 END
		INSERT INTO tblOrderSettlementSelectionShipper (OrderID, SessionID, RateApplySel, BatchSel)
			SELECT ID, @sessionID, RateApplySel, BatchSel FROM #ret
	
		COMMIT TRAN RetrieveOrdersFinancialShipper
	END
	
	-- return the data to the caller
	SELECT * FROM #ret
END

GO

COMMIT
SET NOEXEC OFF