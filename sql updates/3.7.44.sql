-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.7.43.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.7.43.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0
SET NOEXEC OFF
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.7.43'
SELECT  @NewVersion = '3.7.44'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Audit Page: Added error catching rule and message for zero volume loads not marked rejected.'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/**********************************************************
-- Author: Kevin Alons
-- Date Created: 8 Oct 2014
-- Purpose: return the Audit table records (optionally filtered to just the specified driver and/or ordernum)
-- Changes:
	-- 3.7.28 - 2015/06/18 - KDA - add filter to ensure APPROVED orders are not displayed
	-- 3.7.44 - 2015/07/06 - BB - Added error catching rule and message for zero volume loads not marked rejected.
***********************************************************/
ALTER FUNCTION [dbo].[fnOrders_AllTickets_Audit](@carrierID int, @driverID int, @orderNum int, @id int) RETURNS TABLE AS RETURN
SELECT *
	, OriginDistanceText = CASE WHEN OriginGpsArrived = 1 THEN 'Verified' ELSE isnull(ltrim(OriginDistance), 'N/A') END
	, DestDistanceText = CASE WHEN DestGpsArrived = 1 THEN 'Verified' ELSE isnull(ltrim(DestDistance), 'N/A') END
	, HasError = cast(CASE WHEN Errors IS NULL THEN 0 ELSE 1 END as bit)
FROM (
	SELECT O.* 
		, OriginGpsLatLon = ltrim(DLO.Lat) + ',' + ltrim(DLO.Lon)
		, OriginLatLon = ltrim(OO.LAT) + ',' + ltrim(OO.LON)
		, OriginAccuracyMeters = DLO.SourceAccuracyMeters
		, OriginDistance = CASE WHEN DLO.DistanceToPoint < 0 THEN NULL ELSE DLO.DistanceToPoint END
		, OriginGpsArrived = CASE WHEN DLO.DistanceToPoint IS NOT NULL AND DLO.DistanceToPoint BETWEEN 0 AND OO.GeoFenceRadiusMeters THEN 1 ELSE 0 END
		, DestGpsLatLon = ltrim(DLD.Lat) + ',' + ltrim(DLD.Lon)
		, DestLatLon = ltrim(D.LAT) + ',' + ltrim(D.LON)
		, DestAccuracyMeters = DLD.SourceAccuracyMeters
		, DestDistance = CASE WHEN DLD.DistanceToPoint < 0 THEN NULL ELSE DLD.DistanceToPoint END
		, DestGpsArrived = CASE WHEN DLD.DistanceToPoint IS NOT NULL AND DLD.DistanceToPoint BETWEEN 0 AND D.GeoFenceRadiusMeters THEN 1 ELSE 0 END
		, OriginGrossBBLS = dbo.fnConvertUOM(O.OriginGrossUnits, O.OriginUomID, 1)
		, Errors = 
			nullif(
				SUBSTRING(
					CASE WHEN O.OriginArriveTimeUTC IS NULL THEN ',Missing Pickup Arrival' ELSE '' END
				  + CASE WHEN O.OriginDepartTimeUTC IS NULL THEN ',Missing Pickup Departure' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND O.Tickets IS NULL THEN ',No Active Tickets' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND O.DestArriveTimeUTC IS NULL THEN ',Missing Delivery Arrival' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND O.DestDepartTimeUTC IS NULL THEN ',Missing Delivery Departure' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND O.OriginGrossUnits = 0 AND O.OriginNetUnits = 0 THEN ',No Origin Units are entered. '+CHAR(13)+CHAR(10)+' This may be an unmarked rejected load.' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND dbo.fnConvertUOM(O.OriginGrossUnits, O.OriginUomID, 1) > 320 THEN ',Origin GOV Units out of limits' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND dbo.fnConvertUOM(O.OriginGrossStdUnits, O.OriginUomID, 1) > 320 THEN ',Origin GSV Units out of limits' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND dbo.fnConvertUOM(O.OriginNetUnits, O.OriginUomID, 1) > 320 THEN ',Origin NSV Units out of limits' ELSE '' END				  
				  + CASE WHEN dbo.fnToBool(OOR.Value) = 1 AND (SELECT count(*) FROM tblOrderTicket OT WHERE OrderID = O.ID AND OT.DeleteDateUTC IS NULL AND OT.DispatchConfirmNum IS NULL) > 0 THEN ',Missing Ticket Shipper PO' ELSE '' END
				  + CASE WHEN EXISTS(SELECT CustomerID FROM viewOrder_OrderTicket_Full WHERE ID = O.ID AND DeleteDateUTC IS NULL AND nullif(rtrim(T_DispatchConfirmNum), '') IS NOT NULL GROUP BY CustomerID, T_DispatchConfirmNum HAVING COUNT(*) > 1) THEN ',Duplicate Shipper PO' ELSE '' END
				  + CASE WHEN O.TruckID IS NULL THEN ',Truck Missing' ELSE '' END
				  + CASE WHEN O.TrailerID IS NULL THEN ',Trailer 1 Missing' ELSE '' END
				, 2, 8000) 
			, '')
		, IsEditable = cast(CASE WHEN O.StatusID = 3 THEN 1 ELSE 0 END as bit)
	FROM viewOrder_AllTickets O
	JOIN tblOrigin OO ON OO.ID = O.OriginID
	JOIN tblCustomer C ON C.ID = OO.CustomerID
	JOIN tblDestination D ON D.ID = O.DestinationID
	-- ShipperPO_Required OrderRule
	OUTER APPLY (SELECT Value FROM dbo.fnOrderOrderRules(O.ID) WHERE TypeID = 2) OOR
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
	LEFT JOIN tblOrderSettlementShipper OIC ON OIC.OrderID = O.ID
	LEFT JOIN viewDriverLocation_OriginFirstArrive DLO ON DLO.OrderID = O.ID AND DLO.OriginID = O.OriginID
	LEFT JOIN viewDriverLocation_DestinationFirstArrive DLD ON DLD.OrderID = O.ID AND DLD.DestinationID = O.DestinationID
	WHERE OIC.BatchID IS NULL /* don't even show SETTLED orders on the AUDIT page ever */
	  AND nullif(OA.Approved, 0) IS NULL /* don't show Approved orders on the AUDIT page */
	  AND (isnull(@carrierID, 0) = -1 OR O.CarrierID = @carrierID)
	  AND (isnull(@driverID, 0) = -1 OR O.DriverID = @driverID)
	  AND ((nullif(@orderNum, 0) IS NULL AND O.DeleteDateUTC IS NULL AND (O.StatusID = 3 AND DeliverPrintStatusID IN (SELECT ID FROM tblPrintStatus WHERE IsCompleted = 1))) OR O.OrderNum = @orderNum)
	  AND (nullif(@id, 0) IS NULL OR O.ID LIKE @id)
) X

GO


COMMIT 
SET NOEXEC OFF