--select * from tblSetting where ID = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '1.6.1', @NewVersion = '1.6.2'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

/***********************************/
-- Date Created: 4 Apr 2013
-- Author: Kevin Alons
-- Purpose: return Carrier records with translated "friendly" values
/***********************************/
ALTER VIEW [dbo].[viewCarrier] AS
SELECT C.*
	, cast(CASE WHEN C.DeleteDate IS NULL THEN 1 ELSE 0 END as bit) AS Active
	, CT.Name AS CarrierType
	, S.Abbreviation AS StateAbbrev
	, SF.Name AS SettlementFactor 
FROM tblCarrier C 
JOIN tblCarrierType CT ON CT.ID = C.CarrierTypeID
LEFT JOIN tblState S ON S.ID = C.StateID
LEFT JOIN tblSettlementFactor SF ON SF.ID = C.SettlementFactorID

GO

/***********************************/
-- Date Created: 26 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Drivers table records with "friendly" translated values included
/***********************************/
ALTER VIEW [dbo].[viewDriver] AS
SELECT *
	, CASE WHEN DeleteDate IS NULL THEN '' ELSE 'Deleted: ' END + FullName AS FullNameD
FROM (
	SELECT D.*
	, cast(CASE WHEN isnull(C.DeleteDate, D.DeleteDate) IS NULL THEN 1 ELSE 0 END as bit) AS Active
	, D.FirstName + ' ' + D.LastName As FullName
	, D.LastName + ', ' + D.FirstName AS FullNameLF
	, isnull(CT.Name, 'Unknown') AS CarrierType
	, C.Name AS Carrier
	, S.Abbreviation AS StateAbbrev 
	, T.FullName AS Truck
	, T1.FullName AS Trailer
	, T2.FullName AS Trailer2
	FROM dbo.tblDriver D 
	JOIN dbo.tblCarrier C ON C.ID = D.CarrierID 
	LEFT JOIN dbo.tblCarrierType CT ON CT.ID = C.CarrierTypeID
	LEFT JOIN tblState S ON S.ID = D.StateID
	LEFT JOIN viewTruck T ON T.ID = D.TruckID
	LEFT JOIN viewTrailer T1 ON T1.ID = D.TrailerID
	LEFT JOIN viewTrailer T2 ON T2.ID = D.Trailer2ID
) X

GO

/***********************************/
-- Date Created: 26 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Trailer records with FullName & translated "friendly" values
/***********************************/
ALTER VIEW [dbo].[viewTrailer] AS
SELECT T.*
	, cast(CASE WHEN isnull(C.DeleteDate, T.DeleteDate) IS NULL THEN 1 ELSE 0 END as bit) AS Active
	, ISNULL(Compartment1Barrels, 0) 
		+ ISNULL(Compartment2Barrels, 0) 
		+ ISNULL(Compartment3Barrels, 0) 
		+ ISNULL(Compartment4Barrels, 0) 
		+ ISNULL(Compartment5Barrels, 0) AS TotalCapacityBarrels
	, CASE WHEN isnull(Compartment1Barrels, 0) > 0 THEN 1 ELSE 0 END 
		+ CASE WHEN isnull(Compartment2Barrels, 0) > 0 THEN 1 ELSE 0 END 
		+ CASE WHEN isnull(Compartment3Barrels, 0) > 0 THEN 1 ELSE 0 END
		+ CASE WHEN isnull(Compartment4Barrels, 0) > 0 THEN 1 ELSE 0 END 
		+ CASE WHEN isnull(Compartment5Barrels, 0) > 0 THEN 1 ELSE 0 END AS CompartmentCount
	, T.IDNumber AS FullName
	, TT.Name AS TrailerType
	, isnull(CT.Name, 'Unknown') AS CarrierType
	, C.Name AS Carrier
FROM dbo.tblTrailer T
LEFT JOIN dbo.tblTrailerType TT ON TT.ID = T.TrailerTypeID
JOIN dbo.tblCarrier C ON C.ID = T.CarrierID
LEFT JOIN dbo.tblCarrierType CT ON CT.ID = C.CarrierTypeID

GO

/***********************************/
-- Date Created: 26 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Truck records with FullName & translated "friendly" values
/***********************************/
ALTER VIEW [dbo].[viewTruck] AS
SELECT T.*
	, cast(CASE WHEN isnull(C.DeleteDate, T.DeleteDate) IS NULL THEN 1 ELSE 0 END as bit) AS Active
	, T.IDNumber AS FullName
	, isnull(CT.Name, 'Unknown') AS CarrierType
	, C.Name AS Carrier
FROM dbo.tblTruck T
JOIN dbo.tblCarrier C ON C.ID = T.CarrierID
LEFT JOIN dbo.tblCarrierType CT ON CT.ID = C.CarrierTypeID

GO

/***********************************/
-- Date Created: 5 Aug 2013
-- Author: Kevin Alons
-- Purpose: return Trailer Inspection records with translated "friendly" values + missing required records
/***********************************/
ALTER VIEW [dbo].[viewTrailerInspection] AS
SELECT TI.ID
	, T.ID AS TrailerID
	, TIT.ID AS TrailerInspectionTypeID
	, TI.InspectionDate
	, TI.ExpirationDate
	, TI.Document
	, TI.DocName
	, TI.Notes
	, TI.CreateDate
	, TI.CreatedByUser
	, TI.LastChangeDate
	, TI.LastChangedByUser
	, CASE WHEN TI.ExpirationDate IS NULL THEN 'Missing' WHEN TI.ExpirationDate < GETDATE() THEN 'Overdue' ELSE 'Current' END AS Status
	, TIT.Name AS InspectionType
	, TIT.Required
	, TIT.ExpirationMonths
	, T.IdNumber
	, T.VIN
	, T.CarrierID
	, T.Carrier
	, T.CarrierType
	, T.TrailerType
FROM viewTrailer T
CROSS JOIN tblTrailerInspectionType TIT
LEFT JOIN tblTrailerInspection TI ON TI.TrailerID = T.ID AND TI.TrailerInspectionTypeID = TIT.ID
WHERE TIT.Required = 1
  -- include any existing Inspection records or those missing required inspections
  AND (TIT.Required = 1 OR TI.ID IS NOT NULL)
  AND T.Active = 1
  and TIT.DeleteDate IS NULL

GO

/***********************************/
-- Date Created: 8 Aug 2013
-- Author: Kevin Alons
-- Purpose: return Trailer Maintenance records with translated "friendly" values
/***********************************/
ALTER VIEW [dbo].[viewTrailerMaintenance] AS
SELECT TM.ID
	, T.ID AS TrailerID
	, TM.MaintenanceDate
	, TM.Description
	, TM.Document
	, TM.DocName
	, TM.Notes
	, TM.CreateDate
	, TM.CreatedByUser
	, TM.LastChangeDate
	, TM.LastChangedByUser
	, T.IdNumber
	, T.VIN
	, T.CarrierID
	, T.Carrier
	, T.CarrierType
	, T.TrailerType
FROM viewTrailer T
JOIN tblTrailerMaintenance TM ON TM.TrailerID = T.ID
WHERE T.Active = 1

GO

/***********************************/
-- Date Created: 5 Aug 2013
-- Author: Kevin Alons
-- Purpose: return Truck Inspection records with translated "friendly" values + missing required records
/***********************************/
ALTER VIEW [dbo].[viewTruckInspection] AS
SELECT TI.ID
	, T.ID AS TruckID
	, TIT.ID AS TruckInspectionTypeID
	, TI.InspectionDate
	, TI.ExpirationDate
	, TI.Document
	, TI.DocName
	, TI.Notes
	, TI.CreateDate
	, TI.CreatedByUser
	, TI.LastChangeDate
	, TI.LastChangedByUser
	, CASE WHEN TI.ExpirationDate IS NULL THEN 'Missing' WHEN TI.ExpirationDate < GETDATE() THEN 'Overdue' ELSE 'Current' END AS Status
	, TIT.Name AS InspectionType
	, TIT.Required
	, TIT.ExpirationMonths
	, T.IdNumber
	, T.VIN
	, T.CarrierID
	, T.Carrier
	, T.CarrierType
FROM viewTruck T
CROSS JOIN tblTruckInspectionType TIT
LEFT JOIN tblTruckInspection TI ON TI.TruckID = T.ID AND TI.TruckInspectionTypeID = TIT.ID
WHERE TIT.Required = 1
  -- include any existing Inspection records or those missing required inspections
  AND (TIT.Required = 1 OR TI.ID IS NOT NULL)
  AND T.Active = 1
  AND TIT.DeleteDate IS NULL

GO

/***********************************/
-- Date Created: 8 Aug 2013
-- Author: Kevin Alons
-- Purpose: return Truck Maintenance records with translated "friendly" values
/***********************************/
ALTER VIEW [dbo].[viewTruckMaintenance] AS
SELECT TM.ID
	, T.ID AS TruckID
	, TM.MaintenanceDate
	, TM.Description
	, TM.Document
	, TM.DocName
	, TM.Notes
	, TM.CreateDate
	, TM.CreatedByUser
	, TM.LastChangeDate
	, TM.LastChangedByUser
	, T.IdNumber
	, T.VIN
	, T.CarrierID
	, T.Carrier
	, T.CarrierType
FROM viewTruck T
JOIN tblTruckMaintenance TM ON TM.TruckID = T.ID
WHERE T.Active = 1

GO

/***********************************/
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Destination records with translated value and FullName field (which includes Destination Type)
/***********************************/
ALTER VIEW [dbo].[viewDestination] AS
SELECT D.*
	, cast(CASE WHEN D.DeleteDate IS NULL THEN 1 ELSE 0 END as bit) AS Active
	, DT.DestinationType
	, DT.DestinationType + ' - ' + D.Name AS FullName
	, S.FullName AS State, S.Abbreviation AS StateAbbrev
	, R.Name AS Region
	, DTT.Name AS TicketType
FROM dbo.tblDestination D
JOIN dbo.tblDestinationType DT ON DT.ID = D.DestinationTypeID
JOIN dbo.tblDestTicketType DTT ON DTT.ID = D.TicketTypeID
LEFT JOIN tblState S ON S.ID = D.StateID
LEFT JOIN dbo.tblRegion R ON R.ID = D.RegionID

GO

EXEC _spRefreshAllViews
GO

COMMIT
SET NOEXEC OFF