SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.1.8.6'
SELECT  @NewVersion = '4.1.9'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Import Center - do not "find" deleted/inactive entities (but can be edited)'
	UNION SELECT @NewVersion, 1, 'Import Center - new "Comparison Operators": IN, NOT IN'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/*******************************************************/
 -- Created: 3.10.1 - 2016/01/10 - Kevin Alons
 -- Purpose: return all fields + some support fields for tblObject table
 -- Changes:
 -- 4.1.9 - 2016.09.14 - KDA	- add HasActiveField, HasDeleteDateUTCField columns
/*******************************************************/
ALTER VIEW viewObject AS
	SELECT O.*
		, IDFieldName = (SELECT TOP 1 FieldName FROM tblObjectField OBF WHERE OBF.ObjectID = O.ID AND IsKey = 1)
		, HasActiveField = cast(CASE WHEN CA.COLUMN_NAME IS NULL THEN 0 ELSE 1 END as bit) -- 4.1.5
		, HasDeleteDateUTCField = cast(CASE WHEN CD.COLUMN_NAME IS NULL THEN 0 ELSE 1 END as bit) -- 4.1.5
	FROM tblObject O
	LEFT JOIN INFORMATION_SCHEMA.COLUMNS CA ON CA.TABLE_NAME = O.SqlSourceName AND CA.COLUMN_NAME = 'Active'
	LEFT JOIN INFORMATION_SCHEMA.COLUMNS CD ON CD.TABLE_NAME = O.SqlSourceName AND CD.COLUMN_NAME = 'DeleteDateUTC'

GO

insert into tblImportCenterComparisonType (id, name) 
	select 4, 'In'
	union
	select 5, 'Not Equals'
	union 
	select 6, 'Not In'
go

COMMIT
SET NOEXEC OFF