-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.0.17'
SELECT  @NewVersion = '4.1.0'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'DCWEB-945 - Add Independent Driver/Driver Group Settlement'
	UNION SELECT @NewVersion, 1, 'Settlement Enhancements - using paging and independent Rate Apply/Batch selection checkboxes'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

if  OBJECT_ID('tblOrderSettlementSelectionCarrier') IS NOT NULL drop table tblOrderSettlementSelectionCarrier
go
CREATE TABLE tblOrderSettlementSelectionCarrier
(
  OrderID int NOT NULL 
	CONSTRAINT PK_OrderSettlementSelectionCarrier PRIMARY KEY 
	CONSTRAINT FK_OrderSettlementSelectionCarrier_Order FOREIGN KEY REFERENCES tblOrder(ID)
, SessionID varchar(100) NOT NULL 
, RateApplySel bit NOT NULL CONSTRAINT DF_OrderSettlementSelectionCarrier_RateApplySel DEFAULT (1)
, BatchSel bit NOT NULL CONSTRAINT DF_OrderSettlementSelectionCarrier_BatchSel DEFAULT (1)
)
GO

exec _spDropFunction 'fnOrderSettlementUnits'
go
/**************************************************/
-- Created: 2016/08/15 - 4.1.0 - Kevin Alons
-- Purpose: return the order settlement units (based on the specified settlementFactorID parameter value)
-- Changes:
/**************************************************/
CREATE FUNCTION fnOrderSettlementUnits
(
  @SettlementFactorID int
, @OriginGrossUnits decimal(9, 3)
, @OriginNetUnits decimal(9, 3)
, @OriginGrossStdUnits decimal(9, 3)
, @DestGrossUnits decimal(9, 3)
, @DestNetUnits decimal(9, 3)
) RETURNS money AS
BEGIN
	DECLARE @ret money = 0
	
	SELECT @ret = CASE @SettlementFactorID 
		WHEN 1 THEN @OriginGrossUnits
		WHEN 2 THEN @OriginNetUnits
		WHEN 3 THEN @OriginGrossStdUnits
		WHEN 4 THEN	@DestGrossUnits
		WHEN 5 THEN @DestNetUnits 
				ELSE NULL END

	RETURN (@ret)
END
GO
GRANT EXECUTE ON fnOrderSettlementUnits TO role_iis_acct
GO

exec _spDropView 'viewOrder_Financial_Base_Carrier'
go
/***********************************/
-- Created: 4.1.0 - 2016/08/15 - Kevin Alons
-- Purpose: return BASE Order details + Carrier FINANCIAL INFO into a single view
-- Changes:
/***********************************/
CREATE VIEW viewOrder_Financial_Base_Carrier AS 
	SELECT *
		, HasError = cast(CASE WHEN ErrorFieldCSV IS NOT NULL THEN 1 ELSE 0 END as bit)
	FROM (
		SELECT *
			, OrderID = ID
			, ErrorFieldCSV = nullif(substring(CASE WHEN Approved = 0 THEN ',Approved' ELSE '' END
				+ CASE WHEN InvoiceLoadAmount IS NULL THEN ',InvoiceLoadAmount,InvoiceRouteRate,InvoiceRateSheet' ELSE '' END
				+ CASE WHEN FinalChainup = 1 AND InvoiceChainupAmount IS NULL THEN ',InvoiceChainupAmount' ELSE '' END
				+ CASE WHEN FinalRerouteCount > 0 AND InvoiceRerouteAmount IS NULL THEN ',InvoiceRerouteAmount' ELSE '' END
				+ CASE WHEN FinalH2S = 1 AND InvoiceH2SAmount IS NULL THEN ',InvoiceH2SAmount' ELSE '' END
				+ CASE WHEN TicketCount > 1 AND InvoiceSplitLoadAmount IS NULL THEN ',InvoiceSplitLoadAmount' ELSE '' END
				+ CASE WHEN InvoiceOriginWaitBillableMinutes > 0 AND InvoiceOriginWaitAmount IS NULL THEN ',InvoiceOriginWaitAmount' ELSE '' END
				+ CASE WHEN InvoiceDestinationWaitBillableMinutes > 0 AND InvoiceDestinationWaitAmount IS NULL THEN ',InvoiceDestinationWaitAmount' ELSE '' END
				+ CASE WHEN Rejected = 1 AND InvoiceOrderRejectAmount IS NULL THEN ',InvoiceOrderRejectAmount' ELSE '' END
				+ CASE WHEN InvoiceMinSettlementUnitsID IS NULL THEN ',InvoiceMinSettlementUnits' ELSE '' END
				+ CASE WHEN InvoiceSettlementFactorID IS NULL THEN ',InvoiceSettlementFactor' ELSE '' END
				+ CASE WHEN Rejected = 0 AND InvoiceSettlementFactorID IS NULL THEN ',InvoiceSettlementFactor' ELSE '' END
				+ CASE WHEN Rejected = 0 AND InvoiceSettlementFactorID IS NOT NULL AND dbo.fnOrderSettlementUnits(InvoiceSettlementFactorID, OriginGrossUnits, OriginNetUnits, OriginGrossStdUnits, DestGrossUnits, DestNetUnits) IS NULL THEN ','+OrderUnitsField ELSE '' END
				+ CASE WHEN InvoiceWaitFeeParameterID IS NULL THEN ',InvoiceOriginWaitBillableMinutes,InvoiceDestinationWaitBillableMinutes' ELSE '' END
				+ CASE WHEN isnull(ActualMiles, 0) = 0 THEN ',ActualMiles' ELSE '' END, 2, 100000), '')
		FROM (
			SELECT O.* 
				, InvoiceRatesAppliedDateUTC = OS.CreateDateUTC
				, OS.BatchID
				, Approved = cast(ISNULL(OA.Approved, 0) as bit)
				, FinalActualMiles = ISNULL(OA.OverrideActualMiles, O.ActualMiles)
				, FinalOriginMinutes = ISNULL(OA.OverrideOriginMinutes, O.OriginMinutes)
				, FinalDestMinutes = ISNULL(OA.OverrideDestMinutes, O.DestMinutes)
				, FinalH2S = cast(CASE WHEN isnull(OA.OverrideH2S, 0) = 1 THEN 0 ELSE OO.H2S END as bit)
				, FinalChainup = cast(CASE WHEN isnull(OA.OverrideChainup, 0) = 1 THEN 0 ELSE O.Chainup END as bit)
				, FinalRerouteCount = CASE WHEN isnull(OA.OverrideReroute, 0) = 1 THEN 0 ELSE isnull(ORR.RerouteCount, 0) END
				, FC.OrderUnitsField
				, InvoiceBatchNum = OS.BatchNum 
				, InvoiceOriginWaitBillableMinutes = OS.OriginWaitBillableMinutes
				, InvoiceDestinationWaitBillableMinutes = OS.DestinationWaitBillableMinutes
				, InvoiceTotalWaitBillableMinutes = isnull(OS.OriginWaitBillableMinutes, 0) + ISNULL(OS.DestinationWaitBillableMinutes, 0) 
				, InvoiceWaitFeeParameterID = WaitFeeParameterID
				, InvoiceWaitFeeSubUnit = WaitFeeSubUnit
				, InvoiceWaitFeeRoundingType = WaitFeeRoundingType
				, InvoiceOriginWaitRate = OS.OriginWaitRate 
				, InvoiceOriginWaitAmount = OS.OriginWaitAmount 		
				, InvoiceDestinationWaitRate = OS.DestinationWaitRate 
				, InvoiceDestinationWaitAmount = OS.DestinationWaitAmount 
				, InvoiceTotalWaitAmount = OS.TotalWaitAmount
				, InvoiceOrderRejectRate = OS.OrderRejectRate	
				, InvoiceOrderRejectRateType = OS.OrderRejectRateType  
				, InvoiceOrderRejectAmount = OS.OrderRejectAmount  
				, InvoiceChainupRate = OS.ChainupRate
				, InvoiceChainupRateType = OS.ChainupRateType
				, InvoiceChainupAmount = OS.ChainupAmount 
				, InvoiceRerouteRate = OS.RerouteRate
				, InvoiceRerouteRateType = OS.RerouteRateType
				, InvoiceRerouteAmount = OS.RerouteAmount 
				, InvoiceH2SRate = OS.H2SRate
				, InvoiceH2SRateType = OS.H2SRateType
				, InvoiceH2SAmount = OS.H2SAmount
				, InvoiceSplitLoadRate = OS.SplitLoadRate
				, InvoiceSplitLoadRateType = OS.SplitLoadRateType
				, InvoiceSplitLoadAmount = OS.SplitLoadAmount
				, InvoiceOtherAmount = OS.OtherAmount
				, InvoiceTaxRate = OS.OriginTaxRate
				, InvoiceSettlementUom = OS.SettlementUom 
				, InvoiceSettlementUomShort = OS.SettlementUomShort 
				, InvoiceCarrierSettlementFactorID = OS.CarrierSettlementFactorID 
				, InvoiceSettlementFactorID = OS.SettlementFactorID 
				, InvoiceSettlementFactor = OS.SettlementFactor 
				, InvoiceMinSettlementUnitsID = OS.MinSettlementUnitsID 
				, InvoiceMinSettlementUnits = OS.MinSettlementUnits
				, InvoiceUnits = OS.SettlementUnits
				, InvoiceRouteRate = OS.RouteRate
				, InvoiceRouteRateType = OS.RouteRateType
				, InvoiceRateSheetRate = OS.RateSheetRate
				, InvoiceRateSheetRateType = OS.RateSheetRateType
				, InvoiceFuelSurchargeRate = OS.FuelSurchargeRate
				, InvoiceFuelSurchargeAmount = OS.FuelSurchargeAmount
				, InvoiceLoadAmount = OS.LoadAmount
				, InvoiceTotalAmount = OS.TotalAmount
			FROM dbo.viewOrder O
			LEFT JOIN tblOrigin OO ON OO.ID = O.OriginID
			LEFT JOIN viewOrderSettlementCarrier OS ON OS.OrderID = O.ID
			LEFT JOIN (SELECT OrderID, RerouteCount = count(1) FROM tblOrderReroute GROUP BY OrderID) ORR ON ORR.OrderID = O.ID
			LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
			LEFT JOIN tblSettlementFactor FC ON FC.ID = OS.SettlementFactorID
			WHERE O.StatusID NOT IN (-10) -- don't include "Generated" orders (only Assigned+)
		) X
	) X1

GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details + FINANCIAL INFO into a single view
-- Changes:
	-- 3.7.28 - 2015/06/18 - KDA - add new columns: CarrierSettlementFactorID, SettlementFactorID, MinSettlementUnitsID
	-- 4.1.0	2016/08/08	KDA		add Shipper field (renamed "clone" of Customer field) to match viewOrder_Financial_Shipper VIEW
/***********************************/
ALTER VIEW viewOrder_Financial_Carrier AS 
	SELECT O.* 
		, Shipper = isnull(C.Name, 'N/A')
		, ShipperSettled = cast(CASE WHEN OSS.BatchID IS NOT NULL THEN 1 ELSE 0 END as bit)
		, TicketNums = dbo.fnOrderTicketDetails(O.ID, 'TicketNums', '<br/>') 
		, TankNums = dbo.fnOrderTicketDetails(O.ID, 'TankNums', '<br/>') 
		, PreviousDestinations = dbo.fnRerouteDetails(O.ID, 'PreviousDestinations', '<br/>') 
		, RerouteUsers = dbo.fnRerouteDetails(O.ID, 'RerouteUsers', '<br/>') 
		, RerouteDates = dbo.fnRerouteDetails(O.ID, 'RerouteDates', '<br/>') 
		, RerouteNotes = dbo.fnRerouteDetails(O.ID, 'RerouteNotes', '<br/>')
		, InvoiceOtherDetailsTSV = dbo.fnOrderCarrierAssessorialDetailsTSV(O.ID, 0)
		, InvoiceOtherAmountsTSV = dbo.fnOrderCarrierAssessorialAmountsTSV(O.ID, 0)
	FROM dbo.viewOrder_Financial_Base_Carrier O
	LEFT JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = O.ID
	LEFT JOIN tblCustomer C ON C.ID = O.CustomerID
GO

/**********************************/
-- Created: 2013/03/09 - ?.?.? - Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
-- Changes:
-- 3.7.31	- 2015/06/19 - KDA	- reorder input parameters (move ProviderID down) and add @DriverGroupID parameter
-- 3.9.0	- 2/15/08/14 - KDA	- return Approved column
-- 3.9.19.6	- 2015/09/30 - KDA	- add OverrideXXX fields from OrderApproval table
-- 3.9.21	- 2015/10/03 - KDA	- use [viewOrder] O.OriginDriverGroupID instead of O.DriverGroupID (which could be DestDriver.DriverGroupID)
-- 3.9.34	- 2015/12/16 - BB	- Remove deleted orders from the results (DCWEB-851)
-- 3.10.13.6 - 2015/03/01 - JAE	- add TruckTypeID
-- 3.11.3.1	- 2016/03/22 - KDA	- fix to no-op when @BatchID = 0
-- 3.11.19	- 2016/05/02 - BB	- Add Job number and Contract number
-- 3.11.20	- 2016/05/04 - JAE	- 3 month limit was always being applied, skip if batch is provided
-- 3.11.20.1 - 2016/05/04 - JAE	- Undoing change as timeouts reoccurring, need to investigate
-- 3.11.20.3 - 2016/05/11 - KDA	- optimize @BatchID filtering to query directly from tblOrderSettlementCarrier table
--								- use tblOrder and minimal JOINs intead of expensive viewOrder
-- 4.1.0	- 2016/08/08 - KDA	- use new @StartSession & @SessionID parameters to persist the retrieved records as a "Session"
--								- stop defaulting @StartDate to 3 months prior if not supplied (null parameter value provided)
--								- use simplified viewOrder_Financial_Carrier for return results - which is now again optimized for "reasonable" performance
/***********************************/
ALTER PROCEDURE spRetrieveOrdersFinancialCarrier
(
  @StartDate date = NULL -- will default to first day of 3rd prior month for efficiency sake
, @EndDate date = NULL
, @ShipperID int = -1 -- all
, @CarrierID int = -1 -- all
, @ProductGroupID int = -1 -- all 
, @TruckTypeID int = -1 -- all
, @DriverGroupID int = -1 -- all
, @OriginStateID int = -1 -- all 
, @DestStateID int = -1 -- all
, @ProducerID int = -1 -- all
, @BatchID int = NULL -- either show unbatched (NULL), or the specified batch orders
, @OnlyShipperSettled bit = 0 
, @JobNumber varchar(20) = NULL -- 3.11.19
, @ContractNumber varchar(20) = NULL -- 3.11.19
, @StartSession bit = 0 -- when set to 1, assign the retrieved records to a new Session (ID), when 0 and a sessionID was assigned, then retrieve it
, @SessionID varchar(100) = NULL OUTPUT -- if @startSession = 1, then this return a new value, otherwise will retrieve the existing records in that session
) AS 
BEGIN
	IF (@StartSession IS NULL) SET @StartSession = CASE WHEN @SessionID IS NULL THEN 0 ELSE 1 END

	-- validate the incoming parameters
	IF (@BatchID IS NOT NULL
		AND (@ShipperID <> -1 
			OR @CarrierID <> -1 
			OR @ProductGroupID <> -1
			OR @TruckTypeID <> -1
			OR @DriverGroupID <> -1
			OR @OriginStateID <> -1
			OR @DestStateID <> -1
			OR @ProducerID <> -1))
	BEGIN
		RAISERROR('@BatchID parameter cannot be specified with other parameters', 16, 1)
		RETURN
	END

	DECLARE @sql varchar(max) = '
		SELECT O.*
			, OS.RateApplySel, OS.BatchSel, OS.SessionID
		FROM viewOrder_Financial_Carrier O
		LEFT JOIN tblOrderSettlementSelectionCarrier OS ON OS.OrderID = O.ID'

	-- if a @BatchID value was specified then just use this criteria alone
    IF @BatchID IS NOT NULL
	BEGIN
		SET @sql = @sql + '
			WHERE O.BatchID = @BatchID'
		SET @sql = replace(@sql, '@BatchID', @BatchID)
	END
	ELSE IF (@StartSession = 0 AND @SessionID IS NOT NULL)
	BEGIN
		SET @sql = @sql + '
			WHERE O.ID IN (SELECT OrderID FROM tblOrderSettlementSelectionCarrier WHERE SessionID = @SessionID)'
		SET @sql = replace(@sql, '@SessionID', @SessionID)
	END
	ELSE
	BEGIN
		SET @sql = @sql + '
			LEFT JOIN dbo.tblOrderTransfer OTR ON OTR.OrderID = O.ID
			LEFT JOIN dbo.viewDriverBase vODR ON vODR.ID = OTR.OriginDriverID
			LEFT JOIN dbo.viewDriverBase vDDR ON vDDR.ID = O.DriverID
			LEFT JOIN tblOrderSettlementShipper OSP ON OSP.OrderID = O.ID AND OSP.BatchID IS NOT NULL
			WHERE O.StatusID IN (4)  
				AND O.DeleteDateUTC IS NULL
				AND O.BatchID IS NULL 
				AND (@ShipperID=-1 OR O.CustomerID=@ShipperID) 
				AND (@CarrierID=-1 OR O.CarrierID=@CarrierID) 
				AND (@ProductGroupID=-1 OR ProductGroupID=@ProductGroupID) 
				AND (@TruckTypeID=-1 OR TruckTypeID=@TruckTypeID)
				AND (@DriverGroupID=-1 OR ISNULL(vODR.DriverGroupID, vDDR.DriverGroupID) = @DriverGroupID) 
				AND (@OriginStateID=-1 OR O.OriginStateID=@OriginStateID) 
				AND (@DestStateID=-1 OR O.DestStateID=@DestStateID) 
				AND (@ProducerID=-1 OR O.ProducerID=@ProducerID) 
				AND (@StartDate IS NULL OR O.OrderDate >= @StartDate) 
				AND (@EndDate IS NULL OR O.OrderDate <= @EndDate)
				AND (@OnlyShipperSettled = 0 OR OSP.BatchID IS NOT NULL)
				AND (@JobNumber IS NULL OR O.JobNumber = isnull(@JobNumber, ''''))
				AND (@ContractNumber IS NULL OR O.ContractNumber = isnull(@ContractNumber, ''''))'
		SET @sql = replace(@sql, '@ShipperID', @ShipperID)
		SET @sql = replace(@sql, '@CarrierID', @CarrierID)
		SET @sql = replace(@sql, '@ProductGroupID', @ProductGroupID)
		SET @sql = replace(@sql, '@TruckTypeID', @TruckTypeID)
		SET @sql = replace(@sql, '@DriverGroupID', @DriverGroupID)
		SET @sql = replace(@sql, '@OriginStateID', @OriginStateID)
		SET @sql = replace(@sql, '@DestStateID', @DestStateID)
		SET @sql = replace(@sql, '@ProducerID', @ProducerID)
		SET @sql = replace(@sql, '@StartDate', dbo.fnQS(@StartDate))
		SET @sql = replace(@sql, '@EndDate', dbo.fnQS(@EndDate))
		SET @sql = replace(@sql, '@OnlyShipperSettled', @OnlyShipperSettled)
		SET @sql = replace(@sql, '@JobNumber', dbo.fnQS(nullif(rtrim(@JobNumber), '')))
		SET @sql = replace(@sql, '@ContractNumber', dbo.fnQS(nullif(rtrim(@ContractNumber), '')))
	END

	-- PRINT 'sql = ' + @sql
	
	BEGIN TRAN RetrieveOrdersFinancialCarrier

	-- create the temp table to store the data
	SELECT *, RateApplySel = cast(1 as bit), BatchSel = cast(1 as bit), SessionID = cast(NULL AS varchar(100))
	INTO #ret 
	FROM viewOrder_Financial_Carrier 
	WHERE 1 = 0
	
	-- get the data into the temp table
	INSERT INTO #ret EXEC (@sql)

	-- do the SessionID logic
	IF (@StartSession = 1)
	BEGIN
		DELETE FROM tblOrderSettlementSelectionCarrier WHERE OrderID IN (SELECT ID FROM #ret) OR SessionID IN (SELECT SessionID FROM #ret)
		SELECT @sessionID = isnull(max(SessionID), 0) + 1 FROM tblOrderSettlementSelectionCarrier
		UPDATE #ret SET SessionID = @sessionID, RateApplySel = 1, BatchSel = CASE WHEN HasError = 1 THEN 0 ELSE 1 END
		INSERT INTO tblOrderSettlementSelectionCarrier (OrderID, SessionID, RateApplySel, BatchSel)
			SELECT ID, @sessionID, RateApplySel, BatchSel FROM #ret
	END
	
	COMMIT TRAN RetrieveOrdersFinancialCarrier

	-- return the data to the caller
	SELECT * FROM #ret
END
GO

_spDropProcedure 'spUpdateFinancialOrderSessionCarrier'
go
/**********************************************/
-- Created: 4.1.0 - 2016/08/18 - Kevin Alons
-- Purpose: update the db-stored Batch & RateApply Selection for a Carrier Financial Order Session
-- Changes:
/**********************************************/
CREATE PROCEDURE spUpdateFinancialOrderSessionCarrier
(
  @SessionID varchar(100)
, @OrderID int = NULL -- if specified, change sel for that single order, if NULL, then reset the entire session
, @BatchSel bit = NULL OUTPUT
, @RateApplySel bit = NULL OUTPUT
) AS
BEGIN
	IF (@BatchSel IS NOT NULL) 
		UPDATE tblOrderSettlementSelectionCarrier 
		SET BatchSel = @BatchSel 
		WHERE (@OrderID IS NULL OR OrderID = @OrderID) AND SessionID = @SessionID
	IF (@RateApplySel IS NOT NULL) 
		UPDATE tblOrderSettlementSelectionCarrier 
		SET RateApplySel = @RateApplySel 
		WHERE (@OrderID IS NULL OR OrderID = @OrderID) AND SessionID = @SessionID

	-- return 1 if all selected, 0 if none selected, NULL if some selected
	SELECT @BatchSel = CASE WHEN c = BS THEN 1 WHEN BS = 0 THEN 0 ELSE NULL END 
		, @RateApplySel = CASE WHEN c = RAS THEN 1 WHEN RAS = 0 THEN 0 ELSE NULL END 
	FROM (
		SELECT c = count(1), BS = sum(cast(BatchSel as int)), RAS = sum(cast(RateApplySel as int)) 
		FROM tblOrderSettlementSelectionCarrier 
		WHERE SessionID = @SessionID
	) X
END
GO
GRANT EXECUTE ON spUpdateFinancialOrderSessionCarrier TO role_iis_acct
GO

exec _spDropProcedure 'spApplyRatesCarrierSession'
go
/*************************************************
-- Created: 4.1.0 - 2016/08/21 - Kevin Alons
-- Purpose: apply rates to the specified [Carrier] Financial Session
-- Changes:
*************************************************/
CREATE PROCEDURE spApplyRatesCarrierSession
(
  @SessionID varchar(100)
, @UserName varchar(100)
, @ResetOverrides bit = 0
) AS BEGIN

	SET NOCOUNT ON

	DECLARE @IDs IDTABLE
	INSERT INTO @IDs SELECT OrderID FROM tblOrderSettlementSelectionCarrier WHERE SessionID = @SessionID AND RateApplySel = 1

	DECLARE @id int = 0
	WHILE EXISTS (SELECT * FROM @IDs)
	BEGIN
		SELECT @id = min(ID) FROM @IDs
		EXEC spApplyRatesCarrier @ID = @id, @UserName = @UserName, @ResetOverrides = @ResetOverrides
		DELETE FROM @IDs WHERE ID = @id
	END

END

GO
GRANT EXECUTE ON spApplyRatesCarrierSession TO role_iis_acct
GO

/***********************************/
-- Created: ?.?.? - 2013/06/02 - Kevin Alons
-- Purpose: compute and add the various Carrier "Settlement" $$ values to an Audited order
-- Changes:
-- 4.1.0 - 2016/08/21 - KDA - use new @SessionID parameter to create the entire settlement record in a single invocation
/***********************************/
ALTER PROCEDURE spCreateCarrierSettlementBatch
(
  @SessionID varchar(100)
, @CarrierID int
, @InvoiceNum varchar(50)
, @Notes varchar(255)
, @UserName varchar(255)
, @BatchID int = NULL output
, @BatchNum int = NULL output
, @Outcome varchar(25) = 'Success' output
, @Msg varchar(255) = NULL output
) AS BEGIN

	-- validation first
	SELECT OSS.OrderID, IsRated = cast(CASE WHEN OFB.InvoiceRatesAppliedDateUTC IS NULL THEN 0 ELSE 1 END as bit), OFB.HasError
	INTO #data
	FROM tblOrderSettlementSelectionCarrier OSS
	LEFT JOIN viewOrder_Financial_Base_Carrier OFB ON OFB.ID = OSS.OrderID
	WHERE SessionID = @SessionID AND OSS.BatchSel = 1

	IF NOT EXISTS (SELECT * FROM #data)
	BEGIN
		SELECT @Outcome = 'Error', @Msg = 'At least order must be selected to complete settlement!'
		RETURN
	END
	ELSE IF EXISTS (SELECT * FROM #data WHERE IsRated = 0 OR HasError <> 0)
	BEGIN
		SELECT @Outcome = 'Error', @Msg = 'All selected orders must be fully rated with no errors to complete Settlement!'
		RETURN
	END

	-- if validation passed, complete the settlement process below
	SET XACT_ABORT ON
	BEGIN TRAN CarrierSettlement

	INSERT INTO dbo.tblCarrierSettlementBatch(CarrierID, InvoiceNum, BatchNum, BatchDate, Notes, CreateDateUTC, CreatedByUser)
		SELECT @CarrierID
			, @InvoiceNum
			, isnull(max(BatchNum), 0) + 1
			, dbo.fnDateOnly(getdate()), @Notes, getutcdate(), @UserName 
		FROM dbo.tblCarrierSettlementBatch
	SELECT @BatchID = scope_identity()
	SELECT @BatchNum = BatchNum FROM dbo.tblCarrierSettlementBatch WHERE ID = @BatchID

	UPDATE tblOrderSettlementCarrier 
		SET BatchID = @BatchID
	FROM tblOrderSettlementCarrier OSC
	JOIN #data D ON D.OrderID = OSC.OrderID

	SELECT @Outcome = 'Success', @Msg = 'Settlement process complete: ' + ltrim(count(1)) + ' record(s) were added to batch: ' + ltrim(@BatchNum)
	FROM #data

	DELETE FROM tblOrderSettlementSelectionCarrier WHERE SessionID = @SessionID AND BatchSel = 1

	COMMIT TRAN
END
GO

exec _spDropProcedure 'spOrderSettlementCarrierAddToBatch'
GO

CREATE TABLE dbo.tblDriverAssessorialRate
(
	ID int IDENTITY(1,1) NOT NULL,
	TypeID int NOT NULL,
	CarrierID int NULL,
	ProductGroupID int NULL,
	OriginID int NULL,
	DestinationID int NULL,
	OriginStateID int NULL,
	DestStateID int NULL,
	RegionID int NULL,
	ProducerID int NULL,
	EffectiveDate date NOT NULL,
	EndDate date NOT NULL,
	Rate decimal(18, 10) NOT NULL,
	RateTypeID int NOT NULL,
	UomID int NOT NULL,
	CreateDateUTC smalldatetime NOT NULL CONSTRAINT DF_DriverAssessorialRate_CreateDateUTC  DEFAULT (getutcdate()),
	CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_DriverAssessorialRate_CreatedByUser  DEFAULT (suser_name()),
	LastChangeDateUTC smalldatetime NULL,
	LastChangedByUser varchar(100) NULL,
	ShipperID int NULL,
	DriverGroupID int NULL,
	DriverID int NULL CONSTRAINT DF_DriverAssessorialRate_DriverID FOREIGN KEY REFERENCES tblDriver(ID),
	TruckTypeID int NULL,
 CONSTRAINT PK_DriverAssessorialRate PRIMARY KEY NONCLUSTERED (ID)
) ON [PRIMARY]
GO

ALTER TABLE dbo.tblDriverAssessorialRate  WITH CHECK ADD  CONSTRAINT FK_DriverAccessorialRate_RateType FOREIGN KEY(RateTypeID)
REFERENCES dbo.tblRateType (ID)
GO

ALTER TABLE dbo.tblDriverAssessorialRate CHECK CONSTRAINT FK_DriverAccessorialRate_RateType
GO

ALTER TABLE dbo.tblDriverAssessorialRate  WITH CHECK ADD  CONSTRAINT FK_DriverAccessorialRate_Type FOREIGN KEY(TypeID)
REFERENCES dbo.tblAssessorialRateType (ID)
GO

ALTER TABLE dbo.tblDriverAssessorialRate  WITH CHECK ADD  CONSTRAINT FK_DriverAccessorialRate_UomID FOREIGN KEY(UomID)
REFERENCES dbo.tblUom (ID)
GO

ALTER TABLE dbo.tblDriverAssessorialRate  WITH CHECK ADD  CONSTRAINT FK_DriverAssessorialRate_Carrier FOREIGN KEY(CarrierID)
REFERENCES dbo.tblCarrier (ID)
GO

ALTER TABLE dbo.tblDriverAssessorialRate  WITH CHECK ADD  CONSTRAINT FK_DriverAssessorialRate_Destination FOREIGN KEY(DestinationID)
REFERENCES dbo.tblDestination (ID)
GO

ALTER TABLE dbo.tblDriverAssessorialRate  WITH CHECK ADD  CONSTRAINT FK_DriverAssessorialRate_DestState FOREIGN KEY(DestStateID)
REFERENCES dbo.tblState (ID)
GO

ALTER TABLE dbo.tblDriverAssessorialRate  WITH CHECK ADD  CONSTRAINT FK_DriverAssessorialRate_DriverGroup FOREIGN KEY(DriverGroupID)
REFERENCES dbo.tblDriverGroup (ID)
GO

ALTER TABLE dbo.tblDriverAssessorialRate  WITH CHECK ADD  CONSTRAINT FK_DriverAssessorialRate_Origin FOREIGN KEY(OriginID)
REFERENCES dbo.tblOrigin (ID)
GO

ALTER TABLE dbo.tblDriverAssessorialRate  WITH CHECK ADD  CONSTRAINT FK_DriverAssessorialRate_OriginState FOREIGN KEY(OriginStateID)
REFERENCES dbo.tblState (ID)
GO

ALTER TABLE dbo.tblDriverAssessorialRate  WITH CHECK ADD  CONSTRAINT FK_DriverAssessorialRate_Producer FOREIGN KEY(ProducerID)
REFERENCES dbo.tblProducer (ID)
GO

ALTER TABLE dbo.tblDriverAssessorialRate  WITH CHECK ADD  CONSTRAINT FK_DriverAssessorialRate_ProductGroup FOREIGN KEY(ProductGroupID)
REFERENCES dbo.tblProductGroup (ID)
GO

ALTER TABLE dbo.tblDriverAssessorialRate  WITH CHECK ADD  CONSTRAINT FK_DriverAssessorialRate_Region FOREIGN KEY(RegionID)
REFERENCES dbo.tblRegion (ID)
GO

ALTER TABLE dbo.tblDriverAssessorialRate  WITH CHECK ADD  CONSTRAINT FK_DriverAssessorialRate_Shipper FOREIGN KEY(ShipperID)
REFERENCES dbo.tblCustomer (ID)
GO

ALTER TABLE dbo.tblDriverAssessorialRate  WITH CHECK ADD  CONSTRAINT FK_DriverAssessorialRate_TruckType FOREIGN KEY(TruckTypeID)
REFERENCES dbo.tblTruckType (ID)
GO

ALTER TABLE dbo.tblDriverAssessorialRate  WITH CHECK ADD  CONSTRAINT CK_DriverRangeRate_EndDate_Greater CHECK  ((EndDate>=EffectiveDate))
GO

CREATE TABLE tblDriverDestinationWaitRate
(
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ReasonID] [int] NULL,
	[CarrierID] [int] NULL,
	[ProductGroupID] [int] NULL,
	[DestinationID] [int] NULL,
	[StateID] [int] NULL,
	[RegionID] [int] NULL,
	[EffectiveDate] [date] NOT NULL,
	[EndDate] [date] NOT NULL,
	[Rate] [decimal](18, 10) NOT NULL,
	[CreateDateUTC] [datetime] NOT NULL CONSTRAINT [DF_DriverDestinationWaitRate_CreateDateUTC]  DEFAULT (getutcdate()),
	[CreatedByUser] [varchar](100) NOT NULL CONSTRAINT [DF_DriverDestinationWaitRate_CreatedByUser]  DEFAULT (suser_name()),
	[LastChangeDateUTC] [datetime] NULL,
	[LastChangedByUser] [varchar](100) NULL,
	[ShipperID] [int] NULL,
	[ProducerID] [int] NULL,
	[DriverGroupID] [int] NULL,
 	DriverID int NULL CONSTRAINT DF_DriverDestinationWaitRate_DriverID FOREIGN KEY REFERENCES tblDriver(ID),
	[TruckTypeID] [int] NULL,
CONSTRAINT [PK_DriverDestinationWaitRate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tblDriverDestinationWaitRate]  WITH CHECK ADD  CONSTRAINT [FK_DriverDestinationWait_DriverGroup] FOREIGN KEY([DriverGroupID])
REFERENCES [dbo].[tblDriverGroup] ([ID])
GO

ALTER TABLE [dbo].[tblDriverDestinationWaitRate]  WITH CHECK ADD  CONSTRAINT [FK_DriverDestinationWait_Producer] FOREIGN KEY([ProducerID])
REFERENCES [dbo].[tblProducer] ([ID])
GO

ALTER TABLE [dbo].[tblDriverDestinationWaitRate]  WITH CHECK ADD  CONSTRAINT [FK_DriverDestinationWaitRate_Carrier] FOREIGN KEY([CarrierID])
REFERENCES [dbo].[tblCarrier] ([ID])
GO

ALTER TABLE [dbo].[tblDriverDestinationWaitRate]  WITH CHECK ADD  CONSTRAINT [FK_DriverDestinationWaitRate_Destination] FOREIGN KEY([DestinationID])
REFERENCES [dbo].[tblDestination] ([ID])
GO

ALTER TABLE [dbo].[tblDriverDestinationWaitRate]  WITH CHECK ADD  CONSTRAINT [FK_DriverDestinationWaitRate_ProductGroup] FOREIGN KEY([ProductGroupID])
REFERENCES [dbo].[tblProductGroup] ([ID])
GO

ALTER TABLE [dbo].[tblDriverDestinationWaitRate]  WITH CHECK ADD  CONSTRAINT [FK_DriverDestinationWaitRate_Reason] FOREIGN KEY([ReasonID])
REFERENCES [dbo].[tblDestinationWaitReason] ([ID])
GO

ALTER TABLE [dbo].[tblDriverDestinationWaitRate]  WITH CHECK ADD  CONSTRAINT [FK_DriverDestinationWaitRate_Region] FOREIGN KEY([RegionID])
REFERENCES [dbo].[tblRegion] ([ID])
GO

ALTER TABLE [dbo].[tblDriverDestinationWaitRate]  WITH CHECK ADD  CONSTRAINT [FK_DriverDestinationWaitRate_Shipper] FOREIGN KEY([ShipperID])
REFERENCES [dbo].[tblCustomer] ([ID])
GO

ALTER TABLE [dbo].[tblDriverDestinationWaitRate]  WITH CHECK ADD  CONSTRAINT [FK_DriverDestinationWaitRate_State] FOREIGN KEY([StateID])
REFERENCES [dbo].[tblState] ([ID])
GO

ALTER TABLE [dbo].[tblDriverDestinationWaitRate]  WITH CHECK ADD  CONSTRAINT [FK_DriverDestinationWaitRate_TruckType] FOREIGN KEY([TruckTypeID])
REFERENCES [dbo].[tblTruckType] ([ID])
GO

ALTER TABLE [dbo].[tblDriverDestinationWaitRate]  WITH CHECK ADD  CONSTRAINT [CK_DriverDestinationWaitRate_EndDate_Greater] CHECK  (([EndDate]>=[EffectiveDate]))
GO

CREATE TABLE [dbo].[tblDriverFuelSurchargeRate]
(
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ShipperID] [int] NULL,
	[CarrierID] [int] NULL,
	[ProductGroupID] [int] NULL,
	[FuelPriceFloor] [money] NOT NULL,
	[IntervalAmount] [money] NOT NULL,
	[IncrementAmount] [money] NOT NULL,
	[EffectiveDate] [date] NOT NULL,
	[EndDate] [date] NOT NULL,
	[CreateDateUTC] [smalldatetime] NOT NULL,
	[CreatedByUser] [varchar](100) NOT NULL,
	[LastChangeDateUTC] [smalldatetime] NULL,
	[LastChangedByUser] [varchar](100) NULL,
	[DriverGroupID] [int] NULL,
	DriverID int NULL CONSTRAINT DF_DriverFuelSurchargeRate_DriverID FOREIGN KEY REFERENCES tblDriver(ID),
	[TruckTypeID] [int] NULL,
 CONSTRAINT [PK_DriverFuelSurchargeRate] PRIMARY KEY NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tblDriverFuelSurchargeRate] WITH CHECK ADD  CONSTRAINT [DF_DriverFuelSurcharge_CreateDateUTC]  DEFAULT (getutcdate()) FOR [CreateDateUTC]
GO

ALTER TABLE [dbo].[tblDriverFuelSurchargeRate] WITH CHECK ADD  CONSTRAINT [DF_DriverFuelSurcharge_CreatedBy]  DEFAULT (suser_name()) FOR [CreatedByUser]
GO

ALTER TABLE [dbo].[tblDriverFuelSurchargeRate]  WITH CHECK ADD  CONSTRAINT [FK_DriverFuelSurcharge_Carrier] FOREIGN KEY([CarrierID])
REFERENCES [dbo].[tblCarrier] ([ID])
GO

ALTER TABLE [dbo].[tblDriverFuelSurchargeRate]  WITH CHECK ADD  CONSTRAINT [FK_DriverFuelSurcharge_DriverGroup] FOREIGN KEY([DriverGroupID])
REFERENCES [dbo].[tblDriverGroup] ([ID])
GO

ALTER TABLE [dbo].[tblDriverFuelSurchargeRate]  WITH CHECK ADD  CONSTRAINT [FK_DriverFuelSurcharge_ProductGroup] FOREIGN KEY([ProductGroupID])
REFERENCES [dbo].[tblProductGroup] ([ID])
GO

ALTER TABLE [dbo].[tblDriverFuelSurchargeRate]  WITH CHECK ADD  CONSTRAINT [FK_DriverFuelSurcharge_Shipper] FOREIGN KEY([ShipperID])
REFERENCES [dbo].[tblCustomer] ([ID])
GO

ALTER TABLE [dbo].[tblDriverFuelSurchargeRate]  WITH CHECK ADD  CONSTRAINT [FK_DriverFuelSurchargeRate_TruckType] FOREIGN KEY([TruckTypeID])
REFERENCES [dbo].[tblTruckType] ([ID])
GO

ALTER TABLE [dbo].[tblDriverFuelSurchargeRate]  WITH CHECK ADD  CONSTRAINT [CK_DriverFuelSurchargeRate_EndDate_Greater] CHECK  (([EndDate]>=[EffectiveDate]))
GO

CREATE TABLE [dbo].[tblDriverOrderRejectRate]
(
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ReasonID] [int] NULL,
	[CarrierID] [int] NULL,
	[ProductGroupID] [int] NULL,
	[OriginID] [int] NULL,
	[StateID] [int] NULL,
	[RegionID] [int] NULL,
	[EffectiveDate] [date] NOT NULL,
	[EndDate] [date] NOT NULL,
	[Rate] [decimal](18, 10) NOT NULL,
	[RateTypeID] [int] NOT NULL,
	[UomID] [int] NOT NULL,
	[CreateDateUTC] [datetime] NOT NULL CONSTRAINT [DF_DriverOrderRejectRate_CreateDateUTC]  DEFAULT (getutcdate()),
	[CreatedByUser] [varchar](100) NOT NULL CONSTRAINT [DF_DriverOrderRejectRate_CreatedByUser]  DEFAULT (suser_name()),
	[LastChangeDateUTC] [datetime] NULL,
	[LastChangedByUser] [varchar](100) NULL,
	[ShipperID] [int] NULL,
	[ProducerID] [int] NULL,
	[DriverGroupID] [int] NULL,
 	DriverID int NULL CONSTRAINT DF_DriverOrderRejectRate_DriverID FOREIGN KEY REFERENCES tblDriver(ID),
	[TruckTypeID] [int] NULL,
CONSTRAINT [PK_DriverOrderRejectRate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tblDriverOrderRejectRate]  WITH CHECK ADD  CONSTRAINT [FK_DriverOrderReject_DriverGroup] FOREIGN KEY([DriverGroupID])
REFERENCES [dbo].[tblDriverGroup] ([ID])
GO

ALTER TABLE [dbo].[tblDriverOrderRejectRate]  WITH CHECK ADD  CONSTRAINT [FK_DriverOrderReject_Producer] FOREIGN KEY([ProducerID])
REFERENCES [dbo].[tblProducer] ([ID])
GO

ALTER TABLE [dbo].[tblDriverOrderRejectRate]  WITH CHECK ADD  CONSTRAINT [FK_DriverOrderRejectRate_Carrier] FOREIGN KEY([CarrierID])
REFERENCES [dbo].[tblCarrier] ([ID])
GO

ALTER TABLE [dbo].[tblDriverOrderRejectRate]  WITH CHECK ADD  CONSTRAINT [FK_DriverOrderRejectRate_Origin] FOREIGN KEY([OriginID])
REFERENCES [dbo].[tblOrigin] ([ID])
GO

ALTER TABLE [dbo].[tblDriverOrderRejectRate]  WITH CHECK ADD  CONSTRAINT [FK_DriverOrderRejectRate_ProductGroup] FOREIGN KEY([ProductGroupID])
REFERENCES [dbo].[tblProductGroup] ([ID])
GO

ALTER TABLE [dbo].[tblDriverOrderRejectRate]  WITH CHECK ADD  CONSTRAINT [FK_DriverOrderRejectRate_RateType] FOREIGN KEY([RateTypeID])
REFERENCES [dbo].[tblRateType] ([ID])
GO

ALTER TABLE [dbo].[tblDriverOrderRejectRate]  WITH CHECK ADD  CONSTRAINT [FK_DriverOrderRejectRate_Reason] FOREIGN KEY([ReasonID])
REFERENCES [dbo].[tblOrderRejectReason] ([ID])
GO

ALTER TABLE [dbo].[tblDriverOrderRejectRate]  WITH CHECK ADD  CONSTRAINT [FK_DriverOrderRejectRate_Region] FOREIGN KEY([RegionID])
REFERENCES [dbo].[tblRegion] ([ID])
GO

ALTER TABLE [dbo].[tblDriverOrderRejectRate]  WITH CHECK ADD  CONSTRAINT [FK_DriverOrderRejectRate_Shipper] FOREIGN KEY([ShipperID])
REFERENCES [dbo].[tblCustomer] ([ID])
GO

ALTER TABLE [dbo].[tblDriverOrderRejectRate]  WITH CHECK ADD  CONSTRAINT [FK_DriverOrderRejectRate_State] FOREIGN KEY([StateID])
REFERENCES [dbo].[tblState] ([ID])
GO

ALTER TABLE [dbo].[tblDriverOrderRejectRate]  WITH CHECK ADD  CONSTRAINT [FK_DriverOrderRejectRate_TruckType] FOREIGN KEY([TruckTypeID])
REFERENCES [dbo].[tblTruckType] ([ID])
GO

ALTER TABLE [dbo].[tblDriverOrderRejectRate]  WITH CHECK ADD  CONSTRAINT [FK_DriverOrderRejectRate_Uom] FOREIGN KEY([UomID])
REFERENCES [dbo].[tblUom] ([ID])
GO

CREATE TABLE [dbo].[tblDriverOriginWaitRate]
(
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ReasonID] [int] NULL,
	[CarrierID] [int] NULL,
	[ProductGroupID] [int] NULL,
	[OriginID] [int] NULL,
	[StateID] [int] NULL,
	[RegionID] [int] NULL,
	[EffectiveDate] [date] NOT NULL,
	[EndDate] [date] NOT NULL,
	[Rate] [decimal](18, 10) NOT NULL,
	[CreateDateUTC] [datetime] NOT NULL CONSTRAINT [DF_DriverOriginWaitRate_CreateDateUTC]  DEFAULT (getutcdate()),
	[CreatedByUser] [varchar](100) NOT NULL CONSTRAINT [DF_DriverOriginWaitRate_CreatedByUser]  DEFAULT (suser_name()),
	[LastChangeDateUTC] [datetime] NULL,
	[LastChangedByUser] [varchar](100) NULL,
	[ShipperID] [int] NULL,
	[ProducerID] [int] NULL,
	[DriverGroupID] [int] NULL,
	DriverID int NULL CONSTRAINT DF_DriverOriginWaitRate_DriverID FOREIGN KEY REFERENCES tblDriver(ID),
	[TruckTypeID] [int] NULL,
 CONSTRAINT [PK_DriverOriginWaitRate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tblDriverOriginWaitRate]  WITH CHECK ADD  CONSTRAINT [FK_DriverOriginWait_DriverGroup] FOREIGN KEY([DriverGroupID])
REFERENCES [dbo].[tblDriverGroup] ([ID])
GO

ALTER TABLE [dbo].[tblDriverOriginWaitRate]  WITH CHECK ADD  CONSTRAINT [FK_DriverOriginWait_Producer] FOREIGN KEY([ProducerID])
REFERENCES [dbo].[tblProducer] ([ID])
GO

ALTER TABLE [dbo].[tblDriverOriginWaitRate]  WITH CHECK ADD  CONSTRAINT [FK_DriverOriginWaitRate_Carrier] FOREIGN KEY([CarrierID])
REFERENCES [dbo].[tblCarrier] ([ID])
GO

ALTER TABLE [dbo].[tblDriverOriginWaitRate]  WITH CHECK ADD  CONSTRAINT [FK_DriverOriginWaitRate_Origin] FOREIGN KEY([OriginID])
REFERENCES [dbo].[tblOrigin] ([ID])
GO

ALTER TABLE [dbo].[tblDriverOriginWaitRate]  WITH CHECK ADD  CONSTRAINT [FK_DriverOriginWaitRate_ProductGroup] FOREIGN KEY([ProductGroupID])
REFERENCES [dbo].[tblProductGroup] ([ID])
GO

ALTER TABLE [dbo].[tblDriverOriginWaitRate]  WITH CHECK ADD  CONSTRAINT [FK_DriverOriginWaitRate_Reason] FOREIGN KEY([ReasonID])
REFERENCES [dbo].[tblOriginWaitReason] ([ID])
GO

ALTER TABLE [dbo].[tblDriverOriginWaitRate]  WITH CHECK ADD  CONSTRAINT [FK_DriverOriginWaitRate_Region] FOREIGN KEY([RegionID])
REFERENCES [dbo].[tblRegion] ([ID])
GO

ALTER TABLE [dbo].[tblDriverOriginWaitRate]  WITH CHECK ADD  CONSTRAINT [FK_DriverOriginWaitRate_Shipper] FOREIGN KEY([ShipperID])
REFERENCES [dbo].[tblCustomer] ([ID])
GO

ALTER TABLE [dbo].[tblDriverOriginWaitRate]  WITH CHECK ADD  CONSTRAINT [FK_DriverOriginWaitRate_State] FOREIGN KEY([StateID])
REFERENCES [dbo].[tblState] ([ID])
GO

ALTER TABLE [dbo].[tblDriverOriginWaitRate]  WITH CHECK ADD  CONSTRAINT [FK_DriverOriginWaitRate_TruckType] FOREIGN KEY([TruckTypeID])
REFERENCES [dbo].[tblTruckType] ([ID])
GO

ALTER TABLE [dbo].[tblDriverOriginWaitRate]  WITH CHECK ADD  CONSTRAINT [CK_DriverOriginWaitRate_EndDate_Greater] CHECK  (([EndDate]>=[EffectiveDate]))
GO

CREATE TABLE [dbo].[tblDriverRateSheet]
(
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CarrierID] [int] NULL,
	[ProductGroupID] [int] NULL,
	[OriginStateID] [int] NULL,
	[DestStateID] [int] NULL,
	[RegionID] [int] NULL,
	[EffectiveDate] [date] NOT NULL,
	[EndDate] [date] NOT NULL,
	[RateTypeID] [int] NOT NULL,
	[UomID] [int] NOT NULL,
	[CreateDateUTC] [smalldatetime] NOT NULL CONSTRAINT [DF_DriverRateSheet_CreateDateUTC]  DEFAULT (getutcdate()),
	[CreatedByUser] [varchar](100) NOT NULL CONSTRAINT [DF_DriverRateSheet_CreatedByUser]  DEFAULT (suser_name()),
	[LastChangeDateUTC] [smalldatetime] NULL,
	[LastChangedByUser] [varchar](100) NULL,
	[ShipperID] [int] NULL,
	[ProducerID] [int] NULL,
	[DriverGroupID] [int] NULL,
	DriverID int NULL CONSTRAINT DF_DriverRateSheetRate_DriverID FOREIGN KEY REFERENCES tblDriver(ID),
	[TruckTypeID] [int] NULL,
 CONSTRAINT [PK_DriverRateSheet] PRIMARY KEY NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tblDriverRateSheet]  WITH CHECK ADD  CONSTRAINT [FK_DriverRateSheet_Carrier] FOREIGN KEY([CarrierID])
REFERENCES [dbo].[tblCarrier] ([ID])
GO

ALTER TABLE [dbo].[tblDriverRateSheet]  WITH CHECK ADD  CONSTRAINT [FK_DriverRateSheet_DestState] FOREIGN KEY([DestStateID])
REFERENCES [dbo].[tblState] ([ID])
GO

ALTER TABLE [dbo].[tblDriverRateSheet]  WITH CHECK ADD  CONSTRAINT [FK_DriverRateSheet_DriverGroup] FOREIGN KEY([DriverGroupID])
REFERENCES [dbo].[tblDriverGroup] ([ID])
GO

ALTER TABLE [dbo].[tblDriverRateSheet]  WITH CHECK ADD  CONSTRAINT [FK_DriverRateSheet_OriginState] FOREIGN KEY([OriginStateID])
REFERENCES [dbo].[tblState] ([ID])
GO

ALTER TABLE [dbo].[tblDriverRateSheet]  WITH CHECK ADD  CONSTRAINT [FK_DriverRateSheet_Producer] FOREIGN KEY([ProducerID])
REFERENCES [dbo].[tblProducer] ([ID])
GO

ALTER TABLE [dbo].[tblDriverRateSheet]  WITH CHECK ADD  CONSTRAINT [FK_DriverRateSheet_ProductGroup] FOREIGN KEY([ProductGroupID])
REFERENCES [dbo].[tblProductGroup] ([ID])
GO

ALTER TABLE [dbo].[tblDriverRateSheet]  WITH CHECK ADD  CONSTRAINT [FK_DriverRateSheet_RateType] FOREIGN KEY([RateTypeID])
REFERENCES [dbo].[tblRateType] ([ID])
GO

ALTER TABLE [dbo].[tblDriverRateSheet]  WITH CHECK ADD  CONSTRAINT [FK_DriverRateSheet_Region] FOREIGN KEY([RegionID])
REFERENCES [dbo].[tblRegion] ([ID])
GO

ALTER TABLE [dbo].[tblDriverRateSheet]  WITH CHECK ADD  CONSTRAINT [FK_DriverRateSheet_TruckType] FOREIGN KEY([TruckTypeID])
REFERENCES [dbo].[tblTruckType] ([ID])
GO

ALTER TABLE [dbo].[tblDriverRateSheet]  WITH CHECK ADD  CONSTRAINT [FK_DriverRateSheet_UomID] FOREIGN KEY([UomID])
REFERENCES [dbo].[tblUom] ([ID])
GO

ALTER TABLE [dbo].[tblDriverRateSheet]  WITH CHECK ADD  CONSTRAINT [FK_DriverRateSheetRate_Shipper] FOREIGN KEY([ShipperID])
REFERENCES [dbo].[tblCustomer] ([ID])
GO

ALTER TABLE [dbo].[tblDriverRateSheet]  WITH CHECK ADD  CONSTRAINT [CK_DriverRateSheet_EndDate_Greater] CHECK  (([EndDate]>=[EffectiveDate]))
GO

CREATE TABLE [dbo].[tblDriverRangeRate]
(
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RateSheetID] [int] NOT NULL,
	[MinRange] [int] NOT NULL,
	[MaxRange] [int] NOT NULL,
	[Rate] [decimal](18, 10) NOT NULL,
	[CreateDateUTC] [smalldatetime] NULL CONSTRAINT [DF_DriverRangeRate_CreateDateUTC]  DEFAULT (getutcdate()),
	[CreatedByUser] [varchar](100) NULL CONSTRAINT [DF_DriverRangeRate_CreatedByUser]  DEFAULT (suser_name()),
	[LastChangeDateUTC] [smalldatetime] NULL,
	[LastChangedByUser] [varchar](100) NULL,
 CONSTRAINT [PK_DriverRangeRate] PRIMARY KEY NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tblDriverRangeRate]  WITH CHECK ADD  CONSTRAINT [FK_DriverRangeRate_RateSheet] FOREIGN KEY([RateSheetID])
REFERENCES [dbo].[tblDriverRateSheet] ([ID])
GO

ALTER TABLE [dbo].[tblDriverRangeRate]  WITH CHECK ADD  CONSTRAINT [CK_DriverRangeRate_MaxRange_Greater] CHECK  (([MaxRange]>[MinRange]))
GO

ALTER TABLE [dbo].[tblDriverRangeRate]  WITH CHECK ADD  CONSTRAINT [CK_DriverRangeRate_MinRange_Positive] CHECK  (([MinRange]>=(0)))
GO

CREATE TABLE [dbo].[tblDriverRouteRate]
(
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CarrierID] [int] NULL,
	[ProductGroupID] [int] NULL,
	[RouteID] [int] NOT NULL,
	[Rate] [decimal](18, 10) NOT NULL,
	[EffectiveDate] [date] NOT NULL,
	[EndDate] [date] NOT NULL,
	[RateTypeID] [int] NOT NULL,
	[UomID] [int] NOT NULL,
	[CreateDateUTC] [smalldatetime] NOT NULL CONSTRAINT [DF_DriverRouteRate_CreateDateUTC]  DEFAULT (getutcdate()),
	[CreatedByUser] [varchar](100) NOT NULL,
	[LastChangeDateUTC] [smalldatetime] NULL,
	[LastChangedByUser] [varchar](100) NULL,
	[ShipperID] [int] NULL,
	[DriverGroupID] [int] NULL,
	DriverID int NULL CONSTRAINT DF_DriverRouteRate_DriverID FOREIGN KEY REFERENCES tblDriver(ID),
	[TruckTypeID] [int] NULL,
 CONSTRAINT [PK_DriverRouteRate] PRIMARY KEY NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tblDriverRouteRate]  WITH CHECK ADD  CONSTRAINT [FK_DriverRouteRate_Carrier] FOREIGN KEY([CarrierID])
REFERENCES [dbo].[tblCarrier] ([ID])
GO

ALTER TABLE [dbo].[tblDriverRouteRate]  WITH CHECK ADD  CONSTRAINT [FK_DriverRouteRate_DriverGroup] FOREIGN KEY([DriverGroupID])
REFERENCES [dbo].[tblDriverGroup] ([ID])
GO

ALTER TABLE [dbo].[tblDriverRouteRate]  WITH CHECK ADD  CONSTRAINT [FK_DriverRouteRate_ProductGroup] FOREIGN KEY([ProductGroupID])
REFERENCES [dbo].[tblProductGroup] ([ID])
GO

ALTER TABLE [dbo].[tblDriverRouteRate]  WITH CHECK ADD  CONSTRAINT [FK_DriverRouteRate_RateType] FOREIGN KEY([RateTypeID])
REFERENCES [dbo].[tblRateType] ([ID])
GO

ALTER TABLE [dbo].[tblDriverRouteRate]  WITH CHECK ADD  CONSTRAINT [FK_DriverRouteRate_Route] FOREIGN KEY([RouteID])
REFERENCES [dbo].[tblRoute] ([ID])
GO

ALTER TABLE [dbo].[tblDriverRouteRate]  WITH CHECK ADD  CONSTRAINT [FK_DriverRouteRate_Shipper] FOREIGN KEY([ShipperID])
REFERENCES [dbo].[tblCustomer] ([ID])
GO

ALTER TABLE [dbo].[tblDriverRouteRate]  WITH CHECK ADD  CONSTRAINT [FK_DriverRouteRate_TruckType] FOREIGN KEY([TruckTypeID])
REFERENCES [dbo].[tblTruckType] ([ID])
GO

ALTER TABLE [dbo].[tblDriverRouteRate]  WITH CHECK ADD  CONSTRAINT [FK_DriverRouteRate_Uom] FOREIGN KEY([UomID])
REFERENCES [dbo].[tblUom] ([ID])
GO

ALTER TABLE [dbo].[tblDriverRouteRate]  WITH CHECK ADD  CONSTRAINT [CK_DriverRouteRate_EndDate_Greater] CHECK  (([EndDate]>=[EffectiveDate]))
GO

CREATE TABLE [dbo].[tblDriverSettlementBatch]
(
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[BatchNum] [int] NOT NULL,
	[CarrierID] [int] NOT NULL,
	[DriverGroupID] [int] NULL CONSTRAINT FK_DriverSettlementBatch_DriverGroup FOREIGN KEY REFERENCES tblDriverGroup(ID),
	DriverID int NULL CONSTRAINT FK_DriverSettlementBatch_Driver FOREIGN KEY REFERENCES tblDriver(ID),
	[Notes] [varchar](255) NULL,
	[CreateDateUTC] [smalldatetime] NOT NULL CONSTRAINT [DF_DriverSettlementBatch_CreateDateUTC]  DEFAULT (getutcdate()),
	[CreatedByUser] [varchar](100) NULL,
	[LastChangeDateUTC] [smalldatetime] NULL,
	[LastChangedByUser] [varchar](100) NULL,
	[BatchDate] [smalldatetime] NOT NULL CONSTRAINT [DF_DriverSettlementBatch_BatchDate]  DEFAULT (getdate()),
	[InvoiceNum] [varchar](50) NULL,
 CONSTRAINT [PK_DriverSettlementBatch] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tblDriverSettlementBatch]  WITH CHECK ADD  CONSTRAINT [FK_DriverSettlementBatch_Carrier] FOREIGN KEY([CarrierID])
REFERENCES [dbo].[tblCarrier] ([ID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

CREATE TABLE [dbo].[tblOrderSettlementDriver]
(
	[OrderID] [int] NOT NULL,
	[OrderDate] [date] NOT NULL,
	[BatchID] [int] NULL,
	[SettlementFactorID] [int] NULL,
	[SettlementUomID] [int] NOT NULL,
	[MinSettlementUnits] [int] NULL,
	[SettlementUnits] [decimal](18, 10) NOT NULL,
	[RouteRateID] [int] NULL,
	[RangeRateID] [int] NULL,
	[LoadAmount] [money] NULL,
	[WaitFeeParameterID] [int] NULL,
	[OriginWaitRateID] [int] NULL,
	[OriginWaitBillableMinutes] [int] NULL,
	[OriginWaitBillableHours] [money] NULL,
	[OriginWaitAmount] [money] NULL,
	[DestinationWaitRateID] [int] NULL,
	[DestinationWaitBillableMinutes] [int] NULL,
	[DestinationWaitBillableHours] [money] NULL,
	[DestinationWaitAmount] [money] NULL,
	[OrderRejectRateID] [int] NULL,
	[OrderRejectAmount] [money] NULL,
	[FuelSurchargeRateID] [int] NULL,
	[FuelSurchargeRate] [money] NULL,
	[FuelSurchargeAmount] [money] NULL,
	[OriginTaxRate] [money] NULL,
	[TotalAmount] [money] NOT NULL,
	[CreateDateUTC] [datetime] NOT NULL CONSTRAINT [DF_OrderSettlementDriver_CreateDateUTC]  DEFAULT (getutcdate()),
	[CreatedByUser] [varchar](100) NOT NULL CONSTRAINT [DF_OrderSettlementDriver_CreatedByUser]  DEFAULT (suser_name()),
	[MinSettlementUnitsID] [int] NULL,
	[CarrierSettlementFactorID] [int] NULL,
 CONSTRAINT [PK_OrderSettlementDriver] PRIMARY KEY CLUSTERED 
(
	[OrderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tblOrderSettlementDriver]  WITH CHECK ADD  CONSTRAINT [FK_OrderSettlementDriver_Batch] FOREIGN KEY([BatchID])
REFERENCES [dbo].[tblDriverSettlementBatch] ([ID])
GO

ALTER TABLE [dbo].[tblOrderSettlementDriver]  WITH CHECK ADD  CONSTRAINT [FK_OrderSettlementDriver_SettlementFactorID] FOREIGN KEY([SettlementFactorID])
REFERENCES [dbo].[tblCarrierSettlementFactor] ([ID])
GO

ALTER TABLE [dbo].[tblOrderSettlementDriver]  WITH CHECK ADD  CONSTRAINT [FK_OrderSettlementDriver_WaitFeeParameter] FOREIGN KEY([WaitFeeParameterID])
REFERENCES [dbo].[tblCarrierWaitFeeParameter] ([ID])
GO

ALTER TABLE [dbo].[tblOrderSettlementDriver]  WITH CHECK ADD  CONSTRAINT [FK_OrderSettlementDriver_DestinationWaitRate] FOREIGN KEY([DestinationWaitRateID])
REFERENCES [dbo].[tblDriverDestinationWaitRate] ([ID])
GO

ALTER TABLE [dbo].[tblOrderSettlementDriver]  WITH CHECK ADD  CONSTRAINT [FK_OrderSettlementDriver_FuelSurchargeRate] FOREIGN KEY([FuelSurchargeRateID])
REFERENCES [dbo].[tblDriverFuelSurchargeRate] ([ID])
GO

ALTER TABLE [dbo].[tblOrderSettlementDriver]  WITH CHECK ADD  CONSTRAINT [FK_OrderSettlementDriver_MinSettlementUnitsID] FOREIGN KEY([MinSettlementUnitsID])
REFERENCES [dbo].[tblCarrierMinSettlementUnits] ([ID])
GO

ALTER TABLE [dbo].[tblOrderSettlementDriver]  WITH CHECK ADD  CONSTRAINT [FK_OrderSettlementDriver_Order] FOREIGN KEY([OrderID])
REFERENCES [dbo].[tblOrder] ([ID])
GO

ALTER TABLE [dbo].[tblOrderSettlementDriver]  WITH CHECK ADD  CONSTRAINT [FK_OrderSettlementDriver_OrderRejectRate] FOREIGN KEY([OrderRejectRateID])
REFERENCES [dbo].[tblDriverOrderRejectRate] ([ID])
GO

ALTER TABLE [dbo].[tblOrderSettlementDriver]  WITH CHECK ADD  CONSTRAINT [FK_OrderSettlementDriver_OriginWaitRate] FOREIGN KEY([OriginWaitRateID])
REFERENCES [dbo].[tblDriverOriginWaitRate] ([ID])
GO

ALTER TABLE [dbo].[tblOrderSettlementDriver]  WITH NOCHECK ADD  CONSTRAINT [FK_OrderSettlementDriver_RangeRate] FOREIGN KEY([RangeRateID])
REFERENCES [dbo].[tblDriverRangeRate] ([ID])
GO

ALTER TABLE [dbo].[tblOrderSettlementDriver]  WITH CHECK ADD  CONSTRAINT [FK_OrderSettlementDriver_SettlementFactor] FOREIGN KEY([SettlementFactorID])
REFERENCES [dbo].[tblSettlementFactor] ([ID])
GO

ALTER TABLE [dbo].[tblOrderSettlementDriver]  WITH CHECK ADD  CONSTRAINT [FK_OrderSettlementDriver_SettlementUom] FOREIGN KEY([SettlementUomID])
REFERENCES [dbo].[tblUom] ([ID])
GO

ALTER TABLE [dbo].[tblOrderSettlementDriver]  WITH CHECK ADD  CONSTRAINT [FK_OrderSettlmentDriver_RouteRate] FOREIGN KEY([RouteRateID])
REFERENCES [dbo].[tblDriverRouteRate] ([ID])
GO

ALTER TABLE [dbo].[tblOrderSettlementDriver]  WITH CHECK ADD  CONSTRAINT [CK_OrderSettlementDriver_RouteOrRangeRateNull] CHECK  (([RouteRateID] IS NULL OR [RangeRateID] IS NULL))
GO

CREATE TABLE [dbo].[tblOrderSettlementDriverAssessorialCharge]
(
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[OrderID] [int] NOT NULL,
	[AssessorialRateTypeID] [int] NOT NULL,
	[AssessorialRateID] [int] NULL,
	[Amount] [money] NOT NULL,
	[CreateDateUTC] [datetime] NOT NULL CONSTRAINT [DF_OrderSettlmentDriverOtherAssessorialCharge_CreateDateUTC]  DEFAULT (getutcdate()),
	[CreatedByUser] [varchar](100) NOT NULL CONSTRAINT [DF_OrderSettlmentDriverOtherAssessorialCharge_CreatedByUser]  DEFAULT (suser_name()),
	[LastChangeDateUTC] [datetime] NULL,
	[LastChangedByUser] [varchar](100) NULL,
 CONSTRAINT [PK_OrderSettlementDriverOtherAssessorialCharge] PRIMARY KEY NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tblOrderSettlementDriverAssessorialCharge]  WITH CHECK ADD  CONSTRAINT [FK_OrderSettlementDriverOtherAssessorialCharge_AssessorialRate] FOREIGN KEY([AssessorialRateID])
REFERENCES [dbo].[tblDriverAssessorialRate] ([ID])
GO

ALTER TABLE [dbo].[tblOrderSettlementDriverAssessorialCharge]  WITH CHECK ADD  CONSTRAINT [FK_OrderSettlementDriverOtherAssessorialCharge_RateType] FOREIGN KEY([AssessorialRateTypeID])
REFERENCES [dbo].[tblAssessorialRateType] ([ID])
GO

ALTER TABLE [dbo].[tblOrderSettlementDriverAssessorialCharge]  WITH CHECK ADD  CONSTRAINT [FK_OrderSettlementDriverOtherAssessorialCharge_Settlement] FOREIGN KEY([OrderID])
REFERENCES [dbo].[tblOrderSettlementDriver] ([OrderID])
ON DELETE CASCADE
GO

CREATE TABLE tblOrderSettlementSelectionDriver
(
	[OrderID] [int] NOT NULL,
	[SessionID] [varchar](100) NOT NULL,
	[RateApplySel] [bit] NOT NULL CONSTRAINT [DF_OrderSettlementSelectionDriver_RateApplySel]  DEFAULT ((1)),
	[BatchSel] [bit] NOT NULL CONSTRAINT [DF_OrderSettlementSelectionDriver_BatchSel]  DEFAULT ((1)),
 CONSTRAINT [PK_OrderSettlementSelectionDriver] PRIMARY KEY CLUSTERED 
(
	[OrderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tblOrderSettlementSelectionDriver]  WITH CHECK ADD  CONSTRAINT [FK_OrderSettlementSelectionDriver_Order] FOREIGN KEY([OrderID])
REFERENCES [dbo].[tblOrder] ([ID])
GO

/******************************************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: add a computed "EndDate" value to all DriverRateSheet records
-- Changes:
/******************************************************/
CREATE VIEW viewDriverRateSheet AS
	SELECT X.*
		, MaxEffectiveDate = (SELECT min(OrderDate) FROM tblOrderSettlementDriver SC JOIN tblDriverRangeRate RR ON RR.ID = SC.RangeRateID WHERE RR.RateSheetID = X.ID AND SC.BatchID IS NOT NULL)
		, MinEndDate = (SELECT max(OrderDate) FROM tblOrderSettlementDriver SC JOIN tblDriverRangeRate RR ON RR.ID = SC.RangeRateID WHERE RR.RateSheetID = X.ID AND SC.BatchID IS NOT NULL)
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementDriver SC JOIN tblDriverRangeRate RR ON RR.ID = SC.RangeRateID WHERE RR.RateSheetID = X.ID AND SC.BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblDriverRateSheet XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverID, X.DriverID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT max(XN.EndDate) 
			FROM tblDriverRateSheet XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverID, X.DriverID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EndDate < X.EffectiveDate)
	FROM tblDriverRateSheet X

GO

/************************************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: return combined RateSheet + RangeRate data + friendly translated values
--			allow updating of both RateSheet & RangeRate data together
-- Changes:
/************************************************/
CREATE VIEW [dbo].[viewDriverRateSheetRangeRate] AS
	SELECT RR.ID, RateSheetID = R.ID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.TruckTypeID, R.DriverGroupID, R.DriverID, R.OriginStateID, R.DestStateID, R.RegionID, R.ProducerID, RR.Rate, RR.MinRange, RR.MaxRange, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate, R.NextEffectiveDate
		, R.Locked
		, RR.CreateDateUTC, RR.CreatedByUser
		, RR.LastChangeDateUTC, RR.LastChangedByUser
	FROM dbo.viewDriverRateSheet R
	JOIN dbo.tblDriverRangeRate RR ON RR.RateSheetID = R.ID

GO

/***************************************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: return the RateSheet JOIN RangeRate data with translated "friendly" values
-- Changes:
/***************************************************/
CREATE VIEW viewDriverRateSheetRangeRatesDisplay AS
	SELECT R.*
		, Shipper = SH.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, TruckType = TT.Name
		, DriverGroup = DG.Name
		, Driver = DR.FullName
		, OriginState = OS.FullName
		, OriginStateAbbrev = OS.Abbreviation
		, DestinationState = DS.FullName
		, DestinationStateAbbrev = DS.Abbreviation
		, Region = REG.Name
		, Producer = P.Name
		, RateType = RT.Name
		, Uom = U.Name
		, UomShort = U.Abbrev
	FROM viewDriverRateSheetRangeRate R
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblTruckType TT ON TT.ID = R.TruckTypeID
	LEFT JOIN tblDriverGroup DG ON DG.ID = R.DriverGroupID
	LEFT JOIN viewDriverBase DR ON DR.ID = R.DriverID
	LEFT JOIN tblState OS ON OS.ID = R.OriginStateID
	LEFT JOIN tblState DS ON DS.ID = R.DestStateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblProducer P ON P.ID = R.ProducerID
	LEFT JOIN tblRateType RT ON RT.ID = R.RateTypeID
	LEFT JOIN tblUom U ON U.ID = R.UomID

GO

/*************************************/
-- Created: 4.1.0 - 2016.08.31 - Kevin Alons
-- Purpose: handle specialized logic related to editing Route Rates (due to combination of RateSheet + RangeRate tables)
-- Changes:
/*************************************/
CREATE TRIGGER trigViewDriverRateSheetRangeRate_IU_Update ON viewDriverRateSheetRangeRate INSTEAD OF INSERT, UPDATE  AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
		-- PRINT 'apply any RateSheet changes to the underlying RateSheet records'
		UPDATE tblDriverRateSheet
			SET ShipperID = i.ShipperID
				, CarrierID = i.CarrierID
				, ProductGroupID = i.ProductGroupID
				, TruckTypeID = i.TruckTypeID
				, DriverGroupID = i.DriverGroupID
				, DriverID = i.DriverID
				, OriginStateID = i.OriginStateID
				, DestStateID = i.DestStateID
				, RegionID = i.RegionID
				, ProducerID = i.ProducerID
				, EffectiveDate = i.EffectiveDate
				, EndDate = i.EndDate
				, RateTypeID = i.RateTypeID
				, UomID = i.UomID
				, LastChangeDateUTC = isnull(i.LastChangeDateUTC, getutcdate())
				, LastChangedByUser = i.LastChangedByUser
		FROM tblDriverRateSheet RS
		JOIN ( 
			SELECT DISTINCT i.RateSheetID, i.ShipperID, i.CarrierID, i.ProductGroupID, i.TruckTypeID, i.DriverGroupID, i.DriverID, i.OriginStateID, i.DestStateID, i.RegionID, i.ProducerID, i.EffectiveDate, i.EndDate, i.RateTypeID, i.UomID, i.LastChangeDateUTC, i.LastChangedByUser
			FROM inserted i 
			JOIN deleted d ON d.ID = i.ID
			WHERE dbo.fnCompareNullableInts(i.ShipperID, d.ShipperID) = 0
				OR dbo.fnCompareNullableInts(i.CarrierID, d.CarrierID) = 0
				OR dbo.fnCompareNullableInts(i.ProductGroupID, d.ProductGroupID) = 0
			    OR dbo.fnCompareNullableInts(i.TruckTypeID, d.TruckTypeID) = 0
				OR dbo.fnCompareNullableInts(i.DriverGroupID, d.DriverGroupID) = 0
				OR dbo.fnCompareNullableInts(i.DriverID, d.DriverID) = 0
				OR dbo.fnCompareNullableInts(i.OriginStateID, d.OriginStateID) = 0
				OR dbo.fnCompareNullableInts(i.DestStateID, d.DestStateID) = 0
				OR dbo.fnCompareNullableInts(i.RegionID, d.RegionID) = 0
				OR dbo.fnCompareNullableInts(i.ProducerID, d.ProducerID) = 0
				OR i.EndDate <> d.EndDate
		) i ON i.RateSheetID = RS.ID

		--PRINT 'ensure a RateSheet record exists for each new RangeRate records'
		INSERT INTO tblDriverRateSheet (ShipperID, CarrierID, ProductGroupID, TruckTypeID, DriverGroupID, DriverID, OriginStateID, DestStateID, RegionID, ProducerID, EffectiveDate, EndDate, RateTypeID, UomID, CreatedByUser)
			SELECT i.ShipperID, i.CarrierID, i.ProductGroupID, i.TruckTypeID, i.DriverGroupID, i.DriverID, i.OriginStateID, i.DestStateID, i.RegionID, i.ProducerID, i.EffectiveDate, i.EndDate, i.RateTypeID, i.UomID, i.CreatedByUser 
			FROM inserted i
			LEFT JOIN tblDriverRateSheet RS 
				ON i.EffectiveDate = RS.EffectiveDate
					AND dbo.fnCompareNullableInts(i.ShipperID, RS.ShipperID) = 1
					AND dbo.fnCompareNullableInts(i.CarrierID, RS.CarrierID) = 1
					AND dbo.fnCompareNullableInts(i.ProductGroupID, RS.ProductGroupID) = 1
				    AND dbo.fnCompareNullableInts(i.TruckTypeID, RS.TruckTypeID) = 1
					AND dbo.fnCompareNullableInts(i.DriverGroupID, RS.DriverGroupID) = 1
					AND dbo.fnCompareNullableInts(i.DriverID, RS.DriverID) = 1
					AND dbo.fnCompareNullableInts(i.OriginStateID, RS.OriginStateID) = 1
					AND dbo.fnCompareNullableInts(i.DestStateID, RS.DestStateID) = 1
					AND dbo.fnCompareNullableInts(i.RegionID, RS.RegionID) = 1
					AND dbo.fnCompareNullableInts(i.ProducerID, RS.ProducerID) = 1
			WHERE isnull(RateSheetID, 0) = 0
			  AND RS.ID IS NULL
		
		-- optimization for typical usaage (where only 1 record is INSERTed)
		IF (SELECT count(1) FROM inserted i WHERE isnull(ID, 0) = 0) = 1
		BEGIN
			DECLARE @rsID int
			SELECT @rsID = isnull(RateSheetID, SCOPE_IDENTITY()) FROM inserted
			IF @rsID IS NULL
				SELECT @rsID = RS.ID
				FROM inserted i
				LEFT JOIN tblDriverRateSheet RS 
					ON i.EffectiveDate = RS.EffectiveDate
						AND dbo.fnCompareNullableInts(i.ShipperID, RS.ShipperID) = 1
						AND dbo.fnCompareNullableInts(i.CarrierID, RS.CarrierID) = 1
						AND dbo.fnCompareNullableInts(i.ProductGroupID, RS.ProductGroupID) = 1
						AND dbo.fnCompareNullableInts(i.TruckTypeID, RS.TruckTypeID) = 1
						AND dbo.fnCompareNullableInts(i.DriverGroupID, RS.DriverGroupID) = 1
						AND dbo.fnCompareNullableInts(i.DriverID, RS.DriverID) = 1
						AND dbo.fnCompareNullableInts(i.OriginStateID, RS.OriginStateID) = 1
						AND dbo.fnCompareNullableInts(i.DestStateID, RS.DestStateID) = 1
						AND dbo.fnCompareNullableInts(i.RegionID, RS.RegionID) = 1
						AND dbo.fnCompareNullableInts(i.ProducerID, RS.ProducerID) = 1

			INSERT INTO tblDriverRangeRate (RateSheetID, MinRange, MaxRange, Rate, CreateDateUTC, CreatedByUser)
				SELECT @rsID, i.MinRange, i.MaxRange, i.Rate, isnull(i.CreateDateUTC, getutcdate()), i.CreatedByUser
				FROM inserted i
				WHERE isnull(ID, 0) = 0
		END
		ELSE 	-- bulk insert or update
		BEGIN
			-- PRINT 'Updating any existing record editable data'
			UPDATE tblDriverRangeRate
				SET MinRange = i.MinRange
					, MaxRange = i.MaxRange
					, Rate = i.Rate
					, LastChangeDateUTC = isnull(i.LastChangeDateUTC, getutcdate())
					, LastChangedByUser = i.LastChangedByUser
			FROM tblDriverRangeRate X
			JOIN inserted i ON i.ID = X.ID

			-- PRINT 'insert any new records'
			INSERT INTO tblDriverRangeRate (RateSheetID, MinRange, MaxRange, Rate, CreatedByUser)
				SELECT R.ID, i.MinRange, i.MaxRange, i.Rate, isnull(i.CreatedByUser, R.CreatedByUser)
				FROM inserted i
				JOIN tblDriverRateSheet R ON i.EffectiveDate = R.EffectiveDate
					AND dbo.fnCompareNullableInts(i.ShipperID, R.ShipperID) = 1
					AND dbo.fnCompareNullableInts(i.CarrierID, R.CarrierID) = 1
					AND dbo.fnCompareNullableInts(i.ProductGroupID, R.ProductGroupID) = 1
					AND dbo.fnCompareNullableInts(i.TruckTypeID, R.TruckTypeID) = 1
					AND dbo.fnCompareNullableInts(i.DriverGroupID, R.DriverGroupID) = 1
					AND dbo.fnCompareNullableInts(i.DriverID, R.DriverID) = 1
					AND dbo.fnCompareNullableInts(i.OriginStateID, R.OriginStateID) = 1
					AND dbo.fnCompareNullableInts(i.DestStateID, R.DestStateID) = 1
					AND dbo.fnCompareNullableInts(i.RegionID, R.RegionID) = 1
					AND dbo.fnCompareNullableInts(i.ProducerID, R.ProducerID) = 1
				WHERE ISNULL(i.ID, 0) = 0
		END
	END TRY
	BEGIN CATCH
		DECLARE @error varchar(255)
		SET @error = substring(ERROR_MESSAGE(), 1, 255)
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END CATCH
END

GO

/*************************************/
-- Created: 4.1.0 - 2016.08.31 - Kevin Alons
-- Purpose: allow all editing to be done to viewDriverRateSheetRangeRate (due to specialized logic related to RouteID)
-- Changes:
/*************************************/
CREATE TRIGGER trigViewDriverRateSheetRangeRate_D ON viewDriverRateSheetRangeRate INSTEAD OF DELETE AS
BEGIN
	DELETE FROM tblDriverRangeRate WHERE ID IN (SELECT ID FROM deleted)
	DELETE FROM tblDriverRateSheet WHERE ID IN (SELECT DISTINCT RateSheetID FROM deleted) 
		AND ID NOT IN (SELECT DISTINCT RateSheetID FROM tblDriverRangeRate)
END

GO

/******************************************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: add a computed "EndDate" value to all DriverRouteRate records
-- Changes:
/******************************************************/
CREATE VIEW [dbo].[viewDriverRouteRate] AS
	SELECT X.*
		, MaxEffectiveDate = (SELECT MIN(OrderDate) FROM tblOrderSettlementDriver WHERE RouteRateID = X.ID AND BatchID IS NOT NULL)
		, MinEndDate = (SELECT MAX(OrderDate) FROM tblOrderSettlementDriver WHERE RouteRateID = X.ID AND BatchID IS NOT NULL)
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementDriver WHERE RouteRateID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, R.OriginID
		, R.DestinationID
		, R.ActualMiles
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblDriverRouteRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverID, X.DriverID) = 1
			  AND XN.RouteID = X.RouteID
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT min(XN.EndDate) 
			FROM tblDriverRouteRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverID, X.DriverID) = 1
			  AND XN.RouteID = X.RouteID
			  AND XN.EffectiveDate < X.EffectiveDate)
	FROM tblDriverRouteRate X
	JOIN tblRoute R ON R.ID = X.RouteID

GO

/***********************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details + FINANCIAL INFO into a single view
-- Changes:
/***********************************/
CREATE VIEW [dbo].[viewOrderSettlementDriver] AS 
	SELECT OSC.*
		, BatchNum = SB.BatchNum
		, SettlementUom = UOM.Name
		, SettlementUomShort = UOM.Abbrev
		, RouteRate = RR.Rate
		, RouteRateType = RRT.Name
		, RateSheetRate = RSRR.Rate
		, RateSheetRateType = RSRT.Name
		, OrderRejectRate = ORR.Rate
		, OrderRejectRateType = ORRT.Name
		, OriginWaitRate = OWR.Rate
		, DestinationWaitRate = DWR.Rate
		, TotalWaitAmount = cast(isnull(OSC.OriginWaitAmount, 0) as money) + cast(isnull(OSC.DestinationWaitAmount, 0) as money)
		, ChainupRate = CAR.Rate
		, ChainupRateType = CART.Name
		, ChainupAmount = CA.Amount
		, RerouteRate = RAR.Rate
		, RerouteRateType = RART.Name
		, RerouteAmount = RA.Amount
		, SplitLoadRate = SAR.Rate
		, SplitLoadRateType = SART.Name
		, SplitLoadAmount = SA.Amount
		, H2SRate = HAR.Rate
		, H2SRateType = HART.Name
		, H2SAmount = HA.Amount
		, OtherAmount = OA.Amount
		, WaitFeeSubUnit = isnull(WFSU.Name, 'None') 
		, WaitFeeRoundingType = isnull(WFRT.Name, 'None')
		, SettlementFactor = SF.Name
	FROM tblOrderSettlementDriver OSC 
	LEFT JOIN tblDriverSettlementBatch SB ON SB.ID = OSC.BatchID
	LEFT JOIN tblUom UOM ON UOM.ID = OSC.SettlementUomID
	LEFT JOIN tblDriverOriginWaitRate OWR ON OWR.ID = OSC.OriginWaitRateID
	LEFT JOIN tblDriverDestinationWaitRate DWR ON DWR.ID = OSC.DestinationWaitRateID
	LEFT JOIN tblDriverOrderRejectRate ORR ON ORR.ID = OSC.OrderRejectRateID
	LEFT JOIN tblRateType ORRT ON ORRT.ID = ORR.RateTypeID
	-- route rate/ratesheet JOINs
	LEFT JOIN tblDriverRouteRate RR ON RR.ID = OSC.RouteRateID
	LEFT JOIN tblRateType RRT ON RRT.ID = RR.RateTypeID
	LEFT JOIN viewDriverRateSheetRangeRate RSRR ON RSRR.ID = OSC.RangeRateID
	LEFT JOIN tblRateType RSRT ON RSRT.ID = RSRR.RateTypeID
	-- chainup
	LEFT JOIN tblOrderSettlementDriverAssessorialCharge CA ON CA.OrderID = OSC.OrderID AND CA.AssessorialRateTypeID = 1
	LEFT JOIN tblDriverAssessorialRate CAR ON CAR.ID = CA.AssessorialRateID
	LEFT JOIN tblRateType CART ON CART.ID = CAR.RateTypeID
	-- Reroute
	LEFT JOIN tblOrderSettlementDriverAssessorialCharge RA ON RA.OrderID = OSC.OrderID AND RA.AssessorialRateTypeID = 2
	LEFT JOIN tblDriverAssessorialRate RAR ON RAR.ID = RA.AssessorialRateID
	LEFT JOIN tblRateType RART ON RART.ID = RAR.RateTypeID
	-- Split Load
	LEFT JOIN tblOrderSettlementDriverAssessorialCharge SA ON SA.OrderID = OSC.OrderID AND SA.AssessorialRateTypeID = 3
	LEFT JOIN tblDriverAssessorialRate SAR ON SAR.ID = SA.AssessorialRateID
	LEFT JOIN tblRateType SART ON SART.ID = SAR.RateTypeID
	-- H2S
	LEFT JOIN tblOrderSettlementDriverAssessorialCharge HA ON HA.OrderID = OSC.OrderID AND HA.AssessorialRateTypeID = 4
	LEFT JOIN tblDriverAssessorialRate HAR ON HAR.ID = HA.AssessorialRateID
	LEFT JOIN tblRateType HART ON HART.ID = HAR.RateTypeID
	-- Other Assessorial Charges (combined)
	LEFT JOIN (
		SELECT OrderID, Amount = SUM(Amount) FROM tblOrderSettlementDriverAssessorialCharge WHERE AssessorialRateTypeID NOT IN (1,2,3,4) GROUP BY OrderID
	) OA ON OA.OrderID = OSC.OrderID 
	LEFT JOIN tblSettlementFactor SF ON SF.ID = OSC.SettlementFactorID
	LEFT JOIN tblCarrierWaitFeeParameter WFP ON WFP.ID = OSC.WaitFeeParameterID
	LEFT JOIN dbo.tblWaitFeeSubUnit WFSU ON WFSU.ID = WFP.SubUnitID
	LEFT JOIN dbo.tblWaitFeeRoundingType WFRT ON WFRT.ID = WFP.RoundingTypeID

GO

/***********************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: return OrderSettlementDriverAssessorialCharge records with BatchId included (to determine whether actually "Settled" or not)
-- Changes:
/***********************************/
CREATE VIEW [dbo].[viewOrderSettlementDriverAssessorialCharge] AS
	SELECT AC.*
		, SC.OrderDate
		 , SC.BatchID
	FROM tblOrderSettlementDriverAssessorialCharge AC
	JOIN tblOrderSettlementDriver SC ON SC.OrderID = AC.OrderID

GO

/******************************************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: add a computed "EndDate" value to all DriverAssessorialRate records
-- Changes:
/******************************************************/
CREATE VIEW dbo.viewDriverAssessorialRate AS
	SELECT X.*
		, MaxEffectiveDate = (SELECT min(OrderDate) FROM viewOrderSettlementDriverAssessorialCharge WHERE AssessorialRateID = X.ID AND BatchID IS NOT NULL)
		, MinEndDate = (SELECT max(OrderDate) FROM viewOrderSettlementDriverAssessorialCharge WHERE AssessorialRateID = X.ID AND BatchID IS NOT NULL)
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM viewOrderSettlementDriverAssessorialCharge WHERE AssessorialRateID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblDriverAssessorialRate XN 
			WHERE XN.TypeID = X.TypeID
			  AND dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverID, X.DriverID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT max(XN.EndDate) 
			FROM tblDriverAssessorialRate XN 
			WHERE XN.TypeID = X.TypeID
			  AND dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverID, X.DriverID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EndDate < X.EffectiveDate)
	FROM tblDriverAssessorialRate X

GO

/******************************************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: add a computed "EndDate" value to all DriverDestinationWaitRate records
-- Changes:
/******************************************************/
CREATE VIEW [dbo].[viewDriverDestinationWaitRate] AS
	SELECT X.*
		, MaxEffectiveDate = (SELECT Min(OrderDate) FROM tblOrderSettlementDriver WHERE DestinationWaitRateID = X.ID AND BatchID IS NOT NULL)
		, MinEndDate = (SELECT Max(OrderDate) FROM tblOrderSettlementDriver WHERE DestinationWaitRateID = X.ID AND BatchID IS NOT NULL)
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementDriver WHERE DestinationWaitRateID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate)
			FROM tblDriverDestinationWaitRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverID, X.DriverID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(XN.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT max(XN.EndDate)
			FROM tblDriverDestinationWaitRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverID, X.DriverID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(XN.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EndDate < X.EffectiveDate)
	FROM tblDriverDestinationWaitRate X

GO

/******************************************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: add a computed "EndDate" value to all DriverFuelSurcharge records
-- Changes:
/******************************************************/
CREATE VIEW [dbo].[viewDriverFuelSurchargeRate] AS
	SELECT X.*
		, MaxEffectiveDate = (SELECT min(OrderDate) FROM tblOrderSettlementDriver WHERE FuelSurchargeRateID = X.ID AND BatchID IS NOT NULL)
		, MinEndDate = (SELECT max(OrderDate) FROM tblOrderSettlementDriver WHERE FuelSurchargeRateID = X.ID AND BatchID IS NOT NULL)
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementDriver WHERE FuelSurchargeRateID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate)
			FROM tblDriverDestinationWaitRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverID, X.DriverID) = 1
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT max(XN.EndDate)
			FROM tblDriverDestinationWaitRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverID, X.DriverID) = 1
			  AND XN.EndDate < X.EffectiveDate)
	FROM tblDriverFuelSurchargeRate X

GO

/******************************************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: add a computed "EndDate" value to all DriverOrderRejectRate records
-- Changes:
/******************************************************/
CREATE VIEW [dbo].[viewDriverOrderRejectRate] AS
	SELECT X.*
		, MaxEffectiveDate = (SELECT MIN(OrderDate) FROM tblOrderSettlementDriver WHERE OrderRejectRateID = X.ID AND BatchID IS NOT NULL)
		, MinEndDate = (SELECT MAX(OrderDate) FROM tblOrderSettlementDriver WHERE OrderRejectRateID = X.ID AND BatchID IS NOT NULL)
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementDriver WHERE OrderRejectRateID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblDriverOrderRejectRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverID, X.DriverID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(XN.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT max(XN.EndDate) 
			FROM tblDriverOrderRejectRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverID, X.DriverID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(XN.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EndDate < X.EffectiveDate)
	FROM tblDriverOrderRejectRate X

GO

/******************************************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: add a computed "EndDate" value to all DriverOriginWaitRate records
-- Changes:
/******************************************************/
CREATE VIEW [dbo].[viewDriverOriginWaitRate] AS
	SELECT X.*
		, MaxEffectiveDate = (SELECT MIN(OrderDate) FROM tblOrderSettlementDriver WHERE OriginWaitRateID = X.ID AND BatchID IS NOT NULL)
		, MinEndDate = (SELECT Max(OrderDate) FROM tblOrderSettlementDriver WHERE OriginWaitRateID = X.ID AND BatchID IS NOT NULL)
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementDriver WHERE OriginWaitRateID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate)
			FROM tblDriverOriginWaitRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverID, X.DriverID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(XN.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT max(XN.EndDate)
			FROM tblDriverOriginWaitRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverID, X.DriverID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(XN.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EndDate < X.EffectiveDate)
	FROM tblDriverOriginWaitRate X

GO

/***********************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: return the DriverSettlementBatch table contents with the CreateDate formatted DateOnly (for the BatchDate)
-- Changes:
/***********************************/
CREATE VIEW viewDriverSettlementBatch AS
SELECT X.*
	, FullName = '#' + ltrim(BatchNum) + ' [' + dbo.fnDateMdYY(BatchDate) + '] ' + ' / ' + ltrim(OrderCount) + isnull(' | ' + InvoiceNum, '')
FROM (
	SELECT SB.*
		, Carrier = dbo.fnNameWithDeleted(C.Name, C.DeleteDateUTC)
		, DriverGroup = dbo.fnNameWithDeleted(DG.Name, C.DeleteDateUTC)
		, Driver = dbo.fnNameWithDeleted(DR.FullName, DR.DeleteDateUTC)
		, OrderCount = (SELECT COUNT(*) FROM tblOrderSettlementDriver WHERE BatchID = SB.ID)
	FROM dbo.tblDriverSettlementBatch SB
	JOIN dbo.tblCarrier C ON C.ID = SB.CarrierID
	LEFT JOIN tblDriverGroup DG ON DG.ID = SB.DriverGroupID
	LEFT JOIN viewDriverBase DR ON DR.ID = SB.DriverID
) X

GO

/***********************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: retrieve and return the Driver AssessorialRate info for the specified order
-- Changes:
/***********************************/
CREATE FUNCTION [fnDriverAssessorialRates]
(
  @StartDate date
, @EndDate date
, @TypeID int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @TruckTypeID int
, @DriverGroupID int
, @DriverID int
, @OriginID int
, @DestinationID int
, @OriginStateID int
, @DestStateID int
, @RegionID int
, @ProducerID int
, @BestMatchOnly bit = 0
)
RETURNS 
	@ret TABLE (
		ID int
	  , TypeID int
	  , ShipperID int
	  , CarrierID int
	  , ProductGroupID int
	  , TruckTypeID int
	  , DriverGroupID int
	  , DriverID int
	  , OriginID int
	  , DestinationID int
	  , OriginStateID int
	  , DestStateID int
	  , RegionID int
	  , ProducerID int
	  , Rate decimal(18, 10)
	  , RateTypeID int
	  , UomID int
	  , EffectiveDate date
	  , EndDate date
	  , MaxEffectiveDate date
	  , MinEndDate date
	  , NextEffectiveDate date
	  , PriorEndDate date	  
	  , BestMatch bit
	  , Ranking smallmoney
	  , Locked bit
	  , CreateDateUTC datetime
	  , CreatedByUser varchar(100)
	  , LastChangeDateUTC datetime
	  , LastChangedByUser varchar(100)
	)
AS BEGIN
	-- get the raw data (all matched rows)
	DECLARE @src TABLE (ID int, TypeID int, Ranking smallmoney)
	INSERT INTO @src (ID, TypeID, Ranking)
		SELECT ID, TypeID
			, Ranking =	dbo.fnRateRanking(@TruckTypeID, R.TruckTypeID, 2048, 0)
					  + dbo.fnRateRanking(@ShipperID, R.ShipperID, 1024, 0)
					  + dbo.fnRateRanking(@CarrierID, R.CarrierID, 512, 0)
					  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 256, 0)
					  + dbo.fnRateRanking(@DriverGroupID, R.DriverGroupID, 128, 1)
					  + dbo.fnRateRanking(@DriverID, R.DriverID, 64, 1)
					  + dbo.fnRateRanking(@OriginID, R.OriginID, 32, 0)
					  + dbo.fnRateRanking(@DestinationID, R.DestinationID, 16, 0)
					  + dbo.fnRateRanking(@OriginStateID, R.OriginStateID, 8, 0)
					  + dbo.fnRateRanking(@DestStateID, R.DestStateID, 4, 0)
					  + dbo.fnRateRanking(@RegionID, R.RegionID, 2, 0)
					  + dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM dbo.viewDriverAssessorialRate R
		WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(R.ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@TruckTypeID, 0), R.TruckTypeID, 0) = coalesce(R.TruckTypeID, nullif(@TruckTypeID, 0), 0)
		  AND coalesce(nullif(@DriverGroupID, 0), R.DriverGroupID, 0) = coalesce(DriverGroupID, nullif(@DriverGroupID, 0), 0)
		  AND coalesce(nullif(@DriverID, 0), R.DriverID, 0) = coalesce(DriverID, nullif(@DriverID, 0), 0)
		  AND coalesce(nullif(@OriginID, 0), R.OriginID, 0) = coalesce(R.OriginID, nullif(@OriginID, 0), 0)
		  AND coalesce(nullif(@DestinationID, 0), R.DestinationID, 0) = coalesce(R.DestinationID, nullif(@DestinationID, 0), 0)
		  AND coalesce(nullif(@OriginStateID, 0), R.OriginStateID, 0) = coalesce(R.OriginStateID, nullif(@OriginStateID, 0), 0)
		  AND coalesce(nullif(@DestStateID, 0), R.DestStateID, 0) = coalesce(R.DestStateID, nullif(@DestStateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(R.RegionID, nullif(@RegionID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(R.ProducerID, nullif(@ProducerID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
		  AND (nullif(@TypeID, 0) IS NULL OR TypeID = @TypeID)

	-- return the rate data for the best-match rates 
	INSERT INTO @ret (ID, TypeID, ShipperID, CarrierID, ProductGroupID, TruckTypeID, DriverGroupID, DriverID, OriginID, DestinationID, OriginStateID, DestStateID, RegionID, ProducerID, Rate, RateTypeID, UomID, EffectiveDate, EndDate
			, MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate, BestMatch, Ranking, Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser)
		SELECT CAR.ID, TypeID, ShipperID, CarrierID, ProductGroupID, TruckTypeID, DriverGroupID, DriverID, OriginID, DestinationID, OriginStateID, DestStateID, RegionID, ProducerID, Rate, RateTypeID, UomID, EffectiveDate, EndDate
			, MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate, BestMatch, Ranking, Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser
		FROM viewDriverAssessorialRate CAR
		JOIN (
			SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.TypeID IS NULL THEN 0 ELSE 1 END as bit)
			FROM @src S
			LEFT JOIN (
				SELECT TypeID, Ranking = MAX(Ranking)
				FROM @src
				WHERE @StartDate = ISNULL(@EndDate, @StartDate)
				  AND Ranking % 1 = 0.01 * 12  -- ensure some type of match occurred on all criteria choices
				GROUP BY TypeID
			) X ON X.TypeID = S.TypeID AND X.Ranking = S.Ranking
		) X ON X.ID = CAR.ID
		WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
		
	RETURN
END

GO

/***********************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: retrieve and return the Driver AssessorialRate rows for the specified criteria
-- Changes:
/**********************************/
CREATE FUNCTION [fnDriverAssessorialRatesDisplay]
(  
  @StartDate date
, @EndDate date
, @TypeID int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @TruckTypeID int
, @DriverGroupID int
, @DriverID int
, @OriginID int
, @DestinationID int
, @OriginStateID int
, @DestStateID int
, @RegionID int
, @ProducerID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.TruckTypeID, R.DriverGroupID, R.DriverID, R.OriginID, R.DestinationID, R.OriginStateID, R.DestStateID, R.RegionID, R.ProducerID, R.TypeID, R.Rate, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Type = RT.Name
		, Shipper = S.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, TruckType = TT.Name
		, DriverGroup = DG.Name
		, Driver = DR.FullName
		, Origin = O.Name
		, OriginFull = O.FullName
		, Destination = D.Name
		, DestinationFull = D.FullName
		, OriginState = OS.FullName
		, OriginStateAbbrev = OS.Abbreviation
		, DestState = DS.FullName
		, DestStateAbbrev = DS.Abbreviation
		, Region = RE.Name
		, Producer = PR.Name
		, RateType = RT.Name
		, Uom = U.Name
		, UomShort = U.Abbrev
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch, Ranking
	FROM dbo.fnDriverAssessorialRates(@StartDate, @EndDate, @TypeID, @ShipperID, @CarrierID, @ProductGroupID, @TruckTypeID, @DriverGroupID, @DriverID, @OriginID, @DestinationID, @OriginStateID, @DestStateID, @RegionID, @ProducerID, 0) R
	JOIN tblAssessorialRateType RT ON RT.ID = R.TypeID
	LEFT JOIN tblCustomer S ON S.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblTruckType TT ON TT.ID = R.TruckTypeID
	LEFT JOIN tblDriverGroup DG ON DG.ID = R.DriverGroupID
	LEFT JOIN viewDriverBase DR ON DR.ID = R.DriverID
	LEFT JOIN viewOrigin O ON O.ID = R.OriginID
	LEFT JOIN viewDestination D ON D.ID = R.DestinationID
	LEFT JOIN tblState OS ON OS.ID = R.OriginStateID
	LEFT JOIN tblState DS ON DS.ID = R.DestStateID
	LEFT JOIN tblRegion RE ON RE.ID = R.RegionID
	LEFT JOIN tblProducer PR ON PR.ID = R.ProducerID
	LEFT JOIN tblUom U ON U.ID = R.UomID
	ORDER BY EffectiveDate

GO

/***********************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: retrieve and return the Driver DestinationWaitRate info for the specified criteria
-- Changes:
/***********************************/
CREATE FUNCTION fnDriverDestinationWaitRate
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @TruckTypeID int
, @DriverGroupID int
, @DriverID int
, @DestinationID int
, @StateID int
, @RegionID int
, @ProducerID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID, ReasonID
			, Ranking =	dbo.fnRateRanking(@ReasonID, R.ReasonID, 1024, 1)
				  + dbo.fnRateRanking(@TruckTypeID, R.TruckTypeID, 512, 1)
				  + dbo.fnRateRanking(@ShipperID, R.ShipperID, 256, 0)
				  + dbo.fnRateRanking(@CarrierID, R.CarrierID, 128, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 64, 0)
				  + dbo.fnRateRanking(@DriverGroupID, R.DriverGroupID, 32, 1)
				  + dbo.fnRateRanking(@DriverID, R.DriverID, 16, 0)
				  + dbo.fnRateRanking(@DestinationID, R.DestinationID, 8, 0)
				  + dbo.fnRateRanking(@StateID, R.StateID, 4, 0)
				  + dbo.fnRateRanking(@RegionID, R.RegionID, 2, 0)
				  + dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 0)
		FROM  dbo.viewDriverDestinationWaitRate R
		WHERE coalesce(nullif(@ReasonID, 0), R.ReasonID, 0) = coalesce(ReasonID, nullif(@ReasonID, 0), 0)
		  AND coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@TruckTypeID, 0), R.TruckTypeID, 0) = coalesce(TruckTypeID, nullif(@TruckTypeID, 0), 0)
		  AND coalesce(nullif(@DriverGroupID, 0), R.DriverGroupID, 0) = coalesce(DriverGroupID, nullif(@DriverGroupID, 0), 0)
		  AND coalesce(nullif(@DriverID, 0), R.DriverID, 0) = coalesce(DriverID, nullif(@DriverID, 0), 0)
		  AND coalesce(nullif(@DestinationID, 0), R.DestinationID, 0) = coalesce(DestinationID, nullif(@DestinationID, 0), 0)
		  AND coalesce(nullif(@StateID, 0), R.StateID, 0) = coalesce(StateID, nullif(@StateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(ProducerID, nullif(@ProducerID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	),
	cte2 AS
	(
		SELECT ID, S.ReasonID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT ReasonID = ISNULL(ReasonID, 0), Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 11  -- ensure some type of match occurred on all criteria choices
			GROUP BY ReasonID
		) X ON X.ReasonID = isnull(S.ReasonID, 0) AND X.Ranking = S.Ranking
	), 
	cte3 AS
	(	-- when both ReasonID = NULL AND ReasonID = @ReasonID records are BestMatch = 1, favor the ReasonID = @ReasonID record
		SELECT ID, X.ReasonID, X.Ranking, BestMatch = CASE WHEN BEST.ReasonID IS NULL THEN X.BestMatch ELSE 0 END
		FROM cte2 X
		LEFT JOIN (
			SELECT TOP 1 ReasonID FROM cte2 WHERE ReasonID = @ReasonID AND BestMatch = 1 ORDER BY Ranking DESC
		) BEST ON X.ReasonID IS NULL AND X.BestMatch = 1
	)
	
	SELECT TOP (CASE WHEN @BestMatchOnly = 1 THEN 1 ELSE 10000 END) R.ID, R.ReasonID, ShipperID, CarrierID, ProductGroupID, TruckTypeID, DriverGroupID, DriverID, DestinationID, StateID, RegionID, ProducerID
		, Rate, EffectiveDate, EndDate
	  , MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate
	  , BestMatch, Ranking
	  , Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewDriverDestinationWaitRate R
	JOIN cte3 X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
	ORDER BY Ranking DESC
)

GO

/***********************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: retrieve and return the Driver DestinationWaitRate rows for the specified criteria
-- Changes:
/***********************************/
CREATE FUNCTION [fnDriverDestinationWaitRatesDisplay]
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @TruckTypeID int
, @DriverGroupID int
, @DriverID int
, @DestinationID int
, @StateID int
, @RegionID int
, @ProducerID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ReasonID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.TruckTypeID, R.DriverGroupID, R.DriverID, R.DestinationID, R.StateID, R.RegionID, R.ProducerID, R.Rate, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Shipper = SH.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, TruckType = TT.Name
		, DriverGroup = DG.Name
		, Driver = DR.FullName
		, Destination = D.Name
		, DestinationFull = D.FullName
		, State = S.FullName
		, StateAbbrev = S.Abbreviation
		, Region = REG.Name
		, Producer = P.Name
		, Reason = REA.Description
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
		, Ranking
	FROM dbo.fnDriverDestinationWaitRate(@StartDate, @EndDate, @ReasonID, @ShipperID, @CarrierID, @ProductGroupID, @TruckTypeID, @DriverGroupID, @DriverID, @DestinationID, @StateID, @RegionID, @ProducerID, 0) R
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblTruckType TT ON TT.ID = R.TruckTypeID
	LEFT JOIN tblDriverGroup DG ON DG.ID = R.DriverGroupID
	LEFT JOIN viewDriverBase DR ON DR.ID = R.DriverID
	LEFT JOIN viewDestination D ON D.ID = R.DestinationID
	LEFT JOIN tblState S ON S.ID = R.StateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblProducer P ON P.ID = R.ProducerID
	LEFT JOIN tblDestinationWaitReason REA ON REA.ID = R.ReasonID
	ORDER BY EffectiveDate

GO

/***********************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: retrieve and return the Driver DestinationWaitRate info for the specified criteria
-- Changes:
/***********************************/
CREATE FUNCTION [fnDriverFuelSurchargeRate]
(
  @StartDate date
, @EndDate date
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @TruckTypeID int
, @DriverGroupID int
, @DriverID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID
			, Ranking =	dbo.fnRateRanking(@TruckTypeID, R.TruckTypeID, 32, 0)
				  + dbo.fnRateRanking(@ShipperID, R.ShipperID, 16, 0)
				  + dbo.fnRateRanking(@CarrierID, R.CarrierID, 8, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 4, 0)
				  + dbo.fnRateRanking(@DriverGroupID, R.DriverGroupID, 2, 1)
				  + dbo.fnRateRanking(@DriverID, R.DriverID, 1, 1)
		FROM  dbo.viewDriverFuelSurchargeRate R
		WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@TruckTypeID, 0), R.TruckTypeID, 0) = coalesce(TruckTypeID, nullif(@TruckTypeID, 0), 0)
		  AND coalesce(nullif(@DriverGroupID, 0), R.DriverGroupID, 0) = coalesce(DriverGroupID, nullif(@DriverGroupID, 0), 0)
		  AND coalesce(nullif(@DriverID, 0), R.DriverID, 0) = coalesce(DriverID, nullif(@DriverID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	)
	SELECT R.ID, ShipperID, CarrierID, ProductGroupID, TruckTypeID, DriverGroupID, DriverID, FuelPriceFloor, IntervalAmount, IncrementAmount, EffectiveDate, EndDate
	  , MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate
	  , BestMatch, Ranking
	  , Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewDriverFuelSurchargeRate R
	JOIN (
		SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 6  -- ensure some type of match occurred on all criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)

GO

/***********************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: retrieve and return the Driver RouteRate rows for the specified criteria
-- Changes:
/***********************************/
CREATE FUNCTION [fnDriverFuelSurchargeRateDisplay]
(
  @StartDate date
, @EndDate date
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @TruckTypeID int
, @DriverGroupID int
, @DriverID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.TruckTypeID, R.DriverGroupID, R.DriverID, R.FuelPriceFloor, R.IntervalAmount, R.IncrementAmount, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Shipper = S.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, TruckType = TT.Name
		, DriverGroup = DG.Name
		, Driver = DR.FullName
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch, Ranking
	FROM dbo.fnDriverFuelSurchargeRate(@StartDate, @EndDate, @ShipperID, @CarrierID, @ProductGroupID, @TruckTypeID, @DriverGroupID, @DriverID, 0) R
	LEFT JOIN tblCustomer S ON S.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblTruckType TT ON TT.ID = R.TruckTypeID
	LEFT JOIN tblDriverGroup DG ON DG.ID = R.DriverGroupID
	LEFT JOIN viewDriverBase DR ON DR.ID = R.DriverID
	ORDER BY EffectiveDate

GO

/***********************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: retrieve and return the Driver OrderRejectRate info for the specified criteria
-- Changes:
/***********************************/
CREATE FUNCTION fnDriverOrderRejectRate
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @TruckTypeID int
, @DriverGroupID int
, @DriverID int
, @OriginID int
, @StateID int
, @RegionID int
, @ProducerID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID, ReasonID
			, Ranking =	dbo.fnRateRanking(@ReasonID, R.ReasonID, 1024, 1)
				  + dbo.fnRateRanking(@TruckTypeID, R.TruckTypeID, 512, 1)
				  + dbo.fnRateRanking(@ShipperID, R.ShipperID, 256, 0)
				  + dbo.fnRateRanking(@CarrierID, R.CarrierID, 128, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 64, 0)
				  + dbo.fnRateRanking(@DriverGroupID, R.DriverGroupID, 32, 1)
				  + dbo.fnRateRanking(@DriverID, R.DriverID, 16, 0)
				  + dbo.fnRateRanking(@OriginID, R.OriginID, 8, 0)
				  + dbo.fnRateRanking(@StateID, R.StateID, 4, 0)
				  + dbo.fnRateRanking(@RegionID, R.RegionID, 2, 0)
				  + dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM  dbo.viewDriverOrderRejectRate R
		WHERE coalesce(nullif(@ReasonID, 0), R.ReasonID, 0) = coalesce(ReasonID, nullif(@ReasonID, 0), 0)
		  AND coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@TruckTypeID, 0), R.TruckTypeID, 0) = coalesce(TruckTypeID, nullif(@TruckTypeID, 0), 0)
		  AND coalesce(nullif(@DriverGroupID, 0), R.DriverGroupID, 0) = coalesce(DriverGroupID, nullif(@DriverGroupID, 0), 0)
		  AND coalesce(nullif(@DriverID, 0), R.DriverID, 0) = coalesce(DriverID, nullif(@DriverID, 0), 0)
		  AND coalesce(nullif(@OriginID, 0), R.OriginID, 0) = coalesce(OriginID, nullif(@OriginID, 0), 0)
		  AND coalesce(nullif(@StateID, 0), R.StateID, 0) = coalesce(StateID, nullif(@StateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(ProducerID, nullif(@ProducerID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	),
	cte2 AS
	(
		SELECT ID, S.ReasonID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT ReasonID = ISNULL(ReasonID, 0), Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 11  -- ensure some type of match occurred on all criteria choices
			GROUP BY ReasonID
		) X ON X.ReasonID = isnull(S.ReasonID, 0) AND X.Ranking = S.Ranking
	), 
	cte3 AS
	(	-- when both ReasonID = NULL AND ReasonID = @ReasonID records are BestMatch = 1, favor the ReasonID = @ReasonID record
		SELECT ID, X.ReasonID, X.Ranking, BestMatch = CASE WHEN BEST.ReasonID IS NULL THEN X.BestMatch ELSE 0 END
		FROM cte2 X
		LEFT JOIN (
			SELECT TOP 1 ReasonID FROM cte2 WHERE ReasonID = @ReasonID AND BestMatch = 1 ORDER BY Ranking DESC
		) BEST ON X.ReasonID IS NULL AND X.BestMatch = 1
	)
	
	SELECT R.ID, R.ReasonID, ShipperID, CarrierID, ProductGroupID, TruckTypeID, DriverGroupID, DriverID, OriginID, StateID, RegionID, ProducerID
	  , Rate, RateTypeID, UomID, EffectiveDate, EndDate
	  , MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate
	  , BestMatch, Ranking
	  , Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewDriverOrderRejectRate R
	JOIN cte3 X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)

GO

/***********************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: retrieve and return the Driver OrderRejectRate rows for the specified criteria
-- Changes:
/***********************************/
CREATE FUNCTION [fnDriverOrderRejectRatesDisplay]
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @TruckTypeID int
, @DriverGroupID int
, @DriverID int
, @OriginID int
, @StateID int
, @RegionID int
, @ProducerID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ReasonID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.TruckTypeID, R.DriverGroupID, R.DriverID, R.OriginID, R.StateID, R.RegionID, R.ProducerID
		, R.Rate, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Shipper = SH.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, TruckType = TT.Name
		, DriverGroup = DG.Name
		, Driver = DR.FullName
		, Origin = D.Name
		, OriginFull = D.FullName
		, State = S.FullName
		, StateAbbrev = S.Abbreviation
		, Region = REG.Name
		, Producer = PR.Name
		, Reason = REA.Description
		, RateType = RT.Name
		, Uom = U.Name
		, UomShort = U.Abbrev
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
	FROM dbo.fnDriverOrderRejectRate(@StartDate, @EndDate, @ReasonID, @ShipperID, @CarrierID, @ProductGroupID, @TruckTypeID, @DriverGroupID, @DriverID, @OriginID, @StateID, @RegionID, @ProducerID, 0) R
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblTruckType TT ON TT.ID = R.TruckTypeID
	LEFT JOIN tblDriverGroup DG ON DG.ID = R.DriverGroupID
	LEFT JOIN viewDriverBase DR ON DR.ID = R.DriverID
	LEFT JOIN viewOrigin D ON D.ID = R.OriginID
	LEFT JOIN tblState S ON S.ID = R.StateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblProducer PR ON PR.ID = R.ProducerID
	LEFT JOIN tblOrderRejectReason REA ON REA.ID = R.ReasonID
	LEFT JOIN tblRateType RT ON RT.ID = R.RateTypeID
	LEFT JOIN tblUom U ON U.ID = R.UomID
	ORDER BY EffectiveDate

GO

/***********************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: retrieve and return the Driver OriginWaitRate info for the specified criteria
-- Changes:
/***********************************/
CREATE FUNCTION fnDriverOriginWaitRate
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @TruckTypeID int
, @DriverGroupID int
, @DriverID int
, @OriginID int
, @StateID int
, @RegionID int
, @ProducerID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID, ReasonID
			, Ranking =	dbo.fnRateRanking(@ReasonID, R.ReasonID, 1024, 1)
				  + dbo.fnRateRanking(@TruckTypeID, R.TruckTypeID, 512, 1)
				  + dbo.fnRateRanking(@ShipperID, R.ShipperID, 256, 0)
				  + dbo.fnRateRanking(@CarrierID, R.CarrierID, 128, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 64, 0)
				  + dbo.fnRateRanking(@DriverGroupID, R.DriverGroupID, 32, 1)
				  + dbo.fnRateRanking(@DriverID, R.DriverID, 16, 0)
				  + dbo.fnRateRanking(@OriginID, R.OriginID, 8, 0)
				  + dbo.fnRateRanking(@StateID, R.StateID, 4, 0)
				  + dbo.fnRateRanking(@RegionID, R.RegionID, 2, 0)
				  + dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM  dbo.viewDriverOriginWaitRate R
		WHERE coalesce(nullif(@ReasonID, 0), R.ReasonID, 0) = coalesce(ReasonID, nullif(@ReasonID, 0), 0)
		  AND coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@TruckTypeID, 0), R.TruckTypeID, 0) = coalesce(TruckTypeID, nullif(@TruckTypeID, 0), 0)
		  AND coalesce(nullif(@DriverGroupID, 0), R.DriverGroupID, 0) = coalesce(DriverGroupID, nullif(@DriverGroupID, 0), 0)
		  AND coalesce(nullif(@DriverID, 0), R.DriverID, 0) = coalesce(DriverID, nullif(@DriverID, 0), 0)
		  AND coalesce(nullif(@OriginID, 0), R.OriginID, 0) = coalesce(OriginID, nullif(@OriginID, 0), 0)
		  AND coalesce(nullif(@StateID, 0), R.StateID, 0) = coalesce(StateID, nullif(@StateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(ProducerID, nullif(@ProducerID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	),
	cte2 AS
	(
		SELECT ID, S.ReasonID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT ReasonID = ISNULL(ReasonID, 0), Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 11  -- ensure some type of match occurred on all criteria choices
			GROUP BY ReasonID
		) X ON X.ReasonID = isnull(S.ReasonID, 0) AND X.Ranking = S.Ranking
	), 
	cte3 AS
	(	-- when both ReasonID = NULL AND ReasonID = @ReasonID records are BestMatch = 1, favor the ReasonID = @ReasonID record
		SELECT ID, X.ReasonID, X.Ranking, BestMatch = CASE WHEN BEST.ReasonID IS NULL THEN X.BestMatch ELSE 0 END
		FROM cte2 X
		LEFT JOIN (
			SELECT TOP 1 ReasonID FROM cte2 WHERE ReasonID = @ReasonID AND BestMatch = 1 ORDER BY Ranking DESC
		) BEST ON X.ReasonID IS NULL AND X.BestMatch = 1
	)
	
	SELECT TOP (CASE WHEN @BestMatchOnly = 1 THEN 1 ELSE 10000 END) R.ID, R.ReasonID, ShipperID, CarrierID, ProductGroupID, TruckTypeID, DriverGroupID, DriverID, OriginID, StateID, RegionID, ProducerID
	  , Rate, EffectiveDate, EndDate
	  , R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
	  , BestMatch, Ranking
	  , Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewDriverOriginWaitRate R
	JOIN cte3 X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
	ORDER BY Ranking DESC
)

GO

/***********************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: retrieve and return the Driver OriginWaitRate rows for the specified criteria
-- Changes:
/***********************************/
CREATE FUNCTION [fnDriverOriginWaitRatesDisplay]
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @TruckTypeID int
, @OriginID int
, @DriverGroupID int
, @DriverID int
, @StateID int
, @RegionID int
, @ProducerID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ReasonID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.TruckTypeID, R.DriverGroupID, R.DriverID, R.OriginID, R.StateID, R.RegionID, R.ProducerID
		, R.Rate, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Shipper = SH.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, TruckType = TT.Name
		, DriverGroup = DG.Name
		, Driver = DR.FullName
		, Origin = D.Name
		, OriginFull = D.FullName
		, State = S.FullName
		, StateAbbrev = S.Abbreviation
		, Region = REG.Name
		, Producer = PR.Name
		, Reason = REA.Description
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
		, Ranking
	FROM dbo.fnDriverOriginWaitRate(@StartDate, @EndDate, @ReasonID, @ShipperID, @CarrierID, @ProductGroupID, @TruckTypeID, @DriverGroupID, @DriverID, @OriginID, @StateID, @RegionID, @ProducerID, 0) R
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblTruckType TT ON TT.ID = R.TruckTypeID
	LEFT JOIN tblDriverGroup DG ON DG.ID = R.DriverGroupID
	LEFT JOIN viewDriverBase DR ON DR.ID = R.DriverGroupID
	LEFT JOIN viewOrigin D ON D.ID = R.OriginID
	LEFT JOIN tblState S ON S.ID = R.StateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblProducer PR ON PR.ID = R.ProducerID
	LEFT JOIN tblOriginWaitReason REA ON REA.ID = R.ReasonID
	ORDER BY EffectiveDate

GO

/***********************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: retrieve and return the Driver RateSheet info for the specified criteria
-- Changes:
/***********************************/
CREATE FUNCTION fnDriverRateSheet
(
  @StartDate date
, @EndDate date
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @TruckTypeID int
, @DriverGroupID int
, @DriverID int
, @OriginStateID int
, @DestStateID int
, @RegionID int
, @ProducerID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT R.ID
			, Ranking = dbo.fnRateRanking(@TruckTypeID, R.TruckTypeID, 512, 0)
				+ dbo.fnRateRanking(@ShipperID, R.ShipperID, 256, 0)
				+ dbo.fnRateRanking(@CarrierID, R.CarrierID, 128, 0)
				+ dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 64, 0)
				+ dbo.fnRateRanking(@DriverGroupID, R.DriverGroupID, 32, 1)
				+ dbo.fnRateRanking(@DriverID, R.DriverID, 16, 0)
				+ dbo.fnRateRanking(@OriginStateID, R.OriginStateID, 8, 0)
				+ dbo.fnRateRanking(@DestStateID, R.DestStateID, 4, 0)
				+ dbo.fnRateRanking(@RegionID, R.RegionID, 2, 0)
				+ dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM dbo.viewDriverRateSheet R
		WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@TruckTypeID, 0), R.TruckTypeID, 0) = coalesce(TruckTypeID, nullif(@TruckTypeID, 0), 0)
		  AND coalesce(nullif(@DriverGroupID, 0), R.DriverGroupID, 0) = coalesce(DriverGroupID, nullif(@DriverGroupID, 0), 0)
		  AND coalesce(nullif(@DriverID, 0), R.DriverID, 0) = coalesce(DriverID, nullif(@DriverID, 0), 0)
		  AND coalesce(nullif(@OriginStateID, 0), R.OriginStateID, 0) = coalesce(OriginStateID, nullif(@OriginStateID, 0), 0)
		  AND coalesce(nullif(@DestStateID, 0), R.DestStateID, 0) = coalesce(DestStateID, nullif(@DestStateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(ProducerID, nullif(@ProducerID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	)
	
	SELECT R.ID, ShipperID, CarrierID, ProductGroupID, TruckTypeID, DriverGroupID, DriverID, OriginStateID, DestStateID, RegionID, ProducerID, RateTypeID, UomID, EffectiveDate, EndDate
	  , MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate
	  , BestMatch, Ranking
	  , Locked, R.CreateDateUTC, R.CreatedByUser, R.LastChangeDateUTC, R.LastChangedByUser 
	FROM dbo.viewDriverRateSheet R
	JOIN (
		SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 10  -- ensure some type of match occurred on all criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.ID = R.ID 
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)

GO

/***********************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: retrieve and return the Driver RateSheetRangeRate info for the specified criteria
-- Changes:
/***********************************/
CREATE FUNCTION [fnDriverRateSheetDisplay]
(
  @StartDate date
, @EndDate date
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @TruckTypeID int
, @DriverGroupID int
, @DriverID int
, @OriginStateID int
, @DestStateID int
, @RegionID int
, @ProducerID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.TruckTypeID, R.DriverGroupID, R.DriverID, R.OriginStateID, R.DestStateID, R.RegionID, R.ProducerID, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Shipper = SH.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, TruckType = TT.Name
		, DriverGroup = DG.Name
		, Driver = DR.FullName
		, OriginState = OS.FullName
		, OriginStateAbbrev = OS.Abbreviation
		, DestState = OS.FullName
		, DestStateAbbrev = OS.Abbreviation
		, Region = REG.Name
		, Producer = P.Name
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
		, Ranking
	FROM dbo.fnDriverRateSheet(@StartDate, @EndDate, @ShipperID, @CarrierID, @ProductGroupID, @TruckTypeID, @DriverGroupID, @DriverID, @OriginStateID, @DestStateID, @RegionID, @ProducerID, 0) R
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblTruckType TT ON TT.ID = R.TruckTypeID
	LEFT JOIN tblDriverGroup DG ON DG.ID = R.DriverGroupID
	LEFT JOIN viewDriverBase DR ON DR.ID = R.DriverGroupID
	LEFT JOIN tblState OS ON OS.ID = R.OriginStateID
	LEFT JOIN tblState DS ON DS.ID = R.DestStateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblProducer P ON P.ID = R.ProducerID
	ORDER BY EffectiveDate

GO

/***********************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: retrieve and return the Driver RateSheetRangeRate info for the specified criteria
-- Changes:
/***********************************/
CREATE FUNCTION fnDriverRateSheetRangeRate
(
  @StartDate date
, @EndDate date
, @RouteMiles int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @TruckTypeID int
, @DriverGroupID int
, @DriverID int
, @OriginStateID int
, @DestStateID int
, @RegionID int
, @ProducerID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT RateSheetID = R.ID, RangeRateID = RR.ID
			-- the manually added .01 is normally added via the fnRateRanking routine
			, Ranking = CASE WHEN @RouteMiles BETWEEN RR.MinRange AND RR.MaxRange THEN 1024.01 ELSE 0 END
				+ dbo.fnRateRanking(@TruckTypeID, R.TruckTypeID, 512, 0)
				+ dbo.fnRateRanking(@ShipperID, R.ShipperID, 256, 0)
				+ dbo.fnRateRanking(@CarrierID, R.CarrierID, 128, 0)
				+ dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 64, 0)
				+ dbo.fnRateRanking(@DriverGroupID, R.DriverGroupID, 32, 1)
				+ dbo.fnRateRanking(@DriverID, R.DriverID, 16, 0)
				+ dbo.fnRateRanking(@OriginStateID, R.OriginStateID, 8, 0)
				+ dbo.fnRateRanking(@DestStateID, R.DestStateID, 4, 0)
				+ dbo.fnRateRanking(@RegionID, R.RegionID, 2, 0)
				+ dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM dbo.viewDriverRateSheet R
		JOIN dbo.tblDriverRangeRate RR ON RR.RateSheetID = R.ID 
		  AND isnull(@RouteMiles, RR.MinRange) BETWEEN RR.MinRange AND RR.MaxRange
		WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@TruckTypeID, 0), R.TruckTypeID, 0) = coalesce(TruckTypeID, nullif(@TruckTypeID, 0), 0)
		  AND coalesce(nullif(@DriverGroupID, 0), R.DriverGroupID, 0) = coalesce(DriverGroupID, nullif(@DriverGroupID, 0), 0)
		  AND coalesce(nullif(@DriverID, 0), R.DriverID, 0) = coalesce(DriverID, nullif(@DriverID, 0), 0)
		  AND coalesce(nullif(@OriginStateID, 0), R.OriginStateID, 0) = coalesce(OriginStateID, nullif(@OriginStateID, 0), 0)
		  AND coalesce(nullif(@DestStateID, 0), R.DestStateID, 0) = coalesce(DestStateID, nullif(@DestStateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(ProducerID, nullif(@ProducerID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	)
	
	SELECT RR.ID, RateSheetID = R.ID, ShipperID, CarrierID, ProductGroupID, TruckTypeID, DriverGroupID, DriverID, OriginStateID, DestStateID, RegionID, ProducerID
	  , Rate, MinRange, MaxRange, RateTypeID, UomID, EffectiveDate, EndDate
	  , MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate
	  , BestMatch, Ranking
	  , Locked, R.CreateDateUTC, R.CreatedByUser, R.LastChangeDateUTC, R.LastChangedByUser 
	FROM dbo.viewDriverRateSheet R
	JOIN dbo.tblDriverRangeRate RR ON RR.RateSheetID = R.ID
	JOIN (
		SELECT RateSheetID, RangeRateID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 11  -- ensure some type of match occurred on all criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.RateSheetID = R.ID AND X.RangeRateID = RR.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)

GO

/***********************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: retrieve and return the Driver RateSheetRangeRate info for the specified criteria
-- Changes:
/***********************************/
CREATE FUNCTION [fnDriverRateSheetRangeRatesDisplay]
(
  @StartDate date
, @EndDate date
, @RouteMiles int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @TruckTypeID int
, @DriverGroupID int
, @DriverID int
, @OriginStateID int
, @DestStateID int
, @RegionID int
, @ProducerID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.RateSheetID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.TruckTypeID, R.DriverGroupID, R.DriverID, R.OriginStateID, R.DestStateID, R.RegionID, R.ProducerID, R.MinRange, R.MaxRange, R.Rate, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Shipper = SH.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, TruckType = TT.Name
		, DriverGroup = DG.Name
		, Driver = DR.FullName
		, OriginState = OS.FullName
		, OriginStateAbbrev = OS.Abbreviation
		, DestState = OS.FullName
		, DestStateAbbrev = OS.Abbreviation
		, Region = REG.Name
		, Producer = P.Name
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
		, Ranking
	FROM dbo.fnDriverRateSheetRangeRate(@StartDate, @EndDate, @RouteMiles, @ShipperID, @CarrierID, @ProductGroupID, @TruckTypeID, @DriverGroupID, @DriverID, @OriginStateID, @DestStateID, @RegionID, @ProducerID, 0) R
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblTruckType TT ON TT.ID = R.TruckTypeID
	LEFT JOIN tblDriverGroup DG ON DG.ID = R.DriverGroupID
	LEFT JOIN viewDriverBase DR ON DR.ID = R.DriverID
	LEFT JOIN tblState OS ON OS.ID = R.OriginStateID
	LEFT JOIN tblState DS ON DS.ID = R.DestStateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblProducer P ON P.ID = R.ProducerID
	ORDER BY EffectiveDate

GO

/****************************************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: retrieve and return the Driver RouteRate info for the specified criteria
-- Changes:
/*****************************************************/
CREATE FUNCTION [fnDriverRouteRate]
(
  @StartDate date
, @EndDate date
, @OriginID int
, @DestinationID int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @TruckTypeID int
, @DriverGroupID int
, @DriverID int
, @BestMatchOnly bit = 0
) RETURNS @ret TABLE 
(
  ID int
, RouteID int
, OriginID int
, DestinationID int
, ShipperID int
, CarrierID int
, ProductGroupID int
, TruckTypeID int
, DriverGroupID int
, DriverID int
, Rate decimal(18, 10)
, RateTypeID int
, UomID int
, EffectiveDate date
, EndDate date
, MaxEffectiveDate date
, MinEndDate date
, NextEffectiveDate date
, PriorEndDate date
, BestMatch bit
, Ranking decimal(10, 2)
, Locked bit
, CreateDateUTC datetime
, CreatedByUser varchar(100)
, LastChangeDateUTC datetime
, LastChangedByUser varchar(100)
) AS BEGIN

	DECLARE @data TABLE (ID int, RouteID int, Ranking decimal(10, 2))
	INSERT INTO @data
		SELECT R.ID
			, R.RouteID
			, Ranking =	dbo.fnRateRanking(@TruckTypeID, R.TruckTypeID, 32, 0)
				  + dbo.fnRateRanking(@ShipperID, R.ShipperID, 16, 0)
				  + dbo.fnRateRanking(@CarrierID, R.CarrierID, 8, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 4, 0)
				  + dbo.fnRateRanking(@DriverGroupID, R.DriverGroupID, 2, 1)
				  + dbo.fnRateRanking(@DriverID, R.DriverID, 1, 1)
		FROM dbo.viewDriverRouteRate R
		JOIN tblRoute RO ON RO.ID = R.RouteID
		WHERE (nullif(@OriginID, 0) IS NULL OR @OriginID = RO.OriginID)
		  AND (nullif(@DestinationID, 0) IS NULL OR @DestinationID = RO.DestinationID)
		  AND coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@TruckTypeID, 0), R.TruckTypeID, 0) = coalesce(TruckTypeID, nullif(@TruckTypeID, 0), 0)
		  AND coalesce(nullif(@DriverGroupID, 0), R.DriverGroupID, 0) = coalesce(DriverGroupID, nullif(@DriverGroupID, 0), 0)
		  AND coalesce(nullif(@DriverID, 0), R.DriverID, 0) = coalesce(DriverID, nullif(@DriverID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)

	INSERT INTO @ret
	SELECT R.ID, RouteID, OriginID, DestinationID, ShipperID, CarrierID, ProductGroupID, TruckTypeID, DriverGroupID, DriverID, Rate, RateTypeID, UomID, EffectiveDate, EndDate
		, MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate
		, BestMatch, Ranking
		, Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewDriverRouteRate R
	JOIN (
		SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM @data S
		LEFT JOIN (
			SELECT RouteID, Ranking = MAX(Ranking)
			FROM @data
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 6  -- ensure some type of match occurred on all criteria choices
			GROUP BY RouteID			  
		) X ON X.RouteID = S.RouteID AND X.Ranking = S.Ranking
	) X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
	
	RETURN
END

GO

/****************************************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: retrieve and return the Driver RouteRate rows for the specified criteria
-- Changes:
/*****************************************************/
CREATE FUNCTION [fnDriverRouteRatesDisplay]
(
  @StartDate date
, @EndDate date
, @OriginID int
, @DestinationID int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @TruckTypeID int
, @DriverGroupID int
, @DriverID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.RouteID, RO.ActualMiles, R.OriginID, R.DestinationID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.TruckTypeID, R.DriverGroupID, R.DriverID, R.Rate, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Origin = O.Name
		, OriginFull = O.FullName
		, Destination = D.Name
		, DestinationFull = D.FullName
		, Shipper = SH.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, TruckType = TT.Name
		, DriverGroup = DG.Name
		, Driver = DR.FullName
		, RateType = RT.Name
		, Uom = U.Name
		, UomShort = U.Abbrev
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch, Ranking
	FROM dbo.fnDriverRouteRate(@StartDate, @EndDate, @OriginID, @DestinationID, @ShipperID, @CarrierID, @ProductGroupID, @TruckTypeID, @DriverGroupID, @DriverID, 0) R
	JOIN tblRoute RO ON RO.ID = R.RouteID
	JOIN viewOrigin O ON O.ID = R.OriginID
	JOIN viewDestination D ON D.ID = R.DestinationID
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblTruckType TT ON TT.ID = R.TruckTypeID
	LEFT JOIN tblDriverGroup DG ON DG.ID = R.DriverGroupID
	LEFT JOIN viewDriverBase DR ON DR.ID = R.DriverID
	LEFT JOIN tblRateType RT ON RT.ID = R.RateTypeID
	LEFT JOIN tblUom U ON U.ID = R.UomID
	ORDER BY EffectiveDate

GO

/***********************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: retrieve and return the Driver AssessorialRate info for the specified order
-- Changes:
/***********************************/
CREATE FUNCTION [fnOrderDriverAssessorialRates](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, TypeID, Rate, RateTypeID, UomID
	FROM dbo.viewOrder O
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
	CROSS APPLY dbo.fnDriverAssessorialRates(O.OrderDate, null, null, O.CustomerID, O.CarrierID, O.ProductGroupID, O.TruckTypeID, O.OriginDriverGroupID, O.DriverID, O.OriginID, O.DestinationID, O.OriginStateID, O.DestStateID, O.OriginRegionID, O.ProducerID, 1) R
	WHERE O.ID = @ID
	  AND ((R.TypeID = 1 AND CASE WHEN isnull(OA.OverrideChainup, 0) = 1 THEN 0 ELSE O.ChainUp END = 1)
		OR (R.TypeID = 2 AND CASE WHEN isnull(OA.OverrideReroute, 0) = 1 THEN 0 ELSE O.RerouteCount END > 0)
		OR (R.TypeID = 3 AND O.TicketCount > 1)
		OR (R.TypeID = 4 AND CASE WHEN isnull(OA.OverrideH2S, 0) = 1 THEN 0 ELSE O.H2S END = 1)
		OR R.TypeID > 4)

GO

/***********************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: retrieve and return the Driver AssessorialRate Amounts info for the specified order
-- Changes:
/***********************************/
CREATE FUNCTION [fnOrderDriverAssessorialAmounts](@ID int, @Units decimal(18, 10), @UomID int) RETURNS TABLE AS RETURN
	SELECT AssessorialRateTypeID = TypeID, RateID, RateTypeID, Amount
	FROM (
		SELECT TypeID, RateID = ID, RateTypeID
			, Amount = dbo.fnRateToAmount(RateTypeID, @Units, @UomID, Rate, UomID, OSC.LoadAmount
			, CASE WHEN RateTypeID = 6 THEN (SELECT TOP 1 Amount FROM tblOrderSettlementCarrierAssessorialCharge SCAC WHERE OrderID = @ID AND SCAC.AssessorialRateTypeID = CAR.TypeID) ELSE NULL END
			, NULL)
		FROM dbo.fnOrderDriverAssessorialRates(@ID) CAR
		JOIN tblOrderSettlementDriver OSC ON OSC.OrderID = @ID
	) X
	WHERE Amount IS NOT NULL

GO

/***********************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: retrieve and return the Driver DestinationWaitRate info for the specified order
-- Changes
/***********************************/
CREATE FUNCTION [fnOrderDriverDestinationWaitRate](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID
		, Minutes = isnull(OA.OverrideDestMinutes, O.DestMinutes)
		, R.Rate
	FROM viewOrder O
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = @ID
	OUTER APPLY dbo.fnDriverDestinationWaitRate(O.OrderDate, null, O.DestWaitReasonID, O.CustomerID, O.CarrierID, O.ProductGroupID, O.TruckTypeID, O.OriginDriverGroupID, O.DriverID, O.DestinationID, O.DestStateID, O.DestRegionID, O.ProducerID, 1) R
	WHERE O.ID = @ID

GO

/***********************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: retrieve and return the Driver DestinationWait data info for the specified order
-- Changes:
/***********************************/
CREATE FUNCTION [fnOrderDriverDestinationWaitData](@ID int) RETURNS TABLE AS RETURN
	SELECT RateID, BillableMinutes, BillableHours, Amount = BillableHours * Rate
	FROM (
		SELECT RateID, BillableMinutes
			, BillableHours = dbo.fnComputeBillableWaitHours(BillableMinutes, SubUnitID, RoundingTypeID)
			, Rate
		FROM (
			SELECT RateID = WR.ID
				, Rate
				, BillableMinutes = dbo.fnMaxInt(
					-- Total Origin Minutes between range of DestMinBillableMinutes & DestMaxBillableMinutes (will be ignored if either Min|Max value is NULL)
					CASE WHEN WR.Minutes < WFP.DestMinBillableMinutes THEN 0
						 WHEN WR.Minutes - WFP.DestThresholdMinutes > WFP.DestMaxBillableMinutes THEN WFP.DestMaxBillableMinutes
						 ELSE WR.Minutes - WFP.DestThresholdMinutes END, 0)
				, WFP.SubUnitID, WFP.RoundingTypeID
			FROM dbo.fnOrderDriverDestinationWaitRate(@ID) WR 
			CROSS JOIN dbo.fnOrderCarrierWaitFeeParameter(@ID) WFP 
		) X
	) X2
	WHERE BillableHours > 0.0

GO

/***********************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: retrieve and return the Driver FuelSurcharge info for the specified order
-- Changes:
/***********************************/
CREATE FUNCTION [fnOrderDriverFuelSurchargeRate](@ID int) RETURNS TABLE AS RETURN
	SELECT RateID
		, Rate = IncrementAmount * round(Pricediff / nullif(IntervalAmount, 0.0) + .49, 0)
		, RouteMiles
	FROM (
		SELECT TOP 1 RateID = ID, FuelPriceFloor, IntervalAmount, IncrementAmount
			, PriceDiff = dbo.fnMaxDecimal(0, dbo.fnFuelPriceForOrigin(OriginID, OrderDate) - FuelPriceFloor)
			, RouteMiles
		FROM (
			SELECT R.ID, R.FuelPriceFloor, R.IntervalAmount, R.IncrementAmount, O.OrderDate, O.OriginID, RouteMiles = CASE WHEN O.Rejected = 1 THEN 0 ELSE O.ActualMiles END
			FROM dbo.viewOrder O
			CROSS APPLY dbo.fnDriverFuelSurchargeRate(O.OrderDate, null, O.CustomerID, O.CarrierID, O.ProductGroupID, O.TruckTypeID, O.OriginDriverGroupID, O.DriverID, 1) R
			WHERE O.ID = @ID 
		) X
	) X2

GO

/***********************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: retrieve and return the Driver FuelSurcharge data info for the specified order
-- Changes:
/***********************************/
CREATE FUNCTION [fnOrderDriverFuelSurchargeData](@ID int) RETURNS TABLE AS RETURN
	SELECT RateID, Rate, Amount = Rate * R.RouteMiles
	FROM dbo.fnOrderDriverFuelSurchargeRate(@ID) R
	WHERE Rate > 0

GO

/***********************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: retrieve and return the Driver OrderRejectRate info for the specified order
-- Changes:
/***********************************/
CREATE FUNCTION [fnOrderDriverOrderRejectRate](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, R.Rate, R.RateTypeID, R.UomID
	FROM viewOrder O
	CROSS APPLY dbo.fnDriverOrderRejectRate(O.OrderDate, null, O.RejectReasonID, O.CustomerID, O.CarrierID, O.ProductGroupID, O.TruckTypeID, O.OriginDriverGroupID, O.DriverID, O.OriginID, O.OriginStateID, O.OriginRegionID, O.ProducerID, 1) R
	WHERE O.ID = @ID

GO

/***********************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: retrieve and return the Driver OrderReject data info for the specified order
-- Changes:
/***********************************/
CREATE FUNCTION [fnOrderDriverOrderRejectData](@ID int, @Units decimal(18, 10), @UomID int) RETURNS TABLE AS RETURN
	SELECT RateID = RR.ID
		, Amount = dbo.fnRateToAmount(RateTypeID, @Units, @UomID, Rate, UomID, NULL
		, CASE WHEN RateTypeID = 6 THEN (SELECT TOP 1 OrderRejectAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID) ELSE NULL END
		, NULL)
	FROM dbo.fnOrderDriverOrderRejectRate(@ID) RR 
	JOIN tblOrder O ON O.ID = @ID
	WHERE O.Rejected = 1

GO

/***********************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: retrieve and return the Driver OriginWaitRate info for the specified order
-- Changes
/***********************************/
CREATE FUNCTION [fnOrderDriverOriginWaitRate](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, Minutes = isnull(OA.OverrideOriginMinutes, O.OriginMinutes), R.Rate
	FROM viewOrder O
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = @ID
	CROSS APPLY dbo.fnDriverOriginWaitRate(O.OrderDate, null, O.OriginWaitReasonID, O.CustomerID, O.CarrierID, O.ProductGroupID, O.TruckTypeID, O.OriginDriverGroupID, O.DriverID, O.OriginID, O.OriginStateID, O.OriginRegionID, O.ProducerID, 1) R
	WHERE O.ID = @ID

GO

/***********************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: retrieve and return the Driver OriginWait data info for the specified order
-- Changes:
/***********************************/
CREATE FUNCTION [fnOrderDriverOriginWaitData](@ID int) RETURNS TABLE AS RETURN
	SELECT RateID, BillableMinutes, BillableHours, Amount = BillableHours * Rate
	FROM (
		SELECT RateID, BillableMinutes
			, BillableHours = dbo.fnComputeBillableWaitHours(BillableMinutes, SubUnitID, RoundingTypeID)
			, Rate
		FROM (
			SELECT RateID = WR.ID
				, WR.Rate
				, BillableMinutes = dbo.fnMaxInt(
					-- Total Origin Minutes between range of DestMinBillableMinutes & DestMaxBillableMinutes (will be ignored if either Min|Max value is NULL)
					CASE WHEN WR.Minutes < WFP.OriginMinBillableMinutes THEN 0
						 WHEN WR.Minutes - WFP.OriginThresholdMinutes > WFP.OriginMaxBillableMinutes THEN WFP.OriginMaxBillableMinutes
						 ELSE WR.Minutes - WFP.OriginThresholdMinutes END, 0)
				, WFP.SubUnitID, WFP.RoundingTypeID
			FROM dbo.fnOrderDriverOriginWaitRate(@ID) WR 
			CROSS JOIN dbo.fnOrderCarrierWaitFeeParameter(@ID) WFP 
		) X
	) X2
	WHERE BillableHours > 0.0

GO

/***********************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: retrieve and return the Driver RateSheetRangeRate info for the specified order
-- Changes:
/***********************************/
CREATE FUNCTION [fnOrderDriverRateSheetRangeRate](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, RateTypeID, Rate, UomID
	FROM dbo.viewOrder O
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
	CROSS APPLY dbo.fnDriverRateSheetRangeRate(O.OrderDate, NULL, isnull(OA.OverrideActualMiles, O.ActualMiles), O.CustomerID, O.CarrierID, O.ProductGroupID, O.TruckTypeID, O.OriginDriverGroupID, O.DriverID, O.OriginStateID, O.DestStateID, O.OriginRegionID, O.ProducerID, 1) R
	WHERE O.ID = @ID

GO

/****************************************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: retrieve and return the Driver RouteRate info for the specified order
-- Changes:
/*****************************************************/
CREATE FUNCTION [fnOrderDriverRouteRate](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, RateTypeID, Rate, R.UomID
	FROM dbo.viewOrder O
	CROSS APPLY dbo.fnDriverRouteRate(O.OrderDate, null, O.OriginID, O.DestinationID, O.CustomerID, O.CarrierID, O.ProductGroupID, O.TruckTypeID, O.OriginDriverGroupID, O.DriverID, 1) R
	WHERE O.ID = @ID 

GO

/***********************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: retrieve and return the Driver RateSheetRangeRate info for the specified order
-- Changes:
/***********************************/
CREATE FUNCTION [fnOrderDriverLoadAmount](@ID int, @Units decimal(18, 10), @UomID int) RETURNS TABLE AS RETURN
	SELECT RouteRateID, RangeRateID
		, Amount = dbo.fnRateToAmount(RateTypeID, @Units, @UomID, Rate, UomID, NULL
			, CASE WHEN RateTypeID = 6 THEN (SELECT TOP 1 LoadAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID) ELSE NULL END
			, CASE WHEN RateTypeID = 5 THEN (
					SELECT TOP 1 isnull(OA.OverrideActualMiles, O.ActualMiles) 
					FROM tblOrder O 
					LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID 
					WHERE O.ID = @ID
				) ELSE NULL END
			)
	FROM (
		SELECT TOP 1 * 
		FROM (
			SELECT SortID = 1, RouteRateID = ID, RangeRateID = NULL, RateTypeID, UomID, Rate FROM dbo.fnOrderDriverRouteRate(@ID)
			UNION SELECT 2, NULL, ID, RateTypeID, UomID, Rate FROM dbo.fnOrderDriverRateSheetRangeRate(@ID)
		) X
		ORDER BY SortID
	) X

GO

/*********************************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: Concatenate the Assessorial Descriptions for a single order into a tab-separated string
-- Changes:
/*********************************************/
CREATE FUNCTION [fnOrderDriverAssessorialAmountsTSV](@OrderID int, @IncludeSystemRates bit = 0) RETURNS varchar(max) AS
BEGIN
	DECLARE @ret VARCHAR(max) 

	SELECT @ret = isnull(@ret + CHAR(13), '') + ltrim(AC.Amount)
	FROM tblOrderSettlementDriverAssessorialCharge AC
	WHERE AC.OrderID = @OrderID
		  AND (@IncludeSystemRates = 1 OR AC.AssessorialRateTypeID > 4)
	
	RETURN (@ret)
END

GO

/*********************************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: Concatenate the Assessorial Descriptions for a single order into a tab-separated string
-- Changes:
/*********************************************/
CREATE FUNCTION [fnOrderDriverAssessorialDetailsTSV](@OrderID int, @IncludeSystemRates bit = 0) RETURNS varchar(max) AS
BEGIN
	DECLARE @ret VARCHAR(max) 

	SELECT @ret = isnull(@ret + CHAR(13), '') 
		+ Name + isnull(CHAR(9) + RatePrefix + ltrim(Rate) + RateSuffix + CHAR(9) + ltrim(TypeID), '')
	FROM (
		SELECT ART.Name 
			, TypeID = AC.AssessorialRateTypeID
			, Rate = cast(AR.Rate as decimal(18, 4))
			, RatePrefix = CASE RT.ID WHEN 1 THEN '$' WHEN 2 THEN '$' WHEN 3 THEN '' WHEN 4 THEN '' END
			, RateSuffix = CASE RT.ID WHEN 1 THEN '/' + U.Abbrev WHEN 2 THEN '$' ELSE RT.Name END
			, AC.Amount
		FROM tblOrderSettlementDriverAssessorialCharge AC
		JOIN tblAssessorialRateType ART ON ART.ID = AC.AssessorialRateTypeID
		LEFT JOIN tblDriverAssessorialRate AR ON AR.ID = AC.AssessorialRateID
		LEFT JOIN tblRateType RT ON RT.ID = AR.RateTypeID 
		LEFT JOIN tblUom U ON U.ID = AR.UomID
		WHERE AC.OrderID = @OrderID
		  AND (@IncludeSystemRates = 1 OR AC.AssessorialRateTypeID > 4)
	) X
		
	RETURN (@ret)
END

GO

exec _spDropView 'viewOrder_Financial_Base_Driver'
go
/***********************************/
-- Created: 4.1.0 - 2016/08/15 - Kevin Alons
-- Purpose: return BASE Order details + Driver FINANCIAL INFO into a single view
-- Changes:
/***********************************/
CREATE VIEW viewOrder_Financial_Base_Driver AS 
	SELECT *
		, HasError = cast(CASE WHEN ErrorFieldCSV IS NOT NULL THEN 1 ELSE 0 END as bit)
	FROM (
		SELECT *
			, OrderID = ID
			, ErrorFieldCSV = nullif(substring(CASE WHEN Approved = 0 THEN ',Approved' ELSE '' END
				+ CASE WHEN InvoiceLoadAmount IS NULL THEN ',InvoiceLoadAmount,InvoiceRouteRate,InvoiceRateSheet' ELSE '' END
				+ CASE WHEN FinalChainup = 1 AND InvoiceChainupAmount IS NULL THEN ',InvoiceChainupAmount' ELSE '' END
				+ CASE WHEN FinalRerouteCount > 0 AND InvoiceRerouteAmount IS NULL THEN ',InvoiceRerouteAmount' ELSE '' END
				+ CASE WHEN FinalH2S = 1 AND InvoiceH2SAmount IS NULL THEN ',InvoiceH2SAmount' ELSE '' END
				+ CASE WHEN TicketCount > 1 AND InvoiceSplitLoadAmount IS NULL THEN ',InvoiceSplitLoadAmount' ELSE '' END
				+ CASE WHEN InvoiceOriginWaitBillableMinutes > 0 AND InvoiceOriginWaitAmount IS NULL THEN ',InvoiceOriginWaitAmount' ELSE '' END
				+ CASE WHEN InvoiceDestinationWaitBillableMinutes > 0 AND InvoiceDestinationWaitAmount IS NULL THEN ',InvoiceDestinationWaitAmount' ELSE '' END
				+ CASE WHEN Rejected = 1 AND InvoiceOrderRejectAmount IS NULL THEN ',InvoiceOrderRejectAmount' ELSE '' END
				+ CASE WHEN InvoiceMinSettlementUnitsID IS NULL THEN ',InvoiceMinSettlementUnits' ELSE '' END
				+ CASE WHEN InvoiceSettlementFactorID IS NULL THEN ',InvoiceSettlementFactor' ELSE '' END
				+ CASE WHEN Rejected = 0 AND InvoiceSettlementFactorID IS NULL THEN ',InvoiceSettlementFactor' ELSE '' END
				+ CASE WHEN Rejected = 0 AND InvoiceSettlementFactorID IS NOT NULL AND dbo.fnOrderSettlementUnits(InvoiceSettlementFactorID, OriginGrossUnits, OriginNetUnits, OriginGrossStdUnits, DestGrossUnits, DestNetUnits) IS NULL THEN ','+OrderUnitsField ELSE '' END
				+ CASE WHEN InvoiceWaitFeeParameterID IS NULL THEN ',InvoiceOriginWaitBillableMinutes,InvoiceDestinationWaitBillableMinutes' ELSE '' END
				+ CASE WHEN isnull(ActualMiles, 0) = 0 THEN ',ActualMiles' ELSE '' END, 2, 100000), '')
		FROM (
			SELECT O.* 
				, DriverGroup = DG.Name
				, InvoiceRatesAppliedDateUTC = OS.CreateDateUTC
				, OS.BatchID
				, Approved = cast(ISNULL(OA.Approved, 0) as bit)
				, FinalActualMiles = ISNULL(OA.OverrideActualMiles, O.ActualMiles)
				, FinalOriginMinutes = ISNULL(OA.OverrideOriginMinutes, O.OriginMinutes)
				, FinalDestMinutes = ISNULL(OA.OverrideDestMinutes, O.DestMinutes)
				, FinalH2S = cast(CASE WHEN isnull(OA.OverrideH2S, 0) = 1 THEN 0 ELSE OO.H2S END as bit)
				, FinalChainup = cast(CASE WHEN isnull(OA.OverrideChainup, 0) = 1 THEN 0 ELSE O.Chainup END as bit)
				, FinalRerouteCount = CASE WHEN isnull(OA.OverrideReroute, 0) = 1 THEN 0 ELSE isnull(ORR.RerouteCount, 0) END
				, FC.OrderUnitsField
				, InvoiceBatchNum = OS.BatchNum 
				, InvoiceOriginWaitBillableMinutes = OS.OriginWaitBillableMinutes
				, InvoiceDestinationWaitBillableMinutes = OS.DestinationWaitBillableMinutes
				, InvoiceTotalWaitBillableMinutes = isnull(OS.OriginWaitBillableMinutes, 0) + ISNULL(OS.DestinationWaitBillableMinutes, 0) 
				, InvoiceWaitFeeParameterID = WaitFeeParameterID
				, InvoiceWaitFeeSubUnit = WaitFeeSubUnit
				, InvoiceWaitFeeRoundingType = WaitFeeRoundingType
				, InvoiceOriginWaitRate = OS.OriginWaitRate 
				, InvoiceOriginWaitAmount = OS.OriginWaitAmount 		
				, InvoiceDestinationWaitRate = OS.DestinationWaitRate 
				, InvoiceDestinationWaitAmount = OS.DestinationWaitAmount 
				, InvoiceTotalWaitAmount = OS.TotalWaitAmount
				, InvoiceOrderRejectRate = OS.OrderRejectRate	
				, InvoiceOrderRejectRateType = OS.OrderRejectRateType  
				, InvoiceOrderRejectAmount = OS.OrderRejectAmount  
				, InvoiceChainupRate = OS.ChainupRate
				, InvoiceChainupRateType = OS.ChainupRateType
				, InvoiceChainupAmount = OS.ChainupAmount 
				, InvoiceRerouteRate = OS.RerouteRate
				, InvoiceRerouteRateType = OS.RerouteRateType
				, InvoiceRerouteAmount = OS.RerouteAmount 
				, InvoiceH2SRate = OS.H2SRate
				, InvoiceH2SRateType = OS.H2SRateType
				, InvoiceH2SAmount = OS.H2SAmount
				, InvoiceSplitLoadRate = OS.SplitLoadRate
				, InvoiceSplitLoadRateType = OS.SplitLoadRateType
				, InvoiceSplitLoadAmount = OS.SplitLoadAmount
				, InvoiceOtherAmount = OS.OtherAmount
				, InvoiceTaxRate = OS.OriginTaxRate
				, InvoiceSettlementUom = OS.SettlementUom 
				, InvoiceSettlementUomShort = OS.SettlementUomShort 
				, InvoiceCarrierSettlementFactorID = OS.CarrierSettlementFactorID 
				, InvoiceSettlementFactorID = OS.SettlementFactorID 
				, InvoiceSettlementFactor = OS.SettlementFactor 
				, InvoiceMinSettlementUnitsID = OS.MinSettlementUnitsID 
				, InvoiceMinSettlementUnits = OS.MinSettlementUnits
				, InvoiceUnits = OS.SettlementUnits
				, InvoiceRouteRate = OS.RouteRate
				, InvoiceRouteRateType = OS.RouteRateType
				, InvoiceRateSheetRate = OS.RateSheetRate
				, InvoiceRateSheetRateType = OS.RateSheetRateType
				, InvoiceFuelSurchargeRate = OS.FuelSurchargeRate
				, InvoiceFuelSurchargeAmount = OS.FuelSurchargeAmount
				, InvoiceLoadAmount = OS.LoadAmount
				, InvoiceTotalAmount = OS.TotalAmount
			FROM dbo.viewOrder O
			LEFT JOIN tblDriverGroup DG ON DG.ID = O.DriverGroupID
			LEFT JOIN tblOrigin OO ON OO.ID = O.OriginID
			LEFT JOIN viewOrderSettlementDriver OS ON OS.OrderID = O.ID
			LEFT JOIN (SELECT OrderID, RerouteCount = count(1) FROM tblOrderReroute GROUP BY OrderID) ORR ON ORR.OrderID = O.ID
			LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
			LEFT JOIN tblSettlementFactor FC ON FC.ID = OS.SettlementFactorID
			WHERE O.StatusID NOT IN (-10) -- don't include "Generated" orders (only Assigned+)
		) X
	) X1

GO

exec _spDropView 'viewOrder_Financial_Driver'
go
/***********************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details + FINANCIAL INFO into a single view
-- Changes:
/***********************************/
CREATE VIEW viewOrder_Financial_Driver AS 
	SELECT O.* 
		, Shipper = isnull(C.Name, 'N/A')
		, CarrierSettled = cast(CASE WHEN OS.BatchID IS NOT NULL THEN 1 ELSE 0 END as bit)
		, TicketNums = dbo.fnOrderTicketDetails(O.ID, 'TicketNums', '<br/>') 
		, TankNums = dbo.fnOrderTicketDetails(O.ID, 'TankNums', '<br/>') 
		, PreviousDestinations = dbo.fnRerouteDetails(O.ID, 'PreviousDestinations', '<br/>') 
		, RerouteUsers = dbo.fnRerouteDetails(O.ID, 'RerouteUsers', '<br/>') 
		, RerouteDates = dbo.fnRerouteDetails(O.ID, 'RerouteDates', '<br/>') 
		, RerouteNotes = dbo.fnRerouteDetails(O.ID, 'RerouteNotes', '<br/>')
		, InvoiceOtherDetailsTSV = dbo.fnOrderDriverAssessorialDetailsTSV(O.ID, 0)
		, InvoiceOtherAmountsTSV = dbo.fnOrderDriverAssessorialAmountsTSV(O.ID, 0)
	FROM dbo.viewOrder_Financial_Base_Driver O
	LEFT JOIN tblOrderSettlementCarrier OS ON OS.OrderID = O.ID
	LEFT JOIN tblCustomer C ON C.ID = O.CustomerID

GO

/*************************************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: compute and add the various Driver "Settlement" $$ values to an Delivered/Audited order
-- Changes:
/*************************************************/
CREATE PROCEDURE spApplyRatesDriver
(
  @ID int
, @UserName varchar(100)
, @OriginWaitAmount money = NULL
, @DestWaitAmount money = NULL
, @RejectionAmount money = NULL
, @FuelSurchargeAmount money = NULL
, @LoadAmount money = NULL
, @AssessorialRateTypeID_CSV varchar(max) = NULL
, @AssessorialAmount_CSV varchar(max) = null
, @ResetOverrides bit = 0
, @AllowReApplyPostBatch bit = 0
) AS BEGIN

	SET NOCOUNT ON

	PRINT 'spApplyRatesDriver (' + ltrim(@ID) + ') START ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	
	IF (@AllowReApplyPostBatch = 0) AND EXISTS (SELECT * FROM tblOrderSettlementDriver WHERE OrderID = @ID AND BatchID IS NOT NULL)
	BEGIN
		RAISERROR('Invoice has already been Driver Settled', 16, 1)
		RETURN
	END
	
	/* ensure that Carrier Rates have been applied prior to applying Driver rates (since they could be dependent on Carrier rates) */
	IF NOT EXISTS (SELECT * FROM tblOrderSettlementCarrier WHERE OrderID = @ID)
		EXEC spApplyRatesCarrier @ID, @UserName

	/* transform the provided manual Assessorial Rates (if any) into a queryable table variable */
	DECLARE @ManualAssessorialRates TABLE (ID int primary key, Amount money)
	INSERT INTO @ManualAssessorialRates (ID, Amount) 
		SELECT ID.ID, R.Amount
		FROM (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), ID FROM dbo.fnSplitCSVIDs(@AssessorialRateTypeID_CSV)) ID
		JOIN (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), amount = CAST(value as money) FROM dbo.fnSplitCSV(@AssessorialAmount_CSV)) R ON R.sortid = ID.sortid

	/* persist all existing MANUAL Assessorial Rates from the existing Order Settlement (if any present and not resetting them) */
	INSERT INTO @ManualAssessorialRates (ID, Amount)
		SELECT AssessorialRateTypeID, Amount 
		FROM dbo.tblOrderSettlementDriverAssessorialCharge 
		WHERE OrderID = @ID AND AssessorialRateID IS NULL 
			AND ID NOT IN (SELECT ID FROM @ManualAssessorialRates)
			AND @ResetOverrides = 0

	/* persist all existing settlement amount overrides from the existing Order Settlement - if not resetting them */
	IF (@ResetOverrides = 0)
	BEGIN 
		SELECT @OriginWaitAmount = OriginWaitAmount FROM tblOrderSettlementDriver WHERE OrderID = @ID AND @OriginWaitAmount IS NULL AND OriginWaitRateID IS NULL
		SELECT @DestWaitAmount = DestinationWaitAmount FROM tblOrderSettlementDriver WHERE OrderID = @ID AND @DestWaitAmount IS NULL AND DestinationWaitRateID IS NULL
		SELECT @RejectionAmount = OrderRejectAmount FROM tblOrderSettlementDriver WHERE OrderID = @ID AND @RejectionAmount IS NULL AND OrderRejectRateID IS NULL
		SELECT @FuelSurchargeAmount = FuelSurchargeAmount FROM tblOrderSettlementDriver WHERE OrderID = @ID AND @FuelSurchargeAmount IS NULL AND FuelSurchargeRateID IS NULL
		SELECT @LoadAmount = LoadAmount FROM tblOrderSettlementDriver WHERE OrderID = @ID AND @LoadAmount IS NULL AND RouteRateID IS NULL AND RangeRateID IS NULL
	END

	/* compute the basic settlement units from the Order being invoices */
	DECLARE @SettlementUnits TABLE
	(
		OrderID int
	  , SettlementUomID int
	  , SettlementFactorID int
	  , CarrierSettlementFactorID int
	  , MinSettlementUnits decimal(18, 10)
	  , MinSettlementUnitsID int
	  , ActualUnits decimal(18, 10)
	  , SettlementUnits decimal(18, 10) -- calculate value basically max(actual, minimum)
	)
	INSERT INTO @SettlementUnits (OrderID, SettlementUomID, SettlementFactorID, CarrierSettlementFactorID, MinSettlementUnits, MinSettlementUnitsID, ActualUnits, SettlementUnits)
		SELECT OSU.OrderID, OSU.SettlementUomID, OSU.SettlementFactorID, OSU.CarrierSettlementFactorID, ISNULL(OA.OverrideCarrierMinSettlementUnits, OSU.MinSettlementUnits), OSU.MinSettlementUnitsID, OSU.ActualUnits
			-- use passed settlement (already calculated) or pick the max between the actual and overrride min - JAE 1/15/16 - 3.9.39
			, CASE WHEN OA.OverrideCarrierMinSettlementUnits IS NULL THEN OSU.SettlementUnits
					ELSE dbo.fnMaxDecimal(OSU.ActualUnits, OA.OverrideCarrierMinSettlementUnits) END
		FROM dbo.viewOrderSettlementUnitsCarrier OSU
		LEFT JOIN tblOrderApproval OA ON OA.OrderID = OSU.OrderID
		WHERE OSU.OrderID = @ID

	/* compute the order level settlement rateIDs & settlement amounts - into temp table #I */
	SELECT OrderID = @ID
		, OrderDate
		, CarrierSettlementFactorID, MinSettlementUnitsID
		, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
		, RouteRateID, RangeRateID, LoadAmount
		, WaitFeeParameterID
		, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount 
		, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount
		, OrderRejectRateID, OrderRejectAmount
		, FuelSurchargeRateID, FuelSurchargeRate, FuelSurchargeAmount
		, OriginTaxRate
		, TotalAmount = ISNULL(LoadAmount, 0) 
			+ ISNULL(OriginWaitAmount, 0) 
			+ ISNULL(DestWaitAmount, 0) 
			+ ISNULL(OrderRejectAmount, 0) 
			+ ISNULL(FuelSurchargeAmount, 0)
		, CreatedByUser = @UserName
		, ChainUp, H2S, Rerouted
	INTO #I
	FROM (
		SELECT OrderDate
			, CarrierSettlementFactorID, MinSettlementUnitsID
			, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
			, RouteRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RouteRateID ELSE NULL END
			, RangeRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RangeRateID ELSE NULL END
			, LoadAmount = round(ISNULL(@LoadAmount, RR.Amount), 2)
			, WaitFeeParameterID
			, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount = ROUND(OriginWaitAmount, 2)
			, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount = ROUND(DestWaitAmount, 2)
			, OrderRejectRateID = CASE WHEN @RejectionAmount IS NULL THEN RD.RateID ELSE NULL END
			, OrderRejectAmount = round(isnull(@RejectionAmount, RD.Amount), 2)
			, FuelSurchargeRateID = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.RateID ELSE NULL END
			, FuelSurchargeRate = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.Rate ELSE NULL END
			, FuelSurchargeAmount = round(isnull(@FuelSurchargeAmount, FSR.Amount), 2)
			, OriginTaxRate
			, Chainup, H2S, Rerouted = cast(CASE WHEN RerouteCount = 0 THEN 0 ELSE 1 END as bit) /* used for the Auto-Approve logic below */
		FROM (
			SELECT O.ID
				, O.OriginID
				, O.CarrierID
				, O.RouteID
				, SU.CarrierSettlementFactorID
				, SU.SettlementFactorID
				, SU.SettlementUomID
				, SU.ActualUnits
				, SU.MinSettlementUnitsID
				, SU.MinSettlementUnits
				, SettlementUnits = CASE WHEN O.Rejected = 1 THEN 0 ELSE SU.SettlementUnits END
				, RouteMiles = isnull(OA.OverrideActualMiles, O.ActualMiles)
				, OrderDate = isnull(O.OrderDate, O.DueDate)
				, ChainUp = cast(CASE WHEN OA.OverrideChainup = 1 THEN 0 ELSE O.Chainup END as bit)
				, H2S = cast(CASE WHEN OA.OverrideH2S = 1 THEN 0 ELSE O.H2S END as bit)
				, O.Rejected
				, RerouteCount = CASE WHEN OA.OverrideReroute = 1 THEN 0 ELSE O.RerouteCount END
				, IsSplit = CASE WHEN O.Rejected = 0 AND O.TicketCount > 1 THEN 1 ELSE 0 END
				, WaitFeeParameterID = WFP.ID
				, OriginWaitRateID = CASE WHEN @OriginWaitAmount IS NULL THEN OWD.RateID ELSE NULL END
				, OriginWaitBillableMinutes = OWD.BillableMinutes
				, OriginWaitBillableHours = OWD.BillableHours
				, OriginWaitAmount = isnull(@OriginWaitAmount, OWD.Amount)
				, DestWaitRateID = CASE WHEN @DestWaitAmount IS NULL THEN DWD.RateID ELSE NULL END
				, DestWaitBillableMinutes = DWD.BillableMinutes
				, DestWaitBillableHours = DWD.BillableHours
				, DestWaitAmount = isnull(@DestWaitAmount, DWD.Amount)
				, OriginTaxRate = OO.TaxRate
			FROM dbo.viewOrder O
			JOIN @SettlementUnits SU ON SU.OrderID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
			OUTER APPLY dbo.fnOrderDriverOriginWaitData(@ID) OWD 
			OUTER APPLY dbo.fnOrderDriverDestinationWaitData(@ID) DWD 
			OUTER APPLY dbo.fnOrderCarrierWaitFeeParameter(@ID) WFP
			WHERE O.ID = @ID
		) X
		OUTER APPLY dbo.fnOrderDriverLoadAmount(@ID, SettlementUnits, SettlementUomID) RR 
		OUTER APPLY dbo.fnOrderDriverOrderRejectData(@ID, SettlementUnits, SettlementUomID) RD
		OUTER APPLY dbo.fnOrderDriverFuelSurchargeData(@ID) FSR
	) X2

	DECLARE @CreatedTran bit; SET @CreatedTran = CASE WHEN @@TRANCOUNT = 0 THEN 1 ELSE 0 END
	BEGIN TRY
		IF (@CreatedTran = 1)
			BEGIN TRAN

		-- remove the existing settlment record (if any)
		DELETE FROM tblOrderSettlementDriverAssessorialCharge WHERE OrderID = @ID
		DELETE FROM tblOrderSettlementDriver WHERE OrderID = @ID
	
		-- all Units and Rates are first normalized to the Order.OriginUOM then consistent processing
		-- and persisted in the Invoice (Settlement) record in this Order.OriginUOM 
		INSERT INTO tblOrderSettlementDriver 
			(OrderID
			, OrderDate
			, CarrierSettlementFactorID
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnitsID
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestinationWaitRateID 
			, DestinationWaitBillableMinutes 
			, DestinationWaitBillableHours
			, DestinationWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser)
		SELECT OrderID
			, OrderDate
			, CarrierSettlementFactorID
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnitsID
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestWaitRateID 
			, DestWaitBillableMinutes 
			, DestWaitBillableHours
			, DestWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser
		FROM #I

		/* compute the new Assessorial Rates for this order - into temp table #IA */
		SELECT OrderID = @ID, AssessorialRateTypeID, AssessorialRateID = RateID, Amount = ROUND(Amount, 2), CreatedByUser = @UserName 
		INTO #IA
		FROM @SettlementUnits SU
		CROSS APPLY dbo.fnOrderDriverAssessorialAmounts(@ID, SU.SettlementUnits, SU.SettlementUomID) CAA
		WHERE CAA.AssessorialRateTypeID NOT IN (SELECT ID FROM @ManualAssessorialRates)

		/* apply the computed settlement assessorial rate/amount records to the DB */
		INSERT INTO tblOrderSettlementDriverAssessorialCharge (OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser)
			SELECT OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser FROM #IA
			UNION
			SELECT @ID, ID, NULL, Amount, @UserName FROM @ManualAssessorialRates

		IF (@CreatedTran = 1)
			COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @msg varchar(max), @severity int
		SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY()
		ROLLBACK
		RAISERROR(@msg, @severity, 1)
		RETURN
	END CATCH	

	PRINT 'spApplyRatesDriver (' + ltrim(@ID) + ') DONE ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	
END

GO

/*************************************************/
-- Created: 4.1.0 - 2016/08/21 - Kevin Alons
-- Purpose: apply rates to the specified [Driver] Financial Session
-- Changes:
/*************************************************/
CREATE PROCEDURE spApplyRatesDriverSession
(
  @SessionID varchar(100)
, @UserName varchar(100)
, @ResetOverrides bit = 0
) AS BEGIN

	SET NOCOUNT ON

	DECLARE @IDs IDTABLE
	INSERT INTO @IDs SELECT OrderID FROM tblOrderSettlementSelectionDriver WHERE SessionID = @SessionID AND RateApplySel = 1

	DECLARE @id int = 0
	WHILE EXISTS (SELECT * FROM @IDs)
	BEGIN
		SELECT @id = min(ID) FROM @IDs
		EXEC spApplyRatesDriver @ID = @id, @UserName = @UserName, @ResetOverrides = @ResetOverrides
		DELETE FROM @IDs WHERE ID = @id
	END

END

GO

exec _spDropProcedure 'spCreateDriverSettlementBatch'
go
/***********************************/
-- Created: ?.?.? - 2013/06/02 - Kevin Alons
-- Purpose: compute and add the various Driver "Settlement" $$ values to an Audited order
-- Changes:
/***********************************/
CREATE PROCEDURE spCreateDriverSettlementBatch
(
  @SessionID varchar(100)
, @CarrierID int
, @DriverGroupID int = NULL
, @DriverID int = NULL
, @InvoiceNum varchar(50) = NULL
, @Notes varchar(255) = NULL
, @UserName varchar(255) 
, @BatchID int = NULL output
, @BatchNum int = NULL output
, @Outcome varchar(25) = 'Success' output
, @Msg varchar(255) = NULL output
) AS BEGIN
	IF (@DriverGroupID = 0) SET @DriverGroupID = NULL
	IF (@DriverID = 0) SET @DriverID = NULL

	-- validation first
	SELECT OS.OrderID, IsRated = cast(CASE WHEN OFB.InvoiceRatesAppliedDateUTC IS NULL THEN 0 ELSE 1 END as bit), OFB.HasError
	INTO #data
	FROM tblOrderSettlementSelectionDriver OS
	LEFT JOIN viewOrder_Financial_Base_Driver OFB ON OFB.ID = OS.OrderID
	WHERE SessionID = @SessionID AND OS.BatchSel = 1

	IF NOT EXISTS (SELECT * FROM #data)
	BEGIN
		SELECT @Outcome = 'Error', @Msg = 'At least 1 order must be selected to complete settlement!'
		RETURN
	END
	ELSE IF EXISTS (SELECT * FROM #data WHERE IsRated = 0 OR HasError <> 0)
	BEGIN
		SELECT @Outcome = 'Error', @Msg = 'All selected orders must be fully rated with no errors to complete Settlement!'
		RETURN
	END

	-- if validation passed, complete the settlement process below
	SET XACT_ABORT ON
	BEGIN TRAN DriverSettlement

	INSERT INTO dbo.tblDriverSettlementBatch(CarrierID, DriverGroupID, DriverID, InvoiceNum, BatchNum, BatchDate, Notes, CreateDateUTC, CreatedByUser)
		SELECT @CarrierID, @DriverGroupID, @DriverID
			, @InvoiceNum
			, isnull(max(BatchNum), 0) + 1
			, dbo.fnDateOnly(getdate()), @Notes, getutcdate(), @UserName 
		FROM dbo.tblDriverSettlementBatch
	SELECT @BatchID = scope_identity()
	SELECT @BatchNum = BatchNum FROM dbo.tblDriverSettlementBatch WHERE ID = @BatchID

	UPDATE tblOrderSettlementDriver 
		SET BatchID = @BatchID
	FROM tblOrderSettlementDriver OSC
	JOIN #data D ON D.OrderID = OSC.OrderID

	SELECT @Outcome = 'Success', @Msg = 'Settlement process complete: ' + ltrim(count(1)) + ' record(s) were added to batch: ' + ltrim(@BatchNum)
	FROM #data

	DELETE FROM tblOrderSettlementSelectionDriver WHERE SessionID = @SessionID AND BatchSel = 1

	COMMIT TRAN
END

GO

exec _spDropProcedure 'spRetrieveOrdersFinancialDriver'
go
/**********************************/
-- Created: 2013/03/09 - ?.?.? - Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
-- Changes:
/***********************************/
CREATE PROCEDURE spRetrieveOrdersFinancialDriver
(
  @StartDate date = NULL -- will default to first day of 3rd prior month for efficiency sake
, @EndDate date = NULL
, @ShipperID int = -1 -- all
, @CarrierID int = -1 -- all
, @ProductGroupID int = -1 -- all 
, @TruckTypeID int = -1 -- all
, @DriverGroupID int = -1 -- all
, @DriverID int = -1 -- all
, @OriginStateID int = -1 -- all 
, @DestStateID int = -1 -- all
, @ProducerID int = -1 -- all
, @BatchID int = NULL -- either show unbatched (NULL), or the specified batch orders
, @OnlyCarrierSettled bit = 0 
, @JobNumber varchar(20) = NULL -- 3.11.19
, @ContractNumber varchar(20) = NULL -- 3.11.19
, @StartSession bit = 0 -- when set to 1, assign the retrieved records to a new Session (ID), when 0 and a sessionID was assigned, then retrieve it
, @SessionID varchar(100) = NULL OUTPUT -- if @startSession = 1, then this return a new value, otherwise will retrieve the existing records in that session
) AS 
BEGIN
	IF (@StartSession IS NULL) SET @StartSession = CASE WHEN @SessionID IS NULL THEN 0 ELSE 1 END

	-- validate the incoming parameters
	IF (@BatchID IS NOT NULL
		AND (@ShipperID <> -1 
			OR @CarrierID <> -1 
			OR @ProductGroupID <> -1
			OR @TruckTypeID <> -1
			OR @DriverGroupID <> -1
			OR @DriverID <> -1
			OR @OriginStateID <> -1
			OR @DestStateID <> -1
			OR @ProducerID <> -1))
	BEGIN
		RAISERROR('@BatchID parameter cannot be specified with other parameters', 16, 1)
		RETURN
	END

	DECLARE @sql varchar(max) = '
		SELECT O.*
			, OS.RateApplySel, OS.BatchSel, OS.SessionID
		FROM viewOrder_Financial_Driver O
		LEFT JOIN tblOrderSettlementSelectionDriver OS ON OS.OrderID = O.ID'

	-- if a @BatchID value was specified then just use this criteria alone
    IF @BatchID IS NOT NULL
	BEGIN
		SET @sql = @sql + '
			WHERE O.BatchID = @BatchID'
		SET @sql = replace(@sql, '@BatchID', @BatchID)
	END
	ELSE IF (@StartSession = 0 AND @SessionID IS NOT NULL)
	BEGIN
		SET @sql = @sql + '
			WHERE O.ID IN (SELECT OrderID FROM tblOrderSettlementSelectionDriver WHERE SessionID = @SessionID)'
		SET @sql = replace(@sql, '@SessionID', @SessionID)
	END
	ELSE
	BEGIN
		SET @sql = @sql + '
			LEFT JOIN dbo.tblOrderTransfer OTR ON OTR.OrderID = O.ID
			LEFT JOIN dbo.viewDriverBase vODR ON vODR.ID = OTR.OriginDriverID
			LEFT JOIN dbo.viewDriverBase vDDR ON vDDR.ID = O.DriverID
			LEFT JOIN tblOrderSettlementCarrier OSP ON OSP.OrderID = O.ID AND OSP.BatchID IS NOT NULL
			WHERE O.StatusID IN (4)  
				AND O.DeleteDateUTC IS NULL
				AND O.BatchID IS NULL 
				AND (@ShipperID=-1 OR O.CustomerID=@ShipperID) 
				AND (@CarrierID=-1 OR O.CarrierID=@CarrierID) 
				AND (@ProductGroupID=-1 OR ProductGroupID=@ProductGroupID) 
				AND (@TruckTypeID=-1 OR TruckTypeID=@TruckTypeID)
				AND (@DriverGroupID=-1 OR ISNULL(vODR.DriverGroupID, vDDR.DriverGroupID) = @DriverGroupID) 
				AND (@DriverID=-1 OR ISNULL(vODR.ID, vDDR.ID) = @DriverID) 
				AND (@OriginStateID=-1 OR O.OriginStateID=@OriginStateID) 
				AND (@DestStateID=-1 OR O.DestStateID=@DestStateID) 
				AND (@ProducerID=-1 OR O.ProducerID=@ProducerID) 
				AND (@StartDate IS NULL OR O.OrderDate >= @StartDate) 
				AND (@EndDate IS NULL OR O.OrderDate <= @EndDate)
				AND (@OnlyCarrierSettled = 0 OR OSP.BatchID IS NOT NULL)
				AND (@JobNumber IS NULL OR O.JobNumber = isnull(@JobNumber, ''''))
				AND (@ContractNumber IS NULL OR O.ContractNumber = isnull(@ContractNumber, ''''))'
		SET @sql = replace(@sql, '@ShipperID', @ShipperID)
		SET @sql = replace(@sql, '@CarrierID', @CarrierID)
		SET @sql = replace(@sql, '@ProductGroupID', @ProductGroupID)
		SET @sql = replace(@sql, '@TruckTypeID', @TruckTypeID)
		SET @sql = replace(@sql, '@DriverGroupID', @DriverGroupID)
		SET @sql = replace(@sql, '@DriverID', @DriverID)
		SET @sql = replace(@sql, '@OriginStateID', @OriginStateID)
		SET @sql = replace(@sql, '@DestStateID', @DestStateID)
		SET @sql = replace(@sql, '@ProducerID', @ProducerID)
		SET @sql = replace(@sql, '@StartDate', dbo.fnQS(@StartDate))
		SET @sql = replace(@sql, '@EndDate', dbo.fnQS(@EndDate))
		SET @sql = replace(@sql, '@OnlyCarrierSettled', @OnlyCarrierSettled)
		SET @sql = replace(@sql, '@JobNumber', dbo.fnQS(nullif(rtrim(@JobNumber), '')))
		SET @sql = replace(@sql, '@ContractNumber', dbo.fnQS(nullif(rtrim(@ContractNumber), '')))
	END

	PRINT 'sql = ' + @sql
	
	BEGIN TRAN RetrieveOrdersFinancialDriver

	-- create the temp table to store the data
	SELECT *, RateApplySel = cast(1 as bit), BatchSel = cast(1 as bit), SessionID = cast(NULL AS varchar(100))
	INTO #ret 
	FROM viewOrder_Financial_Driver 
	WHERE 1 = 0
	
	-- get the data into the temp table
	INSERT INTO #ret EXEC (@sql)

	-- do the SessionID logic
	IF (@StartSession = 1)
	BEGIN
		DELETE FROM tblOrderSettlementSelectionDriver WHERE OrderID IN (SELECT ID FROM #ret) OR SessionID IN (SELECT SessionID FROM #ret)
		SELECT @sessionID = isnull(max(SessionID), 0) + 1 FROM tblOrderSettlementSelectionDriver
		UPDATE #ret SET SessionID = @sessionID, RateApplySel = 1, BatchSel = CASE WHEN HasError = 1 THEN 0 ELSE 1 END
		INSERT INTO tblOrderSettlementSelectionDriver (OrderID, SessionID, RateApplySel, BatchSel)
			SELECT ID, @sessionID, RateApplySel, BatchSel FROM #ret
	END
	
	COMMIT TRAN RetrieveOrdersFinancialDriver

	-- return the data to the caller
	SELECT * FROM #ret
END

GO
GRANT EXECUTE ON spRetrieveOrdersFinancialDriver TO role_iis_acct
GO

/**********************************************/
-- Created: 4.1.0 - 2016/08/18 - Kevin Alons
-- Purpose: update the db-stored Batch & RateApply Selection for a Driver Financial Order Session
-- Changes:
/**********************************************/
CREATE PROCEDURE spUpdateFinancialOrderSessionDriver
(
  @SessionID varchar(100)
, @OrderID int = NULL -- if specified, change sel for that single order, if NULL, then reset the entire session
, @BatchSel bit = NULL OUTPUT
, @RateApplySel bit = NULL OUTPUT
) AS
BEGIN
	IF (@BatchSel IS NOT NULL) 
		UPDATE tblOrderSettlementSelectionDriver 
		SET BatchSel = @BatchSel 
		WHERE (@OrderID IS NULL OR OrderID = @OrderID) AND SessionID = @SessionID
	IF (@RateApplySel IS NOT NULL) 
		UPDATE tblOrderSettlementSelectionDriver 
		SET RateApplySel = @RateApplySel 
		WHERE (@OrderID IS NULL OR OrderID = @OrderID) AND SessionID = @SessionID

	-- return 1 if all selected, 0 if none selected, NULL if some selected
	SELECT @BatchSel = CASE WHEN c = BS THEN 1 WHEN BS = 0 THEN 0 ELSE NULL END 
		, @RateApplySel = CASE WHEN c = RAS THEN 1 WHEN RAS = 0 THEN 0 ELSE NULL END 
	FROM (
		SELECT c = count(1), BS = sum(cast(BatchSel as int)), RAS = sum(cast(RateApplySel as int)) 
		FROM tblOrderSettlementSelectionDriver 
		WHERE SessionID = @SessionID
	) X
END

GO
GRANT EXECUTE ON spUpdateFinancialOrderSessionDriver TO role_iis_acct
GO

/***********************************/
-- Created: ?.?.? - 2016/07/08 - Kevin Alons
-- Purpose: return Report Center Order data without GPS data
-- Changes:
/***********************************/
ALTER VIEW [dbo].[viewReportCenter_Orders_NoGPS] AS
	SELECT X.*
	FROM (
		SELECT O.*
			--
			, ShipperBatchNum = SS.BatchNum
			, ShipperBatchInvoiceNum = SSB.InvoiceNum
			, ShipperSettlementUomID = SS.SettlementUomID
			, ShipperSettlementUom = SS.SettlementUom
			, ShipperMinSettlementUnits = SS.MinSettlementUnits
			, ShipperSettlementUnits = SS.SettlementUnits
			, ShipperRateSheetRate = SS.RateSheetRate
			, ShipperRateSheetRateType = SS.RateSheetRateType
			, ShipperRouteRate = SS.RouteRate
			, ShipperRouteRateType = SS.RouteRateType
			, ShipperLoadRate = ISNULL(SS.RouteRate, SS.RateSheetRate)
			, ShipperLoadRateType = ISNULL(SS.RouteRateType, SS.RateSheetRateType)
			, ShipperLoadAmount = SS.LoadAmount
			, ShipperOrderRejectRate = SS.OrderRejectRate 
			, ShipperOrderRejectRateType = SS.OrderRejectRateType 
			, ShipperOrderRejectAmount = SS.OrderRejectAmount 
			, ShipperWaitFeeSubUnit = SS.WaitFeeSubUnit  
			, ShipperWaitFeeRoundingType = SS.WaitFeeRoundingType  
			, ShipperOriginWaitBillableHours = SS.OriginWaitBillableHours  
			, ShipperOriginWaitBillableMinutes = SS.OriginWaitBillableMinutes  
			, ShipperOriginWaitRate = SS.OriginWaitRate
			, ShipperOriginWaitAmount = SS.OriginWaitAmount
			, ShipperDestinationWaitBillableHours = SS.DestinationWaitBillableHours 
			, ShipperDestinationWaitBillableMinutes = SS.DestinationWaitBillableMinutes   
			, ShipperDestinationWaitRate = SS.DestinationWaitRate  
			, ShipperDestinationWaitAmount = SS.DestinationWaitAmount  
			, ShipperTotalWaitAmount = SS.TotalWaitAmount
			, ShipperTotalWaitBillableMinutes = NULLIF(ISNULL(SS.OriginWaitBillableMinutes, 0) + ISNULL(SS.DestinationWaitBillableMinutes, 0), 0)
			, ShipperTotalWaitBillableHours = NULLIF(ISNULL(SS.OriginWaitBillableHours, 0) + ISNULL(SS.DestinationWaitBillableHours, 0), 0)		
			, ShipperFuelSurchargeRate = SS.FuelSurchargeRate
			, ShipperFuelSurchargeAmount = SS.FuelSurchargeAmount
			, ShipperChainupRate = SS.ChainupRate
			, ShipperChainupRateType = SS.ChainupRateType  
			, ShipperChainupAmount = SS.ChainupAmount
			, ShipperRerouteRate = SS.RerouteRate
			, ShipperRerouteRateType = SS.RerouteRateType  
			, ShipperRerouteAmount = SS.RerouteAmount
			, ShipperSplitLoadRate = SS.SplitLoadRate
			, ShipperSplitLoadRateType = SS.SplitLoadRateType  
			, ShipperSplitLoadAmount = SS.SplitLoadAmount
			, ShipperH2SRate = SS.H2SRate
			, ShipperH2SRateType = SS.H2SRateType  
			, ShipperH2SAmount = SS.H2SAmount
			, ShipperTaxRate = SS.OriginTaxRate
			, ShipperTotalAmount = SS.TotalAmount
			, ShipperDestCode = CDC.Code
			, ShipperTicketType = STT.TicketType
			--
			, CarrierBatchNum = SC.BatchNum
			, CarrierSettlementUomID = SC.SettlementUomID
			, CarrierSettlementUom = SC.SettlementUom
			, CarrierMinSettlementUnits = SC.MinSettlementUnits
			, CarrierSettlementUnits = SC.SettlementUnits
			, CarrierRateSheetRate = SC.RateSheetRate
			, CarrierRateSheetRateType = SC.RateSheetRateType
			, CarrierRouteRate = SC.RouteRate
			, CarrierRouteRateType = SC.RouteRateType
			, CarrierLoadRate = ISNULL(SC.RouteRate, SC.RateSheetRate)
			, CarrierLoadRateType = ISNULL(SC.RouteRateType, SC.RateSheetRateType)
			, CarrierLoadAmount = SC.LoadAmount
			, CarrierOrderRejectRate = SC.OrderRejectRate 
			, CarrierOrderRejectRateType = SC.OrderRejectRateType 
			, CarrierOrderRejectAmount = SC.OrderRejectAmount 
			, CarrierWaitFeeSubUnit = SC.WaitFeeSubUnit  
			, CarrierWaitFeeRoundingType = SC.WaitFeeRoundingType  
			, CarrierOriginWaitBillableHours = SC.OriginWaitBillableHours  
			, CarrierOriginWaitBillableMinutes = SC.OriginWaitBillableMinutes  
			, CarrierOriginWaitRate = SC.OriginWaitRate
			, CarrierOriginWaitAmount = SC.OriginWaitAmount
			, CarrierDestinationWaitBillableHours = SC.DestinationWaitBillableHours 
			, CarrierDestinationWaitBillableMinutes = SC.DestinationWaitBillableMinutes  
			, CarrierDestinationWaitRate = SC.DestinationWaitRate 
			, CarrierDestinationWaitAmount = SC.DestinationWaitAmount  
			, CarrierTotalWaitAmount = SC.TotalWaitAmount
			, CarrierTotalWaitBillableMinutes = NULLIF(ISNULL(SC.OriginWaitBillableMinutes, 0) + ISNULL(SC.DestinationWaitBillableMinutes, 0), 0)
			, CarrierTotalWaitBillableHours = NULLIF(ISNULL(SC.OriginWaitBillableHours, 0) + ISNULL(SC.DestinationWaitBillableHours, 0), 0)		
			, CarrierFuelSurchargeRate = SC.FuelSurchargeRate
			, CarrierFuelSurchargeAmount = SC.FuelSurchargeAmount
			, CarrierChainupRate = SC.ChainupRate
			, CarrierChainupRateType = SC.ChainupRateType  
			, CarrierChainupAmount = SC.ChainupAmount
			, CarrierRerouteRate = SC.RerouteRate
			, CarrierRerouteRateType = SC.RerouteRateType  
			, CarrierRerouteAmount = SC.RerouteAmount
			, CarrierSplitLoadRate = SC.SplitLoadRate
			, CarrierSplitLoadRateType = SC.SplitLoadRateType  
			, CarrierSplitLoadAmount = SC.SplitLoadAmount
			, CarrierH2SRate = SC.H2SRate
			, CarrierH2SRateType = SC.H2SRateType  
			, CarrierH2SAmount = SC.H2SAmount
			, CarrierTaxRate = SC.OriginTaxRate
			, CarrierTotalAmount = SC.TotalAmount
			--
			, DriverBatchNum = SD.BatchNum
			, DriverSettlementUomID = SD.SettlementUomID
			, DriverSettlementUom = SD.SettlementUom
			, DriverMinSettlementUnits = SD.MinSettlementUnits
			, DriverSettlementUnits = SD.SettlementUnits
			, DriverRateSheetRate = SD.RateSheetRate
			, DriverRateSheetRateType = SD.RateSheetRateType
			, DriverRouteRate = SD.RouteRate
			, DriverRouteRateType = SD.RouteRateType
			, DriverLoadRate = ISNULL(SD.RouteRate, SD.RateSheetRate)
			, DriverLoadRateType = ISNULL(SD.RouteRateType, SD.RateSheetRateType)
			, DriverLoadAmount = SD.LoadAmount
			, DriverOrderRejectRate = SD.OrderRejectRate 
			, DriverOrderRejectRateType = SD.OrderRejectRateType 
			, DriverOrderRejectAmount = SD.OrderRejectAmount 
			, DriverWaitFeeSubUnit = SD.WaitFeeSubUnit  
			, DriverWaitFeeRoundingType = SD.WaitFeeRoundingType  
			, DriverOriginWaitBillableHours = SD.OriginWaitBillableHours  
			, DriverOriginWaitBillableMinutes = SD.OriginWaitBillableMinutes  
			, DriverOriginWaitRate = SD.OriginWaitRate
			, DriverOriginWaitAmount = SD.OriginWaitAmount
			, DriverDestinationWaitBillableHours = SD.DestinationWaitBillableHours 
			, DriverDestinationWaitBillableMinutes = SD.DestinationWaitBillableMinutes  
			, DriverDestinationWaitRate = SD.DestinationWaitRate 
			, DriverDestinationWaitAmount = SD.DestinationWaitAmount  
			, DriverTotalWaitAmount = SD.TotalWaitAmount
			, DriverTotalWaitBillableMinutes = NULLIF(ISNULL(SD.OriginWaitBillableMinutes, 0) + ISNULL(SD.DestinationWaitBillableMinutes, 0), 0)
			, DriverTotalWaitBillableHours = NULLIF(ISNULL(SD.OriginWaitBillableHours, 0) + ISNULL(SD.DestinationWaitBillableHours, 0), 0)		
			, DriverFuelSurchargeRate = SD.FuelSurchargeRate
			, DriverFuelSurchargeAmount = SD.FuelSurchargeAmount
			, DriverChainupRate = SD.ChainupRate
			, DriverChainupRateType = SD.ChainupRateType  
			, DriverChainupAmount = SD.ChainupAmount
			, DriverRerouteRate = SD.RerouteRate
			, DriverRerouteRateType = SD.RerouteRateType  
			, DriverRerouteAmount = SD.RerouteAmount
			, DriverSplitLoadRate = SD.SplitLoadRate
			, DriverSplitLoadRateType = SD.SplitLoadRateType  
			, DriverSplitLoadAmount = SD.SplitLoadAmount
			, DriverH2SRate = SD.H2SRate
			, DriverH2SRateType = SD.H2SRateType  
			, DriverH2SAmount = SD.H2SAmount
			, DriverTaxRate = SD.OriginTaxRate
			, DriverTotalAmount = SD.TotalAmount
			--
			, ProducerBatchNum = SP.BatchNum
			, ProducerSettlementUomID = SP.SettlementUomID
			, ProducerSettlementUom = SP.SettlementUom
			, ProducerSettlementUnits = SP.SettlementUnits
			, ProducerCommodityIndex = SP.CommodityIndex
			, ProducerCommodityIndexID = SP.CommodityIndexID
			, ProducerCommodityMethod = SP.CommodityMethod
			, ProducerCommodityMethodID = SP.CommodityMethodID
			, ProducerIndexPrice = SP.Price
			, ProducerIndexStartDate = SP.IndexStartDate
			, ProducerIndexEndDate = SP.IndexEndDate
			, ProducerPriceAmount = SP.CommodityPurchasePricebookPriceAmount
			, ProducerDeduct = SP.Deduct
			, ProducerDeductType = SP.Deduct
			, ProducerDeductAmount = SP.CommodityPurchasePricebookDeductAmount
			, ProducerPremium = SP.PremiumType
			, ProducerPremiumType = SP.PremiumType
			, ProducerPremiumAmount = SP.CommodityPurchasePricebookPremiumAmount
			, ProducerPremiumDesc = SP.PremiumDesc
			, ProducerTotalAmount = SP.CommodityPurchasePricebookTotalAmount
			--
			, OriginLatLon = LTRIM(OO.LAT) + ',' + LTRIM(OO.LON)
			, OriginGeoFenceRadiusMeters = OO.GeoFenceRadiusMeters
			, OriginCTBNum = OO.CTBNum
			, OriginFieldName = OO.FieldName
			, OriginCity = OO.City																				
			, OriginCityState = OO.City + ', ' + (SELECT Abbreviation FROM tblState WHERE ID = OO.StateID)		
			--
			, DestLatLon = LTRIM(D.LAT) + ',' + LTRIM(D.LON)
			, DestGeoFenceRadiusMeters = D.GeoFenceRadiusMeters
			, DestCity = D.City
			, DestCityState = D.City + ', ' + (SELECT Abbreviation FROM tblState WHERE ID = D.StateID) -- 4.1.0 - fix (was pointing to Origin.StateID)
			--
			, Gauger = GAO.Gauger						
			, GaugerIDNumber = GA.IDNumber
			, GaugerFirstName = GA.FirstName
			, GaugerLastName = GA.LastName
			, GaugerRejected = GAO.Rejected
			, GaugerRejectReasonID = GAO.RejectReasonID
			, GaugerRejectNotes = GAO.RejectNotes
			, GaugerRejectNumDesc = GORR.NumDesc
			, GaugerPrintDateUTC = GAO.PrintDateUTC -- 4.1.0 - added for use by GaugerPrintDate EXPRESSION column for effiency reasons (this is already queried, avoid doing the calculation unless needed)
			--, GaugerPrintDate = dbo.fnUTC_to_Local(GAO.PrintDateUTC, O.OriginTimeZoneID, O.OriginUseDST)
			--
			, T_GaugerCarrierTicketNum = CASE WHEN GAO.TicketCount = 0 THEN LTRIM(GAO.OrderNum) + CASE WHEN GAO.Rejected = 1 THEN 'X' ELSE '' END ELSE GOT.CarrierTicketNum END 
			, T_GaugerTankNum = ISNULL(GOT.OriginTankText, GAO.OriginTankText)
			, T_GaugerIsStrappedTank = GOT.IsStrappedTank 
			, T_GaugerProductObsTemp = GOT.ProductObsTemp
			, T_GaugerProductObsGravity = GOT.ProductObsGravity
			, T_GaugerProductBSW = GOT.ProductBSW		
			, T_GaugerOpeningGaugeFeet = GOT.OpeningGaugeFeet
			, T_GaugerOpeningGaugeInch = GOT.OpeningGaugeInch		
			, T_GaugerOpeningGaugeQ = GOT.OpeningGaugeQ			
			, T_GaugerBottomFeet = GOT.BottomFeet
			, T_GaugerBottomInches = GOT.BottomInches		
			, T_GaugerBottomQ = GOT.BottomQ		
			, T_GaugerRejected = GOT.Rejected
			, T_GaugerRejectReasonID = GOT.RejectReasonID
			, T_GaugerRejectNumDesc = GOT.RejectNumDesc
			, T_GaugerRejectNotes = GOT.RejectNotes	
			--
			--, OrderCreateDate = dbo.fnUTC_to_Local(O.CreateDateUTC, O.OriginTimeZoneID, O.OriginUseDST) -- replaced 4.1.0 with an expression type value (more efficient, only executed when needed)
			, OrderApproved = CAST(ISNULL(OA.Approved, 0) AS BIT)
		FROM viewOrder_OrderTicket_Full O
		JOIN tblOrigin OO ON OO.ID = O.OriginID
		JOIN tblDestination D ON D.ID = O.DestinationID
		--
		LEFT JOIN viewGaugerOrder GAO ON GAO.OrderID = O.ID			            
		LEFT JOIN viewGaugerOrderTicket GOT ON GOT.UID = O.T_UID	            
		LEFT JOIN viewGauger GA ON GA.ID = GAO.GaugerID				            
		LEFT JOIN viewOrderRejectReason GORR ON GORR.ID = GAO.RejectReasonID 
		--
		LEFT JOIN tblCustomerDestinationCode CDC ON CDC.CustomerID = O.CustomerID AND CDC.DestinationID = O.DestinationID
		LEFT JOIN viewOrderSettlementCarrier SC ON SC.OrderID = O.ID
		LEFT JOIN viewOrderSettlementShipper SS ON SS.OrderID = O.ID
		LEFT JOIN viewOrderSettlementDriver SD ON SD.OrderID = O.ID
		LEFT JOIN tblShipperSettlementBatch SSB ON SSB.ID = SS.BatchID
		LEFT JOIN viewOrderSettlementProducer SP ON SP.OrderID = O.ID
		LEFT JOIN tblShipperTicketType STT ON STT.CustomerID = O.CustomerID AND STT.TicketTypeID = O.TicketTypeID
		--
		LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
	) X

GO

exec _sprefreshAllViews
go

set identity_insert tblReportColumnDefinition ON
insert into tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport)
	SELECT 305, 1, 'DriverBatchNum', 'SETTLEMENT | PRODUCER | Driver Batch #', NULL, NULL, 4, NULL, 1, 'Administrator,Management,Operations', 0
insert into tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport)
	select '306', '1', 'DriverH2SRate', 'SETTLEMENT | DRIVER | DRIVER H2S Rate', '#0.0000', 'NULL', '4', 'NULL', '1', 'Administrator,Management,Operations,Carrier', '1'
	union select '307', '1', 'DriverH2SAmount', 'SETTLEMENT | DRIVER | DRIVER H2S Fee', '#0.00', 'NULL', '4', 'NULL', '1', 'Administrator,Management,Operations,Carrier', '1'
	union select '308', '1', 'DriverChainupAmount', 'SETTLEMENT | DRIVER | DRIVER Chainup Fee', '#0.00', 'NULL', '4', 'NULL', '1', 'Administrator,Management,Operations,Carrier', '1'
	union select '309', '1', 'DriverTaxRate', 'SETTLEMENT | DRIVER | DRIVER Origin Tax Rate %', '#0.0000', 'NULL', '4', 'NULL', '1', 'Administrator,Management,Operations,Carrier', '1'
	union select '310', '1', 'DriverRerouteAmount', 'SETTLEMENT | DRIVER | DRIVER Reroute Fee', '#0.00', 'NULL', '4', 'NULL', '1', 'Administrator,Management,Operations,Carrier', '1'
	union select '311', '1', 'DriverRerouteRate', 'SETTLEMENT | DRIVER | DRIVER Reroute Rate', '#0.00', 'NULL', '4', 'NULL', '1', 'Administrator,Management,Operations,Carrier', '1'
	union select '312', '1', 'DriverOriginWaitRate', 'SETTLEMENT | DRIVER | DRIVER Origin Wait Rate', '#0.0000', 'NULL', '4', 'NULL', '1', 'Administrator,Management,Operations,Carrier', '1'
	union select '313', '1', 'DriverOriginWaitAmount', 'SETTLEMENT | DRIVER | DRIVER Origin Wait Fee', '#0.00', 'NULL', '4', 'NULL', '1', 'Administrator,Management,Operations,Carrier', '1'
	union select '314', '1', 'DriverDestinationWaitRate', 'SETTLEMENT | DRIVER | DRIVER Dest Wait Rate', '#0.0000', 'NULL', '4', 'NULL', '1', 'Administrator,Management,Operations,Carrier', '1'
	union select '315', '1', 'DriverDestinationWaitAmount', 'SETTLEMENT | DRIVER | DRIVER Dest Wait Fee', '#0.00', 'NULL', '4', 'NULL', '1', 'Administrator,Management,Operations,Carrier', '1'
	union select '316', '1', 'DriverTotalWaitAmount', 'SETTLEMENT | DRIVER | DRIVER Total Wait Fee', '#0.00', 'NULL', '4', 'NULL', '1', 'Administrator,Management,Operations,Carrier', '1'
	union select '317', '1', 'DriverTotalAmount', 'SETTLEMENT | DRIVER | DRIVER Total Fee', '#0.00', 'NULL', '4', 'NULL', '1', 'Administrator,Management,Operations,Carrier', '1'
	union select '318', '1', 'DriverSettlementUom', 'SETTLEMENT | DRIVER | DRIVER Settlement UOM', 'NULL', 'CarrierSettlementUomID', '2', 'SELECT ID, Name = Abbrev FROM tblUom ORDER BY Abbrev', '1', 'Administrator,Management,Operations,Carrier', '1'
	union select '319', '1', 'DriverRouteRate', 'SETTLEMENT | DRIVER | DRIVER Override Route Rate', '#0.0000', 'NULL', '4', 'NULL', '1', 'Administrator,Management,Operations,Carrier', '1'
	union select '320', '1', 'DriverLoadAmount', 'SETTLEMENT | DRIVER | DRIVER Route Fee', '#0.00', 'NULL', '4', 'NULL', '1', 'Administrator,Management,Operations,Carrier', '1'
	union select '321', '1', 'DriverBatchNum', 'SETTLEMENT | DRIVER | DRIVER Batch #', 'NULL', 'NULL', '4', 'NULL', '1', 'Administrator,Management,Operations,Carrier', '0'
	union select '322', '1', 'DriverFuelSurchargeAmount', 'SETTLEMENT | DRIVER | DRIVER Fuel Surcharge Fee', '#0.00', 'NULL', '4', 'NULL', '1', 'Administrator,Management,Operations,Carrier', '1'
	union select '323', '1', 'DriverFuelSurchargeRate', 'SETTLEMENT | DRIVER | DRIVER Fuel Surcharge Rate', '#0.0000', 'NULL', '4', 'NULL', '1', 'Administrator,Management,Operations,Carrier', '1'
	union select '324', '1', 'DriverSplitLoadAmount', 'SETTLEMENT | DRIVER | DRIVER Split Load Fee', '#0.0000', 'NULL', '4', 'NULL', '1', 'Administrator,Management,Operations,Carrier', '1'
	union select '325', '1', 'DriverOrderRejectRate', 'SETTLEMENT | DRIVER | DRIVER Reject Rate', '#0.00', 'NULL', '4', 'NULL', '1', 'Administrator,Management,Operations,Carrier', '1'
	union select '326', '1', 'DriverOrderRejectRateType', 'SETTLEMENT | DRIVER | DRIVER Reject Rate Type', 'NULL', 'NULL', '1', 'NULL', '1', 'Administrator,Management,Operations,Carrier', '1'
	union select '327', '1', 'DriverOrderRejectAmount', 'SETTLEMENT | DRIVER | DRIVER Reject $$', '#0.00', 'NULL', '4', 'NULL', '1', 'Administrator,Management,Operations,Carrier', '1'
	union select '328', '1', 'DriverOriginWaitBillableMinutes', 'SETTLEMENT | DRIVER | DRIVER Origin Wait Billable Minutes', 'NULL', 'NULL', '2', 'NULL', '1', 'Administrator,Management,Operations,Carrier', '1'
	union select '329', '1', 'DriverDestinationWaitBillableMinutes', 'SETTLEMENT | DRIVER | DRIVER Dest Wait Billable Minutes', 'NULL', 'NULL', '2', 'NULL', '1', 'Administrator,Management,Operations,Carrier', '1'
	union select '330', '1', 'DriverChainupRateType', 'SETTLEMENT | DRIVER | DRIVER Chainup Rate Type', 'NULL', 'NULL', '1', 'NULL', '1', 'Administrator,Management,Operations,Carrier', '1'
	union select '331', '1', 'DriverRerouteRateType', 'SETTLEMENT | DRIVER | DRIVER Reroute Rate Type', 'NULL', 'NULL', '1', 'NULL', '1', 'Administrator,Management,Operations,Carrier', '1'
	union select '332', '1', 'DriverSplitLoadRateType', 'SETTLEMENT | DRIVER | DRIVER Split Load Rate Type', 'NULL', 'NULL', '1', 'NULL', '1', 'Administrator,Management,Operations,Carrier', '1'
	union select '333', '1', 'DriverH2SRateType', 'SETTLEMENT | DRIVER | DRIVER H2S Rate Type', 'NULL', 'NULL', '1', 'NULL', '1', 'Administrator,Management,Operations,Carrier', '1'
	union select '334', '1', 'DriverChainupRate', 'SETTLEMENT | DRIVER | DRIVER Chainup Rate', '#0.00', 'NULL', '4', 'NULL', '1', 'Administrator,Management,Operations,Carrier', '1'
	union select '335', '1', 'DriverRouteRate', 'SETTLEMENT | DRIVER | DRIVER Rate Sheet Rate', '#0.0000', 'NULL', '4', 'NULL', '1', 'Administrator,Management,Operations,Carrier', '1'
	union select '336', '1', 'DriverSplitLoadRate', 'SETTLEMENT | DRIVER | DRIVER Split Load Rate', '#0.0000', 'NULL', '4', 'NULL', '1', 'Administrator,Management,Operations,Carrier', '1'
	union select '337', '1', 'DriverOriginWaitBillableHours', 'SETTLEMENT | DRIVER | DRIVER Origin Wait Billable Hours', 'NULL', 'NULL', '4', 'NULL', '1', 'Administrator,Management,Operations,Carrier', '1'
	union select '338', '1', 'DriverDestinationWaitBillableHours', 'SETTLEMENT | DRIVER | DRIVER Destination Wait Billable Hours', 'NULL', 'NULL', '4', 'NULL', '0', 'Administrator,Management,Operations,Carrier', '0'
	union select '339', '1', 'DriverTotalWaitBillableMinutes', 'SETTLEMENT | DRIVER | DRIVER Total Wait Billable Minutes', 'NULL', 'NULL', '4', 'NULL', '0', 'Administrator,Management,Operations,Carrier', '0'
	union select '340', '1', 'DriverLoadRate', 'SETTLEMENT | DRIVER | DRIVER Load Rate', 'NULL', 'NULL', '1', 'NULL', '1', 'Administrator,Management,Operations,Carrier', '1'
	union select '341', '1', 'OriginDriverGroup', 'SETTLEMENT | DRIVER | DRIVER Origin Driver Group', 'NULL', 'OriginDriverGroupID', '2', 'SELECT ID, Name FROM tblDriverGroup ORDER BY Name', '0', '*', '0'
	union select '90053', '1', 'replace(dbo.fnOrderDriverAssessorialDetailsTSV(ID, 0), char(13), char(13) + char(10))', 'SETTLEMENT | DRIVER | DRIVER Misc Details', 'NULL', 'NULL', '1', 'NULL', '1', 'Administrator,Management,Operations,Carrier', '1'
	union select '90054', '1', 'replace(dbo.fnOrderDriverAssessorialAmountsTSV(ID, 0), char(13), char(13) + char(10))', 'SETTLEMENT | DRIVER | DRIVER Misc $$', 'NULL', 'NULL', '1', 'NULL', '1', 'Administrator,Management,Operations,Carrier', '1'
	union select '90055', '1', '(SELECT sum(Amount) FROM tblOrderSettlementDriverAssessorialCharge X WHERE X.OrderID=RS.ID AND X.AssessorialRateTypeID > 4)', 'SETTLEMENT | DRIVER | DRIVER Misc Total $$', 'NULL', 'NULL', '1', 'NULL', '1', 'Administrator,Management,Operations,Carrier', '1'
set identity_insert tblReportColumnDefinition OFF

ALTER TABLE tblRateType ADD ForDriver bit NOT NULL CONSTRAINT DF_RateType_ForDriver DEFAULT (0)
GO
update tblRateType SET ForDriver = 1 WHERE ID IN (1,2,4,5)
go
insert into tblRateType (ID, Name, ForCarrier, ForShipper, ForOrderReject, ForAssessorialFee, ForLoadFee, ForDriver)
	select 6, '% of Carrier', 0, 0, 1, 1, 1, 1
go

insert into aspnet_roles (Applicationid, RoleId, RoleName, LoweredRoleName, Description, Category, FriendlyName)
	select '446467D6-39CD-45E3-B3E0-CAC7945AF3E8', newid(), 'unsettleDriverBatches', 'unsettledriverbatches', 'Allow user to unsettle a driver settlement batch', 'Settlement', 'Unsettle Driver Batches'
go

/*************************************/
-- Created: 4.1.0 - 2016.08.24 - Kevin Alons
-- Purpose: handle specialized logic related to editing Route Rates (due to RouteID being "inputted" as Origin|Destination combination)
-- Changes:
/*************************************/
CREATE TRIGGER trigViewDriverRouteRate_IU_Update ON viewDriverRouteRate INSTEAD OF INSERT, UPDATE  AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
		--PRINT 'ensure a Route record exists for the new Origin-Destination combo'
		INSERT INTO tblRoute (OriginID, DestinationID)
			SELECT DISTINCT OriginID, DestinationID FROM inserted
			EXCEPT SELECT OriginID, DestinationID FROM tblRoute
		
		-- PRINT 'Updating any existing record editable data'
		UPDATE tblDriverRouteRate
			SET ShipperID = nullif(i.ShipperID, 0)
				, DriverID = nullif(i.DriverID, 0)
				, ProductGroupID = nullif(i.ProductGroupID, 0)
				, TruckTypeID = nullif(i.TruckTypeID, 0)
				, DriverGroupID = nullif(i.DriverGroupID, 0)
				, RouteID = R.ID
				, Rate = i.Rate
				, EffectiveDate = i.EffectiveDate
				, EndDate = i.EndDate
				, RateTypeID = i.RateTypeID
				, UomID = i.UomID
				, LastChangeDateUTC = i.LastChangeDateUTC
				, LastChangedByUser = i.LastChangedByUser
		FROM tblDriverRouteRate X
		JOIN inserted i ON i.ID = X.ID
		JOIN tblRoute R ON R.OriginID = i.OriginID AND R.DestinationID = i.DestinationID 

		-- PRINT 'insert any new records'
		INSERT INTO tblDriverRouteRate (ShipperID, CarrierID, ProductGroupID, TruckTypeID, DriverGroupID, DriverID, RouteID, Rate, EffectiveDate, EndDate, RateTypeID, UomID, CreatedByUser)
			SELECT nullif(ShipperID, 0), nullif(CarrierID, 0), nullif(ProductGroupID, 0), nullif(TruckTypeID, 0), nullif(DriverGroupID, 0), nullif(DriverGroupID, 0), R.ID, Rate, EffectiveDate, EndDate, RateTypeID, UomID, i.CreatedByUser
			FROM inserted i
			JOIN tblRoute R ON R.OriginID = i.OriginID AND R.DestinationID = i.DestinationID 
			WHERE ISNULL(i.ID, 0) = 0
	END TRY
	BEGIN CATCH
		DECLARE @error varchar(255)
		SET @error = ERROR_MESSAGE()
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END CATCH
END
GO

/*************************************/
-- Created: 4.1.0 - 2016.08.24 - Kevin Alons
-- Purpose: allow all editing to be done to viewDriverRouteRate (due to specialized logic related to RouteID)
-- Changes:
/*************************************/
CREATE TRIGGER trigViewDriverRouteRate_D ON viewDriverRouteRate INSTEAD OF DELETE AS
BEGIN
	DELETE FROM tblDriverRouteRate WHERE ID IN (SELECT ID FROM deleted)
END
GO

----------------------- SHIPPER CHANGES -----------------------------------
if  OBJECT_ID('tblOrderSettlementSelectionShipper') IS NOT NULL drop table tblOrderSettlementSelectionShipper
go
CREATE TABLE tblOrderSettlementSelectionShipper
(
  OrderID int NOT NULL 
	CONSTRAINT PK_OrderSettlementSelectionShipper PRIMARY KEY 
	CONSTRAINT FK_OrderSettlementSelectionShipper_Order FOREIGN KEY REFERENCES tblOrder(ID)
, SessionID varchar(100) NOT NULL 
, RateApplySel bit NOT NULL CONSTRAINT DF_OrderSettlementSelectionShipper_RateApplySel DEFAULT (1)
, BatchSel bit NOT NULL CONSTRAINT DF_OrderSettlementSelectionShipper_BatchSel DEFAULT (1)
)
GO

exec _spDropView 'viewOrder_Financial_Base_Shipper'
go
/***********************************/
-- Created: 4.1.0 - 2016/08/15 - Kevin Alons
-- Purpose: return BASE Order details + Shipper FINANCIAL INFO into a single view
-- Changes:
/***********************************/
CREATE VIEW viewOrder_Financial_Base_Shipper AS 
	SELECT *
		, HasError = cast(CASE WHEN ErrorFieldCSV IS NOT NULL THEN 1 ELSE 0 END as bit)
	FROM (
		SELECT *
			, OrderID = ID
			, ErrorFieldCSV = nullif(substring(CASE WHEN Approved = 0 THEN ',Approved' ELSE '' END
				+ CASE WHEN InvoiceLoadAmount IS NULL THEN ',InvoiceLoadAmount,InvoiceRouteRate,InvoiceRateSheet' ELSE '' END
				+ CASE WHEN FinalChainup = 1 AND InvoiceChainupAmount IS NULL THEN ',InvoiceChainupAmount' ELSE '' END
				+ CASE WHEN FinalRerouteCount > 0 AND InvoiceRerouteAmount IS NULL THEN ',InvoiceRerouteAmount' ELSE '' END
				+ CASE WHEN FinalH2S = 1 AND InvoiceH2SAmount IS NULL THEN ',InvoiceH2SAmount' ELSE '' END
				+ CASE WHEN TicketCount > 1 AND InvoiceSplitLoadAmount IS NULL THEN ',InvoiceSplitLoadAmount' ELSE '' END
				+ CASE WHEN InvoiceOriginWaitBillableMinutes > 0 AND InvoiceOriginWaitAmount IS NULL THEN ',InvoiceOriginWaitAmount' ELSE '' END
				+ CASE WHEN InvoiceDestinationWaitBillableMinutes > 0 AND InvoiceDestinationWaitAmount IS NULL THEN ',InvoiceDestinationWaitAmount' ELSE '' END
				+ CASE WHEN Rejected = 1 AND InvoiceOrderRejectAmount IS NULL THEN ',InvoiceOrderRejectAmount' ELSE '' END
				+ CASE WHEN InvoiceMinSettlementUnitsID IS NULL THEN ',InvoiceMinSettlementUnits' ELSE '' END
				+ CASE WHEN InvoiceSettlementFactorID IS NULL THEN ',InvoiceSettlementFactor' ELSE '' END
				+ CASE WHEN Rejected = 0 AND InvoiceSettlementFactorID IS NULL THEN ',InvoiceSettlementFactor' ELSE '' END
				+ CASE WHEN Rejected = 0 AND InvoiceSettlementFactorID IS NOT NULL AND dbo.fnOrderSettlementUnits(InvoiceSettlementFactorID, OriginGrossUnits, OriginNetUnits, OriginGrossStdUnits, DestGrossUnits, DestNetUnits) IS NULL THEN ','+OrderUnitsField ELSE '' END
				+ CASE WHEN InvoiceWaitFeeParameterID IS NULL THEN ',InvoiceOriginWaitBillableMinutes,InvoiceDestinationWaitBillableMinutes' ELSE '' END
				+ CASE WHEN isnull(ActualMiles, 0) = 0 THEN ',ActualMiles' ELSE '' END, 2, 100000), '')
		FROM (
			SELECT O.* 
				, InvoiceRatesAppliedDateUTC = OS.CreateDateUTC
				, OS.BatchID
				, Approved = cast(ISNULL(OA.Approved, 0) as bit)
				, FinalActualMiles = ISNULL(OA.OverrideActualMiles, O.ActualMiles)
				, FinalOriginMinutes = ISNULL(OA.OverrideOriginMinutes, O.OriginMinutes)
				, FinalDestMinutes = ISNULL(OA.OverrideDestMinutes, O.DestMinutes)
				, FinalH2S = cast(CASE WHEN isnull(OA.OverrideH2S, 0) = 1 THEN 0 ELSE OO.H2S END as bit)
				, FinalChainup = cast(CASE WHEN isnull(OA.OverrideChainup, 0) = 1 THEN 0 ELSE O.Chainup END as bit)
				, FinalRerouteCount = CASE WHEN isnull(OA.OverrideReroute, 0) = 1 THEN 0 ELSE isnull(ORR.RerouteCount, 0) END
				, FC.OrderUnitsField
				, InvoiceBatchNum = OS.BatchNum 
				, InvoiceOriginWaitBillableMinutes = OS.OriginWaitBillableMinutes
				, InvoiceDestinationWaitBillableMinutes = OS.DestinationWaitBillableMinutes
				, InvoiceTotalWaitBillableMinutes = isnull(OS.OriginWaitBillableMinutes, 0) + ISNULL(OS.DestinationWaitBillableMinutes, 0) 
				, InvoiceWaitFeeParameterID = WaitFeeParameterID
				, InvoiceWaitFeeSubUnit = WaitFeeSubUnit
				, InvoiceWaitFeeRoundingType = WaitFeeRoundingType
				, InvoiceOriginWaitRate = OS.OriginWaitRate 
				, InvoiceOriginWaitAmount = OS.OriginWaitAmount 		
				, InvoiceDestinationWaitRate = OS.DestinationWaitRate 
				, InvoiceDestinationWaitAmount = OS.DestinationWaitAmount 
				, InvoiceTotalWaitAmount = OS.TotalWaitAmount
				, InvoiceOrderRejectRate = OS.OrderRejectRate	
				, InvoiceOrderRejectRateType = OS.OrderRejectRateType  
				, InvoiceOrderRejectAmount = OS.OrderRejectAmount  
				, InvoiceChainupRate = OS.ChainupRate
				, InvoiceChainupRateType = OS.ChainupRateType
				, InvoiceChainupAmount = OS.ChainupAmount 
				, InvoiceRerouteRate = OS.RerouteRate
				, InvoiceRerouteRateType = OS.RerouteRateType
				, InvoiceRerouteAmount = OS.RerouteAmount 
				, InvoiceH2SRate = OS.H2SRate
				, InvoiceH2SRateType = OS.H2SRateType
				, InvoiceH2SAmount = OS.H2SAmount
				, InvoiceSplitLoadRate = OS.SplitLoadRate
				, InvoiceSplitLoadRateType = OS.SplitLoadRateType
				, InvoiceSplitLoadAmount = OS.SplitLoadAmount
				, InvoiceOtherAmount = OS.OtherAmount
				, InvoiceTaxRate = OS.OriginTaxRate
				, InvoiceSettlementUom = OS.SettlementUom 
				, InvoiceSettlementUomShort = OS.SettlementUomShort 
				, InvoiceShipperSettlementFactorID = OS.ShipperSettlementFactorID 
				, InvoiceSettlementFactorID = OS.SettlementFactorID 
				, InvoiceSettlementFactor = OS.SettlementFactor 
				, InvoiceMinSettlementUnitsID = OS.MinSettlementUnitsID 
				, InvoiceMinSettlementUnits = OS.MinSettlementUnits
				, InvoiceUnits = OS.SettlementUnits
				, InvoiceRouteRate = OS.RouteRate
				, InvoiceRouteRateType = OS.RouteRateType
				, InvoiceRateSheetRate = OS.RateSheetRate
				, InvoiceRateSheetRateType = OS.RateSheetRateType
				, InvoiceFuelSurchargeRate = OS.FuelSurchargeRate
				, InvoiceFuelSurchargeAmount = OS.FuelSurchargeAmount
				, InvoiceLoadAmount = OS.LoadAmount
				, InvoiceTotalAmount = OS.TotalAmount
			FROM dbo.viewOrder O
			LEFT JOIN tblOrigin OO ON OO.ID = O.OriginID
			LEFT JOIN viewOrderSettlementShipper OS ON OS.OrderID = O.ID
			LEFT JOIN (SELECT OrderID, RerouteCount = count(1) FROM tblOrderReroute GROUP BY OrderID) ORR ON ORR.OrderID = O.ID
			LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
			LEFT JOIN tblSettlementFactor FC ON FC.ID = OS.SettlementFactorID
			WHERE O.StatusID NOT IN (-10) -- don't include "Generated" orders (only Assigned+)
		) X
	) X1
GO

/***********************************/
-- Created: ?.?.? - 2013.03/09 - Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details + FINANCIAL INFO into a single view
-- Changes:
--	3.7.27	- 2015/06/18 - KDA	- add new columns: ShipperSettlementFactorID, SettlementFactorID, MinSettlementUnitsID
--  4.1.0	- 2016.08/24 - KDA	- use new viewOrde_Financial_Base_Shipper for core values
/***********************************/
ALTER VIEW viewOrder_Financial_Shipper AS 
	SELECT O.* 
		, Shipper = isnull(C.Name, 'N/A')
		, TicketNums = dbo.fnOrderTicketDetails(O.ID, 'TicketNums', '<br/>') 
		, TankNums = dbo.fnOrderTicketDetails(O.ID, 'TankNums', '<br/>') 
		, PreviousDestinations = dbo.fnRerouteDetails(O.ID, 'PreviousDestinations', '<br/>') 
		, RerouteUsers = dbo.fnRerouteDetails(O.ID, 'RerouteUsers', '<br/>') 
		, RerouteDates = dbo.fnRerouteDetails(O.ID, 'RerouteDates', '<br/>') 
		, RerouteNotes = dbo.fnRerouteDetails(O.ID, 'RerouteNotes', '<br/>')
		, InvoiceOtherDetailsTSV = dbo.fnOrderShipperAssessorialDetailsTSV(O.ID, 0)
		, InvoiceOtherAmountsTSV = dbo.fnOrderShipperAssessorialAmountsTSV(O.ID, 0)
	FROM dbo.viewOrder_Financial_Base_Shipper O
	LEFT JOIN tblCustomer C ON C.ID = O.CustomerID
GO

/***********************************/
-- Created: ?.?.? - 2013/03/09 - Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
-- Changes:
-- 3.8.11	- 2015/07/28 - KDA	- remove OriginShipperRegion field
-- 3.9.0	- 2/15/08/14 - KDA	- return Approved column
-- 3.9.19.6	- 2015/09/30 - KDA	- add OverrideXXX fields from OrderApproval table
-- 3.9.34	- 2015/12/16 - BB	- Remove deleted orders from the results (DCWEB-851)
-- 3.10.13.6 - 2015/03/01 - JAE	- add TruckTypeID
-- 3.11.3.1	- 2016/03/22 - KDA	- fix to no-op when @BatchID = 0
-- 3.11.19	- 2016/05/02 - BB	- Add Job number and Contract number
-- 3.11.20.3 - 2016/05/11 - KDA	- optimize @BatchID filtering to query directly from tblOrderSettlementShipper table
--								- use tblOrder and minimal JOINs intead of expensive viewOrder
-- 4.1.0	- 2016/08/08 - KDA	- use new @StartSession & @SessionID parameters to persist the retrieved records as a "Session"
--								- stop defaulting @StartDate to 3 months prior if not supplied (null parameter value provided)
--								- use simplified viewOrder_Financial_Shipper for return results - which is now again optimized for "reasonable" performance
/***********************************/
ALTER PROCEDURE spRetrieveOrdersFinancialShipper
(
  @StartDate date = NULL -- will default to first day of 3rd prior month for efficiency sake
, @EndDate date = NULL
, @ShipperID int = -1 -- all
, @ProductGroupID int = -1 -- all 
, @TruckTypeID int = -1 -- all
, @DriverGroupID int = -1 -- all
, @OriginStateID int = -1 -- all 
, @DestStateID int = -1 -- all
, @ProducerID int = -1 -- all
, @BatchID int = NULL -- either show unbatched (NULL), or the specified batch orders
, @JobNumber varchar(20) = NULL -- 3.11.19
, @ContractNumber varchar(20) = NULL -- 3.11.19
, @StartSession bit = 0 -- when set to 1, assign the retrieved records to a new Session (ID), when 0 and a sessionID was assigned, then retrieve it
, @SessionID varchar(100) = NULL OUTPUT -- if @startSession = 1, then this return a new value, otherwise will retrieve the existing records in that session
) AS 
BEGIN
	IF (@StartSession IS NULL) SET @StartSession = CASE WHEN @SessionID IS NULL THEN 0 ELSE 1 END

	-- validate the incoming parameters
	IF (@BatchID IS NOT NULL
		AND (@ShipperID <> -1 
			OR @ProductGroupID <> -1
			OR @TruckTypeID <> -1
			OR @DriverGroupID <> -1
			OR @OriginStateID <> -1
			OR @DestStateID <> -1
			OR @ProducerID <> -1))
	BEGIN
		RAISERROR('@BatchID parameter cannot be specified with other parameters', 16, 1)
		RETURN
	END

	DECLARE @sql varchar(max) = '
		SELECT O.*
			, OS.RateApplySel, OS.BatchSel, OS.SessionID
		FROM viewOrder_Financial_Shipper O
		LEFT JOIN tblOrderSettlementSelectionShipper OS ON OS.OrderID = O.ID'

	-- if a @BatchID value was specified then just use this criteria alone
    IF @BatchID IS NOT NULL
	BEGIN
		SET @sql = @sql + '
			WHERE O.BatchID = @BatchID'
		SET @sql = replace(@sql, '@BatchID', @BatchID)
	END
	ELSE IF (@StartSession = 0 AND @SessionID IS NOT NULL)
	BEGIN
		SET @sql = @sql + '
			WHERE O.ID IN (SELECT OrderID FROM tblOrderSettlementSelectionShipper WHERE SessionID = @SessionID)'
		SET @sql = replace(@sql, '@SessionID', @SessionID)
	END
	ELSE
	BEGIN
		SET @sql = @sql + '
			LEFT JOIN dbo.tblOrderTransfer OTR ON OTR.OrderID = O.ID
			LEFT JOIN dbo.viewDriverBase vODR ON vODR.ID = OTR.OriginDriverID
			LEFT JOIN dbo.viewDriverBase vDDR ON vDDR.ID = O.DriverID
			WHERE O.StatusID IN (4)  
				AND O.DeleteDateUTC IS NULL
				AND O.BatchID IS NULL 
				AND (@ShipperID=-1 OR O.CustomerID=@ShipperID) 
				AND (@ProductGroupID=-1 OR ProductGroupID=@ProductGroupID) 
				AND (@TruckTypeID=-1 OR TruckTypeID=@TruckTypeID)
				AND (@DriverGroupID=-1 OR ISNULL(vODR.DriverGroupID, vDDR.DriverGroupID) = @DriverGroupID) 
				AND (@OriginStateID=-1 OR O.OriginStateID=@OriginStateID) 
				AND (@DestStateID=-1 OR O.DestStateID=@DestStateID) 
				AND (@ProducerID=-1 OR O.ProducerID=@ProducerID) 
				AND (@StartDate IS NULL OR O.OrderDate >= @StartDate) 
				AND (@EndDate IS NULL OR O.OrderDate <= @EndDate)
				AND (@JobNumber IS NULL OR O.JobNumber = isnull(@JobNumber, ''''))
				AND (@ContractNumber IS NULL OR O.ContractNumber = isnull(@ContractNumber, ''''))'
		SET @sql = replace(@sql, '@ShipperID', @ShipperID)
		SET @sql = replace(@sql, '@ProductGroupID', @ProductGroupID)
		SET @sql = replace(@sql, '@TruckTypeID', @TruckTypeID)
		SET @sql = replace(@sql, '@DriverGroupID', @DriverGroupID)
		SET @sql = replace(@sql, '@OriginStateID', @OriginStateID)
		SET @sql = replace(@sql, '@DestStateID', @DestStateID)
		SET @sql = replace(@sql, '@ProducerID', @ProducerID)
		SET @sql = replace(@sql, '@StartDate', dbo.fnQS(@StartDate))
		SET @sql = replace(@sql, '@EndDate', dbo.fnQS(@EndDate))
		SET @sql = replace(@sql, '@JobNumber', dbo.fnQS(nullif(rtrim(@JobNumber), '')))
		SET @sql = replace(@sql, '@ContractNumber', dbo.fnQS(nullif(rtrim(@ContractNumber), '')))
	END

	-- PRINT 'sql = ' + @sql
	
	BEGIN TRAN RetrieveOrdersFinancialShipper

	-- create the temp table to store the data
	SELECT *, RateApplySel = cast(1 as bit), BatchSel = cast(1 as bit), SessionID = cast(NULL AS varchar(100))
	INTO #ret 
	FROM viewOrder_Financial_Shipper 
	WHERE 1 = 0
	
	-- get the data into the temp table
	INSERT INTO #ret EXEC (@sql)

	-- do the SessionID logic
	IF (@StartSession = 1)
	BEGIN
		DELETE FROM tblOrderSettlementSelectionShipper WHERE OrderID IN (SELECT ID FROM #ret) OR SessionID IN (SELECT SessionID FROM #ret)
		SELECT @sessionID = isnull(max(SessionID), 0) + 1 FROM tblOrderSettlementSelectionShipper
		UPDATE #ret SET SessionID = @sessionID, RateApplySel = 1, BatchSel = CASE WHEN HasError = 1 THEN 0 ELSE 1 END
		INSERT INTO tblOrderSettlementSelectionShipper (OrderID, SessionID, RateApplySel, BatchSel)
			SELECT ID, @sessionID, RateApplySel, BatchSel FROM #ret
	END
	
	COMMIT TRAN RetrieveOrdersFinancialShipper

	-- return the data to the caller
	SELECT * FROM #ret
END
GO

_spDropProcedure 'spUpdateFinancialOrderSessionShipper'
go
/**********************************************/
-- Created: 4.1.0 - 2016/08/18 - Kevin Alons
-- Purpose: update the db-stored Batch & RateApply Selection for a Shipper Financial Order Session
-- Changes:
/**********************************************/
CREATE PROCEDURE spUpdateFinancialOrderSessionShipper
(
  @SessionID varchar(100)
, @OrderID int = NULL -- if specified, change sel for that single order, if NULL, then reset the entire session
, @BatchSel bit = NULL OUTPUT
, @RateApplySel bit = NULL OUTPUT
) AS
BEGIN
	IF (@BatchSel IS NOT NULL) 
		UPDATE tblOrderSettlementSelectionShipper 
		SET BatchSel = @BatchSel 
		WHERE (@OrderID IS NULL OR OrderID = @OrderID) AND SessionID = @SessionID
	IF (@RateApplySel IS NOT NULL) 
		UPDATE tblOrderSettlementSelectionShipper 
		SET RateApplySel = @RateApplySel 
		WHERE (@OrderID IS NULL OR OrderID = @OrderID) AND SessionID = @SessionID

	-- return 1 if all selected, 0 if none selected, NULL if some selected
	SELECT @BatchSel = CASE WHEN c = BS THEN 1 WHEN BS = 0 THEN 0 ELSE NULL END 
		, @RateApplySel = CASE WHEN c = RAS THEN 1 WHEN RAS = 0 THEN 0 ELSE NULL END 
	FROM (
		SELECT c = count(1), BS = sum(cast(BatchSel as int)), RAS = sum(cast(RateApplySel as int)) 
		FROM tblOrderSettlementSelectionShipper 
		WHERE SessionID = @SessionID
	) X
END
GO
GRANT EXECUTE ON spUpdateFinancialOrderSessionShipper TO role_iis_acct
GO

exec _spDropProcedure 'spApplyRatesShipperSession'
go

/*************************************************
-- Created: 4.1.0 - 2016/08/21 - Kevin Alons
-- Purpose: apply rates to the specified [Shipper] Financial Session
-- Changes:
*************************************************/
CREATE PROCEDURE spApplyRatesShipperSession
(
  @SessionID varchar(100)
, @UserName varchar(100)
, @ResetOverrides bit = 0
) AS BEGIN

	SET NOCOUNT ON

	DECLARE @IDs IDTABLE
	INSERT INTO @IDs SELECT OrderID FROM tblOrderSettlementSelectionShipper WHERE SessionID = @SessionID AND RateApplySel = 1

	DECLARE @id int = 0
	WHILE EXISTS (SELECT * FROM @IDs)
	BEGIN
		SELECT @id = min(ID) FROM @IDs
		EXEC spApplyRatesShipper @ID = @id, @UserName = @UserName, @ResetOverrides = @ResetOverrides
		DELETE FROM @IDs WHERE ID = @id
	END

END

GO
GRANT EXECUTE ON spApplyRatesShipperSession TO role_iis_acct
GO

/***********************************/
-- Created: ?.?.? - 2013/06/02 - Kevin Alons
-- Purpose: compute and add the various Shipper "Settlement" $$ values to an Audited order
-- Changes:
-- 4.1.0 - 2016/08/21 - KDA - use new @SessionID parameter to create the entire settlement record in a single invocation
/***********************************/
ALTER PROCEDURE spCreateShipperSettlementBatch
(
  @SessionID varchar(100)
, @ShipperID int
, @InvoiceNum varchar(50)
, @Notes varchar(255)
, @UserName varchar(255)
, @BatchID int = NULL output
, @BatchNum int = NULL output
, @Outcome varchar(25) = 'Success' output
, @Msg varchar(255) = NULL output
) AS BEGIN

	-- validation first
	SELECT OSS.OrderID, IsRated = cast(CASE WHEN OFB.InvoiceRatesAppliedDateUTC IS NULL THEN 0 ELSE 1 END as bit), OFB.HasError
	INTO #data
	FROM tblOrderSettlementSelectionShipper OSS
	LEFT JOIN viewOrder_Financial_Base_Shipper OFB ON OFB.ID = OSS.OrderID
	WHERE SessionID = @SessionID AND OSS.BatchSel = 1

	IF NOT EXISTS (SELECT * FROM #data)
	BEGIN
		SELECT @Outcome = 'Error', @Msg = 'At least order must be selected to complete settlement!'
		RETURN
	END
	ELSE IF EXISTS (SELECT * FROM #data WHERE IsRated = 0 OR HasError <> 0)
	BEGIN
		SELECT @Outcome = 'Error', @Msg = 'All selected orders must be fully rated with no errors to complete Settlement!'
		RETURN
	END

	-- if validation passed, complete the settlement process below
	SET XACT_ABORT ON
	BEGIN TRAN ShipperSettlement

	INSERT INTO dbo.tblShipperSettlementBatch(ShipperID, InvoiceNum, BatchNum, BatchDate, Notes, CreateDateUTC, CreatedByUser)
		SELECT @ShipperID
			, @InvoiceNum
			, isnull(max(BatchNum), 0) + 1
			, dbo.fnDateOnly(getdate()), @Notes, getutcdate(), @UserName 
		FROM dbo.tblShipperSettlementBatch
	SELECT @BatchID = scope_identity()
	SELECT @BatchNum = BatchNum FROM dbo.tblShipperSettlementBatch WHERE ID = @BatchID

	UPDATE tblOrderSettlementShipper 
		SET BatchID = @BatchID
	FROM tblOrderSettlementShipper OSC
	JOIN #data D ON D.OrderID = OSC.OrderID

	SELECT @Outcome = 'Success', @Msg = 'Settlement process complete: ' + ltrim(count(1)) + ' record(s) were added to batch: ' + ltrim(@BatchNum)
	FROM #data

	DELETE FROM tblOrderSettlementSelectionShipper WHERE SessionID = @SessionID AND BatchSel = 1

	COMMIT TRAN
END
GO

exec _spDropProcedure 'spOrderSettlementShipperAddToBatch'
GO

COMMIT
SET NOEXEC OFF