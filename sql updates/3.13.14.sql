SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20) = '3.13.13.3'
	, @NewVersion varchar(20) = '3.13.14'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'DCDRV-265 - Demurrage Report'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

	
/********************************************
-- Author:		Joe Engler
-- Create date: 8/1/2016
-- Description:	Return suspicious orders with a billable wait time (for carrier, at both origin and destination) that exceed a 
					given minute or mile limit
-- Changes:
*********************************************/
CREATE FUNCTION fnOrdersBadDemurrage (@StartDate DATETIME, @EndDate DATETIME, @LIMIT_MINUTES INT = 15, @LIMIT_MILES INT = 10) 
RETURNS TABLE AS RETURN
SELECT

-- negative is entered early, positive is entered later
OriginArriveDelta = datediff(minute,OriginArriveTimeUTC,OriginArriveActualTimeUTC),
OriginDepartDelta = datediff(minute,OriginDepartTimeUTC, OriginDepartActualTimeUTC),
OriginPickupDelta = datediff(minute, origindeparttimeutc, OriginPickupActualTimeUTC),
DestArriveDelta = datediff(minute,DestArriveTimeUTC,DestArriveActualTimeUTC),
DestDepartDelta = datediff(minute,DestDepartTimeUTC, DestDepartActualTimeUTC),
DestDeliverDelta = datediff(minute,DestDepartTimeUTC, DestDeliverActualTimeUTC),
q.*
FROM (
	SELECT o.ID, OrderNum, o.DriverID, o.CarrierID, o.OrderDate, Driver = dr.LastName+ ', ' + dr.FirstName,

		AcceptActualTimeUTC = a.CreateDateUTC,
		AcceptActualDist = dbo.fnDistance(dbo.fnPointFromLatLon(a.Lat, a.Lon), dbo.fnPointFromLatLon(oo.Lat, oo.Lon), 'MILE'),

		OriginArriveTimeUTC, 
		OriginArriveActualTimeUTC = oa.CreateDateUTC,
		OriginArriveActualDist = dbo.fnDistance(dbo.fnPointFromLatLon(oa.Lat, oa.Lon), dbo.fnPointFromLatLon(oo.Lat, oo.Lon), 'MILE'),

		OriginDepartTimeUTC,
		OriginDepartActualTimeUTC = od.CreateDateUTC,
		OriginDepartActualDist = dbo.fnDistance(dbo.fnPointFromLatLon(od.Lat, od.Lon), dbo.fnPointFromLatLon(oo.Lat, oo.Lon), 'MILE'),

		OriginPickupActualTimeUTC = op.CreateDateUTC,
		OriginPickupActualDist = dbo.fnDistance(dbo.fnPointFromLatLon(op.Lat, op.Lon), dbo.fnPointFromLatLon(oo.Lat, oo.Lon), 'MILE'),

		DestArriveTimeUTC, 
		DestArriveActualTimeUTC = da.CreateDateUTC,
		DestArriveActualDist = dbo.fnDistance(dbo.fnPointFromLatLon(da.Lat, da.Lon), dbo.fnPointFromLatLon(d.Lat, d.Lon), 'MILE'),

		DestDepartTimeUTC,
		DestDepartActualTimeUTC = dd.CreateDateUTC,
		DestDepartActualDist = dbo.fnDistance(dbo.fnPointFromLatLon(dd.Lat, dd.Lon), dbo.fnPointFromLatLon(d.Lat, d.Lon), 'MILE'),

		DestDeliverActualTimeUTC = ddel.CreateDateUTC,
		DestDeliverActualDist = dbo.fnDistance(dbo.fnPointFromLatLon(ddel.Lat, ddel.Lon), dbo.fnPointFromLatLon(d.Lat, d.Lon), 'MILE')

	FROM tblOrder o
	JOIN tblOrigin oo ON o.OriginID = oo.ID
	JOIN tblDestination d ON o.DestinationID = d.ID
	JOIN tblDriver dr ON o.DriverID = dr.ID
	OUTER APPLY (select top 1 * from tbldriverlocation dl where o.id=dl.orderid and LocationTypeID = 9 order by SourceDateUTC DESC, SourceAccuracyMeters) as a
	OUTER APPLY (select top 1 * from tbldriverlocation dl where o.id=dl.orderid and dl.originid=o.originid and LocationTypeID = 2 order by SourceDateUTC DESC, SourceAccuracyMeters) as oa
	OUTER APPLY (select top 1 * from tbldriverlocation dl where o.id=dl.orderid and dl.originid=o.originid and LocationTypeID = 3 order by SourceDateUTC, SourceAccuracyMeters) as od
	OUTER APPLY (select top 1 * from tbldriverlocation dl where o.id=dl.orderid and dl.originid=o.originid and LocationTypeID = 6 order by SourceDateUTC, SourceAccuracyMeters) as op 
	OUTER APPLY (select top 1 * from tbldriverlocation dl where o.id=dl.orderid and dl.destinationid=o.destinationid and LocationTypeID = 2 order by SourceDateUTC DESC, SourceAccuracyMeters) as da
	OUTER APPLY (select top 1 * from tbldriverlocation dl where o.id=dl.orderid and dl.destinationid=o.destinationid and LocationTypeID = 3 order by SourceDateUTC, SourceAccuracyMeters) as dd
	OUTER APPLY (select top 1 * from tbldriverlocation dl where o.id=dl.orderid and dl.destinationid=o.destinationid and LocationTypeID = 7 order by SourceDateUTC, SourceAccuracyMeters) as ddel
	WHERE o.OrderDate BETWEEN @StartDate AND @EndDate
) q
LEFT JOIN tblOrderSettlementCarrier OSC ON OSC.OrderID = q.ID

WHERE (OSC.OriginWaitAmount > 0
		AND (  ABS(DATEDIFF(MINUTE,OriginArriveTimeUTC,OriginArriveActualTimeUTC) ) > @LIMIT_MINUTES
			OR (ABS(DATEDIFF(MINUTE,OriginDepartTimeUTC, OriginDepartActualTimeUTC)) > @LIMIT_MINUTES 
					AND ABS(DATEDIFF(MINUTE,OriginDepartTimeUTC, OriginPickupActualTimeUTC)) > @LIMIT_MINUTES)
			OR OriginArriveActualDist > @LIMIT_MILES
			OR (OriginDepartActualDist > @LIMIT_MILES AND OriginPickupActualDist > @LIMIT_MILES)))
	OR (OSC.DestinationWaitAmount > 0
		AND (ABS(DATEDIFF(MINUTE,DestArriveTimeUTC,DestArriveActualTimeUTC) ) > @LIMIT_MINUTES
			OR (ABS(DATEDIFF(MINUTE,DestDepartTimeUTC, DestDepartActualTimeUTC)) > @LIMIT_MINUTES
					AND ABS(DATEDIFF(MINUTE,DestDepartTimeUTC, DestDeliverActualTimeUTC)) > @LIMIT_MINUTES) 
			OR DestArriveActualDist > @LIMIT_MILES
			OR (DestDepartActualDist > @LIMIT_MILES AND DestDeliverActualDist > @LIMIT_MILES)))


GO

-- Add new role
DECLARE @roleName VARCHAR(100) = 'viewDemurrageReport'
DECLARE @roleDesc VARCHAR(100) = 'View Wait Audit Report (Bad Demurrage)'

INSERT INTO ASPNET_ROLES
select applicationid, NEWID(), @roleName, @roleName, @roleDesc from aspnet_applications 
where not exists (select 1 from aspnet_roles where rolename = @roleName)

-- Add current admins to role
EXEC spAddUserPermissionsByRole 'Administrator', @roleName
GO


COMMIT
SET NOEXEC OFF