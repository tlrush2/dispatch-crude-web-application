SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.1.3.1'
SELECT  @NewVersion = '4.1.3.2'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1714 Report Center Field Permissions.  Also restored original triggers for users and groups because a big issue was discovered with the altered versions from update 4.0.6.1.'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO



/* 
Add new report center fields for control over viewing the fields.  These are to be used only for viewing the fields and will not be associated with the site pages at all
*/
EXEC spAddNewPermission 'viewReportCenterCarrierField','Allow user to view Carrier field in Report Center reports','Report Center','Carrier'
GO
EXEC spAddNewPermission 'viewReportCenterShipperField','Allow user to view Shipper field in Report Center reports','Report Center','Shipper'
GO
EXEC spAddNewPermission 'viewReportCenterCarrierFinancials','Allow user to view Carrier Settlement fields in Report Center reports','Report Center','Carrier Financials'
GO
EXEC spAddNewPermission 'viewReportCenterShipperFinancials','Allow user to view Shipper Settlement fields in Report Center reports','Report Center','Shipper Financials'
GO
EXEC spAddNewPermission 'viewReportCenterDriverFinancials','Allow user to view Driver Settlement fields in Report Center reports','Report Center','Driver Financials'
GO


/* 
Change the existing report center permissions' category since we are creating a new category for the new permissions above 
*/
UPDATE aspnet_Roles
SET Category = 'Report Center'
WHERE FriendlyName IN ('Design RC Reports','Report Center')
GO


/*
Update friendly names for the pre-existing RC permissions 
*/
UPDATE aspnet_Roles
SET FriendlyName = 'View'
WHERE FriendlyName = 'Report Center'
GO

UPDATE aspnet_Roles
SET FriendlyName = 'Design'
, Description = 'Allow user to Create and Edit Report Center reports (Requires View permission)'
WHERE FriendlyName = 'Design RC Reports'
GO


/*
Set AllowedRoles back to * on any non-monetary fields
*/
UPDATE tblReportColumnDefinition
SET AllowedRoles = '*'
WHERE ID IN (83,129,130,149,158,159,162,167,176,177,180,182,183,210,213,215,216,217,218,219,220
			,221,222,223,224,225,226,227,228,229,230,237,238,239,281,309,318,321,326,328,329,330
			,331,332,333,337,338,339,90003,90005,90041,90042,90053)
GO


/*
Set permissions on Driver Settlement fields
*/
UPDATE tblReportColumnDefinition
SET AllowedRoles = 'viewReportCenterDriverFinancials'
WHERE ID IN (308,334,315,314,322,323,307,306,340,90054,90055,313,312,319,335,327,325,310,311,320,324,336,317,316)
GO			


/*
Set permissions on Carrier Settlement fields
*/			
UPDATE tblReportColumnDefinition
SET AllowedRoles = 'viewReportCenterCarrierFinancials'
WHERE ID IN (166,232,173,172,181,186,165,164,243,90004,90007,171,170,178,234,214,212,168,169,179,188,236,175,174)			
GO			


/*
Set permissions on Shipper Settlement fields
*/
UPDATE tblReportColumnDefinition
SET AllowedRoles = 'viewReportCenterShipperFinancials'
WHERE ID IN (148,231,155,154,163,185,147,146,244,90006,90008,153,152,160,233,211,209,150,151,161,187,235,157,156)
GO


/*
Update carrier and shipper fields to have their own permissions
*/
UPDATE tblReportColumnDefinition
SET AllowedRoles = 'viewReportCenterCarrierField'
WHERE ID = 1
GO

UPDATE tblReportColumnDefinition
SET AllowedRoles = 'viewReportCenterShipperField'
WHERE ID = 2
GO


/*
Extra (unrelated to this ticket) request from Maverick.  Make change to permission description
*/
UPDATE aspnet_Roles
SET Description = 'Allow user to settle orders for producers (Requires Commodity Pricing)'
WHERE FriendlyName = 'Settle Producers'
GO



/********************************************************
* Created: 6/28/2016												
* Author: Ben Bloodworth
* SQL Update: 4.0.0
* Purpose: To add permissions to users when the group(s) they are in are given more permissions.
*          The permissions will only be added if they are not already provided by another group.
* Changes: 
*		7/21/16 - BB - Removed Delete portion and turned this into an insert only trigger. spDeleteGroupAndUserPermissions now takes care of the delete task.
*		2016/08/27 - 4.0.6.1	- KDA	- cleanup code and ensure it won't fail if an insert is invoked that affects multiple rows
*		9/14/16 - 4.1.3.2 - BB - Restored original code, Kevin's cleanup produced and undetected error that I'm fixing by restoring this for now.  We need to go back and find out what
*								is not working (what got missed) on the cleanup version.  See DCWEB-1714 for more details in the notes/comments.
********************************************************/
ALTER TRIGGER trigRolesInGroups_I ON aspnet_RolesInGroups AFTER INSERT AS 
BEGIN

	IF OBJECT_ID('tempdb..#tempUsersInsert') IS NOT NULL DROP TABLE #tempUsersInsert	
	
	DECLARE @GROUP VARCHAR(100)
		  , @PERMISSION VARCHAR(100)
					
	SET @PERMISSION = (SELECT RoleId FROM inserted)
	SET @GROUP = (SELECT GroupId FROM inserted)

	-- Get a list of users (in #tempUsersInsert) who are currently in the group which has just been given a/new permission(s)
	SELECT UserId
	INTO #tempUsersInsert
	FROM aspnet_UsersInGroups
	WHERE GroupId = @GROUP

	-- Create the new permission record for each user that is currently in the affected group
	INSERT INTO aspnet_UsersInRoles (UserId, RoleId)
	SELECT TUI.UserId, @PERMISSION
	FROM #tempUsersInsert TUI
	WHERE NOT EXISTS (SELECT UserId, RoleId
						FROM aspnet_UsersInRoles
						WHERE UserId = TUI.UserId
						AND RoleId = @PERMISSION)
END 
GO


/********************************************************
* Created: 6/22/2016
* Author: Ben Bloodworth
* SQL Update: 4.0.0
* Purpose: When a group is assigned to a user this trigger will add records to
*          aspnet_UsersInRoles matching the permissions associated with the users new group.
*          Permissions the user already has assigned to them from another group will not be added a second time.
* Changes: 
*	2016/08/27 - 4.0.6.1	- KDA	- cleanup code and ensure it won't fail if an insert is invoked that affects multiple rows
*	9/14/16 - 4.1.3.2 - BB - Restored original code for good measure after problem with trigRolesInGroups_I was found.  Need to look into these "cleanup" versions before putting them back.
********************************************************/
ALTER TRIGGER trigUsersInGroups_I ON aspnet_UsersInGroups AFTER INSERT AS 
BEGIN		

	DECLARE @USER VARCHAR(100) = (SELECT UserId FROM inserted)
		  , @GROUP VARCHAR(100) = (SELECT GroupId FROM inserted)

	INSERT INTO aspnet_UsersInRoles (UserId, RoleId)
	SELECT @USER, RIG.RoleId 
	FROM (SELECT RoleId, GroupId 
		  FROM aspnet_RolesInGroups 
		  WHERE GroupId = @GROUP) RIG
	WHERE NOT EXISTS (SELECT UserId, RoleId 
					  FROM aspnet_UsersInRoles 
					  WHERE UserId = @USER 
						AND RoleId = RIG.RoleId)	
END
GO



COMMIT
SET NOEXEC OFF