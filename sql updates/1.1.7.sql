/**************************************************************************
-- -Generated by xSQL Schema Compare
-- -Date/Time: Apr 29 2013, 09:07:25 PM

-- -Summary:
    T-SQL script that makes the schema of the database [.\sqlexpr2008r2].[BadlandsDB_SC] 
    the same as the schema of the database [.\sqlexpr2008r2].[BadlandsDB].

-- -Action:
    Execute this script on [.\sqlexpr2008r2].[BadlandsDB_SC].

    **** BACKUP the database [.\sqlexpr2008r2].[BadlandsDB_SC] before running this script ****

-- -Source SQL Server version: 10.50.4000.0
-- -Target SQL Server version: 10.50.4000.0
**************************************************************************/

SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET XACT_ABORT ON
GO


BEGIN TRANSACTION
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/***********************************/
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: create new Order (loads) for the specified criteria
/***********************************/
ALTER PROCEDURE [dbo].[spCreateLoads]
(
  @OriginID int
, @DestinationID int
, @TicketTypeID int
, @DueDate datetime
, @CustomerID int
, @CarrierID int = NULL
, @DriverID int = NULL
, @Qty int
, @UserName varchar(100)
, @TankNum varchar(20) = NULL
, @PriorityID int = 3 -- LOW priority
, @StatusID smallint = -10 -- GENERATED
) AS
BEGIN
	DECLARE @i int, @RouteID int, @ActualMiles int
	SELECT @i = 0, @RouteID = ID, @ActualMiles = ActualMiles
	FROM dbo.tblRoute WHERE OriginID = @OriginID AND DestinationID = @DestinationID
	
	IF @RouteID IS NULL
	BEGIN
		RAISERROR('No Route was found for the specified Origin/Destination combination!', 16, 1)
		RETURN
	END
	
	WHILE @i < @Qty BEGIN
		DECLARE @PumperID int, @OperatorID int, @ProducerID int
		SELECT @PumperID = PumperID, @OperatorID = OperatorID, @ProducerID = ProducerID
		FROM tblOrigin WHERE ID = @OriginID
		
		INSERT INTO dbo.tblOrder (OriginID, DestinationID, TicketTypeID, ActualMiles, RouteID, DueDate, CustomerID
				, CarrierID, DriverID, StatusID, PriorityID, OriginTankNum
				, OrderNum, PumperID, OperatorID, ProducerID, CreateDate, CreatedByUser)
			VALUES (@OriginID, @DestinationID, @TicketTypeID, @ActualMiles, @RouteID, @DueDate, @CustomerID
				, @CarrierID, @DriverID, @StatusID, @PriorityID, @TankNum
				, (SELECT isnull((SELECT max(OrderNum) FROM tblOrder), 100000) + 1)
				, @PumperID, @OperatorID, @ProducerID, GETDATE(), @UserName)
		SET @i = @i + 1
	END
END

GO

UPDATE tblSetting SET Value = '1.1.7' WHERE ID = 0

COMMIT TRANSACTION
GO

