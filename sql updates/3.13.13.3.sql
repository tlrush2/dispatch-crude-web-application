SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20) = '3.13.13.2'
	, @NewVersion varchar(20) = '3.13.13.3'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Rebuild all objects hotfix'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[_spRebuildAllObjects]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[_spRebuildAllObjects]
GO

CREATE PROCEDURE _spRebuildAllObjects AS
BEGIN
	SELECT id = row_number() over (order by ObjectName), done = CAST(0 as bit), *
	INTO #update
	FROM (
		SELECT ObjectName = TABLE_NAME, AlterSql = replace(OBJECT_DEFINITION(OBJECT_ID(TABLE_NAME)), 'CREATE VIEW', 'ALTER VIEW')
		FROM INFORMATION_SCHEMA.VIEWS
		UNION 
		SELECT ROUTINE_Name, replace(OBJECT_DEFINITION(object_id(ROUTINE_NAME)), 'CREATE PROCEDURE', 'ALTER PROCEDURE') 
		FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' AND ROUTINE_DEFINITION LIKE '%CREATE PROCEDURE%'
		UNION 
		SELECT ROUTINE_Name, replace(OBJECT_DEFINITION(object_id(ROUTINE_NAME)), 'CREATE FUNCTION', 'ALTER FUNCTION') 
		FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'FUNCTION' AND ROUTINE_DEFINITION LIKE '%CREATE FUNCTION%'
		UNION 
		SELECT name, 'DROP SYNONYM ' + name + '; CREATE SYNONYM ' + name + ' FOR allegroprod.dbo.' + name + ';'
		FROM sys.objects where type = 'sn'
	) X1
	WHERE ObjectName NOT LIKE '%aspnet%' AND ObjectName NOT LIKE '[_]%' AND ObjectName NOT LIKE 'ELMAH%'
	
	DECLARE @id int, @sql nvarchar(max)
	SELECT TOP 1 @id = id, @sql = altersql FROM #update WHERE done = 0

	WHILE (@id IS NOT NULL) BEGIN
		EXEC sp_executesql @sql
		UPDATE #update SET done = 1 WHERE id = @id
		SET @id = null
		SELECT TOP 1 @id = id, @sql = altersql FROM #update WHERE done = 0
	END

END

GO


exec _sprebuildallobjects
GO

COMMIT
SET NOEXEC OFF