-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.8.22.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.8.22.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0

DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.8.22'
SELECT  @NewVersion = '3.9.0'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'Order Auto-Audit: new System Setting + logic to Auto-Audit orders if no Audit errors detected'
	UNION SELECT @NewVersion, 1, 'Order Approval module/process added after Audit and before Settlement steps'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/*************************************************************
-- Date Created: 15 Jul 2015
-- Author: Kevin Alons
-- Purpose: return the Reroute info for a single order as a single record
-- Changes:
*************************************************************/
CREATE FUNCTION fnOrderRerouteData(@orderID int) 
RETURNS @ret TABLE 
(
  OrderID int
, UserNames varchar(max)
, Notes varchar(max)
, PreviousDestinations varchar(max)
, FirstID int
, RerouteMiles int
, PreviousLat varchar(25)
, PreviousLon varchar(25)
, DestLat varchar(25)
, DestLon varchar(25)
) AS BEGIN
	DECLARE @UserNames varchar(max), @Notes varchar(max), @PreviousDestinations varchar(max), @FirstID int
	
	SELECT @UserNames = ISNULL(@UserNames + ', ', '') + ISNULL(UserName, '')
		, @Notes = ISNULL(@Notes + ', ', '') + ISNULL(Notes, '')
		, @PreviousDestinations = ISNULL(@PreviousDestinations + ', ', '') + ISNULL(PreviousDestination, '')
		, @FirstID = isnull(@FirstID, 0) + CASE WHEN @FirstID IS NULL THEN PreviousDestinationID ELSE 0 END
	FROM viewOrderReroute
	WHERE OrderID = @orderID
	ORDER BY RerouteDate
	
	INSERT INTO @ret (OrderID, UserNames, Notes, PreviousDestinations, FirstID, RerouteMiles, PreviousLat, PreviousLon, DestLat, DestLon)
		SELECT @orderID, @UserNames, @Notes, @PreviousDestinations, @FirstID
			, RerouteMiles = dbo.fnDistance(dbo.fnPointFromLatLon(PD.LAT, PD.LON) , dbo.fnPointFromLatLon(FD.LAT, FD.LON), 'miles')
			, PD.LAT, PD.LON, FD.LAT, FD.LON
		FROM tblOrder O
		JOIN tblDestination PD ON PD.ID = @FirstID
		JOIN tblDestination FD ON FD.ID = O.DestinationID
		WHERE O.ID = @orderID
	
	RETURN
END
GO
GRANT SELECT ON fnOrderRerouteData TO role_iis_acct
GO

EXEC sp_rename 'tblOrderApproval.OriginWaitBillableMinutes', 'OverrideOriginMinutes', 'COLUMN'
GO
EXEC sp_rename 'tblOrderApproval.DestWaitBillableMinutes', 'OverrideDestMinutes', 'COLUMN'
GO
EXEC sp_rename 'tblOrderApproval.BillableActualMiles', 'OverrideActualMiles', 'COLUMN'
GO

ALTER TABLE tblOrderApproval ADD OverrideChainup bit null
ALTER TABLE tblOrderApproval ADD OverrideH2S bit null
ALTER TABLE tblOrderApproval ADD OverrideReroute bit null

EXEC _spDropView 'viewOrderApprovalSource'
GO

EXEC _spRefreshAllViews
GO

EXEC _spDropView 'viewOrderApprovalSource'
GO
/*************************************************************
-- Date Created: 15 Jun 2015
-- Author: Kevin Alons
-- Purpose: return the base data for the Order Approval page
-- Changes:
*************************************************************/
CREATE VIEW viewOrderApprovalSource
AS
	SELECT O.ID, O.StatusID, Status = O.OrderStatus, O.OrderNum, O.OrderDate, DeliverDate = isnull(O.DeliverDate, O.OrderDate), OrderDateUTC = O.OriginDepartTimeUTC, DeliverDateUTC = O.DestDepartTimeUTC
		, ShipperID = O.CustomerID, O.CarrierID, O.DriverID, O.TruckID, O.TrailerID, O.Trailer2ID, O.ProducerID, O.OperatorID, O.PumperID
		, Shipper = O.Customer, O.Carrier, O.Driver, O.Truck, O.Trailer, O.Trailer2, O.Producer, O.Operator, O.Pumper
        , O.TicketTypeID, O.TicketType, O.ProductID, O.Product, O.ProductGroupID, O.ProductGroup
    
		, O.Rejected, RejectReason = O.RejectNumDesc, O.RejectNotes
		, O.ChainUp, OA.OverrideChainup
		, O.H2S, OA.OverrideH2S
        , O.DispatchConfirmNum, O.DispatchNotes, O.PickupDriverNotes, O.DeliverDriverNotes
		, Approved = cast(isnull(OA.Approved, 0) as bit), OSS.WaitFeeParameterID, O.AuditNotes
		
		, O.OriginID, O.Origin, O.OriginStateID, O.OriginArriveTime, O.OriginDepartTime, O.OriginMinutes
		, O.OriginUomID, O.OriginUOM, O.OriginGrossUnits, O.OriginGrossStdUnits, O.OriginNetUnits
		, OriginWaitReason = OWR.NumDesc, O.OriginWaitNotes
		, ShipperOriginWaitBillableMinutes = OSS.OriginWaitBillableMinutes
		, CarrierOriginWaitBillableMinutes = OSC.OriginWaitBillableMinutes
		, OA.OverrideOriginMinutes 
		
		, O.DestinationID, O.Destination, O.DestStateID, O.DestArriveTime, O.DestDepartTime, O.DestMinutes
		, O.DestUomID, O.DestUOM, O.DestGrossUnits, O.DestNetUnits, O.DestOpenMeterUnits, O.DestCloseMeterUnits
		, DestWaitReason = OWR.NumDesc, O.DestWaitNotes
		, ShipperDestinationWaitBillableMinutes = OSS.DestinationWaitBillableMinutes
		, CarrierDestinationWaitBillableMinutes = OSC.DestinationWaitBillableMinutes
		, OA.OverrideDestMinutes
	
		, Rerouted = cast(CASE WHEN ORD.OrderID IS NULL THEN 0 ELSE 1 END as bit)
		, OA.OverrideReroute
		, O.ActualMiles, RerouteMiles = round(ORD.RerouteMiles, 0)
		, OA.OverrideActualMiles
		
		, OA.CreateDateUTC, OA.CreatedByUser
		
		, OrderID = OSC.OrderID | OSS.OrderID -- NULL if either are NULL - if NULL indicates that the order is not yet rates (used below to rate these)
	FROM viewOrderLocalDates O
	OUTER APPLY dbo.fnOrderRerouteData(O.ID) ORD
	LEFT JOIN tblOrderReroute ORE ON ORE.OrderID = O.ID
	LEFT JOIN tblOrderSettlementCarrier OSC ON OSC.OrderID = O.ID
	LEFT JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = O.ID
	LEFT JOIN viewOriginWaitReason OWR ON OWR.ID = O.OriginWaitReasonID
	LEFT JOIN viewDestinationWaitReason DWR ON DWR.ID = O.DestWaitReasonID
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
	WHERE O.StatusID IN (4) /* only include orders that are marked AUDITED */
	  AND OSC.BatchID IS NULL AND OSS.BatchID IS NULL /* don't show orders that have been settled */
GO
GRANT SELECT ON viewOrderApprovalSource TO role_iis_acct
GO

/*************************************************************
-- Date Created: 15 Jun 2015
-- Author: Kevin Alons
-- Purpose: return the data for the Order Approval page
-- Changes:
*************************************************************/
ALTER PROCEDURE spOrderApprovalSource
(
  @userName varchar(100)
, @ShipperID int = NULL
, @CarrierID int = NULL
, @ProductGroupID int = NULL
, @OriginStateID int = NULL
, @DestStateID int = NULL
, @Start date = null
, @End date = null
, @IncludeApproved bit = 0
) AS BEGIN
	SET NOCOUNT ON;
	
	SELECT *
	INTO #X
	FROM viewOrderApprovalSource 
	WHERE (NULLIF(@ShipperID, -1) IS NULL OR ShipperID = @ShipperID)
	  AND (NULLIF(@CarrierID, -1) IS NULL OR CarrierID = @CarrierID)
	  AND (NULLIF(@ProductGroupID, -1) IS NULL OR ProductGroupID = @ProductGroupID)
	  AND (NULLIF(@OriginStateID, -1) IS NULL OR OriginStateID = @OriginStateID)
	  AND (NULLIF(@DestStateID, -1) IS NULL OR DestStateID = @DestStateID)
	  -- use of OrderDateUTC between 1 day outside of expected range is a performance optimization to use an index to pre-filter the data before doing a local date criteria clause
	  AND OrderDateUTC BETWEEN DATEADD(day, -1, isnull(@Start, '1/1/1900')) AND DATEADD(day, 1, isnull(@End, '1/1/2200')) 
	  AND OrderDate BETWEEN isnull(@Start, '1/1/1900') AND isnull(@End, '1/1/2200')
	  AND (@IncludeApproved = 1 OR isnull(Approved, 0) = 0) 
	  
	DECLARE @id int
	WHILE EXISTS (SELECT * FROM #x WHERE OrderID IS NULL)
	BEGIN
		SELECT @id = min(ID) FROM #x WHERE OrderID IS NULL
		EXEC spApplyRatesBoth @id, @userName
		UPDATE #x SET OrderID = @id, Approved = (SELECT Approved FROM tblOrderApproval WHERE OrderID = @id) WHERE ID = @id
	END
	ALTER TABLE #X DROP COLUMN OrderID
	
	SELECT * FROM #x WHERE (@IncludeApproved = 1 OR isnull(Approved, 0) = 0) 

END

GO

/*************************************************
-- Date Created: 2015/08/13
-- Author: Kevin Alons
-- Purpose: mark an order APPROVED
-- Changes:
*************************************************/
CREATE PROCEDURE spApproveOrder(@ID int, @UserName varchar(100)) AS
BEGIN 
	/* only allowed AUDITED | non-settled orders from being APPROVED */
	IF EXISTS (
		SELECT O.* 
		FROM tblOrder O 
		LEFT JOIN tblOrderSettlementCarrier OSC ON OSC.OrderID = O.ID
		LEFT JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = O.ID
		WHERE O.ID = @ID AND O.StatusID IN (4) /* 4 = AUDITED */
		  AND OSC.BatchID IS NULL AND OSS.BatchID IS NULL /* this order is not yet SETTLED */
	)
	BEGIN
		UPDATE tblOrderApproval SET Approved = 1 WHERE OrderID = @ID
		IF (@@ROWCOUNT = 0)
			INSERT INTO tblOrderApproval (OrderID, Approved, CreatedByUser, CreateDateUTC) VALUES (@ID, 1, @UserName, getutcdate())
	END
END
GO
GRANT EXECUTE ON spApproveOrder TO role_iis_acct
GO

/*************************************************
-- Date Created: 2015/08/13
-- Author: Kevin Alons
-- Purpose: put all Auto-Approve logic in one module (function)
-- Returns: NULL = VALID auto-approve, non-NULL = invalid reason(s)
-- Changes:
*************************************************/
CREATE FUNCTION fnIsValidAutoApprove
(
  @StatusID int
, @ShipperBatchID int
, @CarrierBatchID int
, @ShipperWaitBillableMin int
, @CarrierWaitBillableMin int
, @WaitFeeParameterID int
, @H2S bit
, @Chainup bit
, @Rerouted bit
, @Approved bit
) RETURNS varchar(1000) AS
BEGIN
	DECLARE @ret varchar(1000)
	SELECT @ret = CASE WHEN isnull(@StatusID, 0) NOT IN (4) THEN '|Order not in Audited status' ELSE '' END
		+ CASE WHEN @ShipperBatchID IS NOT NULL THEN '|Order already Shipper Settled' ELSE '' END
		+ CASE WHEN @CarrierBatchID IS NOT NULL THEN '|Order already Carrier Settled' ELSE '' END
		+ CASE WHEN isnull(@ShipperWaitBillableMin, 0) > 0 THEN '|Order has Shipper Wait Billable Minutes' ELSE '' END
		+ CASE WHEN isnull(@CarrierWaitBillableMin, 0) > 0 THEN '|Order has Carrier Wait Billable Minutes' ELSE '' END
		+ CASE WHEN @WaitFeeParameterID IS NULL THEN '|Order does not have matching Wait Fee Parameter Rule' ELSE '' END
		+ CASE WHEN isnull(@H2S, 0) = 1 THEN '}Order has H2S = true' ELSE '' END
		+ CASE WHEN isnull(@Chainup, 0) = 1 THEN '|Order has Chainup = true' ELSE '' END
		+ CASE WHEN isnull(@Rerouted, 0) = 1 THEN '|Order has Rerouted = true' ELSE '' END
		+ CASE WHEN isnull(@Approved, 0) = 1 THEN '|Order is already Approved' ELSE '' END
	RETURN nullif(rtrim(replace(@ret, '|', char(13) + char(10))), '')
END
GO
GRANT EXECUTE ON fnIsValidAutoApprove TO role_iis_acct
GO

/*************************************************
-- Date Created: 2015/08/13
-- Author: Kevin Alons
-- Purpose: return AutoApprove message for a single order 
-- Returns: NULL is valid, non-null is Invalid reason(s)
-- Changes:
*************************************************/
CREATE FUNCTION fnOrderIsValidAutoApprove
(
  @ID int
) RETURNS varchar(1000) AS
BEGIN
	DECLARE @ret varchar(1000)

	SELECT @ret = dbo.fnIsValidAutoApprove(ISH.StatusID, ISH.BatchID, OSC.BatchID
		, ISH.InvoiceTotalWaitBillableMinutes, isnull(OSC.OriginWaitBillableMinutes, 0) + ISNULL(OSC.DestinationWaitBillableMinutes, 0)
		, ISH.InvoiceWaitFeeParameterID
		, ISH.ChainUp, ISH.H2S, ISH.RerouteCount
		, OA.Approved)
	FROM viewOrder_Financial_Shipper ISH
	LEFT JOIN tblOrderSettlementCarrier OSC ON OSC.OrderID = ISH.ID
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = ISH.ID AND OA.Approved = 1
	WHERE ISH.ID = @ID

	RETURN @ret
END
GO
GRANT EXECUTE ON fnOrderIsValidAutoApprove TO role_iis_acct
GO

/*************************************************
-- Date Created: 2015/08/13
-- Author: Kevin Alons
-- Purpose: mark an order APPROVED if it qualifies for AUTO-APPROVE
-- Changes:
*************************************************/
CREATE PROCEDURE spAutoApproveOrder(@ID int, @UserName varchar(100), @approved bit = 0 OUTPUT) AS
BEGIN
	SET NOCOUNT ON
	SET @approved = 0 /* default value */
	DECLARE @invalidMsg varchar(1000)
	SET @invalidMsg = dbo.fnOrderIsValidAutoApprove(@ID)
	IF (@invalidMsg IS NULL)
	BEGIN
		EXEC spApproveOrder @ID, @UserName
		SET @approved = 1
		PRINT 'Order ID=' + rtrim(@id) + ' AUTO APPROVED'
	END
	ELSE
		PRINT 'Order ID= ' + rtrim(@id) + ' NOT auto-approved: ' + @invalidMsg
END
GO
GRANT EXECUTE ON spAutoApproveOrder TO role_iis_acct
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OriginWaitRate info for the specified order
-- Changes
	- 3.7.13 - 5/21/15 - KDA - fix bug which prevented missing rate warning when billable minutes existed but no rate matched
	- 3.7.28 - 2015/06/18 - KDA - add usage of DriverID/DriverGroupID/ProducerID parameters
	- 3.7.30 - 2015/06/18 - KDA - remove DriverID
	- 3.9.0  - 2015/08/14 - KDA - honor tblOrderApproval.OverrideOriginMinutes in Minutes computation
***********************************/
ALTER FUNCTION fnOrderCarrierOriginWaitRate(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID
		, Minutes = isnull(OA.OverrideOriginMinutes, O.OriginMinutes)
		, R.Rate
	FROM viewOrderBase O
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = @ID
	CROSS APPLY dbo.fnCarrierOriginWaitRate(O.OrderDate, null, O.OriginWaitReasonID, O.CustomerID, O.CarrierID, O.ProductGroupID, O.DriverGroupID, O.OriginID, O.OriginStateID, O.OriginRegionID, O.ProducerID, 1) R
	WHERE O.ID = @ID
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier DestinationWaitRate info for the specified order
-- Changes
	- 3.7.13 - 5/21/15 - KDA - fix bug which prevented missing rate warning when billable minutes existed but no rate matched
	- 3.7.28 - 2015/05/18 - KDA - add use of @ProducerID, @DriverID, @DriverGroupID parameter
	- 3.7.30 - 2015/05/18 - KDA - remove DriverID
	- 3.9.0  - 2015/08/14 - KDA - honor tblOrderApproval.OverrideOriginMinutes in Minutes computation
***********************************/
ALTER FUNCTION fnOrderCarrierDestinationWaitRate(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID
		, Minutes = isnull(OA.OverrideDestMinutes, O.OriginMinutes)
		, R.Rate
	FROM viewOrderBase O
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = @ID
	OUTER APPLY dbo.fnCarrierDestinationWaitRate(O.OrderDate, null, O.DestWaitReasonID, O.CustomerID, O.CarrierID, O.ProductGroupID, O.DriverGroupID, O.DestinationID, O.DestStateID, O.DestRegionID, O.ProducerID, 1) R
	WHERE O.ID = @ID
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper OriginWaitRate info for the specified order
-- Changes
	- 3.7.13 - 5/21/15 - KDA - fix bug which prevented missing rate warning when billable minutes existed but no rate matched
	- 3.9.0  - 2015/08/14 - KDA - honor tblOrderApproval.OverrideOriginMinutes in Minutes computation
***********************************/
ALTER FUNCTION fnOrderShipperOriginWaitRate(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID
		, Minutes = isnull(OA.OverrideOriginMinutes, O.OriginMinutes)
		, R.Rate
	FROM viewOrderBase O
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = @ID
	CROSS APPLY dbo.fnShipperOriginWaitRate(O.OrderDate, null, O.OriginWaitReasonID, O.CustomerID, O.ProductGroupID, O.OriginID, O.OriginStateID, O.OriginRegionID, O.ProducerID, 1) R
	WHERE O.ID = @ID
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper DestinationWaitRate info for the specified order
-- Changes
	- 3.7.13 - 5/21/15 - KDA - fix bug which prevented missing rate warning when billable minutes existed but no rate matched
	- 3.9.0  - 2015/08/14 - KDA - honor tblOrderApproval.OverrideOriginMinutes in Minutes computation
***********************************/
ALTER FUNCTION fnOrderShipperDestinationWaitRate(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID
		, Minutes = isnull(OA.OverrideDestMinutes, O.OriginMinutes)
		, R.Rate
	FROM viewOrderBase O
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = @ID
	OUTER APPLY dbo.fnShipperDestinationWaitRate(O.OrderDate, null, O.DestWaitReasonID, O.CustomerID, O.ProductGroupID, DestinationID, O.DestStateID, O.DestRegionID, O.ProducerID, 1) R
	WHERE O.ID = @ID
GO

/***********************************
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Carrier "Settlement" $$ values to an Delivered/Audited order
-- Changes:
	- 3.7.40 - 2015/06/30 - KDA - use specific SettlementFactor (not fall thru to less precise if specified one is missing)
								- use the DestinationUomID for the Settlement Uom if using a Destination Settlement Factor
	- 3.9.0 - 2015/08/16 - KDA  - use OUTER APPLY for SettlementUnits/MinSettlementUnits
***********************************/
ALTER VIEW viewOrderSettlementUnitsCarrier AS
	SELECT X3.*
		, SettlementUnits = CASE WHEN X3.ActualUnits IS NULL THEN 0 ELSE dbo.fnMaxDecimal(X3.ActualUnits, MinSettlementUnits) END
	FROM (
		SELECT OrderID
			, CarrierID
			, SettlementUomID
			, CarrierSettlementFactorID
			, SettlementFactorID
			, MinSettlementUnits = dbo.fnMinDecimal(isnull(X2.WRMSVUnits, X2.MinSettlementUnits), X2.MinSettlementUnits)
			, MinSettlementUnitsID
			, ActualUnits
		FROM (
			SELECT OrderID
				, CarrierID
				, SettlementUomID
				, CarrierSettlementFactorID
				, SettlementFactorID
				, WRMSVUnits = dbo.fnConvertUOM(WRMSVUnits, WRMSVUomID, SettlementUomID)
				, MinSettlementUnits = dbo.fnConvertUOM(MinSettlementUnits, MinSettlementUomID, SettlementUomID)
				, MinSettlementUnitsID
				, ActualUnits 
			FROM (
				SELECT OrderID = O.ID
					, O.CarrierID
					, SettlementUomID = CASE WHEN MSF.ID IN (1,2,3) THEN O.OriginUomID ELSE O.DestUomID END
					, CarrierSettlementFactorID = MSF.ID
					, MSF.SettlementFactorID
					, R.WRMSVUomID
					, R.WRMSVUnits
					, MinSettlementUnitsID = MSU.ID
					, MinSettlementUomID = MSU.UomID
					, MinSettlementUnits = isnull(MSU.MinSettlementUnits, 0) 
					, ActualUnits = CASE MSF.SettlementFactorID
							WHEN 1 THEN O.OriginGrossUnits 
							WHEN 3 THEN O.OriginGrossStdUnits 
							WHEN 2 THEN O.OriginNetUnits 
							WHEN 4 THEN O.DestGrossUnits
							WHEN 5 THEN O.DestNetUnits END
				FROM dbo.tblOrder O
				OUTER APPLY dbo.fnOrderCarrierSettlementFactor(O.ID) MSF
				OUTER APPLY dbo.fnOrderCarrierMinSettlementUnits(O.ID) MSU 
				JOIN dbo.tblRoute R ON R.ID = O.RouteID
				JOIN tblCarrier C ON C.ID = O.CarrierID
			) X
		) X2
	) X3
GO

/***********************************
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Shipper "Settlement" $$ values to an Delivered/Audited order
-- Changes:
	- 3.7.40 - 2015/06/30 - KDA - use specific SettlementFactor (not fall thru to less precise if specified one is missing)
								- use the DestinationUomID for the Settlement Uom if using a Destination Settlement Factor
	- 3.9.0 - 2015/08/16 - KDA  - use OUTER APPLY for SettlementUnits/MinSettlementUnits
***********************************/
ALTER VIEW viewOrderSettlementUnitsShipper AS
	SELECT X3.*
		, SettlementUnits = CASE WHEN X3.ActualUnits IS NULL THEN 0 ELSE dbo.fnMaxDecimal(X3.ActualUnits, MinSettlementUnits) END
	FROM (
		SELECT OrderID
			, ShipperID
			, SettlementUomID
			, ShipperSettlementFactorID
			, SettlementFactorID
			, MinSettlementUnits = dbo.fnMinDecimal(isnull(X2.WRMSVUnits, X2.MinSettlementUnits), X2.MinSettlementUnits)
			, MinSettlementUnitsID
			, ActualUnits
		FROM (
			SELECT OrderID
				, ShipperID
				, SettlementUomID
				, ShipperSettlementFactorID
				, SettlementFactorID
				, WRMSVUnits = dbo.fnConvertUOM(WRMSVUnits, WRMSVUomID, SettlementUomID)
				, MinSettlementUnits = dbo.fnConvertUOM(MinSettlementUnits, MinSettlementUomID, SettlementUomID)
				, MinSettlementUnitsID
				, ActualUnits 
			FROM (
				SELECT OrderID = O.ID
					, ShipperID = O.CustomerID
					, SettlementUomID = CASE WHEN MSF.ID IN (1,2,3) THEN O.OriginUomID ELSE O.DestUomID END
					, ShipperSettlementFactorID = MSF.ID
					, MSF.SettlementFactorID
					, R.WRMSVUomID
					, R.WRMSVUnits
					, MinSettlementUnitsID = MSU.ID
					, MinSettlementUomID = MSU.UomID
					, MinSettlementUnits = isnull(MSU.MinSettlementUnits, 0) 
					, ActualUnits = CASE MSF.SettlementFactorID
							WHEN 1 THEN O.OriginGrossUnits 
							WHEN 3 THEN O.OriginGrossStdUnits 
							WHEN 2 THEN O.OriginNetUnits 
							WHEN 4 THEN O.DestGrossUnits
							WHEN 5 THEN O.DestNetUnits END
				FROM dbo.tblOrder O
				OUTER APPLY dbo.fnOrderShipperSettlementFactor(O.ID) MSF
				OUTER APPLY dbo.fnOrderShipperMinSettlementUnits(O.ID) MSU 
				JOIN dbo.tblRoute R ON R.ID = O.RouteID
				JOIN tblCustomer C ON C.ID = O.CustomerID
			) X
		) X2
	) X3
GO

/*************************************************
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Carrier "Settlement" $$ values to an Delivered/Audited order
-- Changes:
	- 3.7.26 - 13 Jun 2015 - KDA - add support for: Driver|DriverGroup, Producer to some rates, separate Carrier|Shipper MinSettlementUnits|SettlementFactor best-match values, 
	- 3.9.0  - 2015/08/13  - KDA - honor Approve overrides in rating process
*************************************************/
ALTER PROCEDURE spApplyRatesCarrier
(
  @ID int
, @UserName varchar(100)
, @OriginWaitAmount money = NULL
, @DestWaitAmount money = NULL
, @RejectionAmount money = NULL
, @FuelSurchargeAmount money = NULL
, @LoadAmount money = NULL
, @AssessorialRateTypeID_CSV varchar(max) = NULL
, @AssessorialAmount_CSV varchar(max) = null
, @ResetOverrides bit = 0
) AS BEGIN

	SET NOCOUNT ON
	
	IF EXISTS (SELECT * FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND BatchID IS NOT NULL)
	BEGIN
		RAISERROR('Invoice has already been Carrier Settled', 16, 1)
		RETURN
	END

	-- ensure that Shipper Rates have been applied prior to applying Carrier rates (since they could be dependent on Shipper rates)
	IF NOT EXISTS (SELECT * FROM tblOrderSettlementShipper WHERE OrderID = @ID)
		EXEC spApplyRatesShipper @ID, @UserName

	-- get the provided manual Assessorial Rates (if any) into a usable able
	DECLARE @ManualAssessorialRates TABLE (ID int primary key, Amount money)
	INSERT INTO @ManualAssessorialRates (ID, Amount) 
		SELECT ID.ID, R.Amount
		FROM (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), ID FROM dbo.fnSplitCSVIDs(@AssessorialRateTypeID_CSV)) ID
		JOIN (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), amount = CAST(value as money) FROM dbo.fnSplitCSV(@AssessorialAmount_CSV)) R ON R.sortid = ID.sortid

	-- add in an existing manual Assessorial Rates from the existing Order Settlement (if any)
	INSERT INTO @ManualAssessorialRates (ID, Amount)
		SELECT AssessorialRateTypeID, Amount 
		FROM dbo.tblOrderSettlementCarrierAssessorialCharge 
		WHERE OrderID = @ID AND AssessorialRateID IS NULL 
			AND ID NOT IN (SELECT ID FROM @ManualAssessorialRates)
			AND @ResetOverrides = 0

	IF (@ResetOverrides = 0)
	BEGIN 
		SELECT @OriginWaitAmount = OriginWaitAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @OriginWaitAmount IS NULL AND OriginWaitRateID IS NULL
		SELECT @DestWaitAmount = DestinationWaitAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @DestWaitAmount IS NULL AND DestinationWaitRateID IS NULL
		SELECT @RejectionAmount = OrderRejectAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @RejectionAmount IS NULL AND OrderRejectRateID IS NULL
		SELECT @FuelSurchargeAmount = FuelSurchargeAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @FuelSurchargeAmount IS NULL AND FuelSurchargeRateID IS NULL
		SELECT @LoadAmount = LoadAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @LoadAmount IS NULL AND RouteRateID IS NULL AND RangeRateID IS NULL
	END

	DECLARE @SettlementUnits TABLE
	(
		OrderID int
	  , SettlementUomID int
	  , SettlementFactorID int
	  , CarrierSettlementFactorID int
	  , MinSettlementUnits decimal(18, 10)
	  , MinSettlementUnitsID int
	  , ActualUnits decimal(18, 10)
	  , SettlementUnits decimal(18, 10)
	)
	INSERT INTO @SettlementUnits (OrderID, SettlementUomID, SettlementFactorID, CarrierSettlementFactorID, MinSettlementUnits, MinSettlementUnitsID, ActualUnits, SettlementUnits)
		SELECT OrderID, SettlementUomID, SettlementFactorID, CarrierSettlementFactorID, MinSettlementUnits, MinSettlementUnitsID, ActualUnits, SettlementUnits
		FROM dbo.viewOrderSettlementUnitsCarrier 
		WHERE OrderID = @ID

	SELECT OrderID = @ID
		, OrderDate
		, CarrierSettlementFactorID, MinSettlementUnitsID
		, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
		, RouteRateID, RangeRateID, LoadAmount
		, WaitFeeParameterID
		, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount 
		, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount
		, OrderRejectRateID, OrderRejectAmount
		, FuelSurchargeRateID, FuelSurchargeRate, FuelSurchargeAmount
		, OriginTaxRate
		, TotalAmount = ISNULL(LoadAmount, 0) 
			+ ISNULL(OriginWaitAmount, 0) 
			+ ISNULL(DestWaitAmount, 0) 
			+ ISNULL(OrderRejectAmount, 0) 
			+ ISNULL(FuelSurchargeAmount, 0)
		, CreatedByUser = @UserName
		, ChainUp, H2S, Rerouted
	INTO #I
	FROM (
		SELECT OrderDate
			, CarrierSettlementFactorID, MinSettlementUnitsID
			, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
			, RouteRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RouteRateID ELSE NULL END
			, RangeRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RangeRateID ELSE NULL END
			, LoadAmount = round(ISNULL(@LoadAmount, RR.Amount), 2)
			, WaitFeeParameterID
			, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount = ROUND(OriginWaitAmount, 2)
			, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount = ROUND(DestWaitAmount, 2)
			, OrderRejectRateID = CASE WHEN @RejectionAmount IS NULL THEN RD.RateID ELSE NULL END
			, OrderRejectAmount = round(isnull(@RejectionAmount, RD.Amount), 2)
			, FuelSurchargeRateID = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.RateID ELSE NULL END
			, FuelSurchargeRate = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.Rate ELSE NULL END
			, FuelSurchargeAmount = round(isnull(@FuelSurchargeAmount, FSR.Amount), 2)
			, OriginTaxRate
			, Chainup, H2S, Rerouted = cast(CASE WHEN RerouteCount = 0 THEN 0 ELSE 1 END as bit) /* used for the Auto-Approve logic below */
		FROM (
			SELECT O.ID
				, O.OriginID
				, O.CarrierID
				, O.RouteID
				, SU.CarrierSettlementFactorID
				, SU.SettlementFactorID
				, SU.SettlementUomID
				, SU.ActualUnits
				, SU.MinSettlementUnitsID
				, SU.MinSettlementUnits
				, SettlementUnits = CASE WHEN O.Rejected = 1 THEN 0 ELSE SU.SettlementUnits END
				, RouteMiles = isnull(OA.OverrideActualMiles, O.ActualMiles)
				, OrderDate = isnull(O.OrderDate, O.DueDate)
				, ChainUp = cast(CASE WHEN OA.OverrideChainup = 1 THEN 0 ELSE O.Chainup END as bit)
				, H2S = cast(CASE WHEN OA.OverrideH2S = 1 THEN 0 ELSE O.H2S END as bit)
				, O.Rejected
				, RerouteCount = CASE WHEN OA.OverrideReroute = 1 THEN 0 ELSE O.RerouteCount END
				, IsSplit = CASE WHEN O.Rejected = 0 AND O.TicketCount > 1 THEN 1 ELSE 0 END
				, WaitFeeParameterID = WFP.ID
				, OriginWaitRateID = CASE WHEN @OriginWaitAmount IS NULL THEN OWD.RateID ELSE NULL END
				, OriginWaitBillableMinutes = OWD.BillableMinutes
				, OriginWaitBillableHours = OWD.BillableHours
				, OriginWaitAmount = isnull(@OriginWaitAmount, OWD.Amount)
				, DestWaitRateID = CASE WHEN @DestWaitAmount IS NULL THEN DWD.RateID ELSE NULL END
				, DestWaitBillableMinutes = DWD.BillableMinutes
				, DestWaitBillableHours = DWD.BillableHours
				, DestWaitAmount = isnull(@DestWaitAmount, DWD.Amount)
				, OriginTaxRate = OO.TaxRate
			FROM dbo.viewOrder O
			JOIN @SettlementUnits SU ON SU.OrderID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
			OUTER APPLY dbo.fnOrderCarrierOriginWaitData(@ID) OWD 
			OUTER APPLY dbo.fnOrderCarrierDestinationWaitData(@ID) DWD 
			OUTER APPLY dbo.fnOrderCarrierWaitFeeParameter(@ID) WFP
			WHERE O.ID = @ID
		) X
		OUTER APPLY dbo.fnOrderCarrierLoadAmount(@ID, SettlementUnits, SettlementUomID) RR 
		OUTER APPLY dbo.fnOrderCarrierOrderRejectData(@ID, SettlementUnits, SettlementUomID) RD
		OUTER APPLY dbo.fnOrderCarrierFuelSurchargeData(@ID) FSR
	) X2

	SELECT OrderID = @ID, AssessorialRateTypeID, AssessorialRateID = RateID, Amount = ROUND(Amount, 2), CreatedByUser = @UserName 
	INTO #IA
	FROM @SettlementUnits SU
	CROSS APPLY dbo.fnOrderCarrierAssessorialAmounts(@ID, SU.SettlementUnits, SU.SettlementUomID) CAA
	WHERE CAA.AssessorialRateTypeID NOT IN (SELECT ID FROM @ManualAssessorialRates)

	DECLARE @CreatedTran bit; SET @CreatedTran = CASE WHEN @@TRANCOUNT = 0 THEN 1 ELSE 0 END
	BEGIN TRY
		IF (@CreatedTran = 1)
			BEGIN TRAN

		-- remove the existing settlment record (if any)
		DELETE FROM tblOrderSettlementCarrierAssessorialCharge WHERE OrderID = @ID
		DELETE FROM tblOrderSettlementCarrier WHERE OrderID = @ID
	
		-- all Units and Rates are first normalized to the Order.OriginUOM then consistent processing
		-- and persisted in the Invoice (Settlement) record in this Order.OriginUOM 
		INSERT INTO tblOrderSettlementCarrier 
			(OrderID
			, OrderDate
			, CarrierSettlementFactorID
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnitsID
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestinationWaitRateID 
			, DestinationWaitBillableMinutes 
			, DestinationWaitBillableHours
			, DestinationWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser)
		SELECT OrderID
			, OrderDate
			, CarrierSettlementFactorID
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnitsID
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestWaitRateID 
			, DestWaitBillableMinutes 
			, DestWaitBillableHours
			, DestWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser
		FROM #I

		-- add the accessorial charges
		INSERT INTO tblOrderSettlementCarrierAssessorialCharge (OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser)
			SELECT OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser FROM #IA
			UNION
			SELECT @ID, ID, NULL, Amount, @UserName FROM @ManualAssessorialRates

		IF (@CreatedTran = 1)
			COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @msg varchar(max), @severity int
		SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY()
		ROLLBACK
		RAISERROR(@msg, @severity, 1)
		RETURN
	END CATCH	
	
END
GO

/***********************************
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Shipper "Settlement" $$ values to an Delivered/Audited order
-- Changes:
	- 3.9.0  - 2015/08/13  - KDA - add Auto-Approve logic
						   - honor Approve overrides in rating process
***********************************/
ALTER PROCEDURE spApplyRatesShipper
(
  @ID int
, @UserName varchar(100)
, @OriginWaitAmount money = NULL
, @DestWaitAmount money = NULL
, @RejectionAmount money = NULL
, @FuelSurchargeAmount money = NULL
, @LoadAmount money = NULL
, @AssessorialRateTypeID_CSV varchar(max) = NULL
, @AssessorialAmount_CSV varchar(max) = null
, @ResetOverrides bit = 0
) AS BEGIN

	SET NOCOUNT ON
	
	-- ensure this order hasn't yet been fully settled
	IF EXISTS (SELECT * FROM tblOrderSettlementShipper WHERE OrderID = @ID AND BatchID IS NOT NULL)
	BEGIN
		RAISERROR('Invoice has already been Shipper Settled', 16, 1)
		RETURN
	END

	-- ensure the current Route.ActualMiles is assigned to the order
	UPDATE tblOrder SET ActualMiles = R.ActualMiles
	FROM tblOrder O
	JOIN tblRoute R ON R.ID = O.RouteID
	LEFT JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = O.ID
	WHERE O.ID = @ID
	  AND O.ActualMiles IS NULL OR O.ActualMiles <> R.ActualMiles
	  AND OSS.BatchID IS NULL
	
	-- get the provided manual Assessorial Rates (if any) into a usable able
	DECLARE @ManualAssessorialRates TABLE (ID int primary key, Amount money)
	INSERT INTO @ManualAssessorialRates (ID, Amount) 
		SELECT ID.ID, R.Amount
		FROM (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), ID FROM dbo.fnSplitCSVIDs(@AssessorialRateTypeID_CSV)) ID
		JOIN (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), amount = CAST(value as money) FROM dbo.fnSplitCSV(@AssessorialAmount_CSV)) R ON R.sortid = ID.sortid

	-- add in an existing manual Assessorial Rates from the existing Order Settlement (if any)
	INSERT INTO @ManualAssessorialRates (ID, Amount)
		SELECT AssessorialRateTypeID, Amount 
		FROM dbo.tblOrderSettlementShipperAssessorialCharge 
		WHERE OrderID = @ID AND AssessorialRateID IS NULL 
			AND ID NOT IN (SELECT ID FROM @ManualAssessorialRates)
			AND @ResetOverrides = 0

	IF (@ResetOverrides = 0)
	BEGIN 
		SELECT @OriginWaitAmount = OriginWaitAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @OriginWaitAmount IS NULL AND OriginWaitRateID IS NULL
		SELECT @DestWaitAmount = DestinationWaitAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @DestWaitAmount IS NULL AND DestinationWaitRateID IS NULL
		SELECT @RejectionAmount = OrderRejectAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @RejectionAmount IS NULL AND OrderRejectRateID IS NULL
		SELECT @FuelSurchargeAmount = FuelSurchargeAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @FuelSurchargeAmount IS NULL AND FuelSurchargeRateID IS NULL
		SELECT @LoadAmount = LoadAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @LoadAmount IS NULL AND RouteRateID IS NULL AND RangeRateID IS NULL
	END

	DECLARE @SettlementUnits TABLE
	(
		OrderID int
	  , SettlementUomID int
	  , SettlementFactorID int
	  , ShipperSettlementFactorID int
	  , MinSettlementUnits decimal(18, 10)
	  , MinSettlementUnitsID int
	  , ActualUnits decimal(18, 10)
	  , SettlementUnits decimal(18, 10)
	)
	INSERT INTO @SettlementUnits (OrderID, SettlementUomID, SettlementFactorID, ShipperSettlementFactorID, MinSettlementUnits, MinSettlementUnitsID, ActualUnits, SettlementUnits)
		SELECT OrderID, SettlementUomID, SettlementFactorID, ShipperSettlementFactorID, MinSettlementUnits, MinSettlementUnitsID, ActualUnits, SettlementUnits
		FROM dbo.viewOrderSettlementUnitsShipper
		WHERE OrderID = @ID

	SELECT OrderID = @ID
		, OrderDate
		, ShipperSettlementFactorID, MinSettlementUnitsID
		, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
		, RouteRateID, RangeRateID, LoadAmount
		, WaitFeeParameterID
		, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount 
		, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount
		, OrderRejectRateID, OrderRejectAmount
		, FuelSurchargeRateID, FuelSurchargeRate, FuelSurchargeAmount
		, OriginTaxRate
		, TotalAmount = ISNULL(LoadAmount, 0) 
			+ ISNULL(OriginWaitAmount, 0) 
			+ ISNULL(DestWaitAmount, 0) 
			+ ISNULL(OrderRejectAmount, 0) 
			+ ISNULL(FuelSurchargeAmount, 0)
		, CreatedByUser = @UserName
		, Chainup, H2S, Rerouted /* used for the Auto-Approve logic below */
	INTO #I
	FROM (
		SELECT OrderDate
			, ShipperSettlementFactorID, MinSettlementUnitsID
			, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
			, RouteRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RouteRateID ELSE NULL END
			, RangeRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RangeRateID ELSE NULL END
			, LoadAmount = round(ISNULL(@LoadAmount, RR.Amount), 2)
			, WaitFeeParameterID
			, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount = ROUND(OriginWaitAmount, 2)
			, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount = ROUND(DestWaitAmount, 2)
			, OrderRejectRateID = CASE WHEN @RejectionAmount IS NULL THEN RD.RateID ELSE NULL END
			, OrderRejectAmount = round(isnull(@RejectionAmount, RD.Amount), 2)
			, FuelSurchargeRateID = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.RateID ELSE NULL END
			, FuelSurchargeRate = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.Rate ELSE NULL END
			, FuelSurchargeAmount = round(isnull(@FuelSurchargeAmount, FSR.Amount), 2)
			, OriginTaxRate
			, Chainup, H2S, Rerouted = cast(CASE WHEN RerouteCount = 0 THEN 0 ELSE 1 END as bit) /* used for the Auto-Approve logic below */
		FROM (
			SELECT O.ID
				, O.OriginID
				, O.CustomerID
				, O.RouteID
				, SU.ShipperSettlementFactorID
				, SU.SettlementFactorID
				, SU.SettlementUomID
				, SU.ActualUnits
				, SU.MinSettlementUnitsID
				, SU.MinSettlementUnits
				, SettlementUnits = CASE WHEN O.Rejected = 1 THEN 0 ELSE SU.SettlementUnits END
				, RouteMiles = isnull(OA.OverrideActualMiles, O.ActualMiles)
				, OrderDate = isnull(O.OrderDate, O.DueDate)
				, ChainUp = cast(CASE WHEN OA.OverrideChainup = 1 THEN 0 ELSE O.Chainup END as bit)
				, H2S = cast(CASE WHEN OA.OverrideH2S = 1 THEN 0 ELSE O.H2S END as bit)
				, O.Rejected
				, RerouteCount = CASE WHEN OA.OverrideReroute = 1 THEN 0 ELSE O.RerouteCount END
				, IsSplit = CASE WHEN O.Rejected = 0 AND O.TicketCount > 1 THEN 1 ELSE 0 END
				, WaitFeeParameterID = WFP.ID
				, OriginWaitRateID = CASE WHEN @OriginWaitAmount IS NULL THEN OWD.RateID ELSE NULL END
				, OriginWaitBillableMinutes = OWD.BillableMinutes
				, OriginWaitBillableHours = OWD.BillableHours
				, OriginWaitAmount = isnull(@OriginWaitAmount, OWD.Amount)
				, DestWaitRateID = CASE WHEN @DestWaitAmount IS NULL THEN DWD.RateID ELSE NULL END
				, DestWaitBillableMinutes = DWD.BillableMinutes
				, DestWaitBillableHours = DWD.BillableHours
				, DestWaitAmount = isnull(@DestWaitAmount, DWD.Amount)
				, OriginTaxRate = OO.TaxRate
			FROM dbo.viewOrder O
			JOIN @SettlementUnits SU ON SU.OrderID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
			OUTER APPLY dbo.fnOrderShipperOriginWaitData(@ID) OWD 
			OUTER APPLY dbo.fnOrderShipperDestinationWaitData(@ID) DWD 
			OUTER APPLY dbo.fnOrderShipperWaitFeeParameter(@ID) WFP
			WHERE O.ID = @ID
		) X
		OUTER APPLY dbo.fnOrderShipperLoadAmount(@ID, SettlementUnits, SettlementUomID) RR 
		OUTER APPLY dbo.fnOrderShipperOrderRejectData(@ID, SettlementUnits, SettlementUomID) RD
		OUTER APPLY dbo.fnOrderShipperFuelSurchargeData(@ID) FSR
	) X2

	SELECT OrderID = @ID, AssessorialRateTypeID, AssessorialRateID = RateID, Amount = ROUND(Amount, 2), CreatedByUser = @UserName 
	INTO #IA
	FROM @SettlementUnits SU
	CROSS APPLY dbo.fnOrderShipperAssessorialAmounts(@ID, SU.SettlementUnits, SU.SettlementUomID) CAA
	WHERE CAA.AssessorialRateTypeID NOT IN (SELECT ID FROM @ManualAssessorialRates)

	DECLARE @CreatedTran bit; SET @CreatedTran = CASE WHEN @@TRANCOUNT = 0 THEN 1 ELSE 0 END
	BEGIN TRY
		IF (@CreatedTran = 1)
			BEGIN TRAN

		-- remove the existing settlment record (if any)
		DELETE FROM tblOrderSettlementShipperAssessorialCharge WHERE OrderID = @ID
		DELETE FROM tblOrderSettlementShipper WHERE OrderID = @ID

		-- all Units and Rates are first normalized to the Order.OriginUOM then consistent processing
		-- and persisted in the Invoice (Settlement) record in this Order.OriginUOM 
		INSERT INTO tblOrderSettlementShipper (
/*1*/		OrderID
/*2*/		, OrderDate
/*3*/		, ShipperSettlementFactorID
/*4*/		, SettlementFactorID 
/*5*/		, SettlementUomID 
/*6*/		, MinSettlementUnitsID
/*7*/		, MinSettlementUnits 
/*8*/		, SettlementUnits
/*9*/		, RouteRateID
/*10*/		, RangeRateID 
/*11*/		, LoadAmount
/*12*/		, WaitFeeParameterID 
/*13*/		, OriginWaitRateID 
/*14*/		, OriginWaitBillableMinutes 
/*15*/		, OriginWaitBillableHours
/*16*/		, OriginWaitAmount 
/*17*/		, DestinationWaitRateID 
/*18*/		, DestinationWaitBillableMinutes 
/*19*/		, DestinationWaitBillableHours
/*20*/		, DestinationWaitAmount 
/*21*/		, OrderRejectRateID 
/*22*/		, OrderRejectAmount 
/*23*/		, FuelSurchargeRateID 
/*24*/		, FuelSurchargeRate 
/*25*/		, FuelSurchargeAmount 
/*26*/		, OriginTaxRate 
/*27*/		, TotalAmount 
/*28*/		, CreatedByUser)
		SELECT 
/*01*/		OrderID
/*02*/		, OrderDate
/*03*/		, ShipperSettlementFactorID
/*04*/		, SettlementFactorID 
/*05*/		, SettlementUomID 
/*06*/		, MinSettlementUnitsID
/*07*/		, MinSettlementUnits 
/*08*/		, SettlementUnits
/*09*/		, RouteRateID
/*10*/		, RangeRateID 
/*11*/		, LoadAmount
/*12*/		, WaitFeeParameterID 
/*13*/		, OriginWaitRateID 
/*14*/		, OriginWaitBillableMinutes 
/*15*/		, OriginWaitBillableHours
/*16*/		, OriginWaitAmount 
/*17*/		, DestWaitRateID 
/*18*/		, DestWaitBillableMinutes 
/*19*/		, DestWaitBillableHours
/*20*/		, DestWaitAmount 
/*21*/		, OrderRejectRateID 
/*22*/		, OrderRejectAmount 
/*23*/		, FuelSurchargeRateID 
/*24*/		, FuelSurchargeRate 
/*25*/		, FuelSurchargeAmount 
/*26*/		, OriginTaxRate 
/*27*/		, TotalAmount 
/*28*/		, CreatedByUser
		FROM #I

		INSERT INTO tblOrderSettlementShipperAssessorialCharge (OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser)
			SELECT OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser FROM #IA
			UNION
			SELECT @ID, ID, NULL, Amount, @UserName FROM @ManualAssessorialRates

		IF (@CreatedTran = 1)
			COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @msg varchar(max), @severity int
		SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY()
		ROLLBACK
		RAISERROR(@msg, @severity, 1)
		RETURN
	END CATCH	
	
END
GO

/***********************************
-- Date Created: 2015/08/14
-- Author: Kevin Alons
-- Purpose: create/update APPROVAL on an individual order (and re-rate the order)
-- Changes:
***********************************/
CREATE PROCEDURE spUpdateOrderApproval
(
  @ID int
, @UserName varchar(100)
, @OverrideActualMiles int = NULL
, @OverrideOriginMinutes int = NULL
, @OverrideDestMinutes int = NULL
, @OverrideChainup bit = NULL
, @OverrideH2S bit = NULL
, @OverrideReroute bit = NULL
, @Approved bit = 1
)
AS BEGIN
	/* only allowed AUDITED | non-settled orders from being APPROVED */
	IF EXISTS (
		SELECT O.* 
		FROM tblOrder O 
		LEFT JOIN tblOrderSettlementCarrier OSC ON OSC.OrderID = O.ID
		LEFT JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = O.ID
		WHERE O.ID = @ID AND O.StatusID IN (4) /* 4 = AUDITED */
		  AND OSC.BatchID IS NULL AND OSS.BatchID IS NULL /* this order is not yet SETTLED */
	)
	BEGIN
		DECLARE @startedTX bit
		BEGIN TRY
			IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRAN
				SET @startedTX = 1
			END
			UPDATE tblOrderApproval 
				SET OverrideActualMiles = @OverrideActualMiles
					, OverrideOriginMinutes = @OverrideOriginMinutes
					, OverrideDestMinutes = @OverrideDestMinutes
					, OverrideChainup = @OverrideChainup
					, OverrideH2S = @OverrideH2S
					, OverrideReroute = @OverrideReroute
					, Approved = @Approved
			WHERE OrderID = @ID
			IF (@@ROWCOUNT = 0)
			BEGIN
				INSERT INTO tblOrderApproval 
					(OrderID, OverrideActualMiles, OverrideOriginMinutes, OverrideDestMinutes, OverrideChainup, OverrideH2S, OverrideReroute
						, Approved, CreatedByUser, CreateDateUTC) 
				VALUES 
					(@ID, @OverrideActualMiles, @OverrideOriginMinutes, @OverrideDestMinutes, @OverrideChainup, @OverrideH2S, @OverrideReroute
						, @Approved, @UserName, getutcdate())
			END
			/* ensure any changes to this Approval are applied - which will affect applied rates */
			EXEC spApplyRatesBoth @ID, @UserName
			
			IF (@startedTX = 1)
				COMMIT TRAN
		END TRY
		BEGIN CATCH
			DECLARE @msg varchar(max), @severity int
			SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY()
			IF (@startedTX = 1)
				ROLLBACK TRAN
			RAISERROR(@msg, @severity, 1)
		END CATCH
	END
END
GO
GRANT EXECUTE ON spUpdateOrderApproval TO role_iis_acct
GO

/***********************************
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
-- Changes:
	- 3.7.31 - 2015/06/19 - KDA - reorder input parameters (move ProviderID down) and add @DriverGroupID parameter
	- 3.9.0  - 2/15/08/14 - KDA - return Approved column
***********************************/
ALTER PROCEDURE spRetrieveOrdersFinancialCarrier
(
  @StartDate datetime = NULL
, @EndDate datetime = NULL
, @ShipperID int = -1 -- all
, @CarrierID int = -1 -- all
, @ProductGroupID int = -1 -- all 
, @DriverGroupID int = -1 -- all
, @OriginStateID int = -1 -- all 
, @DestStateID int = -1 -- all
, @ProducerID int = -1 -- all
, @BatchID int = NULL  -- either show unbatched (NULL), or the specified batch orders
, @OnlyShipperSettled bit = 0 
) AS BEGIN

	SELECT @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	
	SELECT DISTINCT OE.* 
		, ShipperSettled = cast(CASE WHEN OSS.BatchID IS NOT NULL THEN 1 ELSE 0 END as bit)
		, InvoiceOtherDetailsTSV = dbo.fnOrderCarrierAssessorialDetailsTSV(OE.ID, 0)
		, InvoiceOtherAmountsTSV = dbo.fnOrderCarrierAssessorialAmountsTSV(OE.ID, 0)
		, Approved = cast(ISNULL(OA.Approved, 0) as bit)
	FROM dbo.viewOrder_Financial_Carrier OE
	LEFT JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = OE.ID AND OSS.BatchID IS NOT NULL
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = OE.ID
	WHERE ID IN (
		SELECT O.ID
		FROM viewOrderBase O
		JOIN tblProduct P ON P.ID = O.ProductID
		JOIN tblOrigin OO ON OO.ID = O.OriginID
		JOIN tblDestination D ON D.ID = O.DestinationID
		LEFT JOIN tblOrderSettlementCarrier OSC ON OSC.OrderID = O.ID
		WHERE O.StatusID IN (4)  
		  AND (@ShipperID=-1 OR O.CustomerID=@ShipperID) 
		  AND (@CarrierID=-1 OR O.CarrierID=@CarrierID) 
		  AND (@ProductGroupID=-1 OR P.ProductGroupID=@ProductGroupID) 
		  AND (@DriverGroupID=-1 OR O.DriverGroupID=@DriverGroupID) 
		  AND (@OriginStateID=-1 OR OO.StateID=@OriginStateID) 
		  AND (@DestStateID=-1 OR D.StateID=@DestStateID) 
		  AND (@ProducerID=-1 OR O.ProducerID=@ProducerID) 
		  AND (@StartDate IS NULL OR O.OrderDate >= @StartDate) 
		  AND (@EndDate IS NULL OR O.OrderDate <= @EndDate)
		  AND ((@BatchID IS NULL AND OSC.BatchID IS NULL) OR OSC.BatchID = @BatchID)
	)
	  AND (@OnlyShipperSettled = 0 OR OSS.BatchID IS NOT NULL)
	ORDER BY OE.OriginDepartTimeUTC
END
GO

/***********************************
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
-- Changes
	- 3.8.11 - 2015/07/28 - KDA - remove OriginShipperRegion field
	- 3.9.0  - 2/15/08/14 - KDA - return Approved column
***********************************/
ALTER PROCEDURE spRetrieveOrdersFinancialShipper
(
  @StartDate datetime = NULL
, @EndDate datetime = NULL
, @ShipperID int = -1 -- all customers
, @ProductGroupID int = -1 -- all product groups
, @ProducerID int = -1 -- all
, @OriginStateID int = -1 -- all 
, @DestStateID int = -1 -- all
, @BatchID int = NULL  -- either show unbatched (NULL), or the specified batch orders
) AS BEGIN

	SELECT @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	
	SELECT DISTINCT OE.* 
		, InvoiceOtherDetailsTSV = dbo.fnOrderShipperAssessorialDetailsTSV(OE.ID, 0)
		, InvoiceOtherAmountsTSV = dbo.fnOrderShipperAssessorialAmountsTSV(OE.ID, 0)
		, Approved = cast(ISNULL(OA.Approved, 0) as bit)
	FROM viewOrder_Financial_Shipper OE
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = OE.ID
	WHERE ID IN (
		SELECT O.ID
		FROM viewOrderBase O
		JOIN tblProduct P ON P.ID = O.ProductID
		JOIN tblOrigin OO ON OO.ID = O.OriginID
		JOIN tblDestination D ON D.ID = O.DestinationID
		LEFT JOIN tblOrderSettlementShipper ISC ON ISC.OrderID = O.ID
		WHERE O.StatusID IN (4)  
		  AND (@ShipperID=-1 OR O.CustomerID=@ShipperID) 
		  AND (@ProductGroupID=-1 OR P.ProductGroupID=@ProductGroupID) 
		  AND (@ProducerID=-1 OR O.ProducerID=@ProducerID) 
		  AND (@OriginStateID=-1 OR OO.StateID=@OriginStateID) 
		  AND (@DestStateID=-1 OR D.StateID=@DestStateID) 
		  AND (@StartDate IS NULL OR O.OrderDate >= @StartDate) 
		  AND (@EndDate IS NULL OR O.OrderDate <= @EndDate)
		  AND ((@BatchID IS NULL AND ISC.BatchID IS NULL) OR ISC.BatchID = @BatchID)
	)
	ORDER BY OE.OriginDepartTimeUTC
END 
GO

/**********************************************************
-- Author: Kevin Alons
-- Date Created: 8 Oct 2014
-- Purpose: return the Audit table records (optionally filtered to just the specified driver and/or ordernum)
-- Changes:
	- 3.7.28 - 2015/06/18 - KDA - add filter to ensure APPROVED orders are not displayed
	- 3.7.44 - 2015/07/06 - BB - Added error catching rule and message for zero volume loads not marked rejected.
	- 3.8.11 - 2015/07/28 - KDA - revise error mesages for excessive load volumes to honor new associated OrderRule
	- 3.9.0  - 2015/08/20 - KDA - slight revision to WHERE clause to use O.ID = @id instead of LIKE operator (efficiency optimization)
***********************************************************/
ALTER FUNCTION fnOrders_AllTickets_Audit(@carrierID int, @driverID int, @orderNum int, @id int) RETURNS TABLE AS RETURN
SELECT *
	, OriginDistanceText = CASE WHEN OriginGpsArrived = 1 THEN 'Verified' ELSE isnull(ltrim(OriginDistance), 'N/A') END
	, DestDistanceText = CASE WHEN DestGpsArrived = 1 THEN 'Verified' ELSE isnull(ltrim(DestDistance), 'N/A') END
	, HasError = cast(CASE WHEN Errors IS NULL THEN 0 ELSE 1 END as bit)
FROM (
	SELECT O.* 
		, OriginGpsLatLon = ltrim(DLO.Lat) + ',' + ltrim(DLO.Lon)
		, OriginLatLon = ltrim(OO.LAT) + ',' + ltrim(OO.LON)
		, OriginAccuracyMeters = DLO.SourceAccuracyMeters
		, OriginDistance = CASE WHEN DLO.DistanceToPoint < 0 THEN NULL ELSE DLO.DistanceToPoint END
		, OriginGpsArrived = CASE WHEN DLO.DistanceToPoint IS NOT NULL AND DLO.DistanceToPoint BETWEEN 0 AND OO.GeoFenceRadiusMeters THEN 1 ELSE 0 END
		, DestGpsLatLon = ltrim(DLD.Lat) + ',' + ltrim(DLD.Lon)
		, DestLatLon = ltrim(D.LAT) + ',' + ltrim(D.LON)
		, DestAccuracyMeters = DLD.SourceAccuracyMeters
		, DestDistance = CASE WHEN DLD.DistanceToPoint < 0 THEN NULL ELSE DLD.DistanceToPoint END
		, DestGpsArrived = CASE WHEN DLD.DistanceToPoint IS NOT NULL AND DLD.DistanceToPoint BETWEEN 0 AND D.GeoFenceRadiusMeters THEN 1 ELSE 0 END
		, OriginGrossBBLS = dbo.fnConvertUOM(O.OriginGrossUnits, O.OriginUomID, 1)
		, Errors = 
			nullif(
				SUBSTRING(
					CASE WHEN O.OriginArriveTimeUTC IS NULL THEN ',Missing Pickup Arrival' ELSE '' END
				  + CASE WHEN O.OriginDepartTimeUTC IS NULL THEN ',Missing Pickup Departure' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND O.Tickets IS NULL THEN ',No Active Tickets' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND O.DestArriveTimeUTC IS NULL THEN ',Missing Delivery Arrival' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND O.DestDepartTimeUTC IS NULL THEN ',Missing Delivery Departure' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND O.OriginGrossUnits = 0 AND O.OriginNetUnits = 0 THEN ',No Origin Units are entered. '+CHAR(13)+CHAR(10)+' This may be an unmarked rejected load.' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND dbo.fnConvertUOM(O.OriginGrossUnits, O.OriginUomID, 2) > isnull(OORMG.MaxGallons, 320) THEN ',Origin GOV Units out of limits' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND dbo.fnConvertUOM(O.OriginGrossStdUnits, O.OriginUomID, 2) > isnull(OORMG.MaxGallons, 320) THEN ',Origin GSV Units out of limits' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND dbo.fnConvertUOM(O.OriginNetUnits, O.OriginUomID, 2) > isnull(OORMG.MaxGallons, 320) THEN ',Origin NSV Units out of limits' ELSE '' END				  
				  + CASE WHEN dbo.fnToBool(OOR.Value) = 1 AND (SELECT count(*) FROM tblOrderTicket OT WHERE OrderID = O.ID AND OT.DeleteDateUTC IS NULL AND OT.DispatchConfirmNum IS NULL) > 0 THEN ',Missing Ticket Shipper PO' ELSE '' END
				  + CASE WHEN EXISTS(SELECT CustomerID FROM viewOrder_OrderTicket_Full WHERE ID = O.ID AND DeleteDateUTC IS NULL AND nullif(rtrim(T_DispatchConfirmNum), '') IS NOT NULL GROUP BY CustomerID, T_DispatchConfirmNum HAVING COUNT(*) > 1) THEN ',Duplicate Shipper PO' ELSE '' END
				  + CASE WHEN O.TruckID IS NULL THEN ',Truck Missing' ELSE '' END
				  + CASE WHEN O.TrailerID IS NULL THEN ',Trailer 1 Missing' ELSE '' END
				, 2, 8000) 
			, '')
		, IsEditable = cast(CASE WHEN O.StatusID = 3 THEN 1 ELSE 0 END as bit)
	FROM viewOrder_AllTickets O
	JOIN tblOrigin OO ON OO.ID = O.OriginID
	JOIN tblCustomer C ON C.ID = O.CustomerID
	JOIN tblDestination D ON D.ID = O.DestinationID
	-- max Gallons for this order
	OUTER APPLY (SELECT MaxGallons = cast(Value as decimal(18, 2)) FROM dbo.fnOrderOrderRules(O.ID) WHERE TypeID = 6) OORMG 
	-- ShipperPO_Required OrderRule
	OUTER APPLY (SELECT Value FROM dbo.fnOrderOrderRules(O.ID) WHERE TypeID = 2) OOR
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
	LEFT JOIN tblOrderSettlementShipper OIC ON OIC.OrderID = O.ID
	LEFT JOIN viewDriverLocation_OriginFirstArrive DLO ON DLO.OrderID = O.ID AND DLO.OriginID = O.OriginID
	LEFT JOIN viewDriverLocation_DestinationFirstArrive DLD ON DLD.OrderID = O.ID AND DLD.DestinationID = O.DestinationID
	WHERE OIC.BatchID IS NULL /* don't even show SETTLED orders on the AUDIT page ever */
	  AND nullif(OA.Approved, 0) IS NULL /* don't show Approved orders on the AUDIT page */
	  AND (isnull(@carrierID, 0) = -1 OR O.CarrierID = @carrierID)
	  AND (isnull(@driverID, 0) = -1 OR O.DriverID = @driverID)
	  AND ((nullif(@orderNum, 0) IS NULL AND O.DeleteDateUTC IS NULL AND (O.StatusID = 3 AND DeliverPrintStatusID IN (SELECT ID FROM tblPrintStatus WHERE IsCompleted = 1))) OR O.OrderNum = @orderNum)
	  AND (nullif(@id, 0) IS NULL OR O.ID = @id)
) X
GO

/**********************************************************
-- Author: Kevin Alons
-- Date Created: 2015/08/20 - 3.9.0
-- Purpose: auto-audit qualifying an order if it qualifies (NoError = 0)
-- Changes:
***********************************************************/
CREATE PROCEDURE spAutoAuditOrder(@id int, @UserName varchar(100), @Processed bit = NULL OUTPUT) AS
BEGIN
	SET @Processed = 0 -- default value
	IF EXISTS (SELECT * FROM fnOrders_AllTickets_Audit(-1, -1, NULL, @id) WHERE HasError = 0)
	BEGIN
		UPDATE tblOrder SET StatusID = 4, LastChangeDateUTC = GETUTCDATE(), LastChangedByUser = @UserName WHERE ID = @id
		SET @Processed = 1
	END
END
GO
GRANT EXECUTE ON spAutoAuditOrder TO role_iis_acct
GO

/* create new System-Wide setting value to turn on or off Auto-Audit feature */
INSERT INTO tblSetting (ID, Name, SettingTypeID, Value, Category, CreateDateUTC, CreatedByUser, ReadOnly)
	VALUES (54, 'Auto-Audit Qualified orders', 2, 'False', 'Order Rules', GETUTCDATE(), 'System', 0)
GO

/* mark any already settled orders as APPROVED (as they already exist) */
INSERT INTO tblOrderApproval (OrderID, Approved, CreateDateUTC, CreatedByUser)
	SELECT O.ID, 1
		, coalesce(OSS.CreateDateUTC, OSC.CreateDateUTC, O.LastChangeDateUTC), coalesce(OSS.CreatedByUser, OSC.CreatedByUser, O.LastChangedByUser)
	FROM tblOrder O
	LEFT JOIN tblOrderSettlementCarrier OSC ON OSC.OrderID = O.ID
	LEFT JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = O.ID
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
	WHERE O.StatusID IN (4)
	  AND (OSC.BatchID IS NOT NULL OR OSS.BatchID IS NOT NULL)
	  AND OA.OrderID IS NULL
GO

/* auto-process any orders that have been rated but not yet settled - which is the default behavior going forward */
DECLARE @ids TABLE (ID int, ToRate bit)
INSERT INTO @ids
	SELECT O.ID, CASE WHEN OSC.OrderID IS NULL OR OSS.OrderID IS NULL THEN 1 ELSE 0 END
	FROM tblOrder O
	JOIN tblOrderSettlementCarrier OSC ON OSC.OrderID = O.ID
	JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = O.ID
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
	WHERE O.StatusID = 4 AND OSC.BatchID IS NULL AND OSS.BatchID IS NULL AND OA.Approved is null

DECLARE @count int; SELECT @count = COUNT(*) FROM @ids
PRINT 'Attempting to Auto-Approve ' + ltrim(@count) + ' orders'

DECLARE @id int
SELECT @id = MIN(ID) FROM @ids
WHILE @id IS NOT NULL
BEGIN
	IF EXISTS (SELECT * FROM @ids WHERE ID = @id AND ToRate = 1)
		EXEC spApplyRatesBoth @id, 'System'
	EXEC spAutoApproveOrder @id, 'System'
	SET @id = (SELECT MIN(id) FROM @ids WHERE ID > @id)
END
GO

/* =============================================
-- Author:		Kevin Alons
-- Create date: 19 Dec 2012
-- Description:	trigger to 
				1) validate any changes, and fail the update if invalid changes are submitted
				2) add a unique, incrementing OrderNum to each new Order (manual Identity column)
				3) recompute wait times (origin|destination based on times provided)
				4) generate route table entry for any newly used origin-destination combination
				5) keep Order consistent with Origin.Producer|Operator|UOM & Dest.UOM then the Origin|Dest change
				6) roll the PrintStatus back to NOTFINALIZED (0) when status is rolled back
				7) when DriverID changes, mark those orders AS DELETED and clone the order with the new DriverID and associated records
				8) update any ticket quantities for open orders when the UOM is changed for the Origin
				9) update the Driver.Truck\Trailer\Trailer2 defaults & related DISPATCHED orders when driver updates them on an order
-REMOVED		10) update the Pickup/DeliverLastChangeDateUTC when Origin/Destination is changed (respectively)
				11) (re) apply Settlement Amounts to orders not yet fully settled when status is changed to DELIVERED
				12) if DBAudit is turned on, save an audit record for this Order change
-- Changes: 
	- 3.7.4 05/08/15 GSM Added Rack/Bay field to DBAudit logic
	- 3.7.7 - 15 May 2015 - KDA - REMOVE #10 above, instead update the LastChangeDateUTC whenever an Origin or Destination is changed
	- 3.7.11 - 18 May 2015 - KDA - generally use GETUTCDATE for all LastChangeDateUTC revisions (instead of related Order.LastChangeDateUTC, etc)
	- 3.7.12 - 5/19/2015 - KDA - update any existing OrderTicket records when the Order is DISPATCHED to a driver (so the Driver App will ALWAYS receive the existing TICKETS from the GAUGER)
	- 3.7.23 - 06/05/2015 - GSM - DCDRV-154: Ticket Type following Origin Change
	- 3.7.23 - 06/05/2015 - GSM - DCWEB-530 - ensure ASSIGNED orders with a driver defined are updated to DISPATCHED
	- 3.8.1  - 2015/07/04 - KDA - purge any Driver|Gauger App ZPL related SYNC change records when order is AUDITED
	- 3.9.0  - 2015/08/20 - KDA - add invocation of spAutoAuditOrder for DELIVERED orders (that qualify) when the Auto-Audit global setting value is TRUE
								- add invocation of spAutoApproveOrder for AUDITED orders (that qualify)
-- =============================================*/
ALTER TRIGGER trigOrder_IU ON tblOrder AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @changesFound bit
	
		-- Note: the TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU') ) = 1 statement prevents recursion
	IF (TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU')) = 1 AND TRIGGER_NESTLEVEL( OBJECT_ID('trigOrderTicket_IU')) = 0) BEGIN

		/**********  START OF VALIDATION SECTION ************************/

		IF (UPDATE(OrderNum) 
			OR UPDATE(PriorityID) 
			OR UPDATE(DueDate) 
			OR UPDATE(RouteID) 
			OR UPDATE(OriginID) 
			OR UPDATE(OriginArriveTimeUTC) 
			OR UPDATE(OriginDepartTimeUTC) 
			OR UPDATE(OriginMinutes) 
			OR UPDATE(OriginWaitNotes) 
			OR UPDATE(OriginBOLNum) 
			OR UPDATE(OriginGrossUnits) 
			OR UPDATE(OriginNetUnits) 
			OR UPDATE(DestinationID) 
			OR UPDATE(DestArriveTimeUTC) 
			OR UPDATE(DestDepartTimeUTC) 
			OR UPDATE(DestMinutes) 
			OR UPDATE(DestWaitNotes) 
			OR UPDATE(DestBOLNum) 
			OR UPDATE(DestGrossUnits) 
			OR UPDATE(DestNetUnits) 
			OR UPDATE(CustomerID) 
			OR UPDATE(CarrierID) 
			OR UPDATE(DriverID) 
			OR UPDATE(TruckID)
			OR UPDATE(TrailerID) 
			OR UPDATE(Trailer2ID) 
			OR UPDATE(OperatorID) 
			OR UPDATE(PumperID) 
			OR UPDATE(TicketTypeID) 
			OR UPDATE(Rejected) 
			OR UPDATE(RejectNotes) 
			OR UPDATE(ChainUp) 
			-- allow this to be changed even in audit status
			--OR UPDATE(OriginTruckMileage) 
			OR UPDATE(OriginTankNum) 
			-- allow this to be changed even in audit status
			--OR UPDATE(DestTruckMileage) 
			OR UPDATE(CarrierTicketNum) 
			OR UPDATE(AuditNotes) 
			OR UPDATE(CreateDateUTC) 
			-- it is permissible to change the ActualMiles value on Audited orders up to the point the Order has been Shipper-Settled
			OR (UPDATE(ActualMiles) AND EXISTS (SELECT * FROM deleted i JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = i.ID WHERE OSS.BatchID IS NOT NULL))
			OR UPDATE(ProducerID) 
			OR UPDATE(CreatedByUser) 
			OR UPDATE(LastChangeDateUTC) 
			OR UPDATE(LastChangedByUser) 
			OR UPDATE(DeleteDateUTC) 
			OR UPDATE(DeletedByUser) 
			OR UPDATE(DestProductBSW)
			OR UPDATE(DestProductGravity) 
			OR UPDATE(DestProductTemp) 
			OR UPDATE(ProductID) 
			OR UPDATE(AcceptLastChangeDateUTC) 
			OR UPDATE(PickupLastChangeDateUTC) 
			OR UPDATE(DeliverLastChangeDateUTC) 
			OR UPDATE(OriginUomID) 
			OR UPDATE(DestUomID) 
			OR UPDATE(PickupPrintStatusID) 
			OR UPDATE(DeliverPrintStatusID)
			OR UPDATE(PickupPrintDateUTC) 
			OR UPDATE(DeliverPrintDateUTC) 
			OR UPDATE(OriginTankID) 
			OR UPDATE(OriginGrossStdUnits) 
			OR UPDATE(DispatchConfirmNum) 
			OR UPDATE(DispatchNotes)
			OR UPDATE(PickupDriverNotes)
			OR UPDATE(DeliverDriverNotes)
			OR UPDATE(OriginWaitReasonID)
			OR UPDATE(DestWaitReasonID)
			OR UPDATE(RejectReasonID) 
			OR UPDATE(DestOpenMeterUnits) 
			OR UPDATE(DestCloseMeterUnits)
			OR UPDATE(DestRackBay)
		)
			--OR UPDATE(ReassignKey)) we don't do any work if this value changes so just ignore it
		BEGIN
			-- only allow the StatusID value to be changed on an audited order
			IF EXISTS (SELECT * FROM deleted WHERE StatusID = 4)
			BEGIN
				-- if the "Allow Audited Order Update (temp only)" setting is TRUE then only warn when this occurs
				IF (dbo.fnToBool(dbo.fnSettingValue(35)) = 1)
					PRINT 'AUDITED orders is being modified - please investigate why!'
				ELSE BEGIN
					-- otherwise (normal behavior) - prevent AUDITED orders from being modified (except to UN-AUDIT them)
					RAISERROR('AUDITED orders cannot be modified!', 16, 1)
					IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
					RETURN
				END
			END
		END
		ELSE IF NOT UPDATE(StatusID) -- NO CHANGES DETECTED SO JUST QUIT
			RETURN

		PRINT 'trigOrder_IU FIRED'
					
		-- ensure the Origin and Destinations are both specified unless the Status is:
		--   (Generated, Assigned, Dispatched or Declined)
		IF  EXISTS(SELECT * FROM inserted O WHERE (O.OriginID IS NULL OR O.DestinationID IS NULL) AND O.StatusID NOT IN (-10,1,2,9))
		BEGIN
			RAISERROR('Invalid Order Status when the Origin and/or Or Destination is not specified', 16, 1)
			IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
			RETURN
		END
		
		/**********  END OF VALIDATION SECTION ************************/

		WHILE (SELECT count(*) FROM inserted i JOIN tblOrder O ON O.ID = i.ID WHERE O.OrderNum IS NULL) > 0
		BEGIN
			UPDATE tblOrder 
			  SET OrderNum = isnull((SELECT max(OrderNum) FROM tblOrder), 100000) + 1
				, CreateDateUTC = getutcdate()
			WHERE ID = (SELECT min(O.ID) FROM tblOrder O JOIN inserted i ON i.ID = O.ID WHERE O.OrderNum IS NULL)
		END
		
		-- re-compute the OriginMinutes (in case the website failed to compute it properly)
		IF UPDATE(OriginArriveTimeUTC) OR UPDATE(OriginDepartTimeUTC)
		BEGIN
			UPDATE tblOrder SET OriginMinutes = datediff(minute, i.OriginArriveTimeUTC, i.OriginDepartTimeUTC)
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
		END
		-- re-compute the DestMinutes (in case the website failed to compute it properly)
		IF UPDATE(DestArriveTimeUTC) OR UPDATE(DestDepartTimeUTC)
		BEGIN
			UPDATE tblOrder SET DestMinutes = datediff(minute, i.DestArriveTimeUTC, i.DestDepartTimeUTC)
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
		END
		
		-- ensure missing Routes are created and assigned for the specified Origin/Destination combination
		IF UPDATE(OriginID) OR UPDATE(DestinationID)
		BEGIN
			-- create any missing Route records
			INSERT INTO tblRoute (OriginID, DestinationID, CreateDateUTC, CreatedByUser)
				SELECT i.OriginID, i.DestinationID, GETUTCDATE(), ISNULL(i.LastChangedByUser, i.CreatedByUser)
				FROM inserted i
				LEFT JOIN tblRoute R ON R.OriginID = i.OriginID AND R.DestinationID = i.DestinationID
				WHERE i.OriginID IS NOT NULL AND i.DestinationID IS NOT NULL AND R.ID IS NULL
			
			-- ensure the Order records refer to the correct Route (ID)
			UPDATE tblOrder SET RouteID = R.ID
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
			LEFT JOIN tblRoute R ON R.OriginID = O.OriginID AND R.DestinationID = O.DestinationID
			WHERE O.RouteID IS NULL OR O.RouteID <> R.ID
			
			-- update the ActualMiles from the related Route
			UPDATE tblOrder SET ActualMiles = R.ActualMiles
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
			JOIN tblRoute R ON R.ID = O.RouteID
			WHERE O.ActualMiles <> R.ActualMiles
		END
		
		IF (UPDATE(OriginID))
		BEGIN
			-- update Order.ProducerID/OperatorID/PumperID to match what is assigned to the new Origin
			UPDATE tblOrder 
				SET TicketTypeID = OO.TicketTypeID
					, ProducerID = OO.ProducerID
					, OperatorID = OO.OperatorID
					, PumperID = OO.PumperID
					, LastChangeDateUTC = GETUTCDATE() 
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			WHERE d.OriginID <> O.OriginID

			-- update Order.OriginUomID to match what is assigned to the new Origin (quantities are updated below)
			UPDATE tblOrder SET OriginUomID = OO.UomID
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			WHERE d.OriginID <> O.OriginID AND d.OriginUomID <> OO.UomID
		END
		
		-- keep the DestUomID in sync with the Destination (units are updated below)
		IF (UPDATE(DestinationID))
		BEGIN
			-- update Order.DestUomID to match what is assigned to the new Destination
			UPDATE tblOrder 
			  SET DestUomID = DD.UomID
				, LastChangeDateUTC = GETUTCDATE()
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID
			JOIN tblDestination DD ON DD.ID = O.DestinationID
			WHERE d.DestinationID <> O.DestinationID AND d.DestUomID <> DD.UomID
		END
		
		-- ensure that any set PrintStatusID values are reset to 0 when backing up the general StatusID value
		IF (UPDATE(StatusID))
		BEGIN
			UPDATE tblOrder 
			  SET DeliverPrintStatusID = 0 
				, LastChangeDateUTC = GETUTCDATE()
			  FROM tblOrder O
			  JOIN deleted d ON d.ID = O.ID
			WHERE O.StatusID <> d.StatusID AND O.StatusID IN (-10, 1, 2, 7, 8) -- Generated, Assigned, Dispatched, Accepted, Picked-Up

			UPDATE tblOrder 
			  SET PickupPrintStatusID = 0 
				, LastChangeDateUTC = GETUTCDATE()
			  FROM tblOrder O
			  JOIN deleted d ON d.ID = O.ID
			WHERE O.StatusID <> d.StatusID AND O.StatusID IN (-10, 1, 2, 7) -- Generated, Assigned, Dispatched, Accepted
			
			/* ensure that any orders that are DISPATCHED - any existing orders are TOUCHED so they are syncable to the Driver App */
			UPDATE tblOrderTicket
				SET LastChangeDateUTC = GETUTCDATE()
			WHERE OrderID IN (
				SELECT i.ID 
				FROM inserted i
				JOIN deleted d ON d.ID = i.ID
				WHERE i.StatusID <> d.StatusID AND i.StatusID IN (2) -- DISPATCHED
			)
	END

		-- just updating the tickets here, trigOrderTicket_IU will in turn update this table
		-- ensure any existing OrderTickets are recomputed with the new OriginUOM
		UPDATE tblOrderTicket
		  SET GrossUnits = dbo.fnConvertUOM(GrossUnits, d.OriginUomID, O.OriginUomID)
			, NetUnits = dbo.fnConvertUOM(NetUnits, d.OriginUomID, O.OriginUomID)
			, LastChangeDateUTC = GETUTCDATE()
		FROM tblOrderTicket OT
		JOIN tblOrder O ON O.ID = OT.OrderID
		JOIN deleted d ON d.ID = O.ID
		WHERE O.OriginUomID <> d.OriginUomID
		
		-- recalculate the Destination Unit values if only if the DestUomID changed but the values didn't
		UPDATE tblOrder
		  SET DestGrossUnits = dbo.fnConvertUOM(O.DestGrossUnits, d.DestUomID, O.DestUomID)
			, DestNetUnits = dbo.fnConvertUOM(O.DestNetUnits, d.DestUomID, O.DestUomID)
			, LastChangeDateUTC = GETUTCDATE()
		FROM tblOrder O
		JOIN deleted d ON d.ID = O.ID
		WHERE d.DestUomID <> O.DestUomID 
		  AND d.DestGrossUnits = O.DestGrossUnits
		  AND d.DestNetUnits = O.DestNetUnits

		/* DCWEB-530 - ensure any ASSIGNED orders with a DRIVER assigned is set to DISPATCHED */
		UPDATE tblOrder SET StatusID = 2 /*DISPATCHED*/
		FROM tblOrder O
		JOIN inserted i ON I.ID = O.ID
		WHERE i.StatusID = 1 /*ASSIGNED*/ AND i.CarrierID IS NOT NULL AND i.DriverID IS NOT NULL AND i.DeleteDateUTC IS NULL

		/*************************************************************************************************************/
		/* handle driver changes - "orphan" the order/ticket (with a new ORDERNUM) - and clone it for the new driver */
		IF (UPDATE(DriverID))
		BEGIN
			-- create cloned order/ticket records (these records will be cloned then the original reset to orphaned status)
			SELECT O.*, NewOrderNum = CAST(NULL as int)
			INTO #clone
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID AND d.DriverID IS NOT NULL AND d.DriverID <> isnull(O.DriverID, 0)

			/* generate the next OrderNum values for these new records */
			DECLARE @newOrderNum int; SET @newOrderNum = isnull((SELECT max(OrderNum) FROM tblOrder), 100000) + 1;
			DECLARE @newReassignKey int; SET @newReassignKey = isnull((SELECT max(ReassignKey) FROM tblOrder), 0) + 1;
			WHILE (SELECT count(*) FROM #clone WHERE NewOrderNum IS NULL) > 0
			BEGIN
				UPDATE #clone
				  SET NewOrderNum = @newOrderNum, ReassignKey = isnull(ReassignKey, @newReassignKey)
					, CreateDateUTC = getutcdate()
				WHERE ID = (SELECT min(ID) FROM #clone WHERE NewOrderNum IS NULL)
				-- if the @newReassignKey wasn't used above, then that # will be skipped, but this should not be an issue
				SELECT @newOrderNum = @newOrderNum + 1, @newReassignKey = @newReassignKey + 1
			END
			
			-- reset the existing records as orphaned
			UPDATE tblOrder
				SET DeleteDateUTC = isnull(O.DeleteDateUTC, GETUTCDATE()), DeletedByUser = isnull(O.DeletedByUser, O.LastChangedByUser)
					, CarrierID = d.CarrierID, DriverID = d.DriverID, TruckID = d.TruckID, TrailerID = d.TrailerID, Trailer2ID = d.Trailer2ID
					, DispatchConfirmNum = NULL
					, ReassignKey = C.ReassignKey
			FROM tblOrder O
			JOIN #clone C ON C.ID = O.ID
			JOIN deleted d on D.ID = O.id

			-- create the cloned records (which is essentially the original with a new ID and the Carrrier & default Truck|Trailers of the new driver)
			INSERT INTO tblOrder (OrderNum, StatusID, PriorityID, DueDate, RouteID, OriginID, OriginArriveTimeUTC, OriginDepartTimeUTC, OriginMinutes, OriginWaitNotes, OriginBOLNum, OriginGrossUnits, OriginNetUnits, DestinationID, DestArriveTimeUTC, DestDepartTimeUTC, DestMinutes, DestWaitNotes, DestBOLNum, DestGrossUnits, DestNetUnits, CustomerID, CarrierID, DriverID, TruckID, TrailerID, Trailer2ID, OperatorID, PumperID, TicketTypeID, Rejected, RejectNotes, ChainUp, OriginTruckMileage, OriginTankNum, DestTruckMileage, CarrierTicketNum, AuditNotes, CreateDateUTC, ActualMiles, ProducerID, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser, DestProductBSW, DestProductGravity, DestProductTemp, ProductID, AcceptLastChangeDateUTC, PickupLastChangeDateUTC, DeliverLastChangeDateUTC, OriginUomID, DestUomID, PickupPrintStatusID, DeliverPrintStatusID, PickupPrintDateUTC, DeliverPrintDateUTC, OriginTankID, OriginGrossStdUnits, DispatchNotes, PickupDriverNotes, DeliverDriverNotes, DispatchConfirmNum, OriginWaitReasonID, DestWaitReasonID, RejectReasonID, DestOpenMeterUnits, DestCloseMeterUnits, ReassignKey)
				SELECT NewOrderNum, StatusID, PriorityID, DueDate, RouteID, OriginID, OriginArriveTimeUTC, OriginDepartTimeUTC, OriginMinutes, OriginWaitNotes, OriginBOLNum, OriginGrossUnits, OriginNetUnits, DestinationID, DestArriveTimeUTC, DestDepartTimeUTC, DestMinutes, DestWaitNotes, DestBOLNum, DestGrossUnits, DestNetUnits, CustomerID, D.CarrierID, DriverID, D.TruckID, D.TrailerID, D.Trailer2ID, OperatorID, PumperID, TicketTypeID, Rejected, RejectNotes, ChainUp, OriginTruckMileage, OriginTankNum, DestTruckMileage, CarrierTicketNum, AuditNotes, O.CreateDateUTC, ActualMiles, ProducerID, O.CreatedByUser, GETUTCDATE(), O.LastChangedByUser, O.DeleteDateUTC, O.DeletedByUser, DestProductBSW, DestProductGravity, DestProductTemp, ProductID, AcceptLastChangeDateUTC, PickupLastChangeDateUTC, DeliverLastChangeDateUTC, OriginUomID, DestUomID, PickupPrintStatusID, DeliverPrintStatusID, PickupPrintDateUTC, DeliverPrintDateUTC, OriginTankID, OriginGrossStdUnits, DispatchNotes, PickupDriverNotes, DeliverDriverNotes, DispatchConfirmNum, OriginWaitReasonID, DestWaitReasonID, RejectReasonID, DestOpenMeterUnits, DestCloseMeterUnits, ReassignKey
				FROM #clone O
				LEFT JOIN tblDriver D ON D.ID = O.DriverID
		
			-- cache the affected tickets
			SELECT OT.*, C.NewOrderNum, OrderDeletedByUser = C.LastChangedByUser
			INTO #cloneTicket
			FROM tblOrderTicket OT
			JOIN #clone C ON C.ID = OT.OrderID
			WHERE OT.DeleteDateUTC IS NULL
			
			-- create the cloned Order Ticket records (copy of existing ticket(s) pointed at new "original" Order)
			INSERT INTO tblOrderTicket (OrderID, CarrierTicketNum, TicketTypeID, TankNum, ProductObsGravity, ProductObsTemp, ProductBSW, OpeningGaugeFeet, OpeningGaugeInch, OpeningGaugeQ, ClosingGaugeFeet, ClosingGaugeInch, ClosingGaugeQ
					, GrossUnits, NetUnits, Rejected, RejectNotes, SealOff, SealOn, BOLNum, ProductHighTemp, ProductLowTemp, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser
					, UID, FromMobileApp, OriginTankID, BottomFeet, BottomInches, BottomQ, GrossStdUnits, RejectReasonID, MeterFactor, OpenMeterUnits, CloseMeterUnits)
				SELECT O.ID, CT.CarrierTicketNum, CT.TicketTypeID, CT.TankNum, CT.ProductObsGravity, CT.ProductObsTemp, CT.ProductBSW, CT.OpeningGaugeFeet, CT.OpeningGaugeInch, CT.OpeningGaugeQ, CT.ClosingGaugeFeet, CT.ClosingGaugeInch, CT.ClosingGaugeQ
					, CT.GrossUnits, CT.NetUnits, CT.Rejected, CT.RejectNotes, CT.SealOff, CT.SealOn, CT.BOLNum, CT.ProductHighTemp, CT.ProductLowTemp, CT.CreateDateUTC, CT.CreatedByUser, GETUTCDATE(), CT.LastChangedByUser, CT.DeleteDateUTC, CT.DeletedByUser
					, newid(), CT.FromMobileApp, CT.OriginTankID, CT.BottomFeet, CT.BottomInches, CT.BottomQ, CT.GrossStdUnits, CT.RejectReasonID, CT.MeterFactor, CT.OpenMeterUnits, CT.CloseMeterUnits
				FROM #cloneTicket CT
				JOIN tblOrder O ON O.OrderNum = CT.NewOrderNum
		END
		/*************************************************************************************************************/
		
		/* HANDLE TRUCK | TRAILER | TRAILER2 default changes when the driver updates his Truck | Trailer | Trailer2 on ACCEPTANCE */
		-- TRUCK
		IF (UPDATE(TruckID) AND EXISTS (
			SELECT i.* 
			FROM inserted i 
			JOIN deleted d ON d.ID = i.ID
			  AND i.DriverID = d.DriverID 
			  AND i.TruckID <> d.TruckID
			WHERE i.StatusID IN (7, 8, 3) -- ACCEPTED, PICKEDUP, DELIVERED
			  AND d.StatusID IN (2))) -- DISPATCHED
		BEGIN
			UPDATE tblDriver
			  SET TruckID = i.TruckID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblDriver DR
			JOIN inserted i ON i.DriverID = DR.ID
			JOIN deleted d ON d.DriverID = DR.ID
			WHERE i.TruckID <> d.TruckID
			
			UPDATE tblOrder
			  SET TruckID = i.TruckID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblOrder O
			JOIN inserted i ON i.DriverID = O.DriverID 
			  AND O.ID <> i.ID
			  AND O.StatusID IN (2) -- DISPATCHED
			  AND i.TruckID <> O.TruckID
			  AND O.DeleteDateUTC IS NULL
		END
		-- TRAILER
		IF (UPDATE(TrailerID) AND EXISTS (
			SELECT i.* 
			FROM inserted i 
			JOIN deleted d ON d.ID = i.ID
			  AND i.DriverID = d.DriverID 
			  AND i.TrailerID <> d.TrailerID
			WHERE i.StatusID IN (7, 8, 3) -- ACCEPTED, PICKEDUP, DELIVERED
			  AND d.StatusID IN (2))) -- DISPATCHED
		BEGIN
			UPDATE tblDriver
			  SET TrailerID = i.TrailerID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblDriver DR
			JOIN inserted i ON i.DriverID = DR.ID
			JOIN deleted d ON d.DriverID = DR.ID
			WHERE i.TrailerID <> d.TrailerID
			
			UPDATE tblOrder
			  SET TrailerID = i.TrailerID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblOrder O
			JOIN inserted i ON i.DriverID = O.DriverID 
			  AND O.ID <> i.ID
			  AND O.StatusID IN (2) -- DISPATCHED
			  AND i.TrailerID <> O.TrailerID
			  AND O.DeleteDateUTC IS NULL
		END
		-- TRAILER 2
		IF (UPDATE(Trailer2ID) AND EXISTS (
			SELECT i.* 
			FROM inserted i 
			JOIN deleted d ON d.ID = i.ID
			  AND i.DriverID = d.DriverID 
			  AND isnull(i.Trailer2ID, 0) <> isnull(d.Trailer2ID, 0)
			WHERE i.StatusID IN (7, 8, 3) -- ACCEPTED, PICKEDUP, DELIVERED
			  AND d.StatusID IN (2))) -- DISPATCHED
		BEGIN
			UPDATE tblDriver
			  SET TrailerID = i.TrailerID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblDriver DR
			JOIN inserted i ON i.DriverID = DR.ID
			JOIN deleted d ON d.DriverID = DR.ID
			WHERE isnull(i.Trailer2ID, 0) <> isnull(d.Trailer2ID, 0)
			
			UPDATE tblOrder
			  SET Trailer2ID = i.Trailer2ID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblOrder O
			JOIN inserted i ON i.DriverID = O.DriverID 
			  AND O.ID <> i.ID
			  AND O.StatusID IN (2) -- DISPATCHED
			  AND isnull(i.Trailer2ID, 0) <> isnull(O.Trailer2ID, 0)
			  AND O.DeleteDateUTC IS NULL
		END
		/*************************************************************************************************************/
	
		DECLARE @deliveredIDs TABLE (ID int)
		DECLARE @auditedIDs TABLE (ID int)
		DECLARE @id int
		DECLARE @autoAudit bit; SET @autoAudit = dbo.fnToBool(dbo.fnSettingValue(54))
		DECLARE @toAudit bit
		-- Auto-Audit/Apply Settlement Rates/Amounts/Auto-Approve to Order when STATUS is changed to DELIVERED (until it is FINAL settled)
		IF (UPDATE(StatusID) OR UPDATE(DeliverPrintStatusID))
		BEGIN
			INSERT INTO @deliveredIDs (ID)
				SELECT i.ID 
				FROM inserted i
				JOIN tblPrintStatus iPS ON iPS.ID = i.DeliverPrintStatusID
				JOIN deleted d ON d.ID = i.ID
				JOIN tblPrintStatus dPS ON dPS.ID = d.DeliverPrintStatusID
				WHERE i.StatusID = 3 AND iPS.IsCompleted = 1 AND i.StatusID + iPS.IsCompleted <> d.StatusID + dPS.IsCompleted
			
			SELECT @id = MIN(ID) FROM @deliveredIDs
			WHILE @id IS NOT NULL
			BEGIN
				-- attempt to AUTO-AUDIT the order if this feature is enabled (global setting)
				IF (@autoAudit = 1)
				BEGIN
					EXEC spAutoAuditOrder @ID, 'System', @toAudit OUTPUT
					/* if the order was updated to AUDITED status, then we need to attempt also to auto-approve [below] */
					IF (@toAudit = 1)
						INSERT INTO @auditedIDs VALUES (@id)
				END
				-- attempt to apply rates to all newly DELIVERED orders
				EXEC spApplyRatesBoth @id, 'System' 
				SET @id = (SELECT MIN(id) FROM @deliveredIDs WHERE ID > @id)
			END
		END

		-- Auto-Approve any un-approved orders when STATUS changed to AUDIT
		IF (UPDATE(StatusID) OR EXISTS (SELECT * FROM @auditedIDs))
		BEGIN
			INSERT INTO @auditedIDs (ID)
				SELECT i.ID 
				FROM inserted i
				JOIN deleted d ON d.ID = i.ID
				LEFT JOIN tblOrderApproval OA ON OA.OrderID = i.ID
				LEFT JOIN @auditedIDs AID ON AID.ID = i.ID
				WHERE i.StatusID = 4 AND d.StatusID <> 4 AND OA.OrderID IS NULL AND AID.ID IS NULL

			SELECT @id = MIN(ID) FROM @auditedIDs
			WHILE @id IS NOT NULL
			BEGIN
				EXEC spAutoApproveOrder @id, 'System'
				SET @id = (SELECT MIN(id) FROM @auditedIDs WHERE ID > @id)
			END
			
		END 
		
		-- purge any DriverApp/Gauger App sync records for any orders that are not in AUDITED status
		IF UPDATE(StatusID) AND EXISTS (SELECT ID FROM inserted WHERE StatusID IN (4))
		BEGIN
			-- DRIVER APP records
			DELETE FROM tblDriverAppPrintHeaderImageSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID IN (4))
			DELETE FROM tblDriverAppPrintPickupTemplateSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID IN (4))
			DELETE FROM tblDriverAppPrintTicketTemplateSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID IN (4))
			DELETE FROM tblDriverAppPrintDeliverTemplateSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID IN (4))
			-- GAUGER APP records
			DELETE FROM tblGaugerAppPrintHeaderImageSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID IN (4))
			DELETE FROM tblGaugerAppPrintPickupTemplateSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID IN (4))
			DELETE FROM tblGaugerAppPrintTicketTemplateSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID IN (4))
		END
				
		--NOTE: we do not update the DestOpenMeterUnits/DestCloseMeterUnits since they don't auto update
		--  (and they simply represent a reading that is assumed to in the DestUomID UOM at entry)
		--  (if they are updated in the future [manually] the will calculate again using the DestUomID at that time)

		IF EXISTS (SELECT * FROM inserted EXCEPT SELECT * FROM deleted)
		BEGIN
			-- optionally add tblOrderDBAudit records
			BEGIN TRY
				IF EXISTS(SELECT * FROM tblSetting WHERE ID = 32 AND (Value LIKE 'true' OR Value LIKE 'yes'))
					INSERT INTO tblOrderDbAudit (DBAuditDate, ID, OrderNum, StatusID, PriorityID, DueDate, RouteID, OriginID, OriginArriveTimeUTC, OriginDepartTimeUTC, OriginMinutes, OriginWaitNotes, OriginBOLNum, OriginGrossUnits, OriginNetUnits, DestinationID, DestArriveTimeUTC, DestDepartTimeUTC, DestMinutes, DestWaitNotes, DestBOLNum, DestGrossUnits, DestNetUnits, CustomerID, CarrierID, DriverID, TruckID, TrailerID, Trailer2ID, OperatorID, PumperID, TicketTypeID, Rejected, RejectNotes, ChainUp, OriginTruckMileage, OriginTankNum, DestTruckMileage, CarrierTicketNum, AuditNotes, CreateDateUTC, ActualMiles, ProducerID, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser, DestProductBSW, DestProductGravity, DestProductTemp, ProductID, AcceptLastChangeDateUTC, PickupLastChangeDateUTC, DeliverLastChangeDateUTC, OriginUomID, DestUomID, PickupPrintStatusID, DeliverPrintStatusID, PickupPrintDateUTC, DeliverPrintDateUTC, OriginTankID, OriginGrossStdUnits, DispatchNotes, PickupDriverNotes, DeliverDriverNotes, DispatchConfirmNum, OriginWaitReasonID, DestWaitReasonID, RejectReasonID, DestOpenMeterUnits, DestCloseMeterUnits, ReassignKey, DestRackBay)
						SELECT GETUTCDATE(), ID, OrderNum, StatusID, PriorityID, DueDate, RouteID, OriginID, OriginArriveTimeUTC, OriginDepartTimeUTC, OriginMinutes, OriginWaitNotes, OriginBOLNum, OriginGrossUnits, OriginNetUnits, DestinationID, DestArriveTimeUTC, DestDepartTimeUTC, DestMinutes, DestWaitNotes, DestBOLNum, DestGrossUnits, DestNetUnits, CustomerID, CarrierID, DriverID, TruckID, TrailerID, Trailer2ID, OperatorID, PumperID, TicketTypeID, Rejected, RejectNotes, ChainUp, OriginTruckMileage, OriginTankNum, DestTruckMileage, CarrierTicketNum, AuditNotes, CreateDateUTC, ActualMiles, ProducerID, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser, DestProductBSW, DestProductGravity, DestProductTemp, ProductID, AcceptLastChangeDateUTC, PickupLastChangeDateUTC, DeliverLastChangeDateUTC, OriginUomID, DestUomID, PickupPrintStatusID, DeliverPrintStatusID, PickupPrintDateUTC, DeliverPrintDateUTC, OriginTankID, OriginGrossStdUnits, DispatchNotes, PickupDriverNotes, DeliverDriverNotes, DispatchConfirmNum, OriginWaitReasonID, DestWaitReasonID, RejectReasonID, DestOpenMeterUnits, DestCloseMeterUnits, ReassignKey, DestRackBay
						FROM deleted d
			END TRY
			BEGIN CATCH
				PRINT 'trigOrder_IU.DBAUDIT FAILURE: ' + ERROR_MESSAGE()
			END CATCH
		END
		
	PRINT 'trigOrder_IU COMPLETE'

	END
	
END
GO
EXEC sp_settriggerorder @triggername=N'[trigOrder_IU]', @order=N'First', @stmttype=N'INSERT'
GO
EXEC sp_settriggerorder @triggername=N'[trigOrder_IU]', @order=N'First', @stmttype=N'UPDATE'
GO

EXEC _spRebuildAllObjects
GO

/* cleanup comments from past checkin */
GO
/***********************************
Date Created: 9 Mar 2013
Author: Kevin Alons
Purpose: return Order JOIN OrderTicket + computed Reroute details into a single view
Special: 
	1) all TICKET fields should be preceded with T_ to ensure they are unique and to explicitly identify them as TICKET (not ORDER) level values
Changes:
	- 3.7.16 - 2015/05/22 - KDA - added T_UID field
	- 3.8.15 - 2015/08/15 -  BB - added T_TankID field
***********************************/
ALTER VIEW viewOrder_OrderTicket_Full AS 
	SELECT OE.* 
        , T_UID =  OT.UID
		, T_ID = OT.ID
		, T_TicketType = CASE WHEN OE.TicketCount = 0 THEN OE.TicketType ELSE OT.TicketType END 
		, T_CarrierTicketNum = CASE WHEN OE.TicketCount = 0 THEN ltrim(OE.OrderNum) + CASE WHEN OE.Rejected = 1 THEN 'X' ELSE '' END ELSE OT.CarrierTicketNum END 
		, T_BOLNum = CASE WHEN OE.TicketCount = 0 THEN OE.OriginBOLNum ELSE OT.BOLNum END 
		, T_TankNum = isnull(OT.OriginTankText, OE.OriginTankText)
		, T_TankID = OT.OriginTankID
		, T_IsStrappedTank = OT.IsStrappedTank 
		, T_BottomFeet = OT.BottomFeet
		, T_BottomInches = OT.BottomInches
		, T_BottomQ = OT.BottomQ 
		, T_OpeningGaugeFeet = OT.OpeningGaugeFeet 
		, T_OpeningGaugeInch = OT.OpeningGaugeInch 
		, T_OpeningGaugeQ = OT.OpeningGaugeQ 
		, T_ClosingGaugeFeet = OT.ClosingGaugeFeet 
		, T_ClosingGaugeInch = OT.ClosingGaugeInch 
		, T_ClosingGaugeQ = OT.ClosingGaugeQ 
		, T_OpenTotalQ = dbo.fnGaugeQtrInches(OT.OpeningGaugeFeet, OT.OpeningGaugeInch, OT.OpeningGaugeQ) 
		, T_CloseTotalQ = dbo.fnGaugeQtrInches(OT.ClosingGaugeFeet, OT.ClosingGaugeInch, ot.ClosingGaugeQ) 
		, T_OpenReading = ltrim(OT.OpeningGaugeFeet) + 'ft ' + ltrim(OT.OpeningGaugeInch) + 'in ' + ltrim(OT.OpeningGaugeQ) + 'q' 
		, T_CloseReading = ltrim(OT.ClosingGaugeFeet) + 'ft ' + ltrim(OT.ClosingGaugeInch) + 'in ' + ltrim(OT.ClosingGaugeQ) + 'q' 
		, T_CorrectedAPIGravity = round(cast(OT.Gravity60F as decimal(9,4)), 9, 4) 
		, T_GrossStdUnits = OT.GrossStdUnits
		, T_SealOff = ltrim(OT.SealOff) 
		, T_SealOn = ltrim(OT.SealOn) 
		, T_HighTemp = OT.ProductHighTemp
		, T_LowTemp = OT.ProductLowTemp
		, T_ProductObsTemp = OT.ProductObsTemp 
		, T_ProductObsGravity = OT.ProductObsGravity 
		, T_ProductBSW = OT.ProductBSW 
		, T_Rejected = isnull(OT.Rejected, OE.Rejected)
		, T_RejectReasonID = OT.RejectReasonID
		, T_RejectNum = OT.RejectNum
		, T_RejectDesc = OT.RejectDesc
		, T_RejectNumDesc = OT.RejectNumDesc
		, T_RejectNotes = OT.RejectNotes 
		, T_GrossUnits = OT.GrossUnits 
		, T_NetUnits = OT.NetUnits 
		, T_MeterFactor = OT.MeterFactor
		, T_OpenMeterUnits = OT.OpenMeterUnits
		, T_CloseMeterUnits = OT.CloseMeterUnits
		, T_DispatchConfirmNum = CASE WHEN OE.TicketCount = 0 THEN OE.DispatchConfirmNum ELSE isnull(OT.DispatchConfirmNum, OE.DispatchConFirmNum) END
		, PreviousDestinations = dbo.fnRerouteDetails(OE.ID, 'PreviousDestinations', '<br/>') 
		, RerouteUsers = dbo.fnRerouteDetails(OE.ID, 'RerouteUsers', '<br/>') 
		, RerouteDates = dbo.fnRerouteDetails(OE.ID, 'RerouteDates', '<br/>') 
		, RerouteNotes = dbo.fnRerouteDetails(OE.ID, 'RerouteNotes', '<br/>')
	FROM dbo.viewOrderLocalDates OE
	LEFT JOIN viewOrderTicket OT ON OT.OrderID = OE.ID AND OT.DeleteDateUTC IS NULL
	WHERE OE.DeleteDateUTC IS NULL
GO

/*****************************************
-- Date Created: ??
-- Author: Kevin Alons, Modified by Geoff Mochau
-- Purpose: specialized, db-level logic enforcing business rules, audit changes, etc
-- Changes:
    - 3.8.12 - 2015/08/04 - GSM - use the revised (3-temperature) API function
	- 3.8.10 - 2015/07/26 - KDA - always update the associated tblOrder.LastChangeDateUTC to NOW when a Ticket is changed)
	- 3.8.15 - 2015/08/20 - BB  - include TicketTypeID = 7 when updating tblOrder.OriginTankID from first associated OrderTicket
*****************************************/
ALTER TRIGGER trigOrderTicket_IU ON tblOrderTicket AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	-- Note: the TRIGGER_NESTLEVEL( OBJECT_ID('trigOrderTicket_IU') ) = 1 statement prevents recursion
	IF (TRIGGER_NESTLEVEL( OBJECT_ID('trigOrderTicket_IU')) = 1) BEGIN
		
		-- only do anything if something actually changed
		IF EXISTS (SELECT * FROM inserted EXCEPT SELECT * FROM deleted)
		BEGIN
			PRINT 'trigOrderTicket_IU FIRED'
			
			/**********  START OF VALIDATION SECTION ************************/
			DECLARE @errorString varchar(1000); SET @errorString = ''

			IF EXISTS (
				SELECT * 
				FROM tblOrder 
				WHERE ID IN (SELECT OrderID FROM inserted) 
				  -- prevent changes to any tickets belonging to a Delivered or Audited Order
				  AND StatusID IN (4))
			BEGIN
				IF (dbo.fnToBool(dbo.fnSettingValue(35)) = 1)
					PRINT 'Ticket for an AUDITED orders is being modified - please investigate why!'
				ELSE 				
					SET @errorString = @errorString + '|Tickets of Audited Orders cannot be modified'
			END

			-- prevent reassigning a ticket from one order to another
			IF EXISTS (
				SELECT i.* 
				FROM inserted i
				JOIN deleted d ON i.UID = d.UID
				WHERE i.OrderID <> d.OrderID)
			BEGIN
				SET @errorString = @errorString + '|Tickets cannot be moved to a different Order'
			END
			
			IF EXISTS (
				SELECT OT.OrderID
				FROM tblOrderTicket OT
				JOIN tblOrder O ON O.ID = OT.OrderID
				JOIN inserted i ON i.OrderID = OT.OrderID AND i.CarrierTicketNum = OT.CarrierTicketNum AND i.ID <> OT.ID
				WHERE OT.DeleteDateUTC IS NULL and I.DeleteDateUTC IS NULL
				GROUP BY OT.OrderID, OT.CarrierTicketNum
				HAVING COUNT(*) > 1
			)
			BEGIN
				SET @errorString = @errorString + '|Duplicate Ticket Numbers are not allowed'
			END
			
			-- store all the tickets for orders that are not in Generated status and not deleted (only these need validation)
			SELECT i.*, O.StatusID 
			INTO #i
			FROM inserted i
			JOIN tblOrder O ON O.ID = i.OrderID
			WHERE i.DeleteDateUTC IS NULL
			
			/* -- removed with version 3.7.5 
			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (3) AND BOLNum IS NULL)
			BEGIN
				SET @errorString = @errorString + '|BOL # value is required for Meter Run tickets'
			END
			*/
			
			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (1, 2) AND OriginTankID IS NULL AND TankNum IS NULL AND Rejected = 0)
			BEGIN
				SET @errorString = @errorString + '|Tank is required for Gauge Run & Net Volume tickets'
			END

			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (1) AND CarrierTicketNum IS NULL)
			BEGIN
				SET @errorString = @errorString + '|Ticket # value is required for Gauge Run tickets'
			END

			/* require all parameters for orders NOT IN (GAUGER, GENERATED, ASSIGNED, DISPATCHED) */
			IF  EXISTS  (SELECT  ID FROM #i WHERE  Rejected =  0
				AND  ((TicketTypeID IN  (1, 2)  AND  (ProductObsTemp IS  NULL  OR  ProductObsGravity IS  NULL  OR  ProductBSW IS  NULL))
					OR  (TicketTypeID IN  (1)  AND  StatusID NOT  IN  (-9, -10, 1, 2)  AND  (ProductHighTemp IS  NULL  OR  ProductLowTemp IS  NULL)))
				)

			BEGIN
				SET @errorString = @errorString + '|All Product Measurement values are required for Gauge Run tickets'
			END
			
			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (1) AND Rejected = 0 
				AND (OpeningGaugeFeet IS NULL OR OpeningGaugeInch IS NULL OR OpeningGaugeQ IS NULL))
			BEGIN
				SET @errorString = @errorString + '|All Opening Gauge values are required for Gauge Run tickets'
			END
			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (1) AND Rejected = 0 AND StatusID NOT IN (-9, -10, 1, 2)
				AND (ClosingGaugeFeet IS NULL OR ClosingGaugeInch IS NULL OR ClosingGaugeQ IS NULL))
			BEGIN
				SET @errorString = @errorString + '|All Closing Gauge values are required for Gauge Run tickets'
			END

			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (2) AND Rejected = 0 AND StatusID NOT IN (-9, -10, 1, 2) AND (GrossUnits IS NULL))
			BEGIN
				SET @errorString = @errorString + '|Gross Volume value is required for Net Volume tickets'
			END

			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (7) AND Rejected = 0 AND StatusID NOT IN (-9, -10, 1, 2) AND (GrossUnits IS NULL))
			BEGIN
				SET @errorString = @errorString + '|Gross Volume value is required for Gauge Net tickets'
			END

			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (3) AND Rejected = 0 AND StatusID NOT IN (-9, -10, 1, 2) AND (GrossUnits IS NULL OR NetUnits IS NULL))
			BEGIN
				SET @errorString = @errorString + '|Gross & Net Volume values are required for Meter Run tickets'
			END

			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (1) AND Rejected = 0 AND StatusID NOT IN (-9, -10, 1, 2) AND (SealOff IS NULL OR SealOn IS NULL))
			BEGIN
				SET @errorString = @errorString + '|All Seal Off & Seal On values are required for Gauge Run tickets'
			END

			-- if any errors were detected, cancel the entire operation (transaction) and return a LINEFEED-deliminated list of errors
			IF (len(@errorString) > 0)
			BEGIN
				SET @errorString = replace(substring(@errorString, 2, 1000), '|', char(13) + char(10))
				RAISERROR(@errorString, 16, 1)
				IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
				RETURN
			END
			/**********  END OF VALIDATION SECTION ************************/
			
			-- re-compute GaugeRun ticket Gross barrels
			IF UPDATE(OriginTankID) OR UPDATE(OpeningGaugeFeet) OR UPDATE(OpeningGaugeInch) OR UPDATE(OpeningGaugeQ)
				OR UPDATE (ClosingGaugeFeet) OR UPDATE(ClosingGaugeInch) OR UPDATE(ClosingGaugeQ) 
				OR UPDATE (GrossUnits)
			BEGIN
				UPDATE tblOrderTicket
				  SET GrossUnits = dbo.fnConvertUom(dbo.fnOriginTankStrappingLevelDeltaBarrels(
						OT.OriginTankID
					  , OpeningGaugeFeet
					  , OpeningGaugeInch
					  , OpeningGaugeQ
					  , ClosingGaugeFeet
					  , ClosingGaugeInch
					  , ClosingGaugeQ), 1, O.OriginUomID) -- ensure this value is always normalized from BARRELS
				FROM tblOrderTicket OT
				JOIN tblOrder O ON O.ID = OT.OrderID
				WHERE OT.ID IN (SELECT ID FROM inserted) 
				  AND OT.TicketTypeID = 1 -- Gauge Run Ticket
				  AND OT.OpeningGaugeFeet IS NOT NULL
				  AND OT.OpeningGaugeInch IS NOT NULL
				  AND OT.OpeningGaugeQ IS NOT NULL
				  AND OT.ClosingGaugeFeet IS NOT NULL
				  AND OT.ClosingGaugeInch IS NOT NULL
				  AND OT.ClosingGaugeQ IS NOT NULL
			END
			-- recompute the GrossUnits of any changed Meter Run tickets
			IF UPDATE(OpenMeterUnits) OR UPDATE(CloseMeterUnits) BEGIN
				--Print 'updating tblOrderTicket GOV from Open/Close Meter Units'
				UPDATE tblOrderTicket 
					SET GrossUnits = CloseMeterUnits - OpenMeterUnits
				WHERE TicketTypeID = 3  -- meter run tickets ONLY
				  AND OpenMeterUnits IS NOT NULL AND CloseMeterUnits IS NOT NULL
				
			END
			-- re-compute GaugeRun | Net Barrel | Meter Run tickets NetUnits
			IF UPDATE(OpeningGaugeFeet) OR UPDATE(OpeningGaugeInch) OR UPDATE(OpeningGaugeQ)
				OR UPDATE (ClosingGaugeFeet) OR UPDATE(ClosingGaugeInch) OR UPDATE(ClosingGaugeQ) 
				OR UPDATE(OriginTankID) OR UPDATE(GrossUnits) OR UPDATE(GrossStdUnits) OR UPDATE(NetUnits)
				OR UPDATE(ProductObsTemp) OR UPDATE(ProductObsGravity) OR UPDATE(ProductBSW)
				OR UPDATE(OpenMeterUnits) OR UPDATE(CloseMeterUnits)
				OR UPDATE(ProductHighTemp) OR UPDATE(ProductLowTemp)
			BEGIN
				--PRINT 'updating tblOrderTicket NSV/GSV'
				UPDATE tblOrderTicket
				  SET NetUnits = dbo.fnCrudeNetCalculator3T(GrossUnits, ProductObsTemp, ProductHighTemp, ProductLowTemp, ProductObsGravity, isnull(ProductBSW, 0))
					, GrossStdUnits = dbo.fnCrudeNetCalculator3T(GrossUnits, ProductObsTemp, ProductHighTemp, ProductLowTemp, ProductObsGravity, 0)
				WHERE ID IN (SELECT ID FROM inserted)
				  AND TicketTypeID IN (1, 2, 7) -- GAUGE RUN, NET VOLUME, GAUGE NET
				  AND GrossUnits IS NOT NULL
				  AND ProductHighTemp IS NOT NULL
				  AND ProductLowTemp IS NOT NULL
				  AND ProductObsTemp IS NOT NULL
				  AND ProductObsGravity IS NOT NULL
			END
			
			-- ensure the Order record is in-sync with the Tickets
			/* test lines below
			declare @gov decimal(9,3); SELECT TOP 1 @gov = OT.GrossUnits FROM tblOrderTicket OT JOIN inserted i ON i.ID = OT.ID;
			PRINT 'updating tblOrder GOV/GSV/NSV GOV = ' + ltrim(@gov)
			--*/
			UPDATE tblOrder 
			SET OriginGrossUnits = (SELECT sum(GrossUnits) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND OT.Rejected = 0 AND OT.DeleteDateUTC IS NULL)
			  , OriginNetUnits = (SELECT sum(NetUnits) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND OT.Rejected = 0 AND OT.DeleteDateUTC IS NULL)
			  , OriginGrossStdUnits = (SELECT sum(GrossStdUnits) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND OT.Rejected = 0 AND OT.DeleteDateUTC IS NULL)
				-- use the first MeterRun/BasicRun BOLNum as the Order BOL Num (favor the first un-Rejected)
			  , OriginBOLNum = (
					SELECT TOP 1 BOLNum FROM (
						SELECT TOP 1 BOLNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID NOT IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
						UNION SELECT TOP 1 BOLNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID NOT IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X)
				-- use the first GaugeRun|NetVolume CarrierTicketNum as the Order.CarrierTicketNum (favor first un-Rejected)
			  , CarrierTicketNum = (
					SELECT TOP 1 CarrierTicketNum FROM (
						SELECT TOP 1 CarrierTicketNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
						UNION SELECT TOP 1 CarrierTicketNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X)
				-- use the first GaugeRun|NetVolume OriginTankID for the Order (favor first un-Rejected ticket)
				-- BB included ticket additional ticket type (Gauge Net-7)
			  , OriginTankID = (
					SELECT TOP 1 OriginTankID FROM (
						SELECT TOP 1 OriginTankID FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2,7) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
						UNION SELECT TOP 1 OriginTankID FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2,7) AND DeleteDateUTC IS NULL ORDER BY ID) X)
				-- use the first GaugeRun|NetVolume OriginTankNum for the Order (favor first un-Rejected ticket)
			  , OriginTankNum = (
					SELECT TOP 1 OriginTankNum FROM (
						SELECT TOP 1 OriginTankNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
						UNION SELECT TOP 1 OriginTankNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X)
			  , LastChangeDateUTC = GETUTCDATE() -- 3.8.10 update
			  , LastChangedByUser = (SELECT MIN(isnull(LastChangedByUser, CreatedByUser)) FROM inserted WHERE OrderID = O.ID)
			FROM tblOrder O
			WHERE ID IN (SELECT DISTINCT OrderID FROM inserted)

			/* START DB AUDIT *********************************************************/
			BEGIN TRY
				IF EXISTS(SELECT * FROM tblSetting WHERE ID = 32 AND (Value LIKE 'true' OR Value LIKE 'yes'))
					INSERT INTO tblOrderTicketDbAudit (DBAuditDate, ID, OrderID, CarrierTicketNum, TicketTypeID, TankNum, ProductObsGravity, ProductObsTemp, OpeningGaugeFeet, OpeningGaugeInch, OpeningGaugeQ, ClosingGaugeFeet, ClosingGaugeInch, ClosingGaugeQ, GrossUnits, NetUnits, Rejected, RejectNotes, SealOff, SealOn, BOLNum, ProductHighTemp, ProductLowTemp, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser, UID, FromMobileApp, OriginTankID, GrossStdUnits, BottomFeet, BottomInches, BottomQ, RejectReasonID, MeterFactor, OpenMeterUnits, CloseMeterUnits)
						SELECT GETUTCDATE(), ID, OrderID, CarrierTicketNum, TicketTypeID, TankNum, ProductObsGravity, ProductObsTemp, OpeningGaugeFeet, OpeningGaugeInch, OpeningGaugeQ, ClosingGaugeFeet, ClosingGaugeInch, ClosingGaugeQ, GrossUnits, NetUnits, Rejected, RejectNotes, SealOff, SealOn, BOLNum, ProductHighTemp, ProductLowTemp, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser, UID, FromMobileApp, OriginTankID, GrossStdUnits, BottomFeet, BottomInches, BottomQ, RejectReasonID, MeterFactor, OpenMeterUnits, CloseMeterUnits
						FROM deleted d
			END TRY
			BEGIN CATCH
				PRINT 'trigOrderTicket_IU.DBAUDIT FAILURE: ' + ERROR_MESSAGE()
			END CATCH
			/* END DB AUDIT *********************************************************/

			PRINT 'trigOrderTicket_IU COMPLETE'
		END
	END	
END
GO
EXEC sp_settriggerorder @triggername=N'[dbo].[trigOrderTicket_IU]', @order=N'First', @stmttype=N'INSERT'
GO
EXEC sp_settriggerorder @triggername=N'[dbo].[trigOrderTicket_IU]', @order=N'First', @stmttype=N'UPDATE'
GO

/* add new associated Report Center - Order columns */
SET IDENTITY_INSERT tblReportColumnDefinition ON
INSERT INTO tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport)
	SELECT 90010, 1, 'SELECT isnull(OA.OverrideActualMiles, RS.ActualMiles) FROM tblOrderApproval OA WHERE OA.OrderID = RS.ID', 'TRIP | MILES | Actual Miles Final', NULL, NULL, 4, NULL, 1, '*', 0
	UNION
	SELECT 90011, 1, 'SELECT isnull(OA.OverrideOriginMin, RS.OriginMin) FROM tblOrderApproval OA WHERE OA.OrderID = RS.ID', 'TRIP | TIMESTAMPS | Origin Min Final', NULL, NULL, 4, NULL, 1, '*', 0
	UNION
	SELECT 90012, 1, 'SELECT isnull(OA.OverrideDestMin, RS.DestMin) FROM tblOrderApproval OA WHERE OA.OrderID = RS.ID', 'TRIP | TIMESTAMPS | Override Dest Min', NULL, NULL, 4, NULL, 1, '*', 0
	UNION
	SELECT 90013, 1, 'SELECT coalesce(OA.OverrideOriginMin, RS.OriginMin, 0) + coalesce(OA.OverrideDestMin, RS.DestMin, 0) FROM tblOrderApproval OA WHERE OA.OrderID = RS.ID', 'TRIP | TIMESTAMPS | Total Min Final', NULL, NULL, 4, NULL, 1, '*', 0
	UNION
	SELECT 90014, 1, 'SELECT cast(CASE WHEN isnull(OA.OverrideH2S, 0) = 1 THEN 0 ELSE RS.H2S END as bit) FROM tblOrderApproval OA WHERE OA.OrderID = RS.ID', 'ORIGIN | GENERAL | H2S Final', NULL, NULL, 5, 'SELECT ID=1, Name=''Yes'' UNION SELECT 0, ''No''', 0, '*', 0
	UNION
	SELECT 90015, 1, 'SELECT cast(CASE WHEN isnull(OA.OverrideChainup, 0) = 1 THEN 0 ELSE RS.Chainup END as bit) FROM tblOrderApproval OA WHERE OA.OrderID = RS.ID', 'GENERAL | Chain-Up Final', NULL, NULL, 5, 'SELECT ID=1, Name=''Yes'' UNION SELECT 0, ''No''', 0, '*', 0
	UNION
	SELECT 90016, 1, 'SELECT CASE WHEN isnull(OA.OverrideReroute, 0) = 1 THEN 0 ELSE RS.RerouteCount END FROM tblOrderApproval OA WHERE OA.OrderID = RS.ID', 'TRIP | Reroute Count Final', NULL, NULL, 4, NULL, 1, '*', 0
SET IDENTITY_INSERT tblReportColumnDefinition OFF
GO

COMMIT 
SET NOEXEC OFF

/*
	select O.* 
	from vieworderlocaldates O
	join viewRoute R ON R.ID = O.RouteID
	left join tblOrderSettlementCarrier OSS ON OSS.OrderID = O.ID
	join tblorderreroute ORE ON ORE.OrderID = O.ID
	where statusid in (3)
	  and OSS.BatchID is null
	  
	exec spApplyRatesCarrier 32506, 'kalons'
	SELECT * FROM dbo.fnOrderRerouteData(30224)
	exec spOrderApprovalSource 'kalons', null, '1/1/2014', '3/1/2014', 0

*/

