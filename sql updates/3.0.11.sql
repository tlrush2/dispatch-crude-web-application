/*
	-- add fnUserProfileValues() table function
	-- add fnDriverUserNames() table function
	-- revise viewDriver to show Driver.UserNames (from function above)
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.0.10'
SELECT  @NewVersion = '3.0.11'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

/*************************************************************/
-- Date Created: 9 Sep 2014
-- Author: Kevin Alons
-- Purpose: return the User - Profile Values as a table (for the specified PROFILEFIELD)
/*************************************************************/
CREATE FUNCTION fnUserProfileValues(@profileField varchar(25)) RETURNS TABLE AS RETURN
	SELECT UserName, ProfileValue = substring(PropertyValuesString, ValueStart, cast(SUBSTRING(PropertyNames, NameStart, NameEnd - NameStart) as int))
	FROM (
		SELECT UserName, PropertyNames, PropertyValuesString
			, ValueStart = cast(SUBSTRING(PropertyNames, NameStart, NameEnd - NameStart) as int) + 1
			, NameStart = NameEnd + 1
			, NameEnd = CHARINDEX(':', PropertyNames, NameEnd + 1)
		FROM (
			SELECT UserName, PropertyNames, PropertyValuesString
				, NameStart, NameEnd = CHARINDEX(':', PropertyNames, NameStart + 1)
			FROM (
				SELECT U.UserName, PropertyNames, PropertyValuesString
					, NameStart = CHARINDEX(BP.Patt, cast(P.PropertyNames as varchar(max)), 1) + LEN(BP.Patt)
				FROM aspnet_Profile P 
				JOIN aspnet_Users U ON U.UserId = P.UserId
				CROSS JOIN (SELECT Patt = @profileField + ':S:') BP
				WHERE P.PropertyNames LIKE '%' + BP.Patt + '%:%:%'
			) X
		) X
	) X

GO
GRANT SELECT ON fnUserProfileValues TO dispatchcrude_iis_acct
GO

/*************************************************************/
-- Date Created: 9 Sep 2014
-- Author: Kevin Alons
-- Purpose: return the UserNames (CSV style) for each DriverID
/*************************************************************/
CREATE FUNCTION fnDriverUserNames() RETURNS 
	@DriverUserNames TABLE (
		DriverID int
	  , UserNames varchar(1000)
	) AS
BEGIN
	DECLARE @PD TABLE (UserName varchar(100), DriverID int)
	INSERT INTO @PD SELECT * FROM dbo.fnUserProfileValues('DriverID')
	
	INSERT INTO @DriverUserNames (DriverID, UserNames)
	SELECT 
	  ID
	, STUFF(
		(
		  SELECT ',' + [UserName]
		  FROM @PD
		  WHERE DriverID = D.ID
		  FOR XML PATH(''),TYPE
		  ).value('.','VARCHAR(MAX)'
		), 1, 1, '')
	FROM tblDriver D
	
	RETURN
END
GO
GRANT SELECT ON fnDriverUserNames TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 26 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Drivers table records with "friendly" translated values included
/***********************************/
ALTER VIEW [dbo].[viewDriver] AS
SELECT X.*
	, DUN.UserNames
	, CASE WHEN Active = 1 THEN '' ELSE 'Deleted: ' END + FullName AS FullNameD
FROM (
	SELECT D.*
	, cast(CASE WHEN isnull(C.DeleteDateUTC, D.DeleteDateUTC) IS NULL THEN 1 ELSE 0 END as bit) AS Active
	, D.FirstName + ' ' + D.LastName As FullName
	, D.LastName + ', ' + D.FirstName AS FullNameLF
	, isnull(CT.Name, 'Unknown') AS CarrierType
	, C.Name AS Carrier
	, S.Abbreviation AS StateAbbrev 
	, T.FullName AS Truck
	, T1.FullName AS Trailer
	, T2.FullName AS Trailer2
	, R.Name AS Region
	, DS.MobileAppVersion
	, (SELECT MAX(DestDepartTimeUTC) FROM dbo.tblOrder WHERE DriverID = D.ID) AS LastDeliveredOrderTime
	FROM dbo.tblDriver D 
	JOIN dbo.tblCarrier C ON C.ID = D.CarrierID 
	LEFT JOIN dbo.tblDriver_Sync DS ON DS.DriverID = D.ID
	LEFT JOIN dbo.tblCarrierType CT ON CT.ID = C.CarrierTypeID
	LEFT JOIN tblState S ON S.ID = D.StateID
	LEFT JOIN viewTruck T ON T.ID = D.TruckID
	LEFT JOIN viewTrailer T1 ON T1.ID = D.TrailerID
	LEFT JOIN viewTrailer T2 ON T2.ID = D.Trailer2ID
	LEFT JOIN tblRegion R ON R.ID = D.RegionID
) X
JOIN dbo.fnDriverUserNames() DUN ON DUN.DriverID = X.ID
GO

EXEC _spRebuildAllObjects
GO

COMMIT
SET NOEXEC OFF