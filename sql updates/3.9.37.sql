SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.9.36'
SELECT  @NewVersion = '3.9.37'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-897: Capability to override Min Settlement Units.'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO



-- JAE 12/21/15 - add approval feature for override min settlement units (carrier and shipper)
ALTER TABLE tblOrderApproval ADD OverrideCarrierMinSettlementUnits decimal(9,3) NULL,
								 OverrideShipperMinSettlementUnits decimal(9,3) NULL
GO


-- JAE 12/23/15 - Add Final columns for Min Settlement Units (approved)
SET IDENTITY_INSERT tblReportColumnDefinition ON

INSERT tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport)
	SELECT 90041, 1, 'SELECT CASE WHEN OA.Approved=1 THEN isnull(OA.OverrideCarrierMinSettlementUnits, RS.CarrierMinSettlementUnits) ELSE NULL END FROM tblOrderApproval OA WHERE OA.OrderID = RS.ID', 'SETTLEMENT | CARRIER | Carrier Min Settle Units Final', '#0.00', NULL, 4, NULL, 1, 'Administrator,Management,Operations,Carrier', 1
	UNION
	SELECT 90042, 1, 'SELECT CASE WHEN OA.Approved=1 THEN isnull(OA.OverrideShipperMinSettlementUnits, RS.ShipperMinSettlementUnits) ELSE NULL END FROM tblOrderApproval OA WHERE OA.OrderID = RS.ID', 'SETTLEMENT | SHIPPER | Shipper Min Settle Units Final', '#0.00', NULL, 4, NULL, 1, 'Administrator,Management,Operations,Customer', 1
  EXCEPT SELECT ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport FROM tblReportColumnDefinition

SET IDENTITY_INSERT tblReportColumnDefinition OFF
GO


/***********************************
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Carrier "Settlement" $$ values to an Delivered/Audited order
-- Changes:
	- 3.7.40 - 2015/06/30 - KDA - use specific SettlementFactor (not fall thru to less precise if specified one is missing)
								- use the DestinationUomID for the Settlement Uom if using a Destination Settlement Factor
	- 3.9.0 - 2015/08/16 - KDA  - use OUTER APPLY for SettlementUnits/MinSettlementUnits
	- 3.9.19.13 - 2015/08/16 - KDA  - fix typo that used Destination UOM as settlement UOM due to use of MSF.ID instead of correct MSF.SettlementFactorID in logic
	- 3.9.37  - 2015/12/22 - JAE - Propagate Min Settlement UOM for display on approval page
***********************************/
ALTER VIEW viewOrderSettlementUnitsCarrier AS
	SELECT X3.*
		, SettlementUnits = CASE WHEN X3.ActualUnits IS NULL THEN 0 ELSE dbo.fnMaxDecimal(X3.ActualUnits, MinSettlementUnits) END
	FROM (
		SELECT OrderID
			, CarrierID
			, SettlementUomID
			, CarrierSettlementFactorID
			, SettlementFactorID
			, MinSettlementUnits = dbo.fnMinDecimal(isnull(X2.WRMSVUnits, X2.MinSettlementUnits), X2.MinSettlementUnits)
			, MinSettlementUnitsID
			, MinSettlementUomID
			, ActualUnits
		FROM (
			SELECT OrderID
				, CarrierID
				, SettlementUomID
				, CarrierSettlementFactorID
				, SettlementFactorID
				, WRMSVUnits = dbo.fnConvertUOM(WRMSVUnits, WRMSVUomID, SettlementUomID)
				, MinSettlementUnits = dbo.fnConvertUOM(MinSettlementUnits, MinSettlementUomID, SettlementUomID)
				, MinSettlementUomID
				, MinSettlementUnitsID
				, ActualUnits 
			FROM (
				SELECT OrderID = O.ID
					, O.CarrierID
					, SettlementUomID = CASE WHEN MSF.SettlementFactorID IN (1,2,3) THEN O.OriginUomID ELSE O.DestUomID END
					, CarrierSettlementFactorID = MSF.ID
					, MSF.SettlementFactorID
					, R.WRMSVUomID
					, R.WRMSVUnits
					, MinSettlementUnitsID = MSU.ID
					, MinSettlementUomID = MSU.UomID
					, MinSettlementUnits = isnull(MSU.MinSettlementUnits, 0) 
					, ActualUnits = CASE MSF.SettlementFactorID
							WHEN 1 THEN O.OriginGrossUnits 
							WHEN 3 THEN O.OriginGrossStdUnits 
							WHEN 2 THEN O.OriginNetUnits 
							WHEN 4 THEN O.DestGrossUnits
							WHEN 5 THEN O.DestNetUnits END
				FROM dbo.tblOrder O
				OUTER APPLY dbo.fnOrderCarrierSettlementFactor(O.ID) MSF
				OUTER APPLY dbo.fnOrderCarrierMinSettlementUnits(O.ID) MSU 
				JOIN dbo.tblRoute R ON R.ID = O.RouteID
				JOIN tblCarrier C ON C.ID = O.CarrierID
			) X
		) X2
	) X3


GO

EXEC sp_refreshview viewOrderSettlementUnitsCarrier
GO


/***********************************
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Shipper "Settlement" $$ values to an Delivered/Audited order
-- Changes:
	- 3.7.40 - 2015/06/30 - KDA - use specific SettlementFactor (not fall thru to less precise if specified one is missing)
								- use the DestinationUomID for the Settlement Uom if using a Destination Settlement Factor
	- 3.9.0 - 2015/08/16 - KDA  - use OUTER APPLY for SettlementUnits/MinSettlementUnits
	- 3.9.19.13 - 2015/08/16 - KDA  - fix typo that used Destination UOM as settlement UOM due to use of MSF.ID instead of correct MSF.SettlementFactorID in logic
	- 3.9.37  - 2015/12/22 - JAE - Propagate Min Settlement UOM for display on approval page
***********************************/
ALTER VIEW viewOrderSettlementUnitsShipper AS
	SELECT X3.*
		, SettlementUnits = CASE WHEN X3.ActualUnits IS NULL THEN 0 ELSE dbo.fnMaxDecimal(X3.ActualUnits, MinSettlementUnits) END
	FROM (
		SELECT OrderID
			, ShipperID
			, SettlementUomID
			, ShipperSettlementFactorID
			, SettlementFactorID
			, MinSettlementUnits = dbo.fnMinDecimal(isnull(X2.WRMSVUnits, X2.MinSettlementUnits), X2.MinSettlementUnits)
			, MinSettlementUnitsID
			, MinSettlementUomID
			, ActualUnits
		FROM (
			SELECT OrderID
				, ShipperID
				, SettlementUomID
				, ShipperSettlementFactorID
				, SettlementFactorID
				, WRMSVUnits = dbo.fnConvertUOM(WRMSVUnits, WRMSVUomID, SettlementUomID)
				, MinSettlementUnits = dbo.fnConvertUOM(MinSettlementUnits, MinSettlementUomID, SettlementUomID)
				, MinSettlementUnitsID
				, MinSettlementUomID
				, ActualUnits 
			FROM (
				SELECT OrderID = O.ID
					, ShipperID = O.CustomerID
					, SettlementUomID = CASE WHEN MSF.SettlementFactorID IN (1,2,3) THEN O.OriginUomID ELSE O.DestUomID END
					, ShipperSettlementFactorID = MSF.ID
					, MSF.SettlementFactorID
					, R.WRMSVUomID
					, R.WRMSVUnits
					, MinSettlementUnitsID = MSU.ID
					, MinSettlementUomID = MSU.UomID
					, MinSettlementUnits = isnull(MSU.MinSettlementUnits, 0) 
					, ActualUnits = CASE MSF.SettlementFactorID
							WHEN 1 THEN O.OriginGrossUnits 
							WHEN 3 THEN O.OriginGrossStdUnits 
							WHEN 2 THEN O.OriginNetUnits 
							WHEN 4 THEN O.DestGrossUnits
							WHEN 5 THEN O.DestNetUnits END
				FROM dbo.tblOrder O
				OUTER APPLY dbo.fnOrderShipperSettlementFactor(O.ID) MSF
				OUTER APPLY dbo.fnOrderShipperMinSettlementUnits(O.ID) MSU 
				JOIN dbo.tblRoute R ON R.ID = O.RouteID
				JOIN tblCustomer C ON C.ID = O.CustomerID
			) X
		) X2
	) X3

GO

EXEC sp_refreshview viewOrderSettlementUnitsShipper
GO


/*************************************************************
-- Date Created: 15 Jun 2015
-- Author: Kevin Alons
-- Purpose: return the base data for the Order Approval page
-- Changes:
	- 3.9.2  - 2015/08/25 - KDA - remove obsolete OrderDateUTC & DeliverDateUTC fields (which were used to optimize date searches when OrderDate was virtual)
	- 3.9.20 - 2015/10/26 - JAE - Add columns for transfer percentage and override percentage
	- 3.9.25 - 2015/11/10 - JAE - Use origin driver for approval page
	- 3.9.37  - 2015/12/21 - JAE - Add columns for min settlement units (carrier and shipper)
*************************************************************/
ALTER VIEW [dbo].[viewOrderApprovalSource]
AS
	SELECT O.ID
		, O.StatusID
		, Status = O.OrderStatus
		, O.OrderNum
		, O.OrderDate
		, DeliverDate = ISNULL(O.DeliverDate, O.OrderDate)
		, ShipperID = O.CustomerID
		, O.CarrierID
		, DriverID = O.OriginDriverID
		, TruckID = O.OriginTruckID
		, O.TrailerID
		, O.Trailer2ID
		, O.ProducerID
		, O.OperatorID
		, O.PumperID
		, Shipper = O.Customer
		, O.Carrier
		, Driver = O.OriginDriver
		, O.Truck
		, O.Trailer
		, O.Trailer2
		, O.Producer
		, O.Operator
		, O.Pumper
        , O.TicketTypeID
		, O.TicketType
		, O.ProductID
		, O.Product
		, O.ProductGroupID
		, O.ProductGroup
    
		, O.Rejected
		, RejectReason = O.RejectNumDesc
		, O.RejectNotes
		, O.ChainUp
		, OA.OverrideChainup
		, O.H2S
		, OA.OverrideH2S
        , O.DispatchConfirmNum
		, O.DispatchNotes
		, O.PickupDriverNotes
		, O.DeliverDriverNotes
		, Approved = CAST(ISNULL(OA.Approved, 0) AS BIT)
		, OSS.WaitFeeParameterID, O.AuditNotes
		
		, O.OriginID
		, O.Origin
		, O.OriginStateID
		, O.OriginArriveTime
		, O.OriginDepartTime
		, O.OriginMinutes
		, O.OriginUomID
		, O.OriginUOM
		, O.OriginGrossUnits
		, O.OriginGrossStdUnits
		, O.OriginNetUnits
		, OriginWaitReason = OWR.NumDesc
		, O.OriginWaitNotes
		, ShipperOriginWaitBillableMinutes = OSS.OriginWaitBillableMinutes
		, CarrierOriginWaitBillableMinutes = OSC.OriginWaitBillableMinutes
		, OA.OverrideOriginMinutes 
		
		, O.DestinationID
		, O.Destination
		, O.DestStateID
		, O.DestArriveTime
		, O.DestDepartTime
		, O.DestMinutes
		, O.DestUomID
		, O.DestUOM
		, O.DestGrossUnits
		, O.DestNetUnits
		, O.DestOpenMeterUnits
		, O.DestCloseMeterUnits
		, DestWaitReason = OWR.NumDesc
		, O.DestWaitNotes
		, ShipperDestinationWaitBillableMinutes = OSS.DestinationWaitBillableMinutes
		, CarrierDestinationWaitBillableMinutes = OSC.DestinationWaitBillableMinutes
		, OA.OverrideDestMinutes
	
		, Rerouted = CAST(CASE WHEN ORD.OrderID IS NULL THEN 0 ELSE 1 END AS BIT)
		, OA.OverrideReroute
		, O.ActualMiles
		, RerouteMiles = ROUND(ORD.RerouteMiles, 0)
		, OA.OverrideActualMiles
		, IsTransfer = CAST(CASE WHEN OT.OrderID IS NULL THEN 0 ELSE 1 END AS BIT)
		, TransferPercentComplete = ISNULL(OT.PercentComplete,100)

		, OA.OverrideTransferPercentComplete
		, CarrierMinSettlementUnits = OSC.MinSettlementUnits
		, CarrierMinSettlementUOM = CUOM.Abbrev
		, OA.OverrideCarrierMinSettlementUnits
		, ShipperMinSettlementUnits = OSS.MinSettlementUnits
		, ShipperMinSettlementUOM = CUOM.Abbrev
		, OA.OverrideShipperMinSettlementUnits
		
		, OA.CreateDateUTC
		, OA.CreatedByUser
		
		, OrderID = OSC.OrderID | OSS.OrderID -- NULL if either are NULL - if NULL indicates that the order is not yet rates (used below to rate these)
	FROM viewOrderLocalDates O
	OUTER APPLY dbo.fnOrderRerouteData(O.ID) ORD
	LEFT JOIN tblOrderReroute ORE ON ORE.OrderID = O.ID
	LEFT JOIN tblOrderTransfer OT ON OT.OrderID = O.ID
	LEFT JOIN tblOrderSettlementCarrier OSC ON OSC.OrderID = O.ID
	LEFT JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = O.ID
	LEFT JOIN viewOriginWaitReason OWR ON OWR.ID = O.OriginWaitReasonID
	LEFT JOIN viewDestinationWaitReason DWR ON DWR.ID = O.DestWaitReasonID
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
	LEFT JOIN viewOrderSettlementUnitsCarrier OSUC ON OSUC.OrderID = O.ID
	LEFT JOIN viewOrderSettlementUnitsShipper OSUS ON OSUS.OrderID = O.ID
	LEFT JOIN tblUom CUOM ON CUOM.ID = OSUC.MinSettlementUomID
	LEFT JOIN tblUom SUOM ON SUOM.ID = OSUS.MinSettlementUomID
	WHERE O.StatusID IN (4) /* only include orders that are marked AUDITED */
	  AND OSC.BatchID IS NULL AND OSS.BatchID IS NULL /* don't show orders that have been settled */

GO

EXEC sp_refreshview viewOrderApprovalSource
GO

/***********************************
-- Date Created: 2015/08/14
-- Author: Kevin Alons
-- Purpose: create/update APPROVAL on an individual order (and re-rate the order)
-- Changes:
	3.9.12 - 2015/09/01 - KDA - add SET NOCOUNT ON to statement
							  - RAISE an ERROR if no record is updated (either due to target record not in valid state or not found
	3.9.20 - 2015/10/03 - JAE - add OverrideTransferPercentComplete parameters/logic
	3.9.37  - 2015/12/21 - JAE - add overrides for carrier and shipper min settlement units
***********************************/
ALTER PROCEDURE [dbo].[spUpdateOrderApproval]
(
  @ID INT
, @UserName varchar(100)
, @OverrideActualMiles INT = NULL
, @OverrideOriginMinutes INT = NULL
, @OverrideDestMinutes INT = NULL
, @OverrideChainup BIT = NULL
, @OverrideH2S BIT = NULL
, @OverrideReroute BIT = NULL
, @Approved BIT = 1
, @OverrideTransferPercentComplete INT = NULL
, @OverrideCarrierMinSettlementUnits decimal = NULL
, @OverrideShipperMinSettlementUnits decimal = NULL
)
AS BEGIN
	SET NOCOUNT ON;
	
	/* only allowed AUDITED | non-settled orders from being APPROVED */
	IF EXISTS (
		SELECT O.* 
		FROM tblOrder O 
		LEFT JOIN tblOrderSettlementCarrier OSC ON OSC.OrderID = O.ID
		LEFT JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = O.ID
		WHERE O.ID = @ID AND O.StatusID IN (4) /* 4 = AUDITED */
		  AND OSC.BatchID IS NULL AND OSS.BatchID IS NULL /* this order is not yet SETTLED */
	)
	BEGIN
		DECLARE @startedTX BIT
		BEGIN TRY
			IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRAN
				SET @startedTX = 1
			END
			UPDATE tblOrderApproval 
				SET OverrideActualMiles = @OverrideActualMiles
					, OverrideOriginMinutes = @OverrideOriginMinutes
					, OverrideDestMinutes = @OverrideDestMinutes
					, OverrideChainup = @OverrideChainup
					, OverrideH2S = @OverrideH2S
					, OverrideReroute = @OverrideReroute
					, OverrideTransferPercentComplete = @OverrideTransferPercentComplete
					, OverrideCarrierMinSettlementUnits = @OverrideCarrierMinSettlementUnits 
					, OverrideShipperMinSettlementUnits = @OverrideShipperMinSettlementUnits 
					, Approved = @Approved
			WHERE OrderID = @ID
			IF (@@ROWCOUNT = 0)
			BEGIN
				INSERT INTO tblOrderApproval 
					(OrderID, OverrideActualMiles, OverrideOriginMinutes, OverrideDestMinutes, OverrideChainup, OverrideH2S, OverrideReroute
						, OverrideTransferPercentComplete, OverrideCarrierMinSettlementUnits, OverrideShipperMinSettlementUnits
						, Approved, CreatedByUser, CreateDateUTC) 
				VALUES 
					(@ID, @OverrideActualMiles, @OverrideOriginMinutes, @OverrideDestMinutes, @OverrideChainup, @OverrideH2S, @OverrideReroute
						, @OverrideTransferPercentComplete, @OverrideCarrierMinSettlementUnits, @OverrideShipperMinSettlementUnits
						, @Approved, @UserName, GETUTCDATE())
			END
			/* ensure any changes to this Approval are applied - which will affect applied rates */
			EXEC spApplyRatesBoth @ID, @UserName
			
			IF (@startedTX = 1)
				COMMIT TRAN
		END TRY
		BEGIN CATCH
			DECLARE @msg VARCHAR(MAX), @severity INT
			SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY()
			IF (@startedTX = 1)
				ROLLBACK TRAN
			RAISERROR(@msg, @severity, 1)
		END CATCH
	END
	ELSE
	BEGIN
		RAISERROR('Record not valid for Approval or not found', 16, 1)
	END
END

GO


/*************************************************************
-- Date Created: 15 Jun 2015
-- Author: Kevin Alons
-- Purpose: return the data for the Order Approval page
-- Changes:
	- 3.9.2  - 2015/08/26 - KDA - remove obsolete performance optimization using OrderDateUTC to force use of index (now OrderDate is indexed)
	- 3.9.3  - 2015/08/29 - KDA - add new FinalXXX fields
	- 3.9.20 - 2015/10/26 - JAE - update stored procedure to save transfer override values
	- 3.9.37  - 2015/12/21 - JAE - Added carrier and shipper min settlement units
*************************************************************/
ALTER PROCEDURE [dbo].[spOrderApprovalSource]
(
  @userName VARCHAR(100)
, @ShipperID INT = NULL
, @CarrierID INT = NULL
, @ProductGroupID INT = NULL
, @OriginStateID INT = NULL
, @DestStateID INT = NULL
, @Start DATE = NULL
, @End DATE = NULL
, @IncludeApproved BIT = 0
) AS BEGIN
	SET NOCOUNT ON;
	
	SELECT *
		, FinalActualMiles = ISNULL(OverrideActualMiles, ActualMiles)
		, FinalOriginMinutes = ISNULL(OverrideOriginMinutes, OriginMinutes)
		, FinalDestMinutes = ISNULL(OverrideDestMinutes, DestMinutes)
		, FinalChainup = CAST(ISNULL(1-NULLIF(OverrideChainup, 0), Chainup) AS BIT)
		, FinalH2S = CAST(ISNULL(1-NULLIF(OverrideH2S, 0), H2S) AS BIT)
		, FinalRerouted = CAST(ISNULL(1-NULLIF(OverrideReroute, 0), Rerouted) AS BIT)
		, FinalTransferPercentComplete = ISNULL(OverrideTransferPercentComplete, TransferPercentComplete)
		, FinalCarrierMinSettlementUnits = ISNULL(OverrideCarrierMinSettlementUnits, CarrierMinSettlementUnits)
		, FinalShipperMinSettlementUnits = ISNULL(OverrideShipperMinSettlementUnits, ShipperMinSettlementUnits)
	INTO #X
	FROM viewOrderApprovalSource 
	WHERE (NULLIF(@ShipperID, -1) IS NULL OR ShipperID = @ShipperID)
	  AND (NULLIF(@CarrierID, -1) IS NULL OR CarrierID = @CarrierID)
	  AND (NULLIF(@ProductGroupID, -1) IS NULL OR ProductGroupID = @ProductGroupID)
	  AND (NULLIF(@OriginStateID, -1) IS NULL OR OriginStateID = @OriginStateID)
	  AND (NULLIF(@DestStateID, -1) IS NULL OR DestStateID = @DestStateID)
	  AND OrderDate BETWEEN ISNULL(@Start, '1/1/1900') AND ISNULL(@End, '1/1/2200')
	  AND (@IncludeApproved = 1 OR ISNULL(Approved, 0) = 0) 
	  
	DECLARE @id int
	WHILE EXISTS (SELECT * FROM #x WHERE OrderID IS NULL)
	BEGIN
		SELECT @id = min(ID) FROM #x WHERE OrderID IS NULL
		EXEC spApplyRatesBoth @id, @userName
		UPDATE #x SET OrderID = @id, Approved = (SELECT Approved FROM tblOrderApproval WHERE OrderID = @id) WHERE ID = @id
	END
	ALTER TABLE #X DROP COLUMN OrderID
	
	SELECT * FROM #x WHERE (@IncludeApproved = 1 OR ISNULL(Approved, 0) = 0) 

END


GO



/*************************************************
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Carrier "Settlement" $$ values to an Delivered/Audited order
-- Changes:
	- 3.7.26 - 2015/06/13 - KDA - add support for: Driver|DriverGroup, Producer to some rates, separate Carrier|Shipper MinSettlementUnits|SettlementFactor best-match values, 
	- 3.9.0  - 2015/08/13 - KDA - honor Approve overrides in rating process
	- 3.9.9  - 2015/08/31 - KDA - add new @AllowReApplyPostBatch parameter to allow re-applying after batch is applied
	- 3.9.19.2 - 2015/09/24 - KDA - fix erroneous logic that computed Asssessorial Settlement results before the Order Level results were posted
	- 3.9.37  - 2015/12/22 - JAE - Use tblOrderApproval to get override for Carrier Min Settlement Units
*************************************************/
ALTER PROCEDURE [dbo].[spApplyRatesCarrier]
(
  @ID int
, @UserName varchar(100)
, @OriginWaitAmount money = NULL
, @DestWaitAmount money = NULL
, @RejectionAmount money = NULL
, @FuelSurchargeAmount money = NULL
, @LoadAmount money = NULL
, @AssessorialRateTypeID_CSV varchar(max) = NULL
, @AssessorialAmount_CSV varchar(max) = null
, @ResetOverrides bit = 0
, @AllowReApplyPostBatch bit = 0
) AS BEGIN

	SET NOCOUNT ON
	
	IF (@AllowReApplyPostBatch = 0) AND EXISTS (SELECT * FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND BatchID IS NOT NULL)
	BEGIN
		RAISERROR('Invoice has already been Carrier Settled', 16, 1)
		RETURN
	END
	
	/* ensure that Shipper Rates have been applied prior to applying Carrier rates (since they could be dependent on Shipper rates) */
	IF NOT EXISTS (SELECT * FROM tblOrderSettlementShipper WHERE OrderID = @ID)
		EXEC spApplyRatesShipper @ID, @UserName

	/* transform the provided manual Assessorial Rates (if any) into a queryable table variable */
	DECLARE @ManualAssessorialRates TABLE (ID int primary key, Amount money)
	INSERT INTO @ManualAssessorialRates (ID, Amount) 
		SELECT ID.ID, R.Amount
		FROM (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), ID FROM dbo.fnSplitCSVIDs(@AssessorialRateTypeID_CSV)) ID
		JOIN (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), amount = CAST(value as money) FROM dbo.fnSplitCSV(@AssessorialAmount_CSV)) R ON R.sortid = ID.sortid

	/* persist all existing MANUAL Assessorial Rates from the existing Order Settlement (if any present and not resetting them) */
	INSERT INTO @ManualAssessorialRates (ID, Amount)
		SELECT AssessorialRateTypeID, Amount 
		FROM dbo.tblOrderSettlementCarrierAssessorialCharge 
		WHERE OrderID = @ID AND AssessorialRateID IS NULL 
			AND ID NOT IN (SELECT ID FROM @ManualAssessorialRates)
			AND @ResetOverrides = 0

	/* persist all existing settlement amount overrides from the existing Order Settlement - if not resetting them */
	IF (@ResetOverrides = 0)
	BEGIN 
		SELECT @OriginWaitAmount = OriginWaitAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @OriginWaitAmount IS NULL AND OriginWaitRateID IS NULL
		SELECT @DestWaitAmount = DestinationWaitAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @DestWaitAmount IS NULL AND DestinationWaitRateID IS NULL
		SELECT @RejectionAmount = OrderRejectAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @RejectionAmount IS NULL AND OrderRejectRateID IS NULL
		SELECT @FuelSurchargeAmount = FuelSurchargeAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @FuelSurchargeAmount IS NULL AND FuelSurchargeRateID IS NULL
		SELECT @LoadAmount = LoadAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @LoadAmount IS NULL AND RouteRateID IS NULL AND RangeRateID IS NULL
	END

	/* compute the basic settlement units from the Order being invoices */
	DECLARE @SettlementUnits TABLE
	(
		OrderID int
	  , SettlementUomID int
	  , SettlementFactorID int
	  , CarrierSettlementFactorID int
	  , MinSettlementUnits decimal(18, 10)
	  , MinSettlementUnitsID int
	  , ActualUnits decimal(18, 10)
	  , SettlementUnits decimal(18, 10)
	)
	INSERT INTO @SettlementUnits (OrderID, SettlementUomID, SettlementFactorID, CarrierSettlementFactorID, MinSettlementUnits, MinSettlementUnitsID, ActualUnits, SettlementUnits)
		SELECT OSU.OrderID, OSU.SettlementUomID, OSU.SettlementFactorID, OSU.CarrierSettlementFactorID, ISNULL(OA.OverrideCarrierMinSettlementUnits, OSU.MinSettlementUnits), OSU.MinSettlementUnitsID, OSU.ActualUnits, OSU.SettlementUnits
		FROM dbo.viewOrderSettlementUnitsCarrier OSU
		LEFT JOIN tblOrderApproval OA ON OA.OrderID = OSU.OrderID
		WHERE OSU.OrderID = @ID

	/* compute the order level settlement rateIDs & settlement amounts - into temp table #I */
	SELECT OrderID = @ID
		, OrderDate
		, CarrierSettlementFactorID, MinSettlementUnitsID
		, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
		, RouteRateID, RangeRateID, LoadAmount
		, WaitFeeParameterID
		, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount 
		, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount
		, OrderRejectRateID, OrderRejectAmount
		, FuelSurchargeRateID, FuelSurchargeRate, FuelSurchargeAmount
		, OriginTaxRate
		, TotalAmount = ISNULL(LoadAmount, 0) 
			+ ISNULL(OriginWaitAmount, 0) 
			+ ISNULL(DestWaitAmount, 0) 
			+ ISNULL(OrderRejectAmount, 0) 
			+ ISNULL(FuelSurchargeAmount, 0)
		, CreatedByUser = @UserName
		, ChainUp, H2S, Rerouted
	INTO #I
	FROM (
		SELECT OrderDate
			, CarrierSettlementFactorID, MinSettlementUnitsID
			, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
			, RouteRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RouteRateID ELSE NULL END
			, RangeRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RangeRateID ELSE NULL END
			, LoadAmount = round(ISNULL(@LoadAmount, RR.Amount), 2)
			, WaitFeeParameterID
			, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount = ROUND(OriginWaitAmount, 2)
			, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount = ROUND(DestWaitAmount, 2)
			, OrderRejectRateID = CASE WHEN @RejectionAmount IS NULL THEN RD.RateID ELSE NULL END
			, OrderRejectAmount = round(isnull(@RejectionAmount, RD.Amount), 2)
			, FuelSurchargeRateID = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.RateID ELSE NULL END
			, FuelSurchargeRate = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.Rate ELSE NULL END
			, FuelSurchargeAmount = round(isnull(@FuelSurchargeAmount, FSR.Amount), 2)
			, OriginTaxRate
			, Chainup, H2S, Rerouted = cast(CASE WHEN RerouteCount = 0 THEN 0 ELSE 1 END as bit) /* used for the Auto-Approve logic below */
		FROM (
			SELECT O.ID
				, O.OriginID
				, O.CarrierID
				, O.RouteID
				, SU.CarrierSettlementFactorID
				, SU.SettlementFactorID
				, SU.SettlementUomID
				, SU.ActualUnits
				, SU.MinSettlementUnitsID
				, SU.MinSettlementUnits
				, SettlementUnits = CASE WHEN O.Rejected = 1 THEN 0 ELSE SU.SettlementUnits END
				, RouteMiles = isnull(OA.OverrideActualMiles, O.ActualMiles)
				, OrderDate = isnull(O.OrderDate, O.DueDate)
				, ChainUp = cast(CASE WHEN OA.OverrideChainup = 1 THEN 0 ELSE O.Chainup END as bit)
				, H2S = cast(CASE WHEN OA.OverrideH2S = 1 THEN 0 ELSE O.H2S END as bit)
				, O.Rejected
				, RerouteCount = CASE WHEN OA.OverrideReroute = 1 THEN 0 ELSE O.RerouteCount END
				, IsSplit = CASE WHEN O.Rejected = 0 AND O.TicketCount > 1 THEN 1 ELSE 0 END
				, WaitFeeParameterID = WFP.ID
				, OriginWaitRateID = CASE WHEN @OriginWaitAmount IS NULL THEN OWD.RateID ELSE NULL END
				, OriginWaitBillableMinutes = OWD.BillableMinutes
				, OriginWaitBillableHours = OWD.BillableHours
				, OriginWaitAmount = isnull(@OriginWaitAmount, OWD.Amount)
				, DestWaitRateID = CASE WHEN @DestWaitAmount IS NULL THEN DWD.RateID ELSE NULL END
				, DestWaitBillableMinutes = DWD.BillableMinutes
				, DestWaitBillableHours = DWD.BillableHours
				, DestWaitAmount = isnull(@DestWaitAmount, DWD.Amount)
				, OriginTaxRate = OO.TaxRate
			FROM dbo.viewOrder O
			JOIN @SettlementUnits SU ON SU.OrderID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
			OUTER APPLY dbo.fnOrderCarrierOriginWaitData(@ID) OWD 
			OUTER APPLY dbo.fnOrderCarrierDestinationWaitData(@ID) DWD 
			OUTER APPLY dbo.fnOrderCarrierWaitFeeParameter(@ID) WFP
			WHERE O.ID = @ID
		) X
		OUTER APPLY dbo.fnOrderCarrierLoadAmount(@ID, SettlementUnits, SettlementUomID) RR 
		OUTER APPLY dbo.fnOrderCarrierOrderRejectData(@ID, SettlementUnits, SettlementUomID) RD
		OUTER APPLY dbo.fnOrderCarrierFuelSurchargeData(@ID) FSR
	) X2

	DECLARE @CreatedTran bit; SET @CreatedTran = CASE WHEN @@TRANCOUNT = 0 THEN 1 ELSE 0 END
	BEGIN TRY
		IF (@CreatedTran = 1)
			BEGIN TRAN

		-- remove the existing settlment record (if any)
		DELETE FROM tblOrderSettlementCarrierAssessorialCharge WHERE OrderID = @ID
		DELETE FROM tblOrderSettlementCarrier WHERE OrderID = @ID
	
		-- all Units and Rates are first normalized to the Order.OriginUOM then consistent processing
		-- and persisted in the Invoice (Settlement) record in this Order.OriginUOM 
		INSERT INTO tblOrderSettlementCarrier 
			(OrderID
			, OrderDate
			, CarrierSettlementFactorID
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnitsID
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestinationWaitRateID 
			, DestinationWaitBillableMinutes 
			, DestinationWaitBillableHours
			, DestinationWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser)
		SELECT OrderID
			, OrderDate
			, CarrierSettlementFactorID
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnitsID
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestWaitRateID 
			, DestWaitBillableMinutes 
			, DestWaitBillableHours
			, DestWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser
		FROM #I

		/* compute the new Assessorial Rates for this order - into temp table #IA */
		SELECT OrderID = @ID, AssessorialRateTypeID, AssessorialRateID = RateID, Amount = ROUND(Amount, 2), CreatedByUser = @UserName 
		INTO #IA
		FROM @SettlementUnits SU
		CROSS APPLY dbo.fnOrderCarrierAssessorialAmounts(@ID, SU.SettlementUnits, SU.SettlementUomID) CAA
		WHERE CAA.AssessorialRateTypeID NOT IN (SELECT ID FROM @ManualAssessorialRates)

		/* apply the computed settlement assessorial rate/amount records to the DB */
		INSERT INTO tblOrderSettlementCarrierAssessorialCharge (OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser)
			SELECT OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser FROM #IA
			UNION
			SELECT @ID, ID, NULL, Amount, @UserName FROM @ManualAssessorialRates

		IF (@CreatedTran = 1)
			COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @msg varchar(max), @severity int
		SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY()
		ROLLBACK
		RAISERROR(@msg, @severity, 1)
		RETURN
	END CATCH	
	
END


GO


/***********************************
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Shipper "Settlement" $$ values to an Delivered/Audited order
-- Changes:
	- 3.9.0  - 2015/08/13 - KDA - add Auto-Approve logic
						  - honor Approve overrides in rating process
	- 3.9.9  - 2015/08/31 - KDA - add new @AllowReApplyPostBatch parameter to allow re-applying after batch is applied
	- 3.9.19.2 - 2015/09/24 - KDA - fix erroneous logic that computed Asssessorial Settlement results before the Order Level results were posted
	- 3.9.37  - 2015/12/22 - JAE - Use tblOrderApproval to get override for Shipper Min Settlement Units
***********************************/
ALTER PROCEDURE [dbo].[spApplyRatesShipper]
(
  @ID int
, @UserName varchar(100)
, @OriginWaitAmount money = NULL
, @DestWaitAmount money = NULL
, @RejectionAmount money = NULL
, @FuelSurchargeAmount money = NULL
, @LoadAmount money = NULL
, @AssessorialRateTypeID_CSV varchar(max) = NULL
, @AssessorialAmount_CSV varchar(max) = null
, @ResetOverrides bit = 0
, @AllowReApplyPostBatch bit = 0
) AS BEGIN

	SET NOCOUNT ON
	
	-- ensure this order hasn't yet been fully settled
	IF (@AllowReApplyPostBatch = 0) AND EXISTS (SELECT * FROM tblOrderSettlementShipper WHERE OrderID = @ID AND BatchID IS NOT NULL)
	BEGIN
		RAISERROR('Invoice has already been Shipper Settled', 16, 1)
		RETURN
	END

	-- ensure the current Route.ActualMiles is assigned to the order
	UPDATE tblOrder SET ActualMiles = R.ActualMiles
	FROM tblOrder O
	JOIN tblRoute R ON R.ID = O.RouteID
	LEFT JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = O.ID
	WHERE O.ID = @ID
	  AND O.ActualMiles IS NULL OR O.ActualMiles <> R.ActualMiles
	  AND (@AllowReApplyPostBatch = 1 OR OSS.BatchID IS NULL)
	
	/* transform the provided manual Assessorial Rates (if any) into a queryable table variable */
	DECLARE @ManualAssessorialRates TABLE (ID int primary key, Amount money)
	INSERT INTO @ManualAssessorialRates (ID, Amount) 
		SELECT ID.ID, R.Amount
		FROM (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), ID FROM dbo.fnSplitCSVIDs(@AssessorialRateTypeID_CSV)) ID
		JOIN (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), amount = CAST(value as money) FROM dbo.fnSplitCSV(@AssessorialAmount_CSV)) R ON R.sortid = ID.sortid

	/* persist all existing MANUAL Assessorial Rates from the existing Order Settlement (if any present and not resetting them) */
	INSERT INTO @ManualAssessorialRates (ID, Amount)
		SELECT AssessorialRateTypeID, Amount 
		FROM dbo.tblOrderSettlementShipperAssessorialCharge 
		WHERE OrderID = @ID AND AssessorialRateID IS NULL 
			AND ID NOT IN (SELECT ID FROM @ManualAssessorialRates)
			AND @ResetOverrides = 0

	/* persist all existing settlement amount overrides from the existing Order Settlement - if not resetting them */
	IF (@ResetOverrides = 0)
	BEGIN 
		SELECT @OriginWaitAmount = OriginWaitAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @OriginWaitAmount IS NULL AND OriginWaitRateID IS NULL
		SELECT @DestWaitAmount = DestinationWaitAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @DestWaitAmount IS NULL AND DestinationWaitRateID IS NULL
		SELECT @RejectionAmount = OrderRejectAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @RejectionAmount IS NULL AND OrderRejectRateID IS NULL
		SELECT @FuelSurchargeAmount = FuelSurchargeAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @FuelSurchargeAmount IS NULL AND FuelSurchargeRateID IS NULL
		SELECT @LoadAmount = LoadAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @LoadAmount IS NULL AND RouteRateID IS NULL AND RangeRateID IS NULL
	END

	/* compute the basic settlement units from the Order being invoices */
	DECLARE @SettlementUnits TABLE
	(
		OrderID int
	  , SettlementUomID int
	  , SettlementFactorID int
	  , ShipperSettlementFactorID int
	  , MinSettlementUnits decimal(18, 10)
	  , MinSettlementUnitsID int
	  , ActualUnits decimal(18, 10)
	  , SettlementUnits decimal(18, 10)
	)
	INSERT INTO @SettlementUnits (OrderID, SettlementUomID, SettlementFactorID, ShipperSettlementFactorID, MinSettlementUnits, MinSettlementUnitsID, ActualUnits, SettlementUnits)
		SELECT OSU.OrderID, OSU.SettlementUomID, OSU.SettlementFactorID, OSU.ShipperSettlementFactorID, ISNULL(OA.OverrideShipperMinSettlementUnits, OSU.MinSettlementUnits), OSU.MinSettlementUnitsID, OSU.ActualUnits, OSU.SettlementUnits
		FROM dbo.viewOrderSettlementUnitsShipper OSU
		LEFT JOIN tblOrderApproval OA ON OA.OrderID = OSU.OrderID
		WHERE OSU.OrderID = @ID

	/* compute the order level settlement rateIDs & settlement amounts - into temp table #I */
	SELECT OrderID = @ID
		, OrderDate
		, ShipperSettlementFactorID, MinSettlementUnitsID
		, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
		, RouteRateID, RangeRateID, LoadAmount
		, WaitFeeParameterID
		, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount 
		, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount
		, OrderRejectRateID, OrderRejectAmount
		, FuelSurchargeRateID, FuelSurchargeRate, FuelSurchargeAmount
		, OriginTaxRate
		, TotalAmount = ISNULL(LoadAmount, 0) 
			+ ISNULL(OriginWaitAmount, 0) 
			+ ISNULL(DestWaitAmount, 0) 
			+ ISNULL(OrderRejectAmount, 0) 
			+ ISNULL(FuelSurchargeAmount, 0)
		, CreatedByUser = @UserName
		, Chainup, H2S, Rerouted /* used for the Auto-Approve logic below */
	INTO #I
	FROM (
		SELECT OrderDate
			, ShipperSettlementFactorID, MinSettlementUnitsID
			, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
			, RouteRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RouteRateID ELSE NULL END
			, RangeRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RangeRateID ELSE NULL END
			, LoadAmount = round(ISNULL(@LoadAmount, RR.Amount), 2)
			, WaitFeeParameterID
			, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount = ROUND(OriginWaitAmount, 2)
			, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount = ROUND(DestWaitAmount, 2)
			, OrderRejectRateID = CASE WHEN @RejectionAmount IS NULL THEN RD.RateID ELSE NULL END
			, OrderRejectAmount = round(isnull(@RejectionAmount, RD.Amount), 2)
			, FuelSurchargeRateID = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.RateID ELSE NULL END
			, FuelSurchargeRate = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.Rate ELSE NULL END
			, FuelSurchargeAmount = round(isnull(@FuelSurchargeAmount, FSR.Amount), 2)
			, OriginTaxRate
			, Chainup, H2S, Rerouted = cast(CASE WHEN RerouteCount = 0 THEN 0 ELSE 1 END as bit) /* used for the Auto-Approve logic below */
		FROM (
			SELECT O.ID
				, O.OriginID
				, O.CustomerID
				, O.RouteID
				, SU.ShipperSettlementFactorID
				, SU.SettlementFactorID
				, SU.SettlementUomID
				, SU.ActualUnits
				, SU.MinSettlementUnitsID
				, SU.MinSettlementUnits
				, SettlementUnits = CASE WHEN O.Rejected = 1 THEN 0 ELSE SU.SettlementUnits END
				, RouteMiles = isnull(OA.OverrideActualMiles, O.ActualMiles)
				, OrderDate = isnull(O.OrderDate, O.DueDate)
				, ChainUp = cast(CASE WHEN OA.OverrideChainup = 1 THEN 0 ELSE O.Chainup END as bit)
				, H2S = cast(CASE WHEN OA.OverrideH2S = 1 THEN 0 ELSE O.H2S END as bit)
				, O.Rejected
				, RerouteCount = CASE WHEN OA.OverrideReroute = 1 THEN 0 ELSE O.RerouteCount END
				, IsSplit = CASE WHEN O.Rejected = 0 AND O.TicketCount > 1 THEN 1 ELSE 0 END
				, WaitFeeParameterID = WFP.ID
				, OriginWaitRateID = CASE WHEN @OriginWaitAmount IS NULL THEN OWD.RateID ELSE NULL END
				, OriginWaitBillableMinutes = OWD.BillableMinutes
				, OriginWaitBillableHours = OWD.BillableHours
				, OriginWaitAmount = isnull(@OriginWaitAmount, OWD.Amount)
				, DestWaitRateID = CASE WHEN @DestWaitAmount IS NULL THEN DWD.RateID ELSE NULL END
				, DestWaitBillableMinutes = DWD.BillableMinutes
				, DestWaitBillableHours = DWD.BillableHours
				, DestWaitAmount = isnull(@DestWaitAmount, DWD.Amount)
				, OriginTaxRate = OO.TaxRate
			FROM dbo.viewOrder O
			JOIN @SettlementUnits SU ON SU.OrderID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
			OUTER APPLY dbo.fnOrderShipperOriginWaitData(@ID) OWD 
			OUTER APPLY dbo.fnOrderShipperDestinationWaitData(@ID) DWD 
			OUTER APPLY dbo.fnOrderShipperWaitFeeParameter(@ID) WFP
			WHERE O.ID = @ID
		) X
		OUTER APPLY dbo.fnOrderShipperLoadAmount(@ID, SettlementUnits, SettlementUomID) RR 
		OUTER APPLY dbo.fnOrderShipperOrderRejectData(@ID, SettlementUnits, SettlementUomID) RD
		OUTER APPLY dbo.fnOrderShipperFuelSurchargeData(@ID) FSR
	) X2

	DECLARE @CreatedTran bit; SET @CreatedTran = CASE WHEN @@TRANCOUNT = 0 THEN 1 ELSE 0 END
	BEGIN TRY
		IF (@CreatedTran = 1)
			BEGIN TRAN

		-- remove the existing settlment record (if any)
		DELETE FROM tblOrderSettlementShipperAssessorialCharge WHERE OrderID = @ID
		DELETE FROM tblOrderSettlementShipper WHERE OrderID = @ID

		-- all Units and Rates are first normalized to the Order.OriginUOM then consistent processing
		-- and persisted in the Invoice (Settlement) record in this Order.OriginUOM 
		INSERT INTO tblOrderSettlementShipper (
/*1*/		OrderID
/*2*/		, OrderDate
/*3*/		, ShipperSettlementFactorID
/*4*/		, SettlementFactorID 
/*5*/		, SettlementUomID 
/*6*/		, MinSettlementUnitsID
/*7*/		, MinSettlementUnits 
/*8*/		, SettlementUnits
/*9*/		, RouteRateID
/*10*/		, RangeRateID 
/*11*/		, LoadAmount
/*12*/		, WaitFeeParameterID 
/*13*/		, OriginWaitRateID 
/*14*/		, OriginWaitBillableMinutes 
/*15*/		, OriginWaitBillableHours
/*16*/		, OriginWaitAmount 
/*17*/		, DestinationWaitRateID 
/*18*/		, DestinationWaitBillableMinutes 
/*19*/		, DestinationWaitBillableHours
/*20*/		, DestinationWaitAmount 
/*21*/		, OrderRejectRateID 
/*22*/		, OrderRejectAmount 
/*23*/		, FuelSurchargeRateID 
/*24*/		, FuelSurchargeRate 
/*25*/		, FuelSurchargeAmount 
/*26*/		, OriginTaxRate 
/*27*/		, TotalAmount 
/*28*/		, CreatedByUser)
		SELECT 
/*01*/		OrderID
/*02*/		, OrderDate
/*03*/		, ShipperSettlementFactorID
/*04*/		, SettlementFactorID 
/*05*/		, SettlementUomID 
/*06*/		, MinSettlementUnitsID
/*07*/		, MinSettlementUnits 
/*08*/		, SettlementUnits
/*09*/		, RouteRateID
/*10*/		, RangeRateID 
/*11*/		, LoadAmount
/*12*/		, WaitFeeParameterID 
/*13*/		, OriginWaitRateID 
/*14*/		, OriginWaitBillableMinutes 
/*15*/		, OriginWaitBillableHours
/*16*/		, OriginWaitAmount 
/*17*/		, DestWaitRateID 
/*18*/		, DestWaitBillableMinutes 
/*19*/		, DestWaitBillableHours
/*20*/		, DestWaitAmount 
/*21*/		, OrderRejectRateID 
/*22*/		, OrderRejectAmount 
/*23*/		, FuelSurchargeRateID 
/*24*/		, FuelSurchargeRate 
/*25*/		, FuelSurchargeAmount 
/*26*/		, OriginTaxRate 
/*27*/		, TotalAmount 
/*28*/		, CreatedByUser
		FROM #I

		/* compute the new Assessorial Rates for this order - into temp table #IA */
		SELECT OrderID = @ID, AssessorialRateTypeID, AssessorialRateID = RateID, Amount = ROUND(Amount, 2), CreatedByUser = @UserName 
		INTO #IA
		FROM @SettlementUnits SU
		CROSS APPLY dbo.fnOrderShipperAssessorialAmounts(@ID, SU.SettlementUnits, SU.SettlementUomID) CAA
		WHERE CAA.AssessorialRateTypeID NOT IN (SELECT ID FROM @ManualAssessorialRates)

		/* apply the computed settlement assessorial rate/amount records to the DB */
		INSERT INTO tblOrderSettlementShipperAssessorialCharge (OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser)
			SELECT OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser FROM #IA
			UNION
			SELECT @ID, ID, NULL, Amount, @UserName FROM @ManualAssessorialRates

		IF (@CreatedTran = 1)
			COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @msg varchar(max), @severity int
		SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY()
		ROLLBACK
		RAISERROR(@msg, @severity, 1)
		RETURN
	END CATCH	
	
END

GO



COMMIT
SET NOEXEC OFF