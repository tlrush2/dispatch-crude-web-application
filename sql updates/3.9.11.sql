-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.9.10.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.9.10.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0

SET NOEXEC OFF
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.9.10'
SELECT  @NewVersion = '3.9.11'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DriverApp Sync: resolve major performance issue regarding signature PUSH sync to client: add spOrderSignatures_DriverApp'
	UNION SELECT @NewVersion, 0, 'GaugerApp Sync: resolve major performance issue regarding signature PUSH sync to client: add spOrderSignatures_GaugerApp'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

EXEC _spDropProcedure 'spOrderSignatures_DriverApp'
GO
/*******************************************/
-- Date Created: 2015/09/01
-- Author: Kevin Alons
-- Purpose: return Signature records for the specified driver (that are still relevant at this time)
-- Changes: 
/*******************************************/
CREATE PROCEDURE spOrderSignatures_DriverApp(@DriverID int, @LastChangeDateUTC datetime = NULL) AS
BEGIN
	SELECT OS.ID, OS.UID, OS.DriverID, OS.OrderID, OS.OriginID, OS.DestinationID, OS.SignatureBlob, OS.CreateDateUTC, OS.CreatedByUser
	FROM dbo.tblOrder O
	JOIN dbo.tblOrderSignature OS ON OS.OrderID = O.ID
	CROSS JOIN dbo.fnSyncLCDOffset(@LastChangeDateUTC) LCD
	-- all orders are being fully resent to the client (otherwise the client already sent us this record and won't change on the server so don't emit back)
	WHERE @LastChangeDateUTC IS NULL
		AND O.DriverID = @driverID 
		AND O.StatusID IN (2,7,8,3) 
		AND (
			O.CreateDateUTC >= LCD.LCD
			OR O.LastChangeDateUTC >= LCD.LCD
			OR O.AcceptLastChangeDateUTC >= LCD.LCD
			OR O.PickupLastChangeDateUTC >= LCD.LCD
			OR O.DeliverLastChangeDateUTC >= LCD.LCD
		)
END
GO
GRANT EXECUTE ON spOrderSignatures_DriverApp TO role_iis_acct
GO

EXEC _spDropProcedure 'spOrderSignatures_GaugerApp'
GO
/*******************************************/
-- Date Created: 2015/09/01
-- Author: Kevin Alons
-- Purpose: return Signature records for the specified gauger (that are still relevant at this time)
-- Changes: 
/*******************************************/
CREATE PROCEDURE spOrderSignatures_GaugerApp(@GaugerID int, @LastChangeDateUTC datetime = NULL) AS
BEGIN
	SELECT OS.ID, OS.UID, OS.GaugerID, OS.OrderID, OS.OriginID, OS.DestinationID, OS.SignatureBlob, OS.CreateDateUTC, OS.CreatedByUser
	FROM dbo.tblOrder O
	JOIN dbo.tblGaugerOrder GAO ON GAO.OrderID = O.ID
	JOIN dbo.tblOrderSignature OS ON OS.OrderID = O.ID
	CROSS JOIN fnSyncLCDOffset(@LastChangeDateUTC) LCD
	-- all orders are being fully resent to the client (otherwise the client already sent us this record and won't change on the server so don't emit back)
	WHERE @LastChangeDateUTC IS NULL
		AND GAO.GaugerID = @gaugerID 
		AND O.StatusID IN (2,7,8,3) 
		AND (
			O.CreateDateUTC >= LCD.LCD
			OR O.LastChangeDateUTC >= LCD.LCD
			OR O.AcceptLastChangeDateUTC >= LCD.LCD
			OR O.PickupLastChangeDateUTC >= LCD.LCD
			OR O.DeliverLastChangeDateUTC >= LCD.LCD
		)
END
GO
GRANT EXECUTE ON spOrderSignatures_GaugerApp TO role_iis_acct
GO

COMMIT
SET NOEXEC OFF