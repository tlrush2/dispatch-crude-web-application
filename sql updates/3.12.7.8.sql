SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.12.7.7'
SELECT  @NewVersion = '3.12.7.8'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Fix to old Shipper vs. Carrier reference in Report Center '
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/***********************************
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Report Center Order data
-- Changes:
   -		 - 05/21/15	 - GSM - Adding Gauger Process data elements to Report Center
   -		 - 06/08/15	 - BB  - Add "OriginCity" and "OriginCityState" columns			
   -		 - 06/17/15  - BB  - Add "DestCity" and "DestCityState" columns
   - 3.7.39 - 2015/06/30 - BB  - Add "Shipper Ticket Type" column
   - 3.8.20 - 2015/08/26 - BB  - Add Order Create Date column
   - 3.9.14 - 2015/09/08 - BB  - Add Order Approved column
   - 3.9.20 - 2015/10/22 - BB  - Add Origin|Dest Driver fields
						 - JAE - Added Origin Truck field 
   - 3.9.25 - 2015/11/09 - BB  - DCWEB-925 - Add OriginDriverGroup field
            - 2015/11/10 - JAE - Drop driver and truck fields since they now appear in viewOrder
            - 2015/11/16 - BB  - Add Driver GPS & DTP columns for Pickup/Delivery
			- 2015/11/17 - BB  - Changed new GPS & DTP columns to use fnReportCenterDriverGPS & fnReportCenterDriverDTP
   - 3.9.29 - 2015/11/21 - KDA - reorganize view to optimize performance (use a sub-query to avoid executing the costly GPS First@Point views over the entire table)
							   - remove some GPS fields (using the fnOrderDriverLocationDTP & similar functions), for performance reasons
   - 3.11.17.3 - 2016/04/14 - JAE - Added Producer settlement
   - 3.12.7.8 - 2016/06/24 - JAE - Fix Shipper vs. Carrier references
***********************************/
ALTER VIEW [dbo].[viewReportCenter_Orders] AS
	SELECT X.*
		, OriginGpsLatLon = LTRIM(DLO.LAT) + ',' + LTRIM(DLO.LON)
		, OriginAccuracyMeters = DLO.SourceAccuracyMeters
		, OriginDistance = ISNULL(cast(DLO.DistanceToPoint AS INT), 99999)
		, OriginGpsArrived = CASE WHEN DLO.DistanceToPoint IS NOT NULL AND DLO.DistanceToPoint BETWEEN 0 AND OriginGeoFenceRadiusMeters THEN 1 ELSE 0 END
		, DestGpsLatLon = LTRIM(DLD.LAT) + ',' + LTRIM(DLD.Lon)
		, DestAccuracyMeters = DLD.SourceAccuracyMeters
		, DestDistance = ISNULL(CAST(DLD.DistanceToPoint AS INT), 99999)
		, DestGpsArrived = CASE WHEN DLD.DistanceToPoint IS NOT NULL AND DLD.DistanceToPoint BETWEEN 0 AND DestGeoFenceRadiusMeters THEN 1 ELSE 0 END		

/*	-- these should be ultimately be created as an EXPRESSION (90000 series) tblReportColumnDefinition record with DataField LIKE 'dbo.fnOrderDriverLocationDTP(RS.ID, 2, RS.OriginID, NULL)' 
		, DriverDTPPickupArrive = dbo.fnOrderDriverLocationDTP(X.ID, 2, X.OriginID, NULL)
		, DriverDTPPickupDepart = dbo.fnOrderDriverLocationDTP(X.ID, 3, X.OriginID, NULL)
		, DriverGPSPickupArrive = dbo.fnOrderDriverLocation(X.ID, 2, 1)
		, DriverGPSPickupDepart = dbo.fnOrderDriverLocation(X.ID, 3, 1)
		--
		, DriverDTPDeliverArrive = dbo.fnOrderDriverLocationDTP(X.ID, 2, NULL, X.DestinationID)
		, DriverDTPDeliverDepart = dbo.fnOrderDriverLocationDTP(X.ID, 3, NULL, X.DestinationID)
		, DriverGPSDeliverArrive = dbo.fnOrderDriverLocation(X.ID, 2, 0)
		, DriverGPSDeliverDepart = dbo.fnOrderDriverLocation(X.ID, 3, 0)
*/
	FROM (
		SELECT O.*
			--
			, ShipperBatchNum = SS.BatchNum
			, ShipperBatchInvoiceNum = SSB.InvoiceNum
			, ShipperSettlementUomID = SS.SettlementUomID
			, ShipperSettlementUom = SS.SettlementUom
			, ShipperMinSettlementUnits = SS.MinSettlementUnits
			, ShipperSettlementUnits = SS.SettlementUnits
			, ShipperRateSheetRate = SS.RateSheetRate
			, ShipperRateSheetRateType = SS.RateSheetRateType
			, ShipperRouteRate = SS.RouteRate
			, ShipperRouteRateType = SS.RouteRateType
			, ShipperLoadRate = ISNULL(SS.RouteRate, SS.RateSheetRate)
			, ShipperLoadRateType = ISNULL(SS.RouteRateType, SS.RateSheetRateType)
			, ShipperLoadAmount = SS.LoadAmount
			, ShipperOrderRejectRate = SS.OrderRejectRate 
			, ShipperOrderRejectRateType = SS.OrderRejectRateType 
			, ShipperOrderRejectAmount = SS.OrderRejectAmount 
			, ShipperWaitFeeSubUnit = SS.WaitFeeSubUnit  
			, ShipperWaitFeeRoundingType = SS.WaitFeeRoundingType  
			, ShipperOriginWaitBillableHours = SS.OriginWaitBillableHours  
			, ShipperOriginWaitBillableMinutes = SS.OriginWaitBillableMinutes  
			, ShipperOriginWaitRate = SS.OriginWaitRate
			, ShipperOriginWaitAmount = SS.OriginWaitAmount
			, ShipperDestinationWaitBillableHours = SS.DestinationWaitBillableHours 
			, ShipperDestinationWaitBillableMinutes = SS.DestinationWaitBillableMinutes   
			, ShipperDestinationWaitRate = SS.DestinationWaitRate  
			, ShipperDestinationWaitAmount = SS.DestinationWaitAmount  
			, ShipperTotalWaitAmount = SS.TotalWaitAmount
			, ShipperTotalWaitBillableMinutes = NULLIF(ISNULL(SS.OriginWaitBillableMinutes, 0) + ISNULL(SS.DestinationWaitBillableMinutes, 0), 0)
			, ShipperTotalWaitBillableHours = NULLIF(ISNULL(SS.OriginWaitBillableHours, 0) + ISNULL(SS.DestinationWaitBillableHours, 0), 0)		
			, ShipperFuelSurchargeRate = SS.FuelSurchargeRate
			, ShipperFuelSurchargeAmount = SS.FuelSurchargeAmount
			, ShipperChainupRate = SS.ChainupRate
			, ShipperChainupRateType = SS.ChainupRateType  
			, ShipperChainupAmount = SS.ChainupAmount
			, ShipperRerouteRate = SS.RerouteRate
			, ShipperRerouteRateType = SS.RerouteRateType  
			, ShipperRerouteAmount = SS.RerouteAmount
			, ShipperSplitLoadRate = SS.SplitLoadRate
			, ShipperSplitLoadRateType = SS.SplitLoadRateType  
			, ShipperSplitLoadAmount = SS.SplitLoadAmount
			, ShipperH2SRate = SS.H2SRate
			, ShipperH2SRateType = SS.H2SRateType  
			, ShipperH2SAmount = SS.H2SAmount
			, ShipperTaxRate = SS.OriginTaxRate
			, ShipperTotalAmount = SS.TotalAmount
			, ShipperDestCode = CDC.Code
			, ShipperTicketType = STT.TicketType
			--
			, CarrierBatchNum = SC.BatchNum
			, CarrierSettlementUomID = SC.SettlementUomID
			, CarrierSettlementUom = SC.SettlementUom
			, CarrierMinSettlementUnits = SC.MinSettlementUnits
			, CarrierSettlementUnits = SC.SettlementUnits
			, CarrierRateSheetRate = SC.RateSheetRate
			, CarrierRateSheetRateType = SC.RateSheetRateType
			, CarrierRouteRate = SC.RouteRate
			, CarrierRouteRateType = SC.RouteRateType
			, CarrierLoadRate = ISNULL(SC.RouteRate, SC.RateSheetRate)
			, CarrierLoadRateType = ISNULL(SC.RouteRateType, SC.RateSheetRateType)
			, CarrierLoadAmount = SC.LoadAmount
			, CarrierOrderRejectRate = SC.OrderRejectRate 
			, CarrierOrderRejectRateType = SC.OrderRejectRateType 
			, CarrierOrderRejectAmount = SC.OrderRejectAmount 
			, CarrierWaitFeeSubUnit = SC.WaitFeeSubUnit  
			, CarrierWaitFeeRoundingType = SC.WaitFeeRoundingType  
			, CarrierOriginWaitBillableHours = SC.OriginWaitBillableHours  
			, CarrierOriginWaitBillableMinutes = SC.OriginWaitBillableMinutes  
			, CarrierOriginWaitRate = SC.OriginWaitRate
			, CarrierOriginWaitAmount = SC.OriginWaitAmount
			, CarrierDestinationWaitBillableHours = SC.DestinationWaitBillableHours 
			, CarrierDestinationWaitBillableMinutes = SC.DestinationWaitBillableMinutes  
			, CarrierDestinationWaitRate = SC.DestinationWaitRate 
			, CarrierDestinationWaitAmount = SC.DestinationWaitAmount  
			, CarrierTotalWaitAmount = SC.TotalWaitAmount
			, CarrierTotalWaitBillableMinutes = NULLIF(ISNULL(SC.OriginWaitBillableMinutes, 0) + ISNULL(SC.DestinationWaitBillableMinutes, 0), 0)
			, CarrierTotalWaitBillableHours = NULLIF(ISNULL(SC.OriginWaitBillableHours, 0) + ISNULL(SC.DestinationWaitBillableHours, 0), 0)		
			, CarrierFuelSurchargeRate = SC.FuelSurchargeRate
			, CarrierFuelSurchargeAmount = SC.FuelSurchargeAmount
			, CarrierChainupRate = SC.ChainupRate
			, CarrierChainupRateType = SC.ChainupRateType  
			, CarrierChainupAmount = SC.ChainupAmount
			, CarrierRerouteRate = SC.RerouteRate
			, CarrierRerouteRateType = SC.RerouteRateType  
			, CarrierRerouteAmount = SC.RerouteAmount
			, CarrierSplitLoadRate = SC.SplitLoadRate
			, CarrierSplitLoadRateType = SC.SplitLoadRateType  
			, CarrierSplitLoadAmount = SC.SplitLoadAmount
			, CarrierH2SRate = SC.H2SRate
			, CarrierH2SRateType = SC.H2SRateType  
			, CarrierH2SAmount = SC.H2SAmount
			, CarrierTaxRate = SC.OriginTaxRate
			, CarrierTotalAmount = SC.TotalAmount
			--
			, ProducerBatchNum = SP.BatchNum
			, ProducerSettlementUomID = SP.SettlementUomID
			, ProducerSettlementUom = SP.SettlementUom
			, ProducerSettlementUnits = SP.SettlementUnits
			, ProducerCommodityIndex = SP.CommodityIndex
			, ProducerCommodityIndexID = SP.CommodityIndexID
			, ProducerCommodityMethod = SP.CommodityMethod
			, ProducerCommodityMethodID = SP.CommodityMethodID
			, ProducerIndexPrice = SP.Price
			, ProducerIndexStartDate = SP.IndexStartDate
			, ProducerIndexEndDate = SP.IndexEndDate
			, ProducerPriceAmount = SP.CommodityPurchasePricebookPriceAmount
			, ProducerDeduct = SP.Deduct
			, ProducerDeductType = SP.Deduct
			, ProducerDeductAmount = SP.CommodityPurchasePricebookDeductAmount
			, ProducerPremium = SP.PremiumType
			, ProducerPremiumType = SP.PremiumType
			, ProducerPremiumAmount = SP.CommodityPurchasePricebookPremiumAmount
			, ProducerPremiumDesc = SP.PremiumDesc
			, ProducerTotalAmount = SP.CommodityPurchasePricebookTotalAmount
			--
			, OriginLatLon = LTRIM(OO.LAT) + ',' + LTRIM(OO.LON)
			, OriginGeoFenceRadiusMeters = OO.GeoFenceRadiusMeters
			, OriginCTBNum = OO.CTBNum
			, OriginFieldName = OO.FieldName
			, OriginCity = OO.City																				
			, OriginCityState = OO.City + ', ' + (SELECT Abbreviation FROM tblState WHERE ID = OO.StateID)		
			--
			, DestLatLon = LTRIM(D.LAT) + ',' + LTRIM(D.LON)
			, DestGeoFenceRadiusMeters = D.GeoFenceRadiusMeters
			, DestCity = D.City
			, DestCityState = D.City + ', ' + (SELECT Abbreviation FROM tblState WHERE ID = OO.StateID)
			--
			, Gauger = GAO.Gauger						
			, GaugerIDNumber = GA.IDNumber
			, GaugerFirstName = GA.FirstName
			, GaugerLastName = GA.LastName
			, GaugerRejected = GAO.Rejected
			, GaugerRejectReasonID = GAO.RejectReasonID
			, GaugerRejectNotes = GAO.RejectNotes
			, GaugerRejectNumDesc = GORR.NumDesc
			, GaugerPrintDate = dbo.fnUTC_to_Local(GAO.PrintDateUTC, O.OriginTimeZoneID, O.OriginUseDST)
			--
			, T_GaugerCarrierTicketNum = CASE WHEN GAO.TicketCount = 0 THEN LTRIM(GAO.OrderNum) + CASE WHEN GAO.Rejected = 1 THEN 'X' ELSE '' END ELSE GOT.CarrierTicketNum END 
			, T_GaugerTankNum = ISNULL(GOT.OriginTankText, GAO.OriginTankText)
			, T_GaugerIsStrappedTank = GOT.IsStrappedTank 
			, T_GaugerProductObsTemp = GOT.ProductObsTemp
			, T_GaugerProductObsGravity = GOT.ProductObsGravity
			, T_GaugerProductBSW = GOT.ProductBSW		
			, T_GaugerOpeningGaugeFeet = GOT.OpeningGaugeFeet
			, T_GaugerOpeningGaugeInch = GOT.OpeningGaugeInch		
			, T_GaugerOpeningGaugeQ = GOT.OpeningGaugeQ			
			, T_GaugerBottomFeet = GOT.BottomFeet
			, T_GaugerBottomInches = GOT.BottomInches		
			, T_GaugerBottomQ = GOT.BottomQ		
			, T_GaugerRejected = GOT.Rejected
			, T_GaugerRejectReasonID = GOT.RejectReasonID
			, T_GaugerRejectNumDesc = GOT.RejectNumDesc
			, T_GaugerRejectNotes = GOT.RejectNotes	
			--
			, OrderCreateDate = dbo.fnUTC_to_Local(O.CreateDateUTC, O.OriginTimeZoneID, O.OriginUseDST)
			, OrderApproved = CAST(ISNULL(OA.Approved,0) AS BIT)
		FROM viewOrder_OrderTicket_Full O
		JOIN tblOrigin OO ON OO.ID = O.OriginID
		JOIN tblDestination D ON D.ID = O.DestinationID
		--
		LEFT JOIN viewGaugerOrder GAO ON GAO.OrderID = O.ID			            
		LEFT JOIN viewGaugerOrderTicket GOT ON GOT.UID = O.T_UID	            
		LEFT JOIN viewGauger GA ON GA.ID = GAO.GaugerID				            
		LEFT JOIN viewOrderRejectReason GORR ON GORR.ID = GAO.RejectReasonID 
		--
		LEFT JOIN tblCustomerDestinationCode CDC ON CDC.CustomerID = O.CustomerID AND CDC.DestinationID = O.DestinationID
		LEFT JOIN viewOrderSettlementCarrier SC ON SC.OrderID = O.ID
		LEFT JOIN viewOrderSettlementShipper SS ON SS.OrderID = O.ID
		LEFT JOIN tblShipperSettlementBatch SSB ON SSB.ID = SS.BatchID
		LEFT JOIN viewOrderSettlementProducer SP ON SP.OrderID = O.ID
		LEFT JOIN tblShipperTicketType AS STT ON STT.CustomerID = O.CustomerID AND STT.TicketTypeID = O.TicketTypeID
		--
		LEFT JOIN tblOrderApproval AS OA ON OA.OrderID = O.ID
	) X
	LEFT JOIN viewDriverLocation_OriginFirstArrive DLO ON DLO.OrderID = X.ID AND DLO.OriginID = X.OriginID
	LEFT JOIN viewDriverLocation_DestinationFirstArrive DLD ON DLD.OrderID = X.ID AND DLD.DestinationID = X.DestinationID


GO

EXEC sp_refreshview viewReportCenter_Orders
GO


COMMIT
SET NOEXEC OFF