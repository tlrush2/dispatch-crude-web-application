/* revise all relevant datetime fields into **UTC fields and other changes to faciliate Android DriverApp
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '1.7.4', @NewVersion = '2.0.0'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

CREATE TABLE tblProduct
(
  ID int identity(1, 1) not null
, Name varchar(100) not null
, ShortName varchar(25) not null
, CONSTRAINT PK_Product PRIMARY KEY CLUSTERED (ID)
)
GO
GRANT SELECT, INSERT, UPDATE ON tblProduct TO dispatchcrude_iis_acct
GO
CREATE UNIQUE INDEX udxProduct_Name ON tblProduct(Name)
GO
CREATE UNIQUE INDEX udxProduct_ShortName ON tblProduct(ShortName)
GO

INSERT INTO tblProduct (Name, ShortName) VALUES ('Petroleum Crude Oil, 3, UN 1267, PG III', 'Crude Oil')
GO

CREATE TABLE tblTimeZone
(
  ID tinyint identity(1, 1) not null
, Name varchar(100) not null
, SystemName varchar(100) not null
, StandardAbbrev char(3) not null
, DaylightAbbrev char(3) not null
, StandardOffsetHours int not null
, DaylightOffsetHours int not null
, CONSTRAINT PK_TimeZone PRIMARY KEY (ID)
)
GO
GRANT SELECT ON tblTimeZone TO dispatchcrude_iis_acct
GO
CREATE UNIQUE INDEX udxTimeZone_Name ON tblTimeZone(Name)
GO
CREATE UNIQUE INDEX udxTimeZone_SystemName ON tblTimeZone(SystemName)
GO
CREATE UNIQUE INDEX udxTimeZone_StandardAbbrev ON tblTimeZone(StandardAbbrev)
GO
CREATE UNIQUE INDEX udxTimeZone_DaylightAbbrev ON tblTimeZone(DaylightAbbrev)
GO

INSERT INTO tblTimeZone (Name, SystemName, StandardAbbrev, DaylightAbbrev, StandardOffsetHours,DaylightOffsetHours)
	SELECT 'Eastern', 'Eastern Standard Time', 'EST', 'EDT', -5, -4
UNION SELECT 'Central', 'Central Standard Time', 'CST', 'CDT', -6, -5
UNION SELECT 'Mountain', 'Mountain Standard Time', 'MST', 'MDT', -7, -6
UNION SELECT 'Pacific', 'Pacific Standard Time', 'PST', 'PDT', -8, -7
GO

CREATE TABLE tblDaylightSavingsPeriod
(
  [Year] smallint not null
, StartDate smalldatetime not null
, EndDate smalldatetime not null
, CONSTRAINT PK_DaylightSavingsPeriod PRIMARY KEY ([Year])
)
GO
GRANT SELECT, INSERT, UPDATE, DELETE TO dispatchcrude_iis_acct
GO
INSERT INTO tblDaylightSavingsPeriod VALUES(2007, '3/11/2007 02:00', '11/4/2007 02:00')
INSERT INTO tblDaylightSavingsPeriod VALUES(2008, '3/9/2008 02:00', '11/2/2008 02:00')
INSERT INTO tblDaylightSavingsPeriod VALUES(2009, '3/8/2009 02:00', '11/1/2009 02:00')
INSERT INTO tblDaylightSavingsPeriod VALUES(2010, '3/14/2010 02:00', '11/7/2010 02:00')
INSERT INTO tblDaylightSavingsPeriod VALUES(2011, '3/13/2011 02:00', '11/6/2011 02:00')
INSERT INTO tblDaylightSavingsPeriod VALUES(2012, '3/11/2012 02:00', '11/4/2012 02:00')
INSERT INTO tblDaylightSavingsPeriod VALUES(2013, '3/10/2013 02:00', '11/3/2013 02:00')
INSERT INTO tblDaylightSavingsPeriod VALUES(2014, '3/9/2014 02:00', '11/2/2014 02:00')
INSERT INTO tblDaylightSavingsPeriod VALUES(2015, '3/8/2015 02:00', '11/1/2015 02:00')
INSERT INTO tblDaylightSavingsPeriod VALUES(2016, '3/13/2016 02:00', '11/6/2016 02:00')
GO

/************************************************/
-- Create Date: 26 Aug 2013
-- Author: Kevin Alons
-- Purpose: convert a UTC DateTime to local (which is specified)
/************************************************/
CREATE FUNCTION fnUTC_to_Local(@utc datetime, @timeZoneID tinyint, @useDST bit) RETURNS datetime AS
BEGIN
	DECLARE @ret datetime
	SELECT @ret = dateadd(hour
		, CASE WHEN @utc BETWEEN DSP.StartDate AND DSP.EndDate AND @useDST = 1 THEN TZ.DaylightOffsetHours ELSE TZ.StandardOffsetHours END
		, @utc)
	FROM tblTimeZone TZ, tblDaylightSavingsPeriod DSP
	WHERE TZ.ID = @timeZoneID AND DSP.Year = Year(@utc)
	RETURN @ret
END
GO
GRANT EXECUTE ON fnUTC_to_Local TO dispatchcrude_iis_acct
GO

/************************************************/
-- Create Date: 26 Aug 2013
-- Author: Kevin Alons
-- Purpose: convert a local DateTime to UTC
/************************************************/
CREATE FUNCTION fnLocal_to_UTC(@local datetime, @timeZoneID tinyint, @useDST bit) RETURNS datetime AS
BEGIN
	DECLARE @ret datetime
	SELECT @ret = dateadd(hour
		, -CASE WHEN @local BETWEEN DSP.StartDate AND DSP.EndDate AND @useDST = 1 THEN TZ.DaylightOffsetHours ELSE TZ.StandardOffsetHours END
		, @local)
	FROM tblTimeZone TZ, tblDaylightSavingsPeriod DSP
	WHERE TZ.ID = @timeZoneID AND DSP.Year = Year(@local)
	RETURN @ret
END
GO
GRANT EXECUTE ON fnLocal_to_UTC TO dispatchcrude_iis_acct
GO

/************************************************/
-- Create Date: 26 Aug 2013
-- Author: Kevin Alons
-- Purpose: return the Time Zone Abbreviation for the specified time/timezone/useDST combination
/************************************************/
CREATE FUNCTION fnTimeZoneAbbrev(@datetime datetime, @timeZoneID tinyint, @useDST bit) RETURNS char(3) AS
BEGIN
	DECLARE @ret char(3)
	SELECT @ret = CASE WHEN @datetime IS NULL THEN NULL 
						WHEN @datetime BETWEEN DSP.StartDate AND DSP.EndDate AND @useDST = 1 THEN TZ.DaylightAbbrev 
						ELSE TZ.StandardAbbrev END
	FROM tblTimeZone TZ, tblDaylightSavingsPeriod DSP
	WHERE TZ.ID = @timeZoneID AND DSP.Year = Year(@datetime)
	RETURN @ret
END
GO
GRANT EXECUTE ON fnTimeZoneAbbrev TO dispatchcrude_iis_acct
GO

/************************************************/
-- Create Date: 26 Aug 2013
-- Author: Kevin Alons
-- Purpose: convert a Central Time Zone local time to UTC (Z)
/************************************************/
CREATE FUNCTION fnCTZ_to_UTC(@local datetime) RETURNS datetime AS 
BEGIN
	DECLARE @ret datetime
	SELECT @ret = dbo.fnLocal_to_UTC(@local, 1, 1)
	RETURN @ret
END
GO
GRANT EXECUTE ON fnCTZ_TO_UTC TO dispatchcrude_iis_acct
GO

ALTER TABLE tblCarrier Add PrintHeaderBlob varbinary(max) NULL
GO
ALTER TABLE tblCarrier Add PrintHeaderFileName varchar(255) NULL
GO

EXEC sp_Rename 'tblCarrier.CreateDate', 'CreateDateUTC', 'COLUMN'
GO
ALTER TABLE tblCarrier DROP CONSTRAINT DF_tblCarrier_CreateDate
GO
ALTER TABLE tblCarrier ADD CONSTRAINT DF_Carrier_CreateDateUTC  DEFAULT (getutcdate()) FOR CreateDateUTC
GO
EXEC sp_Rename 'tblCarrier.LastChangeDate', 'LastChangeDateUTC', 'COLUMN'
GO
EXEC sp_Rename 'tblCarrier.DeleteDate', 'DeleteDateUTC', 'COLUMN'
GO
UPDATE tblCarrier SET CreateDateUTC = dbo.fnCTZ_TO_UTC(CreateDateUTC)
	, LastChangeDateUTC = dbo.fnCTZ_TO_UTC(LastChangeDateUTC)
	, DeleteDateUTC = dbo.fnCTZ_TO_UTC(DeleteDateUTC)
GO

EXEC sp_Rename 'tblCarrierRates.CreateDate', 'CreateDateUTC', 'COLUMN'
GO
ALTER TABLE tblCarrierRates DROP CONSTRAINT DF_tblCarrierRates_CreateDate
GO
ALTER TABLE tblCarrierRates ADD CONSTRAINT DF_CarrierRates_CreateDateUTC  DEFAULT (getutcdate()) FOR CreateDateUTC
GO
EXEC sp_Rename 'tblCarrierRates.LastChangeDate', 'LastChangeDateUTC', 'COLUMN'
GO
UPDATE tblCarrierRates SET CreateDateUTC = dbo.fnCTZ_TO_UTC(CreateDateUTC)
	, LastChangeDateUTC = dbo.fnCTZ_TO_UTC(LastChangeDateUTC)
GO

EXEC sp_Rename 'tblCarrierRouteRates.CreateDate', 'CreateDateUTC', 'COLUMN'
GO
ALTER TABLE tblCarrierRouteRates DROP CONSTRAINT DF_tblCarrierRouteRates_CreateDate
GO
ALTER TABLE tblCarrierRouteRates ADD CONSTRAINT DF_CarrierRouteRates_CreateDateUTC  DEFAULT (getutcdate()) FOR CreateDateUTC
GO
EXEC sp_Rename 'tblCarrierRouteRates.LastChangeDate', 'LastChangeDateUTC', 'COLUMN'
GO
UPDATE tblCarrierRouteRates SET CreateDateUTC = dbo.fnCTZ_TO_UTC(CreateDateUTC)
	, LastChangeDateUTC = dbo.fnCTZ_TO_UTC(LastChangeDateUTC)
GO

ALTER TABLE tblCarrierSettlementBatch ADD BatchDate smalldatetime NOT NULL CONSTRAINT DF_CarrierSettlementBatch_BatchDate DEFAULT (getdate())
GO
UPDATE tblCarrierSettlementBatch SET BatchDate = dbo.fnDateOnly(CreateDate)
GO
EXEC sp_Rename 'tblCarrierSettlementBatch.CreateDate', 'CreateDateUTC', 'COLUMN'
GO
ALTER TABLE tblCarrierSettlementBatch ADD CONSTRAINT DF_CarrierSettlementBatch_CreateDateUTC  DEFAULT (getutcdate()) FOR CreateDateUTC
GO 
EXEC sp_Rename 'tblCarrierSettlementBatch.LastChangeDate', 'LastChangeDateUTC', 'COLUMN'
GO
UPDATE tblCarrierSettlementBatch SET CreateDateUTC = dbo.fnCTZ_TO_UTC(CreateDateUTC)
	, LastChangeDateUTC = dbo.fnCTZ_TO_UTC(LastChangeDateUTC)
GO

EXEC sp_Rename 'tblCarrierType.CreateDate', 'CreateDateUTC', 'COLUMN'
GO
ALTER TABLE tblCarrierType DROP CONSTRAINT DF_tblCarrierType_CreateDate
GO
ALTER TABLE tblCarrierType ADD CONSTRAINT DF_CarrierType_CreateDateUTC  DEFAULT (getutcdate()) FOR CreateDateUTC
GO
EXEC sp_Rename 'tblCarrierType.LastChangeDate', 'LastChangeDateUTC', 'COLUMN'
GO
UPDATE tblCarrierType SET CreateDateUTC = dbo.fnCTZ_TO_UTC(CreateDateUTC)
	, LastChangeDateUTC = dbo.fnCTZ_TO_UTC(LastChangeDateUTC)
GO

EXEC sp_Rename 'tblCustomer.CreateDate', 'CreateDateUTC', 'COLUMN'
GO
ALTER TABLE tblCustomer DROP CONSTRAINT DF_tblCustomer_CreateDate
GO
ALTER TABLE tblCustomer ADD CONSTRAINT DF_Customer_CreateDateUTC  DEFAULT (getutcdate()) FOR CreateDateUTC
GO
EXEC sp_Rename 'tblCustomer.LastChangeDate', 'LastChangeDateUTC', 'COLUMN'
GO
UPDATE tblCustomer SET CreateDateUTC = dbo.fnCTZ_TO_UTC(CreateDateUTC)
	, LastChangeDateUTC = dbo.fnCTZ_TO_UTC(LastChangeDateUTC)
GO

EXEC sp_Rename 'tblCustomerRates.CreateDate', 'CreateDateUTC', 'COLUMN'
GO
ALTER TABLE tblCustomerRates DROP CONSTRAINT DF_tblCustomerRates_CreateDate
GO
ALTER TABLE tblCustomerRates ADD CONSTRAINT DF_CustomerRates_CreateDateUTC  DEFAULT (getutcdate()) FOR CreateDateUTC
GO
EXEC sp_Rename 'tblCustomerRates.LastChangeDate', 'LastChangeDateUTC', 'COLUMN'
GO
UPDATE tblCustomerRates SET CreateDateUTC = dbo.fnCTZ_TO_UTC(CreateDateUTC)
	, LastChangeDateUTC = dbo.fnCTZ_TO_UTC(LastChangeDateUTC)
GO

EXEC sp_Rename 'tblCustomerRouteRates.CreateDate', 'CreateDateUTC', 'COLUMN'
GO
ALTER TABLE tblCustomerRouteRates DROP CONSTRAINT DF_tblCustomerRouteRates_CreateDate
GO
ALTER TABLE tblCustomerRouteRates ADD CONSTRAINT DF_CustomerRouteRates_CreateDateUTC  DEFAULT (getutcdate()) FOR CreateDateUTC
GO
EXEC sp_Rename 'tblCustomerRouteRates.LastChangeDate', 'LastChangeDateUTC', 'COLUMN'
GO
UPDATE tblCustomerRouteRates SET CreateDateUTC = dbo.fnCTZ_TO_UTC(CreateDateUTC)
	, LastChangeDateUTC = dbo.fnCTZ_TO_UTC(LastChangeDateUTC)
GO

ALTER TABLE tblCustomerSettlementBatch ADD BatchDate smalldatetime NOT NULL CONSTRAINT DF_CustomerSettlementBatch_BatchDate DEFAULT (getdate())
GO
UPDATE tblCustomerSettlementBatch SET BatchDate = dbo.fnDateOnly(CreateDate)
GO
EXEC sp_Rename 'tblCustomerSettlementBatch.CreateDate', 'CreateDateUTC', 'COLUMN'
GO
ALTER TABLE tblCustomerSettlementBatch ADD CONSTRAINT DF_CustomerSettlementBatch_CreateDateUTC  DEFAULT (getutcdate()) FOR CreateDateUTC
GO
EXEC sp_Rename 'tblCustomerSettlementBatch.LastChangeDate', 'LastChangeDateUTC', 'COLUMN'
GO
UPDATE tblCustomerSettlementBatch SET CreateDateUTC = dbo.fnCTZ_TO_UTC(CreateDateUTC)
	, LastChangeDateUTC = dbo.fnCTZ_TO_UTC(LastChangeDateUTC)
GO

ALTER TABLE tblDestination ADD TimeZoneID tinyint not null CONSTRAINT DF_Destination_TimeZone DEFAULT (1)
GO
ALTER TABLE tblDestination ADD UseDST bit not null CONSTRAINT DF_Destination_UseDST DEFAULT (1)
GO
ALTER TABLE tblDestination ADD CONSTRAINT FK_Destination_TimeZone FOREIGN KEY (TimeZoneID) REFERENCES tblTimeZone(ID)
GO  
-- update Montana, Wyoming, Colorado, New Mexico to Mountain
UPDATE tblDestination SET TimeZoneID = 3 WHERE StateID IN (6, 27, 32, 51)
GO
EXEC sp_Rename 'tblDestination.CreateDate', 'CreateDateUTC', 'COLUMN'
GO
ALTER TABLE tblDestination DROP CONSTRAINT DF_tblDestination_CreateDate
GO
ALTER TABLE tblDestination ADD CONSTRAINT DF_Destination_CreateDateUTC  DEFAULT (getutcdate()) FOR CreateDateUTC
GO
EXEC sp_Rename 'tblDestination.LastChangeDate', 'LastChangeDateUTC', 'COLUMN'
GO
EXEC sp_Rename 'tblDestination.DeleteDate', 'DeleteDateUTC', 'COLUMN'
GO
UPDATE tblDestination SET CreateDateUTC = dbo.fnCTZ_TO_UTC(CreateDateUTC)
	, LastChangeDateUTC = dbo.fnCTZ_TO_UTC(LastChangeDateUTC)
	, DeleteDateUTC = dbo.fnCTZ_TO_UTC(DeleteDateUTC)
GO

EXEC sp_Rename 'tblDestinationType.CreateDate', 'CreateDateUTC', 'COLUMN'
GO
ALTER TABLE tblDestinationType DROP CONSTRAINT DF_tblDestinationType_CreateDate
GO
ALTER TABLE tblDestinationType ADD CONSTRAINT DF_DestinationType_CreateDateUTC  DEFAULT (getutcdate()) FOR CreateDateUTC
GO
EXEC sp_Rename 'tblDestinationType.LastChangeDate', 'LastChangeDateUTC', 'COLUMN'
GO
UPDATE tblDestinationType SET CreateDateUTC = dbo.fnCTZ_TO_UTC(CreateDateUTC)
	, LastChangeDateUTC = dbo.fnCTZ_TO_UTC(LastChangeDateUTC)
GO

EXEC sp_Rename 'tblDriver.CreateDate', 'CreateDateUTC', 'COLUMN'
GO
ALTER TABLE tblDriver DROP CONSTRAINT DF_tblDriver_CreateDate
GO
ALTER TABLE tblDriver ADD CONSTRAINT DF_Driver_CreateDateUTC  DEFAULT (getutcdate()) FOR CreateDateUTC
GO
EXEC sp_Rename 'tblDriver.LastChangeDate', 'LastChangeDateUTC', 'COLUMN'
GO
EXEC sp_Rename 'tblDriver.DeleteDate', 'DeleteDateUTC', 'COLUMN'
GO
UPDATE tblDriver SET CreateDateUTC = dbo.fnCTZ_TO_UTC(CreateDateUTC)
	, LastChangeDateUTC = dbo.fnCTZ_TO_UTC(LastChangeDateUTC)
	, DeleteDateUTC = dbo.fnCTZ_TO_UTC(DeleteDateUTC)
GO
ALTER TABLE tblDriver ADD MobilePrint bit NOT NULL CONSTRAINT DF_Driver_MobilePrint DEFAULT (0)
GO

sp_rename 'tblDriver_Sync.LastSync', 'LastSyncUTC', 'COLUMN'
GO

EXEC sp_Rename 'tblOperator.CreateDate', 'CreateDateUTC', 'COLUMN'
GO
ALTER TABLE tblOperator DROP CONSTRAINT DF_tblOperator_CreateDate
GO
ALTER TABLE tblOperator ADD CONSTRAINT DF_Operator_CreateDateUTC  DEFAULT (getutcdate()) FOR CreateDateUTC
GO
EXEC sp_Rename 'tblOperator.LastChangeDate', 'LastChangeDateUTC', 'COLUMN'
GO
UPDATE tblOperator SET CreateDateUTC = dbo.fnCTZ_TO_UTC(CreateDateUTC)
	, LastChangeDateUTC = dbo.fnCTZ_TO_UTC(LastChangeDateUTC)
GO

ALTER TABLE tblOrigin ADD TimeZoneID tinyint not null CONSTRAINT DF_Origin_TimeZone DEFAULT (1)
GO
ALTER TABLE tblOrigin ADD CONSTRAINT FK_Origin_TimeZone FOREIGN KEY (TimeZoneID) REFERENCES tblTimeZone(ID)
GO  
ALTER TABLE tblOrigin ADD UseDST bit not null CONSTRAINT DF_Origin_UseDST DEFAULT (1)
GO
-- update Montana, Wyoming, Colorado, New Mexico to Mountain
UPDATE tblOrigin SET TimeZoneID = 3 WHERE StateID IN (6, 27, 32, 51)
GO
EXEC sp_Rename 'tblOrigin.CreateDate', 'CreateDateUTC', 'COLUMN'
GO
ALTER TABLE tblOrigin DROP CONSTRAINT DF_tblOrigin_CreateDate
GO
ALTER TABLE tblOrigin ADD CONSTRAINT DF_Origin_CreateDateUTC  DEFAULT (getutcdate()) FOR CreateDateUTC
GO
EXEC sp_Rename 'tblOrigin.LastChangeDate', 'LastChangeDateUTC', 'COLUMN'
GO
UPDATE tblOrigin SET CreateDateUTC = dbo.fnCTZ_TO_UTC(CreateDateUTC)
	, LastChangeDateUTC = dbo.fnCTZ_TO_UTC(LastChangeDateUTC)
GO

EXEC sp_Rename 'tblRoute.CreateDate', 'CreateDateUTC', 'COLUMN'
GO
ALTER TABLE tblRoute DROP CONSTRAINT DF_tblRoute_CreateDate
GO
ALTER TABLE tblRoute ADD CONSTRAINT DF_Route_CreateDateUTC  DEFAULT (getutcdate()) FOR CreateDateUTC
GO
EXEC sp_Rename 'tblRoute.LastChangeDate', 'LastChangeDateUTC', 'COLUMN'
GO
UPDATE tblRoute SET CreateDateUTC = dbo.fnCTZ_TO_UTC(CreateDateUTC)
	, LastChangeDateUTC = dbo.fnCTZ_TO_UTC(LastChangeDateUTC)
GO

EXEC sp_Rename 'tblOrder.CreateDate', 'CreateDateUTC', 'COLUMN'
GO
ALTER TABLE tblOrder ADD ProductID int not null CONSTRAINT DF_Order_Product DEFAULT (1) 
GO
ALTER TABLE tblOrder ADD AcceptLastChangeDateUTC smalldatetime NULL
GO
ALTER TABLE tblOrder ADD PickupLastChangeDateUTC smalldatetime NULL
GO
ALTER TABLE tblOrder ADD DeliverLastChangeDateUTC smalldatetime NULL
GO

ALTER TABLE tblOrder DROP CONSTRAINT DF_tblOrder_CreateDate
GO
ALTER TABLE tblOrder ADD CONSTRAINT DF_Order_CreateDateUTC  DEFAULT (getutcdate()) FOR CreateDateUTC
GO
EXEC sp_Rename 'tblOrder.LastChangeDate', 'LastChangeDateUTC', 'COLUMN'
GO
EXEC sp_Rename 'tblOrder.DeleteDate', 'DeleteDateUTC', 'COLUMN'
GO
EXEC sp_Rename 'tblOrder.OriginArriveTime', 'OriginArriveTimeUTC', 'COLUMN'
GO
EXEC sp_Rename 'tblOrder.OriginDepartTime', 'OriginDepartTimeUTC', 'COLUMN'
GO
EXEC sp_Rename 'tblOrder.DestArriveTime', 'DestArriveTimeUTC', 'COLUMN'
GO
EXEC sp_Rename 'tblOrder.DestDepartTime', 'DestDepartTimeUTC', 'COLUMN'
GO 
-- =============================================
-- Author:		Kevin Alons
-- Create date: 19 Dec 2012
-- Description:	trigger to add a unique, incrementing OrderNum to each new Order (manual Identity column)
-- =============================================
ALTER TRIGGER [dbo].[trigOrder_IU] ON [dbo].[tblOrder] AFTER INSERT, UPDATE AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	WHILE (SELECT count(*) FROM inserted i JOIN tblOrder O ON O.ID = i.ID WHERE O.OrderNum IS NULL) > 0
	BEGIN
		UPDATE tblOrder 
		  SET OrderNum = isnull((SELECT max(OrderNum) FROM tblOrder), 100000) + 1
			, CreateDateUTC = getutcdate()
		WHERE ID = (SELECT min(O.ID) FROM tblOrder O JOIN inserted i ON i.ID = O.ID WHERE O.OrderNum IS NULL)
	END
	-- re-compute the OriginMinutes (in case the website failed to compute it properly)
	IF UPDATE(OriginArriveTimeUTC) OR UPDATE(OriginDepartTimeUTC)
	BEGIN
		UPDATE tblOrder SET OriginMinutes = datediff(minute, OriginArriveTimeUTC, OriginDepartTimeUTC)
	END
	-- re-compute the DestWaitMinutes (in case the website failed to compute it properly)
	IF UPDATE(DestArriveTimeUTC) OR UPDATE(DestDepartTimeUTC)
	BEGIN
		UPDATE tblOrder SET DestMinutes = datediff(minute, DestArriveTimeUTC, DestDepartTimeUTC)
	END
	
	-- ensure missing Routes are created and assigned for the specified Origin/Destination combination
	IF UPDATE(OriginID) OR UPDATE(DestinationID)
	BEGIN
		-- create any missing Route records
		INSERT INTO tblRoute (OriginID, DestinationID, CreateDateUTC, CreatedByUser)
			SELECT i.OriginID, i.DestinationID, GETUTCDATE(), ISNULL(i.LastChangedByUser, i.CreatedByUser)
			FROM inserted i
			LEFT JOIN tblRoute R ON R.OriginID = i.OriginID AND R.DestinationID = i.DestinationID
			WHERE i.OriginID IS NOT NULL AND i.DestinationID IS NOT NULL AND R.ID IS NULL
		-- ensure the Order records refer to the correct Route (ID)
		UPDATE tblOrder SET RouteID = R.ID
		FROM tblOrder O
		JOIN inserted i ON i.ID = O.ID
		LEFT JOIN tblRoute R ON R.OriginID = O.OriginID AND R.DestinationID = O.DestinationID
	END
END

GO

UPDATE tblOrder SET CreateDateUTC = dbo.fnLocal_to_UTC(O.CreateDateUTC, 1, 1)
	, LastChangeDateUTC = dbo.fnLocal_to_UTC(O.LastChangeDateUTC, 1, 1)
	, DeleteDateUTC = dbo.fnLocal_to_UTC(O.DeleteDateUTC, 1, 1)
	, OriginArriveTimeUTC = dbo.fnLocal_to_UTC(OriginArriveTimeUTC, OO.TimeZoneID, OO.UseDST)
	, OriginDepartTimeUTC = dbo.fnLocal_to_UTC(OriginDepartTimeUTC, OO.TimeZoneID, OO.UseDST)
	, DestArriveTimeUTC = dbo.fnLocal_to_UTC(DestArriveTimeUTC, OO.TimeZoneID, OO.UseDST)
	, DestDepartTimeUTC = dbo.fnLocal_to_UTC(DestDepartTimeUTC, OO.TimeZoneID, OO.UseDST)
FROM tblOrder O
JOIN tblOrigin OO ON OO.ID = O.OriginID
GO

EXEC sp_Rename 'tblOrderInvoiceCarrier.CreateDate', 'CreateDateUTC', 'COLUMN'
GO
ALTER TABLE tblOrderInvoiceCarrier DROP CONSTRAINT DF_tblOrderInvoiceCarrier_CreateDate
GO
ALTER TABLE tblOrderInvoiceCarrier ADD CONSTRAINT DF_OrderInvoiceCarrier_CreateDateUTC  DEFAULT (getutcdate()) FOR CreateDateUTC
GO
EXEC sp_Rename 'tblOrderInvoiceCarrier.LastChangeDate', 'LastChangeDateUTC', 'COLUMN'
GO
UPDATE tblOrderInvoiceCarrier SET CreateDateUTC = dbo.fnCTZ_TO_UTC(CreateDateUTC)
	, LastChangeDateUTC = dbo.fnCTZ_TO_UTC(LastChangeDateUTC)
GO
ALTER TABLE tblOrderInvoiceCarrier DROP CONSTRAINT DF_OrderInvoiceCarrier
GO
ALTER TABLE tblOrderInvoiceCarrier ADD CONSTRAINT DF_OrderInvoiceCarrier_H2SFee DEFAULT (0) FOR H2SFee
GO

EXEC sp_Rename 'tblOrderInvoiceCustomer.CreateDate', 'CreateDateUTC', 'COLUMN'
GO
ALTER TABLE tblOrderInvoiceCustomer DROP CONSTRAINT DF_tblOrderInvoiceCustomer_CreateDate
GO
ALTER TABLE tblOrderInvoiceCustomer ADD CONSTRAINT DF_OrderInvoiceCustomer_CreateDateUTC  DEFAULT (getutcdate()) FOR CreateDateUTC
GO
EXEC sp_Rename 'tblOrderInvoiceCustomer.LastChangeDate', 'LastChangeDateUTC', 'COLUMN'
GO
UPDATE tblOrderInvoiceCustomer SET CreateDateUTC = dbo.fnCTZ_TO_UTC(CreateDateUTC)
	, LastChangeDateUTC = dbo.fnCTZ_TO_UTC(LastChangeDateUTC)
GO
ALTER TABLE tblOrderInvoiceCustomer DROP CONSTRAINT DF_OrderInvoiceCustomer
GO
ALTER TABLE tblOrderInvoiceCustomer ADD CONSTRAINT DF_OrderInvoiceCustomer_H2SFee DEFAULT (0) FOR H2SFee
GO

EXEC sp_Rename 'tblOrderStatus.CreateDate', 'CreateDateUTC', 'COLUMN'
GO
ALTER TABLE tblOrderStatus DROP CONSTRAINT DF_tblOrderStatus_CreateDate
GO
ALTER TABLE tblOrderStatus ADD CONSTRAINT DF_OrderStatus_CreateDateUTC  DEFAULT (getutcdate()) FOR CreateDateUTC
GO
EXEC sp_Rename 'tblOrderStatus.LastChangeDate', 'LastChangeDateUTC', 'COLUMN'
GO
UPDATE tblOrderStatus SET CreateDateUTC = dbo.fnCTZ_TO_UTC(CreateDateUTC)
	, LastChangeDateUTC = dbo.fnCTZ_TO_UTC(LastChangeDateUTC)
GO

EXEC sp_Rename 'tblOrderTicket.CreateDate', 'CreateDateUTC', 'COLUMN'
GO
ALTER TABLE tblOrderTicket DROP CONSTRAINT DF_tblOrderTicket_CreateDate
GO
ALTER TABLE tblOrderTicket ADD CONSTRAINT DF_OrderTicket_CreateDateUTC  DEFAULT (getutcdate()) FOR CreateDateUTC
GO
EXEC sp_Rename 'tblOrderTicket.LastChangeDate', 'LastChangeDateUTC', 'COLUMN'
GO
EXEC sp_Rename 'tblOrderTicket.DeleteDate', 'DeleteDateUTC', 'COLUMN'
GO
-- =============================================
-- Author:		Kevin Alons
-- Create date: 6 Feb 2013
-- Description:	trigger to ensure the GrossBarrels and NetBarrels are computed for valid, entered data
-- =============================================
ALTER TRIGGER [dbo].[trigOrderTicket_IU] ON [dbo].[tblOrderTicket] AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	-- re-compute GaugeRun ticket Gross barrels
	IF UPDATE(OpeningGaugeFeet) OR UPDATE(OpeningGaugeInch) OR UPDATE(OpeningGaugeQ)
		OR UPDATE (ClosingGaugeFeet) OR UPDATE(ClosingGaugeInch) OR UPDATE(ClosingGaugeQ) OR UPDATE(BarrelsPerInch)
		OR UPDATE (GrossBarrels)
	BEGIN
		UPDATE tblOrderTicket
		  SET GrossBarrels = dbo.fnTankQtyBarrelsDelta(
				OpeningGaugeFeet
			  , OpeningGaugeInch
			  , OpeningGaugeQ
			  , ClosingGaugeFeet
			  , ClosingGaugeInch
			  , ClosingGaugeQ
			  , BarrelsPerInch)
		WHERE ID IN (SELECT ID FROM inserted) 
		  AND TicketTypeID = 1 -- Gauge Run Ticket
		  AND OpeningGaugeFeet IS NOT NULL
		  AND OpeningGaugeInch IS NOT NULL
		  AND OpeningGaugeQ IS NOT NULL
		  AND ClosingGaugeFeet IS NOT NULL
		  AND ClosingGaugeInch IS NOT NULL
		  AND ClosingGaugeQ IS NOT NULL
		  AND BarrelsPerInch IS NOT NULL
	END
	-- re-compute GaugeRun ticket Net Barrels
	IF UPDATE(OpeningGaugeFeet) OR UPDATE(OpeningGaugeInch) OR UPDATE(OpeningGaugeQ)
		OR UPDATE (ClosingGaugeFeet) OR UPDATE(ClosingGaugeInch) OR UPDATE(ClosingGaugeQ) 
		OR UPDATE(BarrelsPerInch) OR UPDATE(GrossBarrels) OR UPDATE(NetBarrels)
		OR UPDATE(ProductObsTemp) OR UPDATE(ProductObsGravity) OR UPDATE(ProductBSW)
	BEGIN
		UPDATE tblOrderTicket
		  SET NetBarrels = dbo.fnCrudeNetCalculator(GrossBarrels, ProductObsTemp, ProductObsGravity, ProductBSW)
		WHERE ID IN (SELECT ID FROM inserted)
		  AND TicketTypeID IN (1,2)
		  AND GrossBarrels IS NOT NULL
		  AND ProductObsTemp IS NOT NULL
		  AND ProductObsGravity IS NOT NULL
		  AND ProductBSW IS NOT NULL
	END
	-- ensure the Order record is in-sync with the Tickets
	UPDATE tblOrder 
	SET OriginGrossBarrels = (SELECT sum(GrossBarrels) FROM tblOrderTicket WHERE OrderID = O.ID AND Rejected = 0 AND DeleteDateUTC IS NULL)
	  , OriginNetBarrels = (SELECT sum(NetBarrels) FROM tblOrderTicket WHERE OrderID = O.ID AND Rejected = 0 AND DeleteDateUTC IS NULL)
		-- use the first MeterRun CarrierTicketNum num as the Order BOL Num
	  , OriginBOLNum = (SELECT min(BOLNum) FROM tblOrderTicket WHERE OrderID = O.ID AND TicketTypeID = 3 AND DeleteDateUTC IS NULL)
	  , CarrierTicketNum = (SELECT min(CarrierTicketNum) FROM tblOrderTicket WHERE ID IN 
		(SELECT min(ID) FROM tblOrderTicket WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL))
	  , LastChangeDateUTC = (SELECT MAX(LastChangeDateUTC) FROM inserted WHERE OrderID = O.ID)
	  , LastChangedByUser = (SELECT MIN(LastChangedByUser) FROM inserted WHERE OrderID = O.ID)
	FROM tblOrder O
	WHERE ID IN (SELECT DISTINCT OrderID FROM inserted)
END

GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 6 Feb 2013
-- Description:	trigger to ensure the entered values for an OrderTicket are actually valid
-- =============================================
ALTER TRIGGER [dbo].[trigOrderTicket_IU_Validate] ON [dbo].[tblOrderTicket] AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	IF (SELECT COUNT(*) FROM (
		SELECT OT.OrderID
		FROM tblOrderTicket OT
		WHERE OrderID IN (SELECT DISTINCT OrderID FROM inserted)
		  AND OT.DeleteDateUTC IS NULL
		GROUP BY OT.OrderID, OT.CarrierTicketNum
		HAVING COUNT(*) > 1) v) > 0
	BEGIN
		RAISERROR('Duplicate Ticket Numbers are not allowed', 16, 1)
		RETURN
	END
	
	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (3) AND BOLNum IS NULL) > 0
	BEGIN
		RAISERROR('BOL # value is required for Meter Run tickets', 16, 1)
		RETURN
	END
	
	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1, 2) AND TankNum IS NULL) > 0
	BEGIN
		RAISERROR('Tank # value is required for Gauge Run & Net Barrel tickets', 16, 1)
		RETURN
	END

	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1) AND CarrierTicketNum IS NULL) > 0
	BEGIN
		RAISERROR('Ticket # value is required for Gauge Run tickets', 16, 1)
		RETURN
	END

	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1) AND BarrelsPerInch IS NULL) > 0
	BEGIN
		RAISERROR('BarrelsPerInch value is required for Gauge Run tickets', 16, 1)
		RETURN
	END

	IF (SELECT COUNT(*) FROM inserted WHERE 
		(TicketTypeID IN (1, 2) AND (ProductOBsTemp IS NULL OR ProductObsGravity IS NULL OR ProductBSW IS NULL))
			OR (TicketTypeID IN (1) AND (ProductHighTemp IS NULL OR ProductLowTemp IS NULL))
		) > 0
	BEGIN
		RAISERROR('All Product Measurement values are required for Gauge Run & Net Barrel tickets', 16, 1)
		RETURN
	END
	
	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1) AND Rejected = 0 
		AND (OpeningGaugeFeet IS NULL OR OpeningGaugeInch IS NULL OR OpeningGaugeQ IS NULL)) > 0
	BEGIN
		RAISERROR('All Opening Gauge values are required for Gauge Run tickets', 16, 1)
		RETURN
	END
	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1) AND Rejected = 0 
		AND (ClosingGaugeFeet IS NULL OR ClosingGaugeInch IS NULL OR ClosingGaugeQ IS NULL)) > 0
	BEGIN
		RAISERROR('All Closing Gauge values are required for Gauge Run tickets', 16, 1)
		RETURN
	END

	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (2) AND Rejected = 0 
		AND (GrossBarrels IS NULL)) > 0
	BEGIN
		RAISERROR('Gross Barrel value is required for Net Barrel tickets', 16, 1)
		RETURN
	END

	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (3) AND Rejected = 0 
		AND (GrossBarrels IS NULL OR NetBarrels IS NULL)) > 0
	BEGIN
		RAISERROR('Gross & Net Barrel values are required for Meter Run tickets', 16, 1)
		RETURN
	END

	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1) AND Rejected = 0 
		AND (SealOff IS NULL OR SealOn IS NULL)) > 0
	BEGIN
		RAISERROR('All Seal Off & Seal On values are required for Gauge Run tickets', 16, 1)
		RETURN
	END

END

GO

UPDATE tblOrderTicket SET CreateDateUTC = dbo.fnCTZ_TO_UTC(CreateDateUTC)
	, LastChangeDateUTC = dbo.fnCTZ_TO_UTC(LastChangeDateUTC)
	, DeleteDateUTC = dbo.fnCTZ_TO_UTC(DeleteDateUTC)
GO

EXEC sp_Rename 'tblOriginType.CreateDate', 'CreateDateUTC', 'COLUMN'
GO
ALTER TABLE tblOriginType DROP CONSTRAINT DF_tblOriginType_CreateDate
GO
ALTER TABLE tblOriginType ADD CONSTRAINT DF_OriginType_CreateDateUTC  DEFAULT (getutcdate()) FOR CreateDateUTC
GO
EXEC sp_Rename 'tblOriginType.LastChangeDate', 'LastChangeDateUTC', 'COLUMN'
GO
UPDATE tblOriginType SET CreateDateUTC = dbo.fnCTZ_TO_UTC(CreateDateUTC)
	, LastChangeDateUTC = dbo.fnCTZ_TO_UTC(LastChangeDateUTC)
GO

EXEC sp_Rename 'tblPriority.CreateDate', 'CreateDateUTC', 'COLUMN'
GO
ALTER TABLE tblPriority DROP CONSTRAINT DF_tblPriority_CreateDate
GO
ALTER TABLE tblPriority ADD CONSTRAINT DF_Priority_CreateDateUTC  DEFAULT (getutcdate()) FOR CreateDateUTC
GO
EXEC sp_Rename 'tblPriority.LastChangeDate', 'LastChangeDateUTC', 'COLUMN'
GO
UPDATE tblPriority SET CreateDateUTC = dbo.fnCTZ_TO_UTC(CreateDateUTC)
	, LastChangeDateUTC = dbo.fnCTZ_TO_UTC(LastChangeDateUTC)
GO

EXEC sp_Rename 'tblProducer.CreateDate', 'CreateDateUTC', 'COLUMN'
GO
ALTER TABLE tblProducer DROP CONSTRAINT DF_tblProducer_CreateDate
GO
ALTER TABLE tblProducer ADD CONSTRAINT DF_Producer_CreateDateUTC  DEFAULT (getutcdate()) FOR CreateDateUTC
GO
EXEC sp_Rename 'tblProducer.LastChangeDate', 'LastChangeDateUTC', 'COLUMN'
GO
UPDATE tblProducer SET CreateDateUTC = dbo.fnCTZ_TO_UTC(CreateDateUTC)
	, LastChangeDateUTC = dbo.fnCTZ_TO_UTC(LastChangeDateUTC)
GO

EXEC sp_Rename 'tblPumper.CreateDate', 'CreateDateUTC', 'COLUMN'
GO
ALTER TABLE tblPumper DROP CONSTRAINT DF_tblPumper_CreateDate
GO
ALTER TABLE tblPumper ADD CONSTRAINT DF_Pumper_CreateDateUTC  DEFAULT (getutcdate()) FOR CreateDateUTC
GO
EXEC sp_Rename 'tblPumper.LastChangeDate', 'LastChangeDateUTC', 'COLUMN'
GO
UPDATE tblPumper SET CreateDateUTC = dbo.fnCTZ_TO_UTC(CreateDateUTC)
	, LastChangeDateUTC = dbo.fnCTZ_TO_UTC(LastChangeDateUTC)
GO

DROP TABLE tblRateTableRange
GO

EXEC sp_Rename 'tblRegion.CreateDate', 'CreateDateUTC', 'COLUMN'
GO
ALTER TABLE tblRegion DROP CONSTRAINT DF_tblRegion_CreateDate
GO
ALTER TABLE tblRegion ADD CONSTRAINT DF_Region_CreateDateUTC  DEFAULT (getutcdate()) FOR CreateDateUTC
GO
EXEC sp_Rename 'tblRegion.LastChangeDate', 'LastChangeDateUTC', 'COLUMN'
GO
UPDATE tblRegion SET CreateDateUTC = dbo.fnCTZ_TO_UTC(CreateDateUTC)
	, LastChangeDateUTC = dbo.fnCTZ_TO_UTC(LastChangeDateUTC)
GO

EXEC sp_Rename 'tblSetting.CreateDate', 'CreateDateUTC', 'COLUMN'
GO
ALTER TABLE tblSetting DROP CONSTRAINT DF_tblSetting_CreateDate
GO
ALTER TABLE tblSetting ADD CONSTRAINT DF_Setting_CreateDateUTC  DEFAULT (getutcdate()) FOR CreateDateUTC
GO
EXEC sp_Rename 'tblSetting.LastChangeDate', 'LastChangeDateUTC', 'COLUMN'
GO
UPDATE tblSetting SET CreateDateUTC = dbo.fnCTZ_TO_UTC(CreateDateUTC)
	, LastChangeDateUTC = dbo.fnCTZ_TO_UTC(LastChangeDateUTC)
GO
-- insert new Settings for Driver App
INSERT INTO tblSetting (ID, Category, Name, SettingTypeID, Value, CreateDateUTC, CreatedByUser, ReadOnly)
  SELECT 11, 'Driver App Settings', 'Sync Interval (minutes)', 3, 15, GETUTCDATE(), 'System', 0
GO

INSERT INTO tblSetting (ID, Category, Name, SettingTypeID, Value, CreateDateUTC, CreatedByUser, ReadOnly)
  SELECT 10, 'Driver App Settings', 'Hours Driver can access past DELIVERY', 3, 12, GETUTCDATE(), 'System', 0
GO

-- insert new Settings for Driver App
INSERT INTO tblSetting (ID, Category, Name, SettingTypeID, Value, CreateDateUTC, CreatedByUser, ReadOnly)
  SELECT 12, 'Driver App Settings', 'LatestAppVersion', 1, '1.0.1', GETUTCDATE(), 'System', 0
GO 

EXEC sp_Rename 'tblSettingType.CreateDate', 'CreateDateUTC', 'COLUMN'
GO
ALTER TABLE tblSettingType DROP CONSTRAINT DF_tblSettingType_CreateDate
GO
ALTER TABLE tblSettingType ADD CONSTRAINT DF_SettingType_CreateDateUTC  DEFAULT (getutcdate()) FOR CreateDateUTC
GO
EXEC sp_Rename 'tblSettingType.LastChangeDate', 'LastChangeDateUTC', 'COLUMN'
GO
UPDATE tblSettingType SET CreateDateUTC = dbo.fnCTZ_TO_UTC(CreateDateUTC)
	, LastChangeDateUTC = dbo.fnCTZ_TO_UTC(LastChangeDateUTC)
GO

EXEC sp_Rename 'tblState.CreateDate', 'CreateDateUTC', 'COLUMN'
GO
ALTER TABLE tblState DROP CONSTRAINT DF_tblState_CreateDate
GO
ALTER TABLE tblState ADD CONSTRAINT DF_State_CreateDateUTC  DEFAULT (getutcdate()) FOR CreateDateUTC
GO
EXEC sp_Rename 'tblState.LastChangeDate', 'LastChangeDateUTC', 'COLUMN'
GO
UPDATE tblState SET CreateDateUTC = dbo.fnCTZ_TO_UTC(CreateDateUTC)
	, LastChangeDateUTC = dbo.fnCTZ_TO_UTC(LastChangeDateUTC)
GO

EXEC sp_Rename 'tblTankType.CreateDate', 'CreateDateUTC', 'COLUMN'
GO
ALTER TABLE tblTankType DROP CONSTRAINT DF_tblTankType_CreateDate
GO
ALTER TABLE tblTankType ADD CONSTRAINT DF_TankType_CreateDateUTC  DEFAULT (getutcdate()) FOR CreateDateUTC
GO
EXEC sp_Rename 'tblTankType.LastChangeDate', 'LastChangeDateUTC', 'COLUMN'
GO
UPDATE tblTankType SET CreateDateUTC = dbo.fnCTZ_TO_UTC(CreateDateUTC)
	, LastChangeDateUTC = dbo.fnCTZ_TO_UTC(LastChangeDateUTC)
GO

EXEC sp_Rename 'tblTicketType.CreateDate', 'CreateDateUTC', 'COLUMN'
GO
ALTER TABLE tblTicketType DROP CONSTRAINT DF_tblTicketType_CreateDate
GO
ALTER TABLE tblTicketType ADD CONSTRAINT DF_TicketType_CreateDateUTC  DEFAULT (getutcdate()) FOR CreateDateUTC
GO
EXEC sp_Rename 'tblTicketType.LastChangeDate', 'LastChangeDateUTC', 'COLUMN'
GO
UPDATE tblTicketType SET CreateDateUTC = dbo.fnCTZ_TO_UTC(CreateDateUTC)
	, LastChangeDateUTC = dbo.fnCTZ_TO_UTC(LastChangeDateUTC)
GO

EXEC sp_Rename 'tblTrailer.CreateDate', 'CreateDateUTC', 'COLUMN'
GO
ALTER TABLE tblTrailer DROP CONSTRAINT DF_tblTrailer_CreateDate
GO
ALTER TABLE tblTrailer ADD CONSTRAINT DF_Trailer_CreateDateUTC  DEFAULT (getutcdate()) FOR CreateDateUTC
GO
EXEC sp_Rename 'tblTrailer.LastChangeDate', 'LastChangeDateUTC', 'COLUMN'
GO
EXEC sp_Rename 'tblTrailer.DeleteDate', 'DeleteDateUTC', 'COLUMN'
GO
UPDATE tblTrailer SET CreateDateUTC = dbo.fnCTZ_TO_UTC(CreateDateUTC)
	, LastChangeDateUTC = dbo.fnCTZ_TO_UTC(LastChangeDateUTC)
	, DeleteDateUTC = dbo.fnCTZ_TO_UTC(DeleteDateUTC)
GO

EXEC sp_Rename 'tblTrailerInspection.CreateDate', 'CreateDateUTC', 'COLUMN'
GO
ALTER TABLE tblTrailerInspection ADD CONSTRAINT DF_TrailerInspection_CreateDateUTC  DEFAULT (getutcdate()) FOR CreateDateUTC
GO
EXEC sp_Rename 'tblTrailerInspection.LastChangeDate', 'LastChangeDateUTC', 'COLUMN'
GO
UPDATE tblTrailerInspection SET CreateDateUTC = dbo.fnCTZ_TO_UTC(CreateDateUTC)
	, LastChangeDateUTC = dbo.fnCTZ_TO_UTC(LastChangeDateUTC)
GO

EXEC sp_Rename 'tblTrailerInspectionType.CreateDate', 'CreateDateUTC', 'COLUMN'
GO
ALTER TABLE tblTrailerInspectionType ADD CONSTRAINT DF_TrailerInspectionType_CreateDateUTC  DEFAULT (getutcdate()) FOR CreateDateUTC
GO
EXEC sp_Rename 'tblTrailerInspectionType.LastChangeDate', 'LastChangeDateUTC', 'COLUMN'
GO
EXEC sp_Rename 'tblTrailerInspectionType.DeleteDate', 'DeleteDateUTC', 'COLUMN'
GO
UPDATE tblTrailerInspectionType SET CreateDateUTC = dbo.fnCTZ_TO_UTC(CreateDateUTC)
	, LastChangeDateUTC = dbo.fnCTZ_TO_UTC(LastChangeDateUTC)
	, DeleteDateUTC = dbo.fnCTZ_TO_UTC(DeleteDateUTC)
GO

EXEC sp_Rename 'tblTrailerMaintenance.CreateDate', 'CreateDateUTC', 'COLUMN'
GO
ALTER TABLE tblTrailerMaintenance ADD CONSTRAINT DF_TrailerMaintenance_CreateDateUTC  DEFAULT (getutcdate()) FOR CreateDateUTC
GO
EXEC sp_Rename 'tblTrailerMaintenance.LastChangeDate', 'LastChangeDateUTC', 'COLUMN'
GO
UPDATE tblTrailerMaintenance SET CreateDateUTC = dbo.fnCTZ_TO_UTC(CreateDateUTC)
	, LastChangeDateUTC = dbo.fnCTZ_TO_UTC(LastChangeDateUTC)
GO

EXEC sp_Rename 'tblTrailerType.CreateDate', 'CreateDateUTC', 'COLUMN'
GO
ALTER TABLE tblTrailerType DROP CONSTRAINT DF_tblTrailerType_CreateDate
GO
ALTER TABLE tblTrailerType ADD CONSTRAINT DF_TrailerType_CreateDateUTC  DEFAULT (getutcdate()) FOR CreateDateUTC
GO
EXEC sp_Rename 'tblTrailerType.LastChangeDate', 'LastChangeDateUTC', 'COLUMN'
GO
UPDATE tblTrailerType SET CreateDateUTC = dbo.fnCTZ_TO_UTC(CreateDateUTC)
	, LastChangeDateUTC = dbo.fnCTZ_TO_UTC(LastChangeDateUTC)
GO

EXEC sp_Rename 'tblTruck.CreateDate', 'CreateDateUTC', 'COLUMN'
GO
ALTER TABLE tblTruck DROP CONSTRAINT DF_tblTruck_CreateDate
GO
ALTER TABLE tblTruck ADD CONSTRAINT DF_Truck_CreateDateUTC  DEFAULT (getutcdate()) FOR CreateDateUTC
GO
EXEC sp_Rename 'tblTruck.LastChangeDate', 'LastChangeDateUTC', 'COLUMN'
GO
EXEC sp_Rename 'tblTruck.DeleteDate', 'DeleteDateUTC', 'COLUMN'
GO
UPDATE tblTruck SET CreateDateUTC = dbo.fnCTZ_TO_UTC(CreateDateUTC)
	, LastChangeDateUTC = dbo.fnCTZ_TO_UTC(LastChangeDateUTC)
	, DeleteDateUTC = dbo.fnCTZ_TO_UTC(DeleteDateUTC)
GO

EXEC sp_Rename 'tblTruckInspection.CreateDate', 'CreateDateUTC', 'COLUMN'
GO
ALTER TABLE tblTruckInspection ADD CONSTRAINT DF_TruckInspection_CreateDateUTC  DEFAULT (getutcdate()) FOR CreateDateUTC
GO
EXEC sp_Rename 'tblTruckInspection.LastChangeDate', 'LastChangeDateUTC', 'COLUMN'
GO
UPDATE tblTruckInspection SET CreateDateUTC = dbo.fnCTZ_TO_UTC(CreateDateUTC)
	, LastChangeDateUTC = dbo.fnCTZ_TO_UTC(LastChangeDateUTC)
GO

EXEC sp_Rename 'tblTruckInspectionType.CreateDate', 'CreateDateUTC', 'COLUMN'
GO
ALTER TABLE tblTruckInspectionType ADD CONSTRAINT DF_TruckInspectionType_CreateDateUTC  DEFAULT (getutcdate()) FOR CreateDateUTC
GO
EXEC sp_Rename 'tblTruckInspectionType.LastChangeDate', 'LastChangeDateUTC', 'COLUMN'
GO
EXEC sp_Rename 'tblTruckInspectionType.DeleteDate', 'DeleteDateUTC', 'COLUMN'
GO
UPDATE tblTruckInspectionType SET CreateDateUTC = dbo.fnCTZ_TO_UTC(CreateDateUTC)
	, LastChangeDateUTC = dbo.fnCTZ_TO_UTC(LastChangeDateUTC)
	, DeleteDateUTC = dbo.fnCTZ_TO_UTC(DeleteDateUTC)
GO

EXEC sp_Rename 'tblTruckMaintenance.CreateDate', 'CreateDateUTC', 'COLUMN'
GO
ALTER TABLE tblTruckMaintenance ADD CONSTRAINT DF_TruckMaintenance_CreateDateUTC  DEFAULT (getutcdate()) FOR CreateDateUTC
GO
EXEC sp_Rename 'tblTruckMaintenance.LastChangeDate', 'LastChangeDateUTC', 'COLUMN'
GO
UPDATE tblTruckMaintenance SET CreateDateUTC = dbo.fnCTZ_TO_UTC(CreateDateUTC)
	, LastChangeDateUTC = dbo.fnCTZ_TO_UTC(LastChangeDateUTC)
GO

CREATE TABLE tblPrintTemplate
(
  ID int identity(1, 1) not null
, TicketTypeID int not null
, TemplateText text null
, HeaderImageLeft int not null CONSTRAINT DF_PrintTemplate_HeaderImageLeft DEFAULT (0)
, HeaderImageTop int not null CONSTRAINT DF_PrintTemplate_HeaderImageTop DEFAULT (0)
, HeaderImageWidth int not null CONSTRAINT DF_PrintTemplate_HeaderImageWidth DEFAULT (580)
, HeaderImageHeight int not null CONSTRAINT DF_PrintTemplate_HeaderImageHeigth DEFAULT (300)
, CreateDateUTC smalldatetime NULL
, CreatedByUser varchar(100) NULL
, LastChangeDateUTC smalldatetime NULL
, LastChangedByUser varchar(100) NULL
, CONSTRAINT PK_PrintTemplate PRIMARY KEY CLUSTERED (ID)
, CONSTRAINT FK_PrintTemplate_TicketType FOREIGN KEY (TicketTypeID) REFERENCES tblTicketType(ID)
)
GO
GRANT SELECT, INSERT, UPDATE ON tblPrintTemplate TO dispatchcrude_iis_acct
GO
CREATE UNIQUE INDEX udxPrintTemplate_TicketType ON tblPrintTemplate(TicketTypeID)
GO
INSERT INTO tblPrintTemplate (TicketTypeID, CreateDateUTC) SELECT ID, GETUTCDATE() FROM tblTicketType
GO

CREATE TABLE [dbo].[tblOrderPrintLog](
	[ID] [int] IDENTITY(1,1) NOT NULL
	, UID uniqueidentifier NOT NULL CONSTRAINT DF_OrderPrintLog_UID DEFAULT (newid())
	, OrderID int not null
	, HandWritten bit NOT NULL CONSTRAINT DF_OrderPrintLog_HandWritten DEFAULT (0)
	, DatePrintedUTC smalldatetime NOT NULL CONSTRAINT DF_OrderPrintLog_DatePrinted DEFAULT (getdate())
	, IsCorrection bit CONSTRAINT DF_OrderPrintLog_IsCorrection DEFAULT (0)
	, [StatusID] [int] NOT NULL
	, [TicketTypeID] [int] NOT NULL
	, [TankTypeID] [int] NOT NULL
	, [TruckID] [int] NULL
	, [TrailerID] [int] NULL
	, [Trailer2ID] [int] NULL
	, [CarrierTicketNum] [varchar](15) NULL
	, [OriginID] [int] NULL
	, [OriginArriveTimeUTC] [datetime] NULL
	, [OriginDepartTimeUTC] [datetime] NULL
	, [OriginMinutes] [int] NULL
	, [OriginWaitNotes] [varchar](255) NULL
	, [OriginTruckMileage] [int] NULL
	, [OriginGrossBarrels] [decimal](9, 3) NULL
	, [OriginNetBarrels] [decimal](9, 3) NULL
	, [ChainUp] [bit] NOT NULL
	, [Rejected] [bit] NOT NULL
	, [RejectNotes] [varchar](255) NULL
	, [DestinationID] [int] NULL
	, [DestArriveTimeUTC] [datetime] NULL
	, [DestDepartTimeUTC] [datetime] NULL
	, [DestMinutes] [int] NULL
	, [DestWaitNotes] [varchar](255) NULL
	, [DestBOLNum] [varchar](15) NULL
	, [DestGrossBarrels] [decimal](9, 3) NULL
	, [DestNetBarrels] [decimal](9, 3) NULL
	, [DestProductBSW] [decimal](9, 3) NULL
	, [DestProductGravity] [decimal](9, 3) NULL
	, [DestProductTemp] [decimal](9, 3) NULL
	, [DestOpenMeterBarrels] [decimal](9, 3) NULL
	, [DestCloseMeterBarrels] [decimal](9, 3) NULL
	, SignatureBlob varbinary(max) NULL
	, [CustomerID] [int] NULL
	, [CarrierID] [int] NULL
	, [DriverID] [int] NULL
	, [OperatorID] [int] NULL
	, [PumperID] [int] NULL
	, [DestTruckMileage] [int] NULL
	, [ProducerID] [int] NULL
	, [PriorityID] [tinyint] NULL
	, CONSTRAINT [PK_OrderPrintLog] PRIMARY KEY CLUSTERED (ID)
	, CONSTRAINT FK_OrderPrintLog_Order FOREIGN KEY (OrderID) REFERENCES tblorder(ID) ON DELETE CASCADE
) ON [PRIMARY]
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblOrderPrintLog TO dispatchcrude_iis_acct
GO
CREATE UNIQUE INDEX udxOrderPrintLog_UID ON tblOrderPrintLog(UID)
GO

CREATE TABLE tblOrderPrintLogTicket(
   ID int identity(1, 1) NOT NULL ,
   UID uniqueidentifier NOT NULL CONSTRAINT DF_OrderPrintLogTicket_UID DEFAULT (newid()),
   OrderPrintLogUID uniqueidentifier NOT NULL ,
   CarrierTicketNum varchar(15) NOT NULL ,
   TicketTypeID int NOT NULL ,
   TankTypeID int NULL ,
   ProductObsGravity decimal(9, 3) NULL ,
   ProductHighTemp decimal(9,3) NULL ,
   ProductLowTemp decimal(9, 3) NULL ,
   ProductObsTemp decimal(9, 3) NULL ,
   ProductBSW decimal(9, 3) NULL ,
   OpeningGaugeFeet tinyint NULL ,
   OpeningGaugeInch tinyint NULL ,
   OpeningGaugeQ tinyint NULL ,
   ClosingGaugeFeet tinyint NULL ,
   ClosingGaugeInch tinyint NULL ,
   ClosingGaugeQ tinyint NULL ,
   GrossBarrels decimal(9, 3) NULL ,
   NetBarrels decimal(9, 3) NULL ,
   Rejected bit NOT NULL CONSTRAINT DF_OrderPrintLogTicket_Rejected DEFAULT (0),
   RejectNotes varchar(25) NULL ,
   SealOff varchar(10) NULL ,
   SealOn varchar(10) NULL ,
   BOLNum varchar(15) NULL 
	, CONSTRAINT PK_OrderPrintLogTicket PRIMARY KEY (ID)
	, CONSTRAINT FK_OrderPrintLogTicket_OrderPrintLog FOREIGN KEY (OrderPrintLogUID) REFERENCES tblOrderPrintLog(UID) ON DELETE CASCADE
)
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblOrderPrintLogTicket TO dispatchcrude_iis_acct
GO
CREATE UNIQUE INDEX udxOrderPrintLogTicket_UID ON tblOrderPrintLogTicket(UID)
GO

/***********************************/
-- Date Created: 4 Apr 2013
-- Author: Kevin Alons
-- Purpose: return Carrier records with translated "friendly" values
/***********************************/
ALTER VIEW [dbo].[viewCarrier] AS
SELECT C.*
	, cast(CASE WHEN C.DeleteDateUTC IS NULL THEN 1 ELSE 0 END as bit) AS Active
	, CT.Name AS CarrierType
	, S.Abbreviation AS StateAbbrev
	, SF.Name AS SettlementFactor 
FROM tblCarrier C 
JOIN tblCarrierType CT ON CT.ID = C.CarrierTypeID
LEFT JOIN tblState S ON S.ID = C.StateID
LEFT JOIN tblSettlementFactor SF ON SF.ID = C.SettlementFactorID

GO

/***********************************/
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Destination records with translated value and FullName field (which includes Destination Type)
/***********************************/
ALTER VIEW [dbo].[viewDestination] AS
SELECT D.*
	, cast(CASE WHEN D.DeleteDateUTC IS NULL THEN 1 ELSE 0 END as bit) AS Active
	, DT.DestinationType
	, DT.DestinationType + ' - ' + D.Name AS FullName
	, S.FullName AS State, S.Abbreviation AS StateAbbrev
	, R.Name AS Region
	, DTT.Name AS TicketType
	, TZ.Name AS TimeZone
FROM dbo.tblDestination D
JOIN dbo.tblDestinationType DT ON DT.ID = D.DestinationTypeID
JOIN dbo.tblDestTicketType DTT ON DTT.ID = D.TicketTypeID
LEFT JOIN tblState S ON S.ID = D.StateID
LEFT JOIN dbo.tblRegion R ON R.ID = D.RegionID
LEFT JOIN dbo.tblTimeZone TZ ON TZ.ID = D.TimeZoneID

GO

/***********************************/
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Origin records with translated value and FullName field (which includes Origin Type)
/***********************************/
ALTER VIEW [dbo].[viewOrigin] AS
SELECT O.*
	, CASE WHEN O.H2S = 1 THEN 'H2S-' ELSE '' END + OT.OriginType + ' - ' + O.Name AS FullName
	, OT.OriginType
	, S.FullName AS State
	, S.Abbreviation AS StateAbbrev
	, OP.Name AS Operator
	, P.FullName AS Pumper
	, TT.Name AS TicketType
	, R.Name AS Region
	, C.Name AS Customer
	, PR.Name AS Producer
	, TAT.FullName AS TankType
	, TZ.Name AS TimeZone
FROM dbo.tblOrigin O
JOIN dbo.tblOriginType OT ON OT.ID = O.OriginTypeID
LEFT JOIN tblState S ON S.ID = O.StateID
LEFT JOIN dbo.tblOperator OP ON OP.ID = O.OperatorID
LEFT JOIN dbo.viewPumper P ON P.ID = O.PumperID
LEFT JOIN dbo.tblTicketType TT ON TT.ID = O.TicketTypeID
LEFT JOIN dbo.tblRegion R ON R.ID = O.RegionID
LEFT JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
LEFT JOIN dbo.tblProducer PR ON PR.ID = O.ProducerID
LEFT JOIN dbo.viewTankType TAT ON TAT.ID = O.TankTypeID
LEFT JOIN dbo.tblTimeZone TZ ON TZ.ID = O.TimeZoneID
GO

/**********************************/
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Route records with translated Origin/Destination values
/***********************************/
ALTER VIEW [dbo].[viewRoute] AS
SELECT R.*
, O.Name AS Origin, O.FullName AS OriginFull
, D.Name AS Destination, D.FullName AS DestinationFull
, cast(CASE WHEN O.Active = 1 AND D.Active = 1 THEN 1 ELSE 0 END as bit) AS Active
FROM dbo.tblRoute R
JOIN dbo.viewOrigin O ON O.ID = R.OriginID
JOIN dbo.viewDestination D ON D.ID = R.DestinationID

GO

/**********************************/
-- Date Created: 7 July 2013
-- Author: Kevin Alons
-- Purpose: return Route records with translated Origin/Destination values (with "missing" routes)
/***********************************/
ALTER VIEW [dbo].[viewRouteWithMissing] AS
SELECT R.ID, O.ID AS OriginID, D.ID AS DestinationID, R.ActualMiles
	, R.CreateDateUTC, R.CreatedByUser, R.LastChangeDateUTC, R.LastChangedByUser
	, O.Name AS Origin, O.FullName AS OriginFull
	, D.Name AS Destination, D.FullName AS DestinationFull
	, cast(CASE WHEN R.ID IS NOT NULL AND O.Active = 1 AND D.Active = 1 THEN 1 ELSE 0 END as bit) AS Active
	, CASE WHEN R.ID IS NULL THEN 1 ELSE 0 END AS Missing
FROM (viewOrigin O CROSS JOIN viewDestination D)
LEFT JOIN dbo.tblRoute R ON R.OriginID = O.ID AND R.DestinationID = D.ID

GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: return Carrier route rates with computed Earliest & End Date values)
/***********************************/
ALTER VIEW [dbo].[viewCarrierRouteRatesBase] AS
SELECT CRR.*
	, R.OriginID, R.DestinationID
	, (SELECT MIN(dateadd(day, -1, EffectiveDate)) FROM tblCarrierRouteRates CRRN WHERE CRRN.CarrierID = CRR.CarrierID AND CRRN.RouteID = CRR.RouteID AND CRRN.EffectiveDate > CRR.EffectiveDate) AS EndDate
	, (SELECT MAX(dateadd(day, 1, EffectiveDate)) FROM tblCarrierRouteRates CRRP WHERE CRRP.CarrierID = CRR.CarrierID AND CRRP.RouteID = CRR.RouteID AND CRRP.EffectiveDate < CRR.EffectiveDate) AS EarliestEffectiveDate
	, R.ActualMiles
	, R.Active
FROM tblCarrierRouteRates CRR
JOIN viewRoute R ON R.ID = CRR.RouteID

GO

/***********************************/
-- Date Created: 9 Jun 2013
-- Author: Kevin Alons
-- Purpose: return Carrier route rates with translated "friendly" values + "missing route rates", computed Earliest & End Date values)
/***********************************/
ALTER VIEW [dbo].[viewCarrierRouteRates] AS
SELECT RB.ID, R.ID AS RouteID, isnull(RB.Rate, 0) AS Rate, isnull(RB.EffectiveDate, dbo.fnDateOnly(GETDATE())) AS EffectiveDate
	, cast(CASE WHEN C.DeleteDateUTC IS NOT NULL OR RB.Active = 0 THEN 0 ELSE 1 END as bit) AS Active
	, isnull(ORD.OpenOrderCount, 0) as OpenOrderCount
	, isnull(R.ActualMiles, 0) AS ActualMiles
	, RB.CreateDateUTC, RB.CreatedByUser, RB.LastChangeDateUTC, RB.LastChangedByUser
	, RB.EndDate
	, RB.EarliestEffectiveDate
	, C.ID AS CarrierID, C.Name AS Carrier
	, R.OriginID, R.Origin, R.OriginFull
	, R.DestinationID , R.Destination, R.DestinationFull
	, CASE WHEN R.ID IS NULL THEN 'Route Missing' WHEN RB.ID IS NULL THEN 'Missing' WHEN C.Active = 0 THEN 'Carrier Deleted' WHEN RB.Active = 0 THEN 'Route Inactive' ELSE 'Active' END AS Status
	, cast(CASE WHEN R.ID IS NULL OR isnull(ORD.OpenOrderCount, 0) = 0 THEN 0 ELSE 1 END as bit) AS RouteInUse
FROM (viewCarrier C CROSS JOIN viewRouteWithMissing R)
LEFT JOIN viewCarrierRouteRatesBase RB ON RB.CarrierID = C.ID AND RB.OriginID = R.OriginID AND RB.DestinationID = R.DestinationID
LEFT JOIN (SELECT CarrierID, RouteID, count(1) AS OpenOrderCount FROM tblOrder WHERE StatusID IN (4) GROUP BY CarrierID, RouteID) ORD ON ORD.CarrierID = C.ID AND ORD.RouteID = R.ID

GO

/***********************************/
-- Date Created: 15 Aug 2013
-- Author: Kevin Alons
-- Purpose: return the CarrierSettlementBatch table contents with the CreateDate formatted DateOnly (for the BatchDate)
/***********************************/
ALTER VIEW [dbo].[viewCarrierSettlementBatch] 
AS
SELECT x.*
	, '#' + ltrim(BatchNum) + ' [' + dbo.fnDateMdYY(BatchDate) 
		+ '] ' + ltrim(OrderCount) + ' / ' + LTRIM(dbo.fnFormatWithCommas(round(GrossBarrels, 0))) + 'BBLS' AS FullName
FROM (
	SELECT SB.*
		, CASE WHEN C.Active = 1 THEN '' ELSE 'Deleted: ' END + C.Name AS Carrier
		, (SELECT COUNT(*) FROM tblOrderInvoiceCarrier WHERE BatchID = SB.ID) AS OrderCount
		, (SELECT sum(O.OriginGrossBarrels) FROM tblOrderInvoiceCarrier OIC JOIN tblOrder O ON O.ID = OIC.OrderID WHERE BatchID = SB.ID) AS GrossBarrels
	FROM dbo.tblCarrierSettlementBatch SB
	JOIN dbo.viewCarrier C ON C.ID = SB.CarrierID
) x

GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: return Customer route rates with computed Earliest & End Date values)
/***********************************/
ALTER VIEW [dbo].[viewCustomerRouteRatesBase] AS
SELECT CRR.*
	, R.OriginID, R.DestinationID
	, (SELECT MIN(dateadd(day, -1, EffectiveDate)) FROM tblCustomerRouteRates CRRN WHERE CRRN.CustomerID = CRR.CustomerID AND CRRN.RouteID = CRR.RouteID AND CRRN.EffectiveDate > CRR.EffectiveDate) AS EndDate
	, (SELECT MAX(dateadd(day, 1, EffectiveDate)) FROM tblCustomerRouteRates CRRP WHERE CRRP.CustomerID = CRR.CustomerID AND CRRP.RouteID = CRR.RouteID AND CRRP.EffectiveDate < CRR.EffectiveDate) AS EarliestEffectiveDate
	, R.ActualMiles
	, R.Active
FROM tblCustomerRouteRates CRR
JOIN viewRoute R ON R.ID = CRR.RouteID

GO

/***********************************/
-- Date Created: 9 Jun 2013
-- Author: Kevin Alons
-- Purpose: return Customer route rates with translated "friendly" values + "missing route rates", computed Earliest & End Date values)
/***********************************/
ALTER VIEW [dbo].[viewCustomerRouteRates] AS
SELECT RB.ID, R.ID AS RouteID, isnull(RB.Rate, 0) AS Rate, isnull(RB.EffectiveDate, dbo.fnDateOnly(GETDATE())) AS EffectiveDate
	, cast(CASE WHEN RB.Active = 0 THEN 0 ELSE 1 END as bit) AS Active
	, isnull(ORD.OpenOrderCount, 0) as OpenOrderCount
	, isnull(R.ActualMiles, 0) AS ActualMiles
	, RB.CreateDateUTC, RB.CreatedByUser, RB.LastChangeDateUTC, RB.LastChangedByUser
	, RB.EndDate
	, RB.EarliestEffectiveDate
	, C.ID AS CustomerID, C.Name AS Customer
	, R.OriginID, R.Origin, R.OriginFull
	, R.DestinationID , R.Destination, R.DestinationFull
	, CASE WHEN R.ID IS NULL THEN 'Route Missing' WHEN RB.ID IS NULL THEN 'Missing' WHEN RB.Active = 0 THEN 'Route Inactive' ELSE 'Active' END AS Status
	, cast(CASE WHEN R.ID IS NULL OR isnull(ORD.OpenOrderCount, 0) = 0 THEN 0 ELSE 1 END as bit) AS RouteInUse
FROM (viewCustomer C CROSS JOIN viewRouteWithMissing R)
LEFT JOIN viewCustomerRouteRatesBase RB ON RB.CustomerID = C.ID AND RB.OriginID = R.OriginID AND RB.DestinationID = R.DestinationID
LEFT JOIN (SELECT CustomerID, RouteID, count(1) AS OpenOrderCount FROM tblOrder WHERE StatusID IN (4) GROUP BY CustomerID, RouteID) ORD ON ORD.CustomerID = C.ID AND ORD.RouteID = R.ID

GO

/***********************************/
-- Date Created: 15 Aug 2013
-- Author: Kevin Alons
-- Purpose: return the CustomerSettlementBatch table contents with the CreateDate formatted DateOnly (for the BatchDate)
/***********************************/
ALTER VIEW [dbo].[viewCustomerSettlementBatch] 
AS
SELECT x.*
	, '#' + ltrim(BatchNum) + ' [' + dbo.fnDateMdYY(BatchDate) 
		+ '] ' + ltrim(OrderCount) + ' / ' + LTRIM(dbo.fnFormatWithCommas(round(GrossBarrels, 0))) + 'BBLS' AS FullName
FROM (
	SELECT SB.*
		, C.Name AS Customer
		, (SELECT COUNT(*) FROM tblOrderInvoiceCustomer WHERE BatchID = SB.ID) AS OrderCount
		, (SELECT sum(O.OriginGrossBarrels) FROM tblOrderInvoiceCustomer OIC JOIN tblOrder O ON O.ID = OIC.OrderID WHERE BatchID = SB.ID) AS GrossBarrels
	FROM dbo.tblCustomerSettlementBatch SB
	JOIN dbo.viewCustomer C ON C.ID = SB.CustomerID
) x

GO

/***********************************/
-- Date Created: 26 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Trailer records with FullName & translated "friendly" values
/***********************************/
ALTER VIEW [dbo].[viewTrailer] AS
SELECT T.*
	, cast(CASE WHEN isnull(C.DeleteDateUTC, T.DeleteDateUTC) IS NULL THEN 1 ELSE 0 END as bit) AS Active
	, ISNULL(Compartment1Barrels, 0) 
		+ ISNULL(Compartment2Barrels, 0) 
		+ ISNULL(Compartment3Barrels, 0) 
		+ ISNULL(Compartment4Barrels, 0) 
		+ ISNULL(Compartment5Barrels, 0) AS TotalCapacityBarrels
	, CASE WHEN isnull(Compartment1Barrels, 0) > 0 THEN 1 ELSE 0 END 
		+ CASE WHEN isnull(Compartment2Barrels, 0) > 0 THEN 1 ELSE 0 END 
		+ CASE WHEN isnull(Compartment3Barrels, 0) > 0 THEN 1 ELSE 0 END
		+ CASE WHEN isnull(Compartment4Barrels, 0) > 0 THEN 1 ELSE 0 END 
		+ CASE WHEN isnull(Compartment5Barrels, 0) > 0 THEN 1 ELSE 0 END AS CompartmentCount
	, T.IDNumber AS FullName
	, TT.Name AS TrailerType
	, isnull(CT.Name, 'Unknown') AS CarrierType
	, C.Name AS Carrier
FROM dbo.tblTrailer T
LEFT JOIN dbo.tblTrailerType TT ON TT.ID = T.TrailerTypeID
JOIN dbo.tblCarrier C ON C.ID = T.CarrierID
LEFT JOIN dbo.tblCarrierType CT ON CT.ID = C.CarrierTypeID

GO

/***********************************/
-- Date Created: 26 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Truck records with FullName & translated "friendly" values
/***********************************/
ALTER VIEW [dbo].[viewTruck] AS
SELECT T.*
	, cast(CASE WHEN isnull(C.DeleteDateUTC, T.DeleteDateUTC) IS NULL THEN 1 ELSE 0 END as bit) AS Active
	, T.IDNumber AS FullName
	, isnull(CT.Name, 'Unknown') AS CarrierType
	, C.Name AS Carrier
FROM dbo.tblTruck T
JOIN dbo.tblCarrier C ON C.ID = T.CarrierID
LEFT JOIN dbo.tblCarrierType CT ON CT.ID = C.CarrierTypeID

GO

/***********************************/
-- Date Created: 26 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Drivers table records with "friendly" translated values included
/***********************************/
ALTER VIEW [dbo].[viewDriver] AS
SELECT *
	, CASE WHEN Active = 1 THEN '' ELSE 'Deleted: ' END + FullName AS FullNameD
FROM (
	SELECT D.*
	, cast(CASE WHEN isnull(C.DeleteDateUTC, D.DeleteDateUTC) IS NULL THEN 1 ELSE 0 END as bit) AS Active
	, D.FirstName + ' ' + D.LastName As FullName
	, D.LastName + ', ' + D.FirstName AS FullNameLF
	, isnull(CT.Name, 'Unknown') AS CarrierType
	, C.Name AS Carrier
	, S.Abbreviation AS StateAbbrev 
	, T.FullName AS Truck
	, T1.FullName AS Trailer
	, T2.FullName AS Trailer2
	FROM dbo.tblDriver D 
	JOIN dbo.tblCarrier C ON C.ID = D.CarrierID 
	LEFT JOIN dbo.tblCarrierType CT ON CT.ID = C.CarrierTypeID
	LEFT JOIN tblState S ON S.ID = D.StateID
	LEFT JOIN viewTruck T ON T.ID = D.TruckID
	LEFT JOIN viewTrailer T1 ON T1.ID = D.TrailerID
	LEFT JOIN viewTrailer T2 ON T2.ID = D.Trailer2ID
) X

GO

/***********************************/
-- Date Created: 27 Jan 2013
-- Author: Kevin Alons
-- Purpose: return Operator records with "translated friendly" values for FK relationships
/***********************************/
ALTER VIEW [dbo].[viewOperator] AS
SELECT O.*, S.Abbreviation AS StateAbbrev, S.FullName AS State
FROM dbo.tblOperator O
LEFT JOIN dbo.tblState S ON S.ID = O.StateID

GO

/***********************************/
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Order records with "translated friendly" values for FK relationships
/***********************************/
ALTER VIEW [dbo].[viewOrder] AS
SELECT O.*
, dbo.fnDateOnly(dbo.fnUTC_to_Local(O.OriginDepartTimeUTC, vO.TimeZoneID, vO.UseDST)) AS OrderDate
, vO.Name AS Origin
, vO.FullName AS OriginFull
, vO.State AS OriginState
, vO.StateAbbrev AS OriginStateAbbrev
, vO.LeaseName
, vO.LeaseNum
, vO.TimeZoneID AS OriginTimeZoneID
, vO.UseDST AS OriginUseDST
, vO.H2S
, vD.Name AS Destination
, vD.FullName AS DestinationFull
, vD.State AS DestinationState
, vD.StateAbbrev AS DestinationStateAbbrev
, vD.DestinationType
, vD.Station
, vD.TimeZoneID AS DestTimeZoneID
, vD.UseDST AS DestUseDST
, C.Name AS Customer
, CA.Name AS Carrier
, CT.Name AS CarrierType
, OT.OrderStatus
, OT.StatusNum
, D.FullName AS Driver
, D.FirstName AS DriverFirst
, D.LastName AS DriverLast
, TRU.FullName AS Truck
, TR1.FullName AS Trailer
, TR2.FullName AS Trailer2
, P.PriorityNum
, TT.Name AS TicketType
, vD.TicketTypeID AS DestTicketTypeID
, vD.TicketType AS DestTicketType
, OP.Name AS Operator
, PR.Name AS Producer
, PU.FullName AS Pumper
, D.IDNumber AS DriverNumber
, CA.IDNumber AS CarrierNumber
, TRU.IDNumber AS TruckNumber
, TR1.IDNumber AS TrailerNumber
, TR2.IDNumber AS Trailer2Number
, PRO.Name as Product
, PRO.ShortName AS ProductShort
, cast((CASE WHEN O.DeleteDateUTC IS NULL THEN 1 ELSE 0 END) as bit) AS Active
, cast((CASE WHEN O.DeleteDateUTC IS NOT NULL THEN 1 ELSE 0 END) as bit) AS IsDeleted
FROM dbo.tblOrder O
LEFT JOIN dbo.viewOrigin vO ON vO.ID = O.OriginID
LEFT JOIN dbo.viewDestination vD ON vD.ID = O.DestinationID
LEFT JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
JOIN dbo.tblOrderStatus AS OT ON OT.ID = O.StatusID
LEFT JOIN dbo.tblCarrier CA ON CA.ID = O.CarrierID
LEFT JOIN dbo.tblCarrierType CT ON CT.ID = CA.CarrierTypeID
LEFT JOIN dbo.viewDriver D ON D.ID = O.DriverID
LEFT JOIN dbo.viewTruck TRU ON TRU.ID = O.TruckID
LEFT JOIN dbo.viewTrailer TR1 ON TR1.ID = O.TrailerID
LEFT JOIN dbo.viewTrailer TR2 ON TR2.ID = O.Trailer2ID
LEFT JOIN dbo.tblPriority P ON P.ID = O.PriorityID
LEFT JOIN dbo.tblTicketType TT ON TT.ID = O.TicketTypeID
LEFT JOIN dbo.tblOperator OP ON OP.ID = O.OperatorID
LEFT JOIN dbo.viewPumper PU ON PU.ID = O.PumperID
LEFT JOIN dbo.tblProducer PR ON PR.ID = O.ProducerID
LEFT JOIN dbo.tblProduct PRO ON PRO.ID = O.ProductID
GO

/***********************************/
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return the Local Arrive|Depart Times + their respective TimeZone abbreviations
/***********************************/
CREATE VIEW viewOrderLocalDates AS
SELECT O.*
, dbo.fnUTC_to_Local(O.OriginArriveTimeUTC, O.OriginTimeZoneID, O.OriginUseDST) AS OriginArriveTime
, dbo.fnUTC_to_Local(O.OriginDepartTimeUTC, O.OriginTimeZoneID, O.OriginUseDST) AS OriginDepartTime
, dbo.fnUTC_to_Local(O.DestArriveTimeUTC, O.DestTimeZoneID, O.DestUseDST) AS DestArriveTime
, dbo.fnUTC_to_Local(O.DestDepartTimeUTC, O.DestTimeZoneID, O.DestUseDST) AS DestDepartTime
, dbo.fnTimeZoneAbbrev(O.OriginArriveTimeUTC, O.OriginTimeZoneID, O.OriginUseDST) AS OriginArriveTZ
, dbo.fnTimeZoneAbbrev(O.OriginDepartTimeUTC, O.OriginTimeZoneID, O.OriginUseDST) AS OriginDepartTZ
, dbo.fnTimeZoneAbbrev(O.DestArriveTimeUTC, O.DestTimeZoneID, O.DestUseDST) AS DestArriveTZ
, dbo.fnTimeZoneAbbrev(O.DestDepartTimeUTC, O.DestTimeZoneID, O.DestUseDST) AS DestDepartTZ
FROM viewOrder O
GO
GRANT SELECT ON viewOrderLocalDates TO dispatchcrude_iis_acct
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 20 Mar 2013
-- Description:	return all CarrierTicketNums for an Order (separated by ",")
-- =============================================
ALTER VIEW [dbo].[viewOrder_AllTickets] AS 
WITH ActiveTickets AS
(
	SELECT ID, OrderID, CarrierTicketNum FROM tblOrderTicket WHERE DeleteDateUTC IS NULL
)
, Tickets AS 
( 
	--initialization 
	SELECT OT.OrderID, OT.ID, cast(CarrierTicketNum as varchar(255)) AS Tickets
	FROM ActiveTickets OT 
	JOIN (SELECT OrderID, MIN(ID) AS ID FROM tblOrderTicket GROUP BY OrderID) OTI 
		ON OTI.OrderID = OT.OrderID AND OTI.ID = OT.ID
	
	UNION ALL 
	--recursive execution 
	SELECT T.OrderID, OT.ID, cast(T.Tickets + ',' + OT.CarrierTicketNum as varchar(255)) AS Tickets
	FROM ActiveTickets OT JOIN Tickets T ON OT.ID > T.ID AND OT.OrderID = T.OrderID
)

SELECT O.*, T.Tickets
FROM viewOrderLocalDates O
LEFT JOIN (
	SELECT OrderID, max(Tickets) AS Tickets
	FROM Tickets T
	GROUP BY OrderID
) T ON T.OrderID = O.ID

GO

/***********************************/
-- Date Created: 28 Feb 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
/***********************************/
ALTER VIEW [dbo].[viewOrderExportFull] AS
SELECT *
  , OriginMinutes + DestMinutes AS TotalMinutes
  , isnull(OriginWaitMinutes, 0) + isnull(DestWaitMinutes, 0) AS TotalWaitMinutes
FROM (
	SELECT O.*
	  , dbo.fnMaxInt(0, isnull(OriginMinutes, 0) - cast(S.Value as int)) AS OriginWaitMinutes
	  , dbo.fnMaxInt(0, isnull(DestMinutes, 0) - cast(S.Value as int)) AS DestWaitMinutes
	  , (SELECT count(*) FROM tblOrderTicket WHERE OrderID = O.ID) AS TicketCount
	  , (SELECT count(*) FROM tblOrderReroute WHERE OrderID = O.ID) AS RerouteCount
	FROM dbo.viewOrderLocalDates O
	JOIN dbo.tblSetting S ON S.ID = 7 -- the Unbillable Wait Time threshold minutes 
WHERE O.StatusID NOT IN (-10) -- don't include "Generated" orders (only Assigned+)
  AND O.DeleteDateUTC IS NULL
) v

GO

/***********************************/
-- Date Created: 2 Feb 2013
-- Author: Kevin Alons
-- Purpose: return OrderReroute records with "computed" fields used in exporting to spreadsheet/etc
/***********************************/
ALTER VIEW [dbo].[viewOrderReroute] AS
SELECT O_R.*, D.FullName AS PreviousDestinationFull, D.Name AS PreviousDestination
FROM dbo.tblOrderReroute O_R
LEFT JOIN dbo.viewDestination D ON D.ID = O_R.PreviousDestinationID

GO

/***********************************/
-- Date Created: 22 Jan 2013
-- Author: Kevin Alons
-- Purpose: query the tblOrderTicket table and include "friendly" translated values
/***********************************/
ALTER VIEW [dbo].[viewOrderTicket] AS
SELECT OT.*, TT.Name AS TicketType, VTT.FullName AS TankType
, cast((CASE WHEN OT.DeleteDateUTC IS NULL THEN 1 ELSE 0 END) as bit) AS Active
, cast((CASE WHEN OT.DeleteDateUTC IS NOT NULL THEN 1 ELSE 0 END) as bit) AS IsDeleted
FROM tblOrderTicket OT
JOIN tblTicketType TT ON TT.ID = OT.TicketTypeID
LEFT JOIN viewTankType VTT ON VTT.ID = OT.TankTypeID

GO

/***********************************/
-- Date Created: 27 Jan 2013
-- Author: Kevin Alons
-- Purpose: return Producer records with "translated friendly" values for FK relationships
/***********************************/
ALTER VIEW [dbo].[viewProducer] AS
SELECT O.*, S.Abbreviation AS StateAbbrev, S.FullName AS State
FROM dbo.tblProducer O
LEFT JOIN dbo.tblState S ON S.ID = O.StateID

GO

/***********************************/
-- Date Created: 27 Jan 2013
-- Author: Kevin Alons
-- Purpose: return Pumper records with FullName computed field
/***********************************/
ALTER VIEW [dbo].[viewPumper] AS
SELECT P.*, FirstName + ' ' + LastName AS FullName, O.Name AS Operator
FROM tblPumper P
LEFT JOIN tblOperator O ON O.ID = P.OperatorID

GO

/******************************************************/
-- Date Created: 16 May 2013
-- Author:		 Kevin Alons
-- Purpose:		 return the translated "friendly" info for a Reroute
/******************************************************/
ALTER VIEW [dbo].[viewReroute] AS
SELECT ORR.ID
	, ORR.OrderID
	, PD.Name AS PrevDestination
	, PD.DestinationType AS PrevDestType
	, ORR.Notes
	, ORR.UserName
	, ORR.RerouteDate
FROM tblOrderReroute  ORR
JOIN viewDestination PD ON PD.ID = ORR.PreviousDestinationID

GO

/***********************************/
-- Date Created: 21 Dec 2012
-- Author: Kevin Alons
-- Purpose: return the Settings with the JOINed SettingType value
/***********************************/
ALTER VIEW [dbo].[viewSetting] AS
SELECT S.*, ST.Name AS SettingType
FROM tblSetting S
JOIN tblSettingType ST ON ST.ID = S.SettingTypeID

GO

/***********************************/
-- Date Created: 9 Dec 2012
-- Author: Kevin Alons
-- Purpose: return TankType data + "Full Name" computed field
/***********************************/
ALTER VIEW [dbo].[viewTankType] AS
SELECT *
	, ltrim(CapacityBarrels) + 'BBL (' + ltrim(HeightFeet) + 'FT)' as FullName
FROM dbo.tblTankType

GO

/***********************************/
-- Date Created: 5 Aug 2013
-- Author: Kevin Alons
-- Purpose: return Trailer Inspection records with translated "friendly" values + missing required records
/***********************************/
ALTER VIEW [dbo].[viewTrailerInspection] AS
SELECT TI.ID
	, T.ID AS TrailerID
	, TIT.ID AS TrailerInspectionTypeID
	, TI.InspectionDate
	, TI.ExpirationDate
	, TI.Document
	, TI.DocName
	, TI.Notes
	, TI.CreateDateUTC
	, TI.CreatedByUser
	, TI.LastChangeDateUTC
	, TI.LastChangedByUser
	, CASE WHEN TI.ExpirationDate IS NULL THEN 'Missing' WHEN TI.ExpirationDate < GETDATE() THEN 'Overdue' ELSE 'Current' END AS Status
	, TIT.Name AS InspectionType
	, TIT.Required
	, TIT.ExpirationMonths
	, T.IdNumber
	, T.VIN
	, T.CarrierID
	, T.Carrier
	, T.CarrierType
	, T.TrailerType
FROM viewTrailer T
CROSS JOIN tblTrailerInspectionType TIT
LEFT JOIN tblTrailerInspection TI ON TI.TrailerID = T.ID AND TI.TrailerInspectionTypeID = TIT.ID
WHERE TIT.Required = 1
  -- include any existing Inspection records or those missing required inspections
  AND (TIT.Required = 1 OR TI.ID IS NOT NULL)
  AND T.Active = 1
  AND TIT.DeleteDateUTC IS NULL

GO

/***********************************/
-- Date Created: 8 Aug 2013
-- Author: Kevin Alons
-- Purpose: return Trailer Maintenance records with translated "friendly" values
/***********************************/
ALTER VIEW [dbo].[viewTrailerMaintenance] AS
SELECT TM.ID
	, T.ID AS TrailerID
	, TM.MaintenanceDate
	, TM.Description
	, TM.Document
	, TM.DocName
	, TM.Notes
	, TM.CreateDateUTC
	, TM.CreatedByUser
	, TM.LastChangeDateUTC
	, TM.LastChangedByUser
	, T.IdNumber
	, T.VIN
	, T.CarrierID
	, T.Carrier
	, T.CarrierType
	, T.TrailerType
FROM viewTrailer T
JOIN tblTrailerMaintenance TM ON TM.TrailerID = T.ID
WHERE T.Active = 1

GO

/***********************************/
-- Date Created: 5 Aug 2013
-- Author: Kevin Alons
-- Purpose: return Truck Inspection records with translated "friendly" values + missing required records
/***********************************/
ALTER VIEW [dbo].[viewTruckInspection] AS
SELECT TI.ID
	, T.ID AS TruckID
	, TIT.ID AS TruckInspectionTypeID
	, TI.InspectionDate
	, TI.ExpirationDate
	, TI.Document
	, TI.DocName
	, TI.Notes
	, TI.CreateDateUTC
	, TI.CreatedByUser
	, TI.LastChangeDateUTC
	, TI.LastChangedByUser
	, CASE WHEN TI.ExpirationDate IS NULL THEN 'Missing' WHEN TI.ExpirationDate < GETDATE() THEN 'Overdue' ELSE 'Current' END AS Status
	, TIT.Name AS InspectionType
	, TIT.Required
	, TIT.ExpirationMonths
	, T.IdNumber
	, T.VIN
	, T.CarrierID
	, T.Carrier
	, T.CarrierType
FROM viewTruck T
CROSS JOIN tblTruckInspectionType TIT
LEFT JOIN tblTruckInspection TI ON TI.TruckID = T.ID AND TI.TruckInspectionTypeID = TIT.ID
WHERE TIT.Required = 1
  -- include any existing Inspection records or those missing required inspections
  AND (TIT.Required = 1 OR TI.ID IS NOT NULL)
  AND T.Active = 1
  AND TIT.DeleteDateUTC IS NULL

GO

/***********************************/
-- Date Created: 8 Aug 2013
-- Author: Kevin Alons
-- Purpose: return Truck Maintenance records with translated "friendly" values
/***********************************/
ALTER VIEW [dbo].[viewTruckMaintenance] AS
SELECT TM.ID
	, T.ID AS TruckID
	, TM.MaintenanceDate
	, TM.Description
	, TM.Document
	, TM.DocName
	, TM.Notes
	, TM.CreateDateUTC
	, TM.CreatedByUser
	, TM.LastChangeDateUTC
	, TM.LastChangedByUser
	, T.IdNumber
	, T.VIN
	, T.CarrierID
	, T.Carrier
	, T.CarrierType
FROM viewTruck T
JOIN tblTruckMaintenance TM ON TM.TruckID = T.ID
WHERE T.Active = 1

GO

/***********************************/
-- Date Created: 1 Jun 2013
-- Author: Kevin Alons
-- Purpose: add a new CarrierRouteRate
/***********************************/
ALTER PROCEDURE [dbo].[spCarrierRouteRate_Add]
(
  @CarrierID int
, @RouteID int
, @Rate smallmoney
, @EffectiveDate smalldatetime
, @ReplaceMatching bit
, @ReplaceAllOverlapping bit
, @UserName varchar(100)
, @Success bit = NULL out
, @Message varchar(255) = NULL out
) AS 
BEGIN
	DECLARE @removed int

	IF (@ReplaceMatching = 0)
	BEGIN
		IF ((SELECT count(1) FROM tblCarrierRouteRates WHERE CarrierID=@CarrierID AND RouteID=@RouteID AND EffectiveDate=@EffectiveDate) > 0)
		BEGIN
			SELECT @Success = 0, @Message = 'Skipped: Existing Route Rate already exists'
			RETURN
		END
	END
	ELSE
	BEGIN
		-- remove any matching/overlapping rate records
		IF (@ReplaceAllOverlapping = 1)
		BEGIN
			SELECT @removed = count(1) FROM tblCarrierRouteRates WHERE CarrierID=@CarrierID AND RouteID=@RouteID AND EffectiveDate>@EffectiveDate
			DELETE FROM tblCarrierRouteRates WHERE ID IN (
				SELECT ID FROM viewCarrierRouteRates WHERE CarrierID=@CarrierID AND RouteID=@RouteID AND EffectiveDate>@EffectiveDate)
		END
		ELSE
		BEGIN
			SELECT @removed = count(1) FROM tblCarrierRouteRates WHERE CarrierID=@CarrierID AND RouteID=@RouteID AND EffectiveDate=@EffectiveDate
			DELETE FROM tblCarrierRouteRates WHERE CarrierID=@CarrierID AND RouteID=@RouteID AND EffectiveDate=@EffectiveDate
		END
	END
	
	-- do the actual insert now
	INSERT INTO tblCarrierRouteRates (CarrierID, RouteID, Rate, EffectiveDate, CreateDateUTC, CreatedByUser) 
		VALUES (@CarrierID, @RouteID, @Rate, @EffectiveDate, GETUTCDATE(), @UserName)
	SELECT @success = 1, @Message = 'Rate added successfully' + isnull(' [' + ltrim(@removed) + ' replaced]', '')
END

GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 1 Jun 2013
-- Description:	provide a way to "clone" an existing set of Accessorial rates
-- =============================================
ALTER PROCEDURE [dbo].[spCloneCarrierAccessorialRates](@ID int, @UserName varchar(100)) AS
BEGIN
	DELETE FROM tblCarrierRates WHERE CarrierID IS NULL AND RegionID IS NULL
	INSERT INTO tblCarrierRates (CarrierID, RegionID, ChainupFee, WaitFee, RejectionFee, RerouteFee, H2SRate, FuelSurcharge
		, WaitFeeSubUnitID, WaitFeeRoundingTypeID, CreateDateUTC, CreatedByUser)
		
		SELECT 0, 0, ChainupFee, WaitFee, RejectionFee, RerouteFee, H2SRate, FuelSurcharge
			, WaitFeeSubUnitID, WaitFeeRoundingTypeID, GETUTCDATE(), @UserName
		FROM tblCarrierRates WHERE ID = @ID
END

GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 1 Jun 2013
-- Description:	provide a way to "clone" an existing set of Accessorial rates
-- =============================================
ALTER PROCEDURE [dbo].[spCloneCustomerAccessorialRates](@ID int, @UserName varchar(100)) AS
BEGIN
	DELETE FROM tblCustomerRates WHERE CustomerID IS NULL AND RegionID IS NULL
	INSERT INTO tblCustomerRates (CustomerID, RegionID, ChainupFee, WaitFee, RejectionFee, RerouteFee, H2SRate, FuelSurcharge
		, WaitFeeSubUnitID, WaitFeeRoundingTypeID, CreateDateUTC, CreatedByUser)
		
		SELECT 0, 0, ChainupFee, WaitFee, RejectionFee, RerouteFee, H2SRate, FuelSurcharge
			, WaitFeeSubUnitID, WaitFeeRoundingTypeID, GETUTCDATE(), @UserName
		FROM tblCustomerRates WHERE ID = @ID
END

GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Customer "Settlement" $$ values to an Audited order
/***********************************/
ALTER PROCEDURE [dbo].[spCreateCarrierSettlementBatch]
(
  @CarrierID int
, @Notes varchar(255)
, @UserName varchar(255)
, @BatchID int = NULL output
, @BatchNum int = NULL output
) AS BEGIN
	INSERT INTO dbo.tblCarrierSettlementBatch(CarrierID, BatchNum, BatchDate, Notes, CreateDateUTC, CreatedByUser)
		SELECT @CarrierID, isnull(max(BatchNum), 0) + 1, dbo.fnDateOnly(getdate()), @Notes, getutcdate(), @UserName 
		FROM dbo.tblCarrierSettlementBatch
		
		SELECT @BatchID = scope_identity()
		SELECT @BatchNum = BatchNum FROM dbo.tblCarrierSettlementBatch WHERE ID = @BatchID
END

GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Customer "Settlement" $$ values to an Audited order
/***********************************/
ALTER PROCEDURE [dbo].[spCreateCustomerSettlementBatch]
(
  @CustomerID int
, @Notes varchar(255)
, @UserName varchar(255)
, @BatchID int = NULL output
, @BatchNum int = NULL output
) AS BEGIN
	INSERT INTO dbo.tblCustomerSettlementBatch(CustomerID, BatchNum, BatchDate, Notes, CreateDateUTC, CreatedByUser)
		SELECT @CustomerID, isnull(max(BatchNum), 0) + 1, dbo.fnDateOnly(getdate()), @Notes, getutcdate(), @UserName 
		FROM dbo.tblCustomerSettlementBatch
		
		SELECT @BatchID = scope_identity()
		SELECT @BatchNum = BatchNum FROM dbo.tblCustomerSettlementBatch WHERE ID = @BatchID
END

GO

/***********************************/
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: create new Order (loads) for the specified criteria
/***********************************/
ALTER PROCEDURE [dbo].[spCreateLoads]
(
  @OriginID int
, @DestinationID int
, @TicketTypeID int
, @DueDate datetime
, @CustomerID int
, @CarrierID int = NULL
, @DriverID int = NULL
, @Qty int
, @UserName varchar(100)
, @TankNum varchar(20) = NULL
, @PriorityID int = 3 -- LOW priority
, @StatusID smallint = -10 -- GENERATED
, @ProductID int = 1 -- basic Crude Oil
) AS
BEGIN
	DECLARE @i int, @RouteID int, @ActualMiles int
	SELECT @i = 0
	
	WHILE @i < @Qty BEGIN
		DECLARE @PumperID int, @OperatorID int, @ProducerID int
		SELECT @PumperID = PumperID, @OperatorID = OperatorID, @ProducerID = ProducerID
		FROM tblOrigin WHERE ID = @OriginID
		
		INSERT INTO dbo.tblOrder (OriginID, DestinationID, TicketTypeID, DueDate, CustomerID
				, CarrierID, DriverID, StatusID, PriorityID, OriginTankNum
				, OrderNum, ProductID, PumperID, OperatorID, ProducerID, CreateDateUTC, CreatedByUser)
			VALUES (@OriginID, @DestinationID, @TicketTypeID, @DueDate, @CustomerID
				, @CarrierID, @DriverID, @StatusID, @PriorityID, @TankNum
				, (SELECT isnull((SELECT max(OrderNum) FROM tblOrder), 100000) + 1), 1 -- Crude Oil
				, @PumperID, @OperatorID, @ProducerID, GETUTCDATE(), @UserName)
		
		IF (@DriverID IS NOT NULL)
		BEGIN
			UPDATE tblOrder SET TruckID = D.TruckID, TrailerID = D.TrailerID, Trailer2ID = D.Trailer2ID
			FROM tblOrder O
			JOIN tblDriver D ON D.ID = O.DriverID
			WHERE O.ID = SCOPE_IDENTITY()
		END
		
		SET @i = @i + 1
	END
END

GO

/***********************************/
-- Date Created: 1 Jun 2013
-- Author: Kevin Alons
-- Purpose: add a new CustomerRouteRate
/***********************************/
ALTER PROCEDURE [dbo].[spCustomerRouteRate_Add]
(
  @CustomerID int
, @RouteID int
, @Rate smallmoney
, @EffectiveDate smalldatetime
, @ReplaceMatching bit
, @ReplaceAllOverlapping bit
, @UserName varchar(100)
, @Success bit = NULL out
, @Message varchar(255) = NULL out
) AS 
BEGIN
	DECLARE @removed int

	IF (@ReplaceMatching = 0)
	BEGIN
		IF ((SELECT count(1) FROM tblCustomerRouteRates WHERE CustomerID=@CustomerID AND RouteID=@RouteID AND EffectiveDate=@EffectiveDate) > 0)
		BEGIN
			SELECT @Success = 0, @Message = 'Skipped: Existing Route Rate already exists'
			RETURN
		END
	END
	ELSE
	BEGIN
		-- remove any matching/overlapping rate records
		IF (@ReplaceAllOverlapping = 1)
		BEGIN
			SELECT @removed = count(1) FROM tblCustomerRouteRates WHERE CustomerID=@CustomerID AND RouteID=@RouteID AND EffectiveDate>@EffectiveDate
			DELETE FROM tblCustomerRouteRates WHERE ID IN (
				SELECT ID FROM viewCustomerRouteRates WHERE CustomerID=@CustomerID AND RouteID=@RouteID AND EffectiveDate>@EffectiveDate)
		END
		ELSE
		BEGIN
			SELECT @removed = count(1) FROM tblCustomerRouteRates WHERE CustomerID=@CustomerID AND RouteID=@RouteID AND EffectiveDate=@EffectiveDate
			DELETE FROM tblCustomerRouteRates WHERE CustomerID=@CustomerID AND RouteID=@RouteID AND EffectiveDate=@EffectiveDate
		END
	END
	
	-- do the actual insert now
	INSERT INTO tblCustomerRouteRates (CustomerID, RouteID, Rate, EffectiveDate, CreateDateUTC, CreatedByUser) 
		VALUES (@CustomerID, @RouteID, @Rate, @EffectiveDate, GETUTCDATE(), @UserName)
	SELECT @success = 1, @Message = 'Rate added successfully' + isnull(' [' + ltrim(@removed) + ' replaced]', '')
END

GO

/***********************************************************************/
-- Date Created: 18 Apr 2013
-- Author: Kevin Alons
-- Purpose: return the DriverApp.MasterData for a single driver/login
/***********************************************************************/
ALTER FUNCTION [dbo].[fnDriverMasterData](@Valid bit, @DriverID int, @UserName varchar(100)) RETURNS TABLE AS 
RETURN
	SELECT CASE WHEN DS.DriverID IS NULL THEN 0 ELSE @Valid END AS Valid
		, @UserName AS UserName
		, @DriverID AS DriverID
		, D.FullName AS DriverName
		, D.MobilePrint
		, DS.LastSyncUTC
		, cast(SSF.Value as int) AS SyncFrequencyMinutes
		, SSV.Value AS SchemaVersion
		, LAV.Value AS LatestAppVersion
		, DS.PasswordHash
	FROM viewDriver D
	LEFT JOIN tblDriver_Sync DS ON DS.DriverID = D.ID
	JOIN tblSetting SSV ON SSV.ID = 0  -- Schema Version
	JOIN tblSetting SSF ON SSF.ID = 11 -- sync frequency (in minutes)
	JOIN tblSetting LAV ON LAV.ID = 12 -- LatestAppVersion
	WHERE D.ID = @DriverID

GO

/*******************************************/
-- Date Created: 22 Apr 2013
-- Author: Kevin Alons
-- Purpose: validate parameters, if valid insert/update the tblDriver_Sync table for the specified DriverID
/*******************************************/
ALTER PROCEDURE [dbo].[spDriver_Sync]
(
  @UserName varchar(100)
, @DriverID int
, @SyncDateUTC datetime = NULL
, @PasswordHash varchar(25) = NULL
, @Valid bit = NULL output
, @Message varchar(255) = NULL output
) AS
BEGIN
	-- if resetting, delete the entire record (it will be recreated below)
	IF (@SyncDateUTC IS NULL)
	BEGIN
		DELETE FROM tblDriver_Sync WHERE DriverID = @DriverID
		-- result of count(*) query will be 1 or 0 (true or false respectively)
		SELECT @Valid = (SELECT COUNT(*) FROM tblDriver WHERE ID = @DriverID)
		IF (@Valid = 0)
			SELECT @Message = 'DriverID was not valid'
	END
	ELSE
	BEGIN
		-- result of count(*) query will be 1 or 0 (true or false respectively)
		SELECT @Valid = (SELECT count(*) FROM tblDriver_Sync WHERE DriverID = @DriverID AND PasswordHash = @PasswordHash)
		IF (@Valid = 0)
			SELECT @Message = 'PasswordHash was not valid'
	END
	
	IF (@Valid = 1)
	BEGIN
		-- if a sync record already exists, just update the new LastSync value
		UPDATE tblDriver_Sync SET LastSyncUTC = @SyncDateUTC WHERE DriverID = @DriverID
		-- otherwise insert a new record with a new passwordhash value
		INSERT INTO tblDriver_Sync (DriverID, LastSyncUTC, PasswordHash)
			SELECT @DriverID, NULL, dbo.fnGeneratePasswordHash()
			FROM tblDriver D
			LEFT JOIN tblDriver_Sync DS ON DS.DriverID = D.ID
			WHERE D.ID = @DriverID AND DS.DriverID IS NULL

		-- return the current "Master" data
		SELECT * FROM dbo.fnDriverMasterData(@Valid, @DriverID, @UserName)
	END
	ELSE
	BEGIN
		SELECT @Valid AS Valid, @UserName AS UserName, 0 AS DriverID, NULL AS DriverName, 0 AS MobilePrint
			, NULL AS LastSyncUTC
			, (SELECT Value FROM tblSetting WHERE ID = 0) AS SchemaVersion
			, (SELECT cast(Value as int) FROM tblSetting WHERE ID = 11) AS SyncFrequencyMinutes
			, (SELECT Value FROM tblSetting WHERE ID = 12) AS LatestAppVersion
			, NULL AS PasswordHash
		FROM tblSetting S WHERE S.ID = 0
	END 
END

GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
/***********************************/
ALTER PROCEDURE [dbo].[spOrdersExportCarrier]
(
  @StartDate datetime = NULL
, @EndDate datetime = NULL
, @CarrierID int = -1 -- all carriers
, @BatchID int = NULL  -- either show unbatched (NULL), or the specified batch orders
) AS BEGIN
	SELECT @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	
	SELECT OE.* 
		, dbo.fnLocal_to_UTC(ISNULL(OIC.LastChangeDateUTC, OIC.CreateDateUTC), OE.OriginTimeZoneID, OE.OriginUseDST) AS RatesAppliedDate
		, SB.BatchNum
		, OIC.ChainupFee
		, OIC.RerouteFee
		, OIC.BillableWaitMinutes
		, OIC.WaitFeeSubUnitID
		, OIC.WaitFeeRoundingTypeID
		, OIC.WaitRate
		, OIC.WaitFee
		, OIC.RejectionFee
		, OIC.H2SRate
		, OIC.H2SFee
		, OIC.TaxRate
		, OIC.RouteRate
		, OIC.LoadFee
		, OIC.TotalFee
		, OIC.FuelSurcharge
		, C.MinSettlementBarrels
		, cast(CASE WHEN OE.TicketCount = 0 THEN OE.TicketType ELSE NULL END as varchar(max)) AS TicketTypes
		, cast(CASE WHEN OE.TicketCount = 0 THEN OE.CarrierTicketNum ELSE NULL END as varchar(max)) AS TicketNums
		, cast(NULL AS varchar(max)) AS TankNums
		, cast(NULL as varchar(max)) AS ProductObsTemps
		, cast(NULL as varchar(max)) AS ProductObsGravities
		, cast(NULL as varchar(max)) AS ProductBSWs
		, cast(NULL as varchar(max)) AS TicketsRejected
		, cast(NULL as varchar(max)) AS TicketsRejectNotes
		, cast(NULL as varchar(max)) AS TicketsGrossBBLS
		, cast(NULL as varchar(max)) AS TicketsNetBBLS
		, cast(NULL as varchar(max)) AS PreviousDestinations
		, cast(NULL as varchar(max)) AS RerouteUsers
		, cast(NULL as varchar(max)) AS RerouteDates
		, cast(NULL as varchar(max)) AS RerouteNotes
	INTO #Orders
	FROM dbo.viewOrderExportFull OE
	JOIN dbo.tblCarrier C ON C.ID = OE.CarrierID
	LEFT JOIN dbo.tblOrderInvoiceCarrier OIC ON OIC.OrderID = OE.ID
	LEFT JOIN dbo.tblCarrierSettlementBatch SB ON SB.ID = OIC.BatchID
	WHERE OE.StatusID IN (4)  
	  AND (@CarrierID=-1 OR OE.CarrierID=@CarrierID) 
	  AND (@StartDate IS NULL OR OE.OrderDate >= @StartDate) 
	  AND (@EndDate IS NULL OR OE.OrderDate <= @EndDate)
	  AND ((@BatchID IS NULL AND (OIC.ID IS NULL OR OIC.BatchID IS NULL)) OR OIC.BatchID = @BatchID)
	  AND OE.DeleteDateUTC IS NULL
	ORDER BY OE.OriginDepartTimeUTC
	
	DECLARE @OrderID int, @TicketID int, @RerouteID int
	SELECT @OrderID = min(ID) FROM #Orders WHERE ID > 0
	PRINT isnull(@OrderID, -1)
	
	WHILE @OrderID IS NOT NULL
	BEGIN
		-- add the Tickets information to the order (done this way since there could be multiple)
		SELECT @TicketID = min(ID) FROM dbo.tblOrderTicket WHERE OrderID = @OrderID AND DeleteDateUTC IS NULL AND ID > 0
		WHILE @TicketID IS NOT NULL
		BEGIN
			UPDATE #Orders 
				SET TicketTypes = isnull(O.TicketTypes + '<br/>', '') + OT.TicketType
					, TicketNums = isnull(O.TicketNums + '<br/>', '') + OT.CarrierTicketNum
					, TankNums = isnull(O.TankNums + '<br/>', '') + OT.TankNum
					, ProductObsTemps = isnull(O.ProductObsTemps + '<br/>', '') + isnull(ltrim(OT.ProductObsTemp), '')
					, ProductObsGravities = isnull(O.ProductObsGravities + '<br/>', '') + isnull(ltrim(OT.ProductObsGravity), '')
					, ProductBSWs = isnull(O.ProductBSWs + '<br/>', '') + isnull(ltrim(OT.ProductBSW), '')
					, TicketsRejected = isnull(O.TicketsRejected + '<br/>', '') + (CASE WHEN OT.Rejected = 0 THEN 'Valid' ELSE 'Rejected' END)
					, TicketsRejectNotes = isnull(O.TicketsRejectNotes + '<br/>', '') + isnull(OT.RejectNotes, '')
					, TicketsGrossBBLS = isnull(O.TicketsGrossBBLS + '<br/>', '') + isnull(ltrim(GrossBarrels), '')
					, TicketsNetBBLS = isnull(O.TicketsNetBBLS + '<br/>', '') + isnull(ltrim(NetBarrels), '')
			FROM #Orders O, viewOrderTicket OT
			WHERE O.ID = @OrderID AND OT.ID = @TicketID
			SELECT @TicketID = min(ID) FROM tblOrderTicket WHERE OrderID = @OrderID AND ID > @TicketID AND DeleteDateUTC IS NULL
		END	
		-- add the Reroutes information to the order (done this way since there could be multiple)
		SELECT @RerouteID = min(ID) FROM dbo.tblOrderReroute WHERE OrderID = @OrderID AND ID > 0
		WHILE @RerouteID IS NOT NULL
		BEGIN
			UPDATE #Orders
				SET PreviousDestinations = O.PreviousDestinations + '<br/>' + O_R.PreviousDestinationFull
					, RerouteUsers = O.RerouteUsers + '<br/>' + O_R.UserName
					, RerouteDates = O.RerouteDates + '<br/>' + dbo.fnDateMdYY(O_R.RerouteDate)
					, RerouteNotes = isnull(O.RerouteNotes + '<br/>', '') + isnull(O_R.Notes, '')
			FROM #Orders O, viewOrderReroute O_R
			WHERE O.ID = @OrderID AND O_R.ID = @RerouteID
			SELECT @RerouteID = min(ID) FROM tblOrderReroute WHERE OrderID = @OrderID AND ID > @RerouteID
		END
		SELECT @OrderID = min(ID) FROM tblOrder WHERE ID > @OrderID
	END
	SELECT * FROM #Orders
END

GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
/***********************************/
ALTER PROCEDURE [dbo].[spOrdersExportCustomer]
(
  @StartDate datetime = NULL
, @EndDate datetime = NULL
, @CustomerID int = -1 -- all carriers
, @BatchID int = NULL  -- either show unbatched (NULL), or the specified batch orders
) AS BEGIN
	SELECT @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	
	SELECT OE.* 
		, dbo.fnLocal_to_UTC(ISNULL(OIC.LastChangeDateUTC, OIC.CreateDateUTC), OE.OriginTimeZoneID, OE.OriginUseDST) AS RatesAppliedDate
		, SB.BatchNum
		, OIC.ChainupFee
		, OIC.RerouteFee
		, OIC.BillableWaitMinutes
		, OIC.WaitFeeSubUnitID
		, OIC.WaitFeeRoundingTypeID
		, OIC.WaitRate
		, OIC.WaitFee
		, OIC.RejectionFee
		, OIC.H2SRate
		, OIC.H2SFee
		, OIC.TaxRate
		, OIC.RouteRate
		, OIC.LoadFee
		, OIC.TotalFee
		, OIC.FuelSurcharge
		, C.MinSettlementBarrels
		, cast(CASE WHEN OE.TicketCount = 0 THEN OE.TicketType ELSE NULL END as varchar(max)) AS TicketTypes
		, cast(CASE WHEN OE.TicketCount = 0 THEN OE.CarrierTicketNum ELSE NULL END as varchar(max)) AS TicketNums
		, cast(NULL AS varchar(max)) AS TankNums
		, cast(NULL as varchar(max)) AS ProductObsTemps
		, cast(NULL as varchar(max)) AS ProductObsGravities
		, cast(NULL as varchar(max)) AS ProductBSWs
		, cast(NULL as varchar(max)) AS TicketsRejected
		, cast(NULL as varchar(max)) AS TicketsRejectNotes
		, cast(NULL as varchar(max)) AS TicketsGrossBBLS
		, cast(NULL as varchar(max)) AS TicketsNetBBLS
		, cast(NULL as varchar(max)) AS PreviousDestinations
		, cast(NULL as varchar(max)) AS RerouteUsers
		, cast(NULL as varchar(max)) AS RerouteDates
		, cast(NULL as varchar(max)) AS RerouteNotes
	INTO #Orders
	FROM dbo.viewOrderExportFull OE
	JOIN dbo.tblCustomer C ON C.ID = OE.CustomerID
	LEFT JOIN dbo.tblOrderInvoiceCustomer OIC ON OIC.OrderID = OE.ID
	LEFT JOIN dbo.tblCustomerSettlementBatch SB ON SB.ID = OIC.BatchID
	WHERE OE.StatusID IN (4)  
	  AND (@CustomerID=-1 OR OE.CustomerID=@CustomerID) 
	  AND (@StartDate IS NULL OR OE.OrderDate >= @StartDate) 
	  AND (@EndDate IS NULL OR OE.OrderDate <= @EndDate)
	  AND ((@BatchID IS NULL AND (OIC.ID IS NULL OR OIC.BatchID IS NULL)) OR OIC.BatchID = @BatchID)
	  AND OE.DeleteDateUTC IS NULL
	ORDER BY OE.OrderDate
	
	DECLARE @OrderID int, @TicketID int, @RerouteID int
	SELECT @OrderID = min(ID) FROM #Orders WHERE ID > 0
	PRINT isnull(@OrderID, -1)
	
	WHILE @OrderID IS NOT NULL
	BEGIN
		-- add the Tickets information to the order (done this way since there could be multiple)
		SELECT @TicketID = min(ID) FROM dbo.tblOrderTicket WHERE OrderID = @OrderID AND DeleteDateUTC IS NULL AND ID > 0
		WHILE @TicketID IS NOT NULL
		BEGIN
			UPDATE #Orders 
				SET TicketTypes = isnull(O.TicketTypes + '<br/>', '') + OT.TicketType
					, TicketNums = isnull(O.TicketNums + '<br/>', '') + OT.CarrierTicketNum
					, TankNums = isnull(O.TankNums + '<br/>', '') + OT.TankNum
					, ProductObsTemps = isnull(O.ProductObsTemps + '<br/>', '') + isnull(ltrim(OT.ProductObsTemp), '')
					, ProductObsGravities = isnull(O.ProductObsGravities + '<br/>', '') + isnull(ltrim(OT.ProductObsGravity), '')
					, ProductBSWs = isnull(O.ProductBSWs + '<br/>', '') + isnull(ltrim(OT.ProductBSW), '')
					, TicketsRejected = isnull(O.TicketsRejected + '<br/>', '') + (CASE WHEN OT.Rejected = 0 THEN 'Valid' ELSE 'Rejected' END)
					, TicketsRejectNotes = isnull(O.TicketsRejectNotes + '<br/>', '') + isnull(OT.RejectNotes, '')
					, TicketsGrossBBLS = isnull(O.TicketsGrossBBLS + '<br/>', '') + isnull(ltrim(GrossBarrels), '')
					, TicketsNetBBLS = isnull(O.TicketsNetBBLS + '<br/>', '') + isnull(ltrim(NetBarrels), '')
			FROM #Orders O, viewOrderTicket OT
			WHERE O.ID = @OrderID AND OT.ID = @TicketID
			SELECT @TicketID = min(ID) FROM tblOrderTicket WHERE OrderID = @OrderID AND ID > @TicketID AND DeleteDateUTC IS NULL
		END	
		-- add the Reroutes information to the order (done this way since there could be multiple)
		SELECT @RerouteID = min(ID) FROM dbo.tblOrderReroute WHERE OrderID = @OrderID AND ID > 0
		WHILE @RerouteID IS NOT NULL
		BEGIN
			UPDATE #Orders
				SET PreviousDestinations = O.PreviousDestinations + '<br/>' + O_R.PreviousDestinationFull
					, RerouteUsers = O.RerouteUsers + '<br/>' + O_R.UserName
					, RerouteDates = O.RerouteDates + '<br/>' + dbo.fnDateMdYY(O_R.RerouteDate)
					, RerouteNotes = isnull(O.RerouteNotes + '<br/>', '') + isnull(O_R.Notes, '')
			FROM #Orders O, viewOrderReroute O_R
			WHERE O.ID = @OrderID AND O_R.ID = @RerouteID
			SELECT @RerouteID = min(ID) FROM tblOrderReroute WHERE OrderID = @OrderID AND ID > @RerouteID
		END
		SELECT @OrderID = min(ID) FROM tblOrder WHERE ID > @OrderID
	END
	SELECT * FROM #Orders
END

GO

/***********************************/
-- Date Created: 28 Feb 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc for 3rd parties (Customer/Carrier)
/***********************************/
ALTER PROCEDURE [dbo].[spOrdersFullExport]
(
  @StartDate datetime
, @EndDate datetime
, @CarrierID int = 0 -- -1 = all carriers
, @CustomerID int = 0 -- -1 = all customers
, @ProducerID int = 0
, @AuditedOnly bit = 0
) AS BEGIN
	SELECT @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	
	SELECT * 
		, cast(CASE WHEN TicketCount = 0 THEN TicketType ELSE NULL END as varchar(max)) AS TicketTypes
		, cast(CASE WHEN TicketCount = 0 THEN CarrierTicketNum ELSE NULL END as varchar(max)) AS TicketNums
		, cast(NULL as varchar(max)) AS BOLNums
		, cast(NULL AS varchar(max)) AS TankNums
		, CAST(NULL AS varchar(max)) AS OpenReadings
		, CAST(NULL AS varchar(max)) AS CloseReadings
		, cast(NULL as varchar(max)) AS ProductObsTemps
		, cast(NULL as varchar(max)) AS ProductObsGravities
		, cast(NULL as varchar(max)) AS ProductBSWs
		, cast(NULL as varchar(max)) AS TicketsRejected
		, cast(NULL as varchar(max)) AS TicketsRejectNotes
		, cast(NULL as varchar(max)) AS TicketsGrossBBLS
		, cast(NULL as varchar(max)) AS TicketsNetBBLS
		, cast(NULL as varchar(max)) AS SealOffs
		, cast(NULL as varchar(max)) AS SealOns
		, cast(NULL as varchar(max)) AS PreviousDestinations
		, cast(NULL as varchar(max)) AS RerouteUsers
		, cast(NULL as varchar(max)) AS RerouteDates
		, cast(NULL as varchar(max)) AS RerouteNotes
	INTO #Orders
	FROM dbo.viewOrderExportFull
	WHERE (@CarrierID=-1 OR @CustomerID=-1 OR CarrierID=@CarrierID OR CustomerID=@CustomerID OR ProducerID=@ProducerID) 
	  AND OrderDate BETWEEN @StartDate AND @EndDate
	  AND DeleteDateUTC IS NULL
	  AND (@AuditedOnly = 0 OR StatusID = 4)
	ORDER BY OrderDate
	
	DECLARE @OrderID int, @TicketID int, @RerouteID int
	SELECT @OrderID = min(ID) FROM #Orders WHERE ID > 0
	PRINT isnull(@OrderID, -1)
	
	WHILE @OrderID IS NOT NULL
	BEGIN
		-- add the Tickets information to the order (done this way since there could be multiple)
		SELECT @TicketID = min(ID) FROM dbo.tblOrderTicket WHERE OrderID = @OrderID AND DeleteDateUTC IS NULL AND ID > 0
		WHILE @TicketID IS NOT NULL
		BEGIN
			UPDATE #Orders 
				SET TicketTypes = isnull(O.TicketTypes + '<br/>', '') + OT.TicketType
					, TicketNums = isnull(O.TicketNums + '<br/>', '') + OT.CarrierTicketNum
					, BOLNums = isnull(O.BOLNums + '<br/>', '') + OT.BOLNum
					, TankNums = isnull(O.TankNums + '<br/>', '') + OT.TankNum
					, OpenReadings = ISNULL(ltrim(OT.OpeningGaugeFeet) + 'ft ' + ltrim(OT.OpeningGaugeInch) + 'in ' + ltrim(OT.OpeningGaugeQ) + 'q' + '<br/>', '')
					, CloseReadings = ISNULL(ltrim(OT.ClosingGaugeFeet) + 'ft ' + ltrim(OT.ClosingGaugeInch) + 'in ' + ltrim(OT.ClosingGaugeQ) + 'q' + '<br/>', '')
					, ProductObsTemps = isnull(O.ProductObsTemps + '<br/>', '') + isnull(ltrim(OT.ProductObsTemp), '')
					, ProductObsGravities = isnull(O.ProductObsGravities + '<br/>', '') + isnull(ltrim(OT.ProductObsGravity), '')
					, ProductBSWs = isnull(O.ProductBSWs + '<br/>', '') + isnull(ltrim(OT.ProductBSW), '')
					, SealOffs = isnull(O.SealOffs + '<br/>', '') + isnull(ltrim(OT.SealOff), '')
					, SealOns = isnull(O.SealOns + '<br/>', '') + isnull(ltrim(OT.SealOn), '')
					, TicketsRejected = isnull(O.TicketsRejected + '<br/>', '') + (CASE WHEN OT.Rejected = 0 THEN 'Valid' ELSE 'Rejected' END)
					, TicketsRejectNotes = isnull(O.TicketsRejectNotes + '<br/>', '') + isnull(OT.RejectNotes, '')
					, TicketsGrossBBLS = isnull(O.TicketsGrossBBLS + '<br/>', '') + isnull(ltrim(GrossBarrels), '')
					, TicketsNetBBLS = isnull(O.TicketsNetBBLS + '<br/>', '') + isnull(ltrim(NetBarrels), '')
			FROM #Orders O, viewOrderTicket OT
			WHERE O.ID = @OrderID AND OT.ID = @TicketID
			SELECT @TicketID = min(ID) FROM tblOrderTicket WHERE OrderID = @OrderID AND ID > @TicketID AND DeleteDateUTC IS NULL
		END	
		-- add the Reroutes information to the order (done this way since there could be multiple)
		SELECT @RerouteID = min(ID) FROM dbo.tblOrderReroute WHERE OrderID = @OrderID AND ID > 0
		WHILE @RerouteID IS NOT NULL
		BEGIN
			UPDATE #Orders
				SET PreviousDestinations = isnull(O.PreviousDestinations + '<br/>', '') + O_R.PreviousDestinationFull
					, RerouteUsers = isnull(O.RerouteUsers + '<br/>', '') + O_R.UserName
					, RerouteDates = isnull(O.RerouteDates + '<br/>', '') + dbo.fnDateMdYY(O_R.RerouteDate)
					, RerouteNotes = isnull(O.RerouteNotes + '<br/>', '') + isnull(O_R.Notes, '')
			FROM #Orders O, viewOrderReroute O_R
			WHERE O.ID = @OrderID AND O_R.ID = @RerouteID
			SELECT @RerouteID = min(ID) FROM tblOrderReroute WHERE OrderID = @OrderID AND ID > @RerouteID
		END
		SELECT @OrderID = min(ID) FROM tblOrder WHERE ID > @OrderID
	END
	SELECT * FROM #Orders
END

GO

/***********************************/
-- Date Created: 28 Feb 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc for 3rd parties (Customer/Carrier)
/***********************************/
ALTER PROCEDURE [dbo].[spOrderTicketFullExport]
(
  @StartDate datetime
, @EndDate datetime
, @CarrierID int = 0 -- -1 = all carriers
, @CustomerID int = 0 -- -1 = all customers
) AS BEGIN
	SELECT @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	
	SELECT OT.ID
		, O.OrderNum
		, O.OrderStatus
		, OT.TicketType 
		, OO.WellAPI
		, O.OriginDepartTimeUTC
		, OT.CarrierTicketNum AS TicketNum
		, OO.Name AS Origin
		, OO.LeaseName
		, O.Operator
		, O.Destination
		, OT.NetBarrels
		, OT.GrossBarrels
		, TT.HeightFeet AS TankHeight
		, TT.CapacityBarrels AS TankBarrels
		, OT.TankNum
		, OT.ProductObsGravity
		, OT.ProductObsTemp
		, OT.ProductBSW
		, isnull(OT.ProductHighTemp, OT.ProductObsTemp) AS ProductHighTemp
		, OT.OpeningGaugeFeet
		, OT.OpeningGaugeInch
		, OT.OpeningGaugeQ
		, OT.ClosingGaugeFeet
		, OT.ClosingGaugeInch
		, OT.ClosingGaugeQ
		, isnull(OT.ProductLowTemp, OT.ProductObsTemp) AS ProductLowTemp
		, dbo.fnGaugeQtrInches(OT.OpeningGaugeFeet, OT.OpeningGaugeInch, OT.OpeningGaugeQ) AS OpenTotalQ
		, dbo.fnGaugeQtrInches(OT.ClosingGaugeFeet, OT.ClosingGaugeInch, ot.ClosingGaugeQ) AS CloseTotalQ
	FROM dbo.viewOrderTicket OT
	JOIN dbo.viewOrder O ON O.ID = OT.OrderID
	JOIN dbo.tblOrigin OO ON OO.ID = O.OriginID
	LEFT JOIN dbo.tblTankType TT ON TT.ID = OT.TankTypeID
	WHERE O.StatusID IN (3, 4) -- Delivered & Audited
	  AND (@CarrierID=-1 OR @CustomerID=-1 OR O.CarrierID=@CarrierID OR O.CustomerID=@CustomerID) 
	  AND OrderDate BETWEEN @StartDate AND @EndDate
	  AND O.DeleteDateUTC IS NULL AND OT.DeleteDateUTC IS NULL
	ORDER BY O.OrderDate
END

GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Carrier "Settlement" $$ values to an Audited order
/***********************************/
ALTER PROCEDURE [dbo].[spProcessCarrierInvoice]
(
  @ID int
, @UserName varchar(100)
, @ChainupFee smallmoney = NULL
, @WaitFee smallmoney = NULL
, @RerouteFee smallmoney = NULL
, @RejectionFee smallmoney = NULL
, @H2SFee smallmoney = NULL
, @LoadFee smallmoney = NULL
) AS BEGIN
	EXEC dbo.spSyncActualMiles @ID
	
	DELETE FROM tblOrderInvoiceCarrier WHERE OrderID = @ID
	
	INSERT INTO tblOrderInvoiceCarrier (OrderID, ChainupFee, RerouteFee, WaitFeeSubUnitID, WaitFeeRoundingTypeID, BillableWaitMinutes, WaitRate, WaitFee
		, RejectionFee, RouteRate, LoadFee, H2SRate, H2SFee, FuelSurcharge, TaxRate
		, TotalFee, CreateDateUTC, CreatedByUser)
	SELECT D.ID, ChainupFee, RerouteFee, WaitFeeSubUnitID, WaitFeeRoundingTypeID, BillableWaitMinutes, WaitRate, WaitFee
		, RejectionFee, RouteRate, LoadFee, H2SRate, H2SFee, FuelSurcharge, TaxRate
		, ChainupFee + RerouteFee + WaitFee + H2SFee + LoadFee AS TotalFee, GETUTCDATE(), @UserName
	FROM (
		SELECT ID
			, coalesce(@ChainupFee, Chainup * ChainupFee, 0) AS ChainupFee
			, coalesce(@RerouteFee, RerouteFee * RerouteCount, 0) AS RerouteFee
			, WaitFeeSubUnitID
			, WaitFeeRoundingTypeID
			, cast(BillableWaitHours * 60 as int) AS BillableWaitMinutes
			, WaitFee AS WaitRate
			, coalesce(@WaitFee, BillableWaitHours * WaitFee, 0) AS WaitFee
			, coalesce(@RejectionFee, Rejected * RejectionFee, 0) AS RejectionFee
			, isnull(H2SRate, 0) AS H2SRate
			, coalesce(@H2SFee, H2S * dbo.fnMaxDecimal(MinSettlementBarrels, ActualBarrels) * H2SRate, 0) AS H2SFee
			, isnull(TaxRate, 0) AS TaxRate
			, isnull(Rate, 0) AS RouteRate
			, coalesce(@LoadFee, dbo.fnMaxDecimal(MinSettlementBarrels, ActualBarrels) * Rate, 0) AS LoadFee
			, isnull(FuelSurcharge, 0) AS FuelSurcharge
		FROM (
			SELECT S.ID
				, S.ChainUp
				, CR.ChainupFee
				, CR.RerouteFee
				, S.RerouteCount
				, S.TotalWaitMinutes
				, CR.WaitFeeSubUnitID
				, CR.WaitFeeRoundingTypeID
				, dbo.fnComputeBillableWaitHours(S.TotalWaitMinutes, CR.WaitFeeSubUnitID, CR.WaitFeeRoundingTypeID) AS BillableWaitHours
				, CR.WaitFee
				, S.Rejected
				, CR.RejectionFee
				, S.H2S
				, isnull(S.H2S * CR.H2SRate, 0) AS H2SRate
				, S.TaxRate
				, dbo.fnCarrierRouteRate(S.CarrierID, S.RouteID, S.OrderDate) AS Rate
				, S.MinSettlementBarrels
				, S.ActualBarrels
				, CR.FuelSurcharge
			FROM (
				SELECT O.ID
					, O.CarrierID
					, O.RouteID
					, O.ChainUp
					, CASE WHEN C.SettlementFactorID = 1 THEN O.OriginGrossBarrels ELSE O.OriginNetBarrels END AS ActualBarrels
					, O.RerouteCount
					, O.TotalWaitMinutes
					, O.Rejected
					, OO.H2S
					, O.OrderDate
					, coalesce(CRXX.ID, CRXA.ID, CRAX.ID, CRAA.ID) AS CRID
					, isnull(C.MinSettlementBarrels, 0) AS MinSettlementBarrels
					, OO.TaxRate
				FROM dbo.viewOrderExportFull O
				JOIN tblOrigin OO ON OO.ID = O.OriginID
				JOIN tblCarrier C ON C.ID = O.CarrierID
				LEFT JOIN tblCarrierRates CRXX ON CRXX.CarrierID = O.CarrierID AND CRXX.RegionID = OO.RegionID
				LEFT JOIN tblCarrierRates CRXA ON CRXA.CarrierID = O.CarrierID AND CRXA.RegionID = -1
				LEFT JOIN tblCarrierRates CRAX ON CRAX.CarrierID = -1 AND CRAX.RegionID = OO.RegionID
				LEFT JOIN tblCarrierRates CRAA ON CRAA.CarrierID = -1 AND CRAA.RegionID = -1
				WHERE O.ID = @ID
			) S
			LEFT JOIN tblCarrierRates CR ON CR.ID = S.CRID
		) SS
	) D
END

GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Customer "Settlement" $$ values to an Audited order
/***********************************/
ALTER PROCEDURE [dbo].[spProcessCustomerInvoice]
(
  @ID int
, @UserName varchar(100)
, @ChainupFee smallmoney = NULL
, @WaitFee smallmoney = NULL
, @RerouteFee smallmoney = NULL
, @RejectionFee smallmoney = NULL
, @H2SFee smallmoney = NULL
, @LoadFee smallmoney = NULL
) AS BEGIN
	EXEC dbo.spSyncActualMiles @ID
	
	DELETE FROM tblOrderInvoiceCustomer WHERE OrderID = @ID
	
	INSERT INTO tblOrderInvoiceCustomer (OrderID, ChainupFee, RerouteFee, WaitFeeSubUnitID, WaitFeeRoundingTypeID, BillableWaitMinutes, WaitRate, WaitFee
		, RejectionFee, RouteRate, LoadFee, H2SRate, H2SFee, FuelSurcharge, TaxRate
		, TotalFee, CreateDateUTC, CreatedByUser)
	SELECT D.ID, ChainupFee, RerouteFee, WaitFeeSubUnitID, WaitFeeRoundingTypeID, BillableWaitMinutes, WaitRate, WaitFee
		, RejectionFee, RouteRate, LoadFee, H2SRate, H2SFee, FuelSurcharge, TaxRate
		, ChainupFee + RerouteFee + WaitFee + H2SFee + LoadFee AS TotalFee, GETUTCDATE(), @UserName
	FROM (
		SELECT ID
			, coalesce(@ChainupFee, Chainup * ChainupFee, 0) AS ChainupFee
			, coalesce(@RerouteFee, RerouteFee * RerouteCount, 0) AS RerouteFee
			, WaitFeeSubUnitID
			, WaitFeeRoundingTypeID
			, cast(BillableWaitHours * 60 as int) AS BillableWaitMinutes
			, WaitFee AS WaitRate
			, coalesce(@WaitFee, BillableWaitHours * WaitFee, 0) AS WaitFee
			, coalesce(@RejectionFee, Rejected * RejectionFee, 0) AS RejectionFee
			, isnull(H2SRate, 0) AS H2SRate
			, coalesce(@H2SFee, H2S * dbo.fnMaxDecimal(MinSettlementBarrels, ActualBarrels) * H2SRate, 0) AS H2SFee
			, isnull(TaxRate, 0) AS TaxRate
			, isnull(Rate, 0) AS RouteRate
			, coalesce(@LoadFee, dbo.fnMaxDecimal(MinSettlementBarrels, ActualBarrels) * Rate, 0) AS LoadFee
			, isnull(FuelSurcharge, 0) AS FuelSurcharge
		FROM (
			SELECT S.ID
				, S.ChainUp
				, CR.ChainupFee
				, CR.RerouteFee
				, S.RerouteCount
				, S.TotalWaitMinutes
				, CR.WaitFeeSubUnitID
				, CR.WaitFeeRoundingTypeID
				, dbo.fnComputeBillableWaitHours(S.TotalWaitMinutes, CR.WaitFeeSubUnitID, CR.WaitFeeRoundingTypeID) AS BillableWaitHours
				, CR.WaitFee
				, S.Rejected
				, CR.RejectionFee
				, S.H2S
				, isnull(S.H2S * CR.H2SRate, 0) AS H2SRate
				, S.TaxRate
				, dbo.fnCustomerRouteRate(S.CustomerID, S.RouteID, S.OrderDate) AS Rate
				, S.MinSettlementBarrels
				, S.ActualBarrels
				, CR.FuelSurcharge
			FROM (
				SELECT O.ID
					, O.CustomerID
					, O.RouteID
					, O.ChainUp
					, CASE WHEN C.SettlementFactorID = 1 THEN O.OriginGrossBarrels ELSE O.OriginNetBarrels END AS ActualBarrels
					, O.RerouteCount
					, O.TotalWaitMinutes
					, O.Rejected
					, OO.H2S
					, O.OrderDate
					, coalesce(CRXX.ID, CRXA.ID, CRAX.ID, CRAA.ID) AS CRID
					, isnull(C.MinSettlementBarrels, 0) AS MinSettlementBarrels
					, OO.TaxRate
				FROM dbo.viewOrderExportFull O
				JOIN tblOrigin OO ON OO.ID = O.OriginID
				JOIN tblCustomer C ON C.ID = O.CustomerID
				LEFT JOIN tblCustomerRates CRXX ON CRXX.CustomerID = O.CustomerID AND CRXX.RegionID = OO.RegionID
				LEFT JOIN tblCustomerRates CRXA ON CRXA.CustomerID = O.CustomerID AND CRXA.RegionID = -1
				LEFT JOIN tblCustomerRates CRAX ON CRAX.CustomerID = -1 AND CRAX.RegionID = OO.RegionID
				LEFT JOIN tblCustomerRates CRAA ON CRAA.CustomerID = -1 AND CRAA.RegionID = -1
				WHERE O.ID = @ID
			) S
			LEFT JOIN tblCustomerRates CR ON CR.ID = S.CRID
		) SS
	) D
END

GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 20 Mar 2013
-- Description:	return the data for the TAX_ND_10A report (for the specified date range)
-- =============================================
ALTER PROCEDURE [dbo].[spReport_Tax_ND10A]
(
  @CustomerID int
, @StartDate datetime
, @EndDate datetime
, @IncludeUninvoiced bit = 0
)
AS BEGIN
	SELECT @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	
	SELECT O.FieldName
		, O.Operator
		, O.LeaseNum AS LeaseNumber
		, O.Name AS [Well Name and Number]
		, O.NDICFileNum AS [NDIC CTB No.]
		, cast(round(SUM(OD.OriginNetBarrels), 0) as int) AS [Lease Total (Bbls)]
	FROM viewOrigin O
	JOIN viewOrder OD ON OD.OriginID = O.ID
	JOIN tblOrderInvoiceCustomer IC ON IC.OrderID = OD.ID AND (@IncludeUninvoiced = 1 OR IC.BatchID IS NOT NULL)
	WHERE O.OriginTypeID IN (2) -- WELLs ONLY
		AND OD.DeleteDateUTC IS NULL
		AND (@CustomerID = -1 OR OD.CustomerID = @CustomerID)
		AND OD.OrderDate BETWEEN @StartDate AND @EndDate
	GROUP BY O.FieldName, O.Operator, O.LeaseNum, O.Name, O.NDICFileNum
	ORDER BY O.FieldName, O.Operator, O.LeaseNum, O.Name, O.NDICFileNum
	END

GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 20 Mar 2013
-- Description:	return the data for the TAX_ND_10B report (for the specified date range)
-- =============================================
ALTER PROCEDURE [dbo].[spReport_Tax_ND10B]
(
  @CustomerID int
, @StartDate datetime
, @EndDate datetime
, @IncludeUninvoiced bit = 0
)
AS BEGIN
	SELECT @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	
	SELECT O.Name AS [Point Received]
		, OD.Customer AS [Purchaser]
		, OD.Destination
		, cast(round(SUM(OD.OriginNetBarrels), 0) as int) AS [Lease Total (Bbls)]
	FROM viewOrder OD
	JOIN tblOrigin O ON O.ID = OD.OriginID
	JOIN tblOrderInvoiceCustomer IC ON IC.OrderID = OD.ID AND (@IncludeUninvoiced = 1 OR IC.BatchID IS NOT NULL)
	WHERE O.OriginTypeID IN (2) -- WELLs ONLY
		AND OD.DeleteDateUTC IS NULL
		AND (@CustomerID = -1 OR OD.CustomerID = @CustomerID)
		AND OD.OrderDate BETWEEN @StartDate AND @EndDate
	GROUP BY O.Name, OD.Customer, OD.Destination
	ORDER BY O.Name, OD.Customer, OD.Destination
	END

GO

/***********************************/
-- Date Created: 24 Jan 2013
-- Author: Kevin Alons
-- Purpose: accomplish an Order Redirect
/***********************************/
ALTER PROCEDURE [dbo].[spRerouteOrder]
(
  @OrderID int
, @NewDestinationID int
, @UserName varchar(255)
, @Notes varchar(255)
)
AS BEGIN
	INSERT INTO tblOrderReroute (OrderID, PreviousDestinationID, UserName, Notes, RerouteDate)
		SELECT ID, DestinationID, @UserName, @Notes, getdate() FROM tblOrder WHERE ID = @OrderID
	
	UPDATE tblOrder 
		SET DestinationID = @NewDestinationID, LastChangeDateUTC = GETUTCDATE(), LastChangedByUser = @UserName
	WHERE ID = @OrderID	
END

GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: update the specified Order Actual Miles from the associated Route
/***********************************/
ALTER PROCEDURE [dbo].[spSyncActualMiles]
(
  @OrderID int
)
AS BEGIN
	UPDATE tblOrder SET ActualMiles = R.ActualMiles
	FROM tblOrder O
	JOIN tblRoute R ON R.ID = O.RouteID
	WHERE O.ID = @OrderID
END

GO

DROP PROCEDURE spSyncProducer
GO

/*******************************************/
-- Date Created: 25 Apr 2013
-- Author: Kevin Alons
-- Purpose: return Order data for Driver App sync
/*******************************************/
ALTER FUNCTION [dbo].[fnOrder_DriverApp]( @DriverID int, @LastChangeDateUTC datetime ) RETURNS  TABLE AS
RETURN 
	SELECT O.ID
		, O.OrderNum
		, O.StatusID
		, O.TicketTypeID
		, OO.TankTypeID
		, P.PriorityNum
		, O.Product
		, O.DueDate
		, O.TruckID
		, O.TrailerID
		, O.Trailer2ID
		, O.Origin
		, O.OriginFull
		, OO.Station AS OriginStation
		, OO.County AS OrigionCounty
		, O.OriginState
		, OO.WellAPI AS OriginAPI
		, O.OriginBOLNum
		, O.OriginArriveTimeUTC
		, O.OriginDepartTimeUTC
		, O.OriginMinutes
		, O.OriginWaitNotes
		, O.OriginTruckMileage
		, O.OriginGrossBarrels
		, O.OriginNetBarrels
		, O.ChainUp
		, O.Rejected
		, O.RejectNotes
		, O.OriginTankNum
		, O.Destination
		, O.DestinationFull
		, D.Station AS DestinationStation
		, O.DestArriveTimeUTC
		, O.DestDepartTimeUTC
		, O.DestMinutes
		, O.DestWaitNotes
		, O.DestTruckMileage
		, O.DestGrossBarrels
		, O.DestNetBarrels
		, O.DestProductTemp
		, O.DestProductBSW
		, O.DestProductGravity
		, O.DestOpenMeterBarrels
		, O.DestCloseMeterBarrels
		, O.CarrierTicketNum
		, cast(CASE WHEN D.TicketTypeID = 2 THEN 1 ELSE 0 END as bit) AS DestBOLAvailable
		, OO.BarrelsPerInch AS OriginBarrelsPerInch
		, O.CreateDateUTC
		, O.CreatedByUser
		, O.LastChangeDateUTC
		, O.LastChangedByUser
		, O.DeleteDateUTC
		, O.DeletedByUser
		, O.OriginID
		, O.DestinationID
		, O.PriorityID
		, O.Operator
		, O.OperatorID
		, O.Pumper
		, O.PumperID
		, O.Producer
		, O.ProducerID
		, O.Customer
		, O.CustomerID
		, O.Carrier
		, O.CarrierID
		, O.ProductID
		, O.AcceptLastChangeDateUTC
		, O.PickupLastChangeDateUTC
		, O.DeliverLastChangeDateUTC
		, C.PrintHeaderBlob
	FROM dbo.viewOrder O
	JOIN dbo.tblPriority P ON P.ID = O.PriorityID
	JOIN dbo.viewOrigin OO ON OO.ID = O.OriginID
	JOIN dbo.tblDestination D ON D.ID = O.DestinationID
	JOIN dbo.tblCarrier C ON C.ID = O.CarrierID
	WHERE (StatusID IN (2, 7, 8) -- Dispatched, Accepted, Picked Up
		-- or status = Delivered and the driver still has access to the record
		OR (StatusID = 3 AND O.DestDepartTimeUTC < DATEADD(hour, (SELECT cast(value as int) FROM tblSetting WHERE ID = 10), GETUTCDATE())))
	  AND O.DriverID = @DriverID
	  AND (@LastChangeDateUTC IS NULL OR O.LastChangeDateUTC >= @LastChangeDateUTC)

GO
  
/*******************************************/
-- Date Created: 25 Apr 2013
-- Author: Kevin Alons
-- Purpose: return OrderTicket data for Driver App sync
/*******************************************/
ALTER FUNCTION [dbo].[fnOrderTicket_DriverApp]( @DriverID int, @LastChangeDateUTC datetime ) RETURNS  TABLE AS
RETURN 
	SELECT OT.ID
		, OT.UID
		, OT.OrderID
		, OT.CarrierTicketNum
		, OT.TankNum
		, OT.TicketTypeID
		, OT.TankTypeID
		, OT.ProductObsGravity
		, OT.ProductObsTemp
		, OT.ProductHighTemp
		, OT.ProductLowTemp
		, OT.ProductBSW
		, OT.OpeningGaugeFeet
		, OT.OpeningGaugeInch
		, OT.OpeningGaugeQ
		, OT.ClosingGaugeFeet
		, OT.ClosingGaugeInch
		, OT.ClosingGaugeQ
		, OT.GrossBarrels
		, OT.NetBarrels
		, OT.Rejected
		, OT.RejectNotes
		, OT.SealOff
		, OT.SealOn
		, OT.BOLNum
		, OT.BarrelsPerInch
		, OT.CreateDateUTC
		, OT.CreatedByUser
		, OT.LastChangeDateUTC
		, OT.LastChangedByUser
		, OT.DeleteDateUTC
		, OT.DeletedByUser
	FROM dbo.tblOrderTicket OT
	JOIN dbo.tblOrder O ON O.ID = OT.OrderID
	WHERE (StatusID IN (2, 7, 8) -- Dispatched, Accepted, Picked Up
		-- or status = Delivered and the driver still has access to the record
		OR (StatusID = 3 AND O.DestDepartTimeUTC < DATEADD(hour, (SELECT cast(value as int) FROM tblSetting WHERE ID = 10), GETUTCDATE())))
	  AND O.DriverID = @DriverID
	  AND (@LastChangeDateUTC IS NULL OR O.LastChangeDateUTC >= @LastChangeDateUTC)

GO

COMMIT TRAN
SET NOEXEC OFF