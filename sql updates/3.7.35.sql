-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.7.34.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.7.34.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0
SET NOEXEC OFF
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.7.34'
SELECT  @NewVersion = '3.7.35'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCDRV-214/215: Canada Changes (new CAN_BASIC_RUN ticket and CAN_BASIC destination).'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier DestinationWait data info for the specified order
-- Changes:
	- 3.7.35 - 2015/06/23 - GSM - DCDRV-214/DCDRV-215 - New Canada Basic Run Ticket Type and Destination.
***********************************/

INSERT INTO tblTicketType
           ([Name]
           ,[CreateDateUTC]
           ,[CreatedByUser]
           ,[LastChangeDateUTC]
           ,[LastChangedByUser]
           ,[ForTanksOnly]
           ,[CountryID_CSV])
     VALUES ('CAN Basic Run','2015-06-18 15:10:00', 'gmochau', NULL, NULL, 0,2)
GO


INSERT INTO tblDestTicketType
           ([ID]
           ,[Name])
	VALUES (5,'CAN Basic')

GO


UPDATE tblUom
   SET Abbrev = 'M3' WHERE Abbrev = 'CM'

GO


COMMIT 
SET NOEXEC OFF