SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.4.22.1'
SELECT  @NewVersion = '4.5.0'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'JT-169 - Add Terminal columns to several pages where records will be filtered by terminal.  Added terminal filtering by profile setting and manual filters to several pages.'	
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO




/*********************************************************
					TABLE UPDATES
*********************************************************/
GO

ALTER TABLE tblCarrierRule ADD TerminalID INT NULL
	CONSTRAINT FK_CarrierRules_Terminal REFERENCES tblTerminal(ID)
GO




/*********************************************************
					TRIGGER UPDATES
*********************************************************/
GO

/**********************************************
-- Date Created: 2016-05-03
-- Author: Joe Engler
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
-- Changes:
--	4.5.0	- 2017/01/25	- BSB	- Add Terminal
***********************************************/
ALTER TRIGGER trigCarrierRule_IU ON tblCarrierRule AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblCarrierRule X 
			ON i.ID <> X.ID
			  AND i.TypeID = X.TypeID
			  AND dbo.fnCompareNullableInts(i.DriverID, X.DriverID) = 1
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1			  
			  AND dbo.fnCompareNullableInts(i.TerminalID, X.TerminalID) = 1
			  AND dbo.fnCompareNullableInts(i.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping carrier rules are not allowed'
	END

	IF @error IS NOT NULL 
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
END
GO




/*********************************************************
					VIEW UPDATES
*********************************************************/
GO

/***********************************/
-- Date Created: 26 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Drivers table records with "friendly" translated values included
-- Changes: 
-- 3.10.7		- 2016/01/27 - BB	- Added TabletID and PrinterSerial from tblDriver_Sync
-- 3.11.15.2	- 2016/04/20 - BB	- Added DriverGroupName to fix filtering on web page
-- 4.4.14		- 2016/12/08 - BB	- Added Operating State, Added DriverShiftTypeName, Move UserNames from viewDriver to viewDriverBase
--							 - KDA	- use fnFnameWithDeleted() method so future changes can be done in a single place (leaving the views unchanged)
-- 4.4.20		- 2017/01/19 - BSB	- Moved LastDeliveredOrderTime from viewDriver to here so it can be displayed on the driver maintenance page
-- 4.5.0		- 2017/01/26 - BSB	- Moved Terminal here from viewDriver
/***********************************/
ALTER VIEW viewDriverBase AS
SELECT X.*
	, FullNameD = dbo.fnNameWithDeleted(FullName, DeleteDateUTC) -- 4.4.14 - Updated from "Deleted" 12/15/16
FROM (
	SELECT D.*
		, Active = cast(CASE WHEN isnull(C.DeleteDateUTC, D.DeleteDateUTC) IS NULL THEN 1 ELSE 0 END as bit) 
		, FullName = D.FirstName + ' ' + D.LastName 
		, FullNameLF = D.LastName + ', ' + D.FirstName 
		, CarrierType = isnull(CT.Name, 'Unknown') 
		, Carrier = C.Name 
		, StateAbbrev = S.Abbreviation 
		, OperatingStateAbbrev = S2.Abbreviation
		, Truck = T.FullName
		, Trailer = T1.FullName 
		, Trailer2 = T2.FullName 
		, Region = R.Name 
		, Terminal = TERM.Name
		, DS.MobileAppVersion
		, DS.TabletID
		, DS.PrinterSerial
		, DriverGroupName = DG.Name
		, DriverShiftTypeName = DST.Name
		, DUN.UserNames
		, LastDeliveredOrderTime = (SELECT MAX(DestDepartTimeUTC) FROM tblOrder WHERE DriverID = D.ID)
	FROM tblDriver D 
	LEFT JOIN dbo.fnDriverUserNames() DUN ON DUN.DriverID = D.ID
	JOIN tblCarrier C ON C.ID = D.CarrierID 
	LEFT JOIN tblDriver_Sync DS ON DS.DriverID = D.ID
	LEFT JOIN tblCarrierType CT ON CT.ID = C.CarrierTypeID
	LEFT JOIN tblState S ON S.ID = D.StateID
	LEFT JOIN tblState S2 ON S2.ID = D.OperatingStateID
	LEFT JOIN viewTruck T ON T.ID = D.TruckID
	LEFT JOIN viewTrailer T1 ON T1.ID = D.TrailerID
	LEFT JOIN viewTrailer T2 ON T2.ID = D.Trailer2ID
	LEFT JOIN tblRegion R ON R.ID = D.RegionID
	LEFT JOIN tblTerminal TERM ON TERM.ID = D.TerminalID
	LEFT JOIN tblDriverGroup DG ON DG.ID = D.DriverGroupID
	LEFT JOIN tblDriverShiftType DST ON DST.ID = D.DriverShiftTypeID
) X
GO

/***********************************/
-- Date Created: 26 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Drivers table records with LastDelivereredOrderTime + computed assigned ASP.NET usernames
-- Changes
--	4.4.14 - JAE & BB	- 2016-11-29 - Add previously included fields, Moved UserNames to viewDriverBase
--	4.4.19 - BB			- 2017-01-18 - Added Terminal
--	4.4.20 - BSB			- 2017-01-19 - Moved LastDeliveredOrderTime from here to viewDriverBase so it can be displayed on the driver maintenance page
--	4.5.0 - BSB			- 2017-01-26 - Removed Terminal and put it in viewDriverBase due to access need in functions
/***********************************/
ALTER VIEW viewDriver AS
SELECT DB.*	
	, DLDocument = dl.Document
	, DLDocName = dl.DocumentName
	, DLExpiration = dl.DocumentDate
	, CDLDocument = cdl.Document
	, CDLDocName = cdl.DocumentName
	, CDLExpiration = cdl.DocumentDate
	, DrugScreenBackgroundCheckDocument = dsbc.Document
	, DrugScreenBackgroundCheckDocName = dsbc.DocumentName
	, MotorVehicleReportDocument = mvr.Document
	, MotorVehicleReportDocName = mvr.DocumentName
	, MedicalCardDocument = mc.Document
	, MedicalCardDocName = mc.DocumentName
	, MedicalCardDate = mc.DocumentDate
	, LongFormPhysicalDocument = lfp.Document
	, LongFormPhysicalDocName = lfp.DocumentName
	, LongFormPhysicalDate = lfp.DocumentDate
	, EmploymentAppDocument = ea.Document
	, EmploymentAppDocName = ea.DocumentName
	, PolicyManualReceiptDocument = pmr.Document
	, PolicyManualReceiptDocName = pmr.DocumentName
	, TrainingReceiptDocument = tr.Document
	, TrainingReceiptDocName = tr.DocumentName
	, OrientationTestDocument = ot.Document
	, OrientationTestDocName = ot.DocumentName
	, DrugScreenBackgroundReleaseDocument = dsbr.Document
	, DrugScreenBackgroundReleaseDocName = dsbr.DocumentName	
FROM viewDriverBase DB	
	OUTER APPLY (select TOP 1 * FROM tblDriverCompliance WHERE ComplianceTypeID=1 AND DriverID = DB.ID ORDER BY DocumentDate DESC) dl
	OUTER APPLY (select TOP 1 * FROM tblDriverCompliance WHERE ComplianceTypeID=2 AND DriverID = DB.ID ORDER BY DocumentDate DESC) cdl
	OUTER APPLY (select TOP 1 * FROM tblDriverCompliance WHERE ComplianceTypeID=3 AND DriverID = DB.ID ORDER BY DocumentDate DESC) dsbc
	OUTER APPLY (select TOP 1 * FROM tblDriverCompliance WHERE ComplianceTypeID=4 AND DriverID = DB.ID ORDER BY DocumentDate DESC) mvr
	OUTER APPLY (select TOP 1 * FROM tblDriverCompliance WHERE ComplianceTypeID=5 AND DriverID = DB.ID ORDER BY DocumentDate DESC) mc
	OUTER APPLY (select TOP 1 * FROM tblDriverCompliance WHERE ComplianceTypeID=6 AND DriverID = DB.ID ORDER BY DocumentDate DESC) lfp
	OUTER APPLY (select TOP 1 * FROM tblDriverCompliance WHERE ComplianceTypeID=7 AND DriverID = DB.ID ORDER BY DocumentDate DESC) ea
	OUTER APPLY (select TOP 1 * FROM tblDriverCompliance WHERE ComplianceTypeID=8 AND DriverID = DB.ID ORDER BY DocumentDate DESC) pmr
	OUTER APPLY (select TOP 1 * FROM tblDriverCompliance WHERE ComplianceTypeID=9 AND DriverID = DB.ID ORDER BY DocumentDate DESC) tr
	OUTER APPLY (select TOP 1 * FROM tblDriverCompliance WHERE ComplianceTypeID=10 AND DriverID = DB.ID ORDER BY DocumentDate DESC) ot
	OUTER APPLY (select TOP 1 * FROM tblDriverCompliance WHERE ComplianceTypeID=11 AND DriverID = DB.ID ORDER BY DocumentDate DESC) dsbr
GO

/******************************************************
-- Date Created: 2016-05-03
-- Author: Joe Engler
-- Purpose: add a computed "EndDate" value to all Carrier rules
-- Changes: 
--		4.5.0		2017/01/24		BSB		Added Terminal
******************************************************/
ALTER VIEW viewCarrierRule AS
	SELECT X.*
		, RT.RuleTypeID
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblCarrierRule XN 
			WHERE XN.TypeID = X.TypeID
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverID, X.DriverID) = 1
			  AND dbo.fnCompareNullableInts(XN.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.TerminalID, X.TerminalID) = 1
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT max(XN.EndDate) 
			FROM tblCarrierRule XN 
			WHERE XN.TypeID = X.TypeID
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverID, X.DriverID) = 1
			  AND dbo.fnCompareNullableInts(XN.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.TerminalID, X.TerminalID) = 1
			  AND XN.EndDate < X.EffectiveDate)
	FROM tblCarrierRule X
	JOIN tblCarrierRuleType RT ON RT.ID = X.TypeID
GO

/*****************************************/
-- Created: 2015.01.19 - Kevin Alons
-- Purpose: return the tblOrder table with a Local OrderDate field added
-- Changes:
-- ??		- 2015/05/17 - KDA	- add local DeliverDate field
-- 3.8.11	- 2015/07/28 - KDA	- remove OriginShipperRegion field
-- 3.9.2	- 2015/08/25 - KDA	- remove computed OrderDate (it is now stored in the table directly)
-- 3.10.13	- 2015/02/29 - JAE	- Add TruckType
-- 4.1.8.6	- 2016.09.24 - KDA	- move H2S | TicketCount | RerouteCount fields from viewOrder down to viewOrderBase
-- 4.4.17	- 2017/01/09 - BSB	- Add ECOT Minutes and ECOT (H:MM) fields
-- 4.5.0	- 2017/01/26 - BSB/JAE	- Add origin and destination terminal
/*****************************************/
ALTER VIEW viewOrderBase AS
	SELECT O.*
		, OriginStateID = OO.StateID
		, OriginRegionID = OO.RegionID
		, OriginTerminalID = OO.TerminalID
		, DestStateID = D.StateID
		, DestRegionID = D.RegionID
		, DestTerminalID = D.TerminalID
		, P.ProductGroupID
		, T.TruckTypeID
		, DR.DriverGroupID
		, DeliverDate = cast(dbo.fnUTC_to_Local(O.DestDepartTimeUTC, D.TimeZoneID, D.UseDST) as date) 
		, OO.H2S
		, OTC.TicketCount
		, ORC.RerouteCount
		, ECOT_Minutes = R.ECOT		-- 4.4.17
		, ECOT_HoursMinutes = (SELECT CAST(R.ECOT / 60 AS VARCHAR) + ':' + CAST(R.ECOT % 60 AS VARCHAR))	-- 4.4.17
	FROM tblOrder O
	JOIN tblOrigin OO ON OO.ID = O.OriginID
	JOIN tblDestination D ON D.ID = O.DestinationID
	JOIN tblRoute R ON R.ID = O.RouteID		-- 4.4.17
	LEFT JOIN tblProduct P ON P.ID = O.ProductID
	LEFT JOIN tblTruck T ON T.ID = O.TruckID
	LEFT JOIN tblDriver DR ON DR.ID = O.DriverID
	LEFT JOIN (SELECT OrderID, TicketCount = COUNT(1) FROM tblOrderTicket OT WHERE OT.DeleteDateUTC IS NULL GROUP BY OrderID) OTC ON OTC.OrderID = O.ID
	LEFT JOIN (SELECT OrderID, RerouteCount = COUNT(1) FROM tblOrderReroute GROUP BY OrderID) ORC ON ORC.OrderID = O.ID
GO

/***************************************/
-- Date Created: ?.?.? - 2012.11.25 - Kevin Alons
-- Purpose: return Order records with "translated friendly" values for FK relationships
-- Changes:
-- 3.9.20  - 2015/10/22 - KDA	- add Origin|Dest DriverID fields (using new tblOrderTransfer table) 
--			2015/10/28  - JAE	- added all Order Transfer fields for ease of use in reporting
--			2015/11/03  - BB	- added cast to make TransferComplet BIT type to avoid "Operand type clash" error when running this update script
-- 3.9.21  - 2015/11/03 - KDA	- add OriginDriverGroupID field (from OrderTransfer.OriginDriverID JOIN)
-- 3.9.25  - 2015/11/10 - JAE	- added origin driver and truck to view
-- 3.10.10 - 2016/02/15 - BB	- Add driver region ID to facilitate new user/region filtering feature
-- 3.10.11 - 2016/02/24 - JAE	- Add destination region (name) to view
-- 3.10.13 - 2016/02/29 - JAE	- Add TruckType
-- 3.13.8  - 2016/07/26	- BB	- Add Destination Station
-- 4.1.0.2 - 2016.08.28 - KDA	- rewrite RerouteCount, TicketCount to use a normal JOIN (instead of uncorrelated sub-query)
--								- use viewDriverBase instead of viewDriver
--								- eliminate viewGaugerBase (was unused), use tblOrderStatus instead of viewOrderPrintStatus
-- 4.1.3.3	- 2016/09/14 - BB	- Add origin and destination driving directions (initially for dispatch and truck order create pages)
-- 4.1.8.6	- 2016.09.24 - KDA	- move H2S | TicketCount | RerouteCount fields from viewOrder down to viewOrderBase
-- 4.5.0	- 2017/01/26 - BSB/JAE	- Add driver terminal
/***************************************/
ALTER VIEW viewOrder AS
SELECT O.*
	-- GENERATED orders that were GAUGED to COMPLETION or SKIPPED will show "Gauger [gauger status]"
	, PrintStatus = CASE WHEN O.StatusID = -10 AND GOS.IsComplete = 1 THEN 'Gauger ' + GOS.Name ELSE OPS.OrderStatus END
FROM (
	SELECT O.*
	, Origin = vO.Name 
	, OriginFull = vO.FullName 
	, OriginState = vO.State 
	, OriginStateAbbrev = vO.StateAbbrev 
	, OriginStation = vO.Station
	, OriginCounty = vO.County 
	, OriginDrivingDirections = vO.DrivingDirections	-- 4.1.3.3
	, vO.LeaseName
	, vO.LeaseNum
	, OriginTerminal = vO.Terminal
	, OriginRegion = vO.Region
	, OriginCountryID = vO.CountryID
	, OriginCountry = vO.Country
	, OriginCountryShort = vO.CountryShort
	, OriginLegalDescription = vO.LegalDescription 
	, OriginNDIC = vO.NDICFileNum 
	, OriginNDM = vO.NDM 
	, OriginCA = vO.CA 
	, OriginTimeZoneID = vO.TimeZoneID 
	, OriginUseDST = vO.UseDST 
	, Destination = vD.Name 
	, DestinationFull = vD.FullName 
	, DestinationState = vD.State 
	, DestinationStateAbbrev = vD.StateAbbrev 
	, DestinationDrivingDirections = vD.DrivingDirections	-- 4.1.3.3
	, DestinationTypeID = vD.ID
	, DestinationStation = vD.Station -- 3.13.8
	, vD.DestinationType
	, DestStation = vD.Station 
	, DestTimeZoneID = vD.TimeZoneID 
	, DestUseDST = vD.UseDST 
	, DestTerminal = vD.Terminal
	, DestRegion = vD.Region -- 3.10.13
	, DestCountryID = vD.CountryID
	, DestCountry = vD.Country
	, DestCountryShort = vD.CountryShort
	, Customer = C.Name 
	, Carrier = CA.Name 
	, CarrierType = CT.Name 
	, OS.OrderStatus
	, OS.StatusNum
	, Driver = D.FullName 
	, DriverFirst = D.FirstName 
	, DriverLast = D.LastName 
	, DriverRegionID = D.RegionID  -- 3.10.10
	, DriverTerminalID = D.TerminalID
	, DriverTerminal = D.Terminal
	, Truck = TRU.FullName 
	, Trailer = TR1.FullName 
	, TrailerTerminalID = TR1.TerminalID
	, TrailerTerminal = TR1.Terminal
	, Trailer2 = TR2.FullName 
	, Trailer2TerminalID = TR2.TerminalID
	, Trailer2Terminal = TR2.Terminal
	, P.PriorityNum
	, TicketType = TT.Name 
	, DestTicketTypeID = vD.TicketTypeID 
	, DestTicketType = vD.TicketType 
	, Operator = OP.Name 
	, Producer = PR.Name 
	, Pumper = PU.FullName 
	, DriverNumber = D.IDNumber
	, CarrierNumber = CA.IDNumber
	, CarrierTypeID = CA.CarrierTypeID
	, TruckNumber = TRU.IDNumber
	, TruckType = TRU.TruckType 
	, TrailerNumber = TR1.IDNumber 
	, Trailer2Number = TR2.IDNumber 
	, Product = PRO.Name
	, ProductShort = PRO.ShortName 
	, PRO.ProductGroup
	, OriginUOM = OUom.Name 
	, OriginUomShort = OUom.Abbrev 
	, OriginTankID_Text = coalesce(ORT.TankNum, O.OriginTankNum, '?')
	, DestUOM = DUom.Name 
	, DestUomShort = DUom.Abbrev 
	, Active = CAST((CASE WHEN O.DeleteDateUTC IS NULL THEN 1 ELSE 0 END) AS BIT) 
	, IsDeleted = CAST((CASE WHEN O.DeleteDateUTC IS NOT NULL THEN 1 ELSE 0 END) AS BIT) 
	, PickupPrintStatus = PPS.Name 
	, PickupCompleted = PPS.IsCompleted 
	, DeliverPrintStatus = DPS.Name 
	, DeliverCompleted = DPS.IsCompleted 
	, PrintStatusID = CASE WHEN O.StatusID = -9 THEN ISNULL(-GAO.StatusID, O.StatusID)
						   WHEN O.StatusID = 8 AND PPS.IsCompleted = 0 THEN 7
						   WHEN O.StatusID = 3 AND DPS.IsCompleted = 0 THEN 8
						ELSE O.StatusID END 
	, OriginTankText = CASE WHEN O.OriginTankID IS NULL OR ORT.TankNum = '*' THEN O.OriginTankNum ELSE ORT.TankNum END
	, OriginWaitNum = OWR.Num
	, OriginWaitDesc = OWR.Description
	, OriginWaitNumDesc = OWR.NumDesc
	, DestWaitNum = DWR.Num
	, DestWaitDesc = DWR.Description
	, DestWaitNumDesc = DWR.NumDesc
	, RejectNum = ORR.Num
	, RejectDesc = ORR.Description
	, RejectNumDesc = ORR.NumDesc
	, TotalMinutes = ISNULL(OriginMinutes, 0) + ISNULL(DestMinutes, 0)
	-- TRANSFER FIELDS
	, OriginDriverID = ISNULL(OTR.OriginDriverID, O.DriverID)
	, OriginDriverGroupID = ISNULL(ODG.ID, DDG.ID) 
	, OriginDriverGroup = ISNULL(ODG.Name, DDG.Name)
	, OriginDriver = ISNULL(vODR.FullName, vDDR.FullName)
	, OriginDriverFirst = ISNULL(vODR.FirstName, vDDR.FirstName)
	, OriginDriverLast = ISNULL(vODR.LastName, vDDR.LastName)
	, OriginTruckID = ISNULL(OTR.OriginTruckID, O.TruckID)
	, OriginTruck = ISNULL(vOTRU.FullName, vDTRU.FullName)
	, OriginTruckTerminalID = ISNULL(vOTRU.TerminalID, vDTRU.TerminalID)
	, OriginTruckTerminal = ISNULL(vOTRU.Terminal, vDTRU.Terminal)
	, OriginDriverTerminalID = ISNULL(vODR.TerminalID, D.TerminalID)
	, OriginDriverTerminal = ISNULL(vODR.Terminal, D.Terminal)
	, DestDriverID = CASE WHEN OTR.OrderID IS NULL THEN NULL ELSE O.DriverID END
	, DestDriver = CASE WHEN OTR.OrderID IS NULL THEN NULL ELSE D.FullName END
	, DestDriverFirst = CASE WHEN OTR.OrderID IS NULL THEN NULL ELSE D.FirstName END
	, DestDriverLast = CASE WHEN OTR.OrderID IS NULL THEN NULL ELSE D.LastName END
	, DestTruckID = CASE WHEN OTR.OrderID IS NULL THEN NULL ELSE O.TruckID END
	, DestTruck = CASE WHEN OTR.OrderID IS NULL THEN NULL ELSE TRU.FullName END
	, DestTruckTerminalID = CASE WHEN OTR.OrderID IS NULL THEN NULL ELSE TRU.TerminalID END
	, DestTruckTerminal = CASE WHEN OTR.OrderID IS NULL THEN NULL ELSE TRU.Terminal END
	, DestDriverTerminalID = CASE WHEN OTR.OrderID IS NULL THEN NULL ELSE D.TerminalID END
	, DestDriverTerminal = CASE WHEN OTR.OrderID IS NULL THEN NULL ELSE D.Terminal END
	, OriginTruckEndMileage = OTR.OriginTruckEndMileage
	, DestTruckStartMileage = OTR.DestTruckStartMileage
	, TransferPercent = OTR.PercentComplete
	, TransferNotes = OTR.Notes
	, TransferDateUTC = OTR.CreateDateUTC
	, TransferComplete = CAST(OTR.TransferComplete AS BIT)
	, IsTransfer = CAST((CASE WHEN OTR.OrderID IS NOT NULL THEN 1 ELSE 0 END) AS BIT)	
	-- 
	, GaugerStatusID = GAO.StatusID
	, GaugerID = GAO.GaugerID	
	FROM viewOrderBase O
	JOIN tblOrderStatus OS ON OS.ID = O.StatusID
	LEFT JOIN tblOrderTransfer OTR ON OTR.OrderID = O.ID
	LEFT JOIN viewOrigin vO ON vO.ID = O.OriginID
	LEFT JOIN viewDestination vD ON vD.ID = O.DestinationID
	LEFT JOIN tblCustomer C ON C.ID = O.CustomerID
	LEFT JOIN tblCarrier CA ON CA.ID = O.CarrierID
	LEFT JOIN tblCarrierType CT ON CT.ID = CA.CarrierTypeID
	LEFT JOIN viewDriverBase D ON D.ID = O.DriverID
	LEFT JOIN viewDriverBase vODR ON vODR.ID = OTR.OriginDriverID
	LEFT JOIN viewDriver vDDR ON vDDR.ID = O.DriverID
	LEFT JOIN tblDriverGroup ODG ON ODG.ID = vODR.DriverGroupID
	LEFT JOIN tblDriverGroup DDG ON DDG.ID = O.DriverGroupID
	LEFT JOIN viewTruck TRU ON TRU.ID = O.TruckID
	LEFT JOIN viewTruck vOTRU ON vOTRU.ID = OTR.OriginTruckID
	LEFT JOIN viewTruck vDTRU ON vDTRU.ID = O.TruckID
	LEFT JOIN viewTrailer TR1 ON TR1.ID = O.TrailerID
	LEFT JOIN viewTrailer TR2 ON TR2.ID = O.Trailer2ID
	LEFT JOIN tblPriority P ON P.ID = O.PriorityID
	LEFT JOIN tblTicketType TT ON TT.ID = O.TicketTypeID
	LEFT JOIN tblOriginTank ORT ON ORT.ID = O.OriginTankID
	LEFT JOIN tblOperator OP ON OP.ID = O.OperatorID
	LEFT JOIN viewPumper PU ON PU.ID = O.PumperID
	LEFT JOIN tblProducer PR ON PR.ID = O.ProducerID
	LEFT JOIN viewProduct PRO ON PRO.ID = O.ProductID
	LEFT JOIN tblUom OUom ON OUom.ID = O.OriginUomID
	LEFT JOIN tblUom DUom ON DUom.ID = O.DestUomID
	LEFT JOIN tblPrintStatus PPS ON PPS.ID = O.PickupPrintStatusID
	LEFT JOIN tblPrintStatus DPS ON DPS.ID = O.DeliverPrintStatusID
	LEFT JOIN viewOriginWaitReason OWR ON OWR.ID = O.OriginWaitReasonID
	LEFT JOIN viewDestinationWaitReason DWR ON DWR.ID = O.DestWaitReasonID
	LEFT JOIN viewOrderRejectReason ORR ON ORR.ID = O.RejectReasonID	
	LEFT JOIN tblGaugerOrder GAO ON GAO.OrderID = O.ID
) O
LEFT JOIN tblOrderStatus OPS ON OPS.ID = O.PrintStatusID
LEFT JOIN tblGaugerOrderStatus GOS ON GOS.ID = O.GaugerStatusID
GO

/****************************************************
-- Date Created: 25 Jan 2015
-- Author: Kevin Alons
-- Purpose: calculate DailyUnits value
** Changes:
- 3.8.4 - 2015/07/10 - KDA - fix DailyUnits being shorted by 1 day (was causing a DIVIDE BY ZERO error for 1 day allocation entry
- 3.10.10 - 2016/02/14 - KDA - add RegionID column to returned data
- 3.12.7 - 2016/05/17 - JAE - Added Number of days in range and days left to view
- 4.5.0 - 2017/01/26 - BSB/JAE - Added Terminal
****************************************************/
ALTER VIEW viewAllocationDestinationShipper AS
	SELECT ADS.*
		, D.RegionID
		, D.TerminalID
		, DailyUnits = Units / cast(DATEDIFF(day, EffectiveDate, EndDate) + 1 as decimal(18, 10))
		, Destination = D.Name
		, Shipper = C.Name
		, ProductGroup = PG.Name
		, Uom = U.Name
		, NumDays = DATEDIFF(day, EffectiveDate, EndDate) + 1
		, DaysLeft = CASE WHEN DATEDIFF(day, GETDATE(), EndDate) < 0 THEN 0 -- target date already past, set to zero
		                  ELSE DATEDIFF(day, GETDATE(), EndDate) + 1 END -- include today
	FROM tblAllocationDestinationShipper ADS
	JOIN viewDestination D ON D.ID = ADS.DestinationID
	JOIN tblCustomer C ON C.ID = ADS.ShipperID
	JOIN tblProductGroup PG ON PG.ID = ADS.ProductGroupID
	JOIN tblUom U ON U.ID = ADS.UomID
GO




/*********************************/
/* Refreshing these views is required at this point so that other things will update next without errors */
/*********************************/
EXEC sp_refreshview 'viewOrderBase'
GO
EXEC sp_refreshview 'viewOrder'
GO
EXEC sp_refreshview 'viewOrderLocalDates'
GO




/*************************************************************
-- Date Created: 15 Jun 2015
-- Author: Kevin Alons
-- Purpose: return the base data for the Order Approval page
-- Changes:
	- 3.9.2  - 2015/08/25 - KDA - remove obsolete OrderDateUTC & DeliverDateUTC fields (which were used to optimize date searches when OrderDate was virtual)
	- 3.9.20 - 2015/10/26 - JAE - Add columns for transfer percentage and override percentage
	- 3.9.25 - 2015/11/10 - JAE - Use origin driver for approval page
	- 3.9.36 - 2015/12/21 - JAE - Add columns for min settlement units (carrier and shipper)
	- 3.10.13  - 2016/02/29 - JAE - Add Truck Type
	- 3.11.17.2 - 2016/04/18 - JAE - Filter producer settled orders
	- 3.11.19 - 2016/05/02 - BB - Add Job number and Contract number
	- 4.4.1	 - 2016/11/04 - JAE - Add Destination Chainup
	- 4.5.0  - 2017/02/03 - JAE - Add Terminal fields
*************************************************************/
ALTER VIEW viewOrderApprovalSource
AS
	SELECT O.ID
		, O.StatusID
		, Status = O.OrderStatus
		, O.OrderNum
		, O.JobNumber
		, O.ContractNumber
		, O.OrderDate
		, DeliverDate = ISNULL(O.DeliverDate, O.OrderDate)
		, ShipperID = O.CustomerID
		, O.CarrierID
		, DriverID = O.OriginDriverID
		, TruckID = O.OriginTruckID
		, O.TrailerID
		, O.Trailer2ID
		, O.ProducerID
		, O.OperatorID
		, O.PumperID
		, Shipper = O.Customer
		, O.Carrier
		, Driver = O.OriginDriver
		, O.Truck
		, O.Trailer
		, O.Trailer2
		, O.Producer
		, O.Operator
		, O.Pumper
		, O.TicketTypeID
		, O.TicketType
		, O.ProductID
		, O.Product
		, O.ProductGroupID
		, O.ProductGroup
		, O.TruckTypeID
		, O.TruckType
    
		, O.Rejected
		, RejectReason = O.RejectNumDesc
		, O.RejectNotes
		, O.OriginChainUp
		, OA.OverrideOriginChainup
		, O.DestChainUp
		, OA.OverrideDestChainup
		, O.H2S
		, OA.OverrideH2S
		, O.DispatchConfirmNum
		, O.DispatchNotes
		, O.PickupDriverNotes
		, O.DeliverDriverNotes
		, Approved = CAST(ISNULL(OA.Approved, 0) AS BIT)
		, OSS.WaitFeeParameterID, O.AuditNotes
		
		, O.OriginID
		, O.Origin
		, O.OriginStateID
		, O.OriginArriveTime
		, O.OriginDepartTime
		, O.OriginMinutes
		, O.OriginUomID
		, O.OriginUOM
		, O.OriginGrossUnits
		, O.OriginGrossStdUnits
		, O.OriginNetUnits
		, OriginWaitReason = OWR.NumDesc
		, O.OriginWaitNotes
		, ShipperOriginWaitBillableMinutes = OSS.OriginWaitBillableMinutes
		, CarrierOriginWaitBillableMinutes = OSC.OriginWaitBillableMinutes
		, OA.OverrideOriginMinutes 
		
		, O.DestinationID
		, O.Destination
		, O.DestStateID
		, O.DestArriveTime
		, O.DestDepartTime
		, O.DestMinutes
		, O.DestUomID
		, O.DestUOM
		, O.DestGrossUnits
		, O.DestNetUnits
		, O.DestOpenMeterUnits
		, O.DestCloseMeterUnits
		, DestWaitReason = OWR.NumDesc
		, O.DestWaitNotes
		, ShipperDestinationWaitBillableMinutes = OSS.DestinationWaitBillableMinutes
		, CarrierDestinationWaitBillableMinutes = OSC.DestinationWaitBillableMinutes
		, OA.OverrideDestMinutes
	
		, Rerouted = CAST(CASE WHEN ORD.OrderID IS NULL THEN 0 ELSE 1 END AS BIT)
		, OA.OverrideReroute
		, O.ActualMiles
		, RerouteMiles = ROUND(ORD.RerouteMiles, 0)
		, OA.OverrideActualMiles
		, IsTransfer = CAST(CASE WHEN OT.OrderID IS NULL THEN 0 ELSE 1 END AS BIT)
		, TransferPercentComplete = ISNULL(OT.PercentComplete,100)

		, OA.OverrideTransferPercentComplete
		, CarrierMinSettlementUnits = OSC.MinSettlementUnits
		, CarrierMinSettlementUOM = CUOM.Abbrev
		, OA.OverrideCarrierMinSettlementUnits
		, ShipperMinSettlementUnits = OSS.MinSettlementUnits
		, ShipperMinSettlementUOM = CUOM.Abbrev
		, OA.OverrideShipperMinSettlementUnits
		
		, OA.CreateDateUTC
		, OA.CreatedByUser
		
		, OrderID = OSC.OrderID | OSS.OrderID -- NULL if either are NULL - if NULL indicates that the order is not yet rates (used below to rate these)
		, O.OriginTerminalID
		, O.DestTerminalID
		, O.DriverTerminalID
	FROM viewOrderLocalDates O
	OUTER APPLY dbo.fnOrderRerouteData(O.ID) ORD
	LEFT JOIN tblOrderReroute ORE ON ORE.OrderID = O.ID
	LEFT JOIN tblOrderTransfer OT ON OT.OrderID = O.ID
	LEFT JOIN tblOrderSettlementCarrier OSC ON OSC.OrderID = O.ID
	LEFT JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = O.ID
	LEFT JOIN tblOrderSettlementProducer OSP ON OSP.OrderID = O.ID
	LEFT JOIN viewOriginWaitReason OWR ON OWR.ID = O.OriginWaitReasonID
	LEFT JOIN viewDestinationWaitReason DWR ON DWR.ID = O.DestWaitReasonID
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
	LEFT JOIN viewOrderSettlementUnitsCarrier OSUC ON OSUC.OrderID = O.ID
	LEFT JOIN viewOrderSettlementUnitsShipper OSUS ON OSUS.OrderID = O.ID
	LEFT JOIN tblUom CUOM ON CUOM.ID = OSUC.MinSettlementUomID
	LEFT JOIN tblUom SUOM ON SUOM.ID = OSUS.MinSettlementUomID
	WHERE O.StatusID IN (4) /* only include orders that are marked AUDITED */
	  AND OSC.BatchID IS NULL AND OSS.BatchID IS NULL AND OSP.BatchID IS NULL /* don't show orders that have been settled */
GO





/*********************************************************
					FUNCTION UPDATES
*********************************************************/
GO

/********************************************
-- Date Created: 25 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve all matching AllocationDestinationShipper records for the specified criteria
-- Changes:
-- 4.5.0	- 2017/01/31	- BSB	- Add Terminal
********************************************/
ALTER FUNCTION fnAllocationDestinationShipper
(
  @DestinationID int = -1
, @ShipperID int = -1
, @ProductGroupID int = -1
, @TerminalID int = -1
, @StartDate date
, @EndDate date
) RETURNS TABLE AS RETURN
	SELECT *
	FROM viewAllocationDestinationShipper
	WHERE @DestinationID IN (-1, DestinationID) 
	  AND @ShipperID IN (-1, ShipperID) 
	  AND @ProductGroupID IN (-1, ProductGroupID) 
	  AND (@TerminalID = -1 OR TerminalID = @TerminalID OR TerminalID IS NULL)
	  AND (@StartDate BETWEEN EffectiveDate AND EndDate
		OR @EndDate BETWEEN EffectiveDate AND EndDate
		OR EffectiveDate BETWEEN @StartDate AND @EndDate)
GO

/***********************************
-- Date Created: 2016-05-03
-- Author: Joe Engler
-- Purpose: retrieve and return the Carrier Rule info for the specified order
-- Changes:
--		4.4.14		2016/12/26		JAE		Require a carrier at a minimum
--		4.5.0		2017/01/25		BSB		Add terminal, Readjust best match order
***********************************/
ALTER FUNCTION fnCarrierRules(@StartDate date, @EndDate date, @TypeID int, @DriverID int, @CarrierID int, @TerminalID int, @StateID int, @RegionID int, @BestMatchOnly bit = 0)
RETURNS 
	@ret TABLE (
		ID int
	  , TypeID int
	  , DriverID int
	  , CarrierID int
	  , TerminalID int
	  , StateID int
	  , RegionID int
	  , Value varchar(255)
	  , RuleTypeID int
	  , EffectiveDate date
	  , EndDate date
	  , MaxEffectiveDate date
	  , MinEndDate date
	  , NextEffectiveDate date
	  , PriorEndDate date
	  , BestMatch bit
	  , Ranking smallmoney
	  , CreateDateUTC datetime
	  , CreatedByUser varchar(100)
	  , LastChangeDateUTC datetime
	  , LastChangedByUser varchar(100)
	)
AS BEGIN
	-- get the raw data (all matched rows)
	DECLARE @src TABLE (ID int, TypeID int, Ranking smallmoney)
	INSERT INTO @src (ID, TypeID, Ranking)
		SELECT ID, TypeID
			, Ranking =	dbo.fnRateRanking(@DriverID, R.DriverID, 16, 1)
					  + dbo.fnRateRanking(@CarrierID, R.CarrierID, 8, 0)
					  + dbo.fnRateRanking(@TerminalID, R.TerminalID, 4, 0)
					  + dbo.fnRateRanking(@StateID, R.StateID, 2, 1)
					  + dbo.fnRateRanking(@RegionID, R.RegionID, 1, 1)
		FROM viewCarrierRule R
		WHERE coalesce(nullif(@DriverID, 0), R.DriverID, 0) = coalesce(R.DriverID, nullif(@DriverID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@TerminalID, 0), R.TerminalID, 0) = coalesce(TerminalID, nullif(@TerminalID, 0), 0)
		  AND coalesce(nullif(@StateID, 0), R.StateID, 0) = coalesce(R.StateID, nullif(@StateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(R.RegionID, nullif(@RegionID, 0), 0)
		  AND (nullif(@TypeID, 0) IS NULL OR TypeID = @TypeID)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)

	-- return the rate data for the best-match rates 
	INSERT INTO @ret (ID, TypeID, DriverID, CarrierID, TerminalID, StateID, RegionID, Value, RuleTypeID, EffectiveDate, EndDate
			, NextEffectiveDate, PriorEndDate, BestMatch, Ranking, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser)
		SELECT R.ID, TypeID, DriverID, CarrierID, TerminalID, StateID, RegionID, R.Value, RuleTypeID, EffectiveDate, EndDate
			, NextEffectiveDate, PriorEndDate, BestMatch, Ranking, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser
		FROM viewCarrierRule R
		JOIN (
			SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.TypeID IS NULL THEN 0 ELSE 1 END as bit)
			FROM @src S
			LEFT JOIN (
				SELECT TypeID, Ranking = MAX(Ranking)
				FROM @src
				WHERE @StartDate = ISNULL(@EndDate, @StartDate)
				  AND Ranking % 1 = 0.01 * 5 -- ensure a complete match is found for BESTMATCH (last number needs to match count of best match fields)
				GROUP BY TypeID
			) X ON X.TypeID = S.TypeID AND X.Ranking = S.Ranking
		) X ON X.ID = R.ID
		WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)		
	RETURN
END
GO

/***********************************/
-- Date Created: 2016-05-03
-- Author: Joe Engler
-- Purpose: retrieve and return the Carrier Rule rows for the specified criteria
-- Changes:
--		4.4.14		JAE			2016-11-30		Switched to viewDriverBase
--		4.5.0		BSB			2017-01-25		Added terminal
/***********************************/
ALTER FUNCTION fnCarrierRulesDisplay(@StartDate date, @EndDate date, @TypeID int, @DriverID int, @CarrierID int, @TerminalID int, @StateID int, @RegionID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT 
		R.ID
		, R.DriverID
		, R.CarrierID
		, R.TerminalID
		, R.StateID
		, R.RegionID
		, R.TypeID
		, R.Value
		, R.RuleTypeID
		, R.EffectiveDate
		, R.EndDate
		, R.NextEffectiveDate
		, R.PriorEndDate
		, Type = CRT.Name		
		, Driver = D.FullName
		, Carrier = C.Name
		, Terminal = T.Name
		, State = S.Abbreviation
		, Region = RE.Name
		, RuleType = RT.Name
		, R.CreateDateUTC
		, R.CreatedByUser
		, R.LastChangeDateUTC
		, R.LastChangedByUser
		, BestMatch
		, Ranking
	FROM dbo.fnCarrierRules(@StartDate, @EndDate, @TypeID, @DriverID, @CarrierID, @TerminalID, @StateID, @RegionID, 0) R
	JOIN tblCarrierRuleType CRT ON CRT.ID = R.TypeID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN viewDriverBase D ON D.ID = R.DriverID
	LEFT JOIN tblRuleType RT ON RT.ID = R.RuleTypeID
	LEFT JOIN tblRegion RE ON RE.ID = R.RegionID
	LEFT JOIN tblTerminal T ON T.ID = R.TerminalID
	LEFT JOIN tblState S ON S.ID = R.StateID
	ORDER BY EffectiveDate
GO

/***********************************************************/
-- Date Created: 4.3.0 - 2016.10.20 - Kevin Alons
-- Purpose: return all "potential" Driver Scheduling records for a date range and input criteria
-- Changes:
-- 4.3.2	- 2016.11.05 - KDA	- add @DriverID parameter (and also honor @DriverGroupID parameter which was present but used)
-- 4.5.0	- 2017-01-26 - BSB	- Add @TerminalID parameter
/***********************************************************/
ALTER FUNCTION fnDriverSchedule
(
  @StartDate date
, @EndDate date
, @TerminalID int = -1
, @RegionID int = -1
, @CarrierID int = -1
, @DriverGroupID int = -1
, @DriverID int = -1
) RETURNS TABLE AS RETURN
	-- generates a full list of records for the specified criteria (with NULL or not assigned as a default)
	WITH cteBASE AS (
		SELECT D.TerminalID, D.Terminal, D.RegionID, D.Region, D.CarrierID, D.Carrier, D.DriverGroupID, DriverGroup = D.DriverGroupName
			, DriverID = D.ID, Driver = D.FullName, D.MobilePhone, DT.ScheduleDate
			, StatusID = 1 /* undefined */, StartHours = cast(null as tinyint), DurationHours = cast(null as tinyint)
			, Pos = ROW_NUMBER() OVER (PARTITION BY D.ID ORDER By DT.ScheduleDate)
		FROM viewDriverBase D
		CROSS JOIN (
			-- get a table with the entire specified date range (date only values)
			SELECT ScheduleDate = dateadd(day, n-1, @StartDate)
			FROM viewNumbers1000 
			WHERE n <= datediff(day, @StartDate, @EndDate) + 1
		) DT
		WHERE (@CarrierID = -1 OR D.CarrierID = @CarrierID)
		 AND (ISNULL(@TerminalID, 0) <= 0 OR 
			(D.TerminalID = @TerminalID OR D.TerminalID IS NULL))
		 AND (@RegionID = -1 OR D.RegionID = @RegionID)
		 AND (@DriverGroupID = -1 OR D.DriverGroupID = @DriverGroupID)
		 AND (@DriverID = -1 OR D.ID = @DriverID)
		 AND D.Active = 1
	)

	SELECT B.TerminalID, B.Terminal, B.RegionID, B.Region, B.CarrierID, B.Carrier, B.DriverGroupID, B.DriverGroup, B.DriverID, B.Driver, B.MobilePhone
		, DSA.DriverShiftTypeID, DSA.DriverShiftStartDate
		, StatusID = isnull(DS.StatusID, B.StatusID), Status = SS.Abbrev, Pos
		, StartHours = isnull(DS.StartHours, B.StartHours), DurationHours = isnull(DS.DurationHours, B.DurationHours)
		, B.ScheduleDate, DS.ManualAssignment
		, DS.CreateDateUTC, DS.CreatedByUser
	FROM cteBASE B
	LEFT JOIN tblDriverSchedule DS ON DS.DriverID = B.DriverID AND DS.ScheduleDate = B.ScheduleDate
	LEFT JOIN tblDriverScheduleAssignment DSA ON DSA.DriverID = B.DriverID
	LEFT JOIN tblDriverScheduleStatus SS ON SS.ID = isnull(DS.StatusID, B.StatusID)
GO

/********************************************
-- Author:		Kevin Alons
-- Create date: 13 May 2013
-- Description:	retrieve all currently eligible Destinations for the specified OriginID/ProductID values
-- Changes:
	- 3.8.11	- 2015/07/28 - KDA - revamp to use revamped viewCustomerOriginDestination
								- add new @shipper parameter
	- 3.13.1.3	- 2016/07/11 - BB - Add logic to check for "filtered by origin region setting"
	- 4.5.0		- 2017/01/26 - BSB - Add Terminal
*********************************************/
ALTER FUNCTION fnRetrieveEligibleDestinations(@originID int, @shipperID int, @productID int, @requireRoute bit = 0) RETURNS TABLE 
AS RETURN
	SELECT D.*
	FROM viewDestination D
	JOIN viewCustomerOriginDestination COD ON COD.DestinationID = D.ID
	LEFT JOIN tblOrigin O ON O.ID = COD.OriginID --Added to gain access to origin region 7/11/16
	LEFT JOIN tblOriginProducts OP ON OP.OriginID = COD.OriginID
	LEFT JOIN tblDestinationProducts DP ON DP.DestinationID = D.ID AND DP.ProductID = OP.ProductID
	WHERE D.DeleteDateUTC IS NULL
	  AND (isnull(@originID, 0) = 0 OR COD.OriginID = @originID)
	  AND (ISNULL(@shipperID, 0) = 0 OR COD.CustomerID = @shipperID)
	  AND (ISNULL(@productID, 0) = 0 OR OP.ProductID = @productID)
	  AND (@requireRoute = 0 OR D.ID IN (SELECT DestinationID FROM viewRoute WHERE OriginID = @OriginID)) 
	  AND (UPPER((SELECT Value FROM tblSetting WHERE ID = 61)) = 'FALSE' OR D.RegionID = O.RegionID)
	  AND (ISNULL(O.TerminalID, 0) = 0 
			OR (D.TerminalID = O.TerminalID OR D.Terminal IS NULL))
GO

/****************************************************/
-- Created: 2016.09.14 - 4.1.7
-- Author: Joe Engler
-- Purpose: Retrieve the drivers (without last known GPS coordinates) for the specified filter criteria
-- Changes:
--	4.1.8.3		JAE		2016-09-21		Fixed reference to current time to start time (passed in)
--	4.1.10.1	JAE		2016-09-27		Added qualifier to DeleteDateUTC column (since same column was added to route table)
--	4.2.0		JAE		2016-09-30		Add hos fields to view
--	4.2.5		JAE		2016-10-26		Use operating state (instead of state) to carrier rule call
--											Added Weekly OnDuty fields
--	4.3.2		KDA		2016.11.07		replace tblDriverAvailability references with viewDriverSchedule (tblDriverSchedule)
--	4.4.14		JAE		2016-12-22		Update compliance check to use new compliance tables			
--  4.4.22.1	JAE		2017-01-31		Join HOS policy to get default times when no history occurs, update hours left based on date
--	4.5.0		BSB		2017-01-25		Add Terminal
/****************************************************/
ALTER FUNCTION fnRetrieveEligibleDrivers_NoGPS(@CarrierID INT, @TerminalID INT, @StateID INT, @RegionID INT, @DriverGroupID INT, @StartDate DATETIME) 
RETURNS @ret TABLE 
(
	ID INT
	, LastName VARCHAR(20)
	, FirstName VARCHAR(20)
	, FullName VARCHAR(41)
	, FullNameLF VARCHAR(42)
	, CarrierID INT
	, Carrier VARCHAR(40)
	, TerminalID INT
	, RegionID INT
	, TruckID INT
	, Truck VARCHAR(10)
	, TrailerID INT
	, Trailer VARCHAR(10)
	, AvailabilityFactor DECIMAL(4,2)
	, ComplianceFactor DECIMAL(4,2)
	, TruckAvailabilityFactor DECIMAL(4,2)
	, CurrentWorkload INT
	, CurrentECOT INT
	, HOSFactor DECIMAL(4,2)
	, OnDutyHoursSinceWeeklyReset DECIMAL(6,2)
	, WeeklyOnDutyHoursLeft DECIMAL(6,2)
	, DrivingHoursSinceWeeklyReset DECIMAL(6,2)
	, WeeklyDrivingHoursLeft DECIMAL(6,2)
	, OnDutyHoursSinceDailyReset DECIMAL(6,2)
	, OnDutyHoursLeft DECIMAL(6,2)
	, DrivingHoursSinceDailyReset DECIMAL(6,2)
	, DrivingHoursLeft DECIMAL(6,2)
	, HOSHrsOnShift DECIMAL(6,2)
	, DriverScore INT 
)
AS BEGIN
	DECLARE @__HOS_POLICY__ INT = 1
	DECLARE @__ENFORCE_DRIVER_SCHEDULING__ INT = 13
	DECLARE @__ENFORCE_DRIVER_COMPLIANCE__ INT = 14
	DECLARE @__ENFORCE_HOS__ INT = 15
	
	IF @StartDate IS NULL 
	   SELECT @StartDate = dbo.fnDateOnly(GETDATE())

	INSERT INTO @ret
	SELECT X.*
		, DriverScore = 100 * AvailabilityFactor * ComplianceFactor * TruckAvailabilityFactor * HOSFactor
	FROM (
		SELECT d.ID
			, d.FirstName
			, d.LastName
			, FullName
			, d.FullNameLF
			, d.CarrierID
			, d.Carrier
			, d.TerminalID
			, d.RegionID
			, d.TruckID
			, d.Truck
			, d.TrailerID
			, d.Trailer
			--Availability
			, AvailabilityFactor = CASE WHEN cr_a.Value IS NULL OR dbo.fnToBool(cr_a.Value) = 0 THEN 1 -- scheduling not enforced
										ELSE isnull(DS.OnDuty, 0) END
			-- Compliance
			, ComplianceFactor = CASE WHEN cr_c.Value IS NULL OR dbo.fnToBool(cr_c.Value) = 0 THEN 1 -- compliance not enforced
									WHEN (select count(*) from fnDriverComplianceSummary(0, 0) WHERE DriverID = d.ID) > 0 THEN 0
									ELSE 1 END -- all ok
			--Truck/Trailer Availability
			, TruckAvailabilityFactor = 1			
			--current workload, time on shift
			, o.CurrentWorkload
			, CurrentECOT = ISNULL(o.CurrentECOT, 0)
			-- HOS
			, HOSFactor = CASE WHEN cr_h.Value IS NULL OR dbo.fnToBool(cr_h.Value) = 0 THEN 1 -- HOS not enforced
							WHEN @StartDate > DATEADD(HOUR, policy.WeeklyResetHR, GETUTCDATE()) THEN 1 -- Equivalent of next "week"
							WHEN @StartDate > GETUTCDATE() AND hos.WeeklyOnDutyHoursLeft <= 0 THEN 0 -- check tomorrow (out of weekly hours)
							WHEN @StartDate > GETUTCDATE() AND hos.WeeklyOnDutyHoursLeft > 0 THEN 1 -- check tomorrow (ignore daily numbers)
							WHEN hos.WeeklyOnDutyHoursLeft <= 0 OR hos.OnDutyHoursLeft <= 0 OR hos.DrivingHoursLeft <= 0 THEN 0 -- check today
							-- compare ECOT to hours and see
							ELSE 1 END
			, hos.OnDutyHoursSinceWeeklyReset
			, WeeklyOnDutyHoursLeft = CASE WHEN @StartDate > DATEADD(HOUR, policy.WeeklyResetHR, GETUTCDATE()) THEN policy.OnDutyWeeklyLimitHR ELSE ISNULL(hos.WeeklyOnDutyHoursLeft, policy.OnDutyWeeklyLimitHR) END
			, hos.DrivingHoursSinceWeeklyReset
			, WeeklyDrivingHoursLeft = CASE WHEN @StartDate > GETUTCDATE() THEN policy.DrivingWeeklyLimitHR ELSE ISNULL(hos.WeeklyDrivingHoursLeft, policy.DrivingWeeklyLimitHR) END
			, hos.OnDutyHoursSinceDailyReset
			, OnDutyHoursLeft = CASE WHEN @StartDate > GETUTCDATE() THEN policy.OnDutyDailyLimitHR ELSE hos.OnDutyHoursLeft END
			, hos.DrivingHoursSinceDailyReset
			, DrivingHoursLeft = CASE WHEN @StartDate > GETUTCDATE() THEN policy.DrivingDailyLimitHR ELSE hos.DrivingHoursLeft END
			, HrsOnShift = CAST(DATEDIFF(MINUTE, ds.StartDateTime, getdate())/60.0*isnull(ds.OnDuty, 0) AS DECIMAL(5,1)) -- from DriverScheduling
			/* return the number of hours the Driver Scheduling module defines the driver is Scheduled on the specified day */
			--,ShiftHours = dbo.fnMinInt(DS.DurationHours, 24 - DS.StartHours % 24)-- from DriverScheduling

		FROM viewDriver d
			LEFT JOIN (SELECT * FROM viewDriverSchedule WHERE OnDuty = 1) ds ON ds.DriverID = d.ID AND ScheduleDate = CAST(@StartDate AS DATE)
			OUTER APPLY dbo.fnCarrierRules(@StartDate, null, @__ENFORCE_DRIVER_SCHEDULING__, d.ID, d.CarrierID, d.TerminalID, d.OperatingStateID, d.RegionID, 1) cr_a -- Enforce Driver Scheduling Carrier Rule
			OUTER APPLY dbo.fnCarrierRules(@StartDate, null, @__ENFORCE_DRIVER_COMPLIANCE__, d.ID, d.CarrierID, d.TerminalID, d.OperatingStateID, d.RegionID, 1) cr_c
			OUTER APPLY dbo.fnCarrierRules(@StartDate, null, @__ENFORCE_HOS__, d.ID, d.CarrierID, d.TerminalID, d.OperatingStateID, d.RegionID, 1) cr_h
			OUTER APPLY (SELECT TOP 1 * FROM dbo.fnHosViolationDetail(d.ID, null, @StartDate) ORDER BY LogDateUTC DESC) hos
			OUTER APPLY (SELECT TOP 1 * FROM viewHosPolicy WHERE ID = ISNULL((SELECT TOP 1 p.ID FROM tblHosPolicy p WHERE Name = 
																					(SELECT Value FROM dbo.fnCarrierRules(@StartDate, null, @__HOS_POLICY__ , d.ID, d.CarrierID, d.TerminalID, d.OperatingStateID, d.RegionID, 1)))
									, 1)) policy
			OUTER APPLY (
						SELECT CurrentWorkload = COUNT(*) 
							, CurrentECOT = SUM(CASE WHEN o.StatusID = 8 THEN ISNULL(r.ECOT, 60+ISNULL(r.ActualMiles,60)+60)/2
													   ELSE COALESCE(r.ECOT, 60+ISNULL(r.ActualMiles,60)+60) END)
						FROM tblOrder o LEFT JOIN tblRoute r ON o.RouteID = r.ID
						WHERE o.DriverID = d.ID
						  AND o.StatusID IN (2,7,8) -- Dispatched, Accepted, Picked Up
						  AND o.DeleteDateUTC IS NULL) o

		WHERE d.DeleteDateUTC IS NULL
			AND d.TruckID IS NOT NULL AND d.TrailerID IS NOT NULL		
			AND (ISNULL(@CarrierID, 0) <= 0 OR @CarrierID = d.CarrierID)
			AND (ISNULL(@TerminalID, 0) <= 0 OR @TerminalID = d.TerminalID OR d.TerminalID IS NULL)
			AND (ISNULL(@RegionID, -1) <= 0 OR @RegionID = d.RegionID)
			AND (ISNULL(@StateID, 0) <= 0 OR @StateID = d.StateID)
			AND (ISNULL(@DriverGroupID, 0) <= 0 OR @DriverGroupID = d.ID)
	) AS X
	ORDER BY DriverScore DESC, LastName, FirstName
	RETURN
END
GO

/****************************************************/
-- Created: 2016.09.14 - 4.1.7 - Joe Engler
-- Purpose: Retrieve the drivers (with last known GPS coordinates) for the specified filter criteria.  
--				HoursBack is number of hours before a location is considered "stale" and excluded from this query
-- Changes:
--	4.5.0	- 2017/01/25	- BSB	- Add terminal
/****************************************************/
ALTER FUNCTION fnRetrieveEligibleDrivers(@CarrierID INT, @TerminalID INT, @StateID INT, @RegionID INT, @DriverGroupID INT, @StartDate DATETIME, @HoursBack INT = 0) 
RETURNS TABLE AS
RETURN
	SELECT d.*, q.Lat, q.Lon, DriverLocationDateUTC = q.CreateDateUTC, DriverLocationID = q.ID
	FROM dbo.fnRetrieveEligibleDrivers_NoGPS(@CarrierID, @TerminalID, @StateID, @RegionID, @DriverGroupID, @StartDate) d
	LEFT JOIN fnRetrieveDriverLastLocation(NULL) q ON q.DriverID = d.ID 
		AND (@HoursBack = 0 OR CreateDateUTC > DATEADD(HOUR, -@HoursBack, GETUTCDATE())) -- exclude "stale" locations 
GO

/********************************************
-- Date Created: 2016 Sep 21
-- Author: Joe Engler
-- Purpose: Get summary counts from HOS records and determine if violations occur.  Use passed date minus a shift for a starting point
-- Changes:
--		4.2.5		2016-10-27		JAE			Added Weekly OnDuty fields
--		4.3.1		2016-11-04		JAE			Fixed on duty daily hours to only include on duty (breaks should not be included)
--		4.4.6.3		2016-12-02		JAE			Alter how Sleeper Berth is calculated
--		4.4.8		2016-12-13		JAE			Updated script to read individual carrier rules from new policy table
--		4.4.13		2016-12-07		JAE			Make sure violation times occur within window (was held up in another branch)
--		4.5.0		2017-01-25		BSB			Add Terminal
********************************************/
ALTER FUNCTION fnHosViolationDetail(@DriverID INT, @StartDate DATETIME, @EndDate DATETIME)
RETURNS @ret TABLE
(
	DriverID INT,
	HosPolicyID INT,
	LogDateUTC DATETIME, 
	EndDateUTC DATETIME, 
	TotalHours FLOAT, 
	TotalHoursSleeping FLOAT,
	TotalHoursDriving FLOAT,
	SleeperStatusID INT,
	WeeklyReset BIT,
	DailyReset BIT,
	SleepBreak BIT, 
	DriverBreak BIT,
	Status VARCHAR(20),
	SleeperReset BIT,
	LastWeeklyReset DATETIME,
	LastDailyReset DATETIME,
	LastBreak DATETIME,
	LastSleeperBerthReset DATETIME,
	HoursSinceWeeklyReset FLOAT,
	OnDutyHoursSinceWeeklyReset FLOAT,
	DrivingHoursSinceWeeklyReset FLOAT,
	OnDutyHoursSinceDailyReset FLOAT,
	DrivingHoursSinceDailyReset FLOAT,
	OnDutyDailyLimit FLOAT,
	HoursSinceBreak FLOAT,
	OnDutyHoursLeft FLOAT,
	OnDutyViolation BIT,
	DrivingDailyLimit FLOAT,
	DrivingHoursLeft FLOAT,
	DrivingViolation BIT, 
	BreakLimit FLOAT,
	HoursTilBreak FLOAT,
	BreakViolation BIT,
	WeeklyOnDutyLimit FLOAT,
	WeeklyOnDutyHoursLeft FLOAT,
	WeeklyOnDutyViolation BIT,
	WeeklyDrivingLimit FLOAT,
	WeeklyDrivingHoursLeft FLOAT,
	WeeklyDrivingViolation BIT,
	OnDutyViolationDateUTC DATETIME,
	DrivingViolationDateUTC DATETIME,
	BreakViolationDateUTC DATETIME,
	WeeklyOnDutyViolationDateUTC DATETIME,
	WeeklyDrivingViolationDateUTC DATETIME
)
AS BEGIN
	DECLARE @HOSPolicyID INT 
	SELECT @HOSPolicyID = ISNULL(
		(SELECT p.ID 
		 FROM tblHosPolicy p
		 WHERE Name = (SELECT cr.Value
						FROM viewDriverBase d
						CROSS APPLY dbo.fnCarrierRules(@EndDate, null, 1 /* HOS Policy Carrier Rule */, d.ID, d.CarrierID, d.TerminalID, d.OperatingStateID, d.RegionID, 1) cr
						WHERE d.id = @DriverID))
	, 1)

	DECLARE @WEEKLYRESET FLOAT
	DECLARE @DAILYRESET FLOAT
	DECLARE @SLEEPERBREAK FLOAT
	DECLARE @BREAK FLOAT
	DECLARE @SLEEPERRESET FLOAT
	DECLARE @QUALIFIEDREST FLOAT
	DECLARE @WEEK FLOAT
	DECLARE @ONDUTY_WEEKLY_LIMIT FLOAT
	DECLARE @DRIVING_WEEKLY_LIMIT FLOAT
	DECLARE @ONDUTY_DAILY_LIMIT FLOAT
	DECLARE @DRIVING_DAILY_LIMIT FLOAT
	DECLARE @DRIVING_BREAK_LIMIT FLOAT

	SELECT @WEEKLYRESET = WeeklyReset / 60.0,
			@DAILYRESET = DailyReset / 60.0,
			@SLEEPERBREAK = SleeperBreak / 60.0,
			@BREAK = DrivingBreak / 60.0,
			@SLEEPERRESET = SleeperBerthSplitTotal / 60.0,
			@QUALIFIEDREST = SleeperBerthSplitMinimum / 60.0,
			@WEEK = ShiftIntervalDays,
			@ONDUTY_WEEKLY_LIMIT = OnDutyWeeklyLimit / 60.0,
			@DRIVING_WEEKLY_LIMIT = DrivingWeeklyLimit / 60.0,
			@ONDUTY_DAILY_LIMIT = OnDutyDailyLimit / 60.0,
			@DRIVING_DAILY_LIMIT = DrivingDailyLimit / 60.0,
			@DRIVING_BREAK_LIMIT = DrivingBreakLimit / 60.0
			FROM tblHosPolicy
			WHERE ID = @HOSPolicyID

	IF @StartDate IS NULL
		SELECT @StartDate = DATEADD(DAY, -1, @EndDate) -- one day

	-- Get records up to previous shift for calculating violations, these older records will get filtered out later
	DECLARE @StartDateX DATETIME = DATEADD(DAY, -@WEEK, @StartDate) -- one shift

	-- Core HOS data with the basic info filled in (i.e. is this a weekly reset, daily reset, etc)
	-- Sleeper and Off Duty are grouped together for summing off duty blocks
	DECLARE @tmpHOS TABLE 
	(
		LogDateUTC DATETIME, 
		EndDateUTC DATETIME, 
		TotalHours FLOAT, 
		TotalHoursSleeping FLOAT,
		TotalHoursDriving FLOAT,
		SleeperStatusID INT,
		WeeklyReset INT,
		DailyReset INT,
		SleepBreak INT, -- As INT for multiplying as factor 
		DriverBreak INT,
		Status VARCHAR(20)
	)

	INSERT INTO @tmpHOS
	SELECT qq.*,
		WeeklyReset = CASE WHEN SleeperStatusID = 1 AND TotalHours >= @WEEKLYRESET THEN 1 ELSE 0 END,
		DailyReset = CASE WHEN SleeperStatusID = 1 AND TotalHours >= @DAILYRESET THEN 1 ELSE 0 END,
		SleepBreak = CASE WHEN SleeperStatusID = 2 AND TotalHours >= @SLEEPERBREAK THEN 1 
						 WHEN SleeperStatusID = 1 AND TotalHours >= @DAILYRESET THEN 1 ELSE 0 END,
		DriverBreak = CASE WHEN SleeperStatusID IN (1,2) AND TotalHours >= @BREAK THEN 1 ELSE 0 END,
		Status = CASE WHEN SleeperStatusID = 1 AND TotalHours >= @WEEKLYRESET THEN 'WEEKLY RESET'
							WHEN SleeperStatusID = 1 AND TotalHours >= @DAILYRESET THEN 'DAILY RESET' 
							WHEN SleeperStatusID = 2 AND TotalHours < @DAILYRESET AND TotalHours >= @SLEEPERBREAK THEN 'SLEEP BREAK'
							WHEN SleeperStatusID = 2 AND TotalHours < @DAILYRESET AND TotalHours >= @BREAK THEN 'BREAK' ELSE '' END
	FROM (
		SELECT LogDateUTC = MIN(LogDateUTC), EndDateUTC = MAX(EndDateUTC), TotalHours = SUM(TotalHours), 
			TotalHoursSleeping = SUM(CASE WHEN HosDriverStatusID = 2 THEN TotalHours ELSE NULL END),
			TotalHoursDriving = SUM(CASE WHEN HosDriverStatusID = 3 THEN TotalHours ELSE NULL END),
			SleeperStatusID = CASE WHEN SleeperStatusID = 1 AND SUM(TotalHours) < @DAILYRESET THEN 2 --break
								ELSE SleeperStatusID END
		FROM
		(
			SELECT LogDateUTC, EndDateUTC, 
				TotalHours, 
				SleeperStatusID, HosDriverStatusID,
				Streak = (SELECT COUNT(*) FROM dbo.fnHosSummary(@driverid, @StartDateX, @EndDate) x
								WHERE x.SleeperStatusID <> y.SleeperStatusID AND x.LogDateUTC <= y.LogDateUTC) -- used just to group streaks
			FROM dbo.fnHosSummary(@DriverID, @StartDateX, @EndDate) y
		) Q
		GROUP BY Streak, SleeperStatusID
	) QQ
	WHERE TotalHours > 0 -- Ignore any "quick" changes
	ORDER BY LogDateUTC
	------------------------------------------
	-- Get core HOS data with "last" dates and sleeper reset information
	DECLARE @tmpHOSwithSleeperReset TABLE 
	(
		LogDateUTC DATETIME, 
		EndDateUTC DATETIME, 
		TotalHours FLOAT, 
		TotalHoursSleeping FLOAT,
		TotalHoursDriving FLOAT,
		SleeperStatusID INT,
		WeeklyReset INT,
		DailyReset INT,
		SleepBreak INT, -- As INT for multiplying as factor 
		DriverBreak INT,
		Status VARCHAR(20),
		SleeperReset INT,
		LastWeeklyReset DATETIME,
		LastDailyReset DATETIME,
		LastBreak DATETIME
	)

	INSERT INTO @tmpHOSwithSleeperReset 
	(
		LogDateUTC,
		EndDateUTC, 
		TotalHours, 
		TotalHoursSleeping,
		TotalHoursDriving,
		SleeperStatusID,
		WeeklyReset,
		DailyReset,
		SleepBreak, 
		DriverBreak,
		Status,
		SleeperReset,
		LastWeeklyReset,
		LastDailyReset,
		LastBreak
	)
	SELECT hos.*, 
		SleeperReset = CASE WHEN lsb.Hrs >= @SLEEPERRESET OR hos.DailyReset = 1 THEN 1 ELSE 0 END,

		LastWeeklyReset = ISNULL(lr.EndDateUTC, @StartDateX),
		LastDailyReset = COALESCE(ldr.EndDateUTC, lr.EndDateUTC, @StartDateX),
		LastBreak = COALESCE(lb.EndDateUTC, ldr.EndDateUTC, lr.EndDateUTC, @StartDateX)

	FROM @tmphos hos
	-- Last Weekly Reset
	OUTER APPLY (SELECT TOP 1 EndDateUTC 
					FROM @tmphos
					WHERE WeeklyReset = 1 AND LogDateUTC < hos.LogDateUTC 
					ORDER BY LogDateUTC DESC) lr
	-- Last Daily Reset
	OUTER APPLY (SELECT TOP 1 EndDateUTC 
					FROM @tmphos
					WHERE DailyReset = 1 AND LogDateUTC < hos.LogDateUTC 
					ORDER BY LogDateUTC DESC) ldr
	-- Last Break
	OUTER APPLY (SELECT TOP 1 EndDateUTC 
					FROM @tmphos
					WHERE DriverBreak = 1 AND LogDateUTC < hos.LogDateUTC 
					ORDER BY LogDateUTC DESC) lb
	-- Last Sleeper Berth Reset (used to see if a reset, date is grabbed later since you use the previous break)
	OUTER APPLY (SELECT EndDateUTC = MIN(EndDateUTC), 
						Hrs = SUM(ISNULL(TotalHoursSleeping,0)) --*MAX(ISNULL(SleepBreak,0)) -- Multiply by sleep break to ensure one of the entries is an 8 hr block
					FROM @tmphos
					WHERE DriverBreak = 1  AND logDateUTC < hos.EndDateUTC 
					AND TotalHoursSleeping >= @QUALIFIEDREST -- Both must be a qualified rest break to count
					AND EndDateUTC >= COALESCE(lb.EndDateUTC, ldr.EndDateUTC)) lsb -- Since Last Daily Reset
	------------------------------------------
	-- Get detail HOS information with total hours and violations
	INSERT INTO @ret (
		DriverID,
		HOSPolicyID,
		LogDateUTC, 
		EndDateUTC, 
		TotalHours, 
		TotalHoursSleeping,
		TotalHoursDriving,
		SleeperStatusID,
		WeeklyReset,
		DailyReset,
		SleepBreak, 
		DriverBreak,
		Status,
		SleeperReset,
		LastWeeklyReset,
		LastDailyReset,
		LastBreak,
		LastSleeperBerthReset,
		HoursSinceWeeklyReset,
		OnDutyHoursSinceWeeklyReset,
		DrivingHoursSinceWeeklyReset,
		OnDutyHoursSinceDailyReset,
		DrivingHoursSinceDailyReset,
		HoursSinceBreak,
		OnDutyDailyLimit,
		OnDutyHoursLeft,
		OnDutyViolation,
		DrivingDailyLimit,
		DrivingHoursLeft,
		DrivingViolation,
		BreakLimit,
		HoursTilBreak,
		BreakViolation,
		WeeklyOnDutyLimit,
		WeeklyOnDutyHoursLeft,
		WeeklyOnDutyViolation,
		WeeklyDrivingLimit,
		WeeklyDrivingHoursLeft,
		WeeklyDrivingViolation,
		OnDutyViolationDateUTC,
		DrivingViolationDateUTC,
		BreakViolationDateUTC,
		WeeklyOnDutyViolationDateUTC,
		WeeklyDrivingViolationDateUTC
	)
	SELECT @DriverID,
		@HOSPolicyID,
		*,
		OnDutyViolationDateUTC = CASE WHEN OnDutyViolation = 0 THEN NULL
				ELSE dbo.fnMaxDateTime(DATEADD(MINUTE, 60*(TotalHours - (OnDutyHoursSinceDailyReset - OnDutyDailyLimit)), LogDateUTC), LogDateUTC) END, -- max of time or start of onduty time (violation doesn't occur until you're on duty)
		DrivingViolationDateUTC = CASE WHEN DrivingViolation = 0 THEN NULL
				ELSE dbo.fnMaxDateTime(DATEADD(MINUTE, 60*(TotalHours - (DrivingHoursSinceDailyReset - DrivingDailyLimit)), LogDateUTC), LogDateUTC) END, -- max of time or start of driving time (violation doesn't occur until you start driving)
		BreakViolationDateUTC = CASE WHEN BreakViolation = 0 THEN NULL
				ELSE dbo.fnMaxDateTime(DATEADD(MINUTE, 60*(TotalHours + HoursTilBreak), LogDateUTC), LogDateUTC) END, -- max of time or start of driving time (violation doesn't occur until you start driving)
		WeeklyOnDutyViolationDateUTC = CASE WHEN WeeklyOnDutyViolation = 0 THEN NULL
				ELSE dbo.fnMaxDateTime(DATEADD(MINUTE, 60*(TotalHours - (OnDutyHoursSinceWeeklyReset - WeeklyOnDutyLimit)), LogDateUTC), LogDateUTC) END, -- max of time or start of onduty time (violation doesn't occur until you're on duty)
		WeeklyDrivingViolationDateUTC = CASE WHEN WeeklyDrivingViolation = 0 THEN NULL
				ELSE dbo.fnMaxDateTime(DATEADD(MINUTE, 60*(TotalHours - (DrivingHoursSinceWeeklyReset - WeeklyDrivingLimit)), LogDateUTC), LogDateUTC) END-- max of time or start of driving time (violation doesn't occur until you start driving)
	FROM 
	(
		SELECT hos.*,
			OnDutyDailyLimit = @ONDUTY_DAILY_LIMIT,
			OnDutyHoursLeft = CASE WHEN @ONDUTY_DAILY_LIMIT < OnDutyHoursSinceDailyReset THEN 0 ELSE @ONDUTY_DAILY_LIMIT - OnDutyHoursSinceDailyReset END,
			OnDutyViolation = CASE WHEN @ONDUTY_DAILY_LIMIT < OnDutyHoursSinceDailyReset AND SleeperStatusID IN (3,4) THEN 1 ELSE 0 END,

			DrivingDailyLimit = @DRIVING_DAILY_LIMIT,
			DrivingHoursLeft = CASE WHEN @DRIVING_DAILY_LIMIT < DrivingHoursSinceDailyReset THEN 0 ELSE @DRIVING_DAILY_LIMIT - DrivingHoursSinceDailyReset END,
			DrivingViolation = CASE WHEN @DRIVING_DAILY_LIMIT < DrivingHoursSinceDailyReset AND SleeperStatusID = 3 THEN 1 ELSE 0 END,

			BreakLimit = @DRIVING_BREAK_LIMIT,
			HoursTilBreak = @DRIVING_BREAK_LIMIT - HoursSinceBreak,
			BreakViolation = CASE WHEN @DRIVING_BREAK_LIMIT < HoursSinceBreak AND SleeperStatusID = 3 THEN 1 ELSE 0 END,

			WeeklyOnDutyLimit = @ONDUTY_WEEKLY_LIMIT,
			WeeklyOnDutyHoursLeft = CASE WHEN @ONDUTY_WEEKLY_LIMIT < OnDutyHoursSinceWeeklyReset THEN 0 ELSE @ONDUTY_WEEKLY_LIMIT - OnDutyHoursSinceWeeklyReset END,
			WeeklyOnDutyViolation = CASE WHEN @ONDUTY_WEEKLY_LIMIT < OnDutyHoursSinceWeeklyReset AND SleeperStatusID IN (3,4) THEN 1 ELSE 0 END,

			WeeklyDrivingLimit = @DRIVING_WEEKLY_LIMIT,
			WeeklyDrivingHoursLeft = CASE WHEN @DRIVING_WEEKLY_LIMIT < DrivingHoursSinceWeeklyReset THEN 0 ELSE @DRIVING_WEEKLY_LIMIT - DrivingHoursSinceWeeklyReset END,
			WeeklyDrivingViolation = CASE WHEN @DRIVING_WEEKLY_LIMIT < DrivingHoursSinceWeeklyReset AND SleeperStatusID = 3 THEN 1 ELSE 0 END
		FROM
		(
			SELECT hos.*,
				HoursSinceWeeklyReset = 
							CASE WHEN WeeklyReset = 1 THEN 0
							ELSE ISNULL((SELECT SUM(TotalHours) FROM @tmphos WHERE LogDateUTC >= LastWeeklyReset AND LogDateUTC < hos.EndDateUTC), 0) END,

				OnDutyHoursSinceWeeklyReset = 
							CASE WHEN WeeklyReset = 1 THEN 0
							ELSE ISNULL((SELECT SUM(TotalHours) FROM @tmphos WHERE LogDateUTC >= LastWeeklyReset AND LogDateUTC < hos.EndDateUTC 
																			AND SleeperStatusID IN (3,4)), 0) END,
				DrivingHoursSinceWeeklyReset = 
							CASE WHEN WeeklyReset = 1 THEN 0
							ELSE ISNULL((SELECT SUM(TotalHours) FROM @tmphos WHERE LogDateUTC >= LastWeeklyReset AND LogDateUTC < hos.EndDateUTC 
																			AND SleeperStatusID = 3), 0) END,
				OnDutyHoursSinceDailyReset = 
							CASE WHEN DailyReset = 1 THEN 0
							ELSE ISNULL((SELECT SUM(TotalHours) FROM @tmphos WHERE LogDateUTC >= LastSleeperBerthReset AND LogDateUTC < hos.EndDateUTC 
																			--AND SleepBreak = 0 -- Skip sleeper block
																			AND SleeperStatusID IN (3,4)), 0) END, 
				DrivingHoursSinceDailyReset = 
							CASE WHEN DailyReset = 1 THEN 0
							ELSE ISNULL((SELECT SUM(TotalHours) FROM @tmphos WHERE LogDateUTC >= LastSleeperBerthReset AND LogDateUTC < hos.EndDateUTC 
																			AND SleeperStatusID = 3), 0) END,
				HoursSinceBreak = 
							CASE WHEN DriverBreak = 1 THEN 0
							ELSE ISNULL((SELECT SUM(TotalHours) FROM @tmphos WHERE SleepBreak = 0 AND LogDateUTC >= LastBreak AND LogDateUTC < hos.EndDateUTC),0) END
			FROM 
			(
				SELECT hos.*,
					LastSleeperBerthReset = dbo.fnMaxDateTime(lb.LastBreak, LastDailyReset) -- get more recent between sleeper reset or daily reset

				FROM @tmpHOSwithSleeperReset hos
				-- Last Sleeper Berth Reset
				OUTER APPLY (SELECT TOP 1 LastBreak
									FROM @tmpHOSwithSleeperReset
									WHERE SleeperReset = 1 AND LogDateUTC < hos.LogDateUTC
									ORDER BY LogDateUTC DESC) lb
			) hos
		) hos
	) Q
	WHERE EndDateUTC > @StartDate -- Filter older records used for calculating violations
	RETURN
END
GO

/****************************************************
 Date Created: 2016/12/22
 Author: Joe Engler
 Purpose: return all driver compliance with non compliant and missing records
 Changes:
--	4.5.0		BSB		2017-01-25		Add Terminal
****************************************************/
ALTER FUNCTION fnDriverComplianceSummary(@showCompliant INT = 0, @showUnenforced INT = 0)
RETURNS @ret TABLE
(
	ID INT,
	DriverID INT,
	FullName VARCHAR(41),
	CarrierID INT,
	Carrier VARCHAR(40),
	ComplianceTypeID INT,
	ComplianceType VARCHAR(100),
	MissingDocument BIT,
	ExpiredNow BIT,
	ExpiredNext30 BIT,
	ExpiredNext60 BIT,
	ExpiredNext90 BIT,
	Missing BIT
)
AS BEGIN
 	DECLARE @__ENFORCE_DRIVER_COMPLIANCE__ INT = 14

	INSERT INTO @ret
	SELECT  ID, DriverID, FullName, CarrierID, Carrier, ComplianceTypeID, ComplianceType, MissingDocument, ExpiredNow, ExpiredNext30, ExpiredNext60, ExpiredNext90, Missing = CAST(0 AS BIT)
	  FROM viewDriverCompliance
	 WHERE DeleteDateUTC IS NULL
	   AND (@showCompliant = 1 OR MissingDocument = 1 OR ExpiredNow = 1)
	UNION 
	SELECT ID = NULL,
		DriverID = q.ID,
		q.FullName,
		q.CarrierID,
		q.Carrier,
		ComplianceTypeID = ct.id,
		ComplianceType = ct.Name,
		MissingDocument = CAST(0 AS BIT),
		ExpiredNow = CAST(0 AS BIT),
		ExpiredNext30 = CAST(0 AS BIT),
		ExpiredNext60 = CAST(0 AS BIT),
		ExpiredNext90 = CAST(0 AS BIT),
		Missing = ct.IsRequired
	FROM ( -- Get active drivers with compliance enforced
			SELECT d.* 
				FROM viewDriverBase d
				OUTER APPLY dbo.fnCarrierRules(GETUTCDATE(), null, @__ENFORCE_DRIVER_COMPLIANCE__, d.ID, d.CarrierID, d.TerminalID, d.OperatingStateID, d.RegionID, 1) cr_c
				WHERE d.DeleteDateUTC is null --active drivers
				AND (@showUnenforced = 1 OR dbo.fnToBool(cr_c.Value) = 1) -- compliance enforced
		) q,
		tbldrivercompliancetype ct
	WHERE (@showCompliant = 1 OR ct.IsRequired = 1)
	 AND NOT EXISTS -- no active record in the driver compliance table
		(SELECT  1
			FROM viewDriverCompliance 
			WHERE DeleteDateUTC IS NULL AND DriverID = q.ID and ComplianceTypeID = ct.id
		)
	RETURN
END
GO

/*************************************************************/
-- Date Created: 2016/10/14
-- Author: Joe Engler
-- Purpose: Get Driver Messages, last 30 days or whatever is in the carrier rule
-- Changes:
--	4.5.0	- 2017/01/26	- BSB	- Add Terminal
/*************************************************************/
ALTER FUNCTION fnDriverMessages (@DriverID INT) RETURNS TABLE AS
RETURN
	SELECT m.* ,
			ToUserName = t.Value,
			FromUserName = f.Value
	  FROM tblDispatchMessage m
	  CROSS APPLY (

			SELECT du.UserNames, DaysBack = CAST(ISNULL(cr.Value, 30) AS INT)
			  FROM viewDriverBase d
			  JOIN tblCACHE_DriverUserNames du ON du.DriverID = d.ID
			 OUTER APPLY dbo.fnCarrierRules(GETUTCDATE(), null, 20, d.ID, d.CarrierID, d.TerminalID, d.OperatingStateID, d.RegionID, 1) cr -- Driver Messaging History
			 WHERE du.DriverID = @DriverID
	 ) q
	 cross apply (select * from fnUserAllProfileValues(m.ToUser) where Name = 'FullName') t
	 cross apply (select * from fnUserAllProfileValues(m.FromUser) where Name = 'FullName') f
	WHERE MessageDateUTC >= DATEADD(DAY, -DaysBack, GETUTCDATE())
      AND (','+UserNames+',' LIKE '%,'+ToUser+',%' OR ','+UserNames+',' LIKE '%,'+FromUser+',%') -- wrap in commas to handle multiple users using the same DriverID
GO

/********************************************
-- Date Created: 2017 Jan 03
-- Author: Joe Engler
-- Purpose: Get daily Personal Use overages for HOS
-- Changes:
--	4.5.0	- 2017/01/26	- BSB	- Add Terminal
********************************************/
ALTER FUNCTION fnHosPersonalConveyance(@StartDate DATETIME, @EndDate DATETIME, @ShowAll BIT = 0)
RETURNS TABLE AS
	RETURN 
	SELECT * 
	FROM (
		SELECT 
			Date, DriverID = dp.ID, Driver = dp.FullName, CarrierID, Carrier,
			HOSPolicyID, TotalHours, PersonalDailyLimitHR,
			Overage = CAST(CASE WHEN PersonalDailyLimitHR IS NULL OR PersonalDailyLimitHR = 0 THEN 0 -- No limit
								WHEN TotalHours > PersonalDailyLimitHR THEN 1 
								ELSE 0 END AS BIT)
		FROM (
				-- Active drivers and their HOS policy
				SELECT 
					d.ID,
					d.FullName,
					d.CarrierID,
					d.Carrier,
					HOSPolicyID = hp.ID,
					PersonalDailyLimitHR = hp.PersonalDailyLimit / 60.0
				FROM viewDriverBase d
				OUTER APPLY dbo.fnCarrierRules(getutcdate(), null, 1, d.ID, d.CarrierID, d.TerminalID, d.OperatingStateID, d.RegionID, 1) cr --best match
				LEFT JOIN tblHosPolicy hp ON RTRIM(cr.Value) = RTRIM(hp.Name)
				WHERE d.DeleteDateUTC IS NULL
		) dp
		OUTER APPLY dbo.fnHosDriverPersonalConveyanceSummary(dp.ID, @startdate, @enddate) pc
	) q
	WHERE @ShowAll = 1 OR Overage = 1
GO


/*********************************/
/* Refreshing these views is required at this point so that other things will update next without errors */
/*********************************/
EXEC sp_refreshview 'viewOrderBase'
GO
EXEC sp_refreshview 'viewOrder'
GO
EXEC sp_refreshview 'viewOrderLocalDates'
GO
EXEC sp_refreshview 'viewOrderApprovalSource'
GO
EXEC sp_refreshview 'viewOrder_OrderTicket_Full'
GO


/**********************************************************
-- Creation Info: 3.13.1 - 2016/07/06
-- Author: Kevin Alons
-- Purpose: return the Audit table records (optionally filtered to just the specified driver and/or ordernum) - without GPS info
-- Changes:
--		4.4.3		2016/11/17		JAE			**NO CHANGE** (ticket was closed)
--		4.5.0		2017/01/30		BSB			Add Terminal
***********************************************************/
ALTER FUNCTION fnOrders_AllTickets_Audit_NoGPS
(
  @carrierID int
, @driverID int
, @TerminalID int
, @orderNum int
, @id int
) RETURNS TABLE AS RETURN
	SELECT *
		, HasError = cast(CASE WHEN Errors IS NULL THEN 0 ELSE 1 END as bit)
	FROM (
		SELECT O.* 
			, OriginLatLon = ltrim(OO.LAT) + ',' + ltrim(OO.LON)
			, OriginGeoFenceRadiusMeters = OO.GeoFenceRadiusMeters
			, DestLatLon = ltrim(D.LAT) + ',' + ltrim(D.LON)
			, DestGeoFenceRadiusMeters = D.GeoFenceRadiusMeters
			, OriginGrossBBLS = dbo.fnConvertUOM(O.OriginGrossUnits, O.OriginUomID, 1)
			, Errors = 
				nullif(
					SUBSTRING(
						CASE WHEN O.OrderDate IS NULL THEN ',Missing Order Date' ELSE '' END
					  + CASE WHEN O.OriginArriveTimeUTC IS NULL THEN ',Missing Pickup Arrival' ELSE '' END
					  + CASE WHEN O.OriginDepartTimeUTC IS NULL THEN ',Missing Pickup Departure' ELSE '' END
					  + CASE WHEN O.Rejected = 0 AND O.Tickets IS NULL THEN ',No Active Tickets' ELSE '' END
					  + CASE WHEN O.Rejected = 0 AND O.DestArriveTimeUTC IS NULL THEN ',Missing Delivery Arrival' ELSE '' END
					  + CASE WHEN O.Rejected = 0 AND O.DestDepartTimeUTC IS NULL THEN ',Missing Delivery Departure' ELSE '' END
					  + CASE WHEN O.Rejected = 0 AND O.OriginGrossUnits = 0 AND O.OriginNetUnits = 0 AND O.OriginWeightNetUnits = 0 THEN ',No Origin Units are entered. '+CHAR(13)+CHAR(10)+' This may be an unmarked rejected load.' ELSE '' END
					  + CASE WHEN O.Rejected = 0 AND dbo.fnConvertUOM(O.OriginGrossUnits, O.OriginUomID, 2) > isnull(OORMG.MaxGallons, 320) THEN ',Origin GOV Units out of limits' ELSE '' END
					  + CASE WHEN O.Rejected = 0 AND dbo.fnConvertUOM(O.OriginGrossStdUnits, O.OriginUomID, 2) > isnull(OORMG.MaxGallons, 320) THEN ',Origin GSV Units out of limits' ELSE '' END
					  + CASE WHEN O.Rejected = 0 AND dbo.fnConvertUOM(O.OriginNetUnits, O.OriginUomID, 2) > isnull(OORMG.MaxGallons, 320) THEN ',Origin NSV Units out of limits' ELSE '' END				  
					  + CASE WHEN dbo.fnToBool(OOR.Value) = 1 AND (SELECT count(*) FROM tblOrderTicket OT WHERE OrderID = O.ID AND OT.DeleteDateUTC IS NULL AND OT.DispatchConfirmNum IS NULL) > 0 THEN ',Missing Ticket Shipper PO' ELSE '' END
					  + CASE WHEN EXISTS(SELECT CustomerID FROM viewOrder_OrderTicket_Full WHERE ID = O.ID AND DeleteDateUTC IS NULL AND nullif(rtrim(T_DispatchConfirmNum), '') IS NOT NULL GROUP BY CustomerID, T_DispatchConfirmNum HAVING COUNT(*) > 1) THEN ',Duplicate Shipper PO' ELSE '' END
					  + CASE WHEN O.TruckID IS NULL THEN ',Truck Missing' ELSE '' END
					  + CASE WHEN O.TrailerID IS NULL THEN ',Trailer 1 Missing' ELSE '' END
					, 2, 8000) 
				, '')
			, IsEditable = cast(CASE WHEN O.StatusID = 3 THEN 1 ELSE 0 END as bit)
		FROM viewOrder_AllTickets O
		JOIN tblOrigin OO ON OO.ID = O.OriginID
		JOIN tblCustomer C ON C.ID = O.CustomerID
		JOIN tblDestination D ON D.ID = O.DestinationID
		-- max Gallons for this order
		OUTER APPLY (SELECT TOP 1 MaxGallons = cast(Value as decimal(18, 2)) FROM dbo.fnOrderOrderRules(O.ID) WHERE TypeID = 6) OORMG 
		-- ShipperPO_Required OrderRule
		OUTER APPLY (SELECT TOP 1 Value FROM dbo.fnOrderOrderRules(O.ID) WHERE TypeID = 2) OOR
		LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
		LEFT JOIN tblOrderSettlementShipper OIC ON OIC.OrderID = O.ID
		LEFT JOIN tblOrderSettlementCarrier OSC ON OSC.OrderID = O.ID
		LEFT JOIN tblOrderSettlementProducer OSP ON OSP.OrderID = O.ID
		WHERE OIC.BatchID IS NULL AND OSC.BatchID IS NULL AND OSP.BatchID IS NULL /* don't even show SETTLED orders on the AUDIT page ever */
		  AND nullif(OA.Approved, 0) IS NULL /* don't show Approved orders on the AUDIT page */
		  AND (isnull(@carrierID, 0) = -1 OR O.CarrierID = @carrierID)
		  AND (isnull(@driverID, 0) = -1 OR O.OriginDriverID = @driverID)
		  AND (@TerminalID = -1
		    OR (OriginID IN (SELECT ID FROM tblOrigin WHERE TerminalID = @TerminalID OR TerminalID IS NULL)	/* All Valid Origins */
			    AND 
                DestinationID IN (SELECT ID FROM tblDestination WHERE TerminalID = @TerminalID OR TerminalID IS NULL) /* All Valid Destinations */
                AND
                DriverID IN (SELECT ID FROM tblDriver WHERE TerminalID = @TerminalID OR TerminalID IS NULL) /* All Valid Drivers */ ))
		  AND ((nullif(@orderNum, 0) IS NULL AND O.DeleteDateUTC IS NULL AND (O.StatusID = 3 AND DeliverPrintStatusID IN (SELECT ID FROM tblPrintStatus WHERE IsCompleted = 1))) OR O.OrderNum = @orderNum)
		  AND (nullif(@id, 0) IS NULL OR O.ID = @id)
	) X
GO

/**********************************************************
-- Author: Kevin Alons
-- Date Created: 8 Oct 2014
-- Purpose: return the Audit table records (optionally filtered to just the specified driver and/or ordernum)
-- Changes:
	- 3.7.28	- 2015/06/18 - KDA	- add filter to ensure APPROVED orders are not displayed
	- 3.7.44	- 2015/07/06 - BB	- Added error catching rule and message for zero volume loads not marked rejected.
	- 3.8.11	- 2015/07/28 - KDA	- revise error mesages for excessive load volumes to honor new associated OrderRule
	- 3.9.0		- 2015/08/20 - KDA	- slight revision to WHERE clause to use O.ID = @id instead of LIKE operator (efficiency optimization)
	- 3.9.2		- 2015/08/26 - KDA	- add Validation Rule to ensure the OrderDate field is NOT NULL 
	- 3.9.25
	- 3.9.38	- 2015/01/18 - BB	- Added weight net units volume check to error list substring
	- 3.11.17.2 - 2016/04/18 - JAE	- Added logic to omit Producer and Carrier settled orders
	- 3.13.1	- 2016/07/06 - KDA	- use fnOrders_AllTickets_Audit_NoGPS as base 
									- use new fnDriverLocation_OriginFirstArrive + fnDriverLocation_DestinatioFirstArrive functions (instead of corresponding views)
	- 4.5.0		- 2017/01/30 - BSB	- Add terminal
***********************************************************/
ALTER FUNCTION fnOrders_AllTickets_Audit
(
  @carrierID int
, @driverID int
, @TerminalID int
, @orderNum int
, @id int
) RETURNS TABLE AS RETURN
SELECT *
	, OriginDistanceText = CASE WHEN OriginGpsArrived = 1 THEN 'Verified' ELSE isnull(ltrim(OriginDistance), 'N/A') END
	, DestDistanceText = CASE WHEN DestGpsArrived = 1 THEN 'Verified' ELSE isnull(ltrim(DestDistance), 'N/A') END
FROM (
	SELECT BASE.* 
		, OriginGpsLatLon = ltrim(DLO.Lat) + ',' + ltrim(DLO.Lon)
		, OriginAccuracyMeters = DLO.SourceAccuracyMeters
		, OriginDistance = CASE WHEN DLO.DistanceToPoint < 0 THEN NULL ELSE DLO.DistanceToPoint END
		, OriginGpsArrived = CASE WHEN DLO.DistanceToPoint IS NOT NULL AND DLO.DistanceToPoint BETWEEN 0 AND BASE.OriginGeoFenceRadiusMeters THEN 1 ELSE 0 END
		, DestGpsLatLon = ltrim(DLD.Lat) + ',' + ltrim(DLD.Lon)
		, DestAccuracyMeters = DLD.SourceAccuracyMeters
		, DestDistance = CASE WHEN DLD.DistanceToPoint < 0 THEN NULL ELSE DLD.DistanceToPoint END
		, DestGpsArrived = CASE WHEN DLD.DistanceToPoint IS NOT NULL AND DLD.DistanceToPoint BETWEEN 0 AND BASE.DestGeoFenceRadiusMeters THEN 1 ELSE 0 END
	FROM dbo.fnOrders_AllTickets_Audit_NoGPS(@carrierID, @driverID, @TerminalID, @orderNum, @id) BASE
	OUTER APPLY fnDriverLocation_OriginFirstArrive(BASE.ID, BASE.OriginID) DLO 
	OUTER APPLY fnDriverLocation_DestinationFirstArrive(BASE.ID, BASE.DestinationID) DLD
) X
GO




/*********************************************************
				STORED PROCEDURE UPDATES
*********************************************************/
GO

/******************************************
** Date Created:	2016/07/15
** Author:			Joe Engler
** Purpose:			Return the historical HOS entries for a driver, calculate the TimeInState for ease of computation on mobile side
** Changes:
--		4.2.5		JAE		2016-10-26		Use operating state (instead of state) to carrier rule call
--		4.4.15		JAE		2017-01-06		Use policy (not carrier rule) to get days to keep
--		4.5.0		BSB		2017-01-26		Add Terminal
******************************************/
ALTER PROCEDURE spDriverHOSLog(@DriverID INT, @LastSyncDate DATETIME) AS
BEGIN
    DECLARE @DaysToKeep INT = 
			(SELECT DriverAppLogRetentionDays 
				FROM tblHosPolicy 
				WHERE ID = COALESCE(
					(SELECT ID FROM tblHosPolicy WHERE Name = (SELECT r.Value
						FROM tblDriver d 
						CROSS APPLY dbo.fnCarrierRules(GETDATE(), null, 1, d.ID, d.CarrierID, d.TerminalID, d.OperatingStateID, d.RegionID, 1) r 
						WHERE d.ID = @DriverID)), 1) -- Use default policy if none set
			)

	DECLARE @StartDate DATETIME = DATEADD(DAY, -@DaysToKeep, GETUTCDATE());

	WITH rows AS
	(
 			-- Dummy start record, gets the previous record prior to the start date
			SELECT 0 AS rownum, *
            FROM tblHos
			WHERE ID = (SELECT TOP 1 ID FROM tblHos WHERE DriverID = @DriverID AND LogDateUTC < @StartDate ORDER BY LogDateUTC DESC) 

			UNION

			SELECT ROW_NUMBER() OVER (ORDER BY LogDateUTC) AS rownum, *
			FROM tblHos
			WHERE DriverID = @DriverID
			AND LogDateUTC BETWEEN @StartDate AND GETUTCDATE()

			UNION

			-- Dummy end record, gets the final record again so it will show in the summary list (with TimeInState = 0)
			SELECT (select count(*)+1 FROM tblHos WHERE DriverID = @DriverID AND LogDateUTC BETWEEN @StartDate AND GETUTCDATE()) AS rownum, *
			FROM tblHos
			WHERE DriverID = @DriverID
			AND ID = (SELECT TOP 1 ID FROM tblHos WHERE DriverID = @DriverID AND LogDateUTC <= GETUTCDATE() ORDER BY LogDateUTC DESC)
	)
		SELECT mc.*, CASE WHEN mc.ID = mp.ID THEN null -- send null time in state for current HOS (repeated at end) 
						else DATEDIFF(MINUTE, mc.LogDateUTC, mp.LogDateUTC) END AS TimeInState
		FROM rows mc
		JOIN rows mp
		ON mc.rownum = mp.rownum-1
		CROSS JOIN fnSyncLCDOffset(@LastSyncDate) LCD
		WHERE @LastSyncDate IS NULL 
		   OR mc.CreateDateUTC >= LCD.LCD -- use offset to get nearest city updates for records just sent to the server
		   OR mc.LastChangeDateUTC >= LCD.LCD
		   OR mc.DeleteDateUTC >= LCD.LCD
		ORDER BY mc.LogDateUTC
END
GO

/**********************************************************
-- Creation Info: 3.13.1 - 2016/07/06
-- Author: Kevin Alons
-- Purpose: return 1 (true) if order is eligible to be Auto Audited, else return 0 (false)
-- Changes:
	- 3.13.1	- 2016/07/04	- KDA	- add START/DONE print statements (for debugging)
	- 4.5.0		- 2017/01/26	- BSB	- Add Terminal
***********************************************************/
ALTER PROCEDURE spIsOrderAutoApproveEligible(@id int, @isEligible bit OUTPUT) AS 
BEGIN
	PRINT 'spIsOrderAutoApproveEligible(' + ltrim(@id) + ') START: ' + (CONVERT(VARCHAR(24), GETDATE(), 121))

	SET @isEligible = (SELECT count(ID) FROM dbo.fnOrders_AllTickets_Audit_NoGPS(-1, -1, -1, NULL, @id) WHERE HasError = 0)

	PRINT 'spIsOrderAutoApproveEligible(' + ltrim(@id) + ') START: ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
END
GO

/*******************************************
-- Date Created: 2016/12/07 - 4.4.15
-- Author: Joe Engler
-- Purpose: return confirmation of Signature records for the specified driver (that are still relevant at this time)
-- Changes: 
--	4.5.0	- 2017/01/26	- BSB	- Add Terminal
*******************************************/
ALTER PROCEDURE spHosSignatures_DriverApp(@DriverID int, @LastChangeDateUTC datetime = NULL) AS
BEGIN
    DECLARE @DaysToKeep INT = 
			(SELECT DriverAppLogRetentionDays 
				FROM tblHosPolicy 
				WHERE ID = COALESCE(
					(SELECT ID FROM tblHosPolicy WHERE Name = (SELECT r.Value
						FROM tblDriver d 
						CROSS APPLY dbo.fnCarrierRules(GETDATE(), null, 1, d.ID, d.CarrierID, d.TerminalID, d.OperatingStateID, d.RegionID, 1) r 
						WHERE d.ID = @DriverID)), 1) -- Use default policy if none set
			)

	DECLARE @StartDate DATETIME = DATEADD(DAY, -@DaysToKeep, GETUTCDATE());

    SELECT HS.ID, HS.UID, HS.DriverID, HOSDate = DATEADD(HOUR, 12, CAST (HS.HosDate AS DATETIME)), SignatureBlob = NULL, HS.CreateDateUTC, HS.CreatedByUser -- Mobile app wants date as local noon
    FROM tblHosSignature HS
    CROSS JOIN dbo.fnSyncLCDOffset(@LastChangeDateUTC) LCD
    WHERE HS.DriverID = @DriverID 
      AND ( HS.CreateDateUTC >= LCD.LCD
		     /* always include days-to-keep signatures if a NULL @LastChangeDateUTC is specified */
			 OR @LastChangeDateUTC IS NULL AND HS.CreateDateUTC > @StartDate
        )
END
GO

/********************************************
 Date Created: 2016/05/16
 Author: Joe Engler
 Purpose: Retrieve the DAILY Allocation + Production Units for the specified selection criteria + Date Range
 Changes:
	3.12.7.1	2016/06/14	JAE		- Add check to use origin volume if destination volume is null
	3.12.7.2	2016/06/14	JAE		- Added ProductGroupID filter		
	3.12.7.4	2016/06/14	JAE		- Add check to use origin volume if destination volume is null or zero
	4.5.0		2017/01/26	BSB		- Add Terminal
********************************************/
ALTER PROCEDURE spAllocationDestinationShipper
( 
    @RegionID int = -1,
	@TerminalID int = -1,
	@DestinationID int = -1,
	@ShipperID int = -1,
	@ProductGroupID int = -1,
	@StartDate date = NULL,
	@EndDate date = NULL,
	@UomID int = 1
) AS BEGIN

SET NOCOUNT ON 
	DECLARE @ret TABLE
	(
	  TicketCount INT,
	  TotalGross DECIMAL(18,10),
	  DestinationID INT,
	  Destination VARCHAR(50),
	  ShipperID INT,
	  Shipper VARCHAR(40),
	  ProductGroupID int,
	  ProductGroup VARCHAR(25),
	  DailyUnits DECIMAL(18,10),
	  AllocationStartDate DATE,
	  AllocationEndDate DATE,
	  AllocatedUnits DECIMAL(18,10),
	  DaysLeft INT
	) 
	
	IF @StartDate IS NULL SET @StartDate = dbo.fnFirstDOM(getdate())
	IF @EndDate IS NULL SET @EndDate = dbo.fnLastDOM(@StartDate)	

 SELECT TicketCount = MAX(TicketCount), -- no need to sum in grouping since line items already aggregated totals 
		TotalGross = MAX(TotalGross), -- no need to sum in grouping since line items already aggregated totals 
 		DestinationID,
		Destination,
		CustomerID,
	    Customer,
	    ProductGroupID,
	    ProductGroup,
		DailyUnits = SUM(AllocatedUnits) / (DATEDIFF(DAY, MIN(qStart), MAX(qEnd)) + 1), -- Recalculate daily units since allocations may be weighted differently
		AllocationStartDate = MIN(qStart),
		AllocationEndDate = MAX(qEnd),
		AllocatedUnits = SUM(AllocatedUnits),
		DaysLeft = MAX(QualifiedDaysLeft)
   FROM (
		 SELECT *,
				AllocatedUnits = DailyUnits * (DATEDIFF(DAY, qStart, qEnd) + 1), -- broken down into daily average * number of days
 				QualifiedDaysLeft = CASE WHEN DATEDIFF(DAY, GETDATE(), qEnd) < 0 THEN 0 -- target date already past, set to zero
										 ELSE DATEDIFF(DAY, GETDATE(), qEnd) + 1 END -- include today
		   FROM (
		         -- Get orders for the given date range and merge with any allocations
				 -- Null allocations will be considered excess
				 SELECT TicketCount = COUNT(*),
						TotalGross = SUM(dbo.fnConvertUOM(COALESCE(NULLIF(DestGrossUnits,0), NULLIF(OriginGrossUnits,0), 0), DestUomID, @UomID)), 
						o.DestinationID,
						o.Destination,
						o.CustomerID,
						o.Customer,
						o.ProductGroupID,
						o.ProductGroup,
						Target = dbo.fnConvertUom(Units, ads.UomID, @UomID),
						DailyUnits = dbo.fnConvertUom(DailyUnits, ads.UomID, @UomID),
						qStart = CASE WHEN EffectiveDate > @StartDate THEN EffectiveDate -- get qualified start and end date
										 ELSE @StartDate END,
						qEnd = CASE WHEN EndDate < @EndDate THEN EndDate
										 ELSE @EndDate END
				   FROM viewOrder o 
 				   LEFT JOIN viewAllocationDestinationShipper ads ON o.DestinationID = ads.DestinationID
							AND o.CustomerID = ads.ShipperID
							AND o.ProductGroupID = ads.ProductGroupID
							AND (EffectiveDate BETWEEN @StartDate AND @EndDate OR EndDate BETWEEN @StartDate AND @EndDate)
 				  WHERE DeliverDate BETWEEN @StartDate AND @EndDate
					AND o.Rejected = 0 
					AND o.DeleteDateUTC IS NULL
					AND (@RegionID = -1 OR DestRegionID = @RegionID)
					AND (@TerminalID = -1 OR o.DestTerminalID = @TerminalID OR o.DestTerminalID IS NULL)
					AND (@DestinationID = -1 OR o.DestinationID = @DestinationID)
					AND (@ShipperID = -1 OR CustomerID = @ShipperID)
					AND (@ProductGroupID = -1 OR o.ProductGroupID = @ProductGroupID)
			   GROUP BY o.DestinationID, o.Destination, 
			            CustomerID, Customer, 
						o.ProductGroupID, o.ProductGroup,
						Units,
						ads.UomID, 
						DailyUnits,
						CASE WHEN EffectiveDate > @StartDate THEN EffectiveDate ELSE @StartDate END,
						CASE WHEN EndDate < @EndDate THEN EndDate ELSE @EndDate END
				 HAVING SUM(COALESCE(NULLIF(DestGrossUnits,0), NULLIF(OriginGrossUnits,0), 0)) > 0
				UNION
				-- Include any allocation with no delivered orders (or none yet)
				SELECT 0, 
						0, 
						DestinationID,
						Destination,
						ShipperID,
						Shipper,
						ProductGroupID,
						ProductGroup, 
						dbo.fnConvertUom(Units, ads.UomID, @UomID),
						dbo.fnConvertUom(ads.DailyUnits, ads.UomID, @UomID),
						qStart = CASE WHEN EffectiveDate > @StartDate THEN EffectiveDate -- get qualified start and end date
										ELSE @StartDate END,
						qEnd = CASE WHEN EndDate < @EndDate THEN EndDate
										ELSE @EndDate END
					FROM viewAllocationDestinationShipper ads
					WHERE (EffectiveDate BETWEEN @StartDate AND @EndDate OR EndDate BETWEEN @StartDate AND @EndDate)
					AND (@RegionID = -1 OR RegionID = @RegionID)
					AND (@TerminalID = -1 OR TerminalID = @TerminalID OR TerminalID IS NULL)
					AND (@ShipperID = -1 OR ShipperID = @ShipperID)
					AND (@ProductGroupID = -1 OR ProductGroupID = @ProductGroupID)
					AND (@DestinationID = -1 OR DestinationID = @DestinationID)
					AND NOT EXISTS (SELECT 1 FROM viewOrder o 
									WHERE DeliverDate BETWEEN @StartDate AND @EndDate
										AND o.DestinationID = ads.DestinationID
										AND o.CustomerID = ads.ShipperID
										AND o.ProductGroupID = ads.ProductGroupID)
				) q2
		) q
   GROUP BY DestinationID,
			Destination,
			CustomerID,
			Customer,
			ProductGroupID,
			ProductGroup
END
GO

/*************************************************************/
-- Date Created: 2016/10/14
-- Author: Joe Engler
-- Purpose: Send blast email to drivers based on filter criteria
-- Changes
--		4.5.0		2017/01/26		BSB			Added Terminal
/*************************************************************/
ALTER PROCEDURE spMessageDrivers(@Message VARCHAR(8000), @CarrierID INT = null, @DriverGroupID INT = null, @DriverID INT = null, @TerminalID INT = null, @RegionID INT = null, @StateID INT = null, @Username VARCHAR(100))
AS BEGIN

	INSERT INTO tblDispatchMessage
		SELECT NEWID(), null, @Username, cs.value, @Message, 0, GETUTCDATE()
		FROM viewDriverBase d 
		JOIN tblCACHE_DriverUserNames du ON du.DriverID = d.ID
		OUTER APPLY dbo.fnSplitString(du.UserNames, ',') cs
		WHERE (ISNULL(@CarrierID, 0) <= 0 OR d.CarrierID = @CarrierID)
		AND (ISNULL(@DriverGroupID, 0) <= 0 OR d.DriverGroupID = @DriverGroupID)
		AND (ISNULL(@DriverID, 0) <= 0 OR d.ID = @DriverID)
		AND (ISNULL(@TerminalID, 0) <= 0 OR d.TerminalID = @TerminalID)
		AND (ISNULL(@RegionID, 0) <= 0 OR d.RegionID = @RegionID)
		AND (ISNULL(@StateID, 0) <= 0 OR d.OperatingStateID = @StateID)
END
GO

/***********************************/
-- Date Created: 20 Sep 2013
-- Author: Kevin Alons
-- Purpose: return the orders be displayed on the OrderCreation page
-- Changes: 
--	4.5.0	- 2017/01/25	- BSB	- Added Terminal
/***********************************/
ALTER PROCEDURE spOrderCreationSource
(
  @TerminalID int
, @RegionID int
, @ProductID int = -1
) AS BEGIN
	SELECT cast(CASE WHEN StatusID=-10 THEN 0 ELSE 0 END as bit) AS Selected
		, isnull(OriginID, 0) AS OriginIDZ
		, * 
	FROM viewOrder 
	WHERE (@RegionID = -1
			OR OriginID IN (SELECT ID FROM tblOrigin WHERE RegionID = @RegionID) 
			OR DestinationID IN (SELECT ID FROM tblDestination WHERE RegionID = @RegionID)) 
	AND (@TerminalID = -1 /* No Terminal Filtering*/
		OR (OriginID IN (SELECT ID FROM tblOrigin WHERE TerminalID = @TerminalID OR TerminalID IS NULL)	/* All Valid Origins */	
			AND DestinationID IN (SELECT ID FROM tblDestination WHERE TerminalID = @TerminalID OR TerminalID IS NULL) /* All Valid Destinations */))
	AND (@ProductID = -1 OR ProductID = @ProductID)
	AND StatusID IN (-10, 9) 
	AND DeleteDateUTC IS NULL 
	ORDER BY PriorityNum, StatusID DESC, Origin, Destination, DueDate
END
GO

/***********************************************************/
-- Date Created: 4.3.0 - 2016.10.20 - Kevin Alons
-- Purpose: return all "potential" Driver Scheduling records for a date range and input criteria, in format for code-based PIVOTing
-- Changes:
-- 4.3.2	- 2016.11.05 - KDA	- add @DriverID parameter
-- 4.5.0	- 2017-01-26 - BSB	- Add @TerminalID parameter
/***********************************************************/
ALTER PROCEDURE spDriverSchedulePivotSource
(
  @Start date
, @End date
, @TerminalID int = -1
, @RegionID int = -1
, @CarrierID int = -1
, @DriverGroupID int = -1
, @DriverID int = -1
) AS
BEGIN
	SELECT DriverID, Terminal, Region, Carrier, DriverGroup, Driver, DriverShiftTypeID, DriverShiftStartDate
		, Heading = FORMAT(ScheduleDate, 'SDM_d_yy')
		, PivotValue = ltrim(Pos) + '|' + ltrim(StatusID) + '|' + Status + '|' + isnull(ltrim(StartHours), '') + '|' + isnull(ltrim(DurationHours), '') + '|' + isnull(ltrim(ManualAssignment), '0')
	FROM dbo.fnDriverSchedule(@Start, @End, @TerminalID, @RegionID, @CarrierID, @DriverGroupID, @DriverID)
	ORDER BY Carrier, Driver, ScheduleDate;
END
GO

/***********************************************************/
-- Date Created: 4.3.2 - 2016.11.05 - Kevin Alons
-- Purpose: return all "potential" Driver Scheduling Report records for a date range and input criteria, in format for code-based PIVOTing
-- Changes:
-- 4.5.0	- 2017-01-26 - BSB	- Add @TerminalID parameter
/***********************************************************/
ALTER PROCEDURE spDriverScheduleReportPivotSource
(
  @Start date
, @End date
, @TerminalID int = -1
, @RegionID int = -1
, @CarrierID int = -1
, @DriverGroupID int = -1
, @DriverID int = -1
) AS
BEGIN
	SELECT DS.DriverID, Terminal, Region, Carrier, DriverGroup, Driver, DriverShiftTypeID, DriverShiftStartDate, MobilePhone
		, TotalOpenCount = isnull(TC.TC, 0)
		, Heading = FORMAT(ScheduleDate, 'SDM_d_yy')
		, PivotStatus =  isnull(Status, '??') 
		, PivotStartHours = format(cast('2016/01/01 ' + ltrim(StartHours) + ':00' as datetime), 'h tt')
		, PivotDurationHours = DurationHours
		, PivotOrderCount = isnull(DC.DC, 0)
	FROM dbo.fnDriverSchedule(@Start, @End, @TerminalID, @RegionID, @CarrierID, @DriverGroupID, @DriverID) DS
	LEFT JOIN (SELECT DriverID, TC = count(*) FROM tblOrder WHERE StatusID IN (2, 7, 8) /* DISPATCHED,ACCEPTED,PICKED UP */ AND DeleteDateUTC IS NULL GROUP BY DriverID) TC ON TC.DriverID = DS.DriverID
	LEFT JOIN (SELECT DriverID, OrderDate, DC = count(*) FROM tblOrder WHERE StatusID IN (2, 7, 8) /* DISPATCHED,ACCEPTED,PICKED UP */ AND DeleteDateUTC IS NULL GROUP BY DriverID, OrderDate) DC ON DC.DriverID = DS.DriverID AND DC.OrderDate = DS.ScheduleDate
	ORDER BY Carrier, Driver, ScheduleDate;
END
GO


/*************************************************************
-- Date Created: 15 Jun 2015
-- Author: Kevin Alons
-- Purpose: return the data for the Order Approval page
-- Changes:
	- 3.9.2  - 2015/08/26 - KDA - remove obsolete performance optimization using OrderDateUTC to force use of index (now OrderDate is indexed)
	- 3.9.3  - 2015/08/29 - KDA - add new FinalXXX fields
	- 3.9.20 - 2015/10/26 - JAE - update stored procedure to save transfer override values
	- 3.9.37 - 2015/12/21 - JAE - Added carrier and shipper min settlement units
	- 4.4.1	 - 2016/11/04 - JAE	- Add Destination Chainup
	- 4.5.0  - 2017-02-02 - JAE - Add Terminal filter
*************************************************************/
ALTER PROCEDURE spOrderApprovalSource
(
  @userName VARCHAR(100)
, @ShipperID INT = NULL
, @CarrierID INT = NULL
, @ProductGroupID INT = NULL
, @OriginStateID INT = NULL
, @DestStateID INT = NULL
, @TerminalID INT = NULL
, @Start DATE = NULL
, @End DATE = NULL
, @IncludeApproved BIT = 0
) AS BEGIN
	SET NOCOUNT ON;
	
	SELECT *
		, FinalActualMiles = ISNULL(OverrideActualMiles, ActualMiles)
		, FinalOriginMinutes = ISNULL(OverrideOriginMinutes, OriginMinutes)
		, FinalDestMinutes = ISNULL(OverrideDestMinutes, DestMinutes)
		, FinalOriginChainup = CAST(ISNULL(1-NULLIF(OverrideOriginChainup, 0), OriginChainup) AS BIT)
		, FinalDestChainup = CAST(ISNULL(1-NULLIF(OverrideDestChainup, 0), DestChainup) AS BIT)
		, FinalH2S = CAST(ISNULL(1-NULLIF(OverrideH2S, 0), H2S) AS BIT)
		, FinalRerouted = CAST(ISNULL(1-NULLIF(OverrideReroute, 0), Rerouted) AS BIT)
		, FinalTransferPercentComplete = ISNULL(OverrideTransferPercentComplete, TransferPercentComplete)
		, FinalCarrierMinSettlementUnits = ISNULL(OverrideCarrierMinSettlementUnits, CarrierMinSettlementUnits)
		, FinalShipperMinSettlementUnits = ISNULL(OverrideShipperMinSettlementUnits, ShipperMinSettlementUnits)
	INTO #X
	FROM viewOrderApprovalSource
	WHERE (NULLIF(@ShipperID, -1) IS NULL OR ShipperID = @ShipperID)
	  AND (NULLIF(@CarrierID, -1) IS NULL OR CarrierID = @CarrierID)
	  AND (NULLIF(@TerminalID, -1) IS NULL 
			OR (    (@TerminalID = OriginTerminalID OR OriginTerminalID IS NULL)
			    AND (@TerminalID = DestTerminalID OR DestTerminalID IS NULL)
				AND (@TerminalID = DriverTerminalID OR DriverTerminalID IS NULL)))
	  AND (NULLIF(@ProductGroupID, -1) IS NULL OR ProductGroupID = @ProductGroupID)
	  AND (NULLIF(@OriginStateID, -1) IS NULL OR OriginStateID = @OriginStateID)
	  AND (NULLIF(@DestStateID, -1) IS NULL OR DestStateID = @DestStateID)
	  AND OrderDate BETWEEN ISNULL(@Start, '1/1/1900') AND ISNULL(@End, '1/1/2200')
	  AND (@IncludeApproved = 1 OR ISNULL(Approved, 0) = 0) 
	  
	DECLARE @id int
	WHILE EXISTS (SELECT * FROM #x WHERE OrderID IS NULL)
	BEGIN
		SELECT @id = min(ID) FROM #x WHERE OrderID IS NULL
		EXEC spApplyRatesBoth @id, @userName
		UPDATE #x SET OrderID = @id, Approved = (SELECT Approved FROM tblOrderApproval WHERE OrderID = @id) WHERE ID = @id
	END
	ALTER TABLE #X DROP COLUMN OrderID
	
	SELECT * FROM #x WHERE (@IncludeApproved = 1 OR ISNULL(Approved, 0) = 0) 

END


GO

/******************
REPORT CENTER FIELDS
******************/
GO

SET IDENTITY_INSERT tblReportColumnDefinition ON
INSERT INTO tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport, IsTicketField)
  SELECT 363, 1, 'OriginTerminal', 'ORIGIN | GENERAL | Origin Terminal', NULL, 'OriginTerminalID', 2, 'SELECT ID, Name FROM tblTerminal ORDER BY Name', 0, '*', 0, 0
  UNION
  SELECT 364, 1, 'DestTerminal', 'DESTINATION | GENERAL | Destination Terminal', NULL, 'DestTerminalID', 2, 'SELECT ID, Name FROM tblTerminal ORDER BY Name', 0, '*', 0, 0
  UNION
  SELECT 365, 1, 'OriginDriverTerminal', 'GENERAL | Driver Terminal', NULL, 'OriginDriverTerminalID', 2, 'SELECT ID, Name FROM tblTerminal ORDER BY Name', 0, '*', 0, 0
  UNION
  SELECT 366, 1, 'DestDriverTerminal', 'TRIP | TRANSFER | Transfer Driver Terminal', NULL, 'DestDriverTerminalID', 2, 'SELECT ID, Name FROM tblTerminal ORDER BY Name', 0, '*', 0, 0
  UNION
  SELECT 367, 1, 'OriginTruckTerminal', 'GENERAL | Truck Terminal', NULL, 'OriginTruckTerminalID', 2, 'SELECT ID, Name FROM tblTerminal ORDER BY Name', 0, '*', 0, 0
  UNION
  SELECT 368, 1, 'DestTruckTerminal', 'TRIP | TRANSFER | Transfer Truck Terminal', NULL, 'DestTruckTerminalID', 2, 'SELECT ID, Name FROM tblTerminal ORDER BY Name', 0, '*', 0, 0
  UNION
  SELECT 369, 1, 'TrailerTerminal', 'GENERAL | Trailer 1 Terminal', NULL, 'TrailerTerminalID', 2, 'SELECT ID, Name FROM tblTerminal ORDER BY Name', 0, '*', 0, 0
  UNION
  SELECT 370, 1, 'Trailer2Terminal', 'GENERAL | Trailer 2 Terminal', NULL, 'Trailer2TerminalID', 2, 'SELECT ID, Name FROM tblTerminal ORDER BY Name', 0, '*', 0, 0
  EXCEPT SELECT * FROM tblReportColumnDefinition
SET IDENTITY_INSERT tblReportColumnDefinition OFF
GO

-- Add terminal columns to base filtering
INSERT INTO tblReportColumnDefinitionBaseFilter
  SELECT 6, 363, 2, 'TerminalID', '(@TerminalID = -1 OR ID = @TerminalID OR ID IS NULL)', '@TerminalID' -- Origin Terminal
  UNION
  SELECT 7, 364, 2, 'TerminalID', '(@TerminalID = -1 OR ID = @TerminalID OR ID IS NULL)', '@TerminalID' -- Dest Terminal
  UNION
  SELECT 8, 365, 2, 'TerminalID', '(@TerminalID = -1 OR ID = @TerminalID OR ID IS NULL)', '@TerminalID' -- Driver Terminal
  UNION
  SELECT 9, 366, 2, 'TerminalID', '(@TerminalID = -1 OR ID = @TerminalID OR ID IS NULL)', '@TerminalID' -- Transfer Driver Terminal
  EXCEPT SELECT * FROM tblReportColumnDefinitionBaseFilter
GO




/******************
REFRESH EVERYTHING
******************/
GO

EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRecompileAllStoredProcedures
GO
EXEC _spRebuildAllObjects
GO
EXEC _spRebuildAllObjects
GO

COMMIT
SET NOEXEC OFF