-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.6.21'
SELECT  @NewVersion = '3.6.22'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'Report Center: add Excel:Summary Category grouping function'
	UNION SELECT @NewVersion, 0, 'Report Center: fix bug preventing (Blank) column with grouping function'
	UNION SELECT @NewVersion, 1, 'Report Center: enable non-global report sharing between multiple users'
	UNION SELECT @NewVersion, 0, 'Report Center: fix Status filtering bug that wasn''t using PrintStatusID'
	UNION SELECT @NewVersion, 1, 'Report Center: add Date Month "Virtual" report column'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

DELETE FROM tblReportGroupingFunction WHERE ID BETWEEN 2 AND 9
GO

INSERT INTO tblReportGroupingFunction (ID, Name) VALUES (3, 'Summary Category')
GO

ALTER TABLE tblUserReportDefinition ADD UserNames varchar(max) NULL
GO
UPDATE tblUserReportDefinition SET UserNames = UserName
GO
DROP INDEX udxUserReportDefinition_UserName_Name ON tblUserReportDefinition
GO

ALTER TABLE tblUserReportDefinition DROP COLUMN UserName
GO

/*******************************************/
-- Date Created: 23 Jul 2014
-- Author: Kevin Alons
-- Purpose: clone an existing UserReport (defaults to a full clone with columns, but can do an AddNew otherwise)
/*******************************************/
ALTER PROCEDURE spCloneUserReport
(
  @userReportID int
, @reportName varchar(50)
, @userNames varchar(max)
, @includeColumns bit = 1
, @newID int = 0 OUTPUT 
) AS
BEGIN
	SET NOCOUNT ON
	SET @userNames = REPLACE(@userNames, ' ', '')
	INSERT INTO tblUserReportDefinition (Name, ReportID, UserNames, CreatedByUser)
		SELECT @reportName, ReportID, @userNames, CASE WHEN @userNames IS NULL OR @userNames LIKE '%,%' THEN 'Administrator' ELSE @userNames END
		FROM tblUserReportDefinition WHERE ID = @userReportID
	SET @newID = SCOPE_IDENTITY()
	IF (@includeColumns = 1)
	BEGIN
		INSERT INTO tblUserReportColumnDefinition (UserReportID, ReportColumnID, Caption
			, SortNum, DataSort, GroupingFunctionID, DataFormat, FilterOperatorID, FilterValue1, FilterValue2
			, CreatedByUser)
			SELECT @newID, ReportColumnID, Caption
				, SortNum, DataSort, GroupingFunctionID, DataFormat, FilterOperatorID, FilterValue1, FilterValue2
				, (SELECT CreatedByUser FROM tblUserReportDefinition WHERE ID = @newID)
			FROM tblUserReportColumnDefinition WHERE UserReportID = @userReportID
	END
	
	SELECT [NewID]=@newID
	RETURN (isnull(@newID, 0))
END

GO

/*************************************************/
-- Date Created: 26 Jul 2014
-- Author: Kevin Alons
-- Purpose: return the tblUserReportDefinition along with any any relevant tblReportDefinition fields
/*************************************************/
ALTER VIEW [dbo].[viewUserReportDefinition] AS
	SELECT URD.*
		, UserEditable = cast(CASE WHEN isnull(URD.UserNames, ',') LIKE '%,%' THEN 0 ELSE 1 END as bit)
		, RD.SourceTable
	FROM tblUserReportDefinition URD
	JOIN tblReportDefinition RD ON RD.ID = URD.ReportID
GO

EXEC _spRebuildAllObjects
GO

/* Report Center: use PrintStatusID for filtering of statuses - not the StatusID field - which doesn't account for PrintStatus */
UPDATE tblReportColumnDefinition SET FilterDataField = 'PrintStatusID' WHERE ID = 12
GO

UPDATE tblReportColumnDefinition SET OrderSingleExport = 0 WHERE ID IN (125, 126, 127)
GO

SET IDENTITY_INSERT tblReportColumnDefinition ON
INSERT INTO tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport)
	SELECT 90009, ReportID, 'cast(LTRIM(month(OrderDate)) + ''/1/'' + LTRIM(year(OrderDate)) as date)', 'TRIP | TIMESTAMPS | Date Month', DataFormat, NULL, 0, NULL, FilterAllowCustomText, AllowedRoles, OrderSingleExport
	FROM tblReportColumnDefinition
	WHERE ID = 4
SET IDENTITY_INSERT tblReportColumnDefinition OFF
GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Report Center Order data
/***********************************/
ALTER VIEW [dbo].[viewReportCenter_Orders] AS
	SELECT *
		-- OFR = OrderFirstRecord, which is used for the OrderSingleExport ReportCenter column attribute
		, OFR = ROW_NUMBER() OVER (PARTITION BY OrderNum ORDER BY OrderNum, T_CarrierTicketNum)
	FROM (
		SELECT O.*
			, ShipperBatchNum = SS.BatchNum
			, ShipperBatchInvoiceNum = SSB.InvoiceNum
			, ShipperSettlementUomID = SS.SettlementUomID
			, ShipperSettlementUom = SS.SettlementUom
			, ShipperMinSettlementUnits = SS.MinSettlementUnits
			, ShipperSettlementUnits = SS.SettlementUnits
			, ShipperRateSheetRate = SS.RateSheetRate
			, ShipperRateSheetRateType = SS.RateSheetRateType
			, ShipperRouteRate = SS.RouteRate
			, ShipperRouteRateType = SS.RouteRateType
			, ShipperLoadRate = isnull(SS.RouteRate, SS.RateSheetRate)
			, ShipperLoadRateType = isnull(SS.RouteRateType, SS.RateSheetRateType)
			, ShipperLoadAmount = SS.LoadAmount
			, ShipperOrderRejectRate = SS.OrderRejectRate -- changed
			, ShipperOrderRejectRateType = SS.OrderRejectRateType -- new
			, ShipperOrderRejectAmount = SS.OrderRejectAmount -- changed
			, ShipperWaitFeeSubUnit = SS.WaitFeeSubUnit  -- new
			, ShipperWaitFeeRoundingType = SS.WaitFeeRoundingType  -- new
			, ShipperOriginWaitBillableHours = SS.OriginWaitBillableHours  -- new
			, ShipperOriginWaitBillableMinutes = SS.OriginWaitBillableMinutes  -- new
			, ShipperOriginWaitRate = SS.OriginWaitRate
			, ShipperOriginWaitAmount = SS.OriginWaitAmount
			, ShipperDestinationWaitBillableHours = SS.DestinationWaitBillableHours  -- new
			, ShipperDestinationWaitBillableMinutes = SS.DestinationWaitBillableMinutes   -- new
			, ShipperDestinationWaitRate = SS.DestinationWaitRate  -- changed
			, ShipperDestinationWaitAmount = SS.DestinationWaitAmount  -- changed
			, ShipperTotalWaitAmount = SS.TotalWaitAmount
			, ShipperTotalWaitBillableMinutes = nullif(ISNULL(SS.OriginWaitBillableMinutes, 0) + ISNULL(SS.DestinationWaitBillableMinutes, 0), 0)
			, ShipperTotalWaitBillableHours = nullif(ISNULL(SS.OriginWaitBillableHours, 0) + ISNULL(SS.DestinationWaitBillableHours, 0), 0)
			
			, ShipperFuelSurchargeRate = SS.FuelSurchargeRate
			, ShipperFuelSurchargeAmount = SS.FuelSurchargeAmount

			, ShipperChainupRate = SS.ChainupRate
			, ShipperChainupRateType = SS.ChainupRateType  -- new
			, ShipperChainupAmount = SS.ChainupAmount
			, ShipperRerouteRate = SS.RerouteRate
			, ShipperRerouteRateType = SS.RerouteRateType  -- new
			, ShipperRerouteAmount = SS.RerouteAmount
			, ShipperSplitLoadRate = SS.SplitLoadRate
			, ShipperSplitLoadRateType = SS.SplitLoadRateType  -- new
			, ShipperSplitLoadAmount = SS.SplitLoadAmount
			, ShipperH2SRate = SS.H2SRate
			, ShipperH2SRateType = SS.H2SRateType  -- new
			, ShipperH2SAmount = SS.H2SAmount

			, ShipperTaxRate = SS.OriginTaxRate
			, ShipperTotalAmount = SS.TotalAmount

			, CarrierBatchNum = SC.BatchNum
			, CarrierSettlementUomID = SC.SettlementUomID
			, CarrierSettlementUom = SC.SettlementUom
			, CarrierMinSettlementUnits = SC.MinSettlementUnits
			, CarrierSettlementUnits = SC.SettlementUnits
			, CarrierRateSheetRate = SC.RateSheetRate
			, CarrierRateSheetRateType = SC.RateSheetRateType
			, CarrierRouteRate = SC.RouteRate
			, CarrierRouteRateType = SC.RouteRateType
			, CarrierLoadRate = isnull(SC.RouteRate, SC.RateSheetRate)
			, CarrierLoadRateType = isnull(SC.RouteRateType, SC.RateSheetRateType)
			, CarrierLoadAmount = SC.LoadAmount
			, CarrierOrderRejectRate = SC.OrderRejectRate -- changed
			, CarrierOrderRejectRateType = SC.OrderRejectRateType -- new
			, CarrierOrderRejectAmount = SC.OrderRejectAmount -- changed
			, CarrierWaitFeeSubUnit = SC.WaitFeeSubUnit  -- new
			, CarrierWaitFeeRoundingType = SC.WaitFeeRoundingType  -- new
			, CarrierOriginWaitBillableHours = SS.OriginWaitBillableHours  -- new
			, CarrierOriginWaitBillableMinutes = SC.OriginWaitBillableMinutes  -- new
			, CarrierOriginWaitRate = SC.OriginWaitRate
			, CarrierOriginWaitAmount = SC.OriginWaitAmount
			, CarrierDestinationWaitBillableHours = SS.DestinationWaitBillableHours  -- new
			, CarrierDestinationWaitBillableMinutes = SC.DestinationWaitBillableMinutes  -- new
			, CarrierDestinationWaitRate = SC.DestinationWaitRate -- changed
			, CarrierDestinationWaitAmount = SC.DestinationWaitAmount  -- changed
			, CarrierTotalWaitAmount = SC.TotalWaitAmount
			, CarrierTotalWaitBillableMinutes = nullif(ISNULL(SS.OriginWaitBillableMinutes, 0) + ISNULL(SS.DestinationWaitBillableMinutes, 0), 0)
			, CarrierTotalWaitBillableHours = nullif(ISNULL(SS.OriginWaitBillableHours, 0) + ISNULL(SS.DestinationWaitBillableHours, 0), 0)
			
			, CarrierFuelSurchargeRate = SC.FuelSurchargeRate
			, CarrierFuelSurchargeAmount = SC.FuelSurchargeAmount

			, CarrierChainupRate = SC.ChainupRate
			, CarrierChainupRateType = SC.ChainupRateType  -- new
			, CarrierChainupAmount = SC.ChainupAmount
			, CarrierRerouteRate = SC.RerouteRate
			, CarrierRerouteRateType = SC.RerouteRateType  -- new
			, CarrierRerouteAmount = SC.RerouteAmount
			, CarrierSplitLoadRate = SC.SplitLoadRate
			, CarrierSplitLoadRateType = SC.SplitLoadRateType  -- new
			, CarrierSplitLoadAmount = SC.SplitLoadAmount
			, CarrierH2SRate = SC.H2SRate
			, CarrierH2SRateType = SC.H2SRateType  -- new
			, CarrierH2SAmount = SC.H2SAmount

			, CarrierTaxRate = SC.OriginTaxRate
			, CarrierTotalAmount = SC.TotalAmount

			, OriginGpsLatLon = ltrim(DLO.Lat) + ',' + ltrim(DLO.Lon)
			, OriginLatLon = ltrim(OO.LAT) + ',' + ltrim(OO.LON)
			, OriginAccuracyMeters = DLO.SourceAccuracyMeters
			, OriginDistance = isnull(cast(DLO.DistanceToPoint as int), 99999)
			, OriginGpsArrived = CASE WHEN DLO.DistanceToPoint IS NOT NULL AND DLO.DistanceToPoint BETWEEN 0 AND OO.GeoFenceRadiusMeters THEN 1 ELSE 0 END
			, DestGpsLatLon = ltrim(DLD.Lat) + ',' + ltrim(DLD.Lon)
			, DestLatLon = ltrim(D.LAT) + ',' + ltrim(D.LON)
			, DestAccuracyMeters = DLD.SourceAccuracyMeters
			, DestDistance = isnull(cast(DLD.DistanceToPoint as int), 99999)
			, DestGpsArrived = CASE WHEN DLD.DistanceToPoint IS NOT NULL AND DLD.DistanceToPoint BETWEEN 0 AND D.GeoFenceRadiusMeters THEN 1 ELSE 0 END
			, ShipperDestCode = CDC.Code
			, OriginCTBNum = OO.CTBNum
		FROM viewOrder_OrderTicket_Full O
		JOIN tblOrigin OO ON OO.ID = O.OriginID
		JOIN tblDestination D ON D.ID = O.DestinationID
		LEFT JOIN tblCustomerDestinationCode CDC ON CDC.CustomerID = O.CustomerID AND CDC.DestinationID = O.DestinationID
		LEFT JOIN viewDriverLocation_OriginFirstArrive DLO ON DLO.OrderID = O.ID AND DLO.OriginID = O.OriginID
		LEFT JOIN viewDriverLocation_DestinationFirstArrive DLD ON DLD.OrderID = O.ID AND DLD.DestinationID = O.DestinationID
		LEFT JOIN viewOrderSettlementCarrier SC ON SC.OrderID = O.ID
		LEFT JOIN viewOrderSettlementShipper SS ON SS.OrderID = O.ID
		LEFT JOIN tblShipperSettlementBatch SSB ON SSB.ID = SS.BatchID
	) X

GO

COMMIT
SET NOEXEC OFF