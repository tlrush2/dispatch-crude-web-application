-- rollback
-- select value from tblsetting where id = 0
/*
	-- fixes to ReportCenter profile filtering
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.1.15'
SELECT  @NewVersion = '3.1.16'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

DELETE FROM tblReportColumnDefinitionBaseFilter WHERE ID NOT IN (1,4)

INSERT INTO tblReportColumnDefinitionBaseFilter (ID, ReportColumnID, BaseFilterTypeID, ProfileField, IncludeWhereClause, IncludeWhereClause_ProfileTemplate)
	SELECT 5, 42, 2, 'ProducerID', '(@ProducerID = -1 OR ProducerID = @ProducerID)', '@ProducerID'
	
COMMIT
SET NOEXEC OFF

select * from tblReportColumnDefinition