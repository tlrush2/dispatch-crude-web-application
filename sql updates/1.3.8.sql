DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '1.3.7', @NewVersion = '1.3.8'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON

ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_Operator
GO
ALTER TABLE dbo.tblOperator SET (LOCK_ESCALATION = TABLE)
GO

ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_Destination
GO
ALTER TABLE dbo.tblDestination SET (LOCK_ESCALATION = TABLE)
GO

ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_Customer
GO
ALTER TABLE dbo.tblCustomer SET (LOCK_ESCALATION = TABLE)
GO

ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_tblOrder_tblProducer
GO
ALTER TABLE dbo.tblProducer SET (LOCK_ESCALATION = TABLE)
GO

ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_Carrier
GO
ALTER TABLE dbo.tblCarrier SET (LOCK_ESCALATION = TABLE)
GO

ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_Origin
GO
ALTER TABLE dbo.tblOrigin SET (LOCK_ESCALATION = TABLE)
GO

ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_Truck
GO
ALTER TABLE dbo.tblTruck SET (LOCK_ESCALATION = TABLE)
GO

ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_Trailer
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_Trailer2
GO
ALTER TABLE dbo.tblTrailer SET (LOCK_ESCALATION = TABLE)
GO

ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_TicketType
GO
ALTER TABLE dbo.tblTicketType SET (LOCK_ESCALATION = TABLE)
GO

ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_Route
GO
ALTER TABLE dbo.tblRoute SET (LOCK_ESCALATION = TABLE)
GO

ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_Driver
GO
ALTER TABLE dbo.tblDriver SET (LOCK_ESCALATION = TABLE)
GO

ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_Pumper
GO
ALTER TABLE dbo.tblPumper SET (LOCK_ESCALATION = TABLE)
GO

ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_Priority
GO
ALTER TABLE dbo.tblPriority SET (LOCK_ESCALATION = TABLE)
GO

ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_OrderStatus
GO
ALTER TABLE dbo.tblOrderStatus SET (LOCK_ESCALATION = TABLE)
GO

ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT DF_tblOrders_StatusID
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT DF_tblOrder__TicketTicketType
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT DF_tblOrder_Rejected
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT DF_tblTransactions_ChainUp
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT DF_tblOrder_CreateDate
GO
CREATE TABLE dbo.Tmp_tblOrder
	(
	ID int NOT NULL IDENTITY (1, 1),
	OrderNum int NULL,
	StatusID int NOT NULL,
	PriorityID tinyint NULL,
	DueDate date NOT NULL,
	RouteID int NULL,
	OriginID int NULL,
	OriginArriveTime datetime NULL,
	OriginDepartTime datetime NULL,
	OriginMinutes int NULL,
	OriginWaitNotes varchar(255) NULL,
	OriginBOLNum varchar(15) NULL,
	OriginGrossBarrels decimal(9, 3) NULL,
	OriginNetBarrels decimal(9, 3) NULL,
	DestinationID int NULL,
	DestArriveTime datetime NULL,
	DestDepartTime datetime NULL,
	DestMinutes int NULL,
	DestWaitNotes varchar(255) NULL,
	DestBOLNum varchar(15) NULL,
	DestGrossBarrels decimal(9, 3) NULL,
	DestNetBarrels decimal(9, 3) NULL,
	CustomerID int NOT NULL,
	CarrierID int NULL,
	DriverID int NULL,
	TruckID int NULL,
	TrailerID int NULL,
	Trailer2ID int NULL,
	OperatorID int NULL,
	PumperID int NULL,
	TicketTypeID int NOT NULL,
	Rejected bit NOT NULL,
	RejectNotes varchar(255) NULL,
	ChainUp bit NOT NULL,
	OriginTruckMileage int NULL,
	OriginTankNum varchar(20) NULL,
	DestTruckMileage int NULL,
	CarrierTicketNum varchar(15) NULL,
	AuditNotes varchar(255) NULL,
	CreateDate smalldatetime NULL,
	ActualMiles int NULL,
	ProducerID int NULL,
	CarrierRateMiles int NULL,
	CustomerRateMiles int NULL,
	CreatedByUser varchar(100) NULL,
	LastChangeDate smalldatetime NULL,
	LastChangedByUser varchar(100) NULL,
	DeleteDate smalldatetime NULL,
	DeletedByUser varchar(100) NULL,
	DestProductBSW decimal(9, 3) NULL,
	DestProductGravity decimal(9, 3) NULL,
	DestProductTemp decimal(9, 3) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_tblOrder SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_tblOrder ADD CONSTRAINT
	DF_tblOrders_StatusID DEFAULT ((1)) FOR StatusID
GO
ALTER TABLE dbo.Tmp_tblOrder ADD CONSTRAINT
	DF_tblOrder__TicketTicketType DEFAULT ((1)) FOR TicketTypeID
GO
ALTER TABLE dbo.Tmp_tblOrder ADD CONSTRAINT
	DF_tblOrder_Rejected DEFAULT ((0)) FOR Rejected
GO
ALTER TABLE dbo.Tmp_tblOrder ADD CONSTRAINT
	DF_tblTransactions_ChainUp DEFAULT ((0)) FOR ChainUp
GO
ALTER TABLE dbo.Tmp_tblOrder ADD CONSTRAINT
	DF_tblOrder_CreateDate DEFAULT (getdate()) FOR CreateDate
GO
SET IDENTITY_INSERT dbo.Tmp_tblOrder ON
GO
IF EXISTS(SELECT * FROM dbo.tblOrder)
	 EXEC('INSERT INTO dbo.Tmp_tblOrder (ID, OrderNum, StatusID, PriorityID, DueDate, RouteID, OriginID, OriginArriveTime, OriginDepartTime, OriginMinutes, OriginWaitNotes, OriginBOLNum, OriginGrossBarrels, OriginNetBarrels, DestinationID, DestArriveTime, DestDepartTime, DestMinutes, DestWaitNotes, DestBOLNum, DestGrossBarrels, DestNetBarrels, CustomerID, CarrierID, DriverID, TruckID, TrailerID, Trailer2ID, OperatorID, PumperID, TicketTypeID, Rejected, RejectNotes, ChainUp, OriginTruckMileage, OriginTankNum, DestTruckMileage, CarrierTicketNum, AuditNotes, CreateDate, ActualMiles, ProducerID, CarrierRateMiles, CustomerRateMiles, CreatedByUser, LastChangeDate, LastChangedByUser, DeleteDate, DeletedByUser, DestProductBSW, DestProductGravity, DestProductTemp)
		SELECT ID, OrderNum, StatusID, PriorityID, DueDate, RouteID, OriginID, OriginArriveTime, OriginDepartTime, OriginMinutes, OriginWaitNotes, OriginBOLNum, OriginGrossBarrels, OriginNetBarrels, DestinationID, DestArriveTime, DestDepartTime, DestMinutes, DestWaitNotes, DestBOLNum, DestGrossBarrels, DestNetBarrels, CustomerID, CarrierID, DriverID, TruckID, TrailerID, Trailer2ID, OperatorID, PumperID, TicketTypeID, Rejected, RejectNotes, ChainUp, OriginTruckMileage, OriginTankNum, DestTruckMileage, CarrierTicketNum, AuditNotes, CreateDate, ActualMiles, ProducerID, CarrierRateMiles, CustomerRateMiles, CreatedByUser, LastChangeDate, LastChangedByUser, DeleteDate, DeletedByUser, DestProductBSW, DestProductGravity, DestProductTemp FROM dbo.tblOrder WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_tblOrder OFF
GO
ALTER TABLE dbo.tblOrderReroute
	DROP CONSTRAINT FK_OrderReroute_Order
GO
ALTER TABLE dbo.tblOrderTicket
	DROP CONSTRAINT FK_OrderTicket_Order
GO
ALTER TABLE dbo.tblOrderInvoiceCustomer
	DROP CONSTRAINT FK_tblOrderInvoiceCustomer_tblOrder
GO
ALTER TABLE dbo.tblOrderInvoiceCarrier
	DROP CONSTRAINT FK_tblOrderInvoiceCarrier_tblOrder
GO
DROP TABLE dbo.tblOrder
GO
EXECUTE sp_rename N'dbo.Tmp_tblOrder', N'tblOrder', 'OBJECT' 
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	PK_Order PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE UNIQUE NONCLUSTERED INDEX udxOrder_OrderNum ON dbo.tblOrder
	(
	OrderNum
	) INCLUDE (ID) 
 WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxOrder_StatusID ON dbo.tblOrder
	(
	StatusID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxOrder_PriorityID ON dbo.tblOrder
	(
	PriorityID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxOrder_CarrierID ON dbo.tblOrder
	(
	CarrierID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxOrder_OriginID ON dbo.tblOrder
	(
	OriginID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxOrder_DestinationID ON dbo.tblOrder
	(
	DestinationID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxOrder_RouteID ON dbo.tblOrder
	(
	RouteID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxOrder_OriginDepartTime ON dbo.tblOrder
	(
	OriginDepartTime
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxOrder_DeleteDate_StatusID_Covering ON dbo.tblOrder
	(
	DeleteDate,
	StatusID
	) INCLUDE (ID, OrderNum, PriorityID, DueDate, RouteID, OriginID, OriginArriveTime, OriginDepartTime, OriginMinutes, OriginWaitNotes, OriginBOLNum, OriginGrossBarrels, OriginNetBarrels, DestinationID, DestArriveTime, DestDepartTime, DestMinutes, DestWaitNotes, DestBOLNum, DestGrossBarrels, DestNetBarrels, CustomerID, CarrierID, DriverID, TruckID, TrailerID, Trailer2ID, OperatorID, PumperID, TicketTypeID, Rejected, RejectNotes, ChainUp, OriginTruckMileage, OriginTankNum, DestTruckMileage, CarrierTicketNum, AuditNotes, CreateDate, ActualMiles, ProducerID, CarrierRateMiles, CustomerRateMiles, CreatedByUser, LastChangeDate, LastChangedByUser, DeletedByUser, DestProductBSW, DestProductGravity, DestProductTemp) 
 WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_OrderStatus FOREIGN KEY
	(
	StatusID
	) REFERENCES dbo.tblOrderStatus
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_Priority FOREIGN KEY
	(
	PriorityID
	) REFERENCES dbo.tblPriority
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  SET NULL 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_Pumper FOREIGN KEY
	(
	PumperID
	) REFERENCES dbo.tblPumper
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  SET NULL 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_Driver FOREIGN KEY
	(
	DriverID
	) REFERENCES dbo.tblDriver
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_Route FOREIGN KEY
	(
	RouteID
	) REFERENCES dbo.tblRoute
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_TicketType FOREIGN KEY
	(
	TicketTypeID
	) REFERENCES dbo.tblTicketType
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_Trailer FOREIGN KEY
	(
	TrailerID
	) REFERENCES dbo.tblTrailer
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  SET NULL 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_Trailer2 FOREIGN KEY
	(
	Trailer2ID
	) REFERENCES dbo.tblTrailer
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_Truck FOREIGN KEY
	(
	TruckID
	) REFERENCES dbo.tblTruck
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  SET NULL 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_Origin FOREIGN KEY
	(
	OriginID
	) REFERENCES dbo.tblOrigin
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_Carrier FOREIGN KEY
	(
	CarrierID
	) REFERENCES dbo.tblCarrier
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_tblOrder_tblProducer FOREIGN KEY
	(
	ProducerID
	) REFERENCES dbo.tblProducer
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_Customer FOREIGN KEY
	(
	CustomerID
	) REFERENCES dbo.tblCustomer
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_Destination FOREIGN KEY
	(
	DestinationID
	) REFERENCES dbo.tblDestination
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_Operator FOREIGN KEY
	(
	OperatorID
	) REFERENCES dbo.tblOperator
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  SET NULL 
	
GO
-- =============================================
-- Author:		Kevin Alons
-- Create date: 13 Dec 2012
-- Description:	ensure that only "Assigned|Dispatched|Declined|Generated" orders can be deleted (marked deleted)
-- =============================================
CREATE TRIGGER [dbo].[trigOrder_IOD] ON dbo.tblOrder INSTEAD OF DELETE AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	IF (SELECT count(*) FROM deleted d where StatusID NOT IN (-10, 1, 2, 9)) > 0
	BEGIN
		RAISERROR('Order could not be deleted - it has a status other than "Generated, Assigned, Dispatched or Declined"', 16, 1)
		RETURN
	END
	ELSE
		UPDATE tblOrder SET DeleteDate = GETDATE(), DeletedByUser = 'System' WHERE ID IN (SELECT ID FROM deleted)
		-- do CASCADING delete here (can't use FK cascading delete because that prevents a IOD trigger on OT table)
		DELETE FROM tblOrderTicket WHERE OrderID IN (SELECT ID FROM deleted)
		--DELETE FROM tblOrder WHERE ID IN (SELECT ID FROM deleted)
END
GO
-- =============================================
-- Author:		Kevin Alons
-- Create date: 19 Dec 2012
-- Description:	trigger to add a unique, incrementing OrderNum to each new Order (manual Identity column)
-- =============================================
CREATE TRIGGER [dbo].[trigOrder_IU] ON dbo.tblOrder AFTER INSERT, UPDATE AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	WHILE (SELECT count(*) FROM inserted i JOIN tblOrder O ON O.ID = i.ID WHERE O.OrderNum IS NULL) > 0
	BEGIN
		UPDATE tblOrder 
		  SET OrderNum = isnull((SELECT max(OrderNum) FROM tblOrder), 100000) + 1
			, CreateDate = getdate()
		WHERE ID = (SELECT min(O.ID) FROM tblOrder O JOIN inserted i ON i.ID = O.ID WHERE O.OrderNum IS NULL)
	END
	-- re-compute the OriginMinutes (in case the website failed to compute it properly)
	IF UPDATE(OriginArriveTime) OR UPDATE(OriginDepartTime)
	BEGIN
		UPDATE tblOrder SET OriginMinutes = datediff(minute, OriginArriveTime, OriginDepartTime)
	END
	-- re-compute the DestWaitMinutes (in case the website failed to compute it properly)
	IF UPDATE(DestArriveTime) OR UPDATE(DestDepartTime)
	BEGIN
		UPDATE tblOrder SET DestMinutes = datediff(minute, DestArriveTime, DestDepartTime)
	END
	
	-- ensure missing Routes are created and assigned for the specified Origin/Destination combination
	IF UPDATE(OriginID) OR UPDATE(DestinationID)
	BEGIN
		-- create any missing Route records
		INSERT INTO tblRoute (OriginID, DestinationID, CreateDate, CreatedByUser)
			SELECT i.OriginID, i.DestinationID, GETDATE(), ISNULL(i.LastChangedByUser, i.CreatedByUser)
			FROM inserted i
			LEFT JOIN tblRoute R ON R.OriginID = i.OriginID AND R.DestinationID = i.DestinationID
			WHERE R.ID IS NULL
		-- ensure the Order records refer to the correct Route (ID)
		UPDATE tblOrder SET RouteID = R.ID
		FROM tblOrder O
		JOIN inserted i ON i.ID = O.ID
		JOIN tblRoute R ON R.OriginID = O.OriginID AND R.DestinationID = O.DestinationID
	END
END
GO

ALTER TABLE dbo.tblOrderInvoiceCarrier ADD CONSTRAINT
	FK_tblOrderInvoiceCarrier_tblOrder FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.tblOrder
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
GO
ALTER TABLE dbo.tblOrderInvoiceCarrier SET (LOCK_ESCALATION = TABLE)
GO

ALTER TABLE dbo.tblOrderInvoiceCustomer ADD CONSTRAINT
	FK_tblOrderInvoiceCustomer_tblOrder FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.tblOrder
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
GO
ALTER TABLE dbo.tblOrderInvoiceCustomer SET (LOCK_ESCALATION = TABLE)
GO

ALTER TABLE dbo.tblOrderTicket ADD CONSTRAINT
	FK_OrderTicket_Order FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.tblOrder
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderTicket SET (LOCK_ESCALATION = TABLE)
GO

ALTER TABLE dbo.tblOrderReroute ADD CONSTRAINT
	FK_OrderReroute_Order FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.tblOrder
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
GO
ALTER TABLE dbo.tblOrderReroute SET (LOCK_ESCALATION = TABLE)
GO

/***********************************/
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Order records with "translated friendly" values for FK relationships
/***********************************/
ALTER VIEW [dbo].[viewOrder] AS
SELECT O.*
, vO.Name AS Origin
, vO.FullName AS OriginFull
, vO.State AS OriginState
, vO.StateAbbrev AS OriginStateAbbrev
, vD.Name AS Destination
, vD.FullName AS DestinationFull
, vD.State AS DestinationState
, vD.StateAbbrev AS DestinationStateAbbrev
, vD.DestinationType
, C.Name AS Customer
, CA.Name AS Carrier
, CT.Name AS CarrierType
, OT.OrderStatus
, OT.StatusNum
, D.FullName AS Driver
, D.FirstName AS DriverFirst
, D.LastName AS DriverLast
, TRU.FullName AS Truck
, TR1.FullName AS Trailer
, TR2.FullName AS Trailer2
, P.PriorityNum
, TT.Name AS TicketType
, vD.BolAvailable
, OP.Name AS Operator
, PR.Name AS Producer
, PU.FullName AS Pumper
, D.IDNumber AS DriverNumber
, CA.IDNumber AS CarrierNumber
, TRU.IDNumber AS TruckNumber
, TR1.IDNumber AS TrailerNumber
, TR2.IDNumber AS Trailer2Number
, cast((CASE WHEN O.DeleteDate IS NOT NULL THEN 1 ELSE 0 END) as bit) AS IsDeleted
FROM dbo.tblOrder O
LEFT JOIN dbo.viewOrigin vO ON vO.ID = O.OriginID
LEFT JOIN dbo.viewDestination vD ON vD.ID = O.DestinationID
JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
JOIN dbo.tblOrderStatus AS OT ON OT.ID = O.StatusID
LEFT JOIN dbo.tblCarrier CA ON CA.ID = O.CarrierID
LEFT JOIN dbo.tblCarrierType CT ON CT.ID = CA.CarrierTypeID
LEFT JOIN dbo.viewDriver D ON D.ID = O.DriverID
LEFT JOIN dbo.viewTruck TRU ON TRU.ID = O.TruckID
LEFT JOIN dbo.viewTrailer TR1 ON TR1.ID = O.TrailerID
LEFT JOIN dbo.viewTrailer TR2 ON TR2.ID = O.Trailer2ID
LEFT JOIN dbo.tblPriority P ON P.ID = O.PriorityID
LEFT JOIN dbo.tblTicketType TT ON TT.ID = O.TicketTypeID
LEFT JOIN dbo.tblOperator OP ON OP.ID = O.OperatorID
LEFT JOIN dbo.viewPumper PU ON PU.ID = O.PumperID
LEFT JOIN dbo.tblProducer PR ON PR.ID = O.ProducerID


GO

EXEC _spRefreshAllViews
EXEC _spRecompileAllStoredProcedures
GO

/**********************************/
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return MilesXref (route) records with translated Origin/Destination values
/***********************************/
ALTER VIEW [dbo].[viewRoute] AS
SELECT X.*
, O.Name AS Origin, O.FullName AS OriginFull
, D.Name AS Destination, D.FullName AS DestinationFull
, cast(CASE WHEN O.Active=1 AND D.DeleteDate IS NULL THEN 1 ELSE 0 END as bit) AS Active
FROM dbo.tblRoute X
JOIN dbo.viewOrigin O ON O.ID = X.OriginID
JOIN dbo.viewDestination D ON D.ID = X.DestinationID


GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 21 Jun 2013
-- Description:	trigger to ensure the entered values for an Order are actually valid
-- =============================================
CREATE TRIGGER [dbo].[trigOrder_IU_Validate] ON [dbo].[tblOrder] AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	-- ensure the Origin and Destinations are both specified unless the Status is:
	--   (Generated, Assigned, Dispatched or Declined)
	IF (SELECT COUNT(1) 
		FROM inserted O
		WHERE (O.OriginID IS NULL OR O.DestinationID IS NULL) AND O.StatusID NOT IN (-10,1,2,9)) > 0
	BEGIN
		RAISERROR('Invalid Order Status when the Origin and/or Or Destination is not specified', 16, 1)
		RETURN
	END
	
END

GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 19 Dec 2012
-- Description:	trigger to add a unique, incrementing OrderNum to each new Order (manual Identity column)
-- =============================================
ALTER TRIGGER [dbo].[trigOrder_IU] ON [dbo].[tblOrder] AFTER INSERT, UPDATE AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	WHILE (SELECT count(*) FROM inserted i JOIN tblOrder O ON O.ID = i.ID WHERE O.OrderNum IS NULL) > 0
	BEGIN
		UPDATE tblOrder 
		  SET OrderNum = isnull((SELECT max(OrderNum) FROM tblOrder), 100000) + 1
			, CreateDate = getdate()
		WHERE ID = (SELECT min(O.ID) FROM tblOrder O JOIN inserted i ON i.ID = O.ID WHERE O.OrderNum IS NULL)
	END
	-- re-compute the OriginMinutes (in case the website failed to compute it properly)
	IF UPDATE(OriginArriveTime) OR UPDATE(OriginDepartTime)
	BEGIN
		UPDATE tblOrder SET OriginMinutes = datediff(minute, OriginArriveTime, OriginDepartTime)
	END
	-- re-compute the DestWaitMinutes (in case the website failed to compute it properly)
	IF UPDATE(DestArriveTime) OR UPDATE(DestDepartTime)
	BEGIN
		UPDATE tblOrder SET DestMinutes = datediff(minute, DestArriveTime, DestDepartTime)
	END
	
	-- ensure missing Routes are created and assigned for the specified Origin/Destination combination
	IF UPDATE(OriginID) OR UPDATE(DestinationID)
	BEGIN
		-- create any missing Route records
		INSERT INTO tblRoute (OriginID, DestinationID, CreateDate, CreatedByUser)
			SELECT i.OriginID, i.DestinationID, GETDATE(), ISNULL(i.LastChangedByUser, i.CreatedByUser)
			FROM inserted i
			LEFT JOIN tblRoute R ON R.OriginID = i.OriginID AND R.DestinationID = i.DestinationID
			WHERE i.OriginID IS NOT NULL AND i.DestinationID IS NOT NULL AND R.ID IS NULL
		-- ensure the Order records refer to the correct Route (ID)
		UPDATE tblOrder SET RouteID = R.ID
		FROM tblOrder O
		JOIN inserted i ON i.ID = O.ID
		LEFT JOIN tblRoute R ON R.OriginID = O.OriginID AND R.DestinationID = O.DestinationID
	END
END

GO

COMMIT

SET NOEXEC OFF