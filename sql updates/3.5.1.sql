-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.5.0'
SELECT  @NewVersion = '3.5.1'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'Completely rewrite Settlement system to be much modular, granular, optimized and auto-rating'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

CREATE FUNCTION fnNowDateOnly() RETURNS date AS 
BEGIN
	DECLARE @ret date
	SET @ret = getdate()
	RETURN (@ret)
END
GO
GRANT EXECUTE ON fnNowDateOnly TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Carrier_WaitFeeRoundingType]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblCarrierRates]'))
	ALTER TABLE [dbo].[tblCarrierRates] DROP CONSTRAINT [FK_Carrier_WaitFeeRoundingType]
GO
IF EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Carrier_WaitFeeSubUnit]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblCarrierRates]'))
	ALTER TABLE [dbo].[tblCarrierRates] DROP CONSTRAINT [FK_Carrier_WaitFeeSubUnit]
GO
IF EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CarrierRates_Uom]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblCarrierRates]'))
	ALTER TABLE [dbo].[tblCarrierRates] DROP CONSTRAINT [FK_CarrierRates_Uom]
GO
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_CarrierRates_CreateDateUTC]') AND type = 'D')
	ALTER TABLE [dbo].[tblCarrierRates] DROP CONSTRAINT [DF_CarrierRates_CreateDateUTC]
GO
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_CarrierRates_UomID]') AND type = 'D')
	ALTER TABLE [dbo].[tblCarrierRates] DROP CONSTRAINT [DF_CarrierRates_UomID]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblCustomerRates]') AND type in (N'U'))
	DROP TABLE [dbo].[tblCustomerRates]
GO
IF EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Customer_WaitFeeRoundingType]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblCustomerRates]'))
	ALTER TABLE [dbo].[tblCustomerRates] DROP CONSTRAINT [FK_Customer_WaitFeeRoundingType]
GO
IF EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Customer_WaitFeeSubUnit]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblCustomerRates]'))
	ALTER TABLE [dbo].[tblCustomerRates] DROP CONSTRAINT [FK_Customer_WaitFeeSubUnit]
GO
IF EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CustomerRates_Uom]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblCustomerRates]'))
	ALTER TABLE [dbo].[tblCustomerRates] DROP CONSTRAINT [FK_CustomerRates_Uom]
GO
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_CustomerRates_CreateDateUTC]') AND type = 'D')
	ALTER TABLE [dbo].[tblCustomerRates] DROP CONSTRAINT [DF_CustomerRates_CreateDateUTC]
GO
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_CustomerRates_UomID]') AND type = 'D')
	ALTER TABLE [dbo].[tblCustomerRates] DROP CONSTRAINT [DF_CustomerRates_UomID]
GO
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblCustomerRates]') AND type in (N'U'))
	DROP TABLE [dbo].[tblCustomerRates]
GO

IF EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CarrierWaitFeeParameter_RoundingType]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblCarrierWaitFeeParameter]'))
	ALTER TABLE [dbo].[tblCarrierWaitFeeParameter] DROP CONSTRAINT [FK_CarrierWaitFeeParameter_RoundingType]
GO
IF EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ShipperWaitFeeParameter_RoundingType]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblShipperWaitFeeParameter]'))
	ALTER TABLE [dbo].[tblShipperWaitFeeParameter] DROP CONSTRAINT [FK_ShipperWaitFeeParameter_RoundingType]
GO

UPDATE tblWaitFeeRoundingType SET ID = ID + 1
UPDATE tblCarrierWaitFeeParameter SET RoundingTypeID = RoundingTypeID + 1
UPDATE tblShipperWaitFeeParameter SET RoundingTypeID = RoundingTypeID + 1
GO

ALTER TABLE [dbo].[tblCarrierWaitFeeParameter] 
	ADD CONSTRAINT [FK_CarrierWaitFeeParameter_RoundingType] FOREIGN KEY([RoundingTypeID]) REFERENCES [dbo].[tblWaitFeeRoundingType] ([ID])
ALTER TABLE [dbo].[tblCarrierWaitFeeParameter] CHECK CONSTRAINT [FK_CarrierWaitFeeParameter_RoundingType]
GO
ALTER TABLE [dbo].[tblShipperWaitFeeParameter] 
	ADD CONSTRAINT [FK_ShipperWaitFeeParameter_RoundingType] FOREIGN KEY([RoundingTypeID]) REFERENCES [dbo].[tblWaitFeeRoundingType] ([ID])
ALTER TABLE [dbo].[tblShipperWaitFeeParameter] CHECK CONSTRAINT [FK_ShipperWaitFeeParameter_RoundingType]
GO

IF EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CarrierWaitFeeParameter_SubUnit]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblCarrierWaitFeeParameter]'))
	ALTER TABLE [dbo].[tblCarrierWaitFeeParameter] DROP CONSTRAINT [FK_CarrierWaitFeeParameter_SubUnit]
GO
IF EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ShipperWaitFeeParameter_SubUnit]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblShipperWaitFeeParameter]'))
	ALTER TABLE [dbo].[tblShipperWaitFeeParameter] DROP CONSTRAINT [FK_ShipperWaitFeeParameter_SubUnit]
GO

UPDATE tblWaitFeeSubUnit SET ID = ID + 1
UPDATE tblCarrierWaitFeeParameter SET SubUnitID = SubUnitID + 1
UPDATE tblShipperWaitFeeParameter SET SubUnitID = SubUnitID + 1
GO

ALTER TABLE [dbo].[tblCarrierWaitFeeParameter] 
	ADD CONSTRAINT [FK_CarrierWaitFeeParameter_SubUnit] FOREIGN KEY([SubUnitID]) REFERENCES [dbo].[tblWaitFeeSubUnit] ([ID])
GO
ALTER TABLE [dbo].[tblCarrierWaitFeeParameter] CHECK CONSTRAINT [FK_CarrierWaitFeeParameter_SubUnit]
GO
ALTER TABLE [dbo].[tblShipperWaitFeeParameter] 
	ADD CONSTRAINT [FK_ShipperWaitFeeParameter_SubUnit] FOREIGN KEY([SubUnitID]) REFERENCES [dbo].[tblWaitFeeSubUnit] ([ID])
GO
ALTER TABLE [dbo].[tblShipperWaitFeeParameter] CHECK CONSTRAINT [FK_ShipperWaitFeeParameter_SubUnit]
GO

-------------------------------------------------------------
-- CARRIER CHANGES
-------------------------------------------------------------
GO

/***********************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			ensure Locked records are not modified (except the EndDate)
***********************************************/
ALTER TRIGGER trigCarrierAssessorialRate_IU ON tblCarrierAssessorialRate AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblCarrierAssessorialRate X 
			ON i.ID <> X.ID
			  AND i.TypeID = X.TypeID
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(i.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(i.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(i.ProducerID, X.ProducerID) = 1
			  AND dbo.fnCompareNullableInts(i.OperatorID, X.OperatorID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Assessorial Rates are not allowed';
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewCarrierAssessorialRate X ON d.ID = X.ID AND X.Locked = 1
		WHERE d.TypeID <> X.TypeID
			OR dbo.fnCompareNullableInts(d.CarrierID, X.CarrierID) = 0
			OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
			OR dbo.fnCompareNullableInts(d.OriginID, X.OriginID) = 0
			OR dbo.fnCompareNullableInts(d.DestinationID, X.DestinationID) = 0
			OR dbo.fnCompareNullableInts(d.OriginStateID, X.OriginStateID) = 0
			OR dbo.fnCompareNullableInts(d.DestStateID, X.DestStateID) = 0
			OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
			OR dbo.fnCompareNullableInts(d.ProducerID, X.ProducerID) = 0
			OR dbo.fnCompareNullableInts(d.OperatorID, X.OperatorID) = 0
			OR d.EffectiveDate <> X.EffectiveDate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed';
	END
	
	IF (@error IS NOT NULL)
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
END
GO

/*************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: handle specialized logic related to Effective/End dates (all handled through the associated view)
/*************************************/
CREATE TRIGGER trigCarrierAssessorialRate_D ON tblCarrierAssessorialRate AFTER DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewCarrierAssessorialRate X ON X.ID = d.ID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('Locked (in use) records cannot be deleted', 16, 1)
		ROLLBACK
	END
END

GO

/***********************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			prevent changes to Locked (in use) records
***********************************************/
ALTER TRIGGER trigCarrierDestinationWaitRate_IU ON tblCarrierDestinationWaitRate AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblCarrierDestinationWaitRate X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ReasonID, X.ReasonID) = 1
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(i.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Wait Rates are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewCarrierDestinationWaitRate X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ReasonID, X.ReasonID) = 0
		  OR dbo.fnCompareNullableInts(d.CarrierID, X.CarrierID) = 0
		  OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
		  OR dbo.fnCompareNullableInts(d.DestinationID, X.DestinationID) = 0
		  OR dbo.fnCompareNullableInts(d.StateID, X.StateID) = 0
		  OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
		  OR d.EffectiveDate <> X.EffectiveDate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	IF @error IS NOT NULL 
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
END
GO

/*************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent deletion of Locked records
/*************************************/
CREATE TRIGGER trigCarrierDestinationWaitRate_D ON tblCarrierDestinationWaitRate AFTER DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewCarrierDestinationWaitRate X ON X.id = d.ID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('Locked (in use) records cannot be deleted', 16, 1)
		RETURN
	END
END

GO

/***********************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			prevent changes to Locked (in use) records
***********************************************/
ALTER TRIGGER trigCarrierOriginWaitRate_IU ON tblCarrierOriginWaitRate AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblCarrierOriginWaitRate X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ReasonID, X.ReasonID) = 1
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(i.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Wait Rates are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewCarrierOriginWaitRate X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ReasonID, X.ReasonID) = 0
		  OR dbo.fnCompareNullableInts(d.CarrierID, X.CarrierID) = 0
		  OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
		  OR dbo.fnCompareNullableInts(d.OriginID, X.OriginID) = 0
		  OR dbo.fnCompareNullableInts(d.StateID, X.StateID) = 0
		  OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
		  OR d.EffectiveDate <> X.EffectiveDate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	IF @error IS NOT NULL 
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
END
GO

/*************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent deletion of Locked records
/*************************************/
CREATE TRIGGER trigCarrierOriginWaitRate_D ON tblCarrierOriginWaitRate AFTER DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewCarrierOriginWaitRate X ON X.id = d.ID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('Locked (in use) records cannot be deleted', 16, 1)
		RETURN
	END
END

GO

/***********************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			prevent changes to Locked (in use) records
***********************************************/
ALTER TRIGGER trigCarrierOrderRejectRate_IU ON tblCarrierOrderRejectRate AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblCarrierOrderRejectRate X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ReasonID, X.ReasonID) = 1
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(i.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Reject Rates are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewCarrierOrderRejectRate X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ReasonID, X.ReasonID) = 0
		  OR dbo.fnCompareNullableInts(d.CarrierID, X.CarrierID) = 0
		  OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
		  OR dbo.fnCompareNullableInts(d.OriginID, X.OriginID) = 0
		  OR dbo.fnCompareNullableInts(d.StateID, X.StateID) = 0
		  OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
		  OR d.EffectiveDate <> X.EffectiveDate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	IF @error IS NOT NULL 
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
END
GO

/*************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent deletion of Locked records
/*************************************/
CREATE TRIGGER trigCarrierOrderRejectRate_D ON tblCarrierOrderRejectRate AFTER DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewCarrierOrderRejectRate X ON X.id = d.ID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('Locked (in use) records cannot be deleted', 16, 1)
		RETURN
	END
END

GO

/***********************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
/***********************************************/
ALTER TRIGGER trigCarrierRateSheet_IU ON tblCarrierRateSheet AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblCarrierRateSheet X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(i.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Rate Sheets are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewCarrierRateSheet X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.CarrierID, X.CarrierID) = 0
			OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
			OR dbo.fnCompareNullableInts(d.OriginStateID, X.OriginStateID) = 0
			OR dbo.fnCompareNullableInts(d.DestStateID, X.DestStateID) = 0
			OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
			OR d.EffectiveDate <> X.EffectiveDate 
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	
	IF (@error IS NOT NULL)
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
END
GO

/*************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: handle specialized logic related to Effective/End dates (all handled through the associated view)
/*************************************/
CREATE TRIGGER trigCarrierRateSheet_D ON tblCarrierRateSheet AFTER DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewCarrierRateSheet X ON X.ID = d.ID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('Locked (in use) records cannot be deleted', 16, 1)
		ROLLBACK
	END
END

GO

IF EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'trigCarrierRangeRate_IU'))
	DROP TRIGGER trigCarrierRangeRate_IU
GO 
/*************************************/
-- Date Created: 18 Jan 2015
-- Author: Kevin Alons
-- Purpose: ensure that a RangeRate record is not moved from one RateSheet to another
/*************************************/
CREATE TRIGGER trigCarrierRangeRate_IU ON tblCarrierRangeRate AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @error varchar(255)
	IF EXISTS (SELECT i.* FROM inserted i JOIN deleted d ON d.ID = i.ID WHERE i.RateSheetID <> d.RateSheetID)
		SET @error = 'RangeRate records cannot be moved to a different RateSheet'
	
	IF @error IS NOT NULL
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
END
GO

/*************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent deletion of RangeRate records that belong to a Locked RateSheet
/*************************************/
CREATE TRIGGER trigCarrierRangeRate_D ON tblCarrierRangeRate AFTER DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewCarrierRateSheet X ON X.ID = d.RateSheetID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('RangeRate records for a Locked Rate Sheet cannot be deleted', 16, 1)
		ROLLBACK
	END
END

GO

/***********************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
/***********************************************/
ALTER TRIGGER trigCarrierRouteRate_IU ON tblCarrierRouteRate AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblCarrierRouteRate X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND i.RouteID = X.RouteID
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Route Rates are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewCarrierRouteRate X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.CarrierID, X.CarrierID) = 0
			OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
			OR d.RouteID = X.RouteID
			OR d.EffectiveDate <> X.EffectiveDate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
		IF (@error IS NOT NULL)
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
END
GO

/*************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: handle specialized logic related to Effective/End dates (all handled through the associated view)
/*************************************/
CREATE TRIGGER trigCarrierRouteRate_D ON tblCarrierRouteRate AFTER DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewCarrierRouteRate X ON X.ID = d.ID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('Locked (in use) records cannot be deleted', 16, 1)
		RETURN
	END
END

GO

/***********************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			prevent changes to Locked records (except for EndDate)
***********************************************/
ALTER TRIGGER trigCarrierWaitFeeParameter_IU ON tblCarrierWaitFeeParameter AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblCarrierWaitFeeParameter X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(i.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Parameters are not allowed'
	END
	ElSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewCarrierWaitFeeParameter X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.CarrierID, X.CarrierID) = 0
			OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
			OR dbo.fnCompareNullableInts(d.OriginStateID, X.OriginStateID) = 0
			OR dbo.fnCompareNullableInts(d.DestStateID, X.DestStateID) = 0
			OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
			OR d.EffectiveDate <> X.EffectiveDate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	
	IF (@error IS NOT NULL)
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
END
GO

/*************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent deletion of Locked (in use) records
/*************************************/
CREATE TRIGGER trigCarrierWaitFeeParameter_D ON tblCarrierWaitFeeParameter AFTER DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewCarrierWaitFeeParameter X ON X.ID = d.ID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('Locked (in use) records cannot be deleted', 16, 1)
		RETURN
	END
END

GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'fnCarrierAssessorialRates_ForRange') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION fnCarrierAssessorialRates_ForRange
GO
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier AssessorialRate rows for the specified criteria
/***********************************/
CREATE FUNCTION [dbo].[fnCarrierAssessorialRates_ForRange](@StartDate date, @EndDate date, @TypeID int, @CarrierID int, @ProductGroupID int, @OriginID int, @DestinationID int, @OriginStateID int, @DestStateID int, @RegionID int, @ProducerID int, @OperatorID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.CarrierID, R.ProductGroupID, R.OriginID, R.DestinationID, R.OriginStateID, R.DestStateID, R.RegionID, R.ProducerID, R.OperatorID, R.TypeID, R.Rate, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate, R.NextEffectiveDate
		, Type = RT.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, Origin = O.Name
		, OriginFull = O.FullName
		, Destination = D.Name
		, DestinationFull = D.FullName
		, OriginState = OS.FullName
		, OriginStateAbbrev = OS.Abbreviation
		, DestState = DS.FullName
		, DestStateAbbrev = DS.Abbreviation
		, Region = RE.Name
		, RateType = RT.Name
		, Uom = U.Name
		, UomShort = U.Abbrev
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
	FROM viewCarrierAssessorialRate R
	JOIN tblAssessorialRateType RT ON RT.ID = R.TypeID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN viewOrigin O ON O.ID = R.OriginID
	LEFT JOIN viewDestination D ON D.ID = R.DestinationID
	LEFT JOIN tblState OS ON OS.ID = R.OriginStateID
	LEFT JOIN tblState DS ON DS.ID = R.DestStateID
	LEFT JOIN tblRegion RE ON RE.ID = R.RegionID
	LEFT JOIN tblUom U ON U.ID = R.UomID
	WHERE coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = isnull(R.CarrierID, 0)
	  AND coalesce(nullif(R.ProductGroupID, 0), R.ProductGroupID, 0) = isnull(R.ProductGroupID, 0)
	  AND coalesce(nullif(R.OriginID, 0), R.OriginID, 0) = isnull(R.OriginID, 0)
	  AND coalesce(nullif(R.DestinationID, 0), R.DestinationID, 0) = isnull(R.DestinationID, 0)
	  AND coalesce(nullif(R.OriginStateID, 0), R.OriginStateID, 0) = isnull(R.OriginStateID, 0)
	  AND coalesce(nullif(R.DestStateID, 0), R.DestStateID, 0) = isnull(R.DestStateID, 0)
	  AND coalesce(nullif(R.RegionID, 0), R.RegionID, 0) = isnull(R.RegionID, 0)
	  AND coalesce(nullif(R.ProducerID, 0), R.ProducerID, 0) = isnull(R.ProducerID, 0)
	  AND coalesce(nullif(R.OperatorID, 0), R.OperatorID, 0) = isnull(R.OperatorID, 0)
	  AND (@StartDate BETWEEN EffectiveDate AND EndDate
	     OR @EndDate BETWEEN EffectiveDate AND EndDate
	     OR EffectiveDate BETWEEN @StartDate AND @EndDate
	     OR EndDate BETWEEN @StartDate AND @EndDate
		 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	  AND (isnull(@TypeID, 0) = 0 OR TypeID = @TypeID)
	ORDER BY EffectiveDate
GO
GRANT SELECT ON fnCarrierAssessorialRates_ForRange TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'fnCarrierDestinationWaitRates_ForRange') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION fnCarrierDestinationWaitRates_ForRange
GO
/***********************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier DestinationWaitRate rows for the specified criteria
/***********************************/
CREATE FUNCTION fnCarrierDestinationWaitRates_ForRange(
  @StartDate date
, @EndDate date
, @ReasonID int
, @CarrierID int
, @ProductGroupID int
, @DestinationID int
, @StateID int
, @RegionID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ReasonID, R.CarrierID, R.ProductGroupID, R.DestinationID, R.StateID, R.RegionID, R.Rate, R.EffectiveDate, R.EndDate, R.NextEffectiveDate
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, Destination = D.Name
		, DestinationFull = D.FullName
		, State = S.FullName
		, StateAbbrev = S.Abbreviation
		, Region = REG.Name
		, Reason = REA.Description
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
	FROM dbo.viewCarrierDestinationWaitRate R
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN viewDestination D ON D.ID = R.DestinationID
	LEFT JOIN tblState S ON S.ID = R.StateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblDestinationWaitReason REA ON REA.ID = R.ReasonID
	WHERE coalesce(nullif(@ReasonID, 0), R.ReasonID, 0) = isnull(R.ReasonID, 0)
	  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = isnull(R.CarrierID, 0)
	  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = isnull(R.ProductGroupID, 0)
	  AND coalesce(nullif(@DestinationID, 0), R.DestinationID, 0) = isnull(R.DestinationID, 0)
	  AND coalesce(nullif(@StateID, 0), R.StateID, 0) = isnull(R.StateID, 0)
	  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = isnull(R.RegionID, 0)
	  AND (@StartDate BETWEEN EffectiveDate AND EndDate
        OR @EndDate BETWEEN EffectiveDate AND EndDate
        OR EffectiveDate BETWEEN @StartDate AND @EndDate
        OR EndDate BETWEEN @StartDate AND @EndDate
		OR EffectiveDate BETWEEN @StartDate AND @EndDate)        
	ORDER BY EffectiveDate
GO
GRANT SELECT ON fnCarrierDestinationWaitRates_ForRange TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'fnCarrierOriginWaitRates_ForRange') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION fnCarrierOriginWaitRates_ForRange
GO
/***********************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OriginWaitRate rows for the specified criteria
/***********************************/
CREATE FUNCTION fnCarrierOriginWaitRates_ForRange(
  @StartDate date
, @EndDate date
, @ReasonID int
, @CarrierID int
, @ProductGroupID int
, @OriginID int
, @StateID int
, @RegionID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ReasonID, R.CarrierID, R.ProductGroupID, R.OriginID, R.StateID, R.RegionID, R.Rate, R.EffectiveDate, R.EndDate, R.NextEffectiveDate
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, Origin = O.Name
		, OriginFull = O.FullName
		, State = S.FullName
		, StateAbbrev = S.Abbreviation
		, Region = REG.Name
		, Reason = REA.Description
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
	FROM dbo.viewCarrierOriginWaitRate R
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN viewOrigin O ON O.ID = R.OriginID
	LEFT JOIN tblState S ON S.ID = R.StateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblOriginWaitReason REA ON REA.ID = R.ReasonID
	WHERE coalesce(nullif(@ReasonID, 0), R.ReasonID, 0) = isnull(R.ReasonID, 0)
	  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = isnull(R.CarrierID, 0)
	  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = isnull(R.ProductGroupID, 0)
	  AND coalesce(nullif(@OriginID, 0), R.OriginID, 0) = isnull(R.OriginID, 0)
	  AND coalesce(nullif(@StateID, 0), R.StateID, 0) = isnull(R.StateID, 0)
	  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = isnull(R.RegionID, 0)
	  AND (@StartDate BETWEEN EffectiveDate AND EndDate
        OR @EndDate BETWEEN EffectiveDate AND EndDate
        OR EffectiveDate BETWEEN @StartDate AND @EndDate
        OR EndDate BETWEEN @StartDate AND @EndDate
		OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	ORDER BY EffectiveDate
GO
GRANT SELECT ON fnCarrierOriginWaitRates_ForRange TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'fnCarrierOrderRejectRates_ForRange') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION fnCarrierOrderRejectRates_ForRange
GO
/***********************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OrderRejectRate rows for the specified criteria
/***********************************/
CREATE FUNCTION fnCarrierOrderRejectRates_ForRange(
  @StartDate date
, @EndDate date
, @ReasonID int
, @CarrierID int
, @ProductGroupID int
, @OriginID int
, @StateID int
, @RegionID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ReasonID, R.CarrierID, R.ProductGroupID, R.OriginID, R.StateID, R.RegionID, R.Rate, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate, R.NextEffectiveDate
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, Origin = O.Name
		, OriginFull = O.FullName
		, State = S.FullName
		, StateAbbrev = S.Abbreviation
		, Region = REG.Name
		, Reason = REA.Description
		, RateType = RT.Name
		, Uom = U.Name
		, UomShort = U.Abbrev
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
	FROM dbo.viewCarrierOrderRejectRate R
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN viewOrigin O ON O.ID = R.OriginID
	LEFT JOIN tblState S ON S.ID = R.StateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblOrderRejectReason REA ON REA.ID = R.ReasonID
	LEFT JOIN tblRateType RT ON RT.ID = R.RateTypeID
	LEFT JOIN tblUom U ON U.ID = R.UomID
	WHERE coalesce(nullif(@ReasonID, 0), R.ReasonID, 0) = isnull(R.ReasonID, 0)
	  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = isnull(R.CarrierID, 0)
	  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = isnull(R.ProductGroupID, 0)
	  AND coalesce(nullif(@OriginID, 0), R.OriginID, 0) = isnull(R.OriginID, 0)
	  AND coalesce(nullif(@StateID, 0), R.StateID, 0) = isnull(R.StateID, 0)
	  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = isnull(R.RegionID, 0)
	  AND (@StartDate BETWEEN EffectiveDate AND EndDate
        OR @EndDate BETWEEN EffectiveDate AND EndDate
        OR EffectiveDate BETWEEN @StartDate AND @EndDate
        OR EndDate BETWEEN @StartDate AND @EndDate
		OR EffectiveDate BETWEEN @StartDate AND @EndDate)        
	ORDER BY EffectiveDate
GO
GRANT SELECT ON fnCarrierOrderRejectRates_ForRange TO dispatchcrude_iis_acct
GO

/*************************************
-- Date Created: 18 Jan 2015
-- Author: Kevin Alons
-- Purpose: handle specialized logic related to editing Route Rates (due to combination of RateSheet + RangeRate tables)
*************************************/
CREATE TRIGGER trigViewCarrierRateSheetRangeRate_IU_Update ON viewCarrierRateSheetRangeRate INSTEAD OF INSERT, UPDATE  AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
		-- PRINT 'apply any RateSheet changes to the underlying RateSheet records'
		UPDATE tblCarrierRateSheet
			SET CarrierID = i.CarrierID
				, ProductGroupID = i.ProductGroupID
				, OriginStateID = i.OriginStateID
				, DestStateID = i.DestStateID
				, RegionID = i.RegionID
				, EffectiveDate = i.EffectiveDate
				, EndDate = i.EndDate
				, RateTypeID = i.RateTypeID
				, UomID = i.UomID
				, LastChangeDateUTC = isnull(i.LastChangeDateUTC, getutcdate())
				, LastChangedByUser = i.LastChangedByUser
		FROM tblCarrierRateSheet RS
		JOIN ( 
			SELECT DISTINCT i.RateSheetID, i.CarrierID, i.ProductGroupID, i.OriginStateID, i.DestStateID, i.RegionID, i.EffectiveDate, i.EndDate, i.RateTypeID, i.UomID, i.LastChangeDateUTC, i.LastChangedByUser
			FROM inserted i 
			JOIN deleted d ON d.ID = i.ID
			WHERE dbo.fnCompareNullableInts(i.CarrierID, d.CarrierID) = 0
				OR dbo.fnCompareNullableInts(i.ProductGroupID, d.ProductGroupID) = 0
				OR dbo.fnCompareNullableInts(i.OriginStateID, d.OriginStateID) = 0
				OR dbo.fnCompareNullableInts(i.DestStateID, d.DestStateID) = 0
				OR dbo.fnCompareNullableInts(i.RegionID, d.RegionID) = 0
		) i ON i.RateSheetID = RS.ID

		--PRINT 'ensure a RateSheet record exists for each new RangeRate records'
		INSERT INTO tblCarrierRateSheet (CarrierID, ProductGroupID, OriginStateID, DestStateID, RegionID, EffectiveDate, EndDate, RateTypeID, UomID, CreatedByUser)
			SELECT i.CarrierID, i.ProductGroupID, i.OriginStateID, i.DestStateID, i.RegionID, i.EffectiveDate, i.EndDate, i.RateTypeID, i.UomID, i.CreatedByUser 
			FROM inserted i
			LEFT JOIN tblCarrierRateSheet RS 
				ON i.EffectiveDate = RS.EffectiveDate
					AND dbo.fnCompareNullableInts(i.CarrierID, RS.CarrierID) = 1
					AND dbo.fnCompareNullableInts(i.ProductGroupID, RS.ProductGroupID) = 1
					AND dbo.fnCompareNullableInts(i.OriginStateID, RS.OriginStateID) = 1
					AND dbo.fnCompareNullableInts(i.DestStateID, RS.DestStateID) = 1
					AND dbo.fnCompareNullableInts(i.RegionID, RS.RegionID) = 1
			WHERE isnull(RateSheetID, 0) = 0
			  AND RS.ID IS NULL
		
		-- optimization for typical usaage (where only 1 record is INSERTed)
		IF (SELECT count(1) FROM inserted i WHERE isnull(ID, 0) = 0) = 1
		BEGIN
			DECLARE @rsID int
			SELECT @rsID = isnull(RateSheetID, SCOPE_IDENTITY()) FROM inserted
			IF @rsID IS NULL
				SELECT @rsID = RS.ID
				FROM inserted i
				LEFT JOIN tblCarrierRateSheet RS 
					ON i.EffectiveDate = RS.EffectiveDate
						AND dbo.fnCompareNullableInts(i.CarrierID, RS.CarrierID) = 1
						AND dbo.fnCompareNullableInts(i.ProductGroupID, RS.ProductGroupID) = 1
						AND dbo.fnCompareNullableInts(i.OriginStateID, RS.OriginStateID) = 1
						AND dbo.fnCompareNullableInts(i.DestStateID, RS.DestStateID) = 1
						AND dbo.fnCompareNullableInts(i.RegionID, RS.RegionID) = 1

			INSERT INTO tblCarrierRangeRate (RateSheetID, MinRange, MaxRange, Rate, CreateDateUTC, CreatedByUser)
				SELECT @rsID, i.MinRange, i.MaxRange, i.Rate, isnull(i.CreateDateUTC, getutcdate()), i.CreatedByUser
				FROM inserted i
				WHERE isnull(ID, 0) = 0
		END
		ELSE 	-- bulk insert or update
		BEGIN
			-- PRINT 'Updating any existing record editable data'
			UPDATE tblCarrierRangeRate
				SET MinRange = i.MinRange
					, MaxRange = i.MaxRange
					, Rate = i.Rate
					, LastChangeDateUTC = isnull(i.LastChangeDateUTC, getutcdate())
					, LastChangedByUser = i.LastChangedByUser
			FROM tblCarrierRangeRate X
			JOIN inserted i ON i.ID = X.ID

			-- PRINT 'insert any new records'
			INSERT INTO tblCarrierRangeRate (RateSheetID, MinRange, MaxRange, Rate, CreatedByUser)
				SELECT R.ID, i.MinRange, i.MaxRange, i.Rate, isnull(i.CreatedByUser, R.CreatedByUser)
				FROM inserted i
				JOIN tblCarrierRateSheet R ON i.EffectiveDate = R.EffectiveDate
					AND dbo.fnCompareNullableInts(i.CarrierID, R.CarrierID) = 1
					AND dbo.fnCompareNullableInts(i.ProductGroupID, R.ProductGroupID) = 1
					AND dbo.fnCompareNullableInts(i.OriginStateID, R.OriginStateID) = 1
					AND dbo.fnCompareNullableInts(i.DestStateID, R.DestStateID) = 1
					AND dbo.fnCompareNullableInts(i.RegionID, R.RegionID) = 1
				WHERE ISNULL(i.ID, 0) = 0
		END
	END TRY
	BEGIN CATCH
		DECLARE @error varchar(255)
		SET @error = substring(ERROR_MESSAGE(), 1, 255)
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END CATCH
END
GO

/*************************************/
-- Date Created: 18 Jan 2015
-- Author: Kevin Alons
-- Purpose: allow all editing to be done to viewCarrierRateSheetRangeRate (due to specialized logic related to RouteID)
/*************************************/
CREATE TRIGGER trigViewCarrierRateSheetRangeRate_D ON viewCarrierRateSheetRangeRate INSTEAD OF DELETE AS
BEGIN
	DELETE FROM tblCarrierRangeRate WHERE ID IN (SELECT ID FROM deleted)
	DELETE FROM tblCarrierRateSheet WHERE ID IN (SELECT DISTINCT RateSheetID FROM tblCarrierRangeRate WHERE RateSheetID IN (SELECT DISTINCT RateSheetID FROM deleted))
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'fnCarrierRateSheetRangeRates_ForRange') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION fnCarrierRateSheetRangeRates_ForRange
GO
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RateSheetRangeRate info for the specified criteria
/***********************************/
CREATE FUNCTION fnCarrierRateSheetRangeRates_ForRange(@StartDate date, @EndDate date, @RouteMiles int, @CarrierID int, @ProductGroupID int, @OriginStateID int, @DestStateID int, @RegionID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT *
	FROM viewCarrierRateSheetRangeRate R
	WHERE coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = isnull(R.CarrierID, 0) 
	  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = isnull(R.ProductGroupID, 0)
	  AND coalesce(nullif(@OriginStateID, 0), R.OriginStateID, 0) = isnull(R.OriginStateID, 0)
	  AND coalesce(nullif(@DestStateID, 0), R.DestStateID, 0) = isnull(R.DestStateID, 0)
	  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = isnull(R.RegionID, 0)
      AND (nullif(@RouteMiles, 0) IS NULL OR @RouteMiles BETWEEN MinRange AND MaxRange)
      AND (@StartDate BETWEEN EffectiveDate AND EndDate
	    OR @EndDate BETWEEN EffectiveDate AND EndDate
        OR EffectiveDate BETWEEN @StartDate AND @EndDate
        OR EndDate BETWEEN @StartDate AND @EndDate
        OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	ORDER BY EffectiveDate
GO
GRANT SELECT ON fnCarrierRateSheetRangeRates_ForRange TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'fnCarrierRouteRates_ForRange') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION fnCarrierRouteRates_ForRange
GO 
/***********************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RouteRate rows for the specified criteria
/***********************************/
CREATE FUNCTION fnCarrierRouteRates_ForRange(@StartDate date, @EndDate date, @CarrierID int, @ProductGroupID int, @OriginID int, @DestinationID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.RouteID, R.ActualMiles, R.OriginID, R.DestinationID, R.CarrierID, R.ProductGroupID, R.Rate, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate, R.NextEffectiveDate
		, Origin = O.Name
		, OriginFull = O.FullName
		, Destination = D.Name
		, DestinationFull = D.FullName
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, RateType = RT.Name
		, Uom = U.Name
		, UomShort = U.Abbrev
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
	FROM dbo.viewCarrierRouteRate R
	JOIN viewOrigin O ON O.ID = R.OriginID
	JOIN viewDestination D ON D.ID = R.DestinationID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblRateType RT ON RT.ID = R.RateTypeID
	LEFT JOIN tblUom U ON U.ID = R.UomID
	WHERE coalesce(nullif(@OriginID, 0), R.OriginID, 0) = isnull(R.OriginID, 0)
	  AND coalesce(nullif(@DestinationID, 0), R.DestinationID, 0) = isnull(R.DestinationID, 0)
	  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = isnull(R.CarrierID, 0)
	  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = isnull(R.ProductGroupID, 0)
	  AND (@StartDate BETWEEN EffectiveDate AND EndDate
	    OR @EndDate BETWEEN EffectiveDate AND EndDate
	    OR EffectiveDate BETWEEN @StartDate AND @EndDate
	    OR EndDate BETWEEN @StartDate AND @EndDate
		OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	ORDER BY EffectiveDate
GO
GRANT SELECT ON fnCarrierRouteRates_ForRange TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'fnCarrierWaitFeeParameters_ForRange') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION fnCarrierWaitFeeParameters_ForRange
GO
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier WaitFeeParameter rows for the specified criteria
/***********************************/
CREATE FUNCTION fnCarrierWaitFeeParameters_ForRange(@StartDate date, @EndDate date, @CarrierID int, @ProductGroupID int, @OriginStateID int, @DestStateID int, @RegionID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.CarrierID, R.ProductGroupID, R.OriginStateID, R.DestStateID, R.RegionID, R.SubUnitID, R.RoundingTypeID, R.ThresholdMinutes, R.EffectiveDate, R.EndDate, R.NextEffectiveDate
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, OriginState = OS.FullName
		, OriginStateAbbrev = OS.Abbreviation
		, DestinationState = DS.FullName
		, DestinationStateAbbrev = DS.Abbreviation
		, Region = REG.Name
		, RoundingType = WFRT.Name
		, SubUnit = WFSU.Name
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
	FROM dbo.viewCarrierWaitFeeParameter R 
	JOIN tblWaitFeeRoundingType WFRT ON WFRT.ID = R.RoundingTypeID
	JOIN tblWaitFeeSubUnit WFSU ON WFSU.ID = R.SubUnitID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblState OS ON OS.ID = R.OriginStateID
	LEFT JOIN tblState DS ON DS.ID = R.DestStateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	WHERE coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = isnull(R.CarrierID, 0)
	  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = isnull(R.ProductGroupID, 0)
	  AND coalesce(nullif(@OriginStateID, 0), R.OriginStateID, 0) = isnull(R.OriginStateID, 0)
	  AND coalesce(nullif(@DestStateID, 0), R.DestStateID, 0) = isnull(R.DestStateID, 0)
	  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = isnull(R.RegionID, 0)
	  AND (@StartDate BETWEEN EffectiveDate AND EndDate
        OR @EndDate BETWEEN EffectiveDate AND EndDate
        OR EffectiveDate BETWEEN @StartDate AND @EndDate
        OR EndDate BETWEEN @StartDate AND @EndDate
		OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	ORDER BY EffectiveDate
GO
GRANT SELECT ON fnCarrierWaitFeeParameters_ForRange TO dispatchcrude_iis_acct
GO

/*************************************/
-- Date Created: 18 Jan 2015
-- Author: Kevin Alons
-- Purpose: handle specialized logic related to editing Route Rates (due to RouteID being "inputted" as Origin|Destination combination)
/*************************************/
CREATE TRIGGER trigViewCarrierRouteRate_IU_Update ON viewCarrierRouteRate INSTEAD OF INSERT, UPDATE  AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
		--PRINT 'ensure a Route record exists for the new Origin-Destination combo'
		INSERT INTO tblRoute (OriginID, DestinationID)
			SELECT DISTINCT OriginID, DestinationID FROM inserted
			EXCEPT SELECT OriginID, DestinationID FROM tblRoute
		
		-- PRINT 'Updating any existing record editable data'
		UPDATE tblCarrierRouteRate
			SET CarrierID = nullif(i.CarrierID, 0)
				, ProductGroupID = nullif(i.ProductGroupID, 0)
				, RouteID = R.ID
				, Rate = i.Rate
				, EffectiveDate = i.EffectiveDate
				, EndDate = i.EndDate
				, RateTypeID = i.RateTypeID
				, UomID = i.UomID
				, LastChangeDateUTC = i.LastChangeDateUTC
				, LastChangedByUser = i.LastChangedByUser
		FROM tblCarrierRouteRate X
		JOIN inserted i ON i.ID = X.ID
		JOIN tblRoute R ON R.OriginID = i.OriginID AND R.DestinationID = i.DestinationID 

		-- PRINT 'insert any new records'
		INSERT INTO tblCarrierRouteRate (CarrierID, ProductGroupID, RouteID, Rate, EffectiveDate, EndDate, RateTypeID, UomID, CreatedByUser)
			SELECT nullif(CarrierID, 0), nullif(ProductGroupID, 0), R.ID, Rate, EffectiveDate, EndDate, RateTypeID, UomID, i.CreatedByUser
			FROM inserted i
			JOIN tblRoute R ON R.OriginID = i.OriginID AND R.DestinationID = i.DestinationID 
			WHERE ISNULL(i.ID, 0) = 0
	END TRY
	BEGIN CATCH
		DECLARE @error varchar(255)
		SET @error = ERROR_MESSAGE()
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END CATCH
END
GO

/*************************************/
-- Date Created: 18 Jan 2015
-- Author: Kevin Alons
-- Purpose: allow all editing to be done to viewCarrierRouteRate (due to specialized logic related to RouteID)
/*************************************/
CREATE TRIGGER trigViewCarrierRouteRate_D ON viewCarrierRouteRate INSTEAD OF DELETE AS
BEGIN
	DELETE FROM tblCarrierRouteRate WHERE ID IN (SELECT ID FROM deleted)
END
GO

GRANT SELECT, UPDATE, INSERT, DELETE ON viewCarrierRouteRate TO dispatchcrude_iis_acct
GO

-------------------------------------------------------------
-- SHIPPER CHANGES
-------------------------------------------------------------
GO
/***********************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			ensure Locked records are not modified (except the EndDate)
***********************************************/
ALTER TRIGGER trigShipperAssessorialRate_IU ON tblShipperAssessorialRate AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblShipperAssessorialRate X 
			ON i.ID <> X.ID
			  AND i.TypeID = X.TypeID
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(i.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(i.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(i.ProducerID, X.ProducerID) = 1
			  AND dbo.fnCompareNullableInts(i.OperatorID, X.OperatorID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Assessorial Rates are not allowed';
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewShipperAssessorialRate X ON d.ID = X.ID AND X.Locked = 1
		WHERE d.TypeID <> X.TypeID
			OR dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
			OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
			OR dbo.fnCompareNullableInts(d.OriginID, X.OriginID) = 0
			OR dbo.fnCompareNullableInts(d.DestinationID, X.DestinationID) = 0
			OR dbo.fnCompareNullableInts(d.OriginStateID, X.OriginStateID) = 0
			OR dbo.fnCompareNullableInts(d.DestStateID, X.DestStateID) = 0
			OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
			OR dbo.fnCompareNullableInts(d.ProducerID, X.ProducerID) = 0
			OR dbo.fnCompareNullableInts(d.OperatorID, X.OperatorID) = 0
			OR d.EffectiveDate <> X.EffectiveDate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed';
	END
	
	IF (@error IS NOT NULL)
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
END
GO

/*************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: handle specialized logic related to Effective/End dates (all handled through the associated view)
/*************************************/
CREATE TRIGGER trigShipperAssessorialRate_D ON tblShipperAssessorialRate AFTER DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewShipperAssessorialRate X ON X.ID = d.ID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('Locked (in use) records cannot be deleted', 16, 1)
		ROLLBACK
	END
END

GO

/***********************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			prevent changes to Locked (in use) records
***********************************************/
ALTER TRIGGER trigShipperDestinationWaitRate_IU ON tblShipperDestinationWaitRate AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblShipperDestinationWaitRate X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ReasonID, X.ReasonID) = 1
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(i.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Wait Rates are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewShipperDestinationWaitRate X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ReasonID, X.ReasonID) = 0
		  OR dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
		  OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
		  OR dbo.fnCompareNullableInts(d.DestinationID, X.DestinationID) = 0
		  OR dbo.fnCompareNullableInts(d.StateID, X.StateID) = 0
		  OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
		  OR d.EffectiveDate <> X.EffectiveDate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	IF @error IS NOT NULL 
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
END
GO

/*************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent deletion of Locked records
/*************************************/
CREATE TRIGGER trigShipperDestinationWaitRate_D ON tblShipperDestinationWaitRate AFTER DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewShipperDestinationWaitRate X ON X.id = d.ID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('Locked (in use) records cannot be deleted', 16, 1)
		RETURN
	END
END

GO

/***********************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			prevent changes to Locked (in use) records
***********************************************/
ALTER TRIGGER trigShipperOriginWaitRate_IU ON tblShipperOriginWaitRate AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblShipperOriginWaitRate X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ReasonID, X.ReasonID) = 1
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(i.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Wait Rates are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewShipperOriginWaitRate X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ReasonID, X.ReasonID) = 0
		  OR dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
		  OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
		  OR dbo.fnCompareNullableInts(d.OriginID, X.OriginID) = 0
		  OR dbo.fnCompareNullableInts(d.StateID, X.StateID) = 0
		  OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
		  OR d.EffectiveDate <> X.EffectiveDate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	IF @error IS NOT NULL 
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
END
GO

/*************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent deletion of Locked records
/*************************************/
CREATE TRIGGER trigShipperOriginWaitRate_D ON tblShipperOriginWaitRate AFTER DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewShipperOriginWaitRate X ON X.id = d.ID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('Locked (in use) records cannot be deleted', 16, 1)
		RETURN
	END
END

GO

/***********************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			prevent changes to Locked (in use) records
***********************************************/
ALTER TRIGGER trigShipperOrderRejectRate_IU ON tblShipperOrderRejectRate AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblShipperOrderRejectRate X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ReasonID, X.ReasonID) = 1
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(i.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Reject Rates are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewShipperOrderRejectRate X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ReasonID, X.ReasonID) = 0
		  OR dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
		  OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
		  OR dbo.fnCompareNullableInts(d.OriginID, X.OriginID) = 0
		  OR dbo.fnCompareNullableInts(d.StateID, X.StateID) = 0
		  OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
		  OR d.EffectiveDate <> X.EffectiveDate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	IF @error IS NOT NULL 
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
END
GO

/*************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent deletion of Locked records
/*************************************/
CREATE TRIGGER trigShipperOrderRejectRate_D ON tblShipperOrderRejectRate AFTER DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewShipperOrderRejectRate X ON X.id = d.ID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('Locked (in use) records cannot be deleted', 16, 1)
		RETURN
	END
END

GO

/***********************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
/***********************************************/
ALTER TRIGGER trigShipperRateSheet_IU ON tblShipperRateSheet AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblShipperRateSheet X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(i.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Rate Sheets are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewShipperRateSheet X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
			OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
			OR dbo.fnCompareNullableInts(d.OriginStateID, X.OriginStateID) = 0
			OR dbo.fnCompareNullableInts(d.DestStateID, X.DestStateID) = 0
			OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
			OR d.EffectiveDate <> X.EffectiveDate 
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	
	IF (@error IS NOT NULL)
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
END
GO

/*************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: handle specialized logic related to Effective/End dates (all handled through the associated view)
/*************************************/
CREATE TRIGGER trigShipperRateSheet_D ON tblShipperRateSheet AFTER DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewShipperRateSheet X ON X.ID = d.ID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('Locked (in use) records cannot be deleted', 16, 1)
		ROLLBACK
	END
END

GO

IF EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'trigShipperRangeRate_IU'))
	DROP TRIGGER trigShipperRangeRate_IU
GO
/*************************************/
-- Date Created: 18 Jan 2015
-- Author: Kevin Alons
-- Purpose: ensure that a RangeRate record is not moved from one RateSheet to another
/*************************************/
CREATE TRIGGER trigShipperRangeRate_IU ON tblShipperRangeRate AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @error varchar(255)
	IF EXISTS (SELECT i.* FROM inserted i JOIN deleted d ON d.ID = i.ID WHERE i.RateSheetID <> d.RateSheetID)
		SET @error = 'RangeRate records cannot be moved to a different RateSheet'
	
	IF @error IS NOT NULL
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
END
GO

/*************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent deletion of RangeRate records that belong to a Locked RateSheet
/*************************************/
CREATE TRIGGER trigShipperRangeRate_D ON tblShipperRangeRate AFTER DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewShipperRateSheet X ON X.ID = d.RateSheetID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('RangeRate records for a Locked Rate Sheet cannot be deleted', 16, 1)
		ROLLBACK
	END
END

GO

/***********************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
/***********************************************/
ALTER TRIGGER trigShipperRouteRate_IU ON tblShipperRouteRate AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblShipperRouteRate X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND i.RouteID = X.RouteID
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Route Rates are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewShipperRouteRate X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
			OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
			OR d.RouteID = X.RouteID
			OR d.EffectiveDate <> X.EffectiveDate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
		IF (@error IS NOT NULL)
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
END
GO

/*************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: handle specialized logic related to Effective/End dates (all handled through the associated view)
/*************************************/
CREATE TRIGGER trigShipperRouteRate_D ON tblShipperRouteRate AFTER DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewShipperRouteRate X ON X.ID = d.ID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('Locked (in use) records cannot be deleted', 16, 1)
		RETURN
	END
END

GO

/***********************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			prevent changes to Locked records (except for EndDate)
***********************************************/
ALTER TRIGGER trigShipperWaitFeeParameter_IU ON tblShipperWaitFeeParameter AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblShipperWaitFeeParameter X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(i.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Parameters are not allowed'
	END
	ElSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewShipperWaitFeeParameter X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
			OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
			OR dbo.fnCompareNullableInts(d.OriginStateID, X.OriginStateID) = 0
			OR dbo.fnCompareNullableInts(d.DestStateID, X.DestStateID) = 0
			OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
			OR d.EffectiveDate <> X.EffectiveDate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	
	IF (@error IS NOT NULL)
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
END
GO

/*************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent deletion of Locked (in use) records
/*************************************/
CREATE TRIGGER trigShipperWaitFeeParameter_D ON tblShipperWaitFeeParameter AFTER DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewShipperWaitFeeParameter X ON X.ID = d.ID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('Locked (in use) records cannot be deleted', 16, 1)
		RETURN
	END
END

GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'fnShipperAssessorialRates_ForRange') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION fnShipperAssessorialRates_ForRange
GO
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper AssessorialRate rows for the specified criteria
/***********************************/
CREATE FUNCTION fnShipperAssessorialRates_ForRange(@StartDate date, @EndDate date, @TypeID int, @ShipperID int, @ProductGroupID int, @OriginID int, @DestinationID int, @OriginStateID int, @DestStateID int, @RegionID int, @ProducerID int, @OperatorID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ShipperID, R.ProductGroupID, R.OriginID, R.DestinationID, R.OriginStateID, R.DestStateID, R.RegionID, R.ProducerID, R.OperatorID, R.TypeID, R.Rate, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate, R.NextEffectiveDate
		, Type = RT.Name
		, Shipper = C.Name
		, ProductGroup = PG.Name
		, Origin = O.Name
		, OriginFull = O.FullName
		, Destination = D.Name
		, DestinationFull = D.FullName
		, OriginState = OS.FullName
		, OriginStateAbbrev = OS.Abbreviation
		, DestState = DS.FullName
		, DestStateAbbrev = DS.Abbreviation
		, Region = RE.Name
		, RateType = RT.Name
		, Uom = U.Name
		, UomShort = U.Abbrev
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
	FROM viewShipperAssessorialRate R
	JOIN tblAssessorialRateType RT ON RT.ID = R.TypeID
	LEFT JOIN tblCustomer C ON C.ID = R.ShipperID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN viewOrigin O ON O.ID = R.OriginID
	LEFT JOIN viewDestination D ON D.ID = R.DestinationID
	LEFT JOIN tblState OS ON OS.ID = R.OriginStateID
	LEFT JOIN tblState DS ON DS.ID = R.DestStateID
	LEFT JOIN tblRegion RE ON RE.ID = R.RegionID
	LEFT JOIN tblUom U ON U.ID = R.UomID
	WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = isnull(R.ShipperID, 0)
	  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = isnull(R.ProductGroupID, 0)
	  AND coalesce(nullif(@OriginID, 0), R.OriginID, 0) = isnull(R.OriginID, 0)
	  AND coalesce(nullif(@DestinationID, 0), R.DestinationID, 0) = isnull(R.DestinationID, 0)
	  AND coalesce(nullif(@OriginStateID, 0), R.OriginStateID, 0) = isnull(R.OriginStateID, 0)
	  AND coalesce(nullif(@DestStateID, 0), R.DestStateID, 0) = isnull(R.DestStateID, 0)
	  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = isnull(R.RegionID, 0)
	  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = isnull(R.ProducerID, 0)
	  AND coalesce(nullif(@OperatorID, 0), R.OperatorID, 0) = isnull(R.OperatorID, 0)
	  AND (@StartDate BETWEEN EffectiveDate AND EndDate
	    OR @EndDate BETWEEN EffectiveDate AND EndDate
	    OR EffectiveDate BETWEEN @StartDate AND @EndDate
	    OR EndDate BETWEEN @StartDate AND @EndDate
		OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	  AND (isnull(@TypeID, 0) = 0 OR TypeID = @TypeID)
	ORDER BY EffectiveDate
GO
GRANT SELECT ON fnShipperAssessorialRates_ForRange TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'fnShipperDestinationWaitRates_ForRange') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION fnShipperDestinationWaitRates_ForRange
GO
/***********************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper DestinationWaitRate rows for the specified criteria
/***********************************/
CREATE FUNCTION fnShipperDestinationWaitRates_ForRange(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @ProductGroupID int
, @DestinationID int
, @StateID int
, @RegionID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ReasonID, R.ShipperID, R.ProductGroupID, R.DestinationID, R.StateID, R.RegionID, R.Rate, R.EffectiveDate, R.EndDate, R.NextEffectiveDate
		, Shipper = C.Name
		, ProductGroup = PG.Name
		, Destination = D.Name
		, DestinationFull = D.FullName
		, State = S.FullName
		, StateAbbrev = S.Abbreviation
		, Region = REG.Name
		, Reason = REA.Description
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
	FROM dbo.viewShipperDestinationWaitRate R
	LEFT JOIN tblCustomer C ON C.ID = R.ShipperID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN viewDestination D ON D.ID = R.DestinationID
	LEFT JOIN tblState S ON S.ID = R.StateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblDestinationWaitReason REA ON REA.ID = R.ReasonID
	WHERE coalesce(nullif(@ReasonID, 0), R.ReasonID, 0) = isnull(R.ReasonID, 0)
	  AND coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = isnull(R.ShipperID, 0)
	  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = isnull(R.ProductGroupID, 0)
	  AND coalesce(nullif(@DestinationID, 0), R.DestinationID, 0) = isnull(R.DestinationID, 0)
	  AND coalesce(nullif(@StateID, 0), R.StateID, 0) = isnull(R.StateID, 0)
	  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = isnull(R.RegionID, 0)
	  AND (@StartDate BETWEEN EffectiveDate AND EndDate
        OR @EndDate BETWEEN EffectiveDate AND EndDate
        OR EffectiveDate BETWEEN @StartDate AND @EndDate
        OR EndDate BETWEEN @StartDate AND @EndDate
		OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	ORDER BY EffectiveDate
GO
GRANT SELECT ON fnShipperDestinationWaitRates_ForRange TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'fnShipperOriginWaitRates_ForRange') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION fnShipperOriginWaitRates_ForRange
GO
/***********************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper OriginWaitRate rows for the specified criteria
/***********************************/
CREATE FUNCTION fnShipperOriginWaitRates_ForRange(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @ProductGroupID int
, @OriginID int
, @StateID int
, @RegionID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ReasonID, R.ShipperID, R.ProductGroupID, R.OriginID, R.StateID, R.RegionID, R.Rate, R.EffectiveDate, R.EndDate, R.NextEffectiveDate
		, Shipper = C.Name
		, ProductGroup = PG.Name
		, Origin = O.Name
		, OriginFull = O.FullName
		, State = S.FullName
		, StateAbbrev = S.Abbreviation
		, Region = REG.Name
		, Reason = REA.Description
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
	FROM dbo.viewShipperOriginWaitRate R
	LEFT JOIN tblCustomer C ON C.ID = R.ShipperID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN viewOrigin O ON O.ID = R.OriginID
	LEFT JOIN tblState S ON S.ID = R.StateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblOriginWaitReason REA ON REA.ID = R.ReasonID
	WHERE coalesce(nullif(@ReasonID, 0), R.ReasonID, 0) = isnull(R.ReasonID, 0)
	  AND coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = isnull(R.ShipperID, 0)
	  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = isnull(R.ProductGroupID, 0)
	  AND coalesce(nullif(@OriginID, 0), R.OriginID, 0) = isnull(R.OriginID, 0)
	  AND coalesce(nullif(@StateID, 0), R.StateID, 0) = isnull(R.StateID, 0)
	  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = isnull(R.RegionID, 0)
	  AND (@StartDate BETWEEN EffectiveDate AND EndDate
        OR @EndDate BETWEEN EffectiveDate AND EndDate
        OR EffectiveDate BETWEEN @StartDate AND @EndDate
        OR EndDate BETWEEN @StartDate AND @EndDate
		OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	ORDER BY EffectiveDate
GO
GRANT SELECT ON fnShipperOriginWaitRates_ForRange TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'fnShipperOrderRejectRates_ForRange') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION fnShipperOrderRejectRates_ForRange
GO
/***********************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper OrderRejectRate rows for the specified criteria
/***********************************/
CREATE FUNCTION fnShipperOrderRejectRates_ForRange(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @ProductGroupID int
, @OriginID int
, @StateID int
, @RegionID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ReasonID, R.ShipperID, R.ProductGroupID, R.OriginID, R.StateID, R.RegionID, R.Rate, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate, R.NextEffectiveDate
		, Shipper = C.Name
		, ProductGroup = PG.Name
		, Origin = O.Name
		, OriginFull = O.FullName
		, State = S.FullName
		, StateAbbrev = S.Abbreviation
		, Region = REG.Name
		, Reason = REA.Description
		, RateType = RT.Name
		, Uom = U.Name
		, UomShort = U.Abbrev
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
	FROM dbo.viewShipperOrderRejectRate R
	LEFT JOIN tblCustomer C ON C.ID = R.ShipperID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN viewOrigin O ON O.ID = R.OriginID
	LEFT JOIN tblState S ON S.ID = R.StateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblOrderRejectReason REA ON REA.ID = R.ReasonID
	LEFT JOIN tblRateType RT ON RT.ID = R.RateTypeID
	LEFT JOIN tblUom U ON U.ID = R.UomID
	WHERE coalesce(nullif(@ReasonID, 0), R.ReasonID, 0) = isnull(R.ReasonID, 0)
	  AND coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = isnull(R.ShipperID, 0)
	  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = isnull(R.ProductGroupID, 0)
	  AND coalesce(nullif(@OriginID, 0), R.OriginID, 0) = isnull(R.OriginID, 0)
	  AND coalesce(nullif(@StateID, 0), R.StateID, 0) = isnull(R.StateID, 0)
	  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = isnull(R.RegionID, 0)
	  AND (@StartDate BETWEEN EffectiveDate AND EndDate
        OR @EndDate BETWEEN EffectiveDate AND EndDate
        OR EffectiveDate BETWEEN @StartDate AND @EndDate
        OR EndDate BETWEEN @StartDate AND @EndDate
		OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	ORDER BY EffectiveDate
GO
GRANT SELECT ON fnShipperOrderRejectRates_ForRange TO dispatchcrude_iis_acct
GO

/*************************************
-- Date Created: 18 Jan 2015
-- Author: Kevin Alons
-- Purpose: handle specialized logic related to editing Route Rates (due to combination of RateSheet + RangeRate tables)
*************************************/
CREATE TRIGGER trigViewShipperRateSheetRangeRate_IU_Update ON viewShipperRateSheetRangeRate INSTEAD OF INSERT, UPDATE  AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
		-- PRINT 'apply any RateSheet changes to the underlying RateSheet records'
		UPDATE tblShipperRateSheet
			SET ShipperID = i.ShipperID
				, ProductGroupID = i.ProductGroupID
				, OriginStateID = i.OriginStateID
				, DestStateID = i.DestStateID
				, RegionID = i.RegionID
				, EffectiveDate = i.EffectiveDate
				, EndDate = i.EndDate
				, RateTypeID = i.RateTypeID
				, UomID = i.UomID
				, LastChangeDateUTC = isnull(i.LastChangeDateUTC, getutcdate())
				, LastChangedByUser = i.LastChangedByUser
		FROM tblShipperRateSheet RS
		JOIN ( 
			SELECT DISTINCT i.RateSheetID, i.ShipperID, i.ProductGroupID, i.OriginStateID, i.DestStateID, i.RegionID, i.EffectiveDate, i.EndDate, i.RateTypeID, i.UomID, i.LastChangeDateUTC, i.LastChangedByUser
			FROM inserted i 
			JOIN deleted d ON d.ID = i.ID
			WHERE dbo.fnCompareNullableInts(i.ShipperID, d.ShipperID) = 0
				OR dbo.fnCompareNullableInts(i.ProductGroupID, d.ProductGroupID) = 0
				OR dbo.fnCompareNullableInts(i.OriginStateID, d.OriginStateID) = 0
				OR dbo.fnCompareNullableInts(i.DestStateID, d.DestStateID) = 0
				OR dbo.fnCompareNullableInts(i.RegionID, d.RegionID) = 0
		) i ON i.RateSheetID = RS.ID

		--PRINT 'ensure a RateSheet record exists for each new RangeRate records'
		INSERT INTO tblShipperRateSheet (ShipperID, ProductGroupID, OriginStateID, DestStateID, RegionID, EffectiveDate, EndDate, RateTypeID, UomID, CreatedByUser)
			SELECT i.ShipperID, i.ProductGroupID, i.OriginStateID, i.DestStateID, i.RegionID, i.EffectiveDate, i.EndDate, i.RateTypeID, i.UomID, i.CreatedByUser 
			FROM inserted i
			LEFT JOIN tblShipperRateSheet RS 
				ON i.EffectiveDate = RS.EffectiveDate
					AND dbo.fnCompareNullableInts(i.ShipperID, RS.ShipperID) = 1
					AND dbo.fnCompareNullableInts(i.ProductGroupID, RS.ProductGroupID) = 1
					AND dbo.fnCompareNullableInts(i.OriginStateID, RS.OriginStateID) = 1
					AND dbo.fnCompareNullableInts(i.DestStateID, RS.DestStateID) = 1
					AND dbo.fnCompareNullableInts(i.RegionID, RS.RegionID) = 1
			WHERE isnull(RateSheetID, 0) = 0
			  AND RS.ID IS NULL
		
		-- optimization for typical usaage (where only 1 record is INSERTed)
		IF (SELECT count(1) FROM inserted i WHERE isnull(ID, 0) = 0) = 1
		BEGIN
			DECLARE @rsID int
			SELECT @rsID = isnull(RateSheetID, SCOPE_IDENTITY()) FROM inserted
			IF @rsID IS NULL
				SELECT @rsID = RS.ID
				FROM inserted i
				LEFT JOIN tblShipperRateSheet RS 
					ON i.EffectiveDate = RS.EffectiveDate
						AND dbo.fnCompareNullableInts(i.ShipperID, RS.ShipperID) = 1
						AND dbo.fnCompareNullableInts(i.ProductGroupID, RS.ProductGroupID) = 1
						AND dbo.fnCompareNullableInts(i.OriginStateID, RS.OriginStateID) = 1
						AND dbo.fnCompareNullableInts(i.DestStateID, RS.DestStateID) = 1
						AND dbo.fnCompareNullableInts(i.RegionID, RS.RegionID) = 1

			INSERT INTO tblShipperRangeRate (RateSheetID, MinRange, MaxRange, Rate, CreateDateUTC, CreatedByUser)
				SELECT @rsID, i.MinRange, i.MaxRange, i.Rate, isnull(i.CreateDateUTC, getutcdate()), i.CreatedByUser
				FROM inserted i
				WHERE isnull(ID, 0) = 0
		END
		ELSE 	-- bulk insert or update
		BEGIN
			-- PRINT 'Updating any existing record editable data'
			UPDATE tblShipperRangeRate
				SET MinRange = i.MinRange
					, MaxRange = i.MaxRange
					, Rate = i.Rate
					, LastChangeDateUTC = isnull(i.LastChangeDateUTC, getutcdate())
					, LastChangedByUser = i.LastChangedByUser
			FROM tblShipperRangeRate X
			JOIN inserted i ON i.ID = X.ID

			-- PRINT 'insert any new records'
			INSERT INTO tblShipperRangeRate (RateSheetID, MinRange, MaxRange, Rate, CreatedByUser)
				SELECT R.ID, i.MinRange, i.MaxRange, i.Rate, isnull(i.CreatedByUser, R.CreatedByUser)
				FROM inserted i
				JOIN tblShipperRateSheet R ON i.EffectiveDate = R.EffectiveDate
					AND dbo.fnCompareNullableInts(i.ShipperID, R.ShipperID) = 1
					AND dbo.fnCompareNullableInts(i.ProductGroupID, R.ProductGroupID) = 1
					AND dbo.fnCompareNullableInts(i.OriginStateID, R.OriginStateID) = 1
					AND dbo.fnCompareNullableInts(i.DestStateID, R.DestStateID) = 1
					AND dbo.fnCompareNullableInts(i.RegionID, R.RegionID) = 1
				WHERE ISNULL(i.ID, 0) = 0
		END
	END TRY
	BEGIN CATCH
		DECLARE @error varchar(255)
		SET @error = substring(ERROR_MESSAGE(), 1, 255)
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END CATCH
END
GO

/*************************************/
-- Date Created: 18 Jan 2015
-- Author: Kevin Alons
-- Purpose: allow all editing to be done to viewShipperRateSheetRangeRate (due to specialized logic related to RouteID)
/*************************************/
CREATE TRIGGER trigViewShipperRateSheetRangeRate_D ON viewShipperRateSheetRangeRate INSTEAD OF DELETE AS
BEGIN
	DELETE FROM tblShipperRangeRate WHERE ID IN (SELECT ID FROM deleted)
	DELETE FROM tblShipperRateSheet WHERE ID IN (SELECT DISTINCT RateSheetID FROM tblShipperRangeRate WHERE RateSheetID IN (SELECT DISTINCT RateSheetID FROM deleted))
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'fnShipperRateSheetRangeRates_ForRange') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION fnShipperRateSheetRangeRates_ForRange
GO
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper RateSheetRangeRate info for the specified criteria
/***********************************/
CREATE FUNCTION fnShipperRateSheetRangeRates_ForRange(@StartDate date, @EndDate date, @RouteMiles int, @ShipperID int, @ProductGroupID int, @OriginStateID int, @DestStateID int, @RegionID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT *
	FROM viewShipperRateSheetRangeRate R
	WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = isnull(R.ShipperID, 0)
	  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = isnull(R.ProductGroupID, 0)
	  AND coalesce(nullif(@OriginStateID, 0), R.OriginStateID, 0) = isnull(R.OriginStateID, 0)
	  AND coalesce(nullif(@DestStateID, 0), R.DestStateID, 0) = isnull(R.DestStateID, 0)
	  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = isnull(R.RegionID, 0)
      AND (nullif(@RouteMiles, 0) IS NULL OR @RouteMiles BETWEEN MinRange AND MaxRange)
      AND (@StartDate BETWEEN EffectiveDate AND EndDate
	    OR @EndDate BETWEEN EffectiveDate AND EndDate
        OR EffectiveDate BETWEEN @StartDate AND @EndDate
        OR EndDate BETWEEN @StartDate AND @EndDate
		OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	ORDER BY EffectiveDate
GO
GRANT SELECT ON fnShipperRateSheetRangeRates_ForRange TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'fnShipperRouteRates_ForRange') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION fnShipperRouteRates_ForRange
GO 
/***********************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper RouteRate rows for the specified criteria
/***********************************/
CREATE FUNCTION fnShipperRouteRates_ForRange(@StartDate date, @EndDate date, @ShipperID int, @ProductGroupID int, @OriginID int, @DestinationID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.RouteID, R.ActualMiles, R.OriginID, R.DestinationID, R.ShipperID, R.ProductGroupID, R.Rate, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate, R.NextEffectiveDate
		, Origin = O.Name
		, OriginFull = O.FullName
		, Destination = D.Name
		, DestinationFull = D.FullName
		, Shipper = C.Name
		, ProductGroup = PG.Name
		, RateType = RT.Name
		, Uom = U.Name
		, UomShort = U.Abbrev
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
	FROM dbo.viewShipperRouteRate R
	JOIN viewOrigin O ON O.ID = R.OriginID
	JOIN viewDestination D ON D.ID = R.DestinationID
	LEFT JOIN tblCustomer C ON C.ID = R.ShipperID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblRateType RT ON RT.ID = R.RateTypeID
	LEFT JOIN tblUom U ON U.ID = R.UomID
	WHERE coalesce(nullif(@OriginID, 0), R.OriginID, 0) = isnull(R.OriginID, 0)
	  AND coalesce(nullif(@DestinationID, 0), R.DestinationID, 0) = isnull(R.DestinationID, 0)
	  AND coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = isnull(R.ShipperID, 0)
	  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = isnull(R.ProductGroupID, 0)
	  AND (@StartDate BETWEEN EffectiveDate AND EndDate
	     OR @EndDate BETWEEN EffectiveDate AND EndDate
	     OR EffectiveDate BETWEEN @StartDate AND @EndDate
	     OR EndDate BETWEEN @StartDate AND @EndDate
		OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	ORDER BY EffectiveDate
GO
GRANT SELECT ON fnShipperRouteRates_ForRange TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'fnShipperWaitFeeParameters_ForRange') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION fnShipperWaitFeeParameters_ForRange
GO
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper WaitFeeParameter rows for the specified criteria
/***********************************/
CREATE FUNCTION fnShipperWaitFeeParameters_ForRange(@StartDate date, @EndDate date, @ShipperID int, @ProductGroupID int, @OriginStateID int, @DestStateID int, @RegionID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ShipperID, R.ProductGroupID, R.OriginStateID, R.DestStateID, R.RegionID, R.SubUnitID, R.RoundingTypeID, R.ThresholdMinutes, R.EffectiveDate, R.EndDate, R.NextEffectiveDate
		, Shipper = C.Name
		, ProductGroup = PG.Name
		, OriginState = OS.FullName
		, OriginStateAbbrev = OS.Abbreviation
		, DestinationState = DS.FullName
		, DestinationStateAbbrev = DS.Abbreviation
		, Region = REG.Name
		, RoundingType = WFRT.Name
		, SubUnit = WFSU.Name
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
	FROM dbo.viewShipperWaitFeeParameter R 
	JOIN tblWaitFeeRoundingType WFRT ON WFRT.ID = R.RoundingTypeID
	JOIN tblWaitFeeSubUnit WFSU ON WFSU.ID = R.SubUnitID
	LEFT JOIN tblCustomer C ON C.ID = R.ShipperID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblState OS ON OS.ID = R.OriginStateID
	LEFT JOIN tblState DS ON DS.ID = R.DestStateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = isnull(R.ShipperID, 0)
	  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = isnull(R.ProductGroupID, 0)
	  AND coalesce(nullif(@OriginStateID, 0), R.OriginStateID, 0) = isnull(R.OriginStateID, 0)
	  AND coalesce(nullif(@DestStateID, 0), R.DestStateID, 0) = isnull(R.DestStateID, 0)
	  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = isnull(R.RegionID, 0)
	  AND (@StartDate BETWEEN EffectiveDate AND EndDate
        OR @EndDate BETWEEN EffectiveDate AND EndDate
        OR EffectiveDate BETWEEN @StartDate AND @EndDate
        OR EndDate BETWEEN @StartDate AND @EndDate
		OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	ORDER BY EffectiveDate
GO
GRANT SELECT ON fnShipperWaitFeeParameters_ForRange TO dispatchcrude_iis_acct
GO

/*************************************/
-- Date Created: 18 Jan 2015
-- Author: Kevin Alons
-- Purpose: handle specialized logic related to editing Route Rates (due to RouteID being "inputted" as Origin|Destination combination)
/*************************************/
CREATE TRIGGER trigViewShipperRouteRate_IU_Update ON viewShipperRouteRate INSTEAD OF INSERT, UPDATE  AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
		--PRINT 'ensure a Route record exists for the new Origin-Destination combo'
		INSERT INTO tblRoute (OriginID, DestinationID)
			SELECT DISTINCT OriginID, DestinationID FROM inserted
			EXCEPT SELECT OriginID, DestinationID FROM tblRoute
		
		-- PRINT 'Updating any existing record editable data'
		UPDATE tblShipperRouteRate
			SET ShipperID = nullif(i.ShipperID, 0)
				, ProductGroupID = nullif(i.ProductGroupID, 0)
				, RouteID = R.ID
				, Rate = i.Rate
				, EffectiveDate = i.EffectiveDate
				, EndDate = i.EndDate
				, RateTypeID = i.RateTypeID
				, UomID = i.UomID
				, LastChangeDateUTC = i.LastChangeDateUTC
				, LastChangedByUser = i.LastChangedByUser
		FROM tblShipperRouteRate X
		JOIN inserted i ON i.ID = X.ID
		JOIN tblRoute R ON R.OriginID = i.OriginID AND R.DestinationID = i.DestinationID 

		-- PRINT 'insert any new records'
		INSERT INTO tblShipperRouteRate (ShipperID, ProductGroupID, RouteID, Rate, EffectiveDate, EndDate, RateTypeID, UomID, CreatedByUser)
			SELECT nullif(ShipperID, 0), nullif(ProductGroupID, 0), R.ID, Rate, EffectiveDate, EndDate, RateTypeID, UomID, i.CreatedByUser
			FROM inserted i
			JOIN tblRoute R ON R.OriginID = i.OriginID AND R.DestinationID = i.DestinationID 
			WHERE ISNULL(i.ID, 0) = 0
	END TRY
	BEGIN CATCH
		DECLARE @error varchar(255)
		SET @error = ERROR_MESSAGE()
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END CATCH
END
GO

/*************************************/
-- Date Created: 18 Jan 2015
-- Author: Kevin Alons
-- Purpose: allow all editing to be done to viewShipperRouteRate (due to specialized logic related to RouteID)
/*************************************/
CREATE TRIGGER trigViewShipperRouteRate_D ON viewShipperRouteRate INSTEAD OF DELETE AS
BEGIN
	DELETE FROM tblShipperRouteRate WHERE ID IN (SELECT ID FROM deleted)
END
GO

GRANT SELECT, UPDATE, INSERT, DELETE ON viewShipperRouteRate TO dispatchcrude_iis_acct
GO

-------------------------------------------
-- END SHIPPER CHANGES
-------------------------------------------
GO
/*****************************************************/
-- Date Created: 14 Jan 2015
-- Author: Kevin Alons
-- Purpose: helper function to convert DeleteDateUTC = NULL to 1 (or 0)
CREATE FUNCTION fnIsActive(@DeleteDateUTC date) RETURNS bit AS
BEGIN
	DECLARE @ret bit
	SELECT @ret = CASE WHEN @DeleteDateUTC IS NULL THEN 1 ELSE 0 END
	RETURN (@ret) 
END
GO
GRANT EXECUTE ON fnIsActive TO dispatchcrude_iis_acct
GO

/*****************************************************/
-- Date Created: 14 Jan 2015
-- Author: Kevin Alons
-- Purpose: helper function to add 'Deleted: ' to "name" string based on DeleteDateUTC value
CREATE FUNCTION fnNameWithDeleted(@name varchar(255), @DeleteDateUTC date) RETURNS varchar(255) AS
BEGIN
	SELECT @name = CASE WHEN @DeleteDateUTC IS NULL THEN '' ELSE 'Deleted: ' END + @name
	RETURN (@name) 
END
GO
GRANT EXECUTE ON fnNameWithDeleted TO dispatchcrude_iis_acct
GO

/***************************************
-- Date Created: 18 Jan 2015
-- Author: Kevin Alons
-- Purpose: convert a string to an Integer value
***************************************/	
CREATE FUNCTION fnToInt(@value varchar(255)) RETURNS int AS
BEGIN
	DECLARE @ret int
	IF ISNUMERIC(@value) = 1
		SET @ret = CAST(@value as int)
	RETURN (@ret)
END
GO
GRANT EXECUTE ON fnToInt TO dispatchcrude_iis_acct
GO

/*****************************************
-- Date Created: 19 Jan 2015
-- Author: Kevin Alons
-- Purpose: return the tblOrder table witha Local OrderDate field added
*****************************************/
CREATE VIEW viewOrderBase AS
	SELECT O.*
		, OrderDate = cast(dbo.fnUTC_to_Local(O.OriginDepartTimeUTC, OO.TimeZoneID, OO.UseDST) as date) 
	FROM tblOrder O
	JOIN tblOrigin OO ON OO.ID = O.OriginID
GO
GRANT SELECT ON viewOrderBase TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Order records with "translated friendly" values for FK relationships
/***********************************/
ALTER VIEW [dbo].[viewOrder] AS
SELECT O.*
	, OPS.OrderStatus AS PrintStatus
FROM (
	SELECT O.*
	, Origin = vO.Name 
	, OriginFull = vO.FullName 
	, OriginState = vO.State 
	, OriginStateAbbrev = vO.StateAbbrev 
	, OriginStation = vO.Station
	, OriginCounty = vO.County 
	, vO.LeaseName
	, vO.LeaseNum
	, OriginRegion = vO.Region
	, OriginRegionID = vO.RegionID
	, OriginCountryID = vO.CountryID
	, OriginCountry = vO.Country
	, OriginCountryShort = vO.CountryShort
	, OriginLegalDescription = vO.LegalDescription 
	, OriginNDIC = vO.NDICFileNum 
	, OriginNDM = vO.NDM 
	, OriginCA = vO.CA 
	, OriginTimeZoneID = vO.TimeZoneID 
	, OriginUseDST = vO.UseDST 
	, OriginStateID = vO.StateID
	, vO.H2S
	, Destination = vD.Name 
	, DestinationFull = vD.FullName 
	, DestinationState = vD.State 
	, DestinationStateAbbrev = vD.StateAbbrev 
	, DestinationTypeID = vD.ID
	, vD.DestinationType
	, DestStation = vD.Station 
	, DestTimeZoneID = vD.TimeZoneID 
	, DestUseDST = vD.UseDST 
	, DestStateID = vD.StateID
	, DestRegion = vO.Region
	, DestRegionID = vO.RegionID
	, DestCountryID = vD.CountryID
	, DestCountry = vD.Country
	, DestCountryShort = vD.CountryShort
	, Customer = C.Name 
	, Carrier = CA.Name 
	, CarrierType = CT.Name 
	, OS.OrderStatus
	, OS.StatusNum
	, Driver = D.FullName 
	, DriverFirst = D.FirstName 
	, DriverLast = D.LastName 
	, Truck = TRU.FullName 
	, Trailer = TR1.FullName 
	, Trailer2 = TR2.FullName 
	, P.PriorityNum
	, TicketType = TT.Name 
	, DestTicketTypeID = vD.TicketTypeID 
	, DestTicketType = vD.TicketType 
	, Operator = OP.Name 
	, Producer = PR.Name 
	, Pumper = PU.FullName 
	, DriverNumber = D.IDNumber
	, CarrierNumber = CA.IDNumber
	, CarrierTypeID = CA.CarrierTypeID
	, TruckNumber = TRU.IDNumber 
	, TrailerNumber = TR1.IDNumber 
	, Trailer2Number = TR2.IDNumber 
	, Product = PRO.Name
	, ProductShort = PRO.ShortName 
	, PRO.ProductGroupID
	, PRO.ProductGroup
	, OriginUOM = OUom.Name 
	, OriginUomShort = OUom.Abbrev 
	, OriginTankID_Text = CASE WHEN ORT.TankNum = '*' THEN '*' + isnull(O.OriginTankNum, '?') ELSE ORT.TankNum END 
	, DestUOM = DUom.Name 
	, DestUomShort = DUom.Abbrev 
	, Active = cast((CASE WHEN O.DeleteDateUTC IS NULL THEN 1 ELSE 0 END) as bit) 
	, IsDeleted = cast((CASE WHEN O.DeleteDateUTC IS NOT NULL THEN 1 ELSE 0 END) as bit) 
	, PickupPrintStatus = PPS.Name 
	, PickupCompleted = PPS.IsCompleted 
	, DeliverPrintStatus = DPS.Name 
	, DeliverCompleted = DPS.IsCompleted 
	, PrintStatusID = CASE WHEN StatusID = 8 AND PPS.IsCompleted = 0 THEN 7
						   WHEN StatusID = 3 AND DPS.IsCompleted = 0 THEN 8
						ELSE StatusID END 
	, OriginTankText = CASE WHEN O.OriginTankID IS NULL OR ORT.TankNum = '*' THEN O.OriginTankNum ELSE ORT.TankNum END
	, OriginWaitNum = OWR.Num
	, OriginWaitDesc = OWR.Description
	, OriginWaitNumDesc = OWR.NumDesc
	, DestWaitNum = DWR.Num
	, DestWaitDesc = DWR.Description
	, DestWaitNumDesc = DWR.NumDesc
	, RejectNum = ORR.Num
	, RejectDesc = ORR.Description
	, RejectNumDesc = ORR.NumDesc
	, RerouteCount = (SELECT COUNT(1) FROM tblOrderReroute ORE WHERE ORE.OrderID = O.ID)
	, TicketCount = (SELECT COUNT(1) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND OT.DeleteDateUTC IS NULL)
	FROM dbo.viewOrderBase O
	LEFT JOIN dbo.viewOrigin vO ON vO.ID = O.OriginID
	LEFT JOIN dbo.viewDestination vD ON vD.ID = O.DestinationID
	LEFT JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
	JOIN dbo.tblOrderStatus AS OS ON OS.ID = O.StatusID
	LEFT JOIN dbo.tblCarrier CA ON CA.ID = O.CarrierID
	LEFT JOIN dbo.tblCarrierType CT ON CT.ID = CA.CarrierTypeID
	LEFT JOIN dbo.viewDriverBase D ON D.ID = O.DriverID
	LEFT JOIN dbo.viewTruck TRU ON TRU.ID = O.TruckID
	LEFT JOIN dbo.viewTrailer TR1 ON TR1.ID = O.TrailerID
	LEFT JOIN dbo.viewTrailer TR2 ON TR2.ID = O.Trailer2ID
	LEFT JOIN dbo.tblPriority P ON P.ID = O.PriorityID
	LEFT JOIN dbo.tblTicketType TT ON TT.ID = O.TicketTypeID
	LEFT JOIN dbo.tblOriginTank ORT ON ORT.ID = O.OriginTankID
	LEFT JOIN dbo.tblOperator OP ON OP.ID = O.OperatorID
	LEFT JOIN dbo.viewPumper PU ON PU.ID = O.PumperID
	LEFT JOIN dbo.tblProducer PR ON PR.ID = O.ProducerID
	LEFT JOIN dbo.viewProduct PRO ON PRO.ID = O.ProductID
	LEFT JOIN dbo.tblUom OUom ON OUom.ID = O.OriginUomID
	LEFT JOIN dbo.tblUom DUom ON DUom.ID = O.DestUomID
	LEFT JOIN dbo.tblPrintStatus PPS ON PPS.ID = O.PickupPrintStatusID
	LEFT JOIN dbo.tblPrintStatus DPS ON DPS.ID = O.DeliverPrintStatusID
	LEFT JOIN dbo.viewOriginWaitReason OWR ON OWR.ID = O.OriginWaitReasonID
	LEFT JOIN dbo.viewDestinationWaitReason DWR ON DWR.ID = O.DestWaitReasonID
	LEFT JOIN dbo.viewOrderRejectReason ORR ON ORR.ID = O.RejectReasonID
) O
LEFT JOIN dbo.tblOrderStatus OPS ON OPS.ID = O.PrintStatusID

GO

CREATE NONCLUSTERED INDEX idxOrderSettlementCarrier_Batch ON tblOrderSettlementCarrier (BatchID) INCLUDE (OrderID)
GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
/***********************************/
ALTER PROCEDURE [dbo].[spOrdersExportCarrier]
(
  @StartDate datetime = NULL
, @EndDate datetime = NULL
, @CarrierID int = -1 -- all customers
, @ProductID int = -1 -- all products
, @BatchID int = NULL  -- either show unbatched (NULL), or the specified batch orders
) AS BEGIN

	SELECT @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	
	SELECT DISTINCT OE.* 
	FROM dbo.viewOrder_Financial_Carrier OE
	WHERE ID IN (
		SELECT O.ID
		FROM viewOrderBase O
		LEFT JOIN tblOrderSettlementCarrier ISC ON ISC.OrderID = O.ID
		WHERE O.StatusID IN (4)  
		  AND (@CarrierID=-1 OR O.CarrierID=@CarrierID) 
		  AND (@ProductID=-1 OR O.ProductID=@ProductID) 
		  AND (@StartDate IS NULL OR O.OrderDate >= @StartDate) 
		  AND (@EndDate IS NULL OR O.OrderDate <= @EndDate)
		  AND ((@BatchID IS NULL AND ISC.BatchID IS NULL) OR ISC.BatchID = @BatchID)
	)
	ORDER BY OE.OriginDepartTimeUTC
END

GO

CREATE NONCLUSTERED INDEX idxOrderSettlementShipper_Batch ON tblOrderSettlementShipper (BatchID) INCLUDE (OrderID)
GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
/***********************************/
ALTER PROCEDURE [dbo].[spOrdersExportCustomer]
(
  @StartDate datetime = NULL
, @EndDate datetime = NULL
, @CustomerID int = -1 -- all customers
, @ProductID int = -1 -- all products
, @BatchID int = NULL  -- either show unbatched (NULL), or the specified batch orders
) AS BEGIN

	SELECT @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	
	SELECT DISTINCT OE.* 
	FROM dbo.viewOrder_Financial_Customer OE
	WHERE ID IN (
		SELECT O.ID
		FROM viewOrderBase O
		LEFT JOIN tblOrderSettlementShipper ISC ON ISC.OrderID = O.ID
		WHERE O.StatusID IN (4)  
		  AND (@CustomerID=-1 OR O.CustomerID=@CustomerID) 
		  AND (@ProductID=-1 OR O.ProductID=@ProductID) 
		  AND (@StartDate IS NULL OR O.OrderDate >= @StartDate) 
		  AND (@EndDate IS NULL OR O.OrderDate <= @EndDate)
		  AND ((@BatchID IS NULL AND ISC.BatchID IS NULL) OR ISC.BatchID = @BatchID)
	)
	ORDER BY OE.OriginDepartTimeUTC
END

GO

INSERT INTO tblSetting (ID, Name, SettingTypeID, Value, Category, CreateDateUTC, CreatedByUser, ReadOnly)
	VALUES (38, 'Disable AJAX', 2, 'false', 'System Wide', getutcdate(), 'System', 0)

COMMIT
SET NOEXEC OFF
RETURN

/*
select * from tblstate
select * from viewCarrierWaitFeeParameter order by effectivedate
SELECT ID, Name = CASE WHEN DeleteDateUTC IS NOT NULL THEN 'Deleted:' ELSE '' END + Name, Active = CASE WHEN DeleteDateUTC IS NULL THEN 1 ELSE 0 END FROM dbo.tblCarrier UNION SELECT NULL, '(All)', 1 ORDER BY Active DESC, Name
insert into viewCarrierWaitFeeParameter (id, effectivedate, istermination, roundingtypeid, subunitid, thresholdminutes, createdbyUser, enddate)
	values (0, '2012/1/1', 0, 4, 30, 60, 'kalons', '6/6/2012')
	
update viewCarrierWaitFeeParameter set enddate = '6/7/2012' where id = 9
update viewCarrierWaitFeeParameter set enddate = '12/30/2012' where id = 9
update viewCarrierWaitFeeParameter set enddate = '6/7/2014' where id = 1
select * from dbo.fnCarrierWaitFeeParameter('6/7/2012', null, null, null, null, null, 1) X
SELECT ID, Name FROM dbo.tblRateType ORDER BY Name
select * from viewcarrierassessorialrate

*/