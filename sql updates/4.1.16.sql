SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.1.15'
SELECT  @NewVersion = '4.1.16'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1834 - Convert several smaller pages to MVC. (Origin/Destination/Truck/Trailer Types, Regions, Equipment Manufacturers)'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO



/* Add delete tracking columns to the tables that are missing these columns */
ALTER TABLE tblDestinationType
	ADD DeleteDateUTC SMALLDATETIME NULL
		, DeletedByUser VARCHAR(100) NULL
GO

ALTER TABLE tblOriginType
	ADD DeleteDateUTC SMALLDATETIME NULL
		, DeletedByUser VARCHAR(100) NULL
GO

ALTER TABLE tblTruckType
	ADD DeleteDateUTC SMALLDATETIME NULL
		, DeletedByUser VARCHAR(100) NULL
GO

ALTER TABLE tblTrailerType
	ADD DeleteDateUTC SMALLDATETIME NULL
		, DeletedByUser VARCHAR(100) NULL
GO

ALTER TABLE tblRegion
	ADD DeleteDateUTC SMALLDATETIME NULL
		, DeletedByUser VARCHAR(100) NULL
GO



/* Update existing page view permission to fit with new permissions being added below */
UPDATE aspnet_Roles
SET FriendlyName = 'Destination Types - View'
WHERE RoleName = 'viewDestinationTypes'
GO

UPDATE aspnet_Roles
SET FriendlyName = 'Origin Types - View'
WHERE RoleName = 'viewOriginTypes'
GO

UPDATE aspnet_Roles
SET FriendlyName = 'Truck Types - View'
WHERE RoleName = 'viewTruckTypes'
GO

UPDATE aspnet_Roles
SET FriendlyName = 'Trailer Types - View'
WHERE RoleName = 'viewTrailerTypes'
GO

UPDATE aspnet_Roles
SET FriendlyName = 'Regions - View'
WHERE RoleName = 'viewRegionMaintenance'
GO

UPDATE aspnet_Roles
SET FriendlyName = 'Manufacturers - View'
WHERE RoleName = 'viewDriverEquipmentManufacturers'
GO



/* Add new permissions for the pages being updated */
EXEC spAddNewPermission 'createDestinationTypes', 'Allow user to create Destination Types', 'System Data Types', 'Destination Types - Create'
GO
EXEC spAddNewPermission 'editDestinationTypes', 'Allow user to edit Destination Types', 'System Data Types', 'Destination Types - Edit'
GO
EXEC spAddNewPermission 'deactivateDestinationTypes', 'Allow user to deactivate Destination Types', 'System Data Types', 'Destination Types - Deactivate'
GO

EXEC spAddNewPermission 'createOriginTypes', 'Allow user to create Origin Types', 'System Data Types', 'Origin Types - Create'
GO
EXEC spAddNewPermission 'editOriginTypes', 'Allow user to edit Origin Types', 'System Data Types', 'Origin Types - Edit'
GO
EXEC spAddNewPermission 'deactivateOriginTypes', 'Allow user to deactivate Origin Types', 'System Data Types', 'Origin Types - Deactivate'
GO

EXEC spAddNewPermission 'createTruckTypes', 'Allow user to create Truck Types', 'System Data Types', 'Truck Types - Create'
GO
EXEC spAddNewPermission 'editTruckTypes', 'Allow user to edit Truck Types', 'System Data Types', 'Truck Types - Edit'
GO
EXEC spAddNewPermission 'deactivateTruckTypes', 'Allow user to deactivate Truck Types', 'System Data Types', 'Truck Types - Deactivate'
GO

EXEC spAddNewPermission 'createTrailerTypes', 'Allow user to create Trailer Types', 'System Data Types', 'Trailer Types - Create'
GO
EXEC spAddNewPermission 'editTrailerTypes', 'Allow user to edit Trailer Types', 'System Data Types', 'Trailer Types - Edit'
GO
EXEC spAddNewPermission 'deactivateTrailerTypes', 'Allow user to deactivate Trailer Types', 'System Data Types', 'Trailer Types - Deactivate'
GO

EXEC spAddNewPermission 'createRegionMaintenance', 'Allow user to create Regions', 'Maintenance', 'Regions - Create'
GO
EXEC spAddNewPermission 'editRegionMaintenance', 'Allow user to edit Regions', 'Maintenance', 'Regions - Edit'
GO
EXEC spAddNewPermission 'deactivateRegionMaintenance', 'Allow user to deactivate Regions', 'Maintenance', 'Regions - Deactivate'
GO

EXEC spAddNewPermission 'createDriverEquipmentManufacturers', 'Allow user to create Driver Equipment Manufacturers', 'Maintenance - Driver Equipment', 'Manufacturers - Create'
GO
EXEC spAddNewPermission 'editDriverEquipmentManufacturers', 'Allow user to edit Driver Equipment Manufacturers', 'Maintenance - Driver Equipment', 'Manufacturers - Edit'
GO
EXEC spAddNewPermission 'deactivateDriverEquipmentManufacturers', 'Allow user to deactivate Driver Equipment Manufacturers', 'Maintenance - Driver Equipment', 'Manufacturers - Deactivate'
GO



/* Refresh/rebuild all views/fn/sp's that could be affected by the above changes */
EXEC _spRefreshAllViews
GO
EXEC _spRebuildAllObjects
GO
EXEC _spRecompileAllStoredProcedures
GO



COMMIT
SET NOEXEC OFF