-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.2.11'
SELECT  @NewVersion = '3.3.0'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'Revision to Report Center showing hierarchical "tree-view" list of Column choices'
	UNION
	SELECT @NewVersion, 0, 'ALTER TABLE tblReportColumnDefinition.Caption from varchar(50) -> varchar(255)'
GO

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
ALTER TABLE dbo.tblReportColumnDefinition
	DROP CONSTRAINT FK_ReportColumnDefinition_ReportDefinition
GO
ALTER TABLE dbo.tblReportDefinition SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblReportColumnDefinition
	DROP CONSTRAINT FK_ReportColumnDefinition_FilterType
GO
ALTER TABLE dbo.tblReportFilterType SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblReportColumnDefinition
	DROP CONSTRAINT DF_ReportColumnDefinition_FilterAllowCustomText
GO
ALTER TABLE dbo.tblReportColumnDefinition
	DROP CONSTRAINT DF_ReportColumnDefinition_AllowedRoles
GO
ALTER TABLE dbo.tblReportColumnDefinition
	DROP CONSTRAINT FK_ReportColumnDefinition_OrderSingleExport
GO
CREATE TABLE dbo.Tmp_tblReportColumnDefinition
	(
	ID int NOT NULL IDENTITY (100000, 1),
	ReportID int NOT NULL,
	DataField varchar(800) NOT NULL,
	Caption varchar(255) NULL,
	DataFormat varchar(255) NULL,
	FilterDataField varchar(50) NULL,
	FilterTypeID int NOT NULL,
	FilterDropDownSql varchar(255) NULL,
	FilterAllowCustomText bit NOT NULL,
	AllowedRoles varchar(1000) NOT NULL,
	OrderSingleExport bit NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_tblReportColumnDefinition SET (LOCK_ESCALATION = TABLE)
GO
GRANT SELECT ON dbo.Tmp_tblReportColumnDefinition TO dispatchcrude_iis_acct  AS dbo
GO
ALTER TABLE dbo.Tmp_tblReportColumnDefinition ADD CONSTRAINT
	DF_ReportColumnDefinition_FilterAllowCustomText DEFAULT ((0)) FOR FilterAllowCustomText
GO
ALTER TABLE dbo.Tmp_tblReportColumnDefinition ADD CONSTRAINT
	DF_ReportColumnDefinition_AllowedRoles DEFAULT ('*') FOR AllowedRoles
GO
ALTER TABLE dbo.Tmp_tblReportColumnDefinition ADD CONSTRAINT
	FK_ReportColumnDefinition_OrderSingleExport DEFAULT ((0)) FOR OrderSingleExport
GO
SET IDENTITY_INSERT dbo.Tmp_tblReportColumnDefinition ON
GO
IF EXISTS(SELECT * FROM dbo.tblReportColumnDefinition)
	 EXEC('INSERT INTO dbo.Tmp_tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport)
		SELECT ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport FROM dbo.tblReportColumnDefinition WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_tblReportColumnDefinition OFF
GO
ALTER TABLE dbo.tblReportColumnDefinitionBaseFilter
	DROP CONSTRAINT FK_ReportColumnDefinitionBaseFilter_ReportColumn
GO
ALTER TABLE dbo.tblUserReportColumnDefinition
	DROP CONSTRAINT FK_UserReportColumnDefinition_ReportColumn
GO
ALTER TABLE dbo.tblReportColumnDefinitionCountryException
	DROP CONSTRAINT FK_ReportColumnDefinitionCountryException_ReportColumnDefinition
GO
DROP TABLE dbo.tblReportColumnDefinition
GO
EXECUTE sp_rename N'dbo.Tmp_tblReportColumnDefinition', N'tblReportColumnDefinition', 'OBJECT' 
GO
ALTER TABLE dbo.tblReportColumnDefinition ADD CONSTRAINT
	PK_ReportColumnDefinition PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE NONCLUSTERED INDEX udxReportColumnDefinition_Report_Caption ON dbo.tblReportColumnDefinition
	(
	ReportID,
	Caption
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.tblReportColumnDefinition ADD CONSTRAINT
	FK_ReportColumnDefinition_FilterType FOREIGN KEY
	(
	FilterTypeID
	) REFERENCES dbo.tblReportFilterType
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblReportColumnDefinition ADD CONSTRAINT
	FK_ReportColumnDefinition_ReportDefinition FOREIGN KEY
	(
	ReportID
	) REFERENCES dbo.tblReportDefinition
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblReportColumnDefinitionCountryException ADD CONSTRAINT
	FK_ReportColumnDefinitionCountryException_ReportColumnDefinition FOREIGN KEY
	(
	ReportColumnID
	) REFERENCES dbo.tblReportColumnDefinition
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblReportColumnDefinitionCountryException SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblUserReportColumnDefinition ADD CONSTRAINT
	FK_UserReportColumnDefinition_ReportColumn FOREIGN KEY
	(
	ReportColumnID
	) REFERENCES dbo.tblReportColumnDefinition
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblUserReportColumnDefinition SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblReportColumnDefinitionBaseFilter ADD CONSTRAINT
	FK_ReportColumnDefinitionBaseFilter_ReportColumn FOREIGN KEY
	(
	ReportColumnID
	) REFERENCES dbo.tblReportColumnDefinition
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblReportColumnDefinitionBaseFilter SET (LOCK_ESCALATION = TABLE)
GO

/**************************************/
delete from tblUserReportColumnDefinition where ReportColumnID = 37
DELETE FROM tblReportColumnDefinition WHERE ID = 37

UPDATE tblReportColumnDefinition
	SET Caption = X.caption
FROM tblReportColumnDefinition RCD
JOIN (
	SELECT id = 1, caption = 'GENERAL | Carrier'
	UNION SELECT id = 2, caption = 'ORIGIN | GENERAL | Shipper'
	UNION SELECT id = 3, caption = 'DESTINATION | GENERAL | Destination'
	UNION SELECT id = 4, caption = 'TRIP | TIMESTAMPS | Date'
	UNION SELECT id = 5, caption = 'GENERAL | Order #'
	UNION SELECT id = 6, caption = 'ORIGIN | GENERAL | Origin'
	UNION SELECT id = 7, caption = 'ORIGIN | VOLUMES | BBLS | Origin GSV'
	UNION SELECT id = 8, caption = 'ORIGIN | VOLUMES | BBLS | Origin GOV'
	UNION SELECT id = 9, caption = 'ORIGIN | VOLUMES | BBLS | Origin NSV'
	UNION SELECT id = 10, caption = 'GENERAL | Product'
	UNION SELECT id = 11, caption = 'GENERAL | Order Rejected?'
	UNION SELECT id = 12, caption = 'GENERAL | Status'
	UNION SELECT id = 13, caption = 'GENERAL | Priority'
	UNION SELECT id = 14, caption = 'TRIP | TIMESTAMPS | Due Date'
	UNION SELECT id = 15, caption = 'TRIP | TIMESTAMPS | Origin Arrival'
	UNION SELECT id = 16, caption = 'TRIP | TIMESTAMPS | Origin Departure'
	UNION SELECT id = 17, caption = 'TRIP | TIMESTAMPS | Origin Minutes'
	UNION SELECT id = 18, caption = 'TRIP | TIMESTAMPS | Origin Wait Notes'
	UNION SELECT id = 19, caption = 'ORIGIN | GENERAL | Origin BOL #'
	UNION SELECT id = 20, caption = 'TRIP | TIMESTAMPS | Dest Arrival'
	UNION SELECT id = 21, caption = 'TRIP | TIMESTAMPS | Dest Departure'
	UNION SELECT id = 22, caption = 'TRIP | TIMESTAMPS | Dest Minutes'
	UNION SELECT id = 23, caption = 'TRIP | TIMESTAMPS | Dest Wait Notes'
	UNION SELECT id = 24, caption = 'DESTINATION | GENERAL | Dest BOL #'
	UNION SELECT id = 25, caption = 'DESTINATION | VOLUMES | BBLS | Dest GOV'
	UNION SELECT id = 26, caption = 'DESTINATION | VOLUMES | BBLS | Dest NSV'
	UNION SELECT id = 27, caption = 'GENERAL | Driver'
	UNION SELECT id = 28, caption = 'GENERAL | Truck'
	UNION SELECT id = 29, caption = 'GENERAL | Trailer 1'
	UNION SELECT id = 30, caption = 'GENERAL | Trailer 2'
	UNION SELECT id = 31, caption = 'ORIGIN | GENERAL | Operator'
	UNION SELECT id = 32, caption = 'ORIGIN | GENERAL | Pumper'
	UNION SELECT id = 33, caption = 'GENERAL | Order Type'
	UNION SELECT id = 34, caption = 'GENERAL | Order Reject Notes'
	UNION SELECT id = 35, caption = 'GENERAL | Chain-Up'
	UNION SELECT id = 36, caption = 'TRIP | MILES | Origin Truck Mileage'
	UNION SELECT id = 38, caption = 'TRIP | MILES | Dest Truck Mileage'
	UNION SELECT id = 40, caption = 'GENERAL | Audit Notes'
	UNION SELECT id = 41, caption = 'TRIP | MILES | Actual Miles'
	UNION SELECT id = 42, caption = 'ORIGIN | GENERAL | Producer'
	UNION SELECT id = 43, caption = 'TICKETS | PRODSPECS | DESTINATION | Dest BS&W'
	UNION SELECT id = 44, caption = 'TICKETS | PRODSPECS | DESTINATION | Dest Gravity'
	UNION SELECT id = 45, caption = 'TICKETS | PRODSPECS | DESTINATION | Dest Temp'
	UNION SELECT id = 46, caption = 'DESTINATION | GENERAL | Dest Open Meters'
	UNION SELECT id = 47, caption = 'DESTINATION | GENERAL | Dest Close Meters'
	UNION SELECT id = 48, caption = 'ORIGIN | GENERAL | Origin UOM'
	UNION SELECT id = 49, caption = 'DESTINATION | GENERAL | Dest UOM'
	UNION SELECT id = 50, caption = 'GENERAL | Dispatch Notes'
	UNION SELECT id = 51, caption = 'GENERAL | Driver Notes'
	UNION SELECT id = 52, caption = 'GENERAL | Dispatch Confirm #'
	UNION SELECT id = 53, caption = 'ORIGIN | GENERAL | Origin State'
	UNION SELECT id = 54, caption = 'ORIGIN | GENERAL | Origin ST'
	UNION SELECT id = 55, caption = 'ORIGIN | GENERAL | Origin Station'
	UNION SELECT id = 56, caption = 'ORIGIN | GENERAL | Origin Lease Name'
	UNION SELECT id = 57, caption = 'ORIGIN | GENERAL | Origin Lease #'
	UNION SELECT id = 58, caption = 'ORIGIN | GENERAL | Origin Legal Desc'
	UNION SELECT id = 59, caption = 'ORIGIN | GENERAL | Origin NDIC'
	UNION SELECT id = 60, caption = 'ORIGIN | GENERAL | Origin NDM'
	UNION SELECT id = 61, caption = 'ORIGIN | GENERAL | Origin CA'
	UNION SELECT id = 62, caption = 'TRIP | TIMESTAMPS | Origin TZ'
	UNION SELECT id = 63, caption = 'TRIP | TIMESTAMPS | Origin Use DST'
	UNION SELECT id = 64, caption = 'ORIGIN | GENERAL | H2S'
	UNION SELECT id = 65, caption = 'DESTINATION | GENERAL | Dest State'
	UNION SELECT id = 66, caption = 'DESTINATION | GENERAL | Destination ST'
	UNION SELECT id = 67, caption = 'DESTINATION | GENERAL | Dest Station'
	UNION SELECT id = 68, caption = 'TRIP | TIMESTAMPS | Dest TZ'
	UNION SELECT id = 69, caption = 'TRIP | TIMESTAMPS | Dest Use DST'
	UNION SELECT id = 70, caption = 'GENERAL | Carrier Type'
	UNION SELECT id = 71, caption = 'GENERAL | Driver First'
	UNION SELECT id = 72, caption = 'GENERAL | Driver Last'
	UNION SELECT id = 73, caption = 'DESTINATION | GENERAL | Dest Ticket Type'
	UNION SELECT id = 74, caption = 'GENERAL | Driver #'
	UNION SELECT id = 75, caption = 'GENERAL | Carrier #'
	UNION SELECT id = 76, caption = 'GENERAL | Product Group'
	UNION SELECT id = 77, caption = 'ORIGIN | GENERAL |Origin UOM Short'
	UNION SELECT id = 78, caption = 'DESTINATION | GENERAL | Dest UOM Short'
	UNION SELECT id = 79, caption = 'TRIP | TIMESTAMPS | Transit Min'
	UNION SELECT id = 80, caption = 'TICKETS | GENERAL | Ticket Count'
	UNION SELECT id = 81, caption = 'TRIP | Reroute Count'
	UNION SELECT id = 82, caption = 'TRIP | TIMESTAMPS | Total Minutes'
	UNION SELECT id = 83, caption = 'TRIP | TIMESTAMPS | Total Minutes (billable)'
	UNION SELECT id = 84, caption = 'TICKETS | GENERAL | Ticket Type'
	UNION SELECT id = 85, caption = 'TICKETS | GENERAL | Ticket #'
	UNION SELECT id = 86, caption = 'TICKETS | GENERAL | Ticket BOL #'
	UNION SELECT id = 87, caption = 'TICKETS | GENERAL | Ticket Tank #'
	UNION SELECT id = 88, caption = '(blank)'
	UNION SELECT id = 89, caption = 'TICKETS | GENERAL | Ticket Bottom FT'
	UNION SELECT id = 90, caption = 'TICKETS | GENERAL | Ticket Bottom IN'
	UNION SELECT id = 91, caption = 'TICKETS | GENERAL | Ticket Bottom Q'
	UNION SELECT id = 92, caption = 'TICKETS | GENERAL | Ticket Open FT'
	UNION SELECT id = 93, caption = 'TICKETS | GENERAL | Ticket Open IN'
	UNION SELECT id = 94, caption = 'TICKETS | GENERAL | Ticket Open Q'
	UNION SELECT id = 95, caption = 'TICKETS | GENERAL | Ticket Close FT'
	UNION SELECT id = 96, caption = 'TICKETS | GENERAL | Ticket Close IN'
	UNION SELECT id = 97, caption = 'TICKETS | GENERAL | Ticket Close Q'
	UNION SELECT id = 98, caption = 'TICKETS | GENERAL | Ticket Open Reading'
	UNION SELECT id = 99, caption = 'TICKETS | GENERAL | Ticket Close Reading'
	UNION SELECT id = 100, caption = 'TICKETS | PRODSPECS | ORIGIN | Ticket Corr. API Gravity'
	UNION SELECT id = 101, caption = 'TICKET | VOLUMES | BBLS | Ticket GSV'
	UNION SELECT id = 102, caption = 'TICKET | VOLUMES | BBLS | Ticket GOV'
	UNION SELECT id = 103, caption = 'TICKET | VOLUMES | BBLS | Ticket NSV'
	UNION SELECT id = 104, caption = 'TICKETS | GENERAL | Ticket Seal Off'
	UNION SELECT id = 105, caption = 'TICKETS | GENERAL | Ticket Seal On'
	UNION SELECT id = 106, caption = 'TICKETS | PRODSPECS | ORIGIN | Ticket Obs Temp'
	UNION SELECT id = 107, caption = 'TICKETS | PRODSPECS | ORIGIN | Ticket Obs Gravity'
	UNION SELECT id = 108, caption = 'TICKETS | PRODSPECS | ORIGIN | Ticket BS&W'
	UNION SELECT id = 109, caption = 'TICKETS | GENERAL | Ticket Rejected'
	UNION SELECT id = 110, caption = 'TICKETS | GENERAL | Ticket Reject Notes'
	UNION SELECT id = 111, caption = 'TRIP | Prev Destinations'
	UNION SELECT id = 112, caption = 'TRIP | Reroute Users'
	UNION SELECT id = 113, caption = 'TRIP | Reroute Notes'
	UNION SELECT id = 114, caption = 'TRIP | TIMESTAMPS | Origin Wait Num+Desc'
	UNION SELECT id = 115, caption = 'TRIP | TIMESTAMPS | Dest Wait Num+Desc'
	UNION SELECT id = 116, caption = 'GENERAL | Order Reject Num+Desc'
	UNION SELECT id = 117, caption = 'TRIP | TIMESTAMPS | Origin Wait Num'
	UNION SELECT id = 118, caption = 'TRIP | TIMESTAMPS | Dest Wait Num'
	UNION SELECT id = 119, caption = 'GENERAL | Order Reject Num'
	UNION SELECT id = 120, caption = 'TRIP | TIMESTAMPS | Origin Wait Desc'
	UNION SELECT id = 121, caption = 'TRIP | TIMESTAMPS | Dest Wait Desc'
	UNION SELECT id = 122, caption = 'GENERAL | Order Reject Desc'
	UNION SELECT id = 123, caption = 'TICKETS | PRODSPECS | ORIGIN | Ticket Open Temp'
	UNION SELECT id = 124, caption = 'TICKETS | PRODSPECS | ORIGIN | Ticket Close Temp'
	UNION SELECT id = 125, caption = 'TICKETS | GENERAL | Ticket Reject Num'
	UNION SELECT id = 126, caption = 'TICKETS | GENERAL | Ticket Reject Desc'
	UNION SELECT id = 127, caption = 'TICKETS | GENERAL | Ticket Reject Num+Desc'
	UNION SELECT id = 128, caption = 'ORIGIN | GENERAL | Origin County'
	UNION SELECT id = 129, caption = 'TRIP | TIMESTAMPS | Origin Minutes (billable)'
	UNION SELECT id = 130, caption = 'TRIP | TIMESTAMPS | Dest Minutes (billable)'
	UNION SELECT id = 131, caption = 'TICKETS | GENERAL | Ticket Open Total Q'
	UNION SELECT id = 132, caption = 'TRIP | Reroute Date'
	UNION SELECT id = 133, caption = 'TICKETS | GENERAL | Ticket Meter Factor'
	UNION SELECT id = 134, caption = 'TICKETS | GENERAL | Ticket Open Meter Units'
	UNION SELECT id = 135, caption = 'TICKETS | GENERAL | Ticket Close Meter Units'
	UNION SELECT id = 136, caption = 'DESTINATION | GENERAL | Dest Railcar #'
	UNION SELECT id = 137, caption = 'DESTINATION | GENERAL | BOL Trailer Water Cap.'
	UNION SELECT id = 138, caption = 'TRIP | GPS | Origin Driver GPS'
	UNION SELECT id = 139, caption = 'TRIP | GPS | Origin Coordinates'
	UNION SELECT id = 140, caption = 'TRIP | GPS | Origin DTP'
	UNION SELECT id = 141, caption = 'TRIP | GPS | Origin GPS Verified?'
	UNION SELECT id = 142, caption = 'TRIP | GPS | Destination Driver GPS'
	UNION SELECT id = 143, caption = 'TRIP | GPS | Destination Coordinates'
	UNION SELECT id = 144, caption = 'TRIP | GPS | Destination DTP'
	UNION SELECT id = 145, caption = 'TRIP | GPS | Destination GPS Verified?'
	UNION SELECT id = 146, caption = 'SETTLEMENT | SHIPPER | Shipper H2S Rate'
	UNION SELECT id = 147, caption = 'SETTLEMENT | SHIPPER | Shipper H2S $$'
	UNION SELECT id = 148, caption = 'SETTLEMENT | SHIPPER | Shipper Chainup $$'
	UNION SELECT id = 149, caption = 'SETTLEMENT | SHIPPER | Shipper Tax Rate'
	UNION SELECT id = 150, caption = 'SETTLEMENT | SHIPPER | Shipper Reroute $$'
	UNION SELECT id = 151, caption = 'SETTLEMENT | SHIPPER | Shipper Reroute $$'
	UNION SELECT id = 152, caption = 'SETTLEMENT | SHIPPER | Shipper Origin Wait Rate'
	UNION SELECT id = 153, caption = 'SETTLEMENT | SHIPPER | Shipper Origin Wait Fee'
	UNION SELECT id = 154, caption = 'SETTLEMENT | SHIPPER | Shipper Dest Wait Rate'
	UNION SELECT id = 155, caption = 'SETTLEMENT | SHIPPER | Shipper Dest Wait Fee'
	UNION SELECT id = 156, caption = 'SETTLEMENT | SHIPPER | Shipper Total Wait Fee'
	UNION SELECT id = 157, caption = 'SETTLEMENT | SHIPPER | Shipper Total Fee'
	UNION SELECT id = 158, caption = 'SETTLEMENT | SHIPPER | Shipper Settlement UOM'
	UNION SELECT id = 159, caption = 'SETTLEMENT | SHIPPER | Shipper Min Settle Units'
	UNION SELECT id = 160, caption = 'SETTLEMENT | SHIPPER | Shipper Route Rate'
	UNION SELECT id = 161, caption = 'SETTLEMENT | SHIPPER | Shipper Route $$'
	UNION SELECT id = 162, caption = 'SETTLEMENT | SHIPPER | Shipper Batch #'
	UNION SELECT id = 163, caption = 'SETTLEMENT | SHIPPER | Shipper Fuel Surcharge $$'
	UNION SELECT id = 164, caption = 'SETTLEMENT | CARRIER | Carrier H2S Rate'
	UNION SELECT id = 165, caption = 'SETTLEMENT | CARRIER | Carrier H2S $$'
	UNION SELECT id = 166, caption = 'SETTLEMENT | CARRIER | Carrier Chainup $$'
	UNION SELECT id = 167, caption = 'SETTLEMENT | CARRIER | Carrier Tax Rate'
	UNION SELECT id = 168, caption = 'SETTLEMENT | CARRIER | Carrier Reroute $$'
	UNION SELECT id = 169, caption = 'SETTLEMENT | CARRIER | Carrier Reroute $$'
	UNION SELECT id = 170, caption = 'SETTLEMENT | CARRIER | Carrier Origin Wait Rate'
	UNION SELECT id = 171, caption = 'SETTLEMENT | CARRIER | Carrier Origin Wait Fee'
	UNION SELECT id = 172, caption = 'SETTLEMENT | CARRIER | Carrier Dest Wait Rate'
	UNION SELECT id = 173, caption = 'SETTLEMENT | CARRIER | Carrier Dest Wait Fee'
	UNION SELECT id = 174, caption = 'SETTLEMENT | CARRIER | Carrier Total Wait Fee'
	UNION SELECT id = 175, caption = 'SETTLEMENT | CARRIER | Carrier Total Fee'
	UNION SELECT id = 176, caption = 'SETTLEMENT | CARRIER | Carrier Settlement UOM'
	UNION SELECT id = 177, caption = 'SETTLEMENT | CARRIER | Carrier Min Settle Units'
	UNION SELECT id = 178, caption = 'SETTLEMENT | CARRIER | Carrier Route Rate'
	UNION SELECT id = 179, caption = 'SETTLEMENT | CARRIER | Carrier Route $$'
	UNION SELECT id = 180, caption = 'SETTLEMENT | CARRIER | Carrier Batch #'
	UNION SELECT id = 181, caption = 'SETTLEMENT | CARRIER | Carrier Fuel Surcharge $$'
	UNION SELECT id = 182, caption = 'SETTLEMENT | SHIPPER | Shipper Settlement Volume'
	UNION SELECT id = 183, caption = 'SETTLEMENT | CARRIER | Carrier Settlement Volume'
	UNION SELECT id = 184, caption = 'DESTINATION | GENERAL | Shipper Destination Code'
	UNION SELECT id = 185, caption = 'SETTLEMENT | SHIPPER | Shipper Fuel Surcharge Rate'
	UNION SELECT id = 186, caption = 'SETTLEMENT | CARRIER | Carrier Fuel Surcharge Rate'
	UNION SELECT id = 187, caption = 'SETTLEMENT | SHIPPER | Shipper Split Load $$'
	UNION SELECT id = 188, caption = 'SETTLEMENT | CARRIER | Carrier Split Load $$'
	UNION SELECT id = 90001, caption = 'TRIP | MILES | Actual Driver Miles'
	UNION SELECT id = 90002, caption = 'TICKETS | PRODSPECS | Unit of Temperature'
) X ON X.id = RCD.id

SET IDENTITY_INSERT tblReportColumnDefinition ON
INSERT INTO tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport)
	SELECT X.newID, RCD.ReportID, X.DataField, X.Caption, RCD.DataFormat, RCD.FilterDataField, RCD.FilterTypeID, RCD.FilterDropDownSql, RCD.FilterAllowCustomText, RCD.AllowedRoles, RCD.OrderSingleExport
	FROM tblReportColumnDefinition RCD
	JOIN (
		SELECT newID = 189, SourceID = 25, datafield = 'dbo.fnConvertUOM(DestGrossUnits, DestUomID, 3)', caption = 'DESTINATION | VOLUMES | CM | Dest GOV'
		UNION SELECT newID = 190, SourceID = 26, datafield = 'dbo.fnConvertUOM(DestNetUnits, DestUomID, 3)', caption = 'DESTINATION | VOLUMES | CM | Dest NSV'
		UNION SELECT newID = 191, SourceID = 25, datafield = 'dbo.fnConvertUOM(DestGrossStdUnits, DestUomID, 2)', caption = 'DESTINATION | VOLUMES | GAL | Dest GOV'
		UNION SELECT newID = 192, SourceID = 26, datafield = 'dbo.fnConvertUOM(DestNetUnits, DestUomID, 2)', caption = 'DESTINATION | VOLUMES | GAL | Dest NSV'
		UNION SELECT newID = 193, SourceID = 8, datafield = 'dbo.fnConvertUOM(OriginGrossUnits, OriginUomID, 3)', caption = 'ORIGIN | VOLUMES | CM | Origin GOV'
		UNION SELECT newID = 194, SourceID = 7, datafield = 'dbo.fnConvertUOM(OriginGrossStdUnits, OriginUomID, 3)', caption = 'ORIGIN | VOLUMES | CM | Origin GSV'
		UNION SELECT newID = 195, SourceID = 9, datafield = 'dbo.fnConvertUOM(OriginNetUnits, OriginUomID, 3)', caption = 'ORIGIN | VOLUMES | CM | Origin NSV'
		UNION SELECT newID = 196, SourceID = 8, datafield = 'dbo.fnConvertUOM(OriginGrossUnits, OriginUomID, 2)', caption = 'ORIGIN | VOLUMES | GAL | Origin GOV'
		UNION SELECT newID = 197, SourceID = 7, datafield = 'dbo.fnConvertUOM(OriginGrossStdUnits, OriginUomID, 2)', caption = 'ORIGIN | VOLUMES | GAL | Origin GSV'
		UNION SELECT newID = 198, SourceID = 9, datafield = 'dbo.fnConvertUOM(OriginNetUnits, OriginUomID, 2)', caption = 'ORIGIN | VOLUMES | GAL | Origin NSV'
		UNION SELECT newID = 199, SourceID = 102, datafield = 'dbo.fnConvertUOM(T_GrossUnits, OriginUomID, 3)', caption = 'TICKET | VOLUMES | CM | Ticket GOV'
		UNION SELECT newID = 200, SourceID = 101, datafield = 'dbo.fnConvertUOM(T_GrossStdUnits, OriginUomID, 3)', caption = 'TICKET | VOLUMES | CM | Ticket GSV'
		UNION SELECT newID = 201, SourceID = 103, datafield = 'dbo.fnConvertUOM(T_NetUnits, OriginUomID, 3)', caption = 'TICKET | VOLUMES | CM | Ticket NSV'
		UNION SELECT newID = 202, SourceID = 102, datafield = 'dbo.fnConvertUOM(T_GrossUnits, OriginUomID, 2)', caption = 'TICKET | VOLUMES | GAL | Ticket GOV'
		UNION SELECT newID = 203, SourceID = 101, datafield = 'dbo.fnConvertUOM(T_GrossStdUnits, OriginUomID, 2)', caption = 'TICKET | VOLUMES | GAL | Ticket GSV'
		UNION SELECT newID = 204, SourceID = 103, datafield = 'dbo.fnConvertUOM(T_NetUnits, OriginUomID, 2)', caption = 'TICKET | VOLUMES | GAL | Ticket NSV'
	) X ON X.SourceID = RCD.id
SET IDENTITY_INSERT tblReportColumnDefinition OFF
GO

UPDATE tblReportColumnDefinition
  SET DataField = X.DataField
FROM tblReportColumnDefinition RCD
JOIN (
	SELECT id=8, datafield='dbo.fnConvertUOM(OriginGrossUnits, OriginUomID, 1)'
	UNION SELECT 7, 'dbo.fnConvertUOM(OriginGrossStdUnits, OriginUomID, 1)'
	UNION SELECT 9, 'dbo.fnConvertUOM(OriginGrossNetUnits, OriginUomID, 1)'
	UNION SELECT 102, 'dbo.fnConvertUOM(T_GrossUnits, OriginUomID, 1)'
	UNION SELECT 101, 'dbo.fnConvertUOM(T_GrossStdUnits, OriginUomID, 1)'
	UNION SELECT 103, 'dbo.fnConvertUOM(T_NetUnits, OriginUomID, 1)'
) X ON X.ID = RCD.ID
 GO
 
-- drop function fnReportColumnTree
/************************************************
-- Date Created: 13 Nov 2014
-- Author: Kevin Alons
-- Purpose: recursively process the tblReportColumnDefinition table into a "tree" parent-child "table" for use for a tree control
************************************************/
CREATE FUNCTION [dbo].[fnReportColumnTree](@UserName varchar(100)) RETURNS @ret TABLE (
  ID int not null
, Caption varchar(255)
, ParentID int null
) AS 
BEGIN
	DECLARE @data TABLE 
	(
	  ID int
	, Caption varchar(255)
	, sortid int
	, parentid int
	, processed bit not null 
	, parents varchar(255)
	)

	INSERT INTO @data (ID, Caption, sortid, processed)
		SELECT ID, Caption
			--, DataField + char(9) + isnull(DataFormat, '') + char(9) + isnull(FilterDataField, '') + char(9) + isnull(ltrim(FilterTypeID), '') + char(9) + ISNULL(FilterDropDownSql, '')
			, sortid = ROW_NUMBER() over (order by caption), 0
		FROM tblReportColumnDefinition
		WHERE dbo.fnUserInRoles(@UserName, AllowedRoles) = 1 
		ORDER BY caption

	DECLARE @sortid int, @caption varchar(255), @seg varchar(100), @pos int, @parentID int, @parents varchar(255), @id int, @newID int, @newRunID int; SET @newRunID = 0
	WHILE EXISTS (SELECT * FROM @data WHERE processed = 0)
	BEGIN
		SELECT TOP 1 @sortid = sortid, @caption = caption from @data where processed = 0 order by sortid

		SELECT @newID = NULL, @parentID = NULL, @parents = '', @pos = CHARINDEX('|', @caption, 1)
		WHILE @pos > 0
		BEGIN
			SELECT @seg = rtrim(ltrim(SUBSTRING(@caption, 1, @pos - 1)))
			SET @caption = rtrim(ltrim(SUBSTRING(@caption, @pos + 1, 255)))

			SET @parentID = @newID
			SET @parents = isnull(@parents + '|' + LTRIM(@parentID), '')
			
			SET @id = (SELECT id FROM @data WHERE ID < 0 AND Caption = @seg AND parents = @parents)
			IF (@id IS NOT NULL)
				SET @newID = @id
			ELSE BEGIN
				SET @newRunID = @newRunID - 1
				SET @newID = @newRunID
				
				INSERT INTO @data (ID, Caption, sortid, parentid, parents, processed)
					SELECT @newID, @seg, -@sortid, @parentid, @parents, 1
			END
			
			SELECT @pos = CHARINDEX('|', @caption, 1)
		END
		
		UPDATE @data SET Caption = @caption, parentid = @newID, processed = 1 where sortid = @sortid
	END

	INSERT INTO @ret (ID, Caption, ParentID)
		SELECT ID, Caption, ParentID FROM @data ORDER BY ABS(sortid), ABS(id)
	
	RETURN
END

GO
GRANT SELECT ON fnReportColumnTree TO dispatchcrude_iis_acct
GO

-- DROP FUNCTION fnCaptionRoot
/************************************************
-- Date Created: 13 Nov 2014
-- Author: Kevin Alons
-- Purpose: strip any leading xxx | xxx | ... from caption values
************************************************/
CREATE FUNCTION fnCaptionRoot(@caption varchar(255)) RETURNS varchar(255) AS
BEGIN
	DECLARE @newPos int, @pos int; SELECT @pos = 0, @newPos = 1
	WHILE (@newPos > 0)
	BEGIN
		SET @pos = @newPos + CASE WHEN @pos = 0 THEN 0 ELSE 1 END
		SET @newPos = CHARINDEX('|', @caption, @pos)
	END
	RETURN rtrim(ltrim(substring(@caption, @pos, 255)))
END
GO
GRANT EXECUTE ON dbo.fnCaptionRoot TO dispatchcrude_iis_acct
GO

/***********************************************/
-- Date Created: 27 Jul 2014
-- Author: Kevin Alons
-- Purpose: add related JOINed fields to the tblUserReportColumnDefinition table results
/***********************************************/
ALTER VIEW [dbo].[viewUserReportColumnDefinition] AS
	SELECT URCD.ID
		, URCD.UserReportID
		, URCD.ReportColumnID
		, URCD.Caption
		, DataFormat = isnull(URCD.DataFormat, RCD.DataFormat)
		, URCD.SortNum
		, URCD.FilterOperatorID
		, URCD.FilterValue1
		, URCD.FilterValue2
		, URCD.Export
		, URCD.CreateDateUTC, URCD.CreatedByUser
		, URCD.LastChangeDateUTC, URCD.LastChangedByUser
		, RCD.DataField
		, BaseCaption = dbo.fnCaptionRoot(RCD.Caption)
		, OutputCaption = coalesce(URCD.Caption, dbo.fnCaptionRoot(RCD.Caption), RCD.DataField)
		, RCD.FilterTypeID
		, FilterDataField = isnull(RCD.FilterDataField, RCD.DataField)
		, RCD.FilterDropDownSql
		, RCD.FilterAllowCustomText
		, RCD.AllowedRoles
		, RCD.OrderSingleExport
		, FilterOperator = RFO.Name
		, FilterValues = CASE 
				WHEN RFO.ID IN (1,4,5) OR RCD.FilterAllowCustomText = 1 THEN FilterValue1 + ISNULL(' - ' + FilterValue2, '')
				ELSE 
					CASE WHEN FilterValue1 IS NOT NULL THEN '&lt;filtered&gt;' 
						 ELSE NULL 
					END 
			END
	FROM tblUserReportColumnDefinition URCD
	JOIN viewReportColumnDefinition RCD ON RCD.ID = URCD.ReportColumnID
	LEFT JOIN tblReportFilterOperator RFO ON RFO.ID = URCD.FilterOperatorID;

GO

COMMIT
SET NOEXEC OFF