SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.10.13.3'
SELECT  @NewVersion = '3.10.13.4'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Truck Type - Update settlement triggers'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


-- PART 5
-- UPDATE THE CORE SETTLEMENT TRIGGERS
--------------------------------------------------------------------------------------------

-- UPDATE CARRIER TRIGGERS

/***********************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			ensure Locked records are not modified (except the EndDate)
			un-associate changed rates from rated orders
-- Changes
	- 3.10.13.4 - 2016/02/29 - JAE - Add truck type
***********************************************/
ALTER TRIGGER [dbo].[trigCarrierAssessorialRate_IU] ON [dbo].[tblCarrierAssessorialRate] AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblCarrierAssessorialRate X 
			ON i.ID <> X.ID
			  AND i.TypeID = X.TypeID
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(i.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(i.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(i.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(i.ProducerID, X.ProducerID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Assessorial Rates are not allowed';
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewCarrierAssessorialRate X ON d.ID = X.ID AND X.Locked = 1
		WHERE d.TypeID <> X.TypeID
			OR dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
			OR dbo.fnCompareNullableInts(d.CarrierID, X.CarrierID) = 0
			OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
			OR dbo.fnCompareNullableInts(d.TruckTypeID, X.TruckTypeID) = 0
			OR dbo.fnCompareNullableInts(d.DriverGroupID, X.DriverGroupID) = 0
			OR dbo.fnCompareNullableInts(d.OriginID, X.OriginID) = 0
			OR dbo.fnCompareNullableInts(d.DestinationID, X.DestinationID) = 0
			OR dbo.fnCompareNullableInts(d.OriginStateID, X.OriginStateID) = 0
			OR dbo.fnCompareNullableInts(d.DestStateID, X.DestStateID) = 0
			OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
			OR dbo.fnCompareNullableInts(d.ProducerID, X.ProducerID) = 0
			OR d.EffectiveDate <> X.EffectiveDate
			OR d.Rate <> X.Rate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed';
	END
	
	IF (@error IS NOT NULL)
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		-- un-rate any Order Assessorial Rates associated with this assessorial rate record
		DELETE AC
		FROM tblOrderSettlementCarrierAssessorialCharge AC
		JOIN tblOrderSettlementCarrier SC ON SC. OrderID = AC.OrderID
		JOIN inserted i ON i.ID = AC.AssessorialRateID
		WHERE SC.BatchID IS NULL
END

GO



/***********************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			prevent changes to Locked (in use) records
			un-associate changed rates from rated orders
-- Changes
	- 3.10.13.4 - 2016/02/29 - JAE - Add truck type
***********************************************/
ALTER TRIGGER [dbo].[trigCarrierDestinationWaitRate_IU] ON [dbo].[tblCarrierDestinationWaitRate] AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblCarrierDestinationWaitRate X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ReasonID, X.ReasonID) = 1
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(i.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(i.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(i.ProducerID, X.ProducerID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Wait Rates are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewCarrierDestinationWaitRate X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ReasonID, X.ReasonID) = 0
		  OR dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
		  OR dbo.fnCompareNullableInts(d.CarrierID, X.CarrierID) = 0
		  OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
		  OR dbo.fnCompareNullableInts(d.TruckTypeID, X.TruckTypeID) = 0
		  OR dbo.fnCompareNullableInts(d.DriverGroupID, X.DriverGroupID) = 0
		  OR dbo.fnCompareNullableInts(d.DestinationID, X.DestinationID) = 0
		  OR dbo.fnCompareNullableInts(d.StateID, X.StateID) = 0
		  OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
		  OR dbo.fnCompareNullableInts(d.ProducerID, X.ProducerID) = 0
		  OR d.EffectiveDate <> X.EffectiveDate
		  OR d.Rate <> X.Rate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	IF @error IS NOT NULL 
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementCarrier SET DestinationWaitRateID = NULL, DestinationWaitAmount = NULL 
		WHERE BatchID IS NULL AND DestinationWaitRateID IN (SELECT ID FROM inserted)
END

GO


/***********************************************
-- Date Created: 20 Feb 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			prevent changes to Locked (in use) records
			un-associate changed rates from rated orders
-- Changes
	- 3.10.13.4 - 2016/02/29 - JAE - Add truck type
***********************************************/
ALTER TRIGGER [dbo].[trigCarrierFuelSurchargeRate_IU] ON [dbo].[tblCarrierFuelSurchargeRate] AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblCarrierFuelSurchargeRate X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(i.DriverGroupID, X.DriverGroupID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Fuel Surcharge Rates are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewCarrierFuelSurchargeRate X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
		  OR dbo.fnCompareNullableInts(d.CarrierID, X.CarrierID) = 0
		  OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
		  OR dbo.fnCompareNullableInts(d.TruckTypeID, X.TruckTypeID) = 0
		  OR dbo.fnCompareNullableInts(d.DriverGroupID, X.DriverGroupID) = 0
		  OR d.EffectiveDate <> X.EffectiveDate
		  OR d.FuelPriceFloor <> X.FuelPriceFloor
		  OR d.IncrementAmount <> X.IncrementAmount
		  OR d.IntervalAmount <> X.IntervalAmount
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	IF @error IS NOT NULL 
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementCarrier SET FuelSurchargeRateID = NULL, FuelSurchargeAmount = NULL 
		WHERE BatchID IS NULL AND FuelSurchargeRateID IN (SELECT ID FROM inserted)
END

GO


/***********************************************
-- Date Created: 20 Feb 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			prevent changes to Locked (in use) records
			un-associate changed rates from rated orders
-- Changes:
	-- 3.7.33 - 2015/06/19 - KDA - ADDED
	- 3.10.13.4 - 2016/02/29 - JAE - Add truck type
***********************************************/
ALTER TRIGGER [dbo].[trigCarrierMinSettlementUnits_IU] ON [dbo].[tblCarrierMinSettlementUnits] AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblCarrierMinSettlementUnits X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(i.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(i.DestinationStateID, X.DestinationStateID) = 1
			  AND dbo.fnCompareNullableInts(i.ProducerID, X.ProducerID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Min Settlement Unit records are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewCarrierMinSettlementUnits  X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
		  OR dbo.fnCompareNullableInts(d.CarrierID, X.CarrierID) = 0
		  OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
		  OR dbo.fnCompareNullableInts(d.TruckTypeID, X.TruckTypeID) = 0
		  OR dbo.fnCompareNullableInts(d.OriginID, X.OriginID) = 0
		  OR dbo.fnCompareNullableInts(d.OriginStateID, X.OriginStateID) = 0
		  OR dbo.fnCompareNullableInts(d.DestinationID, X.DestinationID) = 0
		  OR dbo.fnCompareNullableInts(d.DestinationStateID, X.DestinationStateID) = 0
		  OR dbo.fnCompareNullableInts(d.ProducerID, X.ProducerID) = 0
		  OR d.EffectiveDate <> X.EffectiveDate
		  OR d.MinSettlementUnits <> X.MinSettlementUnits
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	IF @error IS NOT NULL 
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementCarrier SET MinSettlementUnitsID = NULL, MinSettlementUnits = NULL 
		WHERE BatchID IS NULL AND MinSettlementUnitsID IN (SELECT ID FROM inserted)
END

GO


/***********************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			prevent changes to Locked (in use) records
-- Changes:
	- 3.7.33 - 2015/06/19 - KDA - add missing criteria references (DriverID | ProducerID)
	- 3.10.13.4 - 2016/02/29 - JAE - Add truck type
***********************************************/
ALTER TRIGGER [dbo].[trigCarrierOrderRejectRate_IU] ON [dbo].[tblCarrierOrderRejectRate] AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblCarrierOrderRejectRate X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ReasonID, X.ReasonID) = 1
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(i.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(i.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(i.ProducerID, X.ProducerID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Reject Rates are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewCarrierOrderRejectRate X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ReasonID, X.ReasonID) = 0
		  OR dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
		  OR dbo.fnCompareNullableInts(d.CarrierID, X.CarrierID) = 0
		  OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
		  OR dbo.fnCompareNullableInts(d.TruckTypeID, X.TruckTypeID) = 0
		  OR dbo.fnCompareNullableInts(d.DriverGroupID, X.DriverGroupID) = 0
		  OR dbo.fnCompareNullableInts(d.OriginID, X.OriginID) = 0
		  OR dbo.fnCompareNullableInts(d.StateID, X.StateID) = 0
		  OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
		  OR dbo.fnCompareNullableInts(d.ProducerID, X.ProducerID) = 0
		  OR d.EffectiveDate <> X.EffectiveDate
		  OR d.Rate <> X.Rate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	IF @error IS NOT NULL 
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementCarrier SET OrderRejectRateID = NULL, OrderRejectAmount = NULL 
		WHERE BatchID IS NULL AND OrderRejectRateID IN (SELECT ID FROM inserted)
END

GO


/***********************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			prevent changes to Locked (in use) records
-- Changes:
	- 3.7.34 - 2015/06/20 - KDA - fix overlapping create logic to honor DriverGroupID
	- 3.10.13.4 - 2016/02/29 - JAE - Add truck type
***********************************************/
ALTER TRIGGER [dbo].[trigCarrierOriginWaitRate_IU] ON [dbo].[tblCarrierOriginWaitRate] AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblCarrierOriginWaitRate X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ReasonID, X.ReasonID) = 1
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(i.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(i.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Wait Rates are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewCarrierOriginWaitRate X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ReasonID, X.ReasonID) = 0
		  OR dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
		  OR dbo.fnCompareNullableInts(d.CarrierID, X.CarrierID) = 0
		  OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
		  OR dbo.fnCompareNullableInts(d.TruckTypeID, X.TruckTypeID) = 0
		  OR dbo.fnCompareNullableInts(d.DriverGroupID, X.DriverGroupID) = 0
		  OR dbo.fnCompareNullableInts(d.OriginID, X.OriginID) = 0
		  OR dbo.fnCompareNullableInts(d.StateID, X.StateID) = 0
		  OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
		  OR d.EffectiveDate <> X.EffectiveDate
		  OR d.Rate <> X.Rate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	IF @error IS NOT NULL 
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementCarrier SET OriginWaitRateID = NULL, OriginWaitAmount = NULL 
		WHERE BatchID IS NULL AND OriginWaitRateID IN (SELECT ID FROM inserted)
END

GO


/***********************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
-- Changes
	- 3.10.13.4 - 2016/02/29 - JAE - Add truck type
***********************************************/
ALTER TRIGGER [dbo].[trigCarrierRateSheet_IU] ON [dbo].[tblCarrierRateSheet] AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblCarrierRateSheet X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(i.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(i.ProducerID, X.ProducerID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Rate Sheets are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewCarrierRateSheet X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
			OR dbo.fnCompareNullableInts(d.CarrierID, X.CarrierID) = 0
			OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
			OR dbo.fnCompareNullableInts(d.DriverGroupID, X.DriverGroupID) = 0
		    OR dbo.fnCompareNullableInts(d.TruckTypeID, X.TruckTypeID) = 0
			OR dbo.fnCompareNullableInts(d.OriginStateID, X.OriginStateID) = 0
			OR dbo.fnCompareNullableInts(d.DestStateID, X.DestStateID) = 0
			OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
			OR dbo.fnCompareNullableInts(d.ProducerID, X.ProducerID) = 0
			OR d.EffectiveDate <> X.EffectiveDate 
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	
	IF (@error IS NOT NULL)
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
END

GO


/***********************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
-- Changes
	- 3.10.13.4 - 2016/02/29 - JAE - Add truck type
***********************************************/
ALTER TRIGGER [dbo].[trigCarrierRouteRate_IU] ON [dbo].[tblCarrierRouteRate] AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblCarrierRouteRate X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(i.DriverGroupID, X.DriverGroupID) = 1
			  AND i.RouteID = X.RouteID
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Route Rates are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewCarrierRouteRate X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
			OR dbo.fnCompareNullableInts(d.CarrierID, X.CarrierID) = 0
			OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
		    OR dbo.fnCompareNullableInts(d.TruckTypeID, X.TruckTypeID) = 0
			OR dbo.fnCompareNullableInts(d.DriverGroupID, X.DriverGroupID) = 0
			OR d.RouteID <> X.RouteID
			OR d.EffectiveDate <> X.EffectiveDate
			OR d.Rate <> X.Rate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	IF (@error IS NOT NULL)
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementCarrier SET RouteRateID = NULL, RangeRateID = NULL, LoadAmount = NULL 
		WHERE BatchID IS NULL AND RouteRateID IN (SELECT ID FROM inserted)
END

GO


/***********************************************
-- Date Created: 2015/06/19
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			prevent changes to Locked (in use) records
			un-associate changed rates from rated orders
-- Changes:
	-- 3.7.33 - 2015/06/19 - KDA - ADDED
	- 3.10.13.4 - 2016/02/29 - JAE - Add truck type
***********************************************/
ALTER TRIGGER [dbo].[trigCarrierSettlementFactor_IU] ON [dbo].[tblCarrierSettlementFactor] AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblCarrierSettlementFactor X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(i.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(i.DestinationStateID, X.DestinationStateID) = 1
			  AND dbo.fnCompareNullableInts(i.ProducerID, X.ProducerID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Settlements Unit Type records are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewCarrierSettlementFactor  X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
		  OR dbo.fnCompareNullableInts(d.CarrierID, X.CarrierID) = 0
		  OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
		  OR dbo.fnCompareNullableInts(d.TruckTypeID, X.TruckTypeID) = 0
		  OR dbo.fnCompareNullableInts(d.OriginID, X.OriginID) = 0
		  OR dbo.fnCompareNullableInts(d.OriginStateID, X.OriginStateID) = 0
		  OR dbo.fnCompareNullableInts(d.DestinationID, X.DestinationID) = 0
		  OR dbo.fnCompareNullableInts(d.DestinationStateID, X.DestinationStateID) = 0
		  OR dbo.fnCompareNullableInts(d.ProducerID, X.ProducerID) = 0
		  OR d.EffectiveDate <> X.EffectiveDate
		  OR d.SettlementFactorID <> X.SettlementFactorID
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	IF @error IS NOT NULL 
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementCarrier SET CarrierSettlementFactorID = NULL, SettlementFactorID = NULL 
		WHERE BatchID IS NULL AND CarrierSettlementFactorID IN (SELECT ID FROM inserted)
END

GO


/***********************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			prevent changes to Locked records (except for EndDate)
-- Changes
	- 3.10.13.4 - 2016/02/29 - JAE - Add truck type
***********************************************/
ALTER TRIGGER [dbo].[trigCarrierWaitFeeParameter_IU] ON [dbo].[tblCarrierWaitFeeParameter] AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblCarrierWaitFeeParameter X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(i.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(i.ProducerID, X.ProducerID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Parameters are not allowed'
	END
	ElSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewCarrierWaitFeeParameter X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
			OR dbo.fnCompareNullableInts(d.CarrierID, X.CarrierID) = 0
			OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
			OR dbo.fnCompareNullableInts(d.TruckTypeID, X.TruckTypeID) = 0
			OR dbo.fnCompareNullableInts(d.OriginStateID, X.OriginStateID) = 0
			OR dbo.fnCompareNullableInts(d.DestStateID, X.DestStateID) = 0
			OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
			OR dbo.fnCompareNullableInts(d.ProducerID, X.ProducerID) = 0
			OR d.EffectiveDate <> X.EffectiveDate
			OR d.OriginThresholdMinutes <> X.OriginThresholdMinutes
			OR d.DestThresholdMinutes <> X.DestThresholdMinutes
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	
	IF (@error IS NOT NULL)
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementCarrier SET WaitFeeParameterID = NULL
		WHERE BatchID IS NULL AND WaitFeeParameterID IN (SELECT ID FROM inserted)
END

GO


/*************************************
-- Date Created: 18 Jan 2015
-- Author: Kevin Alons
-- Purpose: handle specialized logic related to editing Route Rates (due to combination of RateSheet + RangeRate tables)
-- Changes:
	- 3.7.29 - 2015/06/19 - KDA - add support for DriverID/DriverGroupID/ProducerID
	- 3.7.30 - 2015/06/19 - KDA - remove DriverID
	- 3.10.13.4 - 2016/02/29 - JAE - Add truck type
*************************************/
ALTER TRIGGER [dbo].[trigViewCarrierRateSheetRangeRate_IU_Update] ON [dbo].[viewCarrierRateSheetRangeRate] INSTEAD OF INSERT, UPDATE  AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
		-- PRINT 'apply any RateSheet changes to the underlying RateSheet records'
		UPDATE tblCarrierRateSheet
			SET ShipperID = i.ShipperID
				, CarrierID = i.CarrierID
				, ProductGroupID = i.ProductGroupID
				, TruckTypeID = i.TruckTypeID
				, DriverGroupID = i.DriverGroupID
				, OriginStateID = i.OriginStateID
				, DestStateID = i.DestStateID
				, RegionID = i.RegionID
				, ProducerID = i.ProducerID
				, EffectiveDate = i.EffectiveDate
				, EndDate = i.EndDate
				, RateTypeID = i.RateTypeID
				, UomID = i.UomID
				, LastChangeDateUTC = isnull(i.LastChangeDateUTC, getutcdate())
				, LastChangedByUser = i.LastChangedByUser
		FROM tblCarrierRateSheet RS
		JOIN ( 
			SELECT DISTINCT i.RateSheetID, i.ShipperID, i.CarrierID, i.ProductGroupID, i.TruckTypeID, i.DriverGroupID, i.OriginStateID, i.DestStateID, i.RegionID, i.ProducerID, i.EffectiveDate, i.EndDate, i.RateTypeID, i.UomID, i.LastChangeDateUTC, i.LastChangedByUser
			FROM inserted i 
			JOIN deleted d ON d.ID = i.ID
			WHERE dbo.fnCompareNullableInts(i.ShipperID, d.ShipperID) = 0
				OR dbo.fnCompareNullableInts(i.CarrierID, d.CarrierID) = 0
				OR dbo.fnCompareNullableInts(i.ProductGroupID, d.ProductGroupID) = 0
			    OR dbo.fnCompareNullableInts(i.TruckTypeID, d.TruckTypeID) = 0
				OR dbo.fnCompareNullableInts(i.DriverGroupID, d.DriverGroupID) = 0
				OR dbo.fnCompareNullableInts(i.OriginStateID, d.OriginStateID) = 0
				OR dbo.fnCompareNullableInts(i.DestStateID, d.DestStateID) = 0
				OR dbo.fnCompareNullableInts(i.RegionID, d.RegionID) = 0
				OR dbo.fnCompareNullableInts(i.ProducerID, d.ProducerID) = 0
				OR i.EndDate <> d.EndDate
		) i ON i.RateSheetID = RS.ID

		--PRINT 'ensure a RateSheet record exists for each new RangeRate records'
		INSERT INTO tblCarrierRateSheet (ShipperID, CarrierID, ProductGroupID, TruckTypeID, DriverGroupID, OriginStateID, DestStateID, RegionID, ProducerID, EffectiveDate, EndDate, RateTypeID, UomID, CreatedByUser)
			SELECT i.ShipperID, i.CarrierID, i.ProductGroupID, i.TruckTypeID, i.DriverGroupID, i.OriginStateID, i.DestStateID, i.RegionID, i.ProducerID, i.EffectiveDate, i.EndDate, i.RateTypeID, i.UomID, i.CreatedByUser 
			FROM inserted i
			LEFT JOIN tblCarrierRateSheet RS 
				ON i.EffectiveDate = RS.EffectiveDate
					AND dbo.fnCompareNullableInts(i.ShipperID, RS.ShipperID) = 1
					AND dbo.fnCompareNullableInts(i.CarrierID, RS.CarrierID) = 1
					AND dbo.fnCompareNullableInts(i.ProductGroupID, RS.ProductGroupID) = 1
				    AND dbo.fnCompareNullableInts(i.TruckTypeID, RS.TruckTypeID) = 1
					AND dbo.fnCompareNullableInts(i.DriverGroupID, RS.DriverGroupID) = 1
					AND dbo.fnCompareNullableInts(i.OriginStateID, RS.OriginStateID) = 1
					AND dbo.fnCompareNullableInts(i.DestStateID, RS.DestStateID) = 1
					AND dbo.fnCompareNullableInts(i.RegionID, RS.RegionID) = 1
					AND dbo.fnCompareNullableInts(i.ProducerID, RS.ProducerID) = 1
			WHERE isnull(RateSheetID, 0) = 0
			  AND RS.ID IS NULL
		
		-- optimization for typical usaage (where only 1 record is INSERTed)
		IF (SELECT count(1) FROM inserted i WHERE isnull(ID, 0) = 0) = 1
		BEGIN
			DECLARE @rsID int
			SELECT @rsID = isnull(RateSheetID, SCOPE_IDENTITY()) FROM inserted
			IF @rsID IS NULL
				SELECT @rsID = RS.ID
				FROM inserted i
				LEFT JOIN tblCarrierRateSheet RS 
					ON i.EffectiveDate = RS.EffectiveDate
						AND dbo.fnCompareNullableInts(i.ShipperID, RS.ShipperID) = 1
						AND dbo.fnCompareNullableInts(i.CarrierID, RS.CarrierID) = 1
						AND dbo.fnCompareNullableInts(i.ProductGroupID, RS.ProductGroupID) = 1
						AND dbo.fnCompareNullableInts(i.TruckTypeID, RS.TruckTypeID) = 1
						AND dbo.fnCompareNullableInts(i.DriverGroupID, RS.DriverGroupID) = 1
						AND dbo.fnCompareNullableInts(i.OriginStateID, RS.OriginStateID) = 1
						AND dbo.fnCompareNullableInts(i.DestStateID, RS.DestStateID) = 1
						AND dbo.fnCompareNullableInts(i.RegionID, RS.RegionID) = 1
						AND dbo.fnCompareNullableInts(i.ProducerID, RS.ProducerID) = 1

			INSERT INTO tblCarrierRangeRate (RateSheetID, MinRange, MaxRange, Rate, CreateDateUTC, CreatedByUser)
				SELECT @rsID, i.MinRange, i.MaxRange, i.Rate, isnull(i.CreateDateUTC, getutcdate()), i.CreatedByUser
				FROM inserted i
				WHERE isnull(ID, 0) = 0
		END
		ELSE 	-- bulk insert or update
		BEGIN
			-- PRINT 'Updating any existing record editable data'
			UPDATE tblCarrierRangeRate
				SET MinRange = i.MinRange
					, MaxRange = i.MaxRange
					, Rate = i.Rate
					, LastChangeDateUTC = isnull(i.LastChangeDateUTC, getutcdate())
					, LastChangedByUser = i.LastChangedByUser
			FROM tblCarrierRangeRate X
			JOIN inserted i ON i.ID = X.ID

			-- PRINT 'insert any new records'
			INSERT INTO tblCarrierRangeRate (RateSheetID, MinRange, MaxRange, Rate, CreatedByUser)
				SELECT R.ID, i.MinRange, i.MaxRange, i.Rate, isnull(i.CreatedByUser, R.CreatedByUser)
				FROM inserted i
				JOIN tblCarrierRateSheet R ON i.EffectiveDate = R.EffectiveDate
					AND dbo.fnCompareNullableInts(i.ShipperID, R.ShipperID) = 1
					AND dbo.fnCompareNullableInts(i.CarrierID, R.CarrierID) = 1
					AND dbo.fnCompareNullableInts(i.ProductGroupID, R.ProductGroupID) = 1
					AND dbo.fnCompareNullableInts(i.TruckTypeID, R.TruckTypeID) = 1
					AND dbo.fnCompareNullableInts(i.DriverGroupID, R.DriverGroupID) = 1
					AND dbo.fnCompareNullableInts(i.OriginStateID, R.OriginStateID) = 1
					AND dbo.fnCompareNullableInts(i.DestStateID, R.DestStateID) = 1
					AND dbo.fnCompareNullableInts(i.RegionID, R.RegionID) = 1
					AND dbo.fnCompareNullableInts(i.ProducerID, R.ProducerID) = 1
				WHERE ISNULL(i.ID, 0) = 0
		END
	END TRY
	BEGIN CATCH
		DECLARE @error varchar(255)
		SET @error = substring(ERROR_MESSAGE(), 1, 255)
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END CATCH
END

GO


/*************************************
 Date Created: 18 Jan 2015
 Author: Kevin Alons
 Purpose: handle specialized logic related to editing Route Rates (due to RouteID being "inputted" as Origin|Destination combination)
 Changes:
	3.9.29.4 - 2015/12/02 - KDA - add missing DriverGroupID fields to this trigger
	- 3.10.13.4 - 2016/02/29 - JAE - Add truck type
*************************************/
ALTER TRIGGER [dbo].[trigViewCarrierRouteRate_IU_Update] ON [dbo].[viewCarrierRouteRate] INSTEAD OF INSERT, UPDATE  AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
		--PRINT 'ensure a Route record exists for the new Origin-Destination combo'
		INSERT INTO tblRoute (OriginID, DestinationID)
			SELECT DISTINCT OriginID, DestinationID FROM inserted
			EXCEPT SELECT OriginID, DestinationID FROM tblRoute
		
		-- PRINT 'Updating any existing record editable data'
		UPDATE tblCarrierRouteRate
			SET CarrierID = nullif(i.CarrierID, 0)
				, ProductGroupID = nullif(i.ProductGroupID, 0)
				, TruckTypeID = nullif(i.TruckTypeID, 0)
				, DriverGroupID = nullif(i.DriverGroupID, 0)
				, RouteID = R.ID
				, Rate = i.Rate
				, EffectiveDate = i.EffectiveDate
				, EndDate = i.EndDate
				, RateTypeID = i.RateTypeID
				, UomID = i.UomID
				, LastChangeDateUTC = i.LastChangeDateUTC
				, LastChangedByUser = i.LastChangedByUser
		FROM tblCarrierRouteRate X
		JOIN inserted i ON i.ID = X.ID
		JOIN tblRoute R ON R.OriginID = i.OriginID AND R.DestinationID = i.DestinationID 

		-- PRINT 'insert any new records'
		INSERT INTO tblCarrierRouteRate (CarrierID, ProductGroupID, TruckTypeID, DriverGroupID, RouteID, Rate, EffectiveDate, EndDate, RateTypeID, UomID, CreatedByUser)
			SELECT nullif(CarrierID, 0), nullif(ProductGroupID, 0), nullif(TruckTypeID, 0), nullif(DriverGroupID, 0), R.ID, Rate, EffectiveDate, EndDate, RateTypeID, UomID, i.CreatedByUser
			FROM inserted i
			JOIN tblRoute R ON R.OriginID = i.OriginID AND R.DestinationID = i.DestinationID 
			WHERE ISNULL(i.ID, 0) = 0
	END TRY
	BEGIN CATCH
		DECLARE @error varchar(255)
		SET @error = ERROR_MESSAGE()
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END CATCH
END

GO


-- UPDATE SHIPPER TRIGGERS


/***********************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			ensure Locked records are not modified (except the EndDate)
-- Changes
	- 3.10.13.4 - 2016/02/29 - JAE - Add truck type
***********************************************/
ALTER TRIGGER [dbo].[trigShipperAssessorialRate_IU] ON [dbo].[tblShipperAssessorialRate] AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblShipperAssessorialRate X 
			ON i.ID <> X.ID
			  AND i.TypeID = X.TypeID
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(i.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(i.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(i.ProducerID, X.ProducerID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Assessorial Rates are not allowed';
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewShipperAssessorialRate X ON d.ID = X.ID AND X.Locked = 1
		WHERE d.TypeID <> X.TypeID
			OR dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
			OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
		    OR dbo.fnCompareNullableInts(d.TruckTypeID, X.TruckTypeID) = 0
			OR dbo.fnCompareNullableInts(d.OriginID, X.OriginID) = 0
			OR dbo.fnCompareNullableInts(d.DestinationID, X.DestinationID) = 0
			OR dbo.fnCompareNullableInts(d.OriginStateID, X.OriginStateID) = 0
			OR dbo.fnCompareNullableInts(d.DestStateID, X.DestStateID) = 0
			OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
			OR dbo.fnCompareNullableInts(d.ProducerID, X.ProducerID) = 0
			OR d.EffectiveDate <> X.EffectiveDate
			OR d.Rate <> X.Rate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed';
	END
	
	IF (@error IS NOT NULL)
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		-- un-rate any Order Assessorial Rates associated with this assessorial rate record
		DELETE AC
		FROM tblOrderSettlementShipperAssessorialCharge AC
		JOIN tblOrderSettlementShipper SS ON SS. OrderID = AC.OrderID
		JOIN inserted i ON i.ID = AC.AssessorialRateID
		WHERE SS.BatchID IS NULL
END

GO


/***********************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			prevent changes to Locked (in use) records
-- Changes
	- 3.10.13.4 - 2016/02/29 - JAE - Add truck type
***********************************************/
ALTER TRIGGER [dbo].[trigShipperDestinationWaitRate_IU] ON [dbo].[tblShipperDestinationWaitRate] AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblShipperDestinationWaitRate X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ReasonID, X.ReasonID) = 1
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(i.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(i.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Wait Rates are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewShipperDestinationWaitRate X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ReasonID, X.ReasonID) = 0
		  OR dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
		  OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
		  OR dbo.fnCompareNullableInts(d.TruckTypeID, X.TruckTypeID) = 0
		  OR dbo.fnCompareNullableInts(d.DestinationID, X.DestinationID) = 0
		  OR dbo.fnCompareNullableInts(d.StateID, X.StateID) = 0
		  OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
		  OR d.EffectiveDate <> X.EffectiveDate
		  OR d.Rate <> X.Rate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	IF @error IS NOT NULL 
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementShipper SET DestinationWaitRateID = NULL, DestinationWaitAmount = NULL 
		WHERE BatchID IS NULL AND DestinationWaitRateID IN (SELECT ID FROM inserted)
END

GO


/***********************************************
-- Date Created: 20 Feb 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			prevent changes to Locked (in use) records
			un-associate changed rates from rated orders
-- Changes
	- 3.10.13.4 - 2016/02/29 - JAE - Add truck type
***********************************************/
ALTER TRIGGER [dbo].[trigShipperFuelSurchargeRate_IU] ON [dbo].[tblShipperFuelSurchargeRate] AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblShipperFuelSurchargeRate X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.TruckTypeID, X.TruckTypeID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Fuel Surcharge Rates are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewShipperFuelSurchargeRate X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
		  OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
		  OR dbo.fnCompareNullableInts(d.TruckTypeID, X.TruckTypeID) = 0
		  OR d.EffectiveDate <> X.EffectiveDate
		  OR d.FuelPriceFloor <> X.FuelPriceFloor
		  OR d.IncrementAmount <> X.IncrementAmount
		  OR d.IntervalAmount <> X.IntervalAmount
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	IF @error IS NOT NULL 
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementShipper SET FuelSurchargeRateID = NULL, FuelSurchargeAmount = NULL 
		WHERE BatchID IS NULL AND FuelSurchargeRateID IN (SELECT ID FROM inserted)
END

GO


/***********************************************
-- Date Created: 20 Feb 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			prevent changes to Locked (in use) records
			un-associate changed rates from rated orders
-- Changes:
	-- 3.7.33 - 2015/06/19 - KDA - ADDED
	- 3.10.13.4 - 2016/02/29 - JAE - Add truck type
***********************************************/
ALTER TRIGGER [dbo].[trigShipperMinSettlementUnits_IU] ON [dbo].[tblShipperMinSettlementUnits] AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblShipperMinSettlementUnits X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(i.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(i.DestinationStateID, X.DestinationStateID) = 1
			  AND dbo.fnCompareNullableInts(i.ProducerID, X.ProducerID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Min Settlement Unit records are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewShipperMinSettlementUnits  X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
		  OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
		  OR dbo.fnCompareNullableInts(d.TruckTypeID, X.TruckTypeID) = 0
		  OR dbo.fnCompareNullableInts(d.OriginID, X.OriginID) = 0
		  OR dbo.fnCompareNullableInts(d.OriginStateID, X.OriginStateID) = 0
		  OR dbo.fnCompareNullableInts(d.DestinationID, X.DestinationID) = 0
		  OR dbo.fnCompareNullableInts(d.DestinationStateID, X.DestinationStateID) = 0
		  OR dbo.fnCompareNullableInts(d.ProducerID, X.ProducerID) = 0
		  OR d.EffectiveDate <> X.EffectiveDate
		  OR d.MinSettlementUnits <> X.MinSettlementUnits
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	IF @error IS NOT NULL 
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementShipper SET MinSettlementUnitsID = NULL, MinSettlementUnits = NULL 
		WHERE BatchID IS NULL AND MinSettlementUnitsID IN (SELECT ID FROM inserted)
END

GO


/***********************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			prevent changes to Locked (in use) records
-- Changes:
	- 3.7.33 - 2015/06/19 - KDA - add missing criteria references (DriverID | ProducerID)
	- 3.10.13.4 - 2016/02/29 - JAE - Add truck type
***********************************************/
ALTER TRIGGER [dbo].[trigShipperOrderRejectRate_IU] ON [dbo].[tblShipperOrderRejectRate] AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblShipperOrderRejectRate X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ReasonID, X.ReasonID) = 1
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(i.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(i.ProducerID, X.ProducerID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Reject Rates are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewShipperOrderRejectRate X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ReasonID, X.ReasonID) = 0
		  OR dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
		  OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
		  OR dbo.fnCompareNullableInts(d.TruckTypeID, X.TruckTypeID) = 0
		  OR dbo.fnCompareNullableInts(d.OriginID, X.OriginID) = 0
		  OR dbo.fnCompareNullableInts(d.StateID, X.StateID) = 0
		  OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
		  OR dbo.fnCompareNullableInts(d.ProducerID, X.ProducerID) = 0
		  OR d.EffectiveDate <> X.EffectiveDate
		  OR d.Rate <> X.Rate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	IF @error IS NOT NULL 
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementShipper SET OrderRejectRateID = NULL, OrderRejectAmount = NULL 
		WHERE BatchID IS NULL AND OrderRejectRateID IN (SELECT ID FROM inserted)
END

GO


/***********************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			prevent changes to Locked (in use) records
-- Changes
	- 3.10.13.4 - 2016/02/29 - JAE - Add truck type
***********************************************/
ALTER TRIGGER [dbo].[trigShipperOriginWaitRate_IU] ON [dbo].[tblShipperOriginWaitRate] AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblShipperOriginWaitRate X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ReasonID, X.ReasonID) = 1
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(i.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Wait Rates are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewShipperOriginWaitRate X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ReasonID, X.ReasonID) = 0
		  OR dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
		  OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
		  OR dbo.fnCompareNullableInts(d.TruckTypeID, X.TruckTypeID) = 0
		  OR dbo.fnCompareNullableInts(d.OriginID, X.OriginID) = 0
		  OR dbo.fnCompareNullableInts(d.StateID, X.StateID) = 0
		  OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
		  OR d.EffectiveDate <> X.EffectiveDate
		  OR d.Rate <> X.Rate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	IF @error IS NOT NULL 
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementShipper SET OriginWaitRateID = NULL, OriginWaitAmount = NULL 
		WHERE BatchID IS NULL AND OriginWaitRateID IN (SELECT ID FROM inserted)
END

GO


/***********************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
-- Changes
	- 3.10.13.4 - 2016/02/29 - JAE - Add truck type
***********************************************/
ALTER TRIGGER [dbo].[trigShipperRateSheet_IU] ON [dbo].[tblShipperRateSheet] AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblShipperRateSheet X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(i.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(i.ProducerID, X.ProducerID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Rate Sheets are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewShipperRateSheet X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
			OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
			OR dbo.fnCompareNullableInts(d.TruckTypeID, X.TruckTypeID) = 0
			OR dbo.fnCompareNullableInts(d.OriginStateID, X.OriginStateID) = 0
			OR dbo.fnCompareNullableInts(d.DestStateID, X.DestStateID) = 0
			OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
			OR dbo.fnCompareNullableInts(d.ProducerID, X.ProducerID) = 0
			OR d.EffectiveDate <> X.EffectiveDate 
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	
	IF (@error IS NOT NULL)
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
END

GO


/***********************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
-- Changes
	- 3.10.13.4 - 2016/02/29 - JAE - Add truck type
***********************************************/
ALTER TRIGGER [dbo].[trigShipperRouteRate_IU] ON [dbo].[tblShipperRouteRate] AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblShipperRouteRate X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.TruckTypeID, X.TruckTypeID) = 1
			  AND i.RouteID = X.RouteID
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Route Rates are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewShipperRouteRate X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
			OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
			OR dbo.fnCompareNullableInts(d.TruckTypeID, X.TruckTypeID) = 0
			OR d.RouteID <> X.RouteID
			OR d.EffectiveDate <> X.EffectiveDate
			OR d.Rate <> X.Rate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	IF (@error IS NOT NULL)
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementShipper SET RouteRateID = NULL, RangeRateID = NULL, LoadAmount = NULL 
		WHERE BatchID IS NULL AND RouteRateID IN (SELECT ID FROM inserted)
END

GO


/**********************************************
-- Date Created: 2015/06/19
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			prevent changes to Locked (in use) records
			un-associate changed rates from rated orders
-- Changes:
	-- 3.7.33 - 2015/06/19 - KDA - ADDED
	- 3.10.13.4 - 2016/02/29 - JAE - Add truck type
***********************************************/
ALTER TRIGGER [dbo].[trigShipperSettlementFactor_IU] ON [dbo].[tblShipperSettlementFactor] AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblShipperSettlementFactor X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(i.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(i.DestinationStateID, X.DestinationStateID) = 1
			  AND dbo.fnCompareNullableInts(i.ProducerID, X.ProducerID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Settlements Unit Type records are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewShipperSettlementFactor  X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
		  OR dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
		  OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
		  OR dbo.fnCompareNullableInts(d.TruckTypeID, X.TruckTypeID) = 0
		  OR dbo.fnCompareNullableInts(d.OriginID, X.OriginID) = 0
		  OR dbo.fnCompareNullableInts(d.OriginStateID, X.OriginStateID) = 0
		  OR dbo.fnCompareNullableInts(d.DestinationID, X.DestinationID) = 0
		  OR dbo.fnCompareNullableInts(d.DestinationStateID, X.DestinationStateID) = 0
		  OR dbo.fnCompareNullableInts(d.ProducerID, X.ProducerID) = 0
		  OR d.EffectiveDate <> X.EffectiveDate
		  OR d.SettlementFactorID <> X.SettlementFactorID
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	IF @error IS NOT NULL 
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementShipper SET ShipperSettlementFactorID = NULL, SettlementFactorID = NULL 
		WHERE BatchID IS NULL AND ShipperSettlementFactorID IN (SELECT ID FROM inserted)
END

GO


/***********************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			prevent changes to Locked records (except for EndDate)
-- Changes
	- 3.10.13.4 - 2016/02/29 - JAE - Add truck type
***********************************************/
ALTER TRIGGER [dbo].[trigShipperWaitFeeParameter_IU] ON [dbo].[tblShipperWaitFeeParameter] AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblShipperWaitFeeParameter X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(i.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(i.ProducerID, X.ProducerID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Parameters are not allowed'
	END
	ElSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewShipperWaitFeeParameter X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
			OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
			OR dbo.fnCompareNullableInts(d.TruckTypeID, X.TruckTypeID) = 0
			OR dbo.fnCompareNullableInts(d.OriginStateID, X.OriginStateID) = 0
			OR dbo.fnCompareNullableInts(d.DestStateID, X.DestStateID) = 0
			OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
			OR dbo.fnCompareNullableInts(d.ProducerID, X.ProducerID) = 0
			OR d.EffectiveDate <> X.EffectiveDate
			OR d.OriginThresholdMinutes <> X.OriginThresholdMinutes
			OR d.DestThresholdMinutes <> X.DestThresholdMinutes
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	
	IF (@error IS NOT NULL)
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementShipper SET WaitFeeParameterID = NULL
		WHERE BatchID IS NULL AND WaitFeeParameterID IN (SELECT ID FROM inserted)
END

GO


/*************************************
-- Date Created: 18 Jan 2015
-- Author: Kevin Alons
-- Purpose: handle specialized logic related to editing Route Rates (due to combination of RateSheet + RangeRate tables)
-- Changes:
	- 3.7.30 - 2015/06/19 - KDA - fix missing criteria comparisons
	- 3.10.13.4 - 2016/02/29 - JAE - Add truck type
*************************************/
ALTER TRIGGER [dbo].[trigViewShipperRateSheetRangeRate_IU_Update] ON [dbo].[viewShipperRateSheetRangeRate] INSTEAD OF INSERT, UPDATE  AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
		-- PRINT 'apply any RateSheet changes to the underlying RateSheet records'
		UPDATE tblShipperRateSheet
			SET ShipperID = i.ShipperID
				, ProductGroupID = i.ProductGroupID
				, TruckTypeID = i.TruckTypeID
				, OriginStateID = i.OriginStateID
				, DestStateID = i.DestStateID
				, RegionID = i.RegionID
				, ProducerID = i.ProducerID
				, EffectiveDate = i.EffectiveDate
				, EndDate = i.EndDate
				, RateTypeID = i.RateTypeID
				, UomID = i.UomID
				, LastChangeDateUTC = isnull(i.LastChangeDateUTC, getutcdate())
				, LastChangedByUser = i.LastChangedByUser
		FROM tblShipperRateSheet RS
		JOIN ( 
			SELECT DISTINCT i.RateSheetID, i.ShipperID, i.ProductGroupID, i.TruckTypeID, i.OriginStateID, i.DestStateID, i.RegionID, i.ProducerID, i.EffectiveDate, i.EndDate, i.RateTypeID, i.UomID, i.LastChangeDateUTC, i.LastChangedByUser
			FROM inserted i 
			JOIN deleted d ON d.ID = i.ID
			WHERE dbo.fnCompareNullableInts(i.ShipperID, d.ShipperID) = 0
				OR dbo.fnCompareNullableInts(i.ProductGroupID, d.ProductGroupID) = 0
				OR dbo.fnCompareNullableInts(i.TruckTypeID, d.TruckTypeID) = 0
				OR dbo.fnCompareNullableInts(i.OriginStateID, d.OriginStateID) = 0
				OR dbo.fnCompareNullableInts(i.DestStateID, d.DestStateID) = 0
				OR dbo.fnCompareNullableInts(i.RegionID, d.RegionID) = 0
				OR dbo.fnCompareNullableInts(i.ProducerID, d.ProducerID) = 0
				OR i.EndDate <> d.EndDate
		) i ON i.RateSheetID = RS.ID

		--PRINT 'ensure a RateSheet record exists for each new RangeRate records'
		INSERT INTO tblShipperRateSheet (ShipperID, ProductGroupID, TruckTypeID, OriginStateID, DestStateID, RegionID, ProducerID, EffectiveDate, EndDate, RateTypeID, UomID, CreatedByUser)
			SELECT i.ShipperID, i.ProductGroupID, i.TruckTypeID, i.OriginStateID, i.DestStateID, i.RegionID, i.ProducerID, i.EffectiveDate, i.EndDate, i.RateTypeID, i.UomID, i.CreatedByUser 
			FROM inserted i
			LEFT JOIN tblShipperRateSheet RS 
				ON i.EffectiveDate = RS.EffectiveDate
					AND dbo.fnCompareNullableInts(i.ShipperID, RS.ShipperID) = 1
					AND dbo.fnCompareNullableInts(i.ProductGroupID, RS.ProductGroupID) = 1
					AND dbo.fnCompareNullableInts(i.TruckTypeID, RS.TruckTypeID) = 1
					AND dbo.fnCompareNullableInts(i.OriginStateID, RS.OriginStateID) = 1
					AND dbo.fnCompareNullableInts(i.DestStateID, RS.DestStateID) = 1
					AND dbo.fnCompareNullableInts(i.RegionID, RS.RegionID) = 1
					AND dbo.fnCompareNullableInts(i.ProducerID, RS.ProducerID) = 1
			WHERE isnull(RateSheetID, 0) = 0
			  AND RS.ID IS NULL
		
		-- optimization for typical usaage (where only 1 record is INSERTed)
		IF (SELECT count(1) FROM inserted i WHERE isnull(ID, 0) = 0) = 1
		BEGIN
			DECLARE @rsID int
			SELECT @rsID = isnull(RateSheetID, SCOPE_IDENTITY()) FROM inserted
			IF @rsID IS NULL
				SELECT @rsID = RS.ID
				FROM inserted i
				LEFT JOIN tblShipperRateSheet RS 
					ON i.EffectiveDate = RS.EffectiveDate
						AND dbo.fnCompareNullableInts(i.ShipperID, RS.ShipperID) = 1
						AND dbo.fnCompareNullableInts(i.ProductGroupID, RS.ProductGroupID) = 1
						AND dbo.fnCompareNullableInts(i.TruckTypeID, RS.TruckTypeID) = 1
						AND dbo.fnCompareNullableInts(i.OriginStateID, RS.OriginStateID) = 1
						AND dbo.fnCompareNullableInts(i.DestStateID, RS.DestStateID) = 1
						AND dbo.fnCompareNullableInts(i.RegionID, RS.RegionID) = 1
						AND dbo.fnCompareNullableInts(i.ProducerID, RS.ProducerID) = 1

			INSERT INTO tblShipperRangeRate (RateSheetID, MinRange, MaxRange, Rate, CreateDateUTC, CreatedByUser)
				SELECT @rsID, i.MinRange, i.MaxRange, i.Rate, isnull(i.CreateDateUTC, getutcdate()), i.CreatedByUser
				FROM inserted i
				WHERE isnull(ID, 0) = 0
		END
		ELSE 	-- bulk insert or update
		BEGIN
			-- PRINT 'Updating any existing record editable data'
			UPDATE tblShipperRangeRate
				SET MinRange = i.MinRange
					, MaxRange = i.MaxRange
					, Rate = i.Rate
					, LastChangeDateUTC = isnull(i.LastChangeDateUTC, getutcdate())
					, LastChangedByUser = i.LastChangedByUser
			FROM tblShipperRangeRate X
			JOIN inserted i ON i.ID = X.ID

			-- PRINT 'insert any new records'
			INSERT INTO tblShipperRangeRate (RateSheetID, MinRange, MaxRange, Rate, CreatedByUser)
				SELECT R.ID, i.MinRange, i.MaxRange, i.Rate, isnull(i.CreatedByUser, R.CreatedByUser)
				FROM inserted i
				JOIN tblShipperRateSheet R ON i.EffectiveDate = R.EffectiveDate
					AND dbo.fnCompareNullableInts(i.ShipperID, R.ShipperID) = 1
					AND dbo.fnCompareNullableInts(i.ProductGroupID, R.ProductGroupID) = 1
				    AND dbo.fnCompareNullableInts(i.TruckTypeID, R.TruckTypeID) = 1
					AND dbo.fnCompareNullableInts(i.OriginStateID, R.OriginStateID) = 1
					AND dbo.fnCompareNullableInts(i.DestStateID, R.DestStateID) = 1
					AND dbo.fnCompareNullableInts(i.ProducerID, R.ProducerID) = 1
					AND dbo.fnCompareNullableInts(i.RegionID, R.RegionID) = 1
				WHERE ISNULL(i.ID, 0) = 0
		END
	END TRY
	BEGIN CATCH
		DECLARE @error varchar(255)
		SET @error = substring(ERROR_MESSAGE(), 1, 255)
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END CATCH
END

GO


/*************************************
-- Date Created: 18 Jan 2015
-- Author: Kevin Alons
-- Purpose: handle specialized logic related to editing Route Rates (due to RouteID being "inputted" as Origin|Destination combination)
-- Changes
	- 3.10.13.4 - 2016/02/29 - JAE - Add truck type
*************************************/
ALTER TRIGGER [dbo].[trigViewShipperRouteRate_IU_Update] ON [dbo].[viewShipperRouteRate] INSTEAD OF INSERT, UPDATE  AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
		--PRINT 'ensure a Route record exists for the new Origin-Destination combo'
		INSERT INTO tblRoute (OriginID, DestinationID)
			SELECT DISTINCT OriginID, DestinationID FROM inserted
			EXCEPT SELECT OriginID, DestinationID FROM tblRoute
		
		-- PRINT 'Updating any existing record editable data'
		UPDATE tblShipperRouteRate
			SET ShipperID = nullif(i.ShipperID, 0)
				, ProductGroupID = nullif(i.ProductGroupID, 0)
				, TruckTypeID = nullif(i.TruckTypeID, 0)
				, RouteID = R.ID
				, Rate = i.Rate
				, EffectiveDate = i.EffectiveDate
				, EndDate = i.EndDate
				, RateTypeID = i.RateTypeID
				, UomID = i.UomID
				, LastChangeDateUTC = i.LastChangeDateUTC
				, LastChangedByUser = i.LastChangedByUser
		FROM tblShipperRouteRate X
		JOIN inserted i ON i.ID = X.ID
		JOIN tblRoute R ON R.OriginID = i.OriginID AND R.DestinationID = i.DestinationID 

		-- PRINT 'insert any new records'
		INSERT INTO tblShipperRouteRate (ShipperID, ProductGroupID, TruckTypeID, RouteID, Rate, EffectiveDate, EndDate, RateTypeID, UomID, CreatedByUser)
			SELECT nullif(ShipperID, 0), nullif(ProductGroupID, 0), nullif(TruckTypeID, 0), R.ID, Rate, EffectiveDate, EndDate, RateTypeID, UomID, i.CreatedByUser
			FROM inserted i
			JOIN tblRoute R ON R.OriginID = i.OriginID AND R.DestinationID = i.DestinationID 
			WHERE ISNULL(i.ID, 0) = 0
	END TRY
	BEGIN CATCH
		DECLARE @error varchar(255)
		SET @error = ERROR_MESSAGE()
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END CATCH
END

GO



COMMIT
SET NOEXEC OFF