/*
	-- performance optimizations to Route Rate query logic - fixes
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.1.6'
SELECT  @NewVersion = '3.1.7'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[viewOrder_OriginCarriers]'))
	DROP VIEW [dbo].[viewOrder_OriginCarriers]
GO

/***********************************/
-- Date Created: 30 Aug 2014
-- Author: Kevin Alons
-- Purpose: return Carrier order stats (first active order and open order count)
/***********************************/
CREATE VIEW [dbo].[viewCarrier_OrderInfo] AS
SELECT CarrierID = C.ID
	, OpenOrderCount = (SELECT count(1) FROM tblOrder WHERE CarrierID = C.ID AND DeleteDateUTC IS NULL AND StatusID NOT IN (-10,9,1,3,4)) 
	, OpenOrderMinDate = (SELECT cast(MIN(OriginDepartTimeUTC) as date) FROM tblOrder WHERE CarrierID = C.ID AND DeleteDateUTC IS NULL AND StatusID NOT IN (-10,9,1,3,4)) 
	, AuditOrderCount = (SELECT count(1) FROM tblOrder WHERE CarrierID = C.ID AND DeleteDateUTC IS NULL AND StatusID IN (4)) 
	, AuditOrderMinDate = (SELECT cast(MIN(OriginDepartTimeUTC) as date) FROM tblOrder WHERE CarrierID = C.ID AND DeleteDateUTC IS NULL AND StatusID IN (4)) 
FROM tblCarrier C

GO

/***********************************/
-- Date Created: 30 Aug 2014
-- Author: Kevin Alons
-- Purpose: return Carrier Destination Wait rates with translated "friendly" values, computed Earliest & End Date values)
/***********************************/
ALTER VIEW [dbo].[viewCarrierDestinationWaitRates] AS
SELECT RB.ID
	, DestinationWaitReasonID = WR.ID
	, Rate = coalesce(RB.Rate, CRX.WaitFee, CRN.WaitFee, 0)
	, EffectiveDate = coalesce(RB.EffectiveDate, dbo.fnDateOnly(COI.OpenOrderMinDate), cast(getdate() as date)) 
	, OpenOrderCount = isnull(COI.OpenOrderCount, 0) 
	, RB.CreateDateUTC, RB.CreatedByUser, RB.LastChangeDateUTC, RB.LastChangedByUser
	, RB.EndDate
	, RB.EarliestEffectiveDate
	, CASE WHEN RB.ID IS NULL THEN 'Missing' ELSE 'Active' END AS Status
	, CarrierID = C.ID
	, Carrier = C.Name 
	, DestinationWaitReasonNum = WR.Num
	, DestinationWaitReasonDescription = WR.Description
FROM (viewCarrier C CROSS JOIN tblDestinationWaitReason WR)
LEFT JOIN viewCarrierDestinationWaitRatesBase RB ON RB.CarrierID = C.ID AND RB.DestinationWaitReasonID = WR.ID
LEFT JOIN viewCarrier_OrderInfo COI ON COI.CarrierID = C.ID
-- get the Carrier specific Carrier.WaitFee
LEFT JOIN (SELECT CarrierID, WaitFee FROM tblCarrierRates) CRX ON CRX.CarrierID = C.ID
-- get the generic Carrier.WaitFee
LEFT JOIN (SELECT CarrierID, WaitFee FROM tblCarrierRates WHERE CarrierID = -1 AND RegionID = -1) CRN ON CRN.CarrierID = -1

GO

/***********************************/
-- Date Created: 30 Aug 2014
-- Author: Kevin Alons
-- Purpose: return Carrier Order Reject rates with translated "friendly" values, computed Earliest & End Date values)
/***********************************/
ALTER VIEW [dbo].[viewCarrierOrderRejectRates] AS
SELECT RB.ID
	, OrderRejectReasonID = WR.ID
	, Rate = coalesce(RB.Rate, CRX.WaitFee, CRN.WaitFee, 0)
	, EffectiveDate = coalesce(RB.EffectiveDate, dbo.fnDateOnly(COI.OpenOrderMinDate), cast(getdate() as date)) 
	, OpenOrderCount = isnull(COI.OpenOrderCount, 0) 
	, RB.CreateDateUTC, RB.CreatedByUser, RB.LastChangeDateUTC, RB.LastChangedByUser
	, RB.EndDate
	, RB.EarliestEffectiveDate
	, CASE WHEN RB.ID IS NULL THEN 'Missing' ELSE 'Active' END AS Status
	, CarrierID = C.ID
	, Carrier = C.Name 
	, OrderRejectReasonNum = WR.Num
	, OrderRejectReasonDescription = WR.Description
FROM (viewCarrier C CROSS JOIN tblOrderRejectReason WR)
LEFT JOIN viewCarrierOrderRejectRatesBase RB ON RB.CarrierID = C.ID AND RB.OrderRejectReasonID = WR.ID
LEFT JOIN viewCarrier_OrderInfo COI ON COI.CarrierID = C.ID
-- get the Carrier specific Carrier.WaitFee
LEFT JOIN (SELECT CarrierID, WaitFee FROM tblCarrierRates) CRX ON CRX.CarrierID = C.ID
-- get the generic Carrier.WaitFee
LEFT JOIN (SELECT CarrierID, WaitFee FROM tblCarrierRates WHERE CarrierID = -1 AND RegionID = -1) CRN ON CRN.CarrierID = -1

GO

/***********************************/
-- Date Created: 30 Aug 2014
-- Author: Kevin Alons
-- Purpose: return Carrier Origin Wait rates with translated "friendly" values, computed Earliest & End Date values)
/***********************************/
ALTER VIEW [dbo].[viewCarrierOriginWaitRates] AS
SELECT RB.ID
	, OriginWaitReasonID = WR.ID
	, Rate = coalesce(RB.Rate, CRX.WaitFee, CRN.WaitFee, 0)
	, EffectiveDate = coalesce(RB.EffectiveDate, dbo.fnDateOnly(COI.OpenOrderMinDate), cast(getdate() as date)) 
	, OpenOrderCount = isnull(COI.OpenOrderCount, 0) 
	, RB.CreateDateUTC, RB.CreatedByUser, RB.LastChangeDateUTC, RB.LastChangedByUser
	, RB.EndDate
	, RB.EarliestEffectiveDate
	, CASE WHEN RB.ID IS NULL THEN 'Missing' ELSE 'Active' END AS Status
	, CarrierID = C.ID
	, Carrier = C.Name 
	, OriginWaitReasonNum = WR.Num
	, OriginWaitReasonDescription = WR.Description
FROM (viewCarrier C CROSS JOIN tblOriginWaitReason WR)
LEFT JOIN viewCarrierOriginWaitRatesBase RB ON RB.CarrierID = C.ID AND RB.OriginWaitReasonID = WR.ID
LEFT JOIN viewCarrier_OrderInfo COI ON COI.CarrierID = C.ID
-- get the Carrier specific Carrier.WaitFee
LEFT JOIN (SELECT CarrierID, WaitFee FROM tblCarrierRates) CRX ON CRX.CarrierID = C.ID
-- get the generic Carrier.WaitFee
LEFT JOIN (SELECT CarrierID, WaitFee FROM tblCarrierRates WHERE CarrierID = -1 AND RegionID = -1) CRN ON CRN.CarrierID = -1

GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[viewOrder_CarrierOrderInfo]'))
	DROP VIEW [dbo].viewOrder_CarrierOrderInfo
GO

/***********************************/
-- Date Created: 20 Sep 2014
-- Author: Kevin Alons
-- Purpose: return all valid (AUDIT ORDER) Carrier-Route combinations
/***********************************/
CREATE VIEW [dbo].[viewOrder_CarrierRoutes] AS
	SELECT CarrierID
		, RouteID 
		, AuditOrderCount = COUNT(1)
		, MinAuditOrderDate = cast(MIN(OriginDepartTimeUTC) as date)
	FROM tblOrder 
	WHERE DeleteDateUTC IS NULL 
		AND StatusID = 4 
		AND RouteID IS NOT NULL 
		AND CarrierID IS NOT NULL
	GROUP BY CarrierID, RouteID
GO
GRANT SELECT ON viewOrder_CarrierRoutes TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 18 Sep 2014
-- Author: Kevin Alons
-- Purpose: return Carrier route rates per the specified criteria (used by Carrier Route Rates page)
/***********************************/
ALTER PROCEDURE [dbo].[spGetCarrierRouteRates](
  @Carrierid int
, @originID int
, @destinationID int
, @startDate date
, @includeActive bit
, @includeMissing bit
, @includeRouteInactive bit
, @IncludeCarrierDeleted bit
, @includeMissingRoutes bit
) AS
BEGIN
	SELECT TOP 100 PERCENT *
		, CASE WHEN Status LIKE '%Missing' THEN Rate ELSE NULL END AS NewRate
		, CASE WHEN Status LIKE '%Missing' THEN EffectiveDate ELSE NULL END AS NewEffectiveDate
		, NULL AS ImportOutcome 
	INTO #data
	FROM (
		-- get the currently defined or missing rates
		SELECT ID, RouteID, UomID, Rate, EffectiveDate, Active, OpenOrderCount, ActualMiles
			, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser
			, EndDate, EarliestEffectiveDate, CarrierID, Carrier, OriginID, Origin, OriginFull, DestinationID, Destination, DestinationFull
			, Status, RouteInUse, UOM, UomShort
		FROM dbo.viewCarrierRouteRates 
		WHERE (@CarrierID = -1 OR CarrierID = @CarrierID) 
			AND (@OriginID = -1 OR OriginID = @OriginID) 
			AND (@DestinationID = -1 OR DestinationID = @DestinationID) 
			AND (EndDate IS NULL OR EndDate >= @StartDate) 
			AND (@IncludeActive = 1 OR Status NOT LIKE 'Active') 
			AND (@IncludeMissing = 1 OR Status NOT LIKE 'Missing') 
			AND (@IncludeRouteInactive = 1 OR Status NOT LIKE 'Route Inactive') 
			AND (@IncludeCarrierDeleted = 1 OR Status NOT LIKE 'Carrier Deleted')	
		UNION 

		-- generate any rates that could exist for missing/undefined routes
		SELECT ID = NULL, RouteID = NULL, UomID = O.UomID, Rate = NULL, EffectiveDate = isnull(@StartDate, getdate()), Active = cast(0 as bit), OpenOrderCount = 0, ActualMiles = 0
		, null, null, null, null
		, null, null, C.ID, C.Name, O.ID, O.Name, O.FullName, D.ID, D.Name, D.FullName
		, 'Route Missing', cast(0 as bit), U.Name, U.Abbrev 
		FROM viewOrigin O
		JOIN tblUom U ON U.ID = O.UomID
		CROSS JOIN viewDestination D
		JOIN tblCarrier C ON C.ID = @CarrierID
		LEFT JOIN viewCarrierRouteRates R ON R.OriginID = O.ID AND R.DestinationID = D.ID AND (R.EndDate IS NULL OR EndDate >= @StartDate)
		WHERE (@includeMissingRoutes = 1)
		  AND R.ID IS NULL
		  AND (@CarrierID = -1 OR C.ID = @CarrierID)
		  AND (@originID = -1 OR O.ID = @originID)
		  AND (@destinationID = -1 OR O.ID = @destinationID)
	) X
	
	SELECT TOP 100 PERCENT D.*
	FROM #data D
	JOIN viewOrder_CarrierRoutes OCR ON OCR.RouteID = D.RouteID AND OCR.CarrierID = D.CarrierID
	ORDER BY d.Carrier, d.Origin, D.Destination, D.EffectiveDate DESC
		
END

GO

/***********************************/
-- Date Created: 30 Aug 2014
-- Author: Kevin Alons
-- Purpose: return Customer order stats (first active order and open order count)
/***********************************/
CREATE VIEW [dbo].[viewCustomer_OrderInfo] AS
SELECT CustomerID = C.ID
	, OpenOrderCount = (SELECT count(1) FROM tblOrder WHERE CustomerID = C.ID AND DeleteDateUTC IS NULL AND StatusID NOT IN (-10,9,1,3,4)) 
	, OpenOrderMinDate = (SELECT cast(MIN(OriginDepartTimeUTC) as date) FROM tblOrder WHERE CustomerID = C.ID AND DeleteDateUTC IS NULL AND StatusID NOT IN (-10,9,1,3,4)) 
	, AuditOrderCount = (SELECT count(1) FROM tblOrder WHERE CustomerID = C.ID AND DeleteDateUTC IS NULL AND StatusID IN (4)) 
	, AuditOrderMinDate = (SELECT cast(MIN(OriginDepartTimeUTC) as date) FROM tblOrder WHERE CustomerID = C.ID AND DeleteDateUTC IS NULL AND StatusID IN (4)) 
FROM tblCustomer C

GO

/***********************************/
-- Date Created: 30 Aug 2014
-- Author: Kevin Alons
-- Purpose: return Customer Destination Wait rates with translated "friendly" values, computed Earliest & End Date values)
/***********************************/
ALTER VIEW [dbo].[viewCustomerDestinationWaitRates] AS
SELECT RB.ID
	, DestinationWaitReasonID = WR.ID
	, Rate = coalesce(RB.Rate, CRX.WaitFee, CRN.WaitFee, 0)
	, EffectiveDate = coalesce(RB.EffectiveDate, dbo.fnDateOnly(COI.OpenOrderMinDate), cast(getdate() as date)) 
	, OpenOrderCount = isnull(COI.OpenOrderCount, 0) 
	, RB.CreateDateUTC, RB.CreatedByUser, RB.LastChangeDateUTC, RB.LastChangedByUser
	, RB.EndDate
	, RB.EarliestEffectiveDate
	, CASE WHEN RB.ID IS NULL THEN 'Missing' ELSE 'Active' END AS Status
	, CustomerID = C.ID
	, Customer = C.Name 
	, DestinationWaitReasonNum = WR.Num
	, DestinationWaitReasonDescription = WR.Description
FROM (viewCustomer C CROSS JOIN tblDestinationWaitReason WR)
LEFT JOIN viewCustomerDestinationWaitRatesBase RB ON RB.CustomerID = C.ID AND RB.DestinationWaitReasonID = WR.ID
LEFT JOIN viewCustomer_OrderInfo COI ON COI.CustomerID = C.ID
-- get the Customer specific Customer.WaitFee
LEFT JOIN (SELECT CustomerID, WaitFee FROM tblCustomerRates) CRX ON CRX.CustomerID = C.ID
-- get the generic Customer.WaitFee
LEFT JOIN (SELECT CustomerID, WaitFee FROM tblCustomerRates WHERE CustomerID = -1 AND RegionID = -1) CRN ON CRN.CustomerID = -1

GO

/***********************************/
-- Date Created: 30 Aug 2014
-- Author: Kevin Alons
-- Purpose: return Customer Order Reject rates with translated "friendly" values, computed Earliest & End Date values)
/***********************************/
ALTER VIEW [dbo].[viewCustomerOrderRejectRates] AS
SELECT RB.ID
	, OrderRejectReasonID = WR.ID
	, Rate = coalesce(RB.Rate, CRX.WaitFee, CRN.WaitFee, 0)
	, EffectiveDate = coalesce(RB.EffectiveDate, dbo.fnDateOnly(COI.OpenOrderMinDate), cast(getdate() as date)) 
	, OpenOrderCount = isnull(COI.OpenOrderCount, 0) 
	, RB.CreateDateUTC, RB.CreatedByUser, RB.LastChangeDateUTC, RB.LastChangedByUser
	, RB.EndDate
	, RB.EarliestEffectiveDate
	, CASE WHEN RB.ID IS NULL THEN 'Missing' ELSE 'Active' END AS Status
	, CustomerID = C.ID
	, Customer = C.Name 
	, OrderRejectReasonNum = WR.Num
	, OrderRejectReasonDescription = WR.Description
FROM (viewCustomer C CROSS JOIN tblOrderRejectReason WR)
LEFT JOIN viewCustomerOrderRejectRatesBase RB ON RB.CustomerID = C.ID AND RB.OrderRejectReasonID = WR.ID
LEFT JOIN viewCustomer_OrderInfo COI ON COI.CustomerID = C.ID
-- get the Customer specific Customer.WaitFee
LEFT JOIN (SELECT CustomerID, WaitFee FROM tblCustomerRates) CRX ON CRX.CustomerID = C.ID
-- get the generic Customer.WaitFee
LEFT JOIN (SELECT CustomerID, WaitFee FROM tblCustomerRates WHERE CustomerID = -1 AND RegionID = -1) CRN ON CRN.CustomerID = -1

GO

/***********************************/
-- Date Created: 30 Aug 2014
-- Author: Kevin Alons
-- Purpose: return Customer Origin Wait rates with translated "friendly" values, computed Earliest & End Date values)
/***********************************/
ALTER VIEW [dbo].[viewCustomerOriginWaitRates] AS
SELECT RB.ID
	, OriginWaitReasonID = WR.ID
	, Rate = coalesce(RB.Rate, CRX.WaitFee, CRN.WaitFee, 0)
	, EffectiveDate = coalesce(RB.EffectiveDate, dbo.fnDateOnly(COI.OpenOrderMinDate), cast(getdate() as date)) 
	, OpenOrderCount = isnull(COI.OpenOrderCount, 0) 
	, RB.CreateDateUTC, RB.CreatedByUser, RB.LastChangeDateUTC, RB.LastChangedByUser
	, RB.EndDate
	, RB.EarliestEffectiveDate
	, CASE WHEN RB.ID IS NULL THEN 'Missing' ELSE 'Active' END AS Status
	, CustomerID = C.ID
	, Customer = C.Name 
	, OriginWaitReasonNum = WR.Num
	, OriginWaitReasonDescription = WR.Description
FROM (viewCustomer C CROSS JOIN tblOriginWaitReason WR)
LEFT JOIN viewCustomerOriginWaitRatesBase RB ON RB.CustomerID = C.ID AND RB.OriginWaitReasonID = WR.ID
LEFT JOIN viewCustomer_OrderInfo COI ON COI.CustomerID = C.ID
-- get the Customer specific Customer.WaitFee
LEFT JOIN (SELECT CustomerID, WaitFee FROM tblCustomerRates) CRX ON CRX.CustomerID = C.ID
-- get the generic Customer.WaitFee
LEFT JOIN (SELECT CustomerID, WaitFee FROM tblCustomerRates WHERE CustomerID = -1 AND RegionID = -1) CRN ON CRN.CustomerID = -1

GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[viewOrder_CustomerOrderInfo]'))
	DROP VIEW [dbo].viewOrder_CustomerOrderInfo
GO

/***********************************/
-- Date Created: 20 Sep 2014
-- Author: Kevin Alons
-- Purpose: return all valid (AUDIT ORDER) Customer-Route combinations
/***********************************/
CREATE VIEW [dbo].[viewOrder_CustomerRoutes] AS
	SELECT CustomerID
		, RouteID 
		, AuditOrderCount = COUNT(1)
		, MinAuditOrderDate = cast(MIN(OriginDepartTimeUTC) as date)
	FROM tblOrder 
	WHERE DeleteDateUTC IS NULL 
		AND StatusID = 4 
		AND RouteID IS NOT NULL 
		AND CustomerID IS NOT NULL
	GROUP BY CustomerID, RouteID
GO
GRANT SELECT ON viewOrder_CustomerRoutes TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 18 Sep 2014
-- Author: Kevin Alons
-- Purpose: return Carrier route rates per the specified criteria (used by Carrier Route Rates page)
/***********************************/
ALTER PROCEDURE [dbo].[spGetCustomerRouteRates](
  @customerid int
, @originID int
, @destinationID int
, @startDate date
, @includeActive bit
, @includeMissing bit
, @includeRouteInactive bit
, @includeMissingRoutes bit
) AS
BEGIN
	SELECT TOP 100 PERCENT *
		, CASE WHEN Status LIKE '%Missing' THEN Rate ELSE NULL END AS NewRate
		, CASE WHEN Status LIKE '%Missing' THEN EffectiveDate ELSE NULL END AS NewEffectiveDate
		, NULL AS ImportOutcome 
	INTO #data
	FROM (
		-- get the currently defined or missing rates
		SELECT ID, RouteID, UomID, Rate, EffectiveDate, Active, OpenOrderCount, ActualMiles
			, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser
			, EndDate, EarliestEffectiveDate, CustomerID, Customer, OriginID, Origin, OriginFull, DestinationID, Destination, DestinationFull
			, Status, RouteInUse, UOM, UomShort
		FROM dbo.viewCustomerRouteRates 
		WHERE (@CustomerID = -1 OR CustomerID = @CustomerID) 
			AND (@OriginID = -1 OR OriginID = @OriginID) 
			AND (@DestinationID = -1 OR DestinationID = @DestinationID) 
			AND (EndDate IS NULL OR EndDate >= @StartDate) 
			AND (@IncludeActive = 1 OR Status NOT LIKE 'Active') 
			AND (@IncludeMissing = 1 OR Status NOT LIKE 'Missing') 
			AND (@IncludeRouteInactive = 1 OR Status NOT LIKE 'Route Inactive') 
		
		UNION 

		-- generate any rates that could exist for missing/undefined routes
		SELECT ID = NULL, RouteID = NULL, UomID = O.UomID, Rate = NULL, EffectiveDate = isnull(@StartDate, getdate()), Active = cast(0 as bit), OpenOrderCount = 0, ActualMiles = 0
		, null, null, null, null
		, null, null, C.ID, C.Name, O.ID, O.Name, O.FullName, D.ID, D.Name, D.FullName
		, 'Route Missing', cast(0 as bit), U.Name, U.Abbrev 
		FROM viewOrigin O
		JOIN tblUom U ON U.ID = O.UomID
		CROSS JOIN viewDestination D
		JOIN tblCustomer C ON C.ID = @CustomerID
		LEFT JOIN viewCustomerRouteRates R ON R.OriginID = O.ID AND R.DestinationID = D.ID AND (R.EndDate IS NULL OR EndDate >= @StartDate)
		WHERE (@includeMissingRoutes = 1)
		  AND R.ID IS NULL
		  AND (@customerid = -1 OR C.ID = @customerid)
		  AND (@originID = -1 OR O.ID = @originID)
		  AND (@destinationID = -1 OR O.ID = @destinationID)
	) X
	ORDER BY Customer, Origin, Destination, EffectiveDate DESC

	SELECT TOP 100 PERCENT D.*
	FROM #data D
	JOIN tblOrigin O ON O.ID = D.OriginID AND O.CustomerID = D.CustomerID
	ORDER BY D.Customer, D.Origin, D.Destination, D.EffectiveDate DESC

END

GO

EXEC _spRebuildAllObjects
GO

COMMIT
SET NOEXEC OFF