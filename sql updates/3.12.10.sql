SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.12.9'
SELECT  @NewVersion = '3.12.10'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'ImportCenter: Add ImportBatch functionality'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

CREATE TABLE tblImportCenterGroup
(
  ID int identity(1, 1) NOT NULL CONSTRAINT PK_ImportCenterGroup PRIMARY KEY CLUSTERED
, Name varchar(100) NOT NULL CONSTRAINT CK_ImportCenterGroup_Name_Unique UNIQUE
, UserNames varchar(max) NULL
, CreateDateUTC datetime NOT NULL CONSTRAINT DF_ImportCenterGroup_CreateDateUTC DEFAULT (getutcdate())
, CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_ImportCenterGroup_CreatedByUser DEFAULT ('System')
, LastChangeDateUTC datetime NULL
, LastChangedByUser varchar(100) NULL
)
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblImportCenterGroup TO role_iis_acct
GO

CREATE TABLE tblImportCenterGroupStep
(
  ID int identity(1, 1) NOT NULL CONSTRAINT PK_ImportCenterGroupStep PRIMARY KEY CLUSTERED
, GroupID int NOT NULL CONSTRAINT FK_ImportCenterGroupStep_Group FOREIGN KEY REFERENCES tblImportCenterGroup(ID) ON DELETE CASCADE
, ImportCenterDefinitionID int NOT NULL CONSTRAINT FK_ImportCenterGroupStep_ImportCenterDefinition FOREIGN KEY REFERENCES tblImportCenterDefinition(ID) 
, Position int NOT NULL
)
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblImportCenterGroupStep TO role_iis_acct
GO

COMMIT
SET NOEXEC OFF