SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.1.8.1'
SELECT  @NewVersion = '4.1.8.2'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1756: Check Final Actual Miles (not just actual miles).'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/***********************************/
-- Created: 4.1.0 - 2016/08/15 - Kevin Alons
-- Purpose: return BASE Order details + Carrier FINANCIAL INFO into a single view
-- Changes:
--	4.1.0.2 - 2016.08.28 - KDA	- remove redundant calculation of ORR.RerouteCount
--								- remove redundant tblOrigin JOIN (for H2S which is already available in viewOrder)
--	4.1.8.2 - 2016.09.21 - KDA	- Check Final Actual miles (null actual miles ok as long as override)
/***********************************/
ALTER VIEW [dbo].[viewOrder_Financial_Base_Carrier] AS 
	SELECT *
		, HasError = cast(CASE WHEN ErrorFieldCSV IS NOT NULL THEN 1 ELSE 0 END as bit)
	FROM (
		SELECT *
			, OrderID = ID
			, ErrorFieldCSV = nullif(substring(CASE WHEN Approved = 0 THEN ',Approved' ELSE '' END
				+ CASE WHEN InvoiceLoadAmount IS NULL THEN ',InvoiceLoadAmount,InvoiceRouteRate,InvoiceRateSheet' ELSE '' END
				+ CASE WHEN FinalChainup = 1 AND InvoiceChainupAmount IS NULL THEN ',InvoiceChainupAmount' ELSE '' END
				+ CASE WHEN FinalRerouteCount > 0 AND InvoiceRerouteAmount IS NULL THEN ',InvoiceRerouteAmount' ELSE '' END
				+ CASE WHEN FinalH2S = 1 AND InvoiceH2SAmount IS NULL THEN ',InvoiceH2SAmount' ELSE '' END
				+ CASE WHEN TicketCount > 1 AND InvoiceSplitLoadAmount IS NULL THEN ',InvoiceSplitLoadAmount' ELSE '' END
				+ CASE WHEN InvoiceOriginWaitBillableMinutes > 0 AND InvoiceOriginWaitAmount IS NULL THEN ',InvoiceOriginWaitAmount' ELSE '' END
				+ CASE WHEN InvoiceDestinationWaitBillableMinutes > 0 AND InvoiceDestinationWaitAmount IS NULL THEN ',InvoiceDestinationWaitAmount' ELSE '' END
				+ CASE WHEN Rejected = 1 AND InvoiceOrderRejectAmount IS NULL THEN ',InvoiceOrderRejectAmount' ELSE '' END
				+ CASE WHEN InvoiceMinSettlementUnitsID IS NULL THEN ',InvoiceMinSettlementUnits' ELSE '' END
				+ CASE WHEN InvoiceSettlementFactorID IS NULL THEN ',InvoiceSettlementFactor' ELSE '' END
				+ CASE WHEN Rejected = 0 AND InvoiceSettlementFactorID IS NULL THEN ',InvoiceSettlementFactor' ELSE '' END
				+ CASE WHEN Rejected = 0 AND InvoiceSettlementFactorID IS NOT NULL AND dbo.fnOrderSettlementUnits(InvoiceSettlementFactorID, OriginGrossUnits, OriginNetUnits, OriginGrossStdUnits, DestGrossUnits, DestNetUnits) IS NULL THEN ','+OrderUnitsField ELSE '' END
				+ CASE WHEN InvoiceWaitFeeParameterID IS NULL THEN ',InvoiceOriginWaitBillableMinutes,InvoiceDestinationWaitBillableMinutes' ELSE '' END
				+ CASE WHEN isnull(FinalActualMiles, 0) = 0 THEN ',ActualMiles' ELSE '' END, 2, 100000), '')
		FROM (
			SELECT O.* 
				, InvoiceRatesAppliedDateUTC = OS.CreateDateUTC
				, OS.BatchID
				, Approved = cast(ISNULL(OA.Approved, 0) as bit)
				, FinalActualMiles = ISNULL(OA.OverrideActualMiles, O.ActualMiles)
				, FinalOriginMinutes = ISNULL(OA.OverrideOriginMinutes, O.OriginMinutes)
				, FinalDestMinutes = ISNULL(OA.OverrideDestMinutes, O.DestMinutes)
				, FinalH2S = cast(CASE WHEN isnull(OA.OverrideH2S, 0) = 1 THEN 0 ELSE O.H2S END as bit)
				, FinalChainup = cast(CASE WHEN isnull(OA.OverrideChainup, 0) = 1 THEN 0 ELSE O.Chainup END as bit)
				, FinalRerouteCount = CASE WHEN isnull(OA.OverrideReroute, 0) = 1 THEN 0 ELSE isnull(O.RerouteCount, 0) END
				, FC.OrderUnitsField
				, InvoiceBatchNum = OS.BatchNum 
				, InvoiceOriginWaitBillableMinutes = OS.OriginWaitBillableMinutes
				, InvoiceDestinationWaitBillableMinutes = OS.DestinationWaitBillableMinutes
				, InvoiceTotalWaitBillableMinutes = isnull(OS.OriginWaitBillableMinutes, 0) + ISNULL(OS.DestinationWaitBillableMinutes, 0) 
				, InvoiceWaitFeeParameterID = WaitFeeParameterID
				, InvoiceWaitFeeSubUnit = WaitFeeSubUnit
				, InvoiceWaitFeeRoundingType = WaitFeeRoundingType
				, InvoiceOriginWaitRate = OS.OriginWaitRate 
				, InvoiceOriginWaitAmount = OS.OriginWaitAmount 		
				, InvoiceDestinationWaitRate = OS.DestinationWaitRate 
				, InvoiceDestinationWaitAmount = OS.DestinationWaitAmount 
				, InvoiceTotalWaitAmount = OS.TotalWaitAmount
				, InvoiceOrderRejectRate = OS.OrderRejectRate	
				, InvoiceOrderRejectRateType = OS.OrderRejectRateType  
				, InvoiceOrderRejectAmount = OS.OrderRejectAmount  
				, InvoiceChainupRate = OS.ChainupRate
				, InvoiceChainupRateType = OS.ChainupRateType
				, InvoiceChainupAmount = OS.ChainupAmount 
				, InvoiceRerouteRate = OS.RerouteRate
				, InvoiceRerouteRateType = OS.RerouteRateType
				, InvoiceRerouteAmount = OS.RerouteAmount 
				, InvoiceH2SRate = OS.H2SRate
				, InvoiceH2SRateType = OS.H2SRateType
				, InvoiceH2SAmount = OS.H2SAmount
				, InvoiceSplitLoadRate = OS.SplitLoadRate
				, InvoiceSplitLoadRateType = OS.SplitLoadRateType
				, InvoiceSplitLoadAmount = OS.SplitLoadAmount
				, InvoiceOtherAmount = OS.OtherAmount
				, InvoiceTaxRate = OS.OriginTaxRate
				, InvoiceSettlementUom = OS.SettlementUom 
				, InvoiceSettlementUomShort = OS.SettlementUomShort 
				, InvoiceCarrierSettlementFactorID = OS.CarrierSettlementFactorID 
				, InvoiceSettlementFactorID = OS.SettlementFactorID 
				, InvoiceSettlementFactor = OS.SettlementFactor 
				, InvoiceMinSettlementUnitsID = OS.MinSettlementUnitsID 
				, InvoiceMinSettlementUnits = OS.MinSettlementUnits
				, InvoiceUnits = OS.SettlementUnits
				, InvoiceRouteRate = OS.RouteRate
				, InvoiceRouteRateType = OS.RouteRateType
				, InvoiceRateSheetRate = OS.RateSheetRate
				, InvoiceRateSheetRateType = OS.RateSheetRateType
				, InvoiceFuelSurchargeRate = OS.FuelSurchargeRate
				, InvoiceFuelSurchargeAmount = OS.FuelSurchargeAmount
				, InvoiceLoadAmount = OS.LoadAmount
				, InvoiceTotalAmount = OS.TotalAmount
			FROM dbo.viewOrder O
			LEFT JOIN viewOrderSettlementCarrier OS ON OS.OrderID = O.ID
			LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
			LEFT JOIN tblSettlementFactor FC ON FC.ID = OS.SettlementFactorID
			WHERE O.StatusID NOT IN (-10) -- don't include "Generated" orders (only Assigned+)
		) X
	) X1


GO

/***********************************/
-- Created: 4.1.0 - 2016/08/15 - Kevin Alons
-- Purpose: return BASE Order details + Driver FINANCIAL INFO into a single view
-- Changes:
--	4.1.0.2 - 2016.08.28 - KDA	- remove redundant calculation of ORR.RerouteCount
--								- remove redundant tblOrigin JOIN (for H2S which is already available in viewOrder)
--	4.1.8.2 - 2016.09.21 - JAE	- Check Final Actual miles (null actual miles ok as long as override)
/***********************************/
ALTER VIEW [dbo].[viewOrder_Financial_Base_Driver] AS 
	SELECT *
		, HasError = cast(CASE WHEN ErrorFieldCSV IS NOT NULL THEN 1 ELSE 0 END as bit)
	FROM (
		SELECT *
			, OrderID = ID
			, ErrorFieldCSV = nullif(substring(CASE WHEN Approved = 0 THEN ',Approved' ELSE '' END
				+ CASE WHEN InvoiceLoadAmount IS NULL THEN ',InvoiceLoadAmount,InvoiceRouteRate,InvoiceRateSheet' ELSE '' END
				+ CASE WHEN FinalChainup = 1 AND InvoiceChainupAmount IS NULL THEN ',InvoiceChainupAmount' ELSE '' END
				+ CASE WHEN FinalRerouteCount > 0 AND InvoiceRerouteAmount IS NULL THEN ',InvoiceRerouteAmount' ELSE '' END
				+ CASE WHEN FinalH2S = 1 AND InvoiceH2SAmount IS NULL THEN ',InvoiceH2SAmount' ELSE '' END
				+ CASE WHEN TicketCount > 1 AND InvoiceSplitLoadAmount IS NULL THEN ',InvoiceSplitLoadAmount' ELSE '' END
				+ CASE WHEN InvoiceOriginWaitBillableMinutes > 0 AND InvoiceOriginWaitAmount IS NULL THEN ',InvoiceOriginWaitAmount' ELSE '' END
				+ CASE WHEN InvoiceDestinationWaitBillableMinutes > 0 AND InvoiceDestinationWaitAmount IS NULL THEN ',InvoiceDestinationWaitAmount' ELSE '' END
				+ CASE WHEN Rejected = 1 AND InvoiceOrderRejectAmount IS NULL THEN ',InvoiceOrderRejectAmount' ELSE '' END
				+ CASE WHEN InvoiceMinSettlementUnitsID IS NULL THEN ',InvoiceMinSettlementUnits' ELSE '' END
				+ CASE WHEN InvoiceSettlementFactorID IS NULL THEN ',InvoiceSettlementFactor' ELSE '' END
				+ CASE WHEN Rejected = 0 AND InvoiceSettlementFactorID IS NULL THEN ',InvoiceSettlementFactor' ELSE '' END
				+ CASE WHEN Rejected = 0 AND InvoiceSettlementFactorID IS NOT NULL AND dbo.fnOrderSettlementUnits(InvoiceSettlementFactorID, OriginGrossUnits, OriginNetUnits, OriginGrossStdUnits, DestGrossUnits, DestNetUnits) IS NULL THEN ','+OrderUnitsField ELSE '' END
				+ CASE WHEN InvoiceWaitFeeParameterID IS NULL THEN ',InvoiceOriginWaitBillableMinutes,InvoiceDestinationWaitBillableMinutes' ELSE '' END
				+ CASE WHEN isnull(FinalActualMiles, 0) = 0 THEN ',ActualMiles' ELSE '' END, 2, 100000), '')
		FROM (
			SELECT O.* 
				, DriverGroup = DG.Name
				, InvoiceRatesAppliedDateUTC = OS.CreateDateUTC
				, OS.BatchID
				, Approved = cast(ISNULL(OA.Approved, 0) as bit)
				, FinalActualMiles = ISNULL(OA.OverrideActualMiles, O.ActualMiles)
				, FinalOriginMinutes = ISNULL(OA.OverrideOriginMinutes, O.OriginMinutes)
				, FinalDestMinutes = ISNULL(OA.OverrideDestMinutes, O.DestMinutes)
				, FinalH2S = cast(CASE WHEN isnull(OA.OverrideH2S, 0) = 1 THEN 0 ELSE O.H2S END as bit)
				, FinalChainup = cast(CASE WHEN isnull(OA.OverrideChainup, 0) = 1 THEN 0 ELSE O.Chainup END as bit)
				, FinalRerouteCount = CASE WHEN isnull(OA.OverrideReroute, 0) = 1 THEN 0 ELSE isnull(O.RerouteCount, 0) END
				, FC.OrderUnitsField
				, InvoiceBatchNum = OS.BatchNum 
				, InvoiceOriginWaitBillableMinutes = OS.OriginWaitBillableMinutes
				, InvoiceDestinationWaitBillableMinutes = OS.DestinationWaitBillableMinutes
				, InvoiceTotalWaitBillableMinutes = isnull(OS.OriginWaitBillableMinutes, 0) + ISNULL(OS.DestinationWaitBillableMinutes, 0) 
				, InvoiceWaitFeeParameterID = WaitFeeParameterID
				, InvoiceWaitFeeSubUnit = WaitFeeSubUnit
				, InvoiceWaitFeeRoundingType = WaitFeeRoundingType
				, InvoiceOriginWaitRate = OS.OriginWaitRate 
				, InvoiceOriginWaitAmount = OS.OriginWaitAmount 		
				, InvoiceDestinationWaitRate = OS.DestinationWaitRate 
				, InvoiceDestinationWaitAmount = OS.DestinationWaitAmount 
				, InvoiceTotalWaitAmount = OS.TotalWaitAmount
				, InvoiceOrderRejectRate = OS.OrderRejectRate	
				, InvoiceOrderRejectRateType = OS.OrderRejectRateType  
				, InvoiceOrderRejectAmount = OS.OrderRejectAmount  
				, InvoiceChainupRate = OS.ChainupRate
				, InvoiceChainupRateType = OS.ChainupRateType
				, InvoiceChainupAmount = OS.ChainupAmount 
				, InvoiceRerouteRate = OS.RerouteRate
				, InvoiceRerouteRateType = OS.RerouteRateType
				, InvoiceRerouteAmount = OS.RerouteAmount 
				, InvoiceH2SRate = OS.H2SRate
				, InvoiceH2SRateType = OS.H2SRateType
				, InvoiceH2SAmount = OS.H2SAmount
				, InvoiceSplitLoadRate = OS.SplitLoadRate
				, InvoiceSplitLoadRateType = OS.SplitLoadRateType
				, InvoiceSplitLoadAmount = OS.SplitLoadAmount
				, InvoiceOtherAmount = OS.OtherAmount
				, InvoiceTaxRate = OS.OriginTaxRate
				, InvoiceSettlementUom = OS.SettlementUom 
				, InvoiceSettlementUomShort = OS.SettlementUomShort 
				, InvoiceCarrierSettlementFactorID = OS.CarrierSettlementFactorID 
				, InvoiceSettlementFactorID = OS.SettlementFactorID 
				, InvoiceSettlementFactor = OS.SettlementFactor 
				, InvoiceMinSettlementUnitsID = OS.MinSettlementUnitsID 
				, InvoiceMinSettlementUnits = OS.MinSettlementUnits
				, InvoiceUnits = OS.SettlementUnits
				, InvoiceRouteRate = OS.RouteRate
				, InvoiceRouteRateType = OS.RouteRateType
				, InvoiceRateSheetRate = OS.RateSheetRate
				, InvoiceRateSheetRateType = OS.RateSheetRateType
				, InvoiceFuelSurchargeRate = OS.FuelSurchargeRate
				, InvoiceFuelSurchargeAmount = OS.FuelSurchargeAmount
				, InvoiceLoadAmount = OS.LoadAmount
				, InvoiceTotalAmount = OS.TotalAmount
			FROM dbo.viewOrder O
			LEFT JOIN tblDriverGroup DG ON DG.ID = O.DriverGroupID
			LEFT JOIN viewOrderSettlementDriver OS ON OS.OrderID = O.ID
			LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
			LEFT JOIN tblSettlementFactor FC ON FC.ID = OS.SettlementFactorID
			WHERE O.StatusID NOT IN (-10) -- don't include "Generated" orders (only Assigned+)
		) X
	) X1


GO


/***********************************/
-- Created: 4.1.0 - 2016/08/15 - Kevin Alons
-- Purpose: return BASE Order details + Shipper FINANCIAL INFO into a single view
-- Changes:
--	4.1.0.2 - 2016.08.28 - KDA	- remove redundant calculation of ORR.RerouteCount
--								- remove redundant tblOrigin JOIN (for H2S which is already available in viewOrder)
--	4.1.8.2 - 2016.09.21 - JAE	- Check Final Actual miles (null actual miles ok as long as override)
/***********************************/
ALTER VIEW [dbo].[viewOrder_Financial_Base_Shipper] AS 
	SELECT *
		, HasError = cast(CASE WHEN ErrorFieldCSV IS NOT NULL THEN 1 ELSE 0 END as bit)
	FROM (
		SELECT *
			, OrderID = ID
			, ErrorFieldCSV = nullif(substring(CASE WHEN Approved = 0 THEN ',Approved' ELSE '' END
				+ CASE WHEN InvoiceLoadAmount IS NULL THEN ',InvoiceLoadAmount,InvoiceRouteRate,InvoiceRateSheet' ELSE '' END
				+ CASE WHEN FinalChainup = 1 AND InvoiceChainupAmount IS NULL THEN ',InvoiceChainupAmount' ELSE '' END
				+ CASE WHEN FinalRerouteCount > 0 AND InvoiceRerouteAmount IS NULL THEN ',InvoiceRerouteAmount' ELSE '' END
				+ CASE WHEN FinalH2S = 1 AND InvoiceH2SAmount IS NULL THEN ',InvoiceH2SAmount' ELSE '' END
				+ CASE WHEN TicketCount > 1 AND InvoiceSplitLoadAmount IS NULL THEN ',InvoiceSplitLoadAmount' ELSE '' END
				+ CASE WHEN InvoiceOriginWaitBillableMinutes > 0 AND InvoiceOriginWaitAmount IS NULL THEN ',InvoiceOriginWaitAmount' ELSE '' END
				+ CASE WHEN InvoiceDestinationWaitBillableMinutes > 0 AND InvoiceDestinationWaitAmount IS NULL THEN ',InvoiceDestinationWaitAmount' ELSE '' END
				+ CASE WHEN Rejected = 1 AND InvoiceOrderRejectAmount IS NULL THEN ',InvoiceOrderRejectAmount' ELSE '' END
				+ CASE WHEN InvoiceMinSettlementUnitsID IS NULL THEN ',InvoiceMinSettlementUnits' ELSE '' END
				+ CASE WHEN InvoiceSettlementFactorID IS NULL THEN ',InvoiceSettlementFactor' ELSE '' END
				+ CASE WHEN Rejected = 0 AND InvoiceSettlementFactorID IS NULL THEN ',InvoiceSettlementFactor' ELSE '' END
				+ CASE WHEN Rejected = 0 AND InvoiceSettlementFactorID IS NOT NULL AND dbo.fnOrderSettlementUnits(InvoiceSettlementFactorID, OriginGrossUnits, OriginNetUnits, OriginGrossStdUnits, DestGrossUnits, DestNetUnits) IS NULL THEN ','+OrderUnitsField ELSE '' END
				+ CASE WHEN InvoiceWaitFeeParameterID IS NULL THEN ',InvoiceOriginWaitBillableMinutes,InvoiceDestinationWaitBillableMinutes' ELSE '' END
				+ CASE WHEN isnull(FinalActualMiles, 0) = 0 THEN ',ActualMiles' ELSE '' END, 2, 100000), '')
		FROM (
			SELECT O.* 
				, InvoiceRatesAppliedDateUTC = OS.CreateDateUTC
				, OS.BatchID
				, Approved = cast(ISNULL(OA.Approved, 0) as bit)
				, FinalActualMiles = ISNULL(OA.OverrideActualMiles, O.ActualMiles)
				, FinalOriginMinutes = ISNULL(OA.OverrideOriginMinutes, O.OriginMinutes)
				, FinalDestMinutes = ISNULL(OA.OverrideDestMinutes, O.DestMinutes)
				, FinalH2S = cast(CASE WHEN isnull(OA.OverrideH2S, 0) = 1 THEN 0 ELSE O.H2S END as bit)
				, FinalChainup = cast(CASE WHEN isnull(OA.OverrideChainup, 0) = 1 THEN 0 ELSE O.Chainup END as bit)
				, FinalRerouteCount = CASE WHEN isnull(OA.OverrideReroute, 0) = 1 THEN 0 ELSE isnull(O.RerouteCount, 0) END
				, FC.OrderUnitsField
				, InvoiceBatchNum = OS.BatchNum 
				, InvoiceOriginWaitBillableMinutes = OS.OriginWaitBillableMinutes
				, InvoiceDestinationWaitBillableMinutes = OS.DestinationWaitBillableMinutes
				, InvoiceTotalWaitBillableMinutes = isnull(OS.OriginWaitBillableMinutes, 0) + ISNULL(OS.DestinationWaitBillableMinutes, 0) 
				, InvoiceWaitFeeParameterID = WaitFeeParameterID
				, InvoiceWaitFeeSubUnit = WaitFeeSubUnit
				, InvoiceWaitFeeRoundingType = WaitFeeRoundingType
				, InvoiceOriginWaitRate = OS.OriginWaitRate 
				, InvoiceOriginWaitAmount = OS.OriginWaitAmount 		
				, InvoiceDestinationWaitRate = OS.DestinationWaitRate 
				, InvoiceDestinationWaitAmount = OS.DestinationWaitAmount 
				, InvoiceTotalWaitAmount = OS.TotalWaitAmount
				, InvoiceOrderRejectRate = OS.OrderRejectRate	
				, InvoiceOrderRejectRateType = OS.OrderRejectRateType  
				, InvoiceOrderRejectAmount = OS.OrderRejectAmount  
				, InvoiceChainupRate = OS.ChainupRate
				, InvoiceChainupRateType = OS.ChainupRateType
				, InvoiceChainupAmount = OS.ChainupAmount 
				, InvoiceRerouteRate = OS.RerouteRate
				, InvoiceRerouteRateType = OS.RerouteRateType
				, InvoiceRerouteAmount = OS.RerouteAmount 
				, InvoiceH2SRate = OS.H2SRate
				, InvoiceH2SRateType = OS.H2SRateType
				, InvoiceH2SAmount = OS.H2SAmount
				, InvoiceSplitLoadRate = OS.SplitLoadRate
				, InvoiceSplitLoadRateType = OS.SplitLoadRateType
				, InvoiceSplitLoadAmount = OS.SplitLoadAmount
				, InvoiceOtherAmount = OS.OtherAmount
				, InvoiceTaxRate = OS.OriginTaxRate
				, InvoiceSettlementUom = OS.SettlementUom 
				, InvoiceSettlementUomShort = OS.SettlementUomShort 
				, InvoiceShipperSettlementFactorID = OS.ShipperSettlementFactorID 
				, InvoiceSettlementFactorID = OS.SettlementFactorID 
				, InvoiceSettlementFactor = OS.SettlementFactor 
				, InvoiceMinSettlementUnitsID = OS.MinSettlementUnitsID 
				, InvoiceMinSettlementUnits = OS.MinSettlementUnits
				, InvoiceUnits = OS.SettlementUnits
				, InvoiceRouteRate = OS.RouteRate
				, InvoiceRouteRateType = OS.RouteRateType
				, InvoiceRateSheetRate = OS.RateSheetRate
				, InvoiceRateSheetRateType = OS.RateSheetRateType
				, InvoiceFuelSurchargeRate = OS.FuelSurchargeRate
				, InvoiceFuelSurchargeAmount = OS.FuelSurchargeAmount
				, InvoiceLoadAmount = OS.LoadAmount
				, InvoiceTotalAmount = OS.TotalAmount
			FROM dbo.viewOrder O
			LEFT JOIN viewOrderSettlementShipper OS ON OS.OrderID = O.ID
			LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
			LEFT JOIN tblSettlementFactor FC ON FC.ID = OS.SettlementFactorID
			WHERE O.StatusID NOT IN (-10) -- don't include "Generated" orders (only Assigned+)
		) X
	) X1


GO




EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRecompileAllStoredProcedures
GO
EXEC _spRecompileAllStoredProcedures
GO
EXEC _spRebuildAllObjects
GO
EXEC _spRebuildAllObjects
GO


COMMIT
SET NOEXEC OFF