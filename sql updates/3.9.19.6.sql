-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.9.19.5.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.9.19.5.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0

SET NOEXEC OFF
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.9.19.5'
SELECT  @NewVersion = '3.9.19.6'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'Settlement - display the Order Approval overridden values on the Settlement pages'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/***********************************
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
-- Changes:
	- 3.7.31 - 2015/06/19 - KDA - reorder input parameters (move ProviderID down) and add @DriverGroupID parameter
	- 3.9.0  - 2/15/08/14 - KDA - return Approved column
	- 3.9.19.6 - 2015/09/30 - KDA - add OverrideXXX fields from OrderApproval table
***********************************/
ALTER PROCEDURE spRetrieveOrdersFinancialCarrier
(
  @StartDate datetime = NULL
, @EndDate datetime = NULL
, @ShipperID int = -1 -- all
, @CarrierID int = -1 -- all
, @ProductGroupID int = -1 -- all 
, @DriverGroupID int = -1 -- all
, @OriginStateID int = -1 -- all 
, @DestStateID int = -1 -- all
, @ProducerID int = -1 -- all
, @BatchID int = NULL  -- either show unbatched (NULL), or the specified batch orders
, @OnlyShipperSettled bit = 0 
) AS BEGIN

	SELECT @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	
	SELECT DISTINCT OE.* 
		, ShipperSettled = cast(CASE WHEN OSS.BatchID IS NOT NULL THEN 1 ELSE 0 END as bit)
		, InvoiceOtherDetailsTSV = dbo.fnOrderCarrierAssessorialDetailsTSV(OE.ID, 0)
		, InvoiceOtherAmountsTSV = dbo.fnOrderCarrierAssessorialAmountsTSV(OE.ID, 0)
		, Approved = cast(ISNULL(OA.Approved, 0) as bit)
		, FinalActualMiles = ISNULL(OA.OverrideActualMiles, OE.ActualMiles)
		, FinalOriginMinutes = ISNULL(OA.OverrideOriginMinutes, OE.OriginMinutes)
		, FinalDestMinutes = ISNULL(OA.OverrideDestMinutes, OE.DestMinutes)
		, FinalH2S = cast(CASE WHEN isnull(OA.OverrideH2S, 0) = 1 THEN 0 ELSE OE.H2S END as bit)
		, FinalChainup = cast(CASE WHEN isnull(OA.OverrideChainup, 0) = 1 THEN 0 ELSE OE.Chainup END as bit)
		, FinalRerouteCount = CASE WHEN isnull(OA.OverrideReroute, 0) = 1 THEN 0 ELSE OE.RerouteCount END
	FROM dbo.viewOrder_Financial_Carrier OE
	LEFT JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = OE.ID AND OSS.BatchID IS NOT NULL
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = OE.ID
	WHERE ID IN (
		SELECT O.ID
		FROM viewOrderBase O
		JOIN tblProduct P ON P.ID = O.ProductID
		JOIN tblOrigin OO ON OO.ID = O.OriginID
		JOIN tblDestination D ON D.ID = O.DestinationID
		LEFT JOIN tblOrderSettlementCarrier OSC ON OSC.OrderID = O.ID
		WHERE O.StatusID IN (4)  
		  AND (@ShipperID=-1 OR O.CustomerID=@ShipperID) 
		  AND (@CarrierID=-1 OR O.CarrierID=@CarrierID) 
		  AND (@ProductGroupID=-1 OR P.ProductGroupID=@ProductGroupID) 
		  AND (@DriverGroupID=-1 OR O.DriverGroupID=@DriverGroupID) 
		  AND (@OriginStateID=-1 OR OO.StateID=@OriginStateID) 
		  AND (@DestStateID=-1 OR D.StateID=@DestStateID) 
		  AND (@ProducerID=-1 OR O.ProducerID=@ProducerID) 
		  AND (@StartDate IS NULL OR O.OrderDate >= @StartDate) 
		  AND (@EndDate IS NULL OR O.OrderDate <= @EndDate)
		  AND ((@BatchID IS NULL AND OSC.BatchID IS NULL) OR OSC.BatchID = @BatchID)
	)
	  AND (@OnlyShipperSettled = 0 OR OSS.BatchID IS NOT NULL)
	ORDER BY OE.OriginDepartTimeUTC
END
GO
/***********************************
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
-- Changes
	- 3.8.11 - 2015/07/28 - KDA - remove OriginShipperRegion field
	- 3.9.0  - 2/15/08/14 - KDA - return Approved column
	- 3.9.19.6 - 2015/09/30 - KDA - add OverrideXXX fields from OrderApproval table
***********************************/
ALTER PROCEDURE spRetrieveOrdersFinancialShipper
(
  @StartDate datetime = NULL
, @EndDate datetime = NULL
, @ShipperID int = -1 -- all customers
, @ProductGroupID int = -1 -- all product groups
, @ProducerID int = -1 -- all
, @OriginStateID int = -1 -- all 
, @DestStateID int = -1 -- all
, @BatchID int = NULL  -- either show unbatched (NULL), or the specified batch orders
) AS BEGIN

	SELECT @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	
	SELECT DISTINCT OE.* 
		, InvoiceOtherDetailsTSV = dbo.fnOrderShipperAssessorialDetailsTSV(OE.ID, 0)
		, InvoiceOtherAmountsTSV = dbo.fnOrderShipperAssessorialAmountsTSV(OE.ID, 0)
		, Approved = cast(ISNULL(OA.Approved, 0) as bit)
		, FinalActualMiles = ISNULL(OA.OverrideActualMiles, OE.ActualMiles)
		, FinalOriginMinutes = ISNULL(OA.OverrideOriginMinutes, OE.OriginMinutes)
		, FinalDestMinutes = ISNULL(OA.OverrideDestMinutes, OE.DestMinutes)
		, FinalH2S = cast(CASE WHEN isnull(OA.OverrideH2S, 0) = 1 THEN 0 ELSE OE.H2S END as bit)
		, FinalChainup = cast(CASE WHEN isnull(OA.OverrideChainup, 0) = 1 THEN 0 ELSE OE.Chainup END as bit)
		, FinalRerouteCount = CASE WHEN isnull(OA.OverrideReroute, 0) = 1 THEN 0 ELSE OE.RerouteCount END
	FROM viewOrder_Financial_Shipper OE
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = OE.ID
	WHERE ID IN (
		SELECT O.ID
		FROM viewOrderBase O
		JOIN tblProduct P ON P.ID = O.ProductID
		JOIN tblOrigin OO ON OO.ID = O.OriginID
		JOIN tblDestination D ON D.ID = O.DestinationID
		LEFT JOIN tblOrderSettlementShipper ISC ON ISC.OrderID = O.ID
		WHERE O.StatusID IN (4)  
		  AND (@ShipperID=-1 OR O.CustomerID=@ShipperID) 
		  AND (@ProductGroupID=-1 OR P.ProductGroupID=@ProductGroupID) 
		  AND (@ProducerID=-1 OR O.ProducerID=@ProducerID) 
		  AND (@OriginStateID=-1 OR OO.StateID=@OriginStateID) 
		  AND (@DestStateID=-1 OR D.StateID=@DestStateID) 
		  AND (@StartDate IS NULL OR O.OrderDate >= @StartDate) 
		  AND (@EndDate IS NULL OR O.OrderDate <= @EndDate)
		  AND ((@BatchID IS NULL AND ISC.BatchID IS NULL) OR ISC.BatchID = @BatchID)
	)
	ORDER BY OE.OriginDepartTimeUTC
END 
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier AssessorialRate info for the specified order
-- Changes:
	- 3.7.28 - 2015/06/18 - KDA - add new columns: add DriverID/DriverGroupID parameters, move ProducerID parameter to end (so consistent with all other Best-Match rates/parameters
	- 3.7.30 - 2015/06/19 - KDA - remove DriverID
	- 3.9.19.6 - 2015/09/30 - KDA - honor Order Approval . Override values when computing Rates
***********************************/
ALTER FUNCTION fnOrderCarrierAssessorialRates(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, TypeID, Rate, RateTypeID, UomID
	FROM dbo.viewOrder O
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
	CROSS APPLY dbo.fnCarrierAssessorialRates(O.OrderDate, null, null, O.CustomerID, O.CarrierID, O.ProductGroupID, O.DriverGroupID, O.OriginID, O.DestinationID, O.OriginStateID, O.DestStateID, O.OriginRegionID, O.ProducerID, 1) R
	WHERE O.ID = @ID
	  AND ((R.TypeID = 1 AND CASE WHEN isnull(OA.OverrideChainup, 0) = 1 THEN 0 ELSE O.ChainUp END = 1)
		OR (R.TypeID = 2 AND CASE WHEN isnull(OA.OverrideReroute, 0) = 1 THEN 0 ELSE O.RerouteCount END > 0)
		OR (R.TypeID = 3 AND O.TicketCount > 1)
		OR (R.TypeID = 4 AND CASE WHEN isnull(OA.OverrideH2S, 0) = 1 THEN 0 ELSE O.H2S END = 1)
		OR R.TypeID > 4)
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper AssessorialRate info for the specified order
-- Changes:
	- 3.9.19.6 - 2015/09/30 - KDA - honor Order Approval . Override values when computing Rates
***********************************/
ALTER FUNCTION fnOrderShipperAssessorialRates(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, TypeID, Rate, RateTypeID, UomID
	FROM dbo.viewOrder O
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
	CROSS APPLY dbo.fnShipperAssessorialRates(O.OrderDate, null, null, O.CustomerID, O.ProductGroupID, O.OriginID, O.DestinationID, O.OriginStateID, O.DestStateID, O.OriginRegionID, O.ProducerID, 1) R
	WHERE O.ID = @ID
	  AND ((R.TypeID = 1 AND CASE WHEN isnull(OA.OverrideChainup, 0) = 1 THEN 0 ELSE O.ChainUp END = 1)
		OR (R.TypeID = 2 AND CASE WHEN isnull(OA.OverrideReroute, 0) = 1 THEN 0 ELSE O.RerouteCount END > 0)
		OR (R.TypeID = 3 AND O.TicketCount > 1)
		OR (R.TypeID = 4 AND CASE WHEN isnull(OA.OverrideH2S, 0) = 1 THEN 0 ELSE O.H2S END = 1)
		OR R.TypeID > 4)
GO

COMMIT
SET NOEXEC OFF