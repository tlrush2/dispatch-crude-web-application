-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.6.5'
SELECT  @NewVersion = '3.6.6'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'Order Rules: more granular Order Entry Rules (vs global Settings)'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

CREATE TABLE tblRuleType
(
  ID int NOT NULL CONSTRAINT PK_RuleType PRIMARY KEY CLUSTERED
, Name varchar(25) NOT NULL 
, UsesList bit NOT NULL CONSTRAINT DF_RuleType_UsesList DEFAULT(0)
)
GO
CREATE UNIQUE INDEX udxRuleType_Name ON tblRuleType(Name)
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblRuleType TO dispatchcrude_iis_acct
GO
INSERT INTO tblRuleType (ID, Name, UsesList)
	SELECT ID, Name, 0 FROM tblSettingType
	UNION SELECT 5, 'List', 1
GO

CREATE TABLE tblOrderRuleType
(
  ID int NOT NULL CONSTRAINT PK_OrderRuleType PRIMARY KEY CLUSTERED
, Name varchar(50) NOT NULL
, RuleTypeID int NOT NULL CONSTRAINT FK_OrderRuleType_RuleType FOREIGN KEY REFERENCES tblRuleType(ID)
)
GO
CREATE UNIQUE INDEX udxOrderRuleType_Name ON tblOrderRuleType(Name)
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblOrderRuleType TO dispatchcrude_iis_acct
GO
INSERT INTO tblOrderRuleType (ID, Name, RuleTypeID)
	SELECT 1, 'Print Destination @ Pickup', 2
GO

CREATE TABLE tblOrderRule
(
  ID int NOT NULL identity(1, 1) CONSTRAINT PK_OrderRule PRIMARY KEY NONCLUSTERED
, TypeID int NOT NULL CONSTRAINT FK_OrderRule_Type FOREIGN KEY REFERENCES tblOrderRuleType(ID)
, ShipperID int NULL CONSTRAINT FK_OrderRule_Shipper FOREIGN KEY REFERENCES tblCustomer(ID)
, CarrierID int NULL CONSTRAINT FK_OrderRule_Carrier FOREIGN KEY REFERENCES tblCarrier(ID)
, ProductGroupID int NULL CONSTRAINT FK_OrderRule_ProductGroup FOREIGN KEY REFERENCES tblProductGroup(ID)
, OriginID int NULL CONSTRAINT FK_OrderRule_Origin FOREIGN KEY REFERENCES tblOrigin(ID)
, DestinationID int NULL CONSTRAINT FK_OrderRule_Destination FOREIGN KEY REFERENCES tblDestination(ID)
, OriginStateID int NULL CONSTRAINT FK_OrderRule_OriginState FOREIGN KEY REFERENCES tblState(ID)
, DestStateID int NULL CONSTRAINT FK_OrderRule_DestState FOREIGN KEY REFERENCES tblState(ID)
, RegionID int NULL CONSTRAINT FK_OrderRule_Region FOREIGN KEY REFERENCES tblRegion(ID)
, ProducerID int NULL CONSTRAINT FK_OrderRule_Producer FOREIGN KEY REFERENCES tblProducer(ID)
, EffectiveDate date NOT NULL
, EndDate date NOT NULL
, Value varchar(255) NULL
, CreateDateUTC smalldatetime NOT NULL CONSTRAINT DF_OrderRule_CreateDateUTC DEFAULT (getutcdate())
, CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_OrderRule_CreatedByUser DEFAULT (suser_name())
, LastChangeDateUTC smalldatetime NULL
, LastChangedByUser varchar(100) NULL
) ON [PRIMARY]
GO
ALTER TABLE tblOrderRule WITH CHECK ADD CONSTRAINT CK_OrderRule_EndDate_Greater CHECK  (([EndDate]>=[EffectiveDate]))
GO
ALTER TABLE tblOrderRule CHECK CONSTRAINT [CK_OrderRule_EndDate_Greater]
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblOrderRule TO dispatchcrude_iis_acct
GO

CREATE UNIQUE CLUSTERED INDEX udxOrderRule_Main ON tblOrderRule
(
	TypeID ASC,
	ShipperID ASC,
	CarrierID ASC,
	ProductGroupID ASC,
	OriginID ASC,
	DestinationID ASC,
	OriginStateID ASC,
	DestStateID ASC,
	RegionID ASC,
	ProducerID ASC,
	EffectiveDate ASC
)
GO

INSERT INTO tblOrderRule (TypeID, EffectiveDate, EndDate, Value) values (1, '1/1/2015', '12/31/2099', 'True')
GO

/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all CarrierAssessorialRate records
******************************************************/
CREATE VIEW viewOrderRule AS
	SELECT X.*
		, RT.RuleTypeID
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblOrderRule XN 
			WHERE XN.TypeID = X.TypeID
			  AND dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EffectiveDate > X.EffectiveDate)
	FROM tblOrderRule X
	JOIN tblOrderRuleType RT ON RT.ID = X.TypeID
GO
GRANT SELECT ON viewOrderRule TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 28 Feb 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OrderRule info for the specified order
/***********************************/
CREATE FUNCTION fnOrderRules(@StartDate date, @EndDate date, @TypeID int, @ShipperID int, @CarrierID int, @ProductGroupID int, @OriginID int, @DestinationID int, @OriginStateID int, @DestStateID int, @RegionID int, @ProducerID int, @BestMatchOnly bit = 0)
RETURNS 
	@ret TABLE (
		ID int
	  , TypeID int
	  , ShipperID int
	  , CarrierID int
	  , ProductGroupID int
	  , OriginID int
	  , DestinationID int
	  , OriginStateID int
	  , DestStateID int
	  , RegionID int
	  , ProducerID int
	  , Value varchar(255)
	  , RuleTypeID int
	  , EffectiveDate date
	  , EndDate date
	  , NextEffectiveDate date
	  , BestMatch bit
	  , Ranking smallmoney
	  , CreateDateUTC datetime
	  , CreatedByUser varchar(100)
	  , LastChangeDateUTC datetime
	  , LastChangedByUser varchar(100)
	)
AS BEGIN
	-- get the raw data (all matched rows)
	DECLARE @src TABLE (ID int, TypeID int, Ranking smallmoney)
	INSERT INTO @src (ID, TypeID, Ranking)
		SELECT ID, TypeID
			, Ranking =	dbo.fnRateRanking(@ShipperID, R.ShipperID, 256, 0)
					  + dbo.fnRateRanking(@CarrierID, R.CarrierID, 128, 0)
					  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 64, 0)
					  + dbo.fnRateRanking(@OriginID, R.OriginID, 32, 0)
					  + dbo.fnRateRanking(@DestinationID, R.DestinationID, 16, 0)
					  + dbo.fnRateRanking(@OriginStateID, R.OriginStateID, 8, 0)
					  + dbo.fnRateRanking(@DestStateID, R.DestStateID, 4, 0)
					  + dbo.fnRateRanking(@RegionID, R.RegionID, 2, 0)
					  + dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM dbo.viewOrderRule R
		WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(R.ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@OriginID, 0), R.OriginID, 0) = coalesce(R.OriginID, nullif(@OriginID, 0), 0)
		  AND coalesce(nullif(@DestinationID, 0), R.DestinationID, 0) = coalesce(R.DestinationID, nullif(@DestinationID, 0), 0)
		  AND coalesce(nullif(@OriginStateID, 0), R.OriginStateID, 0) = coalesce(R.OriginStateID, nullif(@OriginStateID, 0), 0)
		  AND coalesce(nullif(@DestStateID, 0), R.DestStateID, 0) = coalesce(R.DestStateID, nullif(@DestStateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(R.RegionID, nullif(@RegionID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(R.ProducerID, nullif(@ProducerID, 0), 0)
		  AND (nullif(@TypeID, 0) IS NULL OR TypeID = @TypeID)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)

	-- return the rate data for the best-match rates 
	INSERT INTO @ret (ID, TypeID, ShipperID, CarrierID, ProductGroupID, OriginID, DestinationID, OriginStateID, DestStateID, RegionID, ProducerID, Value, RuleTypeID, EffectiveDate, EndDate, NextEffectiveDate, BestMatch, Ranking, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser)
		SELECT R.ID, TypeID, ShipperID, CarrierID, ProductGroupID, OriginID, DestinationID, OriginStateID, DestStateID, RegionID, ProducerID, Value, RuleTypeID, EffectiveDate, EndDate, NextEffectiveDate, BestMatch, Ranking, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser
		FROM viewOrderRule R
		JOIN (
			SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.TypeID IS NULL THEN 0 ELSE 1 END as bit)
			FROM @src S
			LEFT JOIN (
				SELECT TypeID, Ranking = MAX(Ranking)
				FROM @src
				WHERE @StartDate = ISNULL(@EndDate, @StartDate)
				  AND Ranking % 1 = 0.01 * 9  -- ensure some type of match occurred on all 10 criteria choices
				GROUP BY TypeID
			) X ON X.TypeID = S.TypeID AND X.Ranking = S.Ranking
		) X ON X.ID = R.ID
		WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
		
	RETURN
END
GO
GRANT SELECT ON fnOrderRules TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 28 Feb 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Order Rule rows for the specified criteria
/***********************************/
CREATE FUNCTION fnOrderRulesDisplay(@StartDate date, @EndDate date, @TypeID int, @ShipperID int, @CarrierID int, @ProductGroupID int, @OriginID int, @DestinationID int, @OriginStateID int, @DestStateID int, @RegionID int, @ProducerID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.OriginID, R.DestinationID, R.OriginStateID, R.DestStateID, R.RegionID, R.ProducerID, R.TypeID
		, R.Value, R.RuleTypeID, R.EffectiveDate, R.EndDate, R.NextEffectiveDate
		, Type = ORT.Name
		, Shipper = S.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, Origin = O.Name
		, OriginFull = O.FullName
		, Destination = D.Name
		, DestinationFull = D.FullName
		, OriginState = OS.FullName
		, OriginStateAbbrev = OS.Abbreviation
		, DestState = DS.FullName
		, DestStateAbbrev = DS.Abbreviation
		, Region = RE.Name
		, RuleType = RT.Name
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch, Ranking
	FROM dbo.fnOrderRules(@StartDate, @EndDate, @TypeID, @ShipperID, @CarrierID, @ProductGroupID, @OriginID, @DestinationID, @OriginStateID, @DestStateID, @RegionID, @ProducerID, 0) R
	JOIN tblOrderRuleType ORT ON ORT.ID = R.TypeID
	LEFT JOIN tblCustomer S ON S.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN viewOrigin O ON O.ID = R.OriginID
	LEFT JOIN viewDestination D ON D.ID = R.DestinationID
	LEFT JOIN tblState OS ON OS.ID = R.OriginStateID
	LEFT JOIN tblState DS ON DS.ID = R.DestStateID
	LEFT JOIN tblRegion RE ON RE.ID = R.RegionID
	LEFT JOIN tblRuleType RT ON RT.ID = R.RuleTypeID
	ORDER BY EffectiveDate
GO
GRANT SELECT ON fnOrderRulesDisplay TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier AssessorialRate info for the specified order
/***********************************/
CREATE FUNCTION fnOrderOrderRules(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, TypeID, Value, RuleTypeID
	FROM dbo.viewOrder O
	CROSS APPLY dbo.fnOrderRules(isnull(O.OrderDate, O.DueDate), null, null, O.CustomerID, O.CarrierID, O.ProductGroupID, O.OriginID, O.DestinationID, O.OriginStateID, O.DestStateID, O.OriginRegionID, O.ProducerID, 1) R
	WHERE O.ID = @ID
GO
GRANT SELECT ON fnOrderOrderRules TO dispatchcrude_iis_acct
GO

/*******************************************/
-- Date Created: 1 Mar 2015
-- Author: Kevin Alons
-- Purpose: return Order Rules for every changed Order
/*******************************************/
CREATE FUNCTION fnOrderRules_DriverApp( @DriverID int, @LastChangeDateUTC datetime ) 
RETURNS 
	@ret TABLE (
		ID int
	  , OrderID int
	  , TypeID int
	  , Value varchar(255)
	)
AS BEGIN
	DECLARE @IDS TABLE (ID int)
	INSERT INTO @IDS
		SELECT O.ID
		FROM dbo.viewOrder O
		JOIN dbo.viewOrigin OO ON OO.ID = O.OriginID
		JOIN dbo.viewDestination D ON D.ID = O.DestinationID
		JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
		JOIN dbo.tblRoute R ON R.ID = O.RouteID
		JOIN dbo.tblCarrier CA ON CA.ID = O.CarrierID
		CROSS JOIN (SELECT DATEADD(second, -5, @LastChangeDateUTC) AS LCD) LCD
		WHERE O.ID IN (
			SELECT ID FROM tblOrder WHERE DriverID = @driverID AND StatusID IN (2,7,8,3) 
		)
		  AND (
			@LastChangeDateUTC IS NULL 
			OR O.CreateDateUTC >= LCD.LCD
			OR O.LastChangeDateUTC >= LCD.LCD
			OR O.DeleteDateUTC >= LCD.LCD
			OR C.LastChangeDateUTC >= LCD.LCD
			OR CA.LastChangeDateUTC >= LCD.LCD
			OR OO.LastChangeDateUTC >= LCD.LCD
			OR R.LastChangeDateUTC >= LCD.LCD)
	
	DECLARE @ID int
	WHILE EXISTS (SELECT * FROM @IDS)
	BEGIN
		SELECT TOP 1 @ID = ID FROM @IDS
		INSERT INTO @ret (ID, OrderID, TypeID, Value)
			SELECT ID, @ID, TypeID, Value FROM dbo.fnOrderOrderRules(@ID)
		DELETE FROM @IDS WHERE ID = @ID
		-- if no records exist, then return a NULL record to tell the Mobile App to delete any existing records for this order
		IF NOT EXISTS (SELECT * FROM @ret WHERE OrderID = @ID)
			INSERT INTO @ret (OrderID) VALUES (@ID)
	END
	RETURN
END
GO
GRANT SELECT ON fnOrderRules_DriverApp to dispatchcrude_iis_acct
GO

COMMIT
SET NOEXEC OFF