SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.11.1'
SELECT  @NewVersion = '3.11.2'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-967: implement DriverApp.Sync updates leveraging new OrdersPresent data from DriverApp to ensure all orders are properly synced'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/************************************
 Creation Info: 2016/02/08 - 3.11.2
 Author: Kevin Alons
 Purpose: table definition used by Driver App Sync operation
************************************/
CREATE TYPE OrdersPresent AS TABLE(     /* This must match its model in OrderPresent/List */
  ID int NOT NULL PRIMARY KEY CLUSTERED /* OrderID for orders present on app */
, LastChangeDateUTC datetime NULL       /* Orders (Order Read-Only) timestamp */
, OELastChangeDateUTC datetime NULL     /* OrderEdits timestamp */
)
GO

/*******************************************
 Date Created: 31 Aug 2013
 Author: Kevin Alons
 Purpose: return driver editable Order data for Driver App sync
 Changes: 
 - 3.8.9	- 07/24/15	 - GSM	- Added TickeTypeID field (was read-only before)
 - 3.7.4	- 05/08/15	 - GSM	- Added Rack/Bay field
 - 3.7.11	- 5/18/2015	 - KDA	- renamed RackBay to DestRackBay
 - 3.9.19.3 - 2015/09/29 - GSM	- remove transaction on Order.Accept|Pickup|Deliver LastChangeDateUTC fields (no longer use max of O.LastChangeDateUTC)
 - 3.9.38	- 2016/01/03 - KDA	- add OriginWeightNetUnits
 - 3.9.38	- 2016/01/11 - JAE	- added destination weight fields
 - 3.10.2.1	- 2015/01/28 - JAE	- Added clause to always send dispatched orders
 - 3.10.5	- 2015/01/30 - KDA	- remove workaround clause to always send DISPATCHED orders
 - 3.11.2    - 2015/02/20 - KDA+ - add @OrdersPresent table parameter, and use in logic
								- eliminate @LastChangeDateUTC parameter
								- no longer return DELETED records, only OrderReadonly records are turned when deleted (which will cause all related Driver App records to purge)
*******************************************/
ALTER FUNCTION fnOrderEdit_DriverApp(@DriverID int, @OrdersPresent OrdersPresent READONLY) RETURNS TABLE AS
RETURN 
      SELECT O.ID
            , O.StatusID
            , O.TruckID
            , O.TrailerID
            , O.Trailer2ID
            , O.OriginBOLNum
            , O.OriginArriveTimeUTC
            , O.OriginDepartTimeUTC
            , O.OriginMinutes
            , O.OriginWaitReasonID
            , O.OriginWaitNotes
            , O.OriginTruckMileage
            , O.OriginGrossUnits
            , O.OriginGrossStdUnits
            , O.OriginNetUnits
            , O.OriginWeightNetUnits
            , O.DestWeightGrossUnits
            , O.DestWeightTareUnits
            , O.DestWeightNetUnits
            , O.ChainUp
            , O.Rejected
            , O.RejectReasonID
            , O.RejectNotes
            , O.OriginTankNum
            , O.DestArriveTimeUTC
            , O.DestDepartTimeUTC
            , O.DestMinutes
            , O.DestWaitReasonID
            , O.DestWaitNotes
            , O.DestBOLNum
            , O.DestTruckMileage
            , O.DestGrossUnits
            , O.DestNetUnits
            , O.DestProductTemp
            , O.DestProductBSW
            , O.DestProductGravity
            , O.DestOpenMeterUnits
            , O.DestCloseMeterUnits
            , O.CarrierTicketNum
            , O.AcceptLastChangeDateUTC 
            , O.PickupLastChangeDateUTC 
            , O.DeliverLastChangeDateUTC
            , O.PickupPrintStatusID
            , O.DeliverPrintStatusID
            , O.PickupPrintDateUTC
            , O.DeliverPrintDateUTC
            , O.PickupDriverNotes
            , O.DeliverDriverNotes
            , O.DestRackBay
            , O.TicketTypeID
            , OELC = O.LastChangeDateUTC /* "OELC" = "OrderEditLastChangeDateUTC" is named to match the OrderEdit model */
      FROM dbo.tblOrder O
	  LEFT JOIN @OrdersPresent OP ON OP.ID = O.ID
	  LEFT JOIN tblOrderAppChanges OAC ON OAC.OrderID = O.ID
      WHERE DriverID = @driverID AND O.StatusID IN (2,7,8,3) AND O.DeleteDateUTC IS NULL
		/* 3.11.2 - change to ensure that any order missing or out-of-date on the tablet is sync'ed */
        AND (OP.ID IS NULL OR cast(O.LastChangeDateUTC as smalldatetime) > cast(isnull(OP.OELastChangeDateUTC, '1/1/1900') as smalldatetime))
GO

/*******************************************
 Date Created: 2015/02/08
 Author: Kevin Alons
 Purpose: return the results of fnOrderEdit_DriverApp (taking the new @OrdersPresent TABLE parameter)
 Changes:
*******************************************/
CREATE PROCEDURE spOrderEdit_DriverApp(@DriverID int, @OrdersPresent OrdersPresent READONLY) AS
BEGIN
	SELECT * FROM fnOrderEdit_DriverApp(@DriverID, @OrdersPresent)
END
GO
GRANT EXECUTE ON spOrderEdit_DriverApp TO role_iis_acct
GO

EXEC _spDropView 'viewOrderReadOnlyAllEligible_DriverApp '
GO
/****************************************************
 Date Created: 2016/02/24
 Author: Kevin Alons
 Purpose: return all currently eligible DriverApp OrderReadOnly records (for all Drivers) 
****************************************************/
CREATE VIEW viewOrderReadOnlyAllEligible_DriverApp AS
SELECT O.ID
	, O.OrderNum
	, O.StatusID
	, O.TicketTypeID
	, PriorityNum = CAST(P.PriorityNum AS INT) 
	, Product = PRO.Name
	, O.DueDate
	, Origin = OO.Name
	, OriginFull = OO.FullName
	, OO.OriginType
	, O.OriginUomID
	, OriginStation = OO.Station 
	, OriginLeaseNum = OO.LeaseNum 
	, OriginCounty = OO.County 
	, OriginLegalDescription = OO.LegalDescription 
	, OriginNDIC = OO.NDICFileNum
	, OriginNDM = OO.NDM
	, OriginCA = OO.CA
	, OriginState = OO.State
	, OriginAPI = OO.WellAPI 
	, OriginLat = OO.LAT 
	, OriginLon = OO.LON 
	, OriginGeoFenceRadiusMeters = OO.GeoFenceRadiusMeters 
	, Destination = D.Name
	, DestinationFull = D.FullName
	, DestType = D.DestinationType 
	, O.DestUomID
	, DestLat = D.LAT 
	, DestLon = D.LON 
	, DestGeoFenceRadiusMeters = D.GeoFenceRadiusMeters 
	, DestinationStation = D.Station 
	, O.CreateDateUTC
	, O.CreatedByUser
	, O.DeleteDateUTC 
	, O.DeletedByUser 
	, O.OriginID
	, O.DestinationID
	, PriorityID = CAST(O.PriorityID AS INT) 
	, Operator = OO.Operator
	, O.OperatorID
	, Pumper = OO.Pumper
	, O.PumperID
	, Producer = OO.Producer
	, O.ProducerID
	, Customer = C.Name
	, O.CustomerID
	, Carrier = CA.Name
	, O.CarrierID
	, O.ProductID
	, TicketType = OO.TicketType
	, EmergencyInfo = ISNULL(C.EmergencyInfo, 'For an emergency (spill, leak, fire, or accident) contact CHEMTREC @ 800.424.9300 (toll free)') 
	, DestTicketTypeID = D.TicketTypeID
	, DestTicketType = D.TicketType
	, O.OriginTankNum
	, O.OriginTankID
	, O.DispatchNotes
	, O.DispatchConfirmNum
	, RouteActualMiles = ISNULL(R.ActualMiles, 0)
	, CarrierAuthority = CA.Authority 
	, OriginTimeZone = OO.TimeZone
	, DestTimeZone = D.TimeZone
	, OCTM.OriginThresholdMinutes
	, OCTM.DestThresholdMinutes
	, ShipperHelpDeskPhone = C.HelpDeskPhone
	, OriginDrivingDirections = OO.DrivingDirections
	, DestDrivingDirections = D.DrivingDirections
	, O.AcceptLastChangeDateUTC
	, O.PickupLastChangeDateUTC
	, O.DeliverLastChangeDateUTC
    , CustomerLastChangeDateUTC = C.LastChangeDateUTC 
    , CarrierLastChangeDateUTC = CA.LastChangeDateUTC 
    , OriginLastChangeDateUTC = OO.LastChangeDateUTC 
    , DestLastChangeDateUTC = D.LastChangeDateUTC
    , RouteLastChangeDateUTC = R.LastChangeDateUTC
    , DriverID = O.DriverID
    , OriginDriver = OD.FullName
    , OriginTruckID = ISNULL(OTR.OriginTruckID, O.TruckID)
    , DestDriver = DD.FullName
FROM dbo.tblOrder O
JOIN dbo.tblPriority P ON P.ID = O.PriorityID
JOIN dbo.viewOrigin OO ON OO.ID = O.OriginID
JOIN dbo.viewDestination D ON D.ID = O.DestinationID
JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
JOIN dbo.tblRoute R ON R.ID = O.RouteID
JOIN dbo.tblCarrier CA ON CA.ID = O.CarrierID
JOIN dbo.tblProduct PRO ON PRO.ID = O.ProductID		
LEFT JOIN dbo.tblOrderTransfer OTR ON OTR.OrderID = O.ID
LEFT JOIN dbo.viewDriver OD ON OD.ID = ISNULL(OTR.OriginDriverID, O.DriverID)
LEFT JOIN dbo.viewDriver DD ON DD.ID = O.DriverID
OUTER APPLY dbo.fnOrderBestMatchDriverAppPrintHeaderImage(O.ID) DAHI
OUTER APPLY dbo.fnOrderCombinedThresholdMinutes(O.ID) OCTM
WHERE StatusID IN (2,7,8,3) AND O.DeleteDateUTC IS NULL
GO
GRANT SELECT ON viewOrderReadOnlyAllEligible_DriverApp TO role_iis_acct
GO

EXEC _spDropFunction 'fnOrderReadOnly_DriverApp'
GO

/*******************************************
 Date Created: 2013/04/25
 Author: Kevin Alons
 Purpose: return readonly Order data for Driver App sync (and log the Print XXX records so we can faithfully re-sync them when a change occurrs)
 Changes:
 - 3.8.1	- 2015/07/02 - KDA	- ADDED
 - 3.9.19	- 2015/09/23 - KDA	- remove references to tblDriverAppPrintTicketTemplateSync
 - 3.10.5	- 2016/01/29 - KDA	- add logic to purge all ImageSync tables on a FULL sync operation (before data retrieval)
 - 3.11.1	- 2016/02/15 - KDA	- support for Footer ZPL template
 - 3.11.2   - 2015/02/08 - KDA	- add @OrdersPresent table parameter, and use in logic
								- rename @LastSyncDateUTC to @LastSyncDateUTC
								- eliminate use of fnOrderReadOnly_DriverApp()
*******************************************/
ALTER PROCEDURE spOrderReadOnly_DriverApp(@DriverID int, @LastSyncDateUTC datetime = NULL, @OrdersPresent OrdersPresent READONLY) AS
BEGIN
	BEGIN TRY	
		BEGIN TRAN
		-- purge all of these records on a FULL sync
		IF (@LastSyncDateUTC IS NULL)
		BEGIN
			DELETE FROM tblDriverAppPrintHeaderImageSync WHERE DriverID = @DriverID 
			DELETE FROM tblDriverAppPrintPickupTemplateSync WHERE DriverID = @DriverID 
			DELETE FROM tblDriverAppPrintDeliverTemplateSync WHERE DriverID = @DriverID 
			DELETE FROM tblDriverAppPrintFooterTemplateSync WHERE DriverID = @DriverID 
		END

		-- retrieve the data eligible records to be returned
		SELECT * INTO #all FROM viewOrderReadOnlyAllEligible_DriverApp WHERE DriverID = @DriverID

		-- load only include the records that are missing or out-of-date on the Tablet (based on @OrdersPresent)
		-- also include the print template data (and include records where the print template data is changed/newer)
		SELECT O.*
			, LastChangeDateUTC = OAC.ReadOnlyChangeDateUTC
			, HeaderImageID = DAHI.ID
			, PrintHeaderBlob = DAHI.ImageBlob
			, HeaderImageLeft = DAHI.ImageLeft
			, HeaderImageTop = DAHI.ImageTop
			, HeaderImageWidth = DAHI.ImageWidth
			, HeaderImageHeight = DAHI.ImageHeight
			, PickupTemplateID = DAPT.ID
			, PickupTemplateText = DAPT.TemplateText
			, DeliverTemplateID = DADT.ID
			, DeliverTemplateText = DADT.TemplateText
			, FooterTemplateID = DAFT.ID
			, FooterTemplateText = DAFT.TemplateText
		INTO #ret
		FROM #all O
		CROSS JOIN fnSyncLCDOffset(@LastSyncDateUTC) LCD
		LEFT JOIN @OrdersPresent OP ON OP.ID = O.ID
		LEFT JOIN tblOrderAppChanges OAC ON OAC.OrderID = O.ID
		OUTER APPLY dbo.fnOrderBestMatchDriverAppPrintHeaderImage(O.ID) DAHI
		LEFT JOIN tblDriverAppPrintHeaderImageSync DAHIS ON DAHIS.OrderID = O.ID AND DAHIS.DriverID = O.DriverID AND DAHIS.RecordID <> DAHI.ID
		OUTER APPLY dbo.fnOrderBestMatchDriverAppPrintPickupTemplate(O.ID) DAPT
		LEFT JOIN tblDriverAppPrintPickupTemplateSync DAPTS ON DAPTS.OrderID = O.ID AND DAPTS.DriverID = O.DriverID AND DAPTS.RecordID <> DAPT.ID
		OUTER APPLY dbo.fnOrderBestMatchDriverAppPrintDeliverTemplate(O.ID) DADT
		LEFT JOIN tblDriverAppPrintDeliverTemplateSync DADTS ON DADTS.OrderID = O.ID AND DADTS.DriverID = O.DriverID AND DADTS.RecordID <> DADT.ID
		OUTER APPLY dbo.fnOrderBestMatchDriverAppPrintFooterTemplate(O.ID) DAFT
		LEFT JOIN tblDriverAppPrintFooterTemplateSync DAFTS ON DADTS.OrderID = O.ID AND DADTS.DriverID = O.DriverID AND DADTS.RecordID <> DADT.ID
		WHERE 
			-- 3.11.2 - change to ensure that any order missing or out-of-date on the tablet is sync'ed 
			OP.ID IS NULL OR cast(OAC.ReadOnlyChangeDateUTC as smalldatetime) > cast(isnull(OP.LastChangeDateUTC, '1/1/1900') as smalldatetime)
			-- if any related (included) object was updated, then we need to resend any associated OrderReadOnly record
			OR CustomerLastChangeDateUTC >= LCD
			OR CarrierLastChangeDateUTC >= LCD
			OR OriginLastChangeDateUTC >= LCD
			OR DestLastChangeDateUTC >= LCD
			OR RouteLastChangeDateUTC >= LCD
			OR OAC.ReadOnlyChangeDateUTC >= LCD
			-- if any print related record was changed or a different template/image is now valid we need to resend any associated OrderReadOnly record
			OR DAHI.LastChangeDateUTC >= LCD
			OR DAHIS.RecordID IS NOT NULL
			OR DAPT.LastChangeDateUTC >= LCD
			OR DAPTS.RecordID IS NOT NULL
			OR DADT.LastChangeDateUTC >= LCD
			OR DADTS.RecordID IS NOT NULL
			OR DAFT.LastChangeDateUTC >= LCD
			OR DAFTS.RecordID IS NOT NULL

		-- forget the last sync records for this driver (if we didn't do it earlier before retrieving records)
		IF (@LastSyncDateUTC IS NOT NULL)
		BEGIN
			DELETE FROM tblDriverAppPrintHeaderImageSync
			WHERE DriverID = @DriverID 
			  AND OrderID IN (SELECT DISTINCT ID FROM #ret)
			DELETE FROM tblDriverAppPrintPickupTemplateSync
			WHERE DriverID = @DriverID 
			  AND OrderID IN (SELECT DISTINCT ID FROM #ret)
			DELETE FROM tblDriverAppPrintDeliverTemplateSync
			WHERE DriverID = @DriverID 
			  AND OrderID IN (SELECT DISTINCT ID FROM #ret)
			DELETE FROM tblDriverAppPrintFooterTemplateSync
			WHERE DriverID = @DriverID 
			  AND OrderID IN (SELECT DISTINCT ID FROM #ret)
		END

		-- re-cache the last sync records for the sync orders
		INSERT INTO tblDriverAppPrintHeaderImageSync (OrderID, DriverID, RecordID)
			SELECT DISTINCT ID, @DriverID, HeaderImageID FROM #ret WHERE HeaderImageID IS NOT NULL
		INSERT INTO tblDriverAppPrintPickupTemplateSync (OrderID, DriverID, RecordID)
			SELECT DISTINCT ID, @DriverID, PickupTemplateID FROM #ret WHERE PickupTemplateID IS NOT NULL
		INSERT INTO tblDriverAppPrintDeliverTemplateSync (OrderID, DriverID, RecordID)
			SELECT DISTINCT ID, @DriverID, DeliverTemplateID FROM #ret WHERE DeliverTemplateID IS NOT NULL
		INSERT INTO tblDriverAppPrintFooterTemplateSync (OrderID, DriverID, RecordID)
			SELECT DISTINCT ID, @DriverID, FooterTemplateID FROM #ret WHERE FooterTemplateID IS NOT NULL
		
		COMMIT TRAN

		-- PUSH back to the DriverApp empty Deleted Records (which will cause the Driver App to purge the order and related records)
		DECLARE @deleteDateUTC datetime; SET @deleteDateUTC = getutcdate()
		INSERT INTO #ret (ID, DeleteDateUTC, DeletedByUser, StatusID, PriorityID, DueDate, TicketTypeID, OriginUomID, DestUomID
				, Product, Origin, OriginFull, Destination, DestinationFull, OriginType, DestType
		        , Customer, Carrier, ProductID, EmergencyInfo, DestTicketTypeID, DestTicketType, RouteActualMiles)
            SELECT ID, @deleteDateUTC, 'Sync', 0, 0, getdate(), 0, 0, 0, '', '', '', '', '', '', '', '', '', 0, '', 0, '', 0		
			FROM (
				SELECT ID
				FROM @OrdersPresent
				EXCEPT SELECT ID  FROM #all
			) X

		-- return the resultant data to the Driver App
		SELECT * FROM #ret
	END TRY
	BEGIN CATCH
		DECLARE @msg varchar(255), @severity int
		SELECT @msg = left(ERROR_MESSAGE(), 255), @severity = ERROR_SEVERITY()
		IF @@TRANCOUNT > 0
			ROLLBACK TRAN 
		RAISERROR(@msg, @severity, 1)
	END CATCH
END
GO

/*******************************************
 Date Created: 25 Apr 2013
 Author: Kevin Alons
 Purpose: return OrderTicket data for Driver App sync
 Changes:
 - 3.9.30 - 2015/12/03 - KDA	- fix to ensure transferred orders are returned to the driver
 - 3.9.38 - 2015/12/16 - BB		- Add WeightTareUnits column and weight gross/net columns (DCWEB-972)
 - 3.11.2  - 2015/02/08 - KDA	- add @OrdersPresent table parameter, and use in logic
								- rename @LastChangeDateUTC to @LastSyncDateUTC
								- stop syncing any tickets for deleted orders (they will be removed when the order is deleted on the driver app)
*******************************************/
ALTER FUNCTION fnOrderTicket_DriverApp(@DriverID int, @LastSyncDateUTC datetime, @OrdersPresent OrdersPresent READONLY) RETURNS TABLE AS
RETURN 
	SELECT OT.ID
		, OT.UID
		, OT.OrderID
		, OT.CarrierTicketNum
		, OT.OriginTankID
		, OT.TankNum
		, OT.TicketTypeID
		, OT.ProductObsGravity
		, OT.ProductObsTemp
		, OT.ProductHighTemp
		, OT.ProductLowTemp
		, OT.ProductBSW
		, OpeningGaugeFeet = cast(OT.OpeningGaugeFeet as tinyint) 
		, OpeningGaugeInch = cast(OT.OpeningGaugeInch as tinyint)  
		, OpeningGaugeQ = cast(OT.OpeningGaugeQ as tinyint) 
		, ClosingGaugeFeet = cast(OT.ClosingGaugeFeet as tinyint) 
		, ClosingGaugeInch = cast(OT.ClosingGaugeInch as tinyint) 
		, ClosingGaugeQ = cast(OT.ClosingGaugeQ as tinyint) 
		, OT.BottomFeet
		, OT.BottomInches
		, OT.BottomQ
		, OT.GrossUnits
		, OT.GrossStdUnits
		, OT.NetUnits
		, OT.WeightGrossUnits  -- 3.9.38
		, OT.WeightNetUnits  -- 3.9.38
		, OT.WeightTareUnits  -- 3.9.38
		, OT.Rejected
		, OT.RejectReasonID
		, OT.RejectNotes
		, OT.SealOff
		, OT.SealOn
		, OT.BOLNum
		, OT.CreateDateUTC
		, OT.CreatedByUser
		, OT.LastChangeDateUTC
		, OT.LastChangedByUser
		, OT.DeleteDateUTC
		, OT.DeletedByUser
		, OT.MeterFactor
		, OT.OpenMeterUnits
		, OT.CloseMeterUnits
		, OT.DispatchConfirmNum
	FROM dbo.tblOrderTicket OT
	JOIN dbo.tblOrder O ON O.ID = OT.OrderID
	LEFT JOIN @OrdersPresent OP ON OP.ID = O.ID
	LEFT JOIN tblOrderTransfer OTR ON OTR.OrderID = O.ID
	CROSS JOIN fnSyncLCDOffset(@LastSyncDateUTC) LCD
	WHERE O.StatusID IN (2, 7, 8, 3) -- Dispatched, Accepted, Picked Up, Delivered
	  AND (O.DriverID = @DriverID OR OTR.OriginDriverID = @DriverID)
	  AND O.DeleteDateUTC IS NULL -- don't send tickets for deleted orders
	  AND (
	    -- if any date field on a ticket is later than LCD PUSH it back to Driver App (if deleted with an order Delete, will be purged on Driver App by OrderReadOnly record)
		   OT.CreateDateUTC >= LCD
	    OR OT.LastChangeDateUTC >= LCD
		OR OT.DeleteDateUTC >= LCD
		-- if an order was missing, then also include any of its existing tickets (TODO: switch to use @TicketsPresent in the future)
		OR OP.ID IS NULL
	  )
GO

/*******************************************
 Date Created: 2015/02/08
 Author: Kevin Alons
 Purpose: return the results of fnOrderTicket_DriverApp (taking the new @OrdersPresent TABLE parameter)
 Changes:
*******************************************/
CREATE PROCEDURE spOrderTicket_DriverApp(@DriverID int, @LastSyncDateUTC datetime = NULL, @OrdersPresent OrdersPresent READONLY) AS
BEGIN
	SELECT * FROM fnOrderTicket_DriverApp(@DriverID, @LastSyncDateUTC, @OrdersPresent)
END
GO
GRANT EXECUTE ON spOrderTicket_DriverApp TO role_iis_acct
GO

ALTER TABLE tblOrderRuleType ADD ForDriverApp bit CONSTRAINT DF_OrderRuleType_ForDriverApp DEFAULT (1)
GO
UPDATE tblOrderRuleType SET ForDriverApp = 1 WHERE ID IN (1, 3, 4, 5, 6, 7, 9, 10, 11, 12)
GO

/*******************************************
 Date Created: 1 Mar 2015
 Author: Kevin Alons
 Purpose: return Order Rules for every changed Order
 - 3.10.5	- 2016/01/29	- KDA	- use tblOrderAppChanges to only include records when Order.OriginID not any Order change
 - 3.10.9.1 - 2016/02/18	- KDA	- WORKAROUND: eliminate any filtering - temporarily ALWAYS PUSH all OrderRules to Driver App on every sync
 - 3.11.2	- 2016/02/20	- KDA	- don't send OrderRules for Deleted orders
									- eliminate @LastChangeDateUTC parameter
*******************************************/
ALTER FUNCTION fnOrderRules_DriverApp(@DriverID int) 
RETURNS 
	@ret TABLE (
		ID int
	  , OrderID int
	  , TypeID int
	  , Value varchar(255)
	)
AS BEGIN
	-- retrieve the eligible Orders for this Driver
	DECLARE @IDS TABLE (ID int)
	INSERT INTO @IDS
		SELECT ID FROM tblOrder WHERE DriverID = @driverID AND StatusID IN (2,7,8,3) AND DeleteDateUTC IS NULL

	-- retrieve the OrderRules for each eligible Order (could use CROSS APPLY but FAR less efficient)
	DECLARE @ID int
	WHILE EXISTS (SELECT * FROM @IDS)
	BEGIN
		SELECT TOP 1 @ID = ID FROM @IDS
		INSERT INTO @ret (ID, OrderID, TypeID, Value)
			SELECT OOR.ID, @ID, TypeID, Value 
			FROM dbo.fnOrderOrderRules(@ID) OOR
			-- only send those rules that apply to the Driver App
			JOIN tblOrderRuleType ORT ON ORT.ID = OOR.TypeID
			WHERE ORT.ForDriverApp = 1
		DELETE FROM @IDS WHERE ID = @ID

		-- if no records exist, then return a NULL record to tell the Mobile App to delete any existing records for this order
		IF NOT EXISTS (SELECT * FROM @ret WHERE OrderID = @ID)
			INSERT INTO @ret (OrderID) VALUES (@ID)
	END
	RETURN
END
GO

/*******************************************
Date Created: 2015/09/24
Author: Kevin Alons
Purpose: return OrderTransfer data for Driver App sync
Changes:
- 3.11.2	- 2016/02/20	- KDA	- remove reference to Virtual Deletes (we don't need to send Transfers for deleted orders, they are purged by the OrderReadOnly deletion)
									- rename @LastChangeDateUTC to @LastSyncDateUTC
*******************************************/
ALTER FUNCTION fnOrderTransfer_DriverApp(@DriverID INT, @LastSyncDateUTC DATETIME) RETURNS TABLE AS
RETURN 
	SELECT OTR.OrderID
        , OTR.DestTruckStartMileage
        , OTR.TransferComplete
        , OTR.Notes
		, OTR.CreateDateUTC
		, OTR.CreatedByUser
		, OTR.LastChangeDateUTC
		, OTR.LastChangedByUser
	FROM dbo.tblOrderTransfer OTR
	JOIN dbo.tblOrder O ON O.ID = OTR.OrderID
	CROSS JOIN fnSyncLCDOffset(@LastSyncDateUTC) LCD
	WHERE O.StatusID IN (2, 7, 8, 3) -- Dispatched, Accepted, Picked Up, Delivered
	  AND O.DeleteDateUTC IS NULL -- don't send OrderTransfers for deleted orders
	  AND O.DriverID = @DriverID
	  AND (
		   @LastSyncDateUTC IS NULL 
		OR OTR.CreateDateUTC >= LCD
		OR OTR.LastChangeDateUTC >= LCD)
GO

/*******************************************
 Date Created: 5 Apr 2014
 Author: Kevin Alons
 Purpose: return OriginTank data for Driver App sync
 Changes:
  - 3.10.5		- 2016/01/29	- KDA	- use tblOrderAppChanges to only include records when Order.OriginID not any Order change
  - 3.10.10.2	- 2016/02/24	- KDA	- remove VirtualDelete logic and ensure when an order.LastChangeDateUTC changes (not just CreateDateUTC)
  - 3.11.2		- 2016/02/20	- KDA	- rename @LastChangeDateUTC to @LastSyncDateUTC
										- remove use of Virtual Deletes - we can't delete from here because they might be needed for another order
*******************************************/
ALTER FUNCTION fnOriginTank_DriverApp(@DriverID int, @LastSyncDateUTC datetime) RETURNS TABLE AS
RETURN 
	SELECT DISTINCT OT.ID
		, OT.OriginID
		, OT.TankNum
		, OT.TankDescription
		, OT.CreateDateUTC
		, OT.CreatedByUser
		, OT.LastChangeDateUTC
		, OT.LastChangedByUser
		, OT.DeleteDateUTC
		, OT.DeletedByUser
	FROM dbo.tblOriginTank OT
	JOIN dbo.tblOrder O ON O.OriginID = OT.OriginID
	LEFT JOIN tblOrderAppChanges OAC ON OAC.OrderID = O.ID
	CROSS JOIN fnSyncLCDOffset(@LastSyncDateUTC) LCD
	WHERE O.StatusID IN (2, 7, 8, 3) -- Dispatched, Accepted, Picked Up, Delivered
	  AND O.DeleteDateUTC IS NULL -- don't send OriginTank records for deleted orders
	  AND O.DriverID = @DriverID
	  AND (@LastSyncDateUTC IS NULL 
		OR O.CreateDateUTC >= LCD
		OR O.LastChangeDateUTC >= LCD
		-- required to ensure that when the Order.OriginID is changed, the OriginTanks/Strappings are always synced
		OR OAC.OriginChangeDateUTC >= LCD
		OR OT.CreateDateUTC >= LCD
		OR OT.LastChangeDateUTC >= LCD
		OR OT.DeleteDateUTC >= LCD
	  )
GO

/*******************************************
 Date Created: 5 Apr 2014
 Author: Kevin Alons
 Purpose: return OriginTankStrapping data for Driver App sync
 Changes:
 - 3.10.5		- 2016/01/29	- KDA	- use new OAC table to only include when the Order.OriginID changes
										- show IsDeleted = 1 when the record or any relevant parent records are deleted
 - 3.10.10.2	- 2016/02/24	- KDA	- remove VirtualDelete logic and ensure when an order.LastChangeDateUTC changes (not just CreateDateUTC)
 - 3.11.2		- 2016/02/20	- KDA	- eliminate use of Virtual Deletes - we can't delete them from here for deleted orders (these could be needed for another order)
										- rename @LastChangeDateUTC to @LastSyncDateUTC
*******************************************/
ALTER FUNCTION fnOriginTankStrapping_DriverApp(@DriverID int, @LastSyncDateUTC datetime) RETURNS TABLE AS
RETURN 
	SELECT DISTINCT OTS.*
		, cast(CASE WHEN isnull(ltrim(OTSD.ID), ltrim(OT.DeleteDateUTC)) IS NULL THEN 0 ELSE 1 END as bit) AS IsDeleted
	FROM dbo.tblOriginTankStrapping OTS
	JOIN dbo.tblOriginTank OT ON OT.ID = OTS.OriginTankID
	JOIN dbo.tblOrder O ON O.OriginID = OT.OriginID
	LEFT JOIN tblOrderAppChanges OAC ON OAC.OrderID = O.ID
	LEFT JOIN dbo.tblOriginTankStrappingDeleted OTSD ON OTSD.ID = OTS.ID
	CROSS JOIN fnSyncLCDOffset(@LastSyncDateUTC) LCD
	WHERE O.StatusID IN (2, 7, 8, 3) -- Dispatched, Accepted, Picked Up, Delivered
	  AND O.DeleteDateUTC IS NULL -- don't send OriginTank records for deleted orders
	  AND O.DriverID = @DriverID
	  AND (@LastSyncDateUTC IS NULL 
		OR O.CreateDateUTC >= LCD
		OR O.LastChangeDateUTC >= LCD
		-- required to ensure that when the Order.OriginID is changed, the OriginTanks/Strappings are always synced
		OR OAC.OriginChangeDateUTC >= LCD
		OR OT.CreateDateUTC >= LCD
		OR OT.LastChangeDateUTC >= LCD
		OR OTS.CreateDateUTC >= LCD
		OR OTS.LastChangeDateUTC >= LCD
		OR OTSD.DeleteDateUTC >= LCD)
GO

/**********************************************************
-- Author: Kevin Alons
-- Date Created: 26 Sep 2013
-- Purpose: DELETED: track changes to Orders that cause it be "virtually" deleted for a Driver (for DriveApp.Sync purposes)
--			track changes to Orders that cause it be "virtually" deleted for a Gauger (for GaugerApp.Sync purposes)
-- Changes: 
 - 3.11.2	- 2016/02/21	- KDA	- remove all Driver App functionality from this trigger (entire trigger can be removed when Gauger App SYNC logic also revised)
**********************************************************/
ALTER TRIGGER trigOrder_U_VirtualDelete ON tblOrder AFTER UPDATE AS
BEGIN
	SET NOCOUNT ON;
	
	IF (TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU')) = 0 AND TRIGGER_NESTLEVEL( OBJECT_ID('trigOrderTicket_IU')) = 0
		AND EXISTS (SELECT * FROM inserted EXCEPT SELECT * FROM deleted)) 
	BEGIN
		PRINT 'trigOrder_U_VirtualDelete FIRED'
		
		-- delete any records that no longer apply because now the record is valid for the new gauger/status
		DELETE FROM tblGaugerOrderVirtualDelete
		FROM tblGaugerOrderVirtualDelete GOVD
		JOIN tblGaugerOrder GAO ON GAO.OrderID = GOVD.OrderID
		JOIN inserted i ON i.ID = GOVD.OrderID AND GAO.GaugerID = GOVD.GaugerID
		WHERE i.StatusID IN (-9, -10)
			
		-- record that the gauger order is no longer in a status that can be viewed/edited by a Gauger
		INSERT INTO tblGaugerOrderVirtualDelete (OrderID, GaugerID, VirtualDeleteDateUTC, VirtualDeletedByUser)
			SELECT i.ID, GAO.GaugerID, i.LastChangeDateUTC, i.LastChangedByUser
			FROM inserted i 
			JOIN deleted d ON d.ID = i.ID
			JOIN tblGaugerOrder GAO ON GAO.OrderID = i.ID
			LEFT JOIN tblGaugerOrderVirtualDelete GOVD ON GOVD.OrderID = i.ID AND GOVD.GaugerID = GAO.GaugerID
			WHERE i.StatusID <> d.StatusID
			  AND GOVD.ID IS NULL
			  AND i.StatusID NOT IN (-9, -10)

	END
END

GO
EXEC sp_settriggerorder @triggername=N'[dbo].[trigOrder_U_VirtualDelete]', @order=N'Last', @stmttype=N'UPDATE'
GO

DROP TABLE tblOrderDriverAppVirtualDelete
GO

COMMIT
SET NOEXEC OFF