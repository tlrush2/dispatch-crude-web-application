/*  
	-- add settlement factor for GSV and relabel the existing
*/ 
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.6.21'
SELECT  @NewVersion = '2.6.22'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

/***********************************/
-- Date Created: 28 Feb 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
/***********************************/
ALTER VIEW [dbo].[viewOrderExportFull] AS
SELECT *
  , OriginMinutes + DestMinutes AS TotalMinutes
  , isnull(OriginWaitMinutes, 0) + isnull(DestWaitMinutes, 0) AS TotalWaitMinutes
FROM (
	SELECT O.*
	  , dbo.fnMaxInt(0, isnull(OriginMinutes, 0) - cast(S.Value as int)) AS OriginWaitMinutes
	  , dbo.fnMaxInt(0, isnull(DestMinutes, 0) - cast(S.Value as int)) AS DestWaitMinutes
	  , (SELECT count(*) FROM tblOrderTicket WHERE OrderID = O.ID AND DeleteDateUTC IS NULL) AS TicketCount
	  , (SELECT count(*) FROM tblOrderReroute WHERE OrderID = O.ID) AS RerouteCount
	FROM dbo.viewOrderLocalDates O
	JOIN dbo.tblSetting S ON S.ID = 7 -- the Unbillable Wait Time threshold minutes 
WHERE O.StatusID NOT IN (-10) -- don't include "Generated" orders (only Assigned+)
  AND O.DeleteDateUTC IS NULL
) v

GO

INSERT INTO dbo.tblSettlementFactor (ID, Name) VALUES (3, 'GSV')
GO
UPDATE dbo.tblSettlementFactor SET Name = 'GOV' WHERE ID = 1
GO
UPDATE dbo.tblSettlementFactor SET Name = 'NSV' WHERE ID = 2
GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Carrier "Settlement" $$ values to an Audited order
/***********************************/
ALTER PROCEDURE [dbo].[spProcessCarrierInvoice]
(
  @ID int
, @UserName varchar(100)
, @ChainupFee smallmoney = NULL
, @OriginWaitFee smallmoney = NULL
, @DestWaitFee smallmoney = NULL
, @RerouteFee smallmoney = NULL
, @RejectionFee smallmoney = NULL
, @H2SFee smallmoney = NULL
, @LoadFee smallmoney = NULL
) AS BEGIN
	DELETE FROM tblOrderInvoiceCarrier WHERE OrderID = @ID
	
	-- all Units and Rates are first normalized to the Order.OriginUOM then consistent processing
	-- and persisted in the Invoice (Settlement) record in this Order.OriginUOM 
	INSERT INTO tblOrderInvoiceCarrier (OrderID
		, ChainupFee, RerouteFee
		, WaitFeeSubUnitID, WaitFeeRoundingTypeID, BillableOriginWaitMinutes, BillableDestWaitMinutes, WaitRate, OriginWaitFee, DestWaitFee
		, UomID, MinSettlementUnits, Units
		, RouteRate, H2SRate, TaxRate
		, RejectionFee, LoadFee, H2SFee, FuelSurcharge
		, TotalFee, CreateDateUTC, CreatedByUser)
	SELECT D.ID
		, ChainupFee, RerouteFee
		, WaitFeeSubUnitID, WaitFeeRoundingTypeID, BillableOriginWaitMinutes, BillableDestWaitMinutes, WaitRate, OriginWaitFee, DestWaitFee
		, OrderOriginUomID, MinSettlementUnits, ActualUnits
		, RouteRate, H2SRate, TaxRate
		, RejectionFee, LoadFee, H2SFee, FuelSurcharge
		, RejectionFee + ChainupFee + RerouteFee + OriginWaitFee + DestWaitFee + H2SFee + LoadFee AS TotalFee
		, GETUTCDATE(), @UserName
	FROM (
		-- compute the actual fees or use the manual overide fee values (if provided)
		SELECT ID
			, coalesce(@ChainupFee, Chainup * ChainupFee, 0) AS ChainupFee
			, coalesce(@RerouteFee, RerouteFee * RerouteCount, 0) AS RerouteFee
			, WaitFeeSubUnitID
			, WaitFeeRoundingTypeID
			, cast(round(BillableOriginWaitHours * 60, 0) as int) AS BillableOriginWaitMinutes
			, cast(round(BillableDestWaitHours * 60, 0) as int) AS BillableDestWaitMinutes
			, WaitRate
			, coalesce(@OriginWaitFee, BillableOriginWaitHours * WaitRate, 0) AS OriginWaitFee
			, coalesce(@DestWaitFee, BillableDestWaitHours * WaitRate, 0) AS DestWaitFee
			, coalesce(@RejectionFee, Rejected * RejectionRate, 0) AS RejectionFee
			, H2SRate
			, MinSettlementUnits
			, ActualUnits
			, coalesce(@H2SFee, H2S * dbo.fnMaxDecimal(MinSettlementUnits, ActualUnits) * H2SRate, 0) AS H2SFee
			, isnull(TaxRate, 0) AS TaxRate
			, isnull(RouteRate, 0) AS RouteRate
			-- if rejected, use 0 AS the Rate
			, round(coalesce(@LoadFee, (1 - Rejected) * dbo.fnMaxDecimal(MinSettlementUnits, ActualUnits) * RouteRate, 0), 4) AS LoadFee
			, isnull(FuelSurcharge, 0) AS FuelSurcharge
			, OrderOriginUomID
		FROM (
			-- normalize the Accessorial Rates to Order.OriginUOM + add normalized Route Rate and other time normalization
			SELECT S.ID
				, S.ChainUp
				-- chainupFee is not UOM dependent
				, CR.ChainupFee
				-- reroutefee is not UOM dependent
				, CR.RerouteFee
				, S.RerouteCount
				, CR.WaitFeeSubUnitID
				, CR.WaitFeeRoundingTypeID
				, dbo.fnComputeBillableWaitHours(S.OriginWaitMinutes, CR.WaitFeeSubUnitID, CR.WaitFeeRoundingTypeID) AS BillableOriginWaitHours
				, dbo.fnComputeBillableWaitHours(S.DestWaitMinutes, CR.WaitFeeSubUnitID, CR.WaitFeeRoundingTypeID) AS BillableDestWaitHours
				-- waitFee is not UOM dependent
				, CR.WaitFee AS WaitRate
				, S.Rejected
				-- rejectionFee is not based on UOM
				, CR.RejectionFee AS RejectionRate
				, S.H2S
				-- normalize the Carrier H2SRate for Order.OriginUOM
				, dbo.fnConvertRateUOM(isnull(S.H2S * CR.H2SRate, 0), CR.UomID, S.OrderOriginUomID) AS H2SRate
				, S.TaxRate
				-- normalize the Order.UOM Route Rate for Origin.OriginUOM
				, dbo.fnCarrierRouteRate(S.CarrierID, S.RouteID, S.OrderDate, S.OrderOriginUomID) AS RouteRate
				, S.MinSettlementUnits
				, isnull(S.ActualUnits, 0) AS ActualUnits
				, CR.FuelSurcharge
				, S.OrderOriginUomID
			FROM (
				-- get the Order raw data (with Units Normalized to Origin UOM) and matching Carrier Accessorial Rate ID
				SELECT O.ID
					, O.CarrierID
					, O.RouteID
					, O.ChainUp
					-- get the correct SettlementFactor ActualUnits
					, CASE C.SettlementFactorID WHEN 1 THEN O.OriginGrossUnits WHEN 3 THEN isnull(O.OriginGrossStdUnits, O.OriginGrossUnits) ELSE coalesce(O.OriginNetUnits, O.OriginGrossStdUnits, O.OriginGrossUnits) END AS ActualUnits
					, O.OriginUomID AS OrderOriginUomID
					, OO.UomID AS OriginUomID
					, O.RerouteCount
					, O.OriginWaitMinutes
					, O.DestWaitMinutes
					, O.Rejected
					, OO.H2S
					, O.OrderDate
					, coalesce(CRXX.ID, CRXA.ID, CRAX.ID, CRAA.ID) AS CRID
					-- normalize the Carrier MinSettlementUnits for the Order.OriginUOM
					, isnull(dbo.fnConvertUOM(C.MinSettlementUnits, C.MinSettlementUomID, O.OriginUomID), 0) AS MinSettlementUnits
					, OO.TaxRate
				FROM dbo.viewOrderExportFull O
				JOIN tblOrigin OO ON OO.ID = O.OriginID
				JOIN tblCarrier C ON C.ID = O.CarrierID
				LEFT JOIN tblCarrierRates CRXX ON CRXX.CarrierID = O.CarrierID AND CRXX.RegionID = OO.RegionID
				LEFT JOIN tblCarrierRates CRXA ON CRXA.CarrierID = O.CarrierID AND CRXA.RegionID = -1
				LEFT JOIN tblCarrierRates CRAX ON CRAX.CarrierID = -1 AND CRAX.RegionID = OO.RegionID
				LEFT JOIN tblCarrierRates CRAA ON CRAA.CarrierID = -1 AND CRAA.RegionID = -1
				WHERE O.ID = @ID
			) S
			LEFT JOIN tblCarrierRates CR ON CR.ID = S.CRID
		) SS
	) D
END

GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Customer "Settlement" $$ values to an Audited order
/***********************************/
ALTER PROCEDURE [dbo].[spProcessCustomerInvoice]
(
  @ID int
, @UserName varchar(100)
, @ChainupFee smallmoney = NULL
, @OriginWaitFee smallmoney = NULL
, @DestWaitFee smallmoney = NULL
, @RerouteFee smallmoney = NULL
, @RejectionFee smallmoney = NULL
, @H2SFee smallmoney = NULL
, @LoadFee smallmoney = NULL
) AS BEGIN
	DELETE FROM tblOrderInvoiceCustomer WHERE OrderID = @ID
	
	-- all Units and Rates are first normalized to the Order.OriginUOM then consistent processing
	-- and persisted in the Invoice (Settlement) record in this Order.OriginUOM 
	INSERT INTO tblOrderInvoiceCustomer (OrderID
		, ChainupFee, RerouteFee
		, WaitFeeSubUnitID, WaitFeeRoundingTypeID, BillableOriginWaitMinutes, BillableDestWaitMinutes, WaitRate, OriginWaitFee, DestWaitFee
		, UomID, MinSettlementUnits, Units
		, RouteRate, H2SRate, TaxRate
		, RejectionFee, LoadFee, H2SFee, FuelSurcharge
		, TotalFee, CreateDateUTC, CreatedByUser)
	SELECT D.ID
		, ChainupFee, RerouteFee
		, WaitFeeSubUnitID, WaitFeeRoundingTypeID, BillableOriginWaitMinutes, BillableDestWaitMinutes, WaitRate, OriginWaitFee, DestWaitFee
		, OrderOriginUomID, MinSettlementUnits, ActualUnits
		, RouteRate, H2SRate, TaxRate
		, RejectionFee, LoadFee, H2SFee, FuelSurcharge
		, RejectionFee + ChainupFee + RerouteFee + OriginWaitFee + DestWaitFee + H2SFee + LoadFee AS TotalFee
		, GETUTCDATE(), @UserName
	FROM (
		-- compute the actual fees or use the manual overide fee values (if provided)
		SELECT ID
			, coalesce(@ChainupFee, Chainup * ChainupFee, 0) AS ChainupFee
			, coalesce(@RerouteFee, RerouteFee * RerouteCount, 0) AS RerouteFee
			, WaitFeeSubUnitID
			, WaitFeeRoundingTypeID
			, cast(round(BillableOriginWaitHours * 60, 0) as int) AS BillableOriginWaitMinutes
			, cast(round(BillableDestWaitHours * 60, 0) as int) AS BillableDestWaitMinutes
			, WaitRate
			, coalesce(@OriginWaitFee, BillableOriginWaitHours * WaitRate, 0) AS OriginWaitFee
			, coalesce(@DestWaitFee, BillableDestWaitHours * WaitRate, 0) AS DestWaitFee
			, coalesce(@RejectionFee, Rejected * RejectionRate, 0) AS RejectionFee
			, H2SRate
			, MinSettlementUnits
			, ActualUnits
			, coalesce(@H2SFee, H2S * dbo.fnMaxDecimal(MinSettlementUnits, ActualUnits) * H2SRate, 0) AS H2SFee
			, isnull(TaxRate, 0) AS TaxRate
			, isnull(RouteRate, 0) AS RouteRate
			-- if rejected, use 0 AS the Rate
			, round(coalesce(@LoadFee, (1 - Rejected) * dbo.fnMaxDecimal(MinSettlementUnits, ActualUnits) * RouteRate, 0), 4) AS LoadFee
			, isnull(FuelSurcharge, 0) AS FuelSurcharge
			, OrderOriginUomID
		FROM (
			-- normalize the Accessorial Rates to Order.OriginUOM + add normalized Route Rate and other time normalization
			SELECT S.ID
				, S.ChainUp
				-- chainupFee is not UOM dependent
				, CR.ChainupFee
				-- reroutefee is not UOM dependent
				, CR.RerouteFee
				, S.RerouteCount
				, CR.WaitFeeSubUnitID
				, CR.WaitFeeRoundingTypeID
				, dbo.fnComputeBillableWaitHours(S.OriginWaitMinutes, CR.WaitFeeSubUnitID, CR.WaitFeeRoundingTypeID) AS BillableOriginWaitHours
				, dbo.fnComputeBillableWaitHours(S.DestWaitMinutes, CR.WaitFeeSubUnitID, CR.WaitFeeRoundingTypeID) AS BillableDestWaitHours
				-- waitFee is not UOM dependent
				, CR.WaitFee AS WaitRate
				, S.Rejected
				-- rejectionFee is not based on UOM
				, CR.RejectionFee AS RejectionRate
				, S.H2S
				-- normalize the Customer H2SRate for Order.OriginUOM
				, dbo.fnConvertRateUOM(isnull(S.H2S * CR.H2SRate, 0), CR.UomID, S.OrderOriginUomID) AS H2SRate
				, S.TaxRate
				-- normalize the Order.UOM Route Rate for Origin.OriginUOM
				, dbo.fnCustomerRouteRate(S.CustomerID, S.RouteID, S.OrderDate, S.OrderOriginUomID) AS RouteRate
				, S.MinSettlementUnits
				, isnull(S.ActualUnits, 0) AS ActualUnits
				, CR.FuelSurcharge
				, S.OrderOriginUomID
			FROM (
				-- get the Order raw data (with Units Normalized to Origin UOM) and matching Customer Accessorial Rate ID
				SELECT O.ID
					, O.CustomerID
					, O.RouteID
					, O.ChainUp
					-- get the correct SettlementFactor ActualUnits
					, CASE C.SettlementFactorID WHEN 1 THEN O.OriginGrossUnits WHEN 3 THEN isnull(O.OriginGrossStdUnits, O.OriginGrossUnits) ELSE coalesce(O.OriginNetUnits, O.OriginGrossStdUnits, O.OriginGrossUnits) END AS ActualUnits
					, O.OriginUomID AS OrderOriginUomID
					, OO.UomID AS OriginUomID
					, O.RerouteCount
					, O.OriginWaitMinutes
					, O.DestWaitMinutes
					, O.Rejected
					, OO.H2S
					, O.OrderDate
					, coalesce(CRXX.ID, CRXA.ID, CRAX.ID, CRAA.ID) AS CRID
					-- normalize the Customer MinSettlementUnits for the Order.OriginUOM
					, isnull(dbo.fnConvertUOM(C.MinSettlementUnits, C.MinSettlementUomID, O.OriginUomID), 0) AS MinSettlementUnits
					, OO.TaxRate
				FROM dbo.viewOrderExportFull O
				JOIN tblOrigin OO ON OO.ID = O.OriginID
				JOIN tblCustomer C ON C.ID = O.CustomerID
				LEFT JOIN tblCustomerRates CRXX ON CRXX.CustomerID = O.CustomerID AND CRXX.RegionID = OO.RegionID
				LEFT JOIN tblCustomerRates CRXA ON CRXA.CustomerID = O.CustomerID AND CRXA.RegionID = -1
				LEFT JOIN tblCustomerRates CRAX ON CRAX.CustomerID = -1 AND CRAX.RegionID = OO.RegionID
				LEFT JOIN tblCustomerRates CRAA ON CRAA.CustomerID = -1 AND CRAA.RegionID = -1
				WHERE O.ID = @ID
			) S
			LEFT JOIN tblCustomerRates CR ON CR.ID = S.CRID
		) SS
	) D
END

GO

COMMIT
SET NOEXEC OFF