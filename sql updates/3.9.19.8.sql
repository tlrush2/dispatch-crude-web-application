-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.9.19.7.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.9.19.7.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0

SET NOEXEC OFF
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.9.19.7'
SELECT  @NewVersion = '3.9.19.8'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Fix dbo.fnVersionToInt to work with 4 segment version numbers'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/***************************************************
-- Date Created: 12/13/2014
-- Author: Kevin Alons
-- Purpose: convert a version value to an int (for comparison/sorting purposes)
-- Changes:
	- 3.9.19.8 - update to allow this to work with 4 section version numbers (x.x.x.x)
***************************************************/
ALTER FUNCTION fnVersionToInt
(
	@version varchar(max)
)
RETURNS bigint
AS
BEGIN
	IF (@version LIKE '%;%' OR @version LIKE '%GO%')
	BEGIN
		RETURN 0
	END

	DECLARE @ID varchar(10), @Pos int, @ret bigint, @offset bigint; SELECT @ret = 0, @offset = 1000000000

	SET @version = LTRIM(RTRIM(@version))+ '.'
	SET @Pos = CHARINDEX('.', @version, 1)

	IF REPLACE(@version, '.', '') <> ''
	BEGIN
		WHILE @Pos > 0
		BEGIN
			SET @ret = @ret + @offset * cast(LTRIM(RTRIM(LEFT(@version, @Pos - 1))) as int)
			SET @offset = @offset / 1000
			SET @version = RIGHT(@version, LEN(@version) - @Pos)
			SET @Pos = CHARINDEX('.', @version, 1)

		END
	END	
	RETURN @ret
END

GO

EXEC _spRefreshAllViews
GO

COMMIT
SET NOEXEC OFF