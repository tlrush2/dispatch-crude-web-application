SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.4.1'
SELECT  @NewVersion = '4.4.1.1'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'Fix destination chainup sync passing wrong column name to sync model'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/*******************************************
 Date Created: 31 Aug 2013
 Author: Kevin Alons
 Purpose: return driver editable Order data for Driver App sync
 Changes: 
 - 3.8.9	- 07/24/15	 - GSM	- Added TickeTypeID field (was read-only before)
 - 3.7.4	- 05/08/15	 - GSM	- Added Rack/Bay field
 - 3.7.11	- 5/18/2015	 - KDA	- renamed RackBay to DestRackBay
 - 3.9.19.3 - 2015/09/29 - GSM	- remove transaction on Order.Accept|Pickup|Deliver LastChangeDateUTC fields (no longer use max of O.LastChangeDateUTC)
 - 3.9.38	- 2016/01/03 - KDA	- add OriginWeightNetUnits
 - 3.9.38	- 2016/01/11 - JAE	- added destination weight fields
 - 3.10.2.1	- 2015/01/28 - JAE	- Added clause to always send dispatched orders
 - 3.10.5	- 2015/01/30 - KDA	- remove workaround clause to always send DISPATCHED orders
 - 3.11.2    - 2015/02/20 - KDA+ - add @OrdersPresent table parameter, and use in logic
								- eliminate @LastChangeDateUTC parameter
								- no longer return DELETED records, only OrderReadonly records are turned when deleted (which will cause all related Driver App records to purge)
 - 4.4.1	- 2016/11/04 - JAE	- Add Destination Chainup
 - 4.4.1.1	- 2016/11/17 - JAE	- Rename OriginChainup column to Chainup since this directly feeds the driver app
*******************************************/
ALTER FUNCTION fnOrderEdit_DriverApp(@DriverID int, @OrdersPresent OrdersPresent READONLY) RETURNS TABLE AS
RETURN 
      SELECT O.ID
            , O.StatusID
            , O.TruckID
            , O.TrailerID
            , O.Trailer2ID
            , O.OriginBOLNum
            , O.OriginArriveTimeUTC
            , O.OriginDepartTimeUTC
            , O.OriginMinutes
            , O.OriginWaitReasonID
            , O.OriginWaitNotes
            , O.OriginTruckMileage
            , O.OriginGrossUnits
            , O.OriginGrossStdUnits
            , O.OriginNetUnits
            , O.OriginWeightNetUnits
            , O.DestWeightGrossUnits
            , O.DestWeightTareUnits
            , O.DestWeightNetUnits
            , Chainup = O.OriginChainUp
            , O.DestChainUp
            , O.Rejected
            , O.RejectReasonID
            , O.RejectNotes
            , O.OriginTankNum
            , O.DestArriveTimeUTC
            , O.DestDepartTimeUTC
            , O.DestMinutes
            , O.DestWaitReasonID
            , O.DestWaitNotes
            , O.DestBOLNum
            , O.DestTruckMileage
            , O.DestGrossUnits
            , O.DestNetUnits
            , O.DestProductTemp
            , O.DestProductBSW
            , O.DestProductGravity
            , O.DestOpenMeterUnits
            , O.DestCloseMeterUnits
            , O.CarrierTicketNum
            , O.AcceptLastChangeDateUTC 
            , O.PickupLastChangeDateUTC 
            , O.DeliverLastChangeDateUTC
            , O.PickupPrintStatusID
            , O.DeliverPrintStatusID
            , O.PickupPrintDateUTC
            , O.DeliverPrintDateUTC
            , O.PickupDriverNotes
            , O.DeliverDriverNotes
            , O.DestRackBay
            , O.TicketTypeID
            , OELC = O.LastChangeDateUTC /* "OELC" = "OrderEditLastChangeDateUTC" is named to match the OrderEdit model */
      FROM dbo.tblOrder O
	  LEFT JOIN @OrdersPresent OP ON OP.ID = O.ID
	  LEFT JOIN tblOrderAppChanges OAC ON OAC.OrderID = O.ID
      WHERE DriverID = @driverID AND O.StatusID IN (2,7,8,3) AND O.DeleteDateUTC IS NULL
		/* 3.11.2 - change to ensure that any order missing or out-of-date on the tablet is sync'ed */
        AND (OP.ID IS NULL OR cast(O.LastChangeDateUTC as smalldatetime) > cast(isnull(OP.OELastChangeDateUTC, '1/1/1900') as smalldatetime))

GO

COMMIT
SET NOEXEC OFF
