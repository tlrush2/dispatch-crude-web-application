/* fix WaitMinutes SubUnit = None to do actual no rounding
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.0.11', @NewVersion = '2.0.12'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

/***********************************************************/
--  Date Created: 22 June 2013
--  Author: Kevin Alons
--  Purpose: Round the Billable Wait minutes into a decimal hour value (based on rounding parameters)
/***********************************************************/
ALTER FUNCTION [dbo].[fnComputeBillableWaitHours](@min int, @SubUnitID int, @RoundingTypeID int) RETURNS decimal (9,3) AS
BEGIN
	DECLARE @ret decimal(9,3) 
	SELECT @ret = @min
	
	IF (@SubUnitID <> 0)
	BEGIN
		SELECT @ret = @ret / @SubUnitID
		IF (@RoundingTypeID = 1)
			SELECT @ret = floor(@ret)
		ELSE IF (@RoundingTypeID = 2)
			SELECT @ret = round(@ret - 0.01, 0)
		ELSE IF (@RoundingTypeID = 3)
			SELECT @ret = round(@ret, 0)
		ELSE IF (@RoundingTypeID = 4)
			SELECT @ret = ceiling(@ret)
		ELSE
			SELECT @ret = 0
	END
	
	RETURN @ret / (60.0 / CASE WHEN @SubUnitID = 0 THEN 1 ELSE @SubUnitID END)
END

GO

COMMIT
SET NOEXEC OFF