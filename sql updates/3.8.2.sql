-- backup database [dispatchcrude.dev] to disk = 'C:\files\Consulting\cts\dc_backups\dispatchcrude.dev.3.8.1.bak'
-- restore database [DispatchCrude.Dev] from disk = 'C:\files\Consulting\cts\dc_backups\dispatchcrude.dev.3.8.1.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.8.1'
SELECT  @NewVersion = '3.8.2'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Destination Allocations Dashboard: performance optimizations'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/********************************************
-- Date Created: 2015/07/06
-- Author: Kevin Alons
-- Purpose: retrieve the DAILY Allocation + Production Units for the specified selection criteria + Date Range
--			done as a function (vs a PROCEDURE) to allow the results to be easily Grouped as needed
** Changes:
	- 3.8.2 - 2015/07/07 - KDA - add use of table variable (UNIQUE) indices & add logic to prevent dupe records
********************************************/
ALTER FUNCTION fnAllocationDestinationShipper_Daily_ByMonth
( 
  @DestinationID int = -1
, @ShipperID int = -1
, @ProductGroupID int = -1
, @StartDate date
, @EndDate date
, @UomID int = 1
) RETURNS @ret TABLE 
(
  DestinationID int
, Destination varchar(100)
, ShipperID int
, Shipper varchar(100)
, ProductGroupID int
, ProductGroup varchar(100)
, UomID int
, Uom varchar(100)
, DayDate date
, AllocatedUnits money
, ProductionUnits money
, LoadCount int
, _ID int
, _Processed bit
, UNIQUE(DestinationID, ShipperID, ProductGroupID, DayDate)
) AS BEGIN
	
	IF @StartDate IS NULL SET @StartDate = dbo.fnFirstDOM(getdate())
	IF @EndDate IS NULL SET @EndDate = dbo.fnLastDOM(@StartDate)
	
	DECLARE @QStart date, @QEnd date
	SELECT @QStart = dbo.fnFirstDOM(@StartDate), @QEnd = dbo.fnLastDOM(@EndDate)

	-- retrieve the relevant defined Allocation records (by DayDate), normalized to BBLS UOM
	INSERT INTO @ret (DestinationID, ShipperID, ProductGroupID, UomID, DayDate, AllocatedUnits, ProductionUnits, _Processed, _ID)
		SELECT ret.DestinationID, ret.ShipperID, ret.ProductGroupID, @UomID, DD.DayDate, Units = sum(dbo.fnConvertUOM(DailyUnits, UomID, @UomID)), 0, 0
			, ROW_NUMBER() OVER (ORDER BY DayDate, DestinationID, ShipperID, ProductGroupID)
		FROM viewAllocationDestinationShipper ret
		CROSS APPLY fnDaysInDateRange(@StartDate, @EndDate) DD 
		WHERE (@QStart BETWEEN ret.EffectiveDate AND ret.EndDate OR @QEnd BETWEEN ret.EffectiveDate AND ret.EndDate OR ret.EffectiveDate BETWEEN @QStart AND @QEnd)
		  AND (@DestinationID = -1 OR ret.DestinationID = @DestinationID)
		  AND (@ShipperID = -1 OR ret.ShipperID = @ShipperID)
		  AND (@ProductGroupID = -1 OR ret.ProductGroupID = @ProductGroupID)
		GROUP BY ret.DestinationID, ret.ShipperID, ret.ProductGroupID, DD.DayDate

	-- retrieve the relevant, eligible DestGrossUnits (or OriginGrossUnits if not available) totals (normalized to BBLS UOM)
	DECLARE @PDS TABLE (DID int, SID int, PGID int, FDOM date, ProdUnits decimal(18, 4), LC int, _ID int, UNIQUE(DID, SID, PGID, FDOM))
	INSERT INTO @PDS
		SELECT O.DestinationID, O.CustomerID, P.ProductGroupID, dbo.fnFirstDOM(O.DeliverDate)
			, Units = sum(isnull(dbo.fnConvertUOM(nullif(O.DestGrossUnits, 0), DestUomID, @UomID), dbo.fnConvertUOM(nullif(O.OriginGrossUnits, 0), OriginUomID, @UomID)))
			, LC = COUNT(*)
			, ROW_NUMBER() OVER (ORDER BY O.DestinationID)
		FROM viewOrderBase O
		JOIN tblProduct P ON P.ID = O.ProductID
		WHERE DeliverDate BETWEEN @QStart AND @QEnd
		  AND DeleteDateUTC IS NULL
		  AND Rejected = 0
		  AND StatusID IN (3, 4)
		  AND (@DestinationID = -1 OR O.DestinationID = @DestinationID)
		  AND (@ShipperID = -1 OR O.CustomerID = @ShipperID)
		  AND (@ProductGroupID = -1 OR P.ProductGroupID = @ProductGroupID)
		GROUP BY O.DestinationID, O.CustomerID, P.ProductGroupID, dbo.fnFirstDOM(O.DeliverDate)

--select * from @ret
--select * from @PDS
	DECLARE @_ID int, @DID int, @SID int, @PGID int, @FDOM date, @Units decimal(18, 4), @LC int, @DD date, @done bit
	SELECT TOP 1 @_ID = _ID, @done = 0 FROM @PDS ORDER BY _ID
	WHILE EXISTS (SELECT * FROM @PDS)
	BEGIN
		SELECT TOP 1 @DID = DID, @SID = SID, @PGID = PGID, @FDOM = FDOM, @Units = ProdUnits, @LC = LC, @DD = FDOM, @done = 0 FROM @PDS WHERE _ID = @_ID
		WHILE MONTH(@DD) = MONTH(@FDOM)
		BEGIN
			-- set @done = FALSE and ensure @ROWCOUNT is reset to 0
			UPDATE @ret SET _Processed = 0 -- ensure they are all reset to FALSE
			SELECT @done = 0 WHERE @@ROWCOUNT >= 0
			-- sequentially "assign" the Production units against the daily allocation records (oldest to newest)
			WHILE (@done = 0)
			BEGIN
				UPDATE @ret
				  SET ProductionUnits = dbo.fnMinDecimal(AllocatedUnits, @Units), @Units = @Units - dbo.fnMinInt(AllocatedUnits, @Units), LoadCount = @LC, @LC = 0, _Processed = 1
				WHERE _ID IN (SELECT TOP 1 _ID FROM @ret WHERE DestinationID = @DID AND ShipperID = @SID AND ProductGroupID = @PGID AND DayDate = @DD AND _Processed = 0 ORDER BY _ID)
				SELECT @done = CASE WHEN @@ROWCOUNT = 0 THEN 1 ELSE 0 END
			END
			SET @DD = DATEADD(day, 1, @DD)
		END
		-- if remaining production units exist, then create a new record for these EXCESS units
		IF (@Units >= 1)
		BEGIN
			UPDATE @ret SET ProductionUnits = ProductionUnits + @Units, LoadCount = LoadCount + @LC WHERE DestinationID = @DID AND ShipperID = @SID AND ProductGroupID = @PGID AND DayDate = @FDOM
			IF @@ROWCOUNT = 0
				INSERT INTO @ret (DestinationID, ShipperID, ProductGroupID, UomID, DayDate, ProductionUnits, LoadCount)
					SELECT @DID, @SID, @PGID, @UomID, @FDOM, @Units, @LC
		END

		DELETE FROM @PDS WHERE _ID = @_ID
		SELECT TOP 1 @_ID = _ID FROM @PDS ORDER BY _ID
	END
	
	UPDATE @ret
		SET Destination = D.Name
			, Shipper = C.Name
			, ProductGroup = PG.Name
			, Uom = U.Name
			, AllocatedUnits = AllocatedUnits
			, _ID = NULL, _Processed = NULL
	FROM @ret X
	JOIN tblDestination D ON D.ID = X.DestinationID
	JOIN tblCustomer C ON C.ID = X.ShipperID
	JOIN tblProductGroup PG ON PG.ID = X.ProductGroupID
	JOIN tblUom U ON U.ID = X.UomID

	RETURN
END
GO

EXEC _spDropProcedure spAllocationDestinationShipper_Daily_ByMonth
GO
/********************************************
-- Date Created: 2015/07/07
-- Author: Kevin Alons
-- Purpose: retrieve the DAILY Allocation + Production Units for the specified selection criteria + Date Range
********************************************/
CREATE PROCEDURE spAllocationDestinationShipper_Daily_ByMonth
( 
  @DestinationID int = -1
, @ShipperID int = -1
, @ProductGroupID int = -1
, @StartDate date = NULL
, @EndDate date = NULL
, @UomID int = 1
) AS BEGIN

SET NOCOUNT ON 
	DECLARE @ret TABLE
	(
	  DestinationID int
	, ShipperID int
	, ProductGroupID int
	, UomID int
	, DayDate date
	, AllocatedUnits money
	, ProductionUnits money
	, LoadCount int
	, _ID int
	, _Processed bit
	, UNIQUE(DestinationID, ShipperID, ProductGroupID, DayDate)
	) 
	
	IF @StartDate IS NULL SET @StartDate = dbo.fnFirstDOM(getdate())
	IF @EndDate IS NULL SET @EndDate = dbo.fnLastDOM(@StartDate)
	
	DECLARE @QStart date, @QEnd date
	SELECT @QStart = dbo.fnFirstDOM(@StartDate), @QEnd = dbo.fnLastDOM(@EndDate)

	-- retrieve the relevant defined Allocation records (by DayDate), normalized to BBLS UOM
	INSERT INTO @ret (DestinationID, ShipperID, ProductGroupID, UomID, DayDate, AllocatedUnits, ProductionUnits, _Processed, _ID)
		SELECT ret.DestinationID, ret.ShipperID, ret.ProductGroupID, @UomID, DD.DayDate, Units = sum(dbo.fnConvertUOM(DailyUnits, UomID, @UomID)), 0, 0
			, ROW_NUMBER() OVER (ORDER BY DayDate, DestinationID, ShipperID, ProductGroupID)
		FROM viewAllocationDestinationShipper ret
		CROSS APPLY fnDaysInDateRange(@StartDate, @EndDate) DD 
		WHERE (@QStart BETWEEN ret.EffectiveDate AND ret.EndDate OR @QEnd BETWEEN ret.EffectiveDate AND ret.EndDate OR ret.EffectiveDate BETWEEN @QStart AND @QEnd)
		  AND (@DestinationID = -1 OR ret.DestinationID = @DestinationID)
		  AND (@ShipperID = -1 OR ret.ShipperID = @ShipperID)
		  AND (@ProductGroupID = -1 OR ret.ProductGroupID = @ProductGroupID)
		GROUP BY ret.DestinationID, ret.ShipperID, ret.ProductGroupID, DD.DayDate

	-- retrieve the relevant, eligible DestGrossUnits (or OriginGrossUnits if not available) totals (normalized to BBLS UOM)
	DECLARE @PDS TABLE (DID int, SID int, PGID int, FDOM date, ProdUnits decimal(18, 4), LC int, _ID int, UNIQUE(DID, SID, PGID, FDOM))
	INSERT INTO @PDS
		SELECT O.DestinationID, O.CustomerID, P.ProductGroupID, dbo.fnFirstDOM(O.DeliverDate)
			, Units = sum(isnull(dbo.fnConvertUOM(nullif(O.DestGrossUnits, 0), DestUomID, @UomID), dbo.fnConvertUOM(nullif(O.OriginGrossUnits, 0), OriginUomID, @UomID)))
			, LC = COUNT(*)
			, ROW_NUMBER() OVER (ORDER BY O.DestinationID)
		FROM viewOrderBase O
		JOIN tblProduct P ON P.ID = O.ProductID
		WHERE DeliverDate BETWEEN @QStart AND @QEnd
		  AND DeleteDateUTC IS NULL
		  AND Rejected = 0
		  AND StatusID IN (3, 4)
		  AND (@DestinationID = -1 OR O.DestinationID = @DestinationID)
		  AND (@ShipperID = -1 OR O.CustomerID = @ShipperID)
		  AND (@ProductGroupID = -1 OR P.ProductGroupID = @ProductGroupID)
		GROUP BY O.DestinationID, O.CustomerID, P.ProductGroupID, dbo.fnFirstDOM(O.DeliverDate)

--select * from @ret
--select * from @PDS
	DECLARE @_ID int, @DID int, @SID int, @PGID int, @FDOM date, @Units decimal(18, 4), @LC int, @DD date, @done bit
	SELECT TOP 1 @_ID = _ID, @done = 0 FROM @PDS ORDER BY _ID
	WHILE EXISTS (SELECT * FROM @PDS)
	BEGIN
		SELECT TOP 1 @DID = DID, @SID = SID, @PGID = PGID, @FDOM = FDOM, @Units = ProdUnits, @LC = LC, @DD = FDOM, @done = 0 FROM @PDS WHERE _ID = @_ID
		WHILE MONTH(@DD) = MONTH(@FDOM)
		BEGIN
			-- set @done = FALSE and ensure @ROWCOUNT is reset to 0
			UPDATE @ret SET _Processed = 0 -- ensure they are all reset to FALSE
			SELECT @done = 0 WHERE @@ROWCOUNT >= 0
			-- sequentially "assign" the Production units against the daily allocation records (oldest to newest)
			WHILE (@done = 0)
			BEGIN
				UPDATE @ret
				  SET ProductionUnits = dbo.fnMinDecimal(AllocatedUnits, @Units), @Units = @Units - dbo.fnMinInt(AllocatedUnits, @Units), LoadCount = @LC, @LC = 0, _Processed = 1
				WHERE _ID IN (SELECT TOP 1 _ID FROM @ret WHERE DestinationID = @DID AND ShipperID = @SID AND ProductGroupID = @PGID AND DayDate = @DD AND _Processed = 0 ORDER BY _ID)
				SELECT @done = CASE WHEN @@ROWCOUNT = 0 THEN 1 ELSE 0 END
			END
			SET @DD = DATEADD(day, 1, @DD)
		END
		-- if remaining production units exist, then create a new record for these EXCESS units
		IF (@Units >= 1)
		BEGIN
			UPDATE @ret SET ProductionUnits = ProductionUnits + @Units, LoadCount = LoadCount + @LC WHERE DestinationID = @DID AND ShipperID = @SID AND ProductGroupID = @PGID AND DayDate = @FDOM
			IF @@ROWCOUNT = 0
				INSERT INTO @ret (DestinationID, ShipperID, ProductGroupID, UomID, DayDate, ProductionUnits, LoadCount)
					SELECT @DID, @SID, @PGID, @UomID, @FDOM, @Units, @LC
		END

		DELETE FROM @PDS WHERE _ID = @_ID
		SELECT TOP 1 @_ID = _ID FROM @PDS ORDER BY _ID
	END;
	
	WITH cteBase AS 
	(
		SELECT DestinationID, Destination = D.Name, ShipperID, Shipper = C.Name, ProductGroupID, ProductGroup = PG.Name, X.UomID, Uom = U.Name
			, DayDate, AllocatedUnits = ISNULL(AllocatedUnits, 0), ProductionUnits, LoadCount  
		FROM @ret X
		JOIN tblDestination D ON D.ID = X.DestinationID
		JOIN tblCustomer C ON C.ID = X.ShipperID
		JOIN tblProductGroup PG ON PG.ID = X.ProductGroupID
		JOIN tblUom U ON U.ID = X.UomID
	)

	SELECT Destination, Shipper, ProductGroup, Uom, DayDate, UnitType
		, Units = round(SUM(Units), 0)
		, RemainingDays = (SELECT RemainingDays = CASE WHEN MONTH(getdate()) = MONTH(@StartDate) THEN DATEDIFF(day, getdate(), dbo.fnLastDOM(getdate())) + 1 ELSE 0 END)
	FROM (
		SELECT DestinationID, Destination, ShipperID, Shipper, ProductGroup, ProductGroupID, UomID, Uom, DayDate
			, Units, UnitType
		FROM (
			SELECT DestinationID, Destination, ShipperID, Shipper, ProductGroup, ProductGroupID, UomID, Uom, DayDate
				, Units = dbo.fnMinDecimal(ProductionUnits, AllocatedUnits), UnitType = 'Production' FROM cteBase
		) X
		WHERE Units <> 0
		UNION
		SELECT DestinationID, Destination, ShipperID, Shipper, ProductGroup, ProductGroupID, UomID, Uom, DayDate
		, Units = abs(Units), UnitType = CASE WHEN Units < 0 THEN ' Excess' ELSE ' Remaining' END
		FROM (
			SELECT DestinationID, Destination, ShipperID, Shipper, ProductGroup, ProductGroupID, UomID, Uom, DayDate
				, Units = isnull(AllocatedUnits, 0) - isnull(ProductionUnits, 0) 
			FROM cteBase
		) X
		WHERE Units <> 0
	) X
	GROUP BY Destination, Shipper, ProductGroup, Uom, DayDate, UnitType
END
GO
GRANT EXECUTE ON spAllocationDestinationShipper_Daily_ByMonth TO role_iis_acct
GO

EXEC _spRebuildAllObjects
GO

COMMIT
SET NOEXEC OFF