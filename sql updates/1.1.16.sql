--rollback
BEGIN TRANSACTION
GO

UPDATE tblSetting SET Value = '1.1.16' WHERE ID=0

EXEC sp_rename 
	@objname = 'tblCarrier.LastChangeDate',
    @newname = 'LastUpdateDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblCarrier.LastChangedByUser',
    @newname = 'LastUpdatedByUser',
    @objtype = 'COLUMN'
GO
ALTER TABLE tblCarrier Add CarrierAgreementDocument varbinary(max) null
GO
ALTER TABLE tblCarrier ADD CarrierAgreementDocName varchar(255) NULL
GO

EXEC sp_rename 
	@objname = 'tblCarrierRangeRates.LastChangeDate',
    @newname = 'LastUpdateDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblCarrierRangeRates.LastChangedByUser',
    @newname = 'LastUpdatedByUser',
    @objtype = 'COLUMN'
GO
EXEC sp_rename 
	@objname = 'tblCarrierRates.LastChangeDate',
    @newname = 'LastUpdateDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblCarrierRates.LastChangedByUser',
    @newname = 'LastUpdatedByUser',
    @objtype = 'COLUMN'
GO
EXEC sp_rename 
	@objname = 'tblCarrierType.LastChangeDate',
    @newname = 'LastUpdateDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblCarrierType.LastChangedByUser',
    @newname = 'LastUpdatedByUser',
    @objtype = 'COLUMN'
GO
EXEC sp_rename 
	@objname = 'tblCustomer.LastChangeDate',
    @newname = 'LastUpdateDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblCustomer.LastChangedByUser',
    @newname = 'LastUpdatedByUser',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblCustomerRangeRates.LastChangeDate',
    @newname = 'LastUpdateDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblCustomerRangeRates.LastChangedByUser',
    @newname = 'LastUpdatedByUser',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblCustomerRates.LastChangeDate',
    @newname = 'LastUpdateDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblCustomerRates.LastChangedByUser',
    @newname = 'LastUpdatedByUser',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblDestination.LastChangeDate',
    @newname = 'LastUpdateDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblDestination.LastChangedByUser',
    @newname = 'LastUpdatedByUser',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblDestinationType.LastChangeDate',
    @newname = 'LastUpdateDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblDestinationType.LastChangedByUser',
    @newname = 'LastUpdatedByUser',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblDriver.LastChangeDate',
    @newname = 'LastUpdateDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblDriver.LastChangedByUser',
    @newname = 'LastUpdatedByUser',
    @objtype = 'COLUMN'
GO
EXEC sp_rename 
	@objname = 'tblOperator.LastChangeDate',
    @newname = 'LastUpdateDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblOperator.LastChangedByUser',
    @newname = 'LastUpdatedByUser',
    @objtype = 'COLUMN'
GO        
EXEC sp_rename 
	@objname = 'tblOrder.LastChangeDate',
    @newname = 'LastUpdateDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblOrder.LastChangedByUser',
    @newname = 'LastUpdatedByUser',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblOrderInvoiceCarrier.LastChangeDate',
    @newname = 'LastUpdateDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblOrderInvoiceCarrier.LastChangedByUser',
    @newname = 'LastUpdatedByUser',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblOrderInvoiceCustomer.LastChangeDate',
    @newname = 'LastUpdateDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblOrderInvoiceCustomer.LastChangedByUser',
    @newname = 'LastUpdatedByUser',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblOrderStatus.LastChangeDate',
    @newname = 'LastUpdateDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblOrderStatus.LastChangedByUser',
    @newname = 'LastUpdatedByUser',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblOrderTicket.LastChangeDate',
    @newname = 'LastUpdateDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblOrderTicket.LastChangedByUser',
    @newname = 'LastUpdatedByUser',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblOrigin.LastChangeDate',
    @newname = 'LastUpdateDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblOrigin.LastChangedByUser',
    @newname = 'LastUpdatedByUser',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblOriginType.LastChangeDate',
    @newname = 'LastUpdateDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblOriginType.LastChangedByUser',
    @newname = 'LastUpdatedByUser',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblPriority.LastChangeDate',
    @newname = 'LastUpdateDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblPriority.LastChangedByUser',
    @newname = 'LastUpdatedByUser',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblProducer.LastChangeDate',
    @newname = 'LastUpdateDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblProducer.LastChangedByUser',
    @newname = 'LastUpdatedByUser',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblPumper.LastChangeDate',
    @newname = 'LastUpdateDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblPumper.LastChangedByUser',
    @newname = 'LastUpdatedByUser',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblRateTableRange.LastChangeDate',
    @newname = 'LastUpdateDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblRateTableRange.LastChangedByUser',
    @newname = 'LastUpdatedByUser',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblRegion.LastChangeDate',
    @newname = 'LastUpdateDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblRegion.LastChangedByUser',
    @newname = 'LastUpdatedByUser',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblRoute.LastChangeDate',
    @newname = 'LastUpdateDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblRoute.LastChangedByUser',
    @newname = 'LastUpdatedByUser',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblSetting.LastChangeDate',
    @newname = 'LastUpdateDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblSetting.LastChangedByUser',
    @newname = 'LastUpdatedByUser',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblSettingType.LastChangeDate',
    @newname = 'LastUpdateDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblSettingType.LastChangedByUser',
    @newname = 'LastUpdatedByUser',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblState.LastChangeDate',
    @newname = 'LastUpdateDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblState.LastChangedByUser',
    @newname = 'LastUpdatedByUser',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblTankType.LastChangeDate',
    @newname = 'LastUpdateDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblTankType.LastChangedByUser',
    @newname = 'LastUpdatedByUser',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblTicketType.LastChangeDate',
    @newname = 'LastUpdateDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblTicketType.LastChangedByUser',
    @newname = 'LastUpdatedByUser',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblTrailer.LastChangeDate',
    @newname = 'LastUpdateDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblTrailer.LastChangedByUser',
    @newname = 'LastUpdatedByUser',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblTrailerType.LastChangeDate',
    @newname = 'LastUpdateDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblTrailerType.LastChangedByUser',
    @newname = 'LastUpdatedByUser',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblTruck.LastChangeDate',
    @newname = 'LastUpdateDate',
    @objtype = 'COLUMN'
GO    
EXEC sp_rename 
	@objname = 'tblTruck.LastChangedByUser',
    @newname = 'LastUpdatedByUser',
    @objtype = 'COLUMN'
GO    
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRecompileAllStoredProcedures
GO
/***********************************/
-- Date Created: 24 Jan 2013
-- Author: Kevin Alons
-- Purpose: accomplish an Order Redirect
/***********************************/
ALTER PROCEDURE [dbo].[spRerouteOrder]
(
  @OrderID int
, @NewDestinationID int
, @UserName varchar(255)
, @Notes varchar(255)
)
AS BEGIN
	INSERT INTO tblOrderReroute (OrderID, PreviousDestinationID, UserName, Notes, RerouteDate)
		SELECT ID, DestinationID, @UserName, @Notes, getdate() FROM tblOrder WHERE ID = @OrderID
	
	UPDATE tblOrder 
		SET DestinationID = R.DestinationID, RouteID = R.ID, ActualMiles = R.ActualMiles
			, LastUpdateDate=GETDATE(), LastUpdatedByUser=@UserName
	FROM tblOrder O
	JOIN tblRoute R ON R.OriginID = O.OriginID AND R.DestinationID = @NewDestinationID
	WHERE O.ID = @OrderID
END

GO
/*******************************************/
-- Date Created: 25 Apr 2013
-- Author: Kevin Alons
-- Purpose: return Order data for Driver App sync
/*******************************************/
ALTER FUNCTION [dbo].[fnOrder_DriverApp]( @DriverID int, @LastUpdateDate datetime ) RETURNS  TABLE AS
RETURN 
	SELECT O.ID
		, O.OrderNum
		, O.TicketTypeID
		, O.StatusID
		, P.PriorityNum
		, O.DueDate
		, O.TruckID
		, O.TrailerID
		, O.Trailer2ID
		, O.Origin
		, O.OriginFull
		, O.OriginArriveTime
		, O.OriginDepartTime
		, O.OriginMinutes
		, O.OriginWaitNotes
		, O.OriginTruckMileage
		, O.OriginGrossBarrels
		, O.OriginNetBarrels
		, O.ChainUp
		, O.Rejected
		, O.RejectNotes
		, O.OriginTankNum
		, O.Destination
		, O.DestinationFull
		, O.DestArriveTime
		, O.DestDepartTime
		, O.DestMinutes
		, O.DestWaitNotes
		, O.DestTruckMileage
		, O.DestGrossBarrels
		, O.DestNetBarrels
		, O.CarrierTicketNum
		, D.BOLAvailable AS DestBOLAvailable
		, OO.TankTypeID
		, OO.LAT AS OriginLAT
		, OO.LON AS OriginLON
		, OO.BarrelsPerInch AS OriginBarrelsPerInch
		, O.CreateDate
		, O.CreatedByUser
		, O.LastUpdateDate
		, O.LastUpdatedByUser
		, O.DeleteDate
		, O.DeletedByUser
	FROM dbo.viewOrder O
	JOIN dbo.tblPriority P ON P.ID = O.PriorityID
	JOIN dbo.tblOrigin OO ON OO.ID = O.OriginID
	JOIN dbo.tblDestination D ON D.ID = O.DestinationID
	WHERE StatusID IN (2, 7, 8) -- Dispatched, Accepted, Picked Up
	  AND O.DriverID = @DriverID
	  AND (@LastUpdateDate IS NULL OR O.LastUpdateDate >= @LastUpdateDate)


GO

/*******************************************/
-- Date Created: 25 Apr 2013
-- Author: Kevin Alons
-- Purpose: return OrderTicket data for Driver App sync
/*******************************************/
ALTER FUNCTION [dbo].[fnOrderTicket_DriverApp]( @DriverID int, @LastUpdateDate datetime ) RETURNS  TABLE AS
RETURN 
	SELECT OT.ID
		, OT.UID
		, OT.OrderID
		, OT.CarrierTicketNum
		, OT.TankNum
		, OT.TicketTypeID
		, OT.TankTypeID
		, OT.ProductObsGravity
		, OT.ProductObsTemp
		, OT.ProductHighTemp
		, OT.ProductLowTemp
		, OT.ProductBSW
		, OT.OpeningGaugeFeet
		, OT.OpeningGaugeInch
		, OT.OpeningGaugeQ
		, OT.ClosingGaugeFeet
		, OT.ClosingGaugeInch
		, OT.ClosingGaugeQ
		, OT.GrossBarrels
		, OT.NetBarrels
		, OT.Rejected
		, OT.RejectNotes
		, OT.SealOff
		, OT.SealOn
		, OT.BOLNum
		, OT.BarrelsPerInch
		, OT.CreateDate
		, OT.CreatedByUser
		, OT.LastUpdateDate
		, OT.LastUpdatedByUser
		, OT.DeleteDate
		, OT.DeletedByUser
	FROM dbo.tblOrderTicket OT
	JOIN dbo.tblOrder O ON O.ID = OT.OrderID
	WHERE StatusID IN (2, 7, 8) -- Dispatched, Accepted, Picked Up
	  AND O.DriverID = @DriverID
	  AND (@LastUpdateDate IS NULL OR O.LastUpdateDate >= @LastUpdateDate)

GO
-- =============================================
-- Author:		Kevin Alons
-- Create date: 6 Feb 2013
-- Description:	trigger to ensure the GrossBarrels and NetBarrels are computed for valid, entered data
-- =============================================
ALTER TRIGGER [dbo].[trigOrderTicket_IU] ON [dbo].[tblOrderTicket] AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	-- re-compute GaugeRun ticket Gross barrels
	IF UPDATE(OpeningGaugeFeet) OR UPDATE(OpeningGaugeInch) OR UPDATE(OpeningGaugeQ)
		OR UPDATE (ClosingGaugeFeet) OR UPDATE(ClosingGaugeInch) OR UPDATE(ClosingGaugeQ) OR UPDATE(BarrelsPerInch)
		OR UPDATE (GrossBarrels)
	BEGIN
		UPDATE tblOrderTicket
		  SET GrossBarrels = dbo.fnTankQtyBarrelsDelta(
				OpeningGaugeFeet
			  , OpeningGaugeInch
			  , OpeningGaugeQ
			  , ClosingGaugeFeet
			  , ClosingGaugeInch
			  , ClosingGaugeQ
			  , BarrelsPerInch)
		WHERE ID IN (SELECT ID FROM inserted) 
		  AND TicketTypeID = 1 -- Gauge Run Ticket
		  AND OpeningGaugeFeet IS NOT NULL
		  AND OpeningGaugeInch IS NOT NULL
		  AND OpeningGaugeQ IS NOT NULL
		  AND ClosingGaugeFeet IS NOT NULL
		  AND ClosingGaugeInch IS NOT NULL
		  AND ClosingGaugeQ IS NOT NULL
		  AND BarrelsPerInch IS NOT NULL
	END
	-- re-compute GaugeRun ticket Net Barrels
	IF UPDATE(OpeningGaugeFeet) OR UPDATE(OpeningGaugeInch) OR UPDATE(OpeningGaugeQ)
		OR UPDATE (ClosingGaugeFeet) OR UPDATE(ClosingGaugeInch) OR UPDATE(ClosingGaugeQ) 
		OR UPDATE(BarrelsPerInch) OR UPDATE(GrossBarrels) OR UPDATE(NetBarrels)
		OR UPDATE(ProductObsTemp) OR UPDATE(ProductObsGravity) OR UPDATE(ProductBSW)
	BEGIN
		UPDATE tblOrderTicket
		  SET NetBarrels = dbo.fnCrudeNetCalculator(GrossBarrels, ProductObsTemp, ProductObsGravity, ProductBSW)
		WHERE ID IN (SELECT ID FROM inserted)
		  AND TicketTypeID IN (1,2)
		  AND GrossBarrels IS NOT NULL
		  AND ProductObsTemp IS NOT NULL
		  AND ProductObsGravity IS NOT NULL
		  AND ProductBSW IS NOT NULL
	END
	-- ensure the Order record is in-sync with the Tickets
	UPDATE tblOrder 
	SET OriginGrossBarrels = (SELECT sum(GrossBarrels) FROM tblOrderTicket WHERE OrderID = O.ID AND Rejected = 0 AND DeleteDate IS NULL)
	  , OriginNetBarrels = (SELECT sum(NetBarrels) FROM tblOrderTicket WHERE OrderID = O.ID AND Rejected = 0 AND DeleteDate IS NULL)
		-- use the first MeterRun CarrierTicketNum num as the Order BOL Num
	  , OriginBOLNum = (SELECT min(BOLNum) FROM tblOrderTicket WHERE OrderID = O.ID AND TicketTypeID = 3 AND DeleteDate IS NULL)
	  , CarrierTicketNum = (SELECT min(CarrierTicketNum) FROM tblOrderTicket WHERE ID IN 
		(SELECT min(ID) FROM tblOrderTicket WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND Rejected = 0 AND DeleteDate IS NULL))
	  , LastUpdateDate = (SELECT MAX(LastUpdateDate) FROM inserted WHERE OrderID = O.ID)
	  , LastUpdatedByUser = (SELECT MIN(LastUpdatedByUser) FROM inserted WHERE OrderID = O.ID)
	FROM tblOrder O
	WHERE ID IN (SELECT DISTINCT OrderID FROM inserted)
END

GO

DROP TABLE tblTrailerCompartment
GO
DROP TABLE tblDriverEndorseRestrict
GO
DROP TABLE tblDriverEndorsementRestrictType
GO

COMMIT
GO