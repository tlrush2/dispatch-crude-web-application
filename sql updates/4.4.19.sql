SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.4.18'
SELECT  @NewVersion = '4.4.19'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'JT-696 - Create Terminal table and add Terminal to Driver table'	
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

-- Create terminal table
CREATE TABLE tblTerminal
(
	ID INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_Terminal PRIMARY KEY
	, Name VARCHAR(50) NOT NULL
	, LAT DECIMAL(18,10) NULL
	, LON DECIMAL(18,10) NULL
	, Address VARCHAR(50) NULL
	, City VARCHAR(30) NULL
	, StateID INT NULL CONSTRAINT FK_Terminal_State REFERENCES tblState(ID)
	, Zip VARCHAR(10) NULL
	, OfficePhone VARCHAR(20) NULL
	, CreateDateUTC SMALLDATETIME NULL CONSTRAINT DF_Terminal_CreateDateUTC  DEFAULT (GETUTCDATE())
	, CreatedByUser VARCHAR(100) NULL
	, LastChangeDateUTC SMALLDATETIME NULL
	, LastChangedByUser VARCHAR(100) NULL
	, DeleteDateUTC SMALLDATETIME NULL
	, DeletedByUser VARCHAR(100) NULL
)
GO

-- Add TerminalID to tblDriver
ALTER TABLE tblDriver ADD TerminalID INT NULL
	CONSTRAINT FK_Driver_Terminal REFERENCES tblTerminal(ID)
GO


EXEC sp_refreshview viewDriverBase
GO


/***********************************/
-- Date Created: 26 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Drivers table records with LastDelivereredOrderTime + computed assigned ASP.NET usernames
-- Changes
--	4.4.14 - JAE & BB	- 2016-11-29 - Add previously included fields, Moved UserNames to viewDriverBase
--	4.4.19 - BB			- 2017-01-18 - Added Terminal
/***********************************/
ALTER VIEW viewDriver AS
SELECT DB.*
	, LastDeliveredOrderTime = (SELECT MAX(DestDepartTimeUTC) FROM dbo.tblOrder WHERE DriverID = DB.ID) 
	, DLDocument = dl.Document
	, DLDocName = dl.DocumentName
	, DLExpiration = dl.DocumentDate
	, CDLDocument = cdl.Document
	, CDLDocName = cdl.DocumentName
	, CDLExpiration = cdl.DocumentDate
	, DrugScreenBackgroundCheckDocument = dsbc.Document
	, DrugScreenBackgroundCheckDocName = dsbc.DocumentName
	, MotorVehicleReportDocument = mvr.Document
	, MotorVehicleReportDocName = mvr.DocumentName
	, MedicalCardDocument = mc.Document
	, MedicalCardDocName = mc.DocumentName
	, MedicalCardDate = mc.DocumentDate
	, LongFormPhysicalDocument = lfp.Document
	, LongFormPhysicalDocName = lfp.DocumentName
	, LongFormPhysicalDate = lfp.DocumentDate
	, EmploymentAppDocument = ea.Document
	, EmploymentAppDocName = ea.DocumentName
	, PolicyManualReceiptDocument = pmr.Document
	, PolicyManualReceiptDocName = pmr.DocumentName
	, TrainingReceiptDocument = tr.Document
	, TrainingReceiptDocName = tr.DocumentName
	, OrientationTestDocument = ot.Document
	, OrientationTestDocName = ot.DocumentName
	, DrugScreenBackgroundReleaseDocument = dsbr.Document
	, DrugScreenBackgroundReleaseDocName = dsbr.DocumentName
	, Terminal = T.Name
FROM viewDriverBase DB
	LEFT JOIN tblTerminal T ON T.ID = DB.TerminalID
	OUTER APPLY (select TOP 1 * FROM tblDriverCompliance WHERE ComplianceTypeID=1 AND DriverID = DB.ID ORDER BY DocumentDate DESC) dl
	OUTER APPLY (select TOP 1 * FROM tblDriverCompliance WHERE ComplianceTypeID=2 AND DriverID = DB.ID ORDER BY DocumentDate DESC) cdl
	OUTER APPLY (select TOP 1 * FROM tblDriverCompliance WHERE ComplianceTypeID=3 AND DriverID = DB.ID ORDER BY DocumentDate DESC) dsbc
	OUTER APPLY (select TOP 1 * FROM tblDriverCompliance WHERE ComplianceTypeID=4 AND DriverID = DB.ID ORDER BY DocumentDate DESC) mvr
	OUTER APPLY (select TOP 1 * FROM tblDriverCompliance WHERE ComplianceTypeID=5 AND DriverID = DB.ID ORDER BY DocumentDate DESC) mc
	OUTER APPLY (select TOP 1 * FROM tblDriverCompliance WHERE ComplianceTypeID=6 AND DriverID = DB.ID ORDER BY DocumentDate DESC) lfp
	OUTER APPLY (select TOP 1 * FROM tblDriverCompliance WHERE ComplianceTypeID=7 AND DriverID = DB.ID ORDER BY DocumentDate DESC) ea
	OUTER APPLY (select TOP 1 * FROM tblDriverCompliance WHERE ComplianceTypeID=8 AND DriverID = DB.ID ORDER BY DocumentDate DESC) pmr
	OUTER APPLY (select TOP 1 * FROM tblDriverCompliance WHERE ComplianceTypeID=9 AND DriverID = DB.ID ORDER BY DocumentDate DESC) tr
	OUTER APPLY (select TOP 1 * FROM tblDriverCompliance WHERE ComplianceTypeID=10 AND DriverID = DB.ID ORDER BY DocumentDate DESC) ot
	OUTER APPLY (select TOP 1 * FROM tblDriverCompliance WHERE ComplianceTypeID=11 AND DriverID = DB.ID ORDER BY DocumentDate DESC) dsbr
GO


-- Refresh views / functions / sp's
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRecompileAllStoredProcedures
GO
EXEC _spRebuildAllObjects
GO
EXEC _spRebuildAllObjects
GO


COMMIT
SET NOEXEC OFF