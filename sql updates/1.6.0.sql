DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '1.4.10', @NewVersion = '1.6.0'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

CREATE TABLE tblTruckInspectionType (
  ID int identity(1, 1) not null
, Name varchar(30) not null
, ExpirationMonths int not null -- 0 = no expiration
, Required bit not null 
, CreateDate smalldatetime NULL
, CreatedByUser varchar(100) NULL
, LastChangeDate smalldatetime NULL
, LastChangedByUser varchar(100) NULL
, DeleteDate smalldatetime NULL
, DeletedByUser varchar(100) NULL
, CONSTRAINT PK_TruckInspectionType PRIMARY KEY (ID)
)
GO
GRANT SELECT, INSERT, DELETE, UPDATE ON tblTruckInspectionType TO dispatchcrude_iis_acct
GO
ALTER TABLE tblTruckInspectionType
  ADD CONSTRAINT DF_TruckInspectionType_Required DEFAULT (1) FOR Required
GO

CREATE TABLE tblTruckInspection (
  ID int identity(1, 1) not null
, TruckID int not null CONSTRAINT FK_TruckInspection_Truck REFERENCES tblTruck(ID)
, TruckInspectionTypeID int not null CONSTRAINT FK_TruckInspection_Type REFERENCES tblTruckInspectionType(ID)
, InspectionDate smalldatetime NOT NULL
, ExpirationDate smalldatetime NULL
, Document varbinary(max) NULL
, DocName varchar(255) NULL
, Notes text NULL
, CreateDate smalldatetime NULL
, CreatedByUser varchar(100) NULL
, LastChangeDate smalldatetime NULL
, LastChangedByUser varchar(100) NULL
, CONSTRAINT PK_TruckInspection PRIMARY KEY (ID)
)
GO
GRANT SELECT, INSERT, DELETE, UPDATE ON tblTruckInspection TO dispatchcrude_iis_acct
GO
ALTER TABLE tblTruckInspection
  ADD CONSTRAINT DF_TruckInspection_InspectionDate DEFAULT (getdate()) FOR InspectionDate
GO

CREATE TABLE tblTrailerInspectionType (
  ID int identity(1, 1) not null
, Name varchar(30) not null
, ExpirationMonths int not null -- 0 = no expiration
, Required bit not null 
, CreateDate smalldatetime NULL
, CreatedByUser varchar(100) NULL
, LastChangeDate smalldatetime NULL
, LastChangedByUser varchar(100) NULL
, DeleteDate smalldatetime NULL
, DeletedByUser varchar(100) NULL
, CONSTRAINT PK_TrailerInspectionType PRIMARY KEY (ID)
)
GO
GRANT SELECT, INSERT, DELETE, UPDATE ON tblTrailerInspectionType TO dispatchcrude_iis_acct
GO
ALTER TABLE tblTrailerInspectionType
  ADD CONSTRAINT DF_TrailerInspectionType_Required DEFAULT (1) FOR Required
GO

CREATE TABLE tblTrailerInspection (
  ID int identity(1, 1) not null
, TrailerID int not null CONSTRAINT FK_TrailerInspection_Trailer REFERENCES tblTrailer(ID)
, TrailerInspectionTypeID int not null CONSTRAINT FK_TrailerInspection_Type REFERENCES tblTrailerInspectionType(ID)
, InspectionDate smalldatetime NOT NULL
, ExpirationDate smalldatetime NULL
, Document varbinary(max) NULL
, DocName varchar(255) NULL
, Notes text NULL
, CreateDate smalldatetime NULL
, CreatedByUser varchar(100) NULL
, LastChangeDate smalldatetime NULL
, LastChangedByUser varchar(100) NULL
, CONSTRAINT PK_TrailerInspection PRIMARY KEY (ID)
)
GO
GRANT SELECT, INSERT, DELETE, UPDATE ON tblTrailerInspection TO dispatchcrude_iis_acct
GO
ALTER TABLE tblTrailerInspection
  ADD CONSTRAINT DF_TrailerInspection_InspectionDate DEFAULT (getdate()) FOR InspectionDate
GO

INSERT INTO tblTruckInspectionType (Name, ExpirationMonths, Required, CreateDate, CreatedByUser)
	VALUES ('General Inspection', 12, 1, GETDATE(), 'System')
INSERT INTO tblTruckInspectionType (Name, ExpirationMonths, Required, CreateDate, CreatedByUser)
	VALUES ('DOT Inspection', 12, 1, GETDATE(), 'System')
GO

INSERT INTO tblTruckInspection (TruckID, TruckInspectionTypeID, InspectionDate, ExpirationDate, Document, DocName, CreateDate, CreatedByUser)
	SELECT ID, 1, DATEADD(month, -12, InspectionExpiration), InspectionExpiration, InspectionDocument, InspectionDocName, GETDATE(), 'System'
	FROM tblTruck
	WHERE InspectionDocument IS NOT NULL
INSERT INTO tblTruckInspection (TruckID, TruckInspectionTypeID, InspectionDate, ExpirationDate, Document, DocName, CreateDate, CreatedByUser)
	SELECT ID, 2, DATEADD(month, -12, DOTInspectionExpiration), DOTInspectionExpiration, DOTInspectionDocument, DOTInspectionDocName, GETDATE(), 'System'
	FROM tblTruck
	WHERE DOTInspectionDocument IS NOT NULL
GO
INSERT INTO tblTrailerInspectionType (Name, ExpirationMonths, Required, CreateDate, CreatedByUser)
	VALUES ('General Inspection', 12, 1, GETDATE(), 'System')
INSERT INTO tblTrailerInspectionType (Name, ExpirationMonths, Required, CreateDate, CreatedByUser)
	VALUES ('DOT Inspection', 12, 1, GETDATE(), 'System')
INSERT INTO tblTrailerInspectionType (Name, ExpirationMonths, Required, CreateDate, CreatedByUser)
	VALUES ('Leak Inspection', 12, 1, GETDATE(), 'System')
INSERT INTO tblTrailerInspectionType (Name, ExpirationMonths, Required, CreateDate, CreatedByUser)
	VALUES ('Vapor Inspection', 12, 1, GETDATE(), 'System')
GO
INSERT INTO tblTrailerInspection (TrailerID, TrailerInspectionTypeID, InspectionDate, ExpirationDate, Document, DocName, CreateDate, CreatedByUser)
	SELECT ID, 1, DATEADD(month, -12, InspectionExpiration), InspectionExpiration, InspectionDocument, InspectionDocName, GETDATE(), 'System'
	FROM tblTrailer
	WHERE InspectionDocument IS NOT NULL
INSERT INTO tblTrailerInspection (TrailerID, TrailerInspectionTypeID, InspectionDate, ExpirationDate, Document, DocName, CreateDate, CreatedByUser)
	SELECT ID, 2, DATEADD(month, -12, DOTInspectionExpiration), DOTInspectionExpiration, DOTInspectionDocument, DOTInspectionDocName, GETDATE(), 'System'
	FROM tblTrailer
	WHERE DOTInspectionDocument IS NOT NULL
INSERT INTO tblTrailerInspection (TrailerID, TrailerInspectionTypeID, InspectionDate, ExpirationDate, Document, DocName, CreateDate, CreatedByUser)
	SELECT ID, 3, DATEADD(month, -12, LeakTestExpiration), LeakTestExpiration, LeakTestDocument, LeakTestDocName, GETDATE(), 'System'
	FROM tblTrailer
	WHERE LeakTestDocument IS NOT NULL
INSERT INTO tblTrailerInspection (TrailerID, TrailerInspectionTypeID, InspectionDate, ExpirationDate, Document, DocName, CreateDate, CreatedByUser)
	SELECT ID, 4, DATEADD(month, -12, VaporTestExpiration), VaporTestExpiration, VaporTestDocument, VaporTestDocName, GETDATE(), 'System'
	FROM tblTrailer
	WHERE VaporTestDocument IS NOT NULL
GO

ALTER TABLE tblTruck DROP COLUMN InspectionExpiration
ALTER TABLE tblTruck DROP COLUMN InspectionDocument
ALTER TABLE tblTruck DROP COLUMN InspectionDocName

ALTER TABLE tblTruck DROP COLUMN DOTInspectionExpiration
ALTER TABLE tblTruck DROP COLUMN DOTInspectionDocument
ALTER TABLE tblTruck DROP COLUMN DOTInspectionDocName

ALTER TABLE tblTrailer DROP COLUMN InspectionExpiration
ALTER TABLE tblTrailer DROP COLUMN InspectionDocument
ALTER TABLE tblTrailer DROP COLUMN InspectionDocName

ALTER TABLE tblTrailer DROP COLUMN DOTInspectionExpiration
ALTER TABLE tblTrailer DROP COLUMN DOTInspectionDocument
ALTER TABLE tblTrailer DROP COLUMN DOTInspectionDocName

ALTER TABLE tblTrailer DROP COLUMN LeakTestExpiration
ALTER TABLE tblTrailer DROP COLUMN LeakTestDocument
ALTER TABLE tblTrailer DROP COLUMN LeakTestDocName

ALTER TABLE tblTrailer DROP COLUMN VaporTestExpiration
ALTER TABLE tblTrailer DROP COLUMN VaporTestDocument
ALTER TABLE tblTrailer DROP COLUMN VaporTestDocName
GO

CREATE TABLE tblTruckMaintenance (
  ID int identity(1, 1) not null
, TruckID int not null CONSTRAINT FK_TruckMaintenance_Truck REFERENCES tblTruck(ID)
, MaintenanceDate smalldatetime not null
, Description varchar(255) not null
, Notes text null
, Document varbinary(max) null
, DocName varchar(255) null
, CreateDate smalldatetime NULL
, CreatedByUser varchar(100) NULL
, LastChangeDate smalldatetime NULL
, LastChangedByUser varchar(100) NULL
, CONSTRAINT PK_TruckMaintenance PRIMARY KEY (ID)
)
GO
GRANT SELECT, INSERT, DELETE, UPDATE ON tblTruckMaintenance TO dispatchcrude_iis_acct
GO

CREATE TABLE tblTrailerMaintenance (
  ID int identity(1, 1) not null
, TrailerID int not null CONSTRAINT FK_TrailerMaintenance_Trailer REFERENCES tblTrailer(ID)
, MaintenanceDate smalldatetime not null
, Description varchar(255) not null
, Notes text null
, Document varbinary(max) null
, DocName varchar(255) null
, CreateDate smalldatetime NULL
, CreatedByUser varchar(100) NULL
, LastChangeDate smalldatetime NULL
, LastChangedByUser varchar(100) NULL
, CONSTRAINT PK_TrailerMaintenance PRIMARY KEY (ID)
)
GO
GRANT SELECT, INSERT, DELETE, UPDATE ON tblTrailerMaintenance TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 26 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Truck records with FullName & translated "friendly" values
/***********************************/
ALTER VIEW [dbo].[viewTruck] AS
SELECT T.*
	, T.IDNumber AS FullName
	, isnull(CT.Name, 'Unknown') AS CarrierType
	, C.Name AS Carrier
FROM dbo.tblTruck T
JOIN dbo.tblCarrier C ON C.ID = T.CarrierID
LEFT JOIN dbo.tblCarrierType CT ON CT.ID = C.CarrierTypeID

GO

/***********************************/
-- Date Created: 26 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Trailer records with FullName & translated "friendly" values
/***********************************/
ALTER VIEW [dbo].[viewTrailer] AS
SELECT T.*
	, ISNULL(Compartment1Barrels, 0) 
		+ ISNULL(Compartment2Barrels, 0) 
		+ ISNULL(Compartment3Barrels, 0) 
		+ ISNULL(Compartment4Barrels, 0) 
		+ ISNULL(Compartment5Barrels, 0) AS TotalCapacityBarrels
	, CASE WHEN isnull(Compartment1Barrels, 0) > 0 THEN 1 ELSE 0 END 
		+ CASE WHEN isnull(Compartment2Barrels, 0) > 0 THEN 1 ELSE 0 END 
		+ CASE WHEN isnull(Compartment3Barrels, 0) > 0 THEN 1 ELSE 0 END
		+ CASE WHEN isnull(Compartment4Barrels, 0) > 0 THEN 1 ELSE 0 END 
		+ CASE WHEN isnull(Compartment5Barrels, 0) > 0 THEN 1 ELSE 0 END AS CompartmentCount
	, T.IDNumber AS FullName
	, TT.Name AS TrailerType
	, isnull(CT.Name, 'Unknown') AS CarrierType
	, C.Name AS Carrier
FROM dbo.tblTrailer T
LEFT JOIN dbo.tblTrailerType TT ON TT.ID = T.TrailerTypeID
JOIN dbo.tblCarrier C ON C.ID = T.CarrierID
LEFT JOIN dbo.tblCarrierType CT ON CT.ID = C.CarrierTypeID

GO

EXEC _spRefreshAllViews
GO
--drop view viewTrailerInspection;
/***********************************/
-- Date Created: 5 Aug 2013
-- Author: Kevin Alons
-- Purpose: return Trailer Inspection records with translated "friendly" values + missing required records
/***********************************/
CREATE VIEW viewTrailerInspection AS
SELECT TI.ID
	, T.ID AS TrailerID
	, TIT.ID AS TrailerInspectionTypeID
	, TI.InspectionDate
	, TI.ExpirationDate
	, TI.Document
	, TI.DocName
	, TI.Notes
	, TI.CreateDate
	, TI.CreatedByUser
	, TI.LastChangeDate
	, TI.LastChangedByUser
	, CASE WHEN TI.ExpirationDate IS NULL THEN 'Missing' WHEN TI.ExpirationDate < GETDATE() THEN 'Overdue' ELSE 'Current' END AS Status
	, TIT.Name AS InspectionType
	, TIT.Required
	, TIT.ExpirationMonths
	, T.IdNumber
	, T.VIN
	, T.CarrierID
	, T.Carrier
	, T.CarrierType
	, T.TrailerType
FROM viewTrailer T
CROSS JOIN tblTrailerInspectionType TIT
LEFT JOIN tblTrailerInspection TI ON TI.TrailerID = T.ID AND TI.TrailerInspectionTypeID = TIT.ID
WHERE TIT.Required = 1
  -- include any existing Inspection records or those missing required inspections
  AND (TIT.Required = 1 OR TI.ID IS NOT NULL)
  AND T.DeleteDate IS NULL
  and TIT.DeleteDate IS NULL
GO
GRANT SELECT ON viewTrailerInspection TO dispatchcrude_iis_acct
GO

-- drop view viewTruckInspection;
/***********************************/
-- Date Created: 5 Aug 2013
-- Author: Kevin Alons
-- Purpose: return Truck Inspection records with translated "friendly" values + missing required records
/***********************************/
CREATE VIEW viewTruckInspection AS
SELECT TI.ID
	, T.ID AS TruckID
	, TIT.ID AS TruckInspectionTypeID
	, TI.InspectionDate
	, TI.ExpirationDate
	, TI.Document
	, TI.DocName
	, TI.Notes
	, TI.CreateDate
	, TI.CreatedByUser
	, TI.LastChangeDate
	, TI.LastChangedByUser
	, CASE WHEN TI.ExpirationDate IS NULL THEN 'Missing' WHEN TI.ExpirationDate < GETDATE() THEN 'Overdue' ELSE 'Current' END AS Status
	, TIT.Name AS InspectionType
	, TIT.Required
	, TIT.ExpirationMonths
	, T.IdNumber
	, T.VIN
	, T.CarrierID
	, T.Carrier
	, T.CarrierType
FROM viewTruck T
CROSS JOIN tblTruckInspectionType TIT
LEFT JOIN tblTruckInspection TI ON TI.TruckID = T.ID AND TI.TruckInspectionTypeID = TIT.ID
WHERE TIT.Required = 1
  -- include any existing Inspection records or those missing required inspections
  AND (TIT.Required = 1 OR TI.ID IS NOT NULL)
  AND T.DeleteDate IS NULL
  AND TIT.DeleteDate IS NULL
GO
GRANT SELECT ON viewTruckInspection TO dispatchcrude_iis_acct
GO

-- drop view viewTrailerMaintenance
/***********************************/
-- Date Created: 8 Aug 2013
-- Author: Kevin Alons
-- Purpose: return Trailer Maintenance records with translated "friendly" values
/***********************************/
CREATE VIEW viewTrailerMaintenance AS
SELECT TM.ID
	, T.ID AS TrailerID
	, TM.MaintenanceDate
	, TM.Description
	, TM.Document
	, TM.DocName
	, TM.Notes
	, TM.CreateDate
	, TM.CreatedByUser
	, TM.LastChangeDate
	, TM.LastChangedByUser
	, T.IdNumber
	, T.VIN
	, T.CarrierID
	, T.Carrier
	, T.CarrierType
	, T.TrailerType
FROM viewTrailer T
JOIN tblTrailerMaintenance TM ON TM.TrailerID = T.ID
WHERE T.DeleteDate IS NULL
GO
GRANT SELECT ON viewTrailerMaintenance TO dispatchcrude_iis_acct
GO

-- drop view viewTruckMaintenance
/***********************************/
-- Date Created: 8 Aug 2013
-- Author: Kevin Alons
-- Purpose: return Truck Maintenance records with translated "friendly" values
/***********************************/
CREATE VIEW viewTruckMaintenance AS
SELECT TM.ID
	, T.ID AS TruckID
	, TM.MaintenanceDate
	, TM.Description
	, TM.Document
	, TM.DocName
	, TM.Notes
	, TM.CreateDate
	, TM.CreatedByUser
	, TM.LastChangeDate
	, TM.LastChangedByUser
	, T.IdNumber
	, T.VIN
	, T.CarrierID
	, T.Carrier
	, T.CarrierType
FROM viewTruck T
JOIN tblTruckMaintenance TM ON TM.TruckID = T.ID
WHERE T.DeleteDate IS NULL
GO
GRANT SELECT ON viewTruckMaintenance TO dispatchcrude_iis_acct
GO

COMMIT
SET NOEXEC OFF