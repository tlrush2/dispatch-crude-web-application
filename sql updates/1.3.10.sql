DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '1.3.9', @NewVersion = '1.3.10'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

CREATE TABLE tblWaitFeeSubUnit
(
  ID int NOT NULL 
, Name varchar(25)
)
GO
ALTER TABLE [dbo].[tblWaitFeeSubUnit] ADD CONSTRAINT PK_WaitFeeSubUnit PRIMARY KEY CLUSTERED
(
	[ID] ASC
)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

INSERT INTO tblWaitFeeSubUnit VALUES (0, 'None')
INSERT INTO tblWaitFeeSubUnit VALUES (5, '5')
INSERT INTO tblWaitFeeSubUnit VALUES (10, '10')
INSERT INTO tblWaitFeeSubUnit VALUES (15, '15')
INSERT INTO tblWaitFeeSubUnit VALUES (20, '20')
INSERT INTO tblWaitFeeSubUnit VALUES (30, '30')
INSERT INTO tblWaitFeeSubUnit VALUES (60, '60')
GO

CREATE TABLE tblWaitFeeRoundingType
(
  ID int NOT NULL
, Name varchar(25)
)
GO
ALTER TABLE [dbo].[tblWaitFeeRoundingType] ADD CONSTRAINT PK_WaitFeeRoundingType PRIMARY KEY CLUSTERED
(
	[ID] ASC
)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

INSERT INTO tblWaitFeeRoundingType VALUES (1, 'Floor')
INSERT INTO tblWaitFeeRoundingType VALUES (2, 'Round Down')
INSERT INTO tblWaitFeeRoundingType VALUES (3, 'Round Up')
INSERT INTO tblWaitFeeRoundingType VALUES (4, 'Ceiling')
GO

ALTER TABLE tblCarrierRates ADD WaitFeeSubUnitID int NULL
ALTER TABLE tblCarrierRates ADD CONSTRAINT
	FK_Carrier_WaitFeeSubUnit FOREIGN KEY
	(
		WaitFeeSubUnitID
	) REFERENCES dbo.tblWaitFeeSubUnit
	(
		ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
UPDATE tblCarrierRates SET WaitFeeSubUnitID = 30

ALTER TABLE tblCarrierRates ADD WaitFeeRoundingTypeID int NULL
GO
ALTER TABLE tblCarrierRates ADD CONSTRAINT
	FK_Carrier_WaitFeeRoundingType FOREIGN KEY
	(
		WaitFeeRoundingTypeID
	) REFERENCES dbo.tblWaitFeeRoundingType
	(
		ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
UPDATE tblCarrierRates SET WaitFeeRoundingTypeID = 4
GO

ALTER TABLE tblCustomerRates ADD WaitFeeSubUnitID int NULL
ALTER TABLE tblCustomerRates ADD CONSTRAINT
	FK_Customer_WaitFeeSubUnit FOREIGN KEY
	(
		WaitFeeSubUnitID
	) REFERENCES dbo.tblWaitFeeSubUnit
	(
		ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
UPDATE tblCustomerRates SET WaitFeeSubUnitID = 30

ALTER TABLE tblCustomerRates ADD WaitFeeRoundingTypeID int NULL
GO
ALTER TABLE tblCustomerRates ADD CONSTRAINT
	FK_Customer_WaitFeeRoundingType FOREIGN KEY
	(
		WaitFeeRoundingTypeID
	) REFERENCES dbo.tblWaitFeeRoundingType
	(
		ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
UPDATE tblCustomerRates SET WaitFeeRoundingTypeID = 4
GO

/***********************************************************/
--  Date Created: 22 June 2013
--  Author: Kevin Alons
--  Purpose: Round the Billable Wait minutes into a decimal hour value (based on rounding parameters)
/***********************************************************/
CREATE FUNCTION [dbo].[fnComputeBillableWaitHours](@min int, @SubUnitID int, @RoundingTypeID int) RETURNS decimal (9,3) AS
BEGIN
	DECLARE @ret decimal(9,3) 
	SELECT @ret = @min
	
	IF (@SubUnitID <> 0)
	BEGIN
		SELECT @ret = @ret / @SubUnitID
		IF (@RoundingTypeID = 1)
			SELECT @ret = floor(@ret)
		ELSE IF (@RoundingTypeID = 2)
			SELECT @ret = round(@ret - 0.01, 0)
		ELSE IF (@RoundingTypeID = 3)
			SELECT @ret = round(@ret, 0)
		ELSE IF (@RoundingTypeID = 4)
			SELECT @ret = ceiling(@ret)
		ELSE
			SELECT @ret = 0
	END
	
	RETURN @ret / CASE WHEN @SubUnitID = 0 THEN 1 ELSE (60.0 / @SubUnitID) END
END
GO

GRANT EXECUTE ON fnComputeBillableWaitHours TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Carrier "Settlement" $$ values to an Audited order
/***********************************/
ALTER PROCEDURE [dbo].[spProcessCarrierInvoice]
(
  @ID int
, @UserName varchar(100)
, @ChainupFee money = null
, @RerouteFee money = null
, @WaitFee money = null
, @RejectionFee money = null
, @LoadFee money = null
) AS BEGIN
	EXEC dbo.spSyncProducer @ID
	EXEC dbo.spSyncActualMiles @ID
	
	DELETE FROM tblOrderInvoiceCarrier WHERE OrderID = @ID
	
	INSERT INTO tblOrderInvoiceCarrier (OrderID, ChainupFee, RerouteFee, BillableWaitMinutes, WaitRate, WaitFee, RejectionFee, RouteRate, LoadFee, TotalFee, CreateDate, CreatedByUser)
	SELECT D.ID, ChainupFee, RerouteFee, BillableWaitMinutes, WaitRate, WaitFee, RejectionFee, RouteRate, LoadFee
		, ChainupFee + RerouteFee + WaitFee + LoadFee AS TotalFee
		, GETDATE(), @UserName
	FROM (
		SELECT ID
			, isnull(Chainup, 0) * coalesce(@ChainupFee, ChainupFee, 0) AS ChainupFee
			, coalesce(@RerouteFee, RerouteFee * RerouteCount, 0) AS RerouteFee
			, cast(BillableWaitHours * 60 as int) AS BillableWaitMinutes
			, WaitFee AS WaitRate
			, coalesce(@WaitFee, BillableWaitHours * WaitFee, 0) AS WaitFee
			, coalesce(@RejectionFee, Rejected * RejectionFee, 0) AS RejectionFee
			, coalesce(@LoadFee, Rate) AS RouteRate
			, coalesce(@LoadFee, dbo.fnMaxDecimal(MinSettlementBarrels, ActualBarrels) * Rate, 0) AS LoadFee
		FROM (
			SELECT S.ID
				, S.ChainUp
				, CR.ChainupFee
				, CR.RerouteFee
				, S.RerouteCount
				, S.TotalWaitMinutes
				, CR.WaitFeeSubUnitID
				, CR.WaitFeeRoundingTypeID
				, CR.WaitFee
				, S.Rejected
				, CR.RejectionFee
				, CRR.Rate
				, S.MinSettlementBarrels
				, S.ActualBarrels
				, dbo.fnComputeBillableWaitHours(S.TotalWaitMinutes, CR.WaitFeeSubUnitID, CR.WaitFeeRoundingTypeID) AS BillableWaitHours
			FROM (
				SELECT O.ID
					, O.CarrierID
					, O.RouteID
					, O.ChainUp
					, CASE WHEN C.SettlementFactorID = 1 THEN O.OriginGrossBarrels ELSE O.OriginNetBarrels END AS ActualBarrels
					, O.RerouteCount
					, O.TotalWaitMinutes
					, O.Rejected
					, coalesce(CRXX.ID, CRXA.ID, CRAX.ID, CRAA.ID) AS CRID
					, isnull(C.MinSettlementBarrels, 0) AS MinSettlementBarrels
				FROM dbo.viewOrderExportFull O
				JOIN tblOrigin OO ON OO.ID = O.OriginID
				JOIN tblCarrier C ON C.ID = O.CarrierID
				LEFT JOIN tblCarrierRates CRXX ON CRXX.CarrierID = O.CarrierID AND CRXX.RegionID = OO.RegionID
				LEFT JOIN tblCarrierRates CRXA ON CRXA.CarrierID = O.CarrierID AND CRXA.RegionID = -1
				LEFT JOIN tblCarrierRates CRAX ON CRAX.CarrierID = -1 AND CRAX.RegionID = OO.RegionID
				LEFT JOIN tblCarrierRates CRAA ON CRAA.CarrierID = -1 AND CRAA.RegionID = -1
				WHERE O.ID = @ID
			) S
			LEFT JOIN tblCarrierRates CR ON CR.ID = S.CRID
			LEFT JOIN tblCarrierRouteRates CRR ON CRR.CarrierID = S.CarrierID AND CRR.RouteID = S.RouteID 
		) SS
	) D
END

GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Customer "Settlement" $$ values to an Audited order
/***********************************/
ALTER PROCEDURE [dbo].[spProcessCustomerInvoice]
(
  @ID int
, @UserName varchar(100)
, @ChainupFee money = null
, @RerouteFee money = null
, @WaitFee money = null
, @RejectionFee money = null
, @LoadFee money = null
) AS BEGIN
	EXEC dbo.spSyncProducer @ID
	EXEC dbo.spSyncActualMiles @ID
	
	DELETE FROM tblOrderInvoiceCustomer WHERE OrderID = @ID
	INSERT INTO tblOrderInvoiceCustomer (OrderID, ChainupFee, RerouteFee, BillableWaitMinutes, WaitRate, WaitFee, RejectionFee, RouteRate, LoadFee, TotalFee, CreateDate, CreatedByUser)
	SELECT D.ID, ChainupFee, RerouteFee, BillableWaitMinutes, WaitRate, WaitFee, RejectionFee, RouteRate, LoadFee
		, ChainupFee + RerouteFee + WaitFee + LoadFee AS TotalFee
		, GETDATE(), @UserName
	FROM (
		SELECT ID
			, isnull(Chainup, 0) * coalesce(@ChainupFee, ChainupFee, 0) AS ChainupFee
			, coalesce(@RerouteFee, RerouteFee * RerouteCount, 0) AS RerouteFee
			, cast(BillableWaitHours * 60 as int) AS BillableWaitMinutes
			, WaitFee AS WaitRate
			, coalesce(@WaitFee, BillableWaitHours * WaitFee, 0) AS WaitFee
			, coalesce(@RejectionFee, Rejected * RejectionFee, 0) AS RejectionFee
			, coalesce(@LoadFee, Rate) AS RouteRate
			, coalesce(@LoadFee, dbo.fnMaxDecimal(MinSettlementBarrels, ActualBarrels) * Rate, 0) AS LoadFee
		FROM (
			SELECT S.ID
				, S.ChainUp
				, CR.ChainupFee
				, CR.RerouteFee
				, S.RerouteCount
				, S.TotalWaitMinutes
				, CR.WaitFeeSubUnitID
				, CR.WaitFeeRoundingTypeID
				, CR.WaitFee
				, S.Rejected
				, CR.RejectionFee
				, CRR.Rate
				, S.MinSettlementBarrels
				, S.ActualBarrels
				, dbo.fnComputeBillableWaitHours(S.TotalWaitMinutes, CR.WaitFeeSubUnitID, CR.WaitFeeRoundingTypeID) AS BillableWaitHours
			FROM (
				SELECT O.ID
					, O.CustomerID
					, O.RouteID
					, O.ChainUp
					, CASE WHEN C.SettlementFactorID = 1 THEN O.OriginGrossBarrels ELSE O.OriginNetBarrels END AS ActualBarrels
					, O.RerouteCount
					, O.TotalWaitMinutes
					, O.Rejected
					, coalesce(CRXX.ID, CRXA.ID, CRAX.ID, CRAA.ID) AS CRID
					, isnull(C.MinSettlementBarrels, 0) AS MinSettlementBarrels
				FROM dbo.viewOrderExportFull O
				JOIN tblOrigin OO ON OO.ID = O.OriginID
				JOIN tblCustomer C ON C.ID = O.CustomerID
				LEFT JOIN tblCustomerRates CRXX ON CRXX.CustomerID = O.CustomerID AND CRXX.RegionID = OO.RegionID
				LEFT JOIN tblCustomerRates CRXA ON CRXA.CustomerID = O.CustomerID AND CRXA.RegionID = -1
				LEFT JOIN tblCustomerRates CRAX ON CRAX.CustomerID = -1 AND CRAX.RegionID = OO.RegionID
				LEFT JOIN tblCustomerRates CRAA ON CRAA.CustomerID = -1 AND CRAA.RegionID = -1
				WHERE O.ID = @ID
			) S
			LEFT JOIN tblCustomerRates CR ON CR.ID = S.CRID
			LEFT JOIN tblCustomerRouteRates CRR ON CRR.CustomerID = S.CustomerID AND CRR.RouteID = S.RouteID 
		) SS
	) D
END

GO

EXEC _spRefreshAllViews
EXEC _spRecompileAllStoredProcedures
GO

COMMIT
SET NOEXEC OFF