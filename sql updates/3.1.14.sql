-- rollback
-- select * from tblsetting where id = 0 
/* 
	-- add support for orphaned Orders (dispatched orders reassigned to a different driver)
*/ 
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.1.13'
SELECT  @NewVersion = '3.1.14'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

ALTER TABLE tblOrder ADD ReassignKey int NULL 
GO
ALTER TABLE tblOrderDbAudit ADD ReassignKey int null
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 19 Dec 2012
-- Description:	trigger to 
--					1) validate any changes, and fail the update if invalid changes are submitted
--					2) add a unique, incrementing OrderNum to each new Order (manual Identity column)
--					3) recompute wait times (origin|destination based on times provided)
--					4) generate route table entry for any newly used origin-destination combination
--					5) keep Order consistent with Origin.Producer|Operator|UOM & Dest.UOM then the Origin|Dest change
--					6) roll the PrintStatus back to NOTFINALIZED (0) when status is rolled back
--					7) when DriverID changes, mark those orders AS DELETED and clone the order with the new DriverID and associated records
--					8) update any ticket quantities for open orders when the UOM is changed for the Origin
--					9) ensure the LastChangeDateUTC is updated for any change to the order
--					10) if DBAudit is turned on, save an audit record for this Order change
-- =============================================
ALTER TRIGGER [dbo].[trigOrder_IU] ON [dbo].[tblOrder] AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @changesFound bit
	
		-- Note: the TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU') ) = 1 statement prevents recursion
	IF (TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU')) = 1 AND TRIGGER_NESTLEVEL( OBJECT_ID('trigOrderTicket_IU')) = 0) BEGIN

		/**********  START OF VALIDATION SECTION ************************/

		IF (UPDATE(OrderNum) 
			OR UPDATE(PriorityID) 
			OR UPDATE(DueDate) 
			OR UPDATE(RouteID) 
			OR UPDATE(OriginID) 
			OR UPDATE(OriginArriveTimeUTC) 
			OR UPDATE(OriginDepartTimeUTC) 
			OR UPDATE(OriginMinutes) 
			OR UPDATE(OriginWaitNotes) 
			OR UPDATE(OriginBOLNum) 
			OR UPDATE(OriginGrossUnits) 
			OR UPDATE(OriginNetUnits) 
			OR UPDATE(DestinationID) 
			OR UPDATE(DestArriveTimeUTC) 
			OR UPDATE(DestDepartTimeUTC) 
			OR UPDATE(DestMinutes) 
			OR UPDATE(DestWaitNotes) 
			OR UPDATE(DestBOLNum) 
			OR UPDATE(DestGrossUnits) 
			OR UPDATE(DestNetUnits) 
			OR UPDATE(CustomerID) 
			OR UPDATE(CarrierID) 
			OR UPDATE(DriverID) 
			OR UPDATE(TruckID)
			OR UPDATE(TrailerID) 
			OR UPDATE(Trailer2ID) 
			OR UPDATE(OperatorID) 
			OR UPDATE(PumperID) 
			OR UPDATE(TicketTypeID) 
			OR UPDATE(Rejected) 
			OR UPDATE(RejectNotes) 
			OR UPDATE(ChainUp) 
			OR UPDATE(OriginTruckMileage) 
			OR UPDATE(OriginTankNum) 
			OR UPDATE(DestTruckMileage) 
			OR UPDATE(CarrierTicketNum) 
			OR UPDATE(AuditNotes) 
			OR UPDATE(CreateDateUTC) 
			-- it is permissible to change the ActualMiles value on Audited orders up to the point the Order has been Shipper-Settled
			OR (UPDATE(ActualMiles) AND EXISTS (SELECT * FROM deleted i JOIN tblOrderInvoiceCustomer OIC ON OIC.OrderID = i.ID WHERE OIC.BatchID IS NOT NULL))
			OR UPDATE(ProducerID) 
			OR UPDATE(CreatedByUser) 
			OR UPDATE(LastChangeDateUTC) 
			OR UPDATE(LastChangedByUser) 
			OR UPDATE(DeleteDateUTC) 
			OR UPDATE(DeletedByUser) 
			OR UPDATE(DestProductBSW)
			OR UPDATE(DestProductGravity) 
			OR UPDATE(DestProductTemp) 
			OR UPDATE(ProductID) 
			OR UPDATE(AcceptLastChangeDateUTC) 
			OR UPDATE(PickupLastChangeDateUTC) 
			OR UPDATE(DeliverLastChangeDateUTC) 
			OR UPDATE(OriginUomID) 
			OR UPDATE(DestUomID) 
			OR UPDATE(PickupPrintStatusID) 
			OR UPDATE(DeliverPrintStatusID)
			OR UPDATE(PickupPrintDateUTC) 
			OR UPDATE(DeliverPrintDateUTC) 
			OR UPDATE(OriginTankID) 
			OR UPDATE(OriginGrossStdUnits) 
			OR UPDATE(DispatchConfirmNum) 
			OR UPDATE(DispatchNotes) 
			OR UPDATE(DriverNotes) 
			OR UPDATE(OriginWaitReasonID) 
			OR UPDATE(DestWaitReasonID) 
			OR UPDATE(RejectReasonID) 
			OR UPDATE(DestOpenMeterUnits) 
			OR UPDATE(DestCloseMeterUnits))
			--OR UPDATE(ReassignKey)) we don't do any work if this value changes so just ignore it
		BEGIN
			-- only allow the StatusID value to be changed on an audited order
			IF EXISTS (SELECT * FROM deleted WHERE StatusID = 4)
			BEGIN				
				RAISERROR('AUDITED orders cannot be modified!', 16, 1)
				IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
				RETURN
			END
		END
		ELSE -- NO CHANGES DETECTED SO JUST QUIT
			RETURN

		PRINT 'trigOrder_IU FIRED'
					
		-- ensure the Origin and Destinations are both specified unless the Status is:
		--   (Generated, Assigned, Dispatched or Declined)
		IF  EXISTS(SELECT * FROM inserted O WHERE (O.OriginID IS NULL OR O.DestinationID IS NULL) AND O.StatusID NOT IN (-10,1,2,9))
		BEGIN
			RAISERROR('Invalid Order Status when the Origin and/or Or Destination is not specified', 16, 1)
			IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
			RETURN
		END
		/**********  END OF VALIDATION SECTION ************************/

		WHILE (SELECT count(*) FROM inserted i JOIN tblOrder O ON O.ID = i.ID WHERE O.OrderNum IS NULL) > 0
		BEGIN
			UPDATE tblOrder 
			  SET OrderNum = isnull((SELECT max(OrderNum) FROM tblOrder), 100000) + 1
				, CreateDateUTC = getutcdate()
			WHERE ID = (SELECT min(O.ID) FROM tblOrder O JOIN inserted i ON i.ID = O.ID WHERE O.OrderNum IS NULL)
		END
		
		-- re-compute the OriginMinutes (in case the website failed to compute it properly)
		IF UPDATE(OriginArriveTimeUTC) OR UPDATE(OriginDepartTimeUTC)
		BEGIN
			UPDATE tblOrder SET OriginMinutes = datediff(minute, i.OriginArriveTimeUTC, i.OriginDepartTimeUTC)
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
		END
		-- re-compute the DestWaitMinutes (in case the website failed to compute it properly)
		IF UPDATE(DestArriveTimeUTC) OR UPDATE(DestDepartTimeUTC)
		BEGIN
			UPDATE tblOrder SET DestMinutes = datediff(minute, i.DestArriveTimeUTC, i.DestDepartTimeUTC)
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
		END
		
		-- ensure missing Routes are created and assigned for the specified Origin/Destination combination
		IF UPDATE(OriginID) OR UPDATE(DestinationID)
		BEGIN
			-- create any missing Route records
			INSERT INTO tblRoute (OriginID, DestinationID, CreateDateUTC, CreatedByUser)
				SELECT i.OriginID, i.DestinationID, GETUTCDATE(), ISNULL(i.LastChangedByUser, i.CreatedByUser)
				FROM inserted i
				LEFT JOIN tblRoute R ON R.OriginID = i.OriginID AND R.DestinationID = i.DestinationID
				WHERE i.OriginID IS NOT NULL AND i.DestinationID IS NOT NULL AND R.ID IS NULL
			-- ensure the Order records refer to the correct Route (ID)
			UPDATE tblOrder SET RouteID = R.ID
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
			LEFT JOIN tblRoute R ON R.OriginID = O.OriginID AND R.DestinationID = O.DestinationID

			-- update the ActualMiles from the related Route
			UPDATE tblOrder SET ActualMiles = R.ActualMiles
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
			JOIN tblRoute R ON R.ID = O.RouteID
		END
		
		IF (UPDATE(OriginID))
		BEGIN
			-- update Order.ProducerID to match what is assigned to the new Origin
			UPDATE tblOrder SET ProducerID = OO.ProducerID
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			WHERE d.OriginID <> O.OriginID

			-- update Order.OriginUomID to match what is assigned to the new Origin (quantities are updated below)
			UPDATE tblOrder SET OriginUomID = OO.UomID
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			WHERE d.OriginID <> O.OriginID AND d.OriginUomID <> OO.UomID
		END
		
		-- keep the DestUomID in sync with the Destination (units are updated below)
		IF (UPDATE(DestinationID))
		BEGIN
			-- update Order.DestUomID to match what is assigned to the new Destination
			UPDATE tblOrder 
			  SET DestUomID = DD.UomID
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID
			JOIN tblDestination DD ON DD.ID = O.DestinationID
			WHERE d.DestinationID <> O.DestinationID AND d.DestUomID <> DD.UomID
		END
		
		-- ensure that any set PrintStatusID values are reset to 0 when backing up the general StatusID value
		IF (UPDATE(StatusID))
		BEGIN
			UPDATE tblOrder 
			  SET DeliverPrintStatusID = 0 
			  FROM tblOrder O
			  JOIN deleted d ON d.ID = O.ID
			WHERE O.StatusID <> d.StatusID AND O.StatusID IN (-10, 1, 2, 7, 8) -- Generated, Assigned, Dispatched, Accepted, Picked-Up

			UPDATE tblOrder 
			  SET PickupPrintStatusID = 0 
			  FROM tblOrder O
			  JOIN deleted d ON d.ID = O.ID
			WHERE O.StatusID <> d.StatusID AND O.StatusID IN (-10, 1, 2, 7) -- Generated, Assigned, Dispatched, Accepted
		END

		-- just updating the tickets here, trigOrderTicket_IU will in turn update this table
		-- ensure any existing OrderTickets are recomputed with the new OriginUOM
		UPDATE tblOrderTicket
		  SET GrossUnits = dbo.fnConvertUOM(GrossUnits, d.OriginUomID, O.OriginUomID)
			, NetUnits = dbo.fnConvertUOM(NetUnits, d.OriginUomID, O.OriginUomID)
		FROM tblOrderTicket OT
		JOIN tblOrder O ON O.ID = OT.OrderID
		JOIN deleted d ON d.ID = O.ID
		WHERE O.OriginUomID <> d.OriginUomID
		
		-- recalculate the Destination Unit values if only if the DestUomID changed but the values didn't
		UPDATE tblOrder
		  SET DestGrossUnits = dbo.fnConvertUOM(O.DestGrossUnits, d.DestUomID, O.DestUomID)
			, DestNetUnits = dbo.fnConvertUOM(O.DestNetUnits, d.DestUomID, O.DestUomID)
		FROM tblOrder O
		JOIN deleted d ON d.ID = O.ID
		WHERE d.DestUomID <> O.DestUomID 
		  AND d.DestGrossUnits = O.DestGrossUnits
		  AND d.DestNetUnits = O.DestNetUnits

		/*************************************************************************************************************/
		/* handle driver changes - "orphan" the order/ticket (with a new ORDERNUM) - and clone it for the new driver */
		IF (UPDATE(DriverID))
		BEGIN
			-- create cloned order/ticket records (these records will be cloned then the original reset to orphaned status)
			SELECT O.*, NewOrderNum = CAST(NULL as int)
			INTO #clone
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID AND d.DriverID IS NOT NULL AND d.DriverID <> isnull(O.DriverID, 0)

			/* generate the next OrderNum values for these new records */
			DECLARE @newOrderNum int; SET @newOrderNum = isnull((SELECT max(OrderNum) FROM tblOrder), 100000) + 1;
			DECLARE @newReassignKey int; SET @newReassignKey = isnull((SELECT max(ReassignKey) FROM tblOrder), 0) + 1;
			WHILE (SELECT count(*) FROM #clone WHERE NewOrderNum IS NULL) > 0
			BEGIN
				UPDATE #clone
				  SET NewOrderNum = @newOrderNum, ReassignKey = isnull(ReassignKey, @newReassignKey)
					, CreateDateUTC = getutcdate()
				WHERE ID = (SELECT min(ID) FROM #clone WHERE NewOrderNum IS NULL)
				-- if the @newReassignKey wasn't used above, then that # will be skipped, but this should not be an issue
				SELECT @newOrderNum = @newOrderNum + 1, @newReassignKey = @newReassignKey + 1
			END
			
			-- reset the existing records as orphaned
			UPDATE tblOrder
				SET DeleteDateUTC = isnull(O.DeleteDateUTC, GETUTCDATE()), DeletedByUser = isnull(O.DeletedByUser, O.LastChangedByUser)
					, CarrierID = d.CarrierID, DriverID = d.DriverID, TruckID = d.TruckID, TrailerID = d.TrailerID, Trailer2ID = d.Trailer2ID
					, DispatchConfirmNum = NULL
					, ReassignKey = C.ReassignKey
			FROM tblOrder O
			JOIN #clone C ON C.ID = O.ID
			JOIN deleted d on D.ID = O.id

			-- create the cloned records (which is essentially the original with a new ID and the Carrrier & default Truck|Trailers of the new driver)
			INSERT INTO tblOrder (OrderNum, [StatusID], [PriorityID], [DueDate], [RouteID], [OriginID], [OriginArriveTimeUTC], [OriginDepartTimeUTC], [OriginMinutes], [OriginWaitNotes], [OriginBOLNum], [OriginGrossUnits], [OriginNetUnits], [DestinationID], [DestArriveTimeUTC], [DestDepartTimeUTC], [DestMinutes], [DestWaitNotes], [DestBOLNum], [DestGrossUnits], [DestNetUnits], [CustomerID], [CarrierID], [DriverID], [TruckID], [TrailerID], [Trailer2ID], [OperatorID], [PumperID], [TicketTypeID], [Rejected], [RejectNotes], [ChainUp], [OriginTruckMileage], [OriginTankNum], [DestTruckMileage], [CarrierTicketNum], [AuditNotes], [CreateDateUTC], [ActualMiles], [ProducerID], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser], [DestProductBSW], [DestProductGravity], [DestProductTemp], [ProductID], [AcceptLastChangeDateUTC], [PickupLastChangeDateUTC], [DeliverLastChangeDateUTC], [OriginUomID], [DestUomID], [PickupPrintStatusID], [DeliverPrintStatusID], [PickupPrintDateUTC], [DeliverPrintDateUTC], [OriginTankID], [OriginGrossStdUnits], [DispatchNotes], [DriverNotes], [DispatchConfirmNum], [OriginWaitReasonID], [DestWaitReasonID], [RejectReasonID], [DestOpenMeterUnits], [DestCloseMeterUnits], [ReassignKey])
				SELECT NewOrderNum, [StatusID], [PriorityID], [DueDate], [RouteID], [OriginID], [OriginArriveTimeUTC], [OriginDepartTimeUTC], [OriginMinutes], [OriginWaitNotes], [OriginBOLNum], [OriginGrossUnits], [OriginNetUnits], [DestinationID], [DestArriveTimeUTC], [DestDepartTimeUTC], [DestMinutes], [DestWaitNotes], [DestBOLNum], [DestGrossUnits], [DestNetUnits], [CustomerID], D.[CarrierID], [DriverID], D.TruckID, D.TrailerID, D.Trailer2ID, [OperatorID], [PumperID], [TicketTypeID], [Rejected], [RejectNotes], [ChainUp], [OriginTruckMileage], [OriginTankNum], [DestTruckMileage], [CarrierTicketNum], [AuditNotes], O.CreateDateUTC, [ActualMiles], [ProducerID], O.CreatedByUser, O.LastChangeDateUTC, O.LastChangedByUser, O.DeleteDateUTC, O.DeletedByUser, [DestProductBSW], [DestProductGravity], [DestProductTemp], [ProductID], [AcceptLastChangeDateUTC], [PickupLastChangeDateUTC], [DeliverLastChangeDateUTC], [OriginUomID], [DestUomID], [PickupPrintStatusID], [DeliverPrintStatusID], [PickupPrintDateUTC], [DeliverPrintDateUTC], [OriginTankID], [OriginGrossStdUnits], [DispatchNotes], [DriverNotes], DispatchConfirmNum, [OriginWaitReasonID], [DestWaitReasonID], [RejectReasonID], [DestOpenMeterUnits], [DestCloseMeterUnits], ReassignKey
				FROM #clone O
				LEFT JOIN tblDriver D ON D.ID = O.DriverID
		
			-- cache the affected tickets
			SELECT OT.*, C.NewOrderNum, OrderDeletedByUser = C.LastChangedByUser
			INTO #cloneTicket
			FROM tblOrderTicket OT
			JOIN #clone C ON C.ID = OT.OrderID

			/*			
			-- mark the existing related tickets as deleted
			UPDATE tblOrderTicket
				SET DeleteDateUTC = isnull(OT.DeleteDateUTC, GETUTCDATE()), DeletedByUser = isnull(OT.DeletedByUser, CT.OrderDeletedByUser)
			FROM tblOrderTicket OT
			JOIN #cloneTicket CT ON CT.ID = OT.ID
			*/
			
			-- create the cloned Order Ticket records (copy of existing ticket(s) pointed at new "original" Order)
			INSERT INTO tblOrderTicket (OrderID, CarrierTicketNum, TicketTypeID, TankNum, ProductObsGravity, ProductObsTemp, ProductBSW, OpeningGaugeFeet, OpeningGaugeInch, OpeningGaugeQ, ClosingGaugeFeet, ClosingGaugeInch, ClosingGaugeQ
					, GrossUnits, NetUnits, Rejected, RejectNotes, SealOff, SealOn, BOLNum, ProductHighTemp, ProductLowTemp, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser
					, UID, FromMobileApp, OriginTankID, BottomFeet, BottomInches, BottomQ, GrossStdUnits, RejectReasonID, MeterFactor, OpenMeterUnits, CloseMeterUnits)
				SELECT O.ID, CT.CarrierTicketNum, CT.TicketTypeID, CT.TankNum, CT.ProductObsGravity, CT.ProductObsTemp, CT.ProductBSW, CT.OpeningGaugeFeet, CT.OpeningGaugeInch, CT.OpeningGaugeQ, CT.ClosingGaugeFeet, CT.ClosingGaugeInch, CT.ClosingGaugeQ
					, CT.GrossUnits, CT.NetUnits, CT.Rejected, CT.RejectNotes, CT.SealOff, CT.SealOn, CT.BOLNum, CT.ProductHighTemp, CT.ProductLowTemp, CT.CreateDateUTC, CT.CreatedByUser, CT.LastChangeDateUTC, CT.LastChangedByUser, CT.DeleteDateUTC, CT.DeletedByUser
					, newid(), CT.FromMobileApp, CT.OriginTankID, CT.BottomFeet, CT.BottomInches, CT.BottomQ, CT.GrossStdUnits, CT.RejectReasonID, CT.MeterFactor, CT.OpenMeterUnits, CT.CloseMeterUnits
				FROM #cloneTicket CT
				JOIN tblOrder O ON O.OrderNum = CT.NewOrderNum
		END
		/*************************************************************************************************************/
		
		--NOTE: we do not update the DestOpenMeterUnits/DestCloseMeterUnits since they don't auto update
		--  (and they simply represent a reading that is assumed to in the DestUomID UOM at entry)
		--  (if they are updated in the future [manually] the will calculate again using the DestUomID at that time)

		IF EXISTS (SELECT * FROM inserted EXCEPT SELECT * FROM deleted)
		BEGIN
			/*
			-- ensure that all changed orders have their LastChangeDateUTC set to UTCNOW
			UPDATE tblOrder
			  SET LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = ISNULL(O.LastChangedByUser, 'System')
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
			JOIN deleted d ON d.ID = O.id
			-- ensure a lastchangedateutc value is set (and updated if it isn't close to NOW and was explicitly changed by the callee)
			WHERE O.LastChangeDateUTC IS NULL 
				--
				OR (i.LastChangeDateUTC = d.LastChangeDateUTC AND abs(datediff(second, O.LastChangeDateUTC, GETUTCDATE())) > 2)
			*/
			
			-- optionally add tblOrderDBAudit records
			BEGIN TRY
				IF EXISTS(SELECT * FROM tblSetting WHERE ID = 32 AND (Value LIKE 'true' OR Value LIKE 'yes'))
					INSERT INTO tblOrderDbAudit (DBAuditDate, [ID], [OrderNum], [StatusID], [PriorityID], [DueDate], [RouteID], [OriginID], [OriginArriveTimeUTC], [OriginDepartTimeUTC], [OriginMinutes], [OriginWaitNotes], [OriginBOLNum], [OriginGrossUnits], [OriginNetUnits], [DestinationID], [DestArriveTimeUTC], [DestDepartTimeUTC], [DestMinutes], [DestWaitNotes], [DestBOLNum], [DestGrossUnits], [DestNetUnits], [CustomerID], [CarrierID], [DriverID], [TruckID], [TrailerID], [Trailer2ID], [OperatorID], [PumperID], [TicketTypeID], [Rejected], [RejectNotes], [ChainUp], [OriginTruckMileage], [OriginTankNum], [DestTruckMileage], [CarrierTicketNum], [AuditNotes], [CreateDateUTC], [ActualMiles], [ProducerID], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser], [DestProductBSW], [DestProductGravity], [DestProductTemp], [ProductID], [AcceptLastChangeDateUTC], [PickupLastChangeDateUTC], [DeliverLastChangeDateUTC], [OriginUomID], [DestUomID], [PickupPrintStatusID], [DeliverPrintStatusID], [PickupPrintDateUTC], [DeliverPrintDateUTC], [OriginTankID], [OriginGrossStdUnits], [DispatchNotes], [DriverNotes], [DispatchConfirmNum], [OriginWaitReasonID], [DestWaitReasonID], [RejectReasonID], [DestOpenMeterUnits], [DestCloseMeterUnits], ReassignKey)
						SELECT GETUTCDATE(), [ID], [OrderNum], [StatusID], [PriorityID], [DueDate], [RouteID], [OriginID], [OriginArriveTimeUTC], [OriginDepartTimeUTC], [OriginMinutes], [OriginWaitNotes], [OriginBOLNum], [OriginGrossUnits], [OriginNetUnits], [DestinationID], [DestArriveTimeUTC], [DestDepartTimeUTC], [DestMinutes], [DestWaitNotes], [DestBOLNum], [DestGrossUnits], [DestNetUnits], [CustomerID], [CarrierID], [DriverID], [TruckID], [TrailerID], [Trailer2ID], [OperatorID], [PumperID], [TicketTypeID], [Rejected], [RejectNotes], [ChainUp], [OriginTruckMileage], [OriginTankNum], [DestTruckMileage], [CarrierTicketNum], [AuditNotes], [CreateDateUTC], [ActualMiles], [ProducerID], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser], [DestProductBSW], [DestProductGravity], [DestProductTemp], [ProductID], [AcceptLastChangeDateUTC], [PickupLastChangeDateUTC], [DeliverLastChangeDateUTC], [OriginUomID], [DestUomID], [PickupPrintStatusID], [DeliverPrintStatusID], [PickupPrintDateUTC], [DeliverPrintDateUTC], [OriginTankID], [OriginGrossStdUnits], [DispatchNotes], [DriverNotes], [DispatchConfirmNum], [OriginWaitReasonID], [DestWaitReasonID], [RejectReasonID], [DestOpenMeterUnits], [DestCloseMeterUnits], ReassignKey
						FROM deleted d
			END TRY
			BEGIN CATCH
				PRINT 'trigOrder_IU.DBAUDIT FAILURE: ' + ERROR_MESSAGE()
			END CATCH
		END
		
	PRINT 'trigOrder_IU COMPLETE'

	END
	
END

GO
EXEC sp_settriggerorder @triggername=N'[dbo].[trigOrder_IU]', @order=N'First', @stmttype=N'INSERT'
GO
EXEC sp_settriggerorder @triggername=N'[dbo].[trigOrder_IU]', @order=N'First', @stmttype=N'UPDATE'
GO

/**********************************************************/
-- Author: Kevin Alons
-- Date Created: 26 Sep 2013
-- Purpose: track changes to Orders that cause it be "virtually" deleted for a Driver (for DriveApp.Sync purposes)
/**********************************************************/
ALTER TRIGGER [dbo].[trigOrder_U_VirtualDelete] ON [dbo].[tblOrder] AFTER UPDATE AS
BEGIN
	DECLARE @NewRecords TABLE (
		  OrderID int NOT NULL
		, DriverID int NOT NULL
		, LastChangeDateUTC smalldatetime NOT NULL
		, LastChangedByUser varchar(100) NULL
	)

	SET NOCOUNT ON;
	
	IF (TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU')) = 0 AND TRIGGER_NESTLEVEL( OBJECT_ID('trigOrderTicket_IU')) = 0
		AND EXISTS (SELECT * FROM inserted EXCEPT SELECT * FROM deleted)) 
	BEGIN
		PRINT 'trigOrder_U_VirtualDelete FIRED'
		
		-- delete any records that no longer apply because now the record is valid for the new driver/status
		DELETE FROM tblOrderDriverAppVirtualDelete
		FROM tblOrderDriverAppVirtualDelete ODAVD
		JOIN inserted i ON i.ID = ODAVD.OrderID AND i.DriverID = ODAVD.DriverID
		WHERE i.DeleteDateUTC IS NULL 
			AND dbo.fnPrintStatusID(i.StatusID, i.PickupPrintStatusID, i.DeliverPrintStatusID) IN (2,7,8,3)

		INSERT INTO @NewRecords
			-- insert any applicable VIRTUALDELETE records where the status changed from 
			--   a valid status to an invalid [DriverApp] status
			SELECT d.ID, d.DriverID, GETUTCDATE(), ISNULL(i.LastChangedByUser, 'System')
			FROM deleted d
			JOIN inserted i ON i.ID = D.ID
			WHERE dbo.fnPrintStatusID(i.StatusID, i.PickupPrintStatusID, i.DeliverPrintStatusID) NOT IN (2,7,8,3)
			  AND dbo.fnPrintStatusID(d.StatusID, d.PickupPrintStatusID, d.DeliverPrintStatusID) IN (2,7,8,3)
			  AND d.DriverID IS NOT NULL
			  
			UNION

			-- insert any records where the DriverID changed to another DriverID (for a valid status)
			SELECT d.ID, d.DriverID, GETUTCDATE(), ISNULL(i.LastChangedByUser, 'System')
			FROM deleted d
			JOIN inserted i ON i.ID = D.ID
			WHERE d.DriverID <> i.DriverID 
			  AND dbo.fnPrintStatusID(d.StatusID, d.PickupPrintStatusID, d.DeliverPrintStatusID) IN (2,7,8,3)
			  AND d.DriverID IS NOT NULL
			  
		-- update the VirtualDeleteDateUTC value for any existing records
		UPDATE tblOrderDriverAppVirtualDelete
		  SET VirtualDeleteDateUTC = NR.LastChangeDateUTC, VirtualDeletedByUser = NR.LastChangedByUser
		FROM tblOrderDriverAppVirtualDelete ODAVD
		JOIN @NewRecords NR ON NR.OrderID = ODAVD.OrderID AND NR.DriverID = ODAVD.DriverID

		-- insert the truly new VirtualDelete records	
		INSERT INTO tblOrderDriverAppVirtualDelete (OrderID, DriverID, VirtualDeleteDateUTC, VirtualDeletedByUser)
			SELECT NR.OrderID, NR.DriverID, NR.LastChangeDateUTC, NR.LastChangedByUser
			FROM @NewRecords NR
			LEFT JOIN tblOrderDriverAppVirtualDelete ODAVD ON ODAVD.OrderID = NR.OrderID AND ODAVD.DriverID = NR.DriverID
			WHERE ODAVD.ID IS NULL
	END
END

GO

EXEC sp_settriggerorder @triggername=N'[dbo].[trigOrder_U_VirtualDelete]', @order=N'Last', @stmttype=N'UPDATE'
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 6 Feb 2013
-- Description:	trigger to ensure the GrossBarrels and NetBarrels are computed for valid, entered data
-- =============================================
ALTER TRIGGER [dbo].[trigOrderTicket_IU] ON [dbo].[tblOrderTicket] AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	-- Note: the TRIGGER_NESTLEVEL( OBJECT_ID('trigOrderTicket_IU') ) = 1 statement prevents recursion
	IF (TRIGGER_NESTLEVEL( OBJECT_ID('trigOrderTicket_IU')) = 1) BEGIN
		
		-- only do anything if something actually changed
		IF EXISTS (SELECT * FROM inserted EXCEPT SELECT * FROM deleted)
		BEGIN
			PRINT 'trigOrderTicket_IU FIRED'
			
			/**********  START OF VALIDATION SECTION ************************/
			DECLARE @errorString varchar(255)
			
			IF (SELECT COUNT(*) FROM (
					SELECT OT.OrderID
					FROM tblOrderTicket OT
					JOIN inserted i ON i.OrderID = OT.OrderID AND i.CarrierTicketNum = OT.CarrierTicketNum
					WHERE OT.DeleteDateUTC IS NULL
					GROUP BY OT.OrderID, OT.CarrierTicketNum
					HAVING COUNT(*) > 1
				) v) > 0
			BEGIN
				SET @errorString = 'Duplicate Ticket Numbers are not allowed'
			END
			
			ELSE IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (3) AND BOLNum IS NULL AND DeleteDateUTC IS NULL) > 0
			BEGIN
				SET @errorString = 'BOL # value is required for Meter Run tickets'
			END
			
			ELSE IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1, 2) AND OriginTankID IS NULL AND TankNum IS NULL AND DeleteDateUTC IS NULL) > 0
			BEGIN
				SET @errorString = 'Tank ID value is required for Gauge Run & Net Volume tickets'
			END

			ELSE IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1) AND CarrierTicketNum IS NULL AND DeleteDateUTC IS NULL) > 0
			BEGIN
				SET @errorString = 'Ticket # value is required for Gauge Run tickets'
			END

			ELSE IF (SELECT COUNT(*) FROM inserted WHERE DeleteDateUTC IS NULL
					AND (TicketTypeID IN (1, 2) AND (ProductObsTemp IS NULL OR ProductObsGravity IS NULL OR ProductBSW IS NULL))
						OR (TicketTypeID IN (1) AND (ProductHighTemp IS NULL OR ProductLowTemp IS NULL))
				) > 0
			BEGIN
				SET @errorString = 'All Product Measurement values are required for Gauge Run & Net Volume tickets'
			END
			
			ELSE IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1) AND Rejected = 0 AND DeleteDateUTC IS NULL
				AND (OpeningGaugeFeet IS NULL OR OpeningGaugeInch IS NULL OR OpeningGaugeQ IS NULL)) > 0
			BEGIN
				SET @errorString = 'All Opening Gauge values are required for Gauge Run tickets'
			END
			ELSE IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1) AND Rejected = 0 AND DeleteDateUTC IS NULL
				AND (ClosingGaugeFeet IS NULL OR ClosingGaugeInch IS NULL OR ClosingGaugeQ IS NULL)) > 0
			BEGIN
				SET @errorString = 'All Closing Gauge values are required for Gauge Run tickets'
			END

			ELSE IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (2) AND Rejected = 0 AND DeleteDateUTC IS NULL
				AND (GrossUnits IS NULL)) > 0
			BEGIN
				SET @errorString = 'Gross Volume value is required for Net Volume tickets'
			END

			ELSE IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (3) AND Rejected = 0 AND DeleteDateUTC IS NULL
				AND (GrossUnits IS NULL OR NetUnits IS NULL)) > 0
			BEGIN
				SET @errorString = 'Gross & Net Volume values are required for Meter Run tickets'
				IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
				RETURN
			END

			ELSE IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1) AND Rejected = 0 AND DeleteDateUTC IS NULL
				AND (SealOff IS NULL OR SealOn IS NULL)) > 0
			BEGIN
				SET @errorString = 'All Seal Off & Seal On values are required for Gauge Run tickets'
			END

			IF (@errorString IS NOT NULL)
			BEGIN
				RAISERROR(@errorString, 16, 1)
				IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
			END
			
			/**********  END OF VALIDATION SECTION ************************/
			
			-- re-compute GaugeRun ticket Gross barrels
			IF UPDATE(OriginTankID) OR UPDATE(OpeningGaugeFeet) OR UPDATE(OpeningGaugeInch) OR UPDATE(OpeningGaugeQ)
				OR UPDATE (ClosingGaugeFeet) OR UPDATE(ClosingGaugeInch) OR UPDATE(ClosingGaugeQ) 
				OR UPDATE (GrossUnits)
			BEGIN
				UPDATE tblOrderTicket
				  SET GrossUnits = dbo.fnConvertUom(dbo.fnOriginTankStrappingLevelDeltaBarrels(
						OT.OriginTankID
					  , OpeningGaugeFeet
					  , OpeningGaugeInch
					  , OpeningGaugeQ
					  , ClosingGaugeFeet
					  , ClosingGaugeInch
					  , ClosingGaugeQ), 1, O.OriginUomID) -- ensure this value is always normalized from BARRELS
				FROM tblOrderTicket OT
				JOIN tblOrder O ON O.ID = OT.OrderID
				WHERE OT.ID IN (SELECT ID FROM inserted) 
				  AND OT.TicketTypeID = 1 -- Gauge Run Ticket
				  AND OT.OpeningGaugeFeet IS NOT NULL
				  AND OT.OpeningGaugeInch IS NOT NULL
				  AND OT.OpeningGaugeQ IS NOT NULL
				  AND OT.ClosingGaugeFeet IS NOT NULL
				  AND OT.ClosingGaugeInch IS NOT NULL
				  AND OT.ClosingGaugeQ IS NOT NULL
			END
			-- recompute the GrossUnits of any changed Meter Run tickets
			IF UPDATE(OpenMeterUnits) OR UPDATE(CloseMeterUnits) BEGIN
				--Print 'updating tblOrderTicket GOV from Open/Close Meter Units'
				UPDATE tblOrderTicket 
					SET GrossUnits = CloseMeterUnits - OpenMeterUnits
				WHERE TicketTypeID = 3  -- meter run tickets ONLY
				  AND OpenMeterUnits IS NOT NULL AND CloseMeterUnits IS NOT NULL
				
			END
			-- re-compute GaugeRun | Net Barrel | Meter Run tickets NetUnits
			IF UPDATE(OpeningGaugeFeet) OR UPDATE(OpeningGaugeInch) OR UPDATE(OpeningGaugeQ)
				OR UPDATE (ClosingGaugeFeet) OR UPDATE(ClosingGaugeInch) OR UPDATE(ClosingGaugeQ) 
				OR UPDATE(OriginTankID) OR UPDATE(GrossUnits) OR UPDATE(GrossStdUnits) OR UPDATE(NetUnits)
				OR UPDATE(ProductObsTemp) OR UPDATE(ProductObsGravity) OR UPDATE(ProductBSW)
				OR UPDATE(OpenMeterUnits) OR UPDATE(CloseMeterUnits)
			BEGIN
				--PRINT 'updating tblOrderTicket NSV/GSV'
				UPDATE tblOrderTicket
				  SET NetUnits = dbo.fnCrudeNetCalculator(GrossUnits, ProductObsTemp, ProductObsGravity, isnull(ProductBSW, 0))
					, GrossStdUnits = dbo.fnCrudeNetCalculator(GrossUnits, ProductObsTemp, ProductObsGravity, 0)
				WHERE ID IN (SELECT ID FROM inserted)
				  AND TicketTypeID IN (1,2)
				  AND GrossUnits IS NOT NULL
				  AND ProductObsTemp IS NOT NULL
				  AND ProductObsGravity IS NOT NULL
			END
			
			-- ensure the Order record is in-sync with the Tickets
			--declare @gov decimal(9,3); SELECT TOP 1 @gov = OT.GrossUnits FROM tblOrderTicket OT JOIN inserted i ON i.ID = OT.ID;
			--PRINT 'updating tblOrder GOV/GSV/NSV GOV = ' + ltrim(@gov)
			UPDATE tblOrder 
			SET OriginGrossUnits = (SELECT sum(GrossUnits) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND OT.Rejected = 0 AND OT.DeleteDateUTC IS NULL)
			  , OriginNetUnits = (SELECT sum(NetUnits) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND OT.Rejected = 0 AND OT.DeleteDateUTC IS NULL)
			  , OriginGrossStdUnits = (SELECT sum(GrossStdUnits) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND OT.Rejected = 0 AND OT.DeleteDateUTC IS NULL)

				-- use the first MeterRun/BasicRun BOLNum as the Order BOL Num (favor the first un-Rejected)
			  , OriginBOLNum = (
					SELECT TOP 1 BOLNum FROM (
						SELECT TOP 1 BOLNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID NOT IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
						UNION SELECT TOP 1 BOLNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID NOT IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X)
				-- use the first GaugeRun|NetVolume CarrierTicketNum as the Order.CarrierTicketNum (favor first un-Rejected)
			  , CarrierTicketNum = (
					SELECT TOP 1 CarrierTicketNum FROM (
						SELECT TOP 1 CarrierTicketNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
						UNION SELECT TOP 1 CarrierTicketNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X)
				-- use the first GaugeRun|NetVolume OriginTankID for the Order (favor first un-Rejected ticket)
			  , OriginTankID = (
					SELECT TOP 1 OriginTankID FROM (
						SELECT TOP 1 OriginTankID FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
						UNION SELECT TOP 1 OriginTankID FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X)
				-- use the first GaugeRun|NetVolume OriginTankNum for the Order (favor first un-Rejected ticket)
			  , OriginTankNum = (
					SELECT TOP 1 OriginTankNum FROM (
						SELECT TOP 1 OriginTankNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
						UNION SELECT TOP 1 OriginTankNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X)
			  , LastChangeDateUTC = (SELECT MAX(isnull(LastChangeDateUTC, CreateDateUTC)) FROM inserted WHERE OrderID = O.ID)
			  , LastChangedByUser = (SELECT MIN(isnull(LastChangedByUser, CreatedByUser)) FROM inserted WHERE OrderID = O.ID)
			FROM tblOrder O
			WHERE ID IN (SELECT DISTINCT OrderID FROM inserted)

			PRINT 'trigOrderTicket_IU COMPLETE'
		END
	END
	
END


GO
EXEC sp_settriggerorder @triggername=N'[dbo].[trigOrderTicket_IU]', @order=N'First', @stmttype=N'INSERT'
GO
EXEC sp_settriggerorder @triggername=N'[dbo].[trigOrderTicket_IU]', @order=N'First', @stmttype=N'UPDATE'
GO

EXEC _spRefreshAllViews
EXEC _spRefreshAllViews
EXEC _spRefreshAllViews
EXEC _spRefreshAllViews
GO

/***********************************
-- Date Created: 18 Sep 2014
-- Author: Kevin Alons
-- Purpose: return Carrier route rates per the specified criteria (used by Carrier Route Rates page)
***********************************/
ALTER PROCEDURE [dbo].[spGetCarrierRouteRates](
  @Carrierid int
, @originID int
, @destinationID int
, @startDate date
, @includeActive bit
, @includeMissing bit
, @includeRouteInactive bit
, @IncludeCarrierDeleted bit
, @includeMissingRoutes bit
) AS
BEGIN
	SELECT TOP 100 PERCENT *
		, CASE WHEN Status LIKE '%Missing' THEN Rate ELSE NULL END AS NewRate
		, CASE WHEN Status LIKE '%Missing' THEN EffectiveDate ELSE NULL END AS NewEffectiveDate
		, NULL AS ImportOutcome 
	INTO #data
	FROM (
		/* get the currently defined or missing rates */
		SELECT ID, RouteID, UomID, Rate, EffectiveDate, Active, OpenOrderCount, ActualMiles
			, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser
			, EndDate, EarliestEffectiveDate, CarrierID, Carrier, OriginID, Origin, OriginFull, DestinationID, Destination, DestinationFull
			, Status, RouteInUse, UOM, UomShort
		FROM dbo.viewCarrierRouteRates 
		WHERE (@CarrierID = -1 OR CarrierID = @CarrierID) 
			AND (@OriginID = -1 OR OriginID = @OriginID) 
			AND (@DestinationID = -1 OR DestinationID = @DestinationID) 
			AND (EndDate IS NULL OR EndDate >= @StartDate) 
			AND (@IncludeActive = 1 OR Status NOT LIKE 'Active') 
			AND (@IncludeMissing = 1 OR Status NOT LIKE 'Missing') 
			AND (@IncludeRouteInactive = 1 OR Status NOT LIKE 'Route Inactive') 
			AND (@IncludeCarrierDeleted = 1 OR Status NOT LIKE 'Carrier Deleted')	
		UNION 

		/* generate any rates that could exist for missing/undefined routes */
		SELECT ID = NULL, RouteID = NULL, UomID = O.UomID, Rate = NULL, EffectiveDate = isnull(@StartDate, getdate()), Active = cast(0 as bit), OpenOrderCount = 0, ActualMiles = 0
		, null, null, null, null
		, null, null, C.ID, C.Name, O.ID, O.Name, O.FullName, D.ID, D.Name, D.FullName
		, 'Route Missing', cast(0 as bit), U.Name, U.Abbrev 
		FROM viewOrigin O
		JOIN tblUom U ON U.ID = O.UomID
		CROSS JOIN viewDestination D
		JOIN tblCarrier C ON C.ID = @CarrierID
		LEFT JOIN viewCarrierRouteRates R ON R.OriginID = O.ID AND R.DestinationID = D.ID AND (R.EndDate IS NULL OR EndDate >= @StartDate)
		WHERE (@includeMissingRoutes = 1)
		  AND R.ID IS NULL
		  AND (@CarrierID = -1 OR C.ID = @CarrierID)
		  AND (@originID = -1 OR O.ID = @originID)
		  AND (@destinationID = -1 OR O.ID = @destinationID)
	) X
	
	SELECT TOP 100 PERCENT D.*
	FROM #data D
	JOIN viewOrder_CarrierRoutes OCR ON OCR.RouteID = D.RouteID AND OCR.CarrierID = D.CarrierID
	ORDER BY d.Carrier, d.Origin, D.Destination, D.EffectiveDate DESC
		
END

GO

/***********************************/
-- Date Created: 18 Sep 2014
-- Author: Kevin Alons
-- Purpose: return Carrier route rates per the specified criteria (used by Carrier Route Rates page)
/***********************************/
ALTER PROCEDURE [dbo].[spGetCarrierRouteRates](
  @CarrierID int
, @originID int
, @destinationID int
, @startDate date
, @includeActive bit
, @includeMissing bit
, @includeRouteInactive bit
, @IncludeCarrierDeleted bit
, @includeMissingRoutes bit
) AS
BEGIN
	SELECT TOP 100 PERCENT *
		, CASE WHEN Status LIKE '%Missing' THEN Rate ELSE NULL END AS NewRate
		, CASE WHEN Status LIKE '%Missing' THEN EffectiveDate ELSE NULL END AS NewEffectiveDate
		, NULL AS ImportOutcome 
	FROM (
		-- get the currently defined or missing rates
		SELECT ID, RouteID, UomID, Rate, EffectiveDate, Active, OpenOrderCount, ActualMiles
			, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser
			, EndDate, EarliestEffectiveDate, CarrierID, Carrier, OriginID, Origin, OriginFull, DestinationID, Destination, DestinationFull
			, Status, RouteInUse, UOM, UomShort
		FROM dbo.viewCarrierRouteRates 
		WHERE (@CarrierID = -1 OR CarrierID = @CarrierID) 
			AND (@OriginID = -1 OR OriginID = @OriginID) 
			AND (@DestinationID = -1 OR DestinationID = @DestinationID) 
			AND (EndDate IS NULL OR EndDate >= @StartDate) 
			AND (@IncludeActive = 1 OR Status NOT LIKE 'Active') 
			AND (@IncludeMissing = 1 OR Status NOT LIKE 'Missing') 
			AND (@IncludeRouteInactive = 1 OR Status NOT LIKE 'Route Inactive') 
			AND (@IncludeCarrierDeleted = 1 OR Status NOT LIKE 'Carrier Deleted')
		UNION 

		-- generate any rates that could exist for missing/undefined routes
		SELECT ID = NULL, RouteID = NULL, UomID = O.UomID, Rate = NULL, EffectiveDate = isnull(@StartDate, getdate()), Active = cast(0 as bit)
			, OpenOrderCount = 0, ActualMiles = 0
			, NULL, NULL, NULL, NULL
			, NULL, NULL, C.ID, C.Name, O.ID, O.Name, O.FullName, D.ID, D.Name, D.FullName
			, 'Route Missing', cast(0 as bit), U.Name, U.Abbrev 
		FROM viewOrigin O
		JOIN tblUom U ON U.ID = O.UomID
		CROSS JOIN viewDestination D
		JOIN tblCarrier C ON (@CarrierID = -1 OR C.ID = @CarrierID)
		LEFT JOIN viewCarrierRouteRates CRR ON CRR.CarrierID = C.ID 
			AND CRR.OriginID = O.ID 
			AND CRR.DestinationID = D.ID 
			AND (CRR.EndDate IS NULL OR EndDate >= @StartDate)
		WHERE @includeMissingRoutes = 1
		  AND CRR.ID IS NULL
		  AND (@originID = -1 OR O.ID = @originID)
		  AND (@destinationID = -1 OR D.ID = @destinationID)
	) X
	ORDER BY Carrier, Origin, Destination, EffectiveDate DESC
		
END

GO

/***********************************/
-- Date Created: 18 Sep 2014
-- Author: Kevin Alons
-- Purpose: return Carrier route rates per the specified criteria (used by Carrier Route Rates page)
/***********************************/
ALTER PROCEDURE [dbo].[spGetCustomerRouteRates](
  @customerid int
, @originID int
, @destinationID int
, @startDate date
, @includeActive bit
, @includeMissing bit
, @includeRouteInactive bit
, @includeMissingRoutes bit
) AS
BEGIN
	SELECT TOP 100 PERCENT D.*
		, CASE WHEN Status LIKE '%Missing' THEN Rate ELSE NULL END AS NewRate
		, CASE WHEN Status LIKE '%Missing' THEN EffectiveDate ELSE NULL END AS NewEffectiveDate
		, NULL AS ImportOutcome 
	FROM (
		-- get the currently defined or missing rates
		SELECT ID, RouteID, UomID, Rate, EffectiveDate, Active, OpenOrderCount, ActualMiles
			, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser
			, EndDate, EarliestEffectiveDate, CustomerID, Customer, OriginID, Origin, OriginFull, DestinationID, Destination, DestinationFull
			, Status, RouteInUse, UOM, UomShort
		FROM dbo.viewCustomerRouteRates 
		WHERE (@CustomerID = -1 OR CustomerID = @CustomerID) 
			AND (@OriginID = -1 OR OriginID = @OriginID) 
			AND (@DestinationID = -1 OR DestinationID = @DestinationID) 
			AND (EndDate IS NULL OR EndDate >= @StartDate) 
			AND (@IncludeActive = 1 OR Status NOT LIKE 'Active') 
			AND (@IncludeMissing = 1 OR Status NOT LIKE 'Missing') 
			AND (@IncludeRouteInactive = 1 OR Status NOT LIKE 'Route Inactive') 
		
		UNION 

		-- generate any rates that could exist for missing/undefined routes
		SELECT ID = NULL, RouteID = NULL, UomID = O.UomID, Rate = NULL, EffectiveDate = isnull(@StartDate, getdate()), Active = cast(0 as bit), OpenOrderCount = 0, ActualMiles = 0
		, null, null, null, null
		, null, null, C.ID, C.Name, O.ID, O.Name, O.FullName, D.ID, D.Name, D.FullName
		, 'Route Missing', cast(0 as bit), U.Name, U.Abbrev 
		FROM viewOrigin O
		JOIN tblUom U ON U.ID = O.UomID
		CROSS JOIN viewDestination D
		JOIN tblCustomer C ON C.ID = @CustomerID
		LEFT JOIN viewCustomerRouteRates R ON R.OriginID = O.ID AND R.DestinationID = D.ID AND (R.EndDate IS NULL OR EndDate >= @StartDate)
		WHERE (@includeMissingRoutes = 1)
		  AND R.ID IS NULL
		  AND (@customerid = -1 OR C.ID = @customerid)
		  AND (@originID = -1 OR O.ID = @originID)
		  AND (@destinationID = -1 OR O.ID = @destinationID)
	) D
	JOIN tblOrigin O ON O.ID = D.OriginID AND O.CustomerID = D.CustomerID
	ORDER BY D.Customer, D.Origin, D.Destination, D.EffectiveDate DESC

END

GO

EXEC _spRebuildAllObjects
GO

COMMIT
SET NOEXEC OFF

/*
	select reassignKey, carrierid, driverid, truckid, trailerid, ticketcount = (select COUNT(*) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID), * 
	from tblOrder O 
	where ordernum = 236117 
select * from tblOrderTicket where OrderID in (select ID from tblorder where OrderNum IN ('236117'))
	select reassignKey, carrierid, driverid, truckid, trailerid, ticketcount = (select COUNT(*) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID), * 
	from tblOrder O 
	where reassignkey is not null
select * from tblOrderTicket where OrderID in (select ID from tblorder where reassignkey is not null)

select * from tblDriver where ID in (1148, 1291) and deletedateutc is null
select * from tblDriver where CarrierID = 1085 and deletedateutc is null

begin tran 
update tblOrder set DriverID = 1272, lastchangedateutc = getutcdate(), lastchangedbyuser = 'kalons' where OrderNum = '236117'
rollback
*/