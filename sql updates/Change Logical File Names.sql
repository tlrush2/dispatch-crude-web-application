use master

alter database badlandsdb
modify file (name=badlandsdb, newname=Data)
go
alter database badlandsdb
modify file (name=badlandsdb_log, newname=Log)
go