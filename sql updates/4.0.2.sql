SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.0.1'
SELECT  @NewVersion = '4.0.2'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'DCWEB-1663: Update API calculations using 2004 standard'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

EXEC _spDropFunction 'fnVCF'
GO
/***********************************************************************/
-- Date Created: 20 Jul 2016
-- Author: Joe Engler
-- Purpose: given the parameters, calculate the Volume Correction Factor
-- Changes:
--		4.0.2		2016-08-17		JAE		Updated constant for water density at 60
/***********************************************************************/
CREATE FUNCTION fnVCF (@Gravity60F decimal(18,8), @ObsTemp decimal(8,3))
RETURNS FLOAT
BEGIN
	DECLARE @WATER_DENSITY_60F FLOAT = 999.016
	DECLARE @K0 FLOAT = 341.0957
	DECLARE @K1 FLOAT = 0.0000

	--calculate difference in observed and base temps
	DECLARE @delta FLOAT = @obsTemp - 60.0

	--convert API gravity to density
	DECLARE @rho FLOAT = ROUND((141.5 * @WATER_DENSITY_60F)/(131.5 + @gravity60f), 2)

	-- calculate coefficient of thermal expansion
	DECLARE @alpha FLOAT = ROUND(@K0 / @rho / @rho + @K1 / @rho, 7)
	--SELECT @factor10 = (341.0957/((1-(0.00001278*(@ambientTemp-60))-((0.0000000062)*((@ambientTemp-60)*(@ambientTemp-60))))*(141360.198/(@const5+@CorrGravity))))/((1-(@const3*(@ambientTemp-60))-((@const6)*((@ambientTemp-60)*(@ambientTemp-60))))*(141360.198/(@const5+@CorrGravity)))

	-- calculate volume correction factor
	DECLARE @vcf FLOAT = ROUND(EXP(ROUND(-@alpha * @delta - 0.8 * @alpha * @alpha * @delta * @delta, 8)),6)
	IF @vcf < 1 
		SELECT @vcf = ROUND(@vcf, 5)
	ELSE
		SELECT @vcf = ROUND(@vcf, 4)

	RETURN @vcf
END

GO
GRANT EXECUTE ON fnVCF TO role_iis_acct
GO

/***********************************************************************/
-- Date Created: 18 Apr 2013
-- Author: Kevin Alons
-- Purpose: given the parameters, convert UncorrectedAPIGravity to CorrectedAPIGravity
-- Source: http://ioilfield.com/units_correl/o_api_co.html
-- Changes:
--	3.13.3.1	07/20/2016	JAE		Corrected formula to match the 11.1 API documentation
--  3.13.8.1	07/29/2016	JAE		Altered to handle extreme temps (which caused large looping)
--	4.0.2		08/17/2016	JAE		Removed hydrometer correction and updated constant for water density at 60
/***********************************************************************/
ALTER FUNCTION fnCorrectedAPIGravity (@ObsGravity decimal(18,8), @ObsTemp decimal(8,3))
RETURNS DECIMAL(18,1)
BEGIN
	DECLARE @WATER_DENSITY_60F FLOAT = 999.016
	DECLARE @K0 FLOAT = 341.0957
	DECLARE @K1 FLOAT = 0.0000
	DECLARE @i INT = 0

	--calculate difference in observed and base temps
	DECLARE @delta FLOAT
	SELECT @delta = @obsTemp - 60.0

	--compute hydrometer correction term
	DECLARE @hyc FLOAT = 1.0 - ROUND(0.00001278 * @delta,9) - ROUND(0.0000000062 * @delta * @delta,9)

	--convert API gravity to density
	DECLARE @rho FLOAT = ROUND((141.5 * @WATER_DENSITY_60F)/(131.5 + @obsGravity), 2)

    -- Apply hydrometer correction
	DECLARE @rhoT FLOAT = ROUND(@rho * @hyc, 2)

	-- Initialize density @ 60F
	DECLARE @rho60 FLOAT = @rhoT

	DECLARE @rho60prev FLOAT = @rho60 -1 -- something to trigger the first loop
	DECLARE @alpha FLOAT
	DECLARE @vcf FLOAT

	WHILE ABS(@rho60 - @rho60prev) >= .05 AND @i < 20
	BEGIN
		-- calculate coefficient of thermal expansion
		SELECT @alpha = ROUND(@K0 / @rho60 / @rho60 + @K1 / @rho60, 7)

		-- calculate volume correction factor
		SELECT @vcf = ROUND(EXP(ROUND(-@alpha * @delta - 0.8 * @alpha * @alpha * @delta * @delta, 8)),6)

		SELECT @rho60prev = @rho60
		SELECT @rho60 = ROUND(@rhoT/@vcf, 3, 1)

		SELECT @i = @i + 1
	END

	RETURN ROUND(141.5 * @WATER_DENSITY_60F / @rho60 - 131.5, 1)
END

GO


EXEC _spDropFunction 'fnCrudeNetCalculator2004api'
GO
/***********************************/
-- Date Created: 20 Jul 2016
-- Author: Joe Engler
-- Purpose: New attempt at calculating NSV using the 2004 API standards
/***********************************/
CREATE FUNCTION dbo.fnCrudeNetCalculator2004api
(
  @Gross float
, @ObsTemp float
, @OpenTemp float
, @CloseTemp float
, @ObsGravity float
, @BSW float
, @SigFig int = 2
) RETURNS FLOAT AS
BEGIN
	-- Round significant digits
	SET @Gross = ROUND(@Gross, @SigFig);
	SET @ObsTemp = ROUND(@ObsTemp, 1);
	SET @OpenTemp = ROUND(@OpenTemp, 1);
	SET @CloseTemp = ROUND(@CloseTemp, 1);
	SET @ObsGravity = ROUND(@ObsGravity, 1);
	SET @BSW = ROUND(@BSW, 3);

	DECLARE @ambientTemp FLOAT

	--calculate the ambient temp
	IF (@OpenTemp IS NULL OR @CloseTemp IS NULL)
		SELECT @ambientTemp = @ObsTemp
	ELSE
		SELECT @ambientTemp = ( (@OpenTemp + @CloseTemp) / 2 ); /* Calculate the mean ambient temperature (primary temp operand) */

	-- calculate volume correction factor
	DECLARE @vcf FLOAT = dbo.fnVCF(dbo.fnCorrectedAPIGravity(@obsGravity, @obsTemp), @ambientTemp)


	RETURN ROUND(@gross * @vcf * (1 - @bsw/100), @SigFig)
END


GO
GRANT EXECUTE ON fnCrudeNetCalculator2004api TO role_iis_acct
GO

/*****************************************
-- Date Created: ??
-- Author: Kevin Alons, Modified by Geoff Mochau
-- Purpose: specialized, db-level logic enforcing business rules, audit changes, etc
-- Changes:
    - 3.8.12	- 2015/08/04 - GSM	- use the revised (3-temperature) API function
	- 3.8.10	- 2015/07/26 - KDA	- always update the associated tblOrder.LastChangeDateUTC to NOW when a Ticket is changed)
	- 3.9.29.5	- 2015/12/03 - JAE	- add ProductBSW to dbaudit trigger
	- 3.9.38	- 2015/12/17 - BB	- Add WeightTareUnits, WeightGrossUnits, and WeightNetUnits columns to db audit (DCWEB-972)
	- 3.9.38	- 2015/12/21 - BB	- Add validation, calculations, etc. for WeightTareUnits, WeightGrossUnits, and WeightNetUnits (DCWEB-972)
	- 3.10.5.2	- 2016/02/11 - JAE	- Update to order table to sync ticket changes also resets the Pickup Last Change Date
	- 3.11.8	- 2016/03/06 - KDA	- add NoDataCalc field logic (do not calculate quantities, ensure raw data is provided - validation)
	- 3.11.11   - 2016/04/08 - JMS  - add Scale Ticket Number to tblOrderTicketDbAudit
	- 3.12.4    - 2016/05/20 - GSM/JAE - add Water Run to Gross auto calculation
    - 3.12.5.1	- 2016/05/23 - JAE	- Added rounding per government standards
	- 3.12.6.3	- 2016/06/08 - JAE	- Suppress errors from gauger ticket when disable requirements are set
	- 3.12.6.6	- 2016/06/13 - JAE	- Include status Generated-Accepted in checks since ticket can be updated by trigger
	- 4.0.2		- 2016/08/23 - JAE	- Use the new crude net calculator (2004 api)
*****************************************/
ALTER TRIGGER trigOrderTicket_IU ON tblOrderTicket AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	-- Note: the TRIGGER_NESTLEVEL( OBJECT_ID('trigOrderTicket_IU') ) = 1 statement prevents recursion
	IF (TRIGGER_NESTLEVEL( OBJECT_ID('trigOrderTicket_IU')) = 1) BEGIN
		
		-- only do anything if something actually changed
		IF EXISTS (SELECT * FROM inserted EXCEPT SELECT * FROM deleted)
		BEGIN
			PRINT 'trigOrderTicket_IU FIRED'
			
			/**********  START OF VALIDATION SECTION ************************/
			DECLARE @errorString varchar(1000); SET @errorString = ''

			IF EXISTS (
				SELECT * 
				FROM tblOrder 
				WHERE ID IN (SELECT OrderID FROM inserted) 
				  -- prevent changes to any tickets belonging to a Delivered or Audited Order
				  AND StatusID IN (4))
			BEGIN
				IF (dbo.fnToBool(dbo.fnSettingValue(35)) = 1)
					PRINT 'Ticket for an AUDITED orders is being modified - please investigate why!'
				ELSE 				
					SET @errorString = @errorString + '|Tickets of Audited Orders cannot be modified'
			END

			-- prevent reassigning a ticket from one order to another
			IF EXISTS (
				SELECT i.* 
				FROM inserted i
				JOIN deleted d ON i.UID = d.UID
				WHERE i.OrderID <> d.OrderID)
			BEGIN
				SET @errorString = @errorString + '|Tickets cannot be moved to a different Order'
			END
			
			IF EXISTS (
				SELECT OT.OrderID
				FROM tblOrderTicket OT
				JOIN tblOrder O ON O.ID = OT.OrderID
				JOIN inserted i ON i.OrderID = OT.OrderID AND i.CarrierTicketNum = OT.CarrierTicketNum AND i.ID <> OT.ID
				WHERE OT.DeleteDateUTC IS NULL and I.DeleteDateUTC IS NULL
				GROUP BY OT.OrderID, OT.CarrierTicketNum
				HAVING COUNT(*) > 1
			)
			BEGIN
				SET @errorString = @errorString + '|Duplicate Ticket Numbers are not allowed'
			END
			
			-- store all the tickets for orders that are not in Generated status and not deleted (only these need validation)
			SELECT i.*, O.StatusID 
			INTO #i
			FROM inserted i
			JOIN tblOrder O ON O.ID = i.OrderID
			WHERE i.DeleteDateUTC IS NULL
			
			/* -- removed with version 3.7.5 
			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (3) AND BOLNum IS NULL)
			BEGIN
				SET @errorString = @errorString + '|BOL # value is required for Meter Run tickets'
			END
			*/
			
			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (1, 2) AND OriginTankID IS NULL AND TankNum IS NULL AND Rejected = 0)
			BEGIN
				SET @errorString = @errorString + '|Tank is required for Gauge Run & Net Volume tickets'
			END

			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (1) AND CarrierTicketNum IS NULL)
			BEGIN
				SET @errorString = @errorString + '|Ticket # value is required for Gauge Run tickets'
			END

			/* require all parameters for orders NOT IN (GAUGER, GENERATED, ASSIGNED, DISPATCHED) */
			IF  EXISTS (SELECT ID FROM #i WHERE Rejected =  0
					AND (   (TicketTypeID IN (2) AND (ProductObsTemp IS NULL OR ProductObsGravity IS NULL OR ProductBSW IS NULL))
						 OR (TicketTypeID IN (1) AND (ProductObsTemp IS NULL OR ProductObsGravity IS NULL OR ProductBSW IS NULL)
						      AND (   (NOT EXISTS (SELECT 1 from dbo.fnOrderOrderRules(OrderID) where TypeID = 16 and dbo.fnToBool(value) = 1)) -- Disable Gauger Validation
				                   OR (StatusID NOT IN (-9, -10, 1, 2, 7))))
						 OR (TicketTypeID IN (1) AND StatusID NOT IN (-9, -10, 1, 2, 7) AND (ProductHighTemp IS NULL OR ProductLowTemp IS NULL))
					)
				)
			BEGIN
				SET @errorString = @errorString + '|All Product Measurement values are required for Gauge Run tickets'
			END
			
			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (1) AND Rejected = 0 
				AND (   (NOT EXISTS (SELECT 1 from dbo.fnOrderOrderRules(OrderID) where TypeID = 16 and dbo.fnToBool(value) = 1)) -- Disable Gauger Validation
				     OR (StatusID NOT IN (-9)))
				AND (OpeningGaugeFeet IS NULL OR OpeningGaugeInch IS NULL OR OpeningGaugeQ IS NULL))
			BEGIN
				SET @errorString = @errorString + '|All Opening Gauge values are required for Gauge Run tickets'
			END
			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (1) AND Rejected = 0 AND StatusID NOT IN (-9, -10, 1, 2)
				AND (ClosingGaugeFeet IS NULL OR ClosingGaugeInch IS NULL OR ClosingGaugeQ IS NULL))
			BEGIN
				SET @errorString = @errorString + '|All Closing Gauge values are required for Gauge Run tickets'
			END

			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (2) AND Rejected = 0 AND StatusID NOT IN (-9, -10, 1, 2) AND (GrossUnits IS NULL))
			BEGIN
				SET @errorString = @errorString + '|Gross Volume value is required for Net Volume tickets'
			END

			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (7) AND Rejected = 0 AND StatusID NOT IN (-9, -10, 1, 2) AND (GrossUnits IS NULL))
			BEGIN
				SET @errorString = @errorString + '|Gross Volume value is required for Gauge Net tickets'
			END

			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (3) AND Rejected = 0 AND StatusID NOT IN (-9, -10, 1, 2) AND (GrossUnits IS NULL OR NetUnits IS NULL))
			BEGIN
				SET @errorString = @errorString + '|Gross & Net Volume values are required for Meter Run tickets'
			END

			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (1) AND Rejected = 0 AND StatusID NOT IN (-9, -10, 1, 2) AND (SealOff IS NULL OR SealOn IS NULL))
			BEGIN
				SET @errorString = @errorString + '|All Seal Off & Seal On values are required for Gauge Run tickets'
			END
			
			-- 3.9.38 - 2015/12/21 - BB - Add WeightGrossUnits check
			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (9) AND Rejected = 0 AND StatusID NOT IN (-9, -10, 1, 2) AND (WeightGrossUnits IS NULL OR WeightNetUnits IS NULL))
			BEGIN
				SET @errorString = @errorString + '|Gross & Net Weight values are required for Mineral Run tickets.'
			END
			
			-- 3.9.38 - 2015/12/21 - BB - Add WeightTareUnits check
			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (9) AND Rejected = 0 AND StatusID NOT IN (-9, -10, 1, 2) AND (WeightTareUnits IS NULL))
			BEGIN
				SET @errorString = @errorString + '|Tare Weight is required for Mineral Run tickets.'
			END

			-- 3.11.8 - 2016/03/06 - KDA - ensure raw data is provided if NoDataCalc = 1 was set
			IF EXISTS (SELECT ID FROM #i WHERE NoDataCalc = 1 AND Rejected = 0 AND StatusID NOT IN (-9, -10, 1, 2) AND TicketTypeID NOT IN (9) AND (GrossUnits IS NULL OR NetUnits IS NULL))
			BEGIN
				SET @errorString = @errorString + '|Gross & Net Units must be provided for all Tickets with No Data Calc turned ON.'
			END

			-- if any errors were detected, cancel the entire operation (transaction) and return a LINEFEED-deliminated list of errors
			IF (len(@errorString) > 0)
			BEGIN
				SET @errorString = replace(substring(@errorString, 2, 1000), '|', char(13) + char(10))
				RAISERROR(@errorString, 16, 1)
				IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
				RETURN
			END
			/**********  END OF VALIDATION SECTION ************************/
			
			-- re-compute GaugeRun ticket Gross barrels
			IF UPDATE(OriginTankID) OR UPDATE(OpeningGaugeFeet) OR UPDATE(OpeningGaugeInch) OR UPDATE(OpeningGaugeQ)
				OR UPDATE (ClosingGaugeFeet) OR UPDATE(ClosingGaugeInch) OR UPDATE(ClosingGaugeQ) 
				OR UPDATE (GrossUnits)
			BEGIN
				UPDATE tblOrderTicket
				  SET GrossUnits = dbo.fnConvertUom(dbo.fnOriginTankStrappingLevelDeltaBarrels(
						OT.OriginTankID
					  , OpeningGaugeFeet
					  , OpeningGaugeInch
					  , OpeningGaugeQ
					  , ClosingGaugeFeet
					  , ClosingGaugeInch
					  , ClosingGaugeQ), 1, O.OriginUomID) -- ensure this value is always normalized from BARRELS
				FROM tblOrderTicket OT
				JOIN tblOrder O ON O.ID = OT.OrderID
				WHERE (OT.NoDataCalc = 0 OR OT.GrossUnits IS NULL) -- 3.11.8 - do not calculate these values when NoDataCalc is turned ON (1) and not provided
				  AND OT.ID IN (SELECT ID FROM inserted) 
				  AND OT.TicketTypeID IN (1, 11) -- Gauge Run Ticket & Production Water Run
				  AND OT.OpeningGaugeFeet IS NOT NULL
				  AND OT.OpeningGaugeInch IS NOT NULL
				  AND OT.OpeningGaugeQ IS NOT NULL
				  AND OT.ClosingGaugeFeet IS NOT NULL
				  AND OT.ClosingGaugeInch IS NOT NULL
				  AND OT.ClosingGaugeQ IS NOT NULL
			END
			-- recompute the GrossUnits of any changed Meter Run tickets
			IF UPDATE(OpenMeterUnits) OR UPDATE(CloseMeterUnits) BEGIN
				--Print 'updating tblOrderTicket GOV from Open/Close Meter Units'
				UPDATE tblOrderTicket 
					SET GrossUnits = CloseMeterUnits - OpenMeterUnits
				WHERE TicketTypeID = 3  -- meter run tickets ONLY
				  AND (NoDataCalc = 0 OR GrossUnits IS NULL)  -- 3.11.8 - do not calculate these values when NoDataCalc is turned ON (1) and not provided
				  AND OpenMeterUnits IS NOT NULL AND CloseMeterUnits IS NOT NULL
				
			END
			-- re-compute GaugeRun | Net Barrel | Meter Run tickets NetUnits
			IF UPDATE(OpeningGaugeFeet) OR UPDATE(OpeningGaugeInch) OR UPDATE(OpeningGaugeQ)
				OR UPDATE (ClosingGaugeFeet) OR UPDATE(ClosingGaugeInch) OR UPDATE(ClosingGaugeQ) 
				OR UPDATE(OriginTankID) OR UPDATE(GrossUnits) OR UPDATE(GrossStdUnits) OR UPDATE(NetUnits)
				OR UPDATE(ProductObsTemp) OR UPDATE(ProductObsGravity) OR UPDATE(ProductBSW)
				OR UPDATE(OpenMeterUnits) OR UPDATE(CloseMeterUnits)
				OR UPDATE(ProductHighTemp) OR UPDATE(ProductLowTemp)
			BEGIN
				--PRINT 'updating tblOrderTicket NSV/GSV'
				UPDATE tblOrderTicket
				  SET NetUnits = dbo.fnCrudeNetCalculator2004api(GrossUnits, ProductObsTemp, ProductHighTemp, ProductLowTemp, ProductObsGravity, isnull(ProductBSW, 0), (SELECT TOP 1 u.SignificantDigits FROM tblUom u, tblOrder o WHERE o.OriginUomID = u.ID AND o.ID IN (SELECT OrderID FROM inserted)))
					, GrossStdUnits = dbo.fnCrudeNetCalculator2004api(GrossUnits, ProductObsTemp, ProductHighTemp, ProductLowTemp, ProductObsGravity, 0, (SELECT TOP 1 u.SignificantDigits FROM tblUom u, tblOrder o WHERE o.OriginUomID = u.ID AND o.ID IN (SELECT OrderID FROM inserted)))
				WHERE ID IN (SELECT ID FROM inserted)
				  AND NoDataCalc = 0  -- 3.11.8 - do not calculate these values when NoDataCalc is turned ON (1)
				  AND TicketTypeID IN (1, 2, 7) -- GAUGE RUN, NET VOLUME, GAUGE NET
				  AND GrossUnits IS NOT NULL
				  AND ProductHighTemp IS NOT NULL
				  AND ProductLowTemp IS NOT NULL
				  AND ProductObsTemp IS NOT NULL
				  AND ProductObsGravity IS NOT NULL
			END
			
			-- 3.9.38 - 2015/12/21 - BB - recompute the Net Weight of any changed Mineral Run tickets (DCWEB-972)
			IF UPDATE(WeightGrossUnits) OR UPDATE(WeightTareUnits) BEGIN
				--Print 'updating tblOrderTicket WeightNetUnits from WeightGrossUnits and WeightTareUnits
				UPDATE tblOrderTicket
					SET WeightNetUnits = WeightGrossUnits - WeightTareUnits
				WHERE TicketTypeID = 9 -- Mineral Run tickets only
				  AND (NoDataCalc = 0 OR WeightNetUnits IS NULL) -- 3.11.8 - do not calculate these values when NoDataCalc is turned ON (1) and not provided
				  AND WeightGrossUnits IS NOT NULL AND  WeightTareUnits IS NOT NULL
			END 			
			
			-- ensure the Order record is in-sync with the Tickets
			/* test lines below
			declare @gov decimal(9,3); SELECT TOP 1 @gov = OT.GrossUnits FROM tblOrderTicket OT JOIN inserted i ON i.ID = OT.ID;
			PRINT 'updating tblOrder GOV/GSV/NSV GOV = ' + ltrim(@gov)
			--*/
			UPDATE tblOrder 
			SET OriginGrossUnits = (SELECT sum(GrossUnits) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND OT.Rejected = 0 AND OT.DeleteDateUTC IS NULL)
			  , OriginNetUnits = (SELECT sum(NetUnits) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND OT.Rejected = 0 AND OT.DeleteDateUTC IS NULL)
			  , OriginGrossStdUnits = (SELECT sum(GrossStdUnits) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND OT.Rejected = 0 AND OT.DeleteDateUTC IS NULL)
			    --3.9.38 - 2015/12/21 - BB - Update weight fields (DCWEB-972)  -- Per Maverick 12/21/15 we do not need gross on the order level.
			  --, OriginWeightGrossUnits = (SELECT sum(WeightGrossUnits) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND OT.Rejected = 0 AND OT.DeleteDateUTC IS NULL)
			  , OriginWeightNetUnits = 	(SELECT sum(WeightNetUnits) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND OT.Rejected = 0 AND OT.DeleteDateUTC IS NULL)
				-- use the first MeterRun/BasicRun BOLNum as the Order BOL Num (favor the first un-Rejected)
			  , OriginBOLNum = (
					SELECT TOP 1 BOLNum FROM (
						SELECT TOP 1 BOLNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID NOT IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
						UNION SELECT TOP 1 BOLNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID NOT IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X)
				-- use the first GaugeRun|NetVolume CarrierTicketNum as the Order.CarrierTicketNum (favor first un-Rejected)
			  , CarrierTicketNum = (
					SELECT TOP 1 CarrierTicketNum FROM (
						SELECT TOP 1 CarrierTicketNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
						UNION SELECT TOP 1 CarrierTicketNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X)
				-- use the first GaugeRun|NetVolume OriginTankID for the Order (favor first un-Rejected ticket)
				-- BB included ticket additional ticket type (Gauge Net-7)
			  , OriginTankID = (
					SELECT TOP 1 OriginTankID FROM (
						SELECT TOP 1 OriginTankID FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2,7) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
						UNION SELECT TOP 1 OriginTankID FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2,7) AND DeleteDateUTC IS NULL ORDER BY ID) X)
				-- use the first GaugeRun|NetVolume OriginTankNum for the Order (favor first un-Rejected ticket)
			  , OriginTankNum = (
					SELECT TOP 1 OriginTankNum FROM (
						SELECT TOP 1 OriginTankNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
						UNION SELECT TOP 1 OriginTankNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X)
			  , PickupLastChangeDateUTC = GETUTCDATE() -- 3.10.5.2 info tied to the pickup has been changed
			  , LastChangeDateUTC = GETUTCDATE() -- 3.8.10 update
			  , LastChangedByUser = (SELECT MIN(isnull(LastChangedByUser, CreatedByUser)) FROM inserted WHERE OrderID = O.ID)
			FROM tblOrder O
			WHERE ID IN (SELECT DISTINCT OrderID FROM inserted)

			/* START DB AUDIT *********************************************************/
			BEGIN TRY
				IF EXISTS(SELECT * FROM tblSetting WHERE ID = 32 AND (Value LIKE 'true' OR Value LIKE 'yes'))
					INSERT INTO tblOrderTicketDbAudit (DBAuditDate, ID, OrderID, CarrierTicketNum, TicketTypeID, TankNum, ProductObsGravity
													, ProductObsTemp, OpeningGaugeFeet, OpeningGaugeInch, OpeningGaugeQ, ClosingGaugeFeet, ClosingGaugeInch
													, ClosingGaugeQ, GrossUnits, NetUnits, Rejected, RejectNotes, SealOff, SealOn, BOLNum, ProductHighTemp
													, ProductLowTemp, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC
													, DeletedByUser, UID, FromMobileApp, OriginTankID, GrossStdUnits, BottomFeet, BottomInches
													, BottomQ, RejectReasonID, MeterFactor, OpenMeterUnits, CloseMeterUnits, ProductBSW, WeightTareUnits
													, WeightGrossUnits, WeightNetUnits, NoDataCalc, ScaleTicketNum)
						SELECT GETUTCDATE(), ID, OrderID, CarrierTicketNum, TicketTypeID, TankNum, ProductObsGravity, ProductObsTemp, OpeningGaugeFeet
							, OpeningGaugeInch, OpeningGaugeQ, ClosingGaugeFeet, ClosingGaugeInch, ClosingGaugeQ, GrossUnits, NetUnits, Rejected, RejectNotes
							, SealOff, SealOn, BOLNum, ProductHighTemp, ProductLowTemp, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser
							, DeleteDateUTC, DeletedByUser, UID, FromMobileApp, OriginTankID, GrossStdUnits, BottomFeet, BottomInches, BottomQ, RejectReasonID
							, MeterFactor, OpenMeterUnits, CloseMeterUnits, ProductBSW, WeightTareUnits, WeightGrossUnits, WeightNetUnits, NoDataCalc, ScaleTicketNum
						FROM deleted d
			END TRY
			BEGIN CATCH
				PRINT 'trigOrderTicket_IU.DBAUDIT FAILURE: ' + ERROR_MESSAGE()
			END CATCH
			/* END DB AUDIT *********************************************************/

			PRINT 'trigOrderTicket_IU COMPLETE'
		END
	END	
END

GO

COMMIT
SET NOEXEC OFF