SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.10.12.1'
SELECT  @NewVersion = '3.10.13'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'DCWEB-1090: Add Truck Type for bobtail trucks'
	UNION 
	SELECT @NewVersion, 0, 'Truck Type - Create tables'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

-- PART 1
-- CREATE TRUCK TYPE TABLE AND UPDATE KEY VIEWS
------------------------------------------------------------------------------------------


-- Create TruckType table
CREATE TABLE tblTruckType
(  
	ID INT IDENTITY(1, 1000) NOT NULL CONSTRAINT PK_TruckType PRIMARY KEY CLUSTERED,
	Name VARCHAR(25) NOT NULL UNIQUE,
	LoadOnTruck bit NOT NULL CONSTRAINT DF_TruckType_LoadOnTruck DEFAULT (0),
	CreateDateUTC smalldatetime NOT NULL CONSTRAINT DF_TruckType_CreateDateUTC DEFAULT GETUTCDATE(),
	CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_TruckType_CreatedByUser DEFAULT ('System'),
	LastChangeDateUTC smalldatetime NULL,
	LastChangedByUser varchar(100) NULL,
) 
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblTruckType TO role_iis_acct 
GO

-- Insert default Tractor and add new Bobtail
SET IDENTITY_INSERT tblTruckType ON
GO
INSERT INTO tblTruckType (ID, Name, LoadOnTruck)
  SELECT 1, 'Tractor', 0
  UNION 
  SELECT 2, 'Bobtail', 1  
GO
SET IDENTITY_INSERT tblTruckType OFF
GO


-- Add TruckType to Truck table
ALTER TABLE tblTruck ADD TruckTypeID INT NOT NULL 
	CONSTRAINT DF_Truck_TruckType DEFAULT (1)
	CONSTRAINT FK_Truck_TruckType FOREIGN KEY REFERENCES tblTruckType(ID)
GO


/***********************************/
-- Date Created: 26 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Truck records with FullName & translated "friendly" values
-- Changes: 3.10.13 - 2016-02-29 - JAE - Add Truck Type
/***********************************/
ALTER VIEW [dbo].[viewTruck] AS
SELECT T.*
	, cast(CASE WHEN isnull(C.DeleteDateUTC, T.DeleteDateUTC) IS NULL THEN 1 ELSE 0 END as bit) AS Active -- mark truck as active if the carrier and the truck are not deleted
	, T.IDNumber AS FullName
	, TT.Name AS TruckType
	, TT.LoadOnTruck AS LoadOnTruck
	, CT.Name AS CarrierType
	, C.Name AS Carrier
FROM dbo.tblTruck T
LEFT JOIN dbo.tblTruckType TT ON TT.ID = T.TruckTypeID
JOIN dbo.tblCarrier C ON C.ID = T.CarrierID
LEFT JOIN dbo.tblCarrierType CT ON CT.ID = C.CarrierTypeID

GO
GRANT SELECT ON viewTruck TO role_iis_acct
GO


/***********************************
-- Date Created: 26 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Truck info with Last Odometer entry 
-- Changes:
		- 3.10.13  - JAE - Add Truck Type
***********************************/
ALTER VIEW [dbo].[viewTruckWithLastOdometer] AS
	SELECT T.ID
		, CarrierID
		, IDNumber
		, DOTNumber
		, VIN
		, Make
		, Model
		, Year
		, CreateDateUTC
		, CreatedByUser
		, LastChangeDateUTC = dbo.fnMaxDateTime(LastChangeDateUTC, OdometerDateUTC)
		, LastChangedByUser
		, LicenseNumber
		, AxleCount
		, MeterType
		, HasSleeper
		, PurchasePrice
		, GarageCity
		, GarageStateID
		, RegistrationDocument
		, RegistrationDocName
		, RegistrationExpiration
		, DeleteDateUTC
		, DeletedByUser
		, InsuranceIDCardDocument
		, InsuranceIDCardDocName
		, InsuranceIDCardExpiration
		, InsuranceIDCardIssue
		, CIDNumber
		, SIDNumber
		, GPSUnit
		, OwnerInfo
		, TireSize
		, Active
		, FullName
		, CarrierType
		, Carrier
		, LastOdometer = TM.Odometer, LastOdometerDate = TM.OdometerDate
		, TruckTypeID
		, TruckType
	FROM dbo.viewTruck T
	LEFT JOIN viewOrderLastTruckMileage TM ON TM.TruckID = T.ID

GO


/*****************************************
-- Date Created: 19 Jan 2015
-- Author: Kevin Alons
-- Purpose: return the tblOrder table witha Local OrderDate field added
-- Changes:
	- ??     - 2015/05/17 - KDA - add local DeliverDate field
	- 3.8.11 - 2015/07/28 - KDA - remove OriginShipperRegion field
	- 3.9.2  - 2015/08/25 - KDA - remove computed OrderDate (it is now stored in the table directly)
	- 3.10.13 - 2015/02/29 - JAE - Add TruckType
*****************************************/
ALTER VIEW [dbo].[viewOrderBase] AS
	SELECT O.*
		, OriginStateID = OO.StateID
		, OriginRegionID = OO.RegionID
		, DestStateID = D.StateID
		, DestRegionID = D.RegionID
		, P.ProductGroupID
		, T.TruckTypeID
		, DR.DriverGroupID
		, DeliverDate = cast(dbo.fnUTC_to_Local(O.DestDepartTimeUTC, D.TimeZoneID, D.UseDST) as date) 
	FROM tblOrder O
	JOIN tblOrigin OO ON OO.ID = O.OriginID
	JOIN tblDestination D ON D.ID = O.DestinationID
	LEFT JOIN tblProduct P ON P.ID = O.ProductID
	LEFT JOIN tblTruck T ON T.ID = O.TruckID
	LEFT JOIN tblDriver DR ON DR.ID = O.DriverID

GO


/***********************************
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Order records with "translated friendly" values for FK relationships
-- Changes:
	- 3.9.20 - 2015/10/22 - KDA - add Origin|Dest DriverID fields (using new tblOrderTransfer table) 
			 - 2015/10/28 - JAE - Added all Order Transfer fields for ease of use in reporting
			 - 2015/11/03 - BB  - added cast to make TransferComplet BIT type to avoid "Operand type clash" error when running this update script
	- 3.9.21 - 2015/11/03 - KDA - add OriginDriverGroupID field (from OrderTransfer.OriginDriverID JOIN)
	- 3.9.25 - 2015/11/10 - JAE - added origin driver and truck to view
	- 3.10.10 - 2016/02/15 - BB - Add driver region ID to facilitate new user/region filtering feature
	- 3.10.11 - 2016/02/24 - JAE - Add destination region (name) to view
	- 3.10.13 - 2016/02/29 - JAE - Add TruckType
***********************************/
ALTER VIEW [dbo].[viewOrder] AS
SELECT O.*
	-- GENERATED orders that were GAUGED to COMPLETION or SKIPPED will show "Gauger [gauger status]"
	, PrintStatus = CASE WHEN O.StatusID = -10 AND GOS.IsComplete = 1 THEN 'Gauger ' + GOS.Name ELSE OPS.OrderStatus END
FROM (
	SELECT O.*
	, Origin = vO.Name 
	, OriginFull = vO.FullName 
	, OriginState = vO.State 
	, OriginStateAbbrev = vO.StateAbbrev 
	, OriginStation = vO.Station
	, OriginCounty = vO.County 
	, vO.LeaseName
	, vO.LeaseNum
	, OriginRegion = vO.Region
	, OriginCountryID = vO.CountryID
	, OriginCountry = vO.Country
	, OriginCountryShort = vO.CountryShort
	, OriginLegalDescription = vO.LegalDescription 
	, OriginNDIC = vO.NDICFileNum 
	, OriginNDM = vO.NDM 
	, OriginCA = vO.CA 
	, OriginTimeZoneID = vO.TimeZoneID 
	, OriginUseDST = vO.UseDST 
	, vO.H2S
	, Destination = vD.Name 
	, DestinationFull = vD.FullName 
	, DestinationState = vD.State 
	, DestinationStateAbbrev = vD.StateAbbrev 
	, DestinationTypeID = vD.ID
	, vD.DestinationType
	, DestStation = vD.Station 
	, DestTimeZoneID = vD.TimeZoneID 
	, DestUseDST = vD.UseDST 
	, DestRegion = vD.Region -- 3.10.13
	, DestCountryID = vD.CountryID
	, DestCountry = vD.Country
	, DestCountryShort = vD.CountryShort
	, Customer = C.Name 
	, Carrier = CA.Name 
	, CarrierType = CT.Name 
	, OS.OrderStatus
	, OS.StatusNum
	, Driver = D.FullName 
	, DriverFirst = D.FirstName 
	, DriverLast = D.LastName 
	, DriverRegionID = D.RegionID  -- 3.10.10
	, Truck = TRU.FullName 
	, Trailer = TR1.FullName 
	, Trailer2 = TR2.FullName 
	, P.PriorityNum
	, TicketType = TT.Name 
	, DestTicketTypeID = vD.TicketTypeID 
	, DestTicketType = vD.TicketType 
	, Operator = OP.Name 
	, Producer = PR.Name 
	, Pumper = PU.FullName 
	, DriverNumber = D.IDNumber
	, CarrierNumber = CA.IDNumber
	, CarrierTypeID = CA.CarrierTypeID
	, TruckNumber = TRU.IDNumber
	, TruckType = TRU.TruckType 
	, TrailerNumber = TR1.IDNumber 
	, Trailer2Number = TR2.IDNumber 
	, Product = PRO.Name
	, ProductShort = PRO.ShortName 
	, PRO.ProductGroup
	, OriginUOM = OUom.Name 
	, OriginUomShort = OUom.Abbrev 
	, OriginTankID_Text = CASE WHEN ORT.TankNum = '*' THEN '*' + isnull(O.OriginTankNum, '?') ELSE ORT.TankNum END 
	, DestUOM = DUom.Name 
	, DestUomShort = DUom.Abbrev 
	, Active = CAST((CASE WHEN O.DeleteDateUTC IS NULL THEN 1 ELSE 0 END) AS BIT) 
	, IsDeleted = CAST((CASE WHEN O.DeleteDateUTC IS NOT NULL THEN 1 ELSE 0 END) AS BIT) 
	, PickupPrintStatus = PPS.Name 
	, PickupCompleted = PPS.IsCompleted 
	, DeliverPrintStatus = DPS.Name 
	, DeliverCompleted = DPS.IsCompleted 
	, PrintStatusID = CASE WHEN O.StatusID = -9 THEN ISNULL(-GAO.StatusID, O.StatusID)
						   WHEN O.StatusID = 8 AND PPS.IsCompleted = 0 THEN 7
						   WHEN O.StatusID = 3 AND DPS.IsCompleted = 0 THEN 8
						ELSE O.StatusID END 
	, OriginTankText = CASE WHEN O.OriginTankID IS NULL OR ORT.TankNum = '*' THEN O.OriginTankNum ELSE ORT.TankNum END
	, OriginWaitNum = OWR.Num
	, OriginWaitDesc = OWR.Description
	, OriginWaitNumDesc = OWR.NumDesc
	, DestWaitNum = DWR.Num
	, DestWaitDesc = DWR.Description
	, DestWaitNumDesc = DWR.NumDesc
	, RejectNum = ORR.Num
	, RejectDesc = ORR.Description
	, RejectNumDesc = ORR.NumDesc
	, RerouteCount = (SELECT COUNT(1) FROM tblOrderReroute ORE WHERE ORE.OrderID = O.ID)
	, TicketCount = (SELECT COUNT(1) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND OT.DeleteDateUTC IS NULL)
	, TotalMinutes = ISNULL(OriginMinutes, 0) + ISNULL(DestMinutes, 0)
	-- TRANSFER FIELDS
	, OriginDriverID = ISNULL(OTR.OriginDriverID, O.DriverID)
	, OriginDriverGroupID = ISNULL(ODG.ID, DDG.ID) 
	, OriginDriverGroup = ISNULL(ODG.Name, DDG.Name)
	, OriginDriver = ISNULL(vODR.FullName, vDDR.FullName)
	, OriginDriverFirst = ISNULL(vODR.FirstName, vDDR.FirstName)
	, OriginDriverLast = ISNULL(vODR.LastName, vDDR.LastName)
	, OriginTruckID = ISNULL(OTR.OriginTruckID, O.TruckID)
	, OriginTruck = ISNULL(vOTRU.FullName, vDTRU.FullName)
	, DestDriverID = O.DriverID
	, DestDriver = D.FullName
	, DestDriverFirst = D.FirstName 
	, DestDriverLast = D.LastName 
	, DestTruckID = O.TruckID
	, DestTruck = TRU.FullName 
	, OriginTruckEndMileage = OTR.OriginTruckEndMileage
	, DestTruckStartMileage = OTR.DestTruckStartMileage
	, TransferPercent = OTR.PercentComplete
	, TransferNotes = OTR.Notes
	, TransferDateUTC = OTR.CreateDateUTC
	, TransferComplete = CAST(OTR.TransferComplete AS BIT)
	, IsTransfer = CAST((CASE WHEN OTR.OrderID IS NOT NULL THEN 1 ELSE 0 END) AS BIT)	
	--
	, GaugerStatusID = GAO.StatusID
	, GaugerID = GAO.GaugerID	
	FROM dbo.viewOrderBase O
	JOIN dbo.tblOrderStatus AS OS ON OS.ID = O.StatusID
	LEFT JOIN dbo.tblOrderTransfer OTR ON OTR.OrderID = O.ID
	LEFT JOIN dbo.viewOrigin vO ON vO.ID = O.OriginID
	LEFT JOIN dbo.viewDestination vD ON vD.ID = O.DestinationID
	LEFT JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
	LEFT JOIN dbo.tblCarrier CA ON CA.ID = O.CarrierID
	LEFT JOIN dbo.tblCarrierType CT ON CT.ID = CA.CarrierTypeID
	LEFT JOIN dbo.viewDriverBase D ON D.ID = O.DriverID
	LEFT JOIN dbo.viewDriver vODR ON vODR.ID = OTR.OriginDriverID
	LEFT JOIN dbo.viewDriver vDDR ON vDDR.ID = O.DriverID
	LEFT JOIN dbo.tblDriverGroup ODG ON ODG.ID = vODR.DriverGroupID
	LEFT JOIN dbo.tblDriverGroup DDG ON DDG.ID = O.DriverGroupID
	LEFT JOIN dbo.viewTruck TRU ON TRU.ID = O.TruckID
	LEFT JOIN dbo.viewTruck vOTRU ON vOTRU.ID = vODR.TruckID
	LEFT JOIN dbo.viewTruck vDTRU ON vDTRU.ID = O.TruckID
	LEFT JOIN dbo.viewTrailer TR1 ON TR1.ID = O.TrailerID
	LEFT JOIN dbo.viewTrailer TR2 ON TR2.ID = O.Trailer2ID
	LEFT JOIN dbo.tblPriority P ON P.ID = O.PriorityID
	LEFT JOIN dbo.tblTicketType TT ON TT.ID = O.TicketTypeID
	LEFT JOIN dbo.tblOriginTank ORT ON ORT.ID = O.OriginTankID
	LEFT JOIN dbo.tblOperator OP ON OP.ID = O.OperatorID
	LEFT JOIN dbo.viewPumper PU ON PU.ID = O.PumperID
	LEFT JOIN dbo.tblProducer PR ON PR.ID = O.ProducerID
	LEFT JOIN dbo.viewProduct PRO ON PRO.ID = O.ProductID
	LEFT JOIN dbo.tblUom OUom ON OUom.ID = O.OriginUomID
	LEFT JOIN dbo.tblUom DUom ON DUom.ID = O.DestUomID
	LEFT JOIN dbo.tblPrintStatus PPS ON PPS.ID = O.PickupPrintStatusID
	LEFT JOIN dbo.tblPrintStatus DPS ON DPS.ID = O.DeliverPrintStatusID
	LEFT JOIN dbo.viewOriginWaitReason OWR ON OWR.ID = O.OriginWaitReasonID
	LEFT JOIN dbo.viewDestinationWaitReason DWR ON DWR.ID = O.DestWaitReasonID
	LEFT JOIN dbo.viewOrderRejectReason ORR ON ORR.ID = O.RejectReasonID	
	LEFT JOIN dbo.tblGaugerOrder GAO ON GAO.OrderID = O.ID
	LEFT JOIN dbo.viewGaugerBase G ON G.ID = GAO.GaugerID
) O
LEFT JOIN dbo.viewOrderPrintStatus OPS ON OPS.ID = O.PrintStatusID
LEFT JOIN dbo.tblGaugerOrderStatus GOS ON GOS.ID = O.GaugerStatusID
GO

EXEC sp_refreshview viewOrderLocalDates
GO


/*************************************************************
-- Date Created: 15 Jun 2015
-- Author: Kevin Alons
-- Purpose: return the base data for the Order Approval page
-- Changes:
	- 3.9.2  - 2015/08/25 - KDA - remove obsolete OrderDateUTC & DeliverDateUTC fields (which were used to optimize date searches when OrderDate was virtual)
	- 3.9.20 - 2015/10/26 - JAE - Add columns for transfer percentage and override percentage
	- 3.9.25 - 2015/11/10 - JAE - Use origin driver for approval page
	- 3.9.36 - 2015/12/21 - JAE - Add columns for min settlement units (carrier and shipper)
	- 3.10.13  - 2016/02/29 - JAE - Add Truck Type
*************************************************************/
ALTER VIEW [dbo].[viewOrderApprovalSource]
AS
	SELECT O.ID
		, O.StatusID
		, Status = O.OrderStatus
		, O.OrderNum
		, O.OrderDate
		, DeliverDate = ISNULL(O.DeliverDate, O.OrderDate)
		, ShipperID = O.CustomerID
		, O.CarrierID
		, DriverID = O.OriginDriverID
		, TruckID = O.OriginTruckID
		, O.TrailerID
		, O.Trailer2ID
		, O.ProducerID
		, O.OperatorID
		, O.PumperID
		, Shipper = O.Customer
		, O.Carrier
		, Driver = O.OriginDriver
		, O.Truck
		, O.Trailer
		, O.Trailer2
		, O.Producer
		, O.Operator
		, O.Pumper
		, O.TicketTypeID
		, O.TicketType
		, O.ProductID
		, O.Product
		, O.ProductGroupID
		, O.ProductGroup
		, O.TruckTypeID
		, O.TruckType
    
		, O.Rejected
		, RejectReason = O.RejectNumDesc
		, O.RejectNotes
		, O.ChainUp
		, OA.OverrideChainup
		, O.H2S
		, OA.OverrideH2S
		, O.DispatchConfirmNum
		, O.DispatchNotes
		, O.PickupDriverNotes
		, O.DeliverDriverNotes
		, Approved = CAST(ISNULL(OA.Approved, 0) AS BIT)
		, OSS.WaitFeeParameterID, O.AuditNotes
		
		, O.OriginID
		, O.Origin
		, O.OriginStateID
		, O.OriginArriveTime
		, O.OriginDepartTime
		, O.OriginMinutes
		, O.OriginUomID
		, O.OriginUOM
		, O.OriginGrossUnits
		, O.OriginGrossStdUnits
		, O.OriginNetUnits
		, OriginWaitReason = OWR.NumDesc
		, O.OriginWaitNotes
		, ShipperOriginWaitBillableMinutes = OSS.OriginWaitBillableMinutes
		, CarrierOriginWaitBillableMinutes = OSC.OriginWaitBillableMinutes
		, OA.OverrideOriginMinutes 
		
		, O.DestinationID
		, O.Destination
		, O.DestStateID
		, O.DestArriveTime
		, O.DestDepartTime
		, O.DestMinutes
		, O.DestUomID
		, O.DestUOM
		, O.DestGrossUnits
		, O.DestNetUnits
		, O.DestOpenMeterUnits
		, O.DestCloseMeterUnits
		, DestWaitReason = OWR.NumDesc
		, O.DestWaitNotes
		, ShipperDestinationWaitBillableMinutes = OSS.DestinationWaitBillableMinutes
		, CarrierDestinationWaitBillableMinutes = OSC.DestinationWaitBillableMinutes
		, OA.OverrideDestMinutes
	
		, Rerouted = CAST(CASE WHEN ORD.OrderID IS NULL THEN 0 ELSE 1 END AS BIT)
		, OA.OverrideReroute
		, O.ActualMiles
		, RerouteMiles = ROUND(ORD.RerouteMiles, 0)
		, OA.OverrideActualMiles
		, IsTransfer = CAST(CASE WHEN OT.OrderID IS NULL THEN 0 ELSE 1 END AS BIT)
		, TransferPercentComplete = ISNULL(OT.PercentComplete,100)

		, OA.OverrideTransferPercentComplete
		, CarrierMinSettlementUnits = OSC.MinSettlementUnits
		, CarrierMinSettlementUOM = CUOM.Abbrev
		, OA.OverrideCarrierMinSettlementUnits
		, ShipperMinSettlementUnits = OSS.MinSettlementUnits
		, ShipperMinSettlementUOM = CUOM.Abbrev
		, OA.OverrideShipperMinSettlementUnits
		
		, OA.CreateDateUTC
		, OA.CreatedByUser
		
		, OrderID = OSC.OrderID | OSS.OrderID -- NULL if either are NULL - if NULL indicates that the order is not yet rates (used below to rate these)
	FROM viewOrderLocalDates O
	OUTER APPLY dbo.fnOrderRerouteData(O.ID) ORD
	LEFT JOIN tblOrderReroute ORE ON ORE.OrderID = O.ID
	LEFT JOIN tblOrderTransfer OT ON OT.OrderID = O.ID
	LEFT JOIN tblOrderSettlementCarrier OSC ON OSC.OrderID = O.ID
	LEFT JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = O.ID
	LEFT JOIN viewOriginWaitReason OWR ON OWR.ID = O.OriginWaitReasonID
	LEFT JOIN viewDestinationWaitReason DWR ON DWR.ID = O.DestWaitReasonID
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
	LEFT JOIN viewOrderSettlementUnitsCarrier OSUC ON OSUC.OrderID = O.ID
	LEFT JOIN viewOrderSettlementUnitsShipper OSUS ON OSUS.OrderID = O.ID
	LEFT JOIN tblUom CUOM ON CUOM.ID = OSUC.MinSettlementUomID
	LEFT JOIN tblUom SUOM ON SUOM.ID = OSUS.MinSettlementUomID
	WHERE O.StatusID IN (4) /* only include orders that are marked AUDITED */
	  AND OSC.BatchID IS NULL AND OSS.BatchID IS NULL /* don't show orders that have been settled */

GO

EXEC _spRebuildAllObjects
GO


COMMIT
SET NOEXEC OFF