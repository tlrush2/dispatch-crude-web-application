-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.7.14.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.7.14.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.7.15'
SELECT  @NewVersion = '3.7.16'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Adding Gauger Process data elements to Report Center'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

-- Database changes START here...

-- 05/21/15 GSM Adding Gauger Process data elements to Report Center (Part 1 of 2)...

/* ensure any existing UserReportColumn records referencing these are removed*/
DELETE FROM tblUserReportColumnDefinition WHERE ReportColumnID BETWEEN 248 AND 269
/* remove any reports that no longer have any fields (due to the above statement*/
DELETE FROM tblUserReportDefinition WHERE ID NOT IN (SELECT UserReportID FROM tblUserReportColumnDefinition)
/* remove these fields before putting them in to the DB (should never be present except for DEV environments */
DELETE FROM tblReportColumnDefinition WHERE ID BETWEEN 248 AND 269
GO

SET identity_insert tblReportColumnDefinition ON 

INSERT INTO [dbo].[tblReportColumnDefinition] ([ID],[ReportID],[DataField],[Caption],[DataFormat],[FilterDataField],[FilterTypeID],[FilterDropDownSql],[FilterAllowCustomText],[AllowedRoles],[OrderSingleExport]) 
	VALUES (248,1,'Gauger','GAUGER | Gauger',null,'GaugerID',2,'SELECT ID, Name = FullName FROM viewGauger ORDER BY FullName',1,'*',0);
INSERT INTO [dbo].[tblReportColumnDefinition] ([ID],[ReportID],[DataField],[Caption],[DataFormat],[FilterDataField],[FilterTypeID],[FilterDropDownSql],[FilterAllowCustomText],[AllowedRoles],[OrderSingleExport]) 
	VALUES (249,1,'GaugerIDNumber','GAUGER | Gauger #',null,NULL,4,null,1,'*',0);
INSERT INTO [dbo].[tblReportColumnDefinition] ([ID],[ReportID],[DataField],[Caption],[DataFormat],[FilterDataField],[FilterTypeID],[FilterDropDownSql],[FilterAllowCustomText],[AllowedRoles],[OrderSingleExport]) 
	VALUES (250,1,'GaugerFirstName','GAUGER | Gauger First Name',null,NULL,1,NULL,1,'*',0);
INSERT INTO [dbo].[tblReportColumnDefinition] ([ID],[ReportID],[DataField],[Caption],[DataFormat],[FilterDataField],[FilterTypeID],[FilterDropDownSql],[FilterAllowCustomText],[AllowedRoles],[OrderSingleExport]) 
	VALUES (251,1,'GaugerLastName','GAUGER | Gauger Last Name',null,NULL,1,NULL,1,'*',0);
INSERT INTO [dbo].[tblReportColumnDefinition] ([ID],[ReportID],[DataField],[Caption],[DataFormat],[FilterDataField],[FilterTypeID],[FilterDropDownSql],[FilterAllowCustomText],[AllowedRoles],[OrderSingleExport]) 
	VALUES (252,1,'T_GaugerProductObsTemp','GAUGER | TICKETS | Gauger Ticket Obs Temp',null,null,4,null,1,'*',0);
INSERT INTO [dbo].[tblReportColumnDefinition] ([ID],[ReportID],[DataField],[Caption],[DataFormat],[FilterDataField],[FilterTypeID],[FilterDropDownSql],[FilterAllowCustomText],[AllowedRoles],[OrderSingleExport]) 
	VALUES (253,1,'T_GaugerProductObsGravity','GAUGER | TICKETS | Gauger Ticket Obs Gravity',null,null,4,null,1,'*',0);
INSERT INTO [dbo].[tblReportColumnDefinition] ([ID],[ReportID],[DataField],[Caption],[DataFormat],[FilterDataField],[FilterTypeID],[FilterDropDownSql],[FilterAllowCustomText],[AllowedRoles],[OrderSingleExport])
	VALUES (254,1,'T_GaugerProductBSW','GAUGER | TICKETS | Gauger Ticket Obs BSW',null,null,4,null,1,'*',0);
INSERT INTO [dbo].[tblReportColumnDefinition] ([ID],[ReportID],[DataField],[Caption],[DataFormat],[FilterDataField],[FilterTypeID],[FilterDropDownSql],[FilterAllowCustomText],[AllowedRoles],[OrderSingleExport])
	VALUES (255,1,'T_GaugerOpeningGaugeFeet','GAUGER | TICKETS | Gauger Ticket Open Feet',null,null,4,null,1,'*',0)
INSERT INTO [dbo].[tblReportColumnDefinition] ([ID],[ReportID],[DataField],[Caption],[DataFormat],[FilterDataField],[FilterTypeID],[FilterDropDownSql],[FilterAllowCustomText],[AllowedRoles],[OrderSingleExport])
	VALUES (256,1,'T_GaugerOpeningGaugeInch','GAUGER | TICKETS | Gauger Ticket Open Inches',null,null,4,null,1,'*',0)
INSERT INTO [dbo].[tblReportColumnDefinition] ([ID],[ReportID],[DataField],[Caption],[DataFormat],[FilterDataField],[FilterTypeID],[FilterDropDownSql],[FilterAllowCustomText],[AllowedRoles],[OrderSingleExport])
	VALUES (257,1,'T_GaugerOpeningGaugeQ','GAUGER | TICKETS | Gauger Ticket Open Q',null,null,4,null,1,'*',0)
INSERT INTO [dbo].[tblReportColumnDefinition] ([ID],[ReportID],[DataField],[Caption],[DataFormat],[FilterDataField],[FilterTypeID],[FilterDropDownSql],[FilterAllowCustomText],[AllowedRoles],[OrderSingleExport])
	VALUES (258,1,'T_GaugerBottomFeet','GAUGER | TICKETS | Gauger Ticket Bottom Feet',null,null,4,null,1,'*',0)
INSERT INTO [dbo].[tblReportColumnDefinition] ([ID],[ReportID],[DataField],[Caption],[DataFormat],[FilterDataField],[FilterTypeID],[FilterDropDownSql],[FilterAllowCustomText],[AllowedRoles],[OrderSingleExport])
	VALUES (259,1,'T_GaugerBottomInches','GAUGER | TICKETS | Gauger Ticket Bottom Inches',null,null,4,null,1,'*',0)
INSERT INTO [dbo].[tblReportColumnDefinition] ([ID],[ReportID],[DataField],[Caption],[DataFormat],[FilterDataField],[FilterTypeID],[FilterDropDownSql],[FilterAllowCustomText],[AllowedRoles],[OrderSingleExport])
	VALUES (260,1,'T_GaugerBottomQ','GAUGER | TICKETS | Gauger Ticket Bottom Q',null,null,4,null,1,'*',0)
INSERT INTO [dbo].[tblReportColumnDefinition] ([ID],[ReportID],[DataField],[Caption],[DataFormat],[FilterDataField],[FilterTypeID],[FilterDropDownSql],[FilterAllowCustomText],[AllowedRoles],[OrderSingleExport])
	VALUES (261,1,'T_GaugerRejected','GAUGER | TICKETS | Gauger Ticket Rejected',null,NULL,5,'SELECT ID=1, Name=''True'' UNION SELECT 0, ''False''',1,'*',0)
INSERT INTO [dbo].[tblReportColumnDefinition] ([ID],[ReportID],[DataField],[Caption],[DataFormat],[FilterDataField],[FilterTypeID],[FilterDropDownSql],[FilterAllowCustomText],[AllowedRoles],[OrderSingleExport])
	VALUES (262,1,'T_GaugerRejectNumDesc','GAUGER | TICKETS | Gauger Ticket Reject Num+Desc',NULL,'T_GaugerRejectReasonID',2,'SELECT ID, Name=NumDesc FROM viewOrderTicketRejectReason WHERE DeleteDateUTC IS NULL ORDER BY NumDesc',0,'*',0)
INSERT INTO [dbo].[tblReportColumnDefinition] ([ID],[ReportID],[DataField],[Caption],[DataFormat],[FilterDataField],[FilterTypeID],[FilterDropDownSql],[FilterAllowCustomText],[AllowedRoles],[OrderSingleExport])
	VALUES (263,1,'T_GaugerRejectNotes','GAUGER | TICKETS | Gauger Ticket Reject Notes',null,NULL,1,null,1,'*',0)
INSERT INTO [dbo].[tblReportColumnDefinition] ([ID],[ReportID],[DataField],[Caption],[DataFormat],[FilterDataField],[FilterTypeID],[FilterDropDownSql],[FilterAllowCustomText],[AllowedRoles],[OrderSingleExport])
	VALUES (264,1,'GaugerPrintDate','GAUGER | Gauger Completed','[$-409]m/d/yy hh:mm AM/PM;@',NULL,6,null,1,'*',0)
INSERT INTO [dbo].[tblReportColumnDefinition] ([ID],[ReportID],[DataField],[Caption],[DataFormat],[FilterDataField],[FilterTypeID],[FilterDropDownSql],[FilterAllowCustomText],[AllowedRoles],[OrderSingleExport])
	VALUES (265,1,'GaugerRejected','GAUGER | Gauger Rejected',null,NULL,5,'SELECT ID=1, Name=''True'' UNION SELECT 0, ''False''',1,'*',0)
INSERT INTO [dbo].[tblReportColumnDefinition] ([ID],[ReportID],[DataField],[Caption],[DataFormat],[FilterDataField],[FilterTypeID],[FilterDropDownSql],[FilterAllowCustomText],[AllowedRoles],[OrderSingleExport])
	VALUES (266,1,'GaugerRejectNumDesc','GAUGER | Gauger Reject Num+Desc',null,'GaugerRejectReasonID',2,'SELECT ID, Name=NumDesc FROM viewOrderRejectReason WHERE DeleteDateUTC IS NULL ORDER BY NumDesc',1,'*',0)
INSERT INTO [dbo].[tblReportColumnDefinition] ([ID],[ReportID],[DataField],[Caption],[DataFormat],[FilterDataField],[FilterTypeID],[FilterDropDownSql],[FilterAllowCustomText],[AllowedRoles],[OrderSingleExport])
	VALUES (267,1,'GaugerRejectNotes','GAUGER | Gauger Reject Notes',null,NULL,1,null,1,'*',0)
INSERT INTO [dbo].[tblReportColumnDefinition] ([ID],[ReportID],[DataField],[Caption],[DataFormat],[FilterDataField],[FilterTypeID],[FilterDropDownSql],[FilterAllowCustomText],[AllowedRoles],[OrderSingleExport])
	VALUES (268,1,'T_GaugerCarrierTicketNum','GAUGER | TICKETS | Gauger Ticket Carrier Ticket #',null,NULL,1,null,1,'*',0)
INSERT INTO [dbo].[tblReportColumnDefinition] ([ID],[ReportID],[DataField],[Caption],[DataFormat],[FilterDataField],[FilterTypeID],[FilterDropDownSql],[FilterAllowCustomText],[AllowedRoles],[OrderSingleExport])
	VALUES (269,1,'T_GaugerTankNum','GAUGER | TICKETS | Gauger Ticket Tank #',NULL,NULL,1,NULL,1,'*',0)

SET identity_insert tblReportColumnDefinition OFF 
GO

-- 05/21/15 GSM Adding Gauger Process data elements to Report Center (Part 2 of 2)...
GO

/***********************************
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details into a single view
-- Special: 
	1) all TICKET fields should be preceded with T_ to ensure they are unique and to explicitly identify them as TICKET (not ORDER) level values
-- Changes: 3.7.16 - KDA - 5/22/2015 - added T_UID field
***********************************/
ALTER VIEW [dbo].[viewOrder_OrderTicket_Full] AS 
	SELECT OE.* 
        , T_UID =  OT.UID
		, T_ID = OT.ID
		, T_TicketType = CASE WHEN OE.TicketCount = 0 THEN OE.TicketType ELSE OT.TicketType END 
		, T_CarrierTicketNum = CASE WHEN OE.TicketCount = 0 THEN ltrim(OE.OrderNum) + CASE WHEN OE.Rejected = 1 THEN 'X' ELSE '' END ELSE OT.CarrierTicketNum END 
		, T_BOLNum = CASE WHEN OE.TicketCount = 0 THEN OE.OriginBOLNum ELSE OT.BOLNum END 
		, T_TankNum = isnull(OT.OriginTankText, OE.OriginTankText)
		, T_IsStrappedTank = OT.IsStrappedTank 
		, T_BottomFeet = OT.BottomFeet
		, T_BottomInches = OT.BottomInches
		, T_BottomQ = OT.BottomQ 
		, T_OpeningGaugeFeet = OT.OpeningGaugeFeet 
		, T_OpeningGaugeInch = OT.OpeningGaugeInch 
		, T_OpeningGaugeQ = OT.OpeningGaugeQ 
		, T_ClosingGaugeFeet = OT.ClosingGaugeFeet 
		, T_ClosingGaugeInch = OT.ClosingGaugeInch 
		, T_ClosingGaugeQ = OT.ClosingGaugeQ 
		, T_OpenTotalQ = dbo.fnGaugeQtrInches(OT.OpeningGaugeFeet, OT.OpeningGaugeInch, OT.OpeningGaugeQ) 
		, T_CloseTotalQ = dbo.fnGaugeQtrInches(OT.ClosingGaugeFeet, OT.ClosingGaugeInch, ot.ClosingGaugeQ) 
		, T_OpenReading = ltrim(OT.OpeningGaugeFeet) + 'ft ' + ltrim(OT.OpeningGaugeInch) + 'in ' + ltrim(OT.OpeningGaugeQ) + 'q' 
		, T_CloseReading = ltrim(OT.ClosingGaugeFeet) + 'ft ' + ltrim(OT.ClosingGaugeInch) + 'in ' + ltrim(OT.ClosingGaugeQ) + 'q' 
		, T_CorrectedAPIGravity = round(cast(OT.Gravity60F as decimal(9,4)), 9, 4) 
		, T_GrossStdUnits = OT.GrossStdUnits
		, T_SealOff = ltrim(OT.SealOff) 
		, T_SealOn = ltrim(OT.SealOn) 
		, T_HighTemp = OT.ProductHighTemp
		, T_LowTemp = OT.ProductLowTemp
		, T_ProductObsTemp = OT.ProductObsTemp 
		, T_ProductObsGravity = OT.ProductObsGravity 
		, T_ProductBSW = OT.ProductBSW 
		, T_Rejected = isnull(OT.Rejected, OE.Rejected)
		, T_RejectReasonID = OT.RejectReasonID
		, T_RejectNum = OT.RejectNum
		, T_RejectDesc = OT.RejectDesc
		, T_RejectNumDesc = OT.RejectNumDesc
		, T_RejectNotes = OT.RejectNotes 
		, T_GrossUnits = OT.GrossUnits 
		, T_NetUnits = OT.NetUnits 
		, T_MeterFactor = OT.MeterFactor
		, T_OpenMeterUnits = OT.OpenMeterUnits
		, T_CloseMeterUnits = OT.CloseMeterUnits
		, T_DispatchConfirmNum = CASE WHEN OE.TicketCount = 0 THEN OE.DispatchConfirmNum ELSE isnull(OT.DispatchConfirmNum, OE.DispatchConFirmNum) END
		, PreviousDestinations = dbo.fnRerouteDetails(OE.ID, 'PreviousDestinations', '<br/>') 
		, RerouteUsers = dbo.fnRerouteDetails(OE.ID, 'RerouteUsers', '<br/>') 
		, RerouteDates = dbo.fnRerouteDetails(OE.ID, 'RerouteDates', '<br/>') 
		, RerouteNotes = dbo.fnRerouteDetails(OE.ID, 'RerouteNotes', '<br/>')
	FROM dbo.viewOrderLocalDates OE
	LEFT JOIN viewOrderTicket OT ON OT.OrderID = OE.ID AND OT.DeleteDateUTC IS NULL
	WHERE OE.DeleteDateUTC IS NULL

GO

/**********************************
-- Date Created: 18 Apr 2015
-- Author: Kevin Alons
-- Purpose: return GaugerOrder records with "translated friendly" values for FK relationships
-- Changes:
	-- 22 May 2015 - KDA - add "TicketCount" field
***********************************/
ALTER VIEW [viewGaugerOrder] AS
SELECT GAO.*
	, OrderNum = O.OrderNum
	, OrderDate = O.OrderDate
	, Status = GOS.Name
	, CarrierTicketNum = O.CarrierTicketNum
	, DispatchConfirmNum = O.DispatchConfirmNum
	, Gauger = G.FullName
	, OriginID = O.OriginID
	, Origin = O.Origin
	, OriginFull = O.OriginFull
	, OriginState = O.OriginState
	, OriginStateAbbrev = O.OriginStateAbbrev
	, CustomerID = O.CustomerID
	, ShipperID = O.CustomerID
	, Shipper = O.Customer
	, DestinationID = O.DestinationID
	, Destination = O.Destination
	, DestinationFull = O.DestinationFull
	, DestinationState = O.DestinationState
	, DestinationStateAbbrev = O.DestinationStateAbbrev
	, ProductID = O.ProductID
	, Product = O.Product
	, ProductShort = O.ProductShort
	, ProductGroupID = O.ProductGroupID
	, ProductGroup = O.ProductGroup
	, OriginTankText = ISNULL(OT.TankNum, GAO.OriginTankNum)
	, P.PriorityNum
	, TicketType = GTT.Name
	, PrintStatusID = O.PrintStatusID
	, OrderStatusID = O.StatusID
	, DeleteDateUTC = O.DeleteDateUTC
	, DeletedByUser = O.DeletedByUser
	, TicketCount = (SELECT COUNT(*) FROM tblGaugerOrderTicket GOT WHERE GOT.OrderID = GAO.OrderID AND GOT.DeleteDateUTC IS NULL)
FROM dbo.tblGaugerOrder GAO 
JOIN viewOrder O ON O.ID = GAO.OrderID 
JOIN dbo.tblGaugerOrderStatus GOS ON GOS.ID = GAO.StatusID
LEFT JOIN dbo.tblOriginTank OT ON OT.ID = GAO.OriginTankID
LEFT JOIN dbo.viewGauger G ON G.ID = GAO.GaugerID
LEFT JOIN dbo.tblGaugerTicketType GTT ON GTT.ID = GAO.TicketTypeID
LEFT JOIN tblPriority P ON P.ID = GAO.PriorityID

GO

EXEC _spDropView 'viewGaugerOrderTicket'
GO
/***********************************/
-- Date Created: 22 May 2015
-- Author: Kevin Alons
-- Purpose: query the tblGaugerOrderTicket table and include "friendly" translated values
-- Changes:
/***********************************/
CREATE VIEW viewGaugerOrderTicket AS
SELECT GOT.*
	, TicketType = TT.Name 
	, OriginTankText = CASE WHEN GOT.OriginTankID IS NULL OR ORT.TankNum = '*' THEN GOT.TankNum ELSE ORT.TankNum END 
	, IsStrappedTank = cast(CASE WHEN GOT.OriginTankID IS NULL OR ORT.TankNum = '*' THEN 0 ELSE 1 END as bit) 
	, Active = cast((CASE WHEN GOT.DeleteDateUTC IS NULL THEN 1 ELSE 0 END) as bit) 
	, IsDeleted = cast((CASE WHEN isnull(OT.DeleteDateUTC, GOT.DeleteDateUTC) IS NOT NULL THEN 1 ELSE 0 END) as bit) 
	, Gravity60F = dbo.fnCorrectedAPIGravity(GOT.ProductObsGravity, GOT.ProductObsTemp)
	, RejectNum = OTRR.Num
	, RejectDesc = OTRR.Description
	, RejectNumDesc = OTRR.NumDesc
FROM tblGaugerOrderTicket GOT
JOIN tblOrderTicket OT ON OT.UID = GOT.UID
JOIN tblTicketType TT ON TT.ID = GOT.TicketTypeID
LEFT JOIN tblOriginTank ORT ON ORT.ID = GOT.OriginTankID
LEFT JOIN dbo.viewOrderTicketRejectReason OTRR ON OTRR.ID = GOT.RejectReasonID

GO
GRANT SELECT ON viewGaugerOrderTicket TO role_iis_acct
GO

/***********************************
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Report Center Order data
-- Special: 
	1) all TICKET fields should be preceded with T_ to ensure they are unique and to explicitly identify them as TICKET (not ORDER) level values
	2) field Prefixed with "Shipper|Carrier|Gauger|etc" should remain the same with the prefix added (ie: SS.BatchNum -> ShipperBatchNum)
-- Changes:
   -- 05/21/15 GSM Adding Gauger Process data elements to Report Center
***********************************/
ALTER VIEW [dbo].[viewReportCenter_Orders] AS
	SELECT O.*
		, ShipperBatchNum = SS.BatchNum
		, ShipperBatchInvoiceNum = SSB.InvoiceNum
		, ShipperSettlementUomID = SS.SettlementUomID
		, ShipperSettlementUom = SS.SettlementUom
		, ShipperMinSettlementUnits = SS.MinSettlementUnits
		, ShipperSettlementUnits = SS.SettlementUnits
		, ShipperRateSheetRate = SS.RateSheetRate
		, ShipperRateSheetRateType = SS.RateSheetRateType
		, ShipperRouteRate = SS.RouteRate
		, ShipperRouteRateType = SS.RouteRateType
		, ShipperLoadRate = isnull(SS.RouteRate, SS.RateSheetRate)
		, ShipperLoadRateType = isnull(SS.RouteRateType, SS.RateSheetRateType)
		, ShipperLoadAmount = SS.LoadAmount
		, ShipperOrderRejectRate = SS.OrderRejectRate -- changed
		, ShipperOrderRejectRateType = SS.OrderRejectRateType -- new
		, ShipperOrderRejectAmount = SS.OrderRejectAmount -- changed
		, ShipperWaitFeeSubUnit = SS.WaitFeeSubUnit  -- new
		, ShipperWaitFeeRoundingType = SS.WaitFeeRoundingType  -- new
		, ShipperOriginWaitBillableHours = SS.OriginWaitBillableHours  -- new
		, ShipperOriginWaitBillableMinutes = SS.OriginWaitBillableMinutes  -- new
		, ShipperOriginWaitRate = SS.OriginWaitRate
		, ShipperOriginWaitAmount = SS.OriginWaitAmount
		, ShipperDestinationWaitBillableHours = SS.DestinationWaitBillableHours  -- new
		, ShipperDestinationWaitBillableMinutes = SS.DestinationWaitBillableMinutes   -- new
		, ShipperDestinationWaitRate = SS.DestinationWaitRate  -- changed
		, ShipperDestinationWaitAmount = SS.DestinationWaitAmount  -- changed
		, ShipperTotalWaitAmount = SS.TotalWaitAmount
		, ShipperTotalWaitBillableMinutes = nullif(ISNULL(SS.OriginWaitBillableMinutes, 0) + ISNULL(SS.DestinationWaitBillableMinutes, 0), 0)
		, ShipperTotalWaitBillableHours = nullif(ISNULL(SS.OriginWaitBillableHours, 0) + ISNULL(SS.DestinationWaitBillableHours, 0), 0)
		
		, ShipperFuelSurchargeRate = SS.FuelSurchargeRate
		, ShipperFuelSurchargeAmount = SS.FuelSurchargeAmount

		, ShipperChainupRate = SS.ChainupRate
		, ShipperChainupRateType = SS.ChainupRateType  -- new
		, ShipperChainupAmount = SS.ChainupAmount
		, ShipperRerouteRate = SS.RerouteRate
		, ShipperRerouteRateType = SS.RerouteRateType  -- new
		, ShipperRerouteAmount = SS.RerouteAmount
		, ShipperSplitLoadRate = SS.SplitLoadRate
		, ShipperSplitLoadRateType = SS.SplitLoadRateType  -- new
		, ShipperSplitLoadAmount = SS.SplitLoadAmount
		, ShipperH2SRate = SS.H2SRate
		, ShipperH2SRateType = SS.H2SRateType  -- new
		, ShipperH2SAmount = SS.H2SAmount

		, ShipperTaxRate = SS.OriginTaxRate
		, ShipperTotalAmount = SS.TotalAmount

		, CarrierBatchNum = SC.BatchNum
		, CarrierSettlementUomID = SC.SettlementUomID
		, CarrierSettlementUom = SC.SettlementUom
		, CarrierMinSettlementUnits = SC.MinSettlementUnits
		, CarrierSettlementUnits = SC.SettlementUnits
		, CarrierRateSheetRate = SC.RateSheetRate
		, CarrierRateSheetRateType = SC.RateSheetRateType
		, CarrierRouteRate = SC.RouteRate
		, CarrierRouteRateType = SC.RouteRateType
		, CarrierLoadRate = isnull(SC.RouteRate, SC.RateSheetRate)
		, CarrierLoadRateType = isnull(SC.RouteRateType, SC.RateSheetRateType)
		, CarrierLoadAmount = SC.LoadAmount
		, CarrierOrderRejectRate = SC.OrderRejectRate -- changed
		, CarrierOrderRejectRateType = SC.OrderRejectRateType -- new
		, CarrierOrderRejectAmount = SC.OrderRejectAmount -- changed
		, CarrierWaitFeeSubUnit = SC.WaitFeeSubUnit  -- new
		, CarrierWaitFeeRoundingType = SC.WaitFeeRoundingType  -- new
		, CarrierOriginWaitBillableHours = SS.OriginWaitBillableHours  -- new
		, CarrierOriginWaitBillableMinutes = SC.OriginWaitBillableMinutes  -- new
		, CarrierOriginWaitRate = SC.OriginWaitRate
		, CarrierOriginWaitAmount = SC.OriginWaitAmount
		, CarrierDestinationWaitBillableHours = SS.DestinationWaitBillableHours  -- new
		, CarrierDestinationWaitBillableMinutes = SC.DestinationWaitBillableMinutes  -- new
		, CarrierDestinationWaitRate = SC.DestinationWaitRate -- changed
		, CarrierDestinationWaitAmount = SC.DestinationWaitAmount  -- changed
		, CarrierTotalWaitAmount = SC.TotalWaitAmount
		, CarrierTotalWaitBillableMinutes = nullif(ISNULL(SS.OriginWaitBillableMinutes, 0) + ISNULL(SS.DestinationWaitBillableMinutes, 0), 0)
		, CarrierTotalWaitBillableHours = nullif(ISNULL(SS.OriginWaitBillableHours, 0) + ISNULL(SS.DestinationWaitBillableHours, 0), 0)
		
		, CarrierFuelSurchargeRate = SC.FuelSurchargeRate
		, CarrierFuelSurchargeAmount = SC.FuelSurchargeAmount

		, CarrierChainupRate = SC.ChainupRate
		, CarrierChainupRateType = SC.ChainupRateType  -- new
		, CarrierChainupAmount = SC.ChainupAmount
		, CarrierRerouteRate = SC.RerouteRate
		, CarrierRerouteRateType = SC.RerouteRateType  -- new
		, CarrierRerouteAmount = SC.RerouteAmount
		, CarrierSplitLoadRate = SC.SplitLoadRate
		, CarrierSplitLoadRateType = SC.SplitLoadRateType  -- new
		, CarrierSplitLoadAmount = SC.SplitLoadAmount
		, CarrierH2SRate = SC.H2SRate
		, CarrierH2SRateType = SC.H2SRateType  -- new
		, CarrierH2SAmount = SC.H2SAmount

		, CarrierTaxRate = SC.OriginTaxRate
		, CarrierTotalAmount = SC.TotalAmount

		, OriginGpsLatLon = ltrim(DLO.Lat) + ',' + ltrim(DLO.Lon)
		, OriginLatLon = ltrim(OO.LAT) + ',' + ltrim(OO.LON)
		, OriginAccuracyMeters = DLO.SourceAccuracyMeters
		, OriginDistance = isnull(cast(DLO.DistanceToPoint as int), 99999)
		, OriginGpsArrived = CASE WHEN DLO.DistanceToPoint IS NOT NULL AND DLO.DistanceToPoint BETWEEN 0 AND OO.GeoFenceRadiusMeters THEN 1 ELSE 0 END
		, DestGpsLatLon = ltrim(DLD.Lat) + ',' + ltrim(DLD.Lon)
		, DestLatLon = ltrim(D.LAT) + ',' + ltrim(D.LON)
		, DestAccuracyMeters = DLD.SourceAccuracyMeters
		, DestDistance = isnull(cast(DLD.DistanceToPoint as int), 99999)
		, DestGpsArrived = CASE WHEN DLD.DistanceToPoint IS NOT NULL AND DLD.DistanceToPoint BETWEEN 0 AND D.GeoFenceRadiusMeters THEN 1 ELSE 0 END
		, ShipperDestCode = CDC.Code
		, OriginCTBNum = OO.CTBNum
		, OriginFieldName = OO.FieldName
		--
		, Gauger = GAO.Gauger						-- 150521 GM BB
		, GaugerIDNumber = GA.IDNumber
		, GaugerFirstName = GA.FirstName
		, GaugerLastName = GA.LastName
		, GaugerRejected = GAO.Rejected
		, GaugerRejectReasonID = GAO.RejectReasonID
		, GaugerRejectNotes = GAO.RejectNotes
		, GaugerRejectNumDesc = GORR.NumDesc
		, GaugerPrintDate = dbo.fnUTC_to_Local(GAO.PrintDateUTC, O.OriginTimeZoneID, O.OriginUseDST)
		--
		, T_GaugerCarrierTicketNum = CASE WHEN GAO.TicketCount = 0 THEN ltrim(GAO.OrderNum) + CASE WHEN GAO.Rejected = 1 THEN 'X' ELSE '' END ELSE GOT.CarrierTicketNum END 
		, T_GaugerTankNum = isnull(GOT.OriginTankText, GAO.OriginTankText)
		, T_GaugerIsStrappedTank = GOT.IsStrappedTank 
		, T_GaugerProductObsTemp = GOT.ProductObsTemp
		, T_GaugerProductObsGravity = GOT.ProductObsGravity
		, T_GaugerProductBSW = GOT.ProductBSW		
		, T_GaugerOpeningGaugeFeet = GOT.OpeningGaugeFeet
		, T_GaugerOpeningGaugeInch = GOT.OpeningGaugeInch		
		, T_GaugerOpeningGaugeQ = GOT.OpeningGaugeQ			
		, T_GaugerBottomFeet = GOT.BottomFeet
		, T_GaugerBottomInches = GOT.BottomInches		
		, T_GaugerBottomQ = GOT.BottomQ		
		, T_GaugerRejected = GOT.Rejected
		, T_GaugerRejectReasonID = GOT.RejectReasonID
		, T_GaugerRejectNumDesc = GOT.RejectNumDesc
		, T_GaugerRejectNotes = GOT.RejectNotes
			
	FROM viewOrder_OrderTicket_Full O
	JOIN tblOrigin OO ON OO.ID = O.OriginID
	JOIN tblDestination D ON D.ID = O.DestinationID
	--
    LEFT JOIN viewGaugerOrder GAO ON GAO.OrderID = O.ID			            -- 150521 GSM BB
    LEFT JOIN viewGaugerOrderTicket GOT ON GOT.UID = O.T_UID	            -- 150521 GSM BB
    LEFT JOIN viewGauger GA ON GA.ID = GAO.GaugerID				            -- 150521 GSM BB
    LEFT JOIN viewOrderRejectReason GORR ON GORR.ID = GAO.OrderID -- 150522 GSM
    --
    LEFT JOIN tblCustomerDestinationCode CDC ON CDC.CustomerID = O.CustomerID AND CDC.DestinationID = O.DestinationID
	LEFT JOIN viewDriverLocation_OriginFirstArrive DLO ON DLO.OrderID = O.ID AND DLO.OriginID = O.OriginID
	LEFT JOIN viewDriverLocation_DestinationFirstArrive DLD ON DLD.OrderID = O.ID AND DLD.DestinationID = O.DestinationID
	LEFT JOIN viewOrderSettlementCarrier SC ON SC.OrderID = O.ID
	LEFT JOIN viewOrderSettlementShipper SS ON SS.OrderID = O.ID
	LEFT JOIN tblShipperSettlementBatch SSB ON SSB.ID = SS.BatchID

GO

-- ... and the database changes end here.

EXEC _spRebuildAllObjects
GO

COMMIT
SET NOEXEC OFF