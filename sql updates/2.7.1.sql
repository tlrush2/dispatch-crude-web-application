/*  
	-- revise the Sunoco Sundex export format
*/ 
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.7.0'
SELECT  @NewVersion = '2.7.1'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

/*****************************************************************************************/
-- Author: Kevin Alons
-- Date Created: 27 Feb 2014
-- Purpose: return export final orders in the SunocoSundex
-- TODO: this has some hardcoded values for the SUNOCO customer (shipper), need to make dynamic
/*****************************************************************************************/
ALTER VIEW [dbo].[viewSunocoSundex] AS
SELECT
	O.ID AS _ID
	, O.CustomerID AS _CustomerID
	, O.OrderNum AS _OrderNum
	, CASE WHEN EP.IsNew = 1 THEN 'A' ELSE 'C' END AS Request_Code
	, '0007020233' AS Company_Code
	, UPPER(left(O.TicketType, 1)) AS Ticket_Type
	, 'BATA' AS Ticket_Source_Code
	, T.CarrierTicketNum AS Ticket_Number
	, dbo.fnDateMMddYYYY(O.OrderDate) AS Ticket_Date
	, O.OriginStation AS SXL_Property_Code
	, '' AS TP_Property_Code
	, O.Origin AS Lease_Company_Name
	, CDC.Code AS Destination
	, T.TankNum AS Tank_Meter_Number
	, dbo.fnDateMMddYYYY(O.OriginArriveTime) AS Open_Date
	, dbo.fnTimeOnly(O.OriginArriveTime) AS Open_Time
	, dbo.fnDateMMddYYYY(O.OriginDepartTime) AS Close_Date
	, dbo.fnTimeOnly(O.OriginDepartTime) AS Close_Time
	, cast(ROUND(O.OriginGrossUnits, 2) as decimal(18, 2)) AS Estimated_Volume
	, cast(ROUND(O.OriginGrossUnits, 2) as decimal(18, 2)) AS Gross_Volume
	, cast(ROUND(O.OriginNetUnits, 2) as decimal(18, 2)) AS Net_Volume
	, T.ProductObsGravity AS Observed_Gravity
	, T.ProductObsTemp AS Observed_Temperature
	, T.ProductBSW AS Observed_BSW
	, 0 AS Corrected_Gravity_API
	, 'Sonoco Logistics' AS Purchaser
	, T.OpeningGaugeFeet AS First_Reading_Gauge_Ft
	, T.OpeningGaugeInch AS First_Reading_Gauge_In
	, T.OpeningGaugeQ AS First_Reading_Gauge_Nu
	, 4 AS First_Reading_Gauge_De
	, T.ProductHighTemp AS First_Temperature
	, T.BottomFeet AS First_Bottom_Ft
	, T.BottomInches AS First_Bottom_In
	, T.BottomQ AS First_Bottom_Nu
	, 4 AS First_Bottom_De
	, T.ClosingGaugeFeet AS Second_Reading_Gauge_Ft
	, T.ClosingGaugeInch AS Second_Reading_Gauge_In
	, T.ClosingGaugeQ AS Second_Reading_Gauge_Nu
	, 4 AS Second_Reading_Gauge_De
	, T.ProductLowTemp AS Second_Temperature
	, 0 AS Second_Bottom_Ft
	, 0 AS Second_Bottom_In
	, 0 AS Second_Bottom_Nu
	, 4 AS Second_Bottom_De
	, '' AS Shrinkage_Incrustation_Factor
	, 0 AS First_Reading_Meter
	, 0 AS Second_Reading_Meter
	, 0 AS Meter_Factor
	, '' AS Temp_Comp_Meter
	, '' AS Avg_Line_Temp
	, '' AS Truck_ID
	, '' AS Trailer_ID
	, '' AS Driver_ID
	, '' AS Miles
	, '' AS CountyState
	, '' AS Invoice_Number
	, '' AS Invoice_Date
	, '' AS Remarks
	, '' AS API_Compliant_Chapter
	, 'Y' AS Use_SXL_Calculation
	, T.SealOn AS Seal_On
	, T.SealOff AS Seal_Off
	, CASE WHEN T.Rejected = 1 THEN 'RF' ELSE '' END AS Ticket_Exclusion_Cd
	, '9999999999' AS Confirmation_Number
	, CASE WHEN (SELECT COUNT(*) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND DeleteDateUTC IS NULL) > 1 THEN 'Y' ELSE 'N' END AS Split_Flag
	, (SELECT min(CarrierTicketNum) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND DeleteDateUTC IS NULL AND OT.CarrierTicketNum <> T.CarrierTicketNum) AS Paired_Ticket_Number
	, 'N' AS Bobtail_Flag
	, '' AS Ticket_Extra_Info_Flag
FROM viewOrderLocalDates O
JOIN dbo.viewOrderCustomerFinalExportPending EP ON EP.ID = O.ID
LEFT JOIN viewOrderTicket T ON T.OrderID = O.ID AND T.DeleteDateUTC IS NULL
LEFT JOIN dbo.tblCustomerDestinationCode CDC ON CDC.DestinationID = O.DestinationID

GO

COMMIT
SET NOEXEC OFF