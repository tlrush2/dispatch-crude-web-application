-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.4.2'
SELECT  @NewVersion = '3.4.3'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Add SYSTEM SETTING "System Wide.Country"'
	UNION SELECT @NewVersion, 0, 'update viewOrigin to show Country fields'
GO

SET IDENTITY_INSERT tblTicketType ON
INSERT INTO tblTicketType (ID, Name, CreateDateUTC, CreatedByUser, ForTanksOnly, CountryID_CSV)
	SELECT 6, 'CAN Meter Run', GETUTCDATE(), 'System', 1, '2'
SET IDENTITY_INSERT tblTicketType OFF
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fnOriginProductsCSV]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION [dbo].[fnOriginProductsCSV]
GO
-- =============================================
-- Author:		Kevin Alons
-- Create date: 3 Oct 2014
-- Description:	return Products [CSV] field for the specified Origin
-- =============================================
CREATE FUNCTION fnOriginProductsCSV(@originID int) RETURNS varchar(1000) AS BEGIN
	DECLARE @ret varchar(max)
	SELECT @ret = 
	  STUFF(
		(
		  SELECT ', ' + ShortName
		  FROM tblProduct P
		  WHERE ID IN (SELECT ProductID FROM tblOriginProducts OP WHERE OP.OriginID = @originID)
		  ORDER BY ShortName
		  FOR XML PATH(''),TYPE
		  ).value('.','VARCHAR(MAX)'
		), 1, 1, '')

	RETURN (@ret)
END

GO
GRANT EXECUTE ON fnOriginProductsCSV TO dispatchcrude_iis_acct
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fnOriginProductIDCSV]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION [dbo].[fnOriginProductIDCSV]
GO
-- =============================================
-- Author:		Kevin Alons
-- Create date: 3 Oct 2014
-- Description:	return Products [CSV] field for the specified Origin
-- =============================================
CREATE FUNCTION fnOriginProductIDCSV(@originID int) RETURNS varchar(1000) AS BEGIN
	DECLARE @ret varchar(max)
	SELECT @ret = 
	  STUFF(
		(
		  SELECT ',' + ltrim(ProductID)
		  FROM tblOriginProducts
		  WHERE OriginID = @originID
		  FOR XML PATH(''),TYPE
		  ).value('.','VARCHAR(MAX)'
		), 1, 1, '')

	RETURN (@ret)
END

GO
GRANT EXECUTE ON fnOrigiNProductIDCSV TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Origin records with translated value and FullName field (which includes Origin Type)
/***********************************/
ALTER VIEW [dbo].[viewOrigin] AS
SELECT O.*
	, FullName = CASE WHEN O.H2S = 1 THEN 'H2S-' ELSE '' END + OT.OriginType + ' - ' + O.Name
	, OT.OriginType
	, State = S.FullName 
	, StateAbbrev = S.Abbreviation 
	, S.CountryID
	, Country = CO.Name, CountryShort = CO.Abbrev
	, Operator = OP.Name
	, Pumper = P.FullName
	, TicketType = TT.Name
	, TT.ForTanksOnly
	, Region = R.Name
	, Customer = C.Name 
	, Producer = PR.Name 
	, TimeZone = TZ.Name 
	, UOM = UOM.Name 
	, UomShort = UOM.Abbrev 
	, OT.HasTanks
	, Active = cast(CASE WHEN O.DeleteDateUTC IS NULL THEN 1 ELSE 0 END as bit)
	, TankCount = (SELECT COUNT(*) FROM tblOriginTank WHERE OriginID = O.ID AND DeleteDateUTC IS NULL) 
FROM dbo.tblOrigin O
JOIN dbo.tblOriginType OT ON OT.ID = O.OriginTypeID
LEFT JOIN tblState S ON S.ID = O.StateID
LEFT JOIN tblCountry CO ON CO.ID = S.CountryID
LEFT JOIN dbo.tblOperator OP ON OP.ID = O.OperatorID
LEFT JOIN dbo.viewPumper P ON P.ID = O.PumperID
LEFT JOIN dbo.tblTicketType TT ON TT.ID = O.TicketTypeID
LEFT JOIN dbo.tblRegion R ON R.ID = O.RegionID
LEFT JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
LEFT JOIN dbo.tblProducer PR ON PR.ID = O.ProducerID
LEFT JOIN dbo.tblTimeZone TZ ON TZ.ID = O.TimeZoneID
LEFT JOIN dbo.tblUom UOM ON UOM.ID = O.UomID

GO

COMMIT
SET NOEXEC OFF