-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.11.6.4'
SELECT  @NewVersion = '3.11.7'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Updated sync to include logging mobile app version'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO
/*******************************************
-- Date Created: 16 Mar 2016
-- Author: Jeremiah Silliman
-- Purpose: Insert record with sync data listing driver, sync time and most recent mobile application version
-- Changes: 3.11.7 2016/03/23 - JS - Updated spDriver_Update to cleanup SQL
*******************************************/
ALTER PROCEDURE [dbo].[spDriver_Update]
(
  @TabletID varchar(20)
, @PrinterSerial varchar(100)
, @MobileAppVersion varchar(100)
, @DriverID int
) AS
BEGIN
IF (@MobileAppVersion IS NOT NULL AND @MobileAppVersion <> '')
	BEGIN
		INSERT INTO tblDriverUpdate (DriverID, MobileAppVersion, UpdateLastUTC, TabletID, PrinterSerial)
			SELECT @DriverID, @MobileAppVersion, GETUTCDATE(), @TabletID, @PrinterSerial 
			WHERE NOT EXISTS (SELECT MobileAppVersion FROM tblDriverUpdate DUT WHERE DriverID = @DriverID AND MobileAppVersion = @MobileAppVersion)
	END
END
GO
/*******************************************
-- Date Created: 22 Apr 2013
-- Author: Kevin Alons
-- Purpose: validate parameters, if valid insert/update the tblDriver_Sync table for the specified DriverID
-- Code References:
					DispatchCrude/Models/Sync/DriverApp/MasterData.cs
-- Changes: 3.10.7 - 2016/01/28 - BB - Added TabletID and PrinterSerial to the synced values
            3.10.7 - 2016/02/01 - KDA - Added EXEC spDriverSync_UpdateDevices @TabletID, @PrinterSerial, @UserName
			3.11.7 - 2016/03/22 - JS - Added call to stored procedure to log mobile app version
*******************************************/
ALTER PROCEDURE [dbo].[spDriver_Sync]
(
  @UserName varchar(100)
, @DriverID int
, @MobileAppVersion varchar(25) = NULL
, @SyncDateUTC datetime = NULL
, @PasswordHash varchar(25) = NULL
, @TabletID varchar(30) = NULL
, @PrinterSerial varchar(30) = NULL
, @Valid bit = NULL output
, @Message varchar(255) = NULL output
) AS
BEGIN
	-- if resetting, delete the entire record (it will be recreated below)
	IF (@SyncDateUTC IS NULL)
	BEGIN
		DELETE FROM tblDriver_Sync WHERE DriverID = @DriverID
		-- result of count(*) query will be 1 or 0 (true or false respectively)
		SELECT @Valid = (SELECT COUNT(*) FROM tblDriver WHERE ID = @DriverID)
		IF (@Valid = 0)
			SELECT @Message = 'DriverID was not valid'
	END
	ELSE
	BEGIN
		-- query the ValidatePasswordHash setting parameter
		DECLARE @ValidatePasswordHash bit
		SELECT @ValidatePasswordHash = CASE WHEN Value IN ('1', 'true', 'yes') THEN 1 ELSE 0 END
		FROM tblSetting
		WHERE ID = 13

		-- result of count(*) query will be 1 or 0 (true or false respectively)
		SELECT @Valid = (SELECT count(*) FROM tblDriver_Sync WHERE DriverID = @DriverID 
			AND (@ValidatePasswordHash = 0 OR PasswordHash = @PasswordHash))
		IF (@Valid = 0)
			SELECT @Message = 'PasswordHash was not valid'
	END
	
	EXEC spDriverSync_UpdateDevices @TabletID, @PrinterSerial, @UserName
	-- Log the version history
	EXEC spDriver_Update @TabletID, @PrinterSerial, @MobileAppVersion, @DriverID

	IF (@Valid = 1)
	BEGIN
		-- if a sync record already exists, just update the new LastSync value
		UPDATE tblDriver_Sync 
		SET LastSyncUTC = @SyncDateUTC
		, MobileAppVersion = @MobileAppVersion
		, TabletID = @TabletID
		, PrinterSerial = @PrinterSerial 
		WHERE DriverID = @DriverID
		-- otherwise insert a new record with a new passwordhash value
		INSERT INTO tblDriver_Sync (DriverID, MobileAppVersion, LastSyncUTC, PasswordHash, TabletID, PrinterSerial)
			SELECT @DriverID, @MobileAppVersion, NULL, dbo.fnGeneratePasswordHash(), @TabletID, @PrinterSerial
			FROM tblDriver D
			LEFT JOIN tblDriver_Sync DS ON DS.DriverID = D.ID
			WHERE D.ID = @DriverID AND DS.DriverID IS NULL

        
        

		-- return the current "Master" data
		SELECT *, TabletID = @TabletID, PrinterSerial = @PrinterSerial FROM dbo.fnDriverMasterData(@Valid, @DriverID, @UserName)
	END
	ELSE
	BEGIN
		SELECT @Valid AS Valid, @UserName AS UserName, 0 AS DriverID, NULL AS DriverName, 0 AS MobilePrint
			, NULL AS LastSyncUTC
			, (SELECT cast(Value as int) FROM tblSetting WHERE ID = 11) AS SyncMinutes
			, (SELECT Value FROM tblSetting WHERE ID = 0) AS SchemaVersion
			, @MobileAppVersion AS MobileAppVersion			
			, (SELECT Value FROM tblSetting WHERE ID = 12) AS LatestAppVersion
			, NULL AS PasswordHash
			, @TabletID AS TabletID
			, @PrinterSerial AS PrinterSerial
		FROM tblSetting S WHERE S.ID = 0
	END 
	
END

GO

COMMIT
SET NOEXEC OFF