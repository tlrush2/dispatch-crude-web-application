SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.10.13.5'
SELECT  @NewVersion = '3.10.13.6'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Truck Type - Update order scripts'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


-- PART 7
-- UPDATE THE CORE ORDER SETTLEMENT SCRIPTS
--------------------------------------------------------------------------------------------

-- UPDATE CARRIER FUNCTIONS/STORED PROCEDURES

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier AssessorialRate info for the specified order
-- Changes:
	- 3.7.28 - 2015/06/18 - KDA - add new columns: add DriverID/DriverGroupID parameters, move ProducerID parameter to end (so consistent with all other Best-Match rates/parameters
	- 3.7.30 - 2015/06/19 - KDA - remove DriverID
	- 3.9.19.6 - 2015/09/30 - KDA - honor Order Approval . Override values when computing Rates
	- 3.9.21 - 2015/10/03 - KDA - use [viewOrder] O.OriginDriverGroupID instead of O.DriverGroupID (which could be DestDriver.DriverGroupID)
	- 3.10.13.6 - 2015/03/01 - JAE - add TruckTypeID
***********************************/
ALTER FUNCTION [dbo].[fnOrderCarrierAssessorialRates](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, TypeID, Rate, RateTypeID, UomID
	FROM dbo.viewOrder O
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
	CROSS APPLY dbo.fnCarrierAssessorialRates(O.OrderDate, null, null, O.CustomerID, O.CarrierID, O.ProductGroupID, O.TruckTypeID, O.OriginDriverGroupID, O.OriginID, O.DestinationID, O.OriginStateID, O.DestStateID, O.OriginRegionID, O.ProducerID, 1) R
	WHERE O.ID = @ID
	  AND ((R.TypeID = 1 AND CASE WHEN isnull(OA.OverrideChainup, 0) = 1 THEN 0 ELSE O.ChainUp END = 1)
		OR (R.TypeID = 2 AND CASE WHEN isnull(OA.OverrideReroute, 0) = 1 THEN 0 ELSE O.RerouteCount END > 0)
		OR (R.TypeID = 3 AND O.TicketCount > 1)
		OR (R.TypeID = 4 AND CASE WHEN isnull(OA.OverrideH2S, 0) = 1 THEN 0 ELSE O.H2S END = 1)
		OR R.TypeID > 4)

GO


/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier DestinationWaitRate info for the specified order
-- Changes
	- 3.7.13 - 5/21/15 - KDA - fix bug which prevented missing rate warning when billable minutes existed but no rate matched
	- 3.7.28 - 2015/05/18 - KDA - add use of @ProducerID, @DriverID, @DriverGroupID parameter
	- 3.7.30 - 2015/05/18 - KDA - remove DriverID
	- 3.9.0  - 2015/08/14 - KDA - honor tblOrderApproval.OverrideOriginMinutes in Minutes computation
	- 3.9.7  - 2015/08/31 - KDA - fix typo where OriginMinutes was used instead of DestMinutes
	- 3.9.21 - 2015/10/03 - KDA - use [viewOrder] O.OriginDriverGroupID instead of O.DriverGroupID (which could be DestDriver.DriverGroupID)
	- 3.10.13.6 - 2015/03/01 - JAE - add TruckTypeID
***********************************/
ALTER FUNCTION [dbo].[fnOrderCarrierDestinationWaitRate](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID
		, Minutes = isnull(OA.OverrideDestMinutes, O.DestMinutes)
		, R.Rate
	FROM viewOrder O
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = @ID
	OUTER APPLY dbo.fnCarrierDestinationWaitRate(O.OrderDate, null, O.DestWaitReasonID, O.CustomerID, O.CarrierID, O.ProductGroupID, O.TruckTypeID, O.OriginDriverGroupID, O.DestinationID, O.DestStateID, O.DestRegionID, O.ProducerID, 1) R
	WHERE O.ID = @ID

GO


/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier FuelSurcharge info for the specified order
-- Changes:
	- 3.7.28 - 2015/06/18 - KDA - add usage of DriverID/DriverGroupID parameters
	- 3.9.21 - 2015/10/03 - KDA - use [viewOrder] O.OriginDriverGroupID instead of O.DriverGroupID (which could be DestDriver.DriverGroupID)
	- 3.10.13.6 - 2015/03/01 - JAE - add TruckTypeID
***********************************/
ALTER FUNCTION [dbo].[fnOrderCarrierFuelSurchargeRate](@ID int) RETURNS TABLE AS RETURN
	SELECT RateID
		, Rate = IncrementAmount * round(Pricediff / nullif(IntervalAmount, 0.0) + .49, 0)
		, RouteMiles
	FROM (
		SELECT TOP 1 RateID = ID, FuelPriceFloor, IntervalAmount, IncrementAmount
			, PriceDiff = dbo.fnMaxDecimal(0, dbo.fnFuelPriceForOrigin(OriginID, OrderDate) - FuelPriceFloor)
			, RouteMiles
		FROM (
			SELECT R.ID, R.FuelPriceFloor, R.IntervalAmount, R.IncrementAmount, O.OrderDate, O.OriginID, RouteMiles = CASE WHEN O.Rejected = 1 THEN 0 ELSE O.ActualMiles END
			FROM dbo.viewOrder O
			CROSS APPLY dbo.fnCarrierFuelSurchargeRate(O.OrderDate, null, O.CustomerID, O.CarrierID, O.ProductGroupID, O.TruckTypeID, O.OriginDriverGroupID, 1) R
			WHERE O.ID = @ID 
		) X
	) X2

GO


/***********************************
-- Date Created: 12 June 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier MinSettlementUnits info for the specified order
-- Changes: 
	- 3.7.28 - 2015/06/18 - KDA - ADDED
	- 3.10.13.6 - 2015/03/01 - JAE - add TruckTypeID
***********************************/
ALTER FUNCTION [dbo].[fnOrderCarrierMinSettlementUnits](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, R.MinSettlementUnits, R.UomID
	FROM viewOrderBase O
	CROSS APPLY dbo.fnCarrierMinSettlementUnits(O.OrderDate, null, O.CustomerID, O.CarrierID, O.ProductGroupID, O.TruckTypeID, O.OriginID, O.OriginStateID, O.DestinationID, O.DestStateID, O.ProducerID, 1) R
	WHERE O.ID = @ID

GO


/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OrderRejectRate info for the specified order
-- Changes:
	- 3.7.28 - 2015/06/18 - KDA - add DriverID/DriverGroupID/ProducerID parameters
	- 3.7.30 - 2015/06/19 - KDA - remove DriverID
	- 3.9.21 - 2015/10/03 - KDA - use [viewOrder] O.OriginDriverGroupID instead of O.DriverGroupID (which could be DestDriver.DriverGroupID)
	- 3.10.13.6 - 2015/03/01 - JAE - add TruckTypeID
***********************************/
ALTER FUNCTION [dbo].[fnOrderCarrierOrderRejectRate](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, R.Rate, R.RateTypeID, R.UomID
	FROM viewOrder O
	CROSS APPLY dbo.fnCarrierOrderRejectRate(O.OrderDate, null, O.RejectReasonID, O.CustomerID, O.CarrierID, O.ProductGroupID, O.TruckTypeID, O.OriginDriverGroupID, O.OriginID, O.OriginStateID, O.OriginRegionID, O.ProducerID, 1) R
	WHERE O.ID = @ID

GO


/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OriginWaitRate info for the specified order
-- Changes
	- 3.7.13 - 5/21/15 - KDA - fix bug which prevented missing rate warning when billable minutes existed but no rate matched
	- 3.7.28 - 2015/06/18 - KDA - add usage of DriverID/DriverGroupID/ProducerID parameters
	- 3.7.30 - 2015/06/18 - KDA - remove DriverID
	- 3.9.0  - 2015/08/14 - KDA - honor tblOrderApproval.OverrideOriginMinutes in Minutes computation
	- 3.9.21 - 2015/10/03 - KDA - use [viewOrder] O.OriginDriverGroupID instead of O.DriverGroupID (which could be DestDriver.DriverGroupID)
	- 3.10.13.6 - 2015/03/01 - JAE - add TruckTypeID
***********************************/
ALTER FUNCTION [dbo].[fnOrderCarrierOriginWaitRate](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, Minutes = isnull(OA.OverrideOriginMinutes, O.OriginMinutes), R.Rate
	FROM viewOrder O
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = @ID
	CROSS APPLY dbo.fnCarrierOriginWaitRate(O.OrderDate, null, O.OriginWaitReasonID, O.CustomerID, O.CarrierID, O.ProductGroupID, O.TruckTypeID, O.OriginDriverGroupID, O.OriginID, O.OriginStateID, O.OriginRegionID, O.ProducerID, 1) R
	WHERE O.ID = @ID

GO


/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RateSheetRangeRate info for the specified order
-- Changes:
	- 3.7.28 - 2015/06/18 - KDA - add usage of DriverID/DriverGroupID/ProducerID parameters
	- 3.7.30 - 2015/06/10 - KDA - remove DriverID
	- 3.9.19.3 - 2015/09/20 - KDA - use OrderApproval.OverrideActualMiles when present
	- 3.9.21 - 2015/10/03 - KDA - use [viewOrder] O.OriginDriverGroupID instead of O.DriverGroupID (which could be DestDriver.DriverGroupID)
	- 3.10.13.6 - 2015/03/01 - JAE - add TruckTypeID
***********************************/
ALTER FUNCTION [dbo].[fnOrderCarrierRateSheetRangeRate](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, RateTypeID, Rate, UomID
	FROM dbo.viewOrder O
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
	CROSS APPLY dbo.fnCarrierRateSheetRangeRate(O.OrderDate, NULL, isnull(OA.OverrideActualMiles, O.ActualMiles), O.CustomerID, O.CarrierID, O.ProductGroupID, O.TruckTypeID, O.OriginDriverGroupID, O.OriginStateID, O.DestStateID, O.OriginRegionID, O.ProducerID, 1) R
	WHERE O.ID = @ID

GO


/****************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RouteRate info for the specified order
-- Changes:
	- 3.7.28 - 2015/06/18 - KDA - add use of DriverID/DriverGroupID parameters
	- 3.7.30 - 2015/06/18 - KDA - remove DriverID
	- 3.9.21 - 2015/10/03 - KDA - use [viewOrder] O.OriginDriverGroupID instead of O.DriverGroupID (which could be DestDriver.DriverGroupID)
	- 3.10.13.6 - 2015/03/01 - JAE - add TruckTypeID
*****************************************************/
ALTER FUNCTION [dbo].[fnOrderCarrierRouteRate](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, RateTypeID, Rate, R.UomID
	FROM dbo.viewOrder O
	CROSS APPLY dbo.fnCarrierRouteRate(O.OrderDate, null, O.OriginID, O.DestinationID, O.CustomerID, O.CarrierID, O.ProductGroupID, O.TruckTypeID, O.OriginDriverGroupID, 1) R
	WHERE O.ID = @ID 

GO


/***********************************
-- Date Created: 12 June 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier SettlementFactor info for the specified order
-- Changes: 
	- 3.7.28 - 2015/06/18 - KDA - ADDED
	- 3.10.13.6 - 2015/03/01 - JAE - add TruckTypeID
***********************************/
ALTER FUNCTION [dbo].[fnOrderCarrierSettlementFactor](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, R.SettlementFactorID
	FROM viewOrderBase O
	CROSS APPLY dbo.fnCarrierSettlementFactor(O.OrderDate, null, O.CustomerID, O.CarrierID, O.ProductGroupID, O.TruckTypeID, O.OriginID, O.OriginStateID, O.DestinationID, O.DestStateID, O.ProducerID, 1) R
	WHERE O.ID = @ID

GO


/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier WaitFeeParameter info for the specified order
-- Changes:
	-- 3.7.28 - 2015/06/18 - KDA - add Producer parameter
	- 3.10.13.6 - 2015/03/01 - JAE - add TruckTypeID
***********************************/
ALTER FUNCTION [dbo].[fnOrderCarrierWaitFeeParameter](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, SubUnitID, RoundingTypeID, OriginThresholdMinutes, DestThresholdMinutes, OriginMinBillableMinutes, OriginMaxBillableMinutes, DestMinBillableMinutes, DestMaxBillableMinutes
	FROM dbo.viewOrderBase O
	CROSS APPLY dbo.fnCarrierWaitFeeParameter(isnull(O.OrderDate, O.DueDate), null, O.CustomerID, O.CarrierID, O.ProductGroupID, O.TruckTypeID, O.OriginStateID, O.DestStateID, O.OriginRegionID, O.ProducerID, 1) R
	WHERE O.ID = @ID 

GO


/***********************************
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
-- Changes:
	- 3.7.31 - 2015/06/19 - KDA - reorder input parameters (move ProviderID down) and add @DriverGroupID parameter
	- 3.9.0  - 2/15/08/14 - KDA - return Approved column
	- 3.9.19.6 - 2015/09/30 - KDA - add OverrideXXX fields from OrderApproval table
	- 3.9.21 - 2015/10/03 - KDA - use [viewOrder] O.OriginDriverGroupID instead of O.DriverGroupID (which could be DestDriver.DriverGroupID)
	- 3.9.34 - 2015/12/16 - BB - Remove deleted orders from the results (DCWEB-851)
	- 3.10.13.6 - 2015/03/01 - JAE - add TruckTypeID
***********************************/
ALTER PROCEDURE [dbo].[spRetrieveOrdersFinancialCarrier]
(
  @StartDate datetime = NULL
, @EndDate datetime = NULL
, @ShipperID int = -1 -- all
, @CarrierID int = -1 -- all
, @ProductGroupID int = -1 -- all 
, @TruckTypeID int = -1 -- all
, @DriverGroupID int = -1 -- all
, @OriginStateID int = -1 -- all 
, @DestStateID int = -1 -- all
, @ProducerID int = -1 -- all
, @BatchID int = NULL  -- either show unbatched (NULL), or the specified batch orders
, @OnlyShipperSettled bit = 0 
) AS BEGIN

	SELECT @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	
	SELECT DISTINCT OE.* 
		, ShipperSettled = cast(CASE WHEN OSS.BatchID IS NOT NULL THEN 1 ELSE 0 END as bit)
		, InvoiceOtherDetailsTSV = dbo.fnOrderCarrierAssessorialDetailsTSV(OE.ID, 0)
		, InvoiceOtherAmountsTSV = dbo.fnOrderCarrierAssessorialAmountsTSV(OE.ID, 0)
		, Approved = cast(ISNULL(OA.Approved, 0) as bit)
		, FinalActualMiles = ISNULL(OA.OverrideActualMiles, OE.ActualMiles)
		, FinalOriginMinutes = ISNULL(OA.OverrideOriginMinutes, OE.OriginMinutes)
		, FinalDestMinutes = ISNULL(OA.OverrideDestMinutes, OE.DestMinutes)
		, FinalH2S = cast(CASE WHEN isnull(OA.OverrideH2S, 0) = 1 THEN 0 ELSE OE.H2S END as bit)
		, FinalChainup = cast(CASE WHEN isnull(OA.OverrideChainup, 0) = 1 THEN 0 ELSE OE.Chainup END as bit)
		, FinalRerouteCount = CASE WHEN isnull(OA.OverrideReroute, 0) = 1 THEN 0 ELSE OE.RerouteCount END
	FROM dbo.viewOrder_Financial_Carrier OE
	LEFT JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = OE.ID AND OSS.BatchID IS NOT NULL
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = OE.ID
	WHERE ID IN (
		SELECT O.ID
		FROM viewOrder O
		JOIN tblProduct P ON P.ID = O.ProductID
		JOIN tblTruck T ON T.ID = O.OriginTruckID
		JOIN tblOrigin OO ON OO.ID = O.OriginID
		JOIN tblDestination D ON D.ID = O.DestinationID
		LEFT JOIN tblOrderSettlementCarrier OSC ON OSC.OrderID = O.ID
		WHERE O.StatusID IN (4)  
		  AND O.DeleteDateUTC IS NULL  -- 3.9.34
		  AND (@ShipperID=-1 OR O.CustomerID=@ShipperID) 
		  AND (@CarrierID=-1 OR O.CarrierID=@CarrierID) 
		  AND (@ProductGroupID=-1 OR P.ProductGroupID=@ProductGroupID) 
		  AND (@TruckTypeID=-1 OR T.TruckTypeID=@TruckTypeID)
		  AND (@DriverGroupID=-1 OR O.OriginDriverGroupID=@DriverGroupID) 
		  AND (@OriginStateID=-1 OR OO.StateID=@OriginStateID) 
		  AND (@DestStateID=-1 OR D.StateID=@DestStateID) 
		  AND (@ProducerID=-1 OR O.ProducerID=@ProducerID) 
		  AND (@StartDate IS NULL OR O.OrderDate >= @StartDate) 
		  AND (@EndDate IS NULL OR O.OrderDate <= @EndDate)
		  AND ((@BatchID IS NULL AND OSC.BatchID IS NULL) OR OSC.BatchID = @BatchID)
	)
	  AND (@OnlyShipperSettled = 0 OR OSS.BatchID IS NOT NULL)
	ORDER BY OE.OriginDepartTimeUTC
END

GO



-- UPDATE SHIPPER FUNCTIONS/STORED PROCEDURES


/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper AssessorialRate info for the specified order
-- Changes:
	- 3.9.19.6 - 2015/09/30 - KDA - honor Order Approval . Override values when computing Rates
	- 3.10.13.6 - 2015/03/01 - JAE - add TruckTypeID
***********************************/
ALTER FUNCTION [dbo].[fnOrderShipperAssessorialRates](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, TypeID, Rate, RateTypeID, UomID
	FROM dbo.viewOrder O
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
	CROSS APPLY dbo.fnShipperAssessorialRates(O.OrderDate, null, null, O.CustomerID, O.ProductGroupID, O.TruckTypeID, O.OriginID, O.DestinationID, O.OriginStateID, O.DestStateID, O.OriginRegionID, O.ProducerID, 1) R
	WHERE O.ID = @ID
	  AND ((R.TypeID = 1 AND CASE WHEN isnull(OA.OverrideChainup, 0) = 1 THEN 0 ELSE O.ChainUp END = 1)
		OR (R.TypeID = 2 AND CASE WHEN isnull(OA.OverrideReroute, 0) = 1 THEN 0 ELSE O.RerouteCount END > 0)
		OR (R.TypeID = 3 AND O.TicketCount > 1)
		OR (R.TypeID = 4 AND CASE WHEN isnull(OA.OverrideH2S, 0) = 1 THEN 0 ELSE O.H2S END = 1)
		OR R.TypeID > 4)

GO


/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper DestinationWaitRate info for the specified order
-- Changes
	- 3.7.13 - 5/21/15 - KDA - fix bug which prevented missing rate warning when billable minutes existed but no rate matched
	- 3.9.0  - 2015/08/14 - KDA - honor tblOrderApproval.OverrideDestMinutes in Minutes computation
	- 3.9.7  - 2015/08/31 - KDA - fix typo where DestMinutes was used instead of DestMinutes
	- 3.10.13.6 - 2015/03/01 - JAE - add TruckTypeID
***********************************/
ALTER FUNCTION [dbo].[fnOrderShipperDestinationWaitRate](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID
		, Minutes = isnull(OA.OverrideDestMinutes, O.DestMinutes)
		, R.Rate
	FROM viewOrderBase O
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = @ID
	OUTER APPLY dbo.fnShipperDestinationWaitRate(O.OrderDate, null, O.DestWaitReasonID, O.CustomerID, O.ProductGroupID, O.TruckTypeID, DestinationID, O.DestStateID, O.DestRegionID, O.ProducerID, 1) R
	WHERE O.ID = @ID

GO


/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper FuelSurcharge info for the specified order
-- Changes: 
	- 3.10.13.6 - 2015/03/01 - JAE - add TruckTypeID
***********************************/
ALTER FUNCTION [dbo].[fnOrderShipperFuelSurchargeRate](@ID int) RETURNS TABLE AS RETURN
	SELECT RateID
		, Rate = IncrementAmount * round(Pricediff / nullif(IntervalAmount, 0.0) + .49, 0)
		, RouteMiles
	FROM (
		SELECT TOP 1 RateID = ID, FuelPriceFloor, IntervalAmount, IncrementAmount
			, PriceDiff = dbo.fnMaxDecimal(0, dbo.fnFuelPriceForOrigin(OriginID, OrderDate) - fuelPriceFloor)
			, RouteMiles
		FROM (
			SELECT R.ID, R.FuelPriceFloor, R.IntervalAmount, R.IncrementAmount, O.OrderDate, O.OriginID, RouteMiles = CASE WHEN O.Rejected = 1 THEN 0 ELSE O.ActualMiles END
			FROM dbo.viewOrder O
			CROSS APPLY dbo.fnShipperFuelSurchargeRate(O.OrderDate, null, O.CustomerID, O.ProductGroupID, O.TruckTypeID, 1) R
			WHERE O.ID = @ID 
		) X
	) X2

GO


/***********************************
-- Date Created: 12 June 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper MinSettlementUnits info for the specified order
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - ADDED
	- 3.10.13.6 - 2015/03/01 - JAE - add TruckTypeID
***********************************/
ALTER FUNCTION [dbo].[fnOrderShipperMinSettlementUnits](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, R.MinSettlementUnits, R.UomID
	FROM viewOrderBase O
	CROSS APPLY dbo.fnShipperMinSettlementUnits(O.OrderDate, null, O.CustomerID, O.ProductGroupID, O.TruckTypeID, O.OriginID, O.OriginStateID, O.DestinationID, O.DestStateID, O.ProducerID, 1) R
	WHERE O.ID = @ID

GO


/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper OrderRejectRate info for the specified order
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - add usage of ProducerID parameter/field as new Best-Match criteria
								- use viewOrderBase vs viewOrder for efficiency reasons
	- 3.10.13.6 - 2015/03/01 - JAE - add TruckTypeID
***********************************/
ALTER FUNCTION [dbo].[fnOrderShipperOrderRejectRate](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, R.Rate, R.RateTypeID, R.UomID
	FROM viewOrderBase O
	CROSS APPLY dbo.fnShipperOrderRejectRate(O.OrderDate, null, O.RejectReasonID, O.CustomerID, O.ProductGroupID, O.TruckTypeID, O.OriginID, O.OriginStateID, O.OriginRegionID, O.ProducerID, 1) R
	WHERE O.ID = @ID

GO


/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper OriginWaitRate info for the specified order
-- Changes
	- 3.7.13 - 5/21/15 - KDA - fix bug which prevented missing rate warning when billable minutes existed but no rate matched
	- 3.9.0  - 2015/08/14 - KDA - honor tblOrderApproval.OverrideOriginMinutes in Minutes computation
	- 3.10.13.6 - 2015/03/01 - JAE - add TruckTypeID
***********************************/
ALTER FUNCTION [dbo].[fnOrderShipperOriginWaitRate](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID
		, Minutes = isnull(OA.OverrideOriginMinutes, O.OriginMinutes)
		, R.Rate
	FROM viewOrderBase O
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = @ID
	CROSS APPLY dbo.fnShipperOriginWaitRate(O.OrderDate, null, O.OriginWaitReasonID, O.CustomerID, O.ProductGroupID, O.TruckTypeID, O.OriginID, O.OriginStateID, O.OriginRegionID, O.ProducerID, 1) R
	WHERE O.ID = @ID

GO


/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper RateSheetRangeRate info for the specified order
-- Changes:
	- 3.9.19.3 - 2015/09/20 - KDA - use OrderApproval.OverrideActualMiles when present
	- 3.10.13.6 - 2015/03/01 - JAE - add TruckTypeID
***********************************/
ALTER FUNCTION [dbo].[fnOrderShipperRateSheetRangeRate](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, RateTypeID, Rate, UomID
	FROM dbo.viewOrder O
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
	CROSS APPLY dbo.fnShipperRateSheetRangeRate(O.OrderDate, null, isnull(OA.OverrideActualMiles, O.ActualMiles), O.CustomerID, O.ProductGroupID, O.TruckTypeID, O.ProducerID, O.OriginStateID, O.DestStateID, O.OriginRegionID, 1) R
	WHERE O.ID = @ID

GO


/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper RouteRate info for the specified order
-- Changes:
	- 3.10.13.6 - 2015/03/01 - JAE - add TruckTypeID
***********************************/
ALTER FUNCTION [dbo].[fnOrderShipperRouteRate](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, RateTypeID, Rate, R.UomID
	FROM dbo.viewOrder O
	CROSS APPLY dbo.fnShipperRouteRate(O.OrderDate, null, O.OriginID, O.DestinationID, O.CustomerID, O.ProductGroupID, O.TruckTypeID, 1) R
	WHERE O.ID = @ID 


GO


/***********************************
-- Date Created: 12 June 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper SettlementFactor info for the specified order
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - ADDED
	- 3.10.13.6 - 2015/03/01 - JAE - add TruckTypeID
***********************************/
ALTER FUNCTION [dbo].[fnOrderShipperSettlementFactor](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, R.SettlementFactorID
	FROM viewOrderBase O
	CROSS APPLY dbo.fnShipperSettlementFactor(O.OrderDate, null, O.CustomerID, O.ProductGroupID, O.TruckTypeID, O.OriginID, O.OriginStateID, O.DestinationID, O.DestStateID, O.ProducerID, 1) R
	WHERE O.ID = @ID

GO


/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper WaitFeeParameter info for the specified order
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - add ProducerID parameter/field as new Best-Match criteria
								- use viewOrderBase instead of viewOrder for efficiency reasons
	- 3.10.13.6 - 2015/03/01 - JAE - add TruckTypeID
***********************************/
ALTER FUNCTION [dbo].[fnOrderShipperWaitFeeParameter](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, SubUnitID, RoundingTypeID, OriginThresholdMinutes, DestThresholdMinutes, OriginMinBillableMinutes, OriginMaxBillableMinutes, DestMinBillableMinutes, DestMaxBillableMinutes
	FROM dbo.viewOrderBase O
	CROSS APPLY dbo.fnShipperWaitFeeParameter(isnull(O.OrderDate, O.DueDate), null, O.CustomerID, O.ProductGroupID, O.TruckTypeID, O.OriginStateID, O.DestStateID, O.OriginRegionID, O.ProducerID, 1) R
	WHERE O.ID = @ID 

GO


/***********************************
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
-- Changes
	- 3.8.11 - 2015/07/28 - KDA - remove OriginShipperRegion field
	- 3.9.0  - 2/15/08/14 - KDA - return Approved column
	- 3.9.19.6 - 2015/09/30 - KDA - add OverrideXXX fields from OrderApproval table
	- 3.9.34 - 2015/12/16 - BB - Remove deleted orders from the results (DCWEB-851)
	- 3.10.13.6 - 2015/03/01 - JAE - add TruckTypeID
***********************************/
ALTER PROCEDURE [dbo].[spRetrieveOrdersFinancialShipper]
(
  @StartDate datetime = NULL
, @EndDate datetime = NULL
, @ShipperID int = -1 -- all customers
, @ProductGroupID int = -1 -- all product groups
, @TruckTypeID int = -1 -- all truck types
, @ProducerID int = -1 -- all
, @OriginStateID int = -1 -- all 
, @DestStateID int = -1 -- all
, @BatchID int = NULL  -- either show unbatched (NULL), or the specified batch orders
) AS BEGIN

	SELECT @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	
	SELECT DISTINCT OE.* 
		, InvoiceOtherDetailsTSV = dbo.fnOrderShipperAssessorialDetailsTSV(OE.ID, 0)
		, InvoiceOtherAmountsTSV = dbo.fnOrderShipperAssessorialAmountsTSV(OE.ID, 0)
		, Approved = cast(ISNULL(OA.Approved, 0) as bit)
		, FinalActualMiles = ISNULL(OA.OverrideActualMiles, OE.ActualMiles)
		, FinalOriginMinutes = ISNULL(OA.OverrideOriginMinutes, OE.OriginMinutes)
		, FinalDestMinutes = ISNULL(OA.OverrideDestMinutes, OE.DestMinutes)
		, FinalH2S = cast(CASE WHEN isnull(OA.OverrideH2S, 0) = 1 THEN 0 ELSE OE.H2S END as bit)
		, FinalChainup = cast(CASE WHEN isnull(OA.OverrideChainup, 0) = 1 THEN 0 ELSE OE.Chainup END as bit)
		, FinalRerouteCount = CASE WHEN isnull(OA.OverrideReroute, 0) = 1 THEN 0 ELSE OE.RerouteCount END
	FROM viewOrder_Financial_Shipper OE
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = OE.ID
	WHERE ID IN (
		SELECT O.ID
		FROM viewOrder O
		JOIN tblProduct P ON P.ID = O.ProductID
		JOIN tblTruck T ON T.ID = O.OriginTruckID
		JOIN tblOrigin OO ON OO.ID = O.OriginID
		JOIN tblDestination D ON D.ID = O.DestinationID
		LEFT JOIN tblOrderSettlementShipper ISC ON ISC.OrderID = O.ID
		WHERE O.StatusID IN (4)  
		  AND O.DeleteDateUTC IS NULL  -- 3.9.34
		  AND (@ShipperID=-1 OR O.CustomerID=@ShipperID) 
		  AND (@ProductGroupID=-1 OR P.ProductGroupID=@ProductGroupID) 
		  AND (@TruckTypeID=-1 OR T.TruckTypeID=@TruckTypeID)
		  AND (@ProducerID=-1 OR O.ProducerID=@ProducerID) 
		  AND (@OriginStateID=-1 OR OO.StateID=@OriginStateID) 
		  AND (@DestStateID=-1 OR D.StateID=@DestStateID) 
		  AND (@StartDate IS NULL OR O.OrderDate >= @StartDate) 
		  AND (@EndDate IS NULL OR O.OrderDate <= @EndDate)
		  AND ((@BatchID IS NULL AND ISC.BatchID IS NULL) OR ISC.BatchID = @BatchID)
	)
	ORDER BY OE.OriginDepartTimeUTC
END 

GO



COMMIT
SET NOEXEC OFF