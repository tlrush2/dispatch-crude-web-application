/*  
	-- fix mising OriginTank sync logic issues when order created after driver initial sync (required logout/login)
*/ 
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.6.24'
SELECT  @NewVersion = '2.6.25'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

/*******************************************/
-- Date Created: 5 Apr 2014
-- Author: Kevin Alons
-- Purpose: return OriginTank data for Driver App sync
/*******************************************/
ALTER FUNCTION [dbo].[fnOriginTank_DriverApp]( @DriverID int, @LastChangeDateUTC datetime ) RETURNS  TABLE AS
RETURN 
	SELECT DISTINCT OT.ID
		, OT.OriginID
		, OT.TankNum
		, OT.TankDescription
		, OT.CreateDateUTC
		, OT.CreatedByUser
		, OT.LastChangeDateUTC
		, OT.LastChangedByUser
		, OT.DeleteDateUTC
		, OT.DeletedByUser
	FROM dbo.tblOriginTank OT
	JOIN dbo.tblOrder O ON O.OriginID = OT.OriginID
	LEFT JOIN dbo.tblPrintStatus DPS ON DPS.ID = O.DeliverPrintStatusID
	CROSS JOIN (SELECT DATEADD(second, -5, @LastChangeDateUTC) AS LCD) LCD
	WHERE (StatusID IN (2, 7, 8) -- Dispatched, Accepted, Picked Up
		-- or status = Delivered and the driver still has access to the record
		OR (StatusID = 3 AND isnull(DPS.IsCompleted, 0) = 0))
	  AND O.DriverID = @DriverID
	  AND (@LastChangeDateUTC IS NULL 
		OR O.CreateDateUTC >= LCD.LCD
		OR OT.CreateDateUTC >= LCD.LCD
		OR OT.LastChangeDateUTC >= LCD.LCD
		OR OT.DeleteDateUTC >= LCD.LCD)

GO

/*******************************************/
-- Date Created: 5 Apr 2014
-- Author: Kevin Alons
-- Purpose: return OriginTankStrapping data for Driver App sync
/*******************************************/
ALTER FUNCTION [dbo].[fnOriginTankStrapping_DriverApp]( @DriverID int, @LastChangeDateUTC datetime ) RETURNS  TABLE AS
RETURN 
	SELECT DISTINCT OTS.*
/*		, OTS.OriginTankID
		, OTS.Feet
		, OTS.Inches
		, OTS.Q
		, OTS.BPQ
		, OTS.IsMinimum
		, OTS.IsMaximum
		, OTS.CreateDateUTC
		, OTS.CreatedByUser
		, OTS.LastChangeDateUTC
		, OTS.LastChangedByUser */
		, cast(CASE WHEN OTSD.ID IS NULL THEN 0 ELSE 1 END as bit) AS IsDeleted
	FROM dbo.tblOriginTankStrapping OTS
	JOIN dbo.tblOriginTank OT ON OT.ID = OTS.OriginTankID
	JOIN dbo.tblOrder O ON O.OriginID = OT.OriginID
	LEFT JOIN dbo.tblOriginTankStrappingDeleted OTSD ON OTSD.ID = OTS.ID
	LEFT JOIN dbo.tblPrintStatus DPS ON DPS.ID = O.DeliverPrintStatusID
	CROSS JOIN (SELECT DATEADD(second, -5, @LastChangeDateUTC) AS LCD) LCD
	WHERE (StatusID IN (2, 7, 8) -- Dispatched, Accepted, Picked Up
		-- or status = Delivered and the driver still has access to the record
		OR (StatusID = 3 AND isnull(DPS.IsCompleted, 0) = 0))
	  AND O.DriverID = @DriverID
	  AND (@LastChangeDateUTC IS NULL 
		OR O.CreateDateUTC >= LCD.LCD
		OR OT.CreateDateUTC >= LCD.LCD
		OR OT.LastChangeDateUTC >= LCD.LCD
		OR OTS.CreateDateUTC >= LCD.LCD
		OR OTS.LastChangeDateUTC >= LCD.LCD
		OR OTSD.DeleteDateUTC >= LCD.LCD)

GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details into a single view
/***********************************/
ALTER VIEW [dbo].[viewOrderExportFull_Reroute] AS 
	SELECT OE.* 
		, dbo.fnRerouteDetails(OE.ID, 'PreviousDestinations', '<br/>') AS PreviousDestinations
		, dbo.fnRerouteDetails(OE.ID, 'RerouteUsers', '<br/>') AS RerouteUsers
		, dbo.fnRerouteDetails(OE.ID, 'RerouteDates', '<br/>') AS RerouteDates
		, dbo.fnRerouteDetails(OE.ID, 'RerouteNotes', '<br/>') AS RerouteNotes
	FROM dbo.viewOrderExportFull OE
	WHERE OE.DeleteDateUTC IS NULL	

GO

COMMIT
SET NOEXEC OFF